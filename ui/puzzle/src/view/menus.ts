import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {bind} from '../util';
import {Controller} from "../interfaces";

export default function renderMenuBox(ctrl: Controller): VNode | null {
  return ctrl.vm.menuIsOpen ? h('div.sub-box', [
    h('div.sub-box__title', '电脑分析'),
    h('div.sub-box__setting', [
      boolSetting({
        name: '箭头指示最佳着',
        id: 'shapes',
        checked: ctrl.vm.showAutoShapes(),
        change: ctrl.toggleAutoShapes
      }, ctrl.redraw),
      boolSetting({
        name: '局面指示器',
        id: 'gauge',
        checked: ctrl.showGauge(),
        change: ctrl.toggleGauge
      }, ctrl.redraw)
    ])
  ]) : null;
}

export interface BoolSetting {
  name: string,
  title?: string,
  id: string,
  checked: boolean;
  disabled?: boolean;
  change(v: boolean): void;
}

export function boolSetting(o: BoolSetting, redraw: () => void) {
  const fullId = 'abset-' + o.id;
  return h('div.setting.' + fullId, o.title ? {
    attrs: { title: o.title }
  } : {}, [
    h('label', { attrs: { 'for': fullId } }, o.name),
    h('div.switch', [
      h('input#' + fullId + '.cmn-toggle', {
        attrs: {
          type: 'checkbox',
          checked: o.checked
        },
        hook: bind('change', e => o.change((e.target as HTMLInputElement).checked), redraw)
      }),
      h('label', { attrs: { 'for': fullId } })
    ])
  ]);
}
