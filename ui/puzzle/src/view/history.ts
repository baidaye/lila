import { h, thunk } from 'snabbdom';
import { VNode } from 'snabbdom/vnode';

import { Controller } from '../interfaces';

const historySize = 15;

function render(ctrl: Controller): VNode {
  const data = ctrl.getData();
  const slots: any[] = [];
  for (let i = 0; i < historySize; i++) slots[i] = data.user.recent[i] || null;
  return h('div.puzzle__history', slots.map(function(s) {
    if (s) return h('a', {
      class: {
        current: data.puzzle.id === s[0],
        win: s[1] >= 0,
        loss: s[1] < 0
      },
      attrs: { 'data-id': s[0], href: ctrl.vm.theme ? s[3] : ('/training/' + s[0]) }
    }, ctrl.vm.theme ? (s[1] >= 0 ? '✓' : '✗') : (s[1] > 0 ? '+' + s[1] : '−' + (-s[1])));
  }));
}

export default function(ctrl) {
  const data = ctrl.getData();
  if (!data.user || (!data.rated && !ctrl.vm.theme)) return;
  return thunk('div.puzzle__history', render, [ctrl, ctrl.recentHash()]);
};
