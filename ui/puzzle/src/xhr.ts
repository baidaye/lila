import { Vm } from './interfaces';
// do NOT set mobile API headers here
// they trigger a compat layer
export function round(puzzleId, win, lines, vm: Vm) {
  let data = {
    win: win ? 1 : 0,
    seconds: vm.puzzleSeconds
  };

  lines.forEach(function (ln, i) {
    data['lines[' + i + '].uci'] = ln.uci;
    data['lines[' + i + '].san'] = ln.san;
    data['lines[' + i + '].fen'] = ln.fen;
  });

  if (vm.homework) {
    data['metaData.homeworkId'] = vm.homework.id;
  }
  if (vm.task) {
    data['metaData.taskId'] = vm.task.id;
  }
  if (vm.capsule) {
    data['metaData.capsuleId'] = vm.capsule.id;
  }

  if(vm.theme) {
    data['source'] = 'theme';
    data['search'] = location.search;
  } else if (vm.homework) {
    data['source'] = 'homework';
  } else if (vm.task) {
    data['source'] = 'task';
  } else if (vm.puzzleErrors) {
    data['source'] = 'error';
  } else if (vm.capsule) {
    data['source'] = 'capsule';
  } else  {
    data['source'] = 'puzzle';
  }

  return $.ajax({
    method: 'POST',
    url: '/training/' + puzzleId + '/round2',
    data: data
  });
}

export function vote(puzzleId, v) {
  return $.ajax({
    method: 'POST',
    url: '/training/' + puzzleId + '/vote',
    data: {
      vote: v ? 1 : 0
    }
  });
}
export function nextPuzzle() {
  return $.ajax({
    url: '/training/new'
  });
}

export function nextThemePuzzle(puzzleId) {
  return $.ajax({
    url: '/training/theme/' + puzzleId +  '/new' + location.search
  });
}

export function nextErrorPuzzle(puzzleId, isDelete) {
  return $.ajax({
    url: '/training/errors/' + puzzleId +  '/new' + location.search + (isDelete ? '&d=' + isDelete : '')
  });
}

export function nextCapsulePuzzle(capsuleId, puzzleId) {
  return $.ajax({
    url: '/training/capsule/' + capsuleId +  '/new?lastPlayed=' + puzzleId
  });
}

export function nextHomeworkPuzzle(homeworkId, puzzleId) {
  return $.ajax({
    url: '/training/homework/' + homeworkId +  '/new?lastPlayed=' + puzzleId
  });
}

export function nextTaskPuzzle(taskId, puzzleId) {
  return $.ajax({
    url: '/training/task/' + taskId +  '/new?lastPlayed=' + puzzleId
  });
}

export function like(puzzleId, v) {
  return $.ajax({
    method: 'POST',
    url: '/training/' + puzzleId + '/like',
    data: {
      like: v
    }
  });
}
export function setTag(puzzleId, v) {
  return $.ajax({
    method: 'POST',
    url: '/training/' + puzzleId + '/setTag',
    data: {
      tags: v
    }
  });
}
