import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import DemonstrateCtrl from '../ctrl';
import * as gridHacks from './gridHacks';
import board from './board';
import tools from './tools';
import {dataIcon, bind} from "./util";

export default function (ctrl: DemonstrateCtrl): VNode {
  return h('main.demonstrate', {
    class: {
      single: !ctrl.isShowAssist
    },
    hook: {
      postpatch(_, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
      }
    }
  }, [
    ctrl.isShowAssist ? null : h('aside.demonstrate__side', [
      h('h2', [
        h('a', {
          hook: bind('click', () => ctrl.showIntro = !ctrl.showIntro, ctrl.redraw)
        }, [ '功能介绍', h('i', {attrs: dataIcon('R')}) ])
      ]),
      ctrl.showIntro ? h('ul',[
        h('li', '1. 支持 标准局面 和 非标准局面；'),
        h('li', '2. 支持右键标记位置，拖动划线，Shift、Alt切换颜色；'),
        h('li', '3. 局面前进和后退，支持鼠标滚轮浏览；'),
        h('li', '4. 可以开启备用棋子，直接编辑局面；'),
        h('li', '5. 可以开启辅棋盘，在一个页面上进行对比演示；'),
        h('li', '6. 请注意：局面和棋谱不在服务器保存，刷新页面会全部清空。')
      ]) : null
    ]),
    h('div.demonstrate__boards',
      ctrl.boards.filter(bd => !bd.removed).map(function (bd) {
        return board(ctrl, bd);
      })
    ),
    h('div.demonstrate__tools', tools(ctrl, ctrl.getActiveBoard()))
  ]);
}

