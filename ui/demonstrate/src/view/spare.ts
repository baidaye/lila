import {h} from 'snabbdom';
import {dragNewPiece} from 'chessground/drag';
import {eventPosition} from 'chessground/util';
import * as cg from 'chessground/types';
import DemonstrateCtrl from "../ctrl";
import {Board} from "../interfaces";

export function sparePieces(ctrl: DemonstrateCtrl, board: Board, color: string, position: string) {
  const spares = ['pointer'].concat(['king', 'queen', 'rook', 'bishop', 'knight', 'pawn']).concat('trash');
  return h(`div.spare.spare-${position}.spare-${color}`, spares.map(function (spare) {
    return h('div.no-square', {
      class: {
        'selected-square': spare === board.spare.piece && (spare === 'pointer' || spare === 'trash' || color === board.spare.color) && (
          Object.keys(board.chessground).length === 0 ||
          !board.chessground.state.draggable.current ||
          !board.chessground.state.draggable.current.newPiece
        ),
        'pointer': spare === 'pointer',
        'trash': spare === 'trash'
      },
      hook: {
        insert(vnode) {
          let el = vnode.elm as HTMLElement;
          el.addEventListener('mousedown', onSelectSparePiece(ctrl, board, spare, color, 'mouseup'));
          el.addEventListener('touchstart', onSelectSparePiece(ctrl, board, spare, color, 'touchend'));
          el.addEventListener('touchmove', function (e) {
            board.lastTouchMovePos = eventPosition(e as cg.MouchEvent);
          });
        }
      }
    }, [
      h('div', h(`piece.${color}.${spare}`, {
        attrs: {
          'data-color': color,
          'data-role': spare
        }
      }))
    ]);
  }));
}

function onSelectSparePiece(ctrl: DemonstrateCtrl, board: Board, piece: string, color: string, upEvent: string) {
  return function (e) {
    e.preventDefault();
    if (piece === 'pointer' || piece === 'trash') {
      ctrl.setSpareSelected(board, piece, color);
    } else {
      ctrl.setSpareSelected(board, 'pointer', color);
      dragNewPiece(board.chessground.state, {
        color: color as cg.Color,
        role: piece as cg.Role
      }, e, true);

      document.addEventListener(upEvent, function (e) {
        let ev = e as cg.MouchEvent;
        let eventPos = eventPosition(ev) || board.lastTouchMovePos;
        if (eventPos && board.chessground && board.chessground.getKeyAtDomPos(eventPos)) {
          ctrl.setSpareSelected(board, 'pointer', color);
        } else {
          ctrl.setSpareSelected(board, piece, color);
        }
      }, { once: true });
    }
  };
}
