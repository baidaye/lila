import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {bind, dataIcon} from "./util";
import DemonstrateCtrl from "../ctrl";
import {Board} from "../interfaces";
import controls from "./controls";

export default function render(ctrl: DemonstrateCtrl, board: Board): VNode[] {
  return [
    tabs(ctrl),
    h('div.action-menu', [
      h('div.action-menu__tools', [
        h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.loadSituation(board);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.loadSituation(board);
              });
            }
          },
          attrs: dataIcon('')
        }, '载入局面'),
        h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.toggleOrientation(board);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.toggleOrientation(board);
              });
            }
          },
          attrs: dataIcon('B')
        }, '翻转棋盘'),
        h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.clearBoard(board);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.clearBoard(board);
              });
            }
          },
          attrs: dataIcon('q')
        }, '清空棋盘'),
        h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.initBoard(board);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.initBoard(board);
              });
            }
          },
          attrs: dataIcon('C')
        }, '起始位置'),
        ctrl.isShowAssist ? h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.copyBoard(ctrl.mainIdx, ctrl.assistIdx);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.copyBoard(ctrl.mainIdx, ctrl.assistIdx);
              });
            }
          },
          attrs: dataIcon('X')
        }, '复制主棋盘') : null,
        ctrl.isShowAssist ? h('a.button.button-empty', {
          hook: {
            insert(vnode) {
              $(vnode.elm as HTMLElement).on('click', () => {
                ctrl.copyBoard(ctrl.assistIdx, ctrl.mainIdx);
              });
            },
            postpatch: (_, vnode) => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.copyBoard(ctrl.assistIdx, ctrl.mainIdx);
              });
            }
          },
          attrs: dataIcon('Y')
        }, '复制辅棋盘') : null
      ]),
      h('h2', '棋盘设置'),
      boolSetting({
        id: 'showAssist',
        name: '显示辅助棋盘',
        checked: ctrl.isShowAssist,
        change: function (checked) {
          ctrl.showAssist(checked);
        }
      }),
      boolSetting({
        id: 'showSpare',
        name: '显示备用棋子',
        checked: board.isShowSpare,
        change: function (checked) {
          ctrl.showSpare(board, checked);
        }
      }),
      boolSetting({
        id: 'showCoord',
        name: '显示棋盘坐标',
        checked: board.isShowCoord,
        change: function (checked) {
          ctrl.showCoordinates(board, checked);
        }
      }),
      h('div.setting.zoom', [
        h('label', '棋盘大小'),
        h('div.slider', {
          hook: { insert: vnode => makeSlider(ctrl, vnode.elm as HTMLElement) }
        })
      ]),
      h('div.setting.fen', [
        h('label', 'FEN'),
        h('div.input', [
          h('input', {
            attrs: {
              id: `board-${board.idx}-fen`,
              type: 'text',
              value: board.fen,
              placeholder: 'FEN'
            },
            hook: {
              insert(vnode) {
                $(vnode.elm as HTMLElement).on('keydown', (e) => {
                  if (e.keyCode == 13){
                    ctrl.changeFen(board, (e.target as HTMLInputElement).value);
                  }
                });
              },
              postpatch: (_, vnode) => {
                (vnode.elm as HTMLInputElement).value = board.fen;
                $(vnode.elm as HTMLElement).off('keydown').on('keydown', (e) => {
                  if (e.keyCode == 13){
                    ctrl.changeFen(board, (e.target as HTMLInputElement).value);
                  }
                });
              }
            }
          })
        ])
      ])
    ]),
    controls(ctrl, board)
  ];
}

function tabs(ctrl: DemonstrateCtrl) {
  return h('div.tabs', [
    /*
    h('div.tab', [
      h('input', {
        attrs: {
          id: 'ctrlAll',
          type: 'checkbox',
          checked: ctrl.ctrlAll
        },
        hook: bind('change', (e: Event) => {
          ctrl.ctrlAll = (e.target as HTMLInputElement).checked;
        })
      }),
      h('label', { attrs: { 'for': 'ctrlAll' } }, '所有')
    ]),*/
    h('div.tab'),
    ...ctrl.boards.filter(bd => !bd.removed).map(function (bd) {
      return h('div.tab', {
        class: {
          active: bd.active
        },
        hook: bind('click', () => {
          ctrl.setActiveBoard(bd.idx);
        })
      }, bd.name);
    })
  ])
}

interface BoolSetting {
  name: string,
  title?: string,
  id: string,
  checked: boolean;
  disabled?: boolean;
  change(v: boolean): void;
}

function boolSetting(o: BoolSetting) {
  const fullId = 'abset-' + o.id;
  return h('div.setting.' + fullId, o.title ? {
    attrs: {title: o.title}
  } : {}, [
    h('label', {attrs: {'for': fullId}}, o.name),
    h('div.switch', [
      h('input#' + fullId + '.cmn-toggle', {
        attrs: {
          type: 'checkbox',
          checked: o.checked
        },
        hook: {
          insert(vnode) {
            $(vnode.elm as HTMLElement).on('change', (e) => {
              o.change((e.target as HTMLInputElement).checked)
            });
          },
          postpatch: (_, vnode) => {
            (vnode.elm as HTMLInputElement).checked = o.checked;
            $(vnode.elm as HTMLElement).off('change').on('change', (e) => {
              o.change((e.target as HTMLInputElement).checked)
            });
          }
        }
      }),
      h('label', {attrs: {'for': fullId}})
    ])
  ]);
}

function makeSlider(ctrl: DemonstrateCtrl, el: HTMLElement) {
  window.lichess.slider().done(() => {
    $(el).slider({
      orientation: 'horizontal',
      min: 100,
      max: 250,
      range: 'min',
      step: 1,
      value: ctrl.readZoom(),
      slide: (_: any, ui: any) => ctrl.setZoom(ui.value)
    });
  });
}
