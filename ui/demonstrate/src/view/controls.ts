import {h} from 'snabbdom';
import DemonstrateCtrl from '../ctrl';
import {Board} from "../interfaces";

function dataAct(e) {
  return e.target.getAttribute('data-act') || e.target.parentNode.getAttribute('data-act');
}

function jumpButton(icon, effect, enabled: boolean) {
  return h('button.fbt', {
    class: { disabled: !enabled },
    attrs: {
      'data-act': effect,
      'data-icon': icon
    }
  });
}

export default function(ctrl: DemonstrateCtrl, board: Board) {
  return h('div.demonstrate__controls.analyse-controls', {
    hook: {
      insert(vnode) {
        $(vnode.elm as HTMLElement).on('click', (e) => {
          const action = dataAct(e);
          if (action === 'prev') ctrl.prev(board);
          else if (action === 'next') ctrl.next(board);
          else if (action === 'first') ctrl.first(board);
          else if (action === 'last') ctrl.last(board);
        });
      },
      postpatch: (_, vnode) => {
        $(vnode.elm as HTMLElement).off('click').on('click', (e) => {
          const action = dataAct(e);
          if (action === 'prev') ctrl.prev(board);
          else if (action === 'next') ctrl.next(board);
          else if (action === 'first') ctrl.first(board);
          else if (action === 'last') ctrl.last(board);
        });
      }
    }
  }, [
    h('div.jumps', [
      jumpButton('W', 'first', ctrl.canFirst(board)),
      jumpButton('Y', 'prev', ctrl.canPrev(board)),
      jumpButton('X', 'next', ctrl.canNext(board)),
      jumpButton('V', 'last', ctrl.canLast(board))
    ])
  ]);
}

