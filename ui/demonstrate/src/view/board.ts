import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Chessground} from 'chessground';
import * as cg from 'chessground/types';
import * as cgUtil from 'chessground/util';
import {Config} from 'chessground/config'
import DemonstrateCtrl from '../ctrl';
import * as spare from "./spare";
import {Board} from "../interfaces";
import {bindMobileMousedown, onInsert, bind, hasTouchEvents} from "./util";

export default function render(ctrl: DemonstrateCtrl, board: Board): VNode {
  let color = Object.keys(board.chessground).length > 0 ? board.chessground.state.orientation : 'white';
  let opposite = cgUtil.opposite(color as cg.Color);
  return h(`div.demonstrate__board board_${board.idx}_${ctrl.boards.map(b => b.isShowSpare).join('_')}`, {
      class: { active: board.active },
      attrs: { style: 'cursor: ' + makeCursor(board) },
      hook: onInsert(el => {
        bindMobileMousedown(el, () => {
          ctrl.setActiveBoard(board.idx);
        });
      })
    }, [
    board.isShowSpare ? spare.sparePieces(ctrl, board, opposite, 'top') : null,
    h('div.main-board', {
      hook: hasTouchEvents ? undefined : bind('wheel', e => wheel(ctrl, board, e as WheelEvent))
    }, [
      chessground(ctrl, board)
    ]),
    board.isShowSpare ? spare.sparePieces(ctrl, board, color, 'bottom') : null
  ])
}

function wheel(ctrl: DemonstrateCtrl, board: Board, e: WheelEvent) {
  const target = e.target as HTMLElement;
  if (target.tagName !== 'PIECE' && target.tagName !== 'SQUARE' && target.tagName !== 'CG-BOARD') return;
  e.preventDefault();
  if(!board.active) {
    ctrl.setActiveBoard(board.idx);
  }
  if (e.deltaY > 0) ctrl.next(board);
  else if (e.deltaY < 0) ctrl.prev(board);
  return false;
}

function makeCursor(board: Board) {
  let spare = board.spare;
  let piece = spare.piece;
  if (piece === 'pointer') return 'pointer';
  let name = piece === 'trash' ? 'trash' : `${spare.color}-${piece}`;
  let url = window.lichess.assetUrl(`cursors/${name}.cur`);
  return 'url(' + url + '), default !important';
}

function chessground(ctrl: DemonstrateCtrl, board: Board): VNode {
  return h('div.cg-wrap', {
    hook: {
      insert: vnode => {
        board.chessground = Chessground((vnode.elm as HTMLElement), makeConfig(ctrl, board));
        const el = vnode.elm as HTMLElement;
        bindEvents(el, ctrl, board);
      },
      destroy: _ => {
        board.chessground.destroy()
      }
    }
  });
}

function bindEvents(el: HTMLElement, ctrl: DemonstrateCtrl, board: Board) {
  let handler = onMouseEvent(ctrl, board);
  ['touchstart', 'touchmove', 'mousedown', 'mousemove', 'contextmenu'].forEach(function (e) {
    el.addEventListener(e, handler);
  });
}

function onMouseEvent(ctrl: DemonstrateCtrl, board: Board) {
  return function (e) {
    let piece = board.spare.piece;
    let color = board.spare.color;

    // do not generate corresponding mouse event
    // (https://developer.mozilla.org/en-US/docs/Web/API/Touch_events/Supporting_both_TouchEvent_and_MouseEvent)
    if (piece !== 'pointer' && e.cancelable !== false && (e.type === 'touchstart' || e.type === 'touchmove')) {
      e.preventDefault();
    }

    if (isLeftClick(e) || e.type === 'touchstart' || e.type === 'touchmove') {
      if (
        piece === 'pointer' ||
        (
          Object.keys(board.chessground).length > 0 &&
          board.chessground.state.draggable.current &&
          board.chessground.state.draggable.current.newPiece
        )
      ) return;

      let ev = e as cg.MouchEvent;
      let pos = cgUtil.eventPosition(ev);
      let key = pos ? board.chessground.getKeyAtDomPos(pos) : undefined;
      if (!key) return;
      if (e.type === 'mousedown' || e.type === 'touchstart') {
        board.downKey = key;
      }
      if (piece === 'trash') {
        deleteOrHidePiece(ctrl, board, key, e);
      } else {
        let existingPiece = board.chessground.state.pieces[key];
        if (
          (e.type === 'mousedown' || e.type === 'touchstart') &&
          existingPiece &&
          color === existingPiece.color &&
          piece === existingPiece.role
        ) {
          deleteOrHidePiece(ctrl, board, key, e);
          board.placeDelete = true;

          let endEvents = {mousedown: 'mouseup', touchstart: 'touchend'};
          document.addEventListener(endEvents[e.type], function () {
            board.placeDelete = false;
          }, {once: true});
        } else if (
          !board.placeDelete &&
          (e.type === 'mousedown' || e.type === 'touchstart' || key !== board.lastKey)
        ) {

          let pieces = {};
          pieces[key] = {
            color: color,
            role: piece
          };
          board.chessground.setPieces(pieces);
          ctrl.onGroundChange(board);
          board.chessground.cancelMove();
        }
      }
      board.lastKey = key;
    } else if (isRightClick(e)) {
      if (piece !== 'pointer') {
        board.chessground.state.drawable.current = undefined;
        board.chessground.state.drawable.shapes = [];
        if (e.type === 'contextmenu' && 'pointer' !== piece && 'trash' !== piece) {
          board.chessground.cancelMove();
          ctrl.setSpareSelected(board, piece, cgUtil.opposite(color as cg.Color));
          ctrl.redraw();
        }
      }
    }
  };
}

function deleteOrHidePiece(ctrl: DemonstrateCtrl, board: Board, key: cg.Key, e: MouseEvent) {
  if (e.type === 'touchstart') {
    if (board.chessground.state.pieces[key]) {
      let dragCurrent = board.chessground.state.draggable.current;
      if (dragCurrent) {
        let element = dragCurrent.element;
        if (element) {
          (element as HTMLElement).style.display = 'none';
        }
      }
      board.chessground.cancelMove();
    }
    document.addEventListener('touchend', function () {
      deletePiece(ctrl, board, key);
    }, {once: true});
  } else if (e.type === 'mousedown' || key !== board.downKey) {
    deletePiece(ctrl, board, key);
  }
}

function deletePiece(ctrl: DemonstrateCtrl, board: Board, key: cg.Key) {
  board.chessground.setPieces({
    [key]: undefined
  });
  ctrl.onGroundChange(board);
}

function isLeftButton(e: MouseEvent) {
  return e.buttons === 1 || e.button === 1;
}

function isLeftClick(e: MouseEvent) {
  return isLeftButton(e) && !e.ctrlKey;
}

function isRightClick(e: MouseEvent) {
  return cgUtil.isRightButton(e) || (e.ctrlKey && isLeftButton(e));
}

function makeConfig(ctrl: DemonstrateCtrl, board: Board): Config {
  const pref = ctrl.pref;
  return {
    fen: board.fen,
    orientation: board.orientation as cg.Color,
    coordinates: board.isShowCoord,
    addPieceZIndex: pref.is3d,
    autoCastle: false,
    movable: {
      free: true,
      color: 'both'
    },
    animation: {
      duration: pref.animationDuration
    },
    premovable: {
      enabled: false
    },
    drawable: {
      enabled: true
    },
    draggable: {
      showGhost: true,
      deleteOnDropOff: true
    },
    selectable: {
      enabled: false
    },
    highlight: {
      lastMove: false
    },
    events: {
      change: function () {
        ctrl.onGroundChange(board);
        if(board.idx !== ctrl.getActiveBoard().idx) {
          ctrl.setActiveBoard(board.idx);
        }
      }
    }
  };
}
