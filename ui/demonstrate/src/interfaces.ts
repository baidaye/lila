import {Pref} from "../../round/src/interfaces";
import { Api as CgApi } from 'chessground/api';

export const emptyFen = '8/8/8/8/8/8/8/8 w - -';
export const initialFen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

export interface DemonstrateOpts {
  data: DemonstrateData;
  pref: Pref;
  userId: string;
  socketSend: SocketSend;
  element: Element;
}

export interface DemonstrateData {
  fen?: string;
  notAccept: boolean;
}

export interface Board {
  idx: number;
  name: string;
  active: boolean;
  removeable: boolean;
  removed: boolean;
  chessground: CgApi;
  spare: Spare;

  fen: string;
  orientation: string;
  isShowCoord: boolean;
  isShowSpare: boolean;

  history: string[];
  historyActiveIdx: number;

  // view ctrl
  downKey?: string;
  lastKey?: string;
  placeDelete?: boolean;
  lastTouchMovePos?: any;
}

export interface Spare {
  color: string;
  piece: string;
}

export type Redraw = () => void;
