const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function get() {
  return $.ajax({
    url: `/get`,
    headers: headers
  });
}

export function post() {
  return $.ajax({
    method: 'post',
    url: '/post',
    headers: headers,
    data: { }
  });
}
