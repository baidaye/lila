import Socket from "./socket";
import * as cgUtil from 'chessground/util';
import { Api as CgApi } from 'chessground/api';
import {Pref} from "../../round/src/interfaces";
import { DemonstrateOpts, DemonstrateData, Redraw, Board, initialFen, emptyFen } from './interfaces';

export default class DemonstrateCtrl {
  data: DemonstrateData;
  pref: Pref;
  socket: Socket;
  boards: Board[] = [];
  showIntro: boolean = true;
  ctrlAll: boolean = false;
  isShowAssist: boolean = false;
  mainIdx: number = 0; assistIdx: number = 1;

  constructor(opts: DemonstrateOpts, readonly redraw: Redraw) {
    this.data = opts.data;
    this.pref = opts.pref;
    this.socket = new Socket(opts.socketSend, this);
    this.createBoard("主棋盘", this.data.fen ? this.data.fen : initialFen, false, true);
    this.createBoard("辅棋盘", initialFen, false, false, true);
  }

  createBoard = (name: string, fen: string = initialFen, removeable: boolean = true, active: boolean = true, remove: boolean = false) => {
    let idx = 0;
    if(this.boards.length > 0) {
      idx = this.boards[this.boards.length - 1].idx + 1;
    }

    let ops = {
      idx: idx,
      name: name,
      active: active,
      removeable: removeable,
      removed: remove,
      chessground: {} as CgApi,
      spare: {
        color: 'white',
        piece: 'pointer'
      },
      fen: fen,
      orientation: 'white',
      isShowCoord: true,
      isShowSpare: false,
      historyActiveIdx: -1,
      history: []
    };
    this.boards.push(ops);
    this.addHistory(this.getBoard(idx), fen);
  };

  removeBoard = (idx: number) => {
    this.boards.forEach((board) => {
      if(board.idx === idx) {
        board.removed = true;
      }
    });
    this.redraw();
    this.redrawAllBoard();
  };

  recoveryBoard = (idx: number) => {
    this.boards.forEach((board) => {
      if(board.idx === idx) {
        board.removed = false;
      }
    });
    this.redraw();
    this.redrawAllBoard();
  };

  showAssist = (v: boolean) => {
    this.isShowAssist = v;
    if(v) {
      this.recoveryBoard(this.assistIdx);
      this.setActiveBoard(this.assistIdx);
    } else {
      this.removeBoard(this.assistIdx);
      this.setActiveBoard(this.mainIdx);
    }
  };

  onGroundChange = (board: Board, writeHis: boolean = true) => {
    let fen = board.chessground.getFen();
    board.fen = fen;
    this.addHistory(board, fen, writeHis);
    this.redraw();
  };

  addHistory = (board: Board, fen: string, writeHis: boolean = true) => {
    if(writeHis) {
      board.history = board.history.slice(0, board.historyActiveIdx + 1);
      board.history.push(fen);
      board.historyActiveIdx = board.history.length - 1;
    }
  };

  setSpareSelected = (board: Board, piece, color) => {
    board.spare.color = color;
    board.spare.piece = piece;
    this.redraw();
  };

  loadSituation = (board: Board) => {
    if(this.data.notAccept) {
      window.lichess.memberIntro();
      return false;
    }

    let ts = this; let fen = '';
    $.ajax({
      url: `/resource/situationdb/select`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });
      window.lichess.pubsub.emit('content_loaded');

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-selector').find('form');
          let $tree = (<any>$jq($form)).find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            },
            'plugins' : [ 'search' ]
          })
            .on('changed.jstree', function (_, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                search();
              }
            });

          let to;
          $form.find('.search input[name="q"]').keyup(function () {
            let q = $form.find('.search input[name="q"]').val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });

          $form.find('#btn-search').click(function () {
            search();
          });

          let $emptyTag = $form.find('.search_form')
            .find('.tag-group.emptyTag')
            .find('input[type="checkbox"]');

          let $tags = $form.find('.search_form')
            .find('.tag-group')
            .find('input[type="checkbox"]');

          $tags.not($emptyTag).click(function () {
            $emptyTag.prop('checked', false);
            search();
          });

          $emptyTag.click(function () {
            $tags.not($emptyTag).prop('checked', false);
            search();
          });

          let $situations = $form.find('.situations');
          function search() {
            let situation = $tree.jstree(true).get_selected(null)[0];
            $.ajax({
              url: `/resource/situationdb/rel/list?selected=${situation}`,
              data: $form.serialize()
            }).then(function (rels) {
              $situations.empty();

              if(!rels || rels.length === 0) {
                let noMore =
                  `<div class="no-more">
                    <i data-icon="4">
                    <p>没有更多了</p>
                  </div>`;
                $situations.html(noMore);
              } else {
                let situations =
                  rels.map(function (rel) {
                    let tags = '';
                    if(rel.tags && rel.tags.length > 0) {
                      rel.tags.map(function (tag) {
                        tags = `<span>${tag}</span>`;
                      });
                    }
                    return `<a class="paginated" target="_blank" data-id="${rel.id}">
                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                <cg-helper>
                                  <cg-container>
                                    <cg-board></cg-board>
                                  </cg-container>
                                </cg-helper>
                              </div>
                              <div class="btm">
                                <label>${rel.name ? rel.name : ''}</label>
                                <div class="tags">${tags}</div>
                              </div>
                            </a>`;
                  });

                $situations.html(situations);
                registerEvent();
                window.lichess.pubsub.emit('content_loaded')
              }
            })
          }

          function registerEvent() {
            let $boards = $situations.find('.paginated');
            $boards.off('click');
            $boards.click(function(e) {
              e.preventDefault();
              let $this = $(e.currentTarget);
              $boards.removeClass('selected');
              $this.addClass('selected');
              fen = $this.find('.mini-board').data('fen');
              return false;
            });
          }
          registerEvent();

          $form.submit(function (e) {
            e.preventDefault();
            ts.changeFen(board, fen);
            $.modal.close();
            return false;
          });
        })
      });
    });
  };

  clearBoard = (board: Board) => this.changeFen(board, emptyFen);

  initBoard = (board: Board) => this.changeFen(board, board.history[0]);

  copyBoard =  (frIdx: number, toIdx: number) => {
    this.changeFen(this.getBoard(toIdx), this.getBoard(frIdx).fen);
  };

  changeFen = (board: Board, fen: string, writeHis: boolean = true) => {
    let singleChangeFen = (board: Board) => {
      board.chessground.set({
        fen: fen
      });
      this.onGroundChange(board, writeHis);
    };

    if(this.ctrlAll) {
      this.boards.forEach((board) => {
        singleChangeFen(board);
      });
    } else {
      singleChangeFen(board);
    }
    this.redraw();
  };

  toggleOrientation = (board: Board) => {
    let singleToggleOrientation = (board: Board) => {
      let opposite = cgUtil.opposite(board.chessground.state.orientation);
      board.orientation = opposite;
      board.chessground.set({
        orientation: opposite
      });
    };

    if(this.ctrlAll) {
      this.boards.forEach((board) => {
        singleToggleOrientation(board);
      });
    } else singleToggleOrientation(board);
    this.redraw();
  };

  showSpare = (board: Board, v: boolean) => {
    let singleShowSpare = (board: Board) => {
      board.isShowSpare = v;
      board.spare.piece = 'pointer';
      this.redraw();
      board.chessground.redrawAll();
    };

    if(this.ctrlAll) {
      this.boards.forEach(function (board) {
        singleShowSpare(board)
      });
    } else singleShowSpare(board);
  };

  showCoordinates = (board: Board, v: boolean) => {
    let singleShowCoordinates = (board: Board) => {
      board.isShowCoord = v;
      board.chessground.set({
        coordinates: v
      });
      board.chessground.redrawAll();
    };

    if(this.ctrlAll) {
      this.boards.forEach(function (board) {
        singleShowCoordinates(board)
      });
    } else singleShowCoordinates(board);
    this.redraw();
  };

  setActiveBoard = (idx: number) => {
    this.boards.forEach(function (board) {
      board.active = board.idx === idx;
    });
    this.redraw();
  };

  getActiveBoard = () => {
    return this.boards.filter(g => g.active)[0];
  };

  getBoard = (idx: number) => {
    return this.boards.filter(g => g.idx === idx)[0];
  };

  redrawAllBoard = () => {
    this.boards.filter(bd => !bd.removed).forEach((bd) => {
      bd.chessground.redrawAll();
    });
  };

  readZoom = () => parseInt(getComputedStyle(document.body).getPropertyValue('--zoom')) + 100;

  setZoom = (v: number) => {
    document.body.setAttribute('style', '--zoom:' + (v - 100));
    window.lichess.dispatchEvent(window, 'resize');
    this.redraw();
  };

  canPrev = (board: Board) => {
    return board.historyActiveIdx > 0;
  };

  prev = (board: Board) => {
    if(!this.canPrev(board)) return;
    this.subHisIdx(board);
    this.changeFen(board, board.history[board.historyActiveIdx], false);
  };

  canNext = (board: Board) => {
    return board.historyActiveIdx < board.history.length - 1;
  };

  next = (board: Board) => {
    if(!this.canNext(board)) return;
    this.addHisIdx(board);
    this.changeFen(board, board.history[board.historyActiveIdx], false);
  };

  canFirst = (board: Board) => {
    return board.historyActiveIdx > 0;
  };

  first = (board: Board) => {
    if(!this.canFirst(board)) return;
    board.historyActiveIdx = 0;
    this.changeFen(board, board.history[board.historyActiveIdx], false);
  };

  canLast = (board: Board) => {
    return board.historyActiveIdx < board.history.length - 1;
  };

  last = (board: Board) => {
    if(!this.canLast(board)) return;
    board.historyActiveIdx = board.history.length - 1;
    this.changeFen(board, board.history[board.historyActiveIdx], false);
  };

  addHisIdx = (board: Board) => {
    board.historyActiveIdx = board.historyActiveIdx >= board.history.length - 1 ? board.historyActiveIdx : board.historyActiveIdx + 1;
  };

  subHisIdx = (board: Board) => {
    board.historyActiveIdx = board.historyActiveIdx <= 0 ? board.historyActiveIdx : board.historyActiveIdx - 1;
  };

}
