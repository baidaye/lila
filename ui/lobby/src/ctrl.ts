import variantConfirm from './variant';
import * as hookRepo from './hookRepo';
import * as seekRepo from './seekRepo';
import { make as makeStores, Stores } from './store';
import * as xhr from './xhr';
import * as poolRangeStorage from './poolRangeStorage';
import {LobbyOpts, LobbyData, Tab, Mode, Sort, Filter, Hook, Seek, Pool, PoolMember} from './interfaces';
import LobbySocket from './socket';
import OAuth2PKCE from '../../lichsssApiOAuth/oauth2'

const li = window.lichess;

export default class LobbyController {

  opts: LobbyOpts;
  data: LobbyData;
  playban: any;
  isBot: boolean;
  oAuth2Ctrl: OAuth2PKCE;
  lichessPooling: boolean = false;
  socket: LobbySocket;
  stores: Stores;
  tab: Tab;
  mode: Mode;
  sort: Sort;
  filterOpen: boolean = false;
  stepHooks: Hook[] = [];
  stepping: boolean = false;
  redirecting: boolean = false;
  poolMember?: PoolMember;
  trans: Trans;
  redraw: () => void;
  pools: Pool[];
  pool: Pool;
  lichessApiAccept: boolean;

  private poolInStorage: LichessStorage;
  private flushHooksTimeout?: number;
  private alreadyWatching: string[] = [];

  constructor(opts: LobbyOpts, redraw: () => void) {
    this.opts = opts;
    this.data = opts.data;
    this.data.hooks = [];
    this.pools = opts.pools;
    this.playban = opts.playban;
    this.isBot = opts.data.me && opts.data.me.isBot;
    this.lichessApiAccept = opts.lichessApiAccept;
    this.redraw = redraw;

    hookRepo.initAll(this);
    seekRepo.initAll(this);
    this.socket = new LobbySocket(opts.socketSend, this);
    this.oAuth2Ctrl = new OAuth2PKCE(opts.oAuth2);
    this.stores = makeStores(this.data.me ? this.data.me.username.toLowerCase() : null);

    this.tab = this.isBot ? 'now_playing' : this.stores.tab.get(),
      this.mode = this.stores.mode.get(),
      this.sort = this.stores.sort.get(),
      this.trans = opts.trans;

    this.poolInStorage = li.storage.make('lobby.pool-in');
    this.poolInStorage.listen(() => { // when another tab joins a pool
      this.leavePool();
      redraw();
    });
    this.flushHooksSchedule();

    this.startWatching();

    if (this.playban) {
      if (this.playban.remainingSecond < 86400) setTimeout(li.reload, this.playban.remainingSeconds * 1000);
    }
    else {
      setInterval(() => {
        if (this.poolMember) this.poolIn();
        else if (this.tab === 'real_time' && !this.data.hooks.length) this.socket.realTimeIn();
      }, 10 * 1000);
      this.onNewOpponent();
    }

    li.pubsub.on('socket.open', () => {
      if (this.tab === 'real_time') {
        this.data.hooks = [];
        this.socket.realTimeIn();
      } else if (this.tab === 'pools' && this.poolMember) this.poolIn();
    });

    window.addEventListener('beforeunload', () => {
      if (this.poolMember) this.socket.poolOut(this.poolMember);
    });

    if(this.oAuth2Ctrl.isLichessAuthed()) {
      this.incomingStream();
    }
  }

  private doFlushHooks() {
    this.stepHooks = this.data.hooks.slice(0);
    if (this.tab === 'real_time') this.redraw();
  };

  flushHooks = (now: boolean) => {
    if (this.flushHooksTimeout) clearTimeout(this.flushHooksTimeout);
    if (now) this.doFlushHooks();
    else {
      this.stepping = true;
      if (this.tab === 'real_time') this.redraw();
      setTimeout(() => {
        this.stepping = false;
        this.doFlushHooks();
      }, 500);
    }
    this.flushHooksTimeout = this.flushHooksSchedule();
  };

  private flushHooksSchedule(): number {
    return setTimeout(this.flushHooks, 8000);
  }

  setTab = (tab: Tab) => {
    if (tab !== this.tab) {
      if (tab === 'seeks') xhr.seeks().then(this.setSeeks);
      else if (tab === 'real_time') this.socket.realTimeIn();
      else if (this.tab === 'real_time') {
        this.socket.realTimeOut();
        this.data.hooks = [];
      }
      this.tab = this.stores.tab.set(tab);
    }
    this.filterOpen = false;
  };

  setMode = (mode: Mode) => {
    this.mode = this.stores.mode.set(mode);
    this.filterOpen = false;
  };

  setSort = (sort: Sort) => {
    this.sort = this.stores.sort.set(sort);
  };

  toggleFilter = () => {
    this.filterOpen = !this.filterOpen;
  };

  setFilter = (filter: Filter) => {
    this.data.filter = filter;
    this.flushHooks(true);
    if (this.tab !== 'real_time') this.redraw();
  };

  clickHook = (id: string) => {
    const hook = hookRepo.find(this, id);
    if (!hook || hook.disabled || this.stepping || this.redirecting) return;
    if (hook.action === 'cancel' || variantConfirm(hook.variant)) this.socket.send(hook.action, hook.id);
  };

  clickSeek = (id: string) => {
    const seek = seekRepo.find(this, id);
    if (!seek || this.redirecting) return;
    if (seek.action === 'cancelSeek' || variantConfirm(seek.variant)) this.socket.send(seek.action, seek.id);
  };

  setSeeks = (seeks: Seek[]) => {
    this.data.seeks = seeks;
    seekRepo.initAll(this);
    this.redraw();
  };

  clickPool = (id: string) => {
    let pool = this.pools.find(function(p) {
      return p.id === id;
    });

    // @ts-ignore
    let $lichessLogin = $('.lobby__third #lichessLogin');
    let lichessChecked = $lichessLogin && $lichessLogin[0] && ($lichessLogin[0] as HTMLInputElement).checked;
    if(lichessChecked && this.oAuth2Ctrl.isLichessAuthed()) {
      if(pool && pool.lichessApi && !this.playban) {
        if(!this.lichessApiAccept) {
          window.lichess.memberIntro();
          return;
        }

        this.pool = pool;
        this.lichessPooling = true;
        this.redraw();
        this.createSeek(pool.lim, pool.inc);
        //this.createChallenge('zhoun945', pool.lim, pool.inc);
      }
    } else {
      let rating = null;
      if(this.data.perfs && pool) {
        rating = this.data.perfs[pool.key];
      }
      xhr.anonPoolSeek(pool, rating);
      this.setTab('real_time');
      $(window).scrollTop(0);
    }
  };

  enterPool = (member: PoolMember) => {
    poolRangeStorage.set(member.id, member.range);
    this.setTab('pools');
    this.poolMember = member;
    this.poolIn();
  };

  leavePool = () => {
    if (!this.poolMember) return;
    this.socket.poolOut(this.poolMember);
    this.poolMember = undefined;
    this.redraw();
  };

  poolIn = () => {
    if (!this.poolMember) return;
    this.poolInStorage.set(li.StrongSocket.sri);
    this.socket.poolIn(this.poolMember);
  };

  gameActivity = gameId => {
    if (this.data.nowPlaying.find(p => p.gameId === gameId))
      xhr.nowPlaying().then(povs => {
        this.data.nowPlaying = povs;
        this.startWatching();
        this.redraw();
      });
  };

  private startWatching() {
    const newIds = this.data.nowPlaying
      .map(p => p.gameId)
      .filter(id => !this.alreadyWatching.includes(id));
    if (newIds.length) {
      setTimeout(() => this.socket.send("startWatching", newIds.join(' ')), 2000);
      newIds.forEach(id => this.alreadyWatching.push(id));
    }
  };

  setRedirecting = () => {
    this.redirecting = true;
    setTimeout(() => {
      this.redirecting = false;
      this.redraw();
    }, 4000);
    this.redraw();
  };

  awake = () => {
    switch (this.tab) {
      case 'real_time':
        this.data.hooks = [];
        this.socket.realTimeIn();
        break;
      case 'seeks':
        xhr.seeks().then(this.setSeeks);
        break;
    }
  };

  lichessLoginChange = (checked) => {
    if(checked && !this.oAuth2Ctrl.isLichessAuthed()) {
      $('.lobby__spinner').removeClass('none');
      this.oAuth2Ctrl.redirectLichessOAuth2();
      return;
    }

    if(this.oAuth2Ctrl.isLichessAuthed()) {
      if(checked) {
        this.pools.filter(p => !p.lichessApi).forEach(p => {
          $(`.lobby__pools .lpools>div[data-id="${p.id}"]`).addClass('disabled');
        });
        $(`.lobby__pools .lpools>div.custom`).addClass('disabled');
      } else {
        this.pools.filter(p => !p.lichessApi).forEach(p => {
          $(`.lobby__pools .lpools>div[data-id="${p.id}"]`).removeClass('disabled');
        });
        $(`.lobby__pools .lpools>div.custom`).removeClass('disabled');
      }
    }
  };

  // after click on round "new opponent" button
  private onNewOpponent() {
    if (location.hash.startsWith('#pool/')) {
      const regex = /^#pool\/(\d+\+\d+)(?:\/(.+))?$/,
        match = regex.exec(location.hash),
        member: any = { id: match![1], blocking: match![2] },
        range = poolRangeStorage.get(member.id);
      if (range) member.range = range;
      if (match) {
        this.setTab('pools');
        this.enterPool(member);
        history.replaceState(null, '', '/');
      }
    }
  }

  createSeek = (limit: number, increment: number) => {
    this.writeLocalStorage('LICHESS-LASTPOOL', {limit: limit * 60, increment});
    this.oAuth2Ctrl.openStream(
      `/api/board/seek`,
      {
        single: this.oAuth2Ctrl.signal,
        method: 'post',
        body: `time=${limit}&increment=${increment}&rated=true&keepAliveStream=true`
      },
      _ => {}
    );
  };

  createChallenge = (username, limit, increment) => {
    this.writeLocalStorage('LICHESS-LASTPOOL', {limit: limit * 60, increment});
    this.oAuth2Ctrl.openStream(
      `/api/challenge/${username}`,
      {
        single: this.oAuth2Ctrl.signal,
        method: 'post',
        body: `clock.limit=${limit * 60}&clock.increment=${increment}&rated=true&keepAliveStream=true`
      },
      _ => {}
    );
  };

  abortLichessPool = () => {
    this.oAuth2Ctrl.controller.abort();
    this.lichessPooling = false;
    this.redraw();
  };

  incomingStream = () => {
    this.oAuth2Ctrl.openStream('/api/stream/event', {}, msg => {
      switch (msg.type) {
        case 'gameStart': {
          this.onGameStart(msg);
          break;
        }
        default:
          console.warn(`Unprocessed message of type ${msg.type}`, msg);
      }
    });
  };

  onGameStart = (msg: any) => {
    const g = msg.game;
    const c = this.readLocalStorage('LICHESS-LASTPOOL');
    if(!c) {
      alert('糟糕！对局不见了~');
    } else {
      const d = this.oAuth2Ctrl.makeRoundData(g, c);
      xhr.toRound(d).then((res) => {
        if(!res.redirected) {
          location.href = `/lichess/round/${g.id}/${g.color}`;
        }
      });
    }
  };

  readLocalStorage = (key) => {
    // allow reading from storage to retrieve previous support results
    // even while the document does not have focus
    let data;
    try {
      data = window.localStorage && window.localStorage.getItem(key);
      data = data ? JSON.parse(data) : {};
    } catch (e) {
      data = {};
    }
    return data;
  };

  writeLocalStorage = (key, value) => {
    if (!document.hasFocus()) {
      // if the document does not have focus when tests are executed, focus() may
      // not be handled properly and events may not be dispatched immediately.
      // This can happen when a document is reloaded while Developer Tools have focus.
      try {
        window.localStorage && window.localStorage.removeItem(key);
      } catch (e) {
        // ignore
      }
      return;
    }
    try {
      window.localStorage &&
      window.localStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      // ignore
    }
  }

}
