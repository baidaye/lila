import { init } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import { Chessground } from 'chessground';
import { LobbyOpts, Tab } from './interfaces';
import LobbyController from './ctrl';

export const patch = init([klass, attributes]);

import makeCtrl from './ctrl';
import view from './view/main';
import boot = require('./boot');

export function start(opts: LobbyOpts) {

  let vnode: VNode, ctrl: LobbyController;

  function redraw() {
    vnode = patch(vnode, view(ctrl));
  }

  ctrl = new makeCtrl(opts, redraw);

  const blueprint = view(ctrl);
  opts.element.innerHTML = '';
  vnode = patch(opts.element, blueprint);
  $('.lobby__pools .lpools>div').click(function(e) {
    const id = (e.target as HTMLElement).getAttribute('data-id') ||
        ((e.target as HTMLElement).parentNode as HTMLElement).getAttribute('data-id');
    if (id === 'custom' && !ctrl.oAuth2Ctrl.isLichessAuthed()) $('.config_hook').trigger('mousedown');
    else if (id) {
      ctrl.clickPool(id);
    }
  });

  $('.lobby__third #lichessLogin').change(function(e) {
    let checked = (e.target as HTMLInputElement).checked;
    ctrl.lichessLoginChange(checked);
  });

  return {
    socketReceive: ctrl.socket.receive,
    setTab(tab: Tab) {
      ctrl.setTab(tab);
      ctrl.redraw();
    },
    gameActivity: ctrl.gameActivity,
    setRedirecting: ctrl.setRedirecting,
    enterPool: ctrl.enterPool,
    leavePool: ctrl.leavePool,
    redraw: ctrl.redraw
  };
}

// that's for the rest of lichess to access chessground
// without having to include it a second time
window.Chessground = Chessground;

window.onload = function() {
  boot(window['lichess_lobby'], document.querySelector('.lobby__app'));
};
