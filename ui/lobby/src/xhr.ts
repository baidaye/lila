const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function seeks() {
  return $.ajax({
    url: '/lobby/seeks',
    headers: headers
  });
}

export function nowPlaying() {
  return $.ajax({
    url: '/account/now-playing',
    headers: headers
  }).then(o => o.nowPlaying);
}

export function anonPoolSeek(pool, rating) {
  let data = {
    variant: 1,
    timeMode: 1,
    time: pool.lim,
    increment: pool.inc,
    days: 1,
    mode: 0,
    color: 'random',
    ratingRange: ''
  };

  if(rating) {
    let min = Math.max((rating - 200), 600);
    let max =  Math.min((rating + 200), 2900);
    data.ratingRange = min + '-' + max;
    data.mode = 1;
  }
  return $.ajax({
    method: 'POST',
    url: '/setup/hook/' + window.lichess.StrongSocket.sri,
    data: data
  });
}

export function toRound(data) {
  return $.ajax({
    method: 'post',
    url: `/lichess/round/${data.gameId}/to`,
    headers: headers,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data)
  });
}
