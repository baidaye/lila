import { h } from 'snabbdom';
import { Hooks } from 'snabbdom/hooks'
import LobbyController from '../ctrl';
import { bind, spinner } from './util';
import {VNode} from "snabbdom/vnode";
import {modal} from "./modal";

function renderRange(range: string) {
  return h('div.range', range.replace('-', '–'));
}

export function hooks(ctrl: LobbyController): Hooks {
  return bind('click', e => {
    const id = (e.target as HTMLElement).getAttribute('data-id') ||
      ((e.target as HTMLElement).parentNode as HTMLElement).getAttribute('data-id');
    if (id === 'custom') $('.config_hook').trigger('mousedown');
    else if (id) ctrl.clickPool(id);
  }, ctrl.redraw);
}

export function render(ctrl: LobbyController) {
  const member = ctrl.poolMember;
  return ctrl.pools.map(pool => {
    const active = !!member && member.id === pool.id,
    transp = !!member && !active;
    return h('div', {
      class: {
        active,
        transp: !active && transp
      },
      attrs: { 'data-id': pool.id }
    }, [
      h('div.clock', pool.lim + '+' + pool.inc),
      (active && member!.range) ? renderRange(member!.range!) : h('div.perf', pool.perf),
      active ? spinner() : null
    ]);
  }).concat(
    h('div.custom', {
      class: { transp: !!member },
      attrs: { 'data-id': 'custom' }
    }, ctrl.trans.noarg('custom'))
  );
}

export function lichessPool(ctrl: LobbyController): VNode {
  return modal({
    class: 'lichess_pool',
    onClose() {
      ctrl.abortLichessPool();
    },
    content: [
      h('h2', '在lichess.org 匹配中'),
      h('div.clock', `${ctrl.pool.lim} + ${ctrl.pool.inc}`),
      h('div.desc', '完成对局前不要关闭页面。'),
      h('div.action', [
        h('button.button.button-red.small', {
          hook: bind('click', ctrl.abortLichessPool)
        }, '取消匹配')
      ]),
    ]
  });
}
