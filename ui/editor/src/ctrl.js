var editor = require('./editor');
var m = require('mithril');
var keyboard = require('./keyboard');
var fenRead = require('chessground/fen').read;
var ChessJS = require('./chess_ext.js');

module.exports = function(cfg) {

  this.cfg = cfg;
  this.data = editor.init(cfg);
  this.options = cfg.options || {};
  this.embed = cfg.embed;
  this.notAccept = cfg.notAccept;
  this.trans = lichess.trans(this.data.i18n);

  this.selected = m.prop('pointer');

  this.chess = null;
  if(this.data.patternsDesigner) {
    this.chess = ChessJS.Chess();
  }

  this.extraPositions = [{
    fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -',
    name: this.trans('startPosition')
  }, {
    fen: '8/8/8/8/8/8/8/8 w - -',
    name: this.trans('clearBoard')
  }, {
    fen: 'prompt',
    name: '输入FEN'
  }, {
    fen: 'situation',
    name: '载入局面'
  }];

  this.extraPatternsDesignerPositions = [{
    fen: 'empty',
    name: '清空棋盘'
  }, {
    fen: 'prompt',
    name: '输入FEN'
  }, {
    fen: 'situation',
    name: '载入局面'
  }];

  this.whiteCheckmateEmptyFen = '4k3/8/8/8/8/8/8/8 b - - 0 1';
  this.blackCheckmateEmptyFen = '8/8/8/8/8/8/8/4K3 w - - 0 1';
  this.colors = [{ c: 'white', n: '白棋', v: this.whiteCheckmateEmptyFen }, { c: 'black', n: '黑棋', v: this.blackCheckmateEmptyFen }];
  this.patternsAnalysisErrors = [];
  this.patternsAnalysisResult = {
    loading: false,
    color: '',
    colorLable: '',
    type: '',
    typeLabel: '',
    success: false,
    op: ''
  };

  this.positionIndex = {};
  cfg.positions && cfg.positions.forEach(function(p, i) {
    this.positionIndex[p.fen.split(' ')[0]] = i;
  }.bind(this));

  this.chessground; // will be set from the view when instanciating chessground
  this.flipped = false;

  this.onChange = function() {
    this.options.onChange && this.options.onChange(this.computeFen());
    m.redraw();
  }.bind(this);

  this.onUserMove = function(orig, dest) {
    const piece = this.chessground.state.pieces[dest];
    if(this.options.showMoveShape) {
      const shapes = [];
      shapes.push({ orig: orig, brush: 'paleBlue'});
      shapes.push({ orig: dest, brush: 'paleBlue'});
      shapes.push({
        orig: orig,
        dest: dest,
        brush: 'paleBlue'
      });
      this.chessground.setAutoShapes(shapes);
    }
    this.options.onUserMove && this.options.onUserMove({piece, orig, dest});
    m.redraw();
  }.bind(this);

  this.computeFen = function() {
    return this.chessground ?
    editor.computeFen(this.data, this.chessground.getFen()) :
    cfg.fen;
  }.bind(this);

  this.bottomColor = function() {
    return this.chessground ?
    this.chessground.state.orientation :
    this.options.orientation || 'white';
  }.bind(this);

  this.setFlipped = function() {
    this.flipped = this.bottomColor() === 'black';
    m.redraw();
  }.bind(this);

  this.setColor = function(letter) {
    this.data.color(letter);
    this.onChange();
  }.bind(this);

  this.setCastle = function(id, value) {
    this.data.castles[id](value);
    this.onChange();
  }.bind(this);

  this.startPosition = function() {
    this.chessground.set({
      fen: 'start'
    });
    this.data.castles = editor.castlesAt(true);
    this.data.color('w');
    this.onChange();
  }.bind(this);

  this.clearBoard = function() {
    this.chessground.set({
      fen: '8/8/8/8/8/8/8/8'
    });
    this.data.castles = editor.castlesAt(false);
    this.onChange();
  }.bind(this);

  this.loadNewFen = function(fen) {
    if(fen === '') {
      return;
    } else if (fen === 'situation') {
      if(this.notAccept) {
        window.lichess.memberIntro();
      } else {
        this.loadSituation();
      }
      return;
    } else if (fen === 'prompt') {
      fen = prompt('请粘贴局面').trim();
      if (!fen) return;
    } else if (this.data.patternsDesigner && fen === 'empty') {
      let $color = $('.patterns-color').find('input[name="checkmate-color"]:checked');
      let emptyFen = $color.data('emptyfen');
      this.changeFen(emptyFen);
      return;
    }

    this.changeFen(fen);
  }.bind(this);

  this.changeFen = function(fen) {
    let $color = $('.patterns-color').find('input[name="checkmate-color"]:checked');
    let color = this.data.patternsDesigner ? $color.val() : null;
    window.location = editor.makeUrl(this.data.baseUrl, fen, color);
  }.bind(this);

  this.changeVariant = function(variant) {
    this.data.variant = variant;
    m.redraw();
  }.bind(this);

  this.changeCheckmateColor = function(emptyFen, color) {
    if (this.data.patternsDesigner){
      let isOnlyKing = () => {
        let pieces = this.chessground ? this.chessground.state.pieces : fenRead(this.cfg.fen);
        let roles = [];
        for (let pos in pieces) {
          if (pieces[pos]) {
            roles.push(pieces[pos].role);
          }
        }
        return roles.length === 1 && roles[0] === 'king';
      };

      if(isOnlyKing()) {
        this.changeFen(color === 'white' ? this.whiteCheckmateEmptyFen : this.blackCheckmateEmptyFen);
      }
      this.setColor(color === 'white' ? 'b' : 'w');
    }
  }.bind(this);

  this.positionLooksLegit = function() {
    var variant = this.data.variant;
    if (variant === "antichess") return true;
    var pieces = this.chessground ? this.chessground.state.pieces : fenRead(this.cfg.fen);
    var kings = {
      white: 0,
      black: 0
    };
    for (var pos in pieces) {
      if (pieces[pos] && pieces[pos].role === 'king') kings[pieces[pos].color]++;
    }
    return kings.white === (variant !== "horde" ? 1 : 0) && kings.black === 1;
  }.bind(this);

  this.patternsAnalysis = function() {
    this.patternsAnalysisResult.loading = true;
    this.patternsAnalysisErrors = [];
    m.redraw();

    let $color = $('.patterns-color').find('input[name="checkmate-color"]:checked');
    let winColor = $color.val();
    let lossColor = winColor === 'white' ? 'black' : 'white';
    let pieces = this.chessground ? this.chessground.state.pieces : fenRead(this.cfg.fen);
    let winSquares = [];
    let lossSquares = [];
    let lossKingPos;

    let kings = {
      white: 0,
      black: 0
    };

    for (let pos in pieces) {
      if (pieces[pos] && pieces[pos].role === 'king') {
        kings[pieces[pos].color]++;

        if (pieces[pos].color === lossColor) {
          lossKingPos = pos;
        }
      }

      if (pieces[pos]) {
        if(pieces[pos].color === winColor) {
          winSquares.push(pos);
        } else {
          lossSquares.push(pos);
        }
      }
    }

    if(kings.white > 1) {
      this.patternsAnalysisErrors.push('白方多于1个王');
    }

    if(kings.black > 1) {
      this.patternsAnalysisErrors.push('黑方多于1个王');
    }

    if(kings[lossColor] < 1) {
      this.patternsAnalysisErrors.push('败方没有王');
    }

    let baseline = ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1', 'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'];
    let baselineHasPawn = false;
    baseline.forEach(function(pos) {
      if(pieces[pos] && pieces[pos].role === 'pawn') {
        baselineHasPawn = true;
      }
    });

    if(baselineHasPawn) {
      this.patternsAnalysisErrors.push('兵在底线');
    }

    let fen = this.computeFen() + ' 0 1';
    this.chess.load(fen);

    if(kings.white === 1 && this.chess.in_king_attacked(winColor[0])) {
      this.patternsAnalysisErrors.push('胜方王被将军');
    }

    if(!this.chess.in_king_attacked(lossColor[0]) || !this.chess.in_checkmate()) {
      this.patternsAnalysisErrors.push('胜方未完成将杀');
    }

    let attackedBySquares = (fen, kingPos) => {
      let attackSquares = [];
      this.chess.load(fen);
      winSquares.forEach(square => {
        const moves = this.chess.moves({
          square: square,
          verbose: true,
          legal: false
        });
        if (moves.length && moves.filter(move => kingPos === move.to).length >= 1) {
          attackSquares.push(square);
        }
      });
      return attackSquares;
    };

    let oppositeTurnFen = (fen) => {
      let arr = fen.split(' ');
      if(arr[1]) {
        if(arr[1] === 'w') arr[1] = 'b';
        else if(arr[1] === 'b') arr[1] = 'w';
      }
      return arr.join(' ');
    };

    // 将杀子
    let checkmateSquares = attackedBySquares(oppositeTurnFen(fen), lossKingPos);

    if(checkmateSquares.length > 2) {
      this.patternsAnalysisErrors.push('将杀子多于2个');
    }

    if(this.patternsAnalysisErrors.length > 0) {
      this.patternsAnalysisResult.loading = false;
      m.redraw();
      return false;
    }

    let files = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    let fileMap = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7};

    let lossKingPosArray = lossKingPos.split('');
    let lossKingPosFile = lossKingPosArray[0];
    let lossKingPosRank = lossKingPosArray[1];
    let lossKingPosFileLeft = lossKingPosFile === 'a' ? '' : files[fileMap[lossKingPosFile] - 1];
    let lossKingPosFileRight = lossKingPosFile === 'h' ? '' : files[fileMap[lossKingPosFile] + 1];
    let lossKingPosRankUp = lossKingPosRank === '8' ? '' : parseInt(lossKingPosRank) + 1;
    let lossKingPosRankDown = lossKingPosRank === '1' ? '' : parseInt(lossKingPosRank) - 1;

    let lossKingPosAround1 = lossKingPosRankUp ? lossKingPosFile + lossKingPosRankUp : '';
    let lossKingPosAround2 = lossKingPosRankUp ? (lossKingPosFileRight ? lossKingPosFileRight + lossKingPosRankUp : '') : '';
    let lossKingPosAround3 = lossKingPosFileRight ? lossKingPosFileRight + lossKingPosRank : '';
    let lossKingPosAround4 = lossKingPosFileRight ? (lossKingPosRankDown ? lossKingPosFileRight + lossKingPosRankDown : '') : '';
    let lossKingPosAround5 = lossKingPosRankDown ? lossKingPosFile + lossKingPosRankDown : '';
    let lossKingPosAround6 = lossKingPosRankDown ? (lossKingPosFileLeft ? lossKingPosFileLeft + lossKingPosRankDown : '') : '';
    let lossKingPosAround7 = lossKingPosFileLeft ? lossKingPosFileLeft + lossKingPosRank : '';
    let lossKingPosAround8 = lossKingPosFileLeft ? (lossKingPosRankUp ? lossKingPosFileLeft + lossKingPosRankUp : '') : '';

    let lossKingPosArounds = [lossKingPosAround1, lossKingPosAround2, lossKingPosAround3, lossKingPosAround4, lossKingPosAround5, lossKingPosAround6, lossKingPosAround7, lossKingPosAround8];

    let isAttackedAfterMoveLossKing = (from, to) => {
      this.chess.load(fen);
      this.chess.remove(from);
      this.chess.remove(to);
      this.chess.put({
        type: 'k',
        color: lossColor[0]
      }, to);

      let isCheck = this.chess.in_king_attacked(lossColor[0]) && this.chess.in_check();
      let attackSquares = attackedBySquares(oppositeTurnFen(this.chess.fen()), to);
      let isCheckerAttack = attackSquares.find(s => checkmateSquares.includes(s));

      return {
        check: isCheck,
        isChecked: isCheckerAttack
      };
    };

    let op = lossKingPosArounds.map(pos => {
      let attackedAfterMove = isAttackedAfterMoveLossKing(lossKingPos, pos);
      if(pos) {
        // 将杀子所在的格子，模式为 X
        if(checkmateSquares.includes(pos)) return 'X';
        else if(attackedAfterMove.check && attackedAfterMove.isChecked) return 'X';
        else if(attackedAfterMove.check && !attackedAfterMove.isChecked) return 'O';
        else if(lossSquares.includes(pos)) return 'P';
        else return '?';
      } else return 'Z'
    }).join('');

    this.patternsAnalysisResult.type = checkmateSquares.length === 1 ? 'single' : 'double';
    this.patternsAnalysisResult.typeLabel = checkmateSquares.length === 1 ? '单子' : '双子';
    this.patternsAnalysisResult.color = winColor;
    this.patternsAnalysisResult.colorLable = $color.next('label').text();

    this.verifyOp(this.patternsAnalysisResult.type, op).then((exists) => {
      if(!exists) {
        this.patternsAnalysisErrors.push('无法实现的将杀');
      }
      this.patternsAnalysisResult.success = exists;
      this.patternsAnalysisResult.loading = false;
      this.patternsAnalysisResult.op = op;
      m.redraw();
    });
  }.bind(this);


  this.verifyOp = function(type, op) {
    return $.ajax({
      url: `/patterns/${type}/verifyOp?op=${op}`,
      headers: {
        'Accept': 'application/vnd.lichess.v3+json'
      }
    }).fail(function(d) {
      alert(d.responseJSON)
    });
  }.bind(this);

  this.setOrientation = function(o) {
    this.options.orientation = o;
    if (this.chessground.state.orientation !== o)
    this.chessground.toggleOrientation();
    m.redraw();
  }.bind(this);

  this.loadSituation = function() {
    let ts = this;
    let fen = '';
    $.ajax({
      url: `/resource/situationdb/select`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });
      window.lichess.pubsub.emit('content_loaded');

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-selector').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            },
            'plugins' : [ 'search' ]
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                search();
              }
            });

          let to = null;
          $form.find('.search input[name="q"]').keyup(function () {
            let q = $(this).val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });

          $form.find('#btn-search').click(function () {
            search();
          });

          let $emptyTag = $form.find('.search_form')
            .find('.tag-group.emptyTag')
            .find('input[type="checkbox"]');

          let $tags = $form.find('.search_form')
            .find('.tag-group')
            .find('input[type="checkbox"]');

          $tags.not($emptyTag).click(function () {
            $emptyTag.prop('checked', false);
            search();
          });

          $emptyTag.click(function () {
            $tags.not($emptyTag).prop('checked', false);
            search();
          });

          let $situations = $form.find('.situations');
          function search() {
            let situation = $tree.jstree(true).get_selected(null)[0];
            $.ajax({
              url: `/resource/situationdb/rel/list?selected=${situation}`,
              data: $form.serialize()
            }).then(function (rels) {
              $situations.empty();

              if(!rels || rels.length === 0) {
                let noMore =
                  `<div class="no-more">
                    <i data-icon="4">
                    <p>没有更多了</p>
                  </div>`;
                $situations.html(noMore);
              } else {
                let situations =
                  rels.map(function (rel) {
                    let tags = '';
                    if(rel.tags && rel.tags.length > 0) {
                      rel.tags.map(function (tag) {
                        tags = `<span>${tag}</span>`;
                      });
                    }
                     return `<a class="paginated" target="_blank" data-id="${rel.id}">
                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                <cg-helper>
                                  <cg-container>
                                    <cg-board></cg-board>
                                  </cg-container>
                                </cg-helper>
                              </div>
                              <div class="btm">
                                <label>${rel.name ? rel.name : ''}</label>
                                <div class="tags">${tags}</div>
                              </div>
                            </a>`;
                  });

                  $situations.html(situations);
                  registerEvent();
                  window.lichess.pubsub.emit('content_loaded')
                }
            })
          }

          function registerEvent() {
            let $boards = $situations.find('.paginated');
            $boards.off('click');
            $boards.click(function(e) {
              e.preventDefault();
              let $this = $(this);
              $boards.removeClass('selected');
              $this.addClass('selected');
              fen = $this.find('.mini-board').data('fen');
              return false;
            });
          }
          registerEvent();

          $form.submit(function (e) {
            e.preventDefault();
            ts.changeFen(fen);
            $.modal.close();
            return false;
          });
        })
      });
      return false
    });
  }.bind(this);

  keyboard(this);

};
