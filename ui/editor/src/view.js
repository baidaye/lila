var chessground = require('./chessground');
var dragNewPiece = require('chessground/drag').dragNewPiece;
var eventPosition = require('chessground/util').eventPosition;
var resizeHandle = require('common/resize').default;
var editor = require('./editor');
var m = require('mithril');

function castleCheckBox(ctrl, id, label, reversed) {
  var input = m('input[type=checkbox]', {
    checked: ctrl.data.castles[id](),
    onchange: function(e) {
      ctrl.setCastle(id, e.target.checked);
    }
  });
  return m('label', reversed ? [input, label] : [label, input]);
}

function optgroup(name, opts) {
  return m('optgroup', {
    label: name
  }, opts);
}

function studyButton(ctrl, fen) {
  return m('form', {
    method: 'post',
    action: '/study/as'
  }, [
    m('input[type=hidden][name=orientation]', {
      value: ctrl.bottomColor()
    }),
    m('input[type=hidden][name=variant]', {
      value: ctrl.data.variant
    }),
    m('input[type=hidden][name=fen]', {
      value: fen
    }),
    m('button.button.button-empty.text', {
      type: 'submit',
      'data-icon': '4',
      disabled: !ctrl.positionLooksLegit(),
      class: ctrl.positionLooksLegit() ? '' : 'disabled',
      onclick: function() {
        history.pushState(null, '', `/editor/${fen}`);
      }
    },
      '研习')
  ]);
}

function variant2option(key, name, ctrl) {
  return {
    tag: 'option',
    attrs: {
      value: key,
      selected: key == ctrl.data.variant
    },
    children: [ctrl.trans('variant') + ' | ' + name]
  };
}

function controls(ctrl, fen) {
  var positionIndex = ctrl.positionIndex[fen.split(' ')[0]];
  var currentPosition = ctrl.data.positions && positionIndex !== -1 ? ctrl.data.positions[positionIndex] : null;
  var position2option = function(pos) {
    return {
      tag: 'option',
      attrs: {
        value: pos.fen,
        selected: currentPosition && currentPosition.fen === pos.fen
      },
      children: [pos.eco ? pos.eco + ' ' + pos.name : pos.name]
    };
  };
  var selectedVariant = ctrl.data.variant;
  var looksLegit = ctrl.positionLooksLegit();
  return m(`div.board-editor__tools`, [
    ctrl.embed ? null : m('div', [
      ctrl.data.positions ? m('select.positions', {
        onchange: function(e) {
          ctrl.loadNewFen(e.target.value);
        }
      }, [
        optgroup(ctrl.trans('setTheBoard'), [
          currentPosition ? null : m('option', {
            value: fen,
            selected: true
          }, '- ' + ctrl.trans('boardEditor') + ' -'),
          ctrl.extraPositions.map(position2option)
        ]),
        optgroup(ctrl.trans('popularOpenings'),
          ctrl.data.positions.map(position2option)
        )
      ]) : null
    ]),
    m('div.metadata', [
      m('div.color',
        m('select', {
          onchange: m.withAttr('value', ctrl.setColor)
        }, ['whitePlays', 'blackPlays'].map(function(key) {
          return m('option', {
            value: key[0],
            selected: ctrl.data.color() === key[0]
          }, ctrl.trans(key));
        }))
      ),
      m('div.castling', [
        m('strong', ctrl.trans('castling')),
        m('div', [
          castleCheckBox(ctrl, 'K', ctrl.trans('whiteCastlingKingside'), ctrl.options.inlineCastling),
          castleCheckBox(ctrl, 'Q', '白方长易位', true)
        ]),
        m('div', [
          castleCheckBox(ctrl, 'k', ctrl.trans('blackCastlingKingside'), ctrl.options.inlineCastling),
          castleCheckBox(ctrl, 'q', '黑方长易位', true)
        ])
      ])
    ]),
    ctrl.embed ? m('div.actions', [
      m('a.button.button-empty', {
        onclick: ctrl.startPosition
      }, '初始局面'),
      m('a.button.button-empty', {
        onclick: ctrl.clearBoard
      }, '空棋盘')
    ]) : [
      m('div', [
        m('select#variants', {
          onchange: function(e) {
            ctrl.changeVariant(e.target.value);
          }
        }, [
          ['standard', '标准国际象棋']/*,
          ['antichess', 'Antichess'],
          ['atomic', 'Atomic'],
          ['crazyhouse', 'Crazyhouse'],
          ['horde', 'Horde'],
          ['kingOfTheHill', 'King of the Hill'],
          ['racingKings', 'Racing Kings'],
          ['threeCheck', 'Three-check']*/
        ].map(function(x) { return variant2option(x[0], x[1], ctrl) })
        )
      ]),
      m('div.actions', [
        m('a.button.button-empty.text[data-icon=B]', {
          onclick: function() {
            ctrl.chessground.toggleOrientation();
          }
        }, ctrl.trans('flipBoard')),
        looksLegit ? m('a.button.button-empty.text[data-icon="A"]', {
          //href: editor.makeUrl('/analysis/' + selectedVariant + '/', fen),
          //rel: 'nofollow'
          onclick: function() {
            history.pushState(null, '', `/editor/${fen}`);
            location.href = editor.makeUrl('/analysis/' + selectedVariant + '/', fen);
          }
        }, ctrl.trans('analysis')) : m('span.button.button-empty.disabled.text[data-icon="A"]', {
          rel: 'nofollow'
        }, ctrl.trans('analysis')),
        m('a.button.button-empty', {
          class: (looksLegit && selectedVariant === 'standard') ? '' : 'disabled',
          onclick: function() {
            history.pushState(null, '', `/editor/${fen}`);
            if (ctrl.positionLooksLegit() && selectedVariant === 'standard') $.modal($('.continue-with'));
          }
        }, m('span.text[data-icon=U]', ctrl.trans('continueFromHere'))),
        studyButton(ctrl, fen),
        m('a.button.button-empty', {
          class: (/*looksLegit && */ selectedVariant === 'standard') ? '' : 'disabled',
          onclick: function() {
            if (/*ctrl.positionLooksLegit() && */ selectedVariant === 'standard') {
              showCreateSituation(ctrl, looksLegit, fen);
            }
          }
        }, m('span.text[data-icon=]', '保存到局面库'))
      ]),
      m('div.continue-with.none', [
        m('a.button', {
          href: '/lobby?fen=' + fen + '#ai',
          rel: 'nofollow'
        }, ctrl.trans.noarg('playWithTheMachine')),
        m('a.button', {
          href: '/lobby?fen=' + fen + '#friend',
          rel: 'nofollow'
        }, ctrl.trans.noarg('playWithAFriend'))
      ])
    ]
  ]);
}

function patternsDesignerControls(ctrl, fen) {
  let looksLegit = ctrl.positionLooksLegit();
  let positionIndex = ctrl.positionIndex[fen.split(' ')[0]];
  let currentPosition = ctrl.data.positions && positionIndex !== -1 ? ctrl.data.positions[positionIndex] : null;
  return m('div.board-editor__tools.patternsDesigner__tools', [
    m('div.patternsDesigner__tools-top', [
      m('div.metadata', [
        m('div.patterns-color', [
          m('label', '将杀方：'),
          m('div.radios', ctrl.colors.map(function (color) {
            return m('div', [
              m('input', {
                type: 'radio',
                id: 'checkmate-color-' + color.c,
                name: 'checkmate-color',
                value: color.c,
                checked: color.c === ctrl.data.patternsDesignerColor,
                'data-emptyFen': color.v,
                onclick: function() {
                  ctrl.changeCheckmateColor(color.v, color.c);
                }
              }),
              m('label', {for: 'checkmate-color-' + color.c}, color.n)
            ])
          }))
        ]),
        m('select.positions', {
          onchange: function(e) {
            ctrl.loadNewFen(e.target.value);
          }
        }, [
          m('option', { value: '', selected: true }, '- 将杀模式设计器 -'),
          ctrl.extraPatternsDesignerPositions.map(pos => {
            return m('option', {
              value: pos.fen,
              selected: currentPosition && currentPosition.fen === pos.fen
            }, pos.name)
          })
        ]),
        m('button.button.patterns-analysis', {
          onclick: function() {
            ctrl.patternsAnalysis();
          }
        }, '将杀模式分析')
      ]),
      m('div.patterns-result', [
        ctrl.patternsAnalysisResult.loading ? m('div.spinner', [
          m('svg', { viewBox: '0 0 40 40' }, [
            m('circle', { cx: 20, cy: 20, r: 18, fill: 'none' })
          ])
        ]) : null,
        !ctrl.patternsAnalysisResult.loading && ctrl.patternsAnalysisErrors.length === 0 ? m('div.result', [
          ctrl.patternsAnalysisResult.colorLable ? m('div.tag', ctrl.patternsAnalysisResult.colorLable) : null,
          ctrl.patternsAnalysisResult.typeLabel ? m('div.tag', ctrl.patternsAnalysisResult.typeLabel) : null,
          ctrl.patternsAnalysisResult.op ? m(`a.op.${ctrl.flipped}`, { href: `/patterns/${ctrl.patternsAnalysisResult.type}/rank/feature?patternsOp=${ctrl.patternsAnalysisResult.op}&color=${ctrl.patternsAnalysisResult.color}&flipped=${ctrl.flipped}`}, ctrl.patternsAnalysisResult.op) : null
        ]) : null,
        !ctrl.patternsAnalysisResult.loading && ctrl.patternsAnalysisErrors.length > 0 ? m('ol.patterns-errors', ctrl.patternsAnalysisErrors.map(err => {
          return m('li', err);
        })) : null,
      ])
    ]),
    m('div.actions', [
      m('a.button.button-empty.text[data-icon=B]', {
        onclick: function() {
          ctrl.chessground.toggleOrientation();
          ctrl.setFlipped();
        }
      }, '翻转棋盘'),
      m('a.button.button-empty', {
        onclick: function() {
          showCreateSituation(ctrl, looksLegit, fen);
        }
      }, m('span.text[data-icon=]', '保存到局面库'))
    ])
  ]);
}

function inputs(ctrl, fen) {
  if (ctrl.embed) return;
  return m('div.copyables', [
    m('p', [
      m('strong', 'FEN'),
      m('input.copyable.autoselect[spellCheck=false]', {
        value: fen,
        onchange: function(e) {
          if (e.target.value !== fen) {
            ctrl.changeFen(e.target.value);
          }
        }
      })
    ]),
    !ctrl.data.patternsDesigner ? m('p', [
      m('strong.name', 'URL'),
      m('input.copyable.autoselect[readonly][spellCheck=false]', {
        value: editor.makeUrl(ctrl.data.baseUrl, fen)
      })
    ]) : null
  ]);
}

// can be 'pointer', 'trash', or [color, role]
function selectedToClass(s) {
  return (s === 'pointer' || s === 'trash') ? s : s.join(' ');
}

var lastTouchMovePos;

function sparePieces(ctrl, color, orientation, position) {

  var selectedClass = selectedToClass(ctrl.selected());

  var pieces = ['king', 'queen', 'rook', 'bishop', 'knight', 'pawn'].map(function(role) {
    return [color, role];
  });

  return m('div', {
    class: ['spare', 'spare-' + position, 'spare-' + color].join(' ')
  }, ['pointer'].concat(pieces).concat('trash').map(function(s) {

    var className = selectedToClass(s);

    var attrs = {
      class: className
    };

    var containerClass = 'no-square' +
      (
        (
          selectedClass === className &&
          (
            !ctrl.chessground ||
            !ctrl.chessground.state.draggable.current ||
            !ctrl.chessground.state.draggable.current.newPiece
          )
        ) ?
        ' selected-square' : ''
      );

    if (s === 'pointer') {
      containerClass += ' pointer';
    } else if (s === 'trash') {
      containerClass += ' trash';
    } else {
      attrs['data-color'] = s[0];
      attrs['data-role'] = s[1];
    }

    return m('div', {
      class: containerClass,
      onmousedown: onSelectSparePiece(ctrl, s, 'mouseup'),
      ontouchstart: onSelectSparePiece(ctrl, s, 'touchend'),
      ontouchmove: function(e) {
        lastTouchMovePos = eventPosition(e)
      }
    }, m('div', m('piece', attrs)));
  }));
}

function onSelectSparePiece(ctrl, s, upEvent) {
  return function(e) {
    e.preventDefault();
    if (['pointer', 'trash'].includes(s)) {
      ctrl.selected(s);
    } else {
      ctrl.selected('pointer');

      dragNewPiece(ctrl.chessground.state, {
        color: s[0],
        role: s[1]
      }, e, true);

      document.addEventListener(upEvent, function(e) {
        var eventPos = eventPosition(e) || lastTouchMovePos;

        if (eventPos && ctrl.chessground.getKeyAtDomPos(eventPos)) {
          ctrl.selected('pointer');
        } else {
          ctrl.selected(s);
        }
        m.redraw();
      }, {once: true});
    }
  };
}

function makeCursor(selected) {

  if (selected === 'pointer') return 'pointer';

  var name = selected === 'trash' ? 'trash' : selected.join('-');
  var url = lichess.assetUrl('cursors/' + name + '.cur');

  return 'url(' + url + '), default !important';
}

function showCreateSituation(ctrl, looksLegit, fen) {
  if(ctrl.notAccept) {
    window.lichess.memberIntro();
    return false;
  }

  $.ajax({
    url: `/resource/situationdb/rel/createForEditor?looksLegit=${looksLegit}&fen=${fen}`
  }).then(function (html) {
    $.modal($(html));
    $('.cancel').click(function () {
      $.modal.close();
    });

    $('.situation-create').find('form').find('#form3-tags').tagsInput({
      'height': '40px',
      'width': '100%',
      'interactive': true,
      'defaultText': '添加',
      'removeWithBackspace': true,
      'minChars': 0,
      'maxChars': 10,
      'placeholderColor': '#666666'
    });

    window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
      window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
        let $jq = $.noConflict(true);
        let $form = $jq('.situation-create').find('form');
        let $tree = $form.find('.dbtree');
        $tree.jstree({
          'core': {
            'worker': false,
            'data': {
              'url': '/resource/situationdb/tree/load'
            },
            'check_callback': true
          }
        })
        .on('changed.jstree', function (e, data) {
          if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
            $form.find('#form3-situationdbId').val(data.node.id);
          }
        });

        $form.submit(function (e) {
          e.preventDefault();
          let situationdbId = $form.find('#form3-situationdbId').val();
          $.ajax({
            method: 'POST',
            url: `/resource/situationdb/rel/create?situationdbId=${situationdbId}`,
            data: $form.serialize()
          }).then(function (res) {
            $.modal.close();
          }, function (err) {
            handleError(err);
          });
          return false;
        });
      })
    });
    return false
  });
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}

module.exports = function(ctrl) {
  var fen = ctrl.computeFen();
  var color = ctrl.bottomColor();
  var opposite = color === 'white' ? 'black' : 'white';

  return m('div.board-editor', {
    style: 'cursor: ' + makeCursor(ctrl.selected())
  }, [
    sparePieces(ctrl, opposite, color, 'top'),
    m('div.main-board', [
      chessground(ctrl),
      m('div.board-resize', {
        config: function(el, isUpdate) {
          if (!isUpdate) resizeHandle(el);
        }
      })
    ]),
    sparePieces(ctrl, color, color, 'bottom'),
    ctrl.data.patternsDesigner ? patternsDesignerControls(ctrl, fen) : null,
    !ctrl.data.patternsDesigner ? controls(ctrl, fen) : null,
    inputs(ctrl, fen)
  ]);
};
