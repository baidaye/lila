import { h, thunk } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import throttle from 'common/throttle';
import { option, onInsert } from '../util';
import AnalyseCtrl from '../ctrl';
import { StudyCtrl, StudyChapter } from './interfaces';

function editable(value: string, submit: (v: string, el: HTMLInputElement) => void): VNode {
  return h('input', {
    key: value, // force to redraw on change, to visibly update the input value
    attrs: {
      spellcheck: false,
      value
    },
    hook: onInsert<HTMLInputElement>(el => {
      el.onblur = function() {
        submit(el.value, el);
      };
      el.onkeypress = function(e) {
        if ((e.keyCode || e.which) == 13) el.blur();
      }
    })
  });
}

function fixed(text: string) {
  return h('span', text);
}

let selectedType: string;

type TagRow = (string | VNode)[];

function renderPgnTags(chapter: StudyChapter, submit, types: string[]): VNode {
  let rows: TagRow[] = [];
  if (chapter.setup.variant.key !== 'standard')
    rows.push(['Variant', fixed(chapter.setup.variant.name)]);
  rows = rows.concat(chapter.tags.map(tag => [
    tag[0],
    submit ? editable(tag[1], submit(tag[0])) : fixed(tag[1])
  ]));
  if (submit) {
    const existingTypes = chapter.tags.map(t => t[0]);
    rows.push([
      h('select', {
        hook: {
          insert: vnode => {
            const el = vnode.elm as HTMLInputElement;
            selectedType = el.value;
            el.addEventListener('change', _ => {
              selectedType = el.value;
              $(el).parents('tr').find('input').focus();
            });
          },
          postpatch: (_, vnode) => {
            selectedType = (vnode.elm as HTMLInputElement).value;
          }
        }
      }, [
        h('option', '- 请选择 -'),
        ...types.map(t => {
          if (!existingTypes.includes(t)) return option(t, '', getTagLabel(t));
        })
      ]),
      editable('', (value, el) => {
        if (selectedType) {
          submit(selectedType)(value);
          el.value = '';
        }
      })
    ]);
  }

  function getTagLabel(tag) {
    let tagMap = {
        'White': '白方', 'WhiteElo': '白方等级分', 'WhiteTitle': '白方头衔', 'WhiteTeam': '白方团队',
        'Black': '黑方', 'BlackElo': '黑方等级分', 'BlackTitle': '黑方头衔', 'BlackTeam': '黑方团队',
        'TimeControl': '对局时限', 'Date': '日期', 'Result': '对局结果', 'Termination': '结束方式', 'Site': '比赛地点',
        'Event': '赛事名称', 'Round': '轮次', 'Annotator': '评注者'
      };
    return tagMap[tag];
  }

  return h('table.study__tags.slist', h('tbody', rows.map(function(r) {
    return h('tr', {
      key: '' + r[0]
    }, [
      h('th', [r[0]]),
      h('td', [r[1]])
    ]);
  })));
}

export function ctrl(root: AnalyseCtrl, getChapter: () => StudyChapter, types) {

  const submit = throttle(500, function(name, value) {
    root.study!.makeChange('setTag', {
      chapterId: getChapter().id,
      name,
      value: value.substr(0, 140)
    });
  });

  return {
    submit(name) {
      return value => submit(name, value);
    },
    getChapter,
    types
  }
}
function doRender(root: StudyCtrl): VNode {
  return h('div', renderPgnTags(
    root.tags.getChapter(),
    root.vm.mode.write && root.tags.submit,
    root.tags.types))
}

export function view(root: StudyCtrl): VNode {
  const chapter = root.tags.getChapter(),
    tagKey = chapter.tags.map(t => t[1]).join(','),
    key = chapter.id + root.data.name + chapter.name + root.data.likes + tagKey + root.vm.mode.write;
  return thunk('div.' + chapter.id, doRender, [root, key]);
}

export function studyTagsView(root: StudyCtrl): VNode {
  return h(`div.study__study-tags.tags-${root.data.id}`,[
    h('input', {
      hook: {
        insert(vnode) { applyTags( vnode, root.setStudyTag) },
        postpatch(_, vnode) { applyTags(vnode, root.setStudyTag) }
      },
      attrs: {
        type: 'text',
        value: root.data.tags.join(','),
        placeholder: '添加标签'
      }
    })
  ]);
}

export function favoriteTagsView(root: StudyCtrl): VNode {
  return h(`div.study__study-tags.tags-${root.data.id}-${root.data.favorited}`,[
    h('input', {
      hook: {
        insert(vnode) { applyTags( vnode, root.setFavoriteTag) },
        postpatch(_, vnode) { applyTags(vnode, root.setFavoriteTag) }
      },
      attrs: {
        type: 'text',
        value: root.data.favoriteTags.join(','),
        placeholder: '添加标签'
      }
    })
  ]);
}

function applyTags(vnode: VNode, setTags:(tags) => void) {
  const $el = $(vnode.elm as HTMLElement);
  (<any>$el).tagsInput({
    'width': '100%',
    'height': '70px',
    'interactive': true,
    'defaultText': '添加标签',
    'removeWithBackspace': true,
    'minChars': 0,
    'maxChars': 10,
    'placeholderColor': '#666666',
    'onAddTag': function () {
      setTags($el.val());
    },
    'onRemoveTag': function () {
      setTags($el.val());
    }
  })
}
