import { h } from 'snabbdom'
import { Hooks } from 'snabbdom/hooks'

export const hasTouchEvents = 'ontouchstart' in window;

export const assetUrl = $('body').data('asset-url') + '/assets/';

export function bind(eventName: string, f: (e: Event) => any, redraw?: () => void): Hooks {
  return onInsert(el =>
    el.addEventListener(eventName, e => {
      const res = f(e);
      if (redraw) redraw();
      return res;
    })
  );
}

export function onInsert<A extends HTMLElement>(f: (element: A) => void): Hooks {
  return {
    insert: vnode => f(vnode.elm as A)
  };
}

export function dataIcon(icon: string) {
  return {
    'data-icon': icon
  };
}

export function spinner() {
  return h('div.spinner', [
    h('svg', { attrs: { viewBox: '0 0 40 40' } }, [
      h('circle', {
        attrs: { cx: 20, cy: 20, r: 18, fill: 'none' }
      })])]);
}

export function getQueryVariable(name) {
  let query = window.location.search.substring(1);
  query = decodeURIComponent(query);
  let vars = query.split("&");
  for (let i = 0; i < vars.length; i++) {
    let pair = vars[i].split("=");
    if (pair[0] == name) {
      return pair[1];
    }
  }
  return null;
}
