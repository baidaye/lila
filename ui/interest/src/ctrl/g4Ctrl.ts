import InterestController from "../ctrl";
import {InterestSubCtrl} from "../interfaces";
import * as cg from 'chessground/types';
import {DrawShape} from 'chessground/draw';
import {sound} from "../sound";

// 未保护棋子
export default function makeCtrl(superCtrl: InterestController): InterestSubCtrl {

  function canSelect(square: cg.Key) {
    // piece { type: 'p', color: 'b' }
    let piece = superCtrl.getChess().get(square);
    return piece && // 格子上有棋子
      piece.color === superCtrl.getData().color[0] && //格子上有棋子棋色与选中棋色相同
      piece.type !== 'k'; // 选中棋色的王不能选中
  }

  function onSelect(square: cg.Key) {
    if (isComplete()) {
      sound.end();
      superCtrl.score++;
      superCtrl.onComplete();
    } else {
      if(isRight(square)) {
        sound.win();
        superCtrl.score++;
        superCtrl.redraw();
      } else {
        sound.loss();
        superCtrl.redraw();
      }

    }
  }

  function isRight(square) {
    let rightSquare = superCtrl.getData().rightSquare;
    return rightSquare.includes(square);
  }

  function isComplete() {
    let rightSquare = superCtrl.getData().rightSquare;
    let selectSquares = superCtrl.getSelectSquares();
    return rightSquare.sort().toString() == selectSquares.filter(s => isRight(s)).sort().toString();
  }

  function showSolution() {
    let rightSquare = superCtrl.getData().rightSquare;
    let shapes = rightSquare.map(square => {
      return {
        brush: 'green',
        orig: square
      }
    });
    superCtrl.chessground.setAutoShapes(shapes.concat(colorKingShapes()) as DrawShape[]);
  }

  function defaultShapes(): DrawShape[] {
    let selectSquares = superCtrl.getSelectSquares();
    return selectSquares.map(square => {
      return {
        brush: isRight(square) ? 'green' : 'red',
        orig: square
      }
    }).concat(colorKingShapes());
  }

  function colorKingShapes() {
    return [
      {
        brush: 'yellow',
        orig: superCtrl.getData().colorKingSquare
      }
    ]
  }

  return {
    defaultShapes,
    filterDests(dests: cg.Dests){ console.log(dests);return {}; },
    onSendMove() { return false },
    canSelect,
    onSelect,
    onComplete() {},
    onFailed() {},
    showSolution
  }

}
