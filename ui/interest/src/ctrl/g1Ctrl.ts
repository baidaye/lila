import InterestController from "../ctrl";
import {InterestSubCtrl} from "../interfaces";
import * as cg from 'chessground/types';
import {DrawShape} from 'chessground/draw';
import {sound} from "../sound";

// 吃星星
export default function makeCtrl(superCtrl: InterestController): InterestSubCtrl {

  function onSendMove() : boolean {
    let lastMove = superCtrl.getLastMove();
    if (lastMove && superCtrl.isCapture(lastMove[1])) {
      if (isComplete()) {
        sound.end();
        superCtrl.score++;
        superCtrl.onComplete();
        return false;
      } else {
        sound.win();
        superCtrl.score++;
        superCtrl.redraw();
      }
    } else {
      sound.move();
    }
    return true;
  }

  function isComplete() {
    return superCtrl.path.length === 0;
  }

  function showSolution() {
    const shapes = superCtrl.data.uciPath.map(uci => {
      return {
        brush: 'green',
        orig: uci.slice(0, 2),
        dest: uci.slice(2, 4)
      }
    });
    superCtrl.chessground.setAutoShapes(shapes as DrawShape[]);
  }

  return {
    defaultShapes() { return [] },
    filterDests(dests: cg.Dests) { return dests; },
    onSendMove,
    canSelect(square: cg.Key) {console.log(square); return true;},
    onSelect(square: cg.Key) {console.log(square);},
    onComplete() {},
    onFailed() {},
    showSolution
  }
}
