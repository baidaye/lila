import InterestController from "../ctrl";
import {InterestSubCtrl} from "../interfaces";
import * as cg from 'chessground/types';
import {DrawShape} from 'chessground/draw';
import {sound} from "../sound";

// 单子攻王
export default function makeCtrl(superCtrl: InterestController): InterestSubCtrl {

  function filterDests(dests: cg.Dests): cg.Dests {
    let lastMove = superCtrl.getLastMove();
    let key = lastMove ? lastMove[1] : superCtrl.getData().square;
    return { [key]: dests[key] };
  }

  function onSendMove() : boolean {
    let lastMove = superCtrl.getLastMove();
    if (isFailed()) {
      sound.loss();
      superCtrl.onFailed();
      return false;
    } else {
      if (isComplete()) {
        sound.end();
        superCtrl.score++;
        superCtrl.onComplete();
        return false;
      } else {
        if (lastMove && superCtrl.isCapture(lastMove[1])) {
          sound.capture();
        } else {
          sound.move();
        }
      }
    }
    return true;
  }

  function isFailed() {
    let lastMove = superCtrl.getLastMove();
    let captures = superCtrl.findCaptures();
    return captures.length > 0 && captures.filter(x => lastMove && x.dest === lastMove[1]).length > 0;
  }

  function isComplete() {
    return superCtrl.chess.in_check();
  }

  function onComplete() {
    superCtrl.showGround();
  }

  function onFailed() {
    let lastMove = superCtrl.getLastMove();
    let captures = superCtrl.findCaptures();

    const shapes = captures.filter(item => lastMove && item.dest === lastMove[1]).map(obj => {
      return { ...{ brush: 'red' }, ...obj };
    });
    let defaultShapes = superCtrl.chessground.state.drawable.autoShapes;
    superCtrl.chessground.setAutoShapes((shapes as DrawShape[]).concat(defaultShapes));
  }

  function showSolution() {
    const shapes = superCtrl.data.uciPath.map(uci => {
      return {
        brush: 'green',
        orig: uci.slice(0, 2),
        dest: uci.slice(2, 4)
      }
    });
    superCtrl.chessground.setAutoShapes((shapes as DrawShape[]).concat(defaultShapes()));
  }

  function defaultShapes(): DrawShape[] {
    let lastMove = superCtrl.getLastMove();
    return [
      {
        brush: 'green',
        orig: lastMove ? lastMove[1] : superCtrl.getData().square,
        modifiers: {
          lineWidth: 8
        }
      },
      {
        brush: 'red',
        orig: superCtrl.getData().kingSquare,
        modifiers: {
          lineWidth: 8
        }
      }
    ]
  }

  return {
    defaultShapes,
    filterDests,
    onSendMove,
    canSelect(square: cg.Key) {console.log(square);return true;},
    onSelect(square: cg.Key) {console.log(square);},
    onComplete,
    onFailed,
    showSolution
  }

}
