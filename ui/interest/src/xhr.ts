const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function next(sc, id, prevResult, prevStep) {
  let param = window.location.search.substring(1).replace("next=false", "next=true");
  return $.ajax({
    url: `/fun/${sc}/${id}/next?${param}&prevResult=${prevResult}&prevStep=${prevStep}`,
    headers: headers
  });
}

export function finish(sc, id, data) {
  return $.ajax({
    method: 'post',
    url: `/fun/${sc}/${id}/finish`,
    data: data,
    headers: headers,
  });
}

export function like(sc, id) {
  return $.ajax({
    method: 'post',
    url: `/fun/${sc}/${id}/like`,
    headers: headers,
  });
}

export function setLikeTag(sc, id, tags) {
  return $.ajax({
    method: 'post',
    url: `/fun/${sc}/${id}/setLikeTag`,
    data: {
      tags: tags
    },
    headers: headers,
  });
}

export function storeSearch(sc, data) {
  return $.ajax({
    method: 'post',
    url: `/fun/${sc}/storeSearch`,
    data: data,
    headers: headers,
  });
}
