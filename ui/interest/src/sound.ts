import {assetUrl} from "./util";
import throttle from "../../common/throttle";

const sounds = window.lichess.sound;
const soundUrl = assetUrl + '/sound/';

let make = function(file, volume) {
  let sound = new window.Howl({
    src: [
      soundUrl + file + '.ogg',
      soundUrl + file + '.mp3'
    ],
    volume: volume || 1
  });
  return function() {
    if (window.lichess.sound.set() !== 'silent') sound.play();
  };
};

export const sound = {
  move: throttle(50, sounds.move),
  capture: throttle(50, sounds.capture),
  win: throttle(50, make('sfx/Tournament3rd', 1)),
  loss: throttle(50, make('other/no-go', 1)),
  start: make('other/ping', 1),
  end: make('other/energy3', 1)
};
