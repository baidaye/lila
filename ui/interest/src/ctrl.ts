import {storedProp, StoredBooleanProp} from 'common/storage';
import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import * as chessUtil from 'chess';
import * as ChessJS from "chess.js";
import InterestSocket from './socket';
import {InterestData, InterestOpts, InterestSubCtrl, JustCaptured} from './interfaces';
import * as promotion from './view/promotion';
import {getQueryVariable} from './util';
import makeG1Ctrl from './ctrl/g1Ctrl';
import makeG2Ctrl from './ctrl/g2Ctrl';
import makeG3Ctrl from './ctrl/g3Ctrl';
import makeG4Ctrl from './ctrl/g4Ctrl';
import makeG5Ctrl from './ctrl/g5Ctrl';
import {sound} from './sound';
import * as xhr from './xhr';

export default class InterestController {

  opts: InterestOpts;
  chess: any; // chess.js
  chessground: CgApi;
  cgConfig: CgConfig;
  flipped: boolean = false;
  socket: InterestSocket;
  subCtrl: InterestSubCtrl;

  isAutoNext: StoredBooleanProp = storedProp('interest-auto-next', false);

  orientation: string = 'attacker';
  loading: boolean = false;
  data: InterestData;
  lastMove?: cg.Key[];
  path: cg.Key[];
  nbMoves: number = 0;
  moveLines: string[] = [];
  selectSquares: cg.Key[] = [];
  score: number = 0;
  stars: number = 0;
  completed: boolean = false;
  success: boolean = false;
  viewOnly: boolean = false;
  prevResult: number = 0; //-1,0,1

  constructor(opts: InterestOpts, readonly redraw: () => void) {
    this.opts = opts;
    this.socket = new InterestSocket(opts.socketSend, this);
    this.chess = ChessJS.Chess();
    this.subCtrl = this.makeSubCtrl();
    this.orientation = getQueryVariable("orientation") || 'attacker';
    this.data = opts.data;
    this.initData(false);
    this.resetForm();
    this.submitForm();
  }

  makeSubCtrl = (): InterestSubCtrl => {
    switch (this.opts.source.id) {
      case 'g1':
        return makeG1Ctrl(this);
      case 'g2':
        return makeG2Ctrl(this);
      case 'g3':
        return makeG3Ctrl(this);
      case 'g4':
        return makeG4Ctrl(this);
      case 'g5':
        return makeG5Ctrl(this);
      default:
        throw new Error('can not init SubCtrl');
    }
  };

  getData = () => {
    return this.data;
  };

  getChess = () => {
    return this.chess;
  };

  getSelectSquares = () => {
    return this.selectSquares;
  };

  getLastMove = () => {
    return this.lastMove;
  };

  isLogin = () => {
    return !!this.opts.userId;
  };

  notFound = () => {
    return this.opts.nf;
  };

  initData = (sd: boolean = true) => {
    this.path = [...this.data.path];
    this.lastMove = undefined;
    this.moveLines = [];
    this.selectSquares = [];
    this.nbMoves = 0;
    this.score = 0;
    this.stars = 0;
    this.completed = false;
    this.success = false;
    this.viewOnly = false;

    if(!this.notFound()) {
      this.setUri();
      this.chess.load(this.data.fen);
      this.showGround();
      if(sd) {
        sound.start();
      }
    } else {
      alert('没有更多题目啦，换个条件吧~');
    }
  };

  makeCgOpts(): CgConfig {
    const fen = this.chess.fen();
    const color = this.data.color;
    const check = this.chess.in_check();
    const dests = this.completed ? {} : this.toDests();
    const config: CgConfig = {
      fen: fen,
      turnColor: color,
      viewOnly: false,
      movable: {
        color: color,
        dests: this.viewOnly ? {} : dests
      },
      check: check,
      lastMove: this.lastMove,
      selected: this.lastMove ? this.lastMove[1] : undefined,
      selectable: {
        enabled: !this.viewOnly
      },
      drawable: {
        enabled: false,
        shapes: [],
        autoShapes: []
      }
    };

    if (check) {
      config.turnColor = opposite(color);
    }
    config.orientation = this.bottomColor();
    this.cgConfig = config;
    return config;
  }

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  showGround = (): void => {
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
      cg.setAutoShapes(this.subCtrl.defaultShapes());
    });
  };

  onGroundInit = () => {
    this.chessground.setAutoShapes(this.subCtrl.defaultShapes());
  };

  toDests = (): cg.Dests => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({
        square: s,
        verbose: true,
        legal: false
      });
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return this.subCtrl.filterDests(dests);
  };

  bottomColor = (): Color => {
    let o;
    if(this.orientation === 'attacker') {
      o = (this.opts.source.id === 'g4' || this.opts.source.id === 'g5') ? opposite(this.data.color) : this.data.color
    } else if(this.orientation === 'defend') {
      o = (this.opts.source.id === 'g4' || this.opts.source.id === 'g5') ? this.data.color : opposite(this.data.color)
    } else {
      o = this.orientation as Color
    }
    return this.flipped ? opposite(o) : o;
  };

  getColor = () => {
    return this.chess.turn() == 'w' ? 'white' : 'black';
  };

  setColor = (c) => {
    let turn = c === 'white' ? 'w' : 'b';
    let newFen = this.setFenTurn(this.chess.fen(), turn);
    this.chess.load(newFen);
    if (this.getColor() !== c) {
      // the en passant square prevents setting color
      newFen = newFen.replace(/ (w|b) ([kKqQ-]{1,4}) \w\d /, ' ' + turn + ' $2 - ');
      this.chess.load(newFen);
    }
  };

  setFenTurn = (fen, turn) => {
    return fen.replace(/ (w|b) /, ' ' + turn + ' ');
  };

  uciToLastMove = (lm?: string): Key[] | undefined => {
    return lm ? ([lm[0] + lm[1], lm[2] + lm[3]] as Key[]) : undefined;
  };

  findCaptures = () => {
    return this.chess.moves({
      verbose: true
    }).filter(function(move) {
      return move.captured;
    }).map(function(move) {
      return {
        orig: move.from,
        dest: move.to
      };
    });
  };

  isCapture = (dest: cg.Key) => {
    if(this.path.includes(dest)) {
      this.path = this.path.filter(p => p !== dest);
      return true;
    }
    return false;
  };

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    if (!promotion.start(this, orig, dest, capture, this.sendMove)) {
      this.sendMove(orig, dest, capture);
    }
  };

  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    this.lastMove = [orig, dest];
    this.chess.move({
      from: orig,
      to: dest,
      promotion: prom ? chessUtil.roleToSan[prom].toLowerCase() : null
    });

    this.nbMoves++;
    this.moveLines.push(orig + dest + (prom ? chessUtil.roleToSan[prom].toLowerCase() : ''));

    let isContinue =  this.subCtrl.onSendMove();
    if(isContinue) {
      this.setColor(this.data.color);
      this.showGround();
    }
    if(capture) console.log(capture);
  };

  onSelect = (key: cg.Key) => {
    if((this.opts.source.id === 'g4' || this.opts.source.id === 'g5') &&
      !this.selectSquares.includes(key) &&
      !this.completed &&
      this.subCtrl.canSelect(key)
    ) {
      this.nbMoves++;
      this.selectSquares.push(key);
      this.showGround();
      this.subCtrl.onSelect(key);
    }
  };

  onComplete = () => {
    this.completed = true;
    this.success = true;
    this.stars = this.calcStar();
    this.prevResult = this.calcResult();
    this.redraw();
    this.subCtrl.onComplete();
    this.finish();
  };

  onFailed = () => {
    this.viewOnly = true;
    this.completed = true;
    this.success = false;
    this.prevResult = this.calcResult();
    this.redraw();
    this.showGround();
    this.subCtrl.onFailed();
    this.finish();
  };

  calcStar = () => {
    if(!this.success) return 0;
    else {
      if(this.nbMoves === this.data.steps) return 3;
      else if(this.nbMoves - 1 === this.data.steps) return 2;
      else return 1;
    }
  };

  calcResult = () => {
    if(this.success) {
      if(this.stars === 3) {
        return 1;
      } else {
        return 0;
      }
    } else {
      return -1;
    }
  };

  solution = () => {
    if(this.notFound()) {
      return;
    }

    if(!this.isLogin()) {
      location.href = `/login?referrer=/fun/${this.opts.source.id}/${this.data.id}`;
      return;
    }

    this.viewOnly = true;
    this.lastMove = undefined;
    this.chess.load(this.data.fen);
    this.showGround();
    this.subCtrl.showSolution();
  };

  retry = () => {
    if(this.notFound()) {
      return;
    }

    this.initData();
    this.redraw();
  };

  next = () => {
    if(this.notFound()) {
      return;
    }

    if(!this.isLogin()) {
      location.href = `/login?referrer=/fun/${this.opts.source.id}/${this.data.id}`;
      return;
    }

    xhr.next(this.opts.source.id, this.data.id, this.prevResult, this.data.steps).then(data => {
      this.data = data;
      this.initData();
      this.setUri();
      this.redraw();
    }).fail(function(err) {
      if(err.status === 404) {
        alert('没有更多题目啦，换个条件吧~');
      } else if(err.status === 406) {
        alert('条件错误，请刷新或重新进入页面');
      } else {
        alert(JSON.stringify(err.responseText));
      }
    });
  };

  like = () => {
    if(this.notFound()) {
      return;
    }

    if(!this.isLogin()) {
      location.href = `/login?referrer=/fun/${this.opts.source.id}/${this.data.id}`;
      return;
    }

    xhr.like(this.opts.source.id, this.data.id).then(_ => {
      this.data.liked = !this.data.liked;
      if(!this.data.liked) {
        this.data.tags = [];
      }
      this.redraw();
    })
  };

  setLikeTag = (tags) => {
    if(this.notFound()) {
      return;
    }

    if(!this.isLogin()) {
      location.href = `/login?referrer=/fun/${this.opts.source.id}/${this.data.id}`;
      return;
    }

    xhr.setLikeTag(this.opts.source.id, this.data.id, tags).then(_ => {
      this.data.liked = true;
      this.data.tags = tags.split(',');
      this.redraw();
    })
  };

  finish = () => {
    xhr.finish(this.opts.source.id, this.data.id, {
      id: this.data.id,
      nbMoves: this.nbMoves,
      score: this.score,
      stars: this.stars,
      success: this.success,
      lines: this.moveLines,
      squares: this.selectSquares
    }).then(_ => {
      this.redraw();
      this.autoNext();
    })
  };

  autoNext = () => {
    let auto = getQueryVariable("autoNext") || 'false';
    if(auto === 'true' && this.stars === 3) {
      setTimeout(() => {
        this.next();
      }, 1000);
    }
  };

  setUri = () => {
    let param = window.location.search.substring(1);
    param = param.replace("next=true", "next=false");
    let url = `/fun/${this.opts.source.id}/${this.data.id}${param ? `?${param}` : ''}`;
    window.history.replaceState(null, '', url);
  };

  storeSearch = () => {
    return xhr.storeSearch(this.opts.source.id, $('.search_form').serialize())
  };

  resetForm = () => {
    let $form = $('.search_form');
    $form.find('.reset').click(() => {
      $form.find('input[name="color"]').prop('checked', false);
      switch (this.opts.source.id) {
        case 'g1':
        case 'g2':
        case 'g3':
          $form.find('#color_white').prop('checked', true);
          break;
        case 'g4':
        case 'g5':
          $form.find('#color_black').prop('checked', true);
          break;
      }

      $form.find('input[name="role"]').prop('checked', false);
      $form.find('#role_').prop('checked', true);

      $form.find('input[name="phase"]').prop('checked', false);
      $form.find('#phase_').prop('checked', true);

      $form.find('input[name="step"]').prop('checked', false);
      $form.find('#step_adapt').prop('checked', true);

      $form.find('input[name="orientation"]').prop('checked', false);
      $form.find('#orientation_attacker').prop('checked', true);

      $form.find('#form3-autoNext').prop('checked', false);

      $form.submit();
    });
  };

  submitForm = () => {
    if(this.isLogin()) {
      let $form = $('.search_form');
      let stored = false;
      $form.submit((e) => {
        if(!stored) {
          e.preventDefault();

          this.storeSearch().then(() => {
            stored = true;
            $form.submit();
          });
        }
      });
    }
  }

}

