import InterestController from './ctrl';

interface Handlers {
  [key: string]: (data: any) => void;
}
export default class InterestSocket {

  send: SocketSend;
  handlers: Handlers;

  constructor(send: SocketSend, ctrl: InterestController) {
    console.log(ctrl.data);
    this.send = send;
    this.handlers = {

    }
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  }
};
