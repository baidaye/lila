import { VNode } from 'snabbdom/vnode'
import * as cg from 'chessground/types';
import {DrawShape} from 'chessground/draw';

export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

interface Untyped {
  [key: string]: any;
}

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}

export interface InterestOpts extends Untyped {
  element: HTMLElement;
  socketSend: SocketSend;
  userId: string;
  notAccept: boolean;
  pref: any;
  data: InterestData;
  source: any;
}

export interface InterestData {
  id: number;
  fen: Fen;
  role: cg.Role;
  color: Color;
  phase: string;
  square: Key;
  kingSquare: Key;
  colorKingSquare: Key;
  path: Key[];
  uciPath: Uci[];
  rightPiece: cg.Role[];
  rightSquare: Key[];
  wrongSquare: Key[];
  steps: number;
  source: any;
  attempts: number;
  likes: number;
  liked: boolean;
  tags: string[];
}

export interface InterestSubCtrl {
  defaultShapes(): DrawShape[];
  filterDests(dests: cg.Dests): cg.Dests;
  onSendMove(): boolean;
  canSelect(square: cg.Key): boolean;
  onSelect(square: cg.Key): void;
  onComplete(): void;
  onFailed(): void;
  showSolution(): void;
}
