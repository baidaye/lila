import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import * as gridHacks from './gridHacks';
import InterestController from '../ctrl';
import {view as renderPromotion} from './promotion';
import * as chessground from './ground';
import * as table from './table';
import { metas, tags, tagBox } from './side';

let drawer;
export default function(ctrl: InterestController): VNode {
  let cls = {};
  cls[ctrl.opts.source.id] = true;
  return h(`main.interest`, {
    class: cls,
    hook: {
      postpatch(_, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
        if (!drawer) {
          drawer = (<any>$('.drawer')).drawer();
        }
      }
    }
  }, [
    h('aside.interest__side', [
      metas(ctrl),
      tagBox(ctrl),
      tags(ctrl)
    ]),
    h('div.interest__board.main-board', [
      chessground.render(ctrl),
      chessground.mask(ctrl),
      renderPromotion(ctrl),
    ]),
    h('div.interest__table', [
      table.main(ctrl),
      table.actions(ctrl)
    ])
  ]);
}
