import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Chessground} from 'chessground';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import * as cg from 'chessground/types';
import InterestController from "../ctrl";

export function render(ctrl: InterestController): VNode {
  return h('div.cg-wrap', {
    class: {
      'star-white': ctrl.opts.source.id === 'g1' && ctrl.data.color === 'white',
      'star-black': ctrl.opts.source.id === 'g1' && ctrl.data.color === 'black'
    },
    hook: {
      insert: vnode => {
        ctrl.chessground = Chessground((vnode.elm as HTMLElement), makeConfig(ctrl));
        ctrl.onGroundInit();
      },
      destroy: _ => ctrl.chessground.destroy()
    }
  });
}

export function promote(ground: CgApi, key: Key, role: cg.Role) {
  const pieces = {};
  const piece = ground.state.pieces[key];
  if (piece && piece.role == 'pawn') {
    pieces[key] = {
      color: piece.color,
      role,
      promoted: true
    };
    ground.setPieces(pieces);
  }
}

export function mask(ctrl: InterestController): VNode | null {
  let position = '';
  let bottomColor = ctrl.bottomColor();
  switch (bottomColor) {
    case 'white':
      position = ctrl.data.color === 'white' ? 'top' : 'bottom';
      break;
    case 'black':
      position = ctrl.data.color === 'white' ? 'bottom' : 'top';
      break;
  }

  let offset = $('#main-wrap').hasClass('is3d') ? '6px': 0;
  let symbol = position === 'bottom' ? '' : '-';

  return ctrl.opts.source.id === 'g5' ? h('div.board-mask', {attrs: { style: `${position}: ${symbol}${offset}` }}) : null;
}

export function makeConfig(ctrl: InterestController): CgConfig {
  const pref = ctrl.opts.pref;
  const opts = ctrl.makeCgOpts();
  const config = {
    turnColor: opts.turnColor,
    fen: opts.fen,
    check: opts.check,
    lastMove: opts.lastMove,
    orientation: opts.orientation,
    coordinates: pref.coords !== 0,
    addPieceZIndex: pref.is3d,
    viewOnly: opts.viewOnly,
    movable: {
      free: false,
      color: opts.movable!.color,
      dests: opts.movable!.dests,
      showDests: pref.destination,
      rookCastle: pref.rookCastle
    },
    events: {
      move: ctrl.userMove,
      select: function (key) {
        ctrl.onSelect(key);
      },
    },
    premovable: {
      enabled: false,
    },
    drawable: {
      enabled: false,
      eraseOnClick: false
    },
    highlight: {
      lastMove: pref.highlight,
      check: pref.highlight
    },
    animation: {
      duration: pref.animationDuration
    },
    disableContextMenu: true
  };
  return config;
}
