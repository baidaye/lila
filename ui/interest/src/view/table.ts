import {h} from 'snabbdom';
import {bind, spinner, dataIcon} from '../util';
import InterestController from "../ctrl";

export function main(ctrl: InterestController) {
  // @ts-ignore
  let stars = Array.from({length: ctrl.stars});
  return h('div.main', [
    h('div.score', {class: {failed: ctrl.completed && !ctrl.success}}, ctrl.score.toString()),
    ctrl.completed && ctrl.success ? h('div.stars', stars.map(() => {
      return h('i', {attrs: dataIcon('t')})
    })) : null,
    ctrl.completed ? h('div.result', result(ctrl)) : null
  ]);
}

export function actions(ctrl: InterestController) {
  return h('div.actions', [
    ctrl.loading ? spinner() : h('div', [
      h('button.button.button-empty', {
        hook: bind('click', _ => {
          ctrl.solution();
        })
      }, '显示答案'),
      h('button.button.button-empty', {
        hook: bind('click', _ => {
          ctrl.retry();
        })
      }, '重试'),
      h('button.button', {
        hook: bind('click', _ => {
          ctrl.next();
        })
      }, '下一题')
    ])
  ]);
}

function result(ctrl: InterestController) {
  if(ctrl.success) {
    switch (ctrl.stars) {
      case 1:  return '干的不错';
      case 2:  return '非常好';
      case 3:  return '棒极了';
      default:  return '666';
    }
  } else {
    return '失败了';
  }
}
