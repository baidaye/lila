import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode';
import {bind} from '../util';
import InterestController from '../ctrl';

export function metas(ctrl: InterestController) {
  return h('div.metas.box', [
    h('h1', ctrl.opts.source.name),
    h('div.desc', ctrl.opts.source.desc),
    h('div.show', [
      h('a', {attrs: { href: ctrl.notFound() ? '' : `/fun/${ctrl.opts.source.id}/${ctrl.data.id}` }}, `${ctrl.data.id}号题`),
      h('a', {
        attrs: {
          'data-icon': ctrl.data.liked ? 't' : 's',
          title: ctrl.data.liked ? '取消收藏': '收藏',
          class: 'like'
        },
        hook: bind('click', () => {
          ctrl.like();
        })
      })
    ]),
    h('div.attempts', ['已被尝试 ', ctrl.data.attempts.toString(), ' 次'])
  ]);
}

export function tagBox(ctrl: InterestController) {
  if (!ctrl.isLogin()) return;
  return h(`div.tagBox.tags-${ctrl.data.id}-${ctrl.data.liked}`, [
    h('input', {
      attrs: {
        id: 'taginput',
        type: 'text',
        value: ctrl.data.tags.join(','),
        placeholder: '添加标签'
      },
      hook: {
        insert(vnode) { applyTags( vnode, ctrl) },
        postpatch(_, vnode) { applyTags(vnode, ctrl) }
      }
    })
  ]);
}

function applyTags(vnode: VNode, ctrl: InterestController) {
  const $el = $(vnode.elm as HTMLElement);
  (<any>$el).tagsInput({
    'width': '100%',
    'height': '70px',
    'interactive': true,
    'defaultText': '添加标签',
    'removeWithBackspace': true,
    'minChars': 0,
    'maxChars': 10,
    'placeholderColor': '#666666',
    'onAddTag': function () {
      ctrl.setLikeTag($el.val());
    },
    'onRemoveTag': function () {
      ctrl.setLikeTag($el.val());
    }
  })
}

export function tags(ctrl: InterestController) {
  const color = ctrl.data.color === 'white' ? '白棋' : '黑棋';
  const steps = `${ctrl.data.steps} ${stepText(ctrl)}`;
  const role = roleName(ctrl.data.role);
  const phase = phaseName(ctrl.data.phase);

  return h('div.tags', [
    h('span', color),
    h('span', steps),
    ['g1', 'g2', 'g3'].includes(ctrl.opts.source.id) ? h('span', role) : null,
    ['g4', 'g5'].includes(ctrl.opts.source.id) ? h('span', phase) : null
  ]);
}

function stepText(ctrl: InterestController) {
  switch (ctrl.opts.source.id) {
    case 'g1':
    case 'g2':
    case 'g3':
      return '步';
    case 'g4':
      return '子';
    case 'g5':
      return '格子';
  }
}

function roleName(r) {
  let n = '';
  switch (r) {
    case 'king':
      n = '王';
      break;
    case 'queen':
      n = '后';
      break;
    case 'rook':
      n = '车';
      break;
    case 'bishop':
      n = '象';
      break;
    case 'knight':
      n = '马';
      break;
    case 'pawn':
      n = '兵';
      break;
  }
  return n;
}

function phaseName(phase) {
  let n = '';
  switch (phase) {
    case 'Opening':
      n = '开局';
      break;
    case 'MiddleGame':
      n = '中局';
      break;
    case 'EndingGame':
      n = '残局';
      break;
  }
  return n;
}
