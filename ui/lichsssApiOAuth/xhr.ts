const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function oAuthCreate(data) {
  return $.ajax({
    method: 'post',
    url: '/lichess/oauth/create',
    data: data,
    headers: headers
  });
}
