// 参考：https://lichess.org/api#tag/OAuth/operation/oauth
import {readStream} from './ndJsonStream';
import {OAuth2Data} from './interfaces';
import * as xhr from './xhr';

export default class OAuth2PKCE {

  PKCE_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';
  lichessHost: string = 'https://lichess.org';
  clientId: string = 'haichess.com';
  redirectUrl: string = `${location.protocol}//${location.host}/lichess/oauth/callback`;
  scopes: string[] = ['board:play', 'challenge:write', 'challenge:read'];

  controller: AbortController;
  signal: AbortSignal;

  constructor(readonly oAuth2: OAuth2Data) {
    this.controller = new AbortController();
    this.signal = this.controller.signal;
  }

  isLichessAuthed = () => {
    return this.oAuth2 && !this.oAuth2.expired && this.oAuth2.accessToken;
  };

  base64urlEncode = (value: string) => {
    let base64 = btoa(value);
    base64 = base64.replace(/\+/g, '-');
    base64 = base64.replace(/\//g, '_');
    base64 = base64.replace(/=/g, '');
    return base64;
  };

  generatePKCECodes = async () => {
    const output = new Uint32Array(96);
    crypto.getRandomValues(output);
    const codeVerifier = this.base64urlEncode(Array
      .from(output)
      .map((num: number) => this.PKCE_CHARSET[num % this.PKCE_CHARSET.length])
      .join(''));

    return crypto
      .subtle
      .digest('SHA-256', (new TextEncoder()).encode(codeVerifier))
      .then((buffer: ArrayBuffer) => {
        let hash = new Uint8Array(buffer);
        let binary = '';
        let hashLength = hash.byteLength;
        for (let i: number = 0; i < hashLength; i++) {
          binary += String.fromCharCode(hash[i]);
        }
        return binary;
      })
      .then(this.base64urlEncode)
      .then((codeChallenge: string) => ({ codeChallenge, codeVerifier }));
  };

  generateState = () => {
    const output = new Uint32Array(32);
    crypto.getRandomValues(output);
    return Array
      .from(output)
      .map((num: number) => this.PKCE_CHARSET[num % this.PKCE_CHARSET.length])
      .join('');
  };

  redirectLichessOAuth2 = async() => {
    const {codeChallenge, codeVerifier} = await this.generatePKCECodes();
    const state = this.generateState();
    let authUrl = `${this.lichessHost}/oauth`;
    let url = authUrl
      + `?response_type=code&`
      + `client_id=${encodeURIComponent(this.clientId)}&`
      + `redirect_uri=${encodeURIComponent(this.redirectUrl)}&`
      + `scope=${encodeURIComponent(this.scopes.join(' '))}&`
      + `state=${state}&`
      + `code_challenge=${encodeURIComponent(codeChallenge)}&`
      + `code_challenge_method=S256`;

    // store {codeVerifier, state} to backend
    xhr.oAuthCreate({state, verifierCode: codeVerifier, scopes: this.scopes}).then(() => {
      location.replace(url);
    });
  };

  openStream = async (path: string, config: any, handler: (_: any) => void, onDisconnect?:() => void) => {
    const stream = await this.fetch(path, config);
    return readStream(`STREAM ${path}`, stream, handler, onDisconnect);
  };

  openBody = async (path: string, config: any = {}) => {
    const res = await this.fetch(path, config);
    const body = await res.json();
    return body;
  };

  fetch = async (path: string, config: any = {}) => {
    const authConfig: any = Object.assign({}, config);
    if (!authConfig.headers) {
      authConfig.headers = {};
    }

    authConfig.headers['Content-Type'] = `application/x-www-form-urlencoded`;
    authConfig.headers['Authorization'] = `Bearer ${this.oAuth2.accessToken}`;
    const response = await window.fetch(`${this.lichessHost}${path}`, authConfig);
    if (!response.ok) {
      response.json().then(data => {
        alert(`${response.status} - ${JSON.stringify(data.error)}`);
      });
    }
    return response;
  };

  formData = (data: any): FormData => {
    const formData = new FormData();
    for (const k of Object.keys(data)) formData.append(k, data[k]);
    return formData;
  };

  makeRoundData = (gameStart, clock: any) => {
    let c = gameStart.color;
    let op = c === 'white' ? 'black' : 'white';
    let p = this.oAuth2.lichessUser!;
    let o = gameStart.opponent;

    let d = {
      gameId: gameStart.id,
      clock: clock,
      rated: gameStart.rated,
      status: gameStart.status.id,
      source: gameStart.source,
      gameStart: JSON.stringify(gameStart)
    };

    d[c] = {
      id: p.id,
      username: p.username,
      rating: -1,
      userId: this.oAuth2.userId,
      color: c
    };

    d[op] = {
      id: o.id,
      username: o.username,
      rating: o.rating,
      title: o.title,
      provisional: o.provisional,
      color: op
    };

    return d;
  };

}

