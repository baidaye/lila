export interface OAuth2Data {
  code: string;
  scopes: string;
  accessToken?: string;
  lichessUser?: OAuth2LichessUser;
  expired: boolean;
  longTerm: boolean;
  userId: string;
}

export interface OAuth2LichessUser {
  id: string;
  username: string;
}
