import TaskCtrl from './ctrl';
import {FromPositionWithResult} from './interfaces';

export default class ItemFromPositionGameTaskCtrl {

  itemType: string = 'fromPosition';
  maxNum: number = 5;

  emptyFen = '8/8/8/8/8/8/8/8 w - -';
  initialFen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  fromPositionGames: FromPositionWithResult[] = [];

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.fromPositionWithResult) {
        this.fromPositionGames = this.ctrl.opts.task.item.fromPositionWithResult;
        this.ctrl.num = this.fromPositionGames.reduce((total, fpg) => {
            return total + fpg.fromPosition.num;
        }, 0);
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.fromPositionGames = [];
  };

  validateFen = (fenElm: HTMLInputElement, index: number) => {
    const fen = fenElm.value;
    if (fen) {
      $.ajax({
        url: '/setup/validate-fen?strict=0',
        data: {
          fen: fen
        },
        success: (data) => {
          fenElm.setCustomValidity('');
          fenElm.className = '';
          this.setFromPositionFen(index, data);
        },
        error: () => {
          fenElm.setCustomValidity('FEN 格式无效');
          fenElm.className = 'is-invalid';
          this.setFromPositionFen(index, '');
        }
      });
    }
  };

  loadSituation = (el: HTMLElement) => {
    if(this.ctrl.opts.notAccept) {
      window.lichess.memberIntro();
      return false;
    }

    let fen = '';
    $.ajax({ url: `/resource/situationdb/select`}).then(function (html) {
      let $modal =
        $(`<div id="modal-overlay" class="situation-modal">
            <div id="modal-wrap">
              <span class="close" data-icon="L"></span>
              ${html}
            </div>
          </div>`);
      $modal.find('.modal-content').removeClass('none');
      $('main').append($modal);

      let closeModal = () => {
        $modal.remove();
      };

      $modal.find('.close').on('click', closeModal);
      $modal.find('.situation-cancel').click(function () {
        closeModal();
      });

      window.lichess.pubsub.emit('content_loaded');
      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-selector').find('form');
          let $tree = (<any>$jq($form)).find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            },
            'plugins' : [ 'search' ]
          })
            .on('changed.jstree', function (_, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                search();
              }
            });

          let to;
          $form.find('.search input[name="q"]').keyup(function () {
            let q = $form.find('.search input[name="q"]').val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });

          $form.find('#btn-search').click(function () {
            search();
          });

          let $emptyTag = $form.find('.search_form')
            .find('.tag-group.emptyTag')
            .find('input[type="checkbox"]');

          let $tags = $form.find('.search_form')
            .find('.tag-group')
            .find('input[type="checkbox"]');

          $tags.not($emptyTag).click(function () {
            $emptyTag.prop('checked', false);
            search();
          });

          $emptyTag.click(function () {
            $tags.not($emptyTag).prop('checked', false);
            search();
          });

          let $situations = $form.find('.situations');
          function search() {
            let situation = $tree.jstree(true).get_selected(null)[0];
            $.ajax({
              url: `/resource/situationdb/rel/list?selected=${situation}`,
              data: $form.serialize()
            }).then(function (rels) {
              $situations.empty();

              if(!rels || rels.length === 0) {
                let noMore =
                  `<div class="no-more">
                    <i data-icon="4">
                    <p>没有更多了</p>
                  </div>`;
                $situations.html(noMore);
              } else {
                let situations =
                  rels.map(function (rel) {
                    let tags = '';
                    if(rel.tags && rel.tags.length > 0) {
                      rel.tags.map(function (tag) {
                        tags = `<span>${tag}</span>`;
                      });
                    }
                    return `<a class="paginated" target="_blank" data-id="${rel.id}">
                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                <cg-helper>
                                  <cg-container>
                                    <cg-board></cg-board>
                                  </cg-container>
                                </cg-helper>
                              </div>
                              <div class="btm">
                                <label>${rel.name ? rel.name : ''}</label>
                                <div class="tags">${tags}</div>
                              </div>
                            </a>`;
                  });

                $situations.html(situations);
                registerEvent();
                window.lichess.pubsub.emit('content_loaded')
              }
            })
          }

          function registerEvent() {
            let $boards = $situations.find('.paginated');
            $boards.off('click');
            $boards.click(function(e) {
              e.preventDefault();
              let $this = $(e.currentTarget);
              $boards.removeClass('selected');
              $this.addClass('selected');
              fen = $this.find('.mini-board').data('fen');
              return false;
            });
          }
          registerEvent();

          $form.submit(function (e) {
            e.preventDefault();
            let $input = $(el).parents('tr').find('.fen input');
            $input.val(fen).trigger('change');
            closeModal();
            return false;
          });
        })
      });
    });
  };

  setFromPositionFen = (index, board) => {
    this.fromPositionGames.forEach((fpwr, i) => {
      if(index === i && fpwr.fromPosition.el) {
        $(fpwr.fromPosition.el).html(board);
      }
    });
    window.lichess.pubsub.emit('content_loaded');
  };

  changeValue = (index, field, subField, v) => {
    this.fromPositionGames.forEach((fpg, i) => {
      index === i ? !!subField ? fpg.fromPosition[field][subField] = v : fpg.fromPosition[field] = v : fpg;
    });
    if(field === 'num') this.setFromPositionGameNum();
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.fromPositionGames.length >= this.maxNum;
  };

  addFromPositionGame = () => {
    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    this.fromPositionGames.push({
      fromPosition: {
        el: null,
        fen: this.initialFen,
        clock: {
          initial: 60 * 5,
          increment: 0
        },
        num: 1,
        isAi: true,
        aiLevel: 1,
        color: 'random',
        chessStatus: 'all',
        canTakeback: 0
      },
      result: {}
    });
    this.setFromPositionGameNum();
    this.ctrl.redraw();
  };

  removeFromPositionGame = (index) => {
    this.fromPositionGames = this.fromPositionGames.filter((_, i) => index !== i);
    this.setFromPositionGameNum();
    this.ctrl.redraw();
  };

  setFromPositionGameNum = () => {
    this.ctrl.num = this.fromPositionGames.reduce((total, fpg) => {
      return total + fpg.fromPosition.num;
    }, 0);
    this.ctrl.setTaskName();
  };

}
