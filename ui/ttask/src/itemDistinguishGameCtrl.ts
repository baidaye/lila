import * as xhr from './xhr'
import TaskCtrl from './ctrl';
import {DistinguishGameWithResult} from './interfaces';

export default class ItemDistinguishGameTaskCtrl {

  itemType: string = 'distinguishGame';
  showDistinguishGameModal: boolean = false;
  maxNum: number = 5;

  activeTab: string = 'classic';
  distinguishGames: DistinguishGameWithResult[] = [];

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.distinguishGameWithResult) {
        this.distinguishGames = this.ctrl.opts.task.item.distinguishGameWithResult;
        this.ctrl.num = this.distinguishGames.length;
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.distinguishGames = [];
    this.ctrl.studyCtrl.reset();
    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.reset();
    }
  };

  onDistinguishGameModalOpen = () => {
    this.showDistinguishGameModal = true;
    this.ctrl.studyCtrl.reset();
    this.ctrl.studyCtrl.onInitStudy();

    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.reset();
      this.ctrl.courseWareCtrl.onInitCourseWare();
    }
  };

  onDistinguishGameModalClose = () => {
    this.showDistinguishGameModal = false;
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.distinguishGames.length >= this.maxNum;
  };

  submitDistinguishGame = () => {
    let $form = $('.distinguishGameForm');
    if(
      !$form.find('input[name="classic"]:checked').val()
      && !$form.find('input[name="game"]').val()
      && !$form.find('input[name="gamedb"]').val()
      && !$form.find('textarea[name="pgn"]').val()
      && !$form.find('input[name="chapter"]').val()
      && !$form.find('input[name="courseWare"]').val()
    ) {
      alert('输入一种PGN获取方式');
      return;
    }

    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    xhr.loadDistinguish($form.serialize()).then((data) => {
      if(data) {
        data.title = this.ctrl.opts.sourceRel.name;
        data.rightTurns = $form.find('#form-rightTurns').val();
        data.turns = $form.find('#form-turns').val();
        data.color = $form.find('#form-color').val();
        data.orientation = $form.find('#form-orientation').val();

        let d = {distinguishGame: data, result: {}};
        this.distinguishGames.push(d);
        this.onDistinguishGameModalClose();
        this.setDistinguishGameNum();
      }
    });
  };

  removeDistinguishGame = (index) => {
    this.distinguishGames = this.distinguishGames.filter((_, i) => index !== i);
    this.setDistinguishGameNum();
    this.ctrl.redraw();
  };

  setDistinguishGameNum = () => {
    this.ctrl.num = this.distinguishGames.length;
    this.ctrl.setTaskName();
  };

}
