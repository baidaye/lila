import TaskCtrl from './ctrl';
import {FromPgnWithResult} from './interfaces';

export default class ItemFromPgnGameTaskCtrl {

  itemType: string = 'fromPgn';
  maxNum: number = 5;

  emptyFen = '8/8/8/8/8/8/8/8 w - -';
  initialFen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  fromPgnGames: FromPgnWithResult[] = [];

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.fromPgnWithResult) {
        this.fromPgnGames = this.ctrl.opts.task.item.fromPgnWithResult;
        this.ctrl.num = this.fromPgnGames.reduce((total, fpg) => {
            return total + fpg.fromPgn.num;
        }, 0);
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.fromPgnGames = [];
  };

  validatePgn = (pgnElm: HTMLInputElement, index: number) => {
    const pgn = pgnElm.value;
    if (pgn) {
      $.ajax({
        url: '/setup/validate-pgn?strict=0',
        data: {
          pgn: pgn
        },
        success: (data) => {
          pgnElm.setCustomValidity('');
          pgnElm.className = '';
          this.setFromPgnPgn(index, data);
        },
        error: (err) => {
          if(err.status === 400) {
            if(err.responseJSON && err.responseJSON.error) {
              pgnElm.setCustomValidity(JSON.stringify(err.responseJSON.error));
            } else {
              pgnElm.setCustomValidity('PGN 格式无效');
            }
          }
          pgnElm.className = 'is-invalid';
          //this.setFromPgnPgn(index, null);
        }
      });
    }
  };

  setFromPgnPgn = (index, data) => {
    this.fromPgnGames.forEach((fpwr, i) => {
      if(index === i && fpwr.fromPgn.el) {
        fpwr.fromPgn.initialPgn = data;
      }
    });
    this.ctrl.redraw();
  };

  changeValue = (index, field, subField, v) => {
    this.fromPgnGames.forEach((fpg, i) => {
      index === i ? !!subField ? fpg.fromPgn[field][subField] = v : fpg.fromPgn[field] = v : fpg;
    });
    if(field === 'num') this.setFromPgnGameNum();
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.fromPgnGames.length >= this.maxNum;
  };

  addFromPgnGame = () => {
    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    this.fromPgnGames.push({
      fromPgn: {
        el: null,
        initialPgn: {
          lastPly: 0,
          initialFen: this.initialFen,
          lastFen: this.initialFen,
          source: 'pgn',
          opening: {
            id: '',
            name: '',
            desc: '',
            nodes: 0,
            orientation: 'white',
            members: {},
            initialFen: this.initialFen,
            enable: true,
            system: false,
            clean: 0,
            ownerId: '',
            mainline: '',
            mainlineMoves: []
          }
        },
        clock: {
          initial: 60 * 5,
          increment: 0
        },
        num: 1,
        isAi: true,
        aiLevel: 1,
        color: 'random',
        chessStatus: 'all',
        canTakeback: 0
      },
      result: {}
    });
    this.setFromPgnGameNum();
    this.ctrl.redraw();
  };

  removeFromPgnGame = (index) => {
    this.fromPgnGames = this.fromPgnGames.filter((_, i) => index !== i);
    this.setFromPgnGameNum();
    this.ctrl.redraw();
  };

  setFromPgnGameNum = () => {
    this.ctrl.num = this.fromPgnGames.reduce((total, fpg) => {
      return total + fpg.fromPgn.num;
    }, 0);
    this.ctrl.setTaskName();
  };

}
