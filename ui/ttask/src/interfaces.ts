import {VNode} from 'snabbdom/vnode'
import * as cg from 'chessground/types';
import { InitialPgn } from 'game';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface TTaskOpts {
  notAccept: boolean;
  sourceRel: SourceRel;
  themePuzzle: any;
  classicGames: ClassicGame[];
  task?: Task | TaskTemplate | null;
  team?: Team;
}

export interface SourceRel {
  id: string;
  name: string;
  source: string;
  deadlineAt: string;
}

export interface ClassicGame {
  id: string;
  name: string;
}

export interface TaskTemplate {
  id: string;
  name: string;
  remark?: string;
  item: TaskItem;
  itemType: TaskItemType;
  sourceRel: TaskSourceRel;
  total: number;
  createdAt: string;
  deadlineAt: string;
  coinRule: number;
  expanded?: boolean;
}

export interface Task {
  id: string;
  name: string;
  remark?: string;
  item: TaskItem;
  itemType: TaskItemType;
  sourceRel: TaskSourceRel;
  status: TaskStatus;
  coinRule: number;
  num: number;
  total: number;
  progress: number;
  completed: boolean,
  link: string;
  createdAt: string;
  deadlineAt: string;
}

export interface TaskItem {
  puzzleWithResult?: PuzzleWithResult;
  themePuzzleWithResult?: ThemePuzzleWithResult;
  coordTrainWithResult?: CoordTrainWithResult;
  puzzleRushWithResult?: PuzzleRushWithResult;
  gameWithResult?: GameWithResult;
  capsulePuzzleWithResult?: CapsulePuzzleWithResult;
  replayGameWithResult?: ReplayGameWithResult[];
  recallGameWithResult?: RecallGameWithResult[];
  distinguishGameWithResult?: DistinguishGameWithResult[];
  fromPositionWithResult?: FromPositionWithResult[];
  fromPgnWithResult?: FromPgnWithResult[];
  fromOpeningdbWithResult?: FromOpeningdbWithResult[];
}

export interface TaskItemType {
  id: string;
  name: string;
  member?: boolean;
}

export interface TaskSourceRel {
  id: string;
  name: string;
  source: TaskSource;
}

export interface TaskSource {
  id: string;
  name: string;
  icon: string;
}

export interface TaskStatus {
  id: string;
  name: string;
}

export interface Clock {
  time: string;
  increment: number;
}

export interface CommonItemExtra {
  cond: string;
}

export interface CommonItem {
  isNumber: boolean;
  num: number;
  extra?: CommonItemExtra;
  tags?: string[];
}

export interface CommonWithResult {
  [key: string]: CommonItem;
  result: any;
}

export interface PuzzleWithResult extends CommonWithResult {}

export interface ThemePuzzleWithResult extends CommonWithResult {}

export interface CoordTrainWithResult extends CommonWithResult {}

export interface PuzzleRush extends CommonItem {
  mode: string;
}

export interface PuzzleRushWithResult {
  puzzleRush: PuzzleRush;
  result: any;
}

export interface Capsule {
  id: string;
  name: string;
  puzzles: MiniPuzzleWithResult[];
}

export interface MiniPuzzle {
  id: string;
  fen: string;
  color: cg.Color;
  lastMove?: string;
  lines: string;
}

export interface MiniPuzzleWithResult {
  puzzle: MiniPuzzle;
  result: any
}

export interface CapsulePuzzleWithResult {
  num: number,
  capsules: Capsule[],
}

export interface ReplayGame {
  name: string;
  color: string;
  root: string;
  pgn: string;
  chapterId: string;
  chapterLink: string;
  moves: Move[];
  el: HTMLElement;
}

export interface ReplayGameWithResult {
  replayGame: ReplayGame;
  result: any;
}

export interface RecallGame {
  turns: number;
  title: string;
  color: string;
  orient: string;
  name: string;
  fen: string;
  pgn: string;
  el: HTMLElement;
  moves: Move[];
}

export interface RecallGameWithResult {
  recallGame: RecallGame;
  result: any;
}

export interface DistinguishGame {
  rightTurns: number;
  turns: number;
  title: string;
  orientation: string;
  name: string;
  fen: string;
  pgn: string;
  el: HTMLElement;
  moves: Move[];
}

export interface DistinguishGameWithResult {
  distinguishGame: DistinguishGame;
  result: any;
}

export interface Game extends CommonItem {
  speed: string;
}

export interface GameWithResult {
  game: Game;
  result: any;
}

export interface FromPosition {
  el: HTMLElement | null;
  fen: string;
  clock: FromPositionClock,
  color: string;
  num: number;
  isAi: boolean;
  aiLevel: number;
  chessStatus: string; //all|win|winOrDraw
  canTakeback: number;
}

export interface FromPositionWithResult {
  fromPosition: FromPosition;
  result: any;
}

export interface FromPositionClock {
  initial: number;
  increment: number;
}


export interface FromPgn {
  el: HTMLElement | null;
  initialPgn: InitialPgn,
  clock: FromPgnClock,
  color: string;
  num: number;
  isAi: boolean;
  aiLevel: number;
  chessStatus: string; //all|win|winOrDraw
  canTakeback: number;
}


export interface FromPgnWithResult {
  fromPgn: FromPgn;
  result: any;
}

export interface FromPgnClock {
  initial: number;
  increment: number;
}

export interface FromOpeningdb {
  el: HTMLElement | null;
  initialPgn: InitialPgn,
  clock: FromOpeningdbClock,
  color: string;
  num: number;
  isAi: boolean;
  aiLevel: number;
  chessStatus: string; //all|win|winOrDraw
  canTakeback: number;
}


export interface FromOpeningdbWithResult {
  fromOpeningdb: FromOpeningdb;
  result: any;
}

export interface FromOpeningdbClock {
  initial: number;
  increment: number;
}

export interface Move {
  index: number;
  white?: Node;
  black?: Node;
}

export interface Node {
  san: string;
  uci: string;
  fen: string;
  active: boolean;
}

export interface Student extends LightUser {
  online: boolean;
  mark?: string;
}

export interface Team {
  id: string;
  name: string;
  coinSetting: TeamCoinSetting;
}

export interface TeamCoinSetting {
  open: boolean;
  name: string;
  singleVal: number;
}

export type Redraw = () => void;


