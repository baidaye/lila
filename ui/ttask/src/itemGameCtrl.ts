import TaskCtrl from './ctrl';
import {Game} from "./interfaces";

export default class ItemGameTaskCtrl {

  itemType: string = 'game';

  game: Game = {
    isNumber: true,
    num: 1,
    speed: 'rapid',
    extra: {
      cond: ''
    },
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.gameWithResult) {
        this.game = this.ctrl.opts.task.item.gameWithResult.game;
        this.ctrl.isNumber = this.game.isNumber;
        this.ctrl.num = this.game.num;
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.game = {
      isNumber: true,
      num: 1,
      speed: 'rapid',
      extra: {
        cond: ''
      },
      tags: []
    };
  };

  changeGameSpeed(speed) {
    this.game.speed = speed.k;
    this.ctrl.setTaskName();
    this.ctrl.redraw();
  }
}
