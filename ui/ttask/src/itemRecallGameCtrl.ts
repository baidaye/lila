import * as xhr from './xhr'
import TaskCtrl from './ctrl';
import {RecallGameWithResult} from './interfaces';

export default class ItemRecallGameTaskCtrl {

  itemType: string = 'recallGame';
  showRecallGameModal: boolean = false;
  maxNum: number = 5;
  activeTab: string = 'study';

  recallGames: RecallGameWithResult[] = [];

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.recallGameWithResult) {
        this.recallGames = this.ctrl.opts.task.item.recallGameWithResult;
        this.ctrl.num = this.recallGames.length;
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.recallGames = [];
    this.ctrl.studyCtrl.reset();
    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.reset();
    }
  };

  onRecallGameModalOpen = () => {
    this.showRecallGameModal = true;
    this.ctrl.studyCtrl.reset();
    this.ctrl.studyCtrl.onInitStudy();

    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.reset();
      this.ctrl.courseWareCtrl.onInitCourseWare();
    }
  };

  onRecallGameModalClose = () => {
    this.showRecallGameModal = false;
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.recallGames.length >= this.maxNum;
  };

  submitRecallGame = () => {
    let $form = $('.recallGameForm');
    if(!$form.find('input[name="game"]').val()
      && !$form.find('input[name="gamedb"]').val()
      && !$form.find('textarea[name="pgn"]').val()
      && !$form.find('input[name="chapter"]').val()
      && !$form.find('input[name="courseWare"]').val()
    ) {
      alert('输入一种PGN获取方式');
      return;
    }

    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    xhr.loadRecall($form.serialize()).then((data) => {
      if(data) {
        data.title = this.ctrl.opts.sourceRel.name;
        data.turns = $form.find('#form-turns').val();
        data.color = $form.find('#form-color').val();
        data.orient = $form.find('#form-orient').val();

        let d = {recallGame: data, result: {}};
        this.recallGames.push(d);
        this.onRecallGameModalClose();
        this.setRecallGameNum();
      }
    });
  };

  removeRecallGame = (index) => {
    this.recallGames = this.recallGames.filter((_, i) => index !== i);
    this.setRecallGameNum();
    this.ctrl.redraw();
  };

  setRecallGameNum = () => {
    this.ctrl.num = this.recallGames.length;
    this.ctrl.setTaskName();
  };

}
