import * as xhr from './xhr'
import TaskCtrl from './ctrl';

export default class StudyCtrl {

  studyTab: string = 'study';
  studyLoading: boolean = true;
  studyPaginator: any;
  chapterLoading: boolean = true;
  chapters: any;
  currentStudy: any;
  currentChapter: any;
  currentPgn: string;
  studyTags: string[] = [];
  studyChannel: string = 'mine';
  studyTagData = {
    emptyTag: 'off',
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {

  }

  reset = () => {
    this.studyTab = 'study';
    this.studyPaginator = null;
    this.studyLoading = true;
    this.chapters = null;
    this.chapterLoading = true;
    this.currentStudy = null;
    this.currentChapter = null;
    this.currentPgn = '';
    this.studyTags = [];
    this.studyChannel = 'mine';
    this.studyTagData = {
      emptyTag: 'off',
      tags: []
    };
  };

  onInitStudy = () => {
    this.loadStudy();
    this.loadStudyTags();
  };

  onStudyTabChange = (channel) => {
    this.studyChannel = channel;
    this.studyTagData = {
      emptyTag: 'off',
      tags: []
    };
    this.loadStudy();
    this.loadStudyTags();
  };

  loadStudyTags = () => {
    xhr.loadStudyTags(this.studyChannel).then((data) => {
      this.studyTags = data;
      this.ctrl.redraw();
    });
  };

  onStudyEmptyTagCheck = (checked) => {
    $('.study-search-input').val('');
    this.studyTagData.emptyTag = checked ? 'on' : 'off';
    this.studyTagData.tags = [];
    this.ctrl.redraw();
    this.loadStudy();
  };

  onStudyTagCheck = (tag, checked) => {
    this.studyTagData.emptyTag = 'off';
    $('.study-search-input').val('');
    this.ctrl.redraw();
    if(checked) {
      // @ts-ignore
      this.studyTagData.tags.push(tag);
    } else {
      this.studyTagData.tags = this.studyTagData.tags.filter(t => t !== tag);
    }
    this.loadStudy();
  };

  loadStudy = (p: number = 1) => {
    this.studyLoading = true;
    this.ctrl.redraw();
    let q = $('.study-search-input').val();
    xhr.loadStudy(this.studyChannel, p, q, this.studyTagData).then((data) => {
      this.studyPaginator = data.paginator;
      this.studyLoading = false;
      this.ctrl.redraw();
    });
  };

  loadChapter = () => {
    this.chapterLoading = true;
    this.ctrl.redraw();
    xhr.loadChapter(this.currentStudy.id).then((data) => {
      this.chapters = data.chapters;
      this.chapterLoading = false;
      this.ctrl.redraw();
    });
  };

}
