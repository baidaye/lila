import TaskCtrl from './ctrl';
import {CommonItem} from './interfaces';

export default class ItemCoordTrainTaskCtrl {

  itemType: string = 'coordTrain';
  showCoordTrainModal: boolean = false;

  coordTrain: CommonItem = {
    isNumber: true,
    num: 1,
    extra: {
      cond: ''
    },
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.coordTrainWithResult) {
        this.coordTrain = this.ctrl.opts.task.item.coordTrainWithResult.coordTrain;
        this.ctrl.isNumber = this.coordTrain.isNumber;
        this.ctrl.num = this.coordTrain.num;
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.coordTrain = {
      isNumber: true,
      num: 1,
      extra: {
        cond: ''
      },
      tags: []
    };
  };

  onCoordTrainModalOpen = () => {
    this.showCoordTrainModal = true;
    this.ctrl.redraw();
  };

  onCoordTrainModalClose = () => {
    this.showCoordTrainModal = false;
    this.ctrl.redraw();
  };

  submitCoordTrain = () => {
    let $form = $('.coordTrainForm');

    let mode = $form.find('#form-mode').val();
    let orient = $form.find('#form-orient').val();
    let coordShow = $form.find('#form-coordShow').val();
    let limitTime = $form.find('#form-limitTime').val();

    this.coordTrain.tags = [];
    if(mode) {
      let text = $form.find('#form-mode').find('option:selected').text();
      this.coordTrain.tags.push(text);
    }
    if(orient) {
      let text = $form.find('#form-orient').find('option:selected').text();
      this.coordTrain.tags.push(text);
    }
    if(coordShow === '1') {
      this.coordTrain.tags.push('显示棋盘坐标');
    } else {
      this.coordTrain.tags.push('不显示棋盘坐标');
    }
    if(limitTime === '1') {
      this.coordTrain.tags.push('限制时间');
    } else {
      this.coordTrain.tags.push('不限时间');
    }

    this.coordTrain.extra!.cond = $form.serialize();
    this.onCoordTrainModalClose();
  };

}
