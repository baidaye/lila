import {h} from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import {bind, spinner} from '../util';
import TaskCtrl from '../ctrl';

const studyChannels = [{ key: 'mine', name: '我的研习' }, { key: 'member', name: '参与研习' }, { key: 'favorites', name: '收藏研习' }, { key: 'all', name: '所有研习' }];
export default function(ctrl: TaskCtrl): VNode {
  const studyCtrl = ctrl.studyCtrl;
  return h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'chapter' } }),
    studyCtrl.currentChapter ? h('input', {
      attrs: {
        type: 'hidden',
        name: 'chapter',
        value: `${location.protocol + '//' + location.host + '/study/' + studyCtrl.currentStudy.id + '/' + studyCtrl.currentChapter.id}`
      }
    }) : null,
    h('div.study', {
      class: {
        none: studyCtrl.studyTab !== 'study'
      }
    }, [
      h('div.study-content', [
        h('div.study-tab', [
          h('div.study-tab-header', studyChannels.map(function (channel) {
            return h('a.study-tab-item', {attrs: { 'data-id': channel.key }, class: { active: studyCtrl.studyChannel === channel.key }, hook: {
                insert(vnode) {
                  $(vnode.elm as HTMLElement).on('click', () => {
                    studyCtrl.onStudyTabChange(channel.key);
                  });
                },
                postpatch: (_, vnode) => {
                  $(vnode.elm as HTMLElement).off('click').on('click', () => {
                    studyCtrl.onStudyTabChange(channel.key);
                  });
                }
              }}, channel.name)
          })),
          h('div.study-tab-content', [
            h('div.study-search', [
              h('div.study-search-term', [
                h('input.study-search-input', {attrs: {placeholder: '研习名称/章节名称/关键词'}}),
                h('a.button.button-empty small', {
                  hook: bind('click', () => {
                    studyCtrl.studyTagData = {
                      emptyTag: 'off',
                      tags: []
                    };
                    studyCtrl.loadStudy();
                  })
                }, '搜索')
              ]),
              (studyCtrl.studyChannel === 'mine' || studyCtrl.studyChannel === 'favorites') ? h('div.study-search-tags.tag-groups', [
                h('div.tag-group.emptyTag', [
                  h('span', [
                    h(`input.${studyCtrl.studyTagData.emptyTag}`, {attrs: {type: 'checkbox', id: `emptyTag`, name: `emptyTag`, checked: studyCtrl.studyTagData.emptyTag === 'on'}, hook: {
                        insert(vnode) {
                          $(vnode.elm as HTMLElement).on('click', (e) => {
                            studyCtrl.onStudyEmptyTagCheck((e.target as HTMLInputElement).checked);
                          });
                        },
                        postpatch: (_, vnode) => {
                          $(vnode.elm as HTMLElement).off('click').on('click', (e) => {
                            studyCtrl.onStudyEmptyTagCheck((e.target as HTMLInputElement).checked);
                          });
                        }
                      }}),
                    h('label', {attrs: {for: `emptyTag`}}, `无标签`)
                  ])
                ]),
                h('div.tag-group.otherTag', studyCtrl.studyTags.map(function (tag, i) {
                  let tags = studyCtrl.studyTagData.tags;
                  // @ts-ignore
                  let checked = tags.includes(tag);
                  return h('span', [
                    h(`input.${tags}`, {attrs: {type: 'checkbox', id: `tags-${tag}`, name: `tags[${i}]`, checked: checked}, hook: {
                        insert(vnode) {
                          $(vnode.elm as HTMLElement).on('change', (e) => {
                            studyCtrl.onStudyTagCheck(tag, (e.target as HTMLInputElement).checked);
                          });
                        },
                        postpatch: (_, vnode) => {
                          $(vnode.elm as HTMLElement).off('change').on('click', (e) => {
                            studyCtrl.onStudyTagCheck(tag, (e.target as HTMLInputElement).checked);
                          });
                        }
                      }}),
                    h('label', {attrs: {for: `tags-${tag}`}}, tag)
                  ])
                }))
              ]) : null
            ]),
            h('div.scroll', [
              studyCtrl.studyLoading ? spinner() :
                h('table.slist.study-list', [
                  h('tbody', studyCtrl.studyPaginator.currentPageResults.length > 0 ?
                    studyCtrl.studyPaginator.currentPageResults.map(data => {
                      return h('tr', [
                        h('td', { attrs: {'data-id': data.id } }, [
                          h('a', {
                            hook: {
                              insert(vnode) {
                                $(vnode.elm as HTMLElement).on('click', () => {
                                  studyCtrl.currentStudy = data;
                                  studyCtrl.studyTab = 'chapter';
                                  studyCtrl.loadChapter();
                                });
                              },
                              postpatch: (_, vnode) => {
                                $(vnode.elm as HTMLElement).off('click').on('click', () => {
                                  studyCtrl.currentStudy = data;
                                  studyCtrl.studyTab = 'chapter';
                                  studyCtrl.loadChapter();
                                });
                              }
                            },
                          }, data.name)
                        ]),
                        studyCtrl.studyChannel !== 'mine' ? h('td', [
                          h('span.user-link.ulpt', { attrs: {'data-href': `/@/${data.owner.name}` } }, [
                            data.owner.title ? h('span.title', data.owner.title) : null,
                            h('span.u_name', data.owner.name)
                          ])
                        ]) : null,
                        h('td', data.visibility.name)
                      ]);
                    }) : [h('tr', [h('td', '没有更多了.')])])
                ])
            ])
          ])
        ])
      ]),
      h('div.pager', [
        h('a.fbt prev', {
          attrs: {
            'data-icon': 'Y'
          },
          class: {
            disabled: studyCtrl.studyLoading || studyCtrl.studyPaginator.currentPage <= 1
          },
          hook: bind('click', () => {
            if(!studyCtrl.studyLoading && studyCtrl.studyPaginator.currentPage > 1) {
              studyCtrl.loadStudy(studyCtrl.studyPaginator.previousPage);
            }
          })
        }, '上一页'),
        h('a.fbt next', {
          attrs: {
            'data-icon': 'X'
          },
          class: {
            disabled: studyCtrl.studyLoading || studyCtrl.studyPaginator.currentPage >= studyCtrl.studyPaginator.nbPages
          },
          hook: bind('click', () => {
            if(!studyCtrl.studyLoading && studyCtrl.studyPaginator.currentPage < studyCtrl.studyPaginator.nbPages) {
              studyCtrl.loadStudy(studyCtrl.studyPaginator.nextPage);
            }
          })
        }, '下一页')
      ])
    ]),
    h('div.chapter', {
      class: {
        none: studyCtrl.studyTab !== 'chapter'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backStudy', {
          hook: bind('click', () => {
            studyCtrl.studyTab = 'study';
            ctrl.redraw();
          })
        }, studyCtrl.currentStudy ? studyCtrl.currentStudy.name : '- 无 -')
      ]),
      h('div.scroll', [
        studyCtrl.chapterLoading ? spinner () : h('ul.chapter-list', studyCtrl.chapters.length > 0 ?
          studyCtrl.chapters.map((item, _) => {
            return h('li', { attrs: {'data-id': item.id } }, [
              h('span', [
                h('input', {
                  attrs: {type: 'radio', id: `rd-chapter-${item.id}`, name: `rd-chapter`, value: item.id },
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).on('click', () => {
                        studyCtrl.currentChapter = item;
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).off('click').on('click', () => {
                        studyCtrl.currentChapter = item;
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    }
                  },
                }),
                h('label', {attrs: {for: `rd-chapter-${item.id}`}}, ` ${item.name}`)
              ]),
              h('a.viewPGN', {
                hook: bind('click', () => {
                  studyCtrl.studyTab = 'pgn';
                  studyCtrl.currentPgn = item.pgn;
                  ctrl.redraw();
                })
              }, 'PGN')
            ])
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.chapter-pgn', {
      class: {
        none: studyCtrl.studyTab !== 'pgn'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backStudy', {
          hook: bind('click', () => {
            studyCtrl.studyTab = 'study';
            ctrl.redraw();
          })
        }, studyCtrl.currentStudy ? studyCtrl.currentStudy.name : '- 无 -'),
        ' / ',
        h('a.backChapter', {
          hook: bind('click', () => {
            studyCtrl.studyTab = 'chapter';
            ctrl.redraw();
          })
        }, studyCtrl.currentChapter ? studyCtrl.currentChapter.name : '- 无 -')
      ]),
      h('div.scroll', [
        h('div.pgn', studyCtrl.currentPgn ? studyCtrl.currentPgn : '- 无 -')
      ])
    ])
  ]);
}
