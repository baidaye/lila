import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import * as ModalBuild from './modal';
import {renderTaskName} from './form'
import {bind, bindSubmit} from '../util';
import TaskCtrl from '../ctrl';
import {Capsule} from "../interfaces";

export function capsulePuzzleItem(ctrl: TaskCtrl) {
  const capsulePuzzleCtrl = ctrl.capsulePuzzleCtrl;

  return h('div.item-capsulePuzzle', [
    renderTaskName(ctrl),
    h(`input.${ctrl.itemType}-${ctrl.num}`, {attrs: {type: 'hidden', value: ctrl.num}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-capsules'}}, '已选战术题'),
      h('a.button.button-empty.small', {
        hook: bind('click', () => {
          capsulePuzzleCtrl.onCapsuleModalOpen();
        })
      },'选择战术题'),
      renderCapsules(ctrl, capsulePuzzleCtrl.capsulePuzzle.capsules)
    ])
  ]);
}

export function renderCapsules(ctrl: TaskCtrl, capsules: Capsule[], viewOnly: boolean = false) {
  return h('div.capsules', capsules.map((c, ci) => {
    return h(`div.capsule.${c.id}`, [
      h('div.capsule-head', [
        h('div.capsule-head', h('a', { attrs: { target: '_blank', href: `/resource/capsule/${c.id}/puzzle/list` } }, c.name)),
        !viewOnly ? h('a.button.button-empty.button-red.small', {
          hook: bind('click', () => ctrl.capsulePuzzleCtrl.removeCapsule(c.id))
        }, '移除') : null,
        !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].id`, value: c.id}}) : null,
        !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].name`, value: c.name}}) : null,
      ]),
      h('div.capsule-puzzles', {
        class: {
          scroll: !viewOnly
        }
      }, c.puzzles.map((pwr, pi) => {
        let puzzle = pwr.puzzle;
        let lines = (puzzle.lines && puzzle.lines.startsWith("%7B")) ? puzzle.lines : encodeURI(puzzle.lines);
        return h('div.puzzle', [
          h('div.mini-board.cg-wrap.parse-fen.is2d', {
            attrs: {
              'data-fen': puzzle.fen,
              'data-color': puzzle.color,
              'data-lastmove': puzzle.lastMove ? puzzle.lastMove : ''
            }
          }),
          !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].puzzles[${pi}].id`, value: puzzle.id}}) : null,
          !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].puzzles[${pi}].fen`, value: puzzle.fen}}) : null,
          !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].puzzles[${pi}].color`, value: puzzle.color}}) : null,
          !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].puzzles[${pi}].lastMove`, value: puzzle.lastMove ? puzzle.lastMove : ''}}) : null,
          !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.capsulePuzzle[${ci}].puzzles[${pi}].lines`, value: lines}}) : null
        ])
      }))
    ])
  }))
}

export function capsuleModal(ctrl: TaskCtrl): VNode {
  const capsulePuzzleCtrl = ctrl.capsulePuzzleCtrl;

  return ModalBuild.modal({
    onClose: () => {
      capsulePuzzleCtrl.onCapsuleModalClose();
    },
    class: `capsulePuzzleModal`,
    content: [
      h('h2', '选择战术题列表'),
      h('div.modal-content-body', [
        h('form.capsulePuzzleForm', {
          hook: bindSubmit(_ => {
            capsulePuzzleCtrl.submitCapsule();
          })
        }, [
          h('div.tabs-horiz', capsulePuzzleCtrl.capsuleTabs.map(function (capsuleTab) {
              return makeTab(ctrl, capsuleTab.id, capsuleTab.name, capsuleTab.name);
          })),
          h('div.tabs-content',capsulePuzzleCtrl.capsuleTabs.map(function (capsuleTab) {
            let cap = capsulePuzzleCtrl.capsules[capsuleTab.id];
            return h(`div.content.capsuleTab-${capsuleTab.id}`, {class:{active: capsulePuzzleCtrl.activeCapsuleTab === capsuleTab.id}}, [
              h('div.capsule-filter', [
                h('div.capsule-filter-tag.tag-group', cap.tags.map(t => {
                  return h('span', [
                    h('input', {
                      attrs: { id: `ckb_${capsuleTab.id}_${t}`, type: 'checkbox', value: t},
                      hook: bind('change', () => {
                        capsulePuzzleCtrl.filterCapsules();
                      })
                    }),
                    h('label', { attrs: { for: `ckb_${capsuleTab.id}_${t}` } }, t)
                  ])
                })),
                h('input.capsule-filter-search', {
                  attrs: { placeholder: '搜索'},
                  hook: {
                    insert(vnode) {
                      const $el = $(vnode.elm as HTMLElement);
                      $el.val('').on('change keyup paste', () => {
                        capsulePuzzleCtrl.filterCapsules();
                      })
                    }
                  },
                }),
              ]),
              h('div.capsule-scroll', [
                cap.list.length === 0 ? noMore(capsuleTab.id) : h('table.capsule-list', [
                  h('tbody', cap.list.map(capsule => {
                    let attr = {id: capsule.id,'name':capsule.name,'tags':capsule.tags};
                    let checked = capsulePuzzleCtrl.capsulePuzzle.capsules.filter(c => c.id === capsule.id).length > 0;
                    return h('tr', {
                      attrs: {
                        'data-id': capsule.id,
                        'data-attr': JSON.stringify(attr)
                      }
                    }, [
                      h('td', [
                        h('span', [
                          h('input', {
                            attrs: { id: `chk_${capsule.id}`, name: 'rdCapsule', type: 'checkbox', value: capsule.id, checked: checked}
                          }),
                          ' ',
                          h('label', { attrs: { for: `chk_${capsule.id}` } }, capsule.name)
                        ])
                      ]),
                      h('td', capsule.total)
                    ]);
                  }))
                ])
              ]),
            ])
          })),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                capsulePuzzleCtrl.onCapsuleModalClose();
              })
            },'取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function noMore(capsuleTab) {
  return capsuleTab === 'mine' ? h('div.no-more', [
    '您还没有创建战术题列表，现在就去', h('a', { attrs: { href: '/resource/capsule/create' } }, '创建'), '吧'
  ]) : h('div.no-more', '没有可用的战术题列表');
}

function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  const capsulePuzzleCtrl = ctrl.capsulePuzzleCtrl;
  return h('span.' + key, {
    class: { active: capsulePuzzleCtrl.activeCapsuleTab === key },
    attrs: { title },
    hook: bind('click', () => {
      capsulePuzzleCtrl.activeCapsuleTab = key;
      ctrl.redraw();
    })
  }, name);
}
