import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import * as ModalBuild from './modal';
import {renderTaskName} from './form'
import {bind, bindSubmit, getFormVariable} from '../util';
import TaskCtrl from '../ctrl';

export function coordTrainItem(ctrl: TaskCtrl) {
  const coordTrainCtrl = ctrl.coordTrainCtrl;
  const cond = coordTrainCtrl.coordTrain.extra ? coordTrainCtrl.coordTrain.extra.cond : '';
  const tags = coordTrainCtrl.coordTrain.tags ? coordTrainCtrl.coordTrain.tags : [];
  return h('div.item-coordTrain', [
    renderTaskName(ctrl),
    h('input', {attrs: {type: 'hidden', name: 'item.coordTrain.isNumber', value: 'true'}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-num'}}, '目标值'),
      h(`input#form-num.form-control.${ctrl.itemType}-${ctrl.num}`, {
        hook: {
          insert(vnode) {
            const $el = $(vnode.elm as HTMLElement);
            $el.val(ctrl.num).on('change keyup paste', () => {
              ctrl.num = $el.val();
              ctrl.setTaskName();
            })
          }
        },
        attrs: {type: 'number', name: 'item.coordTrain.num', value: 1, min: 1, max: 3000, required: true}
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-cond'}}, '条件设置'),
      h('div.cond', [
        h('div.form-control.selected', {
          class: {
            has: tags.length > 0
          },
          hook: bind('click', () => {
            coordTrainCtrl.onCoordTrainModalOpen();
          })
        }, [
          h('div.tags', tags.map(t => {
            return h('span', t);
          })),
          h('input', {
            attrs: {
              type: 'hidden',
              id: 'extraCond',
              name: 'item.coordTrain.extra.cond',
              value: cond
            }
          })
        ])
      ])
    ])
  ]);
}

export function coordTrainModal(ctrl: TaskCtrl): VNode {
  const coordTrainCtrl = ctrl.coordTrainCtrl;
  const cond = coordTrainCtrl.coordTrain.extra ? coordTrainCtrl.coordTrain.extra.cond : '';
  //const tags = coordTrainCtrl.coordTrain.tags ? coordTrainCtrl.coordTrain.tags : [];

  const mode = getFormVariable(cond, 'mode');
  const orient = getFormVariable(cond, 'orient');
  const coordShow = getFormVariable(cond, 'coordShow');
  const limitTime = getFormVariable(cond, 'limitTime');

  return ModalBuild.modal({
    onClose: () => {
      coordTrainCtrl.onCoordTrainModalClose();
    },
    class: `coordTrainModal`,
    content: [
      h('h2', '坐标训练条件设置'),
      h('div.modal-content-body', [
        h('form.coordTrainForm.search_form', {
          hook: bindSubmit(_ => {
            coordTrainCtrl.submitCoordTrain();
          })
        }, [
          h('table', [
            h('tbody', [
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-mode'}}, '训练模式')
                ]),
                h('td', [
                  h('select#form-mode.form-control', {attrs: {name: 'mode', required: true}},
                    [
                      h('option', {attrs: {value: 'basic', selected: mode == 'basic'}}, '坐标训练'),
                      h('option', {attrs: {value: 'move', selected: mode == 'move'}}, '着法训练'),
                      h('option', {attrs: {value: 'combat', selected: mode == 'combat'}}, '实战着发')
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-orient'}}, '棋盘方向'),
                ]),
                h('td', [
                  h('select#form-orient.form-control', {attrs: {name: 'orient', required: true}},
                    [
                      h('option', {attrs: {value: '1', selected: orient == '1'}}, '白方'),
                      h('option', {attrs: {value: '2', selected: orient == '2'}}, '随机'),
                      h('option', {attrs: {value: '3', selected: orient == '3'}}, '黑方')
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-coordShow'}}, '显示棋盘坐标'),
                ]),
                h('td', [
                  h('select#form-coordShow.form-control', {attrs: {name: 'coordShow', required: true}},
                    [
                      h('option', {attrs: {value: '0', selected: coordShow == '0'}}, '否'),
                      h('option', {attrs: {value: '1', selected: coordShow == '1'}}, '是'),
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-limitTime'}}, '限制时间'),
                ]),
                h('td', [
                  h('select#form-limitTime.form-control', {attrs: {name: 'limitTime', required: true}},
                    [
                      h('option', {attrs: {value: '0', selected: limitTime == '0'}}, '否'),
                      h('option', {attrs: {value: '1', selected: limitTime == '1'}}, '是'),
                    ])
                ])
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                coordTrainCtrl.onCoordTrainModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}
