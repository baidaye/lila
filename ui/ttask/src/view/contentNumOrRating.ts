import {h} from 'snabbdom';
import {bind} from '../util';
import TaskCtrl from '../ctrl';

export default function(ctrl: TaskCtrl) {
  return h('div.form-group', [
    h('label.form-label', {attrs: {for: 'form-num'}}, '目标值'),
    h(`div.form-numArea`, [
      h(`select#form-isNumber.form-control`, {
        attrs: {name: `item.${ctrl.itemType}.isNumber`},
        hook: bind('change', (e: Event) => {
          let v = (e.target as HTMLInputElement).value;
          ctrl.changeIsNumber(v === 'true');
        }),
      }, [
        h('option', {attrs: {value: 'true', selected: ctrl.isNumber}}, '数量'),
        h('option', {attrs: {value: 'false', selected: !ctrl.isNumber}}, '等级分')
      ]),
      h('div.form-target', ctrl.target),
      h(`input#form-num.form-control.${ctrl.itemType}-${ctrl.num}`, {
        hook: {
          insert(vnode) {
            const $el = $(vnode.elm as HTMLElement);
            $el.val(ctrl.num).on('change keyup paste', () => {
              ctrl.num = $el.val();
              ctrl.setTaskName();
            })
          }
        },
        attrs: {type: 'number', name: `item.${ctrl.itemType}.num`, value: ctrl.num, min: 1, max: 1000, required: true}
      }),
      h('div.form-unit', ctrl.unit)
    ])
  ]);
}
