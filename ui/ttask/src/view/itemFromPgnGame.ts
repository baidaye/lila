import {h} from 'snabbdom';
import {renderTaskName} from './form'
import {Chessground} from 'chessground';
import TaskCtrl from '../ctrl';
import {bind} from '../util';
import {FromPgn, FromPgnWithResult, Move} from '../interfaces';

export function fromPgnItem(ctrl: TaskCtrl) {
  const fromPgnCtrl = ctrl.fromPgnGameCtrl;

  return h('div.item-fromPgn', [
    renderTaskName(ctrl),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-fromPgns'}}, '已选对局'),
      !fromPgnCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          fromPgnCtrl.addFromPgnGame();
        })
      }, '添加对局') : null,
      renderFromPgns(ctrl, fromPgnCtrl.fromPgnGames)
    ])
  ])
}

export function renderFromPgns(ctrl: TaskCtrl, fromPgns: FromPgnWithResult[], viewOnly: boolean = false) {
  let fromPgnCtrl = ctrl.fromPgnGameCtrl;
  return h('div.fromPgnGames', fromPgns.map((frwr, index) => {
    let fromPgn = frwr.fromPgn;

    return h(`div.fromPgn.${index}`, [
      h('table.tb-fp', [
        h('tbody', [
          h('tr', [
            h('td', '起始局面PGN'),
            h('td.fen', [
              h('textarea', {
                hook: {
                  insert(vnode) {
                    const elm = vnode.elm as HTMLInputElement;
                    $(elm).val(fromPgn.initialPgn.opening.mainline!).on('change keyup paste', () => {
                      fromPgnCtrl.validatePgn(elm, index);
                    })
                  }
                },
                attrs: {
                  rows: 3,
                  placeholder: '在此处粘贴PGN棋谱',
                  name: `item.fromPgn[${index}].pgn`,
                  required: true,
                  disabled: viewOnly
                }
              }, fromPgn.initialPgn.opening.mainline!),
              !viewOnly ? h('a.button.button-empty.button-red.small', {
                hook: bind('click', () => {
                  fromPgnCtrl.removeFromPgnGame(index);
                })
              }, '移除') : null
            ])
          ]),
          h('tr', [
            h('td'),
            h('td.withMoves', [
              h('div.board', [
                h(`div.mini-board.cg-wrap.is2d.${fromPgn.initialPgn.opening.mainline!}`, {
                  hook: {
                    insert(vnode) {
                      fromPgn.el = vnode.elm as HTMLElement;
                      makeChess(fromPgn);
                    }
                  }
                })
              ]),
              h('div.moves', fromPgn.initialPgn.opening.mainlineMoves.map((move, moveIndex) => {
                return h('move', [
                  h('index', `${ move.index}.`),
                  h('span', {
                    class: {active: (!!move.white && move.white.active), disabled: !move.white},
                    attrs: {'data-fen': move.white ? move.white.fen : ''},
                    hook: bind('click', () => {
                      if(move.white) {
                        fromPgn.initialPgn.opening.mainlineMoves.forEach(m => {
                          if(m.white && m.white.active) m.white.active = false;
                          if(m.black && m.black.active) m.black.active = false;
                        });
                        fromPgn.initialPgn.opening.mainlineMoves[moveIndex].white!.active = true;
                        makeChess(fromPgn, move, 'white');
                        ctrl.redraw();
                      }
                    })
                  }, move.white ? move.white.san : '...'),
                  h('span', {
                    class: {active: (!!move.black && move.black!.active), disabled: !move.black},
                    attrs: {'data-fen': move.black ? move.black.fen : ''},
                    hook: bind('click', () => {
                      if(move.black) {
                        fromPgn.initialPgn.opening.mainlineMoves.forEach(m => {
                          if(m.white && m.white.active) m.white.active = false;
                          if(m.black && m.black.active) m.black.active = false;
                        });
                        fromPgn.initialPgn.opening.mainlineMoves[moveIndex].black!.active = true;
                        makeChess(fromPgn, move, 'black');
                        ctrl.redraw();
                      }
                    })
                  }, move.black ? move.black.san : '')
                ])
              }))
            ])
          ]),
          h('tr', [
            h('td', '基本用时'),
            h('td', [
              h('select', {attrs: {name: `item.fromPgn[${index}].clockTime`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'clock', 'initial', v);
                })}, [
                h('option', {attrs: {value: '3.0', selected: fromPgn.clock.initial === 180}}, '3 分钟'),
                h('option', {attrs: {value: '5.0', selected: fromPgn.clock.initial === 300}}, '5 分钟'),
                h('option', {attrs: {value: '10.0', selected: fromPgn.clock.initial === 600}}, '10 分钟'),
                h('option', {attrs: {value: '15.0', selected: fromPgn.clock.initial === 900}}, '15 分钟'),
                h('option', {attrs: {value: '20.0', selected: fromPgn.clock.initial === 1200}}, '20 分钟'),
                h('option', {attrs: {value: '25.0', selected: fromPgn.clock.initial === 1500}}, '25 分钟'),
                h('option', {attrs: {value: '30.0', selected: fromPgn.clock.initial === 1800}}, '30 分钟'),
                h('option', {attrs: {value: '40.0', selected: fromPgn.clock.initial === 2400}}, '40 分钟'),
                h('option', {attrs: {value: '50.0', selected: fromPgn.clock.initial === 3000}}, '50 分钟'),
                h('option', {attrs: {value: '60.0', selected: fromPgn.clock.initial === 3600}}, '60 分钟')
              ])
            ])
          ]),
          h('tr', [
            h('td', '每步棋加时'),
            h('td', [
              h('select', {attrs: {name: `item.fromPgn[${index}].clockIncrement`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'clock', 'increment', v);
                })}, [
                h('option', {attrs: {value: '0', selected: fromPgn.clock.increment === 0}}, '0 秒'),
                h('option', {attrs: {value: '1', selected: fromPgn.clock.increment === 1}}, '1 秒'),
                h('option', {attrs: {value: '2', selected: fromPgn.clock.increment === 2}}, '2 秒'),
                h('option', {attrs: {value: '3', selected: fromPgn.clock.increment === 3}}, '3 秒'),
                h('option', {attrs: {value: '5', selected: fromPgn.clock.increment === 5}}, '5 秒'),
                h('option', {attrs: {value: '10', selected: fromPgn.clock.increment === 10}}, '10 秒'),
                h('option', {attrs: {value: '15', selected: fromPgn.clock.increment === 15}}, '15 秒'),
                h('option', {attrs: {value: '20', selected: fromPgn.clock.increment === 20}}, '20 秒'),
                h('option', {attrs: {value: '25', selected: fromPgn.clock.increment === 25}}, '25 秒'),
                h('option', {attrs: {value: '30', selected: fromPgn.clock.increment === 30}}, '30 秒'),
                h('option', {attrs: {value: '40', selected: fromPgn.clock.increment === 40}}, '40 秒'),
                h('option', {attrs: {value: '50', selected: fromPgn.clock.increment === 50}}, '50 秒'),
                h('option', {attrs: {value: '60', selected: fromPgn.clock.increment === 60}}, '60 秒')
              ])
            ])
          ]),
          h('tr', [
            h('td', '棋色'),
            h('td', [
              h('select', {attrs: {name: `item.fromPgn[${index}].color`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'color', null, v);
                })}, [
                h('option', {attrs: {value: 'random', selected: fromPgn.color === 'random'}}, '随机'),
                h('option', {attrs: {value: 'white', selected: fromPgn.color === 'white'}}, '白'),
                h('option', {attrs: {value: 'black', selected: fromPgn.color === 'black'}}, '黑')
              ])
            ])
          ]),
          h('tr', [
            h('td', '允许人机对弈'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'isAi', null, (v == "1"));
                }),
                attrs: {name: `item.fromPgn[${index}].isAi`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPgn.isAi}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromPgn.isAi}}, '否')
              ])
            ])
          ]),
          h('tr', {class: { none: !fromPgn.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromPgnCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '引擎级别'),
            h('td', [
              h('select', {attrs: {name: `item.fromPgn[${index}].aiLevel`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPgn.aiLevel === 1}}, 'A.I.1（600 分）'),
                h('option', {attrs: {value: '2', selected: fromPgn.aiLevel === 2}}, 'A.I.2（800 分）'),
                h('option', {attrs: {value: '3', selected: fromPgn.aiLevel === 3}}, 'A.I.3（1000 分）'),
                h('option', {attrs: {value: '4', selected: fromPgn.aiLevel === 4}}, 'A.I.4（1200 分）'),
                h('option', {attrs: {value: '5', selected: fromPgn.aiLevel === 5}}, 'A.I.5（1400 分）'),
                h('option', {attrs: {value: '6', selected: fromPgn.aiLevel === 6}}, 'A.I.6（1600 分）'),
                h('option', {attrs: {value: '7', selected: fromPgn.aiLevel === 7}}, 'A.I.7（1800 分）'),
                h('option', {attrs: {value: '8', selected: fromPgn.aiLevel === 8}}, 'A.I.8（2000 分）'),
                h('option', {attrs: {value: '9', selected: fromPgn.aiLevel === 9}}, 'A.I.9（2500 分）'),
                h('option', {attrs: {value: '10', selected: fromPgn.aiLevel === 10}}, 'A.I.10（3000 分）'),
              ])
            ])
          ]),
          h('tr', [
            h('td', '允许悔棋'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'canTakeback', null, (v == "1"));
                }),
                attrs: {name: `item.fromPgn[${index}].canTakeback`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPgn.canTakeback}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromPgn.canTakeback}}, '否')
              ])
            ])
          ]),
          h('tr', [
            h('td', '对局数'),
            h('td', [
              h('input', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPgnCtrl.changeValue(index, 'num', null, parseInt(v));
                }),
                attrs: {type: 'number', name: `item.fromPgn[${index}].num`, value: fromPgn.num, min: 1, max: 20, required: true, disabled: viewOnly}
              })
            ])
          ]),
          h('tr', {class: { none: !fromPgn.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromPgnCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '对局结果'),
            h('td', [
              h('select', {attrs: {name: `item.fromPgn[${index}].chessStatus`, disabled: viewOnly}}, [
                h('option', {attrs: {value: 'all', selected: fromPgn.chessStatus === 'all'}}, '不限定'),
                h('option', {attrs: {value: 'win', selected: fromPgn.chessStatus === 'win'}}, '胜'),
                h('option', {attrs: {value: 'winOrDraw', selected: fromPgn.chessStatus === 'winOrDraw'}}, '胜或和')
              ])
            ])
          ])
        ])
      ])
    ]);
  }))
}

function makeChess(fromPgn: FromPgn, move: Move | undefined = undefined, orient: string | undefined = undefined) {
  let fen = move ? (move && orient && move[orient] ? move[orient].fen : '') : fromPgn.initialPgn.lastFen;
  Chessground(fromPgn.el!, {
    coordinates: false,
    drawable: { enabled: false, visible: false },
    resizable: false,
    viewOnly: true,
    orientation: 'white' as Color,
    fen: fen
  });
}
