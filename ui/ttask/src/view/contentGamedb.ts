import {h} from 'snabbdom';
import TaskCtrl from '../ctrl';

export default function(ctrl: TaskCtrl) {
  ctrl.taskName;
  return h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'gamedb' } }),
    h('input', { attrs: { type: 'hidden', name: 'gamedb' } }),
    h('div.search', [
      h('input.gamedb-search', { attrs: { placeholder: '搜索' } }),
    ]),
    h('div.scroll', [
      h('div.gamedbTree', {
        hook: {
          insert(vnode) {
            let el = vnode.elm as HTMLElement;
            window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
              window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                let $jq = $.noConflict(true);
                let $content = $jq(el).parents('.content');
                let $gamedbInput = $content.find('input[name="gamedb"]');
                let $searchInput = $content.find('.gamedb-search');

                let $tree = <any>$jq(el);
                $tree.jstree({
                  'core': {
                    'worker': false,
                    'data' : {
                      'url' : '/resource/gamedb/tree/loadAll',
                      'data' : function (node) {
                        return { 'selected': node.id };
                      }
                    },
                  },
                  'plugins' : [ 'search' ]
                })
                  .on('changed.jstree', function (_, data) {
                    if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                      if(data.node.original.type === 'file') {
                        let id = data.node.id;
                        $gamedbInput.val(id.split(':')[1]);
                      }
                    }
                  });

                let to;
                $searchInput.keyup(function (e) {
                  let q = (e.target as HTMLInputElement).value;
                  if (to) clearTimeout(to);
                  to = setTimeout(function () {
                    $tree.jstree(true).search(q, false, true);
                  }, 500);
                });
              });
            });
          },
        }
      })
    ])
  ]);
}
