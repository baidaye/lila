import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {Chessground} from 'chessground';
import {bind, bindSubmit} from '../util';
import {Move, ReplayGame, ReplayGameWithResult} from '../interfaces';
import * as ModalBuild from './modal';
import {renderTaskName} from './form'
import renderStudyContent from './contentStudy'
import renderCourseWareContent from './contentCourseWare'
import TaskCtrl from '../ctrl';

export function replayGameItem(ctrl: TaskCtrl) {
  const replayGameCtrl = ctrl.replayGameCtrl;

  return h('div.item-replayGame', [
    renderTaskName(ctrl),
    h(`input.${ctrl.itemType}-${ctrl.num}`, {attrs: {type: 'hidden', value: ctrl.num}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-replayGames'}}, '已选棋谱'),
      !replayGameCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          replayGameCtrl.onReplayGameModalOpen();
        })
      },'选择棋谱') : null,
      renderReplayGames(ctrl, replayGameCtrl.replayGames)
    ])
  ])
}

export function renderReplayGames(ctrl: TaskCtrl, replayGames: ReplayGameWithResult[], viewOnly: boolean = false) {
  return h('div.replayGames', replayGames.map((rgwr, index) => {
    let replayGame = rgwr.replayGame;
    return h(`div.replayGame.${replayGame.chapterId + index}`, [
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.replayGame[${index}].chapterLink`, value: replayGame.chapterLink}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.replayGame[${index}].name`, value: replayGame.name}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.replayGame[${index}].root`, value: replayGame.root}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.replayGame[${index}].color`, value: replayGame.color}}) : null,
      !viewOnly ? h('textarea.none', {attrs: {name: `item.replayGame[${index}].pgn`}}, replayGame.pgn) : null,
      h('div.rg-header', [
        h('a.title', {attrs: {target: '_blank', href: replayGame.chapterLink}}, replayGame.name),
        !viewOnly ? h('a.button.button-empty.button-red.small', {
          hook: bind('click', () => {
            ctrl.replayGameCtrl.removeReplayGame(index);
          })
        }, '移除') : null
      ]),
      h('div.rg-body', [
        h('div.board', [
          h('div.mini-board.cg-wrap.is2d', {
            hook: {
              insert(vnode) {
                replayGame.el = vnode.elm as HTMLElement;
                makeChess(replayGame);
              }
            }
          })
        ]),
        h('div.moves', replayGame.moves.map((move, moveIndex) => {
          return h('move', [
            h('index', `${move.index}.`),
            h('span', {
              class: {active: (!!move.white && move.white.active), disabled: !move.white},
              attrs: {'data-fen': move.white ? move.white.fen : ''},
              hook: bind('click', () => {
                if (move.white) {
                  replayGame.moves.forEach(m => {
                    if (m.white && m.white.active) m.white.active = false;
                    if (m.black && m.black.active) m.black.active = false;
                  });
                  move.white.active = true;
                  makeChess(replayGame, move, 'white');
                  ctrl.redraw();
                }
              })
            }, move.white ? move.white.san : '...'),
            h('span', {
              class: {active: (!!move.black && move.black!.active), disabled: !move.black},
              attrs: {'data-fen': move.black ? move.black.fen : ''},
              hook: bind('click', () => {
                if (move.black) {
                  replayGame.moves.forEach(m => {
                    if (m.white && m.white.active) m.white.active = false;
                    if (m.black && m.black.active) m.black.active = false;
                  });
                  move.black.active = true;
                  makeChess(replayGame, move, 'black');
                  ctrl.redraw();
                }
              })
            }, move.black ? move.black.san : ''),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].index`,
                value: move.index
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].white.san`,
                value: move.white ? move.white.san : ''
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].white.uci`,
                value: move.white ? move.white.uci : ''
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].white.fen`,
                value: move.white ? move.white.fen : ''
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].black.san`,
                value: move.black ? move.black.san : ''
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].black.uci`,
                value: move.black ? move.black.uci : ''
              }
            }),
            h('input', {
              attrs: {
                type: 'hidden',
                name: `item.replayGame[${index}].moves[${moveIndex}].black.fen`,
                value: move.black ? move.black.fen : ''
              }
            }),
          ])
        }))
      ])
    ]);
  }));
}

function makeChess(replayGame: ReplayGame, move: Move | undefined = undefined, color: string | undefined = undefined) {
  Chessground(replayGame.el, {
    coordinates: false,
    drawable: { enabled: false, visible: false },
    resizable: false,
    viewOnly: true,
    orientation: (replayGame.color ? replayGame.color : 'white') as Color,
    fen: move ? (move && color && move[color] ? move[color].fen : '') : replayGame.root
  });
}

export function replayGameModal(ctrl: TaskCtrl): VNode {
  const replayGameCtrl = ctrl.replayGameCtrl;

  return ModalBuild.modal({
    onClose: () => {
      replayGameCtrl.onReplayGameModalClose();
    },
    class: `replayGameModal`,
    content: [
      h('h2', '打谱'),
      h('div.modal-content-body', [
        h('form.replayGameForm', {
          hook: bindSubmit(_ => {
            replayGameCtrl.submitReplayGame();
          })
        }, [
          h('div.tabs-horiz', [
            makeTab(ctrl, 'study', '研习章节', '研习章节'),
            ctrl.hasCourseWare() ? makeTab(ctrl, 'courseWare', '课件', '课件') : null
          ]),
          h('div.tabs-content',[
            studyContent(ctrl),
            ctrl.hasCourseWare() ? courseWareContent(ctrl) : null
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                replayGameCtrl.onReplayGameModalClose();
              })
            },'取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function studyContent(ctrl: TaskCtrl) {
  const replayGameCtrl = ctrl.replayGameCtrl;
  return replayGameCtrl.activeTab === 'study' ? renderStudyContent(ctrl) : null;
}

function courseWareContent(ctrl: TaskCtrl) {
  const replayGameCtrl = ctrl.replayGameCtrl;
  return replayGameCtrl.activeTab === 'courseWare' ? renderCourseWareContent(ctrl) : null;
}

function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  const replayGameCtrl = ctrl.replayGameCtrl;
  return h('span.' + key, {
    class: { active: replayGameCtrl.activeTab === key },
    attrs: { title },
    hook: bind('click', () => {
      replayGameCtrl.activeTab = key;
      ctrl.redraw();
    })
  }, name);
}
