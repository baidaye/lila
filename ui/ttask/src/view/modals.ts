import {VNode} from 'snabbdom/vnode'
import {renderFormModal} from './form';
import {themePuzzleModal as renderThemePuzzleModal} from './itemThemePuzzle';
import {capsuleModal as renderCapsuleModal} from './itemCapsulePuzzle';
import {coordTrainModal as renderCoordTrainModal} from './itemCoordTrain';
import {customPuzzleRushModal as renderCustomPuzzleRushModal} from './itemPuzzleRush';
import {replayGameModal as renderReplayGameModal} from './itemReplayGame';
import {recallGameModal as renderRecallGameModal} from './itemRecallGame';
import {distinguishGameModal as renderDistinguishGameModal} from './itemDistinguishGame';
import {fromOpeningdbSelectModal as fromOpeningdbSelectModal} from './itemFromOpeningdbGame';
import TTaskCtrl from '../ctrl';

export default function (ctrl: TTaskCtrl, excludes: string[] = []): VNode[] {
  let nodes: VNode[] = [];
  if(ctrl.showCreateModal && !excludes.includes('showCreateModal')) {
    nodes.push(renderFormModal(ctrl));
  }
  if(ctrl.themePuzzleCtrl.showThemePuzzleModal && !excludes.includes('showThemePuzzleModal')) {
    nodes.push(renderThemePuzzleModal(ctrl));
  }
  if(ctrl.puzzleRushCtrl.showCustomPuzzleRushModal && !excludes.includes('showCustomPuzzleRushModal')) {
    nodes.push(renderCustomPuzzleRushModal(ctrl));
  }
  if(ctrl.capsulePuzzleCtrl.showCapsuleModal && !excludes.includes('showCapsuleModal')) {
    nodes.push(renderCapsuleModal(ctrl));
  }
  if(ctrl.coordTrainCtrl.showCoordTrainModal && !excludes.includes('showCoordTrainModal')) {
    nodes.push(renderCoordTrainModal(ctrl));
  }
  if(ctrl.replayGameCtrl.showReplayGameModal && !excludes.includes('showReplayGameModal')) {
    nodes.push(renderReplayGameModal(ctrl));
  }
  if(ctrl.recallGameCtrl.showRecallGameModal && !excludes.includes('showRecallGameModal')) {
    nodes.push(renderRecallGameModal(ctrl));
  }
  if(ctrl.distinguishGameCtrl.showDistinguishGameModal && !excludes.includes('showDistinguishGameModal')) {
    nodes.push(renderDistinguishGameModal(ctrl));
  }
  if(ctrl.fromOpeningdbGameCtrl.showOpeningdbSelectModal && !excludes.includes('fromOpeningdbSelectModal')) {
    nodes.push(fromOpeningdbSelectModal(ctrl));
  }
  return nodes;
}

