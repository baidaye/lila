import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {Chessground} from 'chessground';
import {bind, bindSubmit} from '../util';
import {Move, RecallGame, RecallGameWithResult} from '../interfaces';
import * as ModalBuild from './modal';
import {renderTaskName} from './form'
import renderStudyContent from './contentStudy'
import renderGamedbContent from './contentGamedb'
import renderCourseWareContent from "./contentCourseWare";
import TaskCtrl from '../ctrl';


export function recallGameItem(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return h('div.item-recallGame', [
    renderTaskName(ctrl),
    h(`input.${ctrl.itemType}-${ctrl.num}`, {attrs: {type: 'hidden', value: ctrl.num}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-recallGames'}}, '已选棋谱'),
      !recallGameCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          recallGameCtrl.onRecallGameModalOpen();
        })
      },'选择棋谱') : null,
      renderRecallGames(ctrl, recallGameCtrl.recallGames)
    ])
  ])
}

export function renderRecallGames(ctrl: TaskCtrl, recallGames: RecallGameWithResult[], viewOnly: boolean = false) {
  return h('div.recallGames', recallGames.map((rgwr, index) => {
    let recallGame = rgwr.recallGame;
    let colorName = function () {
      if(recallGame.color === 'white') return '白方';
      else if(recallGame.color === 'black') return '黑方';
      else return '双方'
    };

    let orientName = function () {
      if(recallGame.orient === 'white') return '白方';
      else if(recallGame.orient === 'black') return '黑方';
      else return '-'
    };

    let turnsName = function () {
      if(!recallGame.turns) return '所有';
      else return recallGame.turns;
    };

    return h(`div.recallGame.${index}`, [
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].title`, value: recallGame.title}}) : null,
      !viewOnly && recallGame.color !== 'all' ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].color`, value: recallGame.color ? recallGame.color : ''}}) : null,
      !viewOnly ?  h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].orient`, value: recallGame.orient}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].turns`, value: recallGame.turns ? recallGame.turns : ''}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].name`, value: recallGame.name}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].root`, value: recallGame.fen}}) : null,
      !viewOnly ? h('textarea.none', { attrs: { name: `item.recallGame[${index}].pgn`}}, recallGame.pgn) : null,
      h('div.rg-header', [
        h('div.left', [
          h('div.title', recallGame.name),
          h('div.meta', [
            h('span', `棋色：${colorName()}`),
            h('span', `棋盘方向：${orientName()}`),
            h('span', `回合数：${turnsName()}`)
          ])
        ]),
        !viewOnly ? h('a.button.button-empty.button-red.small', {
          hook: bind('click', () => {
            ctrl.recallGameCtrl.removeRecallGame(index);
          })
        },'移除') : null
      ]),
      h('div.rg-body', [
        h('div.board', [
          h('div.mini-board.cg-wrap.is2d', {
            hook: {
              insert(vnode) {
                recallGame.el = vnode.elm as HTMLElement;
                makeChess(recallGame);
              }
            }
          })
        ]),
        h('div.moves', recallGame.moves.map((move, moveIndex) => {
          return h('move', [
            h('index', `${ move.index}.`),
            h('span', {
              class: {active: (!!move.white && move.white.active), disabled: !move.white},
              attrs: {'data-fen': move.white ? move.white.fen : ''},
              hook: bind('click', () => {
                if(move.white) {
                  recallGame.moves.forEach(m => {
                    if(m.white && m.white.active) m.white.active = false;
                    if(m.black && m.black.active) m.black.active = false;
                  });
                  move.white.active = true;
                  makeChess(recallGame, move, 'white');
                  ctrl.redraw();
                }
              })
            }, move.white ? move.white.san : '...'),
            h('span', {
              class: {active: (!!move.black && move.black!.active), disabled: !move.black},
              attrs: {'data-fen': move.black ? move.black.fen : ''},
              hook: bind('click', () => {
                if(move.black) {
                  recallGame.moves.forEach(m => {
                    if(m.white && m.white.active) m.white.active = false;
                    if(m.black && m.black.active) m.black.active = false;
                  });
                  move.black.active = true;
                  makeChess(recallGame, move, 'black');
                  ctrl.redraw();
                }
              })
            }, move.black ? move.black.san : ''),
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].index`, value: move.index}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].white.san`, value: move.white ? move.white.san : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].white.uci`, value: move.white ? move.white.uci : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].white.fen`, value: move.white ? move.white.fen : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].black.san`, value: move.black ? move.black.san : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].black.uci`, value: move.black ? move.black.uci : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.recallGame[${index}].moves[${moveIndex}].black.fen`, value: move.black ? move.black.fen : ''}}) : null,
          ])
        }))
      ])
    ]);
  }))
}

function makeChess(recallGame: RecallGame, move: Move | undefined = undefined, orient: string | undefined = undefined) {
  Chessground(recallGame.el, {
    coordinates: false,
    drawable: { enabled: false, visible: false },
    resizable: false,
    viewOnly: true,
    orientation: recallGame.orient as Color,
    fen: move ? (move && orient && move[orient] ? move[orient].fen : '') : recallGame.fen
  });
}

export function recallGameModal(ctrl: TaskCtrl): VNode {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return ModalBuild.modal({
    onClose: function() {
      recallGameCtrl.onRecallGameModalClose();
    },
    class: `recallGameModal`,
    content: [
      h('h2', '记谱'),
      h('div.modal-content-body', [
        h('form.form3.recallGameForm', {
          hook: bindSubmit(_ => {
            recallGameCtrl.submitRecallGame();
          })
        }, [
          h('input', {attrs: {type: 'hidden', name: 'name', value: 'nil'}}),
          h('div.form-split', [
            h('div.form-group.form-half', [
              h('label.form-label', {attrs: {for: 'form-color'}}, '棋色'),
              h('select#form-color.form-control', {attrs: {name: 'color'}}, [
                h('option', { attrs: { value: 'all' } }, '全部'),
                h('option', { attrs: { value: 'white' } }, '白棋'),
                h('option', { attrs: { value: 'black' } }, '黑棋')
              ])
            ]),
            h('div.form-group.form-half', [
              h('label.form-label', {attrs: {for: 'form-turns'}}, '回合数'),
              h('input.form-control', {
                attrs: {
                  id: 'form-turns',
                  name: 'turns',
                  type: 'number',
                  min: 1,
                  max: 500,
                  placeholder: '不填写表示所有回合'
                }
              })
            ])
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: 'form-orient'}}, '棋盘方向'),
            h('select#form-orient.form-control', {attrs: {name: 'orient'}}, [
              h('option', { attrs: { value: 'white' } }, '白方'),
              h('option', { attrs: { value: 'black' } }, '黑方')
            ])
          ]),
          h('div.tabs-horiz', [
            makeTab(ctrl, 'study', '研习章节', '研习章节'),
            ctrl.hasCourseWare() ? makeTab(ctrl, 'courseWare', '课件', '课件') : null,
            makeTab(ctrl, 'gamedb', '选择对局', '选择对局'),
            makeTab(ctrl, 'game', '对局链接', '对局链接'),
            makeTab(ctrl, 'pgn', 'PGN', 'PGN')
          ]),
          h('div.tabs-content',[
            studyContent(ctrl),
            ctrl.hasCourseWare() ? courseWareContent(ctrl) : null,
            gamedbContent(ctrl),
            gameContent(ctrl),
            pgnContent(ctrl)
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                recallGameCtrl.onRecallGameModalClose();
              })
            },'取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return h('span.' + key, {
    class: { active: recallGameCtrl.activeTab === key },
    attrs: { title },
    hook: bind('click', () => {
      recallGameCtrl.activeTab = key;
      ctrl.redraw();
    })
  }, name);
}

function studyContent(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return recallGameCtrl.activeTab === 'study' ? renderStudyContent(ctrl) : null;
}

function courseWareContent(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return recallGameCtrl.activeTab === 'courseWare' ? renderCourseWareContent(ctrl) : null;
}

function gamedbContent(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return recallGameCtrl.activeTab === 'gamedb' ? renderGamedbContent(ctrl) : null
}

function gameContent(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return recallGameCtrl.activeTab === 'game' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'game' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-game' }
      }, '对局URL'),
      h('input#tab-form-game.form-control', {
        attrs: { name: 'game', required: true, placeholder: '对局URL' },
      }),
      h('small.form-help', [
        h('div', '例：https://haichess.com/XTtL9RRY'),
        h('div', '或：https://haichess.com/XTtL9RRY/white')
      ]),
    ])
  ]) : null
}

function pgnContent(ctrl: TaskCtrl) {
  const recallGameCtrl = ctrl.recallGameCtrl;
  return recallGameCtrl.activeTab === 'pgn' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'pgn' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn' }
      }, '请输入PGN'),
      h('textarea#tab-form-pgn.form-control', {
        attrs: { name: 'pgn', rows: 5, required: true, placeholder: '粘贴PGN文本' }
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn-import' }
      }, '上传PGN文件'),
      h('input#tab-form-pgn-import.form-control', {
        attrs: { type: 'file', name: 'pgnFile', accept: '.pgn' },
        hook: {
          insert(vnode) {
            (vnode.elm as HTMLInputElement).addEventListener('change', function() {
              // @ts-ignore
              let file = this.files[0];
              if (!file) return;
              let reader = new FileReader();
              reader.onload = function(e) {
                // @ts-ignore
                $('#tab-form-pgn').val(e.target.result);
              };
              reader.readAsText(file);
            });
          }
        }
      })
    ])
  ]) : null
}

