import {h} from 'snabbdom';
import {bind} from '../util';
import numOrRatingContent from './contentNumOrRating';
import {renderTaskName} from './form';
import TaskCtrl from '../ctrl';

export function gameItem(ctrl: TaskCtrl) {

  const gameCtrl = ctrl.gameCtrl;
/*  const speeds = [
      {
        'k': 'ultraBullet',
        'v': '闪电棋（小于2分钟）'
      },
      {
        'k': 'bullet',
        'v': '超快棋（2到10分钟）'
      },
      {
        'k': 'blitz',
        'v': '快棋（10到20分钟）'
      },
      {
        'k': 'rapid',
        'v': '中速棋（20到50分钟）'
      },
      {
        'k': 'classical',
        'v': '慢棋（50分钟以上）'
      }
    ];*/

  let gameSpeed = (speed) => {
    return h('span.radio-group', [
      h('input', {
        attrs: {
          id: `game-${speed.k}`,
          name: `item.game.speed`,
          type: 'radio',
          value: speed.k,
          checked: speed.k === gameCtrl.game.speed
        },
        hook: bind('click', _ => {
          gameCtrl.changeGameSpeed(speed);
        })
      }),
      h('label', {attrs: {for: `game-${speed.k}`}}, speed.v)
    ])
  };

  return h('div.item-game', [
    renderTaskName(ctrl),
    numOrRatingContent(ctrl),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-speed'}}, '棋速'),
      h('div.radio-group', [
        h('table.form-speed-table', [
          h('tbody', [
/*            h('tr', [
              h('td', gameSpeed({'k': 'ultraBullet', 'v': '闪电棋(½+0)'})),
              h('td', gameSpeed({'k': 'bullet', 'v': '超快棋(2+1)'}))
            ]),*/
            h('tr', [
              h('td', gameSpeed({'k': 'blitz', 'v': '超快棋'})),
              h('td', '3+2/5+0/5+3'),
              h('td')
            ]),
            h('tr', [
              h('td', gameSpeed({'k': 'rapid', 'v': '快棋'})),
              h('td', '10+0/10+5/15+10'),
              h('td', [h('span.new', {attrs: {title: '通过与lichess用户匹配完成任务'}}, '支持lichess匹配')])
            ]),
            h('tr', [
              h('td', gameSpeed({'k': 'classical', 'v': '慢棋'})),
              h('td', '30+0/30+20'),
              h('td', [h('span.new', {attrs: {title: '通过与lichess用户匹配完成任务'}}, '支持lichess匹配')])
            ])
          ])
        ])
      ])
    ])
  ]);
}

