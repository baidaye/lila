import {h} from 'snabbdom';
import {renderTaskName} from './form'
import {Chessground} from 'chessground';
import TaskCtrl from '../ctrl';
import {bind} from '../util';
import {FromPositionWithResult} from '../interfaces';

export function fromPositionItem(ctrl: TaskCtrl) {
  const fromPositionCtrl = ctrl.fromPositionGameCtrl;

  return h('div.item-fromPosition', [
    renderTaskName(ctrl),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-fromPositions'}}, '已选对局'),
      !fromPositionCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          fromPositionCtrl.addFromPositionGame();
        })
      }, '添加对局') : null,
      renderFromPositions(ctrl, fromPositionCtrl.fromPositionGames)
    ])
  ])
}

export function renderFromPositions(ctrl: TaskCtrl, fromPositions: FromPositionWithResult[], viewOnly: boolean = false) {
  let fromPositionCtrl = ctrl.fromPositionGameCtrl;
  return h('div.fromPositionGames', fromPositions.map((frwr, index) => {
    let fromPosition = frwr.fromPosition;
    return h(`div.fromPosition.${index}`, [
      h('table.tb-fp', [
        h('tbody', [
          h('tr', [
            h('td', [
              !viewOnly ? h('a.button.button-empty.small', {
                hook: bind('click', (e) => {
                  fromPositionCtrl.loadSituation(e.target as HTMLElement);
                })
              }, '起始局面FEN') : '起始局面FEN'
            ]),
            h('td.fen', [
              h('input', {
                hook: {
                  insert(vnode) {
                    const elm = vnode.elm as HTMLInputElement;
                    $(elm).val(fromPosition.fen).on('change keyup paste', () => {
                      fromPositionCtrl.validateFen(elm, index);
                    })
                  }
                },
                attrs: {
                  name: `item.fromPosition[${index}].fen`,
                  value: fromPosition.fen,
                  required: true,
                  disabled: viewOnly
                }
              }),
              !viewOnly ? h('a.button.button-empty.button-red.small', {
                hook: bind('click', () => {
                  fromPositionCtrl.removeFromPositionGame(index);
                })
              }, '移除') : null
            ])
          ]),
          h('tr', [
            h('td'),
            h('td', [
              h('div.board', [
                h(`div.mini-board.cg-wrap.is2d.${fromPosition.fen}`, {
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLElement;
                      let color = ctrl.toColor(fromPosition.fen);
                      Chessground(el, {
                        coordinates: false,
                        drawable: {enabled: false, visible: false},
                        resizable: false,
                        viewOnly: true,
                        orientation: color ? color : 'white',
                        fen: fromPosition.fen
                      });
                      fromPosition.el = el.parentElement;
                    }
                  }
                })
              ])
            ])
          ]),
          h('tr', [
            h('td', '基本用时'),
            h('td', [
              h('select', {attrs: {name: `item.fromPosition[${index}].clockTime`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'clock', 'initial', v);
                })}, [
                h('option', {attrs: {value: '3.0', selected: fromPosition.clock.initial === 180}}, '3 分钟'),
                h('option', {attrs: {value: '5.0', selected: fromPosition.clock.initial === 300}}, '5 分钟'),
                h('option', {attrs: {value: '10.0', selected: fromPosition.clock.initial === 600}}, '10 分钟'),
                h('option', {attrs: {value: '15.0', selected: fromPosition.clock.initial === 900}}, '15 分钟'),
                h('option', {attrs: {value: '20.0', selected: fromPosition.clock.initial === 1200}}, '20 分钟'),
                h('option', {attrs: {value: '25.0', selected: fromPosition.clock.initial === 1500}}, '25 分钟'),
                h('option', {attrs: {value: '30.0', selected: fromPosition.clock.initial === 1800}}, '30 分钟'),
                h('option', {attrs: {value: '40.0', selected: fromPosition.clock.initial === 2400}}, '40 分钟'),
                h('option', {attrs: {value: '50.0', selected: fromPosition.clock.initial === 3000}}, '50 分钟'),
                h('option', {attrs: {value: '60.0', selected: fromPosition.clock.initial === 3600}}, '60 分钟')
              ])
            ])
          ]),
          h('tr', [
            h('td', '每步棋加时'),
            h('td', [
              h('select', {attrs: {name: `item.fromPosition[${index}].clockIncrement`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'clock', 'increment', v);
                })}, [
                h('option', {attrs: {value: '0', selected: fromPosition.clock.increment === 0}}, '0 秒'),
                h('option', {attrs: {value: '1', selected: fromPosition.clock.increment === 1}}, '1 秒'),
                h('option', {attrs: {value: '2', selected: fromPosition.clock.increment === 2}}, '2 秒'),
                h('option', {attrs: {value: '3', selected: fromPosition.clock.increment === 3}}, '3 秒'),
                h('option', {attrs: {value: '5', selected: fromPosition.clock.increment === 5}}, '5 秒'),
                h('option', {attrs: {value: '10', selected: fromPosition.clock.increment === 10}}, '10 秒'),
                h('option', {attrs: {value: '15', selected: fromPosition.clock.increment === 15}}, '15 秒'),
                h('option', {attrs: {value: '20', selected: fromPosition.clock.increment === 20}}, '20 秒'),
                h('option', {attrs: {value: '25', selected: fromPosition.clock.increment === 25}}, '25 秒'),
                h('option', {attrs: {value: '30', selected: fromPosition.clock.increment === 30}}, '30 秒'),
                h('option', {attrs: {value: '40', selected: fromPosition.clock.increment === 40}}, '40 秒'),
                h('option', {attrs: {value: '50', selected: fromPosition.clock.increment === 50}}, '50 秒'),
                h('option', {attrs: {value: '60', selected: fromPosition.clock.increment === 60}}, '60 秒')
              ])
            ])
          ]),
          h('tr', [
            h('td', '棋色'),
            h('td', [
              h('select', {attrs: {name: `item.fromPosition[${index}].color`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'color', null, v);
                })}, [
                h('option', {attrs: {value: 'random', selected: fromPosition.color === 'random'}}, '随机'),
                h('option', {attrs: {value: 'white', selected: fromPosition.color === 'white'}}, '白'),
                h('option', {attrs: {value: 'black', selected: fromPosition.color === 'black'}}, '黑')
              ])
            ])
          ]),
          h('tr', [
            h('td', '允许人机对弈'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'isAi', null, (v == "1"));
                }),
                attrs: {name: `item.fromPosition[${index}].isAi`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPosition.isAi}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromPosition.isAi}}, '否')
              ])
            ])
          ]),
          h('tr', {class: { none: !fromPosition.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromPositionCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '引擎级别'),
            h('td', [
              h('select', {attrs: {name: `item.fromPosition[${index}].aiLevel`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPosition.aiLevel === 1}}, 'A.I.1（600 分）'),
                h('option', {attrs: {value: '2', selected: fromPosition.aiLevel === 2}}, 'A.I.2（800 分）'),
                h('option', {attrs: {value: '3', selected: fromPosition.aiLevel === 3}}, 'A.I.3（1000 分）'),
                h('option', {attrs: {value: '4', selected: fromPosition.aiLevel === 4}}, 'A.I.4（1200 分）'),
                h('option', {attrs: {value: '5', selected: fromPosition.aiLevel === 5}}, 'A.I.5（1400 分）'),
                h('option', {attrs: {value: '6', selected: fromPosition.aiLevel === 6}}, 'A.I.6（1600 分）'),
                h('option', {attrs: {value: '7', selected: fromPosition.aiLevel === 7}}, 'A.I.7（1800 分）'),
                h('option', {attrs: {value: '8', selected: fromPosition.aiLevel === 8}}, 'A.I.8（2000 分）'),
                h('option', {attrs: {value: '9', selected: fromPosition.aiLevel === 9}}, 'A.I.9（2500 分）'),
                h('option', {attrs: {value: '10', selected: fromPosition.aiLevel === 10}}, 'A.I.10（3000 分）'),
              ])
            ])
          ]),
          h('tr', [
            h('td', '允许悔棋'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'canTakeback', null, (v == "1"));
                }),
                attrs: {name: `item.fromPosition[${index}].canTakeback`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromPosition.canTakeback}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromPosition.canTakeback}}, '否')
              ])
            ])
          ]),
          h('tr', [
            h('td', '对局数'),
            h('td', [
              h('input', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromPositionCtrl.changeValue(index, 'num', null, parseInt(v));
                }),
                attrs: {type: 'number', name: `item.fromPosition[${index}].num`, value: fromPosition.num, min: 1, max: 20, required: true, disabled: viewOnly}
              })
            ])
          ]),
          h('tr', {class: { none: !fromPosition.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromPositionCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '对局结果'),
            h('td', [
              h('select', {attrs: {name: `item.fromPosition[${index}].chessStatus`, disabled: viewOnly}}, [
                h('option', {attrs: {value: 'all', selected: fromPosition.chessStatus === 'all'}}, '不限定'),
                h('option', {attrs: {value: 'win', selected: fromPosition.chessStatus === 'win'}}, '胜'),
                h('option', {attrs: {value: 'winOrDraw', selected: fromPosition.chessStatus === 'winOrDraw'}}, '胜或和')
              ])
            ])
          ])
        ])
      ])
    ]);
  }))
}

