import {h} from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import {bind, spinner} from '../util';
import TaskCtrl from '../ctrl';

export default function(ctrl: TaskCtrl): VNode | null {
  const courseWareCtrl = ctrl.courseWareCtrl;
  return ctrl.hasCourseWare() ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'courseWare' } }),
    courseWareCtrl.currentCourseWareId ? h('input', {
      attrs: {
        type: 'hidden',
        name: 'courseWare',
        value: `${courseWareCtrl.currentCourseId + ':' + courseWareCtrl.currentCourseWareId}`
      }
    }) : null,
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-course'}}, '选择课节'),
      h(`select#form-course.form-control`, {
        hook: bind('change', (e: Event) => {
          let v = (e.target as HTMLInputElement).value;
          courseWareCtrl.setCurrentCourse(v);
        })
      }, courseWareCtrl.courses.map(item => {
        return h('option', {attrs: {value: item.id, selected: courseWareCtrl.currentCourseId == item.id }}, item.dateTime)
      }))
    ]),
    h('div.scroll', [
      courseWareCtrl.courseWareLoading ? spinner () : h('ul.courseWare-list', courseWareCtrl.courseWares.length > 0 ?
        courseWareCtrl.courseWares.map((item, _) => {
          return h('li', { attrs: {'data-id': item.id } }, [
            h('span', [
              h('input', {
                attrs: {type: 'radio', id: `rd-courseWare-${item.id}`, name: `rd-courseWare`, value: item.id, checked: courseWareCtrl.currentCourseWareId == item.id },
                hook: bind('click', () => {
                  courseWareCtrl.currentCourseWareId = item.id;
                  ctrl.redraw();
                })
              }),
              h('label', {attrs: {for: `rd-courseWare-${item.id}`}}, ` ${item.name}`)
            ])
          ])
        }) : [h('li', '没有更多了.')])
    ])
  ]) : null;
}
