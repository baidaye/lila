import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {bind, bindSubmit, getFormVariable} from '../util';
import * as ModalBuild from './modal';
import {renderTaskName} from './form'
import TaskCtrl from '../ctrl';

export function themePuzzleItem(ctrl: TaskCtrl) {
  const themePuzzleCtrl = ctrl.themePuzzleCtrl;
  const cond = themePuzzleCtrl.themePuzzle.extra ? themePuzzleCtrl.themePuzzle.extra.cond : '';
  const tags = themePuzzleCtrl.themePuzzle.tags ? themePuzzleCtrl.themePuzzle.tags : [];
  return h('div.item-themePuzzle', [
    renderTaskName(ctrl),
    h('input', {attrs: {type: 'hidden', name: 'item.themePuzzle.isNumber', value: 'true'}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-num'}}, '目标值'),
      h(`input#form-num.form-control.${ctrl.itemType}-${ctrl.num}`, {
        hook: {
          insert(vnode) {
            const $el = $(vnode.elm as HTMLElement);
            $el.val(ctrl.num).on('change keyup paste', () => {
              ctrl.num = $el.val();
              ctrl.setTaskName();
            })
          }
        },
        attrs: {type: 'number', name: 'item.themePuzzle.num', value: ctrl.num, min: 1, max: 3000, required: true}
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-cond'}}, '条件设置'),
      h('div.cond', [
        h('div.form-control.selected', {
          class: {
            has: tags.length > 0
          },
          hook: bind('click', () => {
            themePuzzleCtrl.onThemePuzzleModalOpen();
          })
        }, [
          h('div.tags', tags.map(t => {
            return h('span', t);
          })),
          h('input', {
            attrs: {
              type: 'hidden',
              id: 'extraCond',
              name: 'item.themePuzzle.extra.cond',
              value: cond
            }
          })
        ])
      ])
    ])
  ]);
}

export function themePuzzleModal(ctrl: TaskCtrl): VNode {
  const themePuzzleCtrl = ctrl.themePuzzleCtrl;
  const cond = themePuzzleCtrl.themePuzzle.extra ? themePuzzleCtrl.themePuzzle.extra.cond : '';
  const tags = themePuzzleCtrl.themePuzzle.tags ? themePuzzleCtrl.themePuzzle.tags : [];
  let tagGroup = (field) => {
    return h('div.tag-group', ctrl.opts.themePuzzle[field].map((t, i) => {
      return h('span', [
        h('input', {
          attrs: {
            id: `${field}-${t.id}`,
            name: `${field}[${i}]`,
            type: 'checkbox',
            value: t.id,
            checked: tags.includes(t.name)
          },
          hook: bind('click', () => {
            themePuzzleCtrl.loadThemePuzzleCount();
          })
        }),
        h('label', {attrs: {for: `${field}-${t.id}`}}, t.name)
      ])
    }))
  };

  let trTags = (label, field) => {
    return h('tr', [
      h('th', [
        h('label', label)
      ]),
      h('td', [
        tagGroup(field)
      ])
    ])
  };

  return ModalBuild.modal({
    onClose: () => {
      themePuzzleCtrl.onThemePuzzleModalClose();
    },
    class: `themePuzzleModal`,
    content: [
      h('h2', '主题战术条件设置'),
      h('div.modal-content-body', [
        h('form.search_form.themePuzzleForm', {
          hook: bindSubmit(_ => {
            themePuzzleCtrl.submitThemePuzzle();
          })
        }, [
          h('table', [
            h('tbody', [
              h('tr', [
                h('th', [
                  h('label', '难度范围')
                ]),
                h('td', [
                  h('div.half', [
                    '从',
                    h('input.form-control', {
                      hook: {
                        insert(vnode) {
                          const $el = $(vnode.elm as HTMLElement);
                          $el.on('change keyup paste', () => {
                            themePuzzleCtrl.loadThemePuzzleCount();
                          })
                        }
                      },
                      attrs: {
                        id: 'form3-ratingMin',
                        name: 'ratingMin',
                        type: 'number',
                        min: 500,
                        max: 2600,
                        value: getFormVariable(cond, 'ratingMin'),
                        placeholder: '500 ~ 2600'
                      }
                    })
                  ]),
                  h('div.half', [
                    '到',
                    h('input.form-control', {
                      hook: {
                        insert(vnode) {
                          const $el = $(vnode.elm as HTMLElement);
                          $el.on('change keyup paste', () => {
                            themePuzzleCtrl.loadThemePuzzleCount();
                          })
                        }
                      },
                      attrs: {
                        id: 'form3-ratingMax',
                        name: 'ratingMax',
                        type: 'number',
                        min: 500,
                        max: 2600,
                        value: getFormVariable(cond, 'ratingMax'),
                        placeholder: '500 ~ 2600'
                      }
                    })
                  ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', '答案步数')
                ]),
                h('td', [
                  h('div.half', [
                    '从',
                    h('input.form-control', {
                      hook: {
                        insert(vnode) {
                          const $el = $(vnode.elm as HTMLElement);
                          $el.on('change keyup paste', () => {
                            themePuzzleCtrl.loadThemePuzzleCount();
                          })
                        }
                      },
                      attrs: {
                        id: 'form3-stepsMin',
                        name: 'stepsMin',
                        type: 'number',
                        min: 1,
                        max: 10,
                        value: getFormVariable(cond, 'stepsMin'),
                        placeholder: '1 ~ 10'
                      }
                    })
                  ]),
                  h('div.half', [
                    '到',
                    h('input.form-control', {
                      hook: {
                        insert(vnode) {
                          const $el = $(vnode.elm as HTMLElement);
                          $el.on('change keyup paste', () => {
                            themePuzzleCtrl.loadThemePuzzleCount();
                          })
                        }
                      },
                      attrs: {
                        id: 'form3-stepsMax',
                        name: 'stepsMax',
                        type: 'number',
                        min: 1,
                        max: 10,
                        value: getFormVariable(cond, 'stepsMax'),
                        placeholder: '1 ~ 10'
                      }
                    })
                  ])
                ])
              ]),
              trTags('黑白', 'pieceColor'),
              trTags('阶段', 'phase'),
              trTags('目的', 'moveFor'),
              trTags('子力', 'strength'),
              trTags('局面', 'chessGame'),
              trTags('技战术', 'subject'),
              trTags('综合', 'comprehensive'),
              h('tr', [
                h('th'),
                h('td.action', [
/*                  h('a.button.button-empty.small.btn-search', {
                    hook: bind('click', () => {
                      themePuzzleCtrl.loadThemePuzzleCount();
                    })
                  }, '显示题目数量'),*/
                  '共',
                  h('div.count', '- -'),
                  '题'
                ])
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                themePuzzleCtrl.onThemePuzzleModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}
