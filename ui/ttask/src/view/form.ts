import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {puzzleItem as renderPuzzleItem} from './puzzle';
import {themePuzzleItem as renderThemePuzzleItem} from './itemThemePuzzle';
import {puzzleRushItem as renderPuzzleRushItem} from './itemPuzzleRush';
import {capsulePuzzleItem as renderCapsulePuzzleItem} from './itemCapsulePuzzle';
import {coordTrainItem as renderCoordTrainItem} from './itemCoordTrain';
import {replayGameItem as renderReplayGameItem} from './itemReplayGame';
import {recallGameItem as renderRecallGameItem} from './itemRecallGame';
import {distinguishGameItem as renderDistinguishGameItem} from './itemDistinguishGame';
import {fromPositionItem as renderFromPositionGameItem} from './itemFromPositionGame';
import {fromPgnItem as renderFromPgnGameItem} from './itemFromPgnGame';
import {fromOpeningdbItem as renderFromOpeningdbGameItem} from './itemFromOpeningdbGame';
import {gameItem as renderGameItem} from './itemGame';
import * as ModalBuild from './modal';
import {bind, bindSubmit, nextDayDateTime} from '../util';
import {Student} from '../interfaces';
import TaskCtrl from '../ctrl';

export function renderFormModal(ctrl: TaskCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.closeTaskCreateModal();
    },
    class: `taskCreateModal`,
    content: [
      h('h2', '新建任务'),
      h('div.modal-content-body', [
        h('form.form3.taskForm', {
          hook: bindSubmit(_ => {
            ctrl.submitTask();
          })
        }, [
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.id', value: ctrl.opts.sourceRel.id}}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.name', value: ctrl.opts.sourceRel.name}}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.source', value: ctrl.opts.sourceRel.source}}),
          ctrl.opts.sourceRel.deadlineAt ? h('input', {attrs: {type: 'hidden', name: 'deadlineAt', value: ctrl.opts.sourceRel.deadlineAt}}) : null,
          h('div.form-split', [
            h('div.form-half', [
              h('div.form-group', [
                h('label.form-label', '选择学员'),
                h('div.form-control.student-scroll', [
                  h('ul', ctrl.students.map((student, i) => {
                    return h('li', [
                      h('input', {
                        attrs: {
                          type: 'checkbox',
                          name: `userIds[${i}]`,
                          id: `cbk_task_userId_${student.id}`,
                          value: student.id,
                          checked: (ctrl.studentId === student.id)
                        }
                      }),
                      h('label', {attrs: {for: `cbk_task_userId_${student.id}`}}, [userLink(student, false, false)]),
                    ])
                  }))
                ])
              ]),
              !!ctrl.opts.team && ctrl.opts.team.coinSetting.open ? h('div.form-group', [
                h('label.form-label', {attrs: {for: 'form-coinRule'}}, ctrl.opts.team.coinSetting.name),
                h('input#form-coinRule.form-control', { attrs: {name: 'coinRule', type: 'number', min: 0, max: ctrl.opts.team.coinSetting.singleVal, value: (ctrl.opts.task && ctrl.opts.task.coinRule ? ctrl.opts.task.coinRule : 0)}}),
                h('small.form-help', '完成本次任务所得积分，填0表示不记')
              ]) : null
            ]),
            h('div.form-half', [
              h('div.form-group', [
                h('label.form-label', {attrs: {for: 'form-itemType'}}, '任务类型'),
                h('select#form-itemType.form-control', {
                    hook: bind('change', (e: Event) => {
                      ctrl.changeItemType((e.target as HTMLInputElement).value);
                    }),
                    attrs: {name: 'itemType', required: true}
                  },
                  ctrl.itemTypes.map(function (itemType) {
                    return h('option', {
                      attrs: {
                        value: itemType.id,
                        selected: itemType.id === ctrl.itemType
                      }
                    }, `${itemType.name}${itemType.member ? '（会员）' : ''}`)
                  }))
              ]),
              h('div.form-group', [
                h('label.form-label', {attrs: {for: 'form-remark'}}, '附言'),
                h('textarea#form-remark.form-control', {
                  attrs: {name: 'remark', rows: 2, placeholder: '0-100个字'}
                }, `${ctrl.opts.task ? ctrl.opts!.task.remark : ''}`)
              ]),
              !ctrl.opts.sourceRel.deadlineAt ? h('div.form-group', [
                h('label.form-label', {attrs: {for: 'form-deadlineAt'}}, '截止时间'),
                h('input#form-deadlineAt.form-control.flatpickr', {
                  hook: {
                    insert(vnode) {
                      (<any> $(vnode.elm as HTMLElement)).flatpickr({
                        disableMobile: 'true',
                        enableTime: true,
                        time_24hr: true,
                        minDate: 'today',
                        maxDate: new Date(Date.now() + 1000 * 3600 * 24 * 30),
                      });
                    },
                  },
                  attrs: {name: 'deadlineAt', value: nextDayDateTime(), readonly: true}
                })
              ]) : null
            ])
          ]),
          h('div.form-group.tabs-horiz', [
            makeTab(ctrl, 'setting', '任务设置', '任务设置'),
            makeTab(ctrl, 'template', '模板', '模板'),
          ]),
          h('div.tabs-content', [
            settingContent(ctrl),
            templateContent(ctrl)
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                ctrl.closeTaskCreateModal();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

export function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  return h('span.' + key, {
    class: {active: ctrl.activeTab === key},
    attrs: {title},
    hook: bind('click', () => {
      ctrl.activeTab = key;
      ctrl.redraw();
      window.lichess.pubsub.emit('content_loaded');
    })
  }, name);
}

export function settingContent(ctrl: TaskCtrl) {
  return ctrl.activeTab === 'setting' ? h('div.task-setting.active', [
    ctrl.itemType === 'puzzle' ? renderPuzzleItem(ctrl) : null,
    ctrl.itemType === 'themePuzzle' ? renderThemePuzzleItem(ctrl) : null,
    ctrl.itemType === 'puzzleRush' ? renderPuzzleRushItem(ctrl) : null,
    ctrl.itemType === 'capsulePuzzle' ? renderCapsulePuzzleItem(ctrl) : null,
    ctrl.itemType === 'coordTrain' ? renderCoordTrainItem(ctrl) : null,
    ctrl.itemType === 'game' ? renderGameItem(ctrl) : null,
    ctrl.itemType === 'replayGame' ? renderReplayGameItem(ctrl) : null,
    ctrl.itemType === 'recallGame' ? renderRecallGameItem(ctrl) : null,
    ctrl.itemType === 'distinguishGame' ? renderDistinguishGameItem(ctrl) : null,
    ctrl.itemType === 'fromPosition' ? renderFromPositionGameItem(ctrl) : null,
    ctrl.itemType === 'fromPgn' ? renderFromPgnGameItem(ctrl) : null,
    ctrl.itemType === 'fromOpeningdb' ? renderFromOpeningdbGameItem(ctrl) : null,
  ]) : null
}

export function renderTaskName(ctrl: TaskCtrl) {
  return h('div.form-group', [
    h('label.form-label', {attrs: {for: 'form-name'}}, '任务名称'),
    h('input#form-name.form-control', {
      attrs: {name: 'name', required: true, value: ctrl.taskName}
    })
  ]);
}

export function templateContent(ctrl: TaskCtrl) {
  return ctrl.activeTab === 'template' ? h('div.task-template.active', '暂不支持') : null
}

export function userLink(u: Student, link: boolean = false, showBadges: boolean = true): VNode {
  const baseUrl = $('body').data('asset-url');
  const head = u.head ? '/image/' + u.head : baseUrl + '/assets/images/head-default-64.png';

  return h(link ? 'a.user-link.ulpt' : 'span.user-link', {
    attrs: { href: `/@/${u.name}`},
    class: { online: u.online, offline: !u.online }
  }, [
    h('div.head-line', [
      h('img.head', { attrs: { src: head }}),
      h('i.line')
    ]),
    u.title ? h('span.title', u.title == 'BOT' ? { attrs: { 'data-bot': true } } : {}, u.title) : null,
    h('span.u_name', u.mark ? u.mark : u.name),
    showBadges ? h('span.badges', [
      u.member && u.member !== 'general' ? h('img.badge', { title: (u.member === 'gold' ? '金牌会员': '银牌会员'), attrs: { src: baseUrl + `/assets/images/icons/${u.member}.svg` }}) : null,
      u.coach ? h('img.badge', { attrs: { title: '认证教练', src: baseUrl + `/assets/images/icons/coach.svg` }}) : null,
      u.team ? h('img.badge', { attrs: { title: '认证俱乐部', src: baseUrl + `/assets/images/icons/team.svg` }}) : null
    ]) : null
  ]);
}
