import {h} from 'snabbdom';
import {renderTaskName} from './form'
import TaskCtrl from '../ctrl';
import {bind, bindSubmit} from '../util';
import {FromOpeningdb, FromOpeningdbWithResult} from '../interfaces';
import * as ModalBuild from './modal';

export function fromOpeningdbItem(ctrl: TaskCtrl) {
  const fromOpeningdbGameCtrl = ctrl.fromOpeningdbGameCtrl;

  return h('div.item-fromOpeningdb', [
    renderTaskName(ctrl),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-fromOpeningdbs'}}, '已选对局'),
      !fromOpeningdbGameCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          fromOpeningdbGameCtrl.addFromOpeningdbGame();
        })
      }, '添加对局') : null,
      renderFromOpeningdbs(ctrl, fromOpeningdbGameCtrl.fromOpeningdbGames)
    ])
  ])
}

export function renderFromOpeningdbs(ctrl: TaskCtrl, fromOpeningdbs: FromOpeningdbWithResult[], viewOnly: boolean = false) {
  let fromOpeningdbGameCtrl = ctrl.fromOpeningdbGameCtrl;
  return h('div.fromOpeningdbGames', fromOpeningdbs.map((frwr, index) => {
    let fromOpeningdb = frwr.fromOpeningdb;
    let initialPgn = fromOpeningdb.initialPgn;
    let opening = initialPgn.opening;

    return h(`div.fromOpeningdb.${index}`, [
      h('table.tb-fp', [
        h('tbody', [
          h('tr', [
            h('td'),
            h('td.select', [
              !viewOnly ? (
                !opening.id ? h('a.button.flat.small', {
                  hook: bind('click', () => {
                    fromOpeningdbGameCtrl.onOpeningdbSelectModalOpen(fromOpeningdb, index);
                  })
                }, '选择开局库') : h('a.flat', {
                  hook: bind('click', () => {
                    fromOpeningdbGameCtrl.onOpeningdbSelectModalOpen(fromOpeningdb, index);
                  })
                }, opening.name)
              ) : null,
              !viewOnly ? h('a.button.button-empty.button-red.small', {
                hook: bind('click', () => {
                  fromOpeningdbGameCtrl.removeFromOpeningdbGame(index);
                })
              }, '移除') : null,
              viewOnly ? h('a.flat', {
                attrs: { target: '_blank', href: `/resource/openingdb/${opening.id}` }
              }, opening.name) : null,
              h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbId`, value: `${opening.id}`  } }),
            ])
          ]),
          opening.id ? h('tr', [
            h('td', '着法设置'),
            h('td', [
              h('div', [
                h('div', [
                  h('label', '白方：'),
                  h('span', [
                    initialPgn.openingdbExcludeWhiteBlunder ? '排除错误着法' : '不排除错误着法',
                    '、',
                    initialPgn.openingdbExcludeWhiteJscx ? '排除少见着法' : '不排除少见着法',
                    h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbExcludeWhiteBlunder`, value: `${initialPgn.openingdbExcludeWhiteBlunder!}`  } }),
                    h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbExcludeWhiteJscx`, value: `${initialPgn.openingdbExcludeWhiteJscx!}` } })
                  ])
                ]),
                h('div', [
                  h('label', '黑方：'),
                  h('span', [
                    initialPgn.openingdbExcludeBlackBlunder ? '排除错误着法' : '不排除错误着法',
                    '、',
                    initialPgn.openingdbExcludeBlackJscx ? '排除少见着法' : '不排除少见着法',
                    h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbExcludeBlackBlunder`, value: `${initialPgn.openingdbExcludeBlackBlunder!}` } }),
                    h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbExcludeBlackJscx`, value: `${initialPgn.openingdbExcludeBlackJscx!}` } })
                  ])
                ]),
                h('div', [
                  h('label', '可以提前脱谱：'),
                  h('span', initialPgn.openingdbCanOff ? '是' : '否'),
                  h('input', { attrs: { type: 'hidden', name: `item.fromOpeningdb[${index}].openingdbCanOff`, value: `${initialPgn.openingdbCanOff!}` } })
                ])
              ])
            ])
          ]) : null,
          h('tr', [
            h('td', '基本用时'),
            h('td', [
              h('select', {attrs: {name: `item.fromOpeningdb[${index}].clockTime`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'clock', 'initial', v);
                })}, [
                h('option', {attrs: {value: '3.0', selected: fromOpeningdb.clock.initial === 180}}, '3 分钟'),
                h('option', {attrs: {value: '5.0', selected: fromOpeningdb.clock.initial === 300}}, '5 分钟'),
                h('option', {attrs: {value: '10.0', selected: fromOpeningdb.clock.initial === 600}}, '10 分钟'),
                h('option', {attrs: {value: '15.0', selected: fromOpeningdb.clock.initial === 900}}, '15 分钟'),
                h('option', {attrs: {value: '20.0', selected: fromOpeningdb.clock.initial === 1200}}, '20 分钟'),
                h('option', {attrs: {value: '25.0', selected: fromOpeningdb.clock.initial === 1500}}, '25 分钟'),
                h('option', {attrs: {value: '30.0', selected: fromOpeningdb.clock.initial === 1800}}, '30 分钟'),
                h('option', {attrs: {value: '40.0', selected: fromOpeningdb.clock.initial === 2400}}, '40 分钟'),
                h('option', {attrs: {value: '50.0', selected: fromOpeningdb.clock.initial === 3000}}, '50 分钟'),
                h('option', {attrs: {value: '60.0', selected: fromOpeningdb.clock.initial === 3600}}, '60 分钟')
              ])
            ])
          ]),
          h('tr', [
            h('td', '每步棋加时'),
            h('td', [
              h('select', {attrs: {name: `item.fromOpeningdb[${index}].clockIncrement`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'clock', 'increment', v);
                })}, [
                h('option', {attrs: {value: '0', selected: fromOpeningdb.clock.increment === 0}}, '0 秒'),
                h('option', {attrs: {value: '1', selected: fromOpeningdb.clock.increment === 1}}, '1 秒'),
                h('option', {attrs: {value: '2', selected: fromOpeningdb.clock.increment === 2}}, '2 秒'),
                h('option', {attrs: {value: '3', selected: fromOpeningdb.clock.increment === 3}}, '3 秒'),
                h('option', {attrs: {value: '5', selected: fromOpeningdb.clock.increment === 5}}, '5 秒'),
                h('option', {attrs: {value: '10', selected: fromOpeningdb.clock.increment === 10}}, '10 秒'),
                h('option', {attrs: {value: '15', selected: fromOpeningdb.clock.increment === 15}}, '15 秒'),
                h('option', {attrs: {value: '20', selected: fromOpeningdb.clock.increment === 20}}, '20 秒'),
                h('option', {attrs: {value: '25', selected: fromOpeningdb.clock.increment === 25}}, '25 秒'),
                h('option', {attrs: {value: '30', selected: fromOpeningdb.clock.increment === 30}}, '30 秒'),
                h('option', {attrs: {value: '40', selected: fromOpeningdb.clock.increment === 40}}, '40 秒'),
                h('option', {attrs: {value: '50', selected: fromOpeningdb.clock.increment === 50}}, '50 秒'),
                h('option', {attrs: {value: '60', selected: fromOpeningdb.clock.increment === 60}}, '60 秒')
              ])
            ])
          ]),
          h('tr', [
            h('td', '棋色'),
            h('td', [
              h('select', {attrs: {name: `item.fromOpeningdb[${index}].color`, disabled: viewOnly},
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'color', null, v);
                })}, [
                h('option', {attrs: {value: 'random', selected: fromOpeningdb.color === 'random'}}, '随机'),
                h('option', {attrs: {value: 'white', selected: fromOpeningdb.color === 'white'}}, '白'),
                h('option', {attrs: {value: 'black', selected: fromOpeningdb.color === 'black'}}, '黑')
              ])
            ])
          ]),
          h('tr.none', [
            h('td', '允许人机对弈'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'isAi', null, (v == "1"));
                }),
                attrs: {name: `item.fromOpeningdb[${index}].isAi`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromOpeningdb.isAi}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromOpeningdb.isAi}}, '否')
              ])
            ])
          ]),
          h('tr', {class: { none: !fromOpeningdb.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromOpeningdbGameCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '引擎级别'),
            h('td', [
              h('select', {attrs: {name: `item.fromOpeningdb[${index}].aiLevel`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromOpeningdb.aiLevel === 1}}, 'A.I.1（600 分）'),
                h('option', {attrs: {value: '2', selected: fromOpeningdb.aiLevel === 2}}, 'A.I.2（800 分）'),
                h('option', {attrs: {value: '3', selected: fromOpeningdb.aiLevel === 3}}, 'A.I.3（1000 分）'),
                h('option', {attrs: {value: '4', selected: fromOpeningdb.aiLevel === 4}}, 'A.I.4（1200 分）'),
                h('option', {attrs: {value: '5', selected: fromOpeningdb.aiLevel === 5}}, 'A.I.5（1400 分）'),
                h('option', {attrs: {value: '6', selected: fromOpeningdb.aiLevel === 6}}, 'A.I.6（1600 分）'),
                h('option', {attrs: {value: '7', selected: fromOpeningdb.aiLevel === 7}}, 'A.I.7（1800 分）'),
                h('option', {attrs: {value: '8', selected: fromOpeningdb.aiLevel === 8}}, 'A.I.8（2000 分）'),
                h('option', {attrs: {value: '9', selected: fromOpeningdb.aiLevel === 9}}, 'A.I.9（2500 分）'),
                h('option', {attrs: {value: '10', selected: fromOpeningdb.aiLevel === 10}}, 'A.I.10（3000 分）'),
              ])
            ])
          ]),
          h('tr', [
            h('td', '允许悔棋'),
            h('td', [
              h('select', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'canTakeback', null, (v == "1"));
                }),
                attrs: {name: `item.fromOpeningdb[${index}].canTakeback`, disabled: viewOnly}}, [
                h('option', {attrs: {value: '1', selected: fromOpeningdb.canTakeback}}, '是'),
                h('option', {attrs: {value: '0', selected: !fromOpeningdb.canTakeback}}, '否')
              ])
            ])
          ]),
          h('tr', [
            h('td', '对局数'),
            h('td', [
              h('input', {
                hook: bind('change', (e) => {
                  let v = (e.target as HTMLInputElement).value;
                  fromOpeningdbGameCtrl.changeValue(index, 'num', null, parseInt(v));
                }),
                attrs: {type: 'number', name: `item.fromOpeningdb[${index}].num`, value: fromOpeningdb.num, min: 1, max: 20, required: true, disabled: viewOnly}
              })
            ])
          ]),
          h('tr', {class: { none: !fromOpeningdb.isAi },
            hook: bind('change', (e) => {
              let v = (e.target as HTMLInputElement).value;
              fromOpeningdbGameCtrl.changeValue(index, 'aiLevel', null, parseInt(v));
            })}, [
            h('td', '对局结果'),
            h('td', [
              h('select', {attrs: {name: `item.fromOpeningdb[${index}].chessStatus`, disabled: viewOnly}}, [
                h('option', {attrs: {value: 'all', selected: fromOpeningdb.chessStatus === 'all'}}, '不限定'),
                h('option', {attrs: {value: 'win', selected: fromOpeningdb.chessStatus === 'win'}}, '胜'),
                h('option', {attrs: {value: 'winOrDraw', selected: fromOpeningdb.chessStatus === 'winOrDraw'}}, '胜或和')
              ])
            ])
          ])
        ])
      ])
    ]);
  }))
}

export function fromOpeningdbSelectModal(ctrl: TaskCtrl) {
  const fromOpeningdbGameCtrl = ctrl.fromOpeningdbGameCtrl;
  const selectFromOpeningdb = fromOpeningdbGameCtrl.selectFromOpeningdb;
  return ModalBuild.modal({
    onClose: () => {
      fromOpeningdbGameCtrl.onOpeningdbSelectModalClose();
    },
    class: `fromOpeningdbSelectModal`,
    content: [
      h('h2', '选择开局库'),
      h('div.modal-content-body', [
        h('form.fromOpeningdbSelectForm', {
          hook: bindSubmit(_ => {
            fromOpeningdbGameCtrl.submitOpeningdbSelect();
          })
        }, [
          h('div', [
            h('div.tabs-horiz', [
              makeTab(ctrl, 'mine', '我的', '我的'),
              makeTab(ctrl, 'member', '参与的', '参与的'),
              makeTab(ctrl, 'visible', '已授权', '已授权'),
              makeTab(ctrl, 'system', '已购', '已购')
            ]),
            h('div.tabs-content', [
              renderOpeningdbs(ctrl, 'mine', selectFromOpeningdb),
              renderOpeningdbs(ctrl, 'member', selectFromOpeningdb),
              renderOpeningdbs(ctrl, 'visible', selectFromOpeningdb),
              renderOpeningdbs(ctrl, 'system', selectFromOpeningdb)
            ])
          ]),
          h('div.fromOpeningdb-setting', [
            h('table', [
              h('tbody', [
                h('tr', [
                  h('td', '排除错误着法'),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'checkbox',
                          id: 'openingdbExcludeWhiteBlunder-true',
                          name: 'openingdbExcludeWhiteBlunder',
                          checked: !!selectFromOpeningdb.initialPgn.openingdbExcludeWhiteBlunder
                        }
                      }),
                      h('label', { attrs: { 'for': 'openingdbExcludeWhiteBlunder-true' } }, '白方')
                    ]),
                  ]),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'checkbox',
                          id: 'openingdbExcludeBlackBlunder-false',
                          name: 'openingdbExcludeBlackBlunder',
                          checked: !!selectFromOpeningdb.initialPgn.openingdbExcludeBlackBlunder
                        }
                      }),
                      h('label', { attrs: { 'for': 'openingdbExcludeBlackBlunder-false' } }, '黑方')
                    ]),
                  ])
                ]),
                h('tr', [
                  h('td', '排除少见着法'),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'checkbox',
                          id: 'openingdbExcludeWhiteJscx-true',
                          name: 'openingdbExcludeWhiteJscx',
                          checked: !!selectFromOpeningdb.initialPgn.openingdbExcludeWhiteJscx
                        }
                      }),
                      h('label', { attrs: { 'for': 'openingdbExcludeWhiteJscx-true' } }, '白方')
                    ]),
                  ]),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'checkbox',
                          id: 'openingdbExcludeBlackJscx-false',
                          name: 'openingdbExcludeBlackJscx',
                          checked: !!selectFromOpeningdb.initialPgn.openingdbExcludeBlackJscx
                        },
                      }),
                      h('label', { attrs: { 'for': 'openingdbExcludeBlackJscx-false' } }, '黑方')
                    ]),
                  ])
                ]),
                h('tr', [
                  h('td', '可以提前脱谱'),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'radio', name: 'openingdbCanOff', id: 'openingdbCanOff-true', value: 'true', checked: !!selectFromOpeningdb.initialPgn.openingdbCanOff
                        }
                      }),
                      h('label', { attrs: { for: 'openingdbCanOff-true' }}, '是')
                    ])
                  ]),
                  h('td', [
                    h('div', [
                      h('input', {
                        attrs: {
                          type: 'radio', name: 'openingdbCanOff', id: 'openingdbCanOff-false', value: 'false', checked: !selectFromOpeningdb.initialPgn.openingdbCanOff
                        }
                      }),
                      h('label', { attrs: { for: 'openingdbCanOff-false' }}, '否')
                    ])
                  ])
                ])
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                fromOpeningdbGameCtrl.onOpeningdbSelectModalClose();
              })
            },'取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function noMore(capsuleTab) {
  return capsuleTab === 'mine' ? h('div.no-more', [
    '您还没有创建开局库，现在就去', h('a', { attrs: { href: '/resource/openingdb/mine' } }, '创建'), '吧'
  ]) : h('div.no-more', '没有可用开局库');
}

function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  const fromOpeningdbGameCtrl = ctrl.fromOpeningdbGameCtrl;
  return h('span.' + key, {
    class: { active: fromOpeningdbGameCtrl.activeFromOpeningdbSelectTab === key },
    attrs: { title },
    hook: bind('click', () => {
      fromOpeningdbGameCtrl.activeFromOpeningdbSelectTab = key;
      ctrl.redraw();
    })
  }, name);
}

function renderOpeningdbs(ctrl: TaskCtrl, tab: string, selectFromOpeningdb: FromOpeningdb) {
  const fromOpeningdbGameCtrl = ctrl.fromOpeningdbGameCtrl;
  return h(`div.content`, {
    class: {
      active: fromOpeningdbGameCtrl.activeFromOpeningdbSelectTab === tab
    }
  }, [
    h('div.fromOpeningdb-scroll', [
      fromOpeningdbGameCtrl.openingdbs[tab].length === 0 ? noMore(tab) : h('table.fromOpeningdb-list', [
        h('tbody', fromOpeningdbGameCtrl.openingdbs[tab].map(openingdb => {
          const checked = selectFromOpeningdb.initialPgn.opening.id === openingdb.id;
          return h('tr', [
            h('td', [
              h('div', [
                h('input', {
                  attrs: {
                    type: 'radio', name: 'openingdbId', id: `openingdbId-${openingdb.id}`, value: openingdb.id, checked: checked
                  },
                  hook: bind('click', _ => {
                  })
                }),
                h('label', { attrs: { for: `openingdbId-${openingdb.id}` }}, openingdb.name)
              ])
            ]),
            h('td', openingdb.nodes)
          ]);
        }))
      ])
    ]),
  ])
}
