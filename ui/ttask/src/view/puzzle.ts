import {h} from 'snabbdom';
import TaskCtrl from '../ctrl';
import numOrRatingContent from './contentNumOrRating';
import {renderTaskName} from './form';

export function puzzleItem(ctrl: TaskCtrl) {
  return h('div.item-puzzle', [
    renderTaskName(ctrl),
    numOrRatingContent(ctrl)
  ]);
}
