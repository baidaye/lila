import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {bind, bindSubmit, getFormVariable} from '../util';
import {renderTaskName} from './form';
import * as ModalBuild from './modal';
import TaskCtrl from '../ctrl';

export function puzzleRushItem(ctrl: TaskCtrl) {
  const puzzleRushCtrl = ctrl.puzzleRushCtrl;
  const cond = puzzleRushCtrl.puzzleRush.extra ? puzzleRushCtrl.puzzleRush.extra.cond : '';
  const tags = puzzleRushCtrl.puzzleRush.tags ? puzzleRushCtrl.puzzleRush.tags : [];

  const modes = [
    {
      'k': 'threeMinutes',
      'v': '3分钟'
    },
    {
      'k': 'fiveMinutes',
      'v': '5分钟'
    },
    {
      'k': 'survival',
      'v': '生存'
    },
    {
      'k': 'custom',
      'v': '自定义'
    }
  ];

  let puzzleRushMode = (mode) => {
    return h('span', [
      h('input', {
        attrs: {
          id: `rushMode-${mode.k}`,
          name: `item.puzzleRush.mode`,
          type: 'radio',
          value: mode.k,
          checked: mode.k === puzzleRushCtrl.puzzleRush.mode
        },
        hook: bind('click', _ => {
          puzzleRushCtrl.puzzleRush.mode = mode.k;
          ctrl.redraw();
        })
      }),
      h('label', {attrs: {for: `rushMode-${mode.k}`}}, mode.v)
    ])
  };

  return h('div.item-puzzleRush', [
    renderTaskName(ctrl),
    h('input', {attrs: {type: 'hidden', name: 'item.puzzleRush.isNumber', value: 'true'}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-num'}}, '目标值'),
      h(`input#form-num.form-control.${ctrl.itemType}-${ctrl.num}`, {
        hook: {
          insert(vnode) {
            const $el = $(vnode.elm as HTMLElement);
            $el.val(ctrl.num).on('change keyup paste', () => {
              ctrl.num = $el.val();
              ctrl.setTaskName();
            })
          }
        },
        attrs: {type: 'number', name: 'item.puzzleRush.num', value: 1, min: 1, max: 3000, required: true}
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-mode'}}, '战术模式'),
      h('div.form-control.radio-group', modes.map(mode => {
        return puzzleRushMode(mode);
      }))
    ]),
    puzzleRushCtrl.puzzleRush.mode === 'custom' ? h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-cond'}}, '条件设置'),
      h('div.cond', [
        h('div.form-control.selected', {
          class: {
            has: tags.length > 0
          },
          hook: bind('click', () => {
            puzzleRushCtrl.onCustomPuzzleRushModalOpen();
          })
        }, [
          h('div.tags', tags.map(t => {
            return h('span', t);
          })),
          h('input', {
            attrs: {
              type: 'hidden',
              id: 'extraCond',
              name: 'item.puzzleRush.extra.cond',
              value: cond
            }
          })
        ])
      ])
    ]) : null
  ]);
}

export function customPuzzleRushModal(ctrl: TaskCtrl): VNode {
  const puzzleRushCtrl = ctrl.puzzleRushCtrl;
  const cond = puzzleRushCtrl.puzzleRush.extra ? puzzleRushCtrl.puzzleRush.extra.cond : '';
  //const tags = puzzleRushCtrl.puzzleRush.tags ? puzzleRushCtrl.puzzleRush.tags : [];

  const ratingMin = getFormVariable(cond, 'ratingMin', '600');
  const ratingMax = getFormVariable(cond, 'ratingMax', '1500');
  const stepsMin = getFormVariable(cond, 'stepsMin', '1');
  const stepsMax = getFormVariable(cond, 'stepsMax', '3');
  const color = getFormVariable(cond, 'color');
  const phase = getFormVariable(cond, 'phase');
  const selector = getFormVariable(cond, 'selector');
  const minutes = getFormVariable(cond, 'minutes', '3');
  const limit = getFormVariable(cond, 'limit', '3');

  return ModalBuild.modal({
    onClose: () => {
      puzzleRushCtrl.onCustomPuzzleRushModalClose();
    },
    class: `customPuzzleRushModal`,
    content: [
      h('h2', '战术冲刺条件设置'),
      h('div.modal-content-body', [
        h('form.search_form', {
          hook: bindSubmit(_ => {
            puzzleRushCtrl.submitCustomPuzzleRush();
          })
        }, [
          h('table', [
            h('tbody', [
              h('tr', [
                h('th', [
                  h('label', '难度范围')
                ]),
                h('td', [
                  h('div.half', [
                    '从',
                    h('input.form-control', {
                      attrs: {
                        id: 'form-ratingMin',
                        name: 'ratingMin',
                        type: 'number',
                        required: true,
                        min: 500,
                        max: 2600,
                        value: ratingMin,
                        placeholder: '500 ~ 2600'
                      }
                    })
                  ]),
                  h('div.half', [
                    '到',
                    h('input.form-control', {
                      attrs: {
                        id: 'form-ratingMax',
                        name: 'ratingMax',
                        type: 'number',
                        required: true,
                        min: 500,
                        max: 2600,
                        value: ratingMax,
                        placeholder: '500 ~ 2600'
                      }
                    })
                  ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', '答案步数')
                ]),
                h('td', [
                  h('div.half', [
                    '从',
                    h('input.form-control', {
                      attrs: {
                        id: 'form-stepsMin',
                        name: 'stepsMin',
                        type: 'number',
                        required: true,
                        min: 1,
                        max: 10,
                        value: stepsMin,
                        placeholder: '1 ~ 10'
                      }
                    })
                  ]),
                  h('div.half', [
                    '到',
                    h('input.form-control', {
                      attrs: {
                        id: 'form-stepsMax',
                        name: 'stepsMax',
                        type: 'number',
                        required: true,
                        min: 1,
                        max: 10,
                        value: stepsMax,
                        placeholder: '1 ~ 10'
                      }
                    })
                  ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-color'}}, '棋色'),
                ]),
                h('td', [
                  h('select#form-color.form-control', {attrs: {name: 'color'}},
                    [
                      h('option', {attrs: {value: ''}}, '全部'),
                      h('option', {attrs: {value: 'White', selected: color == 'White'}}, '白方'),
                      h('option', {attrs: {value: 'Black', selected: color == 'Black'}}, '黑方')
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-phase'}}, '阶段'),
                ]),
                h('td', [
                  h('select#form-phase.form-control', {attrs: {name: 'phase'}},
                    [
                      h('option', {attrs: {value: ''}}, '全部'),
                      h('option', {attrs: {value: 'Opening', selected: phase == 'Opening'}}, '开局'),
                      h('option', {attrs: {value: 'MiddleGame', selected: phase == 'MiddleGame'}}, '中局'),
                      h('option', {attrs: {value: 'EndingGame', selected: phase == 'EndingGame'}}, '残局'),
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-selector'}}, '出题模式'),
                ]),
                h('td', [
                  h('select#form-selector.form-control', {attrs: {name: 'selector'}},
                    [
                      h('option', {attrs: {value: 'round', selected: selector == 'round'}}, '循环'),
                      h('option', {attrs: {value: 'random', selected: selector == 'random'}}, '随机'),
                    ])
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-minutes'}}, '练习时间（分钟）'),
                ]),
                h('td', [
                  h('input.form-control', {
                    attrs: {
                      id: 'form-minutes',
                      name: 'minutes',
                      type: 'number',
                      required: true,
                      min: 1,
                      max: 30,
                      value: minutes
                    }
                  })
                ])
              ]),
              h('tr', [
                h('th', [
                  h('label', {attrs: {for: 'form-limit'}}, '错题上限（个）'),
                ]),
                h('td', [
                  h('input.form-control', {
                    attrs: {
                      id: 'form-limit',
                      name: 'limit',
                      type: 'number',
                      required: true,
                      min: 1,
                      max: 20,
                      value: limit
                    }
                  })
                ])
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                puzzleRushCtrl.onCustomPuzzleRushModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}
