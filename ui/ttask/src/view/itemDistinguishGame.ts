import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {Chessground} from 'chessground';
import {bind, bindSubmit} from '../util';
import {Move, DistinguishGame, DistinguishGameWithResult} from '../interfaces';
import {renderTaskName} from './form'
import * as ModalBuild from './modal';
import renderStudyContent from './contentStudy'
import renderGamedbContent from './contentGamedb'
import renderCourseWareContent from './contentCourseWare';
import TaskCtrl from '../ctrl';


export function distinguishGameItem(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return h('div.item-distinguishGame', [
    renderTaskName(ctrl),
    h(`input.${ctrl.itemType}-${ctrl.num}`, {attrs: {type: 'hidden', value: ctrl.num}}),
    h('div.form-group', [
      h('label.form-label', {attrs: {for: 'form-distinguishGames'}}, '已选棋谱'),
      !distinguishGameCtrl.isFull() ? h('a.button.button-empty.small', {
        hook: bind('click', () => {
          distinguishGameCtrl.onDistinguishGameModalOpen();
        })
      },'选择棋谱') : null,
      renderDistinguishGames(ctrl, distinguishGameCtrl.distinguishGames)
    ])
  ])
}

export function renderDistinguishGames(ctrl: TaskCtrl, distinguishGames: DistinguishGameWithResult[], viewOnly: boolean = false) {
  return h('div.distinguishGames', distinguishGames.map((dgwr, index) => {
    let distinguishGame = dgwr.distinguishGame;
    let orientName = function () {
      if(distinguishGame.orientation === 'white') return '白方';
      else if(distinguishGame.orientation === 'black') return '黑方';
      else return '-'
    };

    let turnsName = function () {
      if(!distinguishGame.turns) return '所有';
      else return distinguishGame.turns;
    };

    return h(`div.distinguishGame.${index}`, [
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].title`, value: distinguishGame.title}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].orientation`, value: distinguishGame.orientation}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].rightTurns`, value: distinguishGame.rightTurns ? distinguishGame.rightTurns : ''}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].turns`, value: distinguishGame.turns ? distinguishGame.turns : ''}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].name`, value: distinguishGame.name}}) : null,
      !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].root`, value: distinguishGame.fen}}) : null,
      !viewOnly ? h('textarea.none', { attrs: { name: `item.distinguishGame[${index}].pgn`}}, distinguishGame.pgn) : null,
      h('div.rg-header', [
        h('div.left', [
          h('div.title', distinguishGame.name),
          h('div.meta', [
            h('span', `棋盘方向：${orientName()}`),
            h('span', `走棋步数：${ distinguishGame.rightTurns + '/' + turnsName()}`)
          ])
        ]),
        !viewOnly ? h('a.button.button-empty.button-red.small', {
          hook: bind('click', () => {
            ctrl.distinguishGameCtrl.removeDistinguishGame(index);
          })
        },'移除') : null
      ]),
      h('div.rg-body', [
        h('div.board', [
          h('div.mini-board.cg-wrap.is2d', {
            hook: {
              insert(vnode) {
                distinguishGame.el = vnode.elm as HTMLElement;
                makeChess(distinguishGame);
              }
            }
          })
        ]),
        h('div.moves', distinguishGame.moves.map((move, moveIndex) => {
          return h('move', [
            h('index', `${ move.index}.`),
            h('span', {
              class: {active: (!!move.white && move.white.active), disabled: !move.white},
              attrs: {'data-fen': move.white ? move.white.fen : ''},
              hook: bind('click', () => {
                if(move.white) {
                  distinguishGame.moves.forEach(m => {
                    if(m.white && m.white.active) m.white.active = false;
                    if(m.black && m.black.active) m.black.active = false;
                  });
                  move.white.active = true;
                  makeChess(distinguishGame, move, 'white');
                  ctrl.redraw();
                }
              })
            }, move.white ? move.white.san : '...'),
            h('span', {
              class: {active: (!!move.black && move.black!.active), disabled: !move.black},
              attrs: {'data-fen': move.black ? move.black.fen : ''},
              hook: bind('click', () => {
                if(move.black) {
                  distinguishGame.moves.forEach(m => {
                    if(m.white && m.white.active) m.white.active = false;
                    if(m.black && m.black.active) m.black.active = false;
                  });
                  move.black.active = true;
                  makeChess(distinguishGame, move, 'black');
                  ctrl.redraw();
                }
              })
            }, move.black ? move.black.san : ''),
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].index`, value: move.index}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].white.san`, value: move.white ? move.white.san : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].white.uci`, value: move.white ? move.white.uci : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].white.fen`, value: move.white ? move.white.fen : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].black.san`, value: move.black ? move.black.san : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].black.uci`, value: move.black ? move.black.uci : ''}}) : null,
            !viewOnly ? h('input', {attrs: {type: 'hidden', name: `item.distinguishGame[${index}].moves[${moveIndex}].black.fen`, value: move.black ? move.black.fen : ''}}) : null,
          ])
        }))
      ])
    ]);
  }))
}

function makeChess(distinguishGame: DistinguishGame, move: Move | undefined = undefined, orientation: string | undefined = undefined) {
  Chessground(distinguishGame.el, {
    coordinates: false,
    drawable: { enabled: false, visible: false },
    resizable: false,
    viewOnly: true,
    orientation: distinguishGame.orientation as Color,
    fen: move ? (move && orientation && move[orientation] ? move[orientation].fen : '') : distinguishGame.fen
  });
}

export function distinguishGameModal(ctrl: TaskCtrl): VNode {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return ModalBuild.modal({
    onClose: function() {
      distinguishGameCtrl.onDistinguishGameModalClose();
    },
    class: `distinguishGameModal`,
    content: [
      h('h2', '棋谱记录'),
      h('div.modal-content-body', [
        h('form.form3.distinguishGameForm', {
          hook: bindSubmit(_ => {
            distinguishGameCtrl.submitDistinguishGame();
          })
        }, [
          h('input', {attrs: {type: 'hidden', name: 'name', value: 'nil'}}),
          h('div.form-split', [
            h('div.form-group.form-half', [
              h('label.form-label', {attrs: {for: 'form-rightTurns'}}, '正确步数'),
              h('input.form-control', {
                attrs: {
                  id: 'form-rightTurns',
                  name: 'rightTurns',
                  type: 'number',
                  min: 1,
                  max: 500,
                  required: true
                }
              })
            ]),
            h('div.form-group.form-half', [
              h('label.form-label', {attrs: {for: 'form-turns'}}, '走棋步数'),
              h('input.form-control', {
                attrs: {
                  id: 'form-turns',
                  name: 'turns',
                  type: 'number',
                  min: 1,
                  max: 500,
                  placeholder: '不填写表示所有走棋'
                }
              })
            ])
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: 'form-orientation'}}, '棋盘方向'),
            h('select#form-orientation.form-control', {attrs: {name: 'orientation'}}, [
              h('option', { attrs: { value: 'white' } }, '白方'),
              h('option', { attrs: { value: 'black' } }, '黑方')
            ])
          ]),
          h('div.tabs-horiz', [
            makeTab(ctrl, 'classic', '经典对局', '经典对局'),
            makeTab(ctrl, 'study', '研习章节', '研习章节'),
            ctrl.hasCourseWare() ? makeTab(ctrl, 'courseWare', '课件', '课件') : null,
            makeTab(ctrl, 'gamedb', '选择对局', '选择对局'),
            makeTab(ctrl, 'game', '对局链接', '对局链接'),
            makeTab(ctrl, 'pgn', 'PGN', 'PGN')
          ]),
          h('div.tabs-content',[
            classicContent(ctrl),
            studyContent(ctrl),
            ctrl.hasCourseWare() ? courseWareContent(ctrl) : null,
            gamedbContent(ctrl),
            gameContent(ctrl),
            pgnContent(ctrl)
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                distinguishGameCtrl.onDistinguishGameModalClose();
              })
            },'取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function makeTab(ctrl: TaskCtrl, key: string, name: string, title: string) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return h('span.' + key, {
    class: { active: distinguishGameCtrl.activeTab === key },
    attrs: { title },
    hook: bind('click', () => {
      distinguishGameCtrl.activeTab = key;
      ctrl.redraw();
    })
  }, name);
}

function classicContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'classic' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'classic' } }),
    h('div.content-classic', [
      h('table.games', [
        h('tbody.games', ctrl.opts.classicGames.map((cg, index) => {
          return h('tr', [
            h('td', `${index + 1}.`),
            h('td', [
              h('label', {attrs: {for: `rd-cg-${cg.id}`}}, ` ${cg.name}`)
            ]),
            h('td', h('input', {attrs: {type: 'radio', id: `rd-cg-${cg.id}`, name: `classic`, value: cg.id}}))
          ])
        }))
      ])
    ])
  ]) : null
}

function studyContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'study' ? renderStudyContent(ctrl) : null;
}

function courseWareContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'courseWare' ? renderCourseWareContent(ctrl) : null;
}

function gamedbContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'gamedb' ? renderGamedbContent(ctrl) : null;
}

function gameContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'game' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'game' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-game' }
      }, '对局URL'),
      h('input#tab-form-game.form-control', {
        attrs: { name: 'game', required: true, placeholder: '对局URL' },
      }),
      h('small.form-help', [
        h('div', '例：https://haichess.com/XTtL9RRY'),
        h('div', '或：https://haichess.com/XTtL9RRY/white')
      ]),
    ])
  ]) : null
}

function pgnContent(ctrl: TaskCtrl) {
  const distinguishGameCtrl = ctrl.distinguishGameCtrl;
  return distinguishGameCtrl.activeTab === 'pgn' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'tab', value: 'pgn' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn' }
      }, '请输入PGN'),
      h('textarea#tab-form-pgn.form-control', {
        attrs: { name: 'pgn', rows: 5, required: true, placeholder: '粘贴PGN文本' }
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn-import' }
      }, '上传PGN文件'),
      h('input#tab-form-pgn-import.form-control', {
        attrs: { type: 'file', name: 'pgnFile', accept: '.pgn' },
        hook: {
          insert(vnode) {
            (vnode.elm as HTMLInputElement).addEventListener('change', function() {
              // @ts-ignore
              let file = this.files[0];
              if (!file) return;
              let reader = new FileReader();
              reader.onload = function(e) {
                // @ts-ignore
                $('#tab-form-pgn').val(e.target.result);
              };
              reader.readAsText(file);
            });
          }
        }
      })
    ])
  ]) : null
}

