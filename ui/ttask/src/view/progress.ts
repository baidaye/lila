import {h} from 'snabbdom';
import ProgressCtrl from '../progressCtrl';
import {dataIcon} from '../util';

export default function (ctrl: ProgressCtrl) {
  return ctrl.tasks.length ? h('div#modal-wrap.task-progress', {
    class: {
      minimize: ctrl.minimize,
      maximize: !ctrl.minimize
    },
    hook: {
      insert(vnode) {
        let elm = (vnode.elm as HTMLElement);
        ctrl.initPopupBox(elm);
      }
    }
  }, ctrl.minimize ? minimizeBox(ctrl) : maximizeBox(ctrl)) : null;
}

function minimizeBox(ctrl: ProgressCtrl) {
  return [
    h('div.modal-header', [
      h('span.maximize', {
        attrs: {'data-icon': '左', title: '展开'},
        hook: {
          insert(vnode) {
            $(vnode.elm as HTMLElement).on('click', () => {
              ctrl.changeToMaximize();
            });
          },
          postpatch: (_, vnode) => {
            $(vnode.elm as HTMLElement).off('click').on('click', () => {
              ctrl.changeToMaximize();
            });
          }
        }
      }),
      h('h3.title', [
        '进度',
        h('div.progress', {
          class: {
            completed: (ctrl.firstTask !== undefined && (ctrl.firstTask!.completed || ctrl.firstTask!.num >= ctrl.firstTask!.total))
          }
        }, ctrl.firstTask ? `(${ctrl.firstTask.num}/${ctrl.firstTask.total})` : `(-/-)`)
      ])
    ])
  ]
}

function maximizeBox(ctrl: ProgressCtrl) {
  return [
    h('div.modal-header', [
      h('h3.title', {attrs: dataIcon('训')}, [
        '进度',
        h('div.progress', {
          class: {
            completed: (ctrl.firstTask !== undefined && (ctrl.firstTask!.completed || ctrl.firstTask!.num >= ctrl.firstTask!.total))
          }
        }, ctrl.firstTask ? `(${ctrl.firstTask.num}/${ctrl.firstTask.total})` : `(-/-)`)
      ]),
      h('span.maximize', {
        attrs: {'data-icon': '小', title: '缩小'},
        hook: {
          insert(vnode) {
            $(vnode.elm as HTMLElement).on('click', () => {
              ctrl.changeToMinimize();
            });
          },
          postpatch: (_, vnode) => {
            $(vnode.elm as HTMLElement).off('click').on('click', () => {
              ctrl.changeToMinimize();
            });
          }
        }
      })
    ]),
    h('div.modal-content',  ctrl.tasks.length === 0 ? [h('div.empty', '没有任务')] : ctrl.tasks.map(task => {
      return h('div.task', {
        class: {
          current: (ctrl.firstTask !== undefined && (ctrl.firstTask!.id === task.id))
        }
      }, [
        h('a.name', {
          attrs: {
            'data-icon': task.sourceRel.source.icon,
            title: `${task.name} - ${task.sourceRel.source.name}`,
            href: task.link
          }
        }, task.name),
        h('div.progress', {
          class: {
            completed: task.completed || task.num >= task.total
          }
        }, `(${task.num}/${task.total})`)
      ])
    }))
  ]
}
