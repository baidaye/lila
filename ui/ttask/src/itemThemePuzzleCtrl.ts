import * as xhr from './xhr'
import TaskCtrl from './ctrl';
import {CommonItem} from "./interfaces";

export default class ItemThemePuzzleTaskCtrl {

  itemType: string = 'themePuzzle';
  showThemePuzzleModal: boolean = false;
  count: number = 0;

  themePuzzle: CommonItem = {
    isNumber: true,
    num: 1,
    extra: {
      cond: ''
    },
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.themePuzzleWithResult) {
        this.themePuzzle = this.ctrl.opts.task.item.themePuzzleWithResult.themePuzzle;
        this.ctrl.isNumber = this.themePuzzle.isNumber;
        this.ctrl.num = this.themePuzzle.num;
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.themePuzzle = {
      isNumber: true,
      num: 1,
      extra: {
        cond: ''
      },
      tags: []
    };
  };

  loadThemePuzzleCount = () => {
    let $form = $('.themePuzzleForm');
    let data = $form.serialize();
    xhr.loadThemePuzzleCount(data).then((res) => {
      $form.find('.count').text(res.count);
    });
  };

  submitThemePuzzle = () => {
    let $form = $('.themePuzzleForm');
    let ratingMin = $form.find('#form3-ratingMin').val();
    let ratingMax = $form.find('#form3-ratingMax').val();
    let stepsMin = $form.find('#form3-stepsMin').val();
    let stepsMax = $form.find('#form3-stepsMax').val();

    this.themePuzzle.tags = [];

    if(ratingMin) {
      this.themePuzzle.tags.push(`>=${ratingMin}分`);
    }
    if(ratingMax) {
      this.themePuzzle.tags.push(`<=${ratingMax}分`);
    }
    if(stepsMin) {
      this.themePuzzle.tags.push(`>=${stepsMin}步`);
    }
    if(stepsMax) {
      this.themePuzzle.tags.push(`<=${stepsMax}步`);
    }

    $form.find('span input:checked').each((_, el: Element) => {
      // @ts-ignore
      this.themePuzzle.tags.push($(el).next().text());
    });

    this.themePuzzle.extra!.cond = $form.serialize();
    xhr.loadThemePuzzleCount(this.themePuzzle.extra!.cond).then((res) => {
      this.count = res.count;
      this.onThemePuzzleModalClose();
    });
  };

  onThemePuzzleModalOpen = () => {
    this.showThemePuzzleModal = true;
    this.ctrl.redraw();
    this.loadThemePuzzleCount();
  };

  onThemePuzzleModalClose = () => {
    this.showThemePuzzleModal = false;
    this.ctrl.redraw();
  };

}
