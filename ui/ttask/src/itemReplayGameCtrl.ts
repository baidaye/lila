import * as xhr from './xhr'
import TaskCtrl from './ctrl';
import {ReplayGameWithResult} from './interfaces';

export default class ItemReplayGameTaskCtrl {

  itemType: string = 'replayGame';
  showReplayGameModal: boolean = false;
  maxNum: number = 5;

  activeTab: string = 'study';

  replayGames: ReplayGameWithResult[] = [];

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.replayGameWithResult) {
        this.replayGames = this.ctrl.opts.task.item.replayGameWithResult;
        this.ctrl.num = this.replayGames.length;
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.replayGames = [];
    this.ctrl.studyCtrl.reset();
    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.reset();
    }
  };

  onReplayGameModalOpen = () => {
    this.showReplayGameModal = true;
    this.ctrl.studyCtrl.reset();
    this.ctrl.studyCtrl.onInitStudy();
    if(this.ctrl.hasCourseWare()) {
      this.ctrl.courseWareCtrl.onInitCourseWare();
    }
  };

  onReplayGameModalClose = () => {
    this.showReplayGameModal = false;
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.replayGames.length >= this.maxNum;
  };

  submitReplayGame = () => {
    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    let infoLink;
    let chapterLink;
    let chapterId;
    if(this.activeTab === 'study') {
      let currentStudy = this.ctrl.studyCtrl.currentStudy;
      let currentChapter = this.ctrl.studyCtrl.currentChapter;
      if(currentStudy && currentChapter) {
        let studyId = currentStudy.id;
            chapterId = currentChapter.id;
        infoLink = `/study/${studyId}/${chapterId}/info`;
        chapterLink = `/study/${studyId}/${chapterId}`;
      } else this.onReplayGameModalClose();
    } else if(this.activeTab === 'courseWare') {
      let olClassId = this.ctrl.courseWareCtrl.currentCourseId;
      let courseWareId = this.ctrl.courseWareCtrl.currentCourseWareId;
          chapterId = courseWareId;
      if(olClassId && courseWareId) {
        infoLink = `/olclass/${olClassId}/courseWare/info?courseWareId=${courseWareId}`;
        chapterLink = `/olclass/${olClassId}#${courseWareId}`;
      } else this.onReplayGameModalClose();
    }

    if(infoLink && chapterLink) {
      xhr.loadReplayInfo(infoLink).then((data) => {
        if(data) {
          data.chapterId = chapterId;
          data.chapterLink = chapterLink;
          let d = {replayGame: data, result: {}};
          // @ts-ignore
          this.replayGames.push(d);
          this.onReplayGameModalClose();
          this.setReplayGameNum();
        }
      });
    } else this.onReplayGameModalClose();
  };

  removeReplayGame = (index) => {
    this.replayGames = this.replayGames.filter((_, i) => index !== i);
    this.setReplayGameNum();
    this.ctrl.redraw();
  };

  setReplayGameNum = () => {
    this.ctrl.num = this.replayGames.length;
    this.ctrl.setTaskName();
  };

}
