const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function submitTask(data: any) {
  return $.ajax({
    method: 'post',
    url: `/ttask/create`,
    headers: headers,
    data: data
  });
}

export function loadThemePuzzleCount(data: any) {
  return $.ajax({
    url: `/resource/puzzle/themeOrCount`,
    data: data
  });
}

export function validateFen(fen: string) {
  return $.ajax({
    url: `/setup/validate-fen?strict=0&playable=true`,
    headers: headers,
    data: {
      fen: fen
    }
  });
}

export function loadCapsules() {
  return $.ajax({
    url: '/resource/capsule/mineApi',
    headers: headers
  });
}

export function getCapsulePuzzles(ids: string[]) {
  return $.ajax({
    url: '/resource/capsule/infos?ids=' + ids.join(','),
    headers: headers
  });
}

export function loadStudyTags(channel) {
  return $.ajax({
    url: `/study/tags/${channel}`,
    headers: headers
  });
}

export function loadStudy(channel, page, q, tagData) {
  let url = q && q.trim() ? `/study/search?channel=${channel}&page=${page}&q=${q}` : `/study/${channel}/popular?page=${page}`;
  return $.ajax({
    url: url,
    data: tagData,
    headers: headers
  });
}

export function loadChapter(studyId) {
  return $.ajax({
    url: `/study/${studyId}/chapters`,
    headers: headers
  });
}

export function loadChapterInfo(studyId, chapterId) {
  return $.ajax({
    url: `/study/${studyId}/${chapterId}/info`,
    headers: headers
  });
}

export function loadRecall(data: any) {
  return $.ajax({
    method: 'post',
    url: `/recall/pgn`,
    headers: headers,
    data: data
  });
}

export function loadDistinguish(data: any) {
  return $.ajax({
    method: 'post',
    url: `/distinguish/pgn`,
    headers: headers,
    data: data
  });
}

export function mineTask(item: string) {
  return $.ajax({
    method: 'get',
    url: `/ttask/mineItem?item=${item}`,
    headers: headers
  });
}

export function createTpl(data: any) {
  return $.ajax({
    method: 'post',
    url: `/ttask/tpl/create`,
    headers: headers,
    data: data
  });
}

export function updateTpl(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/ttask/tpl/${id}/update`,
    headers: headers,
    data: data
  });
}

export function removeTpl(id: string) {
  return $.ajax({
    method: 'post',
    url: `/ttask/tpl/${id}/remove`,
    headers: headers
  });
}

export function loadCourses(clazzId: string) {
  return $.ajax({
    method: 'get',
    url: `/clazz/${clazzId}/allCourses`,
    headers: headers
  });
}

export function loadCourseWares(courseId: string) {
  return $.ajax({
    method: 'get',
    url: `/olclass/${courseId}/courseWare/list`,
    headers: headers
  });
}

export function loadCourseWare(courseId: string, courseWareId: string) {
  return $.ajax({
    method: 'get',
    url: `/olclass/${courseId}/courseWare/info?courseWareId=${courseWareId}`,
    headers: headers
  });
}

// loadChapterInfo + loadCourseWare
export function loadReplayInfo(url) {
  return $.ajax({
    url: url,
    headers: headers
  });
}

export function loadOpeningdbs() {
  return $.ajax({
    url: '/resource/openingdb/mineApi',
    headers: headers
  });
}
