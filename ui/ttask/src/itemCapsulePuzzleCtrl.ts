import * as xhr from './xhr'
import TaskCtrl from './ctrl';
import {CapsulePuzzleWithResult} from './interfaces';

export default class ItemCapsulePuzzleTaskCtrl {

  itemType: string = 'capsulePuzzle';
  showCapsuleModal: boolean = false;
  maxNum: number = 5;

  capsules: any = {
    mine: {
      list: [],
      tags: []
    },
    member: {
      list: [],
      tags: []
    },
    teamManager: {
      list: [],
      tags: []
    }
  };

  capsulePuzzle: CapsulePuzzleWithResult = {
    num: 0,
    capsules: []
  };

  capsuleTabs = [{id: 'mine', name: '我的列表'}, {id: 'member', name: '参与列表'}, {id: 'teamManager', name: '俱乐部授权'}];
  activeCapsuleTab: string = 'mine';

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.capsulePuzzleWithResult) {
        this.capsulePuzzle = this.ctrl.opts.task.item.capsulePuzzleWithResult;
        this.ctrl.num = this.capsulePuzzle.num;
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.capsules = {
      mine: {
        list: [],
        tags: []
      },
      member: {
        list: [],
        tags: []
      },
      teamManager: {
        list: [],
        tags: []
      }
    };
    this.capsulePuzzle = {
      num: 0,
      capsules: []
    }
  };

  onCapsuleModalOpen = () => {
    this.showCapsuleModal = true;
    this.loadCapsules();
  };

  onCapsuleModalClose = () => {
    this.showCapsuleModal = false;
    this.ctrl.redraw();
  };

  loadCapsules = () => {
    xhr.loadCapsules().then((res) => {
      this.capsules = res;
      this.ctrl.redraw();
    });
  };

  filterCapsules = () => {
    let $form = $(`.capsulePuzzleForm .capsuleTab-${this.activeCapsuleTab}`);
    let $filter = $form.find('.capsule-filter');
    let name = $filter.find('.capsule-filter-search').val();
    let tags: string[] = [];
    $filter.find('.capsule-filter-tag').find('input:checked').each(function (_, el: HTMLInputElement) {
      tags.push(el.value);
    });

    let $lst = $form.find('.capsule-list');
    let arr: any[] = [];
    $lst.find('tr').each(function (_, el) {
      arr.push($(el).data('attr'));
    });

    let filterIds = arr.filter(function (n) {
      return (!name || n.name.indexOf(name) > -1) && (tags.length === 0 || tags.every(t => n.tags.includes(t)))
    }).map(n => n.id);

    $lst.find('tr').addClass('none');
    filterIds.forEach(function (id) {
      $lst.find('#chk_' + id).parents('tr').removeClass('none');
    });
  };

  submitCapsule = () => {
    let $form = $(`.capsulePuzzleForm`);
    let ids: string[] = [];
    $form.find('input[name="rdCapsule"]:checked').each(function (_, el: HTMLInputElement) {
      ids.push(el.value);
    });

    if(ids.length > this.maxNum) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    xhr.getCapsulePuzzles(ids).then((res) => {
      this.capsulePuzzle.capsules = res.map((capsule) => {
        return {
          id: capsule.id,
          name: capsule.name,
          puzzles: capsule.puzzles.map((puzzle) => {
            return {
              puzzle: puzzle
            }
          })
        }
      });

      this.onCapsuleModalClose();
      this.setCapsuleNum();
      setTimeout(() => {
        window.lichess.pubsub.emit('content_loaded');
      }, 200);
    });
  };

  removeCapsule = (id) => {
    this.capsulePuzzle.capsules = this.capsulePuzzle.capsules.filter(c => c.id !== id);
    this.setCapsuleNum();
    this.ctrl.redraw();
  };

  setCapsuleNum = () => {
    let arr: string[] = [];
    this.capsulePuzzle.capsules.forEach(c => {
      arr = arr.concat(c.puzzles.map(pwr => pwr.puzzle.id));
    });
    this.capsulePuzzle.num = arr.length;
    this.ctrl.num = arr.length;
    this.ctrl.setTaskName();
  };

}
