import TaskCtrl from './ctrl';
import * as xhr from './xhr'

export default class CourseWareCtrl {

  courseWareLoading: boolean = true;

  courses: any;
  courseWares: any;

  currentCourseId: string;
  currentCourseWareId: string | undefined;

  constructor(readonly ctrl: TaskCtrl) {

  }

  reset = () => {
    this.courses = [];
    this.courseWares = [];
    this.currentCourseId = this.ctrl.courseId();
    this.currentCourseWareId = undefined;
    this.courseWareLoading = false;
  };

  onInitCourseWare = () => {
    this.currentCourseId = this.ctrl.courseId();
    this.loadCourses();
    this.loadCourseWares();
  };

  setCurrentCourse = (courseId: string) => {
    this.currentCourseId = courseId;
    this.loadCourseWares();
  };

  loadCourses = () => {
    let clazzId = this.ctrl.clazzId();
    xhr.loadCourses(clazzId).then((data) => {
      this.courses = data;
      this.ctrl.redraw();
    });
  };

  loadCourseWares = () => {
    this.courseWareLoading = true;
    this.ctrl.redraw();

    xhr.loadCourseWares(this.currentCourseId).then((data) => {
      this.courseWares = data;
      this.courseWareLoading = false;
      this.ctrl.redraw();
    });
  };

}
