import TaskCtrl from './ctrl';
import {FromOpeningdb, FromOpeningdbWithResult} from './interfaces';
import * as xhr from "./xhr";

export default class ItemFromOpeningdbGameTaskCtrl {

  itemType: string = 'fromOpeningdb';
  maxNum: number = 5;

  emptyFen = '8/8/8/8/8/8/8/8 w - -';
  initialFen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  fromOpeningdbGames: FromOpeningdbWithResult[] = [];

  selectFromOpeningdb: FromOpeningdb;
  selectFromOpeningdbIndex: number;
  showOpeningdbSelectModal: boolean = false;
  activeFromOpeningdbSelectTab: string = 'mine';

  openingdbs: any = {
    mine: [],
    auth: [],
    system: []
  };

  constructor(readonly ctrl: TaskCtrl) {
    this.maxNum = ctrl.maxNum;
  }

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.fromOpeningdbWithResult) {
        this.fromOpeningdbGames = this.ctrl.opts.task.item.fromOpeningdbWithResult;
        this.ctrl.num = this.fromOpeningdbGames.reduce((total, fpg) => {
            return total + fpg.fromOpeningdb.num;
        }, 0);
        this.ctrl.delayRedraw();
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
      this.loadOpeningdbs();
    }
  };

  resetSetting = () => {
    this.fromOpeningdbGames = [];
  };

  setFromOpeningdbPgn = (index, data) => {
    this.fromOpeningdbGames.forEach((fpwr, i) => {
      if(index === i && fpwr.fromOpeningdb.el) {
        fpwr.fromOpeningdb.initialPgn = data;
      }
    });
    this.ctrl.redraw();
  };

  changeValue = (index, field, subField, v) => {
    this.fromOpeningdbGames.forEach((fpg, i) => {
      index === i ? !!subField ? fpg.fromOpeningdb[field][subField] = v : fpg.fromOpeningdb[field] = v : fpg;
    });
    if(field === 'num') this.setFromOpeningdbGameNum();
    this.ctrl.redraw();
  };

  isFull = () => {
    return this.fromOpeningdbGames.length >= this.maxNum;
  };

  addFromOpeningdbGame = () => {
    if(this.isFull()) {
      alert(`最多添加${this.maxNum}项`);
      return;
    }

    this.fromOpeningdbGames.push({
      fromOpeningdb: {
        el: null,
        initialPgn: {
          lastPly: 0,
          initialFen: this.initialFen,
          lastFen: this.initialFen,
          source: 'opening',
          opening: {
            id: '',
            name: '',
            desc: '',
            nodes: 0,
            orientation: 'white',
            members: {},
            initialFen: this.initialFen,
            enable: true,
            system: false,
            clean: 0,
            ownerId: '',
            mainline: '',
            mainlineMoves: []
          },
          openingdbExcludeWhiteBlunder: true,
          openingdbExcludeBlackBlunder: true,
          openingdbExcludeWhiteJscx: true,
          openingdbExcludeBlackJscx: true,
          openingdbCanOff: false
        },
        clock: {
          initial: 60 * 5,
          increment: 0
        },
        num: 1,
        isAi: true,
        aiLevel: 1,
        color: 'random',
        chessStatus: 'all',
        canTakeback: 0
      },
      result: {}
    });
    this.setFromOpeningdbGameNum();
    this.ctrl.redraw();
  };

  removeFromOpeningdbGame = (index) => {
    this.fromOpeningdbGames = this.fromOpeningdbGames.filter((_, i) => index !== i);
    this.setFromOpeningdbGameNum();
    this.ctrl.redraw();
  };

  setFromOpeningdbGameNum = () => {
    this.ctrl.num = this.fromOpeningdbGames.reduce((total, fpg) => {
      return total + fpg.fromOpeningdb.num;
    }, 0);
    this.ctrl.setTaskName();
  };

  onOpeningdbSelectModalOpen = (fromOpeningdb, index) => {
    this.selectFromOpeningdb = fromOpeningdb;
    this.selectFromOpeningdbIndex = index;
    this.showOpeningdbSelectModal = true;
    this.loadOpeningdbs();
    this.ctrl.redraw();
  };

  onOpeningdbSelectModalClose = () => {
    this.showOpeningdbSelectModal = false;
    this.ctrl.redraw();
  };

  submitOpeningdbSelect = () => {
    let $form = $(`.fromOpeningdbSelectForm`);
    let openingdbId = $form.find('input[name=openingdbId]:checked').val();
    let openingdbExcludeWhiteBlunder = $form.find('input[name=openingdbExcludeWhiteBlunder]').is(':checked');
    let openingdbExcludeBlackBlunder = $form.find('input[name=openingdbExcludeBlackBlunder]').is(':checked');
    let openingdbExcludeWhiteJscx = $form.find('input[name=openingdbExcludeWhiteJscx]').is(':checked');
    let openingdbExcludeBlackJscx = $form.find('input[name=openingdbExcludeBlackJscx]').is(':checked');
    let openingdbCanOff = $form.find('input[name=openingdbCanOff]:checked').val();

    if(!openingdbId) {
      alert('请选择开局库');
      return;
    }

    let all = this.openingdbs.mine.concat(this.openingdbs.member).concat(this.openingdbs.visible).concat(this.openingdbs.system);
    let selectOpeningdb = all.find((f) => f.id === openingdbId);

    this.fromOpeningdbGames.forEach((f, i) => {
      if(i === this.selectFromOpeningdbIndex) {
        f.fromOpeningdb.initialPgn = {
          lastPly: 0,
          initialFen: this.initialFen,
          lastFen: this.initialFen,
          source: 'opening',
          opening: selectOpeningdb!,
          openingdbExcludeWhiteBlunder: openingdbExcludeWhiteBlunder,
          openingdbExcludeBlackBlunder: openingdbExcludeBlackBlunder,
          openingdbExcludeWhiteJscx: openingdbExcludeWhiteJscx,
          openingdbExcludeBlackJscx: openingdbExcludeBlackJscx,
          openingdbCanOff: openingdbCanOff
        };
      }
    });

    this.onOpeningdbSelectModalClose();
  };

  loadOpeningdbs = () => {
    if(!this.openingdbs.length) {
      xhr.loadOpeningdbs().then((res) => {
        this.openingdbs = res;
        this.ctrl.redraw();
      });
    }
  };

}
