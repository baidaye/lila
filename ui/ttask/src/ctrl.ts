import * as xhr from './xhr'
import * as ChessJS from 'chess.js';
import {TTaskOpts, Redraw, Student} from './interfaces';
import ItemPuzzleTaskCtrl from './itemPuzzleCtrl';
import ItemThemePuzzleTaskCtrl from './itemThemePuzzleCtrl';
import ItemCapsulePuzzleCtrl from './itemCapsulePuzzleCtrl';
import ItemCoordTrainCtrl from './itemCoordTrainCtrl';
import ItemPuzzleRushTaskCtrl from './itemPuzzleRushCtrl';
import ItemReplayGameTaskCtrl from './itemReplayGameCtrl';
import ItemRecallGameTaskCtrl from './itemRecallGameCtrl';
import ItemDistinguishGameTaskCtrl from './itemDistinguishGameCtrl';
import ItemGameTaskCtrl from './itemGameCtrl';
import ItemFromPositionGameTaskCtrl from './itemFromPositionGameCtrl';
import ItemFromPgnGameTaskCtrl from './itemFromPgnGameCtrl';
import ItemFromOpeningdbGameTaskCtrl from './itemFromOpeningdbGameCtrl';
import ContentStudyCtrl from './contentStudyCtrl';
import ContentCourseWareCtrl from './contentCourseWareCtrl'

export default class TaskCtrl {

  chess = ChessJS.Chess();

  itemTypes = [
    {id: 'puzzle', name: '战术训练', unit: '题', defNum: 5},
    {id: 'themePuzzle', name: '主题战术', unit: '题', defNum: 5},
    {id: 'capsulePuzzle', name: '指定战术题', unit: '题', defNum: 1},
    {id: 'puzzleRush', name: '战术冲刺', unit: '次', defNum: 1},
    {id: 'coordTrain', name: '坐标训练', unit: '盘', defNum: 1},
    {id: 'replayGame', name: '打谱', unit: '盘', defNum: 1},
    {id: 'recallGame', name: '记谱', unit: '盘', defNum: 1},
    {id: 'distinguishGame', name: '棋谱记录', unit: '盘', defNum: 1},
    {id: 'game', name: '对局', unit: '盘', defNum: 1},
    {id: 'fromPosition', name: '指定开局FEN对弈', unit: '盘', defNum: 1},
    {id: 'fromPgn', name: '指定开局PGN对弈', unit: '盘', defNum: 1, member: true},
    {id: 'fromOpeningdb', name: '指定开局库对弈', unit: '盘', defNum: 1, member: true}
  ];

  maxNum: number = 5;

  students: Student[];
  studentId: string;

  puzzleCtrl: ItemPuzzleTaskCtrl;
  themePuzzleCtrl: ItemThemePuzzleTaskCtrl;
  capsulePuzzleCtrl: ItemCapsulePuzzleCtrl;
  puzzleRushCtrl: ItemPuzzleRushTaskCtrl;
  coordTrainCtrl: ItemCoordTrainCtrl;
  replayGameCtrl: ItemReplayGameTaskCtrl;
  recallGameCtrl: ItemRecallGameTaskCtrl;
  distinguishGameCtrl: ItemDistinguishGameTaskCtrl;
  gameCtrl: ItemGameTaskCtrl;
  fromPositionGameCtrl: ItemFromPositionGameTaskCtrl;
  fromPgnGameCtrl: ItemFromPgnGameTaskCtrl;
  fromOpeningdbGameCtrl: ItemFromOpeningdbGameTaskCtrl;
  studyCtrl: ContentStudyCtrl;
  courseWareCtrl: ContentCourseWareCtrl;

  showCreateModal: boolean = false;
  itemType: string = 'puzzle';
  activeTab: string = 'setting';

  taskName: string = '';
  num: number = this.itemTypes.find(t => t.id === this.itemType)!.defNum;
  isNumber: boolean = true;
  target: string = '完成';
  unit: string = '题';

  constructor(readonly opts: TTaskOpts, readonly redraw: Redraw, readonly afterSubmit: () => void, maxNum: number = 5) {
    this.maxNum = maxNum;
    this.puzzleCtrl = new ItemPuzzleTaskCtrl(this);
    this.themePuzzleCtrl = new ItemThemePuzzleTaskCtrl(this);
    this.capsulePuzzleCtrl = new ItemCapsulePuzzleCtrl(this);
    this.puzzleRushCtrl = new ItemPuzzleRushTaskCtrl(this);
    this.coordTrainCtrl = new ItemCoordTrainCtrl(this);
    this.gameCtrl = new ItemGameTaskCtrl(this);
    this.replayGameCtrl = new ItemReplayGameTaskCtrl(this);
    this.recallGameCtrl = new ItemRecallGameTaskCtrl(this);
    this.distinguishGameCtrl = new ItemDistinguishGameTaskCtrl(this);
    this.fromPositionGameCtrl = new ItemFromPositionGameTaskCtrl(this);
    this.fromPgnGameCtrl = new ItemFromPgnGameTaskCtrl(this);
    this.fromOpeningdbGameCtrl = new ItemFromOpeningdbGameTaskCtrl(this);
    this.studyCtrl = new ContentStudyCtrl(this);
    if(this.hasCourseWare()) {
      this.courseWareCtrl = new ContentCourseWareCtrl(this);
    }
  }

  initTask = () => {
    let t = this.opts.task;
    if(t) {
      this.itemType = t.itemType.id;
      this.taskName = t.name;

      this.puzzleCtrl.initTask(this.itemType);
      this.themePuzzleCtrl.initTask(this.itemType);
      this.capsulePuzzleCtrl.initTask(this.itemType);
      this.puzzleRushCtrl.initTask(this.itemType);
      this.coordTrainCtrl.initTask(this.itemType);
      this.gameCtrl.initTask(this.itemType);
      this.replayGameCtrl.initTask(this.itemType);
      this.recallGameCtrl.initTask(this.itemType);
      this.distinguishGameCtrl.initTask(this.itemType);
      this.fromPositionGameCtrl.initTask(this.itemType);
      this.fromPgnGameCtrl.initTask(this.itemType);
      this.fromOpeningdbGameCtrl.initTask(this.itemType);

      this.setItemTarget();
      this.setItemUnit();
    } else {
      this.setItemType(this.itemType);
      this.setItemTarget();
      this.setItemUnit();
      this.setTaskName();
    }
  };

  openTaskCreateModal = (students: Student[], studentId: string) => {
    this.students = students;
    this.studentId = studentId;
    this.showCreateModal = true;
    this.redraw();
    this.changeItemType(this.itemType);
  };

  closeTaskCreateModal = () => {
    this.showCreateModal = false;
    this.resetSetting();
    this.redraw();
  };

  resetSetting = () => {
    this.taskName = '';
    this.num = this.itemTypes.find(t => t.id === this.itemType)!.defNum;
    this.isNumber = true;
    this.target = '完成';
    this.unit = '题';
  };

  changeItemType = (itemType: string) => {
    this.setItemType(itemType);
    this.setItemDefNum();
    this.setItemDefIsNumber();
    this.setItemTarget();
    this.setItemUnit();
    this.setTaskName();
  };

  setItemType = (itemType: string) => {
    this.itemType = itemType;
    this.puzzleCtrl.changeItemType(itemType);
    this.themePuzzleCtrl.changeItemType(itemType);
    this.capsulePuzzleCtrl.changeItemType(itemType);
    this.puzzleRushCtrl.changeItemType(itemType);
    this.coordTrainCtrl.changeItemType(itemType);
    this.gameCtrl.changeItemType(itemType);
    this.replayGameCtrl.changeItemType(itemType);
    this.recallGameCtrl.changeItemType(itemType);
    this.distinguishGameCtrl.changeItemType(itemType);
    this.fromPositionGameCtrl.changeItemType(itemType);
    this.fromPgnGameCtrl.changeItemType(itemType);
    this.fromOpeningdbGameCtrl.changeItemType(itemType);
  };

  changeIsNumber = (isNumber: boolean) => {
    this.isNumber = isNumber;
    this.setItemTarget();
    this.setItemUnit();
    this.setTaskName();
  };

  setItemDefIsNumber = () => {
    this.isNumber = true;
  };

  setItemDefNum = () => {
    this.num = this.itemTypes.find(t => t.id === this.itemType)!.defNum;
  };

  setItemTarget = () => {
    this.target = this.isNumber ? '完成' : '增加';
  };

  setItemUnit = () => {
    this.unit = this.isNumber ? this.itemTypes.find(t => t.id === this.itemType)!.unit : '分';
  };

  setTaskName = () => {
    let $form = $('.taskForm');
    let itemTypeName = this.itemTypes.find(t => t.id === this.itemType)!.name;
    this.taskName = `“${itemTypeName}”${this.target} ${this.num} ${this.unit}`;

    if(this.itemType === 'game') {
      setTimeout(() => {
        let speedName = $form.find('input[name="item.game.speed"]:checked').next('label').text();
        this.taskName = `“${speedName}”${this.target} ${this.num} ${this.unit}`;
        this.redraw();
      }, 200);
    }
    this.redraw();

    $form
      .find('#form-num')
      .val('')
      .focus()
      .val(this.num);
  };

  isUpdate = () => {
    return !!this.opts.task;
  };

  hasCourseWare = () => {
    return !!this.clazzId();
  };

  clazzId = () => {
    // @ts-ignore
    return this.opts.clazz ? this.opts.clazz.id : undefined;
  };

  courseId = () => {
    // @ts-ignore
    return this.opts.course ? this.opts.course.id : undefined;
  };

  toColor = (fen: string) => {
    this.chess.load(fen);
    return ((this.chess.turn() === 'w') ? 'white' : 'black') as Color;
  };

  submitValid = ($form) => {

    if(this.itemType === 'capsulePuzzle') {
      if (this.capsulePuzzleCtrl.capsulePuzzle.capsules.length === 0) {
        alert('请添加战术题');
        return false;
      }
    }

    if(this.itemType === 'replayGame') {
      if (this.replayGameCtrl.replayGames.length === 0) {
        alert('请添加打谱');
        return false;
      }
    }

    if(this.itemType === 'recallGame') {
      if (this.recallGameCtrl.recallGames.length === 0) {
        alert('请添加记谱');
        return false;
      }
    }

    if(this.itemType === 'distinguishGame') {
      if (this.distinguishGameCtrl.distinguishGames.length === 0) {
        alert('请添加棋谱记录');
        return false;
      }
    }

    if(this.itemType === 'fromPosition') {
      if (this.fromPositionGameCtrl.fromPositionGames.length === 0) {
        alert('请添加指定开局FEN对弈');
        return false;
      }
    }

    if(this.itemType === 'fromPgn') {
      if (this.fromPgnGameCtrl.fromPgnGames.length === 0) {
        alert('请添加指定开局PGN对弈');
        return false;
      }
    }

    if(this.itemType === 'fromOpeningdb') {
      if (this.fromOpeningdbGameCtrl.fromOpeningdbGames.length === 0 ||
        this.fromOpeningdbGameCtrl.fromOpeningdbGames.filter(f => !!f.fromOpeningdb.initialPgn.opening.id).length === 0) {
        alert('请添加指定开局库对局');
        return false;
      }
    }

    if (
      this.itemType === 'themePuzzle' ||
      this.itemType === 'coordTrain' ||
      (this.itemType === 'puzzleRush' && this.puzzleRushCtrl.puzzleRush.mode === 'custom')
    ) {
      let $cond = $form.find('#extraCond');
      let $parent = $cond.parent('.selected');
      let val = $cond.val();
      if(!val || val === 'ratingMin=&ratingMax=&stepsMin=&stepsMax=') {
        $parent.addClass('is-invalid');
        $parent.prop('title', '请添加条件');
        return;
      } else {
        $parent.removeClass('is-invalid');
        $parent.prop('title', '请添加条件');
      }
    }

    return true;
  };

  submitTask = () => {
    let $form = $('.taskForm');

    if($form.find('.student-scroll ul input:checked').length === 0) {
      alert('请至少选择一名学员');
      return;
    }

    if(!this.submitValid($form)) {
      return;
    }

    let data = $form.serialize();
    xhr.submitTask(data).then(() => {
      this.showCreateModal = false;
      this.redraw();
      this.afterSubmit();
    });
  };

  delayRedraw = () => {
    setTimeout(() => {
      window.lichess.pubsub.emit('content_loaded');
    }, 200);
  }

}
