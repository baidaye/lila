import TaskCtrl from './ctrl';
import {CommonItem} from './interfaces';

export default class ItemPuzzleTaskCtrl {

  itemType: string = 'puzzle';

  puzzle: CommonItem = {
    isNumber: true,
    num: 1,
    extra: {
      cond: ''
    },
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.puzzleWithResult) {
        this.puzzle = this.ctrl.opts.task.item.puzzleWithResult.puzzle;
        this.ctrl.isNumber = this.puzzle.isNumber;
        this.ctrl.num = this.puzzle.num;
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.puzzle = {
      isNumber: true,
      num: 1,
      extra: {
        cond: ''
      },
      tags: []
    };
  }

}
