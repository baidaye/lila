import TaskCtrl from './ctrl';
import {PuzzleRush} from "./interfaces";

export default class ItemPuzzleRushTaskCtrl {

  itemType: string = 'puzzleRush';
  showCustomPuzzleRushModal: boolean = false;

  puzzleRush: PuzzleRush = {
    isNumber: true,
    num: 1,
    mode: 'threeMinutes',
    extra: {
      cond: ''
    },
    tags: []
  };

  constructor(readonly ctrl: TaskCtrl) {}

  initTask = (itemType: string) => {
    if(itemType === this.itemType) {
      if(this.ctrl.opts.task && this.ctrl.opts.task.item && this.ctrl.opts.task.item.puzzleRushWithResult) {
        this.puzzleRush = this.ctrl.opts.task.item.puzzleRushWithResult.puzzleRush;
        this.ctrl.isNumber = this.puzzleRush.isNumber;
        this.ctrl.num = this.puzzleRush.num;
      }
    }
  };

  changeItemType = (itemType: string) => {
    if(itemType === this.itemType) {
      this.resetSetting();
    }
  };

  resetSetting = () => {
    this.puzzleRush = {
      isNumber: true,
      num: 1,
      mode: 'threeMinutes',
      extra: {
        cond: ''
      },
      tags: []
    };
  };

  onCustomPuzzleRushModalOpen = () => {
    this.showCustomPuzzleRushModal = true;
    this.ctrl.redraw();
  };

  onCustomPuzzleRushModalClose = () => {
    this.showCustomPuzzleRushModal = false;
    this.ctrl.redraw();
  };

  submitCustomPuzzleRush = () => {
    let $form = $('.customPuzzleRushModal form');
    let ratingMin = $form.find('#form-ratingMin').val();
    let ratingMax = $form.find('#form-ratingMax').val();
    let stepsMin = $form.find('#form-stepsMin').val();
    let stepsMax = $form.find('#form-stepsMax').val();
    let color = $form.find('#form-color').val();
    let phase = $form.find('#form-phase').val();
    let selector = $form.find('#form-selector').val();
    let minutes = $form.find('#form-minutes').val();
    let limit = $form.find('#form-limit').val();

    this.puzzleRush.tags = [];

    if(ratingMin) {
      this.puzzleRush.tags.push(`>=${ratingMin}分`);
    }
    if(ratingMax) {
      this.puzzleRush.tags.push(`<=${ratingMax}分`);
    }
    if(stepsMin) {
      this.puzzleRush.tags.push(`>=${stepsMin}步`);
    }
    if(stepsMax) {
      this.puzzleRush.tags.push(`<=${stepsMax}步`);
    }
    if(color) {
      let text = $form.find('#form-color').find('option:selected').text();
      this.puzzleRush.tags.push(`${text}`);
    }
    if(phase) {
      let text = $form.find('#form-phase').find('option:selected').text();
      this.puzzleRush.tags.push(`${text}`);
    }
    if(selector) {
      let text = $form.find('#form-selector').find('option:selected').text();
      this.puzzleRush.tags.push(`${text}`);
    }
    if(minutes) {
      this.puzzleRush.tags.push(`${minutes}分钟`);
    }
    if(limit) {
      this.puzzleRush.tags.push(`错题<${limit}`);
    }

    this.puzzleRush.extra!.cond = $form.serialize();
    this.onCustomPuzzleRushModalClose();
  };

}
