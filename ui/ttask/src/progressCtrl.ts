import * as xhr from './xhr'
import {Task} from './interfaces';

export default class ProgressCtrl {

  firstTask: Task | undefined;
  tasks: Task[] = [];

  minimize: boolean;
  popup: HTMLElement;
  popupHeader: HTMLElement;

  isDrag: boolean;
  leftBox: number;
  topBox: number;
  nowX: number;
  nowY: number;
  maxLeft: number;
  maxTop: number;
  width: number;
  height: number;
  top: number;
  left: number;

  // case object PuzzleItem extends TTaskItemType("puzzle", "战术训练", TTaskItemSource.PuzzleItem)
  // case object ThemePuzzleItem extends TTaskItemType("themePuzzle", "主题战术", TTaskItemSource.PuzzleItem)
  // case object CapsulePuzzleItem extends TTaskItemType("capsulePuzzle", "指定战术题", TTaskItemSource.PuzzleItem)
  // case object PuzzleRushItem extends TTaskItemType("puzzleRush", "战术冲刺", TTaskItemSource.RushItem)
  // case object CoordTrainItem extends TTaskItemType("coordTrain", "坐标训练", TTaskItemSource.CoordTrainItem)
  // case object ReplayGameItem extends TTaskItemType("replayGame", "打谱", TTaskItemSource.ReplayGameItem)
  // case object RecallGameItem extends TTaskItemType("recallGame", "记谱", TTaskItemSource.RecallGameItem)
  // case object DistinguishGameItem extends TTaskItemType("distinguishGame", "棋谱记录", TTaskItemSource.DistinguishGameItem)
  // case object FromPositionItem extends TTaskItemType("fromPosition", "对局", TTaskItemSource.GameItem)
  // case object TrainGameItem extends TTaskItemType("trainGame", "战术训练对局", TTaskItemSource.GameItem)
  // case object TeamTest extends TTaskItemType("teamTest", "战术测评", TTaskItemSource.TeamTestItem)
  constructor(readonly item: string, readonly redraw: () => void, readonly fr: string | undefined = undefined) {
    this.minimize = true;
    this.loadTasks();
  }

  changeToMaximize = () => {
    this.minimize = false;
    this.redraw();
    this.changePopup();
  };

  changeToMinimize = () => {
    this.minimize = true;
    this.redraw();
    this.changePopup();
  };

  changePopup = () => {
    this.maxTop = document.documentElement.clientHeight - this.popup.offsetHeight;
    this.maxLeft = document.documentElement.clientWidth - this.popup.offsetWidth;
    this.left = this.left > this.maxLeft ? this.maxLeft : this.left;
    this.popup.style.left = this.left + 'px';
  };

  initPopupBox = (elm) => {
    this.addDragEvent(elm);
  };

  addDragEvent = (elm) => {
    this.popup = elm;
    this.popupHeader = elm.querySelector('.modal-header');

    const hasTouchEvents = window.lichess.hasTouchEvents;
    const mousedownEvent = hasTouchEvents ? 'touchstart' : 'mousedown';
    const mouseupEvent = hasTouchEvents ? 'touchend' : 'mouseup';
    const mousemoveEvent = hasTouchEvents ? 'touchmove' : 'mousemove';

    let onMousedown = (e) => {
      e = e || window.event;
      this.maxTop = document.documentElement.clientHeight - this.popup.offsetHeight;
      this.maxLeft = document.documentElement.clientWidth - this.popup.offsetWidth;
      this.topBox = (hasTouchEvents ? e.targetTouches[0].pageY : e.pageY) - this.popup.offsetTop;
      this.leftBox = (hasTouchEvents ? e.targetTouches[0].pageX : e.pageX) - this.popup.offsetLeft;
      this.isDrag = true;
    };

    let onMouseup = () => {
      this.isDrag = false;
    };

    let onMousemove = (e) => {
      e = e || window.event;
      //阻止事件默认行为和事件冒泡
      pauseEvent(e);
      //获取鼠标移动时的位置
      this.nowX = hasTouchEvents ? e.targetTouches[0].pageX : e.pageX;
      this.nowY = hasTouchEvents ? e.targetTouches[0].pageY : e.pageY;
      //拖动时
      if (this.isDrag) {
        this.left = this.nowX - this.leftBox;
        this.top = this.nowY - this.topBox;
        //弹窗范围限制
        this.left = this.left < 0 ? 0 : this.left;
        this.top = this.top < 0 ? 0 : this.top;
        this.left = this.left > this.maxLeft ? this.maxLeft : this.left;
        this.top = this.top > this.maxTop ? this.maxTop : this.top;
        //设置弹窗位置
        this.popup.style.left = this.left + 'px';
        this.popup.style.top = this.top + 'px';
        e.preventDefault();
      }
    };

    let pauseEvent = (e) => {
      if (e.preventDefault && !hasTouchEvents) e.preventDefault();
      if (e.stopPropagation) e.stopPropagation();
      e.cancelBubble = true;
      if(!hasTouchEvents) {
        e.returnValue = false;
      }
      return false
    };

    //鼠标按下选中弹窗标题变为可拖动,同时获取鼠标据弹窗左和上的距离及可拖动的最大距离
    this.popupHeader.addEventListener(mousedownEvent, onMousedown);

    //鼠标松开,变为不可拖动和拖拽改变大小
    this.popupHeader.addEventListener(mouseupEvent, onMouseup);

    //鼠标移动时改变大小或拖动弹窗
    if(hasTouchEvents) {
      this.popupHeader.addEventListener(mousemoveEvent, onMousemove);
    } else {
      document.addEventListener(mousemoveEvent, onMousemove);
    }
  };

  loadTasks = (): void => {
    xhr.mineTask(this.item).then((data) => {
      this.tasks = data;
      this.currTask();
      this.redraw();
    });
  };

  currTask = () => {
    if(this.fr) {
      // @ts-ignore
      let t = this.tasks.find(task => task.id === this.fr);
      if(t) {
        this.firstTask = t;
      } else {
        this.firstTask = this.tasks.length ? this.tasks[0] : undefined;
      }
    } else {
      this.firstTask = this.tasks.length ? this.tasks[0] : undefined;
    }
  };

  updateProgress = (data): void => {
    if (data.id) {
      this.tasks = this.tasks.map(task => {
        if (task.id === data.id) {
          return data;
        } else return task;
      });
      this.currTask();
    }
    this.redraw();
  };

  socketHandler = (type: string, data: any) => {
    const handlers = {
      ttaskCreate: (data) => {
        if (data && data.itemType && data.itemType.id === this.item) {
          this.loadTasks();
        }
      },
      ttaskStatus: (data) => {
        if (data && data.itemType && data.itemType.id === this.item) {
          this.updateProgress(data);
        }
      },
      ttaskProgress: (data) => {
        if (data && data.itemType && data.itemType.id === this.item) {
          this.updateProgress(data);
        }
      },
      ttaskFinish: (data) => {
        if (data && data.itemType && data.itemType.id === this.item) {
          this.updateProgress(data);
        }
      }
    };
    const handler = handlers[type];
    if (handler) handler(data);
    return false
  }

}
