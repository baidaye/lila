import {VNode} from 'snabbdom/vnode'
import {TTaskOpts, TaskTemplate} from '../../ttask/src/interfaces';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface HomeworkOpts extends TTaskOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  isTeamManager: boolean;
  userId: string;
  clazz: Clazz;
  course: Course;
  nextCourse?: Course;
  homework: Homework;
  prevHomework?: Homework;
  cloneHomework?: Homework;
  limit: TaskLimit;
}

export interface Clazz {
  id: string;
  name: string;
  coach: User;
  stopped: boolean;
  deleted: boolean;
}

export interface Course {
  id: string;
  index: number;
  clazz: string;
  date: string;
  timeBegin: string;
  timeEnd: string;
  dateTimeStart: string;
  week: string;
}

export interface Homework {
  id: string;
  name?: string;
  clazzId: string;
  courseId: string;
  index: number;
  startAt: string;
  deadlineAt?: string;
  isDeadline: boolean;
  status: HomeworkStatus;
  summary?: string;
  prepare?: string;
  common: TaskTemplate[];
  practice: TaskTemplate[];
  coinRule: number;
}

export interface HomeworkStatus {
  id: number;
  name: string;
}

export interface TaskLimit {
  common: number;
  practice: number;
}

export interface User extends LightUser {
  online: boolean;
}

export type Redraw = () => void;

