import {Redraw, HomeworkOpts, Clazz, Course, Homework} from './interfaces';
import {TaskItemType, TaskTemplate} from '../../ttask/src/interfaces';
import {make as makeSocket, Socket} from './socket';
import * as homeworkXhr from './xhr';
import TaskCtrl from '../../ttask/src/ctrl';
import * as xhr from '../../ttask/src/xhr';
import * as format from 'date-fns/format'
import * as addWeeks from 'date-fns/add_weeks'
import * as addMonths from 'date-fns/add_months'

export default class HomeworkCtrl extends TaskCtrl {

  opts: HomeworkOpts;
  clazz: Clazz;
  course: Course;
  homework: Homework;

  socket: Socket;
  deadlineAt: string;
  showDeadlineAtModal = false;
  showCloneToModal = false;

  clazzes: any;
  courses: any;
  filteredCourses: any;

  partId: string = 'common';
  commonItems: TaskItemType[] = [
    {id: 'puzzle', name: '战术训练'},
    {id: 'themePuzzle', name: '主题战术'},
    {id: 'puzzleRush', name: '战术冲刺'},
    {id: 'coordTrain', name: '坐标训练'},
    {id: 'game', name: '对局'}
  ];

  practiceItems: TaskItemType[] = [
    {id: 'capsulePuzzle', name: '指定战术题'},
    {id: 'replayGame', name: '打谱'},
    {id: 'recallGame', name: '记谱'},
    {id: 'distinguishGame', name: '棋谱记录'},
    {id: 'fromPosition', name: '指定开局FEN对弈'},
    {id: 'fromPgn', name: '指定开局PGN对弈', member: true},
    {id: 'fromOpeningdb', name: '指定开局库对弈', member: true}
  ];

  constructor(opts: HomeworkOpts, readonly redraw: Redraw) {
    super(opts, redraw, () => {}, 1);
    this.opts = opts;
    this.clazz = opts.clazz;
    this.course = opts.course;
    this.homework = opts.homework;
    this.deadlineAt = this.homework && this.homework.deadlineAt ? this.homework.deadlineAt : this.defaultDeadlineAt();
    this.socket = makeSocket(this.opts.socketSend, this);
  }

  openDeadlineAtModal = () => {
    this.showDeadlineAtModal = true;
    this.redraw();
  };

  closeDeadlineAtModal = () => {
    this.showDeadlineAtModal = false;
    this.redraw();
  };

  updateDeadlineAt = () => {
    let $form = $('.form-deadlineAt');
    let deadlineAt = $form.find('#deadlineAt').val();
    homeworkXhr.updateDeadlineAt(this.homework.id,  deadlineAt).then(() => {
      location.reload();
    });
  };

  defaultDeadlineAt = () => {
    let course = this.opts.course;
    let nextCourse = this.opts.nextCourse;
    let dateTimeStart = nextCourse ? nextCourse.dateTimeStart : course.dateTimeStart;
    let now = format(Date.now(), 'YYYY-MM-DD HH:mm');
    let minDeadlineAt = dateTimeStart > now ? dateTimeStart : now;
    let dda = nextCourse ? nextCourse.dateTimeStart : format(addWeeks(minDeadlineAt, 1), 'YYYY-MM-DD HH:mm');
    return dda;
  };

  isPublish = () => {
    return this.homework.status.id === 20;
  };

  isDeadline = () => {
    return this.homework.isDeadline;
  };

  itemTypeName = () => {
    return this.itemTypes.find(t => t.id === this.itemType)!.name;
  };

  openTaskTplCreateModal = (partId: string, itemType: string) => {
    let len = this.homework[partId].length;
    let max = this.opts.limit[partId];
    if(len >= max) {
      alert(`最多添加${max}项任务`);
      return;
    }

    this.partId = partId;
    this.itemType = itemType;
    this.openTaskCreateModal([], '');

    switch (itemType) {
      case 'capsulePuzzle':
        this.capsulePuzzleCtrl.onCapsuleModalOpen();
        break;
      case 'replayGame':
        this.replayGameCtrl.onReplayGameModalOpen();
        break;
      case 'recallGame':
        this.recallGameCtrl.onRecallGameModalOpen();
        break;
      case 'distinguishGame':
        this.distinguishGameCtrl.onDistinguishGameModalOpen();
        break;
      case 'fromPosition':
        setTimeout(() => {
          this.fromPositionGameCtrl.addFromPositionGame();
        }, 500);
        break;
      case 'fromPgn':
        setTimeout(() => {
          this.fromPgnGameCtrl.addFromPgnGame();
        }, 500);
        break;
      case 'fromOpeningdb':
        setTimeout(() => {
          this.fromOpeningdbGameCtrl.addFromOpeningdbGame();
        }, 500);
        break;
    }
  };

  openTaskTplUpdateModal = (partId: string, itemType: string, tplId: string) => {
    this.opts.task = this.homework[partId].filter(tpl => tpl.id === tplId)[0];
    this.partId = partId;
    this.itemType = itemType;
    this.initTask();
    this.showCreateModal = true;
    this.redraw();
  };

  closeTaskTplModal = () => {
    this.opts.task = null;
    this.closeTaskCreateModal();
  };

  removeTask = (partId: string, tplId: string) => {
    //xhr.removeTpl(tplId).then(() => {
      this.homework[partId] = this.homework[partId].filter(tpl => tpl.id !== tplId);
      this.redraw();
    //});
  };

  sortTask = (partId: string, tplIds: string[]) => {
    let oldTemplates = this.homework[partId];
    this.homework[partId] = tplIds.map((tplId) => {
      return oldTemplates.filter(tpl => tpl.id === tplId)[0];
    });
    this.redraw();
  };

  toggleExpand = (partId: string, tplId: string) => {
    this.homework[partId] = this.homework[partId].map((tpl) => {
      if(tpl.id === tplId) {
        tpl.expanded = !tpl.expanded;
        return tpl;
      } else {
        return tpl;
      }
    });
    this.redraw();
    window.lichess.pubsub.emit('content_loaded');
  };

  cloneToModal = () => {
    this.showCloneToModal = true;
    this.redraw();
    this.loadClazzes();
  };

  onCloneToModalClose = () => {
    this.showCloneToModal = false;
    this.clazzes = [];
    this.courses = [];
    this.redraw();
  };

  loadClazzes = () => {
    homeworkXhr.loadClazzes().then((data) => {
      this.clazzes = data;
      this.redraw();

      if(data && data.length > 0) {
        const courseId = this.clazzes[0].id;
        this.loadCourses(courseId);
      }
    });
  };

  loadCourses = (clazzId) => {
    homeworkXhr.loadCourses(clazzId).then((data) => {
      if(data && data.length > 0) {
        this.courses = data.filter(c => !c.homework);
      } else this.courses = [];

      this.filterCourses(true);
      this.redraw();
    });
  };

  filterCourses = (v) => {
    if(v) {
      let prev1Month = format(addMonths(Date.now(), -1), 'YYYY-MM-DD HH:mm');
      let next1Month = format(addMonths(Date.now(), 1), 'YYYY-MM-DD HH:mm');
      this.filteredCourses = this.courses.filter(c =>  c.date > prev1Month && c.date < next1Month);
    } else {
      this.filteredCourses = this.courses
    }
    this.redraw();
  };

  submitCloneTo = () => {
    let $form = $('.form-cloneTo');
    let clazzId = $form.find('#form-clazz').val();
    let courseId = $form.find('#form-course').val();
    homeworkXhr.cloneTo(this.homework.id, clazzId, courseId).then((data) => {
      if(data.ok) {
        this.onCloneToModalClose();
        location.href = `/clazz/homework/create?clazzId=${clazzId}&courseId=${courseId}`;
      } else alert('error')
    });
  };

  submitTask = () => {
    let $form = $('.taskForm');

    if(!this.submitValid($form)) {
      return;
    }

    if(!this.validSimilarTask()) {
      return;
    }

    if (this.itemType === 'themePuzzle') {
      if(this.themePuzzleCtrl.count < this.num) {
        let $num = $form.find('input[name="item.themePuzzle.num"]');
        let $parent = $num.parent('.form-group');
        $parent.addClass('is-invalid');
        $parent.prop('title',`目标值数量必须小于主题战术数量（${this.themePuzzleCtrl.count}）`);
        return;
      }
    }

    let data = $form.serialize();
    if(this.isUpdate()) {
      xhr.updateTpl(this.opts.task!.id, data).then((response) => {
        this.homework[this.partId] = this.homework[this.partId].map(tpl => {
          return (response.id === tpl.id) ? response : tpl;
        });
        this.closeTaskTplModal();
      });
    } else {
      xhr.createTpl(data).then((response) => {
        this.homework[this.partId].push(response);
        this.closeTaskTplModal();
      });
    }
  };

  validSimilarTask = () => {

    let buildItemSimilarId = (tpl: TaskTemplate) => {
      let similarId;
      switch (tpl.itemType.id) {
        case 'puzzle':
          similarId = tpl.itemType.id + ':' + tpl.item.puzzleWithResult!.puzzle.isNumber;
          break;
        case 'themePuzzle':
          similarId = tpl.itemType.id + ':' + tpl.item.themePuzzleWithResult!.themePuzzle.isNumber + ':' + tpl.item.themePuzzleWithResult!.themePuzzle.extra!.cond;
          break;
        case 'puzzleRush':
          similarId = tpl.itemType.id + ':' + tpl.item.puzzleRushWithResult!.puzzleRush.isNumber + ':' + tpl.item.puzzleRushWithResult!.puzzleRush.mode;
          break;
        case 'coordTrain':
          similarId = tpl.itemType.id + ':' + tpl.item.coordTrainWithResult!.coordTrain.isNumber + ':' + tpl.item.coordTrainWithResult!.coordTrain.extra!.cond;
          break;
        case 'game':
          similarId = tpl.itemType.id + ':' + tpl.item.gameWithResult!.game.isNumber + ':' + tpl.item.gameWithResult!.game.speed;
          break;
      }
      return similarId
    };

    if (!this.isUpdate() && this.partId === 'common') {
      let tpls = this.homework['common'];
      let similarIds: string[] = [];
      if (tpls) {
        similarIds = tpls.map(tpl => buildItemSimilarId(tpl));
      }

      let similarId;
      switch (this.itemType) {
        case 'puzzle':
          similarId = this.itemType + ':' + this.isNumber;
          break;
        case 'themePuzzle':
          similarId = this.itemType + ':' + this.isNumber + ':' + this.themePuzzleCtrl.themePuzzle.extra!.cond;
          break;
        case 'puzzleRush':
          similarId = this.itemType + ':' + this.isNumber + ':' + this.puzzleRushCtrl.puzzleRush.mode;
          break;
        case 'coordTrain':
          similarId = this.itemType + ':' + this.isNumber + ':' + this.coordTrainCtrl.coordTrain.extra!.cond;
          break;
        case 'game':
          similarId = this.itemType + ':' + this.isNumber + ':' + this.gameCtrl.game.speed;
          break;
      }

      if (similarIds.includes(similarId)) {
        alert('已经存在类似任务，请确认任务类型后再试');
        return false;
      }

      return true;
    }

    return true;
  };

  submitHomework = (method: string) => {
    let $form = $('.homework-form__form');
    let summary = $form.find('#form-summary').val();
    let prepare = $form.find('#form-prepare').val();
    let coinRule = $form.find('#form-coinRule').val();

    if(method === 'publish') {
      if(!summary && !summary && this.homework.common.length === 0 && this.homework.practice.length === 0) {
        alert('您不能发布一个空的课后练!');
        return;
      }

      if(!confirm('发布后将不可修改，是否继续？')) {
        return;
      }
    }

    let homeworkData = {
      deadlineAt: $form.find('#form-deadlineAt').val(),
      summary: summary,
      prepare: prepare,
    };

    if(coinRule) {
      homeworkData['coinRule'] = coinRule;
    }

    this.homework.common.forEach(function (tpl, i) {
      homeworkData[`common[${i}].taskItem`] = tpl.itemType.id;
      homeworkData[`common[${i}].taskTplId`] = tpl.id;
    });

    this.homework.practice.forEach(function (tpl, i) {
      homeworkData[`practice[${i}].taskItem`] = tpl.itemType.id;
      homeworkData[`practice[${i}].taskTplId`] = tpl.id;
    });

    if(method === 'publish') {
      homeworkXhr.updateAndPublish(this.homework.id, homeworkData).then(() => {
        alert('发布成功!');
        location.reload();
      }).fail(this.onXhrFail);
    } else {
      homeworkXhr.updateHomework(this.homework.id, homeworkData).then(() => {
        alert('保存成功!');
        location.reload();
      }).fail(this.onXhrFail);
    }
  };

  revokeHomework = () => {
    if(this.homework.id && this.homework.status.id === 20) {
      homeworkXhr.revokeValid(this.homework.id).then((data) => {
        if(data.exists) {
          if(confirm(`已经有 ${data.count} 名学员开始完成，撤回重新发布需要清除全部已完成数据。是否继续操作？`)) {
            homeworkXhr.revoke(this.homework.id).then(() => {
              alert('撤回成功!');
              location.reload();
            }).fail(this.onXhrFail);
          }
        } else {
          if(confirm('当前没有学员完成课后练，可以放心撤回。是否继续操作？')) {
            homeworkXhr.revoke(this.homework.id).then(() => {
              alert('撤回成功!');
              location.reload();
            }).fail(this.onXhrFail);
          }
        }
      }).fail(this.onXhrFail);
    }
  };

  onXhrFail = (d1, _, d3) => {
    if(d1 && d1.responseJSON) {
      alert(JSON.stringify(d1.responseJSON.error));
    } else {
      alert(d3);
    }
  };

}

