import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import throttle from 'common/throttle';
import {HomeworkOpts} from './interfaces';
import HomeworkCtrl from './ctrl';

export const patch = init([klass, attributes]);
import view from './view/main';

export function start(opts: HomeworkOpts) {

  let vnode: VNode;

  let redraw = throttle(100, () => {
    vnode = patch(vnode, view(ctrl));
  });

  const ctrl = new HomeworkCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}


