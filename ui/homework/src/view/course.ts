import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import HomeworkCtrl from '../ctrl';

export default function (ctrl: HomeworkCtrl): VNode {
  return h('table.homework__course', [
    h('tbody', [
      h('tr', [
        h('td', [
          h('label', '班级名称：'),
          h('a', {attrs: {href: `/clazz/${ctrl.clazz.id}`}}, [h('strong', ctrl.clazz.name)])
        ]),
        h('td', [
          h('label', '课节：'),
          h('strong', ctrl.course.index.toString())
        ]),
        h('td', [
          h('label', '上课时间：'),
          h('strong', `${ctrl.course.date}（${ctrl.course.week}）${ctrl.course.timeBegin}`)
        ]),
        h('td', [
          !!ctrl.opts.prevHomework ? h('a', {attrs: {target: '_blank', href: `/clazz/homework/create?clazzId=${ctrl.opts.prevHomework.clazzId}&courseId=${ctrl.opts.prevHomework.courseId}`}}, '上次课后练') : null
        ])
      ])
    ])
  ]);
}
