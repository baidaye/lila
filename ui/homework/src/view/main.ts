import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import HomeworkCtrl from '../ctrl';
import {bind} from '../util';
import readerCourse from './course'
import readerForm from './form'
import renderTaskModals from '../../../ttask/src/view/modals';
import {renderTaskTplCreateModal, renderDeadlineAtModal, readerCloneToModal} from './form'

export default function (ctrl: HomeworkCtrl): VNode {
  return h('main.page.page-small.box.box-pad.homework.homework-form', [
    h('div.box__top', [
      h('h1', ['新建课后练', !!ctrl.opts.cloneHomework ? h('div.cloneFrom', [
        '复制自',
        h('a', {attrs: {target: '_blank', href: `/clazz/homework/create?clazzId=${ctrl.opts.cloneHomework.clazzId}&courseId=${ctrl.opts.cloneHomework.courseId}`}}, ctrl.opts.cloneHomework.name ? ctrl.opts.cloneHomework.name : ctrl.opts.cloneHomework.id)
      ]) : null]),
      h('div.box-top__actions', [
        ctrl.isPublish() ? h('a.button.button-empty', {
          hook: bind('click', () => {
            ctrl.cloneToModal();
          })
        }, '复制') : null
      ])
    ]),
    readerCourse(ctrl),
    readerForm(ctrl),
    ctrl.showCreateModal ? renderTaskTplCreateModal(ctrl) : null,
    ctrl.showDeadlineAtModal ? renderDeadlineAtModal(ctrl) : null,
    ctrl.showCloneToModal ? readerCloneToModal(ctrl) : null
  ].concat(renderTaskModals(ctrl, ['showCreateModal'])));
}


