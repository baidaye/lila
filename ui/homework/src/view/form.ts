import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import HomeworkCtrl from '../ctrl';
import {bind, bindSubmit} from '../util';
import * as ModalBuild from '../../../ttask/src/view/modal';
import {settingContent} from '../../../ttask/src/view/form';
import {renderCapsules} from '../../../ttask/src/view/itemCapsulePuzzle';
import {renderReplayGames} from '../../../ttask/src/view/itemReplayGame';
import {renderRecallGames} from '../../../ttask/src/view/itemRecallGame';
import {renderDistinguishGames} from '../../../ttask/src/view/itemDistinguishGame';
import {renderFromPositions} from '../../../ttask/src/view/itemFromPositionGame';
import {renderFromPgns} from '../../../ttask/src/view/itemFromPgnGame';
import {renderFromOpeningdbs} from '../../../ttask/src/view/itemFromOpeningdbGame';
import {CommonItem, TaskTemplate} from '../../../ttask/src/interfaces';

export default function (ctrl: HomeworkCtrl): VNode {
  let hw = ctrl.homework;
  return h('div.homework-form__form', [
    h('div.form-split', [
      h('div.form-group.form-half', [
        h('label.form-label', {attrs: {for: 'form-deadlineAt'}}, '截止时间'),
        h('div.deadlineAt-warp', [
          h('input#form-deadlineAt.form-control.flatpickr', {
            hook: {
              insert(vnode) {
                let $el = $(vnode.elm as HTMLElement);
                (<any>$el).flatpickr({
                  disableMobile: 'true',
                  enableTime: true,
                  time_24hr: true,
                  minDate: 'today',
                  maxDate: new Date(Date.now() + 1000 * 3600 * 24 * 30)
                });

                $el.change(function () {
                  ctrl.deadlineAt = $el.val();
                });
              }
            },
            attrs: {name: 'deadlineAt', value: ctrl.deadlineAt, readonly: true}
          }),
          ctrl.isPublish() ? h('a.button.button-empty.small', {
            hook: bind('click', () => {
              ctrl.openDeadlineAtModal();
            })
          }, '修改') : null
        ])
      ]),
      !!ctrl.opts.team && ctrl.opts.team.coinSetting.open ? h('div.form-group.form-half', [
        h('label.form-label', {attrs: {for: 'form-coinRule'}}, ctrl.opts.team.coinSetting.name),
        h('input#form-coinRule.form-control', { attrs: {name: 'coinRule', type: 'number', min: 0, max: ctrl.opts.team.coinSetting.singleVal, value: hw.coinRule}}),
        h('small.form-help', '完成本次课后练所得积分，填0表示不记')
      ]) : null
    ]),
    h('div.form-split', [
      h('div.form-group.form-half', [
        h('label.form-label', {attrs: {for: 'form-summary'}}, '课节总结'),
        h('textarea#form-summary.form-control', {
          attrs: {name: 'summary', rows: 5, maxlength: 2048, placeholder: '课节总结'}
        }, hw.summary ? hw!.summary : '')
      ]),
      h('div.form-group.form-half', [
        h('label.form-label', {attrs: {for: 'form-prepare'}}, '预习内容'),
        h('textarea#form-prepare.form-control', {
          attrs: {name: 'prepare', rows: 5, maxlength: 2048, placeholder: '预习内容'}
        }, hw.prepare ? hw!.prepare : '')
      ])
    ]),
    taskPart(ctrl, 'common', '训练目标'),
    taskPart(ctrl, 'practice', '练习任务'),
    h('div.form-actions', [
      h('a.cancel', {attrs: {href: `/clazz/${ctrl.opts.clazz.id}#courses`}}, '取消'),
      h('div', [
        ctrl.isPublish() ? h('button.button.button-green.small.publish.disabled',   {attrs: {disabled: true}}, '已发布') : null,
        ctrl.isPublish() && !ctrl.isDeadline() ? h('button.button.button-red.small.revoke', {
          hook: bind('click', () => {
            ctrl.revokeHomework();
          })
        }, '撤回') : null,
        !ctrl.isPublish() ? h('button.button.button-green.small.publish', {
          hook: bind('click', () => {
            ctrl.submitHomework('publish');
          })
        }, '保存并发布') : null,
        !ctrl.isPublish() ? h('button.button.small', {
          hook: bind('click', () => {
            ctrl.submitHomework('update');
          })
        }, '保存') : null
      ])
    ])
  ]);
}

function taskPart(ctrl: HomeworkCtrl, partId: string, partName: string) {
  return h('div.form-group.task-part', [
    h('label.form-label', {attrs: {for: `form-${partId}`}}, `${partName}（最多${ctrl.opts.limit[partId]}项）`),
    h(`div#form-${partId}`, [
      !ctrl.isPublish() ? h('div.task-items', ctrl[`${partId}Items`].map(item => {
        return h('div.task-item', [
          h('button.button.small.item-button', {
            hook: bind('click', () => {
              ctrl.openTaskTplCreateModal(partId, item.id);
            })
          }, [
            h('span', [
              h('span', item.name),
              item.member ? h('span.vTip', {attrs: {title: '会员可用'}}, 'V') : null
            ]),
            h('span.split'),
            h('i', {attrs: {'data-icon': 'O'}})
          ])
        ])
      })) : null,
      h('div.task-block', [
        h('span.task-block-title', `${partName}`),
        h('table.task-items-table', [
          h('tbody', {
            hook: {
              insert(vnode) {
                vnode.data!.tr = {};
                sortTask(ctrl, partId, ctrl.homework[partId].length, vnode);
              },
              postpatch(old, vnode) {
                vnode.data!.tr = old.data!.tr;
                sortTask(ctrl, partId, ctrl.homework[partId].length, vnode);
              },
              destroy: vnode => {
                const sortable = vnode.data!.tr!.sortable;
                if (sortable) sortable.destroy()
              }
            }
          }, ctrl.homework[partId].map(function (tpl, index) {
            return partId === 'common' ? renderCommonTemplate(ctrl, partId, tpl) : renderPracticeTemplate(ctrl, partId, tpl, index)
          }))
        ])
      ])
    ])
  ])
}

function sortTask(ctrl: HomeworkCtrl, partId: string, len: number, vnode: VNode) {
  const vData = vnode.data!.tr!;
  const el = vnode.elm as HTMLElement;
  vData.count = len;
  if (!window.lichess.hasTouchEvents && len > 1 && !vData.sortable) {
    const makeSortable = function() {
      vData.sortable = window['Sortable'].create(el, {
        draggable: '.draggable',
        scrollSpeed: 5,
        onSort() {
          ctrl.sortTask(partId, vData.sortable.toArray());
        }
      });
    };
    if (window['Sortable']) makeSortable();
    else window.lichess.loadScript('javascripts/vendor/Sortable.min.js').done(makeSortable);
  }
}

function renderCommonTemplate(ctrl: HomeworkCtrl, partId: string, tpl: TaskTemplate) {
  let item = tpl.item[`${tpl.itemType.id}WithResult`][tpl.itemType.id] as CommonItem;
  let tags = item.tags ? item.tags : [];
  return h(`tr.task-line.sortline.task-${tpl.id}`, {
    attrs: { 'data-id': tpl.id },
    class: { draggable: !ctrl.isPublish() }
  }, [
    h('td.name', {attrs: {title: tpl.name}, class: {move: !ctrl.isPublish()}}, tpl.name),
    h('td.cond', {class: {move: !ctrl.isPublish()}}, [
      h('div.tags', tags.map( tag => {
          return h('span', tag)
        }
      ))
    ]),
    h(`td.actions1.${tpl.id}`, [
      !ctrl.isPublish() ? h('button.button.button-empty.small', {
        hook: bind('click', () => {
          ctrl.openTaskTplUpdateModal(partId, tpl.itemType.id, tpl.id);
        })
      }, '修改') : null,
      !ctrl.isPublish() ? h('button.button.button-empty.button-red.small', {
        hook: bind('click', () => {
          ctrl.removeTask(partId, tpl.id);
        })
      }, '移除') : null
    ])
  ]);
}

function renderPracticeTemplate(ctrl: HomeworkCtrl, partId: string, tpl: TaskTemplate, index: number) {
  let tags = [];
  return h(`tr.sortline.task-${tpl.id}`, {
    attrs: { 'data-id': tpl.id },
    class: { draggable: !ctrl.isPublish() }
  }, [
    h('td', [
      h('table.task-items-expand-table', [
        h('tbody', [
          h(`tr.task-line`, [
            h('td.name', {attrs: {title: tpl.name}, class: {move: !ctrl.isPublish()}}, `${index + 1}、${tpl.name}`),
            h('td.cond', {class: {move: !ctrl.isPublish()}}, [
              h('div.tags', tags.map( tag => {
                  return h('span', tag)
                }
              ))
            ]),
            h(`td.actions2.${tpl.id}`, [
              h('button.button.button-empty.small', {
                hook: bind('click', () => {
                  ctrl.toggleExpand(partId, tpl.id);
                })
              }, (!!tpl.expanded ? '收起' : '展开')),
              !ctrl.isPublish() ? h('button.button.button-empty.small', {
                hook: bind('click', () => {
                  ctrl.openTaskTplUpdateModal(partId, tpl.itemType.id, tpl.id);
                })
              }, '修改') : null,
              !ctrl.isPublish() ? h('button.button.button-empty.button-red.small', {
                hook: bind('click', () => {
                  ctrl.removeTask(partId, tpl.id);
                })
              }, '移除') : null
            ])
          ]),
          !!tpl.expanded ? h(`tr`, [
            h('td', {attrs: {colspan: 3}}, [
              h('div.expanded', [renderPracticeTemplateExpand(ctrl, tpl)])
            ])
          ]) : null
        ])
      ])
    ])
  ]);
}

function renderPracticeTemplateExpand(ctrl: HomeworkCtrl, tpl: TaskTemplate) {
  switch (tpl.itemType.id) {
    case 'capsulePuzzle':
      return h('div.item-capsulePuzzle', renderCapsules(ctrl, tpl.item.capsulePuzzleWithResult!.capsules, true));
    case 'replayGame':
      return h('div.item-replayGame', renderReplayGames(ctrl, tpl.item.replayGameWithResult!, true));
    case 'recallGame':
      return h('div.item-recallGame', renderRecallGames(ctrl, tpl.item.recallGameWithResult!, true));
    case 'distinguishGame':
      return h('div.item-distinguishGame', renderDistinguishGames(ctrl, tpl.item.distinguishGameWithResult!, true));
    case 'fromPosition':
      return h('div.item-fromPosition', renderFromPositions(ctrl, tpl.item.fromPositionWithResult!, true));
    case 'fromPgn':
      return h('div.item-fromPgn', renderFromPgns(ctrl, tpl.item.fromPgnWithResult!, true));
    case 'fromOpeningdb':
      return h('div.item-fromOpeningdb', renderFromOpeningdbs(ctrl, tpl.item.fromOpeningdbWithResult!, true));
    default:
      return null;
  }
}

export function renderTaskTplCreateModal(ctrl: HomeworkCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.closeTaskTplModal();
    },
    class: `taskCreateModal`,
    content: [
      h('h2', `${ctrl.itemTypeName()}${ctrl.isUpdate() ? '（修改）' : ''}`),
      h('div.modal-content-body', [
        h('form.form3.taskForm', {
          hook: bindSubmit(_ => {
            ctrl.submitTask();
          })
        }, [
          ctrl.isUpdate()? h('input', {attrs: {type: 'hidden', name: 'id', value: ctrl.opts.task!.id}}) : null,
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.id', value: ctrl.opts.sourceRel.id }}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.name', value: ctrl.opts.sourceRel.name}}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.source', value: ctrl.opts.sourceRel.source}}),
          h('input', {attrs: {type: 'hidden', name: 'itemType', value: ctrl.itemType}}),
          settingContent(ctrl),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                ctrl.closeTaskTplModal();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '保存')
          ])
        ])
      ])
    ]
  });
}

export function renderDeadlineAtModal(ctrl: HomeworkCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.closeDeadlineAtModal();
    },
    class: `taskCreateModal`,
    content: [
      h('h2', '修改截止时间'),
      h('div.modal-content-body', [
        h('form.form3.form-deadlineAt', {
          hook: bindSubmit(_ => {
            ctrl.updateDeadlineAt();
          })
        }, [
          h('div.form-split', [
            h('div.form-group', [
              h('label.form-label', {attrs: {for: 'deadlineAt'}}, '截止时间'),
              h('input#deadlineAt.form-control.flatpickr', {
                hook: {
                  insert(vnode) {
                    let $el = $(vnode.elm as HTMLElement);
                    (<any>$el).flatpickr({
                      disableMobile: 'true',
                      enableTime: true,
                      time_24hr: true,
                      minDate: 'today',
                      maxDate: new Date(Date.now() + 1000 * 3600 * 24 * 30)
                    });
                  }
                },
                attrs: {name: 'deadlineAt', value: ctrl.deadlineAt, readonly: true}
              })
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                ctrl.closeDeadlineAtModal();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '保存')
          ])
        ])
      ])
    ]
  });
}

export function readerCloneToModal(ctrl: HomeworkCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.onCloneToModalClose();
    },
    class: `taskCreateModal`,
    content: [
      h('h2', '复制到...'),
      h('div.modal-content-body', [
        h('form.form3.form-cloneTo', {
          hook: bindSubmit(_ => {
            ctrl.submitCloneTo();
          })
        }, [
          h('div.form-group', [
            h('label.form-label', {attrs: {for: 'form-clazz'}}, '选择班级'),
            h(`select#form-clazz.form-control`, {
              hook: bind('change', (e: Event) => {
                let v = (e.target as HTMLInputElement).value;
                ctrl.loadCourses(v);
              })
            }, ctrl.clazzes ? ctrl.clazzes.map(item => {
              return h('option', {attrs: {value: item.id }}, item.name)
            }) : [])
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: 'form-course'}}, '选择课节'),
            h(`select#form-course.form-control`, ctrl.filteredCourses ? ctrl.filteredCourses.map(item => {
              return h('option', {attrs: {value: item.id}}, `第${item.index}节 ${item.dateTime}`)
            }) : [])
          ]),
          h('div.form-group', [
            h('input', {
              attrs: {
                id: 'form-near1Month',
                type: 'checkbox',
                title: '最近1个月',
                checked: true
              },
              hook: bind('change', (e: Event) => {
                let c = (e.target as HTMLInputElement).checked;
                ctrl.filterCourses(c);
              })
            }),
            h('label', {attrs: {for: 'form-near1Month'}}, '最近1个月')
          ]),
          h('p', '注意：当前选择课节的课后练原有内容将被覆盖。'),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                ctrl.onCloneToModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit', disabled: (!ctrl.filteredCourses || ctrl.filteredCourses.length == 0)}, class: {disabled : (!ctrl.filteredCourses || ctrl.filteredCourses.length == 0)}}, '确认')
          ])
        ])
      ])
    ]
  });
}
