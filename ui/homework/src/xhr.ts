const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function updateHomework(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/update`,
    headers: headers,
    data: data
  });
}

export function updateAndPublish(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/updateAndPublish`,
    headers: headers,
    data: data
  });
}

export function revokeValid(id: string) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/revokeValid`,
    headers: headers
  });
}

export function revoke(id: string) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/revoke`,
    headers: headers
  });
}

export function updateDeadlineAt(id: string, deadlineAt: string) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/updateDeadline`,
    headers: headers,
    data: { deadlineAt }
  });
}

export function loadClazzes() {
  return $.ajax({
    url: `/clazz/allClazzes`,
    headers: headers
  });
}

export function loadCourses(clazzId: string) {
  return $.ajax({
    method: 'get',
    url: `/clazz/${clazzId}/allCourses`,
    headers: headers
  });
}

export function cloneTo(id: string, clazzId: string, courseId: string) {
  return $.ajax({
    method: 'post',
    url: `/clazz/homework/${id}/clone?clazzId=${clazzId}&courseId=${courseId}`,
    headers: headers,
  });
}

