import RoundController from './ctrl';

export const headers = {
  'Accept': 'application/vnd.lichess.v4+json'
};

export function reload(ctrl: RoundController) {
  return $.ajax({
    url: ctrl.data.url.round,
    headers
  }).fail(window.lichess.reload);
}

export function whatsNext(ctrl: RoundController) {
  return $.ajax({
    url: '/whats-next/' + ctrl.data.game.id + ctrl.data.player.id,
    headers
  });
}

export function challengeRematch(gameId: string) {
  return $.ajax({
    method: 'POST',
    url: '/challenge/rematch-of/' + gameId,
    headers
  });
}

export function getSimulApplicants(simulId: string) {
  return $.ajax({
    method: 'GET',
    url: '/simul/' + simulId + '/applicants',
    headers
  });
}

export function acceptSimulApplicant(simulId: string, userId: string) {
  return $.ajax({
    method: 'POST',
    url: '/simul/' + simulId + '/accept/' + userId,
    headers
  });
}

export function rejectSimulApplicant(simulId: string, userId: string) {
  return $.ajax({
    method: 'POST',
    url: '/simul/' + simulId + '/reject/' + userId,
    headers
  });
}

export function contestSign(gameId: string, contestId: string, isTeam: boolean) {
  let url = isTeam ? `/contest/team/${contestId}/board/setSign?boardId=${gameId}` : `/contest/board/setSign?id=${gameId}`;
  return $.ajax({
    method: 'POST',
    url: url,
    headers
  });
}
