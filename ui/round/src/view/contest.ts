import {h} from 'snabbdom'
import {VNode} from "snabbdom/vnode";
import RoundController from "../ctrl";
import {bind} from "../util";
import {PlayerUser} from "../../../game/interfaces";

export function signModal(ctrl: RoundController): VNode | undefined {
  let contest = ctrl.data.contest;
  if(!contest) return;

  let whitePlayer = ctrl.data.player.color === 'white' ? ctrl.data.player.user : ctrl.data.opponent.user;
  let blackPlayer = ctrl.data.player.color === 'black' ? ctrl.data.player.user : ctrl.data.opponent.user;

  let white = readerAnonymous();
  let black = readerAnonymous();

  if(whitePlayer && whitePlayer.id === contest.players.white.userId) {
    white = readerPlayer(whitePlayer);
  }

  if(blackPlayer && blackPlayer.id === contest.players.black.userId) {
    black = readerPlayer(blackPlayer);
  }

  const signed = ctrl.data.contest && ctrl.data!.contest.players[ctrl.data.player.color].signed;

  return h('div#modal-overlay', [
    h('div#modal-wrap.contestSign__modal', [
      h('div.contest-name', contest.name),
      h('div.contest-round', [
        h('span', `第${contest.round}轮`),
        h('span.board-date', contest.date)
      ]),
      h('div.contest-setup', contest.setup),
      h('div.contest-player', [ white, h('strong', 'Vs.'), black ]),
      h('div.action', [
        ctrl.startContestSign ? h('button.button.small', {
            class: {
              'button-green': !!signed
            },
            hook: bind('click', () => {
              ctrl.contestSign();
            })
          }, !!signed ? '√ 已准备（签到）' : '准备（签到）') : h('div.startSign', [
            h('span', '准备（签到）等待时间：'),
            h('span.clock', {
              hook: startClock(ctrl)
            }, [
              h('span.time')
            ])
        ])
      ])
    ])
  ]);
}

function startClock(ctrl: RoundController) {
  let option = {
    time: ctrl.data.contest ? Math.max(ctrl.data.contest.secondsToStart - 60, 0) : 0,
    stopped: () => {
      ctrl.onContestStartSign();
    }
  };
  return {
    insert: vnode => $(vnode.elm as HTMLElement).clock(option)
  };
}

function readerPlayer(player: PlayerUser): VNode {
  return h('a.user-link.ulpt', {
    attrs: { href: `/@/${player.username}`},
    class: { online: player.online, offline: !player.online }
  }, [
    h('div.head-line', [
      h('i.line')
    ]),
    player.title ? h('span.title', player.title == 'BOT' ? { attrs: { 'data-bot': true } } : {}, player.title) : null,
    h('span.u_name', player.username)
  ]);
}

function readerAnonymous() {
  return h('a.user-link', 'Anonymous');
}
