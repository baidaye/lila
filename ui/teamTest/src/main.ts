import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import {Chessground} from 'chessground';
import {menuHover} from 'common/menuHover';
import {TestOpts} from './interfaces';
import TestCtrl from './ctrl';

menuHover();

const patch = init([klass, attributes]);

import view from './view/main';

export function start(opts: TestOpts) {

  let vnode: VNode;

  function redraw() {
    vnode = patch(vnode, view(ctrl));
  }

  const ctrl = new TestCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}

// that's for the rest of lichess to access chessground
// without having to include it a second time
window.Chessground = Chessground;

