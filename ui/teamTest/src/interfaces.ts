import * as cg from 'chessground/types';
import {Pref} from "../../round/src/interfaces";


export interface TestOpts {
  data: TestData;
  pref: Pref;
  userId: string;
  socketSend: SocketSend;
  element: Element;
}

export interface TestData {
  id: string;
  studentId: string;
  studentMarkOrName?: string;
  tplId: string;
  teamId: string;
  remainsTime: number; // seconds
  spendTime: number; // minutes
  items: TestStudentItems;
  rightQ: number;
  inviteId: string;
  deadline: string; // yyyy-MM-dd HH:mm:ss
  tpl: LightTestTemplate;
  status: Status;
  startAt?: string; // yyyy-MM-dd HH:mm:ss
  finishAt?: string; // yyyy-MM-dd HH:mm:ss
  isPassed?: boolean;
}

export interface TestStudentItems {
  puzzles: MiniPuzzleWithResult[];
}

export interface MiniPuzzleWithResult {
  puzzle: MiniPuzzle;
  result?: PuzzleResult;
  retry: number;
}

export interface MiniPuzzle {
  id: string;
  fen: string;
  color: cg.Color;
  lines: any;
  depth: number;
  lastMove?: string;
}

export interface PuzzleResult {
  win: boolean;
  completed: boolean;
  moves: PuzzleMove[];
  startActive?: boolean;
}

export interface PuzzleMove {
  index: number;
  white?: PuzzleNode;
  black?: PuzzleNode;
}

export interface PuzzleNode {
  san: string;
  uci: string;
  fen: string;
  lastMove: string;
  active: boolean
}

export interface LightTestTemplate {
  id: string;
  name: string;
  limitTime: number; // minutes
  nbQ: number;
  passedQ: number;
  maxRetry: number;
  desc?: string;
}

export interface Status {
  id: string;
  name: string;
}

export type Redraw = () => void;

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}
