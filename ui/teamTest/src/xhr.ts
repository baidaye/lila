const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function start(id) {
  return $.ajax({
    method: 'post',
    url: `/team/test/stu/${id}/start`,
    headers: headers,
    data: {}
  });
}

export function finish(id) {
  return $.ajax({
    method: 'post',
    url: `/team/test/stu/${id}/finish`,
    headers: headers,
    data: {}
  });
}

export function finishResult(id) {
  return $.ajax({
    method: 'get',
    url: `/team/test/stu/${id}/finishResult`,
    headers: headers,
    data: {}
  });
}

export function redo(id, data) {
  return $.ajax({
    method: 'post',
    url: `/team/test/stu/${id}/redo`,
    headers: headers,
    data: data
  });
}

export function round(id, data) {
  return $.ajax({
    method: 'post',
    url: `/team/test/stu/${id}/round`,
    headers: headers,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data)
  });
}
