import TestCtrl from './ctrl';

interface Handlers {
  [key: string]: (data: any) => void;
}

export default class Socket {

  send: SocketSend;
  handlers: Handlers;
  connected = () => true;

  constructor(send: SocketSend, ctrl: TestCtrl) {
    this.send = send;
    this.handlers = {
      stuTestAborted(id) {
        if(id === ctrl.data.id) {
          ctrl.finish();
        }
      },
    };
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  };

};
