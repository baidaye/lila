import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import * as gridHacks from './gridHacks';
import {view as renderPromotion} from './promotion';
import * as ground from './ground';
import side from './side';
import clock from './clock';
import moves from './moves';
import navs from './navs';
import control from './control';
import TestCtrl from '../ctrl';

export default function (ctrl: TestCtrl): VNode {
  return h('main.teamTest', {
    hook: {
      postpatch(_, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
      }
    }
  }, [
    side(ctrl),
    navs(ctrl),
    h('div.teamTest__board.main-board', [
      ground.render(ctrl),
      renderPromotion(ctrl),
    ]),
    control(ctrl),
    moves(ctrl),
    clock(ctrl)
  ]);
}

