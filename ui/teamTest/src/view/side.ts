import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import TestCtrl from '../ctrl';

export default function (ctrl: TestCtrl): VNode {
  return h('div.teamTest__side', [
    h('div.meta', [
      ctrl.isFinished() ? finishedMeta(ctrl) : defaultMeta(ctrl)
    ])
  ]);
}

function finishedMeta(ctrl: TestCtrl) {
  let tpl = ctrl.data.tpl;
  return h('table.tpl-desc', [
    h('tr', [
      h('td', {attrs: {colspan: 2}}, [
        h('div.name', {attrs: {title: tpl.name}}, ctrl.opts.userId === ctrl.data.studentId ? tpl.name : `${ctrl.data.studentMarkOrName} · ${tpl.name}`)
      ])
    ]),
    h('tr', [
      h('td.label', '用时：'),
      h('td', `${ctrl.data.spendTime}/${tpl.limitTime} 分钟`)
    ]),
    h('tr', [
      h('td.label', '正确题目：'),
      h('td', `${ctrl.data.rightQ}/${tpl.nbQ}`)
    ]),
    h('tr', [
      h('td.label', '达标数量：'),
      h('td', `${tpl.passedQ}`)
    ]),
    h('tr', [
      h('td.label', '状态：'),
      h('td.state', {class : {passed: !!ctrl.data.isPassed}}, `${ctrl.data.isPassed ? '已通过' : '未通过'}`)
    ])
  ]);
}

function defaultMeta(ctrl: TestCtrl) {
  let tpl = ctrl.data.tpl;
  return h('table.tpl-desc', [
    h('tr', [
      h('td', {attrs: {colspan: 2}}, [
        h('div.name', {attrs: {title: tpl.name}}, tpl.name)
      ])
    ]),
    h('tr', [
      h('td.label', '限时：'),
      h('td', `${tpl.limitTime}分钟`)
    ]),
    h('tr', [
      h('td.label', '题目数量：'),
      h('td', `${tpl.nbQ}`)
    ]),
    h('tr', [
      h('td.label', '达标数量：'),
      h('td', `${tpl.passedQ}`)
    ])
  ]);
}


