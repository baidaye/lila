import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import TestCtrl from '../ctrl';
import {bind} from './util';

export default function (ctrl: TestCtrl): VNode {
  return h('div.teamTest__control', [
    h('button.button', {
      attrs: {
        disabled: ctrl.pzNo === 0
      },
      class: {
        disabled: ctrl.pzNo === 0
      },
      hook: bind('click', () => {
        ctrl.changePuzzle(ctrl.pzNo - 1);
      })
    }, '上一题'),
    h('span.puzNo', `${ctrl.pzNo + 1}/${ctrl.data.tpl.nbQ}, 待完成 ${ctrl.uncompleted()}`),
    (!ctrl.isFinished() && ctrl.data.tpl.maxRetry > 0) ? h('div.right', [
      h('button.button.button-green', {
        attrs: {
          disabled: !ctrl.isStarted() || !ctrl.pzWr.result || !ctrl.pzWr.result!.completed || ctrl.pzWr.retry >= ctrl.data.tpl.maxRetry
        },
        class: {
          disabled: !ctrl.isStarted() || !ctrl.pzWr.result || !ctrl.pzWr.result!.completed || ctrl.pzWr.retry >= ctrl.data.tpl.maxRetry
        },
        hook: bind('click', () => {
          ctrl.redo();
        })
      }, '重做')
    ]) : null,
    h('button.button', {
      attrs: {
        disabled: ctrl.pzNo === ctrl.data.items.puzzles.length - 1
      },
      class: {
        disabled: ctrl.pzNo === ctrl.data.items.puzzles.length - 1
      },
      hook: bind('click', () => {
        ctrl.changePuzzle(ctrl.pzNo + 1);
      })
    }, '下一题')
  ])
}
