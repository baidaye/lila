import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import TestCtrl from '../ctrl';
import {bind} from './util';

export default function (ctrl: TestCtrl): VNode | undefined {
    if(ctrl.isCreated() || ctrl.isStarted()) {
      return h('div.teamTest__clock', [
        h('div.clock', {
          hook: {
            insert(vnode) {
              ctrl.start(vnode.elm as HTMLElement);
            },
          }
        }, [h('span.time')]),
        h('button.button.submit', {
          hook: bind('click', () => {
            ctrl.submitTest();
          })
        }, '提交测评答案')
      ])
    }
}

