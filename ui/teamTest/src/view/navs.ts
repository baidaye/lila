import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {Chessground} from 'chessground';
import TestCtrl from '../ctrl';
import {bind} from './util';

export default function (ctrl: TestCtrl): VNode {
  return h('div.teamTest__navs', [
    h('div.puzzles-title', '全部题目'),
    h('div.puzzles-warp', [
      h('div.puzzles', ctrl.data.items.puzzles.map((pz, i) => {
        return h(`div.board-nav.board-nav-${i}`, {
          hook: bind('click', () => {
            ctrl.changePuzzle(i);
          })
        }, [
          h('div.board-mask', {
            class: {
              selected: ctrl.pzNo === i,
              completed: (!!pz.result && pz.result.completed),
              win: (ctrl.isFinished() && !!pz.result && pz.result.completed && pz.result.win),
              error: (ctrl.isFinished() && !!pz.result && pz.result.completed && !pz.result.win)
            }
          }, [
            h('span.no', `${i + 1}`)
          ]),
          h('div.mini-board.cg-wrap.is2d', {
            hook: {
              insert(vnode) {
                let el = vnode.elm as HTMLElement;
                let puzzle = pz.puzzle;
                Chessground(el, {
                  coordinates: false,
                  drawable: {enabled: false, visible: false},
                  resizable: false,
                  viewOnly: true,
                  orientation: puzzle.color,
                  fen: puzzle.fen,
                  lastMove: ctrl.uciToLastMove(puzzle.lastMove)
                });
              }
            }
          })
        ])
      }))
    ])
  ])
}
