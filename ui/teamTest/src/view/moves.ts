import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import TestCtrl from '../ctrl';
import {bind} from './util';

export default function (ctrl: TestCtrl): VNode {
  let result = ctrl.pzWr.result;
  return h('div.teamTest__moves', [
    h('div.moves-area', [
      h('div.move-head', [
        h('div.tabs', [
          h('span.tab.mine', {
            class: {
              active: ctrl.moveTab === 'mine'
            },
            hook: bind('click', () => {
              ctrl.changePuzzle(ctrl.pzNo);
              ctrl.moveTab = 'mine';
              ctrl.redraw();
            })
          }, '我的答案'),
          ctrl.isFinished() ? h('span.tab.right', {
            class: {
              active: ctrl.moveTab === 'right'
            },
            hook: bind('click', () => {
              ctrl.changePuzzle(ctrl.pzNo, true, ctrl.getRightPzWr());
              ctrl.moveTab = 'right';
              ctrl.redraw();
            })
          },'正确答案') : null
        ]),
        h('span', {
          class: {
            completed: !!result && result.completed
          }
        }, `${result ? result.completed ? '已完成' : '请继续' : '未完成'}`)
      ]),
      h('div.move-body', [
        h('div.panel.mine', {
          class: {
            none: ctrl.moveTab !== 'mine'
          }
        }, [ moves(ctrl), intro(ctrl), resultTip(ctrl) ]),
        ctrl.isFinished() ? h('div.panel.right', {
          class: {
            none: ctrl.moveTab !== 'right'
          }
        }, [ moves(ctrl) ]) : null
      ])
    ])
  ]);
}

function moves(ctrl: TestCtrl) {
  let puzzle = ctrl.pzWr.puzzle;
  let result = ctrl.pzWr.result;
  return result ? h(`div.moves.${puzzle.id}`, [
    h('div.move', h('span.san', {
      class: {
        active: result && !!result.startActive,
      },
      hook: bind('click', () => {
        ctrl.moveToStart();
      })
    }, '起始局面')),
    ...result.moves.map((m, index) => {
      return h('div.move', [
        h('span.index', `${m.index}.`),
        m.white ? h(`span.san`, {
          class: {
            active: m.white.active,
            answer: puzzle.color === 'white'
          },
          hook: bind('click', () => {
            ctrl.moveTo(index, 'white');
          })
        }, m.white.san) : '..',
        m.black ? h(`span.san`, {
          class: {
            active: m.black.active,
            answer: puzzle.color === 'black'
          },
          hook: bind('click', () => {
            ctrl.moveTo(index, 'black');
          })
        }, m.black.san) : null
      ])
    })
  ]) : null
}

function intro(ctrl: TestCtrl) {
  let puzzle = ctrl.pzWr.puzzle;
  let result = ctrl.pzWr.result;
  return !result && !ctrl.isFinished() ? h('div.intro', [
    h('div.no-square', h('piece.king.' + puzzle.color)),
    h('div.info', [
      h('strong', '该你走'),
      h('em', `请完成${puzzle.color === 'white' ? '白方' : '黑方'}的最佳着法`)
    ])
  ]) : null
}

function resultTip(ctrl: TestCtrl) {
  let result = ctrl.pzWr.result;
  return ctrl.isFinished() ? h('div.result', [
    h('span', {
      class: {
        win: !!result && result.win,
        error: !!result && !result.win
      }
    }, (result && result!.completed) ? (result.win ? '正确' : '错误') : '未完成')
  ]) : null
}
