import Socket from './socket';
import * as ChessJS from 'chess.js';
import * as chessUtil from 'chess';
import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import {Pref} from '../../round/src/interfaces';
import {sound} from './sound';
import * as xhr from './xhr';
import {decodeLines} from './util';
import * as promotion from './view/promotion';
import {DrawShape} from 'chessground/draw';
import {
  TestOpts,
  TestData,
  Redraw,
  PuzzleMove,
  JustCaptured, MiniPuzzleWithResult
} from './interfaces';


export default class TestCtrl {

  opts: TestOpts;
  data: TestData;
  pref: Pref;
  socket: Socket;
  chess: any; // chess.js
  chessground: CgApi;
  cgConfig: CgConfig;
  clockElm: HTMLElement;

  // current states
  pzNo: number = 0;
  pzWr: MiniPuzzleWithResult;
  mvList: any[] = [];
  lastMove: cg.Key[] | undefined = undefined;
  moveTab: string = 'mine';

  constructor(opts: TestOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.data = opts.data;
    this.pref = opts.pref;
    this.socket = new Socket(opts.socketSend, this);
    this.chess = ChessJS.Chess();
    this.changePuzzle(0, false);
  }

  changePuzzle = (no: number, rd: boolean = true, pzWr?: MiniPuzzleWithResult) => {
    this.pzNo = no;
    this.pzWr = pzWr ? pzWr : this.data.items.puzzles[no];
    this.moveTab = pzWr ? this.moveTab : 'mine';
    this.initData();

    if (rd) {
      if (this.pzWr.result && this.pzWr.result.moves.length > 0) {
        let moves = this.pzWr.result.moves;
        let maxIdx = moves.length - 1;
        this.moveTo(maxIdx, moves[maxIdx]['black'] ? 'black' : 'white', true);
      }
      this.redraw();
    }

    setTimeout(() => {
      this.scrollTop();
    }, 200);
  };

  initData = () => {
    this.mvList = [];
    this.lastMove = this.uciToLastMove(this.pzWr.puzzle.lastMove);
    this.chess.load(this.pzWr.puzzle.fen);
    this.showGround();
  };

  scrollTop = () => {
    let outerEl = $('.puzzles-warp')[0] as HTMLElement;
    let innerEl = $(`.board-nav-${this.pzNo}`)[0] as HTMLElement;
    outerEl.scrollTop = innerEl.offsetTop;
  };

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  showGround = (): void => {
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
      cg.setAutoShapes([]);
    });
  };

  setAutoShapes = (shapes: DrawShape[]): void => {
    this.withCg(cg => cg.setAutoShapes(shapes));
  };

  makeCgOpts = (): CgConfig => {
    const fen = this.chess.fen();
    const color = (this.chess.turn() === 'w') ? 'white' : 'black';
    const check = this.chess.in_check();
    const dests = this.toDests();
    const viewOnly = (!!this.pzWr.result && this.pzWr.result.completed) || !this.isStarted();
    const config: CgConfig = {
      fen: fen,
      turnColor: color,
      movable: {
        color: color,
        dests: viewOnly ? {} : dests
      },
      check: check,
      lastMove: this.lastMove,
      drawable: {
        enabled: false,
        autoShapes: []
      }
    };
    if (!dests && !check) {
      config.turnColor = opposite(color);
    }
    config.orientation = this.pzWr.puzzle.color;
    this.cgConfig = config;
    return config;
  };

  uciToLastMove = (uci: string | undefined): [cg.Key, cg.Key] | undefined => {
    return uci ? [uci.substr(0, 2) as cg.Key, uci.substr(2, 2) as cg.Key] : undefined;
  };

  toDests = (): cg.Dests => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({
        square: s,
        verbose: true
      });
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests;
  };

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    if (!promotion.start(this, orig, dest, capture, this.sendMove)) {
      this.sendMove(orig, dest, capture);
    }
  };

  // @ts-ignore
  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    this.jump({
      from: orig,
      to: dest,
      promotion: prom ? chessUtil.roleToSan[prom].toLowerCase() : null,
      sloppy: true
    });

    let progress = this.nextProgress();
    if (progress) {
      if (progress.completed) {
        this.pzWr.result!.completed = progress.completed;
        this.pzWr.result!.win = progress.win;
        this.data.items.puzzles[this.pzNo].result = this.pzWr.result;

        let roundData = {...this.lightPuzzle(), ...this.pzWr.result};
        xhr.round(this.data.id, roundData).then(() => {
          this.redraw();
          this.showGround();
          this.setLastShape();
        });
      } else {
        setTimeout(() => {
          this.jump(progress.move);
        }, 500);
      }
    } else {
      alert(`题目解析异常: ${this.pzWr.puzzle.id}`);
    }
  };

  jump = (move: any) => {
    let m = this.chess.move(move);

    this.mvList.push({
      san: m.san,
      uci: `${m.from}${m.to}${m.promotion ? m.promotion : ''}`,
      fen: this.chess.fen(),
      color: (m.color === 'w') ? 'white' : 'black',
      checkmate: this.chess.in_checkmate(),
      lastMove: `${m.from}${m.to}`
    });
    this.lastMove = [m.from, m.to];
    this.showGround();

    if (this.pzWr.result) {
      this.pzWr.result!.moves = this.buildMoves(this.mvList);
    } else {
      this.pzWr.result = {
        win: false,
        completed: false,
        moves: this.buildMoves(this.mvList)
      }
    }
    this.redraw();

    if (m.san.includes('x')) sound.capture();
    else sound.move();
    if (/[+#]/.test(m.san)) sound.check();
  };

  nextProgress = () => {
    let progress = decodeLines(this.pzWr.puzzle.lines);
    let result;
    for (let i in this.mvList) {
      let node = this.mvList[i];
      if (node.checkmate) {
        result = {
          completed: true,
          win: true
        };
        break;
      }

      // @ts-ignore
      let nextProgress = progress[node.uci];
      let nextProgressType = typeof nextProgress;
      if (nextProgressType === 'undefined') {
        result = {
          completed: true,
          win: false
        };
        break;
      } else if (nextProgressType === 'boolean') {
        result = {
          completed: true,
          win: nextProgress
        };
        break;
      } else if (nextProgressType === 'object') {
        progress = nextProgress;
      }
    }

    if (!result) {
      // 最后一步是对方走，忽略这步
      let pz = this.pzWr.puzzle;
      if (this.mvList.filter(m => m.color === pz.color).length >= pz.depth) {
        result = {
          completed: true,
          win: true
        };
      } else {
        let nextUci = Object.keys(progress)[0];
        let opponentUci = chessUtil.decomposeUci(nextUci);
        let promotion = opponentUci[2] ? opponentUci[2] : null;
        result = {
          completed: false,
          win: null,
          move: {
            from: opponentUci[0],
            to: opponentUci[1],
            promotion: promotion,
            sloppy: true
          }
        };
      }
    }
    return result;
  };

  buildMoves = (mvList): PuzzleMove[] => {
    let result: any[] = [];
    let move;
    mvList.forEach((m, index) => {
      let node = {
        san: m.san,
        uci: m.uci,
        fen: m.fen,
        lastMove: m.lastMove
      };

      if (m.color === 'white') {
        move = {
          index: result.length + 1,
          white: node
        }
      } else {
        if (!move) {
          move = {
            index: result.length + 1,
            black: node
          }
        } else {
          move.black = node;
        }
      }

      if (index === mvList.length - 1 || m.color === 'black') {
        result.push(move);
        move = undefined;
      }
    });
    return result;
  };

  moveTo = (index: number, color: cg.Color, isLast: boolean = false) => {
    let result = this.pzWr.result;
    if (result && result.completed) {
      let moves = result.moves;
      let node = moves[index][color];
      if (node) {
        this.lastMove = this.uciToLastMove(node.lastMove);
        this.chess.load(node.fen);
        this.showGround();
        if (isLast || (index == moves.length - 1 && (color === 'black' || (color === 'white' && !moves[index]['black'])))) {
          this.setLastShape();
        }

        this.pzWr.result!.startActive = false;
        moves.forEach((_, idx) => {
          let colors = ['white', 'black'];
          colors.forEach(c => {
            if (this.pzWr.result!.moves[idx][c]) {
              this.pzWr.result!.moves[idx][c]!.active = (index === idx && color === c);
            }
          });
        });
        this.redraw();
      }
    }
  };

  moveToStart = () => {
    let puzzle = this.pzWr.puzzle;
    let result = this.pzWr.result;
    if (result && result.completed) {
      this.lastMove = this.uciToLastMove(puzzle.lastMove);
      this.chess.load(puzzle.fen);
      this.showGround();

      result.moves.forEach((_, idx) => {
        let colors = ['white', 'black'];
        colors.forEach(c => {
          if (this.pzWr.result!.moves[idx][c]) {
            this.pzWr.result!.moves[idx][c]!.active = false;
          }
        });
      });
      this.pzWr.result!.startActive = true;
      this.redraw();
    }
  };

  setLastShape = () => {
    if (this.lastMove) {
      this.setAutoShapes([
        {
          orig: this.lastMove[0],
          dest: this.lastMove[1],
          brush: 'blue'
        }
      ])
    }
  };

  uncompleted = () => {
    return this.data.items.puzzles.filter(p => !p.result || !p.result.completed).length;
  };

  start = (ele: HTMLElement) => {
    this.clockElm = ele;
    if (this.isCreated()) {
      xhr.start(this.data.id).then(() => {
        this.data.status = {
          id: 'started',
          name: '考核中'
        };
        this.startClock();
        this.showGround();
        this.redraw();
      });
    } else if (this.isStarted()) {
      this.startClock();
    }
  };

  startClock = () => {
    let option = {
      time: this.data.remainsTime,
      stopped: () => {
        this.finish();
      }
    };
    $(this.clockElm).clock(option);
  };

  stopClock = () => {
    $(this.clockElm).clock('destroy');
  };

  finish = () => {
    if (this.isStarted()) {
      this.stopClock();
      xhr.finish(this.data.id).then(() => {
        this.showFinish();
      });
    }
  };

  showFinish = () => {
    xhr.finishResult(this.data.id).then((res) => {
      let modalHtml =
        `
         <div class="finish-modal">
            <h3>测评结束</h3>
           <div class="header">
             <i class="${res.isPassed ? 'success' : 'failed'}" data-icon="${res.isPassed ? 'E' : 'L'}"></i>
             <span class="${res.isPassed ? 'success' : 'failed'}">${res.isPassed ? '恭喜您通过本次测评！' : '很遗憾，您未通过本次测评'}</span>
           </div>
           <div class="body">
              <div>
                <label>用时：</label>
                <span>${res.spendTime}分钟</span>
              </div>
              <div>
                <label>通过标准：</label>
                <span>${res.rightQ}/${res.passedQ}</span>
              </div>
           </div>
           <div class="footer">
              <a class="button button-empty small" href="/team/test/stu/${this.data.id}/score">查看详情</a>
           </div>
         </div>`;
      $.modal($(modalHtml), 'team-test', () => {
        location.href = `/team/test/stu/${this.data.id}/score`;
      });
    });
  };

  submitTest = () => {
    let nb = this.uncompleted();
    let message = `${nb > 0 ? `还有 ${nb} 未完成题目，` : ''}是否确认提交测评？`;
    if (confirm(message)) {
      this.finish();
    }
  };

  redo = () => {
    if (this.isStarted()) {
      if (this.pzWr.result && this.pzWr.result.completed) {
        if(this.pzWr.retry >= this.data.tpl.maxRetry) {
          return;
        }

        if (confirm('确认重做此题？')) {
          xhr.redo(this.data.id, this.lightPuzzle()).then(() => {
            this.pzWr.result = undefined;
            this.pzWr.retry = this.pzWr.retry + 1;
            this.data.items.puzzles[this.pzNo].result = undefined;
            this.data.items.puzzles[this.pzNo].retry = this.pzWr.retry + 1;
            this.initData();
            this.redraw();
          });
        }
      }
    }
  };

  lightPuzzle = () => {
    return {id: this.pzWr.puzzle.id, index: this.pzNo};
  };

  getRightPzWr = () => {
    let pzWr = {
      puzzle: this.pzWr.puzzle,
      result: {
        win: true,
        completed: true,
        startActive: true,
        moves: this.buildRightMoves()
      },
      retry: this.data.tpl.maxRetry
    };
    return pzWr;
  };

  buildRightMoves = () => {
    let puzzle = this.pzWr.puzzle;
    let lines = decodeLines(puzzle.lines);
    const chess = ChessJS.Chess();
    chess.load(puzzle.fen);
    let mvList = [];
    this.buildRightMvList(chess, mvList, puzzle, lines);
    return this.buildMoves(mvList);
  };

  buildRightMvList = (chess, mvList, pz, lines) => {
    let keys = Object.keys(lines);
    if (keys.length > 0) {
      let nextUci = keys[0];
      let decomposeUci = chessUtil.decomposeUci(nextUci);
      let promotion = decomposeUci[2] ? decomposeUci[2] : null;
      let m = chess.move({
        from: decomposeUci[0],
        to: decomposeUci[1],
        promotion: promotion,
        sloppy: true
      });

      mvList.push({
        san: m.san,
        uci: `${m.from}${m.to}${m.promotion ? m.promotion : ''}`,
        fen: chess.fen(),
        color: (m.color === 'w') ? 'white' : 'black',
        lastMove: `${m.from}${m.to}`
      });

      let nextLines = lines[nextUci];
      if (typeof nextLines === 'object' && mvList.filter(m => m.color === pz.color).length < pz.depth) {
        this.buildRightMvList(chess, mvList, pz, nextLines);
      }
    }
  };

  isCreated = () => this.data.status.id === 'created';
  isStarted = () => this.data.status.id === 'started';
  isFinished = () => this.data.status.id === 'finished';

}
