import {VNode} from 'snabbdom/vnode'
import {Pref} from '../../round/src/interfaces';
import {Task, ClassicGame, Team} from '../../ttask/src/interfaces';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface TrainCourseOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  notAccept: boolean;
  clazzes: Clazz[]
  trainCourse: TrainCourse;
  students: Student[];
  pref: Pref;
  chat: any;
  themePuzzle: any;
  classicGames: ClassicGame[];
  team: Team;
  gameTasks: TrainGameTask[];
}

export interface Clazz {
  id: string;
  name: string;
  studentIds: string[];
}

export interface TrainCourse {
  id: string;
  name: string;
  studentIds: string[];
  max: number;
  date: string;
  timeBegin: string;
  timeEnd: string;
  status: TrainCourseStatus;
  startAt: string;
  stopAt: string;
  taskDeadlineAt: string;
}

export interface TrainCourseStatus {
  id: string;
  name: string;
}

export interface TrainGameTask extends Task {
  trainGameWithResult: TrainGameWithResult
}

export interface TrainGameWithResult {
  trainGame: TrainGameItem;
  result?: GameTaskResult;
}

export interface TrainGameItem {
  gameId: string;
  name: string;
  white: Player;
  black: Player;
  clock: Clock;
  speed: GameTaskSpeed;
  variant: Variant;
  rated: boolean;
  fen: string;
  color: Color;
  turnColor: Color;
  lastMove: string;
  remainClock: {
    white: number;
    black: number;
  };
  status: number; // 对局状态
  isLive: boolean;
  pause: boolean;
}

export interface GameTaskSpeed {
  key: string;
  name: string;
}

export interface Clock {
  time: string;
  increment: number;
}

export interface GameTaskResult {
  winner?: Color;
  finishAt: number;
}

export interface Student extends LightUser {
  online: boolean;
  isPlaying: boolean;
  mark?: string;
  status: SignStatus;
}

export interface SignStatus {
  id: string;
  name: string;
}

export interface Player extends LightUser {
  online: boolean;
  isPlaying: boolean;
  mark?: string;
}

export type Redraw = () => void;

