import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import CoachCtrl from '../ctrl';
import {onInsert} from '../util';

export default function(ctrl: CoachCtrl): VNode {
  return h('div.trainCourse-coach__side', [
    h('div.chat', [
      h('div.trainCourseTop', [
        h('div.title', '聊天互动')
      ]),
      h('section.mchat', {
        hook: onInsert(_ => {
          if (ctrl.opts.chat.instance) ctrl.opts.chat.instance.destroy();
          ctrl.opts.chat.parseMoves = true;
          window.lichess.makeChat(ctrl.opts.chat, chat => {
            ctrl.opts.chat.instance = chat;
          });
        })
      })
    ])
  ]);
}
