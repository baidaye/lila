import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import readerTop from './top'
import readerSide from './side'
import readerStudent from './student'
import readerTask from './task'
import readerBottom from './bottom'
import renderTaskModals from '../../../ttask/src/view/modals';
import {trainGameTaskModal as renderTrainGameTaskModal} from './trainGame';
import {studentModal as renderStudentModal, studentAddModal as renderStudentAddModal} from './student';
import CoachCtrl from '../ctrl';

export default function (ctrl: CoachCtrl): VNode {
  return h('main.page.trainCourse-coach', [
    readerTop(ctrl),
    readerSide(ctrl),
    readerStudent(ctrl),
    readerTask(ctrl),
    readerBottom(ctrl),
    ctrl.studentModalShow ? renderStudentModal(ctrl) : null,
    ctrl.studentAddModalShow ? renderStudentAddModal(ctrl) : null,
    ctrl.trainGameCtrl.showTrainGameTaskModal ? renderTrainGameTaskModal(ctrl) : null
  ].concat(renderTaskModals(ctrl.taskCtrl)));
}

