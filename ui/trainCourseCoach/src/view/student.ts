import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {Student} from '../interfaces';
import CoachCtrl from '../ctrl';
import {bind, bindSubmit, spinner} from '../util';
import * as ModalBuild from './modal';

export default function(ctrl: CoachCtrl): VNode {
  let signedStudents = ctrl.signedStudents();
  return h('div.trainCourse-coach__student', [
    h('div.trainCourseTop', [
      h('div.title', '学员列表'),
      h('div.action', [
        h(`button.button.button-empty.small`, {
          hook: bind('click', () => {
            ctrl.studentModalShow = true;
            ctrl.redraw();
          })
        }, '全部学员')
      ])
    ]),
    h('div', [
      h('table.slist', [
        h('thead', [
          h('tr', [
            h('th.tName', '学员'),
            h('th.tStatus', '学员状态')
          ])
        ])
      ]),
      h('div.scroll', [
        h('table.slist', [
          h('tbody', ctrl.studentLoading ? h('tr', [
            h('td.empty', {attrs: {colspan: 2}}, [spinner()])
          ]) : signedStudents.length == 0 ? h('tr', [
            h('td.empty', {
              attrs: {colspan: 2}
            }, '暂无签到学员')
          ]) : signedStudents.map((student) => {
            return h('tr',{
              class: {
                selected: ctrl.selected() && ctrl.student.id === student.id
              }
            }, [
              h('td.tName', {
                hook: {
                  insert(vnode) {
                    let el = vnode.elm as HTMLElement;
                    $(el).off('click').on('click', () => {
                      ctrl.select(student);
                    });
                  }
                }
              }, [userLink(student)]),
              h('td.tStatus', {
                class: {
                  training: (student.status.id === 'train'),
                  complete: (student.status.id === 'free'),
                  canceled: (student.status.id === 'canceled'),
                  uncomplete: (student.status.id === 'created')
                }
              }, student.status.name)
            ]);
          }))
        ])
      ])
    ])
  ]);
}

export function studentModal(ctrl: CoachCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.onStudentModalClose();
    },
    class: `studentModal`,
    content: [
      h('h2', [
        h('label', '全部学员'),
        h('a.button.button-empty.small.add', {
          hook: bind('click', () => {
            ctrl.onStudentAddModalOpen();
          })
        }, '添加')
      ]),
      h('div.modal-content-body', [
        h('form', {
          hook: bindSubmit(_ => {
            ctrl.onStudentModalClose();
          })
        }, [
          h('div', [
            h('table.slist', [
              h('thead', [
                h('tr', [
                  h('th.tName', '学员'),
                  h('th.tStatus', '学员状态'),
                  h('th.tAction')
                ])
              ])
            ]),
            h('div.scroll', [
              h('table.slist', [
                h('tbody', ctrl.studentLoading ? h('tr', [
                  h('td.empty', {attrs: {colspan: 3}}, [spinner()])
                ]) : ctrl.students.length == 0 ? h('tr', [
                  h('td.empty', {
                    attrs: {colspan: 3}
                  }, '暂无学员')
                ]) : ctrl.students.map((student) => {
                  return h(`tr.tr-${student.id}-${student.status.id}`, [
                    h('td.tName', userLink(student)),
                    h('td.tStatus', {
                      class: {
                        training: (student.status.id === 'train'),
                        complete: (student.status.id === 'free'),
                        canceled: (student.status.id === 'canceled'),
                        uncomplete: (student.status.id === 'created')
                      }
                    }, student.status.name),
                    h('td.tAction', [
                      h('a.button.button-empty.button-red.small', {
                        class:{
                          disabled: student.status.id === 'kick'
                        },
                        hook: bind('click', () => {
                          if(student.status.id !== 'kick') {
                            if(confirm('确认移除学员？')){
                              ctrl.kick(student.id);
                            }
                          }
                        })
                      }, '移除')
                    ])
                  ]);
                }))
              ])
            ])
          ]),
          h('div.form-actions', [
            h('button.button.small', {attrs: {type: 'submit'}}, '确定')
          ])
        ])
      ])
    ]
  });
}

export function studentAddModal(ctrl: CoachCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.onStudentAddModalClose();
    },
    class: `studentAddModal`,
    content: [
      h('h2', '添加学员'),
      h('div.modal-content-body', [
        h('form.studentForm', {
          hook: bindSubmit(_ => {
            ctrl.submitStudentAdd();
          })
        }, [
          h('div', [
            h('select.filter.form-control', {
              hook: bind('change', (e: Event) => {
                let v = (e.target as HTMLInputElement).value;
                ctrl.filterUnAddStudents(v);
              })
            },
            [
              h('option', {attrs: {value: 'all'}}, '全部'),
              h('option', {attrs: {value: 'none'}}, '无班级'),
              ...ctrl.opts.clazzes.map(cls => {
                return h('option', {attrs: {value: cls.id}}, cls.name)
              })
            ]),
            h('table.slist', [
              h('thead', [
                h('tr', [
                  h('th', '学员')
                ])
              ])
            ]),
            h('div.scroll', [
              h('table.slist', [
                h('tbody', ctrl.studentUnAddsFiltered.length == 0 ? h('tr', [
                  h('td.empty', { attrs: {colspan: 2} }, '暂无学员')
                ]) : ctrl.studentUnAddsFiltered.map((student, i) => {
                  return h('tr', [
                    h('td', h('input', { attrs: { type: 'checkbox', name: `students[${i}]`, value: student.id } })),
                    h('td', userLink(student))
                  ]);
                }))
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                ctrl.onStudentAddModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

export function userLink(u: Student, link: boolean = false, showBadges: boolean = true): VNode {
  const baseUrl = $('body').data('asset-url');
  const head = u.head ? '/image/' + u.head : baseUrl + '/assets/images/head-default-64.png';

  return h(link ? 'a.user-link.ulpt' : 'span.user-link', {
    attrs: { href: `/@/${u.name}`},
    class: { online: u.online, offline: !u.online, playing: u.isPlaying }
  }, [
    h('div.head-line', [
      h('img.head', { attrs: { src: head }}),
      h('i.line')
    ]),
    u.title ? h('span.title', u.title == 'BOT' ? { attrs: { 'data-bot': true } } : {}, u.title) : null,
    h('span.u_name', u.mark ? u.mark : u.name),
    showBadges ? h('span.badges', [
      u.member && u.member !== 'general' ? h('img.badge', { title: (u.member === 'gold' ? '金牌会员': '银牌会员'), attrs: { src: baseUrl + `/assets/images/icons/${u.member}.svg` }}) : null,
      u.coach ? h('img.badge', { attrs: { title: '认证教练', src: baseUrl + `/assets/images/icons/coach.svg` }}) : null,
      u.team ? h('img.badge', { attrs: { title: '认证俱乐部', src: baseUrl + `/assets/images/icons/team.svg` }}) : null
    ]) : null
  ]);
}
