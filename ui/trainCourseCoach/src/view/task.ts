import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {userLink} from './student';
import {bind, spinner} from '../util';
import CoachCtrl from '../ctrl';

export default function (ctrl: CoachCtrl): VNode {
  return h('div.trainCourse-coach__task', [
    h('div.trainCourseTop', [
      h('div.left', [
        h('div.title', '任务列表'),
        ctrl.selected() ? h('div.separator', '·' ): null,
        ctrl.selected() ? userLink(ctrl.student) : null
      ]),
      h('div.action', [
        h(`button.button.button-green.small`, {
          class: {
            disabled: !ctrl.selected() || ctrl.trainCourse.status.id !== 'started'
          },
          attrs: {
            disabled: !ctrl.selected() || ctrl.trainCourse.status.id !== 'started'
          },
          hook: bind('click', () => {
            ctrl.openCreateTaskModal();
          })
        }, '新建任务')
      ])
    ]),
    h('div', [
      h('table.slist', [
        h('thead', [
          h('tr', [
            h('th.tName', '任务名称'),
            h('th.tStatus', '任务状态'),
            h('th.tProgress', '进度'),
            h('th.tAction', '操作')
          ])
        ])
      ]),
      h('div.scroll', [
        h('table.slist', [
          h('tbody', ctrl.taskLoading ? h('tr', [
            h('td.empty', {attrs: {colspan: 4}}, [spinner()])
          ]) : !ctrl.tasks || ctrl.tasks.length == 0 ? h('tr', [
            h('td.empty', {attrs: {colspan: 4}}, '暂无任务')
          ]) : ctrl.tasks.map((task) => {
            return h(`tr.${task.id}`, [
              h('td.tName', task.name),
              h('td.tStatus', {
                class: {
                  training: (task.status.id === 'train'),
                  complete: (task.status.id === 'finished'),
                  canceled: (task.status.id === 'canceled'),
                  uncomplete: (task.status.id === 'created')
                }
              }, task.status.name),
              h('td.tProgress', `${task.progress}%`),
              h('td.tAction.action', [
                h(`a.button.button-empty`, { attrs: { target: '_blank', href: (task.itemType.id === 'trainGame' ? `/ttask/${task.id}/toTask` : `/ttask/${task.id}`) } }, '进入'),
                h(`button.button.button-empty.button-red.small`, {
                  class: {
                    disabled: task.status.id === 'train' ||  task.status.id === 'expired' || task.status.id === 'canceled' || task.status.id === 'finished'
                  },
                  attrs: {
                    disabled: task.status.id === 'train' || task.status.id === 'expired' || task.status.id === 'canceled' || task.status.id === 'finished'
                  },
                  hook: bind('click', () => {
                    if(confirm('取消任务后学员将不能再继续该任务，是否确认？')) {
                      ctrl.cancelTask(task.id);
                    }
                  })
                }, '取消')
              ])
            ]);
          }))
        ])
      ])
    ])
  ]);
}
