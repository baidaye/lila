import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import CoachCtrl from '../ctrl';
import {bind, bindSubmit} from '../util';
import * as ModalBuild from './modal';

export function trainGameTaskModal(ctrl: CoachCtrl): VNode {
  const trainGameCtrl = ctrl.trainGameCtrl;

  let readerPlayer = (key, label) => {
    return h('div.form-group.form-half', [
      h('label.form-label', {attrs: {for: `form-${key}`}}, label),
      h('select.form-control', {
          attrs: {id: `form-${key}`, name: key, required: true}
        },
        trainGameCtrl.gamePlayers.filter( p => !trainGameCtrl.onlyFreePlayers || (trainGameCtrl.onlyFreePlayers && !p.isPlaying)).map(function (player) {
          return h('option', {attrs: {value: player.id, 'data-free': (player.isPlaying ? 0 : 1) }}, player.mark ? player.mark : player.name)
        }))
    ])
  };

  const defaultTime = 20;
  const defaultIncrement = 0;
  return ModalBuild.modal({
    onClose: () => {
      trainGameCtrl.onTrainGameModalClose();
    },
    class: `trainGameCreateModal`,
    content: [
      h('h2', '创建新对局'),
      h('div.modal-content-body', [
        h('form.form3.trainGameTaskForm', {
          hook: bindSubmit(_ => {
            trainGameCtrl.submitTrainGameTask();
          })
        }, [
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.id', value: ctrl.trainCourse.id}}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.name', value: ctrl.trainCourse.name}}),
          h('input', {attrs: {type: 'hidden', name: 'sourceRel.source', value: 'trainCourse'}}),
          h('input', {attrs: {type: 'hidden', name: 'deadlineAt', value: ctrl.trainCourse.taskDeadlineAt}}),
          h(`div`,[
            h('input', {
              attrs: {
                id: `stu-free`,
                type: 'checkbox',
                checked: trainGameCtrl.onlyFreePlayers
              },
              hook: bind('change', (e: Event) => {
                trainGameCtrl.onlyFreePlayers = (e.target as HTMLInputElement).checked;
                ctrl.redraw();
              })
            }),
            h('label.form-label', {attrs: {for: `stu-free`}}, '空闲棋手')
          ]),
          h('div.form-split', [
            readerPlayer('white', '白方'),
            readerPlayer('black', '黑方')
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: `form-variant`}}, '开局方式'),
            h('select.form-control', {
                hook: bind('change', (e: Event) => {
                  let v = (e.target as HTMLInputElement).value;
                  trainGameCtrl.variant = v;
                  if(v === '3') {
                    $('.form-group-fen').removeClass('none');
                    $('.form-group-fen').find('input').trigger('change');
                  } else {
                    $('.form-group-fen').find('input').trigger('change');
                    $('.form-group-fen').addClass('none');
                  }
                }),
                attrs: {id: `form-variant`, name: 'variant', required: true}
              },
              trainGameCtrl.variants.map(function (variant) {
                return h('option', {attrs: {value: variant.id}}, variant.name)
              }))
          ]),
          h('div.form-group.form-group-fen.none', [
            h('label.form-label', {attrs: {for: `form-fen`}}),
            h('div', [
              h('input.form-control', {
                hook: {
                  insert(vnode) {
                    trainGameCtrl.validateFen();
                    const $el = $(vnode.elm as HTMLElement);
                    $el.val(trainGameCtrl.fen).on('change keyup paste', () => {
                      trainGameCtrl.fen = $el.val();
                      trainGameCtrl.validateFen();
                    })
                  }
                },
                attrs: {id: 'form-fen', name: 'fen'}
              }),
              h('div.preview')
            ])
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: `form-time`}}, ['每方分钟数：', h('span.form-time', defaultTime.toString())]),
            h('div.form-control', { hook: { insert: (vnode: VNode) => makeTimeSlider(vnode.elm as HTMLElement, defaultTime) } }, [
              h('input', {attrs: {type: 'hidden', name: 'time', value: defaultTime}}),
            ])
          ]),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: `form-increment`}}, ['步时：', h('span.form-increment', defaultIncrement.toString())]),
            h('div.form-control', { hook: { insert: (vnode: VNode) => makeIncrementSlider(vnode.elm as HTMLElement, defaultIncrement) } }, [
              h('input', {attrs: {type: 'hidden', name: 'increment', value: defaultIncrement}}),
            ])
          ]),
          h('div.form-group'),
          h('div.form-group', [
            h('label.form-label', {attrs: {for: `form-rated`}}),
            h('div.form-control', [
              h('group.radio', [
                h('div', [
                  h('input', {
                    attrs: { id: `rated-true`, name: `rated`, type: 'radio', value: 'true', checked: true }
                  }),
                  h('label', { attrs: { for: `rated-true` }}, '计算等级分')
                ]),
                h('div', [
                  h('input', {
                    attrs: { id: `rated-false`, name: `rated`, type: 'radio', value: 'false' }
                  }),
                  h('label', { attrs: { for: `rated-false` }}, '不计算等级分')
                ])
              ])
            ])
          ]),
          h('div.form-actions', [
            h('a.cancel', {
              hook: bind('click', () => {
                trainGameCtrl.onTrainGameModalClose();
              })
            }, '取消'),
            h('button.button.small', {attrs: {type: 'submit'}}, '确认')
          ])
        ])
      ])
    ]
  });
}

function makeTimeSlider(el: HTMLElement, v: number) {
  const sliderTimes = [
    0, 1/4, 1/2, 3/4, 1, 3/2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 25, 30, 35, 40, 45, 60, 90, 120, 150, 180
  ];

  const sliderTime = (v) => {
    return v < sliderTimes.length ? sliderTimes[v] : 180;
  };

  const showTime = (v) => {
    if (v === 1 / 4) return '¼';
    if (v === 1 / 2) return '½';
    if (v === 3 / 4) return '¾';
    return v;
  };

  let $el = $(el);
  if(!$el.data('slider')) {
    let slider = $el.slider({
      orientation: 'horizontal',
      range: 'min',
      min: 0,
      max: 34,
      step: 1,
      value: v,
      slide: (_: any, ui: any) => {
        let v = sliderTime(ui.value);
        $(el).parents('.form-group').find('input[name="time"]').val(v);
        $(el).parents('.form-group').find('.form-time').text(showTime(v));
      }
    });
    $el.data('slider', slider);
  }

}

function makeIncrementSlider(el: HTMLElement, v: number) {
  const sliderIncrement = (v) => {
    if (v <= 20) return v;
    switch (v) {
      case 21:
        return 25;
      case 22:
        return 30;
      case 23:
        return 35;
      case 24:
        return 40;
      case 25:
        return 45;
      case 26:
        return 60;
      case 27:
        return 90;
      case 28:
        return 120;
      case 29:
        return 150;
      default:
        return 180;
    }
  };

  let $el = $(el);
  if(!$el.data('slider')) {
    let slider = $el.slider({
      orientation: 'horizontal',
      range: 'min',
      min: 0,
      max: 30,
      step: 1,
      value: v,
      slide: (_: any, ui: any) => {
        let v = sliderIncrement(ui.value);
        $(el).parents('.form-group').find('input[name="increment"]').val(v);
        $(el).parents('.form-group').find('.form-increment').text(v);
      }
    });
    $el.data('slider', slider);
  }

}
