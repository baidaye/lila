import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import CoachCtrl from '../ctrl';
import {bind, spinner} from '../util';
import {TrainGameTask} from '../interfaces';

export default function(ctrl: CoachCtrl): VNode {
  const trainGameCtrl = ctrl.trainGameCtrl;

  return h('div.trainCourse-coach__bottom', [
    h('div.trainCourseTop', [
      h('div.tabs-horiz', [
        makeTab(ctrl, 'current', `当前对局(${trainGameCtrl.currentGameTasks().length})`),
        makeTab(ctrl, 'history', `已完成对局(${trainGameCtrl.historyGameTasks().length})`),
      ]),
      h('div.action', [
        h(`button.button.button-green.small`, {
          class: {
            disabled: ctrl.trainCourse.status.id !== 'started'
          },
          attrs: {
            disabled: ctrl.trainCourse.status.id !== 'started'
          },
          hook: bind('click', () => {
            trainGameCtrl.onTrainGameModalOpen();
          })
        }, '新建对局')
      ])
    ]),
    h('div.tabs-content', [
      h('div.trainGames', {
        class: {
          active: trainGameCtrl.activeGameTab === 'current'
        }
      }, [readerGames(ctrl, trainGameCtrl.currentGameTasks())]),
      h('div.trainGames', {
        class: {
          active: trainGameCtrl.activeGameTab === 'history'
        }
      }, [readerGames(ctrl, trainGameCtrl.historyGameTasks())]),
    ]),
  ]);
}

function readerGames(ctrl: CoachCtrl, gameTasks: TrainGameTask[]) {
  const trainGameCtrl = ctrl.trainGameCtrl;
  return h('div.trainGames', trainGameCtrl.gameTaskLoading ? spinner() : (
    gameTasks.length === 0 ? h('div.empty', '暂无对局') :
      h('div.now-playing', gameTasks.map(task => {
        return miniBoardWithPlayer(task);
      }))
  ))
}

function makeTab(ctrl: CoachCtrl, key: string, name: string) {
  const trainGameCtrl = ctrl.trainGameCtrl;
  return h('span.' + key, {
    class: {active: trainGameCtrl.activeGameTab === key},
    hook: bind('click', () => {
      trainGameCtrl.activeGameTab = key;
      ctrl.redraw();
    })
  }, name);
}

function miniBoardWithPlayer(task: TrainGameTask): VNode {
  return h(`div.mini-game.mini-game-${task.trainGameWithResult.trainGame.gameId}`, [
    renderPlayer(task, 'black'),
    miniBoard(task),
    renderPlayer(task, 'white'),
    renderFooter(task)
  ]);
}

export function miniBoard(task: TrainGameTask) {
  const trainGame = task.trainGameWithResult.trainGame;
  return h(`a.mini-board.parse-fen.is2d.mini-board-${trainGame.gameId}`, {
    class: {
      live: trainGame.isLive
    },
    key: trainGame.gameId,
    attrs: {
      target: '_blank',
      href: '/' + trainGame.gameId,
      'data-pause': trainGame.pause ? '1' : '0',
      'data-live': trainGame.isLive ? trainGame.gameId : '',
      'data-fen': trainGame.fen,
      'data-color': trainGame.color,
      'data-turncolor': trainGame.turnColor,
      'data-lastmove': trainGame.lastMove
    }
  }, [
    h('div.cg-wrap')
  ]);
}

function renderPlayer(task: TrainGameTask, color: Color) {
  const p = task.trainGameWithResult.trainGame[color];
  const fullName = p.mark ? p.mark : p.name;
  return h('span.mini-game__player', [
    h('span.mini-game__user', [
      p.title ? h('span.title', p.title == 'BOT·' ? { attrs: { 'data-bot': true } } : {}, `${p.title}·`) : null,
      h('span.name', fullName)
    ]),
    renderResult(task, color),
    renderClock(task, color)
  ]);
}

function renderResult(task: TrainGameTask, color: Color) {
  let result = task.trainGameWithResult.result;
  return !!result ? h('span.mini-game__result', [
    result.winner ? (
      result.winner === color ? h('span.win', '1') : h('span.loss', '0')
    ) : h('span.draw', '½')
  ]) : null;
}

function renderClock(task: TrainGameTask, color: Color) {
  let remainSeconds = task.trainGameWithResult.trainGame.remainClock[color];
  return !task.trainGameWithResult.result ? h(`mini-game__clock.mini-game__clock--${color}`, {
    attrs: {
      'data-managed': 1,
      'data-time': remainSeconds
    }
  }, `${addZero(Math.floor(remainSeconds / 60))}:${addZero(Math.floor(remainSeconds % 60))}`) : null;
}

function renderFooter(task: TrainGameTask) {
  return h('div.mini-game__footer', [
    h('div.name', task.trainGameWithResult.trainGame.name),
    h('div.status', {
      class: {
        training: (task.status.id === 'created' || task.status.id === 'train'),
        complete: (task.status.id === 'finished'),
        canceled: (task.status.id === 'canceled'),
      }
    }, statusName(task))
  ]);
}

function statusName(task: TrainGameTask) {
  switch (task.status.id) {
    case 'created':
    case 'train':
      return '对局中';
    default:
      return task.status.name;
  }
}

function addZero(i) {
  return i < 10 ? '0' + i : i.toString();
}
