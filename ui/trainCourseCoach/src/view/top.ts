import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import CoachCtrl from '../ctrl';
import {bind} from '../util';

export default function(ctrl: CoachCtrl): VNode {
  return h('div.trainCourse-coach__top', [
    h('div.trainCourseTop', [
      h('div.left', [
        h('div.title', ctrl.trainCourse.name),
        h('div.time', `${ctrl.trainCourse.timeBegin} ~ ${ctrl.trainCourse.timeEnd}`),
        h('div.status', {
          class: {
            created: (ctrl.trainCourse.status.id === 'created'),
            started: (ctrl.trainCourse.status.id === 'started'),
            stopped: (ctrl.trainCourse.status.id === 'stopped')
          }
        }, ctrl.trainCourse.status.name)
      ]),
      h('div.action', [
        (ctrl.trainCourse.status.id === 'created') ? h(`button.button.button-green.small.start.${ctrl.trainCourse.status}`, {
          hook: bind('click', () => {
            ctrl.start();
          })
        }, '开始上课') : null,
        (ctrl.trainCourse.status.id === 'started') ? h(`button.button.button-red.small.start.${ctrl.trainCourse.status}`, {
          hook: bind('click', () => {
            if(confirm('确认下课？')) {
              ctrl.stop();
            }
          })
        }, '下    课') : null,
      ])
    ])
  ]);
}

