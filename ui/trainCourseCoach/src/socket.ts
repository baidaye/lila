import CoachCtrl from './ctrl';

interface Handlers {
  [key: string]: any;
}

export interface Socket {
  send: SocketSend;
  receive(type: string, data: any): boolean;
}

export function make(send: SocketSend, ctrl: CoachCtrl): Socket {
  const handlers: Handlers = {
    signIn() {
       ctrl.refreshStudent();
    },
    signOut() {
      ctrl.refreshStudent();
    },
    stop() {
      ctrl.trainInfo();
    },
    taskCreate() {
      ctrl.getTasks();
    },
    taskChangeStatus() {
      ctrl.refreshStudent();
      ctrl.getTasks();
    },
    taskChangeProgress() {
      ctrl.getTasks();
    },
    gameTaskCreate() {
      ctrl.refreshStudent();
      ctrl.getTasks();
      ctrl.trainGameCtrl.getTrainGameTasks();
    },
    gameTaskStart(gameId) {
      ctrl.trainGameCtrl.startTrainGame(gameId)
    },
    gameTaskFinish() {
      ctrl.refreshStudent();
      ctrl.getTasks();
      ctrl.trainGameCtrl.getTrainGameTasks();
    }
  };

  return {
    receive(type: string, data: any): boolean {
      const handler = handlers[type];
      if (handler) handler(data);
      return true;
    },
    send
  };
}
