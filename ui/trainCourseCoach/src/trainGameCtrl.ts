import * as xhr from './xhr'
import CoachCtrl from './ctrl';
import {Player, TrainGameTask} from './interfaces';

export default class TrainGameTaskCtrl {

  itemType: string = 'trainGame';
  showTrainGameTaskModal: boolean = false;
  gamePlayers: Player[] = [];
  onlyFreePlayers: boolean = true;
  variant: string = '1';
  fen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

  gameTasks: TrainGameTask[] = [];
  gameTaskLoading: boolean = false;

  variants = [
    {id: '1', name: '标准国际象棋'},
    {id: '3', name: '指定局面FEN'}
  ];

  activeGameTab: string = 'current'; // current, history

  constructor(readonly ctrl: CoachCtrl) {
    this.gameTasks = this.ctrl.opts.gameTasks;
    window.lichess.slider();
  }

  onTrainGameModalOpen = () => {
    this.getPlayers();
    this.showTrainGameTaskModal = true;
    this.gamePlayers = [];
    this.onlyFreePlayers = true;
    this.variant = '1';
    this.fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
    this.ctrl.redraw();
  };

  onTrainGameModalClose = () => {
    this.showTrainGameTaskModal = false;
    this.ctrl.redraw();
  };

  getPlayers = () => {
    xhr.getPlayers(this.ctrl.trainCourse.id).then((data) => {
      this.gamePlayers = data;
      this.ctrl.redraw();
    });
  };

  submitTrainGameTask = () => {
    let $form = $('.trainGameTaskForm');
    let data = $form.serialize();
    let white = $form.find('#form-white').val();
    let black = $form.find('#form-black').val();
    if(white === black) {
      alert('黑白双方不能相同');
      return
    }

    xhr.submitTrainGameTask(data).then(() => {
      this.onTrainGameModalClose();
    });
  };

  validateFen = () => {
    let $form = $('.trainGameTaskForm');
    let $preview = $form.find('.preview');
    let $fen = $form.find('#form-fen');
    if(this.variant !== '3') {
      $fen.parent('.form-group').removeClass('is-invalid');
      ($fen[0] as HTMLInputElement).setCustomValidity('');
      window.lichess.pubsub.emit('content_loaded');
    } else if (this.fen && this.variant === '3') {
      xhr.validateFen(this.fen).then((data) => {
        $preview.html(data);
        $fen.parent('.form-group').removeClass('is-invalid');
        ($fen[0] as HTMLInputElement).setCustomValidity('');
        window.lichess.pubsub.emit('content_loaded');
      }, () => {
        $preview.empty();
        $fen.parent('.form-group').addClass('is-invalid');
        ($fen[0] as HTMLInputElement).setCustomValidity('FEN 格式无效');
      });
    }
  };

  startTrainGame = (gameId) => {
    $(`.mini-game-${gameId}`).each(function(_, el) {
      let $game = $(el);
      let $board = $game.find('.mini-board');
      let turnColor = $board.data('turncolor');
      $(`.mini-board-${gameId}`).data('pause', '0');
      ['white', 'black'].forEach(color =>
        $game.find('.mini-game__clock--' + color).each(function (_, el) {
          let $clock = $(el);
          $clock.clock('set', {
            time: parseInt($clock.data('time')),
            pause: color != turnColor,
          });
        }));
    });
  };

  getTrainGameTasks = window.lichess.debounce(() => {
    this.gameTaskLoading = true;
    this.ctrl.redraw();
    xhr.getTrainGameTasks(this.ctrl.trainCourse.id).then((data) => {
      this.gameTasks = data;
      this.gameTaskLoading = false;
      this.ctrl.redraw();
      setTimeout(() => {
        window.lichess.pubsub.emit('content_loaded');
      }, 200);
    });
  }, 500);

  currentGameTasks = () => {
    return this.gameTasks.filter(task => {
      let result = task.trainGameWithResult.result;
      return !result || ((Date.now() - result.finishAt) < 10 * 60 * 1000);
    });
  };

  historyGameTasks = () => {
    return this.gameTasks.filter(task => {
      let result = task.trainGameWithResult.result;
      return result && ((Date.now() - result.finishAt) >= 10 * 60 * 1000);
    });
  };

}
