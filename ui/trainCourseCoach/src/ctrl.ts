import {TrainCourseOpts, TrainCourse, Redraw, Student} from './interfaces';
import TaskCtrl from '../../ttask/src/ctrl';
import {Task} from '../../ttask/src/interfaces';
import {make as makeSocket, Socket} from './socket';
import TrainGameTaskCtrl from './trainGameCtrl';
import * as xhr from './xhr'

export default class CoachCtrl {

  opts: TrainCourseOpts;
  socket: Socket;
  trainCourse: TrainCourse;
  students: Student[];
  studentLoading: boolean = false;

  studentModalShow: boolean = false;
  studentAddModalShow: boolean = false;
  studentUnAdds: Student[] = [];
  studentUnAddsFiltered: Student[] = [];

  student: Student;
  tasks: Task[] = [];
  taskLoading: boolean = false;

  trainGameCtrl: TrainGameTaskCtrl;
  taskCtrl: TaskCtrl;

  constructor(opts: TrainCourseOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.trainCourse = opts.trainCourse;
    this.students = opts.students;
    this.socket = makeSocket(this.opts.socketSend, this);
    this.trainGameCtrl = new TrainGameTaskCtrl(this);
    this.taskCtrl = new TaskCtrl({
      notAccept: this.opts.notAccept,
      sourceRel: {
        id: this.trainCourse.id,
        name: this.trainCourse.name,
        deadlineAt: this.trainCourse.taskDeadlineAt,
        source: 'trainCourse',
      },
      themePuzzle: this.opts.themePuzzle,
      classicGames: this.opts.classicGames,
      team: this.opts.team
    }, this.redraw, this.getTasksNow);
  }

  refreshStudent = window.lichess.debounce(() => {
    this.studentLoading = true;
    this.redraw();
    xhr.getStudents(this.trainCourse.id).then((data) => {
      this.students = data;
      this.studentLoading = false;
      this.redraw();
    });
  }, 500);

  signedStudents = () => {
    return this.students.filter(stu => (stu.status.id === 'free' || stu.status.id === 'train'))
  };

  kick = (userId: string) => {
    xhr.kick(this.trainCourse.id, userId).then(() => {
      this.refreshStudent();
    });
  };

  onStudentAddModalOpen = () => {
    this.studentAddModalShow = true;
    this.getUnAddStudents();
  };

  onStudentAddModalClose = () => {
    this.studentAddModalShow = false;
    this.redraw();
  };

  getUnAddStudents = () => {
    xhr.getUnAddStudents(this.trainCourse.id).then((data) => {
      this.studentUnAdds = data;
      this.filterUnAddStudents('all');
      this.redraw();
    });
  };

  filterUnAddStudents = (v) => {
    if (v === 'all') {
      this.studentUnAddsFiltered = this.studentUnAdds;
    } else if (v === 'none') {
      let clsStudents: string[] = [];
      clsStudents = this.opts.clazzes.reduce((a, b) => a.concat(b.studentIds), clsStudents);
      this.studentUnAddsFiltered = this.studentUnAdds.filter(s => {
        return !clsStudents.includes(s.id);
      });
    } else {
      let clsStudents = this.opts.clazzes.filter(c => c.id === v)[0].studentIds;
      this.studentUnAddsFiltered = this.studentUnAdds.filter(s => {
        return clsStudents.includes(s.id);
      });
    }
    this.redraw();
  };

  submitStudentAdd = () => {
    let $form = $('.studentForm');
    let data = $form.serialize();
    if(!data) {
      this.onStudentAddModalClose();
      return;
    }

    xhr.addStudents(this.trainCourse.id, data).then(() => {
      this.studentAddModalShow = false;
      this.refreshStudent();
    });
  };

  select = (data: Student) => {
    this.student = data;
    this.getTasksNow();
    this.redraw();
  };

  selected = () => this.student !== undefined;

  trainInfo = () => {
    xhr.trainInfo(this.trainCourse.id).then((data) => {
      this.trainCourse = data;
      this.redraw();
    });
  };

  start = () => {
    xhr.start(this.trainCourse.id).then(() => {
      this.trainInfo();
    });
  };

  stop = () => {
    xhr.stop(this.trainCourse.id).then(() => {
      //this.trainInfo();
    });
  };

  onStudentModalClose = () => {
    this.studentModalShow = false;
    this.redraw();
  };

  getTasksNow = (): void => {
    if(this.student && this.student.id) {
      this.taskLoading = true;
      this.redraw();
      xhr.getTasks(this.trainCourse.id, this.student.id).then((data) => {
        this.tasks = data;
        this.taskLoading = false;
        this.redraw();
      });
    }
  };

  getTasks = window.lichess.debounce(this.getTasksNow, 500);

  cancelTask = (taskId: string) => {
    xhr.cancelTask(taskId).then(() => {
      this.getTasksNow();
      this.trainGameCtrl.getTrainGameTasks();
      this.refreshStudent();
    });
  };

  openCreateTaskModal = () => {
    this.taskCtrl.openTaskCreateModal(this.signedStudents(), this.student.id)
  };

}

