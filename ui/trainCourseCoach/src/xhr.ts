const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function start(id: string) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/coach/start`,
    headers: headers
  });
}

export function stop(id: string) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/coach/stop`,
    headers: headers
  });
}

export function addStudents(id: string, students) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/coach/addStudents`,
    headers: headers,
    data: students
  });
}

export function kick(id: string, userId: string) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/coach/kick`,
    headers: headers,
    data: {
      userId: userId
    }
  });
}

export function trainInfo(id: string) {
  return $.ajax({
    url: `/trainCourse/${id}/info`,
    headers: headers
  });
}

export function getUnAddStudents(id: string) {
  return $.ajax({
    url: `/trainCourse/${id}/coach/unAddStudents`,
    headers: headers
  });
}

export function getStudents(id: string) {
  return $.ajax({
    url: `/trainCourse/${id}/coach/students`,
    headers: headers
  });
}

export function getPlayers(id: string) {
  return $.ajax({
    url: `/trainCourse/${id}/coach/players`,
    headers: headers
  });
}

export function getTasks(id: string, userId: string) {
  return $.ajax({
    url: `/ttask/list/trainCourse?metaId=${id}&userId=${userId}&excludeTrainGame=false`,
    headers: headers
  });
}

export function getTrainGameTasks(id: string) {
  return $.ajax({
    url: `/ttask/trainGameTasks?trainCourseId=${id}`,
    headers: headers
  });
}

export function getTask(taskId: string) {
  return $.ajax({
    url: `/ttask/${taskId}`,
    headers: headers
  });
}

export function cancelTask(taskId: string) {
  return $.ajax({
    method: 'post',
    url: `/ttask/${taskId}/cancel`,
    headers: headers,
  });
}

export function submitTrainGameTask(data: any) {
  return $.ajax({
    method: 'post',
    url: `/ttask/createTrainGame`,
    headers: headers,
    data: data
  });
}

export function validateFen(fen: string) {
  return $.ajax({
    url: `/setup/validate-fen?strict=0&playable=true`,
    headers: headers,
    data: {
      fen: fen
    }
  });
}
