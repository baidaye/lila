import {h, thunk} from 'snabbdom'
import {VNode, VNodeData} from 'snabbdom/vnode'
import {Ctrl, Line} from './interfaces'
import * as spam from './spam'
import * as enhance from './enhance';
import throttle from 'common/throttle';
//import {presetView} from './preset';
import {lineAction} from './moderation';
import {userLink, bind} from './util';

const whisperRegex = /^\/w(?:hisper)?\s/;

let emojiDisplay = false;

export default function (ctrl: Ctrl): Array<VNode | undefined> {
  if (!ctrl.vm.enabled) return [];
  const scrollCb = (vnode: VNode) => {
      const el = vnode.elm as HTMLElement;
      if (ctrl.data.lines.length > 5) {
        const autoScroll = (el.scrollTop === 0 || (el.scrollTop > (el.scrollHeight - el.clientHeight - 100)));
        if (autoScroll) {
          el.scrollTop = 999999;
          setTimeout((_: any) => el.scrollTop = 999999, 300)
        }
      }
    },
    m = ctrl.moderation();
  const vnodes = [
    h('ol.mchat__messages.chat-v-' + ctrl.data.domVersion, {
      attrs: {
        role: 'log',
        'aria-live': 'polite',
        'aria-atomic': false
      },
      hook: {
        insert(vnode) {
          const $el = $(vnode.elm as HTMLElement).on('click', 'a.jump', (e: Event) => {
            window.lichess.pubsub.emit('jump', (e.target as HTMLElement).getAttribute('data-ply'));
          });
          if (m) $el.on('click', '.mod', (e: Event) => {
            m.open(((e.target as HTMLElement).getAttribute('data-username') as string).split(' ')[0]);
          });
          scrollCb(vnode);
        },
        postpatch: (_, vnode) => scrollCb(vnode)
      }
    }, selectLines(ctrl).map(line => renderLine(ctrl, line))),
    renderInput(ctrl)
  ];
  // const presets = presetView(ctrl.preset);
  // if (presets) vnodes.push(presets);
  return vnodes;
}

function renderInput(ctrl: Ctrl): VNode | undefined {
  if (!ctrl.vm.writeable) return;

  let disabled = false;
  let placeholder = '';
  if ((ctrl.data.loginRequired && !ctrl.data.userId) || ctrl.data.restricted) {
    placeholder = ctrl.trans('loginToChat');
    disabled = true;
  } else if (ctrl.vm.timeout) {
    placeholder = ctrl.trans('youHaveBeenTimedOut');
  } else if (ctrl.opts.blind) {
    placeholder = '聊天';
  } else {
    placeholder = ctrl.trans.noarg(ctrl.vm.placeholderKey);
  }
  disabled = disabled || ctrl.vm.timeout || !ctrl.vm.writeable;


  return h('div.mchat__post', [
    h('form.mchat__post-form', {
        hook: bind('submit', e => {
          e.preventDefault();
          const area = (e.target as HTMLElement).querySelector('textarea');
          if (area) {
            area.dispatchEvent(new Event('send'));
          }
        }),
      },
      [
        renderEmoji(ctrl),
        h('textarea.mchat__post-text', {
          attrs: {
            rows: 1,
            placeholder,
            maxlength: 150,
            disabled: disabled,
          },
          hook: {
            insert(vnode) {
              setupTextarea(vnode.elm as HTMLTextAreaElement, ctrl);
            },
          },
        }),
        h('button.button.button-green.small.mchat__post-submit', {
          attrs: {
            disabled: disabled,
            type: 'submit'
          }
        }, '发送')
      ]
    )
  ])
}


function setupTextarea(area: HTMLTextAreaElement, ctrl: Ctrl) {
  let prev = 0;

  function send() {
    const now = Date.now();
    if (prev > now - 500) return;
    prev = now;

    const txt = area.value.trim();
    if (txt.length > 150) {
      alert('The message is too long.');
      return;
    }

    if (txt) {
      ctrl.post(txt);
    }
    area.value = '';
    area.focus();
    area.dispatchEvent(new Event('input'));
  }

  // hack to automatically resize the textarea based on content
  area.value = '';
  const baseScrollHeight = area.scrollHeight;
  area.addEventListener('input',
    throttle(500, () => {
      const text = area.value.trim();
      area.rows = 1;
      // the resize magic
      if (text) {
        area.rows = Math.min(10, 1 + Math.ceil((area.scrollHeight - baseScrollHeight) / 19));
      }
    })
  );

  if (area.value) {
    area.dispatchEvent(new Event('input'));
  }

  // send the content on <enter.
  area.addEventListener('keypress', (e: KeyboardEvent) => {
    const pub = ctrl.opts.public;
    const txt = area.value.trim();
    if (e.which == 10 || e.which == 13) {
      e.preventDefault();
      if (txt === '') {
        area.focus();
      } else {
        spam.report(txt);
        if (pub && spam.hasTeamUrl(txt)) {
          alert("Please don't advertise teams in the chat.");
        }
        else send();
        if (!pub) area.classList.remove('whisper');
      }
    } else {
      area.removeAttribute('placeholder');
      if (!pub) area.classList.toggle('whisper', !!txt.match(whisperRegex));
    }
  });
  area.addEventListener('send', send);
}

function renderEmoji(ctrl: Ctrl) {
  let emojis = [
    '🙂', '😊', '😃', '😁', '😂', '😍',
    '😎', '😘', '😙', '😜', '😝', '🤑',
    '😪', '😴', '😷', '😵', '😟', '😨',
    '😰', '😱', '😤', '😈', '💩', '💋',
    '💖', '💣', '💯', '💪', '🕛', '⭐',
    '🌙', '🌧️', '☔', '🌈', '🐵', '🐶',
    '🐳', '🐙', '🐚', '🍉', '🍌', '🍅',
    '🍺', '🎂', '🌴', '🌹', '🥀', '🌳',
    '🎵', '💰', '📞', '📰', '💼', '💡'];

  return h('div.emoji', {
    hook: {
      insert() {
        window.onclick = function(e) {
          if(!(e.target as HTMLElement).matches('.emoji-dropbtn')) {
            emojiDisplay = false;
            ctrl.redraw();
          }
        }
      }
    }
  }, [
    h('a.emoji-dropbtn', {
      hook: bind('click', () => {
        emojiDisplay = !emojiDisplay;
        ctrl.redraw();
      })
    }, '🙂'),
    h('div.emoji-picker',{
      class: {
        none: !emojiDisplay
      }
    }, [
      h('div.emoji-content', emojis.map((emoji) => {
        return h('span', {
          hook: bind('click', (e) => {
            let $area = $('.mchat__post-text');
            let v = $area.val() + (e.target as HTMLElement).innerText;
            $area.val(v);
          })
        }, emoji)
      }))
    ])
  ]);
}

// function renderInput(ctrl: Ctrl): VNode | undefined {
//   if (!ctrl.vm.writeable) return;
//   if ((ctrl.data.loginRequired && !ctrl.data.userId) || ctrl.data.restricted)
//     return h('input.mchat__say', {
//       attrs: {
//         placeholder: ctrl.trans('loginToChat'),
//         disabled: true
//       }
//     });
//   let placeholder: string;
//   if (ctrl.vm.timeout) placeholder = ctrl.trans('youHaveBeenTimedOut');
//   else if (ctrl.opts.blind) placeholder = 'Chat';
//   else placeholder = ctrl.trans.noarg(ctrl.vm.placeholderKey);
//   return h('input.mchat__say', {
//     attrs: {
//       placeholder,
//       autocomplete: 'off',
//       maxlength: 140,
//       disabled: ctrl.vm.timeout || !ctrl.vm.writeable
//     },
//     hook: {
//       insert(vnode) {
//         setupHooks(ctrl, vnode.elm as HTMLElement);
//       }
//     }
//   });
// }
//
// const setupHooks = (ctrl: Ctrl, chatEl: HTMLElement) => {
//   chatEl.addEventListener('keypress',
//     (e: KeyboardEvent) => setTimeout(() => {
//       const el = e.target as HTMLInputElement,
//         txt = el.value,
//         pub = ctrl.opts.public;
//       if (e.which == 10 || e.which == 13) {
//         if (txt === '') $('.keyboard-move input').focus();
//         else {
//           spam.report(txt);
//           if (pub && spam.hasTeamUrl(txt)) {
//             alert("Please don't advertise teams in the chat.");
//           }
//           else ctrl.post(txt);
//           el.value = '';
//           if (!pub) el.classList.remove('whisper');
//         }
//       } else {
//         el.removeAttribute('placeholder');
//         if (!pub) el.classList.toggle('whisper', !!txt.match(whisperRegex));
//       }
//     })
//   );
// };

function sameLines(l1: Line, l2: Line) {
  return l1.d && l2.d && l1.u === l2.u;
}

function selectLines(ctrl: Ctrl): Array<Line> {
  let prev: Line, ls: Array<Line> = [];
  ctrl.data.lines.forEach(line => {
    if (!line.d &&
      (!prev || !sameLines(prev, line)) &&
      (!line.r || ctrl.opts.kobold) &&
      !spam.skip(line.t)
    ) ls.push(line);
    prev = line;
  });
  return ls;
}

function updateText(parseMoves: boolean) {
  return (oldVnode: VNode, vnode: VNode) => {
    if ((vnode.data as VNodeData).lichessChat !== (oldVnode.data as VNodeData).lichessChat) {
      (vnode.elm as HTMLElement).innerHTML = enhance.enhance((vnode.data as VNodeData).lichessChat, parseMoves);
    }
  };
}

function renderText(t: string, parseMoves: boolean) {
  if (enhance.isMoreThanText(t)) {
    const hook = updateText(parseMoves);
    return h('t', {
      lichessChat: t,
      hook: {
        create: hook,
        update: hook
      }
    });
  }
  return h('t', t);
}

function renderLine(ctrl: Ctrl, line: Line) {

  const textNode = renderText(line.t, ctrl.opts.parseMoves);

  if (line.u === 'haichess' || line.u === 'lichess') return h('li.system', textNode);

  if (line.c) return h('li', [
    h('span.color', '[' + line.c + ']'),
    textNode
  ]);

  const userNode = thunk('a', line.u, userLink, [line.u, line.title]);

  return h('li', {}, ctrl.moderation() ? [
    line.u ? lineAction(line.u) : null,
    userNode,
    textNode
  ] : [userNode, textNode]);
}
