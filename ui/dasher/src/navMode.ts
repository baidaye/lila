import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import {Redraw, Close, header, bind} from './util'

export interface NavMode {
  key: string,
  name: string
}

export interface NavModeCtrl {
  list: NavMode[]
  set(k: string): void
  get(): string
  close: Close
}

export function ctrl(current: string, redraw: Redraw, close: Close): NavModeCtrl {

  const list: NavMode[] = [
    { key: 'auto', name: "自适应" },
    { key: 'touch', name: "触摸大屏" }
  ];

  return {
    list,
    get:() => current,
    set(mode: string) {
      $.post('/pref/navMode', { mode: mode }, window.lichess.reload);
      redraw();
    },
    close
  };
}

export function view(ctrl: NavModeCtrl): VNode {

  const cur = ctrl.get();

  return h('div.sub.navMode', [
    header("主菜单", ctrl.close),
    h('div.selector.large', ctrl.list.map(bg => {
      return h('a.text', {
        class: { active: cur === bg.key },
        attrs: { 'data-icon': 'E' },
        hook: bind('click', () => ctrl.set(bg.key))
      }, bg.name);
    }))
  ])
}
