import { build as treeBuild, ops as treeOps, path as treePath } from 'tree';
import { readDests, readDrops, decomposeUci, sanToRole } from 'chess';
import { Config as CgConfig } from 'chessground/config';
import { Api as CgApi } from 'chessground/api';
import { opposite } from 'chessground/util';
import * as cg from 'chessground/types';
import keyboard from './keyboard';
import socketBuild from './socket';
import { prop } from 'common';
import { sound } from './sound';
import { Vm, Controller } from './interfaces';
import TaskProgressCtrl from "../../ttask/src/progressCtrl";

export default function(opts, redraw: () => void): Controller {

  let vm: Vm = {} as Vm;
  let data, lastingTree, lastingMainline, tree, home;
  const ground = prop<CgApi | undefined>(undefined);
  let taskProgressCtrl;

  function initiate(opts) {
    data = opts.data;
    home = opts.home;

    lastingTree = treeBuild(treeOps.reconstruct(data.game.treeParts));
    lastingMainline = treeOps.mainlineNodeList(lastingTree.root);
    tree = lastingTree;

    let initialPath = lastingMainline[0].id;
    let distinguish = data.distinguish;

    vm.mainline = lastingMainline;
    vm.initialPath = initialPath;
    vm.initialNode = lastingTree.nodeAtPath(initialPath);
    vm.historys = data.history;
    vm.hashId = data.hashId;

    vm.stage = 'pending';
    vm.readonly = distinguish.readonly;
    vm.orientation = distinguish.orientation;
    vm.coords = opts.pref.coords !== 0;
    vm.maxTurns = lastingMainline.length - 1;
    vm.turns = distinguish.turns ? Math.min(distinguish.turns, vm.maxTurns) : vm.maxTurns;
    vm.rightTurns = data.rightTurns ? data.rightTurns : vm.turns;
    vm.currTurns = 0;
    vm.wins = 0;
    vm.answers = [];
    setPath(initialPath, true);

    if(opts.create) {
      openCreateModal(opts.create, opts.tab, opts.gameId);
    }
  }

  function setPath(path, init: boolean = false) {
    vm.previousPath = init ? vm.initialPath : vm.currentPath;
    vm.currentPath = path;
    vm.nodeList = lastingTree.getNodeList(path);
    vm.currentNode = treeOps.last(vm.nodeList)!;
  }

  function withGround<A>(f: (cg: CgApi) => A): A | undefined {
    const g = ground();
    if (g) return f(g);
  }

  function showGround(g) {
    g.set(makeCgOpts());
  }

  function makeCgOpts() {
    const node = vm.currentNode,
      color = plyColor(node.ply),
      config: CgConfig = {
        fen: node.fen,
        orientation: vm.orientation as cg.Color,
        coordinates: vm.coords,
        turnColor: color,
        check: !!node.check,
        lastMove: uciToLastMove(node.uci)
      };

    if (config.check) {
      config.turnColor = opposite(color);
    }
    vm.cgConfig = config;
    return config;
  }

  function plyToTurn(ply) {
    return Math.floor((ply - 1) / 2) + 1;
  }

  function plyColor(ply: number) {
    return (ply % 2 === 0) ? 'black' : 'white';
  }

  function currPlyColor() {
    return plyColor(vm.currentNode.ply);
  }

  function uciToLastMove(uci) {
    return uci && [uci.substr(0, 2), uci.substr(2, 2)];
  }

  function jump(path, init: boolean = false) {
    const pathChanged = path !== vm.currentPath,
        isForwardStep = pathChanged && path.length === vm.currentPath.length + 2;
    setPath(path, init);
    withGround(showGround);
    if (pathChanged) {
      if (isForwardStep) {
        if (!vm.currentNode.uci) {
          sound.move(); // initial position
        } else if (vm.currentNode.san.indexOf('x') != -1) {
          sound.capture();
        } else if (vm.currentNode.san.indexOf('+') != -1) {
          sound.check();
        } else {
          sound.move()
        }
      }
    }
    vm.autoScrollRequested = true;
  }

  function start() {
    vm.stage = 'running';
    vm.startTimeMs = new Date().getTime();
    let r = lastingTree.root;
    let root = {
      id: r.id,
      ply: r.ply,
      fen: r.fen,
      children: []
    } as Tree.Node;
    tree = treeBuild(root);
    lastingMainline = lastingMainline.slice(0, vm.turns + 1);
    jump(vm.initialPath, true);
    playNextMove();
  }

  function playNextMove() {
    vm.loading = false;
    let nextNode = treeOps.nodeAtPly(lastingMainline, vm.currentNode.ply + 1);
    if(nextNode) {
      let nodeList = lastingMainline.filter(n => n.ply <= nextNode.ply);
      let nextPath = treePath.fromNodeList(nodeList);
      jump(nextPath);
      vm.answers = buildAnswers(nextNode.san);
      redraw();
    } else finish();
  }

  function judge(san) {
    let win = vm.currentNode.san === san;

    vm.currWin = win;
    vm.currAnswer = vm.currentNode.san;
    vm.currTurns = vm.currTurns + 1;
    vm.wins = win ? vm.wins + 1 : vm.wins;
    let node = {
      id: vm.currentNode.id,
      ply: vm.currentNode.ply,
      fen: vm.currentNode.fen,
      san: vm.currentNode.san,
      uci: vm.currentNode.uci,
      opening: vm.currentNode.opening,
      fail: !win,
      children: []
    } as Tree.Node;

    vm.answers = [];
    vm.loading = true;
    vm.loading = true;
    tree.addNode(node, vm.previousPath);
    redraw();

    if(win) sound.win(); else sound.loss();
    setTimeout(playNextMove, 1500);
  }

  function finish() {
    sendResult().done(function () {
      vm.stage = 'finished';
      vm.timeMs = new Date().getTime() - vm.startTimeMs;
      withGround(showGround);
      redraw();
    });
  }

  function formatMs(msTime) {
    const date = new Date(Math.max(0, msTime + 500)),
        hours = date.getUTCHours(),
        minutes = date.getUTCMinutes(),
        seconds = date.getUTCSeconds();
    return hours > 0 ? hours + ':' + pad(minutes) + ':' + pad(seconds) : minutes + ':' + pad(seconds);
  }

  function pad(x) {
    return (x < 10 ? '0' : '') + x;
  }

  function sendResult() {
    return $.ajax({
      method: 'POST',
      data: {
        'win' : vm.wins >= vm.rightTurns,
        'turns': vm.wins
      },
      url: '/distinguish/finish?id=' + data.distinguish.id + (vm.hashId ? '&hashId=' + vm.hashId : '')
    })
  }

  function startAgain() {
    vm.wins = 0;
    vm.currTurns = 0;
    vm.answers = [];
    start();
  }

  function buildAnswers(san) {
    let files = 'abcdefgh';
    let rows = '12345678';
    if (san === 'O-O' || san === 'O-O-O') {
      return ['O-O', 'O-O-O'];
    } else if(san.indexOf("=") != -1) {
      return [san];
    } else {
      let sanCharArr = san.split('');
      let fileIndex = (san.indexOf("+") || san.indexOf("#")) > -1 ? sanCharArr.length - 3 : sanCharArr.length - 2;
      let rowIndex = (san.indexOf("+") || san.indexOf("#")) > -1 ? sanCharArr.length - 2 : sanCharArr.length - 1;
      let file = sanCharArr[fileIndex];
      let row = sanCharArr[rowIndex];
      let fs = nearChars(files, file);
      let rs = nearChars(rows, row);

      // 行反了
      let rowReverse: string[] = [];
      let reverseRowArr = [...sanCharArr];
      reverseRowArr[rowIndex] = reverseRow(row);
      rowReverse.push(reverseRowArr.join(''));

      // 列反了
      let fileReverse: string[] = [];
      let reverseFileArr = [...sanCharArr];
      reverseFileArr[fileIndex] = reverseFile(file);
      fileReverse.push(reverseFileArr.join(''));

      // 行变了
      let rowChange: string[] = [];
      rs.forEach(r => {
        if(r != row) {
          let arr = [...sanCharArr];
          arr[rowIndex] = r;
          rowChange.push(arr.join(''));
        }
      });

      // 列变了
      let fileChange: string[] = [];
      fs.forEach(f => {
        if(f != file) {
          let arr = [...sanCharArr];
          arr[fileIndex] = f;
          fileChange.push(arr.join(''));
        }
      });

      // 行列都变了
      let allChange: string[] = [];
      rs.forEach(r => {
        fs.forEach(f => {
          let arr = [...sanCharArr];
          arr[fileIndex] = f;
          arr[rowIndex] = r;
          allChange.push(arr.join(''));
        });
      });

      allChange = allChange.filter(s => {
        return s != san && !rowChange.includes(s) && !fileChange.includes(s) && !rowReverse.includes(s) && !fileReverse.includes(s)
      });

      let rowMax = Math.min(2, rowChange.length);
      let fileMax = Math.min(2, fileChange.length);
      let allMax = Math.min(2, allChange.length);
      let all = rowReverse
          .concat(fileReverse)
          .concat(shuffle(rowChange).slice(0, rowMax))
          .concat(shuffle(fileChange).slice(0, fileMax))
          .concat(shuffle(allChange).slice(0, allMax));

      let answers = shuffle([...all]).slice(0, 2);
      answers.push(san);
      answers = shuffle(answers);

      //console.log(san);
      return answers;
    }
  }

  function shuffle(arr) {
    let result = [], random;
    while (arr.length > 0) {
      random = Math.floor(Math.random() * arr.length);
      result.push(arr[random]);
      arr.splice(random, 1)
    }
    return result;
  }

  function nearChars(chars, c) {
    let index = chars.indexOf(c);
    if(index === 0) {
      return [c, chars[index + 1]];
    } else if(index === chars.length - 1) {
      return [c, chars[index - 1]];
    } else {
      return [c, chars[index - 1], chars[index + 1]];
    }
  }

  function reverseRow(row) {
    return 9 - row;
  }

  function reverseFile(file) {
    let files = 'abcdefgh';
    let index = files.indexOf(file);
    return files[7 - index];
  }

  function toggleCoords(coords) {
    vm.coords = coords;
    withGround(function (g) {
        g.set(makeCgOpts());
        g.redrawAll();
      }
    );
  }

  keyboard({
    vm,
    jump,
    redraw
  });

  if(!taskProgressCtrl) {
    taskProgressCtrl = new TaskProgressCtrl('distinguishGame', redraw, opts.fr);
  }
  const socket = socketBuild({
    send: opts.socketSend,
    taskProgressSocket: taskProgressCtrl.socketHandler
  });

  initiate(opts);

  function openCreateModal(autoCreate, tab, gameId) {
    $.ajax({
      url: '/distinguish/createForm'
    }).then(function(html) {
      $.modal($(html), '', () => {
        if(autoCreate) {
          history.replaceState(null, '', '/distinguish');
        }
      });
      let $md = $('.distinguish-create');
      $md.find('.tabs-horiz span').click(function () {
        let $this = $(this);
        $md.find('.tabs-horiz span').removeClass("active");
        $md.find('.tabs-content div').removeClass("active");

        let cls = $this.attr('class');
        $this.addClass('active');
        $md.find('.tabs-content div.' + cls).addClass('active');
        $md.find('input[name=tab]').val(cls);
      });

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $tree = (<any>$jq($md)).find('.dbtree');
          $tree.jstree({
            'core': {
              'data' : {
                'url' : '/resource/gamedb/tree/loadAll',
                'data' : function (node) {
                  return { 'selected': gameId };
                }
              },
            },
            'plugins' : [ 'search' ]
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                let node = data.node;
                if (node.original.type === 'file') {
                  let id = node.id;
                  $md.find('input[name="gamedb"]').val(id.split(':')[1]);
                  $md.find('#form3-name').val(node.text);
                }
              }
            }).on('ready.jstree', function () {
            let selecteds = $tree.jstree(true).get_selected(true);
            if(autoCreate && tab === 'gamedb' && gameId) {
              selecteds.filter(s => s.id === gameId).forEach(function (node) {
                $md.find('#form3-name').val(node.text);
              });
            }
          });;

          let to = null;
          $md.find('.gamedb .search input[name="q"]').keyup(function () {
            let q = $(this).val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });
        });
      });

      $md.find('input[type=file]').on('change', function() {
        let file = this.files[0];
        if (!file) return;
        let reader = new FileReader();
        reader.onload = function(e1) {
          let r = <string> (<FileReader> e1.target).result;
          $md.find('textarea').val(r);
        };
        reader.readAsText(file);
      });

      $md.find('.tabs-content .classic .games tr input[type=radio]').change(function () {
        let title = $(this).parents('td').prev('td').find('label').html();
        $md.find('#form3-name').val(title.replace(/<br>/g," "));
      });

      if(autoCreate && tab) {
        $md.find('.tabs-horiz .' + tab).trigger('click');
      }

      if(autoCreate && tab === 'gamedb' && gameId) {
        $md.find('#form3-gamedb').val(gameId.split(':')[1]);
      }

      if(autoCreate && tab === 'game' && gameId) {
        $md.find('#form3-game').val(location.origin + '/' + gameId);
      }

      $('.cancel').click(function () {
        $.modal.close();
      });
      create($md);
    });
  }

  function create($md) {
    $md.find('form').submit(function(e) {
      e.preventDefault();

      let $form = $md.find('.form3');
      if(!$form.find('input[name=classic]:checked').val() && !$form.find('#form3-gamedb').val()
          && !$form.find('#form3-game').val() && !$form.find('#form3-pgn').val() && !$form.find('#form3-chapter').val()) {
        alert('输入一种PGN获取方式');
        return false;
      }

      $.ajax({
        method: 'POST',
        url: '/distinguish/create',
        data: $form.serialize()
      }).then(function(res) {
        location.href = `/distinguish/${res.id}`
      }, function (err) {
        handleError(err)
      });
      return false;
    });
  }

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if(typeof json.error === 'string') {
          alert(json.error);
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }

  return {
    vm,
    pref: opts.pref,
    socketReceive: socket.receive,
    getData() {
      return data;
    },
    getTree() {
      return tree;
    },
    ground,
    makeCgOpts,
    toggleCoords,
    jump,
    openCreateModal,
    start,
    finish,
    judge,
    startAgain,
    currPlyColor,
    formatMs,
    taskProgressCtrl,
    redraw,
  };
}
