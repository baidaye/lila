import * as control from './control';

const preventing = (f: () => void) => (e: MouseEvent) => {
  e.preventDefault();
  f();
};

export default function(ctrl) {
  if (!window.Mousetrap) return;
  const kbd = window.Mousetrap;
  kbd.bind(['left', 'k'], preventing(function() {
    if (ctrl.vm.stage === 'running') return;
    control.prev(ctrl);
    ctrl.redraw();
  }));
  kbd.bind(['right', 'j'], preventing(function() {
    if (ctrl.vm.stage === 'running') return;
    control.next(ctrl);
    ctrl.redraw();
  }));
  kbd.bind(['up', '0'], preventing(function() {
    if (ctrl.vm.stage === 'running') return;
    control.first(ctrl);
    ctrl.redraw();
  }));
  kbd.bind(['down', '$'], preventing(function() {
    if (ctrl.vm.stage === 'running') return;
    control.last(ctrl);
    ctrl.redraw();
  }));
}
