import throttle from 'common/throttle';
import {assetsUrl} from "../../puzzleRush/src/util";

const sounds = window.lichess.sound;

const soundUrl = assetsUrl + '/sound/';
let make = function(file, volume) {
  var sound = new window.Howl({
    src: [
      soundUrl + file + '.ogg',
      soundUrl + file + '.mp3'
    ],
    volume: volume || 1
  });
  return function() {
    if (window.lichess.sound.set() !== 'silent') sound.play();
  };
};

export const sound = {
  move: throttle(50, sounds.move),
  capture: throttle(50, sounds.capture),
  check: throttle(50, sounds.check),
  win: throttle(50, make('other/win', 0.1)),
  loss: throttle(50, make('other/loss', 0.1))
};
