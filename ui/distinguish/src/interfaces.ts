import { VNode } from 'snabbdom/vnode'

export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface Controller {
  vm: Vm;
  [key: string]: any;
}

export interface Vm {
  initialPath: Tree.Path;
  initialNode: Tree.Node;

  previousPath: Tree.Path;
  currentPath: Tree.Path;
  currentNode: Tree.Node;
  nodeList: Tree.Node[];
  mainline: Tree.Node[];

  answers: string[];
  loading: boolean;
  autoScrollRequested: boolean;
  autoScrollNow: boolean;
  stage: string; // pending, running, finished
  orientation: string; // white, black
  coords: boolean;
  turns: number;//应走步数
  rightTurns: number;//应走步数中的应该正确步数
  maxTurns: number;
  currWin: boolean;//当前步是否正确
  currAnswer: string;
  currTurns: number;//已走步数
  wins: number;//已走正确步数
  cgConfig: any;
  historys: any;
  readonly: boolean;
  hashId: string;
  startTimeMs: number;
  timeMs: number;
}
