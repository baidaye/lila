export default function(opts) {

  let handlers = {

  };

  return {
    send: opts.send,
    receive: function(type, data) {
      if (handlers[type]) {
        handlers[type](data);
        return true;
      }
      return !!opts.taskProgressSocket && opts.taskProgressSocket(type, data);
    }
  };
}
