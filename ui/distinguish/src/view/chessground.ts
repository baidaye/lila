import {h} from 'snabbdom'
import resizeHandle from 'common/resize';
import {Chessground} from 'chessground';
import {Config as CgConfig} from 'chessground/config';

export default function(ctrl) {
  return h('div.cg-wrap', {
    hook: {
      insert: vnode => ctrl.ground(Chessground((vnode.elm as HTMLElement), makeConfig(ctrl))),
      destroy: _ => ctrl.ground().destroy()
    }
  });
}

function makeConfig(ctrl): CgConfig {
  const opts = ctrl.makeCgOpts();
  const config = {
    fen: opts.fen,
    orientation: opts.orientation,
    turnColor: opts.turnColor,
    check: opts.check,
    coordinates: opts.coordinates,
    viewOnly: true,
    addPieceZIndex: ctrl.pref.is3d,
    draggable: {
      enabled: ctrl.pref.moveEvent > 0,
      showGhost: ctrl.pref.highlight
    },
    selectable: {
      enabled: ctrl.pref.moveEvent !== 1
    },
    events: {
      move: ctrl.userMove,
      insert(elements) {
        resizeHandle(
            elements,
            ctrl.pref.resizeHandle,
            ctrl.vm.currentNode.ply,
            (_) => true
        )
      }
    },
    highlight: {
      lastMove: ctrl.pref.highlight,
      check: ctrl.pref.highlight
    },
    animation: {
      enabled: true,
      duration: ctrl.pref.animation.duration
    },
    disableContextMenu: true
  };
  return config;
}

