import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Controller} from "../interfaces";
import {bind, dataIcon, spinner} from "../util";

export default function(ctrl: Controller): VNode | undefined {
  let data = ctrl.getData();
  return h('div.practice-box', [
    h('div.title', data.distinguish.name),
    ctrl.vm.stage === 'pending' ? pending(ctrl) : null,
    ctrl.vm.stage === 'running' ? running(ctrl) : null,
    ctrl.vm.stage === 'finished' ? finished(ctrl) : null
  ]);
}

function pending(ctrl: Controller) {
  return h('div.pending', [
    h('div.conf', [
      h('div.switch', [
        h('strong', '显示棋盘坐标'),
        h('input.cmn-toggle', {
          attrs: {
            id: 'change-coords',
            type: 'checkbox',
            checked: ctrl.vm.coords
          },
          hook: bind('change', e => {
            ctrl.toggleCoords((e.target as HTMLInputElement).checked);
          })
        }),
        h('label', { attrs: { 'for': 'change-coords' } })
      ]),
      h('div.turns', [
        h('label', '走棋步数：'),
        h('input', {
          attrs: {
            id: 'input-turns',
            type: 'number',
            min: '1',
            max: '500',
            disabled: ctrl.vm.readonly,
            value: ctrl.vm.turns
          },
          hook: bind('change', e => {
            let $this = $(e.target as HTMLElement);
            let turns = $this.val() as number;
            turns = Math.min(turns, ctrl.vm.maxTurns);
            $this.val(turns);
            ctrl.vm.turns = turns;
          })
        })
      ])
    ]),
    h('div.action', h('button.button', {
      class: {
        'disabled': ctrl.getData().distinguish.id === 'synthetic'
      },
      attrs: {
        'disabled': ctrl.getData().distinguish.id === 'synthetic'
      },
      hook: bind('click', e => {
        ctrl.start();
      })
    }, '开始练习'))
  ])
}

function running(ctrl: Controller) {
  let color = ctrl.currPlyColor();
  return h('div.feedback', [
    h('div.player', [
      ctrl.vm.loading ? h('div.result', {
        class: {
          'win': ctrl.vm.currWin,
          'fail': !ctrl.vm.currWin,
        }
      },[
          h('icon', ctrl.vm.currWin ? '✓' : '✗'),
          h('span', ctrl.vm.currWin ? '正确' : '错误，答案是 ' + ctrl.vm.currAnswer)
      ]) : null,
      ctrl.vm.loading ? null : h('div.no-square', h('piece.king.'+ color)),
      ctrl.vm.loading ? null : h('div.judge', [
          h('div.message', '请选择刚刚的走棋:'),
          h('div.answers', ctrl.vm.answers.map(function (san) {
          return h('div.radio', [
            h('input', {
              attrs: {
                type: 'radio', name: 'move', id: 'move-' + san, value: san, checked: false
              },
              hook: bind('click', e => {
                ctrl.judge((e.target as HTMLInputElement).value);
              })
            }),
            h('label', { attrs: { for: 'move-' + san }}, san)
          ])
        }))
      ]),
      h('div.action', h('button.button.button-red', {
        hook: bind('click', e => {
          ctrl.finish()
        })
      }, '结束'))
    ]),
    h('div.progress', [
      h('div.text', [
        '进度：', ctrl.vm.currTurns, '/', ctrl.vm.turns
      ]),
      h('div.bar', {
        attrs: {
          style:  'width: ' + (ctrl.vm.currTurns / ctrl.vm.turns * 100) + '%'
        }
      })
    ])
  ])
}

function finished(ctrl: Controller) {
  return h('div.finished', [
    h('div.infos', [
      h('span.progress', [
        '进度：', ctrl.vm.currTurns, '/', ctrl.vm.turns
      ]),
      h('span.turns', [
        '正确率：', ctrl.vm.wins, '/', ctrl.vm.currTurns
      ]),
      h('span.spend', [
        '用时：', ctrl.formatMs(ctrl.vm.timeMs)
      ])
    ]),
    h('div.action', h('button.button', {
      hook: bind('click', e => {
        ctrl.startAgain();
      })
    }, '再来一次'))
  ])
}
