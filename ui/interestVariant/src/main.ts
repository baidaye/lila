import makeCtrl from './ctrl';
import view from './view/main';
import {Chessground} from 'chessground';
import InterestVariantController from './ctrl';
import {VNode} from 'snabbdom/vnode'
import {init} from 'snabbdom';
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import {menuHover} from 'common/menuHover';

menuHover();

const patch = init([klass, attributes]);

export default function (opts) {

  let vnode: VNode, ctrl: InterestVariantController;

  function redraw() {
    vnode = patch(vnode, view(ctrl));
  }

  ctrl = new makeCtrl(opts, redraw);

  const v = view(ctrl);
  opts.element.innerHTML = '';
  vnode = patch(opts.element, v);

  return {
    socketReceive: ctrl.socket.receive
  };
};

// that's for the rest of lichess to access chessground
// without having to include it a second time
window.Chessground = Chessground;
