import InterestVariantController from './ctrl';

interface Handlers {
  [key: string]: (data: any) => void;
}
export default class InterestVariantSocket {

  send: SocketSend;
  handlers: Handlers;

  constructor(send: SocketSend, ctrl: InterestVariantController) {
    console.log(ctrl.name);
    this.send = send;
    this.handlers = {

    }
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  }
};
