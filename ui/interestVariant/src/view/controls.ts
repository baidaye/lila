import {h} from 'snabbdom';
import InterestVariantController from '../ctrl';

function dataAct(e) {
  return e.target.getAttribute('data-act') || e.target.parentNode.getAttribute('data-act');
}

function jumpButton(icon, effect, enabled: boolean) {
  return h('button.fbt', {
    class: { disabled: !enabled },
    attrs: {
      'data-act': effect,
      'data-icon': icon
    }
  });
}

export default function(ctrl: InterestVariantController) {
  return ctrl.opts.source.id === 'g6' ? h('div.interest__controls.analyse-controls', {
    hook: {
      insert(vnode) {
        $(vnode.elm as HTMLElement).on('click', (e) => {
          const action = dataAct(e);
          if (action === 'prev') ctrl.prev();
          else if (action === 'next') ctrl.next();
          else if (action === 'first') ctrl.first();
          else if (action === 'last') ctrl.last();
        });
      },
      postpatch: (_, vnode) => {
        $(vnode.elm as HTMLElement).off('click').on('click', (e) => {
          const action = dataAct(e);
          if (action === 'prev') ctrl.prev();
          else if (action === 'next') ctrl.next();
          else if (action === 'first') ctrl.first();
          else if (action === 'last') ctrl.last();
          else if (action === 'flip') ctrl.flip();
        });
      }
    }
  }, [
    h('div.jumps', [
      jumpButton('W', 'first', ctrl.canFirst()),
      jumpButton('Y', 'prev', ctrl.canPrev()),
      jumpButton('X', 'next', ctrl.canNext()),
      jumpButton('V', 'last', ctrl.canLast()),
      jumpButton('B', 'flip', true)
    ])
  ]) : null;
}

