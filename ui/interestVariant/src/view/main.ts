import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import * as gridHacks from './gridHacks';
import InterestVariantController from '../ctrl';
import {view as renderPromotion} from './promotion';
import * as chessground from './ground';
import * as table from './table';
import { metas, tags } from './side';

export default function(ctrl: InterestVariantController): VNode {
  let cls = {};
  cls[ctrl.opts.source.id] = true;
  return h(`main.interest`, {
    class: cls,
    hook: {
      postpatch(_, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
        if (!ctrl.drawer) {
          ctrl.drawer = (<any>$('.drawer')).drawer();
        }
      }
    }
  }, [
    h('aside.interest__side', [
      metas(ctrl),
      tags(ctrl)
    ]),
    h('div.interest__board.main-board', [
      chessground.render(ctrl),
      renderPromotion(ctrl),
    ]),
    h('div.interest__table', [
      table.main(ctrl),
      table.actions(ctrl)
    ])
  ]);
}
