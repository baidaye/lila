import {h} from 'snabbdom';
import {bind} from '../util';
import InterestVariantController from '../ctrl';
import controls from "./controls";
import {assetsUrl} from "../../../puzzleRush/src/util";

export function main(ctrl: InterestVariantController) {
  return h('div.main', [
    ctrl.opts.source.id === 'g7' ? h('div.score', ctrl.score.toString()) : null,
    ctrl.completed ? h('div.result', {
     class: {
       win: ctrl.result === 'win',
       loss: ctrl.result === 'loss',
       draw: ctrl.result === 'draw'
     }
    }, [
      h('h2', resultText(ctrl))
    ]) : null,
    ctrl.opts.source.id === 'g6' ? h('div.octopus', h('img', { attrs: { src: assetsUrl + '/images/mascot/octopus.svg'}})) : null
  ]);
}

function resultText(ctrl: InterestVariantController) {
  switch (ctrl.result) {
    case 'win':
      return '恭喜！挑战成功';
    case 'loss':
      return '再加油哦!';
    case 'draw':
      return '和棋';
    default:
        return ''
  }
}

export function actions(ctrl: InterestVariantController) {
  return h('div.actions', {
    class: {
      normal: ctrl.started || ctrl.opts.source.id === 'g7'
    }
  }, [
    h('div.buttons',[
      ctrl.started ? h('button.button.button-red', {
        hook: bind('click', _ => {
          ctrl.stop();
        })
      }, '结束训练') : null,
      !ctrl.started ? h('button.button.button-empty', {
        hook: bind('click', _ => {
          ctrl.openDrawer();
        })
      }, '修改设置') : null,
      !ctrl.started ? h('button.button', {
        hook: bind('click', _ => {
          ctrl.start();
        })
      }, '重新开始') : null,
      ctrl.opts.source.id === 'g6' && !ctrl.started ? h('button.button', {
        attrs: {
          disabled: !ctrl.cansStartFrom()
        },
        class: {
          disabled: !ctrl.cansStartFrom()
        },
        hook: bind('click', _ => {
          ctrl.startFrom();
        })
      }, '从当前位置开始') : null,
    ]),
    controls(ctrl)
  ]);
}
