import { h } from 'snabbdom';
import InterestVariantController from '../ctrl';

export function metas(ctrl: InterestVariantController) {
  return h('div.metas.box', [
    h('h1', ctrl.opts.source.name),
    h('div.desc', ctrl.opts.source.desc)
  ]);
}

export function tags(ctrl: InterestVariantController) {
  const rookColor = ctrl.$form.find('input[name^="rookColor"]:checked').next('label').text();
  const color = ctrl.$form.find('input[name^="color"]:checked').next('label').text();
  const level = ctrl.$form.find('input[name^="level"]:checked').next('label').text();
  const situation = ctrl.$form.find('input[name^="situation"]:checked').next('label').text();

  return h('div.tags', [
    ctrl.opts.source.id === 'g7' ? h('span', rookColor) : null,
    h('span', color),
    h('span', `AI ${level}`),
    ctrl.opts.source.id === 'g6' ? h('span', situation) : null
  ]);
}
