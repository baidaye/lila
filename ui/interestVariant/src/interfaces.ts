import { VNode } from 'snabbdom/vnode'
import * as cg from 'chessground/types';

export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

interface Untyped {
  [key: string]: any;
}

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}

export interface InterestVariantOpts extends Untyped {
  element: HTMLElement;
  socketSend: SocketSend;
  userId: string;
  notAccept: boolean;
  pref: any;
}

export interface InterestVariantSubCtrl {
  initFile(): VariantInitFile;
  completed(capture?: JustCaptured, prom?: cg.Role): string; // win|loss|draw|''
  randomFen(whiteRook: boolean): string;
  validFen(fen: string): ValidFenResult;
}

export interface ValidFenResult {
  ok: boolean;
  error?: string;
  normalization?: string;
}

export interface VariantInitFile {
  name: string;
  content: string;
}

export interface VariantLevel {
  depth: number;
  movetime: number;
}

export interface Board {
  fen: string;
  lastMove?: cg.Key[];
}
