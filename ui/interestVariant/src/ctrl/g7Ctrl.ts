import InterestVariantController from '../ctrl';
import {InterestVariantSubCtrl, JustCaptured, ValidFenResult, VariantInitFile} from "../interfaces";
import * as cg from 'chessground/types';

// 双车捉双象
export default function makeCtrl(superCtrl: InterestVariantController): InterestVariantSubCtrl {

  console.log(superCtrl.name);

  const fens = [{"is_white_rook":true,"is_white_attacked":true,"fen":"5R2/7R/8/8/4b3/8/8/6b1 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"4b3/8/1R6/8/8/8/5b2/R7 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/8/8/5RR1/b7/4b3/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/8/7R/2b5/7R/8/6b1 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/4b3/7R/5b2/3R4/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"7R/2b5/6b1/8/5R2/8/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"7b/5b2/8/8/8/6R1/8/2R5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/1R3R2/4b3/8/8/b7/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/3b4/5R2/2b5/8/8/8/4R3 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/6b1/8/8/1R6/5b2/2R5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"b7/8/8/8/7R/7R/3b4/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"4R3/7b/8/6b1/8/4R3/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"1R6/6b1/8/3b4/8/8/8/5R2 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/5R2/R7/8/8/7b/1b6 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"7b/1b6/8/8/R7/8/R7/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/3b4/2R5/4b3/8/8/2R5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"6R1/8/8/8/1R6/8/b7/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"3b4/8/6b1/4R3/8/7R/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/5b2/3R4/8/8/3R4/8/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"3b4/5R2/b7/8/8/8/6R1/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/8/8/3b4/8/4b3/R1R5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"b7/1R6/7b/8/8/8/8/3R4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/6R1/8/5R2/1b6/8/8/3b4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/R7/6b1/R7/5b2/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"2R5/8/8/8/8/6b1/R7/3b4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/3R4/8/8/7b/6R1/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"1b6/8/4R3/8/8/8/5R2/3b4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/6b1/8/8/3R4/5b2/2R5/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"6RR/8/8/8/8/5b2/8/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/4R3/6R1/b7/8/8/8/3b4 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/8/3rB3/1r6/8/2B5/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/1r3r1B/8/8/8/8/8/2B5 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/6B1/1rB5/8/8/8/7r/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/1r6/7B/7r/8/8/2B5 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/8/3r4/6r1/8/3B4/5B2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/1r6/3B4/8/6B1/8/3r4 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/2B5/8/7B/8/r7/2r5 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"r7/3B4/8/8/1B6/8/8/5r2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"4r3/8/8/8/8/3Br3/8/B7 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/4r3/8/8/8/6B1/Br6/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"r7/8/4B3/8/r6B/8/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/5r2/B7/8/8/8/1B5r/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/Br6/8/7B/8/8/1r6 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/1B6/4r3/6B1/8/2r5/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"2B5/8/8/8/8/1r6/5B2/1r6 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/8/6B1/1r1r4/8/B7/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/B7/8/3r4/8/8/2r5/5B2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/6rr/8/8/7B/1B6/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"3B4/8/4B3/8/r7/8/8/7r w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/1B6/8/B7/r7/6r1/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/8/8/6r1/8/7B/1r3B2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/5r2/8/B7/8/8/1r6/3B4 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"B7/8/1B6/8/8/7r/8/r7 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/r7/2r4B/8/6B1/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"2r5/r7/8/8/5B2/8/2B5/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/2B5/8/8/8/5r2/B2r4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/8/5R2/3b4/7b/8/4R3 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"4R3/8/8/3R4/2b5/8/8/b7 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"3R4/8/6R1/8/8/7b/8/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/8/8/3R4/6R1/1b6/5b2 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/b7/6R1/6R1/4b3/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"4b3/6R1/7b/2R5/8/8/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/3R4/2b5/8/1b6/4R3/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"5b2/8/8/7R/8/1b6/7R/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"6b1/8/1R3R2/8/8/8/3b4/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"2R5/6b1/8/1R6/8/8/4b3/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/4b3/3R4/3R4/8/8/8/5b2 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/8/b7/6R1/8/7R/3b4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"7R/8/6b1/8/8/8/5R2/2b5 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"3b4/8/2R5/8/8/2R5/b7/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/8/7b/8/1RR5/8/8/5b2 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"4b3/8/3R4/3R4/1b6/8/8/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":true,"fen":"8/1R6/8/8/2b5/8/5b2/6R1 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"3R4/8/8/7R/8/6b1/2b5/8 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/8/6b1/8/8/8/7b/2RR4 w - - 0 1"},
    {"is_white_rook":true,"is_white_attacked":false,"fen":"8/6R1/8/8/1b6/3b4/8/2R5 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"r1r5/B7/8/7B/8/8/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/1rB5/8/7B/8/8/8/1r6 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/8/3r4/r7/7B/8/4B3/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/1B6/5B2/8/8/r7/8/4r3 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"4r3/8/8/8/8/6Br/8/1B6 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"7r/7B/8/8/8/1r6/8/6B1 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"7B/8/8/3B4/8/8/3rr3/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/8/2B5/8/6r1/3r4/3B4 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"2r5/8/5B2/1r6/8/1B6/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/2B5/4B3/8/4r3/8/5r2/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"6r1/8/1B6/8/8/2r5/2B5/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"8/4B3/8/5r2/8/2r5/8/7B w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"B7/8/1B6/8/5r2/8/2r5/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"6B1/8/8/4B3/8/3r1r2/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"5B2/1B6/8/8/8/8/4r3/4r3 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":false,"fen":"r7/8/6r1/8/8/2B5/8/5B2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"1r6/4B3/8/8/8/1B6/1r6/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"1B6/8/8/8/8/2r5/6B1/6r1 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/1r6/7B/8/1r6/8/8/1B6 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/r7/5B2/7B/8/8/8/5r2 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/1r4B1/4B3/8/8/3r4/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/4r3/6B1/3rB3/8/8/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"5B2/8/8/3B2r1/8/8/7r/8 w - - 0 1"},
    {"is_white_rook":false,"is_white_attacked":true,"fen":"8/8/2B2r2/8/1B6/8/7r/8 w - - 0 1"}];

  function initFile(): VariantInitFile {
    return {
      name: 'dualRooksandBishops',
      content:
        '[dualRooksandBishops]\n' +
        'rook = r\n' +
        'bishop = b\n' +
        'startFen = bb6/8/8/8/8/2RR4/8/8 w - - 0 1\n' +
        'startFen = b7/8/1b6/8/8/2RR4/8/8 w - - 0 1\n' +
        'extinctionPieceTypes = *\n' +
        'extinctionPieceCount = 1\n' +
        'extinctionValue = loss'
    };
  }

  function completed(capture?: JustCaptured, prom?: cg.Role): string {
    if(prom) console.log(prom);
    if(!!capture) {
      if(superCtrl.isAiTurn()) return 'win';
      else return 'loss';
    }
    return '';
  }

  function randomFen(whiteRook: boolean) {
    let idx = Math.floor((Math.random() * 50));
    return fens.filter(d => d.is_white_rook === whiteRook)[idx].fen;
  }

  return {
    initFile,
    completed,
    randomFen,
    validFen(fen: string): ValidFenResult { return {ok: !!fen, normalization: fen} }
  }

}
