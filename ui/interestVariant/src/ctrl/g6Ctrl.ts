import * as ChessJS from 'chess.js';
import * as cg from 'chessground/types';
import InterestVariantController from '../ctrl';
import {InterestVariantSubCtrl, JustCaptured, VariantInitFile, ValidFenResult} from '../interfaces';

// 小兵大战
export default function makeCtrl(superCtrl: InterestVariantController): InterestVariantSubCtrl {

  let chess = ChessJS.Chess();

  function initFile(): VariantInitFile {

    return {
      name: 'pawnsonly',
      content:
        '[pawnsonly]\n'
        + 'pawn = p\n'
        + 'queen = q\n'
        + 'startFen = 8/pppppppp/8/8/8/8/PPPPPPPP/8 w - - 0 1\n'
        + 'promotionPieceTypes = q\n'
        + 'castling = false\n'
        + 'flagPiece = q\n'
        + 'whiteFlag = *8\n'
        + 'blackFlag = *1'
    };
  }

  function completed(capture?: JustCaptured, prom?: cg.Role): string {
    if(capture) console.log(capture);
    let scm = selfCanMove();
    let ocm = oppositeCanMove();
    let promotion = !!prom;
    if(promotion || !scm) {
      if(promotion) {
        if(superCtrl.isAiTurn()) return 'win';
        else return 'loss';
      } else {
        if(!scm) {
          if(!ocm) return 'draw';
          else {
            if(superCtrl.isAiTurn()) return 'win';
            else return 'loss';
          }
        }
        return '';
      }
    }
    return '';
  }

  function selfCanMove() {
    return canMove(superCtrl.chess.fen());
  }

  function oppositeCanMove() {
    let color = superCtrl.chess.turn();
    let fen = superCtrl.chess.fen();
    let oppositeColor = color === 'w' ? 'b' : 'w';
    let newFen = setFenTurn(fen, oppositeColor);
    return canMove(newFen);
  }

  function canMove(fen) {
    chess.load(fen);
    let dests = Object.keys(toDests());
    return dests.length > 0;
  }

  function setFenTurn(fen, turn) {
    return fen.replace(/ (w|b) /, ' ' + turn + ' ');
  }

  function toDests(): cg.Dests {
    const dests = {};
    chess.SQUARES.forEach(s => {
      const ms = chess.moves({
        square: s,
        verbose: true,
        legal: false
      });
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests;
  }

  function validFen(fen: string): ValidFenResult {

    const tokens = fen.split(/\s+/);
    if (tokens.length < 2) {
      return {
        ok: false,
        error: 'FEN 至少包含两个部分',
      }
    }

    if (!/^(w|b)$/.test(tokens[1])) {
      return { ok: false, error: 'FEN 第二个部分必须为w或b' }
    }

    const rows = tokens[0].split('/');
    if (rows.length !== 8) {
      return {
        ok: false,
        error: "FEN 第一部分必须由8段组成",
      }
    }

    for (let i = 0; i < rows.length; i++) {
      let sumFields = 0;
      let previousWasNumber = false;

      for (let k = 0; k < rows[i].length; k++) {
        if (isDigit(rows[i][k])) {
          if (previousWasNumber) {
            return {
              ok: false,
              error: 'FEN 中数字不能时连续的',
            }
          }
          sumFields += parseInt(rows[i][k], 10);
          previousWasNumber = true
        } else {
          if (!/^[pP]$/.test(rows[i][k])) {
            return {
              ok: false,
              error: 'FEN 中只能包含棋子兵',
            }
          }
          sumFields += 1;
          previousWasNumber = false
        }
      }
      if (sumFields !== 8) {
        return {
          ok: false,
          error: 'Invalid FEN: piece data is invalid (too many squares in rank)',
        }
      }
    }

    if (
      Array.from(rows[0] + rows[7]).some((char) => char.toUpperCase() === 'P')
    ) {
      return {
        ok: false,
        error: '兵不能在底线',
      }
    }

    return {
      ok: true,
      normalization: tokens[0] + ' ' + tokens[1] + ' - - 0 1'
    }
  }

  function isDigit(c: string): boolean {
    return '0123456789'.indexOf(c) !== -1
  }

  return {
    initFile,
    completed,
    randomFen(whiteRook: boolean): string { console.log(whiteRook); return '' },
    validFen
  }
}
