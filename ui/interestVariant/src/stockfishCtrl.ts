import InterestVariantController from './ctrl';
import {VariantInitFile} from "./interfaces";

export default class stockfishCtrl {

  stockfish: any;
  stockfishReady: boolean = false;

  started: boolean = false;

  constructor(readonly ctrl: InterestVariantController) {
    this.loadStockfish();
  }

  loadStockfish = () => {
    if(this.isSupported()) {
      // @ts-ignore
      Stockfish().then((sf) => {
        this.stockfish = sf;
        this.stockfish.addMessageListener((line) => {
          this.received(line);
        });
        this.stockfishReady = true;
        this.setVariant(this.ctrl.subCtrl.initFile());
      });
    } else alert('很遗憾，该功能暂不支持移动端用户');
  };

  received = (line: string) => {
    //console.log('receivedMessage: ', line);
    if (line.startsWith("bestmove")) {
      let bestmove = line.split(" ")[1];
      if(this.ctrl.isAiTurn()) {
        this.ctrl.playUci(bestmove);
      }
    }
  };

  // pawnsonly
  setVariant = (int: VariantInitFile) => {
    if(this.started) this.stop();
    const fileName = `${int.name}.ini`;
    const fileContent = int.content;

    const array = new TextEncoder().encode(fileContent);
    const FS = this.stockfish.FS;
    FS.writeFile(fileName, array);

    this.send(`check ${fileName}`);
    this.send(`load ${fileName}`);
    this.send(`uci`);
    this.send(`setoption name UCI_Variant value ${int.name}`);
    this.send(`position startpos`);

    this.ctrl.start();
  };

  start = (fen: string) => {
    this.stop();
    this.started = true;

    this.send(`setoption name Hash value 8`);
    this.send(`setoption name MultiPV value 1`);
    this.send(`setoption name Threads value 1`);
    this.send(`position fen ${fen}`);
    this.send(`go movetime ${this.ctrl.level.movetime} depth ${this.ctrl.level.depth}`);
  };

  stop = () => {
    this.send(`stop`);
  };

  send = (cmd: string) => {
    //console.log('postMessage: ', cmd);
    this.stockfish.postMessage(cmd);
  };

  isSupported = () => {
    if (typeof WebAssembly !== "object") return false;
    const source = Uint8Array.from([
      0, 97, 115, 109, 1, 0, 0, 0, 1, 5, 1, 96, 0, 1, 123, 3, 2, 1, 0, 7, 8,
      1, 4, 116, 101, 115, 116, 0, 0, 10, 15, 1, 13, 0, 65, 0, 253, 17, 65, 0,
      253, 17, 253, 186, 1, 11,
    ]);
    if (typeof WebAssembly.validate !== "function" || !WebAssembly.validate(source)) return false;
    if (typeof Atomics !== "object") return false;
    if (typeof SharedArrayBuffer !== "function") return false;
    return true;
  };


}

