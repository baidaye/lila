import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import * as chessUtil from 'chess';
import * as ChessJS from "chess.js";
import InterestVariantSocket from './socket';
import makeG6Ctrl from './ctrl/g6Ctrl';
import makeG7Ctrl from './ctrl/g7Ctrl';
import StockfishCtrl from './stockfishCtrl';
import {sound} from './sound';
import {
  InterestVariantOpts,
  JustCaptured,
  InterestVariantSubCtrl,
  VariantLevel,
  Board,
  ValidFenResult
} from './interfaces';

export default class InterestVariantController {

  name: string = 'InterestVariantController';

  opts: InterestVariantOpts;
  socket: InterestVariantSocket;
  subCtrl: InterestVariantSubCtrl;

  drawer: any;

  chessground: CgApi;
  cgConfig: CgConfig;
  flipped: boolean = false;
  lastMove?: cg.Key[];

  chess: any; // chess.js
  stockfishCtrl: StockfishCtrl;

  started: boolean = false;
  completed: boolean = false;
  result: string = '';// win|loss|draw
  score: number = 0;

  color: Color;
  level: VariantLevel;
  initFen: string;
  validResult: ValidFenResult;

  $form: JQuery = $('.search_form');

  history: Board[] = [];
  historyActiveIdx: number = 0;

  constructor(opts: InterestVariantOpts, readonly redraw: () => void) {
    this.opts = opts;
    this.socket = new InterestVariantSocket(opts.socketSend, this);
    this.subCtrl = this.makeSubCtrl();
    this.chess = ChessJS.Chess('8/8/8/8/8/8/8/8 w - -');
    this.stockfishCtrl = new StockfishCtrl(this);
    this.initArgs();
    this.setUri();
    this.resetForm();
    this.setFen();
    this.submitForm();
  }

  makeSubCtrl = (): InterestVariantSubCtrl => {
    switch (this.opts.source.id) {
      case 'g6':
        return makeG6Ctrl(this);
      case 'g7':
        return makeG7Ctrl(this);
      default:
        throw new Error('can not init SubCtrl');
    }
  };

  makeCgOpts(): CgConfig {
    const fen = this.chess.fen();
    const color = this.getColor();
    const orientation = this.bottomColor();
    const check = this.chess.in_check();
    const dests = this.completed || !this.started ? {} : this.toDests();
    const config: CgConfig = {
      fen: fen,
      turnColor: color,
      viewOnly: false,
      orientation: orientation,
      movable: {
        color: color,
        dests: dests
      },
      check: check,
      lastMove: this.lastMove,
      selectable: {
        enabled: this.opts.pref.moveEvent !== 1
      },
      drawable: {
        enabled: false,
        shapes: [],
        autoShapes: []
      }
    };

    if (check) {
      config.turnColor = opposite(color);
    }
    this.cgConfig = config;
    return config;
  }

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  showGround = (): void => {
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
    });
  };

  flip = () => {
    this.flipped = !this.flipped;
    this.chessground.set({
      orientation: this.bottomColor()
    });
    this.redraw();
  };

  uciToLastMove = (lm?: string): Key[] | undefined => {
    return lm ? ([lm[0] + lm[1], lm[2] + lm[3]] as Key[]) : undefined;
  };

  playUci = (uci: Uci): void => {
    const move = chessUtil.decomposeUci(uci);
    const capture = this.chessground.state.pieces[move[1]];
    const promotion = move[2] && chessUtil.sanToRole[move[2].toUpperCase()];
    this.sendMove(move[0], move[1], capture, promotion);
  };

  aiColor = () => {
    return opposite(this.color);
  };

  isUserTurn = () => {
    return this.chess.turn() === this.color[0];
  };

  isAiTurn = () => {
    return this.chess.turn() === this.aiColor()[0];
  };

  aiMove = () => {
    if(this.isAiTurn()) {
      setTimeout(() => {
        this.stockfishCtrl.start(this.chess.fen());
      }, 500);
    }
  };

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    if (this.isPromotion(dest)) {
      this.sendMove(orig, dest, capture, 'queen');
    } else this.sendMove(orig, dest, capture);
  };

  isPromotion = (dest: Key): boolean => {
    let state = this.chessground.state;
    let piece = state.pieces[dest];
    let turnColor = state.turnColor;
    if(piece) {
      return (piece.role == 'pawn' && ((dest[1] == '8' && turnColor == 'black') || (dest[1] == '1' && turnColor == 'white')));
    }
    return false;
  };

  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    if(this.isUserTurn()) {
      this.score = this.score + 1;
      this.redraw();
    }
    this.lastMove = [orig, dest];
    this.chess.move({
      from: orig,
      to: dest,
      promotion: prom ? chessUtil.roleToSan[prom].toLowerCase() : null
    });

    this.history.push({
      lastMove: [orig, dest],
      fen: this.chess.fen()
    });

    let result = this.subCtrl.completed(capture, prom);
    if(result) {
      switch (result) {
        case 'win': this.onSuccess(); break;
        case 'loss': this.onFailed(); break;
        case 'draw': this.onDraw(); break;
      }
    }

    this.showGround();
    if(!this.completed) {
      if(capture) sound.capture();
      else sound.move();
    }

    if(!this.completed) {
      this.aiMove();
    }
  };

  bottomColor = () => {
    return this.flipped ? opposite(this.color) : this.color;
  };

  toDests = (): cg.Dests => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({
        square: s,
        verbose: true,
        legal: false
      });
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests;
  };

  getColor = () => {
    return this.chess.turn() == 'w' ? 'white' : 'black';
  };

  start = (fen?: string) => {
    this.initArgs(fen);
    this.lastMove = undefined;
    this.completed = false;
    this.result = '';
    this.score = 0;
    this.started = true;
    this.history = [];
    this.historyActiveIdx = 0;

    this.setUri();
    this.chess.load(this.initFen);
    this.showGround();
    sound.start();
    this.redraw();

    this.history.push({
      lastMove: this.lastMove,
      fen: this.chess.fen()
    });

    this.aiMove();
  };

  cansStartFrom = () => {
    return this.historyActiveIdx > 0 && this.historyActiveIdx < this.history.length - 1
  };

  startFrom = () => {
    let fen = this.history[this.historyActiveIdx].fen;
    this.start(fen);
  };

  stop = () => {
    this.completed = false;
    this.result = '';
    this.started = false;
    this.historyActiveIdx = this.history.length - 1;
    this.showGround();
    sound.end();
    this.redraw();
  };

  onSuccess = () => {
    this.completed = true;
    this.result = 'win';
    this.started = false;
    this.historyActiveIdx = this.history.length - 1;
    sound.win();
    this.redraw();
  };

  onFailed = () => {
    this.completed = true;
    this.result = 'loss';
    this.started = false;
    this.historyActiveIdx = this.history.length - 1;
    sound.loss();
    this.redraw();
  };

  onDraw = () => {
    this.completed = true;
    this.result = 'draw';
    this.started = false;
    this.historyActiveIdx = this.history.length - 1;
    sound.end();
    this.redraw();
  };

  openDrawer = () => {
    if(this.drawer) this.drawer.open();
  };

  canPrev = () => {
    return !this.started && this.historyActiveIdx > 0;
  };

  prev = () => {
    if(!this.canPrev()) return;
    this.historyActiveIdx = this.historyActiveIdx - 1;
    this.jumpTo(this.history[this.historyActiveIdx]);
  };

  canNext = () => {
    return !this.started && this.historyActiveIdx < this.history.length - 1;
  };

  next = () => {
    if(!this.canNext()) return;
    this.historyActiveIdx = this.historyActiveIdx + 1;
    this.jumpTo(this.history[this.historyActiveIdx]);
  };

  canFirst = () => {
    return !this.started && this.historyActiveIdx > 0;
  };

  first = () => {
    if(!this.canFirst()) return;
    this.historyActiveIdx = 0;
    this.jumpTo(this.history[this.historyActiveIdx]);
  };

  canLast = () => {
    return !this.started && this.historyActiveIdx < this.history.length - 1;
  };

  last = () => {
    if(!this.canLast()) return;
    this.historyActiveIdx = this.history.length - 1;
    this.jumpTo(this.history[this.historyActiveIdx]);
  };

  jumpTo = (board: Board) => {
    this.lastMove = board.lastMove;
    this.chess.load(board.fen);
    this.showGround();
    this.redraw();
  };

  resetForm = () => {
    this.$form.find('.reset').click(() => {
      const defaultLevel = '6-5000';
      switch (this.opts.source.id) {
        case 'g6': {
          const defaultFen = '8/pppppppp/8/8/8/8/PPPPPPPP/8 w - - 0 1';

          this.$form.find('input[name="color"]').prop('checked', false);
          this.$form.find('#color_black').prop('checked', true);

          this.$form.find('input[name="level"]').prop('checked', false);
          this.$form.find(`#level_${defaultLevel}`).prop('checked', true);

          this.$form.find('input[name="situation"]').prop('checked', false);
          this.$form.find(`#level_8pawn`).prop('checked', true);

          this.$form.find('input[name="fen"]').val(defaultFen);
          this.$form.find('.init-fen').data('fen', defaultFen).addClass('parse-fen');
          window.lichess.pubsub.emit('content_loaded');
          this.$form.submit();
          break;
        }
        case 'g7': {
          this.$form.find('input[name="rookColor"]').prop('checked', false);
          this.$form.find('#rookColor_white').prop('checked', true);

          this.$form.find('input[name="color"]').prop('checked', false);
          this.$form.find('#color_black').prop('checked', true);

          this.$form.find('input[name="level"]').prop('checked', false);
          this.$form.find(`#level_${defaultLevel}`).prop('checked', true);

          let fen = this.subCtrl.randomFen(true);
          this.$form.find('input[name="fen"]').val(fen);
          this.$form.find('.init-fen').data('fen', fen).addClass('parse-fen');
          window.lichess.pubsub.emit('content_loaded');
          this.$form.submit();
          break;
        }
        default:
          throw new Error('can not execute resetForm');
      }
    });
  };

  setUri = () => {
    let url = `/fun/${this.opts.source.id}/variant/${this.opts.variant}?${$('.search_form').serialize()}`;
    window.history.replaceState(null, '', url);
  };

  initArgs = (fen?: string) => {
    let lv = this.$form.find('input[name=level]:checked').val();
    let aiColor = this.$form.find('input[name=color]:checked').val();
    this.color = opposite(aiColor);
    this.level = {
      depth: lv.split('-')[0],
      movetime: lv.split('-')[1]
    };
    if(fen) {
      this.initFen = fen;
    } else {
      this.initFen = this.$form.find('input[name=fen]').val();
    }
  };

  setFen = () => {
    switch (this.opts.source.id) {
      case 'g6': {
        let $custom = this.$form.find('.td-fen');
        let $fen = $custom.find('input[name="fen"]');
        this.$form.find('input[name="situation"]').click(() => {
          let situation = this.$form.find('input[name=situation]:checked').val();
          if(situation === 'custom') {
            $custom.removeClass('transparent');
          } else {
            const situationFenMap = {"8pawn": "8/pppppppp/8/8/8/8/PPPPPPPP/8 w - - 0 1", "3pawn1": "8/8/5ppp/8/5PPP/8/8/8 w - - 0 1", "3pawn2": "8/8/5ppp/8/8/5PPP/8/8 w - - 0 1", "3pawn3": "8/5ppp/8/8/8/8/5PPP/8 w - - 0 1"};
            let fen = situationFenMap[situation];
            $fen.val(fen);
            $custom.addClass('transparent');
            this.$form.find('.init-fen').data('fen', fen).addClass('parse-fen');
            window.lichess.pubsub.emit('content_loaded');
          }
        });

        $fen.on('change paste', (e) => {
          let el = (e.target as HTMLInputElement);
          let fen = el.value;
          if(fen) {
            this.validResult = this.subCtrl.validFen(fen);
            if(this.validResult.ok) {
              if(this.validResult.normalization) {
                el.value = this.validResult.normalization;
              }
              this.$form.find('.init-fen').data('fen', fen).addClass('parse-fen');
              window.lichess.pubsub.emit('content_loaded');
            } else {
              alert(this.validResult.error);
            }
          }
        });
        break;
      }
      case 'g7':
        this.$form.find('.button.random,input[name="rookColor"]').click(() => {
          let rookColor = this.$form.find('input[name=rookColor]:checked').val();
          let fen = this.subCtrl.randomFen(rookColor === 'white');
          this.$form.find('input[name="fen"]').val(fen);
          this.$form.find('.init-fen').data('fen', fen).addClass('parse-fen');
          window.lichess.pubsub.emit('content_loaded');
        });
        break;
      default:
        throw new Error('can not execute changeFen');
    }
  };

  submitForm = () => {
    let $form = $('.search_form');
    $form.submit((e) => {
      if(this.opts.source.id === 'g6') {
        if(this.validResult && !this.validResult.ok) {
          e.preventDefault();
          alert(this.validResult.error);
        } else {
          $form.submit();
        }
      }
    });
  }

}


