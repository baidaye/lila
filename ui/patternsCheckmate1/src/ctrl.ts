import Socket from './socket';
import * as ChessJS from 'chess.js';
import * as chessUtil from 'chess';
import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import {DrawShape} from 'chessground/draw';
import {Pref} from '../../round/src/interfaces';
import {sound} from './sound';
import * as xhr from './xhr';
import * as promotion from './view/promotion';
import {
  PatternsCheckmate1Opts,
  Redraw,
  JustCaptured,
  PatternsCheckmate1,
  PatternsCheckmate1Data,
} from './interfaces';

export default class PatternsCheckmate1Ctrl {

  orients = [{id: 'white', name: '白方视角'}, {id: 'black', name: '黑方视角'}];
  typs = [{id: 'order', name: '顺序'}, {id: 'random', name: '随机'}];

  opts: PatternsCheckmate1Opts;
  data: PatternsCheckmate1Data;
  pref: Pref;
  socket: Socket;
  chess: any; // chess.js
  chessground: CgApi;
  cgConfig: CgConfig;

  // current states
  activeChapterColor: cg.Color; // 点击后选择的
  chapterColor: cg.Color; // 当前题目
  win: boolean;
  finished: boolean = false;
  solution: boolean = false;
  fenMode: string = 'original'; // original | simplified
  patternsCheckmate1: PatternsCheckmate1;
  lastMove: cg.Key[] | undefined = undefined;

  constructor(opts: PatternsCheckmate1Opts, readonly redraw: Redraw) {
    this.opts = opts;
    this.data = opts.data;
    this.pref = opts.pref;
    this.socket = new Socket(opts.socketSend, this);
    this.chess = ChessJS.Chess();
    this.initData(this.data.checkmate1, false);
    this.activeChapterColor = this.currentChapter().color;

    if(opts.notAccept) {
      window.lichess.memberIntro();
    }
  }

  nextCheckmate1 = () => {
    xhr.nextCheckmate1(this.data.chapter, this.patternsCheckmate1.id, this.data.progress.typ).done(data => {
      this.initData(data);
    }).fail(function(err) {
      if(err.status === 406) {
        window.lichess.memberIntro();
      } else alert(err.responseTest)
    });
  };

  roundCheckmate1 = () => {
    const orient = this.data.progress.orient ? this.data.progress.orient : this.data.checkmate1.color;
    xhr.roundCheckmate1(this.data.chapter, this.patternsCheckmate1.id, this.win, orient, this.data.progress.typ).then(data => {
      this.data.progress = data;
      this.redraw();
    });
  };

  currentChapter = () => {
    return this.data.progress.chapters.filter(c => c.id === this.data.chapter)[0];
  };

  initData = (data, rd: boolean = true) => {
    this.finished = false;
    this.solution = false;
    this.patternsCheckmate1 = data;
    this.chapterColor = this.currentChapter().color;
    this.lastMove = this.simplified() ? undefined : this.uciToLastMove(this.patternsCheckmate1.originalInitMove);
    this.chess.load(this.patternsCheckmate1[`${this.fenMode}Fen`]);
    this.showGround();

    history.replaceState(null, '', `/patterns/checkmate1/chapter?no=${this.data.chapter}&id=${this.patternsCheckmate1.id}`);

    if(rd) this.redraw();

/*    setTimeout(() => {
      let initMove = this.patternsCheckmate1.originalInitMove;
      let decomposeUci = chessUtil.decomposeUci(initMove);
      let promotion = decomposeUci[2] ? decomposeUci[2] : null;
      this.jump({
        from: decomposeUci[0],
        to: decomposeUci[1],
        promotion: promotion,
        sloppy: true
      });
    }, 200);*/
  };

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  showGround = (): void => {
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
      cg.setAutoShapes([]);
    });
  };

  setAutoShapes = (shapes: DrawShape[]): void => {
    this.withCg(cg => cg.setAutoShapes(shapes));
  };

  makeCgOpts = (): CgConfig => {
    const fen = this.chess.fen();
    const color = this.patternsCheckmate1.color;
    const dests = this.toDests();
    const viewOnly = this.finished || this.opts.notAccept;
    const check = this.chess.in_check();
    const orient = this.data.progress.orient ? this.data.progress.orient : this.data.checkmate1.color;
    const config: CgConfig = {
      fen: fen,
      orientation: orient,
      turnColor: this.toColor(),
      movable: {
        color: color,
        dests: viewOnly ? {} : dests
      },
      check: check,
      lastMove: this.lastMove,
      drawable: {
        enabled: false,
        autoShapes: []
      }
    };
    if (!dests && !check) {
      config.turnColor = opposite(color);
    }
    this.cgConfig = config;
    return config;
  };

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    if (!promotion.start(this, orig, dest, capture, this.sendMove)) {
      this.sendMove(orig, dest, capture);
    }
  };

  // @ts-ignore
  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    this.jump({
      from: orig,
      to: dest,
      promotion: prom ? chessUtil.roleToSan[prom].toLowerCase() : null,
      sloppy: true
    });

    let moveUci =  orig + dest + (prom ? chessUtil.roleToSan[prom].toLowerCase() : '');
    this.win = this.patternsCheckmate1.checkmateMove === moveUci || this.chess.in_checkmate();
    this.finished = true;
    this.roundCheckmate1();
  };

  jump = (move: any) => {
    let m = this.chess.move(move);
    this.lastMove = [m.from, m.to];
    this.showGround();

    this.redraw();
    if (m.san.includes('x')) sound.capture();
    else sound.move();
    if (/[+#]/.test(m.san)) sound.check();
  };

  showSolution = () => {
    this.lastMove = this.uciToLastMove(this.patternsCheckmate1.checkmateMove);
    this.chess.load(this.patternsCheckmate1[`${this.fenMode}CheckmateFen`]);
    this.showGround();
    if (this.lastMove) {
      this.setAutoShapes([
        {
          orig: this.lastMove[0],
          dest: this.lastMove[1],
          brush: 'green'
        }
      ])
    }
    this.solution = true;
    this.redraw();
  };

  setOrient = (c: cg.Color) => {
    this.data.progress.orient = c;
    this.showGround();
  };

  setSimplified = () => {
    this.fenMode = 'simplified';
    this.initData(this.patternsCheckmate1);
    this.redraw();
  };

  setOriginal = () => {
    this.fenMode = 'original';
    this.initData(this.patternsCheckmate1);
    this.redraw();
  };

  retry = () => {
    this.initData(this.patternsCheckmate1);
    this.redraw();
  };

  simplified = () => {
    return this.fenMode === 'simplified';
  };

  toDests = (): cg.Dests => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({
        square: s,
        verbose: true
      });
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests;
  };

  toColor = () => {
    return (this.chess.turn() === 'w') ? 'white' : 'black';
  };

  uciToLastMove = (uci: string | undefined): [cg.Key, cg.Key] | undefined => {
    return uci ? [uci.substr(0, 2) as cg.Key, uci.substr(2, 2) as cg.Key] : undefined;
  };
}
