import * as cg from 'chessground/types';
import {Pref} from '../../round/src/interfaces';

export const emptyFen = '8/8/8/8/8/8/8/8 w - -';

export interface PatternsCheckmate1Opts {
  userId: string;
  notAccept: boolean;
  pref: Pref;
  data: PatternsCheckmate1Data;
  socketSend: SocketSend;
  element: HTMLElement;
}

export interface PatternsCheckmate1Data {
  chapter: number;
  progress: PatternsCheckmate1Progress;
  checkmate1: PatternsCheckmate1;
}

export interface PatternsCheckmate1Progress {
  typ: string;
  orient: cg.Color;
  chapters: PatternsCheckmate1Chapter[];
}

export interface PatternsCheckmate1 {
  id: number;
  originalInitFen: string;
  originalInitMove: string;
  originalFen: string;
  originalCheckmateFen: string;
  simplifiedFen: string;
  simplifiedCheckmateFen: string;
  checkmateMove: string;
  color: cg.Color;
}

export interface PatternsCheckmate1Chapter {
  id: number;
  name: string;
  color: cg.Color;
  progress: number;
  lastId: number;
}

export type Redraw = () => void;

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}
