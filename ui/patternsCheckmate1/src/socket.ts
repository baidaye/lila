import PatternsCheckmate1Ctrl from './ctrl';

interface Handlers {
  [key: string]: (data: any) => void;
}

export default class Socket {

  send: SocketSend;
  handlers: Handlers;
  connected = () => true;

  constructor(send: SocketSend, ctrl: PatternsCheckmate1Ctrl) {
    ctrl.opts;
    this.send = send;
    this.handlers = {

    };
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  };

};
