const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function nextCheckmate1(chapter, lastId, typ) {
  return $.ajax({
    method: 'get',
    url: `/patterns/checkmate1/chapter/next?no=${chapter}&lastId=${lastId}&typ=${typ}`,
    headers: headers
  });
}

export function roundCheckmate1(chapter, id, win, orient, typ) {
  return $.ajax({
    method: 'post',
    url: `/patterns/checkmate1/chapter/round?no=${chapter}&id=${id}`,
    headers: headers,
    data: {
      win,
      orient,
      typ
    }
  });
}
