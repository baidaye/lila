import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import * as cg from 'chessground/types';
import {bind} from './util';
import PatternsCheckmate1Ctrl from '../ctrl';


export default function (ctrl: PatternsCheckmate1Ctrl): VNode {
  let orient = ctrl.data.progress.orient ? ctrl.data.progress.orient : ctrl.data.checkmate1.color;
  return h('div.patternsCheckmate1-chapter__side', [
    h('div.chapters', [
      readerChapters(ctrl, 'white', '白棋杀王'),
      readerChapters(ctrl, 'black', '黑棋杀王')
    ]),
    h('div.settings', [
      h('div.orient', [
        h('group.radio', ctrl.orients.map(function (c) {
          return h('div', [
            h('input', {
              attrs: {
                type: 'radio',
                id: 'orient-' + c.id,
                name: 'orient',
                value: c.id,
                checked: orient === c.id
              },
              hook: bind('click', _ => {
                ctrl.setOrient(c.id as cg.Color);
              })
            }),
            h('label', {attrs: {for: 'orient-' + c.id}}, c.name)
          ])
        }))
      ]),
      h('div.typ', [
        h('group.radio', ctrl.typs.map(function (t) {
          return h('div', [
            h('input', {
              attrs: {
                type: 'radio',
                id: 'typ-' + t.id,
                name: 'typ',
                value: t.id,
                checked: ctrl.data.progress.typ === t.id
              },
              hook: bind('click', _ => {
                ctrl.data.progress.typ = t.id;
              })
            }),
            h('label', {attrs: {for: 'typ-' + t.id}}, t.name)
          ])
        }))
      ])
    ])
  ]);

}

function readerChapters(ctrl: PatternsCheckmate1Ctrl, color: cg.Color, title: string) {
  let chapters = ctrl.data.progress.chapters.filter(c => c.color === color);
  return h(`section.${color}`, {
    class: { active: ctrl.activeChapterColor === color }
  }, [
    h('h2', {
      hook: bind('click', _ => {
        ctrl.activeChapterColor = color;
        ctrl.redraw()
      })
    }, title),
    h('div.chapters', chapters.map(c => {
      return h('a.chapter', {
        class: {
          active: ctrl.opts.data.chapter === c.id
        },
        attrs: {
          href: `/patterns/checkmate1/chapter?no=${c.id}`
        }
      }, [
        h('div.name', c.name),
        h('div.progress', [
          h('div.bar', {attrs: {style: `width: ${c.progress}%`}}), h('div.percent', `${c.progress}%`)
        ]),
      ])
    }))
  ]);
}



