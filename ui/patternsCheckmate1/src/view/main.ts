import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import * as gridHacks from './gridHacks';
import {view as renderPromotion} from './promotion';
import * as ground from './ground';
import PatternsCheckmate1Ctrl from '../ctrl';
import side from './side';
import table from './table';

export default function (ctrl: PatternsCheckmate1Ctrl): VNode {
  return h('main.patternsCheckmate1-chapter', {
    hook: {
      postpatch(_, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
      }
    }
  }, [
    side(ctrl),
    h('div.patternsCheckmate1-chapter__board.main-board', [
      ground.render(ctrl),
      renderPromotion(ctrl),
    ]),
    table(ctrl)
  ]);
}

