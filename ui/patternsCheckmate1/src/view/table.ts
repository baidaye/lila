import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import PatternsCheckmate1Ctrl from '../ctrl';
import {bind} from './util';

export default function (ctrl: PatternsCheckmate1Ctrl): VNode {
  return h('div.patternsCheckmate1-chapter__table', [
    h('div.wrap', [
      h('div.title', [
        h(`piece.king.${ctrl.patternsCheckmate1.color}`),
        h('div.text', [
          h('h2', ctrl.currentChapter().name),
          h('p.subtitle', `${ctrl.chapterColor === 'white' ? '白' : '黑'}棋一步杀`),
        ])
      ]),
      h('div.tips', [
        !ctrl.finished ? '移动棋子完成一步杀' : null,
        (ctrl.finished && ctrl.win) ? h('div.feedback.win', [
          h('div.player', [
            h('div.icon', '✓'),
            h('div.instruction', [
              h('strong', '解题成功'),
              h('em', '加油！')
            ])
          ])
        ]) : null,
        (ctrl.finished && !ctrl.win) ? h('div.feedback.fail', [
          h('div.player', [
            h('div.icon', '✗'),
            h('div.instruction', [
              h('strong', '解题失败'),
              h('em', '继续努力吧！')
            ])
          ])
        ]) : null
      ]),
      h('div.actions', [
        h('button.button.button-empty.nextCheckmate1', {
          class: {
            disabled: ctrl.opts.notAccept
          },
          attrs: {
            disabled: ctrl.opts.notAccept
          },
          hook: bind('click', () => {
            ctrl.nextCheckmate1();
          })
        }, '下一题'),
        ((ctrl.finished && !ctrl.win) || !ctrl.finished && !ctrl.solution) ? h('button.button.button-empty.showSolution', {
          class: {
            disabled: ctrl.opts.notAccept
          },
          attrs: {
            disabled: ctrl.opts.notAccept
          },
          hook: bind('click', () => {
            ctrl.showSolution();
          })
        }, '看解答') : null,
        ctrl.finished || ctrl.solution ? h('button.button.button-empty.retry', {
          class: {
            disabled: ctrl.opts.notAccept
          },
          attrs: {
            disabled: ctrl.opts.notAccept
          },
          hook: bind('click', () => {
            ctrl.retry();
          })
        }, '重试') : null,
        !ctrl.simplified() ? h('button.button.button-empty.setSimplified', {
          class: {
            disabled: ctrl.opts.notAccept
          },
          attrs: {
            disabled: ctrl.opts.notAccept
          },
          hook: bind('click', () => {
            ctrl.setSimplified();
          })
        }, '简化局面') : null,
        ctrl.simplified() ? h('button.button.button-empty.setOriginal', {
          class: {
            disabled: ctrl.opts.notAccept
          },
          attrs: {
            disabled: ctrl.opts.notAccept
          },
          hook: bind('click', () => {
            ctrl.setOriginal();
          })
        }, '原始局面') : null
      ])
    ])
  ]);
}



