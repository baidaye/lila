import {h} from 'snabbdom'
import * as ground from './ground';
import * as cg from 'chessground/types';
import * as util from 'chessground/util';
import {Role} from 'chessground/types';
import {bind, onInsert} from './util';
import {JustCaptured} from '../interfaces';
import PatternsCheckmate1Ctrl from '../ctrl';

interface Promoting {
  orig: cg.Key;
  dest: cg.Key;
  capture?: JustCaptured;
  callback: Callback
}

type Callback = (orig: cg.Key, dest: cg.Key, capture: JustCaptured | undefined, role: Role) => void;

let promoting: Promoting | undefined;

export function start(ctrl: PatternsCheckmate1Ctrl, orig: cg.Key, dest: cg.Key, capture: JustCaptured | undefined, callback: Callback): boolean {
  let s = ctrl.chessground.state;
  let piece = s.pieces[dest];
  if (piece && piece.role == 'pawn' && (
    (dest[1] == '8' && s.turnColor == 'black') ||
    (dest[1] == '1' && s.turnColor == 'white'))) {
    promoting = {
      orig,
      dest,
      capture,
      callback
    };
    ctrl.redraw();
    return true;
  }
  return false;
}

function finish(ctrl: PatternsCheckmate1Ctrl, role) {
  if (promoting) {
    ground.promote(ctrl.chessground, promoting.dest, role);
    if (promoting.callback) {
      promoting.callback(promoting.orig, promoting.dest, promoting.capture, role);
    }
  }
  promoting = undefined;
}

export function cancel(ctrl: PatternsCheckmate1Ctrl) {
  if (promoting) {
    promoting = undefined;
    ctrl.chessground.set(ctrl.cgConfig);
    ctrl.redraw();
  }
}

function renderPromotion(ctrl: PatternsCheckmate1Ctrl, dest: cg.Key, pieces, color: cg.Color, orientation: cg.Color) {
  if (!promoting) return;

  let left = (8 - util.key2pos(dest)[0]) * 12.5;
  if (orientation === 'white') left = 87.5 - left;

  const vertical = color === orientation ? 'top' : 'bottom';

  return h('div#promotion-choice.' + vertical, {
    hook: onInsert(el => {
      el.addEventListener('click', _ => cancel(ctrl));
      el.oncontextmenu = () => false;
    })
  }, pieces.map(function (serverRole, i) {
    const top = (color === orientation ? i : 7 - i) * 12.5;
    return h('square', {
      attrs: {
        style: 'top:' + top + '%;left:' + left + '%'
      },
      hook: bind('click', e => {
        e.stopPropagation();
        finish(ctrl, serverRole);
      })
    }, [h('piece.' + serverRole + '.' + color)]);
  }));
}

export function view(ctrl: PatternsCheckmate1Ctrl) {
  if (!promoting) return;
  let pieces = ['queen', 'knight', 'rook', 'bishop'];

  return renderPromotion(ctrl, promoting.dest, pieces,
    util.opposite(ctrl.chessground.state.turnColor),
    ctrl.chessground.state.orientation);
}
