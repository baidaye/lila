import {Vm, CoordOpts, ModeCtrl} from './interfaces';
import HomeSocket from './socket';
import * as xhr from './xhr';
import {prop} from 'common';
import {readDests, readDrops} from 'chess';
import {Api as CgApi} from 'chessground/api';
import makeBasic from "./ctrl/basicCtrl";
import makeMove from "./ctrl/moveCtrl";
import makeCombat from "./ctrl/combatCtrl";
import makePromotion from "./promotion";
import { sound } from './sound';
import TaskProgressCtrl from '../../ttask/src/progressCtrl'

export default class CoordController {

    vm: Vm = {} as Vm;
    opts: CoordOpts;
    socket: HomeSocket;
    redraw: () => void;
    ground: any;
    promotion: any;

    progressInterval: number;
    duration: number;
    startAt: Date;

    basic: ModeCtrl;
    move: ModeCtrl;
    combat: ModeCtrl;

    colors = [{ v: 1, c: 'white', n: '白棋' }, { v: 2, c: 'random', n: '随机' }, { v: 3, c: 'black', n: '黑棋' }];
    coords = [{ v: false, c: 'hide', n: '不显示棋盘坐标' }, { v: true, c: 'show', n: '显示棋盘坐标' }];
    limits = [{ v: 1, c: 'yes', n: '限制时间' }, { v: 0, c: 'no', n: '不限时间' }];
    modes = [
            { c: 'basic', n: '坐标训练', t: 30, time: '在30秒的时间内', desc: '按照棋盘格子的名称，点击正确的格子，多多益善！' },
            { c: 'move', n: '着法训练', t: 60, time: '在1分钟的时间内', desc: '尽快将棋子移动到格子名称的位置，多多益善！'},
            { c: 'combat', n: '实战着法', t: 60, time: '在1分钟的时间内', desc: '按照提示的着法，移动棋子来完成战术题目，多多益善！'}
        ];

    taskProgressCtrl: TaskProgressCtrl;

    constructor(opts: CoordOpts, redraw: () => void) {
        this.vm = {
            userId: opts.userId,
            pref: opts.pref,
            mode: opts.pref.coordTrain.mode,
            colorV: opts.pref.coordTrain.color,
            colorC: this.getColor(opts.pref.coordTrain.color),
            coordinates: this.getCoords(opts.pref.coordTrain.coord),
            limit: opts.pref.coordTrain.limit === 1 ? 1 : 0,
            stage: 'pending',
            noteCard: '',
            noteCardAnm: false,
            wrong: false,
            score: 0,
            scores: opts.scores,
            loading: false,
            cgConfig: {}
        };

        this.opts = opts;
        this.redraw = redraw;
        this.taskProgressCtrl = new TaskProgressCtrl('coordTrain', redraw, opts.fr);
        this.socket = new HomeSocket(opts.socketSend, this);
        this.ground = prop<CgApi | undefined>(undefined);
        this.promotion = makePromotion(this.vm, this.ground, this.redraw);
        this.makeCgOpts();

        this.duration = this.getMode().t * 1000;
        this.basic = makeBasic(this);
        this.move = makeMove(this);
        this.combat = makeCombat(this);
    }

    makeCgOpts() {
        const config = {
            fen: '8/8/8/8/8/8/8/8 w - -',
            turnColor: null,
            orientation: this.vm.colorC,
            coordinates: this.vm.coordinates,
            addPieceZIndex: this.opts.pref.is3d,
            drawable: { enabled: false },
            movable: {
                free: false,
                color: null
            },
            premovable: {
                enabled: false
            },
            lastMove: null
        };
        return config;
    };

    setGround() {
        this.ground().set(this.makeCgOpts());
    };

    redrawGround() {
        this.ground().set(this.makeCgOpts());
        this.ground().redrawAll();
    };

    start() {
        this.vm.score = 0;
        this.startAt = new Date();
        this.bindGroundEvent();
        this.nextNodeCard();

        if(this.vm.userId) {
            let coord = this.vm.coordinates ? 1 : 0;
            xhr.start(this.vm.mode, this.vm.colorV, coord, this.vm.limit);
        }
    };

    setRunning() {
        this.vm.stage = 'running';
    }

    startInterval() {
        if(this.vm.limit) {
            this.progressInterval = setInterval(_ => {
                if (!this.isFinished() && this.startAt) {
                    let spent = Math.min(this.duration, (new Date().getTime() - this.startAt.getTime()));
                    let progress = 100 * spent / this.duration;
                    $('.progress_bar').css('width', progress + '%');

                    if (progress >= 100) {
                        this.finish();
                    }
                }
            }, 50);
        }
    }

    success() {
        if (this.isFinished()) {
            this.redraw();
            return;
        }
        this.vm.wrong = false;
        this.vm.score = this.vm.score + 1;
        this.winSound();
        this.nextNodeCard();
    }

    failed() {
        if (this.isFinished()) {
            this.redraw();
            return;
        }
        this.vm.wrong = true;
        this.modeFailed();
        this.lossSound();
        this.redraw();
    }

    finish() {
        if(this.vm.limit) {
            clearInterval(this.progressInterval);
        }
        this.vm.stage = 'finished';
        this.vm.noteCard = '';
        this.vm.wrong = false;
        this.startAt = null;
        this.unbindGroundEvent();

        if(this.vm.userId) {
            let coord = this.vm.coordinates ? 1 : 0;
            xhr.finish(this.vm.mode, this.vm.colorV, this.vm.colorC, coord, this.vm.limit, this.vm.score).then(scores => {
                this.vm.scores = scores;
                this.redraw();
            });
        }
        this.redraw();
    };

    bindGroundEvent() {
        if(this.vm.mode === 'basic') this.basic.bindGroundEvent();
        if(this.vm.mode === 'move') this.move.bindGroundEvent();
        if(this.vm.mode === 'combat') this.combat.bindGroundEvent();
    }

    unbindGroundEvent() {
        if(this.vm.mode === 'basic') this.basic.unbindGroundEvent();
        if(this.vm.mode === 'move') this.move.unbindGroundEvent();
        if(this.vm.mode === 'combat') this.combat.unbindGroundEvent();
    }

    nextNodeCard() {
        if(this.vm.mode === 'basic') this.basic.nextNodeCard();
        if(this.vm.mode === 'move') this.move.nextNodeCard();
        if(this.vm.mode === 'combat') this.combat.nextNodeCard();
    }

    modeFailed() {
        if(this.vm.mode === 'basic') this.basic.failed();
        if(this.vm.mode === 'move') this.move.failed();
        if(this.vm.mode === 'combat') this.combat.failed();
    }

    isPending() {
        return this.vm.stage === 'pending';
    };

    isRunning() {
        return this.vm.stage === 'running';
    };

    isFinished() {
        return this.vm.stage === 'finished';
    };

    getMode() {
        return this.modes.filter(m => m.c === this.vm.mode)[0];
    };

    changeMode(mode) {
        this.vm.mode = mode;
        this.vm.stage = 'pending';
        this.vm.noteCard = '';
        this.vm.wrong = false;
        this.startAt = null;
        this.duration = this.getMode().t * 1000;
        this.setGround();
        if(this.vm.userId) {
            xhr.getScore(mode).then(scores => {
                this.vm.scores = scores;
                this.redraw();
            });
        }
    };

    getColor(v: number) {
        let color = this.colors.filter(c => c.v === v)[0];
        let code;
        if(color.c === 'random') {
            code = ['white', 'black'][Math.round(Math.random())];
        } else {
            code = color.c;
        }
        return <Color>code;
    };

    getCoords(v: number) {
        return v >= 1;
    };

    winSound() {
        sound.win();
    }

    lossSound() {
        sound.loss();
    }

}

