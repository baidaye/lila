import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import board from './board';
import * as gridHacks from './gridHacks';
import CoordController from '../ctrl';
import * as side from './side';
import * as table from './table';
import taskProgressView from "../../../ttask/src/view/progress";


export default function(ctrl: CoordController): VNode {
  return h('main.coord-train', {
    hook: {
      postpatch(old, vnode) {
        gridHacks.start(vnode.elm as HTMLElement);
      }
    }
  }, [
    taskProgressView(ctrl.taskProgressCtrl),
    h('aside.coord-train__side', [
      side.scores(ctrl),
      side.setting(ctrl)
    ]),
    h('div.coord-train__board.main-board', [
      h('div.board_color.top', { class: {'black': ctrl.vm.colorC === 'white', 'white': ctrl.vm.colorC === 'black'}}, ctrl.vm.colorC === 'white' ? '黑方' : '白方'),
      h('div.board_color.bottom', { class: {'white': ctrl.vm.colorC === 'white', 'black': ctrl.vm.colorC === 'black'}},ctrl.vm.colorC === 'black' ? '黑方' : '白方'),
      board(ctrl),
      ctrl.promotion.view(),
      ctrl.isRunning() ? h('div.note-card', { class: { 'nope': ctrl.isRunning() && ctrl.vm.wrong, 'anm': ctrl.vm.noteCardAnm } }, ctrl.vm.noteCard) : null
    ]),
    h('div.coord-train__table', [
      table.main(ctrl),
      table.actions(ctrl)
    ]),
    h('div.coord-train__progress', [
      h('div.progress_bar', { class: { 'wrong': ctrl.isRunning() && ctrl.vm.wrong } })
    ])
  ]);
}
