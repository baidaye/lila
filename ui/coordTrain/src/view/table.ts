import CoordController from '../ctrl';
import { h } from 'snabbdom';
import {bind, spinner} from "../util";

export function main(ctrl: CoordController) {
    let mode =  ctrl.getMode();
    return h('div.main', [
        ctrl.isPending() ? h('div.intro', [
            h('h1', ctrl.getMode().n),
            h('p', '了解棋盘坐标是一门很重要的象棋技能：'),
            h('ul', [
                h('li', '象棋课程及练习普遍使用代数记谱法。'),
                h('li', '这能让您更便捷地与棋友交谈，因为你们都懂这门“象棋语言”。'),
                h('li', '如果不需寻找格子的名称，分析对局会更高效。')
            ]),
            h('p', ctrl.vm.limit ? `${mode.time}，${mode.desc}` : mode.desc)
        ]) : null,
        !ctrl.isPending() ? h('div.score', { class: { 'wrong': ctrl.isRunning() && ctrl.vm.wrong } }, ctrl.vm.score.toString()) : null
    ]);
}

export function actions(ctrl: CoordController) {
    return h('div.actions', [
        ctrl.vm.loading ? spinner() : null,
        ctrl.isRunning() && !ctrl.vm.loading ? h('div.running', '训练中...') : null,
        !ctrl.isRunning() && !ctrl.vm.loading ? h('group.radio.pick', ctrl.modes.map(function (mode) {
            return h('div', [
                h('input', {
                    attrs: {
                        type: 'radio', name: 'mode', id: 'mode-' + mode.c, checked: ctrl.vm.mode === mode.c, value: mode.c
                    },
                    hook: bind('click', _ => {
                        ctrl.changeMode(mode.c);
                    })
                }),
                h('label', { attrs: { for: 'mode-' + mode.c }}, mode.n)
            ])
        })) : null,
        !ctrl.isRunning() && !ctrl.vm.loading ? h('div.start', [
            h('button.button', {
                hook: bind('click', _ => {
                    ctrl.start();
                })}, '开始训练')
        ]) : null,
        ctrl.isRunning() && !ctrl.vm.limit && !ctrl.vm.loading ? h('div.finish', [
            h('button.button.button-red', {
                hook: bind('click', _ => {
                    ctrl.finish();
                })}, '结束训练')
        ]) : null
    ]);
}

