import { h } from 'snabbdom'
import { Chessground } from 'chessground';
import { Config as CgConfig } from 'chessground/config';
import CoordController from '../ctrl';

export default function(ctrl: CoordController) {
  return h('div.cg-wrap', {
    hook: {
      insert: vnode => ctrl.ground(Chessground((vnode.elm as HTMLElement), makeConfig(ctrl))),
      destroy: _ => ctrl.ground().destroy()
    }
  });
}

function makeConfig(ctrl: CoordController): CgConfig {
  let opts = ctrl.makeCgOpts();
  let config = {
    fen: opts.fen,
    turnColor: opts.turnColor,
    orientation: opts.orientation,
    coordinates: opts.coordinates,
    addPieceZIndex: opts.addPieceZIndex,
    movable: {
      free: false,
      color: null,
      dests: {},
      showDests: true,
      rookCastle: ctrl.vm.pref.rookCastle
    },
    premovable: opts.premovable,
    lastMove: opts.lastMove,
    drawable: { enabled: false },
    selectable: {
      enabled: ctrl.vm.pref.moveEvent !== 1
    },
    highlight: {
      lastMove: ctrl.vm.pref.highlight,
      check: ctrl.vm.pref.highlight
    },
    animation: {
      enabled: true,
      duration: ctrl.vm.pref.animation.duration
    },
    disableContextMenu: true
  };
  return config;
}

