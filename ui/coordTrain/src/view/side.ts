import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode';
import { bind, age } from "../util";
import CoordController from '../ctrl';

export function scores(ctrl: CoordController) {
    return h('div.box', [
        h('h1', ctrl.getMode().n),
        ctrl.vm.userId ? h('div.scores', ctrl.colors.filter(c => c.v != 2).map(function (color) {
            let scores = ctrl.vm.scores[color.c];
            return h('div.chart_container', [
                h('p',  [
                    `${ color.n }平均得分：`,
                    h('strong', age(scores))
                ]),
                (scores.length > 1) ? ratingChart(ctrl, color.c) : null
            ])
        })) : null
    ]);
}

function ratingChart(ctrl: CoordController, color: string) {
    return h('div.rating_chart', {
        hook: {
            insert(vnode) { drawRatingChart(ctrl, vnode, color) },
            postpatch(_, vnode) { drawRatingChart(ctrl, vnode, color) }
        }
    });
}

function drawRatingChart(ctrl: CoordController, vnode: VNode, color: string) {
    const $el = $(vnode.elm as HTMLElement);
    const dark = document.body.classList.contains('dark');
    const points = ctrl.vm.scores[color];
    const redraw = () => $el['sparkline'](points, {
        type: 'line',
        height: '80px',
        width: Math.round($el.outerWidth()) + 'px',
        lineColor: dark ? '#4444ff' : '#0000ff',
        fillColor: dark ? '#222255' : '#ccccff',
        numberFormatter: (x: number) => { return x; }
    });
    window.lichess.raf(redraw);
    window.addEventListener('resize', redraw);
}

export function setting(ctrl: CoordController) {
    return !ctrl.isRunning() ? h('div.setting', [
        h('div.color-orient', [
            h('group.radio', ctrl.colors.map(function (color) {
                return h('div', [
                    h('input', {
                        attrs: {
                            type: 'radio', name: 'color-orient', id: 'color-orient-' + color.c, checked: ctrl.vm.colorV === color.v, value: color.v
                        },
                        hook: bind('click', _ => {
                            ctrl.vm.colorV = color.v;
                            ctrl.vm.colorC = ctrl.getColor(color.v);
                            ctrl.setGround();
                            ctrl.redraw();
                        })
                    }),
                    h('label.color.color_' + color.v, { attrs: { for: 'color-orient-' + color.c }}, [h('i')])
                ])
            }))
        ]),
        h('div.coord-show', [
            h('group.radio', ctrl.coords.map(function (coord) {
                return h('div', [
                    h('input', {
                        attrs: {
                            type: 'radio', name: 'coord-show', id: 'coord-show-' + coord.c, checked: ctrl.vm.coordinates === coord.v, value: coord.v
                        },
                        hook: bind('click', _ => {
                            ctrl.vm.coordinates = coord.v;
                            ctrl.redrawGround();
                        })
                    }),
                    h('label', { attrs: { for: 'coord-show-' + coord.c }}, coord.n)
                ])
            }))
        ]),
        h('div.time-limit', [
            h('group.radio', ctrl.limits.map(function (limit) {
                return h('div', [
                    h('input', {
                        attrs: {
                            type: 'radio', name: 'time-limit', id: 'time-limit-' + limit.c, checked: ctrl.vm.limit === limit.v, value: limit.v
                        },
                        hook: bind('click', _ => {
                            ctrl.vm.limit = limit.v;
                            ctrl.redraw();
                        })
                    }),
                    h('label', { attrs: { for: 'time-limit-' + limit.c }}, limit.n)
                ])
            }))
        ])
    ]) : null;
}

