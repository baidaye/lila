const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function start(mode, color, coord, limit) {
  return $.ajax({
    method: 'post',
    url: '/training/coord/start',
    headers: headers,
    data: {
      mode,
      color,
      coord,
      limit
    }
  }).fail(function(d) {
    alert(d.responseJSON)
  });
}

export function finish(mode, colorV, colorC, coord, limit, score) {
  return $.ajax({
    method: 'post',
    url: '/training/coord/finish',
    headers: headers,
    data: {
      mode,
      colorV,
      colorC,
      coord,
      limit,
      score
    }
  }).fail(function(d) {
    alert(d.responseJSON)
  });
}

export function getScore(mode) {
  return $.ajax({
    url: '/training/coord/score?mode='+ mode,
    headers: headers
  }).fail(function(d) {
    alert(d.responseJSON)
  });
}

export function nextMove(color) {
  return $.ajax({
    url: '/training/coord/move?color='+ color,
    headers: headers
  }).fail(function(d) {
    alert(d.responseJSON)
  });
}

export function nextCombat(color, prevId) {
  return $.ajax({
    url: '/training/coord/combat?color='+ color + (prevId ? '&prevId='+ prevId : ''),
    headers: headers
  }).fail(function(d) {
    alert(d.responseJSON)
  });
}