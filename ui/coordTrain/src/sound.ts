import throttle from 'common/throttle';
import {assetsUrl} from "../../puzzleRush/src/util";

const soundUrl = assetsUrl + '/sound/';
let make = function(file, volume) {
  let sound = new window.Howl({
    src: [
      soundUrl + file + '.ogg',
      soundUrl + file + '.mp3'
    ],
    volume: volume || 1
  });
  return function() {
    if (window.lichess.sound.set() !== 'silent') sound.play();
  };
};

export const sound = {
  win: throttle(50, make('other/win', 0.1)),
  loss: throttle(50, make('other/loss', 0.1))
};