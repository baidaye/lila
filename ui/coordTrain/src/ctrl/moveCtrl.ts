import CoordController from "../ctrl";
import {JustCaptured, ModeCtrl} from "../interfaces";
import * as xhr from "../xhr";
import * as ChessJS from "chess.js";

export default function(ctrl: CoordController): ModeCtrl {

    const chess = ChessJS.Chess();
    let move;

    function nextNodeCard() {
        ctrl.vm.loading = true;
        ctrl.vm.noteCard = '';
        ctrl.vm.noteCardAnm = false;
        ctrl.redraw();

        xhr.nextMove(ctrl.vm.colorC).then( m => {
            ctrl.vm.noteCard = m.san;
            ctrl.vm.noteCardAnm = true;
            move = m;

            setGround();
            if(ctrl.isPending() || ctrl.isFinished()) {
                ctrl.startInterval();
            }
            ctrl.setRunning();
            ctrl.vm.loading = false;
            ctrl.redraw();
        });
    }

    function setGround() {
        chess.load(move.fen);
        let moves = chess.moves({ verbose: true });
        let dests = {};
        dests[move.fr] = moves.map((v) => v.to);

        const cfg = {
            fen: move.fen,
            turnColor: move.color,
            movable: {
                free: false,
                color: move.color,
                dests: dests,
                showDests: true
            },
            premovable: {
                enabled: false
            },
            lastMove: null
        };
        ctrl.ground().set(cfg);
    }

    function bindGroundEvent() {
        ctrl.ground().set({
            events: {
                move: function(orig: Key, dest: Key, capture?: JustCaptured){
                    if ((orig + dest) === move.uci) {
                        ctrl.success();
                    } else ctrl.failed();
                }
            }
        });
    }

    function unbindGroundEvent() {
        ctrl.ground().set({
            events: {
                move: false
            }
        });
    }

    function failed() {
        setGround()
    }

    return {
        nextNodeCard,
        bindGroundEvent,
        unbindGroundEvent,
        failed
    }
}
