import CoordController from "../ctrl";
import {ModeCtrl} from "../interfaces";
import * as xhr from "../xhr";
import * as ChessJS from "chess.js";

export default function(ctrl: CoordController): ModeCtrl {

    const chess = ChessJS.Chess();
    let lines = [];
    let prevId;
    let depth;
    let color;
    let opponentColor;
    let fly;

    function nextNodeCard() {
        ctrl.vm.loading = true;
        ctrl.vm.noteCard = '';
        ctrl.vm.noteCardAnm = false;
        ctrl.redraw();

        xhr.nextCombat(ctrl.vm.colorC, prevId).then( c => {
            prevId = c.id;
            depth = c.depth;
            color = c.color;
            opponentColor = c.color === 'white' ? 'black' : 'white';
            lines = [];
            lines.push({
                fen: c.fen,
                uci: c.uci,
                san: '',
                dests: {},
                color: opponentColor
            });

            fly = 0;
            setLines(c.lines);
            setGround();
            //console.log(prevId, lines);
            if(ctrl.isPending() || ctrl.isFinished()) {
                ctrl.startInterval();
            }
            ctrl.setRunning();
            ctrl.vm.loading = false;
            ctrl.redraw();
        });
    }

    function setGround() {
        let move = lines[fly];
        let preMove = lines[fly - 1];

        const cfg = {
            fen: move.fen,
            turnColor: move.color,
            movable: {
                free: false,
                color: move.color,
                dests: move.dests,
                showDests: true
            },
            premovable: {
                enabled: false
            },
            lastMove: uciToLastMove(preMove ? preMove.uci : null)
        };
        ctrl.vm.cgConfig = cfg;
        ctrl.ground().set(cfg);
        // 我方走棋
        if(fly % 2 !== 0) {
            ctrl.vm.noteCard = move.san;
            ctrl.vm.noteCardAnm = true;
            ctrl.redraw();
        }
        // 对方走棋
        if (fly % 2 === 0) {
            fly++;
            setTimeout(function () {
                setGround();
            }, 100);
        }
    }

    function bindGroundEvent() {
        ctrl.ground().set({
            events: {
                move: function(orig: Key, dest: Key, role?: any){
                    if(!ctrl.promotion.start(orig, dest, userMove)) {
                        userMove(orig, dest, role);
                    }
                }
            }
        });
    }

    function userMove(orig: Key, dest: Key, role?: any) {
        let uci = (typeof role === 'string') ? ((orig + dest) + (role == 'knight' ? 'n' : role[0])) : (orig + dest);
        if (uci === lines[fly].uci) {
            //console.log(fly);
            ctrl.vm.wrong = false;
            ctrl.vm.score = ctrl.vm.score + 1;
            ctrl.winSound();
            if(fly >= lines.length - 1) {
                nextNodeCard();
            } else {
                ctrl.vm.noteCard = '';
                ctrl.vm.noteCardAnm = false;
                ctrl.redraw();
                setTimeout(function () {
                    setGround();
                }, 100);
            }
            fly++;
        } else ctrl.failed();
        ctrl.promotion.cancel();
    }

    function unbindGroundEvent() {
        ctrl.ground().set({
            events: {
                move: false
            }
        });
    }

    function failed() {
        setGround()
    }
    
    function setLines(ln) {
        let preMove = lines[lines.length - 1];
        let moves = Object.keys(ln);

        if(moves.length > 0) {
            chess.load(preMove.fen);
            let info = chess.move(preMove.uci, { sloppy: true });
            lines[lines.length - 1].san = info.san;
            lines.push({
                fen: chess.fen(),
                uci: moves[0],
                san: '',
                dests: toDests(),
                color: info.color === 'w' ? 'black': 'white'
            });

            let childLn = ln[moves[0]];
            if(typeof childLn !== "boolean") {
                setLines(childLn);
            } else {
                let preMove = lines[lines.length - 1];
                chess.load(preMove.fen);
                let info = chess.move(preMove.uci, { sloppy: true });
                lines[lines.length - 1].san = info.san;
                if(lines[lines.length - 1].color === opponentColor) {
                    lines = lines.slice(0, lines.length - 1)
                }
            }
        }
    }

    function toDests() {
        const dests = {};
        chess.SQUARES.forEach(s => {
            const ms = chess.moves({square: s, verbose: true});
            if (ms.length) dests[s] = ms.map(m => m.to);
        });
        return dests;
    }

    function uciToLastMove(lm?: string): Key[] | undefined {
        return lm ? ([lm[0] + lm[1], lm[2] + lm[3]] as Key[]) : undefined;
    }

    return {
        nextNodeCard,
        bindGroundEvent,
        unbindGroundEvent,
        failed
    }
}
