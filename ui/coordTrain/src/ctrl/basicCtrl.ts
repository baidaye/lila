import CoordController from "../ctrl";
import {ModeCtrl} from "../interfaces";

export default function(ctrl: CoordController): ModeCtrl {

    function nextNodeCard() {
        ctrl.vm.loading = true;
        ctrl.vm.noteCard = '';
        ctrl.vm.noteCardAnm = false;
        ctrl.redraw();

        setTimeout(function () {
            let files = 'ABCDEFGH';
            let rows = '12345678';
            let square = files[Math.round(Math.random() * (files.length - 1))] + rows[Math.round(Math.random() * (rows.length - 1))];
            ctrl.vm.noteCard = square;
            ctrl.vm.noteCardAnm = true;
            if(ctrl.isPending() || ctrl.isFinished()) {
                ctrl.startInterval();
            }
            ctrl.setRunning();
            ctrl.vm.loading = false;
            ctrl.redraw();
        }, 100);
    }

    function bindGroundEvent() {
        ctrl.ground().set({
            events: {
                select: function(key) {
                    if (key === ctrl.vm.noteCard.toLowerCase()) {
                        ctrl.success();
                    } else ctrl.failed();
                }
            }
        });
    }

    function unbindGroundEvent() {
        ctrl.ground().set({
            events: {
                select: false
            }
        });
    }

    function failed() {
    }

    return {
        nextNodeCard,
        bindGroundEvent,
        unbindGroundEvent,
        failed
    }
}
