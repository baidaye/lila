import { VNode } from 'snabbdom/vnode'
import * as cg from 'chessground/types';

export { Key, Piece } from 'chessground/types';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

interface Untyped {
  [key: string]: any;
}

export interface CoordOpts extends Untyped {
  element: HTMLElement;
  socketSend: SocketSend;
  userId: string;
}

export interface Vm extends Untyped {
  userId: string,
  pref: any;
  mode: string; // basic, move, combat
  colorV: number;
  colorC: Color;
  coordinates: boolean;
  limit: number;
  stage: string; // pending, running, finished
  noteCard: string;
  noteCardAnm: boolean;
  wrong: boolean;
  score: number,
  scores: Scores,
  loading: boolean;
  cgConfig: any;
}

export interface Scores {
  white: number[],
  black: number[]
}

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}

export interface ModeCtrl {
  nextNodeCard(),
  bindGroundEvent(),
  unbindGroundEvent(),
  failed()
}
