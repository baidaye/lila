import CoordController from "./ctrl";

interface Handlers {
  [key: string]: (data: any) => void;
}
export default class HomeSocket {

  send: SocketSend;
  handlers: Handlers;

  constructor(send: SocketSend, readonly ctrl: CoordController) {
    this.send = send;
    this.handlers = {

    }
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }

    // @ts-ignore
    return !!this.ctrl.taskProgressCtrl && !!this.ctrl.taskProgressCtrl.socketHandler && this.ctrl.taskProgressCtrl.socketHandler(type, data);
  }

};
