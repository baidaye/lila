import OlClassCtrl from './ctrl';
import {path as treePath} from 'tree';

export function canGoForward(ctrl: OlClassCtrl): boolean {
  return ctrl.groundCtrl.node.children.length > 0;
}

export function next(ctrl: OlClassCtrl): void {
  const child = ctrl.groundCtrl.node.children[0];
  if (child && !ctrl.groundCtrl.isSubSync()) {
    ctrl.groundCtrl.userJump(ctrl.groundCtrl.path + child.id);
  }
}

export function prev(ctrl: OlClassCtrl): void {
  if(!ctrl.groundCtrl.isSubSync()) {
    ctrl.groundCtrl.userJump(treePath.init(ctrl.groundCtrl.path));
  }
}

export function last(ctrl: OlClassCtrl): void {
  if(!ctrl.groundCtrl.isSubSync()) {
    ctrl.groundCtrl.userJump(treePath.fromNodeList(ctrl.groundCtrl.mainline));
  }
}

export function first(ctrl: OlClassCtrl): void {
  if(!ctrl.groundCtrl.isSubSync()) {
    ctrl.groundCtrl.userJump(treePath.root);
  }
}

