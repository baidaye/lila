import {defined} from 'common';
import throttle from 'common/throttle';
import {storedProp, StoredBooleanProp} from 'common/storage';
import * as speech from './speech';
import {sound} from './sound';
import * as chessUtil from 'chess';
import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {DrawShape} from 'chessground/draw';
import {Config as CgConfig} from 'chessground/config';
import {build as makeTree, path as treePath, ops as treeOps, TreeWrapper} from 'tree';
import {AnalyseData as GameData} from '../../analyse/src/interfaces';
import {CgDests, JustCaptured, User} from './interfaces';
import * as promotion from './view/promotion';
import keyboard from './keyboard';
import OlClassCtrl from './ctrl';
import explorerCtrl from './view/explorer/explorerCtrl';
import {ExplorerCtrl} from './view/explorer/interfaces';
import {plural} from './util';
import * as xhr from "./xhr";

export default class OlClassGroundCtrl {

  round: GameData;
  chessground: CgApi;
  tree: TreeWrapper;
  cgConfig: CgConfig;
  initialPath: Tree.Path;
  explorer: ExplorerCtrl;

  // current tree state
  path: Tree.Path;
  node: Tree.Node;
  nodeList: Tree.Node[];
  mainline: Tree.Node[];

  // state flags
  justPlayed?: string; // pos
  justDropped?: string; // role
  justCaptured?: JustCaptured;
  autoScrollRequested: boolean = false;
  onMainline: boolean = true;

  contextMenuPath?: Tree.Path;

  // display flags
  flipped: boolean = false;
  menuIsOpen: boolean = false;
  situationModal: boolean = false;
  situationActiveTab: string = 'fen';

  syncActiveMember: User | null = null;
  glyphs: any;
  featureActive: string;

  showTree: StoredBooleanProp = storedProp('show-tree', true);

  underboardTab: string;

  constructor(readonly ctrl: OlClassCtrl, readonly trans: Trans) {
    this.underboardTab = this.ctrl.opts.userId === this.ctrl.opts.clazz.coach ? 'note' : 'tags';
    this.round = ctrl.opts.round;
    this.initBoard();
  }

  initBoard = (): void => {
    this.flipped = false;
/*    this.showTree = this.ctrl.opts.clazz.coach === this.ctrl.opts.userId ? (
      (this.ctrl.courseWareCtrl && this.ctrl.courseWareCtrl.selected) ? this.ctrl.courseWareCtrl.selected.showTree : true
    ) : true;*/
    this.menuIsOpen = false;

    this.tree = makeTree(treeOps.reconstruct(this.round.treeParts));
    this.initialPath = treePath.root;
    this.setPath(this.initialPath);
    this.showGround();
    keyboard(this.ctrl);
    this.explorer = explorerCtrl(this, this.ctrl.opts.explorer, this.explorer ? this.explorer.allowed() : true);
    this.explorer.setNode();

    // @ts-ignore
    if(this.round.initPly) {
      setTimeout(() => {
        // @ts-ignore
        this.jumpToPly(this.round.initPly);
      }, 500);
    }

    setTimeout(() => {
      this.ctrl.evalCtrl.startCeval();
    }, 1000);
  };

/*  toggleTree = () => {
    this.showTree = !this.showTree;
  };*/

  setPath = (path: Tree.Path): void => {
    this.path = path;
    this.nodeList = this.tree.getNodeList(path);
    this.node = treeOps.last(this.nodeList) as Tree.Node;
    this.mainline = treeOps.mainlineNodeList(this.tree.root);
    this.onMainline = this.tree.pathIsMainline(path)
  };

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  toggleExplorer = (): void => {
    if(!this.explorer.enabled()) {
      this.featureActive = 'explorer';
    } else {
      this.featureActive = '';
    }
    this.explorer.toggle();
  };

  showComments = (path: Tree.Path): void => {
    this.userJump(path);
    if(this.featureActive === 'comments') {
      this.featureActive = '';
    } else {
      this.featureActive = 'comments';
    }
    this.ctrl.redraw();
  };

  showGlyphs = (path: Tree.Path): void => {
    this.userJump(path);
    if(this.featureActive === 'glyphs') {
      this.featureActive = '';
    } else {
      this.featureActive = 'glyphs';
    }
    this.ctrl.redraw();
  };

  explorerMove(uci: Uci) {
    this.playUci(uci);
    this.explorer.loading(true);
  }

  toggleMenuBox = () => {
    if(!this.isSubSync()) {
      this.menuIsOpen = !this.menuIsOpen;
      this.ctrl.redraw();
    }
  };

  flip = () => {
    if(!this.isSubSync()) {
      this.flipped = !this.flipped;
      this.chessground.set({
        orientation: this.bottomColor()
      });
      this.ctrl.redraw();
    }
  };

  topColor = (): Color => {
    return opposite(this.bottomColor());
  };

  bottomColor = (): Color => {
    return this.flipped ? opposite(this.round.orientation) : this.round.orientation;
  };

  turnColor = (): Color => {
    return (this.node.ply % 2 === 0) ? 'white' : 'black';
  };

  uciToLastMove = (uci?: Uci): cg.Key[] | undefined => {
    if (!uci) return;
    if (uci[1] === '@') {
      return [uci.substr(2, 2), uci.substr(2, 2)] as cg.Key[];
    }
    return [uci.substr(0, 2), uci.substr(2, 2)] as cg.Key[];
  };

  reset = (): void => {
    this.showGround();
    this.ctrl.redraw();
  };

  showGround = (): void => {
    if (!defined(this.node.dests)) {
      this.getDests();
    }
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
      this.ctrl.evalCtrl.setAutoShapes();

      if (this.node.shapes) {
        cg.setShapes(this.node.shapes as DrawShape[]);
      }
    });
  };

  getDests: () => void = throttle(800, () => {
    if (!defined(this.node.dests)) {
      this.ctrl.socket.sendAnaDests({
        variant: 'standard',
        fen: this.node.fen,
        path: this.path
      });
    }
  });

  makeCgOpts(): CgConfig {
    const node = this.node,
      color = this.turnColor(),
      dests = chessUtil.readDests(this.node.dests),
      drops = chessUtil.readDrops(this.node.drops),
      movableColor = ((dests && Object.keys(dests).length > 0) || drops === null || drops.length) ? color : undefined,
      config: CgConfig = {
        fen:  node.fen,
        turnColor: color,
        movable: {
          color: movableColor,
          dests: (movableColor === color ? (dests || {}) : {}) as CgDests
        },
        check: !!node.check,
        lastMove: this.uciToLastMove(node.uci)
      };
    if (!dests && !node.check) {
      // premove while dests are loading from server
      // can't use when in check because it highlights the wrong king
      config.turnColor = opposite(color);
      config.movable!.color = color;
    }
    config.viewOnly = this.isSubSync();
    config.orientation = this.bottomColor();
    config.selected =  undefined;
    this.cgConfig = config;
    return config;
  }

  autoScroll(): void {
    this.autoScrollRequested = true;
  }

  playedLastMoveMyself = () => !!this.justPlayed && !!this.node.uci && this.node.uci.startsWith(this.justPlayed);

  jumpToPly(ply: Ply): void {
    let nodes = this.mainline.filter(n => n.ply <= ply);
    let path = treePath.fromNodeList(nodes);
    this.userJump(path);
    this.ctrl.redraw();
  }

  jump(path: Tree.Path): void {
    const pathChanged = path !== this.path,
      isForwardStep = pathChanged && path.length == this.path.length + 2;
    this.setPath(path);
    this.showGround();
    if (pathChanged) {
      const playedMyself = this.playedLastMoveMyself();
      this.sendPath(path);
      if (isForwardStep) {
        if (!this.node.uci) { // initial position
          sound.move();
        }
        else if (!playedMyself) {
          if (this.node.san!.includes('x')) {
            sound.capture();
          } else sound.move();
        }
        if (/\+|\#/.test(this.node.san!)) {
          sound.check();
        }
      }
      this.ctrl.evalCtrl.threatMode(false);
      this.ctrl.evalCtrl.ceval.stop();
      this.ctrl.evalCtrl.startCeval();

      speech.node(this.node);
    }
    this.justPlayed = this.justDropped = this.justCaptured = undefined;
    this.explorer.setNode();
    this.autoScroll();
    promotion.cancel(this.ctrl);
  }

  userJump = (path: Tree.Path): void => {
    this.withCg(cg => cg.selectSquare(null));
    this.jump(path);
    this.ctrl.redraw();
  };

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    this.justPlayed = orig;
    this.justDropped = undefined;
    const piece = this.chessground.state.pieces[dest];
    const isCapture = capture || (piece && piece.role == 'pawn' && orig[0] != dest[0]);
    if(isCapture) {
      sound.capture();
    } else {
      sound.move()
    }
    if (!promotion.start(this.ctrl, orig, dest, capture, this.sendMove)) {
      this.sendMove(orig, dest, capture);
    }
  };

  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    const move: any = {
      orig,
      dest,
      variant: 'standard',
      fen: this.node.fen,
      path: this.path
    };
    if (capture) this.justCaptured = capture;
    if (prom) move.promotion = prom;
    this.ctrl.socket.sendAnaMove(move);
    this.ctrl.redraw();
  };

  playUci(uci: Uci): void {
    const move = chessUtil.decomposeUci(uci);
    if (uci[1] === '@') this.chessground.newPiece({
      color: this.chessground.state.movable.color as Color,
      role: chessUtil.sanToRole[uci[0]]
    }, move[1]);
    else {
      const capture = this.chessground.state.pieces[move[1]];
      const promotion = move[2] && chessUtil.sanToRole[move[2].toUpperCase()];
      this.sendMove(move[0], move[1], capture, promotion);
    }
  }

  addNode(node: Tree.Node, path: Tree.Path) {
    const newPath = this.tree.addNode(node, path);
    if (!newPath) {
      console.log("Can't addNode', node, path");
      return this.ctrl.redraw();
    }
    this.jump(newPath);
    this.ctrl.redraw();
  }

  addDests(dests: string, path: Tree.Path, opening?: Tree.Opening): void {
    this.tree.addDests(dests, path, opening);
    if (path === this.path) {
      this.showGround();
    }
  }

  isSync = () => this.ctrl.opts.olClass.syncs.length > 0;

  isPubSync = () => this.ctrl.liveCtrl.localLiveCtrl.isJoined && this.ctrl.liveCtrl.localIsSync();

  isSubSync = () => this.ctrl.liveCtrl.localLiveCtrl.isJoined && this.isSync() && !this.isPubSync();

  isSubOrPubSync = () => true;/* this.ctrl.liveCtrl.localLiveCtrl.isJoined && this.isSync();*/

  setMemberActive = (who?: {u: string}) => {
    if(who) {
      this.syncActiveMember = this.ctrl.liveCtrl.getUser(who.u);
    }
  };

  // 教练任何时候都可以写，学员只有在开课同步的时候才能写
  isWriting = (): boolean => {
    return this.ctrl.liveCtrl.localIsCoach() || (this.ctrl.liveCtrl.isStarted() && this.ctrl.liveCtrl.localIsSync());
  };

  // 兼容发送到后台时的同步状态判断
  isSticky = (): boolean => {
    return this.ctrl.liveCtrl.isStarted() && this.ctrl.liveCtrl.localIsSync();
  };

  send = (t: string, d: any): boolean => {
    if (this.isWriting()) {
      this.ctrl.socket.send(t, d);
      return true;
    }
    return false;
  };

  withCourseWareId = (req) => {
    req.ch = this.ctrl.courseWareCtrl.selectedId;
    return req;
  };

  withPosition = (req) => {
    req.ch = this.ctrl.courseWareCtrl.selectedId;
    req.path = this.path;
    return req;
  };

  wrongCourseWare = (position) => {
    if (position.courseWareId !== this.ctrl.courseWareCtrl.selectedId) {
      this.ctrl.courseWareCtrl.loadCourseWare(position.courseWareId);
      return true;
    }
    return false;
  };

  sendPath = (path: Tree.Path, force: boolean = false) => {
    if (this.isSticky() && (path !== this.ctrl.opts.olClass.position.path || force)) {
      this.send('setPath', this.withCourseWareId({
        path
      }));
    }
  };

  deleteNode = (path: Tree.Path) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      const node = this.tree.nodeAtPath(path);
      if (!node) return;
      const count = treeOps.countChildrenAndComments(node);
      if ((count.nodes >= 10 || count.comments > 0) && !confirm(
        '删除 ' + plural('步', count.nodes) + (count.comments ? ' 和 ' + plural('评注', count.comments) : '') + '?'
      )) return;
      this.tree.deleteNodeAt(path);

      if (treePath.contains(this.path, path)) {
        this.userJump(treePath.init(path));
      } else {
        this.jump(this.path);
      }

      this.send('deleteNode', this.withCourseWareId({
        path,
        jumpTo: this.path
      }));
    }
  };

  promote = (path: Tree.Path, toMainline: boolean) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.tree.promoteAt(path, toMainline);
      this.jump(path);

      this.send('promote', this.withCourseWareId({
        toMainline,
        path
      }));
    }
  };

  forceVariation = (path: Tree.Path, force: boolean) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.tree.forceVariationAt(path, force);
      this.jump(path);

      this.send('forceVariation', this.withCourseWareId({
        force,
        path
      }));
    }
  };

  changeShape = (shapes) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.tree.setShapes(shapes, this.path);
      this.send('shapes', this.withCourseWareId({
        path: this.path,
        shapes
      }));
    }
  };

  deleteComment = (id: string) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.send('deleteComment', {
        ch: this.ctrl.courseWareCtrl.selectedId,
        path: this.path,
        id
      });
    }
  };

  setComment = (text: string)  => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.send('setComment', {
        ch: this.ctrl.courseWareCtrl.selectedId,
        path: this.path,
        text
      })
    }
  };

  loadGlyphs = () => {
    if (!this.glyphs) {
      xhr.glyphs().then(gs => {
        this.glyphs = gs;
        this.ctrl.redraw();
      });
    }
  };

  toggleGlyph = throttle(500, (id: string) => {
    this.send('toggleGlyph',this.withPosition({
      id: id
    }));
  });

  setTag = throttle(500, (name, value) => {
    if ((this.isSticky() || this.isWriting()) && this.ctrl.courseWareCtrl.isSelected()) {
      this.send('setTag', {
        ch: this.ctrl.courseWareCtrl.selectedId,
        name: name,
        value: value.substr(0, 140)
      })
    }
  });

  onSendPath = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.ctrl.groundCtrl.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;

      if (position.courseWareId !== this.ctrl.courseWareCtrl.selectedId ||
        !this.tree.pathExists(position.path)) {
        return this.ctrl.courseWareCtrl.loadCourseWare(position.courseWareId);
      }

      if (who && who.u === this.ctrl.opts.userId) {
        return;
      }
      this.userJump(position.path);
      this.ctrl.redraw();
    }
  };

  onAddNode = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        node = d.n,
        who = d.w;
      this.setMemberActive(who);

      if(this.wrongCourseWare(position)) return;
      if (who && who.u === this.ctrl.opts.userId) {
        this.ctrl.opts.olClass.position.path = position.path + node.id;
        return;
      }

      const newPath = this.tree.addNode(node, position.path);
      if (!newPath) {
        return this.ctrl.courseWareCtrl.loadCourseWare(position.courseWareId);
      }

      this.tree.addDests(d.d, newPath, d.o);
      this.ctrl.opts.olClass.position.path = newPath;
      this.jump(newPath);
      this.ctrl.redraw();
    }
  };

  onDeleteNode = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      if (who && who.u === this.ctrl.opts.userId) return;

      if (!this.tree.pathExists(position.path)) {
        return this.ctrl.courseWareCtrl.loadCourseWare(position.courseWareId);
      }

      this.tree.deleteNodeAt(position.path);
      this.jump(this.path);
      this.ctrl.redraw();
    }
  };

  onPromote = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      if (who && who.u === this.ctrl.opts.userId) return;
      if (!this.tree.pathExists(position.path)) {
        return this.ctrl.courseWareCtrl.loadCourseWare(position.courseWareId);
      }
      this.tree.promoteAt(position.path, d.toMainline);
      this.jump(this.path);
      this.ctrl.redraw();
    }
  };

  onForceVariation = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;

      this.tree.forceVariationAt(position.path, d.force);
      this.ctrl.redraw();
    }
  };

  onShapes = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      if (who && who.u === this.ctrl.opts.userId) {
        return;
      }
      this.tree.setShapes(d.s, this.path);
      if (this.path === position.path) {
        this.withCg(cg => cg.setShapes(d.s));
      }
      this.ctrl.redraw();
    }
  };

  onGlyphs = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      this.tree.setGlyphsAt(d.g, position.path);
      this.ctrl.redraw();
    }
  };

  onSelectNode = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      this.showGround();
      this.ctrl.redraw();
    }
  };

  onSetComment = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      this.tree.setCommentAt(d.c, position.path);
      this.ctrl.redraw();
    }
  };

  onDeleteComment = (d) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      const position = d.p,
        who = d.w;
      this.setMemberActive(who);
      this.ctrl.opts.olClass.position = position;
      if(this.wrongCourseWare(position)) return;
      this.tree.deleteCommentAt(d.id, position.path);
      this.ctrl.redraw();
    }
  };

  onSetTags = (d) => {
    this.setMemberActive(d.w);
    if (!this.ctrl.courseWareCtrl.isSelected() || d.courseWareId !== this.ctrl.courseWareCtrl.selected.id) return;
    this.ctrl.courseWareCtrl.selected.tags = d.tags;
    this.ctrl.redraw();
  };

  changePgn = (pgn: string, orientation?: string, initPly?: number): void => {
    this.ctrl.redirecting = true;
    $.ajax({
      method: 'post',
      url: `/olclass/pgn?orientation=${orientation ? orientation : ''}&initPly=${initPly ? initPly : ''}`,
      data: { pgn },
      success: (round: GameData) => {
        this.changeSituation(round);
      },
      error: error => {
        console.log(error);
        this.ctrl.redirecting = false;
        this.ctrl.redraw();
      }
    });
  };

  changeFen = (fen: string): void => {
    this.ctrl.redirecting = true;
    $.ajax({
      url: `/olclass/fen`,
      data: { urlFen: encodeURIComponent(fen).replace(/%20/g, '_').replace(/%2F/g, '/') },
      success: (round: GameData) => {
        this.changeSituation(round);
      },
      error: error => {
        console.log(error);
        this.ctrl.redirecting = false;
        this.ctrl.redraw();
      }
    });
  };

  changeGame = (gameId: string): void => {
    this.ctrl.redirecting = true;
    $.ajax({
      url: `/olclass/game`,
      data: { gameId: gameId },
      success: (round: GameData) => {
        this.changeSituation(round);
      },
      error: error => {
        console.log(error);
        this.ctrl.redirecting = false;
        this.ctrl.redraw();
      }
    });
  };

  changeSituation = (round: GameData, hash?: string) => {
    this.round = round;
    this.initBoard();
    if(this.ctrl.groundCtrl.isSync() && this.ctrl.liveCtrl.localLiveCtrl.isJoined && this.tree.pathExists(this.ctrl.opts.olClass.position.path)) {
      this.userJump(this.ctrl.opts.olClass.position.path);
    }
    this.situationModal = false;
    this.ctrl.redirecting = false;
    if(hash) {
      location.hash = hash;
    }
    this.ctrl.redraw();
  };

  reloadOlClass = () => {
    xhr.get(this.ctrl.opts.olClass.id).then(d => {
      this.ctrl.opts.olClass = d;
      if(d.position.courseWareId && this.isSubOrPubSync()) {
        this.ctrl.courseWareCtrl.loadCourseWare(d.position.courseWareId);
      }
    })
  };

  redrawGround = () => {
    setTimeout(() => {
      this.ctrl.groundCtrl.chessground.redrawAll();
    }, 500);
  };

}

