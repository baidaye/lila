import {RemoteStream} from 'trtc-js-sdk';
import {RemoteStreamConfig} from './interfaces';
import {toastify} from './util';
import OlClassCtrl from './ctrl';

export default class OlClassLiveRemoteCtrl {

  remoteStreamConfigMap: Map<String, RemoteStreamConfig>;

  constructor(readonly ctrl: OlClassCtrl) {
    this.remoteStreamConfigMap = new Map<String, RemoteStreamConfig>();

    window.lichess.pubsub.on('rtc_client_init', () => {
      this.handleClientEvents();
    });

    window.lichess.pubsub.on('rtc_room_joined', () => {
      this.startGetAudioLevel();
    });

  }

  remotes = () => [...this.remoteStreamConfigMap.values()];

  getStream = (userId: string) => {
    return this.remoteStreamConfigMap.get(userId);
  };

  // 远端用户进房: 新增远端用户
  addUser = (userId: string) => {
    this.remoteStreamConfigMap.set(userId, {
      userId: userId,
      user: this.ctrl.liveCtrl.getUser(userId),
      stream: null,
      streamType: 'main',
      hasAudio: false,
      hasVideo: false,
      mutedAudio: true,
      mutedVideo: true,
      subscribedAudio: false,
      subscribedVideo: false,
      audioVolume: 0,
      remote: true
    });
    this.ctrl.redraw();
  };

  // 移除远端用户
  removeUser = (userId: string) => {
    this.remoteStreamConfigMap.delete(userId);
    this.ctrl.redraw();
  };

  // 新增远端流
  addStream = async (stream: RemoteStream) => {
    const userId = stream.getUserId();
    const config = this.getStream(userId);
    if(config) {
      this.remoteStreamConfigMap.set(userId, {
        userId: userId,
        user: config.user,
        stream: stream,
        streamType: 'main',
        hasAudio: stream.hasAudio(),
        hasVideo: stream.hasVideo(),
        mutedAudio: false,
        mutedVideo: false,
        subscribedAudio: false,
        subscribedVideo: false,
        audioVolume: 0,
        remote: true,
      });
    } else {
      toastify('error', `user ${userId} is not exists`)
    }

    if(
      this.ctrl.liveCtrl.localIsCoach() ||
      this.ctrl.liveCtrl.userIsCoach(userId) ||
      (stream.hasAudio() && this.ctrl.liveCtrl.userIsSpeaker(userId))
    ) {
      await this.handleSubscribe(stream, stream.hasAudio(), stream.hasVideo());
      await this.handleStreamEvents(stream);
    }
  };

  // 移除远端流
  removeStream = async (remoteStream: RemoteStream) => {
    await this.handleUnSubscribe(remoteStream);
    this.closeStream(remoteStream);
    this.updateStream(remoteStream.getUserId(), {
      stream: null,
      hasAudio: false,
      hasVideo: false,
      mutedAudio: true,
      mutedVideo: true,
      audioVolume: 0,
      subscribedAudio: false,
      subscribedVideo: false,
    });
  };

  // 更新远端流的操作状态
  updateStream = (userId: string, config: any) => {
    let oldConfig = this.getStream(userId);
    if(oldConfig) {
      let newConfig = {...oldConfig, ...config} as RemoteStreamConfig;
      this.remoteStreamConfigMap.set(userId, newConfig);
      this.ctrl.redraw();
    } else {
      console.error(`user ${userId} is not exists`);
    }
  };

  // 开始订阅远端流
  handleSubscribe = async (remoteStream: RemoteStream, audio: boolean = true, video: boolean = true) => {
    try {
      await this.ctrl.liveCtrl.localLiveCtrl.client.subscribe(remoteStream, {
        audio: audio,
        video: video,
      });

      this.updateStream(remoteStream.getUserId(), {
        subscribedAudio: audio,
        subscribedVideo: video,
      })
    } catch (error) {
      console.error(`subscribe ${remoteStream.getUserId()} with audio: true video: true error`, error);
      toastify('error', `subscribe ${remoteStream.getUserId()} failed!`);
    }
  };

  // 取消订阅远端流
  handleUnSubscribe = async (remoteStream: RemoteStream) => {
    try {
      await this.ctrl.liveCtrl.localLiveCtrl.client.unsubscribe(remoteStream);
      this.updateStream(remoteStream.getUserId(), {
        subscribedAudio: false,
        subscribedVideo: false,
      });
    } catch (error) {
      //console.error(`unsubscribe ${remoteStream.getUserId()} error`, error);
    }
  };

  // 播放音视频流
  playStream = (stream: RemoteStream) => {
    stream.play(`video-${stream.getUserId()}`).catch();
  };

  // 恢复播放音视频
  resumeStream = async (stream: RemoteStream) => {
    await stream.resume();
  };

  // 关闭音频流
  closeStream = (stream: RemoteStream) => {
    stream.stop();
    stream.close();
  };

  // 停止/启用 视频
  toggleMuteVideo = (userId: string) => {
    let config = this.getStream(userId);
    if(config && config.stream) {
      if(config.subscribedVideo) {
        this.handleSubscribe(config.stream, config.subscribedAudio, false).catch();
      } else {
        this.handleSubscribe(config.stream, config.subscribedAudio, true).catch();
      }
    } else {
      toastify('error', `user ${userId} is not exists`)
    }
  };

  // 停止/启用 音频
  toggleMuteAudio = (userId: string) => {
    let config = this.getStream(userId);
    if(config && config.stream) {
      if(config.subscribedAudio) {
        this.handleSubscribe(config.stream, false, config.subscribedVideo).catch();
      } else {
        this.handleSubscribe(config.stream, true, config.subscribedVideo).catch();
      }
    } else {
      toastify('error', `user ${userId} is not exists`)
    }
  };

  // 订阅
  unmute = (userId: string) => {
    let config = this.getStream(userId);
    if(config && config.stream && config.hasAudio) {
      this.handleSubscribe(config.stream, true, config.hasVideo).catch();
    } else {
      toastify('error', `user ${userId} is not exists`)
    }
  };

  // 取消订阅
  mute = (userId: string) => {
    let config = this.getStream(userId);
    if(config && config.stream) {
      this.handleSubscribe(config.stream, false, false).catch();
    } else {
      toastify('error', `user ${userId} is not exists`)
    }
  };

  startGetAudioLevel = () => {
    this.ctrl.liveCtrl.localLiveCtrl.client.on('audio-volume', event => {
      // @ts-ignore
      event.result.forEach(({ userId, audioVolume }) => {
        if (audioVolume > 2) {
          let oldConfig = this.remoteStreamConfigMap.get(userId);
          if(oldConfig && oldConfig.audioVolume !== audioVolume) {
            this.updateStream(userId, {audioVolume: audioVolume});
          }
        } else {
          let oldConfig = this.remoteStreamConfigMap.get(userId);
          if(oldConfig && oldConfig.audioVolume !== 0) {
            this.updateStream(userId, {audioVolume: 0});
          }
        }
      });
    });
    this.ctrl.liveCtrl.localLiveCtrl.client.enableAudioVolumeEvaluation(200);
  };

  handleClientEvents = () => {

    // 远端用户进房通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('peer-join', (event) => {
      const { userId } = event;
      console.log(`peer-join ${userId}`, event);
      this.addUser(userId);
    });

    // 远端用户退房通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('peer-leave', (event) => {
      const { userId } = event;
      console.log(`peer-leave ${userId}`, event);
      this.removeUser(userId);
    });

    // 远端流添加事件，当远端用户发布流后会收到该通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('stream-added', async (event) => {
      const { stream: remoteStream } = event;
      const remoteUserId = remoteStream.getUserId();
      console.log(`remote stream added: [${remoteUserId}] type: ${remoteStream.getType()}`);
      await this.addStream(remoteStream);
    });

    // 远端流订阅成功事件，本地用户订阅远端流成功后收到该通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('stream-subscribed', (event) => {
      const { stream: remoteStream } = event;
      console.log('stream-subscribed userId: ', remoteStream.getUserId());
      this.playStream(remoteStream);
    });

    // 远端流移除事件，当远端用户取消发布流后会收到该通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('stream-removed', async (event) => {
      const { stream: remoteStream } = event;
      console.log(`stream-removed userId: ${remoteStream.getUserId()} type: ${remoteStream.getType()}`);
      await this.removeStream(remoteStream);
    });

    // 远端流更新事件，当远端用户添加、移除或更换音视频轨道后会收到该通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('stream-updated', (event) => {
      const { stream: remoteStream } = event;
      this.updateStream(remoteStream.getUserId(), {
        stream: remoteStream,
        hasAudio: remoteStream.hasAudio(),
        hasVideo: remoteStream.hasVideo(),
      });
      console.log(`type: ${remoteStream.getType()} stream-updated hasAudio: ${remoteStream.hasAudio()} hasVideo: ${remoteStream.hasVideo()}`);
    });

    // 远端用户禁用音频通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('mute-audio', (event) => {
      const { userId } = event;
      console.log(`${userId} mute audio`);
      this.updateStream(userId, {mutedAudio: true});
    });

    // 远端用户启用音频通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('unmute-audio', (event) => {
      const { userId } = event;
      console.log(`${userId} unmute audio`);
      this.updateStream(userId, {mutedAudio: false});
    });

    // 远端用户禁用视频通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('mute-video', (event) => {
      const { userId } = event;
      console.log(`${userId} mute video`);
      this.updateStream(userId, {mutedVideo: true});
    });

    // 远端用户启用视频通知
    this.ctrl.liveCtrl.localLiveCtrl.client.on('unmute-video', (event) => {
      const { userId } = event;
      console.log(`${userId} unmute video`);
      this.updateStream(userId, {mutedVideo: false});
    });

  };

  handleStreamEvents = (stream: RemoteStream) => {
    stream.on('player-state-changed', async (event)  => {
      if (event.state === 'PAUSED') {
        await this.resumeStream(stream);
      }
    });

    stream.on('error', async (error) => {
      const errorCode = error.getCode();
      if (errorCode === 0x4043) {
        await this.resumeStream(stream);
      }
    });
  }

}

