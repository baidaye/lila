import {VNode} from 'snabbdom/vnode'
import {RemoteStreamType, RemoteStream} from 'trtc-js-sdk';
import * as cg from 'chessground/types';
import {Pref} from '../../round/src/interfaces';
import {AnalyseData as GameData} from "../../analyse/src/interfaces";
export type OSName = 'Android' | 'iOS' | 'Windows' | 'MacOS' | 'Linux';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]
export const emptyFen = '8/8/8/8/8/8/8/8 w - -';
export const initialFen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

export const DEVICE_KIND = {
  CAMERA: 'camera',
  MICROPHONE: 'microphone',
  SPEAKER: 'speaker',
  NETWORK: 'network'
};

export interface OlClassOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  sdkAppId: number;
  userSig: string;
  tagTypes: TagTypes;
  olClass: OlClass;
  courseWares: CourseWare[];
  pref: Pref;
  round: GameData;
  clazz: Clazz;
  course: Course;
  coach: User;
  students: User[];
  chat: any;
  explorer: any;
}

export interface DeviceInfo {
  system: SystemInfo;
  APISupported: APISupportedInfo;
  codecsSupported: CodecsSupportedInfo;
  devices: DevicesInfo;
  detects: any;
}

export interface SystemInfo {
  OS: OSName;
  UA: string;
  browser: BrowserInfo;
  displayResolution: DisplayResolutionInfo;
  hardwareConcurrency: number;
}

export interface BrowserInfo {
  name: string;
  version: string;
}

export interface DisplayResolutionInfo {
  width: number;
  height: number;
}

export interface APISupportedInfo {
  isUserMediaSupported: boolean;
  isWebRTCSupported: boolean;
  isWebSocketSupported: boolean;
  isWebAudioSupported: boolean;
  isScreenCaptureAPISupported: boolean;
  isCanvasCapturingSupported: boolean;
  isVideoCapturingSupported: boolean;
  isRTPSenderReplaceTracksSupported: boolean;
  isApplyConstraintsSupported: boolean;
}

export interface CodecsSupportedInfo {
  isH264EncodeSupported: boolean;
  isVp8EncodeSupported: boolean;
  isH264DecodeSupported: boolean;
  isVp8DecodeSupported: boolean;
}

export interface DevicesInfo {
  cameras: MediaDeviceInfo[];
  microphones: MediaDeviceInfo[];
  speakers: MediaDeviceInfo[];
  hasCameraPermission: boolean;
  hasMicrophonePermission: boolean;
}

export {Key, Piece} from 'chessground/types';

export interface CgDests {
  [key: string]: cg.Key[]
}

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}

export type StreamRole = 'host' | 'self' | 'spectator' | 'speaker';
export interface StreamConfig {
  userId: string;
  user: User;
  hasAudio: boolean;
  hasVideo: boolean;
  mutedAudio: boolean;
  mutedVideo: boolean;
  subscribedAudio: boolean;
  subscribedVideo: boolean;
  audioVolume: number;
  remote: boolean;
}

export interface LocalStreamConfig extends StreamConfig {
  uplinkNetworkQuality: number;
  downlinkNetworkQuality: number;
  shareDesk: boolean;
}

export interface RemoteStreamConfig extends StreamConfig {
  stream: RemoteStream | null;
  streamType: RemoteStreamType;
}

export interface LiveConfig extends StreamConfig {
  roles: StreamRole[];
  isJoined: boolean;
  isHandUp: boolean;
  toggleMuteVideo: (userId?: string) => void;
  toggleMuteAudio: (userId?: string) => void;
}

export type OlClassStatus = 'created' | 'started' | 'stopped';
export interface OlClass {
  id: string;
  status: OlClassStatus;
  position: Position;
  seconds: number
  muteAll: boolean;
  syncs: string[];
  handsUp: string[];
  speakers: string[];
}

interface Position {
  courseWareId: string;
  path: Tree.Path;
}

export type TagTypes = string[];
export type TagArray = [string, string];
export interface CourseWare {
  id: string;
  name: string;
  order: number
  source: string;
  tags: TagArray[]
  orientation: string;
  showTree: boolean;
  initPly: number;
  note: string;
}

export interface Clazz {
  id: string;
  name: string;
  coach: string;
  students: string[];
}

export interface Course {
  id: string;
  index: number;
  dateStartTime: string;
  dateEndTime: string;
}

export interface User extends LightUser {
  online: boolean;
  mark?: string;
}

export type Redraw = () => void;

