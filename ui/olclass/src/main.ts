import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import throttle from 'common/throttle';
import {Chessground} from 'chessground';
import {OlClassOpts} from './interfaces';
import OlClassCtrl from './ctrl';
import * as chat from 'chat';

export const patch = init([klass, attributes]);

import view from './view/main';

export function start(opts: OlClassOpts) {

  let vnode: VNode;

  let redraw = throttle(100, () => {
    vnode = patch(vnode, view(ctrl));
  });

  const ctrl = new OlClassCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}

// function throttleOnlyExeLast(fn, delay) {
//   let timer = null;
//   return function () {
//     var context = this;
//     var args = arguments;
//     if (!timer) {
//       timer = setTimeout(function () {
//         fn.apply(context, args);
//         timer = null;
//       }, delay)
//     }
//   }
// }

// that's for the rest of lichess to access chessground
// without having to include it a second time
window.Chessground = Chessground;
window.LichessChat = chat;

