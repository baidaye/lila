import {OlClassOpts, Redraw} from './interfaces';
import {make as makeSocket, Socket} from './socket';
import OlClassGroundCtrl from './groundCtrl';
import OlClassEvalCtrl from './evalCtrl';
import OlClassCheckCtrl from './checkCtrl';
import OlClassLiveCtrl from  './liveCtrl'
import OlCourseWareCtrl from  './courseWareCtrl'

export default class OlClassCtrl {

  opts: OlClassOpts;
  trans: Trans;
  socket: Socket;
  groundCtrl: OlClassGroundCtrl;
  evalCtrl: OlClassEvalCtrl;
  checkCtrl: OlClassCheckCtrl;
  liveCtrl: OlClassLiveCtrl;
  courseWareCtrl: OlCourseWareCtrl;

  redirecting: boolean = false;

  constructor(opts: OlClassOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.trans = window.lichess.trans(opts.i18n);
    this.socket = makeSocket(this.opts.socketSend, this);
    this.evalCtrl = new OlClassEvalCtrl(this, this.trans);
    this.groundCtrl = new OlClassGroundCtrl(this, this.trans);
    this.courseWareCtrl = new OlCourseWareCtrl(this);
    this.checkCtrl = new OlClassCheckCtrl(this);
    this.liveCtrl = new OlClassLiveCtrl(this);
  }

}

