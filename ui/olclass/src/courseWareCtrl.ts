import * as xhr from "./xhr";
import OlClassCtrl from "./ctrl";
import {CourseWare} from "./interfaces";

export default class CourseWareClassCtrl {

  createFormModal: boolean = false;
  editFormModal: boolean = false;
  activeTab: string = 'study';
  loading: boolean = false;
  edit: CourseWare | null = null;

  selectedId: string;
  selected: CourseWare;
  checked: CourseWare | null = null;  // 选中后可以在当前章节下添加新章节（解决排序问题）

  //--------------study--------------
  studyTab: string = 'study';
  studyLoading: boolean = true;
  studyPaginator: any;
  chapterLoading: boolean = true;
  chapters: any;
  currentStudy: any;
  currentChapter: any;
  currentPgn: string;
  studyTags: string[] = [];
  studyChannel: string = 'mine';
  studyTagData = {
    emptyTag: 'off',
    tags: []
  };

  //--------------homework--------------
  homeworkRelTab: string = 'homework';
  homeworkRelContentTab: string = 'replay';
  filteredRelHomeworks: any = [];
  currentRelHomework: any;


  //--------------contest--------------
  contestTab: string = 'contest';
  contestLoading: boolean = true;
  contestPaginator: any;
  roundLoading: boolean = true;
  rounds: any;
  boardLoading: boolean = true;
  boards: any;
  currentContest: any;
  currentRound: any;

  //--------------gamedb--------------
  gamedbSelected: any = [];

  //--------------situationdb--------------
  situationLoading: boolean = true;
  situationQuery: any = {
    selected: this.ctrl.opts.userId,
    emptyTag: '',
    standard: true,
    name: '',
    tagSelected: []
  };

  situationTagsLoading: boolean = true;
  situationTags: any = [];
  situations: any = [];

  //--------------capsule--------------
  puzzleRelTab: string = 'capsule';
  capsuleTab: string = 'capsule';
  capsuleLoading: boolean = true;
  capsule: any;
  capsules: any = [];
  capsuleQuery: any = {
    tab: 'mine',
    name: '',
    tagSelected: []
  };
  currentCapsule: any;
  capsulePuzzleLoading: boolean = true;
  capsulePuzzles: any = [];
  capsuleTabs = [{id: 'mine', name: '我的列表'}, {id: 'member', name: '参与列表'}, {id: 'teamManager', name: '俱乐部授权'}];

  //--------------homework puzzle--------------
  homeworkTab: string = 'homework';
  filteredHomeworks: any = [];
  currentHomework: any;

  //--------------editor--------------
  editor: any;

  // --------------homework common---------------
  homeworksLoading: boolean = true;
  homeworks: any;
  homeworkLoading: boolean = true;
  homework: any;

  constructor(readonly ctrl: OlClassCtrl) {
    let hash = location.hash.replace('#', '');
    let hashCourseWare = this.ctrl.opts.courseWares.find(c => c.id === hash);
    if(hash && hashCourseWare) {
      this.loadCourseWare(hash)
    } else {
      if (this.ctrl.opts.courseWares.length > 0) {
        this.loadCourseWare(this.ctrl.opts.courseWares[0].id);
      }
    }
  }

  isSelected = () => this.selectedId !== null && this.selectedId !== undefined && this.selectedId !== '';

  getCourseWare = () => {
    if(this.isSelected()) {
      return this.selected;
    }
  };

  checkCourseWare = (data: CourseWare) => {
    this.checked = data;
    this.ctrl.redraw();
  };

  setCourseWare = (data: CourseWare, check: boolean = false, force: boolean = false) => {
    if(this.selectedId === data.id && !force) return;
    if(check) {
      this.checkCourseWare(data);
    }

    // 只有教练能切换章节时控制同步
    if(this.ctrl.liveCtrl.localIsCoach() && this.ctrl.liveCtrl.localIsSync() && this.ctrl.liveCtrl.isStarted()) {
      this.ctrl.groundCtrl.send('setCourseWare', data.id);
    } else {
      this.loadCourseWare(data.id);
    }
  };

  // callback after send setCourseWare
  onChangeCourseWare = (data) => {
    if(this.ctrl.groundCtrl.isSubOrPubSync()) {
      this.ctrl.opts.olClass.position = data.p;
      this.ctrl.groundCtrl.setMemberActive(data.w);
      this.loadCourseWare(data.p.courseWareId);
    }
  };

  loadCourseWare = (id: string) => {
    xhr.loadCourseWare(this.ctrl.opts.course.id, id).then((data) => {
      this.selectedId = id;
      this.selected = data;
      this.ctrl.groundCtrl.changeSituation(data.round, id);
    });
  };

  create = () => {
    let data = $('.courseWareCreateForm').serialize();
    xhr.createCourseWare(this.ctrl.opts.course.id, data).then(() => {

      this.reload(true);
    }, (err) => {
      if(err.status === 400) {
        if(err.responseJSON && err.responseJSON.error) {
          let er = err.responseJSON.error;
          if(er.global && er.global[0]) {
            alert(JSON.stringify(er.global[0]));
          } else {
            alert(JSON.stringify(er.error));
          }
        } else {
          alert(JSON.stringify(err.responseText));
        }
      } else {
        alert(JSON.stringify(err.responseText));
      }
    });
  };

  update = () => {
    if(this.edit) {
      let editId = this.edit.id;
      let data = $('.courseWareEditForm').serialize();
      xhr.updateCourseWare(this.ctrl.opts.course.id, editId, data).then(() => {
        this.reload();
        if(editId === this.selectedId) {
          this.loadCourseWare(editId);
        }
      }, (err) => {
        if(err.status === 400) {
          if(err.responseJSON && err.responseJSON.error) {
            alert(JSON.stringify(err.responseJSON.error));
          } else {
            alert(JSON.stringify(err.responseText));
          }
        } else {
          alert(JSON.stringify(err.responseText));
        }
      });
    }
  };

  remove = () => {
    if(this.edit) {
      xhr.removeCourseWare(this.ctrl.opts.course.id, this.edit.id).then(() => {
        if(this.edit && this.edit.id === this.selectedId) {
          let firstCourseWare = this.ctrl.opts.courseWares.find(c => this.edit && c.id !== this.edit.id);
          if(firstCourseWare) {
            this.loadCourseWare(firstCourseWare.id)
          } else {
            location.reload();
          }
        }
        this.reload();
      });
    }
  };

  sort = (ids) => {
    xhr.sortCourseWare(this.ctrl.opts.course.id, ids).then(() => {
      this.reload();
    });
  };

  reload = (st: boolean = false) => {
    //this.loading = true;
    this.ctrl.redraw();

    xhr.loadCourseWares(this.ctrl.opts.course.id).then((data) => {
      this.ctrl.opts.courseWares = data;
      this.createFormModal = false;
      this.editFormModal = false;
      //this.loading = false;
      this.ctrl.redraw();

      setTimeout(() => {
        if(st) {
          if(this.checked) {
            let el = $('.olclass__courseWare')[0] as HTMLElement;
            let scrollHeight = el.scrollHeight;
            let checkedTopHeight = ($(`.cw-${this.checked.id}`)[0] as HTMLElement).offsetTop;
            el.scrollTop = Math.min(scrollHeight, checkedTopHeight);
          }

          if (this.ctrl.opts.courseWares.length === 1) {
            this.loadCourseWare(this.ctrl.opts.courseWares[0].id);
          }
        }
      }, 200);
    });
  };

  setNote = (text) => {
    xhr.setNote(this.ctrl.opts.course.id, this.selectedId, text).then(() => {
      this.selected.note = text;
      this.ctrl.redraw();
    });
  };

  closeTaskCreateModal = () => {
    this.studyTab = 'study';
    this.studyPaginator = null;
    this.studyLoading = true;
    this.chapters = null;
    this.chapterLoading = true;
    this.currentStudy = null;
    this.currentChapter = null;
    this.currentPgn = '';
    this.studyTags = [];
    this.studyChannel = 'mine';
    this.studyTagData = {
      emptyTag: 'off',
      tags: []
    };

    // --------------homework common---------------
    this.homeworksLoading = true;
    this.homeworks = null;
    this.homeworkLoading = true;
    this.homework = null;

    //--------------homework--------------
    this.homeworkRelTab = 'homework';
    this.filteredRelHomeworks = [];
    this.currentRelHomework = null;

    //-----------
    this.contestTab = 'contest';
    this.contestLoading = true;
    this.contestPaginator = null;
    this.roundLoading = true;
    this.rounds = null;
    this.boardLoading = true;
    this.boards = null;
    this.currentContest = null;
    this.currentRound = null;

    //-----------
    this.gamedbSelected = [];

    //-----------
    this.situationLoading = true;
    this.situationQuery = {
      selected: this.ctrl.opts.userId,
      emptyTag: '',
      standard: true,
      name: '',
      tagSelected: []
    };
    this.situationTagsLoading = true;
    this.situationTags = [];
    this.situations = [];

    //-----------
    this.capsuleTab = 'capsule';
    this.capsuleLoading = true;
    this.capsule = null;
    this.capsules = [];
    this.capsuleQuery = {
      tab: 'mine',
      name: '',
      tagSelected: []
    };

    this.currentCapsule = null;
    this.capsulePuzzleLoading = true;
    this.capsulePuzzles = [];

    //------homework puzzle-----
    this.homeworkTab = 'homework';
    this.homeworkLoading = true;
    this.homeworks = [];
    this.filteredHomeworks = [];
    this.currentHomework = null;

    //-----------
    this.editor = null;

    //-----------
    this.createFormModal = false;
    this.ctrl.redraw();
  };

  onInitStudy = () => {
    this.loadStudy();
    this.loadStudyTags();
  };

  onInitContest = () => {
    this.loadContest();
  };

  onInitSituation = () => {
    this.loadSituationTags();
    this.loadSituation();
  };

  onInitPuzzleRels = () => {
    this.loadCapsules();
    this.loadHomeworks();
  };

  onStudyTabChange = (channel) => {
    this.studyChannel = channel;
    this.studyTagData = {
      emptyTag: 'off',
      tags: []
    };
    this.loadStudy();
    this.loadStudyTags();
  };

  loadStudyTags = () => {
    xhr.loadStudyTags(this.studyChannel).then((data) => {
      this.studyTags = data;
      this.ctrl.redraw();
    });
  };

  onStudyEmptyTagCheck = (checked) => {
    $('.study-search-input').val('');
    this.studyTagData.emptyTag = checked ? 'on' : 'off';
    this.studyTagData.tags = [];
    this.ctrl.redraw();
    this.loadStudy();
  };

  onStudyTagCheck = (tag, checked) => {
    this.studyTagData.emptyTag = 'off';
    $('.study-search-input').val('');
    this.ctrl.redraw();
    if(checked) {
      // @ts-ignore
      this.studyTagData.tags.push(tag);
    } else {
      this.studyTagData.tags = this.studyTagData.tags.filter(t => t !== tag);
    }
    this.loadStudy();
  };

  loadStudy = (p: number = 1) => {
    this.studyLoading = true;
    this.ctrl.redraw();
    let q = $('.study-search-input').val();
    xhr.loadStudy(this.studyChannel, p, q, this.studyTagData).then((data) => {
      this.studyPaginator = data.paginator;
      this.studyLoading = false;
      this.ctrl.redraw();
    });
  };

  loadChapter = () => {
    this.chapterLoading = true;
    this.ctrl.redraw();
    xhr.loadChapter(this.currentStudy.id).then((data) => {
      this.chapters = data.chapters;
      this.chapterLoading = false;
      this.ctrl.redraw();
    });
  };

  loadContest = (p: number = 1) => {
    this.contestLoading = true;
    this.ctrl.redraw();

    let q = $('.contest-search-input').val();
    q = q ? q.trim() : '';
    xhr.loadContest(p, q).then((data) => {
      this.contestPaginator = data.paginator;
      this.contestLoading = false;
      this.ctrl.redraw();
    });
  };

  loadRound = () => {
    this.roundLoading = true;
    this.ctrl.redraw();
    xhr.loadRound(this.currentContest.id).then((data) => {
      this.rounds = data;
      this.roundLoading = false;
      this.ctrl.redraw();
    });
  };

  loadBoard = () => {
    this.boardLoading = true;
    this.ctrl.redraw();
    xhr.loadBoard(this.currentContest.id, this.currentRound.id).then((data) => {
      this.boards = data;
      this.boardLoading = false;
      this.ctrl.redraw();
    });
  };

  loadSituationTags = () => {
    this.situationTagsLoading = true;
    this.ctrl.redraw();
    xhr.loadSituationTags().then((data) => {
      this.situationTags = data;
      this.situationTagsLoading = false;
      this.ctrl.redraw();
    });
  };

  loadSituation = () => {
    let situationQueryExt = {};
    this.situationQuery.tagSelected.forEach((tag, i) => {
      situationQueryExt[`tags[${i}]`] = tag;
    });

    this.situationLoading = true;
    this.ctrl.redraw();
    xhr.loadSituation({...this.situationQuery, ...situationQueryExt}).then((data) => {
      this.situations = data;
      this.situationLoading = false;
      this.ctrl.redraw();
    });
  };

  loadCapsules = () => {
    this.capsuleLoading = true;
    this.ctrl.redraw();
    xhr.loadCapsules().then((data) => {
      this.capsule = data;
      this.capsules = this.capsule[this.capsuleQuery.tab].list;
      this.capsuleLoading = false;
      this.ctrl.redraw();
    });
  };

  filterCapsule = () => {
    if(this.capsule && this.capsule[this.capsuleQuery.tab].list) {
      this.capsules = this.capsule[this.capsuleQuery.tab].list.filter((n) => {
        return (!this.capsuleQuery.name || n.name.indexOf(this.capsuleQuery.name) > -1) && (this.capsuleQuery.tagSelected.length === 0 || this.capsuleQuery.tagSelected.every(t => n.tags.includes(t)))
      })
    }
    this.ctrl.redraw();
  };

  loadCapsulePuzzle = () => {
    this.capsulePuzzleLoading = true;
    this.ctrl.redraw();
    xhr.loadCapsulePuzzles(this.currentCapsule.id).then((data) => {
      this.capsulePuzzles = data;
      this.capsulePuzzleLoading = false;
      this.ctrl.redraw();
    });
  };

  loadHomeworks = () => {
    this.homeworksLoading = true;
    this.ctrl.redraw();
    xhr.loadHomeworks(this.ctrl.opts.clazz.id).then((data) => {
      this.homeworks = data;
      this.filteredHomeworks = data;
      this.filteredRelHomeworks = data;
      this.homeworksLoading = false;
      this.ctrl.redraw();
    });
  };

  filterHomework = () => {
    let q = $('.homework-search-input').val();
    if(this.homeworks) {
      this.filteredHomeworks = this.homeworks.filter((n) => {
        return (!q || n.index.toString().indexOf(q) > -1)
      })
    }
    this.ctrl.redraw();
  };


  filterRelHomework = () => {
    let q = $('.homework-rel-search-input').val();
    if(this.homeworks) {
      this.filteredRelHomeworks = this.homeworks.filter((n) => {
        return (!q || n.index.toString().indexOf(q) > -1)
      })
    }
    this.ctrl.redraw();
  };

  loadHomework = () => {
    this.homeworkLoading = true;
    this.ctrl.redraw();
    let curr = this.activeTab === 'puzzleRels' ? this.currentHomework : this.currentRelHomework;
    let id = `${this.ctrl.opts.clazz.id}@${curr.id}@${curr.index}`;
    let sort = $('select[name="homeworkPuzzleSort"]').val();
    xhr.loadHomework(id, sort ? sort : 'no').then((data) => {
      this.homework = data;
      this.homeworkLoading = false;
      this.ctrl.redraw();
    });
  }

}

