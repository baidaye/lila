import * as TRTC from 'trtc-js-sdk/trtc.js';
import {Client, ConnectionState, LocalStream} from 'trtc-js-sdk';
import {DEVICE_KIND, LocalStreamConfig} from './interfaces';
import {toastify, handleGetUserMediaError} from './util';
import OlClassCtrl from './ctrl';

export default class OlClassLiveLocalCtrl {

  client: Client;
  localStream: LocalStream | null;
  localStreamConfig: LocalStreamConfig;

  isJoining: boolean = false;
  isJoined: boolean = false;
  isPublishing: boolean = false;
  isPublished: boolean = false;
  isUnPublishing: boolean = false;
  isLeaving: boolean = false;
  isAudioMuted: boolean = false;
  isVideoMuted: boolean = false;

  constructor(readonly ctrl: OlClassCtrl) {
    this.addWindowListener();

    window.lichess.pubsub.on('switchDevice', (type, deviceId) => {
      this.switchDevice(type, deviceId);
    });
  }

  // 重新加入房间
  reJoin = async () => {
    await this.ctrl.liveCtrl.leave();
    await this.ctrl.liveCtrl.join();
  };

  // 用户加入房间
  handleJoin = async () => {
    if (this.isJoining || this.isJoined) {
      return;
    }
    this.isJoining = true;
    this.resetLocalUser();
    this.ctrl.redraw();

    await this.initClient();
    try {
      await this.client.join({ roomId: this.ctrl.opts.course.id });
      this.isJoining = false;
      this.isJoined = true;
      this.startGetAudioLevel();
      await this.handlePublish();

      if(!this.ctrl.liveCtrl.localIsCoach()) {
        this.muteAudio();
      }
      this.ctrl.groundCtrl.reloadOlClass();
      this.ctrl.groundCtrl.redrawGround();
      window.lichess.pubsub.emit('rtc_room_joined');
      console.info('join room success');
    } catch (error) {
      this.isJoining = false;
      handleGetUserMediaError(error);
      // toastify('error', 'join room failed!');
      // console.error('join room failed', error);
    }
  };

  // 初始化客户端
  initClient = async () => {
    this.client = TRTC.createClient({
      mode: 'rtc',
      sdkAppId: this.ctrl.opts.sdkAppId,
      userId: this.ctrl.opts.userId,
      userSig: this.ctrl.opts.userSig,
      useStringRoomId: true,
    });
    window.lichess.pubsub.emit('rtc_client_init');
    this.handleClientEvents();
  };

  // 新增本地用户
  resetLocalUser = () => {
    this.localStreamConfig = {
      userId: this.ctrl.opts.userId,
      user: this.ctrl.liveCtrl.getUser(this.ctrl.opts.userId),
      hasAudio: this.ctrl.liveCtrl.hasAudio(),
      hasVideo: this.ctrl.liveCtrl.hasVideo(),
      mutedAudio: false,
      mutedVideo: false,
      subscribedAudio: true,
      subscribedVideo: true,
      audioVolume: 0,
      remote: false,
      uplinkNetworkQuality: 0,
      downlinkNetworkQuality: 0,
      shareDesk: false
    };
    this.ctrl.redraw();
  };

  // 发布本地流
  handlePublish = async () => {
    if (!this.isJoined || this.isPublishing || this.isPublished) {
      return;
    }
    this.isPublishing = true;
    !this.localStream && (await this.initLocalStream());
    try {
      this.localStream && await this.client.publish(this.localStream);
      this.isPublishing = false;
      this.isPublished = true;
      console.info('publish localStream success');
    } catch (error) {
      this.isPublishing = false;
      handleGetUserMediaError(error);
      // console.error('publish localStream failed', error);
      // toastify('error', 'publish localStream failed!');
    }
  };

  // 初始化本地流
  initLocalStream = async () => {
    this.localStream = TRTC.createStream({
      audio: true,
      video: true,
      userId: this.ctrl.opts.userId,
      cameraId: this.ctrl.liveCtrl.getCameraId(),
      microphoneId: this.ctrl.liveCtrl.getMicrophoneId()
    });

    try {
      this.localStream && (this.localStream.setVideoProfile('480p'));
      this.localStream && (await this.localStream.initialize());
      this.localStream && this.playStream(this.localStream);
    } catch (error) {
      this.localStream = null;
      //toastify('error', `${JSON.stringify(error.message)}`);
      handleGetUserMediaError(error);
    }
    this.localStream && this.handleStreamEvents(this.localStream);
  };

  // 更新本地流的操作状态
  updateStream = (config: any) => {
    this.localStreamConfig = {...this.localStreamConfig, ...config} as LocalStreamConfig;
    this.ctrl.redraw();
  };

  // 播放音视频流
  playStream = (stream: LocalStream) => {
    let id = $(`#video-${stream.getUserId()}`)[0].id;
    console.log('elementId1: ', id);
    stream.play(`video-${stream.getUserId()}`).catch();
    this.ctrl.redraw();
  };

  // 恢复播放音视频
  resumeStream = async (stream: LocalStream) => {
    await stream.resume();
  };

  // 关闭音频流
  closeStream = (stream: LocalStream) => {
    stream.stop();
    stream.close();
  };

  handleLeave = async () => {
    if (!this.isJoined || this.isLeaving) {
      return;
    }

    this.isLeaving = true;
    this.stopGetAudioLevel();
    if (this.isPublished) {
      await this.handleUnPublish();
    }

    try {
      await this.client.leave();
      this.removeUser();
      this.isLeaving = false;
      this.isJoined = false;
      this.ctrl.redraw();
      this.ctrl.groundCtrl.showGround();
      this.ctrl.groundCtrl.redrawGround();
    } catch (error) {
      this.isLeaving = false;
      //console.error('leave room error', error);
      handleGetUserMediaError(error);
    }
  };

  handleUnPublish = async () => {
    if (!this.isPublished || this.isUnPublishing) {
      return;
    }

    this.isUnPublishing = true;
    try {
      this.localStream && (await this.client.unpublish(this.localStream));
      this.isUnPublishing = false;
      this.isPublished = false;
    } catch (error) {
      this.isUnPublishing = false;
      console.error('unpublish localStream failed', error);
      switch (error.getCode()) {
        case 4096: // ErrorCode = 0x1001 INVALID_OPERATION
          toastify('error', 'stream has not been published yet, please publish first');
          break;
        case 4097: // ErrorCode = 0x1001 INVALID_PARAMETER
          toastify('error', 'publish is ongoing, please try unpublish later');
          break;
        default:
          toastify('error', 'unpublish localStream failed! please try again later');
          break;
      }
    }
    this.destroyStream();
  };

  // 销毁本地流
  destroyStream = () => {
    if(this.localStream) {
      this.closeStream(this.localStream);
      this.localStream = null;
    }
  };

  // 移除本地用户
  removeUser = () => {
    this.resetLocalUser();
  };

  // 禁用/启用 视频
  toggleMuteVideo = () => {
    if (this.isJoined && this.localStream) {
      if (this.localStreamConfig.mutedVideo) {
        this.localStream.unmuteVideo();
      } else {
        this.localStream.muteVideo();
      }
      this.updateStream({mutedVideo: !this.localStreamConfig.mutedVideo});
    }
  };

  // 禁用/启用 音频
  toggleMuteAudio = () => {
    if (this.isJoined && this.localStream) {
      if (this.localStreamConfig.mutedAudio) {
        this.localStream.unmuteAudio();
      } else {
        this.localStream.muteAudio();
      }
      this.updateStream({mutedAudio: !this.localStreamConfig.mutedAudio});
    }
  };

  // 禁用音频
  muteAudio = () => {
    if (this.isJoined && this.localStream && !this.localStreamConfig.mutedAudio) {
      this.localStream.muteAudio();
      this.updateStream({mutedAudio: true});
    }
  };

  // 禁用
  mute = () => {
    if (this.isJoined && this.localStream) {
      this.localStream.muteAudio();
      this.localStream.muteVideo();
      this.updateStream({mutedAudio: true, mutedVideo: true});
    }
  };

  // 取消禁用
  unmute = () => {
    if (this.isJoined && this.localStream && this.localStream.hasAudio()) {
      if(this.localStream.hasAudio()) {
        this.localStream.unmuteAudio();
        this.updateStream({mutedAudio: false});
      }

      if(this.localStream.hasVideo()) {
        this.localStream.unmuteVideo();
        this.updateStream({ mutedVideo: false});
      }
    }
  };

  startGetAudioLevel = () => {
    this.client.on('audio-volume', event => {
      // @ts-ignore
      event.result.forEach(({ userId, audioVolume }) => {
        if(this.isJoined) {
          if (audioVolume > 2) {
            if(this.localStreamConfig.audioVolume !== audioVolume) {
              this.updateStream({audioVolume: audioVolume});
            }
          } else {
            if(this.localStreamConfig.audioVolume !== 0) {
              this.updateStream({audioVolume: 0});
            }
          }
        }
      });
    });
    this.client.enableAudioVolumeEvaluation(200);
  };

  stopGetAudioLevel = () => {
    this.client.enableAudioVolumeEvaluation(-1);
  };

  switchDevice = (type: 'audio' | 'video', deviceId: string) => {
    if(type === 'audio') {
      this.switchAudio(deviceId);
    } else if(type === 'video') {
      this.switchVideo(deviceId);
    }
  };

  switchVideo = (deviceId: string) => {
    if(this.isJoined && this.ctrl.liveCtrl.getCameraId() !== deviceId) {
      this.localStream && this.localStream.switchDevice('video', deviceId).then(_ => {
        this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.CAMERA].deviceId = deviceId;
        this.ctrl.checkCtrl.setCache();
        this.ctrl.redraw();
        toastify('success', '切换设备成功');
      });
    }
  };

  switchAudio = (deviceId: string) => {
    if(this.isJoined && this.ctrl.liveCtrl.getMicrophoneId() !== deviceId) {
      this.localStream && this.localStream.switchDevice('audio', deviceId).then(_ => {
        this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.MICROPHONE].deviceId = deviceId;
        this.ctrl.checkCtrl.setCache();
        this.ctrl.redraw();
        toastify('success', '切换设备成功');
      });
    }
  };

  handelStateChanged = (prevState: ConnectionState, currState: ConnectionState) => {
    // DISCONNECTED -> CONNECTING: 正在尝试建立连接，调用进房接口时触发。
    if(prevState === 'DISCONNECTED' && currState === 'CONNECTING') {
      toastify('info', '正在进入房间');
    }

    // CONNECTING -> CONNECTED: 连接建立成功，调用进房接口， Websocket 连接成功时触发
    if(prevState === 'CONNECTING' && currState === 'CONNECTED') {
      toastify('success', '您已进入房间');
    }

    // CONNECTED -> DISCONNECTED: 连接中断，当网络异常导致连接断开时触发。
    if(prevState === 'CONNECTED' && currState === 'DISCONNECTED') {
      toastify('warn', '连接中断');
    }

    // DISCONNECTED -> RECONNECTING: 连接中断，当网络异常导致连接断开时触发。
    if(prevState === 'DISCONNECTED' && currState === 'RECONNECTING') {
      toastify('info', '正在重连');
    }

    // RECONNECTING -> CONNECTED: SDK 自动重连成功。
    if(prevState === 'RECONNECTING' && currState === 'CONNECTED') {
      toastify('success', '重连成功');
    }

    // RECONNECTING -> DISCONNECTED: SDK 自动重连失败。
    if(prevState === 'RECONNECTING' && currState === 'DISCONNECTED') {
      toastify('warn', '重连失败');
    }
  };

  handleClientEvents = () => {
    // 错误事件，当出现不可恢复错误后，会抛出此事件
    this.client.on('error', async (error) => {
      console.error(error);
      await this.reJoin();
    });

    // 用户被踢出房间通知
    this.client.on('client-banned', async (error) => {
      console.info(`client has been banned for ${error}`);
      toastify('error', `client has been banned for ${error}`);
      await this.handleLeave();
    });

    // SDK 和腾讯云的连接状态变更事件
    this.client.on('connection-state-changed', (event) => {
      console.log(`RtcClient state changed to ${event.state} from ${event.prevState}`);
      this.handelStateChanged(event.prevState, event.state);
    });

    // 网络质量统计数据事件，进房后开始统计，每两秒触发一次
    // this.client.on('network-quality', (event) => {
    //   const { uplinkNetworkQuality, downlinkNetworkQuality } = event;
    //   this.updateStream({uplinkNetworkQuality: uplinkNetworkQuality});
    //   this.updateStream({downlinkNetworkQuality: downlinkNetworkQuality});
    // });
  };

  handleStreamEvents = (stream: LocalStream) => {
    stream.on('player-state-changed', async (event)  => {
      if (event.state === 'PAUSED') {
        await this.resumeStream(stream);
      }
    });

    stream.on('error', async (error) => {
      const errorCode = error.getCode();
      if (errorCode === 0x4043) {
        await this.resumeStream(stream);
      }
    });
  };

  addWindowListener = () => {
    const that = this;
    window.addEventListener('beforeunload', (e) => {
      if (that.isJoined) {
        let confirmationMessage = "\o/";
        (e || window.event).returnValue = confirmationMessage;
        return confirmationMessage;
      }
    });
  };

  isShowLive = () => {
    return this.isJoining || this.isJoined || this.isLeaving;
  }


}

