import * as TRTC from 'trtc-js-sdk/trtc.js';
import * as RTCDetect from 'rtc-detect';
import {DEVICE_KIND, DeviceInfo} from './interfaces';
import {readLocalStorage, writeLocalStorage} from "./util";
import ConnectDetector from "./view/check/connectDetector";
import CameraDetector from "./view/check/cameraDetector";
import MicrophoneDetector from "./view/check/microphoneDetector";
import SpeakerDetector from "./view/check/speakerDetector";
import NetworkDetector from "./view/check/networkDetector";
import CheckReport from "./view/check/checkReport";
import OlClassCtrl from './ctrl';

export default class OlClassCheckCtrl {

  detect: RTCDetect;
  deviceInfo: DeviceInfo;

  deviceKinds: string[];
  checkModal: boolean;
  rtcDetectModal: boolean;
  detectStage = 0;

  connectDetector: ConnectDetector;
  cameraDetector: CameraDetector;
  microphoneDetector: MicrophoneDetector;
  speakerDetector: SpeakerDetector;
  networkDetector: NetworkDetector;
  checkReport: CheckReport;

  constructor(readonly ctrl: OlClassCtrl) {
    // this.initDeviceInfo().then(() => {
    //   this.deviceKinds = Object.keys(this.ctrl.checkCtrl.deviceInfo.detects);
    //   this.connectDetector = new ConnectDetector(ctrl, this.startDeviceCheck);
    //   this.cameraDetector = new CameraDetector(ctrl, this.handleCompleted);
    //   this.microphoneDetector = new MicrophoneDetector(ctrl, this.handleCompleted);
    //   this.speakerDetector = new SpeakerDetector(ctrl, this.handleCompleted);
    //   this.networkDetector = new NetworkDetector(ctrl, this.handleCompleted);
    //   this.checkReport = new CheckReport(ctrl, this.handleReCheck, this.handleAllComplete);
    //   this.startCheck();
    //
    //   window.lichess.pubsub.emit('device_loaded');
    // });
  }

  initDeviceInfo = async () => {
    this.detect = new RTCDetect();
    this.deviceInfo = await this.detect.getReportAsync();
    this.deviceInfo.detects =
      {
        [DEVICE_KIND.CAMERA]: {
          name: DEVICE_KIND.CAMERA,
          icon: '摄',
          isDetect: true,
          deviceId: '',
          detect: {
            isConnect: false
          },
          check: {
            isActive: false,
            isComplete: false,
            isSuccess: false
          }
        },
        [DEVICE_KIND.MICROPHONE]: {
          name: DEVICE_KIND.MICROPHONE,
          icon: '录',
          isDetect: true,
          deviceId: '',
          detect: {
            isConnect: false
          },
          check: {
            isActive: false,
            isComplete: false,
            isSuccess: false
          }
        },
        [DEVICE_KIND.SPEAKER]: {
          name: DEVICE_KIND.SPEAKER,
          icon: '音',
          isDetect: true,
          deviceId: '',
          detect: {
            isConnect: false
          },
          check: {
            isActive: false,
            isComplete: false,
            isSuccess: false
          }
        },
        [DEVICE_KIND.NETWORK]: {
          name: DEVICE_KIND.NETWORK,
          icon: '网',
          isDetect: true,
          deviceId: '',
          detect: {
            isConnect: false
          },
          check: {
            isActive: false,
            isComplete: false,
            isSuccess: false
          }
        }
      };

    // iOS系统和firefox浏览器，不包含扬声器检测
    if (this.deviceInfo.system.browser.name === 'Firefox' || this.deviceInfo.system.OS === 'iOS') {
      this.deviceInfo.detects[DEVICE_KIND.SPEAKER].isDetect = false;
    }

  };

  startCheck = () => {
    if(!this.getCache()) {
      this.startTrtcDetect().then( (result) => {
        if(result) {
          this.startConnectCheck();
        }
      });
    }
  };

  startTrtcDetect = async () => {
    let checkResult = await TRTC.checkSystemRequirements();
    if (!checkResult.result) {
      console.error('checkResult', checkResult.result, 'checkDetail', checkResult.detail);
      this.rtcDetectModal = true;
      this.ctrl.redraw();
      return false;
    } return true;
  };

  startConnectCheck = () => {
    this.connectDetector.startConnectCheck();
    this.ctrl.checkCtrl.checkModal = true;
    this.ctrl.redraw();
  };

  startDeviceCheck = () => {
    this.setActive(DEVICE_KIND.CAMERA);
    this.setDetectStage(1);
    this.ctrl.redraw();
  };

  setDetectStage = (stage: number) => {
    this.detectStage = stage;
    this.ctrl.redraw();
  };

  // 重新检测
  handleReCheck = () => {
    this.setDetectStage(0);
    this.deviceKinds.forEach(key => {
      this.ctrl.checkCtrl.deviceInfo.detects[key].deviceId = '';
      this.ctrl.checkCtrl.deviceInfo.detects[key].detect.isConnect = false;
      this.ctrl.checkCtrl.deviceInfo.detects[key].check.isActive = false;
      this.ctrl.checkCtrl.deviceInfo.detects[key].check.isComplete = false;
      this.ctrl.checkCtrl.deviceInfo.detects[key].check.isSuccess = false;
    });
    this.connectDetector.handleReConnect()
  };

  // 关闭检测
  handleClose = () => {
    this.setDetectStage(0);
    this.connectDetector.handleReset();
    this.cameraDetector.handleClose();
    this.microphoneDetector.handleClose();
    this.speakerDetector.handleClose();
    this.ctrl.checkCtrl.checkModal = false;
    this.ctrl.redraw();
  };

  // 完成检测
  handleAllComplete = () => {
    this.handleClose();
    this.setCache();
  };

  // 点击切换step
  handleStep = (deviceKind) => {
    if (this.isComplete(deviceKind)) {
      this.setActive(deviceKind);
    }
  };

  // 处理step的完成事件
  handleCompleted = (deviceKind, deviceId, success: boolean) => {
    this.setCompleted(deviceKind, deviceId, success);
    let nextStepDevice = this.getNextStepDevice(deviceKind);
    if (nextStepDevice) {
      this.setActive(nextStepDevice);
    } else {
      this.setDetectStage(2);
    }
    this.ctrl.redraw();
  };

  getNextStepDevice = (deviceKind) => {
    let currIndex = this.deviceKinds.indexOf(deviceKind);
    let nextStepDevice;
    if (currIndex < this.deviceKinds.length - 1) {
      nextStepDevice = this.deviceKinds[currIndex + 1];
      if (!this.hasSpeakerDetect() && nextStepDevice === DEVICE_KIND.SPEAKER) {
        if (currIndex < this.deviceKinds.length - 2) {
          nextStepDevice = this.deviceKinds[currIndex + 2];
        } else {
          return null;
        }
      }
    }
    return nextStepDevice;
  };

  setActive = (deviceKind) => {
    this.deviceKinds.forEach(key => {
      this.ctrl.checkCtrl.deviceInfo.detects[key].check.isActive = key === deviceKind;
    });
  };

  setCompleted = (deviceKind, deviceId, success: boolean) => {
    this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].deviceId = deviceId;
    this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].check.isComplete = true;
    this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].check.isSuccess = success;
  };

  hasSpeakerDetect = () => this.ctrl.checkCtrl.deviceInfo.detects.speaker.isDetect;
  isComplete = (deviceKind) => this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].check.isComplete;
  isActive = (deviceKind) => this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].isDetect && this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].check.isActive;


  setCache = () => {
    let deviceInfo = this.ctrl.checkCtrl.deviceInfo;
    let key = `TRTC:${deviceInfo.system.OS}:${deviceInfo.system.browser.name}:${deviceInfo.system.browser.version}`;
    writeLocalStorage(key, {'trtcSupport': true, detects: this.ctrl.checkCtrl.deviceInfo.detects});
  };

  getCache = () => {
    let deviceInfo = this.ctrl.checkCtrl.deviceInfo;
    let key = `TRTC:${deviceInfo.system.OS}:${deviceInfo.system.browser.name}:${deviceInfo.system.browser.version}`;
    let result = readLocalStorage(key);
    if(!result.trtcSupport || Object.keys(result.detects).length === 0) {
      return false;
    } else {
      this.ctrl.checkCtrl.deviceInfo.detects = result.detects;
      return true;
    }
  }

}

