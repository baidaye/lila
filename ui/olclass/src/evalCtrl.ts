import {prop, Prop} from 'common';
import throttle from 'common/throttle';
import {storedProp, StoredBooleanProp} from 'common/storage';
import {CevalCtrl, NodeEvals, CevalOpts, ctrl as cevalCtrl, Work as CevalWork, isEvalBetter} from 'ceval';
import * as chessUtil from 'chess';
import {ops as treeOps} from 'tree';
import OlClassCtrl from './ctrl';
import {compute as computeAutoShapes} from "./autoShape";

export default class OlClassEvalCtrl {

  ceval: CevalCtrl;
  threatMode: Prop<boolean> = prop(false);
  showGauge: StoredBooleanProp = storedProp('show-gauge', true);
  showComputer: StoredBooleanProp = storedProp('show-computer', true);
  showAutoShapes: StoredBooleanProp = storedProp('show-auto-shapes', true);
  ongoing: boolean; // true if real game is ongoing

  constructor(readonly ctrl: OlClassCtrl, readonly trans: Trans) {
    this.initCeval();
  }

  private resetAutoShapes() {
    if (this.showAutoShapes()) this.setAutoShapes();
    else this.ctrl.groundCtrl.chessground && this.ctrl.groundCtrl.chessground.setAutoShapes([]);
  }

  toggleAutoShapes = (v: boolean): void => {
    this.showAutoShapes(v);
    this.resetAutoShapes();
  };

  toggleGauge = () => {
    this.showGauge(!this.showGauge());
  };

  onToggleComputer = () => {
    if (!this.showComputer()) {
      this.ctrl.groundCtrl.tree.removeComputerVariations();
      if (this.ceval.enabled()) this.toggleCeval();
      this.ctrl.groundCtrl.chessground && this.ctrl.groundCtrl.chessground.setAutoShapes([]);
    } else this.resetAutoShapes();
  };

  toggleComputer = () => {
    const value = !this.showComputer();
    this.showComputer(value);
    this.onToggleComputer();
  };

  private cevalReset(): void {
    this.ceval.stop();
    if (!this.ceval.enabled()) this.ceval.toggle();
    this.startCeval();
    this.ctrl.redraw();
  }

  cevalSetMultiPv = (v: number): void => {
    this.ceval.multiPv(v);
    this.ctrl.groundCtrl.tree.removeCeval();
    this.cevalReset();
  };

  cevalSetThreads = (v: number): void => {
    this.ceval.threads(v);
    this.cevalReset();
  };

  cevalSetHashSize = (v: number): void => {
    this.ceval.hashSize(v);
    this.cevalReset();
  };

  cevalSetInfinite = (v: boolean): void => {
    this.ceval.infinite(v);
    this.cevalReset();
  };

  getCeval = (): CevalCtrl => {
    return this.ceval;
  };

  nextNodeBest = (): string | undefined => {
    return treeOps.withMainlineChild(this.ctrl.groundCtrl.node, (n: Tree.Node) => n.eval ? n.eval.best : undefined);
  };

  disableThreatMode = (): boolean => {
    return false;
  };

  toggleThreatMode = () => {
    if (this.ctrl.groundCtrl.node.check) return;
    if (!this.ceval.enabled()) this.ceval.toggle();
    if (!this.ceval.enabled()) return;
    this.threatMode(!this.threatMode());
    this.setAutoShapes();
    this.startCeval();
    this.ctrl.redraw();
  };

  toggleCeval = () => {
    this.ceval.toggle();
    this.setAutoShapes();
    this.startCeval();
    if (!this.ceval.enabled()) {
      this.threatMode(false);
    }
    this.ctrl.redraw();
  };

  gameOver = (node?: Tree.Node): 'draw' | 'checkmate' | false => {
    const n = node || this.ctrl.groundCtrl.node;
    if (n.dests !== '' || n.drops) return false;
    if (n.check) return 'checkmate';
    return 'draw';
  };

  mandatoryCeval = (): boolean => {
    return false;
  };

  showEvalGauge = (): boolean => {
    return this.hasAnyComputerAnalysis() && this.showGauge() && !this.gameOver() && this.showComputer();
  };

  hasAnyComputerAnalysis = (): boolean => {
    return this.ctrl.groundCtrl.round.analysis ? true : this.ceval.enabled();
  };

  currentEvals = (): NodeEvals => {
    return {
      server: this.ctrl.groundCtrl.node.eval,
      client: this.ctrl.groundCtrl.node.ceval
    };
  };

  playUci = (uci: Uci): void => {
    const move = chessUtil.decomposeUci(uci);
    if (uci[1] === '@') this.ctrl.groundCtrl.chessground.newPiece({
      color: this.ctrl.groundCtrl.chessground.state.movable.color as Color,
      role: chessUtil.sanToRole[uci[0]]
    }, move[1]);
    else {
      const capture = this.ctrl.groundCtrl.chessground.state.pieces[move[1]];
      const promotion = move[2] && chessUtil.sanToRole[move[2].toUpperCase()];
      this.ctrl.groundCtrl.sendMove(move[0], move[1], capture, promotion);
    }
  };

  getOrientation = (): Color => {
    return this.ctrl.groundCtrl.bottomColor();
  };

  getNode = (): Tree.Node => {
    return this.ctrl.groundCtrl.node;
  };

  startCeval = throttle(800, () => {
    if (this.ceval.enabled() && !this.ctrl.groundCtrl.isSubSync()) {
      if (this.canUseCeval()) {
        this.ceval.start(this.ctrl.groundCtrl.path, this.ctrl.groundCtrl.nodeList, this.threatMode(), false);
      } else this.ceval.stop();
    }
  });

  canUseCeval = (): boolean => {
    return !this.gameOver() && !this.ctrl.groundCtrl.node.threefold;
  };

  initCeval = (): void => {
    if (this.ceval) this.ceval.destroy();
    const cfg: CevalOpts = {
      storageKeyPrefix: 'olclass',
      multiPvDefault: 1,
      variant: {
        short: 'Std',
        name: 'Standard',
        key: 'standard'
      },
      possible: true,
      emit: (ev: Tree.ClientEval, work: CevalWork) => {
        this.onNewCeval(ev, work.path, work.threatMode);
      },
      setAutoShapes: this.setAutoShapes,
      redraw: this.ctrl.redraw
    };
    this.ceval = cevalCtrl(cfg);
  };

  onNewCeval = (ev: Tree.ClientEval, path: Tree.Path, isThreat: boolean): void => {
    this.ctrl.groundCtrl.tree.updateAt(path, (node: Tree.Node) => {
      if (node.fen !== ev.fen && !isThreat) return;
      if (isThreat) {
        if (!node.threat || isEvalBetter(ev, node.threat) || node.threat.maxDepth < ev.maxDepth) {
          node.threat = ev;
        }
      } else if (isEvalBetter(ev, node.ceval)) {
        node.ceval = ev;
      } else if (node.ceval && ev.maxDepth > node.ceval.maxDepth) {
        node.ceval.maxDepth = ev.maxDepth;
      }

      if (path === this.ctrl.groundCtrl.path) {
        this.setAutoShapes();
        if (!isThreat) {
          if (ev.cloud && ev.depth >= this.ceval.effectiveMaxDepth()) {
            this.ceval.stop();
          }
        }
        this.ctrl.redraw();
      }
    });
  };

  setAutoShapes = (): void => {
    if(!this.ctrl.groundCtrl.isSubSync()) {
      this.ctrl.groundCtrl.withCg(cg => cg.setAutoShapes(computeAutoShapes(this.ctrl)));
    }
  };

}

