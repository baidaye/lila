import OlClassCtrl from './ctrl';

interface Handlers {
  [key: string]: any;
}

interface Req {
  [key: string]: any;
}

export interface Socket {
  send: SocketSend;
  receive(type: string, data: any): boolean;
  sendAnaMove(req: Req): void;
  sendAnaDrop(req: Req): void;
  sendAnaDests(req: Req): void;
}

export function make(send: SocketSend, ctrl: OlClassCtrl): Socket {

  let anaMoveTimeout;
  let anaDestsTimeout;

  const handlers: Handlers = {
    node(d) {
      clearTimeout(anaMoveTimeout);
      ctrl.groundCtrl.addNode(d.node, d.path);
    },
    stepFailure() {
      clearTimeout(anaMoveTimeout);
      ctrl.groundCtrl.reset();
    },
    dests(data) {
      clearTimeout(anaDestsTimeout);
      ctrl.groundCtrl.addDests(data.dests, data.path, data.opening);
    },
    destsFailure(data) {
      console.log(data);
      clearTimeout(anaDestsTimeout);
    },
    changeCourseWare(d) {
      ctrl.courseWareCtrl.onChangeCourseWare(d);
    },
    path(d) {
      ctrl.groundCtrl.onSendPath(d);
    },
    addNode(d) {
      ctrl.groundCtrl.onAddNode(d);
    },
    deleteNode(d) {
      ctrl.groundCtrl.onDeleteNode(d);
    },
    promote(d) {
      ctrl.groundCtrl.onPromote(d);
    },
    forceVariation(d) {
      ctrl.groundCtrl.onForceVariation(d);
    },
    shapes(d) {
      ctrl.groundCtrl.onShapes(d);
    },
    selectNode(d) {
      ctrl.groundCtrl.onSelectNode(d);
    },
    glyphs(d) {
      ctrl.groundCtrl.onGlyphs(d);
    },
    setComment(d) {
      ctrl.groundCtrl.onSetComment(d);
    },
    deleteComment(d) {
      ctrl.groundCtrl.onDeleteComment(d);
    },
    setTags(d) {
      ctrl.groundCtrl.onSetTags(d);
    },
    olcStarted(d) {
      ctrl.liveCtrl.onStarted(d);
    },
    olcStopped(d) {
      ctrl.liveCtrl.onStopped(d);
    },
    olcMemberQuit(userId) {
      console.log(userId, 'quit')
    },
    olcMuteAll(d) {
      ctrl.liveCtrl.onMuteAll(d);
    },
    olcAddSync(userId) {
      ctrl.liveCtrl.onAddSync(userId);
    },
    olcRemoveSync(userId) {
      ctrl.liveCtrl.onRemoveSync(userId);
    },
    olcAddHandUp(userId) {
      ctrl.liveCtrl.onAddHandUp(userId);
    },
    olcRemoveHandUp(userId) {
      ctrl.liveCtrl.onRemoveHandUp(userId);
    },
    olcAddSpeaker(userId) {
      ctrl.liveCtrl.onAddSpeaker(userId);
    },
    olcRemoveSpeaker(userId) {
      ctrl.liveCtrl.onRemoveSpeaker(userId);
    },
  };

  function sendAnaDests(req) {
    clearTimeout(anaDestsTimeout);
    addSyncData(req);
    send('anaDests', req);
    anaDestsTimeout = setTimeout(function() {
      sendAnaDests(req);
    }, 3000);
  }

  function sendAnaMove(req) {
    clearTimeout(anaMoveTimeout);
    addSyncData(req);
    send('anaMove', req);

    anaMoveTimeout = setTimeout(() => sendAnaMove(req), 3000);
  }

  function sendAnaDrop(req) {
    clearTimeout(anaMoveTimeout);
    addSyncData(req);
    send('anaDrop', req);
    anaMoveTimeout = setTimeout(() => sendAnaDrop(req), 3000);
  }

  function addSyncData(req) {
    if(ctrl.groundCtrl) {
      req.ch = ctrl.courseWareCtrl.selectedId;
      req.write = ctrl.groundCtrl.isWriting();
      req.sticky = ctrl.groundCtrl.isSticky();
    }
  }

  return {
    receive(type: string, data: any): boolean {
      const handler = handlers[type];
      if (handler) handler(data);
      return true;
    },
    sendAnaMove,
    sendAnaDrop,
    sendAnaDests,
    send
  };
}
