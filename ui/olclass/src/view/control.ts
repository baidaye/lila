import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import OlClassCtrl from '../ctrl';
import * as control from '../control';
import {bindMobileMousedown, onInsert} from '../util';

export function renderControls(ctrl: OlClassCtrl): VNode {
  return h('div.olclass__controls', {
    hook: onInsert(el => {
      bindMobileMousedown(el, e => {
        const action = dataAct(e);
        if (action === 'prev') control.prev(ctrl);
        else if (action === 'next') control.next(ctrl);
        else if (action === 'first') control.first(ctrl);
        else if (action === 'last') control.last(ctrl);
      }, ctrl.redraw);
    })
  }, [
    h('div.features', [
      h('button.fbt', {
        attrs: {
          title: '开局浏览器 & 终局数据库',
          'data-icon': ']'
        },
        class: {
          hidden: ctrl.groundCtrl.menuIsOpen || !ctrl.groundCtrl.explorer.allowed() || (!ctrl.liveCtrl.localIsCoach() && !ctrl.liveCtrl.isStopped()) || !ctrl.courseWareCtrl.isSelected(),
          active: ctrl.groundCtrl.explorer.enabled() && ctrl.groundCtrl.featureActive === 'explorer'
        },
        hook: onInsert(el => {
          bindMobileMousedown(el, () => {
            ctrl.groundCtrl.toggleExplorer();
          })
        })
      }),
      h('button.fbt', {
        attrs: {
          title: '翻转棋盘',
          'data-icon': 'B'
        },
        class: {
          hidden: ctrl.groundCtrl.menuIsOpen || !ctrl.courseWareCtrl.isSelected(),
          active: ctrl.groundCtrl.featureActive === 'flip'
        },
        hook: onInsert(el => {
          bindMobileMousedown(el, () => {
            ctrl.groundCtrl.flip();
          })
        })
      })
/*      h('button.fbt', {
        attrs: {
          title: '评注这步走法',
          'data-icon': 'c'
        },
        class: {
          hidden: ctrl.groundCtrl.menuIsOpen || !ctrl.groundCtrl.isWriting() || !ctrl.courseWareCtrl.isSelected(),
          active: ctrl.groundCtrl.featureActive === 'comments'
        },
        hook: onInsert(el => {
          bindMobileMousedown(el, () => {
            ctrl.groundCtrl.showComments(ctrl.groundCtrl.path);
          })
        })
      }),
      h('button.fbt.glyph-icon', {
        attrs: {
          title: '用图形标注'
        },
        class: {
          hidden: ctrl.groundCtrl.menuIsOpen || !ctrl.groundCtrl.isWriting() || !ctrl.courseWareCtrl.isSelected(),
          active: ctrl.groundCtrl.featureActive === 'glyphs'
        },
        hook: onInsert(el => {
          bindMobileMousedown(el, () => {
            ctrl.groundCtrl.showGlyphs(ctrl.groundCtrl.path);
          })
        })
      }),*/
    ]),
    h('div.jumps', [
      jumpButton('W', 'first'),
      jumpButton('Y', 'prev'),
      jumpButton('X', 'next'),
      jumpButton('V', 'last')
    ]),
    h('button.fbt', {
      class: { active: ctrl.groundCtrl.menuIsOpen },
      attrs: {
        title: '菜单',
        'data-act': 'menu',
        'data-icon': '['
      },
      hook: onInsert(el => {
        bindMobileMousedown(el, () => {
          ctrl.groundCtrl.toggleMenuBox();
        })
      })
    })
  ]);
}

function dataAct(e) {
  return e.target.getAttribute('data-act') || e.target.parentNode.getAttribute('data-act');
}

function jumpButton(icon, effect) {
  return h('button.fbt', {
    attrs: {
      'data-act': effect,
      'data-icon': icon
    }
  });
}
