import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {User, LiveConfig} from "../interfaces";
import OlClassCtrl from "../ctrl";

export default function (ctrl: OlClassCtrl, config: LiveConfig): VNode {
  let isVideoDisabled = isDisabled(config.remote, ctrl.liveCtrl.userIsCoach(config.userId), ctrl.liveCtrl.userIsSpeaker(config.userId), config.isJoined, config.hasVideo, false, config.mutedVideo);
  let isAudioDisabled = isDisabled(config.remote, ctrl.liveCtrl.userIsCoach(config.userId), ctrl.liveCtrl.userIsSpeaker(config.userId), config.isJoined, config.hasAudio, ctrl.opts.olClass.muteAll, config.mutedAudio);
  let isVideoMuted = isShowMuted(isVideoDisabled, config.mutedVideo, config.subscribedVideo);
  let isAudioMuted = isShowMuted(isAudioDisabled, config.mutedAudio, config.subscribedAudio);
  let isPubSync = ctrl.liveCtrl.userIsSync(config.userId);

  // console.log(
  //   'user: ', config.userId,
  //   'isVideoDisabled: ', isVideoDisabled, 'isAudioDisabled: ', isAudioDisabled,
  //   'isVideoMuted: ', isVideoMuted, 'isAudioMuted: ', isAudioMuted,
  //   'subscribedVideo: ', config.subscribedVideo, 'subscribedAudio: ', config.subscribedAudio
  // );
  return h(`div.olclass__video.v-${config.userId}`, {
    class: {
      host: config.roles.includes('host'),
      self: config.roles.includes('self'),
      speaker: config.roles.includes('speaker'),
      spectator: config.roles.includes('spectator')
    }
  }, [
    h('div.video', {
      attrs: {
        id: `video-${config.userId}`
      }
    }),
    notAvailable(config.isJoined, config.hasVideo, config.mutedVideo, config.subscribedVideo),
    h('div.video-bar.top', [
      config.user ? userLink(config.user, false, false) : h('span.user-link', config.userId),
      h('div.bar-items', [
        h('div.bar-item.camera', {
          attrs: { 'data-icon': '摄', 'title': '关闭摄像头' },
          class: { disabled: isVideoDisabled },
          hook: {
            postpatch: vnode => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                if(!isVideoDisabled) {
                  config.toggleMuteVideo(config.userId)
                }
              })
            }
          }
        }, [
          isVideoMuted ? h('div.muted', { attrs: { 'data-icon': 'k' } }) : null
        ]),
        h('div.bar-item.microphone', {
          attrs: { 'data-icon': '录', 'title': '关闭麦克风' },
          class: { disabled: isAudioDisabled },
          hook: {
            postpatch: vnode => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                if(!isAudioDisabled) {
                  config.toggleMuteAudio(config.userId)
                }
              })
            }
          }
        }, [
          !isAudioMuted ? h('div.volume', { attrs: {style: `height: ${config.audioVolume * 5}%`}},[
            h('i', {
              attrs: { 'data-icon': '录' }
            })
          ]) : null,
          isAudioMuted ? h('div.muted', { attrs: { 'data-icon': 'k' } }) : null
        ])
      ])
    ]),
    h('div.video-bar.bottom', [
      roleTags(ctrl, config),
      ctrl.liveCtrl.localIsCoach() && !config.roles.includes('host') ? h('div.bar-items', [
        h('div.bar-item', {
          attrs: { 'data-icon': '同', 'title': '同步局面' },
          class: {
            disabled: !ctrl.courseWareCtrl.isSelected(),
            active: isPubSync
          },
          hook: {
            postpatch: vnode => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                ctrl.liveCtrl.toggleSync(config.userId);
              })
            }
          }
        }),
        h('div.bar-item', {
          attrs: { 'data-icon': '言', 'title': '发言' },
          class: {
            disabled: !config.hasAudio,
            active: config.roles.includes('speaker')
          },
          hook: {
            postpatch: vnode => {
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                if(config.hasAudio) {
                  ctrl.liveCtrl.toggleSpeaker(config.userId);
                }
              })
            }
          }
        }),
      ]) : h('div.bar-items')
    ]),
    config.isHandUp ? h('span.handUp', { attrs: { 'data-icon': '2' } }) : null,
  ])
}

export function isShowMuted(disabled: boolean, muted: boolean, subscribed: boolean) {
  return disabled || muted || !subscribed;
}

export function isDisabled(remote: boolean, isCoach: boolean, isSpeaker: boolean, isJoined: boolean, hasVideo: boolean, muteAll: boolean, muted: boolean) {
  return !isJoined || !hasVideo || (muteAll && !isCoach && !isSpeaker) || (remote && muted);
}

// 本地没有进入课堂：还未进入课堂
// 对方没有摄像头/未打开摄像头(muted)：摄像头未打开
// 本地没有订阅：未订阅
function notAvailable(isJoined, hasVideo, videoMuted, videoSubscribed) {
  let message = '暂无';
  if(!isJoined) message = '未进入课堂';
  else if(videoMuted || !hasVideo) message = '摄像头未打开';
  else if(!videoSubscribed) message = '未订阅';

  let show = !isJoined || videoMuted || !videoSubscribed;
  return show ? h('div.video-notAvailable',[
    h('div.icon', {
      attrs: { 'data-icon': '摄' }
    }),
    h('div.text', message)
  ]) : null;
}

export function userLink(u: User, showHead: boolean = true, showTitle: boolean = true, mini: boolean = true): VNode {
  const baseUrl = $('body').data('asset-url');
  const head = u.head ? '/image/' + u.head : baseUrl + '/assets/images/head-default-64.png';
  return h('span.user-link', {
    class: { online: u.online, offline: !u.online }
  }, [
    showHead ? h('div.head-line', [
      h('img.head', { attrs: { src: head }}),
      h('i.line')
    ]) : null,
    (showTitle && u.title) ? h('span.title', u.title == 'BOT' ? { attrs: { 'data-bot': true } } : {}, u.title) : null,
    h('span.u_name', {class: { mini: mini }}, u.mark ? u.mark : u.name)
  ]);
}

export function roleTags(ctrl: OlClassCtrl, config: LiveConfig) {
  let roles = config.roles;
  return h('div.bar-tags',[
    roles.includes('host') ? h('div.tag', '教练') : null,
    !roles.includes('host') && roles.includes('self') ? h('div.tag', '自己') : null,
    ctrl.liveCtrl.userIsSync(config.userId) ? h('div.tag', '同步中') : null,
    roles.includes('speaker') ? h('div.tag', '发言者') : null,
  ]);
}
