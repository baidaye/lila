import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Chessground} from 'chessground';
import {Api as CgApi} from 'chessground/api';
import {Config as CgConfig} from 'chessground/config';
import * as cg from 'chessground/types';
import {DrawShape} from 'chessground/draw';
import resizeHandle from 'common/resize';
import OlClassCtrl from '../ctrl';

export function render(ctrl: OlClassCtrl): VNode {
  return h('div.cg-wrap', {
    hook: {
      insert: vnode => {
        ctrl.groundCtrl.chessground = Chessground((vnode.elm as HTMLElement), makeConfig(ctrl));
        ctrl.evalCtrl.setAutoShapes();
        if (ctrl.groundCtrl.node.shapes) {
          ctrl.groundCtrl.chessground.setShapes(ctrl.groundCtrl.node.shapes as DrawShape[]);
        }
      },
      destroy: _ => ctrl.groundCtrl.chessground.destroy()
    }
  });
}

export function promote(ground: CgApi, key: Key, role: cg.Role) {
  const pieces = {};
  const piece = ground.state.pieces[key];
  if (piece && piece.role == 'pawn') {
    pieces[key] = {
      color: piece.color,
      role,
      promoted: true
    };
    ground.setPieces(pieces);
  }
}

export function makeConfig(ctrl: OlClassCtrl): CgConfig {
  const pref = ctrl.opts.pref;
  const opts = ctrl.groundCtrl.makeCgOpts();
  const couldDraw = !window.lichess.hasTouchEvents;
  const config = {
    turnColor: opts.turnColor,
    fen: opts.fen,
    check: opts.check,
    lastMove: opts.lastMove,
    selected: opts.selected,
    orientation: opts.orientation,
    coordinates: pref.coords !== 0,
    addPieceZIndex: pref.is3d,
    viewOnly: opts.viewOnly,
    movable: {
      free: false,
      color: opts.movable!.color,
      dests: opts.movable!.dests,
      showDests: pref.destination,
      rookCastle: pref.rookCastle
    },
    events: {
      select: function () {
        //ctrl.groundCtrl.sendSync();
      },
      move: ctrl.groundCtrl.userMove,
      insert(elements) {
        resizeHandle(elements, ctrl.opts.pref.resizeHandle, ctrl.groundCtrl.node.ply);
      }
    },
    premovable: {
      enabled: false,
    },
    drawable: {
      enabled: couldDraw,
      eraseOnClick: couldDraw,
      onChange: function (shapes) {
        ctrl.groundCtrl.changeShape(shapes);
      }
    },
    highlight: {
      lastMove: pref.highlight,
      check: pref.highlight
    },
    animation: {
      duration: pref.animationDuration
    },
    disableContextMenu: true
  };
  return config;
}
