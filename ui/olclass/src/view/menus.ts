import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Hooks} from "snabbdom/hooks";
import OlClassCtrl from '../ctrl';
import {boolSetting} from "./boolSetting";
import * as ModalBuild from "./modal";
import {bind, gameRegex, spinner} from '../util';

export default function renderMenuBox(ctrl: OlClassCtrl): VNode | null {
  return ctrl.groundCtrl.menuIsOpen ? h('div.sub-box', [
    /*h('div.sub-box__tools', [
      h('a.button.button-empty.disabled', {
        attrs: {'data-icon': 'O', title: '加载局面'},
        hook: bind('click', () => {
          openModal(ctrl);
        })
      }, '加载局面'),
      h('a.button.button-empty', {
        attrs: {'data-icon': 'B', title: '翻转棋盘'},
        hook: bind('click', () => {
          ctrl.groundCtrl.flip();
        })
      }, '翻转棋盘')
    ]),*/
    boolSetting({
      name: '隐藏棋谱',
      id: 'tree',
      checked: (!ctrl.groundCtrl.showTree()),
      change(v) {
        ctrl.groundCtrl.showTree(!v)
      }
    }, ctrl.redraw),
    h('h2', '电脑分析'),
    h('div.sub-box__setting', [
      boolSetting({
        name: '启动',
        title: window.lichess.engineName,
        id: 'all',
        checked: ctrl.evalCtrl.showComputer(),
        disabled: false,
        change: ctrl.evalCtrl.toggleComputer
      }, ctrl.redraw),
    ].concat(
      ctrl.evalCtrl.showComputer() ? [
        boolSetting({
          name: '箭头指示最佳着',
          id: 'shapes',
          checked: ctrl.evalCtrl.showAutoShapes(),
          change: ctrl.evalCtrl.toggleAutoShapes
        }, ctrl.redraw),
        boolSetting({
          name: '局面指示器',
          id: 'gauge',
          checked: ctrl.evalCtrl.showGauge(),
          change: ctrl.evalCtrl.toggleGauge
        }, ctrl.redraw),
        boolSetting({
          name: '无限分析',
          title: '取消深度限制，您的电脑可能会发热。',
          id: 'infinite',
          checked: ctrl.evalCtrl.getCeval().infinite(),
          change: ctrl.evalCtrl.cevalSetInfinite
        }, ctrl.redraw),
        (id => {
          const max = 5;
          return h('div.setting', [
            h('label', { attrs: { 'for': id } }, '多线分析'),
            h('input#' + id, {
              attrs: {
                type: 'range',
                min: 1,
                max,
                step: 1
              },
              hook: rangeConfig(
                () => parseInt(ctrl.evalCtrl.getCeval()!.multiPv()),
                ctrl.evalCtrl.cevalSetMultiPv)
            }),
            h('div.range_value', ctrl.evalCtrl.getCeval().multiPv() + ' / ' + max)
          ]);
        })('olclass-multipv')
      ] : []
    ))
  ]) : null;
}


function rangeConfig(read: () => number, write: (value: number) => void): Hooks {
  return {
    insert: vnode => {
      const el = vnode.elm as HTMLInputElement;
      el.value = '' + read();
      el.addEventListener('input', _ => write(parseInt(el.value)));
      el.addEventListener('mouseout', _ => el.blur());
    }
  };
}

export function renderSituationModal(ctrl: OlClassCtrl): VNode {
  return ModalBuild.modal({
    onClose: function() {
      closeModal(ctrl);
    },
    class: 'situationModal',
    content: [
      h('h2', '加载局面'),
      h('div.modal-content-body', [
        h('div.tabs-horiz', [
          makeTab(ctrl, 'fen', 'FEN', '加载FEN'),
          makeTab(ctrl, 'pgn', 'PGN', '加载PGN'),
          makeTab(ctrl, 'game', 'URL', '加载对局URL')
        ]),
        h('div.tabs-content', [
          ctrl.groundCtrl.situationActiveTab === 'fen' ? fenContent(ctrl) : null,
          ctrl.groundCtrl.situationActiveTab === 'pgn' ? pgnContent(ctrl) : null,
          ctrl.groundCtrl.situationActiveTab === 'game' ? gameContent(ctrl) : null
        ])
      ])
    ]
  });
}

function makeTab(ctrl: OlClassCtrl, key: string, name: string, title: string) {
  return h('span.' + key, {
    class: { active: ctrl.groundCtrl.situationActiveTab === key },
    attrs: { title },
    hook: bind('click', () => {
      ctrl.groundCtrl.situationActiveTab = key;
      ctrl.redraw();
    })
  }, name);
}

function fenContent(ctrl: OlClassCtrl): VNode | undefined {
  if (ctrl.redirecting) return spinner();
  return h('div.content.fen', {
    class: { active: ctrl.groundCtrl.situationActiveTab === 'fen' }
  }, [
    h('div.pair', [
      h('input.field.situation-fen', {
        attrs: {
          value: ctrl.groundCtrl.node.fen
        }
      })
    ]),
    h('div.actions', [
      h('button.button.button-empty', {
        hook: bind('click', () => {
          closeModal(ctrl);
        })
      }, '取消'),
      h('button.button', {
        hook: bind('click', () => {
          let value = $('.situation-fen').val();
          if (value !== ctrl.groundCtrl.node.fen) {
            ctrl.groundCtrl.changeFen(value);
          } else {
            closeModal(ctrl);
          }
        })
      }, '确定')
    ])
  ]);
}

function pgnContent(ctrl: OlClassCtrl): VNode | undefined {
  if (ctrl.redirecting) return spinner();
  return h('div.content.pgn', {
    class: { active: ctrl.groundCtrl.situationActiveTab === 'pgn' }
  }, [
    h('div.pair', [
      h('textarea.field.situation-pgn', {
        attrs: { placeholder: '粘贴PGN文本' }
      })
    ]),
    h('div.actions', [
      h('button.button.button-empty', {
        hook: bind('click', () => {
          closeModal(ctrl);
        })
      }, '取消'),
      h('button.button', {
        hook: bind('click', () => {
          let value = $('.situation-pgn').val();
          ctrl.groundCtrl.changePgn(value);
        })
      }, '确定')
    ])
  ]);
}

function gameContent(ctrl: OlClassCtrl): VNode | undefined {
  if (ctrl.redirecting) return spinner();
  return h('div.content.game', {
    class: { active: ctrl.groundCtrl.situationActiveTab === 'game' }
  }, [
    h('div.pair', [
      h('input.field.situation-game', {
        attrs: { placeholder: '输入对局URL，如：https://haichess.com/6er8bfyz' }
      })
    ]),
    h('div.actions', [
      h('button.button.button-empty', {
        hook: bind('click', () => {
          closeModal(ctrl);
        })
      }, '取消'),
      h('button.button', {
        hook: bind('click', () => {
          let value = $('.situation-game').val();
          // @ts-ignore
          const [id, pov, ply] = Array.from(value.match(gameRegex) || []).slice(1);
          ctrl.groundCtrl.changeGame(id as string);
        })
      }, '确定')
    ])
  ]);
}
//
// function openModal(ctrl: OlClassCtrl) {
//   if(!ctrl.groundCtrl.isSubSync()) {
//     //ctrl.groundCtrl.situationModal = true;
//     //ctrl.redraw();
//   }
// }

function closeModal(ctrl: OlClassCtrl) {
  ctrl.groundCtrl.situationModal = false;
  ctrl.redraw();
}
