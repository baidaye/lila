import {h} from "snabbdom";
import renderVideo from "./video";
import OlClassCtrl from "../ctrl";

export function renderLives(ctrl: OlClassCtrl) {
  return ctrl.liveCtrl.localLiveCtrl.isShowLive() ? h('div.olclass__lives', ctrl.liveCtrl.lives().map(liveConfig => {
    return renderVideo(ctrl, liveConfig);
  })) : null;
}
