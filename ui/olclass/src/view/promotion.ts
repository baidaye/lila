import {h} from 'snabbdom'
import * as ground from './ground';
import * as util from 'chessground/util';
import {Role} from 'chessground/types';
import {JustCaptured} from '../interfaces';
import {bind, onInsert} from '../util';
import OlClassCtrl from '../ctrl';

interface Promoting {
  orig: Key;
  dest: Key;
  capture?: JustCaptured;
  callback: Callback
}

type Callback = (orig: Key, dest: Key, capture: JustCaptured | undefined, role: Role) => void;

let promoting: Promoting | undefined;

export function start(ctrl: OlClassCtrl, orig: Key, dest: Key, capture: JustCaptured | undefined, callback: Callback): boolean {
  let s = ctrl.groundCtrl.chessground.state;
  let piece = s.pieces[dest];
  if (piece && piece.role == 'pawn' && (
    (dest[1] == '8' && s.turnColor == 'black') ||
    (dest[1] == '1' && s.turnColor == 'white'))) {
    promoting = {
      orig,
      dest,
      capture,
      callback
    };
    ctrl.redraw();
    return true;
  }
  return false;
}

function finish(ctrl: OlClassCtrl, role) {
  if (promoting) {
    ground.promote(ctrl.groundCtrl.chessground, promoting.dest, role);
    if (promoting.callback) {
      promoting.callback(promoting.orig, promoting.dest, promoting.capture, role);
    }
  }
  promoting = undefined;
}

export function cancel(ctrl: OlClassCtrl) {
  if (promoting) {
    promoting = undefined;
    ctrl.groundCtrl.chessground.set(ctrl.groundCtrl.cgConfig);
    ctrl.redraw();
  }
}

function renderPromotion(ctrl: OlClassCtrl, dest: Key, pieces, color: Color, orientation: Color) {
  if (!promoting) return;

  let left = (8 - util.key2pos(dest)[0]) * 12.5;
  if (orientation === 'white') left = 87.5 - left;

  const vertical = color === orientation ? 'top' : 'bottom';

  return h('div#promotion-choice.' + vertical, {
    hook: onInsert(el => {
      el.addEventListener('click', _ => cancel(ctrl));
      el.oncontextmenu = () => false;
    })
  }, pieces.map(function (serverRole, i) {
    const top = (color === orientation ? i : 7 - i) * 12.5;
    return h('square', {
      attrs: {
        style: 'top:' + top + '%;left:' + left + '%'
      },
      hook: bind('click', e => {
        e.stopPropagation();
        finish(ctrl, serverRole);
      })
    }, [h('piece.' + serverRole + '.' + color)]);
  }));
}

export function view(ctrl: OlClassCtrl) {
  if (!promoting) return;
  let pieces = ['queen', 'knight', 'rook', 'bishop'];

  return renderPromotion(ctrl, promoting.dest, pieces,
    util.opposite(ctrl.groundCtrl.chessground.state.turnColor),
    ctrl.groundCtrl.chessground.state.orientation);
}
