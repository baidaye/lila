import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {bind, dataIcon, spinner} from "../util";
import OlClassCtrl from '../ctrl';

export default function (ctrl: OlClassCtrl): VNode {

  function update(vnode: VNode) {

    const newCount = ctrl.opts.courseWares.length,
      vData = vnode.data!.li!,
      el = vnode.elm as HTMLElement;

    vData.count = newCount;
    if (!window.lichess.hasTouchEvents && ctrl.liveCtrl.localIsCoach() && newCount > 1 && !vData.sortable) {
      const makeSortable = function() {
        vData.sortable = window['Sortable'].create(el, {
          draggable: '.draggable',
          scrollSpeed: 5,
          onSort() {
            ctrl.courseWareCtrl.sort(vData.sortable.toArray());
          }
        });
      };
      if (window['Sortable']) makeSortable();
      else window.lichess.loadScript('javascripts/vendor/Sortable.min.js').done(makeSortable);
    }
  }

  return h('div.olclass__side', [
    h('div.course-info', [
      h('div', [
        h('a.class-name', {attrs: { title: ctrl.opts.clazz.name, href : `/clazz/${ctrl.opts.clazz.id}` }}, ctrl.opts.clazz.name),
        h('span.course-name', ` - 第${ctrl.opts.course.index}节`),
      ]),
/*      h('div.live-status', [
        status(ctrl),
        h('span.clock', ctrl.liveCtrl.liveClock)
      ])*/
    ]),
    /*(ctrl.liveCtrl.localIsCoach() || (!ctrl.liveCtrl.localIsCoach() && ctrl.liveCtrl.isStopped())) ?*/
      (ctrl.courseWareCtrl.loading ? spinner() : (
        ctrl.opts.courseWares.length > 0 ? h('div.olclass__courseWare', {
          hook: {
            insert(vnode) {
              vnode.data!.li = {};
              update(vnode);
            },
            postpatch(old, vnode) {
              vnode.data!.li = old.data!.li;
              update(vnode);
            },
            destroy: vnode => {
              const sortable = vnode.data!.li!.sortable;
              if (sortable) sortable.destroy()
            }
          }
        }, courseWares(ctrl)) : h('div.olclass__courseWare.empty', '暂无课件')
      ))/* : h('div.olclass__courseWare.empty', '课件暂不可见')*/,
    ctrl.liveCtrl.localIsCoach() ?
      h('a.courseWare-add', {
        attrs: dataIcon('O'),
        hook: bind('click', () => {
          ctrl.courseWareCtrl.createFormModal = true;
          ctrl.redraw();
        })
      }, '添加章节') : null
  ]);
}

function courseWares(ctrl: OlClassCtrl) {
  return ctrl.opts.courseWares.map(courseWare => {
    return h(`div.cw-${courseWare.id}`, {
      key: courseWare.id,
      attrs: {'data-id': courseWare.id},
      class: {
        checked: (ctrl.courseWareCtrl.checked !== null && ctrl.courseWareCtrl.checked.id === courseWare.id),
        active: (ctrl.courseWareCtrl.selectedId === courseWare.id),
        draggable: ctrl.liveCtrl.localIsCoach()
      },
      hook: {
        insert(vnode) {
          let el = vnode.elm as HTMLElement;
          if(ctrl.opts.olClass.status === 'started') {
            $(el).off('click').on('click', () => {
              ctrl.courseWareCtrl.checkCourseWare(courseWare);
            });
            $(el).off('dblclick').on('dblclick', () => {
              ctrl.courseWareCtrl.setCourseWare(courseWare);
            });
          } else {
            $(el).off('click').on('click', () => {
              ctrl.courseWareCtrl.setCourseWare(courseWare, true);
            });
          }
        },
        postpatch: (_, vnode) => {
          let el = vnode.elm as HTMLElement;
          if(ctrl.opts.olClass.status === 'started') {
            $(el).off('click').on('click', () => {
              ctrl.courseWareCtrl.checkCourseWare(courseWare);
            });
            $(el).off('dblclick').on('dblclick', () => {
              ctrl.courseWareCtrl.setCourseWare(courseWare);
            });
          } else {
            $(el).off('click').on('click', () => {
              ctrl.courseWareCtrl.setCourseWare(courseWare, true);
            });
          }
        }
      }
    }, [
      h('span', courseWare.order.toFixed()),
      h('h3', courseWare.name),
      ctrl.liveCtrl.localIsCoach() && (ctrl.liveCtrl.isCreated() || ctrl.liveCtrl.isStarted()) ? h('act', {
        attrs: dataIcon('%'),
        hook: {
          insert(vnode) {
            $(vnode.elm as HTMLElement).on('click', (e) => {
              e.stopPropagation();
              if(ctrl.liveCtrl.isCreated() || ctrl.liveCtrl.isStarted()) {
                ctrl.courseWareCtrl.edit = courseWare;
                ctrl.courseWareCtrl.editFormModal = true;
                ctrl.redraw();
              }
            });
          },
          postpatch: (_, vnode) => {
            $(vnode.elm as HTMLElement).off('click').on('click', (e) => {
              e.stopPropagation();
              if(ctrl.liveCtrl.isCreated() || ctrl.liveCtrl.isStarted()) {
                ctrl.courseWareCtrl.edit = courseWare;
                ctrl.courseWareCtrl.editFormModal = true;
                ctrl.redraw();
              }
            });
          }
        },
      }) : null
    ]);
  });
}

// function status(ctrl: OlClassCtrl) {
//   switch (ctrl.opts.olClass.status) {
//     case 'created':
//       return '未开课';
//     case 'started':
//       return '上课中';
//     case 'stopped':
//       return '已结束';
//     default:
//       return '--';
//   }
// }
