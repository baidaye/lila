import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import OlClassCtrl from '../ctrl';
import {bind} from '../util';
import {isShowMuted, isDisabled} from "./video";

let localOldIsVideoDisabled;
let localOldIsAudioDisabled;

export function renderControls(ctrl: OlClassCtrl): VNode {
  return ctrl.liveCtrl.localIsCoach() ? coachControls(ctrl) : studentControls(ctrl)
}

function coachControls(ctrl: OlClassCtrl) {
  return h('div.controls', [
    checkButton(ctrl),
    cameraButton(ctrl),
    microphoneButton(ctrl),
    microphoneMultiButton(ctrl),
    syncButton(ctrl),
    ctrl.liveCtrl.isCreated() || ctrl.liveCtrl.isStopped() ? startButton(ctrl) : null,
    ctrl.liveCtrl.isStarted() ? stopButton(ctrl) : null,
  ]);
}

function studentControls(ctrl: OlClassCtrl) {
  return h('div.controls', [
    checkButton(ctrl),
    cameraButton(ctrl),
    microphoneButton(ctrl),
    handUpButton(ctrl),
    ctrl.liveCtrl.localLiveCtrl.isJoined ? leaveButton(ctrl) : joinButton(ctrl)
  ]);
}

function cameraButton(ctrl: OlClassCtrl) {
  let localConfig = ctrl.liveCtrl.localLiveCtrl.localStreamConfig;
  let hasVideo = localConfig ? localConfig.hasVideo : false;
  let videoMuted = localConfig ? localConfig.mutedVideo : true;
  let videoSubscribed = localConfig ? localConfig.subscribedAudio : true;

  let isVideoDisabled = isDisabled(false, ctrl.liveCtrl.localIsCoach(), ctrl.liveCtrl.localIsSpeaker(), ctrl.liveCtrl.localLiveCtrl.isJoined, hasVideo, false, videoMuted);
  let isVideoMuted = isShowMuted(isVideoDisabled, videoMuted, videoSubscribed);

  return h('div.radio-button-drop', [
    h('div.radio-button-wrap', [
      h('div.radio-button', {
        attrs: { 'data-icon': '摄', title: '关闭' },
        class: { disabled: isVideoDisabled },
        hook: {
          postpatch: vnode => {
            if(isVideoDisabled !== localOldIsVideoDisabled) {
              localOldIsVideoDisabled = isVideoDisabled;
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                if(!isVideoDisabled) {
                  ctrl.liveCtrl.localLiveCtrl.toggleMuteVideo();
                }
              })
            }
          }
        }
      }, [
        isVideoMuted ? h('div.muted', { attrs: { 'data-icon': 'k' } }) : null
      ]),
      h('div.label', '摄像头')
    ]),
    cameraDropdown(ctrl)
  ])
}

function microphoneButton(ctrl: OlClassCtrl) {
  let localConfig = ctrl.liveCtrl.localLiveCtrl.localStreamConfig;
  let hasAudio = localConfig ? localConfig.hasAudio : false;
  let audioMuted = localConfig ? localConfig.mutedAudio : true;
  let audioSubscribed = localConfig ? localConfig.subscribedAudio : true;

  let isAudioDisabled = isDisabled(false, ctrl.liveCtrl.localIsCoach(), ctrl.liveCtrl.localIsSpeaker(), ctrl.liveCtrl.localLiveCtrl.isJoined, hasAudio, ctrl.opts.olClass.muteAll, audioMuted);
  let isAudioMuted = isShowMuted(isAudioDisabled, audioMuted, audioSubscribed);

  return h('div.radio-button-drop', [
    h('div.radio-button-wrap', [
      h('div.radio-button', {
        attrs: { 'data-icon': '录', title: '关闭' },
        class: { disabled: isAudioDisabled },
        hook: {
          postpatch: vnode => {
            if(isAudioMuted !== localOldIsAudioDisabled || isAudioDisabled !== localOldIsAudioDisabled) {
              localOldIsAudioDisabled = isAudioMuted;
              $(vnode.elm as HTMLElement).off('click').on('click', () => {
                if(!isAudioDisabled) {
                  ctrl.liveCtrl.localLiveCtrl.toggleMuteAudio();
                }
              })
            }
          }
        }
      }, [
        isAudioMuted ? h('div.muted', { attrs: { 'data-icon': 'k' } }) : null
      ]),
      h('div.label', '麦克风')
    ]),
    microphoneDropdown(ctrl)
  ])
}

function microphoneMultiButton(ctrl: OlClassCtrl) {
  let disabled = !ctrl.liveCtrl.canMuteAll();
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '禄', title: ctrl.opts.olClass.muteAll ? '取消全员禁麦' : "全员禁麦" },
      class: { disabled },
      hook: bind('click', () => {
        ctrl.liveCtrl.muteAll()
      })
    }),
    h('div.label', '全员禁麦')
  ])
}

function syncButton(ctrl: OlClassCtrl) {
  let disabled = !ctrl.liveCtrl.canSync(ctrl.opts.userId);
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '同', title: ctrl.liveCtrl.localIsSync() ? '结束同步局面' : '开始同步局面' },
      class: { disabled },
      hook: {
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', () => {
            ctrl.liveCtrl.toggleSync(ctrl.opts.userId);
          })
        }
      }
    }),
    h('div.label', ctrl.liveCtrl.localIsSync() ? '结束同步' : '开始同步')
  ])
}

function startButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '1', title: '上课' },
      class: {
        disabled: !ctrl.liveCtrl.isCreated() || ctrl.liveCtrl.loadingDevice
      },
      hook: {
        insert: (vnode: VNode) => {
          $(vnode.elm as HTMLElement).on('click', async () => {
            if(confirm('确认开始上课？')) {
              await ctrl.liveCtrl.start();
            }
          })
        },
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            if(confirm('确认开始上课？')) {
              await ctrl.liveCtrl.start();
            }
          })
        }
      }
    }),
    h('div.label', '上课')
  ])
}

function stopButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '0', title: '下课' },
      class: {
        disabled: !ctrl.liveCtrl.isStarted() || ctrl.liveCtrl.loadingDevice
      },
      hook: {
        insert: (vnode: VNode) => {
          $(vnode.elm as HTMLElement).on('click', async () => {
            if(confirm('确认下课？')) {
              await ctrl.liveCtrl.stop();
            }
          })
        },
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            if(confirm('确认下课？')) {
              await ctrl.liveCtrl.stop();
            }
          })
        }
      }
    }),
    h('div.label', '下课')
  ])
}

function joinButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '1', title: '进入' },
      class: {
        disabled: !ctrl.liveCtrl.isStarted() || ctrl.liveCtrl.loadingDevice
      },
      hook: {
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            await ctrl.liveCtrl.join();
          })
        }
      }
    }),
    h('div.label', '进入')
  ])
}

function handUpButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '2', title: '举手' },
      class: {
        disabled: !ctrl.liveCtrl.canHandUp()
      },
      hook: {
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            await ctrl.liveCtrl.handUp();
          })
        }
      }
    }),
    h('div.label', '举手')
  ])
}

function leaveButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '0', title: '离开' },
      class: {
        disabled: !ctrl.liveCtrl.isStarted() || ctrl.liveCtrl.loadingDevice
      },
      hook: {
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            await ctrl.liveCtrl.leave()
          })
        }
      }
    }),
    h('div.label', '离开')
  ])
}

function cameraDropdown(ctrl: OlClassCtrl) {
  let devices: MediaDeviceInfo[] = [];
  if(!ctrl.liveCtrl.loadingDevice) {
    devices = ctrl.checkCtrl.deviceInfo.devices.cameras
  }

  return h('div.dropdown', [
    h('i.dropdown-button', {
      attrs: { 'data-icon': 'u' },
      hook: bind('click', () => {
        ctrl.liveCtrl.cameraDropdownShow = true;
        ctrl.redraw();
      })
    }),
    h('div.dropdown-content',{
      class: {
        none: !ctrl.liveCtrl.cameraDropdownShow
      },
      hook: {
        insert() {
          $('body').click(function (e) {
            if(!(e.target as HTMLElement).matches('.dropdown-button')) {
              ctrl.liveCtrl.cameraDropdownShow = false;
              ctrl.redraw();
            }
          });
        }
      },
    }, devices.map(device => {
      return h('div.dropdown-content-item', {
        hook: bind('click', () => {
          ctrl.liveCtrl.localLiveCtrl.switchVideo(device.deviceId);
        }),
        class: {
          active: device.deviceId === ctrl.liveCtrl.getCameraId()
        },
        attrs: {
          'title': device.label,
          'data-id': device.deviceId
        }
      }, device.label)
    }))
  ])

}

function microphoneDropdown(ctrl: OlClassCtrl) {
  let devices: MediaDeviceInfo[] = [];
  if(!ctrl.liveCtrl.loadingDevice) {
    devices = ctrl.checkCtrl.deviceInfo.devices.microphones
  }

  return h('div.dropdown', [
    h('i.dropdown-button', {
      attrs: { 'data-icon': 'u' },
      hook: bind('click', () => {
        ctrl.liveCtrl.microphoneDropdownShow = true;
        ctrl.redraw();
      })
    }),
    h('div.dropdown-content', {
      class: {
        none: !ctrl.liveCtrl.microphoneDropdownShow
      },
      hook: {
        insert() {
          $('body').click(function (e) {
            if(!(e.target as HTMLElement).matches('.dropdown-button')) {
              ctrl.liveCtrl.microphoneDropdownShow = false;
              ctrl.redraw();
            }
          });
        }
      },
    }, devices.map(device => {
      return h('div.dropdown-content-item', {
        hook: bind('click', () => {
          ctrl.liveCtrl.localLiveCtrl.switchAudio(device.deviceId);
        }),
        class: {
          active: device.deviceId === ctrl.liveCtrl.getMicrophoneId()
        },
        attrs: {
          'title': device.label,
          'data-id': device.deviceId
        }
      }, device.label)
    }))
  ])
}

function checkButton(ctrl: OlClassCtrl) {
  return h('div.radio-button-wrap', [
    h('div.radio-button', {
      attrs: { 'data-icon': '<', title: '设备检测' },
      class: {
        disabled: ctrl.liveCtrl.loadingDevice
      },
      hook: {
        insert: (vnode: VNode) => {
          $(vnode.elm as HTMLElement).on('click', async () => {
            ctrl.checkCtrl.startConnectCheck();
          })
        },
        postpatch: vnode => {
          $(vnode.elm as HTMLElement).off('click').on('click', async () => {
            ctrl.checkCtrl.startConnectCheck();
          })
        }
      }
    }),
    h('div.label', '检测')
  ])
}

