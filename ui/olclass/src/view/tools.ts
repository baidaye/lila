import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {view as cevalView} from 'ceval';
import {render as treeView} from './tree';
import OlClassCtrl from '../ctrl';
import explorerView from './explorer/explorerView';
import renderMenuBox from './menus';
import renderCommentsBox from './comment';
import renderGlyphsBox from './glyph';

export function renderTools(ctrl: OlClassCtrl): VNode {
  const showCeval = ctrl.evalCtrl.showComputer();
  return h('div.olclass__tools', [
    !ctrl.groundCtrl.isSubSync() && showCeval ? h('div.ceval-wrap',  [
      cevalView.renderCeval(ctrl.evalCtrl),
      cevalView.renderPvs(ctrl.evalCtrl)
    ]) : null,
    renderAreplay(ctrl),
    renderMenuBox(ctrl),
    explorerView(ctrl),
    renderCommentsBox(ctrl),
    renderGlyphsBox(ctrl)
  ])
}

function renderAreplay(ctrl: OlClassCtrl) {
  return h('div.olclass__moves.areplay', [
    renderOpeningBox(ctrl),
    treeView(ctrl)
  ]);
}

function renderOpeningBox(ctrl: OlClassCtrl) {
  let opening = ctrl.groundCtrl.tree.getOpening(ctrl.groundCtrl.nodeList);
  if (opening) return h('div.opening_box', {
    attrs: { title: opening.eco + ' ' + opening.name }
  }, [
    h('strong', opening.eco),
    ' ' + opening.name
  ]);
}


