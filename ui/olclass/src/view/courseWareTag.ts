import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { option, onInsert } from '../util';
import {CourseWare} from "../interfaces";
import OlClassCtrl from "../ctrl";

let selectedType: string;

function editable(ctrl: OlClassCtrl, name: string, value: string, n: boolean = false): VNode {
  return h('input', {
    key: selectedType, // force to redraw on change, to visibly update the input value
    attrs: {
      spellcheck: false,
      value
    },
    hook: onInsert<HTMLInputElement>(el => {
      el.onblur = function() {
        let nm = n ? selectedType : name;
        if(nm !== '- 请选择 -') {
          ctrl.groundCtrl.setTag(nm, el.value);
        }
      };
      el.onkeypress = function(e) {
        if ((e.keyCode || e.which) == 13) {
          el.blur();
        }
      }
    })
  });
}

function fixed(text: string) {
  return h('span', text);
}

type TagRow = (string | VNode)[];

function renderPgnTags(ctrl: OlClassCtrl, courseWare: CourseWare, types: string[]): VNode {
  let rows: TagRow[] = [];
  // 当前章节已经存在的标签
  rows = rows.concat(courseWare.tags.map(tag => {
    let label = getTagLabel(tag[0]);
    return [
      label ? label : tag[0],
      ctrl.groundCtrl.isWriting() ? editable(ctrl, tag[0], tag[1]) : fixed(tag[1])
    ]
  }));

  // 当前章节可以继续添加的标签
  if (ctrl.groundCtrl.isWriting()) {
    const existingTypes = courseWare.tags.map(t => t[0]);
    rows.push([
      h('select', {
        hook: {
          insert: vnode => {
            const el = vnode.elm as HTMLInputElement;
            selectedType = el.value;
            el.addEventListener('change', _ => {
              selectedType = el.value;
              $(el).parents('tr').find('input').focus();
            });
          },
          postpatch: (_, vnode) => {
            selectedType = (vnode.elm as HTMLInputElement).value;
          }
        }
      }, [
        h('option', '- 请选择 -'),
        ...types.map(t => {
          if (!existingTypes.includes(t)) {
            return option(t, '', getTagLabel(t));
          }
        })
      ]),
      editable(ctrl, selectedType, '', true)
    ]);
  }

  function getTagLabel(tag) {
    let tagMap = {
      'White': '白方',
      'WhiteElo': '白方等级分',
      'WhiteTitle': '白方头衔',
      'WhiteTeam': '白方团队',
      'Black': '黑方',
      'BlackElo': '黑方等级分',
      'BlackTitle': '黑方头衔',
      'BlackTeam': '黑方团队',
      'TimeControl': '对局时限',
      'Date': '日期',
      'Result': '对局结果',
      'Termination': '结束方式',
      'Site': '比赛地点',
      'Event': '赛事名称',
      'Round': '轮次',
      'Annotator': '评注者'
    };
    return tagMap[tag];
  }

  return h('table.olclass__tags.slist', h('tbody', rows.map(function(r) {
    return h('tr', {
      key: '' + r[0]
    }, [
      h('th', [r[0]]),
      h('td', [r[1]])
    ]);
  })));
}

export function view(ctrl: OlClassCtrl): VNode | undefined {
  let c = ctrl.courseWareCtrl.getCourseWare();
  if(c) {
    return h('div.' + c.id, [
      h('div', renderPgnTags(ctrl, c, ctrl.opts.tagTypes))
    ]);
  }
}
