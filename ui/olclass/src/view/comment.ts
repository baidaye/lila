import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import OlClassCtrl from '../ctrl';
import {bind, nodeFullName} from '../util';

export default function renderCommentsBox(ctrl: OlClassCtrl): VNode | undefined {
  if (ctrl.groundCtrl.menuIsOpen || ctrl.groundCtrl.featureActive !== 'comments') return;
  return h('section.comments-box.sub-box', [
    h('div.olclass__comments', [
      currentComments(ctrl, !ctrl.groundCtrl.isWriting()),
      commentForm(ctrl)
    ])
  ]);
}

export function renderUnderCommentsBox(ctrl: OlClassCtrl): VNode | undefined {
  return h('div.olclass__comments', [
    currentComments(ctrl, !ctrl.groundCtrl.isWriting()),
    ctrl.groundCtrl.isWriting() ? commentForm(ctrl) : null
  ]);
}

function authorDom(author) {
  if (!author) return 'Unknown';
  if (!author.name) return author;
  return h('span.user-link.ulpt', {
    attrs: { 'data-href': '/@/' + author.id }
  }, author.name);
}

function authorText(author): string {
  if (!author) return 'Unknown';
  if (!author.name) return author;
  return author.name;
}

function currentComments(ctrl: OlClassCtrl, includingMine: boolean): VNode | undefined {
  const node = ctrl.groundCtrl.node,
    comments = node.comments!;
  if (!comments || !comments.length) {
    return h('div.empty', '暂无评注说明.');
  }

  return h('div', comments.map((comment: Tree.Comment) => {
    const by: any = comment.by;
    const isMine = by.id && ctrl.opts.userId === by.id;
    if (!includingMine && isMine) return;
    const canDelete = isMine || ctrl.liveCtrl.localIsCoach();
    return h('div.olclass__comment.' + comment.id, [
      canDelete && ctrl.groundCtrl.isWriting() ? h('a.edit', {
        attrs: {
          'data-icon': 'q',
          title: '删除'
        },
        hook: bind('click', _ => {
          if (confirm('删除 ' + authorText(by) + ' 的评注?'))
            ctrl.groundCtrl.deleteComment(comment.id);
        }, ctrl.redraw)
      }) : null,
      authorDom(by),
      ...(node.san ? [
        ' on ',
        h('span.node', nodeFullName(node))
      ] : []),
      ': ',
      h('div.text', comment.text)
    ]);
  }));
}

function commentForm(ctrl: OlClassCtrl) {
  return h('form', [
    h('textarea#comment-text.form-control', {
      attrs: {
        style: 'height: 5em'
      },
      hook: {
        insert(vnode) {
          setupTextarea(ctrl, vnode);
          const el = vnode.elm as HTMLInputElement;
          function onChange() {
            setTimeout(function() {
              ctrl.groundCtrl.setComment(el.value);
            }, 100);
          }
          el.onkeyup = el.onpaste = onChange;
          el.focus();
          vnode.data!.hash = ctrl.courseWareCtrl.selectedId + ctrl.groundCtrl.path;
        },
        postpatch(old, vnode) {
          const newKey = ctrl.courseWareCtrl.selectedId + ctrl.groundCtrl.path;
          if (old.data!.path !== newKey) setupTextarea(ctrl, vnode);
          vnode.data!.path = newKey;
        }
      }
    })
  ])
}

function setupTextarea(ctrl: OlClassCtrl, vnode: VNode) {
  const el = vnode.elm as HTMLInputElement,
    mine = (ctrl.groundCtrl.node.comments || []).find(function(c: any) {
      return c.by && c.by.id && c.by.id === ctrl.opts.userId;
    });
  el.value = mine ? mine.text : '';
}
