import {h} from "snabbdom";
import {VNode} from "snabbdom/vnode";
import OlClassCtrl from "../ctrl";
import * as ModalBuild from "./modal";
import {bind, bindSubmit, spinner, uciToLastMove} from "../util";
import {Chessground} from "chessground";

export function renderCreateFormModal(ctrl: OlClassCtrl): VNode {
  let defaultName = `新章节`;
  return ModalBuild.modal({
    onClose: function() {
      ctrl.courseWareCtrl.closeTaskCreateModal();
    },
    class: `courseWareModal.${ctrl.courseWareCtrl.activeTab === 'situationdb' ? '.situation-selector' : ''}`,
    content: [
      h('h2', '添加章节'),
      h('div.modal-content-body', [
        h('form.form3.courseWareCreateForm', {
          hook: bindSubmit(_ => {
            ctrl.courseWareCtrl.create();
          })
        }, [
          ctrl.courseWareCtrl.checked ? h('input', { attrs: { type: 'hidden', name: 'prevOrder', value: ctrl.courseWareCtrl.checked.order } }) : null,
          h('div.form-group', [
            h('label.form-label', {
              attrs: { for: 'form-name' }
            }, '名称'),
            h('input#form-name.form-control', {
              attrs: {name: 'name', value: defaultName, required: true}
            })
          ]),
          h('div.tabs-horiz', [
            makeTab(ctrl, 'study', '研习', '研习'),
            makeTab(ctrl, 'homeworkRels', '课后练', '课后练'),
            makeTab(ctrl, 'contest', '比赛', '比赛'),
            makeTab(ctrl, 'gamedb', '对局库', '对局库'),
            makeTab(ctrl, 'situationdb', '局面库', '局面库'),
            makeTab(ctrl, 'puzzleRels', '战术题', '战术题'),
            makeTab(ctrl, 'editor', '编辑器', '编辑器'),
            makeTab(ctrl, 'game', '对局', '对局'),
            makeTab(ctrl, 'fen', 'FEN', 'FEN'),
            makeTab(ctrl, 'pgn', 'PGN', 'PGN')
          ]),
          h('div.tabs-content', {
            hook: {
              init() {
                ctrl.courseWareCtrl.onInitStudy();
                ctrl.courseWareCtrl.onInitContest();
                ctrl.courseWareCtrl.onInitSituation();
                ctrl.courseWareCtrl.onInitPuzzleRels();
              }
            }
          },[
            studyContent(ctrl),
            homeworkRelsContent(ctrl),
            contestContent(ctrl),
            gamedbContent(ctrl),
            situationdbContent(ctrl),
            puzzleRelsContent(ctrl),
            editorContent(ctrl),
            gameContent(ctrl),
            fenContent(ctrl),
            pgnContent(ctrl)
          ]),
          h('div.form-actions', {
            class: {
              single: !(ctrl.courseWareCtrl.activeTab === 'puzzleRels' && ctrl.courseWareCtrl.puzzleRelTab === 'homework')
            }
          }, [
            ctrl.courseWareCtrl.activeTab === 'puzzleRels' && ctrl.courseWareCtrl.puzzleRelTab === 'homework' ? h('div.homework-tags', [
              h('span.homework-tag.completeRate'), '- 完成率 ，',
              h('span.homework-tag.rightRate'), '- 正确率 ，',
              h('span.homework-tag.firstMoveRightRate'), '- 首次正确率 ',
            ]) : null,
            h('button.button', {
              attrs: { type: 'submit' },
            }, '保存章节')
          ])
        ])
      ])
    ]
  });
}

function makeTab(ctrl: OlClassCtrl, key: string, name: string, title: string) {
  return h('span.' + key, {
    class: { active: ctrl.courseWareCtrl.activeTab === key },
    attrs: { title },
    hook: bind('click', () => {
      ctrl.courseWareCtrl.activeTab = key;
      ctrl.redraw();
    })
  }, name);
}

function makeCapsuleTab(ctrl: OlClassCtrl, key: string, name: string, title: string) {
  return h('span.' + key, {
    class: { active: ctrl.courseWareCtrl.capsuleQuery.tab === key },
    attrs: { title },
    hook: bind('click', () => {
      ctrl.courseWareCtrl.capsuleQuery.tab = key;
      ctrl.courseWareCtrl.capsuleTab = 'capsule';
      ctrl.courseWareCtrl.filterCapsule();
      ctrl.redraw();
    })
  }, name);
}

const studyChannels = [{ key: 'mine', name: '我的研习' }, { key: 'member', name: '参与研习' }, { key: 'favorites', name: '收藏研习' }, { key: 'all', name: '所有研习' }];
function studyContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'study' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'study' } }),
    h('div.study', {
      class: {
        none: ctrl.courseWareCtrl.studyTab !== 'study'
      }
    }, [
      h('div.study-content', [
        h('div.study-tab', [
          h('div.study-tab-header', studyChannels.map(function (channel) {
            return h('a.study-tab-item', {attrs: { 'data-id': channel.key }, class: { active: ctrl.courseWareCtrl.studyChannel === channel.key }, hook: {
                insert(vnode) {
                  $(vnode.elm as HTMLElement).on('click', () => {
                    ctrl.courseWareCtrl.onStudyTabChange(channel.key);
                  });
                },
                postpatch: (_, vnode) => {
                  $(vnode.elm as HTMLElement).off('click').on('click', () => {
                    ctrl.courseWareCtrl.onStudyTabChange(channel.key);
                  });
                }
              }}, channel.name)
          })),
          h('div.study-tab-content', [
            h('div.study-search', [
              h('div.study-search-term', [
                h('input.study-search-input', {attrs: {placeholder: '研习名称/章节名称/关键词'}}),
                h('a.button.button-empty small', {
                  hook: bind('click', () => {
                    ctrl.courseWareCtrl.studyTagData = {
                      emptyTag: 'off',
                      tags: []
                    };
                    ctrl.courseWareCtrl.loadStudy();
                  })
                }, '搜索')
              ]),
              (ctrl.courseWareCtrl.studyChannel === 'mine' || ctrl.courseWareCtrl.studyChannel === 'favorites') ? h('div.study-search-tags.tag-groups', [
                h('div.tag-group.emptyTag', [
                  h('span', [
                    h(`input.${ctrl.courseWareCtrl.studyTagData.emptyTag}`, {attrs: {type: 'checkbox', id: `emptyTag`, name: `emptyTag`, checked: ctrl.courseWareCtrl.studyTagData.emptyTag === 'on'}, hook: {
                        insert(vnode) {
                          $(vnode.elm as HTMLElement).on('click', (e) => {
                            ctrl.courseWareCtrl.onStudyEmptyTagCheck((e.target as HTMLInputElement).checked);
                          });
                        },
                        postpatch: (_, vnode) => {
                          $(vnode.elm as HTMLElement).off('click').on('click', (e) => {
                            ctrl.courseWareCtrl.onStudyEmptyTagCheck((e.target as HTMLInputElement).checked);
                          });
                        }
                      }}),
                    h('label', {attrs: {for: `emptyTag`}}, `无标签`)
                  ])
                ]),
                h('div.tag-group.otherTag', ctrl.courseWareCtrl.studyTags.map(function (tag, i) {
                  let tags = ctrl.courseWareCtrl.studyTagData.tags;
                  // @ts-ignore
                  let checked = tags.includes(tag);
                  return h('span', [
                    h(`input.${tags}`, {attrs: {type: 'checkbox', id: `tags-${tag}`, name: `tags[${i}]`, checked: checked}, hook: {
                        insert(vnode) {
                          $(vnode.elm as HTMLElement).on('change', (e) => {
                            ctrl.courseWareCtrl.onStudyTagCheck(tag, (e.target as HTMLInputElement).checked);
                          });
                        },
                        postpatch: (_, vnode) => {
                          $(vnode.elm as HTMLElement).off('change').on('click', (e) => {
                            ctrl.courseWareCtrl.onStudyTagCheck(tag, (e.target as HTMLInputElement).checked);
                          });
                        }
                      }}),
                    h('label', {attrs: {for: `tags-${tag}`}}, tag)
                  ])
                }))
              ]) : null
            ]),
            h('div.scroll', [
              ctrl.courseWareCtrl.studyLoading ? spinner() :
                h('table.slist.study-list', [
                  h('tbody', ctrl.courseWareCtrl.studyPaginator.currentPageResults.length > 0 ?
                    ctrl.courseWareCtrl.studyPaginator.currentPageResults.map(data => {
                      return h('tr', [
                        h('td', { attrs: {'data-id': data.id } }, [
                          h('a', {
                            hook: {
                              insert(vnode) {
                                $(vnode.elm as HTMLElement).on('click', () => {
                                  ctrl.courseWareCtrl.currentStudy = data;
                                  ctrl.courseWareCtrl.studyTab = 'chapter';
                                  ctrl.courseWareCtrl.loadChapter();
                                });
                              },
                              postpatch: (_, vnode) => {
                                $(vnode.elm as HTMLElement).off('click').on('click', () => {
                                  ctrl.courseWareCtrl.currentStudy = data;
                                  ctrl.courseWareCtrl.studyTab = 'chapter';
                                  ctrl.courseWareCtrl.loadChapter();
                                });
                              }
                            },
                          }, data.name)
                        ]),
                        ctrl.courseWareCtrl.studyChannel !== 'mine' ? h('td', [
                          h('span.user-link.ulpt', { attrs: {'data-href': `/@/${data.owner.name}` } }, [
                            data.owner.title ? h('span.title', data.owner.title) : null,
                            h('span.u_name', data.owner.name)
                          ])
                        ]) : null,
                        h('td', data.visibility.name)
                      ]);
                    }) : [h('tr', [h('td', '没有更多了.')])])
                ])
            ])
          ])
        ])
      ]),
      h('div.pager', [
        h('a.fbt prev', {
          attrs: {
            'data-icon': 'Y'
          },
          class: {
            disabled: ctrl.courseWareCtrl.studyLoading || ctrl.courseWareCtrl.studyPaginator.currentPage <= 1
          },
          hook: bind('click', () => {
            if(!ctrl.courseWareCtrl.studyLoading && ctrl.courseWareCtrl.studyPaginator.currentPage > 1) {
              ctrl.courseWareCtrl.loadStudy(ctrl.courseWareCtrl.studyPaginator.previousPage);
            }
          })
        }, '上一页'),
        h('a.fbt next', {
          attrs: {
            'data-icon': 'X'
          },
          class: {
            disabled: ctrl.courseWareCtrl.studyLoading || ctrl.courseWareCtrl.studyPaginator.currentPage >= ctrl.courseWareCtrl.studyPaginator.nbPages
          },
          hook: bind('click', () => {
            if(!ctrl.courseWareCtrl.studyLoading && ctrl.courseWareCtrl.studyPaginator.currentPage < ctrl.courseWareCtrl.studyPaginator.nbPages) {
              ctrl.courseWareCtrl.loadStudy(ctrl.courseWareCtrl.studyPaginator.nextPage);
            }
          })
        }, '下一页')
      ])
    ]),
    h('div.chapter', {
      class: {
        none: ctrl.courseWareCtrl.studyTab !== 'chapter'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backStudy', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.studyTab = 'study';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentStudy ? ctrl.courseWareCtrl.currentStudy.name : '- 无 -')
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.chapterLoading ? spinner () : h('ul.ul-list.chapter-list', ctrl.courseWareCtrl.chapters.length > 0 ?
          ctrl.courseWareCtrl.chapters.map((item, i) => {
            return h('li', { attrs: {'data-id': item.id } }, [
              item.checked ? h('input', { attrs: { type: 'hidden', name: `study[${i}].name`, value: item.name  } }) : null,
              item.checked ? h('input', { attrs: { type: 'hidden', name: `study[${i}].setup`, value: item.id  } }) : null,
              h('span', [
                h('input', {
                  attrs: {type: 'checkbox', id: `rd-chapter-${item.id}`, name: `study[${i}].content`, value: item.id },
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).on('change', () => {
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).off('change').on('change', () => {
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    }
                  },
                }),
                h('label', {attrs: {for: `rd-chapter-${item.id}`}}, ` ${item.name}`)
              ]),
              h('a.viewPGN', {
                hook: bind('click', () => {
                  ctrl.courseWareCtrl.currentChapter = item;
                  ctrl.courseWareCtrl.currentPgn = item.pgn;
                  ctrl.courseWareCtrl.studyTab = 'pgn';
                  ctrl.redraw();
                })
              }, 'PGN')
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.chapter-pgn', {
      class: {
        none: ctrl.courseWareCtrl.studyTab !== 'pgn'
      }
    }, [
      h('div.item-nav', [
      '回到：',
        h('a.backStudy', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.studyTab = 'study';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentStudy ? ctrl.courseWareCtrl.currentStudy.name : '- 无 -'),
        ' / ',
        h('a.backChapter', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.studyTab = 'chapter';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentChapter ? ctrl.courseWareCtrl.currentChapter.name : '- 无 -')
      ]),
      h('div.scroll', [
        h('div.pgn', ctrl.courseWareCtrl.currentPgn ? ctrl.courseWareCtrl.currentPgn : '- 无 -')
      ])
    ])
  ]) : null;
}

function homeworkRelsContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'homeworkRels' ? h('div.content.active', [
    h('div.homework', {
      class: {
        none: ctrl.courseWareCtrl.homeworkRelTab !== 'homework'
      }
    }, [
      h('div.search_form', [
        h('table', [
          h('tbody', [
            h('tr', [
              h('td', [
                h('div.search', [
                  h('input.homework-rel-search-input', { attrs: { placeholder: '搜索' } }),
                  h('a.button.button-empty small', {
                    hook: bind('click', () => {
                      ctrl.courseWareCtrl.filterRelHomework();
                    })
                  }, '搜索')
                ])
              ])
            ])
          ])
        ])
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.homeworksLoading ? spinner () : h('ul.ul-list.homework-list', ctrl.courseWareCtrl.homeworks.length > 0 ?
          ctrl.courseWareCtrl.filteredRelHomeworks.map(item => {
            return h('li', { attrs: {'data-id': item.id } }, [
              h('a', {
                hook: {
                  insert(vnode) {
                    $(vnode.elm as HTMLElement).on('click', () => {
                      ctrl.courseWareCtrl.currentRelHomework = item;
                      ctrl.courseWareCtrl.homeworkRelTab = 'other';
                      ctrl.courseWareCtrl.loadHomework();
                    });
                  },
                  postpatch: (_, vnode) => {
                    $(vnode.elm as HTMLElement).off('click').on('click', () => {
                      ctrl.courseWareCtrl.currentRelHomework = item;
                      ctrl.courseWareCtrl.homeworkRelTab = 'other';
                      ctrl.courseWareCtrl.loadHomework();
                    });
                  }
                }
              }, `第${item.index}节`),
              h('span', item.dateTime)
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.homework-other', {
      class: {
        none: ctrl.courseWareCtrl.homeworkRelTab !== 'other'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backHomework', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.homeworkRelTab = 'homework';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentRelHomework ? `第${ctrl.courseWareCtrl.currentRelHomework.index}节` : '- 无 -')
      ]),
      h('div.homeworkRel-radios', [
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'homeworkRel', id: 'homework-rel-rd-replay', checked: ctrl.courseWareCtrl.homeworkRelContentTab === 'replay'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.homeworkRelContentTab = 'replay';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'homework-rel-rd-replay' }}, '打谱')
        ]),
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'homeworkRel', id: 'homework-rel-rd-recall', checked: ctrl.courseWareCtrl.homeworkRelContentTab === 'recall'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.homeworkRelContentTab = 'recall';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'homework-rel-rd-recall' }}, '记谱')
        ]),
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'homeworkRel', id: 'homework-rel-rd-distinguish', checked: ctrl.courseWareCtrl.homeworkRelContentTab === 'distinguish'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.homeworkRelContentTab = 'distinguish';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'homework-rel-rd-distinguish' }}, '棋谱记录')
        ])
      ]),
      ctrl.courseWareCtrl.homeworkRelContentTab === 'replay' ? h('div', [
        h('input', { attrs: { type: 'hidden', name: 'source', value: 'homeworkReplayGame' } }),
        h('div.scroll', [
          ctrl.courseWareCtrl.homeworkLoading ? spinner() : ctrl.courseWareCtrl.homework.replayGames.length > 0 ?
            h('div.puzzles', ctrl.courseWareCtrl.homework.replayGames.map((replayGame, i) => {
              return h('a', {
                class: {
                  selected: replayGame.checked
                }
              },[
                replayGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkReplayGame[${i}].name`, value: `第${ctrl.courseWareCtrl.currentRelHomework.index}节#${replayGame.name}` } }) : null,
                replayGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkReplayGame[${i}].setup`, value: `${ctrl.courseWareCtrl.currentRelHomework.id}:replayGame:${i}`  } }) : null,
                replayGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkReplayGame[${i}].content`, value: replayGame.pgn  } }) : null,
                h('div.mini-board.cg-wrap.is2d', {
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLElement;
                      Chessground(el, {
                        coordinates: false,
                        drawable: { enabled: false, visible: false },
                        resizable: false,
                        viewOnly: true,
                        orientation: 'white',
                        fen: replayGame.fen
                      });

                      $(el).off('click').on('click', () => {
                        replayGame.checked = !replayGame.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLElement;
                      $(el).off('click').on('click', () => {
                        replayGame.checked = !replayGame.checked;
                        ctrl.redraw();
                      });
                    }
                  }
                }),
                h('div.btm', replayGame.name)
              ]);
            })) : h('div.empty', '没有更多了.')
        ])
      ]): null,
      ctrl.courseWareCtrl.homeworkRelContentTab === 'recall' ? h('div', [
        h('input', { attrs: { type: 'hidden', name: 'source', value: 'homeworkRecallGame' } }),
        h('div.scroll', [
          ctrl.courseWareCtrl.homeworkLoading ? spinner() : ctrl.courseWareCtrl.homework.recallGames.length > 0 ?
            h('div.puzzles', ctrl.courseWareCtrl.homework.recallGames.map((recallGame, i) => {
              return h('a', {
                class: {
                  selected: recallGame.checked
                }
              },[
                recallGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkRecallGame[${i}].name`, value: `第${ctrl.courseWareCtrl.currentRelHomework.index}节#${recallGame.name}` } }) : null,
                recallGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkRecallGame[${i}].setup`, value: `${ctrl.courseWareCtrl.currentRelHomework.id}:recallGame:${i}`  } }) : null,
                recallGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkRecallGame[${i}].content`, value: recallGame.pgn  } }) : null,
                h('div.mini-board.cg-wrap.is2d', {
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLElement;
                      Chessground(el, {
                        coordinates: false,
                        drawable: { enabled: false, visible: false },
                        resizable: false,
                        viewOnly: true,
                        orientation: 'white',
                        fen: recallGame.fen
                      });

                      $(el).off('click').on('click', () => {
                        recallGame.checked = !recallGame.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLElement;
                      $(el).off('click').on('click', () => {
                        recallGame.checked = !recallGame.checked;
                        ctrl.redraw();
                      });
                    }
                  }
                }),
                h('div.btm', recallGame.name)
              ]);
            })) : h('div.empty', '没有更多了.')
        ])
      ]): null,
      ctrl.courseWareCtrl.homeworkRelContentTab === 'distinguish' ? h('div', [
        h('input', { attrs: { type: 'hidden', name: 'source', value: 'homeworkDistinguishGame' } }),
        h('div.scroll', [
          ctrl.courseWareCtrl.homeworkLoading ? spinner() : ctrl.courseWareCtrl.homework.distinguishGames.length > 0 ?
            h('div.puzzles', ctrl.courseWareCtrl.homework.distinguishGames.map((distinguishGame, i) => {
              return h('a', {
                class: {
                  selected: distinguishGame.checked
                }
              },[
                distinguishGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkDistinguishGame[${i}].name`, value: `第${ctrl.courseWareCtrl.currentRelHomework.index}节#${distinguishGame.name}` } }) : null,
                distinguishGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkDistinguishGame[${i}].setup`, value: `${ctrl.courseWareCtrl.currentRelHomework.id}:distinguishGame:${i}`  } }) : null,
                distinguishGame.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkDistinguishGame[${i}].content`, value: distinguishGame.pgn  } }) : null,
                h('div.mini-board.cg-wrap.is2d', {
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLElement;
                      Chessground(el, {
                        coordinates: false,
                        drawable: { enabled: false, visible: false },
                        resizable: false,
                        viewOnly: true,
                        orientation: 'white',
                        fen: distinguishGame.fen
                      });

                      $(el).off('click').on('click', () => {
                        distinguishGame.checked = !distinguishGame.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLElement;
                      $(el).off('click').on('click', () => {
                        distinguishGame.checked = !distinguishGame.checked;
                        ctrl.redraw();
                      });
                    }
                  }
                }),
                h('div.btm', distinguishGame.name)
              ]);
            })) : h('div.empty', '没有更多了.')
        ])
      ]) : null
    ])
  ]) : null
}

function contestContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'contest' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'contest' } }),
    h('div.contest', {
      class: {
        none: ctrl.courseWareCtrl.contestTab !== 'contest'
      }
    }, [
      h('div.contest-content', [
        h('div.search', [
          h('input.contest-search-input', { attrs: { placeholder: '比赛名称' } }),
          h('a.button.button-empty small', {
            hook: bind('click', () => {
              ctrl.courseWareCtrl.loadContest();
            })
          }, '搜索')
        ]),
        h('div.scroll', [
          ctrl.courseWareCtrl.contestLoading ? spinner() :
            h('ul.ul-list.contest-list', ctrl.courseWareCtrl.contestPaginator.currentPageResults.length > 0 ?
              ctrl.courseWareCtrl.contestPaginator.currentPageResults.map(item => {
                return h('li', { attrs: {'data-id': item.id } }, [
                  h('a', {
                    hook: {
                      insert(vnode) {
                        $(vnode.elm as HTMLElement).on('click', () => {
                          ctrl.courseWareCtrl.currentContest = item;
                          ctrl.courseWareCtrl.contestTab = 'round';
                          ctrl.courseWareCtrl.loadRound();
                        });
                      },
                      postpatch: (_, vnode) => {
                        $(vnode.elm as HTMLElement).off('click').on('click', () => {
                          ctrl.courseWareCtrl.currentContest = item;
                          ctrl.courseWareCtrl.contestTab = 'round';
                          ctrl.courseWareCtrl.loadRound();
                        });
                      }
                    },
                  }, `${item.name}, ${item.startsAt}`)
                ]);
              }) : [h('li', '没有更多了.')])
        ])
      ]),
      h('div.pager', [
        h('a.fbt prev', {
          attrs: {
            'data-icon': 'Y'
          },
          class: {
            disabled: ctrl.courseWareCtrl.contestLoading || ctrl.courseWareCtrl.contestPaginator.currentPage <= 1
          },
          hook: bind('click', () => {
            if(!ctrl.courseWareCtrl.contestLoading && ctrl.courseWareCtrl.contestPaginator.currentPage > 1) {
              ctrl.courseWareCtrl.loadContest(ctrl.courseWareCtrl.contestPaginator.previousPage);
            }
          })
        }, '上一页'),
        h('a.fbt next', {
          attrs: {
            'data-icon': 'X'
          },
          class: {
            disabled: ctrl.courseWareCtrl.contestLoading || ctrl.courseWareCtrl.contestPaginator.currentPage >= ctrl.courseWareCtrl.contestPaginator.nbPages
          },
          hook: bind('click', () => {
            if(!ctrl.courseWareCtrl.contestLoading && ctrl.courseWareCtrl.contestPaginator.currentPage < ctrl.courseWareCtrl.contestPaginator.nbPages) {
              ctrl.courseWareCtrl.loadContest(ctrl.courseWareCtrl.contestPaginator.nextPage);
            }
          })
        }, '下一页')
      ])
    ]),
    h('div.contest-chapter', {
      class: {
        none: ctrl.courseWareCtrl.contestTab !== 'round'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backContest', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.contestTab = 'contest';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentContest ? ctrl.courseWareCtrl.currentContest.name : '- 无 -')
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.roundLoading ? spinner() : h('ul.ul-list.round-list', ctrl.courseWareCtrl.rounds.length > 0 ?
          ctrl.courseWareCtrl.rounds.map(item => {
            return h('li', { attrs: {'data-id': item.id } }, [
              h('a', {
                hook: {
                  insert(vnode) {
                    $(vnode.elm as HTMLElement).on('click', () => {
                      ctrl.courseWareCtrl.currentRound = item;
                      ctrl.courseWareCtrl.contestTab = 'board';
                      ctrl.courseWareCtrl.loadBoard();
                    });
                  },
                  postpatch: (_, vnode) => {
                    $(vnode.elm as HTMLElement).off('click').on('click', () => {
                      ctrl.courseWareCtrl.currentRound = item;
                      ctrl.courseWareCtrl.contestTab = 'board';
                      ctrl.courseWareCtrl.loadBoard();
                    });
                  }
                },
              }, `第 ${item.no} 轮, ${item.startsAt}, 共 ${item.boards ? item.boards : 0} 盘`)
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.contest-board', {
      class: {
        none: ctrl.courseWareCtrl.contestTab !== 'board'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backContest', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.contestTab = 'contest';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentContest ? ctrl.courseWareCtrl.currentContest.name : '- 无 -'),
        ' / ',
        h('a.backRound', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.contestTab = 'round';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentRound ? `第 ${ctrl.courseWareCtrl.currentRound.no} 轮` : '- 无 -')
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.boardLoading ? spinner() : h('ul.ul-list.board-list',ctrl.courseWareCtrl.boards.length > 0 ?
          ctrl.courseWareCtrl.boards.map((item, i) => {
            return h('li', { attrs: {'data-id': item.id } }, [
              item.checked ? h('input', { attrs: { type: 'hidden', name: `contest[${i}].name`, value: `${item.white} vs ${item.black}`  } }) : null,
              item.checked ? h('input', { attrs: { type: 'hidden', name: `contest[${i}].setup`, value: item.id  } }) : null,
              h('span', [
                h('input', {
                  attrs: { type: 'checkbox', id: `rd-board-${item.id}`, name: `contest[${i}].content`, value: item.id },
                  hook: {
                    insert(vnode) {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).on('change', () => {
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    },
                    postpatch: (_, vnode) => {
                      let el = vnode.elm as HTMLInputElement;
                      $(el).off('change').on('change', () => {
                        item.checked = el.checked;
                        ctrl.redraw();
                      });
                    }
                  },
                }),
                h('label', {attrs: {for: `rd-board-${item.id}`}}, [
                  h('strong', ` #${item.no} `),
                  h('span', ` ${item.white} `),
                  h('strong', ` ${item.result} `),
                  h('span', ` ${item.black} `)
                ])
              ])
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ])
  ]) : null;
}

function gamedbContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'gamedb' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'gamedb' } }),
    ...ctrl.courseWareCtrl.gamedbSelected.map((item, i) => {
      return h('div', [
        h('input', { attrs: { type: 'hidden', name: `gamedb[${i}].content`, value: item.content  } }),
        h('input', { attrs: { type: 'hidden', name: `gamedb[${i}].name`, value: item.name  } }),
        h('input', { attrs: { type: 'hidden', name: `gamedb[${i}].setup`, value: item.setup  } })
      ])
    }),
    h('div.search', [
      h('input.gamedb-search', { attrs: { placeholder: '搜索' } }),
    ]),
    h('div.scroll', [
      h('div.gamedbTree', {
        hook: {
          insert(vnode) {
            let el = vnode.elm as HTMLElement;
            window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
              window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                let $jq = $.noConflict(true);
                let $tree = <any>$jq(el);
                $tree.jstree({
                  'core': {
                    'data' : {
                      'url' : '/resource/gamedb/tree/loadAll',
                    }
                  },
                  'checkbox' : {
                    "keep_selected_style" : false
                  },
                  'plugins' : [ 'search', 'checkbox', 'changed' ]
                })
                  .on('changed.jstree', function (_, data) {
                    if (data && data.node && data.event && data.event.type === 'click') {
                      ctrl.courseWareCtrl.gamedbSelected = [];
                      let selectedNodes = $tree.jstree(true).get_selected(true);
                      selectedNodes.forEach(n => {
                        if(n.original.type === 'file') {
                          let id = n.id;
                          let relId = id.split(':')[0];
                          let gameId = id.split(':')[1];
                          ctrl.courseWareCtrl.gamedbSelected.push({
                            content: gameId,
                            name: n.text,
                            setup: relId
                          })
                        }
                      });
                      ctrl.redraw();
                    }
                  });

                let to;
                $jq('.gamedb-search').keyup(function () {
                  let q = $jq('.gamedb-search').val();
                  if (to) clearTimeout(to);
                  to = setTimeout(function () {
                    $tree.jstree(true).search(q, false, true);
                  }, 500);
                });
              });
            });
          },
        }
      })
    ])
  ]) : null
}

function situationdbContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'situationdb' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'situationdb' } }),
    h('div.situation-content', [
      h('div.situation-dir', [
        h('div.situationdbTree', {
          hook: {
            insert(vnode) {
              let el = vnode.elm as HTMLElement;
              window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
                window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                  let $jq = $.noConflict(true);
                  let $tree = <any>$jq(el);
                  $tree.jstree({
                    'core': {
                      'worker': false,
                      'data': {
                        'url': '/resource/situationdb/tree/load'
                      },
                      'check_callback': true
                    }
                  })
                    .on('changed.jstree', function (_, data) {
                      if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                        ctrl.courseWareCtrl.situationQuery.selected = data.node.id;
                        ctrl.courseWareCtrl.loadSituation();
                      }
                    });
                })
              });
            }
          }
        })
      ]),
      h('div.situation-situations', [
        h('div.search_form', {
          attrs: {
            style: 'padding: 0'
          }
        },[
          h('table', [
            h('tbody', [
              h('tr', [
                h('td.tag-groups', [
                  h('div.tag-group.emptyTag', [
                    h('span', [
                      h('input', {
                        attrs: { type: 'checkbox', id: 'emptyTag', name: 'situation-emptyTag' },
                        hook: bind('click', (e) => {
                          let el = e.target as HTMLInputElement;
                          if(el.checked) {
                            ctrl.courseWareCtrl.situationQuery.emptyTag = el.value;
                          } else {
                            ctrl.courseWareCtrl.situationQuery.emptyTag = '';
                          }
                          ctrl.courseWareCtrl.loadSituation();
                        })
                      }),
                      h('label', { attrs: { for: 'emptyTag' } }, '无标签')
                    ])
                  ]),
                  h('div.tag-group', ctrl.courseWareCtrl.situationTagsLoading ? [] : ctrl.courseWareCtrl.situationTags.map((tag, index) => {
                    return h('span', [
                      h('input', {
                        attrs: { type: 'checkbox', id: `situation-tag-${index}`, name: `situation-tags[${index}]`, value: tag },
                        hook: bind('click', () => {
                          ctrl.courseWareCtrl.situationQuery.tagSelected = [];
                          $('input[name^="situation-tags"]:checked').each(function (_, el) {
                            let tg = $(el).val();
                            ctrl.courseWareCtrl.situationQuery.tagSelected.push(tg);
                          });
                          ctrl.courseWareCtrl.loadSituation();
                        })
                      }),
                      h('label', { attrs: { for: `situation-tag-${index}` } }, tag)
                    ])
                  }))
                ])
              ]),
              h('tr', [
                h('td', [
                  h('select', {
                    attrs: { name: 'situationStandard'},
                    hook: bind('change', (e: Event) => {
                      ctrl.courseWareCtrl.situationQuery.standard = (e.target as HTMLSelectElement).value;
                    })
                  }, [
                    h('option', { attrs: { value: 'true' } }, '标准局面')
                  ]),
                  h('input', {
                    attrs: { name: 'situation-name', placeholder: '搜索' },
                    hook: {
                      insert(vnode) {
                        const $el = $(vnode.elm as HTMLElement);
                        $el.on('change keyup paste', () => {
                          ctrl.courseWareCtrl.situationQuery.name = $el.val();
                        })
                      }
                    }
                  }),
                  h('a.button', {
                    hook: bind('click', () => {
                      ctrl.courseWareCtrl.loadSituation();
                    })
                  }, '搜索')
                ])
              ])
            ])
          ])
        ]),
        ctrl.courseWareCtrl.situationLoading ? spinner() : (ctrl.courseWareCtrl.situations && ctrl.courseWareCtrl.situations.length > 0) ?
        h('div.situations', ctrl.courseWareCtrl.situations.map((sit, i) => {
          return h('a', {
            class: {
              selected: sit.checked
            }
          },[
            sit.checked ? h('input', { attrs: { type: 'hidden', name: `situationdb[${i}].name`, value: sit.name ? sit.name : ''} }) : null,
            sit.checked ? h('input', { attrs: { type: 'hidden', name: `situationdb[${i}].setup`, value: sit.id  } }) : null,
            sit.checked ? h('input', { attrs: { type: 'hidden', name: `situationdb[${i}].content`, value: sit.fen  } }) : null,
            h('div.mini-board.cg-wrap.is2d.orientation-white', {
              hook: {
                insert(vnode) {
                  let el = vnode.elm as HTMLElement;
                  Chessground(el, {
                    coordinates: false,
                    drawable: { enabled: false, visible: false },
                    resizable: false,
                    viewOnly: true,
                    orientation:  'white',
                    fen: sit.fen
                  });

                  $(el).off('click').on('click', () => {
                    sit.checked = !sit.checked;
                    ctrl.redraw();
                  });
                },
                postpatch: (_, vnode) => {
                  let el = vnode.elm as HTMLElement;
                  $(el).off('click').on('click', () => {
                    sit.checked = !sit.checked;
                    ctrl.redraw();
                  });
                }
              }
            }),
            h('div.btm', [
              h('div.label', sit.name ? sit.name : ''),
              h('div.tags', sit.tags ? sit.tags.map(tag => {
                return h('span', tag)
              }) : [])
            ])
          ]);
        })) : h('div.empty', '没有更多了.')
      ])
    ])
  ]) : null
}

function puzzleRelsContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'puzzleRels' ?
    h('div.content.active', [
      h('div.puzzleRel-radios', [
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'puzzleRel', id: 'puzzle-rel-rd-capsule', checked: ctrl.courseWareCtrl.puzzleRelTab === 'capsule'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.puzzleRelTab = 'capsule';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'puzzle-rel-rd-capsule' }}, '战术题列表')
        ]),
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'puzzleRel', id: 'puzzle-rel-rd-homework', checked: ctrl.courseWareCtrl.puzzleRelTab === 'homework'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.puzzleRelTab = 'homework';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'puzzle-rel-rd-homework' }}, '课后练')
        ]),
        h('span.group', [
          h('input', {
            attrs: {
              type: 'radio', name: 'puzzleRel', id: 'puzzle-rel-rd-puzzle', checked: ctrl.courseWareCtrl.puzzleRelTab === 'puzzle'
            },
            hook: bind('click', _ => {
              ctrl.courseWareCtrl.puzzleRelTab = 'puzzle';
              ctrl.redraw();
            })
          }),
          h('label', { attrs: { for: 'puzzle-rel-rd-puzzle' }}, '战术题')
        ])
      ]),
      capsuleContent(ctrl),
      puzzleHomeworkContent(ctrl),
      puzzleContent(ctrl)
    ]) : null;
}

function capsuleContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.puzzleRelTab === 'capsule' ? h('div.puzzleRel-capsule', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'capsule' } }),
    h('div.tabs-horiz', ctrl.courseWareCtrl.capsuleTabs.map(function (capsuleTab) {
      return makeCapsuleTab(ctrl, capsuleTab.id, capsuleTab.name, capsuleTab.name);
    })),
    h('div.tabs-content', ctrl.courseWareCtrl.capsuleTabs.map(function (capsuleTab) {
      return capsuleContentChild(ctrl, capsuleTab.id);
    }))
  ]) : null
}

function capsuleContentChild(ctrl: OlClassCtrl, tab: string) {
  return h('div.content', {class:{active: tab === ctrl.courseWareCtrl.capsuleQuery.tab}}, [
    h('div.capsule', {
      class: {
        none: ctrl.courseWareCtrl.capsuleTab !== 'capsule'
      }
    }, [
      h('div.search_form', [
        h('table', [
          h('tbody', [
            h('tr', [
              h('td.tag-groups', [
                h('div.tag-group', ctrl.courseWareCtrl.capsuleLoading ? [] : ctrl.courseWareCtrl.capsule[tab].tags.map((tag, index) => {
                  return h('span', [
                    h('input', {
                      attrs: { type: 'checkbox', id: `capsule-tag-${tab}-${index}`, name: `capsule-tags-${tab}-[${index}]`, value: tag },
                      hook: bind('click', () => {
                        ctrl.courseWareCtrl.capsuleQuery.tagSelected = [];
                        $(`input[name^="capsule-tags-${tab}"]:checked`).each(function (_, el) {
                          let tg = $(el).val();
                          ctrl.courseWareCtrl.capsuleQuery.tagSelected.push(tg);
                        });
                        ctrl.courseWareCtrl.filterCapsule();
                      })
                    }),
                    h('label', { attrs: { for: `capsule-tag-${tab}-${index}` } }, tag)
                  ])
                }))
              ])
            ]),
            h('tr', [
              h('td', [
                h('div.search', [
                  h('input', {
                    attrs: { placeholder: '搜索' },
                    hook: {
                      insert(vnode) {
                        const $el = $(vnode.elm as HTMLElement);
                        $el.on('change keyup paste', () => {
                          ctrl.courseWareCtrl.capsuleQuery.name = $el.val();
                        })
                      }
                    }
                  }),
                  h('a.button.button-empty small', {
                    hook: bind('click', () => {
                      ctrl.courseWareCtrl.filterCapsule();
                    })
                  }, '搜索')
                ])
              ])
            ])
          ])
        ])
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.capsuleLoading ? spinner () : h('ul.ul-list.capsule-list', ctrl.courseWareCtrl.capsules.length > 0 ?
          ctrl.courseWareCtrl.capsules.map(item => {
            return h('li', { attrs: {'data-id': item.id } }, [
              h('a', {
                hook: {
                  insert(vnode) {
                    $(vnode.elm as HTMLElement).on('click', () => {
                      ctrl.courseWareCtrl.currentCapsule = item;
                      ctrl.courseWareCtrl.capsuleTab = 'puzzle';
                      ctrl.courseWareCtrl.loadCapsulePuzzle();
                    });
                  },
                  postpatch: (_, vnode) => {
                    $(vnode.elm as HTMLElement).off('click').on('click', () => {
                      ctrl.courseWareCtrl.currentCapsule = item;
                      ctrl.courseWareCtrl.capsuleTab = 'puzzle';
                      ctrl.courseWareCtrl.loadCapsulePuzzle();
                    });
                  }
                }
              }, item.name),
              h('span', item.total)
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.capsule-puzzle', {
      class: {
        none: ctrl.courseWareCtrl.capsuleTab !== 'puzzle'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backCapsule', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.capsuleTab = 'capsule';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentCapsule ? ctrl.courseWareCtrl.currentCapsule.name : '- 无 -')
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.capsulePuzzleLoading ? spinner() : ctrl.courseWareCtrl.capsulePuzzles.length > 0 ?
          h('div.puzzles', ctrl.courseWareCtrl.capsulePuzzles.map((puz, i) => {
            return h('a', {
              class: {
                selected: puz.checked
              }
            },[
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `capsule[${i}].name`, value: `${ctrl.courseWareCtrl.currentCapsule.name}#${puz.id}` } }) : null,
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `capsule[${i}].setup`, value: `${ctrl.courseWareCtrl.currentCapsule.id}:${puz.id}`  } }) : null,
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `capsule[${i}].content`, value: puz.id  } }) : null,
              h('div.mini-board.cg-wrap.is2d', {
                hook: {
                  insert(vnode) {
                    let el = vnode.elm as HTMLElement;
                    Chessground(el, {
                      coordinates: false,
                      drawable: { enabled: false, visible: false },
                      resizable: false,
                      viewOnly: true,
                      orientation: puz.color,
                      lastMove: uciToLastMove(puz.lastMove),
                      fen: puz.fen
                    });

                    $(el).off('click').on('click', () => {
                      puz.checked = !puz.checked;
                      ctrl.redraw();
                    });
                  },
                  postpatch: (_, vnode) => {
                    let el = vnode.elm as HTMLElement;
                    $(el).off('click').on('click', () => {
                      puz.checked = !puz.checked;
                      ctrl.redraw();
                    });
                  }
                }
              })
            ]);
          })) : h('div.empty', '没有更多了.')
      ])
    ])
  ]);
}

function puzzleHomeworkContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.puzzleRelTab === 'homework' ? h('div.puzzleRel-homework', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'homeworkPuzzle' } }),
    h('div.homework', {
      class: {
        none: ctrl.courseWareCtrl.homeworkTab !== 'homework'
      }
    }, [
      h('div.search_form', [
        h('table', [
          h('tbody', [
            h('tr', [
              h('td', [
                h('div.search', [
                  h('input.homework-search-input', { attrs: { placeholder: '搜索' } }),
                  h('a.button.button-empty small', {
                    hook: bind('click', () => {
                      ctrl.courseWareCtrl.filterHomework();
                    })
                  }, '搜索')
                ])
              ])
            ])
          ])
        ])
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.homeworksLoading ? spinner () : h('ul.ul-list.homework-list', ctrl.courseWareCtrl.homeworks.length > 0 ?
          ctrl.courseWareCtrl.filteredHomeworks.map(item => {
            return h('li', { attrs: {'data-id': item.id } }, [
              h('a', {
                hook: {
                  insert(vnode) {
                    $(vnode.elm as HTMLElement).on('click', () => {
                      ctrl.courseWareCtrl.currentHomework = item;
                      ctrl.courseWareCtrl.homeworkTab = 'puzzle';
                      ctrl.courseWareCtrl.loadHomework();
                    });
                  },
                  postpatch: (_, vnode) => {
                    $(vnode.elm as HTMLElement).off('click').on('click', () => {
                      ctrl.courseWareCtrl.currentHomework = item;
                      ctrl.courseWareCtrl.homeworkTab = 'puzzle';
                      ctrl.courseWareCtrl.loadHomework();
                    });
                  }
                }
              }, `第${item.index}节`),
              h('span', item.dateTime)
            ]);
          }) : [h('li', '没有更多了.')])
      ])
    ]),
    h('div.homework-puzzle', {
      class: {
        none: ctrl.courseWareCtrl.homeworkTab !== 'puzzle'
      }
    }, [
      h('div.item-nav', [
        '回到：',
        h('a.backHomework', {
          hook: bind('click', () => {
            ctrl.courseWareCtrl.homeworkTab = 'homework';
            ctrl.redraw();
          })
        }, ctrl.courseWareCtrl.currentHomework ? `第${ctrl.courseWareCtrl.currentHomework.index}节` : '- 无 -'),
        ' · 排序：',
        h('select', {
          attrs: { name: 'homeworkPuzzleSort'},
          hook: bind('change', () => {
            ctrl.courseWareCtrl.loadHomework();
          })
        }, [
          h('option', { attrs: { value: 'no' } }, '题目编号'),
          h('option', { attrs: { value: 'completeRate' } }, '完成率'),
          h('option', { attrs: { value: 'rightRate' } }, '正确率'),
          h('option', { attrs: { value: 'firstRightRate' } }, '首次正确率'),
        ]),
      ]),
      h('div.scroll', [
        ctrl.courseWareCtrl.homeworkLoading ? spinner() : ctrl.courseWareCtrl.homework.puzzles.length > 0 ?
          h('div.puzzles', ctrl.courseWareCtrl.homework.puzzles.map((puz, i) => {
            return h('a', {
              class: {
                selected: puz.checked
              }
            },[
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkPuzzle[${i}].name`, value: `第${ctrl.courseWareCtrl.currentHomework.index}节#${puz.id}` } }) : null,
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkPuzzle[${i}].setup`, value: `${ctrl.courseWareCtrl.currentHomework.id}:puzzle:${puz.id}`  } }) : null,
              puz.checked ? h('input', { attrs: { type: 'hidden', name: `homeworkPuzzle[${i}].content`, value: puz.id  } }) : null,
              h('div.mini-board.cg-wrap.is2d', {
                hook: {
                  insert(vnode) {
                    let el = vnode.elm as HTMLElement;
                    Chessground(el, {
                      coordinates: false,
                      drawable: { enabled: false, visible: false },
                      resizable: false,
                      viewOnly: true,
                      orientation: puz.color,
                      lastMove: uciToLastMove(puz.lastMove),
                      fen: puz.fen
                    });

                    $(el).off('click').on('click', () => {
                      puz.checked = !puz.checked;
                      ctrl.redraw();
                    });
                  },
                  postpatch: (_, vnode) => {
                    let el = vnode.elm as HTMLElement;
                    $(el).off('click').on('click', () => {
                      puz.checked = !puz.checked;
                      ctrl.redraw();
                    });
                  }
                }
              }),
              h('div.btm.homework-tags', [
                h('span.homework-tag.completeRate', {attrs: { title : '完成率' }}, `${puz.completeRate}%`),
                h('span.homework-tag.rightRate', {attrs: { title : '正确率' }}, `${puz.rightRate}%`),
                h('span.homework-tag.firstMoveRightRate', {attrs: { title : '首次正确率' }}, `${puz.firstMoveRightRate}%`),
              ])
            ]);
          })) : h('div.empty', '没有更多了.')
      ])
    ])
  ]) : null
}

function puzzleContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.puzzleRelTab === 'puzzle' ? h('div.puzzleRel-puzzle', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'puzzle' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-puzzle' }
      }, '战术题题号（每行1个）'),
      h('textarea#tab-form-puzzle.form-control', {
        attrs: { name: 'puzzle', rows: 5, required: true, placeholder: '题号1\n题号2\n题号3\n题号4\n...' }
      })
    ])
  ]) : null
}

function editorContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'editor' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'editor' } }),
    h('input.editor-fen', { attrs: { type: 'hidden', name: 'editor', value: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1' } }),
    h('div.board-editor-wrap', {
      hook: {
        insert: vnode => {
          $.when(
            window.lichess.loadScript('compiled/lichess.editor.min.js'),
            $.get('/editor.json', {
              fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
            })
          ).then(function(_, b) {
            const data = b[0];
            data.embed = true;
            data.options = {
              inlineCastling: true,
              onChange: function (fen) {
                $('.editor-fen').val(fen);
              }
            };
            ctrl.courseWareCtrl.editor = window['LichessEditor'](vnode.elm as HTMLElement, data);
          });
        },
        destroy: _ => {
          ctrl.courseWareCtrl.editor = null;
        }
      }
    }, [spinner()])
  ]) : null
}

function gameContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'game' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'game' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-game' }
      }, '对局URL'),
      h('input#tab-form-game.form-control', {
        attrs: { name: 'game', required: true, placeholder: '对局URL' },
      }),
      h('small.form-help', [
        h('div', '例：https://haichess.com/XTtL9RRY'),
        h('div', '或：https://haichess.com/XTtL9RRY/white')
      ]),
    ])
  ]) : null
}

function fenContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'fen' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'fen' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-fen' }
      }, '请输入FEN'),
      h('input#tab-form-fen.form-control', {
        attrs: { name: 'fen', required: true, placeholder: '粘贴FEN文本' }
      })
    ])
  ]) : null
}

function pgnContent(ctrl: OlClassCtrl) {
  return ctrl.courseWareCtrl.activeTab === 'pgn' ? h('div.content.active', [
    h('input', { attrs: { type: 'hidden', name: 'source', value: 'pgn' } }),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn' }
      }, '请输入PGN'),
      h('textarea#tab-form-pgn.form-control', {
        attrs: { name: 'pgn', rows: 5, required: true, placeholder: '粘贴PGN文本' }
      })
    ]),
    h('div.form-group', [
      h('label.form-label', {
        attrs: { for: 'tab-form-pgn-import' }
      }, '上传PGN文件'),
      h('input#tab-form-pgn-import.form-control', {
        attrs: { type: 'file', name: 'pgnFile', accept: '.pgn' },
        hook: {
          insert(vnode) {
            (vnode.elm as HTMLInputElement).addEventListener('change', function() {
              // @ts-ignore
              let file = this.files[0];
              if (!file) return;
              let reader = new FileReader();
              reader.onload = function(e) {
                // @ts-ignore
                $('#tab-form-pgn').val(e.target.result);
              };
              reader.readAsText(file);
            });
          }
        }
      })
    ])
  ]) : null
}

export function renderEditFormModal(ctrl: OlClassCtrl): VNode {
  return ModalBuild.modal({
    onClose: function() {
      ctrl.courseWareCtrl.editFormModal = false;
      ctrl.redraw();
    },
    class: 'courseWareModal',
    content: [
      h('h2', '修改章节'),
      h('div.modal-content-body', [
        h('form.form3.courseWareEditForm', {
          hook: bindSubmit(_ => {
            ctrl.courseWareCtrl.update();
          })
        }, [
          h('div.form-group', [
            h('label.form-label', {
              attrs: { for: 'form-name' }
            }, '名称'),
            h('input#form-name.form-control', {
              attrs: {name: 'name', value: ctrl.courseWareCtrl.edit ? ctrl.courseWareCtrl.edit.name : '', required: true}
            })
          ]),
          h('div.form-split', [
            h('div.form-group.form-half', [
              h('label.form-label', {
                attrs: { 'for': 'form-orientation' }
              }, '棋盘方向'),
              h('select#form-orientation.form-control', {
                  attrs: {name: 'orientation'}
                },
                [{'key':'white', 'name': '白方'}, {'key':'black', 'name': '黑方'}].map(function(color) {
                  return h('option', {
                    attrs: {
                      value: color.key,
                      selected: ctrl.courseWareCtrl.edit ? ctrl.courseWareCtrl.edit.orientation === color.key : false
                    }
                  }, color.name)
                }))
            ]),
            h('div.form-group.form-half.none', [
              h('label.form-label', {
                attrs: { 'for': 'form-showTree' }
              }, '显示棋谱'),
              h('select#form-showTree.form-control', {
                  attrs: {name: 'showTree'}
                },
                [{'key': 'true', 'name': '显示'}, {'key': 'false', 'name': '不显示'}].map(function(st) {
                  return h('option', {
                    attrs: {
                      value: st.key,
                      selected: ctrl.courseWareCtrl.edit ? ctrl.courseWareCtrl.edit.showTree.toString() === st.key : true
                    }
                  }, st.name)
                }))
            ])
          ]),
          h('div.form-actions', [
            h('a.button.button-empty.button-red', {
              attrs: { type: 'submit' },
              hook: bind('click', _ => {
                if(confirm('确认删除？')) {
                  ctrl.courseWareCtrl.remove();
                }
              })
            }, '删除'),
            h('button.button', {
              attrs: { type: 'submit' },
            }, '保存章节')
          ])
        ])
      ])
    ]
  });
}
