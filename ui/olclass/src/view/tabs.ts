import {h} from 'snabbdom';
import {VNode} from "snabbdom/vnode";
import {bind, onInsert} from '../util';
import OlClassCtrl from '../ctrl';
import {userLink} from "./video";

const items = [{k: 'chat', n: '聊天互动'}, {k: 'member', n: '成员列表'}];

export default function (ctrl: OlClassCtrl): VNode {
  return h('div.olclass__tabs', [
    readerHeader(ctrl),
    readerContent(ctrl),
  ]);
}

function readerHeader(ctrl: OlClassCtrl) {
  return h('div.tab-head',
    items.map(function (item) {
      return h('a.tab-item', {
        class: {
          active: item.k === ctrl.liveCtrl.activeItem
        },
        hook: bind('click', e => {
          e.stopPropagation();
          ctrl.liveCtrl.activeItem = item.k;
          ctrl.redraw();
        })
      }, /*item.k === 'member' ? `${item.n}(${ctrl.liveCtrl.onLineUsers()}/${ctrl.opts.students.length})` :*/ item.n)
    })
  )
}

function readerContent(ctrl: OlClassCtrl) {
  return h('div.tab-body', [
    renderChat(ctrl),
    renderMember(ctrl)
  ])
}

function renderChat(ctrl: OlClassCtrl) {
  return h('div.tab-content.chat', {
    class: {
      none: ctrl.liveCtrl.activeItem !== 'chat'
    }
  }, [
    h('section.mchat', {
      hook: onInsert(_ => {
        if (ctrl.opts.chat.instance) ctrl.opts.chat.instance.destroy();
        ctrl.opts.chat.parseMoves = true;
        window.lichess.makeChat(ctrl.opts.chat, chat => {
          ctrl.opts.chat.instance = chat;
        });
      })
    })
  ]);
}

function renderMember(ctrl: OlClassCtrl) {
  return h('div.tab-content.members', {
    class: {
      none: ctrl.liveCtrl.activeItem !== 'member'
    }
  }, [
    h('table.slist', [
      h('tbody', ctrl.liveCtrl.members().map(member => {
        //let isJoined = (ctrl.liveCtrl.localLiveCtrl.isJoined && ctrl.liveCtrl.userIsLocal(member.id)) || ctrl.liveCtrl.remoteLiveCtrl.remoteStreamConfigMap.has(member.id);
        return h('tr', [
          member ? h('td.mem', [userLink(member)/*, h('span.state', { class: {onLine: isJoined} }, isJoined ? '在线': '离线')*/]) : null
        ]);
      }))
    ])
  ]);
}


