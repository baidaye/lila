import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {bind, spinner} from '../util';
import OlClassCtrl from '../ctrl';

export default function renderGlyphsBox(ctrl: OlClassCtrl): VNode | undefined {
  if (ctrl.groundCtrl.menuIsOpen || ctrl.groundCtrl.featureActive !== 'glyphs') return;
  return h('section.glyphs-box.sub-box', [
    h('div.olclass__glyphs' + (ctrl.groundCtrl.glyphs ? '' : '.empty'), {
        hook: {insert: ctrl.groundCtrl.loadGlyphs}
      },
      ctrl.groundCtrl.glyphs ? [
        h('div.move', ctrl.groundCtrl.glyphs.move.map(renderGlyph(ctrl))),
        h('div.position', ctrl.groundCtrl.glyphs.position.map(renderGlyph(ctrl))),
        h('div.observation', ctrl.groundCtrl.glyphs.observation.map(renderGlyph(ctrl)))
      ] : [h('div.olclass__message', spinner())]
    )
  ]);
}

export function renderUnderGlyphsBox(ctrl: OlClassCtrl): VNode {
  return h('div.olclass__glyphs' + (ctrl.groundCtrl.glyphs ? '' : '.empty'), {
      hook: {insert: ctrl.groundCtrl.loadGlyphs}
    },
    ctrl.groundCtrl.glyphs ? [
      h('div.move', ctrl.groundCtrl.glyphs.move.map(renderGlyph(ctrl))),
      h('div.position', ctrl.groundCtrl.glyphs.position.map(renderGlyph(ctrl))),
      h('div.observation', ctrl.groundCtrl.glyphs.observation.map(renderGlyph(ctrl)))
    ] : [h('div.olclass__message', spinner())]
  );
}

function renderGlyph(ctrl: OlClassCtrl) {
  return function (glyph) {
    return h('a', {
      hook: bind('click', _ => {
        ctrl.groundCtrl.toggleGlyph(glyph.id);
        return false;
      }, ctrl.redraw),
      attrs: {'data-symbol': glyph.symbol},
      class: {
        active: !!ctrl.groundCtrl.node.glyphs && !!ctrl.groundCtrl.node.glyphs.find(g => g.id === glyph.id)
      }
    }, [
      glyph.name
    ]);
  };
}
