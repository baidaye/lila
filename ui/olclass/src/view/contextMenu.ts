import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { bind, nodeFullName } from '../util';
import { patch } from '../main';
import OlClassCtrl from '../ctrl';


export interface Opts {
  path: Tree.Path;
  root: OlClassCtrl;
}

interface Coords {
  x: number;
  y: number;
}

const elementId = 'olclass-cm';

function getPosition(e: MouseEvent): Coords {
  let posx = 0, posy = 0;
  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement!.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement!.scrollTop;
  }
  return {
    x: posx,
    y: posy
  }
}

function positionMenu(menu: HTMLElement, coords: Coords): void {

  const menuWidth = menu.offsetWidth + 4,
  menuHeight = menu.offsetHeight + 4,
  windowWidth = window.innerWidth,
  windowHeight = window.innerHeight;

  menu.style.left = (windowWidth - coords.x) < menuWidth ?
    windowWidth - menuWidth + "px" :
    menu.style.left = coords.x + "px";

  menu.style.top = (windowHeight - coords.y) < menuHeight ?
    windowHeight - menuHeight + "px" :
    menu.style.top = coords.y + "px";
}

function action(icon: string, text: string, handler: () => void, cls?: any): VNode {
  return h('a', {
    class: cls,
    attrs: { 'data-icon': icon },
    hook: bind('click', handler)
  }, text);
}

function view(opts: Opts, coords: Coords): VNode {
  const ctrl = opts.root,
  node = ctrl.groundCtrl.tree.nodeAtPath(opts.path),
  onMainline = ctrl.groundCtrl.tree.pathIsMainline(opts.path) && !ctrl.groundCtrl.tree.pathIsForcedVariation(opts.path);
  return h('div#' + elementId + '.visible', {
    hook: {
      insert: vnode => positionMenu(vnode.elm as HTMLElement, coords),
      postpatch: (_, vnode) => positionMenu(vnode.elm as HTMLElement, coords)
    }
  }, [
    h('p.title', nodeFullName(node)),
    onMainline ? null : action('S', '提升变着', () => ctrl.groundCtrl.promote(opts.path, false)),
    onMainline ? null : action('E', '做为主线', () => ctrl.groundCtrl.promote(opts.path, true)),
    action('q', '从此处开始删除', () => ctrl.groundCtrl.deleteNode(opts.path)),
    action('c', '评注这步走法', () => { ctrl.groundCtrl.showComments(opts.path); }),
    action('', '用图形标注', () => { ctrl.groundCtrl.showGlyphs(opts.path); }, {'glyph-icon': true}),
    !onMainline ? null : action('F', '强制变着', () => ctrl.groundCtrl.forceVariation(opts.path, true))
  ]);
}

export default function(e: MouseEvent, opts: Opts): void {
  if(!(opts.root.groundCtrl.isSticky() || opts.root.groundCtrl.isWriting())) {
    return;
  }

  const el = $('#' + elementId)[0] || $('<div id="' + elementId + '">').appendTo($('body'))[0];
  opts.root.groundCtrl.contextMenuPath = opts.path;
  function close(e: MouseEvent) {
    if (e.button === 2) return; // right click
    opts.root.groundCtrl.contextMenuPath = undefined;
    document.removeEventListener('click', close, false);
    $('#' + elementId).removeClass('visible');
    opts.root.redraw();
  };
  document.addEventListener('click', close, false);
  el.innerHTML = '';
  patch(el, view(opts, getPosition(e)));
}
