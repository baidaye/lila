import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { bind, iconTag } from '../util';
import { view as courseWareTag } from './courseWareTag';
import { renderUnderCommentsBox } from './comment';
import { renderUnderGlyphsBox } from './glyph';
import OlClassCtrl from "../ctrl";
import * as pgnExport from "../pgnExport";

export function renderUnderboard(ctrl: OlClassCtrl): VNode {
  return h('div.olclass__underboard_tab', [
    h('div.tabs-horiz', [
      ctrl.liveCtrl.localIsCoach() && ctrl.courseWareCtrl.isSelected() ? toolButton({
        ctrl,
        tab: 'note',
        title: '笔记',
        icon: iconTag('写'),
        onClick() {
        },
      }) : null,
      toolButton({
        ctrl,
        tab: 'tags',
        title: '标签',
        icon: iconTag('o'),
        onClick() {
        },
      }),
      toolButton({
        ctrl,
        tab: 'comments',
        title: '评注',
        icon: iconTag('c'),
        onClick() {
        },
        count: (ctrl.groundCtrl.node.comments || []).length
      }),
      ctrl.groundCtrl.isWriting() ? toolButton({
        ctrl,
        tab: 'glyphs',
        title: ctrl.trans.noarg('annotateWithGlyphs'),
        icon: h('i.glyph-icon'),
        count: (ctrl.groundCtrl.node.glyphs || []).length,
        onClick() {
        },
      }) : null,
      toolButton({
        ctrl,
        tab: 'share',
        title: '分享',
        icon: iconTag('4'),
        onClick() {
        },
        count: 0
      })
    ]),
    h('div.tabs-content', [
      ctrl.liveCtrl.localIsCoach() && ctrl.courseWareCtrl.isSelected() && ctrl.groundCtrl.underboardTab === 'note' ? h('div.content.active', [
        courseWareNote(ctrl)
      ]) : null,
      ctrl.groundCtrl.underboardTab === 'tags' ? h('div.content.active', [
        courseWareTag(ctrl)
      ]) : null,
      ctrl.groundCtrl.underboardTab === 'comments' ? h('div.content.active', [
        renderUnderCommentsBox(ctrl)
      ]) : null,
      ctrl.groundCtrl.isWriting() && ctrl.groundCtrl.underboardTab === 'glyphs' ? h('div.content.active', [
        renderUnderGlyphsBox(ctrl)
      ]) : null,
      ctrl.groundCtrl.underboardTab === 'share' ? h('div.content.active', [
        renderUnderShareBox(ctrl)
      ]) : null
    ])
  ]);
}

interface ToolButtonOpts {
  ctrl: OlClassCtrl;
  tab: string;
  title: string;
  icon: VNode;
  onClick?: () => void;
  count?: number | string;
}

function toolButton(opts: ToolButtonOpts): VNode {
  return h('span.' + opts.tab, {
    attrs: { title: opts.title },
    class: { active: opts.tab === opts.ctrl.groundCtrl.underboardTab },
    hook: bind('mousedown', () => {
      if (opts.onClick) opts.onClick();
      opts.ctrl.groundCtrl.underboardTab = opts.tab;
    }, opts.ctrl.redraw)
  }, [
    opts.count ? h('count.data-count', { attrs: { 'data-count': opts.count } }) : null,
    opts.icon
  ]);
}

function courseWareNote(ctrl: OlClassCtrl) {
  return h(`div.olclass__note.${ctrl.courseWareCtrl.selectedId}`, [
    h('textarea', {
      attrs: {
        rows: 5,
        placeholder: '输入笔记内容，仅教练可见'
      },
      hook: {
        insert(vnode) {
          const $el = $(vnode.elm as HTMLElement);
          $el.val(ctrl.courseWareCtrl.selected.note).on('change keyup paste', window.lichess.debounce(() => {
            ctrl.courseWareCtrl.setNote($el.val())
          }, 1000))
        }
      }
    })
  ])
}

export function renderUnderShareBox(ctrl: OlClassCtrl): VNode | undefined {
  return h('div.olclass__shares', [
    h('button.button.text', {
      attrs: {
        'data-icon': '4'
      },
      hook: bind('click', () => {
        const pgn = pgnExport.renderMainLineTxt(ctrl);
        window.lichess.pubsub.emit('pgn.saveTo', {pgn: pgn, source: 'olclass', rel: ctrl.courseWareCtrl.selected.id});
      })
    }, ['保存PGN', h('span.vTip', {attrs: {title: '会员可用'}}, 'V')])
  ]);
}
