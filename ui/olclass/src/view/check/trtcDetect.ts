import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {bind} from '../../util';
import * as ModalBuild from '../modal';
import {MaybeVNodes} from '../../interfaces';
import OlClassCtrl from '../../ctrl';

export default function (ctrl: OlClassCtrl): VNode {
  return ModalBuild.modal({
    onClose: function() {
      ctrl.checkCtrl.rtcDetectModal = false;
      ctrl.redraw();
    },
    class: 'rtcDetect',
    content: trtcDetect(ctrl)
  });
}

function trtcDetect(ctrl: OlClassCtrl): MaybeVNodes {
  return [
    h('h2', 'RTC支持检测'),
    h('div.modal-content-body', [
      h('div', [
        h('div.recommend', [
          h('div', '当前浏览器不支持 WebRTC SDK'),
          h('div', getRecommendBrowserInfo(ctrl))
        ]),
        h('div.system', [
          h('div', ['系统：', ctrl.checkCtrl.deviceInfo.system.OS]),
          h('div', ['浏览器：', `${ctrl.checkCtrl.deviceInfo.system.browser.name} ${ctrl.checkCtrl.deviceInfo.system.browser.version}`])
        ])
      ]),
      h('div.actions', [
        h('button.button.yes', {
          hook: bind('click', () => {
            ctrl.checkCtrl.rtcDetectModal = false;
            ctrl.redraw();
          })
        }, '确定')
      ]),
    ])
  ];
}

function getRecommendBrowserInfo(ctrl: OlClassCtrl) {
  let os = ctrl.checkCtrl.deviceInfo.system.OS;
  let recommendBrowserInfo = '';
  switch (os) {
    case 'MacOS':
      recommendBrowserInfo = '请使用 Chrome，Safari，Firefox 56+ 或 Edge 80+ 浏览器打开链接';
      break;
    case 'Windows':
      recommendBrowserInfo = '请使用 Chrome, Firefox 56+ 或 Edge 80+ 浏览器打开链接';
      break;
    case 'Android':
      recommendBrowserInfo = '请使用 Chrome 浏览器打开链接';
      break;
    case 'iOS':
      recommendBrowserInfo = '请使用 Safari 浏览器打开链接';
      break;
    default:
      recommendBrowserInfo = '建议下载最新版Chrome浏览器（http://www.google.cn/chrome/）打开链接';
      break;
  }

  return recommendBrowserInfo;
}
