import {h} from 'snabbdom';
import {MaybeVNodes} from '../../interfaces';
import * as TRTC from 'trtc-js-sdk/trtc.js';
import {LocalStream} from 'trtc-js-sdk';
import OlClassCtrl from '../../ctrl';
import {DEVICE_KIND} from '../../interfaces'
import {bind, handleGetUserMediaError} from '../../util';

export default class MicrophoneDetector {

  ctrl: OlClassCtrl;
  deviceKind = DEVICE_KIND.MICROPHONE;
  microphoneId: string;
  localStream: LocalStream;

  TIMER_STOPPED = -1;
  volumeNum = 0;
  timer = this.TIMER_STOPPED;

  constructor(ctrl: OlClassCtrl, readonly handleCompleted: (deviceKind, deviceId, success) => void) {
    this.ctrl = ctrl;
    this.microphoneId = this.getMicrophoneId();

    window.lichess.pubsub.on('check_microphone_loaded', () => {
      this.initStream();
    });
  }

  getMicrophoneId = () => {
    let deviceId = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.MICROPHONE].deviceId;
    return deviceId ? deviceId : (this.ctrl.checkCtrl.deviceInfo.devices.microphones.length > 0 ? this.ctrl.checkCtrl.deviceInfo.devices.microphones[0].deviceId : '')
  };

  initStream = () => {
    if (this.ctrl.checkCtrl.deviceInfo.detects[this.deviceKind].check.isActive && !this.localStream) {
      this.localStream = TRTC.createStream({
        microphoneId: this.microphoneId,
        video: false,
        audio: true
      });

      this.localStream.initialize().then(() => {
        console.log('initialize stream success');
        this.playStream();
      }).catch(error => {
        handleGetUserMediaError(error);
      });
    }
  };

  playStream = () => {
    if(this.ctrl.checkCtrl.deviceInfo.detects[this.deviceKind].check.isActive && this.localStream) {
      this.localStream.play('check-microphone-audio').then().catch(_ => {
        this.closeStream();
      });
      this.initVolumeTimer();
    }
  };

  closeStream = () => {
    if (this.localStream) {
      this.localStream.stop();
      this.localStream.close();
      // @ts-ignore
      this.localStream = null;
    }
    this.clearVolumeTimer();
  };

  initVolumeTimer = () => {
    if (this.timer === this.TIMER_STOPPED) {
      this.timer = setInterval(() => {
        const volume = this.localStream.getAudioLevel();
        const newVolume = Math.ceil(28 * volume);
        if (newVolume !== this.volumeNum) {
          this.volumeNum = newVolume;
          this.ctrl.redraw();
        }
      }, 200);
    }
  };

  clearVolumeTimer = () => {
    if (this.timer != this.TIMER_STOPPED) {
      clearInterval(this.timer);
      this.timer = this.TIMER_STOPPED;
      this.volumeNum = 0;
      this.ctrl.redraw();
    }
  };

  handleMicrophoneChange = (microphoneId) => {
    this.clearVolumeTimer();
    this.localStream.switchDevice('audio', microphoneId).then(_ => {
      this.microphoneId = microphoneId;
      this.initVolumeTimer();
      window.lichess.pubsub.emit('switchDevice', {type: 'audio', deviceId: microphoneId});
    });
  };

  handleError = () => {
    this.closeStream();
    this.handleCompleted(this.deviceKind, this.microphoneId, false);

  };

  handleSuccess = () => {
    this.closeStream();
    this.handleCompleted(this.deviceKind, this.microphoneId, true);
  };

  handleClose = () => {
    this.closeStream();
  };

  view = (): MaybeVNodes => {
    return [
      h('div.step-container',{hook:{ insert() { window.lichess.pubsub.emit('check_microphone_loaded'); }}}, [
        h('div.device-select', [
          h('label', '麦克风选择：'),
          h('select', {
            hook: {
              insert: vnode => {
                (vnode.elm as HTMLSelectElement).addEventListener('change', (e) => {
                  let microphoneId = (e.target as HTMLSelectElement).value;
                  this.handleMicrophoneChange(microphoneId);
                });
              }
            }
          }, this.ctrl.checkCtrl.deviceInfo.devices.microphones.map(microphone => {
            return h('option', {
              attrs: {
                value: microphone.deviceId,
                selected: microphone.deviceId === this.microphoneId
              }
            }, microphone.label)
          }))
        ]),
        h('div.audio-container', [
          h('p', '对着麦克风说 哈喽 试试～'),
          h('div.microphone-bar-container', [...Array(28).keys()].map(index => {
            return h('div.microphone-bar', {class: {active: this.volumeNum > index}})
          })),
          h('div.microphone-audio', {attrs: {id: 'check-microphone-audio'}})
        ]),
        h('div.check-info', '是否可以看到音量图标跳动？')
      ]),
      h('div.actions', [
        h('button.button.button-empty', {
          hook: bind('click', () => this.handleError())
        }, '看不到'),
        h('button.button', {
          hook: bind('click', () => this.handleSuccess())
        }, '看得到')
      ])
    ]
  }
}
