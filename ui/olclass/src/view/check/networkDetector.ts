import {h} from 'snabbdom';
import {MaybeVNodes} from '../../interfaces';
import OlClassCtrl from '../../ctrl';
import {DEVICE_KIND} from '../../interfaces'
import {bind} from '../../util';

export default class NetworkDetector {

  ctrl: OlClassCtrl;
  deviceKind = DEVICE_KIND.NETWORK;

  constructor(ctrl: OlClassCtrl, readonly handleCompleted: (deviceKind, deviceId, success) => void) {
    this.ctrl = ctrl;
  }

  view = (): MaybeVNodes => {
    return [
      h('div.step-container', [
        h('table.network-container', [
          h('tbody', [
            h('tr', [
              h('td', '操作系统'),
              h('td', this.ctrl.checkCtrl.deviceInfo.system.OS)
            ]),
            h('tr', [
              h('td', '浏览器'),
              h('td', [this.ctrl.checkCtrl.deviceInfo.system.browser.name, ' ', this.ctrl.checkCtrl.deviceInfo.system.browser.version])
            ]),
            h('tr', [
              h('td', '是否支持WebRTC'),
              h('td', this.ctrl.checkCtrl.deviceInfo.APISupported.isWebRTCSupported ? '支持' : '不支持')
            ]),
            h('tr', [
              h('td', '是否支持屏幕分享'),
              h('td', this.ctrl.checkCtrl.deviceInfo.APISupported.isScreenCaptureAPISupported ? '支持' : '不支持')
            ]),
            h('tr', [
              h('td', '是否支持H264编码'),
              h('td', this.ctrl.checkCtrl.deviceInfo.codecsSupported.isH264EncodeSupported ? '支持' : '不支持')
            ]),
            h('tr', [
              h('td', '是否支持H264解码'),
              h('td', this.ctrl.checkCtrl.deviceInfo.codecsSupported.isH264DecodeSupported ? '支持' : '不支持')
            ]),
            h('tr', [
              h('td', '是否支持VP8编码'),
              h('td', this.ctrl.checkCtrl.deviceInfo.codecsSupported.isVp8EncodeSupported ? '支持' : '不支持')
            ]),
            h('tr', [
              h('td', '是否支持VP8解码'),
              h('td', this.ctrl.checkCtrl.deviceInfo.codecsSupported.isVp8DecodeSupported ? '支持' : '不支持')
            ])
          ])
        ])
      ]),
      h('div.actions', [
        h('button.button', {
          hook: bind('click', () => { this.handleCompleted(this.deviceKind, '', true) })
        }, '查看检测报告')
      ])
    ]
  }
}
