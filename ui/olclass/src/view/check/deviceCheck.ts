import {h} from 'snabbdom';
import * as ModalBuild from '../modal';
import {MaybeVNodes, DEVICE_KIND} from '../../interfaces';
import {bind} from '../../util';
import OlClassCtrl from '../../ctrl';
import {VNode} from "snabbdom/vnode";


export default function (ctrl: OlClassCtrl): VNode {
  return ModalBuild.modal({
    onClose: () => {
      ctrl.checkCtrl.handleClose();
    },
    class: 'deviceCheck',
    content: deviceCheckView(ctrl)
  });
}

function deviceCheckView(ctrl: OlClassCtrl): MaybeVNodes {
  switch (ctrl.checkCtrl.detectStage) {
    case 0:
      return ctrl.checkCtrl.connectDetector.view();
    case 1:
      return deviceCheckStepView(ctrl);
    case 2:
      return ctrl.checkCtrl.checkReport.view();
    default:
      return [];
  }
}

function deviceCheckStepView(ctrl: OlClassCtrl): MaybeVNodes {
  return [
    h('div.modal-content-body.steps', [
      h('div.step-list', ctrl.checkCtrl.deviceKinds.map((key) => {
        let d = ctrl.checkCtrl.ctrl.checkCtrl.deviceInfo.detects[key];
        return d.isDetect ? h(`div.device.device-${key}`, {
          hook: bind('click', () => {
            if (d.check.isComplete) {
              ctrl.checkCtrl.setActive(key);
              ctrl.checkCtrl.ctrl.redraw();
            }
          }),
          class: {
            complete: d.check.isComplete || d.check.isActive,
            success: d.check.isComplete || d.check.isSuccess,
            error: d.check.isComplete && !d.check.isSuccess
          }
        }, [
          h('div.icon', {attrs: {'data-icon': d.icon}})
        ]) : null
      })),
      h('div', ctrl.checkCtrl.isActive(DEVICE_KIND.CAMERA) ? ctrl.checkCtrl.cameraDetector.view() : []),
      h('div', ctrl.checkCtrl.isActive(DEVICE_KIND.MICROPHONE) ? ctrl.checkCtrl.microphoneDetector.view() : []),
      h('div', ctrl.checkCtrl.isActive(DEVICE_KIND.SPEAKER) ? ctrl.checkCtrl.speakerDetector.view() : []),
      h('div', ctrl.checkCtrl.isActive(DEVICE_KIND.NETWORK) ? ctrl.checkCtrl.networkDetector.view() : [])
    ])
  ]
}

