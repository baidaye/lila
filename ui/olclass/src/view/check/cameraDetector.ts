import {h} from 'snabbdom';
import * as TRTC from 'trtc-js-sdk/trtc.js';
import {LocalStream} from 'trtc-js-sdk';
import OlClassCtrl from '../../ctrl';
import {MaybeVNodes, DEVICE_KIND} from '../../interfaces'
import {bind, handleGetUserMediaError} from '../../util';

export default class CameraDetector {

  ctrl: OlClassCtrl;
  deviceKind = DEVICE_KIND.CAMERA;
  cameraId: string;
  localStream: LocalStream;

  constructor(ctrl: OlClassCtrl, readonly handleCompleted: (deviceKind, deviceId, success) => void) {
    this.ctrl = ctrl;
    this.cameraId = this.getCameraId();

    window.lichess.pubsub.on('check_camera_loaded', () => {
      this.initStream();
    });
  }

  getCameraId = () => {
    let deviceId = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.CAMERA].deviceId;
    return deviceId ? deviceId : (this.ctrl.checkCtrl.deviceInfo.devices.cameras.length > 0 ? this.ctrl.checkCtrl.deviceInfo.devices.cameras[0].deviceId : '')
  };

  initStream = () => {
    if (this.ctrl.checkCtrl.deviceInfo.detects[this.deviceKind].check.isActive && !this.localStream) {
      this.localStream = TRTC.createStream({
        cameraId: this.cameraId,
        video: true,
        audio: false
      });

      this.localStream.initialize().then(() => {
        console.log('initialize stream success');
        this.playStream();
      }).catch(error => {
        handleGetUserMediaError(error);
      });
    }
  };

  playStream = () => {
    if(this.ctrl.checkCtrl.deviceInfo.detects[this.deviceKind].check.isActive && this.localStream) {
      this.localStream.play('check-camera-video').catch(_ => {
        this.closeStream();
      });
    }
  };

  closeStream = () => {
    if (this.localStream) {
      this.localStream.stop();
      this.localStream.close();
      // @ts-ignore
      this.localStream = null;
    }
  };

  handleCameraChange = (cameraId) => {
    this.localStream.switchDevice('video', cameraId).then(_ => {
      this.cameraId = cameraId;
      window.lichess.pubsub.emit('switchDevice', {type: 'video', deviceId: cameraId});
    });
  };

  handleError = () => {
    this.closeStream();
    this.handleCompleted(this.deviceKind, this.cameraId, false);
  };

  handleSuccess = () => {
    this.closeStream();
    this.handleCompleted(this.deviceKind, this.cameraId, true);
  };

  handleClose = () => {
    this.closeStream();
  };

  view = (): MaybeVNodes => {
    return [
      h('div.step-container',{hook:{ insert() { window.lichess.pubsub.emit('check_camera_loaded'); }}}, [
        h('div.device-select', [
          h('label', '摄像头选择：'),
          h('select', {
            hook: {
              insert: vnode => {
                (vnode.elm as HTMLSelectElement).addEventListener('change', (e) => {
                  let cameraId = (e.target as HTMLSelectElement).value;
                  this.handleCameraChange(cameraId);
                });
              }
            }
          }, this.ctrl.checkCtrl.deviceInfo.devices.cameras.map(camera => {
            return h('option', {
              attrs: {
                value: camera.deviceId,
                selected: camera.deviceId === this.cameraId
              }
            }, camera.label)
          }))
        ]),
        h('div.camera-container', [
          h('div.camera-video', {attrs: {id: 'check-camera-video'}})
        ]),
        h('div.check-info', '是否可以清楚的看到自己？')
      ]),
      h('div.actions', [
        h('button.button.button-empty', {
          hook: bind('click', () => this.handleError())
        }, '看不到'),
        h('button.button', {
          hook: bind('click', () => this.handleSuccess())
        }, '看得到')
      ])
    ]
  }
}
