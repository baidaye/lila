import {h} from 'snabbdom';
import {DEVICE_KIND} from '../../interfaces'
import {bind, isOnline, handleGetUserMediaError} from '../../util';
import OlClassCtrl from '../../ctrl';

export default class ConnectDetector {

  ctrl: OlClassCtrl;
  progress = 0;
  showConnectResult = false;

  connectResult = {
    success: false,
    info: '',
    remind: ''
  };

  deviceFailAttention =
    h('ul', [
      h('li', '1. 若浏览器弹出提示，请选择“允许”'),
      h('li', '2. 若杀毒软件弹出提示，请选择“允许”'),
      h('li', '3. 检查系统设置，允许浏览器访问摄像头及麦克风'),
      h('li', '4. 检查浏览器设置，允许网页访问摄像头及麦克风'),
      h('li', '5. 检查摄像头/麦克风是否正确连接并开启'),
      h('li', '6. 尝试重新连接摄像头/麦克风'),
      h('li', '7. 尝试重启设备后重新检测')
    ]);

  networkFailAttention =
    h('ul', [
      h('li', '1. 请检查设备是否联网'),
      h('li', '2. 请刷新网页后再次检测'),
      h('li', '3. 请尝试更换网络后再次检测')
    ]);

  constructor(ctrl: OlClassCtrl, readonly startDeviceCheck: () => void) {
    this.ctrl = ctrl;
  }

  startConnectCheck = () => {
    this.handleProgress();
    this.getDeviceConnectResult();
  };

  handleProgress = () => {
    let interval;
    if (!this.showConnectResult) {
      interval = setInterval(() => {
        if (this.progress >= 100) {
          clearInterval(interval);
          this.setShowConnectResult(true);
          this.ctrl.redraw();
        } else {
          this.setProgress(this.progress + 10);
        }
      }, 200);
    }
  };

  handleReset = () => {
    this.setProgress(0);
    this.setConnectResult({});
    this.setShowConnectResult(false);
  };

  handleReConnect = () => {
    this.handleReset();
    this.startConnectCheck();
    this.ctrl.redraw();
  };

  getDeviceConnectResult = () => {
    this.setConnect(DEVICE_KIND.SPEAKER, this.hasSpeakerDevice());
    this.setConnect(DEVICE_KIND.NETWORK, this.hasNetworkConnect());
    this.setConnectResult(this.getDeviceConnectInfo());

    if (this.hasCameraDevice()) {
      navigator.mediaDevices
        .getUserMedia({video: true, audio: false})
        .then((stream) => {
          this.setConnect(DEVICE_KIND.CAMERA, true);
          // 显示设备连接信息
          this.setConnectResult(this.getDeviceConnectInfo());
          // 释放摄像头设备
          stream.getTracks().forEach(track => track.stop());
          this.ctrl.redraw();
        })
        .catch((error) => {
          handleGetUserMediaError(error);
        });
    }

    if (this.hasMicrophoneDevice()) {
      navigator.mediaDevices
        .getUserMedia({video: false, audio: true})
        .then((stream) => {
          this.setConnect(DEVICE_KIND.MICROPHONE, true);
          // 显示设备连接信息
          this.setConnectResult(this.getDeviceConnectInfo());
          // 释放麦克风设备
          stream.getTracks().forEach(track => track.stop());
          this.ctrl.redraw();
        })
        .catch((error) => {
          handleGetUserMediaError(error);
        });
    }
  };

  getDeviceConnectInfo = () => {
    let connectInfo = '连接出错，请重试';
    if (this.hasCameraConnect()
      && this.hasMicrophoneConnect()
      && this.hasSpeakerConnect()
      && this.hasNetworkConnect()) {
      if (this.hasNetworkDetect()) {
        connectInfo = '设备及网络连接成功，请开始设备检测';
      } else {
        connectInfo = '设备连接成功，请开始设备检测';
      }
      return {
        info: connectInfo,
        success: true,
      };
    }
    // 第一步：浏览器未检测到摄像头/麦克风/扬声器设备的提示
    if (!(this.hasCameraDevice() && this.hasMicrophoneDevice() && this.hasSpeakerDevice())) {
      connectInfo = `未检测到${this.hasCameraDevice() ? '' : ('【摄像头】')}${this.hasMicrophoneDevice() ? '' : ('【麦克风】')}${this.hasSpeakerDevice() ? '' : ('【扬声器】')}设备，请检查设备连接`;
      return {
        info: connectInfo,
        success: false,
      };
    }
    // 第二步：浏览器未拿到摄像头/麦克风权限的提示
    if (!(this.hasCameraConnect() && this.hasMicrophoneConnect())) {
      connectInfo = this.hasNetworkConnect()
        ? ('请允许浏览器及网页访问摄像头/麦克风设备')
        : ('请允许浏览器及网页访问摄像头/麦克风设备，并检查网络连接');
      return {
        info: connectInfo,
        success: false,
        remind: this.deviceFailAttention,
      };
    }
    // 第三步：浏览器检测未连接网络的提示
    if (!this.hasNetworkConnect()) {
      connectInfo = ('网络连接失败，请检查网络连接');
      return {
        info: connectInfo,
        success: false,
        remind: this.networkFailAttention,
      };
    }
    return {
      info: connectInfo,
      success: false,
    };
  };

  hasCameraDevice = () => this.ctrl.checkCtrl.deviceInfo.devices.cameras.length > 0;
  hasMicrophoneDevice = () => this.ctrl.checkCtrl.deviceInfo.devices.microphones.length > 0;
  hasSpeakerDevice = () => this.ctrl.checkCtrl.deviceInfo.detects.speaker.isDetect ? this.ctrl.checkCtrl.deviceInfo.devices.speakers.length > 0 : true;

  hasCameraDetect = () => this.ctrl.checkCtrl.deviceInfo.detects.camera.isDetect;
  hasMicrophoneDetect = () => this.ctrl.checkCtrl.deviceInfo.detects.microphone.isDetect;
  hasSpeakerDetect = () => this.ctrl.checkCtrl.deviceInfo.detects.speaker.isDetect;
  hasNetworkDetect = () => this.ctrl.checkCtrl.deviceInfo.detects.network.isDetect;

  hasCameraConnect = () => this.ctrl.checkCtrl.deviceInfo.detects.camera.detect.isConnect;
  hasMicrophoneConnect = () => this.ctrl.checkCtrl.deviceInfo.detects.microphone.detect.isConnect;
  hasSpeakerConnect = () => this.ctrl.checkCtrl.deviceInfo.detects.speaker.detect.isConnect;
  hasNetworkConnect = () => this.ctrl.checkCtrl.deviceInfo.detects.network.isDetect ? isOnline() : true;

  setConnect = (deviceKind: string, connect: boolean) => {
    this.ctrl.checkCtrl.deviceInfo.detects[deviceKind].detect.isConnect = connect;
  };

  setProgress = (pgs: number) => {
    this.progress = pgs;
    this.ctrl.redraw();
  };

  setConnectResult = (cr: any) => {
    this.connectResult = cr;
  };

  setShowConnectResult = (scr: boolean) => {
    this.showConnectResult = scr;
  };

  view = () => [
    h('h2', '设备连接'),
    h('div.modal-content-body.connect', [
      h('div.prepare-info', `设备检测前请确认设备连接了${this.hasCameraDetect ? '摄像头' : ''}${this.hasMicrophoneDetect ? '、麦克风' : ''}${this.hasSpeakerDetect ? '、扬声器' : ''}${this.hasNetworkDetect ? '和网络' : ''}`),
      h('div.device-list', Object.keys(this.ctrl.checkCtrl.deviceInfo.detects).map(key => {
        let d = this.ctrl.checkCtrl.deviceInfo.detects[key];
        return d.isDetect ? h(`div.device.device-${key}`, [
          h('div.icon', {attrs: {'data-icon': d.icon}}),
          this.showConnectResult && d.detect.isConnect ? h('div.state', {
            class: {'text-green': true},
            attrs: {'data-icon': 'E'}
          }) : null,
          this.showConnectResult && !d.detect.isConnect ? h('div.state', {
            class: {'text-red': true},
            attrs: {'data-icon': 'L'}
          }) : null
        ]) : null
      })),
      !this.showConnectResult ? h('div.outer-progress', [
        h('div.inner-progress', {attrs: {style: `transform: translateX(${this.progress - 100}%)`}})
      ]) : null,
      !this.showConnectResult ? h('div.connect-attention.text-grey', '设备正在连接中，请稍后') : null,
      this.showConnectResult ? h('div.connect-attention', {
        class: {
          'text-green': this.connectResult.success,
          'text-red': !this.connectResult.success
        }
      }, [
        h('span', this.connectResult.info),
        !this.connectResult.success ? h('i', {attrs: {'data-icon': ''}}) : null,
        //!this.connectResult.success && this.showRemind ? this.connectResult.remind : null
      ]) : null,
      h('div.actions', [
        !this.showConnectResult ? h('button.button.disabled', {attrs: {disabled: true}}, '开始检测') : null,
        this.showConnectResult &&
        !(this.hasCameraConnect()
          && this.hasMicrophoneConnect()
          && this.hasSpeakerConnect()
          && this.hasNetworkConnect()) ? h('button.button', {
          hook: bind('click', () => this.handleReConnect())
        }, '重新连接') : null,
        this.showConnectResult &&
        (this.hasCameraConnect()
          && this.hasMicrophoneConnect()
          && this.hasSpeakerConnect()
          && this.hasNetworkConnect()) ? h('button.button', {
          hook: bind('click', () => this.startDeviceCheck())
        }, '开始检测') : null
      ]),
    ])
  ];
}
