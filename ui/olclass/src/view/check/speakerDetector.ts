import {h} from 'snabbdom';
import {MaybeVNodes} from '../../interfaces';
import OlClassCtrl from '../../ctrl';
import {DEVICE_KIND} from '../../interfaces'
import {bind, assetsUrl} from '../../util';

export default class SpeakerDetector {

  ctrl: OlClassCtrl;
  deviceKind = DEVICE_KIND.SPEAKER;
  speakerId: string;
  audioPlayer: HTMLMediaElement;

  constructor(ctrl: OlClassCtrl, readonly handleCompleted: (deviceKind, deviceId, success) => void) {
    this.ctrl = ctrl;
    this.speakerId = this.getSpeakerId();

    window.lichess.pubsub.on('check_speaker_loaded', () => {
      this.initAudio();
    });
  }

  getSpeakerId = () => {
    let deviceId = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.SPEAKER].deviceId;
    return deviceId ? deviceId : (this.ctrl.checkCtrl.deviceInfo.devices.speakers.length > 0 ? this.ctrl.checkCtrl.deviceInfo.devices.speakers[0].deviceId : '')
  };

  initAudio = () => {
    if (this.ctrl.checkCtrl.deviceInfo.detects[this.deviceKind].check.isActive && !this.audioPlayer) {
      this.audioPlayer = document.getElementById('check-speaker-audio') as HTMLMediaElement;
    }
  };

  closeAudio = () => {
    if (this.audioPlayer) {
      if (!this.audioPlayer.paused) {
        this.audioPlayer.pause();
      }
      this.audioPlayer.currentTime = 0;
    }
  };

  handleSpeakerChange = (speakerId) => {
    // @ts-ignore
    this.audioPlayer.setSinkId(speakerId).then(_ => {
      this.speakerId = speakerId;
    });
  };

  handleError = () => {
    this.closeAudio();
    this.handleCompleted(this.deviceKind, this.speakerId, false);
  };

  handleSuccess = () => {
    this.closeAudio();
    this.handleCompleted(this.deviceKind, this.speakerId, true);
  };

  handleClose = () => {
    this.closeAudio();
  };

  view = (): MaybeVNodes => {
    return [
      h('div.step-container',{hook:{ insert() { window.lichess.pubsub.emit('check_speaker_loaded'); }}}, [
        h('div.device-select', [
          h('label', '扬声器选择：'),
          h('select', {
            hook: {
              insert: vnode => {
                (vnode.elm as HTMLSelectElement).addEventListener('change', (e) => {
                  let speakerId = (e.target as HTMLSelectElement).value;
                  this.handleSpeakerChange(speakerId);
                });
              }
            }
          }, this.ctrl.checkCtrl.deviceInfo.devices.speakers.map(speaker => {
            return h('option', {
              attrs: {
                value: speaker.deviceId,
                selected: speaker.deviceId === this.speakerId
              }
            }, speaker.label)
          }))
        ]),
        h('div.speaker-audio-container', [
          h('p', '请调高设备音量，点击播放下面的音频试试～'),
          h('audio', {
            attrs: {
              id: 'check-speaker-audio',
              src: `${assetsUrl}/sound/bgm-test.mp3`,
              controls: 'controls'
            }
          })
        ]),
        h('div.check-info', '是否可以听到声音？')
      ]),
      h('div.actions', [
        h('button.button.button-empty', {
          hook: bind('click', () => this.handleError())
        }, '听不到'),
        h('button.button', {
          hook: bind('click', () => this.handleSuccess())
        }, '听得到')
      ])
    ]
  }
}
