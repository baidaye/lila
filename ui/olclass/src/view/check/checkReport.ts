import {h} from 'snabbdom';
import {MaybeVNodes, DEVICE_KIND} from '../../interfaces';
import OlClassCtrl from '../../ctrl';
import {bind} from '../../util';

export default class CheckReport {

  ctrl: OlClassCtrl;

  constructor(ctrl: OlClassCtrl, readonly handleReCheck: () => void, readonly handleAllComplete: () => void) {
    this.ctrl = ctrl;
  }

  view = (): MaybeVNodes => {
    let deviceInfo = this.ctrl.checkCtrl.deviceInfo;
    let camera = deviceInfo.detects[DEVICE_KIND.CAMERA];
    let microphone = deviceInfo.detects[DEVICE_KIND.MICROPHONE];
    let speaker = deviceInfo.detects[DEVICE_KIND.SPEAKER];
    let network = deviceInfo.detects[DEVICE_KIND.NETWORK];

    return [
      h('h2', '检测报告'),
      h('div.modal-content-body.report', [
        h('table.report-container', [
          h('tbody', [
            camera.isDetect ? h('tr', [
              h('td', [
                h('div.label', {attrs: {'data-icon': '摄'}}, deviceInfo.devices['cameras'].filter(device => device.deviceId === camera.deviceId)[0].label)
              ]),
              h('td', {
                class: {
                  'text-green': camera.check.isSuccess,
                  'text-red': !camera.check.isSuccess,
                }
              }, camera.check.isSuccess ? '正常' : '异常')
            ]) : null,
            microphone.isDetect ? h('tr', [
              h('td', [
                h('div.label', {attrs: {'data-icon': '录'}}, deviceInfo.devices['microphones'].filter(device => device.deviceId === microphone.deviceId)[0].label)
              ]),
              h('td', {
                class: {
                  'text-green': microphone.check.isSuccess,
                  'text-red': !microphone.check.isSuccess,
                }
              }, microphone.check.isSuccess ? '正常' : '异常')
            ]) : null,
            speaker.isDetect ? h('tr', [
              h('td', [
                h('div.label', {attrs: {'data-icon': '音'}}, deviceInfo.devices['speakers'].filter(device => device.deviceId === speaker.deviceId)[0].label)
              ]),
              h('td', {
                class: {
                  'text-green': speaker.check.isSuccess,
                  'text-red': !speaker.check.isSuccess,
                }
              }, speaker.check.isSuccess ? '正常' : '异常')
            ]) : null,
            network.isDetect ? h('tr', [
              h('td', [
                h('div.label', {attrs: {'data-icon': '网'}}, '是否支持WebRTC')
              ]),
              h('td', {
                class: {
                  'text-green': deviceInfo.APISupported.isWebRTCSupported,
                  'text-red': !deviceInfo.APISupported.isWebRTCSupported
                }
              }, deviceInfo.APISupported.isWebRTCSupported ? '支持' : '不支持')
            ]) : null,
            network.isDetect ? h('tr', [
              h('td', [
                h('div.label', {attrs: {'data-icon': '网'}}, '是否支持屏幕分享')
              ]),
              h('td', {
                class: {
                  'text-green': deviceInfo.APISupported.isScreenCaptureAPISupported,
                  'text-red': !deviceInfo.APISupported.isScreenCaptureAPISupported
                }
              }, deviceInfo.APISupported.isScreenCaptureAPISupported ? '支持' : '不支持')
            ]) : null
          ])
        ]),
        h('div.actions', [
          h('button.button.button-empty', {
            hook: bind('click', () => {
              this.handleReCheck();
            })
          }, '重新检测'),
          h('button.button', {
            hook: bind('click', () => {
              this.handleAllComplete();
            })
          }, '完成检测')
        ])
      ])
    ]
  }
}
