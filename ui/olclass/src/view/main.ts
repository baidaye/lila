import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {view as cevalView} from 'ceval';
import * as gridHacks from './gridHacks';
import {view as renderPromotion} from './promotion';
import * as chessground from './ground';
import renderSideView from './side';
import renderTabsView from './tabs';
import renderTrtcDetectView from './check/trtcDetect';
import renderDeviceCheckView from './check/deviceCheck';
//import {renderControls as renderLiveControls} from './underboard';
import {renderUnderboard} from './underboard';
import {renderControls} from './control';
import {renderTools} from './tools';
import {renderSituationModal} from './menus';
import {renderCreateFormModal as renderCourseWareCreateFormModal, renderEditFormModal as renderCourseWareEditFormModal} from './courseWareForm';
import {renderLives} from './lives';
import * as control from '../control';
import OlClassCtrl from '../ctrl';
import {bind} from '../util';

const li = window.lichess;
export default function (ctrl: OlClassCtrl): VNode {
  const showCeval = ctrl.evalCtrl.showComputer();
  let gaugeOn = ctrl.evalCtrl.showEvalGauge();
  let needsInnerCoords = gaugeOn;

  return h('main.olclass', {
    hook: {
      insert: vn => {
        forceInnerCoords(ctrl, needsInnerCoords);
        gridHacks.start(vn.elm as HTMLElement);
      },
      update(_, _2) {
        forceInnerCoords(ctrl, needsInnerCoords);
      },
      postpatch(old, vnode) {
        if (old.data!.gaugeOn !== gaugeOn) {
          li.dispatchEvent(document.body, 'chessground.resize');
        }
        vnode.data!.gaugeOn = gaugeOn;
      }
    },
    class: {
      'comp-off': !showCeval,
      'gauge-on': gaugeOn
    }
  }, [
    renderLives(ctrl),
    renderSideView(ctrl),
    renderTabsView(ctrl),
    h('div.olclass__board.main-board', {
      hook: li.hasTouchEvents ? undefined : bind('wheel', (e: WheelEvent) => wheel(ctrl, e))
    }, [
      chessground.render(ctrl),
      renderPromotion(ctrl),
      ctrl.liveCtrl.localLiveCtrl.isJoined && ctrl.groundCtrl.isSync() && ctrl.groundCtrl.syncActiveMember ? h('div.sync', `正在观看：${ctrl.groundCtrl.syncActiveMember.mark ? ctrl.groundCtrl.syncActiveMember.mark : ctrl.groundCtrl.syncActiveMember.name}`) : null
    ]),
    gaugeOn ? cevalView.renderGauge(ctrl.evalCtrl) : null,
    renderTools(ctrl),
    h('div.olclass__underboard', [
      renderUnderboard(ctrl)
    ]),
    renderControls(ctrl),
    ctrl.checkCtrl.rtcDetectModal ? renderTrtcDetectView(ctrl) : null,
    ctrl.checkCtrl.checkModal ? renderDeviceCheckView(ctrl) : null,
    ctrl.groundCtrl.situationModal ? renderSituationModal(ctrl) : null,
    ctrl.courseWareCtrl.createFormModal ? renderCourseWareCreateFormModal(ctrl) : null,
    ctrl.courseWareCtrl.editFormModal ? renderCourseWareEditFormModal(ctrl) : null
  ]);
}

function forceInnerCoords(ctrl: OlClassCtrl, v: boolean) {
  if (ctrl.opts.pref.coords == 2)
    $('body').toggleClass('coords-in', v).toggleClass('coords-out', !v);
}

function wheel(ctrl: OlClassCtrl, e: WheelEvent) {
  const target = e.target as HTMLElement;
  if (target.tagName !== 'PIECE' && target.tagName !== 'SQUARE' && target.tagName !== 'CG-BOARD') return;
  e.preventDefault();
  if (e.deltaY > 0) control.next(ctrl);
  else if (e.deltaY < 0) control.prev(ctrl);
  ctrl.redraw();
  return false;
}

