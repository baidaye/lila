import {DEVICE_KIND, OlClass, StreamConfig, LiveConfig, RemoteStreamConfig, StreamRole, User} from './interfaces';
import OlClassLiveLocalCtrl from './liveLocalCtrl';
import OlClassLiveRemoteCtrl from './liveRemoteCtrl';
import OlClassCtrl from './ctrl';
import * as xhr from './xhr'

export default class OlClassLiveCtrl {

  activeItem: string;
  loadingDevice: boolean = true;

  cameraDropdownShow: boolean = false;
  microphoneDropdownShow: boolean = false;

  localLiveCtrl: OlClassLiveLocalCtrl;
  remoteLiveCtrl: OlClassLiveRemoteCtrl;

  liveInterval: number;
  liveClock: string = '';

  constructor(readonly ctrl: OlClassCtrl) {
    this.activeItem = 'chat';
    this.localLiveCtrl = new OlClassLiveLocalCtrl(ctrl);
    this.remoteLiveCtrl = new OlClassLiveRemoteCtrl(ctrl);

    window.lichess.pubsub.on('device_loaded', async () => {
      this.loadingDevice = false;
      this.ctrl.redraw();

      if(this.isStarted() && this.localIsCoach()) {
        await this.ctrl.liveCtrl.join();
      }
    });

    if(this.isStarted()) {
      this.startClock();
    }
  }

  lives = (): LiveConfig[] => {
    return this.hosts().concat(this.selfs()).concat(this.speakers()).concat(this.handUps()).concat(this.spectators());
  };

  members = () => {
    return this.ctrl.opts.students;
  };

  onLineUsers = () => {
    return this.ctrl.opts.students.filter(s => this.remoteLiveCtrl.remoteStreamConfigMap.has(s.id)).length + ((this.localLiveCtrl.isJoined && !this.localIsCoach()) ? 1 : 0);
  };

  hosts = (): LiveConfig[] => {
    if(this.localIsCoach()) {
      return [{
        ...this.localLiveCtrl.localStreamConfig as StreamConfig, ...{
          roles: this.getRoles(this.ctrl.opts.userId),
          isJoined: this.localLiveCtrl.isJoined,
          isHandUp: this.userIsHandUp(this.ctrl.opts.userId),
          toggleMuteVideo: this.localLiveCtrl.toggleMuteVideo,
          toggleMuteAudio: this.localLiveCtrl.toggleMuteAudio
        }
      }];
    } else {
      return this.remoteLiveCtrl.remotes().filter(c => this.userIsCoach(c.userId)).map((c) => {
        return this.toRemoteLiveConfig(c);
      })
    }
  };

  selfs = (): LiveConfig[] => {
    if(this.localIsCoach()) {
      return [];
    } else {
      return this.localLiveCtrl.isJoined ? [{
        ...this.localLiveCtrl.localStreamConfig as StreamConfig, ...{
          roles: this.getRoles(this.ctrl.opts.userId),
          isJoined: this.localLiveCtrl.isJoined,
          isHandUp: this.userIsHandUp(this.ctrl.opts.userId),
          toggleMuteVideo: this.localLiveCtrl.toggleMuteVideo,
          toggleMuteAudio: this.localLiveCtrl.toggleMuteAudio
        }
      }] : [];
    }
  };

  speakers = (): LiveConfig[] => {
    return this.remoteLiveCtrl.remotes().map((c) => {
      return this.toRemoteLiveConfig(c);
    }).filter(c => c.roles.includes('speaker') && !c.roles.includes('self'))
  };

  handUps = (): LiveConfig[] => {
    return this.remoteLiveCtrl.remotes().map((c) => {
      return this.toRemoteLiveConfig(c);
    }).filter(c => c.isHandUp && !c.roles.includes('speaker') && !c.roles.includes('self'));
  };

  spectators = (): LiveConfig[] => {
    return this.remoteLiveCtrl.remotes().map((c) => {
      return this.toRemoteLiveConfig(c);
    }).filter(c => c.roles.includes('spectator') && !c.roles.includes('self') && !c.isHandUp);
  };

  toRemoteLiveConfig = (remoteConfig: RemoteStreamConfig) => {
    return {
      ...remoteConfig as StreamConfig, ...{
        roles: this.getRoles(remoteConfig.userId),
        isJoined: this.localLiveCtrl.isJoined,
        isHandUp: this.userIsHandUp(remoteConfig.userId),
        toggleMuteVideo: this.remoteLiveCtrl.toggleMuteVideo,
        toggleMuteAudio: this.remoteLiveCtrl.toggleMuteAudio
      }
    }
  };

  /*
    第一种场景：
    spectator -> handUp -> speaker -> spectator
    观众 -> 举手 -> 发言 -> 结束

    第二种场景：
    spectator -> speaker -> spectator
    观众 -> 发言 -> 结束

    三种角色互斥
    */
  getRoles = (userId: string) => {
    let roles: StreamRole[] = [];

    if(this.ctrl.opts.userId === userId) {
      roles.push('self');
    }

    if(this.ctrl.liveCtrl.userIsCoach(userId)) {
      roles.push('host');
    } else {
      if(this.userIsSpeaker(userId)) {
        roles.push('speaker');
      } else {
        roles.push('spectator');
      }
    }
    return roles;
  };

  getUser = (userId) => {
    if(this.userIsCoach(userId)) {
      return this.ctrl.opts.coach;
    } else {
      return this.ctrl.opts.students.filter((u: User) => u.id === userId)[0];
    }
  };

  startClock = () => {
    this.liveInterval = setInterval(() => {
      this.ctrl.opts.olClass.seconds = this.ctrl.opts.olClass.seconds + 1;
      this.liveClock = formatTime(this.ctrl.opts.olClass.seconds);
      this.ctrl.redraw();
    }, 1000);

    let pad = (x) => {
      return (x < 10 ? '0' : '') + x;
    };

    let formatTime = (total: number) => {
      let hours = Math.floor(total / 3600);
      let minutes = Math.floor(total / 60 % 60);
      let seconds = Math.floor(total % 60);
      return pad(hours) + ':' + pad(minutes) + ':' + pad(seconds);
    };
  };

  start = () => {
    if(this.loadingDevice || !this.isCreated()){
      return;
    }

    xhr.start(this.ctrl.opts.course.id).then(() => {
      this.ctrl.redraw();
    })
  };

  stop = () => {
    if(this.loadingDevice || !this.isStarted()){
      return;
    }

    xhr.stop(this.ctrl.opts.course.id).then(() => {
      this.ctrl.redraw();
    })
  };

  // called by websocket
  onStarted = (data: OlClass) => {
    this.ctrl.opts.olClass = data;
    this.ctrl.liveCtrl.join().then(() => {
      this.startClock();
      this.ctrl.redraw();
    });
  };

  // called by websocket
  onStopped = (data: OlClass) => {
    this.localLiveCtrl.handleLeave().then(() => {
      this.ctrl.opts.olClass = data;
      clearInterval(this.liveInterval);
      this.ctrl.redraw();
    });
  };

  join = async () => {
    if(this.isStarted() && !this.loadingDevice) {
      await xhr.join(this.ctrl.opts.course.id);
      await this.localLiveCtrl.handleJoin();
    }
  };

  leave = async () => {
    if(this.isStarted() && !this.loadingDevice) {
      xhr.leave(this.ctrl.opts.course.id).then(() => {
        setTimeout(async () => {
          await this.localLiveCtrl.handleLeave();
        }, 1000)
      });
    }
  };

  canMuteAll = () => {
    return this.isStarted() && this.localIsCoach() && this.ctrl.liveCtrl.localLiveCtrl.isJoined;
  };

  muteAll = () => {
    if(this.canMuteAll()) {
      xhr.muteAll(this.ctrl.opts.course.id, !this.ctrl.opts.olClass.muteAll).then(() => {
        console.log(`muteAll: ${!this.ctrl.opts.olClass.muteAll}`);
      })
    }
  };

  onMuteAll = (data: any) => {
    this.ctrl.opts.olClass = data;
    if(!this.localIsCoach()) {
      this.localLiveCtrl.muteAudio();
    }
  };

  canSync = (userId) => {
    return this.isStarted() && this.userIsJoin(userId) && this.ctrl.courseWareCtrl.isSelected();
  };

  // = local pub sync
  localIsSync = () => {
    return this.userIsSync(this.ctrl.opts.userId);
  };

  // = user pub sync
  userIsSync = (userId) => {
    return this.ctrl.opts.olClass.syncs.includes(userId);
  };

  toggleSync = (userId: string) => {
    if(this.userIsSync(userId)) {
      this.removeSync(userId);
    } else {
      this.addSync(userId);
    }
  };

  addSync = (userId: string) => {
    if(this.canSync(userId)) {
      xhr.addSync(this.ctrl.opts.course.id, userId).then(() => {
        console.log(`add sync ${userId}`);
      })
    }
  };

  removeSync = (userId: string) => {
    if(this.canSync(userId)) {
      xhr.removeSync(this.ctrl.opts.course.id, userId).then(() => {
        console.log(`remove sync ${userId}`);
      })
    }
  };

  onAddSync = (userId) => {
    this.ctrl.opts.olClass.syncs.push(userId);
    if(this.userIsLocal(userId)) {
      this.ctrl.groundCtrl.showGround();

      if(this.localIsCoach()) {
        this.ctrl.groundCtrl.sendPath(this.ctrl.groundCtrl.path, true);
      }
    }
    this.ctrl.redraw();
  };

  onRemoveSync = (userId) => {
    this.ctrl.opts.olClass.syncs = this.ctrl.opts.olClass.syncs.filter(s => s !== userId);
    if(this.userIsLocal(userId)) {
      this.ctrl.groundCtrl.showGround();
    }
    this.ctrl.redraw();
  };

  canHandUp = () => {
    return this.isStarted() && this.ctrl.liveCtrl.localLiveCtrl.isJoined && !this.localIsHandUp();
  };

  handUp = () => {
    if(this.canHandUp()) {
      xhr.addHandUp(this.ctrl.opts.course.id).then(() => {
        console.log(`local handUp`);
      })
    }
  };

  localIsHandUp = () => {
    return this.userIsHandUp(this.ctrl.opts.userId);
  };

  userIsHandUp = (userId) => {
    return this.ctrl.opts.olClass.handsUp.includes(userId);
  };

  onAddHandUp = (userId) => {
    this.ctrl.opts.olClass.handsUp.push(userId);
    this.ctrl.redraw();
  };

  onRemoveHandUp = (userId) => {
    this.ctrl.opts.olClass.handsUp = this.ctrl.opts.olClass.handsUp.filter(s => s !== userId);
    this.ctrl.redraw();
  };

  canSpeak = () => {
    return this.isStarted();
  };

  toggleSpeaker = (userId) => {
    if(this.canSpeak()) {
      if(this.userIsSpeaker(userId)) {
        xhr.removeSpeaker(this.ctrl.opts.course.id, userId).then(() => {
          console.log(`remove speaker: ${userId}`);
        })
      } else {
        xhr.addSpeaker(this.ctrl.opts.course.id, userId).then(() => {
          console.log(`add speaker: ${userId}`);
        })
      }
    }
  };

  localIsSpeaker = () => {
    return this.userIsSpeaker(this.ctrl.opts.userId);
  };

  userIsSpeaker = (userId) => {
    return this.ctrl.opts.olClass.speakers.includes(userId);
  };

  onAddSpeaker = (userId) => {
    this.ctrl.opts.olClass.speakers.push(userId);
    if(this.userIsLocal(userId)) {
      this.localLiveCtrl.unmute();
    } else {
      this.remoteLiveCtrl.unmute(userId);
    }
    this.ctrl.redraw();
  };

  onRemoveSpeaker = (speaker) => {
    this.ctrl.opts.olClass.speakers = this.ctrl.opts.olClass.speakers.filter(s => s !== speaker);
    if(this.userIsLocal(speaker)) {
      this.localLiveCtrl.muteAudio();
    } else {
      if(!this.localIsCoach()) {
        this.remoteLiveCtrl.mute(speaker);
      }
    }
    this.ctrl.redraw();
  };

  toggleHostMuteVideo = () => {
    if(this.ctrl.liveCtrl.localIsCoach()) {
      this.localLiveCtrl.toggleMuteVideo();
    } else {
      this.remoteLiveCtrl.toggleMuteVideo(this.coach());
    }
  };

  toggleHostMuteAudio = () => {
    if(this.ctrl.liveCtrl.localIsCoach()) {
      this.localLiveCtrl.toggleMuteAudio();
    } else {
      this.remoteLiveCtrl.toggleMuteAudio(this.coach());
    }
  };

  host = () => {
    let config = this.getHostConfig();
    return config ? config.user : this.ctrl.opts.coach;
  };

  hostVolume = () => {
    let config = this.getHostConfig();
    if(config && config.hasAudio && !config.mutedAudio) {
      return config.audioVolume
    } else {
      return 0;
    }
  };

  hostHasAudio = () => {
    let config = this.getHostConfig();
    return config ? config.hasAudio : false;
  };

  hostHasVideo = () => {
    let config = this.getHostConfig();
    return config ? config.hasVideo : false;
  };

  hostMutedAudio = () => {
    let config = this.getHostConfig();
    return config ? config.mutedAudio : true;
  };

  hostMutedVideo = () => {
    let config = this.getHostConfig();
    return config ? config.mutedVideo : true;
  };

  hostAudioSubscribed = () => {
    let config = this.getHostConfig();
    return config ? config.subscribedAudio : false;
  };

  hostVideoSubscribed = () => {
    let config = this.getHostConfig();
    return config ? config.subscribedVideo : false;
  };

  getHostConfig(): StreamConfig | undefined | null {
    let config: StreamConfig | undefined | null;
    if(this.ctrl.liveCtrl.localIsCoach()) {
      config = this.ctrl.liveCtrl.localLiveCtrl.localStreamConfig;
    } else {
      config = this.ctrl.liveCtrl.remoteLiveCtrl.getStream(this.coach());
    }
    return config;
  }

  isLocalUser = (userId: string) => this.ctrl.opts.userId === userId;

  hasVideo = () => {
    let check = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.CAMERA].check;
    return check.isComplete ? check.isSuccess : this.ctrl.checkCtrl.deviceInfo.devices.cameras.length > 0;
  };

  hasAudio = () => {
    let check = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.MICROPHONE].check;
    return check.isComplete ? check.isSuccess : this.ctrl.checkCtrl.deviceInfo.devices.microphones.length > 0;
  };

  getCameraId = () => {
    let deviceId = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.CAMERA].deviceId;
    return deviceId ? deviceId : (this.ctrl.checkCtrl.deviceInfo.devices.cameras.length > 0 ? this.ctrl.checkCtrl.deviceInfo.devices.cameras[0].deviceId : '')
  };

  getMicrophoneId = () => {
    let deviceId = this.ctrl.checkCtrl.deviceInfo.detects[DEVICE_KIND.MICROPHONE].deviceId;
    return deviceId ? deviceId : (this.ctrl.checkCtrl.deviceInfo.devices.microphones.length > 0 ? this.ctrl.checkCtrl.deviceInfo.devices.microphones[0].deviceId : '')
  };

  userIsJoin = (userId) => {
    return (this.userIsLocal(userId) && this.ctrl.liveCtrl.localLiveCtrl.isJoined) || this.remoteLiveCtrl.remoteStreamConfigMap.has(userId);
  };

  localIsCoach = () => {
    return this.coach() === this.ctrl.opts.userId;
  };

  userIsCoach = (userId: string) => this.coach() === userId;

  userIsStudent = (userId: string) => this.ctrl.opts.clazz.students.indexOf(userId) !== -1;

  userIsLocal = (userId: string) => this.ctrl.opts.userId === userId;

  coach = () => this.ctrl.opts.clazz.coach;

  isCreated = () => this.ctrl.opts.olClass.status === 'created';
  isStarted = () => this.ctrl.opts.olClass.status === 'started';
  isStopped = () => this.ctrl.opts.olClass.status === 'stopped';

}

