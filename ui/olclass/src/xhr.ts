const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function getUser(userId: string) {
  return $.ajax({
    url: `/lightUser/${userId}`,
    headers: headers
  });
}

export function get(id: string) {
  return $.ajax({
    method: 'get',
    url: `/olclass/${id}/get`,
    headers: headers,
  });
}

export function start(id: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/start`,
    headers: headers,
  });
}

export function stop(id: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/stop`,
    headers: headers,
  });
}

export function join(id: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/join`,
    headers: headers,
  });
}

export function leave(id: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/leave`,
    headers: headers,
  });
}

export function muteAll(id: string, m: boolean) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/muteAll?m=${m}`,
    headers: headers,
  });
}

export function addSync(id: string, userId: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/addSync?userId=${userId}`,
    headers: headers,
  });
}

export function removeSync(id: string, userId: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/removeSync?userId=${userId}`,
    headers: headers,
  });
}

export function addHandUp(id: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/addHandUp`,
    headers: headers,
  });
}

export function addSpeaker(id: string, userId: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/addSpeaker?userId=${userId}`,
    headers: headers,
  });
}

export function removeSpeaker(id: string, userId: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/removeSpeaker?userId=${userId}`,
    headers: headers,
  });
}

// ------------- course ware ---------------
export function createCourseWare(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/courseWare/create`,
    data: data,
    headers: headers,
  });
}

export function updateCourseWare(id: string, courseWareId: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/courseWare/update?courseWareId=${courseWareId}`,
    data: data,
    headers: headers,
  });
}

export function removeCourseWare(id: string, courseWareId: string) {
  return $.ajax({
    method: 'post',
    url: `/olclass/${id}/courseWare/delete?courseWareId=${courseWareId}`,
    headers: headers,
  });
}

export function sortCourseWare(id: string, courseWareIds: string) {
  return $.ajax({
    method: 'post',
    data: {
      ids: courseWareIds
    },
    url: `/olclass/${id}/courseWare/sort`,
    headers: headers,
  });
}

export function loadCourseWares(id: string) {
  return $.ajax({
    url: `/olclass/${id}/courseWare/list`,
    headers: headers
  });
}

export function loadCourseWare(id: string, courseWareId: string) {
  return $.ajax({
    url: `/olclass/${id}/courseWare/one?courseWareId=${courseWareId}`,
    headers: headers
  });
}

export function setNote(id: string, courseWareId: string, text: string) {
  return $.ajax({
    method: 'post',
    data: {
      text: text
    },
    url: `/olclass/${id}/courseWare/setNote?courseWareId=${courseWareId}`,
    headers: headers,
  });
}

export function loadStudyTags(channel) {
  return $.ajax({
    url: `/study/tags/${channel}`,
    headers: headers
  });
}

export function loadStudy(channel, page, q, tagData) {
  let url = q && q.trim() ? `/study/search?channel=${channel}&page=${page}&q=${q}` : `/study/${channel}/popular?page=${page}`;
  return $.ajax({
    url: url,
    data: tagData,
    headers: headers
  });
}

export function loadChapter(studyId) {
  return $.ajax({
    url: `/study/${studyId}/chapters`,
    headers: headers
  });
}

export function loadContest(page, q) {
  return $.ajax({
    url: `/contest/page/json?page=${page}&q=${q}`,
    headers: headers
  });
}

export function loadRound(contestId) {
  return $.ajax({
    url: `/contest/${contestId}/round/json`,
    headers: headers
  });
}

export function loadBoard(contestId, roundId) {
  return $.ajax({
    url: `/contest/${contestId}/board/json?roundId=${roundId}`,
    headers: headers
  });
}

export function loadSituation(data) {
  return $.ajax({
    url: `/resource/situationdb/rel/list`,
    data: data,
    headers: headers
  });
}

export function loadSituationTags() {
  return $.ajax({
    url: `/resource/situationdb/rel/tags`,
    headers: headers
  });
}

export function loadCapsules() {
  return $.ajax({
    url: `/resource/capsule/mineApi`,
    headers: headers
  });
}

export function loadCapsulePuzzles(id) {
  return $.ajax({
    url: `/resource/capsule/puzzleApi?id=${id}`,
    headers: headers
  });
}

export function loadHomeworks(clazzId) {
  return $.ajax({
    url: `/clazz/${clazzId}/courses`,
    headers: headers
  });
}

export function loadHomework(id, sort) {
  return $.ajax({
    url: `/clazz/homework/jsonData?id=${id}&s=${sort}`,
    headers: headers
  });
}

export function glyphs() {
  return $.ajax({
    url: window.lichess.assetUrl('glyphs.json', { noVersion: true }),
    headers,
    cache: true
  });
}
