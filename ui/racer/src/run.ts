import { Run } from './interfaces';
import { Config as CgConfig } from 'chessground/config';
import { opposite } from 'chessground/util';
import { uciToLastMove } from './util';
import * as cg from 'chessground/types';

export const makeCgOpts = (run: Run, canMove: boolean, flipped: boolean): CgConfig => {
  const cur = run.current;
  const node = cur.node();
  return {
    fen: node.fen,
    orientation: flipped ? opposite(run.color) : run.color,
    turnColor: node.turnColor as cg.Color,
    movable: {
      color: run.color,
      dests: canMove ? node.dests : undefined,
    },
    check: node.check,
    lastMove: uciToLastMove(node.lastMove),
    animation: {
      enabled: cur.moveIndex >= 0,
    },
  };
};

