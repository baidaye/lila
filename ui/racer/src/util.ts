import {h} from 'snabbdom'
import {Hooks} from 'snabbdom/hooks'

export const getNow = (): number => Math.round(performance.now());

export const assetsUrl = $('body').data('asset-url') + '/assets';

export const uciToLastMove = (uci: string | undefined): [Key, Key] | undefined =>
    uci ? [uci.substr(0, 2) as Key, uci.substr(2, 2) as Key] : undefined;

export function bind(eventName: string, f: (e: Event) => any, redraw?: () => void): Hooks {
    return onInsert(el =>
        el.addEventListener(eventName, e => {
            const res = f(e);
            if (redraw) redraw();
            return res;
        })
    );
}

export function onInsert<A extends HTMLElement>(f: (element: A) => void): Hooks {
    return {
        insert: vnode => f(vnode.elm as A)
    };
}

export function dataIcon(icon: string) {
    return {
        'data-icon': icon
    };
}

export function spinner() {
    return h('div.spinner', [
        h('svg', {attrs: {viewBox: '0 0 40 40'}}, [
            h('circle', {
                attrs: {cx: 20, cy: 20, r: 18, fill: 'none'}
            })])]);
}

export function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


export function setCookie(name, value, hours) {
    let now = new Date();
    now.setTime(now.getTime() + (hours * 60 * 60 * 1000));
    let expires = "expires=" + now.toUTCString();
    document.cookie = name + "=" + value + ";" + expires;
}
