import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import { Boost } from '../boost';
import RacerCtrl from '../ctrl';
import { PlayerWithScore as PlayerWithScore } from '../interfaces';

// to [0,1]
type RelativeScore = (score: number) => number;

const trackHeight = 25;

export const renderRace = (ctrl: RacerCtrl) => {
    const players = ctrl.players();
    const minMoves = players.reduce((m, p) => (p.score < m ? p.score : m), 130) / 3;
    const maxMoves = players.reduce((m, p) => (p.score > m ? p.score : m), 35);
    const delta = maxMoves - minMoves;
    const relative: RelativeScore = score => (score - minMoves) / delta;
    const bestScore = players.reduce((b, p) => (p.score > b ? p.score : b), 0);
    const myName = ctrl.player().username;
    const tracks: VNode[] = [];
    players.forEach((p, i) => {
        const isMe = p.username == myName;
        const track = renderTrack(relative, isMe, bestScore, ctrl.boost, p, i, ctrl.mark(p.userId));
        if (isMe) tracks.unshift(track);
        else tracks.push(track);
    });
    return h(
        'div.racer__race',
        {
            attrs: {
                style: `height:${players.length * trackHeight + 14}px`,
            },
        },
        [
            h('div.racer__race__tracks', tracks)
        ]
    );
};

const renderTrack = (
    relative: RelativeScore,
    isMe: boolean,
    bestScore: number,
    boost: Boost,
    player: PlayerWithScore,
    index: number,
    mark: string | undefined | null
): VNode => {
    return h('div.racer__race__track',{
            class: {
                'racer__race__track--me': isMe,
                'racer__race__track--first': player.score > 0 && player.score == bestScore,
                'racer__race__track--boost': boost.isBoosting(index)
            }
        },
        [
            h(
                'div.racer__race__player',
                {
                    attrs: {
                        style: `transform:translateX(${relative(player.score) * 95}%)`,
                    },
                },
                [
                    h(`div.racer__race__player__car.car-${index}`, [0]),
                    h('span.racer__race__player__name', playerLink(player, mark)),
                ]
            ),
            h('div.racer__race__score', [player.score]),
        ]
    );
};

export const playerLink = (player: PlayerWithScore, mark: string | undefined | null) =>
    h('a.user-link.ulpt',{ attrs: {href: '/@/' + player.username} },player.title ? [h('span.utitle', player.title), mark ? mark : player.username] : [mark ? mark : player.username]);
