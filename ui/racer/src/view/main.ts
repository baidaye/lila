import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import config from '../config';
import RacerCtrl from '../ctrl';
import renderClock from './clock';
import { bind } from '../util';
import { playModifiers, renderCombo } from './util';
import { renderRace } from './race';
import { renderBoard } from './board';
import { MaybeVNodes } from '../interfaces'

export default function (ctrl: RacerCtrl): VNode {
  return h(
    'div.racer.racer-app.racer--play',
    {
      class: {
        ...playModifiers(ctrl.run),
        [`racer--${ctrl.status()}`]: true,
      },
    },
    [
        renderRace(ctrl),
        renderBoard(ctrl),
        h('div.puz-side', selectScreen(ctrl))
    ]
  );
}

const selectScreen = (ctrl: RacerCtrl): MaybeVNodes => {
  let vnodes: MaybeVNodes = [];
  switch (ctrl.status()) {
    case 'pre':
        vnodes = renderPre(ctrl);
        break;
    case 'racing':
        vnodes = renderRacing(ctrl);
        break;
    case 'post':
        vnodes = renderPost(ctrl);
        break;
  }
  return vnodes;
};

const renderPre = (ctrl: RacerCtrl) => {
    const povMsg = h('p.racer__pre__message__pov', ctrl.data.round.rule === 'range' ? `你将在所有题中执${ctrl.run.color == 'white' ? '白' : '黑'}` : '你将根据题目随机持有棋色！');
    return ctrl.race.lobby
        ? [
            waitingToStart(ctrl),
            h('div.racer__pre__message.racer__pre__message--with-skip', [
                h('div.racer__pre__message__text', [
                    h('p', '等待更多棋手加入...'),
                    povMsg,
                ])
            ]),
            comboZone(ctrl),
        ]
        : [
            waitingToStart(ctrl),
            h('div.racer__pre__message', [
                renderLink(ctrl),
                renderJoin(ctrl),
                ctrl.canJoin() ? povMsg : null,
                !ctrl.canJoin() ? backLobby() : null,
                renderSetting(ctrl)
            ]),
            comboZone(ctrl),
        ];
};

const renderRacing = (ctrl: RacerCtrl) => {
    const clock = renderClock(ctrl.run, ctrl.end, false);
    return ctrl.isJoinedAndUnreload()
        ? [playerScore(ctrl), h('div.puz-clock', [clock, renderSkip(ctrl)]), renderSetting(ctrl), renderNoMore(ctrl), comboZone(ctrl)]
        : [spectating(ctrl),
            h('div.racer__spectating', [
                h('div.puz-clock', clock),
                ctrl.race.lobby ? lobbyNext(ctrl) : renderJoin(ctrl),
                !ctrl.canJoin() ? backLobby() : null
            ]),
            renderSetting(ctrl),
            comboZone(ctrl),
        ];
};

const renderNoMore = (ctrl: RacerCtrl) => {
    return ctrl.run.puzzleIsAllUsed ? h('div.racer__pre__message__text', [ h('p', '厉害了！所有题目都已经做完~')]) : null;
};

const renderPost = (ctrl: RacerCtrl) => {
    const nextRace = ctrl.race.lobby ? lobbyNext(ctrl) : nextRound(ctrl);
    const raceComplete = h('h2', ctrl.isOverRound() ? '比赛结束！': '本轮结束！');
    return ctrl.isJoinedAndUnreload()
        ? [playerScore(ctrl), h('div.racer__post', [raceComplete, yourRank(ctrl), nextRace]), renderSetting(ctrl), comboZone(ctrl)]
        : [spectating(ctrl), h('div.racer__post', [raceComplete, nextRace]), renderSetting(ctrl), comboZone(ctrl)];
};


const renderSkip = (ctrl: RacerCtrl) =>
    !ctrl.run.puzzleIsAllUsed ?
      h(
        'button.racer__skip.button.button-red',
        {
          class: {
            disabled: !ctrl.canSkip(),
          },
          attrs: {
            title: '跳过这一步来保留你的连击！每场比赛只能用一次',
          },
          hook: bind('click', ctrl.skip),
        },
        '跳过'
      ) : null;

const puzzleRacer = (ctrl: RacerCtrl) => h('strong', ctrl.race.lobby ? '战术竞速赛' : ctrl.race.name);

const puzzleRound = (ctrl: RacerCtrl) => {
    return ctrl.race.lobby ? null : h('span.round', `第${ctrl.data.round.no}轮`);
};

const waitingToStart = (ctrl: RacerCtrl) =>
  h(
    'div.puz-side__top.puz-side__start',
    h('div.puz-side__start__text', [
        puzzleRacer(ctrl),
        h('div', [
            puzzleRound(ctrl),
            h('span.clock', {
                hook: startClock(ctrl.startInSecond())
            }, [ h('span.time') ])
        ])
    ])
  );

const startClock = (time) => {
    return {
        insert: vnode => $(vnode.elm as HTMLElement).clock({ time: time })
    };
};

const spectating = (ctrl: RacerCtrl) =>
  h(
    'div.puz-side__top.puz-side__start',
    h('div.puz-side__start__text', [
        puzzleRacer(ctrl),
        h('div', [
            puzzleRound(ctrl),
            h('span.subline', '观战中')
        ])
    ])
  );

const renderBonus = (bonus: number) => `+${bonus}`;

const renderControls = (ctrl: RacerCtrl): VNode =>
  h(
    'div.puz-side__control',
    h('a.puz-side__control__flip.button', {
      class: {
        active: ctrl.flipped,
        'button-empty': !ctrl.flipped,
      },
      attrs: {'data-icon': 'B', title: '翻转棋盘' },
      hook: bind('click', ctrl.flip),
    })
  );

const comboZone = (ctrl: RacerCtrl) =>
  h('div.puz-side__table', [renderControls(ctrl), renderCombo(config, renderBonus)(ctrl.run)]);

const playerScore = (ctrl: RacerCtrl): VNode =>
  h('div.puz-side__top.puz-side__solved', [
      h('div.puz-side__solved__text', [
          puzzleRacer(ctrl),
          h('div', [
              puzzleRound(ctrl),
              h('span.subline', '得分:' + ctrl.myScore())
          ])
      ])
  ]);

const renderLink = (ctrl: RacerCtrl) => {
    return ctrl.isOwner() ? h('div.puz-side__link', [
        h('p', '邀请比赛，请分享这个网址'),
        h('div', [
            h(`input#racer-url-${ctrl.race.id}.copyable.autoselect`, {
                attrs: {
                    spellcheck: false,
                    readonly: 'readonly',
                    value: `${window.location.protocol}//${window.location.host}/racer/${ctrl.race.id}`,
                },
            }),
            h('button.copy.button', {
                attrs: {
                    title: '复制 URL',
                    'data-rel': `racer-url-${ctrl.race.id}`,
                    'data-icon': '"',
                },
            }),
        ]),
    ]) : null;
};

const renderSetting = (ctrl: RacerCtrl) =>
    ctrl.isOwner() ?
        h(
            'div.puz-side__setting',
            h(
                'button.button.button-empty.button-navaway',
                {
                    hook: bind('click', _ => settingModal(ctrl)),
                },
                '房间设置'
            )
        ) : null;

function settingModal(ctrl: RacerCtrl) {
    $.ajax({
        url: `/racer/${ctrl.race.id}/setting`
    }).then(function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
            $.modal.close();
        });
    })
}

const renderJoin = (ctrl: RacerCtrl) =>
    ctrl.canJoin() ?
      h(
        'div.puz-side__join',
        h(
          'button.button.button-fat',
          {
            hook: bind('click', ctrl.join),
          },
          '加入比赛（' + ctrl.data.players.length + '/' + ctrl.race.maxPlayers + '）'
        )
      ) : null;

const yourRank = (ctrl: RacerCtrl) => {
  const score = ctrl.myScore();
  if (!score) return;
  const players = ctrl.players();
  const rank = players.filter(p => p.score > score).length + 1;
  return h('strong.race__post__rank', `你的排名 ${rank}/${players.length}`);
};

const lobbyNext = (ctrl: RacerCtrl) =>
  h('form',{attrs: { action: `/racer/lobby`, method: 'get' }},[
      h('input', { attrs: { 'type': 'hidden', 'name': 'referId', 'value': ctrl.race.id } }),
      h(
        `button.racer__new-race.button.button-navaway${ctrl.race.lobby ? '.button-fat' : '.button-empty'}`,
        '下一场比赛'
      )
    ]
  );

const nextRound = (ctrl: RacerCtrl) =>
    h('div.racer__post__next', [
        ctrl.isOverRound() ? backLobby() : null,
        !ctrl.isOverRound() ? h('form',{attrs: { action: `/racer/${ctrl.race.id}`, method: 'get' }},[
                h(`button.racer__rematch.button.button-fat.button-navaway`, { class: { disabled: !ctrl.run.serverFinished }, attrs: { disabled: !ctrl.run.serverFinished } },
                    `进入下一轮（${ ctrl.data.round.no + 1 }/${ctrl.race.maxRounds}）`)
            ]
        ) : null
    ]);

const backLobby = () => h('div', [h('a.racer__back.button.button-fat.button-navaway', {attrs: {href: '/racer'}}, '返回大厅')]);