import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import { Chessground } from 'chessground';
import RacerCtrl from '../ctrl';
import { makeCgOpts } from '../run';
import { makeConfig as makeCgConfig } from './chessground';
import { initialFen } from '../interfaces';

export const renderBoard = (ctrl: RacerCtrl) => {
    let cd = ctrl.countdownSeconds();
    return h('div.puz-board.main-board', [
        renderGround(ctrl),
        ctrl.promotion.view(),
        cd ? renderCountdown(cd) : null,
    ]);
};

const renderGround = (ctrl: RacerCtrl): VNode =>
  h('div.cg-wrap', {
    hook: {
      insert: vnode =>
        ctrl.ground(
          Chessground(
            vnode.elm as HTMLElement,
            makeCgConfig(
              ctrl.isRacing() && ctrl.isJoined() && !ctrl.run.puzzleIsAllUsed ?
                  makeCgOpts(ctrl.run, true, ctrl.flipped) :
                  {
                    fen: initialFen,
                    orientation: ctrl.run.color,
                    movable: { color: ctrl.run.color },
                  },
              ctrl.pref,
              ctrl.userMove
            )
          )
        ),
    },
  });

const renderCountdown = (seconds: number) =>
  h('div.racer__countdown', [
    h('div.racer__countdown__lights', [
      h('light.red', {
        class: { active: seconds > 4 },
      }),
      h('light.orange', {
        class: { active: seconds == 3 || seconds == 4 },
      }),
      h('light.green', {
        class: { active: seconds <= 2 },
      }),
    ]),
    h('div.racer__countdown__seconds', [seconds]),
  ]);
