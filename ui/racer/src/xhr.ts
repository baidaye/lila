const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function feedback(raceId, roundNo, puzzleId, win, seconds) {
  let data = {
    win: win ? 1 : 0,
    seconds: seconds
  };

  return $.ajax({
    method: 'POST',
    url: `/racer/${raceId}/${roundNo}/${puzzleId}/feedback`,
    headers: headers,
    data: data
  });
}