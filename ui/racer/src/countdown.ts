import { Clock } from './clock';
import { sound } from './sound';

export class Countdown {
  played = new Set<number>();

  public constructor(readonly cd: number, readonly clock: Clock, readonly onStart: () => void, readonly redraw: () => void) { }

  start = (startsAt: Date): void => {
    const countdown = () => {
      const diff = startsAt.getTime() - Date.now();
      const diffSecond = Math.floor(diff / 1000);
      const diffSecondRemainder = diff % 1000;
      if (diff > 0) {
        if (diffSecond < this.cd) {
          this.playOnce(diffSecond);
        }
        setTimeout(countdown, diffSecondRemainder);
      } else {
        this.clock.start();
        this.onStart();
      }
      this.redraw();
    };
    countdown();
  };

  private playOnce = (i: number) => {
    if (!this.played.has(i)) {
      this.played.add(i);
      if(i === 0) sound.rushGo();
      else {
        sound.countdown();
      }
    }
  };
}
