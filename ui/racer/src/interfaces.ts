import { Api as CgApi } from 'chessground/api';
import * as cg from 'chessground/types';
import { Clock } from './clock';
import { Combo } from './combo';
import CurrentPuzzle from './current';
import { VNode } from 'snabbdom/vnode'

export type MaybeVNode = string | VNode | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export const initialFen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

export type RaceStatus = 'pre' | 'racing' | 'post';

export type WithGround = <A>(f: (g: CgApi) => A) => A | false;

export type PlayerId = string;

export type UserMove = (orig: Key, dest: Key) => void;

export interface RacerOpts {
  element: Element;
  data: RacerData;
  pref: any;
  socketSend: SocketSend;
}

export interface UpdatableData {
  players: PlayerWithScore[];
  startsIn: number;
}

export interface RacerData extends UpdatableData {
  notAccept: boolean,
  race: Race;
  round: Round;
  marks: any;
  player: Player;
}

export interface Race {
  id: string;
  name: string;
  lobby?: boolean;
  owner?: boolean;
  countdown: number;
  initial: number;
  maxRounds: number;
  maxPlayers: number;
}

export interface Round {
  id: string,
  no: number,
  rule: string;
  color: Color;
  puzzles: Puzzle[];
}

export interface Player {
  id: PlayerId;
  userId: string;
  username: string;
  title?: string;
  canJoin: boolean;
}

export interface PlayerWithScore extends Player {
  score: number;
  boostAt?: Date;
}

export interface Puzzle {
  id: number;
  fen: string;
  lastMove: string;
  color: Color;
  lines: string;
  depth: number;
  rating: number;
}

export interface RacerVm {
  startsAt?: Date;
  alreadyStarted: boolean;
}

export interface Run {
  color: Color;
  moves: number;
  errors: number;
  current: CurrentPuzzle;
  clock: Clock;
  history: Result[];
  combo: Combo;
  modifier: Modifier;
  endAt?: number;
  puzzleIsAllUsed: boolean;
  serverFinished: boolean;
}

export interface Result {
  puzzle: Puzzle;
  win: boolean;
  millis: number;
}

export interface Modifier {
  moveAt: number;
  malus?: TimeMod;
  bonus?: TimeMod;
}

export interface TimeMod {
  seconds: number;
  at: number;
}

export interface Config {
  clock: {
    initial: number;
    malus: number;
  };
  combo: {
    levels: [number, number][];
  };
}

export interface RacerConfig extends Config {
  minFirstMoveTime: number;
}

export interface Node {
  fen: Fen;
  uci: Uci;
  lastMove: Uci;
  turnColor: Color;
  dests?: cg.Dests;
  check?: boolean;
  checkmate?: boolean;
}