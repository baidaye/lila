import config from './config';
import throttle from 'common/throttle';
import { prop, Prop } from 'common';
import { storedProp } from 'common/storage';
import { Api as CgApi } from 'chessground/api';
import { Boost } from './boost';
import { Countdown } from './countdown';
import { sound } from './sound';
import { Clock } from './clock';
import { Combo } from './combo';
import CurrentPuzzle from './current';
import { Role } from 'chessground/types';
import { makeCgOpts } from './run';
import makePromotion from './promotion';
import { getNow, getCookie, setCookie } from './util';
import Socket from './socket';
import * as xhr from './xhr'
import {
  RacerOpts,
  RacerData,
  RacerVm,
  Race,
  UpdatableData,
  RaceStatus,
  WithGround,
  Run,
  initialFen
} from './interfaces';


export default class RacerCtrl {

  redraw: () => void;
  data: RacerData;
  race: Race;
  pref: any;
  run: Run;
  vm: RacerVm;
  promotion: any;
  socket: Socket;
  countdown: Countdown;
  boost: Boost = new Boost();
  localScore = 0;
  skipAvailable = true;
  knowsSkip = storedProp('racer.skip', false);
  ground = prop<CgApi | false>(false) as Prop<CgApi | false>;
  flipped = false;

  constructor(opts: RacerOpts, redraw: (data: RacerData) => void) {
    this.data = opts.data;
    this.race = opts.data.race;
    this.pref = opts.pref;
    this.redraw = () => redraw(this.data);
    config.clock.initial = opts.data.race.initial;
    let firstPuzzle = this.data.round.puzzles[0];
    this.run = {
      color: this.data.round.rule === 'range' ? this.data.round.color : firstPuzzle.color,
      moves: 0,
      errors: 0,
      current: new CurrentPuzzle(0, firstPuzzle),
      clock: new Clock(config, Math.max(0, -opts.data.startsIn)),
      history: [],
      combo: new Combo(config),
      modifier: {
        moveAt: 0,
      },
      puzzleIsAllUsed: false,
      serverFinished: false
    };

    this.vm = {
      alreadyStarted: opts.data.startsIn <= 0,
    };

    this.countdown = new Countdown(
      this.race.countdown,
      this.run.clock,
      () => this.onStart(),
      () => setTimeout(this.redraw)
    );

    this.promotion = makePromotion(this.withGround, this.cgOpts, this.redraw);
    this.serverUpdate(opts.data);

    this.socket = new Socket(opts.socketSend, this);

    if(this.data.notAccept && (this.isPre() || this.isRacing())) {
      window.lichess.memberIntro();
    }

    window.onbeforeunload = this.beforeunload;
    window.onunload = this.unload;

    if (this.isJoinedAndUnreload() && this.isRacing()) {
      this.onStart()
    }
  }

  beforeunload = (e) => {
    if(this.isRacing() && this.isJoined()) {
      let message = '关闭页面将无法继续比赛，确定离开此页面吗？';
      e = (e || window.event);
      e.returnValue = message;
      return message;
    }
  };

  unload = () => {
    if(this.isRacing() && this.isJoined()) {
      setCookie(this.data.round.id, 'reloaded', 1);
    }
  };

  onStart = () => {
    this.setGround();
    this.run.current.moveIndex = 0;
    this.setGround();
  };

  serverUpdate = (data: UpdatableData) => {
    this.data.players = data.players;
    this.boost.setPlayers(data.players);
    if (data.startsIn) {
      this.vm.startsAt = new Date(Date.now() + data.startsIn);
      if (data.startsIn > 0) this.countdown.start(this.vm.startsAt);
      else this.run.clock.start();
    }
  };

  serverJoin = (data: UpdatableData) => {
    this.serverUpdate(data);
    if (this.isJoined()) {
      this.onStart()
    }
  };

  serverFinish = (no) => {
    if (this.data.round.no === no) {
      this.run.serverFinished = true;
    }
  };

  reloaded = () => getCookie(this.data.round.id) === 'reloaded';

  player = () => this.data.player;

  mark = (userId: string) => this.data.marks[userId];

  status = (): RaceStatus => this.run.clock.started() ? (this.run.clock.flag() ? 'post' : 'racing') : 'pre';

  players = () => this.data.players;

  //isPlayer = () => !this.vm.alreadyStarted && this.isJoined();

  isJoined = () => this.data.players.some(p => p.id == this.data.player.id);

  isJoinedAndUnreload = () => this.isJoined() && !this.reloaded();

  canJoin = () => !this.raceFull() && this.player().canJoin && !this.isJoined() && !this.reloaded();

  raceFull = () => this.data.players.length >= this.race.maxPlayers;

  isRacing = () => this.status() == 'racing';

  isPre = () => this.status() == 'pre';

  isOwner = () => this.race.owner;

  isOverRound = () => this.data.round.no >= this.race.maxRounds;

  stopClock = () => this.run.clock.stop();

  myScore = (): number => {
    const p = this.data.players.find(p => p.id == this.data.player.id);
    return p ? p.score : 0;
  };

  private socketSend = (tpe: string, data?: any) => this.socket.send(tpe, data);

  join = throttle(1000, () => {
    if(!this.raceFull() && !this.isJoined() && this.player().canJoin) {
      this.socketSend('racerJoin');
    }
  });

  countdownSeconds = (): number | undefined =>
    this.status() == 'pre' && this.vm.startsAt && this.vm.startsAt > new Date() && this.startInSecond() <= this.race.countdown
      ? Math.min(this.race.countdown, Math.ceil(this.startInSecond()))
      : undefined;

  startInSecond = () => {
    if(!this.vm.startsAt) return -1;
    else return (this.vm.startsAt.getTime() - Date.now()) / 1000;
  };

  end = (): void => {
    this.setGround();
    this.redraw();
    sound.rushOver();
    this.redrawSlow();
  };

  canSkip = () => this.skipAvailable;

  skip = () => {
    if (this.skipAvailable && this.run.clock.started()) {
      this.skipAvailable = false;
      sound.win();
      this.playUci(this.run.current.expectedMove());
      this.knowsSkip(true);
    }
  };

  userMove = (orig: Key, dest: Key): void => {
    if (!this.promotion.start(orig, dest, this.playUserMove)) this.playUserMove(orig, dest);
  };

  playUserMove = (orig: Key, dest: Key, promotion?: Role): void =>
    this.playUci(`${orig}${dest}${promotion ? (promotion == 'knight' ? 'n' : promotion[0]) : ''}`);

  playUci = (uci: Uci): void => {
    const now = getNow();
    const puzzle = this.run.current;
    const spend = Math.floor((now - puzzle.startAt) / 1000);
    if (puzzle.startAt + config.minFirstMoveTime > now) console.log('reverted!');
    else {
      this.run.moves++;
      this.promotion.cancel();
      const node = puzzle.node();

      if (node.checkmate || uci == node.uci) {
        sound.win();
        puzzle.moveIndex++;
        this.localScore++;
        this.run.combo.inc();
        this.run.modifier.moveAt = now;
        const bonus = this.run.combo.bonus();
        if (bonus) {
          this.run.modifier.bonus = bonus;
          this.localScore += bonus.seconds; // yeah, ah well
        }
        this.socketSend('racerScore', this.localScore);
        if (puzzle.isOver()) {
          xhr.feedback(this.race.id, this.data.round.no, puzzle.id, true, spend);
          if (!this.incPuzzle()) {
            this.run.puzzleIsAllUsed = true;
            this.end();
          }
        } else {
          puzzle.moveIndex++;
        }
      } else {
        sound.loss();
        this.run.errors++;
        this.run.combo.reset();
        xhr.feedback(this.race.id, this.data.round.no, puzzle.id, false, spend);
        if (this.run.clock.flag()) this.end();
        else if (!this.incPuzzle()) {
          this.run.puzzleIsAllUsed = true;
          this.end();
        }
      }
      this.redraw();
      this.redrawQuick();
      this.redrawSlow();
    }
    this.setGround();
    if (this.run.current.moveIndex < 0) {
      this.run.current.moveIndex = 0;
      this.setGround();
    }
  };

  private incPuzzle = (): boolean => {
    const puzzleIndex = this.run.current.puzzleIndex;
    if (puzzleIndex < this.data.round.puzzles.length - 1) {
      this.run.current = new CurrentPuzzle(puzzleIndex + 1, this.data.round.puzzles[puzzleIndex + 1]);
      this.run.color = this.run.current.puzzle.color;
      return true;
    }
    this.run.current.moveIndex--;
    return false;
  };

  private cgOpts = () =>
    this.isRacing() && this.isJoinedAndUnreload() && !this.run.puzzleIsAllUsed ? makeCgOpts(this.run, this.isRacing(), this.flipped) : {
      fen: initialFen,
      orientation: this.run.color,
      movable: { color: this.run.color }
    };

  private setGround = () =>
    this.withGround(g => g.set(this.cgOpts()));

  withGround: WithGround = f => {
    const g = this.ground();
    return g && f(g);
  };

  flip = () => {
    this.flipped = !this.flipped;
    this.withGround(g => g.toggleOrientation());
    this.redraw();
  };

  redrawQuick = () => setTimeout(this.redraw, 100);
  redrawSlow = () => setTimeout(this.redraw, 1000);

}
