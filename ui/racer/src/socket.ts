import RacerCtrl from './ctrl';
import {UpdatableData} from "./interfaces";

interface Handlers {
  [key: string]: (data: any) => void;
}
export default class Socket {

  send: SocketSend;
  handlers: Handlers;

  constructor(send: SocketSend, ctrl: RacerCtrl) {
    this.send = send;
    this.handlers = {
      racerState: (data: UpdatableData) => {
        ctrl.serverUpdate(data);
        ctrl.redraw();
        ctrl.redrawSlow();
      },
      racerJoin: (data: UpdatableData) => {
        ctrl.serverJoin(data);
        ctrl.redraw();
        ctrl.redrawSlow();
      },
      racerFinish: (d) => {
        ctrl.serverFinish(d.no);
        ctrl.redrawSlow();
      },
      racerRemove: (_) => {
        location.href = '/racer';
      },
      notAccept: (_) => {
        window.lichess.memberIntro();
      }
    };
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  }
};
