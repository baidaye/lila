import * as cg from 'chessground/types';
import { getNow } from './util';
import { Puzzle, Node, initialFen } from './interfaces';
import * as ChessJS from "chess.js";

export default class CurrentPuzzle {
  chess = ChessJS.Chess();
  id: number;
  lines: string[];
  nodes: Node[];
  startAt: number;
  moveIndex = -1;

  constructor(readonly puzzleIndex: number, readonly puzzle: Puzzle) {
    this.id = puzzle.id;
    this.lines = puzzle.lines.split(' ');
    this.nodes = this.buildNodes();
    this.startAt = getNow();
  }

  node = () => {
    if(this.moveIndex >= 0) return this.nodes[this.moveIndex];
    else return {
      fen: initialFen,
      uci: '',
      lastMove: '',
      turnColor: '',
      dests: {},
      check: false,
      checkmate: false
    };
  };

  expectedMove = () => this.node().uci;

  isOver = () => this.moveIndex >= this.lines.length - 1;

  toDests = () => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({square: s, verbose: true});
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests as cg.Dests;
  };

  toColor = () => {
    return (this.chess.turn() === 'w') ? 'white' : 'black';
  };

  buildNodes = () => {
    let nodes: Node[] = [];

    this.chess.load(this.puzzle.fen);
    let node = {
      fen: this.puzzle.fen,
      uci: this.lines[0],
      lastMove: this.puzzle.lastMove,
      turnColor: this.toColor(),
      dests: this.toDests(),
      check: this.chess.in_check(),
      checkmate: this.chess.in_checkmate()
    } as Node;
    nodes.push(node);

    this.lines.forEach((uci, index) => {
      this.chess.move(uci, { sloppy: true });
      let node = {
        fen: this.chess.fen(),
        uci: index < this.lines.length - 1 ? this.lines[index + 1] : '',
        lastMove: uci,
        turnColor: this.toColor(),
        dests: this.toDests(),
        check: this.chess.in_check(),
        checkmate: this.chess.in_checkmate()
      } as Node;

      nodes.push(node);
    });

    nodes = nodes.slice(0, this.puzzle.depth * 2 - 1);
    return nodes;
  }

}
