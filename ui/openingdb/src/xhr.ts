const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function getNextNodes(id: string, fen: string) {
  return $.ajax({
    method: 'get',
    url: `/resource/openingdb/${id}/nextNodes?fen=${encodeURI(fen)}`,
    headers: headers,
  });
}

export function transferNodes(id: string, fen: string) {
  return $.ajax({
    method: 'get',
    url: `/resource/openingdb/${id}/transferNodes?fen=${encodeURI(fen)}`,
    headers: headers,
  });
}

export function searchNodes(id: string, data: string) {
  return $.ajax({
    method: 'get',
    url: `/resource/openingdb/${id}/searchNodes`,
    headers: headers,
    data: data
  });
}

export function setClean(id: string) {
  return $.ajax({
    method: 'post',
    url: `/resource/openingdb/${id}/setClean`,
    headers: headers
  });
}

export function update(id: string, data) {
  return $.ajax({
    method: 'post',
    url: `/resource/openingdb/${id}/update`,
    headers: headers,
    data: data
  });
}

export function addNode(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/resource/openingdb/${id}/addNode`,
    headers: headers,
    data: data
  });
}

export function updateNode(id: string, nodeId: string, data: any, m: string) {
  return $.ajax({
    method: 'post',
    url: `/resource/openingdb/${id}/node/${m}?nodeId=${encodeURI(nodeId)}`,
    headers: headers,
    data: data
  });
}


export function glyphs() {
  return $.ajax({
    url: window.lichess.assetUrl('glyphs2.json', { noVersion: true }),
    headers,
    cache: true
  });
}

export function loadOpeningdbs() {
  return $.ajax({
    url: '/resource/openingdb/nimamadewen',
    headers: headers
  });
}

export function copyFrom(fr: string, to: string) {
  return $.ajax({
    method: 'post',
    url: `/resource/openingdb/${to}/copyFrom?fr=${fr}`,
    headers: headers
  });
}
