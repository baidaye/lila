import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { Chessground } from 'chessground';
import { OpeningDBNode } from 'game';
import * as ModalBuild from './modal';
import { bind, spinner } from '../util';
import OpeningDBCtrl from '../ctrl';

export default function renderSearch(ctrl: OpeningDBCtrl): VNode {
  let d = renderEditorText(ctrl);
  return h('div.openingdb__search',  [
    h('form.openingdb__search_form',  [
      h('div.form-inline',  [
        h('label',  '开局：'),
        h('input', { attrs: {type: 'text', name: 'name', placeholder: '开局名称'}})
      ]),
      h('div.form-inline',  [
        h('label',  '标识：'),
        h('select', {
          attrs: { name: 'glyph' }
        }, [
          h('option', {attrs: {value: ''}}, '全部'), ...(ctrl.glyphs.move.filter(g => g.id !== 22).concat(ctrl.glyphs.frequency).concat(ctrl.glyphs.special).concat(ctrl.glyphs.puzhao)).map(function (glyph) {
            return h('option', { attrs: { value: glyph.id } }, `${glyph.name}（${glyph.symbol}）`)
          })
        ])
      ]),
      h('div.form-inline',  [
        h('label',  '棋色：'),
        h(`select.${JSON.stringify(ctrl.editorMove)}`, {
          hook: bind('change', (e: Event) => {
            let c = (e.target as HTMLInputElement).value;
            setColor(ctrl, c);
          })
        }, [
          h('option', { attrs: { value: '', selected: !isEditorMoveDefined(ctrl) } }, '全部'),
          h('option', { attrs: { value: 'white', selected: isEditorMoveDefined(ctrl) && ctrl.editorMove.color === 'white' } }, '白棋'),
          h('option', { attrs: { value: 'black', selected: isEditorMoveDefined(ctrl) && ctrl.editorMove.color === 'black' } }, '黑棋')
        ])
      ]),
      h('div.form-inline',  [
        isEditorMoveDefined(ctrl) ? h('input', { attrs: { 'type': 'hidden', name: 'color', value: ctrl.editorMove.color }}) : null,
        isEditorMoveDefined(ctrl) ? h('input', { attrs: { 'type': 'hidden', name: 'role', value: ctrl.editorMove.role }}) : null,
        isEditorMoveDefined(ctrl) ? h('input', { attrs: { 'type': 'hidden', name: 'uci', value: ctrl.editorMove.uci }}) : null,
        h('label',  '着法：'),
        h('input', {
          attrs:{type: 'text', name: 'fullUci', value: d, placeholder: '例：Pe2e4'},
          hook: {
            insert(vnode) {
              const $el = $(vnode.elm as HTMLElement);
              $el.val(d).on('change keyup paste', (e) => {
                let v = (e.target as HTMLInputElement).value;
                setEditorMove(ctrl, v);
              })
            },
            postpatch(_, vnode) {
              const $el = $(vnode.elm as HTMLElement);
              $el.val(d).on('change keyup paste', (e) => {
                let v = (e.target as HTMLInputElement).value;
                setEditorMove(ctrl, v);
              })
            }
          }
        }),
        h('a.button.button-empty.small', {
          hook: bind('click', _ => {
            ctrl.showEditorModal = true;
            ctrl.redraw();
          })
        }, '选择')
      ]),
      h('div.form-inline',  [
        h('label',  'FEN：'),
        h('input', {
          attrs: {type: 'text', name: 'fen', placeholder: '合法的FEN'}
        })
      ]),
      h('div.form-action',  [
        h('a.button.small', {
          hook: bind('click', ctrl.loadSearchNodes)
        }, '搜 索')
      ])
    ]),
    h('div.openingdb__search_boards',  [
      ctrl.searchNodesUciFail ? renderUciFail() : (
        ctrl.searchNodesLoading ? spinner() : ( ctrl.searchNodes.length ? renderBoards(ctrl) : h('div.empty', '没有更多了.'))
      )
    ])
  ]);
}

function renderBoards(ctrl: OpeningDBCtrl) {
  return h('div.now-playing', ctrl.searchNodes.map(node => {
    return renderBoard(ctrl, node)
  }));
}

function renderBoard(ctrl: OpeningDBCtrl, node: OpeningDBNode) {
  let currentOpeningdbNode = ctrl.groundCtrl.getCurrentOpeningdbNode();
  return h('a.' + node.id, {
    attrs: {target: '_blank', href: `/resource/openingdb/${ctrl.openingdb.id}/node?nodeId=${encodeURI(node.id)}`},
    class: { active: (!!currentOpeningdbNode && node.id == currentOpeningdbNode.id) },
    hook: bind('mousedown', _ => {})
  }, [
    h('div.mini-board.cg-wrap', {
      hook: {
        insert(vnode) {
          Chessground(vnode.elm as HTMLElement, {
            coordinates: false,
            drawable: { enabled: false, visible: false },
            resizable: false,
            viewOnly: true,
            orientation: ctrl.openingdb.orientation,
            fen: node.fen,
            lastMove: uciToLastMove(node.uci)
          });
        }
      }
    }),
    h('div.btm', [
      h('div.name', (node.shortName || '-')),
      h('div.san', node.san)
    ]),
  ]);
}

function uciToLastMove(lm?: string): Key[] | undefined {
  return lm ? ([lm[0] + lm[1], lm[2] + lm[3]] as Key[]) : undefined;
}

function renderUciFail() {
  return h('div.uciFail', [
    h('h3.message', '着法校验失败'),
    h('div.block', [
      h('h3.title', '正确输入格式：'),
      h('ul', [
        h('li', '1、棋子类型+原始位置+目标位置；'),
        h('li', '2、棋子类型大写：兵 P，马 N，象 B，车 R，后 Q，王 K；'),
        h('li', '3、棋子位置坐标，字母为小写；'),
        h('li', '4、共5位字符，不含空格。')
      ])
    ]),
    h('div.block', [
      h('h3.title', '示例：'),
      h('ul', [
        h('li', '1、Pe2e4，兵从e2格走到e4格；'),
        h('li', '2、Ke8g8，黑棋短易位，Ke8c8，长易位；'),
        h('li', '3、Ke1g1，白棋短易位，Ke1c1，长易位。')
      ])
    ]),
    h('div.block', [
      h('h3.title', '注意：'),
      h('div', '系统不做着法合法性校验，如果输入不和法的着法，将无法返回结果。')
    ])
  ])
}

export function renderEditorModal(ctrl: OpeningDBCtrl): VNode {
  return ModalBuild.modal({
    onClose: function() {
      ctrl.showEditorModal = false;
      ctrl.redraw();
    },
    class: 'uciBoardModal',
    content: [
      h('h2', '选择着法'),
      h('div.modal-content-body', [
        h('div.board-editor-wrap', {
          hook: {
            insert: vnode => {
              $.when(
                window.lichess.loadScript('compiled/lichess.editor.min.js'),
                $.get('/editor.json', {
                  fen: '8/8/8/8/8/8/8/8 w - -'
                })
              ).then(function(_, b) {
                const data = b[0];
                data.embed = true;
                data.options = {
                  inlineCastling: true,
                  autoPointer: true,
                  showMoveShape: true,
                  coordinates: true,
                  onUserMove: function (d) {
                    ctrl.onEditorMove(d);
                  }
                };
                ctrl.editor = window['LichessEditor'](vnode.elm as HTMLElement, data);
              });
            },
            destroy: _ => {
              ctrl.editor = null;
            }
          }
        }, [spinner()]),
        h('div.selected', [
          h('label',  '棋子着法：'),
          renderEditorTags(ctrl)
        ])
      ]),
      h('button.button.small', {
        hook: bind('click', () => {
          ctrl.showEditorModal = false;
          ctrl.redraw();
        })
      }, '确认')
    ]
  });
}

function renderEditorTags(ctrl: OpeningDBCtrl) {
  if(isEditorMoveDefined(ctrl)) {
    const role = ctrl.editorMove.role;
    return h('div.search-tag', [
      ctrl.editorMove.color ? h('span', ctrl.editorMove.color === 'white' ? '白棋' : '黑棋') : null,
      h('span', `${role.toUpperCase()}${ctrl.editorMove.uci}`),
      h('a.tag-close', {
        attrs: { title: '清除' },
        hook: bind('click', () => {
          ctrl.editorMove = null;
          ctrl.redraw();
        })
      }, '| x')
    ]);
  } else {
    return '- 无 -';
  }
}

function renderEditorText(ctrl: OpeningDBCtrl) {
  if(isEditorMoveDefined(ctrl)) {
    const role = ctrl.editorMove.role;
    return role ? `${role.toUpperCase()}${ctrl.editorMove.uci}` : '';
  } else return '';
}

function setEditorMove(ctrl: OpeningDBCtrl, v: string) {
  let role = v.slice(0, 1);
  let uci = v.slice(1, 5);
  ctrl.editorMove = {color: ctrl.editorMove ? ctrl.editorMove.color : '', role: role ? role.toLowerCase() : '', uci: uci};
  ctrl.redraw();
}

function setColor(ctrl: OpeningDBCtrl, v: string) {
  if(ctrl.editorMove) {
    ctrl.editorMove.color = v;
  } else {
    ctrl.editorMove = {color: v};
  }
  ctrl.redraw();
}

function isEditorMoveDefined(ctrl: OpeningDBCtrl) {
  return ctrl.editorMove && Object.keys(ctrl.editorMove).length;
}
