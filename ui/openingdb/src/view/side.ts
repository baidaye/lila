import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { bind, iconTag } from '../util';
import renderMetadata from './metadata';
import renderCommentBox from './comment';
import renderGlyphsBox from './glyph';
import renderTransfer from './transfer';
import renderSearch from './search';
import renderShare from './share';
import {dataIcon} from '../util';
import OpeningDBCtrl from '../ctrl';


export default function (ctrl: OpeningDBCtrl): VNode {
  return h('div.openingdb__side', {
    hook: {insert: ctrl.loadGlyphs},
    class: {
      showSide: ctrl.groundCtrl.showSide
    }
  }, [
    h('div.header', {
      hook: bind('click', _ => {
        ctrl.groundCtrl.toggleShowSide();
      })
    }, [
      h('div.left', [
        h('span.title', ctrl.openingdb.name),
        h('span.readWriteTag', ctrl.canWrite() ? '读写' : '只读')
      ]),
      h('i', { attrs: dataIcon(ctrl.groundCtrl.showSide ? 'R' : 'S') })
    ]),
    h('div.body', [
      h('div.tabs-horiz', [
        toolButton({
          ctrl,
          tab: 'comment',
          title: '评注',
          icon: iconTag('c'),
          onClick() {
          },
          count: ctrl.groundCtrl.getCurrentOpeningdbNodeComment()
        }),
        toolButton({
          ctrl,
          tab: 'glyphs',
          title: '标注',
          icon: h('i.glyph-icon'),
          count: (ctrl.groundCtrl.node.glyphs || []).length,
          onClick() {
          },
        }),
        toolButton({
          ctrl,
          tab: 'transfer',
          title: '转换',
          icon: iconTag('题'),
          onClick() {
            let node = ctrl.groundCtrl.getCurrentOpeningdbNode();
            if(node) {
              ctrl.loadTransferNodes(node.fen)
            }
          },
          count: (ctrl.transferNodes.length - 1)
        }),
        toolButton({
          ctrl,
          tab: 'search',
          title: '搜索',
          icon: iconTag('y'),
          count: 0,
          onClick() {
          },
        }),
        toolButton({
          ctrl,
          tab: 'share',
          title: '分享',
          icon: iconTag('$'),
          onClick() {
          },
          count: 0
        }),
        toolButton({
          ctrl,
          tab: 'metadata',
          title: '基本信息',
          icon: iconTag('%'),
          onClick() {
          },
          count: 0
        }),
        ctrl.canWrite() && !ctrl.isFull() && !ctrl.isCleaning() ? h('span.sync', [
          h(`input.${ctrl.sync}`, {
            attrs: { type: 'checkbox', id: 'chk-sync', checked: ctrl.sync },
            hook: bind('click', e => {
              ctrl.syncChange((e.target as HTMLInputElement).checked);
            })
          }),
          h('label', { attrs: { for: 'chk-sync' }}, '录入')
        ]) : null
      ]),
      h('div.tabs-content', [
        ctrl.groundCtrl.sideTab === 'comment' ? h('div.content.active', [
          renderCommentBox(ctrl)
        ]) : null,
        ctrl.groundCtrl.sideTab === 'glyphs' ? h('div.content.active', [
          renderGlyphsBox(ctrl)
        ]) : null,
        ctrl.groundCtrl.sideTab === 'transfer' ? h('div.content.active', [
          renderTransfer(ctrl)
        ]) : null,
        ctrl.groundCtrl.sideTab === 'search' ? h('div.content.active', [
          renderSearch(ctrl)
        ]) : null,
        ctrl.groundCtrl.sideTab === 'share' ? h('div.content.active', [
          renderShare(ctrl)
        ]) : null,
        ctrl.groundCtrl.sideTab === 'metadata' ? h('div.content.active', [
          renderMetadata(ctrl)
        ]) : null,
      ])
    ])
  ]);
}

interface ToolButtonOpts {
  ctrl: OpeningDBCtrl;
  tab: string;
  title: string;
  icon: VNode;
  onClick?: () => void;
  count?: number | string;
}

function toolButton(opts: ToolButtonOpts): VNode {
  return h('span.' + opts.tab, {
    attrs: { title: opts.title },
    class: { active: opts.tab === opts.ctrl.groundCtrl.sideTab },
    hook: bind('mousedown', () => {
      if (opts.onClick) opts.onClick();
      opts.ctrl.groundCtrl.sideTab = opts.tab;
    }, opts.ctrl.redraw)
  }, [
    opts.count && opts.count > 0 ? h('count.data-count', { attrs: { 'data-count': opts.count } }) : null,
    opts.icon
  ]);
}
