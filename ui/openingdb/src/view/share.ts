import { h } from 'snabbdom'
import { bind, dataIcon } from '../util';
import * as pgnExport from '../pgnExport';
import OpeningDBCtrl from '../ctrl';

export default function renderShare(ctrl: OpeningDBCtrl) {
  return h('div.openingdb__share', [
    h('div.shares', [
      h('a.button.button-empty', {
        hook: bind('click', () => {
          let pgn = encodePgn(ctrl);
          window.open(`/analysis/pgnPage?urlPgn=${pgn}`, '_blank');
        }),
        attrs: dataIcon('A')
      }, '开局浏览器'),
      h('a.button.button-empty', {
        attrs: {
          href: '/editor?fen=' + ctrl.groundCtrl.node.fen,
          rel: 'nofollow',
          target: '_blank',
          'data-icon': 'm'
        }
      }, '棋盘编辑器'),
      h('a.button.button-empty', {
        hook: bind('click', _ => $.modal($('.continue-with.fen'))),
        attrs: dataIcon('U')
      }, '当前局面下棋'),
      h('a.button.button-empty', {
        hook: bind('click', _ => $.modal($('.continue-with.pgn'))),
        attrs: dataIcon('U')
      }, '当前棋谱下棋'),
      studyButton(ctrl),
    ]),
    h('div.copyables', [
      h('div.pair', [
        h('label.name', 'FEN'),
        h('input.copyable.autoselect.openingdb__underboard__fen', {
          attrs: {
            readonly: true,
            spellCheck: false,
            value: ctrl.groundCtrl.node.fen
          }
        })
      ]),
      h('div.pgn', [
        h('div.pair', [
          h('label.name', 'PGN'),
          h('div.copyable-wrap', [
            h('textarea.autoselect', {
              attrs: { readonly: true, spellCheck: false },
              hook: {
                insert(vnode) {
                  (vnode.elm as HTMLInputElement).value = pgnExport.renderFullTxt(ctrl);
                },
                postpatch: (_, vnode) => {
                  (vnode.elm as HTMLInputElement).value = pgnExport.renderFullTxt(ctrl);
                }
              }
            }),
            h('button.button.small', {
              attrs: {
                'data-icon': '4'
              },
              hook: bind('click', () => {
                if(!ctrl.opts.isMemberOrCoachOrTeam) {
                  window.lichess.memberIntro();
                } else {
                  const pgn = pgnExport.renderFullTxt(ctrl);
                  window.lichess.pubsub.emit('pgn.saveTo', {pgn: pgn, source: 'openingdb', rel: ctrl.openingdb.id});
                }
              })
            }, ['保存PGN', h('span.vTip', {attrs: {title: '会员可用'}}, 'V')])
          ])
        ])
      ])
    ]),
    continueWith(ctrl, 'fen'),
    continueWith(ctrl, 'pgn')
  ])
}

function continueWith(ctrl: OpeningDBCtrl, target: string) {
  const url = `/lobby?${target}=${target === 'fen' ? encodeNodeFen(ctrl) : encodePgn(ctrl)}`;
  return h(`div.continue-with.${target}.none`, [
    h('a.button', {
      attrs: {
        href: `${url}#ai`,
        rel: 'nofollow'
      }
    }, '和机器下棋'),
    h('a.button', {
      attrs: {
        href: `${url}#friend`,
        rel: 'nofollow'
      }
    }, '和朋友下棋')
  ])
}

function encodeNodeFen(ctrl: OpeningDBCtrl) {
  return ctrl.groundCtrl.node.fen.replace(/\s/g, '_');
}

function encodePgn(ctrl: OpeningDBCtrl) {
  const pgn = pgnExport.renderFullTxt(ctrl);
  return encodeURI(pgn);
}

function studyButton(ctrl: OpeningDBCtrl) {
  return h('form', {
    attrs: {
      method: 'post',
      action: '/study/as'
    },
    hook: bind('submit', e => {
      const pgnInput = (e.target as HTMLElement).querySelector('input[name=pgn]') as HTMLInputElement;
      if (pgnInput) pgnInput.value = pgnExport.renderFullTxt(ctrl);
    })
  }, [
    hiddenInput('pgn', ''),
    hiddenInput('orientation', ctrl.openingdb.orientation),
    hiddenInput('variant', 'standard'),
    hiddenInput('fen', ctrl.groundCtrl.node.fen),
    h('button.button.button-empty', {
      attrs: {
        type: 'submit',
        'data-icon': '4'
      }
    }, '研习')
  ]);
}

function hiddenInput(name: string, value: string) {
  return h('input', {
    attrs: { 'type': 'hidden', name, value }
  });
}
