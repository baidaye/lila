import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {Hooks} from "snabbdom/hooks";
import OpeningDBCtrl from '../ctrl';
import {boolSetting} from './boolSetting';

export default function renderMenuBox(ctrl: OpeningDBCtrl): VNode | null {
  return ctrl.groundCtrl.menuIsOpen ? h('div.sub-box', [
    boolSetting({
      name: '隐藏错误着法',
      id: 'blunder',
      checked: ctrl.groundCtrl.showBlunderNode(),
      change: ctrl.groundCtrl.toggleBlunderNode
    }, ctrl.redraw),
    boolSetting({
      name: '隐藏极少出现着法',
      id: 'jscx',
      checked: ctrl.groundCtrl.showJscxNode(),
      change: ctrl.groundCtrl.toggleJscxNode
    }, ctrl.redraw),
    boolSetting({
      name: '显示着法标记',
      id: 'sign',
      checked: ctrl.groundCtrl.showSignNode(),
      change: ctrl.groundCtrl.toggleSignNode
    }, ctrl.redraw),
    boolSetting({
      name: '箭头提示备选',
      id: 'shape',
      title: '鼠标悬停在备选着法，棋盘上显示绿色箭头',
      checked: ctrl.groundCtrl.showNodeShapes(),
      change: ctrl.groundCtrl.toggleNodeShapes
    }, ctrl.redraw),
    h('h2', '电脑分析'),
    h('div.sub-box__setting', [
      boolSetting({
        name: '启动',
        title: window.lichess.engineName,
        id: 'all',
        checked: ctrl.evalCtrl.showComputer(),
        disabled: false,
        change: ctrl.evalCtrl.toggleComputer
      }, ctrl.redraw),
    ].concat(
      ctrl.evalCtrl.showComputer() ? [
        boolSetting({
          name: '箭头指示最佳着',
          id: 'shapes',
          checked: ctrl.evalCtrl.showAutoShapes(),
          change: ctrl.evalCtrl.toggleAutoShapes
        }, ctrl.redraw),
        boolSetting({
          name: '局面指示器',
          id: 'gauge',
          checked: ctrl.evalCtrl.showGauge(),
          change: ctrl.evalCtrl.toggleGauge
        }, ctrl.redraw),
        boolSetting({
          name: '无限分析',
          title: '取消深度限制，您的电脑可能会发热。',
          id: 'infinite',
          checked: ctrl.evalCtrl.getCeval().infinite(),
          change: ctrl.evalCtrl.cevalSetInfinite
        }, ctrl.redraw),
        (id => {
          const max = 5;
          return h('div.setting', [
            h('label', { attrs: { 'for': id } }, '多线分析'),
            h('input#' + id, {
              attrs: {
                type: 'range',
                min: 1,
                max,
                step: 1
              },
              hook: rangeConfig(
                () => parseInt(ctrl.evalCtrl.getCeval()!.multiPv()),
                ctrl.evalCtrl.cevalSetMultiPv)
            }),
            h('div.range_value', ctrl.evalCtrl.getCeval().multiPv() + ' / ' + max)
          ]);
        })('openingdb-multipv')
      ] : []
    ))
  ]) : null;
}


function rangeConfig(read: () => number, write: (value: number) => void): Hooks {
  return {
    insert: vnode => {
      const el = vnode.elm as HTMLInputElement;
      el.value = '' + read();
      el.addEventListener('input', _ => write(parseInt(el.value)));
      el.addEventListener('mouseout', _ => el.blur());
    }
  };
}


