import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import OpeningDBCtrl from '../ctrl';
import * as control from '../control';
import {bindMobileMousedown, onInsert} from '../util';

export function renderControls(ctrl: OpeningDBCtrl): VNode {
  const canJumpPrev = ctrl.groundCtrl.path !== '',
    canJumpNext = !!ctrl.groundCtrl.node.children[0];
  return h('div.openingdb__controls', {
    hook: onInsert(el => {
      bindMobileMousedown(el, e => {
        const action = dataAct(e);
        if (action === 'prev') control.prev(ctrl);
        else if (action === 'next') control.next(ctrl);
        else if (action === 'first') control.first(ctrl);
        else if (action === 'last') control.last(ctrl);
      }, ctrl.redraw);
    })
  }, [
    h('div.features', [
      h('button.fbt', {
        attrs: {
          title: '翻转棋盘',
          'data-icon': 'B'
        },
        class: {
          hidden: ctrl.groundCtrl.menuIsOpen,
          active: ctrl.groundCtrl.featureActive === 'flip'
        },
        hook: onInsert(el => {
          bindMobileMousedown(el, () => {
            ctrl.groundCtrl.flip();
          })
        })
      })
    ]),
    h('div.jumps', [
      jumpButton('W', 'first', canJumpPrev),
      jumpButton('Y', 'prev', canJumpPrev),
      jumpButton('X', 'next', canJumpNext),
      jumpButton('V', 'last', canJumpNext)
    ]),
    h('button.fbt', {
      class: { active: ctrl.groundCtrl.menuIsOpen },
      attrs: {
        title: '菜单',
        'data-act': 'menu',
        'data-icon': '['
      },
      hook: onInsert(el => {
        bindMobileMousedown(el, () => {
          ctrl.groundCtrl.toggleMenuBox();
        })
      })
    })
  ]);
}

function dataAct(e) {
  return e.target.getAttribute('data-act') || e.target.parentNode.getAttribute('data-act');
}

function jumpButton(icon, effect, enabled: boolean) {
  return h('button.fbt', {
    class: {
      disabled: !enabled
    },
    attrs: {
      disabled: !enabled,
      'data-act': effect,
      'data-icon': icon
    }
  });
}
