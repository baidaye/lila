import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {defined} from 'common';
import throttle from 'common/throttle';
import {renderEval as normalizeEval} from 'chess';
import {path as treePath, ops as treeOps} from 'tree';
import { plyToTurn } from '../util';
import contextMenu from './contextMenu';
import {MaybeVNodes} from '../interfaces';
import OpeningDBCtrl from "../ctrl";
import { empty } from 'common';

export interface Ctx {
  ctrl: OpeningDBCtrl;
  showComputer: boolean;
  showGlyphs: boolean;
  showEval: boolean;
  truncateComments: boolean;
  currentPath: Tree.Path;
}

export interface Opts {
  parentPath: Tree.Path;
  isMainline: boolean;
  inline?: Tree.Node;
  withIndex?: boolean;
  truncate?: number;
}

const autoScroll = throttle(150, (ctrl: OpeningDBCtrl, el: HTMLElement) => {
  let cont = el.parentNode as HTMLElement;
  let target = el.querySelector('.active') as HTMLElement;
  if (!target) {
    cont.scrollTop = ctrl.groundCtrl.path === treePath.root ? 0 : 99999;
    return;
  }
  cont.scrollTop = target.offsetTop - cont.offsetHeight / 2 + target.offsetHeight;
});

function renderIndex(ply: number, withDots: boolean): VNode {
  return h('index', plyToTurn(ply) + (withDots ? (ply % 2 === 1 ? '.' : '...') : ''));
}

function renderChildrenOf(ctx: Ctx, node: Tree.Node, opts: Opts): MaybeVNodes {
  const cs = node.children,
    main = cs[0];
  if (!main) return [];

  if (opts.isMainline) {
    const isWhite = main.ply % 2 === 1,
      commentTags = renderMainlineCommentsOf(ctx, main, true).filter(nonEmpty);
    if (!cs[1] && empty(commentTags) && !main.forceVariation) return ((isWhite ? [renderIndex(main.ply, false)] : []) as MaybeVNodes).concat(
      renderMoveAndChildrenOf(ctx, main, {
        parentPath: opts.parentPath,
        isMainline: true
      }) || []
    );
    const mainChildren = main.forceVariation ? undefined : renderChildrenOf(ctx, main, {
      parentPath: opts.parentPath + main.id,
      isMainline: true
    });
    const passOpts = {
      parentPath: opts.parentPath,
      isMainline: !main.forceVariation,
    };
    return (isWhite ? [renderIndex(main.ply, false)] : [] as MaybeVNodes).concat(
      main.forceVariation ? [] : [
        renderMoveOf(ctx, main, passOpts),
        isWhite ? emptyMove() : null
      ]).concat([
      h('interrupt', commentTags.concat(
        renderLines(ctx, main.forceVariation ? cs : cs.slice(1), {
          parentPath: opts.parentPath,
          isMainline: passOpts.isMainline,
        })
      ))
    ] as MaybeVNodes).concat(
      isWhite && mainChildren ? [
        renderIndex(main.ply, false),
        emptyMove()
      ] : []).concat(mainChildren || []);
  }
  if (!cs[1]) return renderMoveAndChildrenOf(ctx, main, opts);
  return renderInlined(ctx, cs, opts) || [renderLines(ctx, cs, opts)];
}

function renderInlined(ctx: Ctx, nodes: Tree.Node[], opts: Opts): MaybeVNodes | undefined {
  // only 2 branches
  if (!nodes[1] || nodes[2]) return;
  // only if second branch has no sub-branches
  if (treeOps.hasBranching(nodes[1], 6)) return;
  return renderMoveAndChildrenOf(ctx, nodes[0], {
    parentPath: opts.parentPath,
    isMainline: false,
    inline: nodes[1]
  });
}

function renderLines(ctx: Ctx, nodes: Tree.Node[], opts: Opts): VNode {
  return h('lines', {
    class: {single: !nodes[1]}
  }, nodes.map(function (n) {
    return h('line', renderMoveAndChildrenOf(ctx, n, {
      parentPath: opts.parentPath,
      isMainline: false,
      withIndex: true
    }));
  }));
}

function renderMoveOf(ctx: Ctx, node: Tree.Node, opts: Opts): VNode {
  return opts.isMainline ? renderMainlineMoveOf(ctx, node, opts) : renderVariationMoveOf(ctx, node, opts);
}

function renderMainlineMoveOf(ctx: Ctx, node: Tree.Node, opts: Opts): VNode {
  const path = opts.parentPath + node.id;
  return h('move', {
    attrs: {p: path},
    class: nodeClasses(ctx, node, path)
  }, renderMove(ctx, node));
}

function renderVariationMoveOf(ctx: Ctx, node: Tree.Node, opts: Opts): VNode {
  const withIndex = opts.withIndex || node.ply % 2 === 1;
  const path = opts.parentPath + node.id;

  let content: MaybeVNodes = [
    withIndex ? renderIndex(node.ply, true) : null,
    !!node.fromOpening ? node.san! : `*${node.san!}`
  ];
  if (node.glyphs) renderGlyphs(node.glyphs).forEach(g => content.push(g));

  return h('move', {
    attrs: {p: path},
    class: nodeClasses(ctx, node, path)
  }, content);
}

function nodeClasses(ctx: Ctx, node: Tree.Node, path: Tree.Path) {
  const glyphIds = ctx.showGlyphs && node.glyphs ? node.glyphs.map(g => g.id) : [];
  return {
    active: path === ctx.ctrl.groundCtrl.path,
    current: path === ctx.ctrl.groundCtrl.initialPath,
    inaccuracy: glyphIds.includes(6),
    mistake: glyphIds.includes(2),
    blunder: glyphIds.includes(4),
    fromOpening: !!node.fromOpening,
    'context-menu': path === ctx.ctrl.groundCtrl.contextMenuPath,
  };
}

function renderMoveAndChildrenOf(ctx: Ctx, node: Tree.Node, opts: Opts): MaybeVNodes {
  return ([renderMoveOf(ctx, node, opts)] as MaybeVNodes)
    .concat(renderInlineCommentsOf(ctx, node))
    .concat(opts.inline ? renderInline(ctx, opts.inline, opts) : null)
    .concat(renderChildrenOf(ctx, node, {
      parentPath: opts.parentPath + node.id,
      isMainline: opts.isMainline
    }) || []);
}

function renderInline(ctx: Ctx, node: Tree.Node, opts: Opts): VNode {
  return h('inline', renderMoveAndChildrenOf(ctx, node, {
    withIndex: true,
    parentPath: opts.parentPath,
    isMainline: false,
    truncate: opts.truncate
  }));
}

export function renderInlineCommentsOf(ctx: Ctx, node: Tree.Node): MaybeVNodes {
  if (empty(node.comments)) return [];
  return node.comments!.map(comment => {
    if ((comment.by === 'lichess' || comment.by === 'lichess.org' || comment.by === 'haichess' || comment.by === 'haichess.com') && !ctx.showComputer) return;
    const truncated = truncateComment(comment.text, 300, ctx);
    return h('comment', [
      node.comments![1] ? h('span.by', authorText(comment.by)) : null,
      ' ',
      truncated
    ]);
  }).filter(nonEmpty);
}

function renderMove(ctx: Ctx, node: Tree.Node): MaybeVNodes {
  const ev = node.eval || node.ceval || {};
  return [h('san', !!node.fromOpening ? node.san! : `*${node.san!}`)]
    .concat((node.glyphs && ctx.showGlyphs) ? renderGlyphs(node.glyphs) : [])
    .concat(ctx.showEval ? (
      defined(ev.cp) ? [renderEval(normalizeEval(ev.cp))] : (
        defined(ev.mate) ? [renderEval('#' + ev.mate)] : []
      )
    ) : []);
}

function emptyMove() {
  return h('move.empty', '...');
}

function renderGlyphs(glyphs): VNode[] {
  return glyphs.map(glyph => h('glyph', {
    attrs: { title: glyph.name }
  }, glyph.symbol));
}

function renderEval(e) {
  return h('eval', e);
}

function eventPath(e) {
  return e.target.getAttribute('p') || e.target.parentNode.getAttribute('p');
}

function renderMainlineCommentsOf(ctx: Ctx, node: Tree.Node, withColor: boolean): MaybeVNodes {
  if (empty(node.comments)) return [];

  return node.comments!.map(comment => {
    if ((comment.by === 'lichess' || comment.by === 'lichess.org' || comment.by === 'haichess' || comment.by === 'haichess.com') && !ctx.showComputer) return;

    //const by = node.comments![1] ? `<span class="by">${authorText(comment.by)}</span>` : '';
    const truncated = truncateComment(comment.text, 400, ctx);
    const colorClass = withColor ? (node.ply % 2 === 0 ? '.black ' : '.white ') : '';

    return h(`comment${colorClass}`, {
      class: {
        inaccuracy: comment.text.startsWith('轻微失误：'),
        mistake: comment.text.startsWith('错误：'),
        blunder: comment.text.startsWith('严重错误：'),
      }
    },[
      node.comments![1] ? h('span.by', authorText(comment.by)) : null,
      ' ',
      truncated
    ]);
  });
}

function authorText(author): string {
  if (!author) return 'Unknown';
  if (!author.name) return author;
  return author.name;
}

function truncateComment(text: string, len: number, ctx: Ctx) {
  return ctx.truncateComments && text.length > len ? text.slice(0, len - 10) + ' [...]' : text;
}

function nonEmpty(x: any): boolean {
  return !!x;
}

export function render(ctrl: OpeningDBCtrl): VNode | undefined {
  const root = ctrl.groundCtrl.tree.root;
  const ctx: Ctx = {
    ctrl: ctrl,
    showComputer: ctrl.evalCtrl.showComputer(),
    showGlyphs: ctrl.evalCtrl.showComputer(),
    showEval: ctrl.evalCtrl.showComputer(),
    truncateComments: false,
    currentPath: ctrl.groundCtrl.path
  };
  const commentTags = renderMainlineCommentsOf(ctx, root, false);
  return h(`div.tview2.tview2-column`, {
    hook: {
      insert: vnode => {
        const el = vnode.elm as HTMLElement;
        if (ctrl.groundCtrl.path !== treePath.root) {
          autoScroll(ctrl, el);
        }
        el.oncontextmenu = (e: MouseEvent) => {
          const path = eventPath(e);
          if (path !== null) {
            contextMenu(e, {
              path,
              root: ctrl
            });
          }
          ctrl.redraw();
          return false;
        };
        el.addEventListener('mousedown', (e: MouseEvent) => {
          if (defined(e.button) && e.button !== 0) return; // only touch or left click
          const path = eventPath(e);
          if (path) ctrl.groundCtrl.userJump(path);
          ctrl.redraw();
        });
      },
      postpatch: (_, vnode) => {
        if (ctrl.groundCtrl.autoScrollRequested) {
          autoScroll(ctrl, vnode.elm as HTMLElement);
          ctrl.groundCtrl.autoScrollRequested = false;
        }
      }
    }
  }, [
    empty(commentTags) ? null : h('interrupt', commentTags),
    ...(root.ply % 2 === 1 ? [
      renderIndex(root.ply, false),
      emptyMove()
    ] : []),
    ...renderChildrenOf(ctx, root, {
      parentPath: '',
      isMainline: true
    })
  ]);
}
