import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {bind, bindSubmit, onInsert, spinner} from '../util';
import OpeningDBCtrl from '../ctrl';
import * as ModalBuild from "./modal";

export default function renderMetadata(ctrl: OpeningDBCtrl): VNode | null {
  return h('form.form3.form-metadata', [
    h('div.form-name', [
      h('input#form-name.form-control', {
        attrs: {name: 'name', required: true, readonly: !ctrl.canWrite(), value: ctrl.openingdb.name},
        hook: onInsert<HTMLInputElement>(el => {
          el.onkeyup = el.onpaste = () => {
            ctrl.update()
          };
        })
      })
    ]),
    h('div.form-desc', [
      h('textarea#form-desc.form-control', {
        attrs: {name: 'desc', required: true, readonly: !ctrl.canWrite(), rows: 5},
        hook: onInsert<HTMLInputElement>(el => {
          el.onkeyup = el.onpaste = () => {
            ctrl.update()
          };
        })
      }, ctrl.openingdb.desc)
    ]),
    h('div.orientation', [
      h('label', '默认棋盘方向：'),
      h('div.radio-group', [
        h('span', [
          h('input', {
            attrs: {id: `orientation-white`, name: `orientation`, type: 'radio', readonly: !ctrl.canWrite(), disabled: !ctrl.canWrite(), value: 'white', checked: ctrl.openingdb.orientation === 'white'},
            hook: bind('change', () => {
              ctrl.openingdb.orientation = 'white';
              ctrl.update();
            })
          }),
          h('label', {attrs: {for: `orientation-white`}}, '白方')
        ]),
        h('span', [
          h('input', {
            attrs: {id: `orientation-black`, name: `orientation`, type: 'radio', readonly: !ctrl.canWrite(), disabled: !ctrl.canWrite(), value: 'black', checked: ctrl.openingdb.orientation === 'black'},
            hook: bind('change', () => {
              ctrl.openingdb.orientation = 'black';
              ctrl.update();
            })
          }),
          h('label', {attrs: {for: `orientation-black`}}, '黑方')
        ])
      ])
    ]),
    h('div.form-actions', [
      ctrl.canWrite() ? h('a.button.button-red.small.clean', {
        class: { disabled: (ctrl.openingdb.clean !== 0 || !ctrl.opts.isMemberOrCoachOrTeam) },
        hook: bind('click', () => {
          if(ctrl.opts.isMemberOrCoachOrTeam) {
            if(ctrl.openingdb.clean === 0) {
              if(confirm('加入“清理”队列，删除库中无法到达的孤立着法。清理完成后，您将收到通知。是否继续执行？')) {
                ctrl.setClean();
              }
            }
          } else {
            window.lichess.memberIntro();
          }
        })
      }, [cleanLabel(ctrl.openingdb.clean), h('span.vTip', {attrs: {title: '会员可用'}}, 'V')]) : null,
      ctrl.canWrite() ? h('a.button.button-green.small.copyFrom', {
        class: {
          disabled: (ctrl.openingdb.clean !== 0 || !ctrl.opts.isMemberOrCoachOrTeam)
        },
        hook: bind('click', () => {
          if(ctrl.opts.isMemberOrCoachOrTeam) {
            if(ctrl.openingdb.clean === 0) {
              ctrl.openCopyFromModal();
            }
          } else {
            window.lichess.memberIntro();
          }
        })
      }, ['同 步', h('span.vTip', {attrs: {title: '会员可用'}}, 'V')]) : null
    ])
  ])
}

function cleanLabel(clean: number) {
  switch (clean) {
    case 0:
      return '清 理';
      break;
    case 1:
      return '已加入清理队列';
      break;
    case 2:
      return '正在清理';
      break;
    default:
      return '清 理';
  }
}

export function renderCopyFromModal(ctrl: OpeningDBCtrl): VNode {
  return ModalBuild.modal({
    onClose: function() {
      ctrl.closeCopyFromModal();
    },
    class: 'copyFromModal',
    content: [
      h('h2', '同步开局库'),
      h('div.modal-content-body', [
        h('div.tip', '用目标库覆盖当前开局库，当前库中录入的着法、PGN、评注等都会被覆盖，无法恢复，请谨慎操作！'),
        ctrl.copyFroming ? h('div.copyFroming', [
          h('div.tip', '正在同步...'),
          spinner()
        ]) : h('form.form3.copyFromForm', {
          hook: bindSubmit(_ => {
            ctrl.copyFromSubmit();
          })
        }, [
          h('div.tabs-horiz', [
            makeTab(ctrl, 'mine', '我的', '我的'),
            !ctrl.opts.isCoachOrTeam ? makeTab(ctrl, 'system', '已购', '已购') : null
          ]),
          h('div.tabs-content', [
            renderOpeningdbs(ctrl, 'mine'),
            !ctrl.opts.isCoachOrTeam ? renderOpeningdbs(ctrl, 'system') : null
          ]),
          h('div.form-actions', [
            h('button.button.button-empty.small', {
              hook: bind('click', () => {
                ctrl.closeCopyFromModal();
              })
            }, '取消'),
            h('button.button.small', {
              attrs: { type: 'submit' },
              }, '确定')
          ])
        ])
      ])
    ]
  });
}

function renderOpeningdbs(ctrl: OpeningDBCtrl, tab: string) {
  return h(`div.content`, {
    class: {
      active: ctrl.copyFromTab === tab
    }
  }, [
    h('div.openingdb-scroll', [
      ctrl.copyFromOpeningdbs[tab].length === 0 ? noMore() : h('table.openingdb-list', [
        h('tbody', ctrl.copyFromOpeningdbs[tab].map(openingdb => {
          return h('tr', [
            h('td', [
              h('div.radio', [
                h('input', {
                  attrs: {
                    type: 'radio', name: 'openingdbId', id: `openingdbId-${openingdb.id}`, value: openingdb.id
                  },
                  hook: bind('click', _ => {
                  })
                }),
                h('label', { attrs: { for: `openingdbId-${openingdb.id}` }}, openingdb.name)
              ])
            ]),
            h('td', openingdb.nodes)
          ]);
        }))
      ])
    ]),
  ])
}

function noMore() {
  return h('div.no-more', '没有可用的开局库。');
}

function makeTab(ctrl: OpeningDBCtrl, key: string, name: string, title: string) {
  return h('span.' + key, {
    class: { active: ctrl.copyFromTab === key },
    attrs: { title },
    hook: bind('click', () => {
      ctrl.copyFromTab = key;
      ctrl.redraw();
    })
  }, name);
}
