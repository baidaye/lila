import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { Chessground } from 'chessground';
import { OpeningDBNode } from 'game';
import { bind, spinner } from '../util';
import OpeningDBCtrl from '../ctrl';

export default function renderTransfer(ctrl: OpeningDBCtrl): VNode {
  let node = ctrl.groundCtrl.getCurrentOpeningdbNode();
  if(node) {
    return h(`div.openingdb__transfer.${node.id}`, [
      ctrl.transferNodesLoading ? spinner() : ( ctrl.transferNodes.length ? renderBoards(ctrl) : h('div.empty', '没有更多了.'))
    ])
  } else {
    return h('div.openingdb__transfer.empty', '请选择着法.');
  }
}

function renderBoards(ctrl: OpeningDBCtrl) {
  return h('div.now-playing', ctrl.transferNodes.map(node => {
    return renderBoard(ctrl, node)
  }));
}

function renderBoard(ctrl: OpeningDBCtrl, node: OpeningDBNode) {
  let currentOpeningdbNode = ctrl.groundCtrl.getCurrentOpeningdbNode();
  return h('a.' + node.id, {
    attrs: {target: '_blank', href: `/resource/openingdb/${ctrl.openingdb.id}/node?nodeId=${encodeURI(node.id)}`},
    class: { active: (!!currentOpeningdbNode && node.id == currentOpeningdbNode.id) },
    hook: bind('mousedown', _ => {})
  }, [
    h('div.mini-board.cg-wrap', {
      hook: {
        insert(vnode) {
          Chessground(vnode.elm as HTMLElement, {
            coordinates: false,
            drawable: { enabled: false, visible: false },
            resizable: false,
            viewOnly: true,
            orientation: ctrl.openingdb.orientation,
            fen: node.fen,
            lastMove: uciToLastMove(node.uci)
          });
        }
      }
    }),
    h('div.btm', [
      h('div.name', (node.shortName || '-')),
      h('div.san', node.san)
    ]),
  ]);
}

function uciToLastMove(lm?: string): Key[] | undefined {
  return lm ? ([lm[0] + lm[1], lm[2] + lm[3]] as Key[]) : undefined;
}
