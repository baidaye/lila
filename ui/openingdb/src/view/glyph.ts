import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {bind, spinner} from '../util';
import OpeningDBCtrl from '../ctrl';

export default function renderGlyphsBox(ctrl: OpeningDBCtrl): VNode {
  let node = ctrl.groundCtrl.getCurrentOpeningdbNode();
  if(node) {
    return h(`div.openingdb__glyphs.${node.id}` + (ctrl.glyphs ? '' : '.empty'),ctrl.glyphs ? [
        h('div.move', ctrl.glyphs.move.filter(g => g.id !== 22).map(renderGlyph(ctrl))),
        h('div.frequency', ctrl.glyphs.frequency.concat(ctrl.glyphs.special).concat(ctrl.glyphs.puzhao).map(renderGlyph(ctrl))),
      ] : [h('div.openingdb__message', spinner())]
    );
  } else {
    return h('div.openingdb__glyphs.empty', '请选择着法.')
  }
}

function renderGlyph(ctrl: OpeningDBCtrl) {
  return function (glyph) {
    return h('a', {
      hook: bind('click', _ => {
        ctrl.toggleNodeGlyph(glyph);
        return false;
      }, ctrl.redraw),
      attrs: {'data-symbol': glyph.symbol},
      class: {
        active: !!ctrl.groundCtrl.node.glyphs && !!ctrl.groundCtrl.node.glyphs.find(g => g.id === glyph.id)
      }
    }, [
      glyph.name
    ]);
  };
}
