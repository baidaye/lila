import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import {onInsert} from '../util';
import OpeningDBCtrl from '../ctrl';

export default function renderCommentBox(ctrl: OpeningDBCtrl): VNode | undefined {
  let node = ctrl.groundCtrl.getCurrentOpeningdbNode();
  if(node) {
    return h(`form.form3.form-comment.${node.id}`, [
      h('div.form-shortName', [
        h('input#form-shortName.form-control', {
          attrs: {name: 'shortName', required: true, placeholder: '开局名称', readonly: !ctrl.canWrite(), value: (node.shortName || '')},
          hook: onInsert<HTMLInputElement>(el => {
            el.onkeyup = el.onpaste = () => {
              ctrl.setNodeShortName({shortName: el.value});
            };
          })
        })
      ]),
      h('div.form-name', [
        h('input#form-name.form-control', {
          attrs: {name: 'name', required: true, placeholder: '开局说明', readonly: !ctrl.canWrite(), value: (node.name || '')},
          hook: onInsert<HTMLInputElement>(el => {
            el.onkeyup = el.onpaste = () => {
              ctrl.setNodeName({name: el.value});
            };
          })
        })
      ]),
      h('div.form-comment', [
        h('textarea#form-comment.form-control', {
          attrs: {rows: 5, placeholder: '局面评注', readonly: !ctrl.canWrite()},
          hook: {
            insert(vnode) {
              const el = vnode.elm as HTMLInputElement;
              el.onkeyup = el.onpaste = () => {
                ctrl.setNodeCommnet({comment: el.value});
              };
            }
          }
        }, node.comments && node.comments.length ? node.comments[0].text : '')
      ])
    ]);
  } else {
    return h('form.form3.form-comment.empty', '请选择着法.')
  }
}

