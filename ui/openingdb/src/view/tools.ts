import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import {view as cevalView} from 'ceval';
import {render as treeView} from './tree';
import {bind, dataIcon} from '../util';
import OpeningDBCtrl from '../ctrl';

export function renderTools(ctrl: OpeningDBCtrl): VNode {
  const showCeval = ctrl.evalCtrl.showComputer();
  return h('div.openingdb__tools', {
    class: { showTool: ctrl.groundCtrl.showTool }
  }, [
    showCeval ? h('div.ceval-wrap',  {
      hook: bind('click', _ => {
        ctrl.groundCtrl.toggleShowTool();
      })
    }, [
      h('div.ceval-header', [
        cevalView.renderCeval(ctrl.evalCtrl),
        h('div.arrow', [
          h('i', { attrs: dataIcon(ctrl.groundCtrl.showTool ? 'R' : 'S') })
        ])
      ]),
      cevalView.renderPvs(ctrl.evalCtrl)
    ]) : null,
    renderAreplay(ctrl)
  ])
}

function renderAreplay(ctrl: OpeningDBCtrl) {
  return h('div.openingdb__moves.areplay', [
    treeView(ctrl)
  ]);
}


