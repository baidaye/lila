import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode';
import OpeningDBCtrl from '../ctrl';
import {bind, bindMobileMousedown, dataIcon, onInsert, spinner} from '../util';
import renderMenuBox from "./menus";

export default function renderOpeningDBNodes(ctrl: OpeningDBCtrl): VNode | null {
  let groundCtrl = ctrl.groundCtrl;
  let node = groundCtrl.node;
  let dbNodes = groundCtrl.openingdbNodeMap[groundCtrl.path];
  let filteredDBNodes = dbNodes ? dbNodes.filter(dbNode => {
    return (!groundCtrl.showBlunderNode() || !dbNode.glyphs || !dbNode.glyphs.find(g => g.id === 2 || g.id === 4)) &&
    (!groundCtrl.showJscxNode() || !dbNode.glyphs || !dbNode.glyphs.find(g => g.id === 202))
  }) : [];

  return h('div.openingdb__nodes',  {
    class: {
      showNode: ctrl.groundCtrl.showNode
    }
  }, [
    h('div.header', {
      hook: bind('click', _ => {
        ctrl.groundCtrl.toggleShowNode();
      })
    }, [
      h('div.title', [
        (node.id !== '') ? h('a.back', {
          attrs: { 'data-icon': 'i', title: '返回' },
          hook: bind('click', e => {
            e.stopPropagation();
            groundCtrl.goBack();
          })
        }, `${node.san ? node.san : '返回'}${ (node.opening && node.opening.eco) ? `（${node.opening.eco}）` : ''}`) : '备选着法'
      ]),
      h('i', { attrs: dataIcon(ctrl.groundCtrl.showNode ? 'R' : 'S') })
    ]),
    h('div.body', {
      class: {
        deleting: ctrl.deleting
      }
    }, [
      ctrl.groundCtrl.menuIsOpen ? renderMenuBox(ctrl) : (
        ctrl.groundCtrl.openingdbNodesLoading ? spinner() : (
          filteredDBNodes.length ? h('table.nodes', [
            h('tbody', filteredDBNodes.map(node => {
              return h('tr', {
                attrs: { 'data-uci': node.uci, title: node.name || '' },
                hook: {
                  insert: vnode => {
                    const el = vnode.elm as HTMLElement;
                    el.addEventListener('mouseover', _ => {
                      groundCtrl.setOpeningHovering($(el).attr('data-uci'));
                    });
                    el.addEventListener('mouseout', _ => {
                      groundCtrl.setOpeningHovering(null);
                    });
                    el.addEventListener('mousedown', e => {
                      groundCtrl.setOpeningHovering(null);
                      const uci = $(e.target as HTMLElement).parents('tr').attr('data-uci');
                      if (uci) groundCtrl.openingMove(uci);
                    });
                  }
                }
              }, [
                h('td', h('div.san', node.san)),
                h('td', node.glyphs ? node.glyphs.map(g => {
                  return h('span.symbol', g.symbol)
                }) : []),
                h('td', (node.shortName || '-')),
                ctrl.canWrite() ? h('td', [
                  h(`a.remove.${node.id}`, {
                    hook: onInsert(el => {
                      bindMobileMousedown(el, (e) => {
                        e.stopPropagation();
                        if(confirm('着法删除后，后续着法节点会不可见（数据库清理永久删除），请谨慎操作。')) {
                          ctrl.deleteNode(node)
                        }
                      })
                    })
                  }, '删除')
                ]) : null
              ]);
            }))
          ]) : h('div.empty', '没有更多了.')
        )
      )
    ])
  ]);
}

