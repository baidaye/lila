import throttle from 'common/throttle';
import {storedProp, StoredBooleanProp} from 'common/storage';
import * as speech from './speech';
import {sound} from './sound';
import * as chessUtil from 'chess';
import * as cg from 'chessground/types';
import {opposite} from 'chessground/util';
import {Api as CgApi} from 'chessground/api';
import {DrawShape} from 'chessground/draw';
import {Config as CgConfig} from 'chessground/config';
import {build as makeTree, path as treePath, ops as treeOps, TreeWrapper} from 'tree';
import {AnalyseData as GameData} from '../../analyse/src/interfaces';
import {OpeningDBNode} from 'game';
import {CgDests, JustCaptured, Hovering} from './interfaces';
import * as promotion from './view/promotion';
import keyboard from './keyboard';
import { toEPD } from './util'
import OpeningDBCtrl from './ctrl';
import * as xhr from './xhr';

export default class OpeningDBGroundCtrl {

  round: GameData;
  chessground: CgApi;
  tree: TreeWrapper;
  cgConfig: CgConfig;
  initialPath: Tree.Path;

  // current tree state
  path: Tree.Path;
  node: Tree.Node;
  nodeList: Tree.Node[];
  mainline: Tree.Node[];

  // state flags
  justPlayed?: string; // pos
  justDropped?: string; // role
  justCaptured?: JustCaptured;
  autoScrollRequested: boolean = false;
  onMainline: boolean = true;

  contextMenuPath?: Tree.Path;

  // display flags
  flipped: boolean = false;
  menuIsOpen: boolean = false;
  situationModal: boolean = false;
  situationActiveTab: string = 'fen';

  featureActive: string;
  sideTab: string = 'comment';

  showTool: boolean = true;
  showSide: boolean = true;
  showNode: boolean = true;

  inOpening: boolean = true;
  openingdbNodesLoading: boolean = true;
  openingdbNodeMap: {[path: string]: OpeningDBNode[]} = {};
  openingdbHovering: Hovering | null;
  pathLoaded: string[] = [];

  showBlunderNode: StoredBooleanProp = storedProp('show-blunder-node', false);
  showJscxNode: StoredBooleanProp = storedProp('show-jscx-node', false);
  showSignNode: StoredBooleanProp = storedProp('show-sign-node', true);
  showNodeShapes: StoredBooleanProp = storedProp('show-node-shapes', true);

  constructor(readonly ctrl: OpeningDBCtrl, readonly trans: Trans) {
    this.round = ctrl.opts.round;
    this.initBoard();
  }

  initBoard = (): void => {
    this.flipped = false;
    this.menuIsOpen = false;

    this.tree = makeTree(treeOps.reconstruct(this.round.treeParts));
    this.initialPath = treePath.root;
    this.setPath(this.initialPath);
    this.initNodes();
    this.showGround();

    keyboard(this.ctrl);

    setTimeout(() => {
      this.jumpToLast();
      this.getDests();
      this.ctrl.evalCtrl.startCeval();
    }, 500);
  };

  setPath = (path: Tree.Path): void => {
    this.path = path;
    this.nodeList = this.tree.getNodeList(path);
    this.node = treeOps.last(this.nodeList) as Tree.Node;
    this.mainline = treeOps.mainlineNodeList(this.tree.root);
    this.onMainline = this.tree.pathIsMainline(path);
  };

  initNodes = () => {
    let nodePathMap = this.ctrl.opts.nodePathMap;
    if(nodePathMap) {
      this.openingdbNodeMap = nodePathMap;
      Object.keys(nodePathMap).forEach(path => {
        let nodes = nodePathMap![path];
        if(nodes && nodes.length) {
          let dbNode = nodes[0];
          this.tree.updateAt(path + dbNode.uciId, (node) => {
            this.mergeNode(node, dbNode);
          });
        }
      });
    }
  };

  withCg = <A>(f: (cg: CgApi) => A): A | undefined => {
    if (this.chessground) {
      return f(this.chessground);
    }
  };

  showComments = (path: Tree.Path): void => {
    this.userJump(path);
    if(this.featureActive === 'comments') {
      this.featureActive = '';
    } else {
      this.featureActive = 'comments';
    }
    this.ctrl.redraw();
  };

  showGlyphs = (path: Tree.Path): void => {
    this.userJump(path);
    if(this.featureActive === 'glyphs') {
      this.featureActive = '';
    } else {
      this.featureActive = 'glyphs';
    }
    this.ctrl.redraw();
  };

  toggleBlunderNode = (v: boolean): void => {
    this.showBlunderNode(v);
    this.ctrl.redraw();
  };

  toggleJscxNode = (v: boolean): void => {
    this.showJscxNode(v);
    this.ctrl.redraw();
  };

  toggleSignNode = (v: boolean): void => {
    this.showSignNode(v);
    this.showGround();
  };

  toggleNodeShapes = (v: boolean): void => {
    this.showNodeShapes(v);
    this.ctrl.evalCtrl.resetAutoShapes();
    this.showGround();
  };

  toggleMenuBox = () => {
    this.menuIsOpen = !this.menuIsOpen;
    this.ctrl.redraw();
  };

  toggleShowSide = () => {
    this.showSide = !this.showSide;
    this.ctrl.redraw();
  };

  toggleShowNode = () => {
    this.showNode = !this.showNode;
    this.ctrl.redraw();
  };

  toggleShowTool = () => {
    this.showTool = !this.showTool;
    this.ctrl.redraw();
  };

  flip = () => {
    this.flipped = !this.flipped;
    this.chessground.set({
      orientation: this.bottomColor()
    });
    this.ctrl.redraw();
  };

  topColor = (): Color => {
    return opposite(this.bottomColor());
  };

  bottomColor = (): Color => {
    return this.flipped ? opposite(this.ctrl.openingdb.orientation) : this.ctrl.openingdb.orientation;
  };

  turnColor = (): Color => {
    return (this.node.ply % 2 === 0) ? 'white' : 'black';
  };

  uciToLastMove = (uci?: Uci): cg.Key[] | undefined => {
    if (!uci) return;
    if (uci[1] === '@') {
      return [uci.substr(2, 2), uci.substr(2, 2)] as cg.Key[];
    }
    return [uci.substr(0, 2), uci.substr(2, 2)] as cg.Key[];
  };

  showGround = (): void => {
    this.withCg(cg => {
      cg.set(this.makeCgOpts());
      this.ctrl.evalCtrl.setAutoShapes();

      if(this.ctrl.groundCtrl.showSignNode()) {
        if (this.node.shapes) {
          cg.setShapes(this.node.shapes as DrawShape[]);
        }
      } else {
        cg.setShapes([]);
      }
    });
  };

  getDests: () => void = throttle(800, () => {
    if (this.ctrl.sync) {
      this.ctrl.socket.sendAnaDests({
        variant: 'standard',
        fen: this.node.fen,
        path: this.path
      });
    }
    if(!this.pathLoaded.includes(this.path)) {
      this.getNextNodes();
    }
  });

  getNextNodes = () => {
    this.openingdbNodesLoading = true;
    this.ctrl.redraw();

    xhr.getNextNodes(this.ctrl.openingdb.id, this.node.fen).then((data) => {
      if(!this.ctrl.sync) {
        this.addDests(data.dests, this.path);
      }
      this.pathLoaded.push(this.path);
      this.openingdbNodeMap[this.path] = data.nodes;
      this.openingdbNodesLoading = false;
      this.ctrl.redraw();
    });
  };

  addDests(dests: string, path: Tree.Path): void {
    this.tree.addDests(dests, path);
    if (path === this.path) {
      this.showGround();
    }
  }

  getCurrentOpeningdbNodeComment = () => {
    let node = this.getCurrentOpeningdbNode();
    if(node && node.comments) {
      return node.comments.length;
    } else return 0;
  };

  getCurrentOpeningdbNode = () => {
    let path = treePath.init(this.path);
    return this.getOpeningdbNode(this.node, path);
  };

  getOpeningdbNode = (node: Tree.Node, path: string) => {
    return (this.openingdbNodeMap && this.openingdbNodeMap[path]) ? this.openingdbNodeMap[path].find(dbn => this.sameMove(node, dbn)) : undefined;
  };

  sameMove = (node: Tree.Node, dbNode: OpeningDBNode) => {
    const castling = (dbNode.san == node.san) && this.isCastling(dbNode.san);
    return dbNode.fen == toEPD(node.fen) && (dbNode.uci == node.uci || castling) && !dbNode.tmp;
  };

  isCastling = (san: string) => {
    return san.includes('O-O');
  };

  addOpeningdbNode = (dbNode: OpeningDBNode, path: string) => {
    let dbNodes: OpeningDBNode[] = this.openingdbNodeMap[path];
    let newdbNodes = (dbNodes && dbNodes.length) ? dbNodes.concat(dbNode) : [dbNode];
    this.openingdbNodeMap[path] = newdbNodes;
  };

  deleteOpeningdbNode = (dbNode: OpeningDBNode, path: string) => {
    let paths = Object.keys(this.openingdbNodeMap);
    paths.forEach((p) => {
      if(p === path) {
        this.openingdbNodeMap[p] = this.openingdbNodeMap[p].filter(n => n.id !== dbNode.id);
      } else if(p.startsWith(path)) {
        this.openingdbNodeMap[p] = [];
      }
    });
    this.pathLoaded = this.pathLoaded.filter(p => p === path || !p.startsWith(path));
  };

  makeCgOpts(): CgConfig {
    const node = this.node,
      color = this.turnColor(),
      dests = chessUtil.readDests(this.node.dests),
      drops = chessUtil.readDrops(this.node.drops),
      movableColor = ((dests && Object.keys(dests).length > 0) || drops === null || drops.length) ? color : undefined,
      config: CgConfig = {
        fen:  node.fen,
        turnColor: color,
        viewOnly: false,
        movable: {
          color: movableColor,
          dests: (movableColor === color ? (dests || {}) : {}) as CgDests
        },
        drawable: {
          enabled: !window.lichess.hasTouchEvents && this.showSignNode()
        },
        check: !!node.check,
        lastMove: this.uciToLastMove(node.uci)
      };
    if (!dests && !node.check) {
      // premove while dests are loading from server
      // can't use when in check because it highlights the wrong king
      config.turnColor = opposite(color);
      config.movable!.color = color;
    }
    config.orientation = this.bottomColor();
    config.selected =  undefined;
    this.cgConfig = config;
    return config;
  }

  autoScroll(): void {
    this.autoScrollRequested = true;
  }

  playedLastMoveMyself = () => !!this.justPlayed && !!this.node.uci && this.node.uci.startsWith(this.justPlayed);

  jumpToLast(): void {
    let len = this.mainline.length;
    this.jumpToPly(len);
  }

  jumpToPly(ply: Ply): void {
    let nodes = this.mainline.filter(n => n.ply <= ply);
    let path = treePath.fromNodeList(nodes);
    this.userJump(path);
    this.ctrl.redraw();
  }

  userJump = (path: Tree.Path): void => {
    this.withCg(cg => cg.selectSquare(null));
    this.jump(path);
  };

  jump(path: Tree.Path): void {
    const pathChanged = path !== this.path,
      isForwardStep = pathChanged && path.length == this.path.length + 2;
    this.setPath(path);
    this.showGround();
    this.getDests();

    if (pathChanged) {
      const playedMyself = this.playedLastMoveMyself();
      if (isForwardStep) {
        if (!this.node.uci) { // initial position
          sound.move();
        }
        else if (!playedMyself) {
          if (this.node.san!.includes('x')) {
            sound.capture();
          } else sound.move();
        }
        if (/\+|\#/.test(this.node.san!)) {
          sound.check();
        }
      }

      this.ctrl.evalCtrl.threatMode(false);
      this.ctrl.evalCtrl.ceval.stop();
      this.ctrl.evalCtrl.startCeval();
      if(this.sideTab === 'transfer') {
        this.ctrl.loadTransferNodes(this.node.fen);
      }
      speech.node(this.node);
    }
    this.justPlayed = this.justDropped = this.justCaptured = undefined;
    this.autoScroll();
    promotion.cancel(this.ctrl);

    if(!this.pathLoaded.includes(this.path)) {
      this.openingdbNodesLoading = true;
    }
    this.ctrl.redraw();
  }

  userMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured): void => {
    this.justPlayed = orig;
    this.justDropped = undefined;

    const piece = this.chessground.state.pieces[dest];
    const isCapture = capture || (piece && piece.role == 'pawn' && orig[0] != dest[0]);
    if(isCapture) {
      sound.capture();
    } else {
      sound.move()
    }
    if (!promotion.start(this.ctrl, orig, dest, capture, this.sendMove)) {
      this.sendMove(orig, dest, capture);
    }
  };

  sendMove = (orig: cg.Key, dest: cg.Key, capture?: JustCaptured, prom?: cg.Role): void => {
    const move: any = {
      orig,
      dest,
      variant: 'standard',
      fen: this.node.fen,
      path: this.path
    };
    if (capture) this.justCaptured = capture;
    if (prom) move.promotion = prom;
    this.ctrl.socket.sendAnaMove(move);
  };

  playUci(uci: Uci): void {
    const move = chessUtil.decomposeUci(uci);
    if (uci[1] === '@') this.chessground.newPiece({
      color: this.chessground.state.movable.color as Color,
      role: chessUtil.sanToRole[uci[0]]
    }, move[1]);
    else {
      const capture = this.chessground.state.pieces[move[1]];
      const promotion = move[2] && chessUtil.sanToRole[move[2].toUpperCase()];
      this.sendMove(move[0], move[1], capture, promotion);
    }
  }

  addNode(node: Tree.Node, path: Tree.Path) {
    let openingdbNode = this.getOpeningdbNode(node, path);
    this.mergeNode(node, openingdbNode);

    const newPath = this.tree.addNode(node, path);
    if (!newPath) {
      console.log("Can't addNode', node, path");
      return this.ctrl.redraw();
    }
    this.jump(newPath);

    if(this.ctrl.canWrite()) {
      const move = chessUtil.decomposeUci(node.uci!);
      const castlingMap = {"h1": "g1", "a1": "c1", "h8": "g8", "a8": "c8"};
      const dest = this.isCastling(node.san!) ? castlingMap[move[1]] : move[1];
      const piece = this.chessground.state.pieces[dest];
      const parentNode = this.tree.nodeAtPath(path);
      this.ctrl.syncMove(parentNode, node, piece!);
    }
  }

  deleteNode(path: Tree.Path){
    const node = this.tree.nodeAtPath(path);
    if (!node) return;
    this.tree.deleteNodeAt(path);

    if (treePath.contains(this.path, path)) {
      this.userJump(treePath.init(path));
    } else {
      this.jump(this.path);
    }
  };


  mergeNode(node: Tree.Node, openingdbNode: OpeningDBNode | undefined) {
    if(openingdbNode) {
      node.dests = undefined;
      //node.comments = openingdbNode.comments;
      node.glyphs = openingdbNode.glyphs;
      node.shapes = openingdbNode.shapes;
      node.fromOpening = !openingdbNode.tmp;
      node.opening = {
        eco: (openingdbNode.shortName || ''),
        name: (openingdbNode.name || '')
      }
    } else {
      node.opening = {
        eco: '',
        name: ''
      }
    }
  }

  setOpeningHovering(uci) {
    this.openingdbHovering = uci ? {
      fen: this.node.fen,
      uci,
    } : null;
    this.ctrl.evalCtrl.setAutoShapes();
  }

  openingMove(uci) {
    this.playUci(uci);
  }

  goBack() {
    this.deleteNode(this.path);
  }

  send = (t: string, d: any) => {
    this.ctrl.socket.send(t, d);
  };

  redrawGround = () => {
    setTimeout(() => {
      this.ctrl.groundCtrl.chessground.redrawAll();
    }, 500);
  };

}

