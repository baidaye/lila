import { Piece } from 'chessground/types';
import {path as treePath} from 'tree';
import {OpeningDB} from 'game';
import {OpeningDBOpts, Redraw} from './interfaces';
import {make as makeSocket, Socket} from './socket';
import OpeningDBGroundCtrl from './groundCtrl';
import OpeningDBEvalCtrl from './evalCtrl';
import throttle from 'common/throttle';
import { OpeningDBNode } from 'game';
import * as xhr from './xhr'
import * as pgnExport from './pgnExport';

export default class OpeningDBCtrl {

  opts: OpeningDBOpts;
  openingdb: OpeningDB;
  trans: Trans;
  socket: Socket;
  groundCtrl: OpeningDBGroundCtrl;
  evalCtrl: OpeningDBEvalCtrl;

  redirecting: boolean = false;
  glyphs: any;

  showEditorModal: boolean = false;
  editor: any;
  editorMove: any;

  transferNodes: OpeningDBNode[] = [];
  transferNodesLoading: boolean = false;

  searchNodes: OpeningDBNode[] = [];
  searchNodesUciFail: boolean = false;
  searchNodesLoading: boolean = false;

  sync: boolean = false;
  deleting: boolean = false;

  showCopyFromModal: boolean = false;
  copyFromTab: string = 'mine';
  copyFroming: boolean = false;
  copyFromOpeningdbs: any = {
    mine: [],
    system: []
  };

  constructor(opts: OpeningDBOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.openingdb = opts.openingdb;
    this.trans = window.lichess.trans(opts.i18n);
    this.socket = makeSocket(this.opts.socketSend, this);
    this.evalCtrl = new OpeningDBEvalCtrl(this, this.trans);
    this.groundCtrl = new OpeningDBGroundCtrl(this, this.trans);
  }

  canWrite = () => {
    return this.openingdb.members[this.opts.userId] && this.openingdb.members[this.opts.userId].canWrite;
  };

  isFull = () => {
    return this.openingdb.nodes >= 10000;
  };

  isCleaning = () => {
    return this.openingdb.clean === 2;
  };

  loadGlyphs = () => {
    if (!this.glyphs) {
      xhr.glyphs().then(gs => {
        this.glyphs = gs;
        this.redraw();
      });
    }
  };

  loadTransferNodes = (fen) => {
    this.transferNodesLoading = true;
    this.redraw();

    xhr.transferNodes(this.openingdb.id, fen).then((data) => {
      this.transferNodes = data;
      this.transferNodesLoading = false;
      this.redraw();
    });
  };

  loadSearchNodes = () => {
    let $form = $('.openingdb__search_form');
    let fullUci = $form.find('input[name=fullUci]').val();
    if(!fullUci || fullUci.length === 5) {
      let data = $form.serialize();
      this.searchNodesLoading = true;
      this.searchNodesUciFail = false;
      this.redraw();
      xhr.searchNodes(this.openingdb.id, data).then((data) => {
        this.searchNodes = data;
        this.searchNodesLoading = false;
        this.redraw();
      });
    } else {
      this.searchNodesUciFail = true;
      this.redraw();
    }
  };

  update = throttle(500, () => {
    let data = $('.form-metadata').serialize();
    xhr.update(this.openingdb.id, data).then(() => {
      this.groundCtrl.showGround();
      this.redraw();
    });
  });

  setNodeName = throttle(500, (d: any) => {
    let openingdbNode = this.groundCtrl.getCurrentOpeningdbNode();
    if(openingdbNode) {
      let nodeId = openingdbNode.id;
      xhr.updateNode(this.openingdb.id, nodeId, d, 'setName').then(() => {
        this.groundCtrl.getCurrentOpeningdbNode()!.name = d.name;
        this.groundCtrl.tree.updateAt(this.groundCtrl.path, function(node) {
          node.opening!.name = d.name;
        });
        this.redraw();
      });
    }
  });

  setNodeShortName = throttle(500, (d: any) => {
    let openingdbNode = this.groundCtrl.getCurrentOpeningdbNode();
    if(openingdbNode) {
      let nodeId = openingdbNode.id;
      xhr.updateNode(this.openingdb.id, nodeId, d, 'setShortName').then(() => {
        this.groundCtrl.getCurrentOpeningdbNode()!.shortName = d.shortName;
        this.groundCtrl.tree.updateAt(this.groundCtrl.path, function(node) {
          node.opening!.eco = d.shortName;
        });
        this.redraw();
      });
    }
  });

  setNodeCommnet = throttle(500, (d: any) => {
    let openingdbNode = this.groundCtrl.getCurrentOpeningdbNode();
    if(openingdbNode) {
      let nodeId = openingdbNode.id;
      xhr.updateNode(this.openingdb.id, nodeId, d, 'setComment').then((data) => {
        this.groundCtrl.getCurrentOpeningdbNode()!.comments = data.comments;
/*        this.groundCtrl.tree.updateAt(this.groundCtrl.path, function(node) {
          node.comments = data.comments;
        });*/
        this.redraw();
      });
    }
  });

  toggleNodeGlyph = throttle(500, (glyph: Tree.Glyph) => {
    if(this.canWrite()) {
      let openingdbNode = this.groundCtrl.getCurrentOpeningdbNode();
      if(openingdbNode) {
        let nodeId = openingdbNode.id;
        xhr.updateNode(this.openingdb.id, nodeId, {glyphId: glyph.id}, 'toggleGlyph').then((data) => {
          this.groundCtrl.getCurrentOpeningdbNode()!.glyphs = data.glyphs;
          this.groundCtrl.tree.setGlyphsAt(data.glyphs, this.groundCtrl.path);
          this.redraw();
        });
      }
    }
  });

  setShapes = (shapes) => {
    if(this.canWrite()) {
      let openingdbNode = this.groundCtrl.getCurrentOpeningdbNode();
      if(openingdbNode) {
        let nodeId = openingdbNode.id;
        xhr.updateNode(this.openingdb.id, nodeId, {"shapes": JSON.stringify(shapes)}, 'setShapes').then((data) => {
          this.groundCtrl.getCurrentOpeningdbNode()!.shapes = data.shapes;
          this.groundCtrl.tree.setShapes(data.shapes, this.groundCtrl.path);
          this.redraw();
        });
      }
    }
  };

  syncChange = (checked) => {
    this.sync = checked;
    if(checked) {
      let modalHtml =
        `<div class="modal-content modal-startSync">
          <h2>开启手工录入</h2>
          <div style="text-align: left">
            <p>开启手工录入后，棋盘上录入的新着法会保存进开局库。</p>
            <p><strong>注意：</strong>手工录入的着法，无法在“开局查询”中按pgn查找。</p>
          </div>
          <div class="form-actions">
            <a class="cancel">取消</a>
            <button class="button start small">确定</button>
          </div>
        </div>`;

      $.modal($(modalHtml));
      $('.cancel, .close').click(() => {
        $.modal.close();

        this.sync = false;
        this.redraw();
        this.groundCtrl.getDests();
      });

      $('.modal-startSync .start').click(() => {
        $.modal.close();
        this.groundCtrl.getDests();
      });
    } else {
      this.groundCtrl.getDests();
    }
    this.redraw();
  };

  syncMove = (parentNode: Tree.Node, node: Tree.Node, piece: Piece) => {
    if(this.canWrite() && this.sync && !node.fromOpening) {
      const d = {
        uci : node.uci,
        san : node.san,
        fen: node.fen,
        prevFen: parentNode.fen,
        check: node.check,
        role: piece.role,
        color: piece.color,
        pgn: pgnExport.renderFullTxt(this)
      };

      xhr.addNode(this.openingdb.id, d).then((newOpeningdbNode: OpeningDBNode) => {
        let path = this.groundCtrl.path;
        let parentPath = treePath.init(this.groundCtrl.path);
        this.groundCtrl.addOpeningdbNode(newOpeningdbNode, parentPath);
        this.groundCtrl.tree.updateAt(path, (node) => {
          this.groundCtrl.mergeNode(node, newOpeningdbNode);
        });
        this.redraw();
      }).fail(function(d) {
        alert(d.responseJSON.error)
      });
    }
  };

  deleteNode = (dbNode) => {
    if(this.canWrite()) {
      this.deleting = true;
      this.redraw();
      xhr.updateNode(this.openingdb.id, dbNode.id, {}, 'delete').then(() => {
        let path = this.groundCtrl.path + dbNode.uciId;
        if(path) {
          this.groundCtrl.deleteNode(path);
          this.groundCtrl.deleteOpeningdbNode(dbNode, this.groundCtrl.path);
        }
        this.deleting = false;
        this.redraw();
      });
    }
  };

  setClean = () => {
    if(this.openingdb.clean === 0) {
      xhr.setClean(this.openingdb.id).then(() => {
        this.openingdb.clean = 1;
        this.redraw();
      });
    }
  };

  onEditorMove = (d) => {
    const letters = { pawn: 'p', rook: 'r', knight: 'n', bishop: 'b', queen: 'q', king: 'k' };
    this.editorMove = {color: d.piece.color, role: letters[d.piece.role], uci: `${d.orig}${d.dest}`};
    this.redraw();
  };

  openCopyFromModal = () => {
    this.showCopyFromModal = true;
    this.redraw();
    xhr.loadOpeningdbs().then((res) => {
      if(res && res.mine) {
        res.mine = res.mine.filter(o => o.id !== this.openingdb.id);
      }
      this.copyFromOpeningdbs = res;
      this.redraw();
    });
  };

  closeCopyFromModal = () => {
    this.showCopyFromModal = false;
    this.redraw();
  };

  copyFromSubmit = () => {
    if(confirm('当前库中录入的着法、PGN、评注等都会被覆盖，是否继续操作？')) {
      let $form = $(`.copyFromForm`);
      let openingdbId = $form.find('input[name=openingdbId]:checked').val();
      if(!openingdbId) {
        alert('请选择开局库');
        return;
      }

      this.copyFroming = true;
      this.redraw();
      xhr.copyFrom(openingdbId, this.openingdb.id).then(() => {
        alert('同步完成');
        location.reload();
      });
    }
  };

}

