import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {Hooks} from 'snabbdom/hooks'
import { fixCrazySan } from 'chess';
import * as Toastify from 'toastify-js'

export const hasTouchEvents = 'ontouchstart' in window;

export const assetsUrl = $('body').data('asset-url') + '/assets';

export const domain = window.location.host;

export const gameRegex = new RegExp(`(?:https?://)${domain}/(?:embed/)?(\\w{8})(?:(?:/(white|black))|\\w{4}|)(#\\d+)?$`);

export function nodeFullName(node: Tree.Node) {
  if (node.san) return plyToTurn(node.ply) + (
    node.ply % 2 === 1 ? '.' : '...'
  ) + ' ' + fixCrazySan(node.san);
  return 'Initial position';
}

export function toEPD(fen: Fen) {
  return fen.split(' ').slice(0, 4).join(' ');
}

export function plyToTurn(ply: number): number {
  return Math.floor((ply - 1) / 2) + 1;
}

export function plural(noun: string, nb: number): string {
  return nb + ' ' + (nb === 1 ? noun : noun/* + 's'*/);
}

export const uciToLastMove = (uci: string | undefined): [Key, Key] | undefined =>
  uci ? [uci.substr(0, 2) as Key, uci.substr(2, 2) as Key] : undefined;

export function bindSubmit(f: (e: Event) => any, redraw?: () => void): Hooks {
  return bind('submit', e => {
    e.preventDefault();
    return f(e);
  }, redraw);
}

export function bindMobileMousedown(el: HTMLElement, f: (e: Event) => any, redraw?: () => void) {
  el.addEventListener(hasTouchEvents ? 'touchstart' : 'mousedown', e => {
    f(e);
    e.preventDefault();
    if (redraw) redraw();
  })
}

export function bind(eventName: string, f: (e: Event) => any, redraw?: () => void): Hooks {
  return onInsert(el =>
    el.addEventListener(eventName, e => {
      const res = f(e);
      if (redraw) redraw();
      return res;
    })
  );
}

export function onInsert<A extends HTMLElement>(f: (element: A) => void): Hooks {
  return {
    insert: vnode => f(vnode.elm as A)
  };
}

export function spinner(): VNode {
  return h('div.spinner', [
    h('svg', {attrs: {viewBox: '0 0 40 40'}}, [
      h('circle', {
        attrs: {cx: 20, cy: 20, r: 18, fill: 'none'},
      }),
    ]),
  ]);
}

export function dataIcon(icon: string) {
  return {
    'data-icon': icon
  };
}

export function iconTag(icon: string) {
  return h('i', { attrs: dataIcon(icon) });
}

export function option(value: string, current: string | undefined, name: string) {
  return h('option', {
    attrs: {
      value: value,
      selected: value === current
    },
  }, name);
}

export function readLocalStorage(key) {
  // allow reading from storage to retrieve previous support results
  // even while the document does not have focus
  let data;
  try {
    data = window.localStorage && window.localStorage.getItem(key);
    data = data ? JSON.parse(data) : {};
  } catch (e) {
    data = {};
  }
  return data;
}

export function scrollTo(el: HTMLElement | undefined, target: HTMLElement |  null) {
  if (el && target) el.scrollTop = target.offsetTop - el.offsetHeight / 2 + target.offsetHeight / 2;
}

export function toastify(type, message) {
  Toastify({
    text: message,
    className: `toastify-${type}`,
    offset: {
      y: 100
    },
  }).showToast();
}

export function isUndefined(data) {
  return typeof data === 'undefined';
}
