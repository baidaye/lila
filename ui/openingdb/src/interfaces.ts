import {VNode} from 'snabbdom/vnode'
import * as cg from 'chessground/types';
import { OpeningDB, OpeningDBNode } from 'game';
import {Pref} from '../../round/src/interfaces';
import {AnalyseData as GameData} from '../../analyse/src/interfaces';

export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface OpeningDBOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  isCoachOrTeam: boolean;
  isMemberOrCoachOrTeam: boolean;
  openingdb: OpeningDB;
  nodePathMap?: {[path: string]: OpeningDBNode[]};
  round: GameData;
  pref: Pref;
}

export interface CgDests {
  [key: string]: cg.Key[]
}

export interface JustCaptured extends cg.Piece {
  promoted?: boolean;
}

export type Redraw = () => void

export interface Hovering {
  fen: Fen;
  uci: Uci;
}
