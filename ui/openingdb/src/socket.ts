import OpeningDBCtrl from './ctrl';

interface Handlers {
  [key: string]: any;
}

interface Req {
  [key: string]: any;
}

export interface Socket {
  send: SocketSend;
  receive(type: string, data: any): boolean;
  sendAnaDests(req: Req): void;
  sendAnaMove(req: Req): void;
}

export function make(send: SocketSend, ctrl: OpeningDBCtrl): Socket {

  let anaMoveTimeout;
  let anaDestsTimeout;

  const handlers: Handlers = {
    dests(data) {
      clearTimeout(anaDestsTimeout);
      ctrl.groundCtrl.addDests(data.dests, data.path);
    },
    destsFailure(data) {
      console.log(data);
      clearTimeout(anaDestsTimeout);
    },
    node(d) {
      clearTimeout(anaMoveTimeout);
      ctrl.groundCtrl.addNode(d.node, d.path);
    },
  };

  function sendAnaDests(req) {
    clearTimeout(anaDestsTimeout);
    send('anaDests', req);
    anaDestsTimeout = setTimeout(function() {
      sendAnaDests(req);
    }, 3000);
  }

  function sendAnaMove(req) {
    clearTimeout(anaMoveTimeout);
    send('anaMove', req);

    anaMoveTimeout = setTimeout(() => sendAnaMove(req), 3000);
  }

  return {
    receive(type: string, data: any): boolean {
      const handler = handlers[type];
      if (handler) handler(data);
      return true;
    },
    sendAnaDests,
    sendAnaMove,
    send
  };
}
