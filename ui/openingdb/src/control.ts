import OpeningDBCtrl from './ctrl';
import {path as treePath} from 'tree';

export function canGoForward(ctrl: OpeningDBCtrl): boolean {
  return ctrl.groundCtrl.node.children.length > 0;
}

export function next(ctrl: OpeningDBCtrl): void {
  const child = ctrl.groundCtrl.node.children[0];
  if(child) {
    ctrl.groundCtrl.userJump(ctrl.groundCtrl.path + child.id);
  }
}

export function prev(ctrl: OpeningDBCtrl): void {
  ctrl.groundCtrl.userJump(treePath.init(ctrl.groundCtrl.path));
}

export function last(ctrl: OpeningDBCtrl): void {
  ctrl.groundCtrl.userJump(treePath.fromNodeList(ctrl.groundCtrl.mainline));
}

export function first(ctrl: OpeningDBCtrl): void {
  ctrl.groundCtrl.userJump(treePath.root);
}

