import notify from 'common/notification';
import throttle from 'common/throttle';
import * as xhr from './xhr';
import { scroller } from './view/scroller';
import Socket from "./socket";
import {
  MessageData,
  Contact,
  Convo,
  Message,
  LastMessage,
  Search,
  SearchResult,
  Typing,
  Pane,
  Redraw,
  User,
  MessageOpts
} from './interfaces';

export default class MessageCtrl {
  data: MessageData;
  search: Search = {
    input: '',
  };
  pane: Pane;
  loading = false;
  socket: Socket;
  msgsPerPage = 100;
  canGetMoreSince?: Date;
  typing?: Typing;
  textStore?: LichessStorage;

  constructor(opts: MessageOpts, readonly redraw: Redraw) {
    this.data = this.upgradeData(opts.data);
    this.pane = this.data.convo ? 'convo' : 'side';
    this.socket = new Socket(opts.socketSend, this);
    if (this.data.convo) this.onLoadConvo(this.data.convo);
    window.addEventListener('focus', this.setRead);
  }

  convoUser = () => {
    return this.data.convo ? this.data.convo.user.id : undefined
  };

  convoMessages = () => {
    return this.data.convo ? this.data.convo.messages : undefined
  };

  connected = () => this.socket.connected();

  openConvo = (userId: string) => {
    if (this.convoUser() != userId) {
      this.data.convo = undefined;
      this.loading = true;
    }
    xhr.loadConvo(userId).then(res => {
      this.data = this.upgradeData(res);
      this.search.result = undefined;
      this.loading = false;
      if (this.data.convo) {
        history.replaceState({ contact: userId }, '', `/inbox/${this.data.convo.user.name}`);
        this.onLoadConvo(this.data.convo);
        this.redraw();
      } else this.showSide();
    });
    this.pane = 'convo';
    this.redraw();
  };

  showSide = () => {
    this.pane = 'side';
    this.redraw();
  };

  getMore = () => {
    if (this.data.convo && this.canGetMoreSince) {
      xhr.getMore(this.data.convo.user.id, this.canGetMoreSince).then(res => {
        const data = this.upgradeData(res);
        if (!this.data.convo || !data.convo || data.convo.user.id != this.data.convo.user.id || !data.convo.messages[0]) {
          return;
        }
        if (data.convo.messages[0].date >= this.data.convo.messages[this.data.convo.messages.length - 1].date) {
          return;
        }
        this.data.convo.messages = this.data.convo.messages.concat(data.convo.messages);
        this.onLoadMessages(data.convo.messages);
        this.redraw();
      });
    }
    this.canGetMoreSince = undefined;
    this.redraw();
  };

  private onLoadConvo = (convo: Convo) => {
    this.textStore = window.lichess.storage.make(`msg:area:${convo.user.id}`);
    this.onLoadMessages(convo.messages);
    if (this.typing) {
      clearTimeout(this.typing.timeout);
      this.typing = undefined;
    }
    setTimeout(this.setRead, 500);
  };

  private onLoadMessages = (messages: Message[]) => {
    const oldFirstMessage = messages[this.msgsPerPage - 1];
    this.canGetMoreSince = oldFirstMessage ? oldFirstMessage.date : undefined;
  };

  post = (text: string) => {
    if (this.data.convo) {
      this.socket.sendMessage(this.data.convo.user.id, text);
      const msg: LastMessage = {
        text,
        user: this.data.me.id,
        date: new Date(),
        read: true,
      };
      this.data.convo.messages.unshift(msg);
      const contact = this.currentContact();
      if (contact) this.addMessage(msg, contact);
      else
        setTimeout(
          () =>
            xhr.loadContacts().then(res => {
              const data = this.upgradeData(res);
              this.data.contacts = data.contacts;
              this.redraw();
            }),
          1000
        );
      scroller.enable(true);
      this.redraw();
    }
  };

  receive = (msg: LastMessage) => {
    const contact = this.findContact(msg.user);
    this.addMessage(msg, contact);
    if (contact) {
      let redrawn = false;
      if (msg.user == this.convoUser()) {
        if(this.data.convo) {
          this.data.convo.messages.unshift(msg);
        }
        if (document.hasFocus()) redrawn = this.setRead();
        else this.notify(contact, msg);
        this.receiveTyping(msg.user, true);
      }
      if (!redrawn) this.redraw();
    } else
      xhr.loadContacts().then(res => {
        const data = this.upgradeData(res);
        this.data.contacts = data.contacts;
        this.notify(this.findContact(msg.user)!, msg);
        this.redraw();
      });
  };

  private addMessage = (msg: LastMessage, contact?: Contact) => {
    if (contact) {
      contact.lastMessage = msg;
      this.data.contacts = [contact].concat(this.data.contacts.filter(c => c.user.id != contact.user.id));
    }
  };

  private findContact = (userId: string): Contact | undefined => this.data.contacts.find(c => c.user.id == userId);

  private currentContact = (): Contact | undefined => this.data.convo && this.findContact(this.data.convo.user.id);

  private notify = (contact: Contact, msg: Message) => {
    notify(() => `${contact.user.name}: ${msg.text}`);
  };

  searchInput = (q: string) => {
    this.search.input = q;
    if (q.length >= 2)
      xhr.search(q).then(res => {
        const data = ({
          ...res,
          contacts: res.contacts.map(this.upgradeContact),
        } as SearchResult);

        this.search.result = this.search.input[1] ? data : undefined;
        this.redraw();
      });
    else {
      this.search.result = undefined;
      this.redraw();
    }
  };

  setRead = () => {
    const contact = this.currentContact();
    const msg = contact ? contact.lastMessage : undefined;
    if (msg && msg.user != this.data.me.id) {
      window.lichess.pubsub.emit('notify-app.set-read', msg.user);
      if (msg.read) return false;
      msg.read = true;
      this.socket.setRead(msg.user);
      this.redraw();
      return true;
    }
    return false;
  };

  delete = () => {
    const userId = this.convoUser();
    if (userId)
      xhr.del(userId).then(res => {
        this.data = this.upgradeData(res);
        this.redraw();
        history.replaceState({}, '', '/inbox');
        if('ontouchstart' in window) {
          this.showSide();
        }
      });
  };

  report = () => {
    const convo = this.data.convo;
    if (convo && convo.user) {
      const messages = this.convoMessages();
      if(messages) {
        const fullText = messages.find(m => m.user != this.data.me.id);
        if(fullText) {
          const text = fullText.text.slice(0, 140);
          xhr.report(convo.user.name, text).then(_ =>
            alert('举报成功')
          );
        }
      }
    }
  };

  block = () => {
    const userId = this.convoUser();
    if (userId) xhr.block(userId).then(() => this.openConvo(userId));
  };

  unblock = () => {
    const userId = this.convoUser();
    if (userId) xhr.unblock(userId).then(() => this.openConvo(userId));
  };

  changeBlockBy = (userId: string) => {
    if (userId == this.convoUser()) this.openConvo(userId);
  };

  sendTyping = throttle(3000, (user: string) => {
    if (this.textStore && this.textStore.get()) {
      this.socket.sendTyping(user);
    }
  });

  receiveTyping = (userId: string, cancel?: any) => {
    if (this.typing) {
      clearTimeout(this.typing.timeout);
      this.typing = undefined;
    }
    if (cancel !== true && this.convoUser() == userId) {
      this.typing = {
        user: userId,
        timeout: setTimeout(() => {
          if (this.convoUser() == userId) this.typing = undefined;
          this.redraw();
        }, 3000),
      };
    }
    this.redraw();
  };

  onReconnect = () => {
    this.data.convo && this.openConvo(this.data.convo.user.id);
    this.redraw();
  };

  upgradeData = (d: any): MessageData => {
    return {
      ...d,
      convo: d.convo && this.upgradeConvo(d.convo),
      contacts: d.contacts.map(this.upgradeContact),
    };
  };

  upgradeMessage = (m: any): Message => {
    return {
      ...m,
      date: new Date(m.date),
    }
  };

  upgradeContact = (c: any): Contact => {
    return {
      ...c,
      user: this.upgradeUser(c.user),
      lastMessage: this.upgradeMessage(c.lastMessage),
    }
  };

  upgradeConvo = (c: any): Convo => {
    return {
      ...c,
      user: this.upgradeUser(c.user),
      messages: c.messages.map(this.upgradeMessage),
    }
  };

  upgradeUser = (u: any): User => {
    return {
      ...u,
      id: u.name.toLowerCase(),
    }
  };

}
