import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import throttle from 'common/throttle';
import MessageCtrl from '../ctrl';
import { SearchResult, User } from '../interfaces';
import renderContacts from './contact';
import { userLink, userIcon, bindMobileMousedown } from './util';

export function renderInput(ctrl: MessageCtrl): VNode {
  return h('div.message-app__side__search', [
    h('input', {
      attrs: {
        value: '',
        placeholder: '搜索或开始新对话',
      },
      hook: {
        insert(vnode) {
          const input = vnode.elm as HTMLInputElement;
          let inputLock = false;
          input.addEventListener('compositionstart', function(){
            inputLock = true;
          });
          input.addEventListener('compositionend', function(){
            inputLock = false;
          });
          input.addEventListener(
            'input',
            throttle(500, () => {
              if(!inputLock) {
                ctrl.searchInput(input.value.trim())
              }
            })
          );
          input.addEventListener('blur', () =>
            setTimeout(() => {
              input.value = '';
              ctrl.searchInput('');
            }, 500)
          );
        },
      },
    }),
  ]);
}

export function renderResults(ctrl: MessageCtrl, res: SearchResult): VNode {
  return h('div.message-app__search.message-app__side__content', [
    res.contacts[0] &&
      h('section', [
        h('h2', '会话'),
        h(
          'div.message-app__search__contacts',
          res.contacts.map(t => renderContacts(ctrl, t))
        ),
      ]),
    res.friends[0] &&
      h('section', [
        h('h2', '好友'),
        h(
          'div.message-app__search__users',
          res.friends.map(u => renderUser(ctrl, u))
        ),
      ]),
    res.users[0] &&
      h('section', [
        h('h2', '棋手'),
        h(
          'div.message-app__search__users',
          res.users.map(u => renderUser(ctrl, u))
        ),
      ]),
  ]);
}

function renderUser(ctrl: MessageCtrl, user: User): VNode {
  return h(
    'div.message-app__side__contact',
    {
      key: user.id,
      hook: bindMobileMousedown(_ => ctrl.openConvo(user.id)),
    },
    [
      userIcon(user, 'message-app__side__contact__icon'),
      h('div.message-app__side__contact__user', [
        h('div.message-app__side__contact__head', [h('div.message-app__side__contact__name', userLink(user, false))]),
      ]),
    ]
  );
}

