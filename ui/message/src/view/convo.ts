import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import renderActions from './actions';
import renderInteract from './interact';
import renderMessages from './messages';
import { Convo } from '../interfaces';
import MessageCtrl from "../ctrl";
import { userLink, bindMobileMousedown } from './util';

export default function renderConvo(ctrl: MessageCtrl, convo: Convo): VNode {
  const user = convo.user;
  return h(
    'div.message-app__convo',
    {
      key: user.id,
    },
    [
      h('div.message-app__convo__head', [
        h('div.message-app__convo__head__left', [
          h('span.message-app__convo__head__back', {
            attrs: { 'data-icon': 'I' },
            hook: bindMobileMousedown(ctrl.showSide),
          }),
          userLink(user),
        ]),
        h('div.message-app__convo__head__actions', renderActions(ctrl, convo)),
      ]),
      renderMessages(ctrl, convo),
      h('div.message-app__convo__reply', [
        convo.relations.out === false || convo.relations.in === false
          ? h(
              'div.message-app__convo__reply__block.text',
              {
                attrs: { 'data-icon': 'L' },
              },
              '当前会话被阻止'
            )
          : convo.postable
          ? renderInteract(ctrl, user)
          : h(
              'div.message-app__convo__reply__block.text',
              [
                `${user.name} 无法发起消息`,
                h('i', {attrs: { 'data-icon': '', title : '可能原因：1.已被对方拉黑；2.对方隐私设置中开启了私信保护；3.对方开启了儿童模式；4.自方或对方未进行账号验证。' }} )
              ]
            ),
      ]),
    ]
  );
}
