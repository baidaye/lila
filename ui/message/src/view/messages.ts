import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import * as enhance from './enhance';
import { scroller } from './scroller';
import { bind } from './util';
import MessageCtrl from '../ctrl';
import { Convo, Message, Daily } from '../interfaces';

export default function renderMessages(ctrl: MessageCtrl, convo: Convo): VNode {
  return h(
    'div.message-app__convo__messages',
    {
      hook: {
        insert: setupMessages(true),
        postpatch: setupMessages(false),
      },
    },
    [
      h('div.message-app__convo__messages__init'),
      h('div.message-app__convo__messages__content', [
        ctrl.canGetMoreSince
          ? h(
              'button.message-app__convo__messages__more.button.button-empty',
              {
                key: 'more',
                hook: bind('click', _ => {
                  scroller.setMarker();
                  ctrl.getMore();
                }),
              },
              '加载更多'
            )
          : null,
        ...contentMessages(ctrl, convo.messages),
        ctrl.typing ? h('div.message-app__convo__messages__typing', `对方正在输入...`) : null,
      ]),
    ]
  );
}

function contentMessages(ctrl: MessageCtrl, messages: Message[]): VNode[] {
  const dailies = groupMessages(messages);
  const nodes: VNode[] = [];
  dailies.forEach(daily => nodes.push(...renderDaily(ctrl, daily)));
  return nodes;
}

function renderDaily(ctrl: MessageCtrl, daily: Daily): VNode[] {
  return [
    h('day', renderDate(daily.date)),
    ...daily.messages.map(group =>
      h(
        'group',
        group.map(message => renderMessage(ctrl, message))
      )
    ),
  ];
}

function renderMessage(ctrl: MessageCtrl, message: Message) {
  const tag = message.user == ctrl.data.me.id ? 'mine' : (message.warn ? 'their.warn' : 'their');
  return h(tag, [renderText(message), h('em', `${pad2(message.date.getHours())}:${pad2(message.date.getMinutes())}`)]);
}
const pad2 = (num: number): string => (num < 10 ? '0' : '') + num;

function groupMessages(messages: Message[]): Daily[] {
  let prev: Message = messages[0];
  if (!prev) return [{ date: new Date(), messages: [] }];
  const dailies: Daily[] = [
    {
      date: prev.date,
      messages: [[prev]],
    },
  ];
  messages.slice(1).forEach(message => {
    if (sameDay(message.date, prev.date)) {
      if (message.user == prev.user) dailies[0].messages[0].unshift(message);
      else dailies[0].messages.unshift([message]);
    } else
      dailies.unshift({
        date: message.date,
        messages: [[message]],
      });
    prev = message;
  });
  return dailies;
}

const today = new Date();
const yesterday = new Date();
yesterday.setDate(yesterday.getDate() - 1);

function renderDate(date: Date) {
  if (sameDay(date, today)) return '今天';
  if (sameDay(date, yesterday)) return '昨天';
  return renderFullDate(date);
}

const renderFullDate = (date: Date) => `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;

const sameDay = (d: Date, e: Date) =>
  d.getDate() == e.getDate() && d.getMonth() == e.getMonth() && d.getFullYear() == e.getFullYear();

const renderText = (message: Message) =>
  enhance.isMoreThanText(message.text)
    ? h('t', {
        hook: {
          create(_, vnode: VNode) {
            const el = vnode.elm as HTMLElement;
            el.innerHTML = enhance.enhance(message.text);
            el.querySelectorAll('img').forEach(c => c.addEventListener('load', scroller.auto, { once: true }));
          },
        },
      })
    : h('t', message.text);

const setupMessages = (insert: boolean) => (vnode: VNode) => {
  const el = vnode.elm as HTMLElement;
  if (insert) scroller.init(el);
  enhance.expandIFrames(el);
  scroller.toMarker() || scroller.auto();
};
