import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import MessageCtrl from '../ctrl';
import { Contact, LastMessage } from '../interfaces';
import { userLink, userIcon, bindMobileMousedown } from './util';

export default function renderContact(ctrl: MessageCtrl, contact: Contact, active?: string): VNode {
  const user = contact.user,
    message = contact.lastMessage,
    isNew = !message.read && message.user != ctrl.data.me.id;
  return h(
    'div.message-app__side__contact',
    {
      key: user.id,
      class: { active: active == user.id },
      hook: bindMobileMousedown(_ => ctrl.openConvo(user.id)),
    },
    [
      userIcon(user, 'message-app__side__contact__icon'),
      h('div.message-app__side__contact__user', [
        h('div.message-app__side__contact__head', [
          h('div.message-app__side__contact__name', userLink(user, false)),
          h('div.message-app__side__contact__date', renderDate(message)),
        ]),
        h('div.message-app__side__contact__body', [
          h(
            'div.message-app__side__contact__message',
            {
              class: { 'message-app__side__contact__message--new': isNew },
            },
            message.text
          ),
          isNew
            ? h('i.message-app__side__contact__new', {
                attrs: { 'data-icon': '' },
              })
            : null,
        ]),
      ]),
    ]
  );
}

function renderDate(message: LastMessage): VNode {
  const date = message.date;
  return h(
    'time.timeago',
    {
      key: date.getTime(),
      attrs: {
        title: date.toLocaleString(),
        datetime: date.getTime(),
      },
    },
    window.lichess.timeago.format(date)
  );
}

