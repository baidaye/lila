import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import { Convo } from '../interfaces';
import { bind } from './util';
import MessageCtrl from '../ctrl';

export default function renderActions(ctrl: MessageCtrl, convo: Convo): VNode[] {
  if (convo.user.id == 'haichess') return [];
  const nodes: VNode[] = [];
  const cls = 'message-app__convo__action.button.button-empty';
  nodes.push(
    h(`a.${cls}.play`, {
      key: 'play',
      attrs: {
        'data-icon': 'U',
        href: `/lobby?user=${convo.user.name}#friend`,
        title: '发起挑战'
      }
    })
  );
  nodes.push(h('div.message-app__convo__action__sep', '|'));
  if (convo.relations.out === false)
    nodes.push(
      h(`button.${cls}.text.hover-text`, {
        key: 'unblock',
        attrs: {
          'data-icon': 'k',
          title: '已加入黑名单',
          'data-hover-text': '移除出黑名单',
        },
        hook: bind('click', ctrl.unblock)
      })
    );
  else
    nodes.push(
      h(`button.${cls}.bad`, {
        key: 'block',
        attrs: {
          'data-icon': 'k',
          title: '加入黑名单',
        },
        hook: bind('click', withConfirm(ctrl.block))
      })
    );
  nodes.push(
    h(`button.${cls}.bad`, {
      key: 'delete',
      attrs: {
        'data-icon': 'q',
        title: '删除消息',
      },
      hook: bind('click', withConfirm(ctrl.delete))
    })
  );
  if (convo.messages[0])
    nodes.push(
      h(`button.${cls}.bad`, {
        key: 'report',
        attrs: {
          'data-icon': '!',
          title: `举报 ${convo.user.name}`,
        },
        hook: bind('click', withConfirm(ctrl.report))
      })
    );
  return nodes;
}

const withConfirm = (f: () => void) => (e: MouseEvent) => {
  if (confirm(`${(e.target as HTMLElement).getAttribute('title') || 'Confirm'}?`)) f();
};
