import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import { User } from '../interfaces';

export function userIcon(user: User, cls: string): VNode {
  return h(
    'div.user-link.' + cls,
    {
      class: {
        online: user.online,
        offline: !user.online,
      },
    },
    [h('i.line' + (user.patron ? '.patron' : ''))]
  );
}

export const userLink = (u: User, link: boolean = true): VNode => {
  const baseUrl = $('body').data('asset-url');
  const head = u.head ? '/image/' + u.head : baseUrl + '/assets/images/head-default-64.png';
  return h(link ? 'a.user-link.ulpt' : 'span.user-link', {
    attrs: { href: `/@/${u.name}`},
    class: { online: u.online, offline: !u.online }
  }, [
    h('div.head-line', [
      h('img.head', { attrs: { src: head }}),
      h('i.line')
    ]),
    u.title ? h('span.title', u.title == 'BOT' ? { attrs: { 'data-bot': true } } : {}, u.title) : null,
    h('span.u_name', u.name),
    h('span.badges', [
      u.member && u.member !== 'general' ? h('img.badge', { title: (u.member === 'gold' ? '金牌会员': '银牌会员'), attrs: { src: baseUrl + `/assets/images/icons/${u.member}.svg` }}) : null,
      u.coach ? h('img.badge', { attrs: { title: '认证教练', src: baseUrl + `/assets/images/icons/coach.svg` }}) : null,
      u.team ? h('img.badge', { attrs: { title: '认证俱乐部', src: baseUrl + `/assets/images/icons/team.svg` }}) : null
    ])
  ]);
}

export function bind(eventName: string, f: (e: Event) => void) {
  return {
    insert(vnode: VNode) {
      (vnode.elm as HTMLElement).addEventListener(eventName, e => {
        e.stopPropagation();
        f(e);
        return false;
      });
    },
  };
}

export function bindMobileMousedown(f: (e: Event) => any) {
  return bind('ontouchstart' in window ? 'click' : 'mousedown', f);
}

export function spinner(): VNode {
  return h('div.spinner', [
    h('svg', { attrs: { viewBox: '0 0 40 40' } }, [
      h('circle', {
        attrs: { cx: 20, cy: 20, r: 18, fill: 'none' },
      }),
    ]),
  ]);
}
