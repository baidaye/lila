import { h } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import MsgCtrl from '../ctrl';
import renderConvo from './convo';
import renderContact from './contact';
import * as search from './search';
import { spinner } from './util';

export default function (ctrl: MsgCtrl): VNode {
  const activeId = ctrl.convoUser();
  return h(
    'main.box.message-app',
    {
      class: {
        [`pane-${ctrl.pane}`]: true,
      },
    },
    [
      h('div.message-app__side', [
        search.renderInput(ctrl),
        ctrl.search.result
          ? search.renderResults(ctrl, ctrl.search.result)
          : h(
              'div.message-app__contacts.message-app__side__content',
              ctrl.data.contacts.map(t => renderContact(ctrl, t, activeId))
            ),
      ]),
      ctrl.data.convo
        ? renderConvo(ctrl, ctrl.data.convo)
        : ctrl.loading
        ? h('div.message-app__convo', { key: ':' }, [h('div.message-app__convo__head'), spinner()])
        : '',
    ]
  );
}
