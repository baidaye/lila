const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function loadConvo(userId: string) {
  return $.ajax({
    url: `/inbox/${userId}`,
    headers: headers
  });
}

export function getMore(userId: string, before: Date) {
  return $.ajax({
    url: `/inbox/${userId}?before=${before.getTime()}`,
    headers: headers
  });
}

export function loadContacts() {
  return $.ajax({
    url: `/inbox`,
    headers: headers
  });
}

export function search(q: string) {
  return $.ajax({
    url: `/inbox/search?q=${q}`,
    headers: headers
  });
}

export function block(u: string) {
  return $.ajax({
    method: 'post',
    url: `/rel/block/${u}`,
    headers: headers
  });
}

export function unblock(u: string) {
  return $.ajax({
    method: 'post',
    url: `/rel/unblock/${u}`,
    headers: headers
  });
}

export function del(u: string) {
  return $.ajax({
    method: 'post',
    url: `/inbox/${u}/delete`,
    headers: headers
  });
}

export function report(name: string, text: string) {
  return $.ajax({
    method: 'post',
    url: '/report/flag',
    headers: headers,
    data: {
      username: name,
      text: text,
      resource: 'msg',
    }
  });
}
