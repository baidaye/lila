import { init } from 'snabbdom';
import { VNode } from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import { MessageOpts } from './interfaces';
import MessageCtrl from './ctrl';

const patch = init([klass, attributes]);

import view from './view/main';

export function start(opts: MessageOpts) {

  let vnode: VNode;

  const appHeight = () => document.body.style.setProperty('--app-height', `${window.innerHeight}px`);
  window.addEventListener('resize', appHeight);
  appHeight();

  function redraw() {
    vnode = patch(vnode, view(ctrl));
  }

  const ctrl = new MessageCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}

