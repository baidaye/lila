import MessageCtrl from './ctrl';
import { Message } from './interfaces';

interface Handlers {
  [key: string]: (data: any) => void;
}
export default class Socket {

  send: SocketSend;
  handlers: Handlers;
  connected = () => true;

  constructor(send: SocketSend, ctrl: MessageCtrl) {
    this.send = send;
    this.handlers = {
      messageNew: (message: Message) => {
        let newMessage = ctrl.upgradeMessage(message);
        ctrl.receive({
          ...newMessage,
          read: false,
        });
      },
      messageType: (userId: string) => {
        ctrl.receiveTyping(userId);
      },
      blockedBy: (userId: string) => {
        ctrl.changeBlockBy(userId);
      },
      unblockedBy: (userId: string) => {
        ctrl.changeBlockBy(userId);
      }
    };
    this.connected = this.socketConnectHandler(ctrl);
  }

  receive = (type: string, data: any): boolean => {
    if (this.handlers[type]) {
      this.handlers[type](data);
      return true;
    }
    return false;
  };

  sendMessage(dest: string, text: string) {
    this.send('messageSend', { dest, text });
  };

  sendTyping(dest) {
    this.send('messageType', dest);
  };

  setRead(dest) {
    this.send('messageRead', dest);
  };

  socketConnectHandler = (ctrl: MessageCtrl) => {
    let connected = true;
    window.lichess.pubsub.on('socket.close', () => {
      connected = false;
      ctrl.redraw();
    });

    window.lichess.pubsub.on('socket.open', () => {
      if (!connected) {
        connected = true;
        ctrl.onReconnect();
      }
    });
    return () => connected;
  };

};
