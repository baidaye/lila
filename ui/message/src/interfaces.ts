export interface MessageOpts {
  data: MessageData;
  element: Element;
  socketSend: SocketSend;
}
export interface MessageData {
  me: Me;
  contacts: Contact[];
  convo?: Convo;
}
export interface Contact {
  user: User;
  lastMessage: LastMessage;
}
export interface User extends LightUser {
  online: boolean;
}
export interface Me extends User {
  kid: boolean;
}
export interface Message {
  user: string;
  text: string;
  date: Date;
  warn?: boolean;
}
export interface LastMessage extends Message {
  read: boolean;
}
export interface Convo {
  user: User;
  messages: Message[];
  relations: Relations;
  postable: boolean;
}

export interface Relations {
  in?: boolean;
  out?: boolean;
}

export interface Daily {
  date: Date;
  messages: Message[][];
}

export interface Search {
  input: string;
  result?: SearchResult;
}
export interface SearchResult {
  contacts: Contact[];
  friends: User[];
  users: User[];
}

export interface Typing {
  user: string;
  timeout: number;
}

export type Pane = 'side' | 'convo';

export type Redraw = () => void;
