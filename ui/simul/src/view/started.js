let m = require('mithril');
let simul = require('../simul');
let util = require('./util');
let xhr = require('../xhr');
let pairings = require('./pairings');
let results = require('./results');
let candidate = require('./candidate');

module.exports = function(ctrl) {
  return [
    m('div.box__top', [
      util.title(ctrl),
      m('div.box__top__actions', [
        ctrl.userId ?
          (!simul.createdByMe(ctrl) && ctrl.data.joinAnyTime ? [
            !simul.containsMe(ctrl) ? m('a.button.text' + (ctrl.teamBlock ? '.disabled' : ''), {
              disabled: ctrl.teamBlock, 'data-icon': 'G', onclick: function() { join(ctrl) }
            }, ctrl.teamBlock ? `您必须在俱乐部 ${ctrl.data.team.name} 中` : '参与车轮战') : null,
            simul.containsMe(ctrl) && !simul.pairingsContainMe(ctrl) ? m('a.button', { onclick: function() { xhr.withdraw(ctrl) } }, '离开车轮战') : null
          ] : null)
          : m('a.button.text', { 'data-icon': 'G', href: '/login?referrer=' + window.location.pathname }, ctrl.trans('signIn'))
      ])
    ]),
    candidate(ctrl),
    results(ctrl),
    pairings(ctrl)
  ];
};

function join(ctrl) {
  if(!ctrl.teamBlock) {
    if (ctrl.data.variants.length === 1) {
      xhr.join(ctrl.data.variants[0].key)(ctrl);
    } else {
      $.modal($('.simul .continue-with'));
      $('#modal-wrap .continue-with a').click(function() {
        $.modal.close();
        xhr.join($(this).data('variant'))(ctrl);
      });
    }
  }
}
