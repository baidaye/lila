let m = require('mithril');

let created = require('./created');
let started = require('./started');
let finished = require('./finished');

module.exports = function(ctrl) {
  let handler;
  if (ctrl.data.isRunning) handler = started;
  else if (ctrl.data.isFinished) handler = finished;
  else handler = created;

  return [
    m('aside.simul__side', {
      config: function(el, done) {
        if (!done) {
          $(el).replaceWith(ctrl.env.$side);
          ctrl.env.chat && window.lichess.makeChat(ctrl.env.chat);
        }
      }
    }),
    m('div.simul__main.box', handler(ctrl)),
    m('div.chat__members.none', m('span.list'))
  ];
};
