let m = require('mithril');
let util = require('./util');
let text = require('../text');
let pairings = require('./pairings');
let results = require('./results');

module.exports = function(ctrl) {
  return [
    m('div.box__top', [
      util.title(ctrl),
      m('div.box__top__actions', m('div.finished', ctrl.trans('finished')))
    ]),
    text.view(ctrl),
    results(ctrl),
    pairings(ctrl)
  ];
};
