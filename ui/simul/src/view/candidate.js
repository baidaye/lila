let m = require('mithril');
let simul = require('../simul');
let util = require('./util');
let text = require('../text');
let xhr = require('../xhr');

function byName(a, b) {
  return a.player.username > b.player.username
}

module.exports = function(ctrl) {
  let candidates = simul.candidates(ctrl).sort(byName);
  let accepted = simul.accepted(ctrl).sort(byName);
  let isHost = simul.createdByMe(ctrl);
  return m('div.halves',
      m('div.half.candidates',
          m('table.slist.slist-pad',
              m('thead', m('tr', m('th', {
                colspan: 3
              }, [
                m('strong', candidates.length),
                ' 候选棋手'
              ]))),
              m('tbody', candidates.map(function(applicant) {
                let variant = util.playerVariant(ctrl, applicant.player);
                return m('tr', {
                  key: applicant.player.id,
                  class: ctrl.userId === applicant.player.id ? 'me' : ''
                }, [
                  m('td', util.player(applicant.player)),
                  m('td.variant', {
                    'data-icon': variant.icon
                  }),
                  m('td.action', applicant.rejected ? '已经被拒绝加入' :
                      isHost ? m('a.button', {
                        'data-icon': 'E',
                        title: '接受',
                        onclick: function() {
                          xhr.accept(applicant.player.id)(ctrl);
                        }
                      }) : null)
                ])
              })))
      ),
      m('div.half.accepted', [
        m('table.slist.user_list',
            m('thead', [
              m('tr', m('th', {
                colspan: 3
              }, [
                m('strong', accepted.length),
                ' 已接受的棋手'
              ])), (simul.createdByMe(ctrl) && candidates.length && !accepted.length) ? m('tr.help',
                  m('th',
                      '现在你可以接受一些棋手，然后开始车轮战')) : null
            ]),
            m('tbody', accepted.map(function(applicant) {
              let variant = util.playerVariant(ctrl, applicant.player);
              return m('tr', {
                key: applicant.player.id,
                class: ctrl.userId === applicant.player.id ? 'me' : ''
              }, [
                m('td', util.player(applicant.player)),
                m('td.variant', {
                  'data-icon': variant.icon
                }),
                m('td.action', isHost ? m('a.button.button-red', {
                  'data-icon': 'L',
                  onclick: function() {
                    xhr.reject(applicant.player.id)(ctrl);
                  }
                }) : null)
              ])
            })))
      ])
  )
};