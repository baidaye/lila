let m = require('mithril');

let ctrl = require('./ctrl');
let view = require('./view/main');

module.exports = function(opts) {

  let controller = new ctrl(opts);

  m.module(opts.element, {
    controller: function() {
      return controller;
    },
    view: view
  });

  return {
    socketReceive: controller.socket.receive
  };
};

window.LichessChat = require('chat');
