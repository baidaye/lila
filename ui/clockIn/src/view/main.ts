import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import ClockInCtrl from '../ctrl';
import {bind, bindSubmit} from '../util';
import {nextDayDateTime} from '../../../ttask/src/util';
import {settingContent} from '../../../ttask/src/view/form';
import renderTaskModals from '../../../ttask/src/view/modals';

export default function (ctrl: ClockInCtrl): VNode {
  return h('main.box.box-pad.page-small.clockIn', [
    h('div.box__top', [
      h('h1', ctrl.isUpdate() ? '更新打卡任务' : '新建打卡任务')
    ]),
    h('form.form3.taskForm', {
      hook: bindSubmit(_ => {
        ctrl.submitTask();
      })
    }, [
      h('input', {attrs: {type: 'hidden', name: 'sourceRel.id', value: ctrl.team().id}}),
      h('input', {attrs: {type: 'hidden', name: 'sourceRel.name', value: `${ctrl.team().name}打卡任务`}}),
      h('input', {attrs: {type: 'hidden', name: 'sourceRel.source', value: 'teamClockIn'}}),
      h('div.form-split', [
        h('div.form-half.form-group', [
          h('label.form-label', {attrs: {for: 'form-startDate'}}, '开始日期'),
          h('input#form-startDate.form-control.flatpickr', {
            hook: {
              insert(vnode) {
                (<any> $(vnode.elm as HTMLElement)).flatpickr({
                  disableMobile: 'true',
                  minDate: 'today',
                  maxDate: new Date(Date.now() + 1000 * 3600 * 24 * 30),
                });
              },
            },
            attrs: {name: 'startDate', value: (ctrl.clockInSetting ? ctrl.clockInSetting.startDate : nextDayDateTime()), readonly: true}
          })
        ]),
        h('div.form-half.form-group', [
          h('label.form-label', {attrs: {for: 'form-period'}}, '任务周期'),
          h('select#form-period.form-control', {
              hook: bind('change', (e: Event) => {
                ctrl.period = (e.target as HTMLInputElement).value;
                ctrl.setTaskName();
              }),
              attrs: {name: 'period', required: true}
            },
            ctrl.periods.map(function (period) {
              return h('option', {
                attrs: {
                  value: period.id,
                  selected: ctrl.period === period.id
                }
              }, period.name)
            }))
        ])
      ]),
      !!ctrl.opts.team && ctrl.opts.team.coinSetting.open ? h('div.form-group.form-half', [
        h('label.form-label', {attrs: {for: 'form-coinRule'}}, ctrl.opts.team.coinSetting.name),
        h('input#form-coinRule.form-control', { attrs: {name: 'coinRule', type: 'number', min: 0, max: ctrl.opts.team.coinSetting.singleVal, value: ctrl.coinRule}}),
        h('small.form-help', '每完成一次打卡所得积分，填0表示不记')
      ]) : null,
      h('div.form-group', [
        h('label.form-label', {attrs: {for: 'form-itemType'}}, '任务类型'),
        h('select#form-itemType.form-control', {
            hook: bind('change', (e: Event) => {
              ctrl.changeItemType((e.target as HTMLInputElement).value);
            }),
            attrs: {name: 'itemType', required: true}
          },
          ctrl.itemTypes.map(function (itemType) {
            return h('option', {
              attrs: {
                value: itemType.id,
                selected: ctrl.itemType === itemType.id
              }
            }, itemType.name)
          }))
      ]),
      settingContent(ctrl),
      h('div.calendar', [
        h('div.calendar-top', [
          h('label.title', '任务日历'),
          h('a.button.button-empty.small', {
            hook: bind('click', () => {
              ctrl.buildTasks();
            })
          }, '生成日历')
        ]),
        h('div.zabutoCalendar'),
        h('div.calendar-data.none', ctrl.clockInTask.map(task => {
          return h('div', [
            h('input', {attrs: {type: 'hidden', name: `tasks[${task.index}].name`, value: task.name}}),
            h('input', {attrs: {type: 'hidden', name: `tasks[${task.index}].index`, value: task.index}}),
            h('input', {attrs: {type: 'hidden', name: `tasks[${task.index}].date`, value: task.date}}),
            h('input', {attrs: {type: 'hidden', name: `tasks[${task.index}].status`, value: task.status.id}})
          ])
        }))
      ]),
      h('div.form-actions', [
        h('a.cancel', {
          hook: bind('click', () => {
            window.history.back();
          })
        }, '取消'),
        h('button.button', {attrs: {type: 'submit'}}, '保存')
      ])
    ])
  ].concat(renderTaskModals(ctrl)));
}


