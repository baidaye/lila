import ClockInCtrl from './ctrl';

interface Handlers {
  [key: string]: any;
}

export interface Socket {
  send: SocketSend;
  receive(type: string, data: any): boolean;
}

export function make(send: SocketSend, ctrl: ClockInCtrl): Socket {
  console.log(ctrl.opts.userId);
  const handlers: Handlers = {

  };

  return {
    receive(type: string, data: any): boolean {
      const handler = handlers[type];
      if (handler) handler(data);
      return true;
    },
    send
  };
}
