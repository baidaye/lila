import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import throttle from 'common/throttle';
import {ClockInOpts} from './interfaces';
import CoachCtrl from './ctrl';

export const patch = init([klass, attributes]);

import view from './view/main';

export function start(opts: ClockInOpts) {

  let vnode: VNode;

  let redraw = throttle(100, () => {
    vnode = patch(vnode, view(ctrl));
  });

  const ctrl = new CoachCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}


