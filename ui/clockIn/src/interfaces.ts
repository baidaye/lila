import {VNode} from 'snabbdom/vnode'
import {TTaskOpts} from '../../ttask/src/interfaces';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface ClockInOpts extends TTaskOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  clockInSetting: ClockInSetting;
  clockInTask: ClockInTask[];
}

export interface Team {
  id: string;
  name: string;
}

export interface ClockInSetting {
  id: string;
  name: string;
  startDate: string,
  period: ClockInSettingPeriod;
  status: ClockInSettingStatus;
  coinRule: number;
  task: any;
}

export interface ClockInSettingPeriod {
  id: string;
  name: string;
}

export interface ClockInSettingStatus {
  id: string;
  name: string;
}

export interface TaskItemType {
  id: string;
  name: string;
}

export interface ClockInTask {
  index: number;
  name: string;
  date: string;
  status: ClockInTaskStatus;
}

export interface ClockInTaskStatus {
  id: string;
  name: string;
}

export type Redraw = () => void;

