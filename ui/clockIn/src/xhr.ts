const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function createTask(teamId: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/team/clockIn/create?teamId=${teamId}`,
    headers: headers,
    data: data
  });
}

export function updateTask(id: string, data: any) {
  return $.ajax({
    method: 'post',
    url: `/team/clockIn/${id}/update`,
    headers: headers,
    data: data
  });
}
