import * as xhr from './xhr'
import TaskCtrl from '../../ttask/src/ctrl';
import {ClockInOpts, ClockInSetting, ClockInTask, Redraw} from './interfaces';
import {make as makeSocket, Socket} from './socket';
import * as addDays from 'date-fns/add_days'
import * as addWeeks from 'date-fns/add_weeks'
import * as addMonths from 'date-fns/add_months'
import * as eachDay from 'date-fns/each_day'
import * as format from 'date-fns/format'

export default class ClockInCtrl extends TaskCtrl {

  socket: Socket;

  $zabutoCalendar: JQuery;

  itemTypes = [
    {id: 'puzzle', name: '战术训练', unit: '题', defNum: 5},
    {id: 'fromPosition', name: '指定开局FEN对弈', unit: '盘', defNum: 1}
  ];

  periods = [
    {id: 'week', name: '周', days: 7},
    {id: 'month', name: '月', days: 30}
  ];

  period: string = 'week';
  coinRule: number = 0;
  clockInSetting: ClockInSetting;
  clockInTask: ClockInTask[] = [];

  constructor(readonly opts: ClockInOpts, readonly redraw: Redraw) {
    super(opts, redraw, () => {});
    this.socket = makeSocket(this.opts.socketSend, this);
    this.clockInSetting = opts.clockInSetting;
    this.clockInTask = opts.clockInTask ? opts.clockInTask : [];
    this.period = opts.clockInSetting ? opts.clockInSetting.period.id : 'week';
    this.coinRule = opts.clockInSetting ? opts.clockInSetting.coinRule : 0;

    setTimeout(() => {
      this.initTask();
      if(this.isUpdate()) {
        this.setCalendar();
      }
    }, 200);
  }

  restoreTask = (index: number) => {
    if(this.clockInTask[index]) {
      this.clockInTask[index].status = {
        id: 'created',
        name: '新建'
      };
    }
    this.setCalendar();
  };

  cancelTask = (index: number) => {
    if(this.clockInTask[index]) {
      this.clockInTask[index].status = {
        id: 'canceled',
        name: '取消'
      };
    }
    this.setCalendar();
  };

  buildTasks = () => {
    if(this.clockInTask.length === 0 || confirm('是否重新生成任务日历？')) {
      let $form = $('.taskForm');
      let dayStart = $form.find('#form-startDate').val();
      let period = $form.find('#form-period').val();

      let dayEnd;
      if(period === 'week') dayEnd = addWeeks(dayStart, 1);
      if(period === 'month') dayEnd = addMonths(dayStart, 1);
      dayEnd = addDays(dayEnd, -1);

      const days = eachDay(dayStart, dayEnd);

      this.clockInTask = days.map((day, index) => {
        return {
          index: index + 1,
          name: `第${ index + 1 }次`,
          date: format(day, 'YYYY-MM-DD'),
          status: {
            id: 'created',
            name: '新建'
          }
        };
      });

      this.period = period;
    }

    this.setCalendar();
  };

  setCalendar = () => {
    let $form = $('.taskForm');
    let dayStart = $form.find('#form-startDate').val();
    let sDate = new Date(dayStart);

    if(!this.$zabutoCalendar) {
      this.$zabutoCalendar = $form.find('.zabutoCalendar');
    } else {
      (<any> this.$zabutoCalendar).zabuto_calendar('destroy');
    }

    let events = this.clockInTask.map(item => {
      return {
        date: item.date,
        classname: `task ${item.status.id}`,
        markup: `<div class="day-wrap"><div>[day]</div><div class="dot"></div></div></div>`
      }
    });

    (<any> this.$zabutoCalendar).zabuto_calendar({
      language: 'cn',
      classname: 'slist',
      navigation_markup: {
        prev: '<i data-icon="左"></i>',
        next: '<i data-icon="右"></i>'
      },
      year: sDate.getFullYear(),
      month: sDate.getMonth() + 1,
      events: events
    });

    (<any> this.$zabutoCalendar).off('zabuto:calendar:day').on('zabuto:calendar:day', (e) => {
      let date = e.value;
      let data = this.clockInTask.find(d => d.date === date);
      if(data) {
        if(data.status.id === 'canceled') {
          if(confirm('是否确认恢复本次任务？')) {
            this.restoreTask(data.index - 1);
          }
        } else {
          if(confirm('是否确认取消本次任务？')) {
            this.cancelTask(data.index - 1);
          }
        }
      }
    });

    this.redraw();
  };

  submitTask = () => {
    let $form = $('.taskForm');
    let dayStart = $form.find('#form-startDate').val();
    let period = $form.find('#form-period').val();
    let data = $form.serialize();


    if(this.itemType === 'fromPosition') {
      if (this.fromPositionGameCtrl.fromPositionGames.length === 0) {
        alert('请添加对局');
        return;
      }
    }

    if(period !== this.period) {
      alert('任务日历与任务周期不一致');
      return;
    }

    if(this.clockInTask.length === 0) {
      alert('请生成任务日历');
      return;
    }

    if(this.clockInTask[0].date !== dayStart) {
      alert('任务日历开始日期与设置不一致，请重新生成任务日历');
      return;
    }

    if(this.isUpdate()) {
      xhr.updateTask(this.opts.clockInSetting.id, data).then(() => {
        location.href = `/team/clockIn/current?teamId=${this.team().id}`
      });
    } else {
      xhr.createTask(this.team().id, data).then(() => {
        location.href = `/team/clockIn/current?teamId=${this.team().id}`
      });
    }
  };

  team = () => {
    return this.opts.team!;
  };

  setTaskName = () => {
    let itemTypeName = this.itemTypes.find(t => t.id === this.itemType)!.name;
    let periodName = this.periods.find(t => t.id === this.period)!.name;
    this.taskName = `“${itemTypeName}” ${periodName}打卡`;
    this.redraw();
  };

}

