$(function() {
  $('#trainer').each(function() {
    let $trainer = $(this);
    let $board = $('.coord-trainer__board .cg-wrap');
    let ground;
    let $side = $('.coord-trainer__side');
    let $right = $('.coord-trainer__table');
    let $bar = $trainer.find('.progress_bar');
    let $coords = [
      $('#next_coord0'),
      $('#next_coord1'),
      $('#next_coord2')
    ];
    let $start = $right.find('.start');
    let $explanation = $right.find('.explanation');
    let $score = $('.coord-trainer__score');
    let scoreUrl = $trainer.data('score-url');
    let duration = 30 * 1000;
    let tickDelay = 50;
    let colorPref = $trainer.data('color-pref');
    let coordinates = true;
    let color;
    let startAt, score;

    let setGround = function() {
      color = colorPref === 'random' ? ['white', 'black'][Math.round(Math.random())] : colorPref;
      coordinates = $trainer.find('input[name="coordShow"]:checked').val();

      let cfg = {
        coordinates: coordinates == 1,
        drawable: { enabled: false },
        movable: {
          free: false,
          color: null
        },
        orientation: color,
        addPieceZIndex: $('#main-wrap').hasClass('is3d')
      };
      if (!ground) {
        ground = Chessground($board[0], cfg);
      } else ground.set(cfg);
      $trainer.removeClass('white black').addClass(color);
      ground.redrawAll();
    };
    setGround();

    $trainer.find('form.color').each(function() {
      let $form = $(this);
      $form.find('input').on('change', function() {
        let selected = $form.find('input:checked').val();
        let c = {
          1: 'white',
          2: 'random',
          3: 'black'
        }[selected];
        if (c !== colorPref) $.ajax(window.lichess.formAjax($form));
        colorPref = c;
        setGround();
        return false;
      });
    });

    $trainer.find('form.coord_show').each(function() {
      let $form = $(this);
      $form.find('input').on('change', function() {
        setGround();
        return false;
      });
    });

    let showCharts = function() {
      let dark = $('body').hasClass('dark');
      let theme = {
        type: 'line',
        width: '100%',
        height: '80px',
        lineColor: dark ? '#4444ff' : '#0000ff',
        fillColor: dark ? '#222255' : '#ccccff'
      };
      $side.find('.user_chart').each(function() {
        $(this).sparkline($(this).data('points'), theme);
      });
    };
    showCharts();

    let centerRight = function() {
      $right.css('top', (256 - $right.height() / 2) + 'px');
    };
    centerRight();

    let clearCoords = function() {
      $.each($coords, function(i, e) {
        e.text('');
      });
    };

    let newCoord = function(prevCoord) {
      // disallow the previous coordinate's row or file from being selected
      let files = 'abcdefgh';
      let fileIndex = files.indexOf(prevCoord[0]);
      files = files.slice(0, fileIndex) + files.slice(fileIndex + 1, 8);

      let rows = '12345678';
      let rowIndex = rows.indexOf(prevCoord[1]);
      rows = rows.slice(0, rowIndex) + rows.slice(rowIndex + 1, 8);

      return files[Math.round(Math.random() * (files.length - 1))] + rows[Math.round(Math.random() * (rows.length - 1))];
    };

    let advanceCoords = function() {
      $('#next_coord0').removeClass('nope');
      let lastElement = $coords.shift();
      $.each($coords, function(i, e) {
        e.attr('id', 'next_coord' + i);
      });
      lastElement.attr('id', 'next_coord' + ($coords.length));
      lastElement.text(newCoord($coords[$coords.length - 1].text()));
      $coords.push(lastElement);
    };

    let stop = function() {
      clearCoords();
      $trainer.removeClass('play');
      centerRight();
      $trainer.removeClass('wrong');
      ground.set({
        events: {
          select: false
        }
      });
      if (scoreUrl) $.ajax({
        url: scoreUrl,
        method: 'post',
        data: {
          color: color,
          score: score
        },
        success: function(charts) {
          $side.find('.scores').html(charts);
          showCharts();
        }
      });
    };

    let tick = function() {
      let spent = Math.min(duration, (new Date() - startAt));
      $bar.css('width', (100 * spent / duration) + '%');
      if (spent < duration) setTimeout(tick, tickDelay);
      else stop();
    };

    $start.click(function() {
      //$explanation.remove();
      $trainer.addClass('play').removeClass('init');
      setGround();
      clearCoords();
      centerRight();
      score = 0;
      $score.text(score);
      $bar.css('width', 0);
      setTimeout(function() {

        startAt = new Date();
        ground.set({
          events: {
            select: function(key) {
              let hit = key == $coords[0].text();
              if (hit) {
                score++;
                $score.text(score);
                advanceCoords();
              } else {
                $('#next_coord0').addClass('nope');
                setTimeout(function() {
                  $('#next_coord0').removeClass('nope');
                }, 500);
              }
              $trainer.toggleClass('wrong', !hit);
            }
          }
        });
        $coords[0].text(newCoord('a1'));
        let i;
        for (i = 1; i < $coords.length; i++)
          $coords[i].text(newCoord($coords[i - 1].text()));
        tick();
      }, 1000);
    });
  });

});
