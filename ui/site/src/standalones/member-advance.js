function memberAdvance() {
  let $search = $('.member-search');

  $search.find('.show-search').click(function (e) {
    let $this = $(this);
    if($this.hasClass('expanded')) {
      $this.removeClass('expanded').html('显示高级搜索<i data-icon="R"></i>');
      $search.find('.tr-tag').addClass('none');
      $search.find('input[name="advance"]').val("false");
    } else {
      $this.addClass('expanded').html('隐藏高级搜索<i data-icon="S"></i>');
      $search.find('.tr-tag').removeClass('none');
      $search.find('input[name="advance"]').val("true");
    }
  });
}
