function transfer(onChange) {
  let $transfer = $('.transfer');
  let $leftPanel = $transfer.find('.left');
  let $rightPanel = $transfer.find('.right');
  let $leftList = $leftPanel.find('.transfer-panel-list table tbody');
  let $rightList = $rightPanel.find('.transfer-panel-list table tbody');
  let $leftButton = $transfer.find('.arrow-left');
  let $rightButton = $transfer.find('.arrow-right');

  function check($lst, $btn) {
    $lst.find('input[type="checkbox"]').click(function () {
      setBtn($lst, $btn);
    });
  }

  function setBtn($lst, $btn) {
    if ($lst.find('input:checked').length > 0) {
      $btn.removeClass('disabled').prop('disabled', false);
    } else {
      $btn.addClass("disabled").prop('disabled', true);
    }
  }

  function tsf($lst1, $lst2, $btn1, $btn2) {
    $btn1.click(function (e) {
      e.preventDefault();
      $lst1.find('input:checked').each(function () {
        let $this = $(this);
        let $line = $this.parents('tr');
        if(!$line.hasClass('none')) {
          $this.prop('checked', false);
          $line.clone().appendTo($lst2);
          $line.remove()
        }
      });

      setBtn($lst1, $btn1);
      $lst2.find('input[type="checkbox"]').off('click');
      check($lst2, $btn2);

      $leftPanel.find('.transfer-panel-head .count').text($leftList.find('tr').length);
      $rightPanel.find('.transfer-panel-head .count').text($rightList.find('tr').length);

      let leftPlayers = [];
      $leftList.find('input').each(function () {
        leftPlayers.push($(this).val());
      });

      let rightPlayers = [];
      $rightList.find('input').each(function () {
        rightPlayers.push($(this).val());
      });

      if(onChange) {
        onChange(rightPlayers, leftPlayers);
      }
      return false;
    });
  }

  function search($panel, $lst) {
    $panel.find('.transfer-search').on('input propertychange', function() {
      let txt = $(this).val();
      if($.trim(txt) !== ''){
        let arr = [];
        $lst.find('input').each(function () {
          arr.push($(this).data('attr'));
        });

        let filterIds = arr.filter(function (n) {
          return n.id.startsWith(txt) || n.name.startsWith(txt)
        }).map(n => n.id);

        $lst.find('tr').addClass('none');
        filterIds.forEach(function (id) {
          $lst.find('#chk_' + id).parents('tr').removeClass('none');
        });
      } else {
        $lst.find('tr').removeClass('none');
      }
    });
  }

  function checkAll($panel, $lst, $btn) {
    $panel.find('#transfer_chk_all').click(function () {
      let isChecked = $(this).is(':checked');
      $lst.find('input[type="checkbox"]').prop('checked', false);
      if(isChecked) {
        $lst.find('input[type="checkbox"]').prop('checked', true);
      }
      setBtn($lst, $btn);
    });
  }

  check($leftList, $rightButton);
  check($rightList, $leftButton);
  checkAll($leftPanel, $leftList, $rightButton);
  checkAll($rightPanel, $rightList, $leftButton);
  tsf($leftList, $rightList, $rightButton, $leftButton);
  tsf($rightList, $leftList, $leftButton, $rightButton);
  search($leftPanel, $leftList);
  search($rightPanel, $rightList);
}
