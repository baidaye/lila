import {VNode} from 'snabbdom/vnode'
import {Task, ClassicGame, Team} from '../../ttask/src/interfaces';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface ThroughTrainOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  notAccept: boolean;
  taskPager: Paginator<Task>;
  student: Student;
  themePuzzle: any;
  classicGames: ClassicGame[];
  team?: Team;
}

export interface Student extends LightUser {
  online: boolean;
  isPlaying: boolean;
  mark?: string;
  coachId: string;
  coachName: string;
  isMember: boolean;
}

export type Redraw = () => void;

