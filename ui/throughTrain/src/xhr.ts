const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function cancelTask(taskId: string) {
  return $.ajax({
    method: 'post',
    url: `/ttask/${taskId}/cancel`,
    headers: headers,
  });
}

export function getTask(page: number, metaId: string, studentId: string, status: string) {
  return $.ajax({
    url: `/ttask/page/throughTrain?page=${page}&metaId=${metaId}&userId=${studentId}&status=${status}`,
    headers: headers
  });
}
