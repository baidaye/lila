import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {TaskStatus} from '../../../ttask/src/interfaces';
import renderTaskModals from '../../../ttask/src/view/modals';
import ThroughTrainCtrl from '../ctrl';
import {bind} from '../util';

const statusArray: TaskStatus[] = [
  {id: '', name: '全部'},
  {id: 'train', name: '训练中'},
  {id: 'created', name: '未完成'},
  {id: 'finished', name: '已完成'},
  {id: 'canceled', name: '已取消'},
  {id: 'expired', name: '已过期'}
];

export default function (ctrl: ThroughTrainCtrl): VNode {
  return h('main.box.page-small.throughTrain', [
    h('div.box__top', [
      h('h1', [
        ctrl.isCoach() ? h(`a.text`, { attrs: { 'data-icon': 'I', href: '/coach/student/list' } }) : null,
        '教学直通车',
        ctrl.isCoach() ? '·': null,
        ctrl.isCoach() ? `${ctrl.student.mark ? ctrl.student.mark : ctrl.student.name}` : null
      ]),
      ctrl.isCoach() && ctrl.student.isMember ? h('div.box__top__actions', [
        h('select', {
          hook: bind('change', e => {
            ctrl.status = (e.target as HTMLInputElement).value;
            ctrl.getTasks();
          })
        }, statusArray.map(function (item) {
          return h('option', { attrs: {id: 'taskStatus', value: item.id } }, item.name)
        })),
        h(`a.button.button-green.text`, {
          attrs: { 'data-icon': 'O' },
          class: {
            disabled: !ctrl.student.isMember
          },
          hook: bind('click', () => {
            ctrl.openCreateTaskModal();
          })
        }, '新建任务')
      ]) : null
    ]),
    h('table.slist', [
      h('thead', [
        h('tr', [
          h('th', '任务名称'),
          h('th', '任务状态'),
          h('th', '进度'),
          h('th', '发布时间'),
          h('th', '操作')
        ])
      ]),
      h('tbody', !ctrl.taskPager || ctrl.taskPager.currentPageResults.length == 0 ? h('tr', [
        h('td.empty', {attrs: {colspan: 4}}, '暂无任务')
      ]) : ctrl.taskPager.currentPageResults.map((task) => {
        return h(`tr.${task.id}`, [
          h('td', task.name),
          h('td', {
            class: {
              training: (task.status.id === 'train'),
              complete: (task.status.id === 'finished'),
              canceled: (task.status.id === 'canceled' || task.status.id === 'expired'),
              uncomplete: (task.status.id === 'created')
            }
          }, task.status.name),
          h('td', `${task.progress}%`),
          h('td', task.createdAt),
          h('td.action', [
            h(`a.button.button-empty.small`, { attrs: { target: '_blank', href: `/ttask/${task.id}` } }, '进入'),
            ctrl.isCoach() ? h(`button.button.button-empty.button-red.small`, {
              class: {
                disabled: task.status.id === 'expired' || task.status.id === 'canceled' || task.status.id === 'finished'
              },
              attrs: {
                disabled: task.status.id === 'expired' || task.status.id === 'canceled' || task.status.id === 'finished'
              },
              hook: bind('click', () => {
                if(confirm('取消任务后学员将不能再继续该任务，是否确认？')) {
                  ctrl.cancelTask(task.id);
                }
              })
            }, '取消') : null
          ])
        ]);
      }))
    ]),
    h('div.pager', [
      h('button.fbt prev', {
        attrs: {
          'data-icon': 'Y',
          disabled: ctrl.taskPager.currentPage <= 1
        },
        class: {
          disabled: ctrl.taskPager.currentPage <= 1
        },
        hook: bind('click', () => {
          if(ctrl.taskPager.currentPage > 1) {
            ctrl.getTasks(ctrl.taskPager.previousPage);
          }
        })
      }, '上一页'),
      h('button.fbt next', {
        attrs: {
          'data-icon': 'X',
          disabled: ctrl.taskPager.currentPage >= ctrl.taskPager.nbPages
        },
        class: {
          disabled: ctrl.taskPager.currentPage >= ctrl.taskPager.nbPages
        },
        hook: bind('click', () => {
          if(ctrl.taskPager.currentPage < ctrl.taskPager.nbPages) {
            ctrl.getTasks(ctrl.taskPager.nextPage);
          }
        })
      }, '下一页')
    ])
  ].concat(renderTaskModals(ctrl.taskCtrl)));
}

