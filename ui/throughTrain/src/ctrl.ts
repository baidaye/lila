import {ThroughTrainOpts, Redraw, Student} from './interfaces';
import TaskCtrl from '../../ttask/src/ctrl';
import {Task} from '../../ttask/src/interfaces';
import {make as makeSocket, Socket} from './socket';
import * as xhr from './xhr'

export default class ThroughTrainCtrl {

  socket: Socket;
  opts: ThroughTrainOpts;
  student: Student;
  taskPager: Paginator<Task>;
  taskCtrl: TaskCtrl;

  status: string = '';

  constructor(opts: ThroughTrainOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.taskPager = opts.taskPager;
    this.student = opts.student;
    this.socket = makeSocket(this.opts.socketSend, this);
    this.taskCtrl = new TaskCtrl({
      notAccept: this.opts.notAccept,
      sourceRel: {
        id: this.metaId(),
        name: `${this.student.coachName} 教学直通车`,
        deadlineAt: '',
        source: 'throughTrain'
      },
      themePuzzle: this.opts.themePuzzle,
      classicGames: this.opts.classicGames,
      team: this.opts.team
    }, this.redraw, this.getTasks);
  }

  metaId = () => `${this.student.coachId}@${this.student.id}`;

  isCoach = () => this.opts.userId === this.student.coachId;

  getTasks = (p: number = 1) => {
    xhr.getTask(p, this.metaId(), this.student.id, this.status).then((data) => {
      this.taskPager = data;
      this.redraw();
    });
  };

  cancelTask = (taskId: string) => {
    xhr.cancelTask(taskId).then(() => {
      this.getTasks(this.taskPager.currentPage);
      this.redraw();
    });
  };

  openCreateTaskModal = () => {
    if(this.student.isMember) {
      this.taskCtrl.openTaskCreateModal([this.student], this.student.id);
    }
  };

}

