import RoundController from './ctrl';

export default class ApiCtrl {

  constructor(readonly ctrl: RoundController) {
    this.incomingStream();
    this.boardStream(this.ctrl.opts.round.id);
  }

  incomingStream = () => {
    this.ctrl.oAuth2Ctrl.openStream('/api/stream/event', {}, msg => {
      switch (msg.type) {
        case 'gameStart': {
          this.ctrl.onGameStart(msg);
          break;
        }
        case 'gameFinish': {
          this.ctrl.onGameFinish(msg.game.status, msg.game.winner, this.ctrl.toRatingDiff(msg.game, msg.game.status.id));
          break;
        }
        case 'challenge': {
          this.ctrl.onChallenge(msg.challenge);
          break;
        }
        case 'challengeCanceled': {
          this.ctrl.onChallengeCanceled(msg.challenge);
          break;
        }
        case 'challengeDeclined': {
          this.ctrl.onChallengeDeclined(msg.challenge);
          break;
        }
        default:
          console.debug(`Unprocessed message of type ${msg.type}`, msg);
      }
    });
  };

  boardStream = (gameId: string) => {
    this.ctrl.oAuth2Ctrl.openStream(`/api/board/game/stream/${gameId}`, {}, msg => {
      switch (msg.type) {
        case 'gameFull':
          this.ctrl.onGameFull(msg);
          break;
        case 'gameState':
          this.ctrl.onGameState(msg);
          break;
        case 'opponentGone':
          this.ctrl.onOpponentGone(msg);
          break;
        case 'chatLine':
          this.ctrl.onChatLine(msg);
          break;
        default:
          console.debug(`Unprocessed message of type ${msg.type}`, msg);
      }
    }, this.ctrl.openConnectModal);
  };

  userMove = (gameId: string, uci: Uci) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/move/${uci}`, { method: 'post' });
  };

  offerDraw = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/draw/true`, { method: 'post' });
  };

  acceptDraw = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/draw/true`, { method: 'post' });
  };

  declineDraw = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/draw/false`, { method: 'post' });
  };

  offerTakeback = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/takeback/true`, { method: 'post' });
  };

  acceptTakeback = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/takeback/true`, { method: 'post' });
  };

  declineTakeback = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/takeback/false`, { method: 'post' });
  };

  resign = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/resign`, { method: 'post' });
  };

  abort = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/abort`, { method: 'post' });
  };

  claimVictory = (gameId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/board/game/${gameId}/claim-victory`, { method: 'post' });
  };

  createChallenge = (username, limit, increment) => {
    this.ctrl.oAuth2Ctrl.openStream(
      `/api/challenge/${username}`,
      {
        single: this.ctrl.oAuth2Ctrl.signal,
        method: 'post',
        body: `clock.limit=${limit}&clock.increment=${increment}&rated=true&keepAliveStream=true`
      },
      _ => {}
    );
  };

  acceptChallenge = (challengeId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/challenge/${challengeId}/accept`, { method: 'post' });
  };

  declineChallenge = (challengeId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/challenge/${challengeId}/decline`, { method: 'post' });
  };

  cancelChallenge = (challengeId: string) => {
    return this.ctrl.oAuth2Ctrl.fetch(`/api/challenge/${challengeId}/cancel`, { method: 'post' });
  };

}
