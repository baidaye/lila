import {VNode} from 'snabbdom/vnode';
import {GameData, PlayerUser, Status, Source} from 'game';
import {ClockData} from './clock/clockCtrl';
import {OAuth2Data} from '../../lichsssApiOAuth/interfaces';

export const initialFen: Fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

export type MaybeVNode = VNode | null | undefined;
export type MaybeVNodes = MaybeVNode[];

export type Redraw = () => void;

export interface Untyped {
  [key: string]: any;
}

export interface RoundData extends GameData {
  clock?: ClockData;
  pref: Pref;
  steps: Step[];
  expiration?: Expiration;
}

export interface Expiration {
  idleMillis: number;
  movedAt: number;
  millisToMove: number;
}

export interface RoundOpts {
  userId: string,
  user: PlayerUser,
  color: Color,
  pref: Pref,
  oAuth2: OAuth2Data;
  round: BackendRound;
  element: HTMLElement;
  i18n: any;
}

export interface BackendRound {
  id: string;
  white: BackendRoundPlayer;
  black: BackendRoundPlayer;
  clock: BackendRoundClock;
  perf: BackendRoundPerf;
  speed: Speed;
  variant: Variant;
  status: Status;
  source: Source;
  rated: boolean;
  redirected: boolean;
  gameStart: any;
  createdAt: number;
}

export interface BackendRoundClock {
  initial: number;
  increment: number;
  show: string;
}

export interface BackendRoundPerf {
  key: string;
  name: string;
  icon: string;
}

export interface BackendRoundPlayer {
  local: BackendRoundUser;
  lichess: BackendRoundUser;
  color: Color;
  isLichess: boolean;
  offeringRematch: boolean;
  offeringDraw: boolean;
  proposingTakeback: boolean;
}

export interface BackendRoundUser {
  id: string;
  username: string;
  rating: number;
  ratingDiff?: number;
  title?: string;
  provisional: boolean;
  head?: string;
}

export interface Chat {
  preset: 'start' | 'end' | undefined;
  parseMoves?: boolean;
  alwaysEnabled: boolean;
}

export interface Step {
  ply: Ply;
  fen: Fen;
  san: San;
  uci: Uci;
  color: string;
  check?: boolean;
}

export interface Pref {
  animationDuration: number;
  autoQueen: 1 | 2 | 3;
  blindfold: boolean;
  clockBar: boolean;
  clockSound: boolean;
  clockTenths: 0 | 1 | 2;
  confirmResign: boolean;
  coords: 0 | 1 | 2;
  destination: boolean;
  enablePremove: boolean;
  highlight: boolean;
  is3d: boolean;
  keyboardMove: boolean;
  moveEvent: 0 | 1 | 2;
  replay: 0 | 1 | 2;
  rookCastle: boolean;
  showCaptured: boolean;
  submitMove: boolean;
  resizeHandle: 0 | 1 | 2;
}

export type Position = 'top' | 'bottom';

export interface MaterialDiffSide {
  [role: string]: number;
}

export interface MaterialDiff {
  white: MaterialDiffSide;
  black: MaterialDiffSide;
}

export interface CheckCount {
  white: number;
  black: number;
}
