export const headers = {
  'Accept': 'application/vnd.lichess.v4+json'
};

export function toRound(data) {
  return $.ajax({
    method: 'post',
    url: `/lichess/round/${data.gameId}/to`,
    headers: headers,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data)
  });
}

export function updatePlayer(gameId, data) {
  return $.ajax({
    method: 'post',
    url: `/lichess/round/${gameId}/updatePlayer`,
    headers: headers,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data)
  });
}

export function finish(gameId, data) {
  return $.ajax({
    method: 'post',
    url: `/lichess/round/${gameId}/finish`,
    headers: headers,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data)
  });
}

export function redirected(gameId) {
  return $.ajax({
    method: 'post',
    url: `/lichess/round/${gameId}/redirected`,
    headers: headers
  });
}
