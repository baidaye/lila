import * as round from './round';
import * as game from 'game';
import * as status from 'game/status';
import * as ground from './ground';
import * as title from './title';
import * as promotion from './promotion';
import * as speech from './speech';
import * as cg from 'chessground/types';
import { Player, Source, Status } from 'game';
import { Config as CgConfig } from 'chessground/config';
import { Api as CgApi } from 'chessground/api';
import {ClockController, ClockData} from './clock/clockCtrl';
import sound = require('./sound');
import util = require('./util');
import * as xhr from './xhr'
import * as keyboard from './keyboard';
import * as ChessJS from 'chess.js';
import ApiCtrl from './apiCtrl';
import OAuth2PKCE from '../../lichsssApiOAuth/oauth2';
import {
  RoundOpts,
  RoundData,
  Redraw,
  Position,
  Pref,
  Step,
  initialFen, BackendRound
} from './interfaces';

type Timeout = number;

const li = window.lichess;

export default class RoundController {

  gameId: string;
  chess: any; // chess.js
  oAuth2Ctrl: OAuth2PKCE;
  apiCtrl: ApiCtrl;
  data: RoundData;
  chessground: CgApi;
  clock: ClockController;
  trans: Trans;
  noarg: TransNoArg;

  ply: number = 0;
  flip: boolean = false;
  loading: boolean = false;
  loadingTimeout: number;
  resignConfirm?: Timeout = undefined;
  drawConfirm?: Timeout = undefined;
  // will be replaced by view layer
  autoScroll: () => void = $.noop;
  lastDrawOfferAtPly?: Ply;
  importFinished: boolean = false;
  canClaimVictory: boolean = false;
  takebackDeclined: boolean = false;
  drawDeclined: boolean = false;

  private music?: any;

  challenge: any;

  connectModal: boolean = false;

  constructor(readonly opts: RoundOpts, readonly redraw: Redraw) {
    this.gameId = opts.round.id;
    this.trans = li.trans(this.opts.i18n);
    this.noarg = this.trans.noarg;
    this.chess = ChessJS.Chess();
    this.oAuth2Ctrl = new OAuth2PKCE(opts.oAuth2);
    if(this.isSpectator()) {
      location.href = `https://lichess.org/${this.gameId}`;
    }

    this.data = this.defaultRoundData(this.opts.pref, this.opts.round);
    this.clock = new ClockController(this.data, {
      onFlag: () => {
        // clock end
        this.redraw();
      },
      soundColor: this.data.player.color
    });
    this.apiCtrl = new ApiCtrl(this);

    li.pubsub.on('sound_set', set => {
      if (!this.music && set === 'music')
        li.loadScript('javascripts/music/play.js').then(() => {
          this.music = li.playMusic();
        });
      if (this.music && set !== 'music') this.music = undefined;
    });

    li.pubsub.on('zen', () => {
      if (this.isPlaying()) {
        const zen = !$('body').hasClass('zen');
        $('body').toggleClass('zen', zen);
        li.dispatchEvent(window, 'resize');
        $.post('/pref/zen', { zen: zen ? 1 : 0 });
      }
    });
  }

  openConnectModal = () => {
    this.connectModal = true;
    this.redraw();
  };

  closeConnectModal = () => {
    this.connectModal = false;
    this.redraw();
  };

  reconnect = () => {
    location.reload();
  };

  onGameFull = (gameFull: any) => {
    this.updatePlayer(gameFull);
    this.onGameState(gameFull.state);
    const d = this.data;

    this.ply = round.lastPly(d);
    this.jump(this.ply);

    setTimeout(() => this.delayedInit(gameFull), 200);

    setTimeout(this.showExpiration, 350);
  };

  private delayedInit = (gameFull: any): void => {
    const d = this.data;
    if (this.isPlaying() && game.nbMoves(d, d.player.color) === 0) {
      li.sound.genericNotify();
    }
    li.requestIdleCallback(() => {
      if (this.isPlaying()) {
        title.init();
        this.setTitle();

        window.addEventListener('beforeunload', e => {
          if (li.hasToReload || !game.playable(d) || !d.clock) return;
          const msg = '对局正在进行中！';
          (e || window.event).returnValue = msg;
          return msg;
        });
      }

      keyboard.init(this);
      speech.setup(this);
    });

    if(status.finished(d) || status.aborted(d)) {
      this.importFinished = true;
    }
    this.setRedirected();
    this.ifGameFinish(gameFull);
    this.redraw();
  };

  private showExpiration = () => {
    if (!this.data.expiration) return;
    this.redraw();
    setTimeout(this.showExpiration, 250);
  };

  setRedirected = () => {
    if(this.isPlayer() && !this.opts.round.redirected) {
      xhr.redirected(this.gameId);
    }
  };

  ifGameFinish = (gameFull) => {
    const state = gameFull.state;
    const statusId = status.ids[state.status];
    const s = {
      id: statusId,
      name: state.status
    };
    this.onGameFinish(s, state.winner);
  };

  onGameStart = (msg: any) => {
    const g = msg.game;
    const c = this.data.clock!;
    const d = this.oAuth2Ctrl.makeRoundData(g, {limit: c.initial, increment: c.increment});
    if(!location.href.includes(g.id)) {
      xhr.toRound(d).then((res) => {
        if(!res.redirected) {
          location.href = `/lichess/round/${g.id}/${g.color}`;
        }
      });
    }
  };

  onGameFinish = (s: Status, winner?: Color, ratingDiff?: any): void => {
    this.data.game.winner = winner;
    this.data.game.status = s;
    if(!status.finished(this.data) && !status.aborted(this.data)) return;

    this.userJump(round.lastPly(this.data));
    this.chessground.stop();
    if (!this.data.spectator && this.data.game.turns > 1) {
      li.sound[winner ? (this.data.player.color === winner ? 'victory' : 'defeat') : 'draw']();
    }

    this.setTitle();
    this.redraw();
    this.autoScroll();
    speech.status(this);

    if(!this.isFinishedOrAborted(this.opts.round.status.id)) {
      this.setLoading(true);
      let data = {status: s.id, winner: winner, ratingDiff: ratingDiff };
      xhr.finish(this.gameId, data).then((res) => {
        this.importFinished = true;
        this.playerByColor('white').ratingDiff = res.white;
        this.playerByColor('black').ratingDiff = res.black;
        this.setLoading(false);
      });
    }
  };

  toRatingDiff = (data, statusId) => {
    if(data && data.rated && this.isFinishedOrAborted(statusId)) {
      let ratingDiff = {};
      ratingDiff[data.color] = data.ratingDiff;
      ratingDiff[this.opponentOf(data.color)] = data.opponent.ratingDiff;
      return ratingDiff;
    }
    return null;
  };

  onGameState = (state: any): void => {
    const d = this.data;
    const playing = this.isPlaying();
    const steps = this.movesToSteps(initialFen, state.moves);
    const lastStep = steps[steps.length - 1];
    const playedColor = lastStep.color as Color; //lastStep.ply % 2 === 0 ? 'black' : 'white';
    const afterPlayedColor = this.opponentOf(playedColor);
    const activeColor = d.player.color === d.game.player;
    const turns = this.stepsToTurns(steps);

    if (!this.replaying() && (steps.length !== d.steps.length)) {
      d.steps = steps;
      this.userJump(lastStep.ply);
    } else {
      this.ply = lastStep.ply;
    }

    d.steps = steps;
    d.game.turns = turns;
    d.game.player = afterPlayedColor;
    d.game.threefold = !!state.threefold;
    d.game.status = {
      id: status.ids[state.status],
      name: state.status
    };

    this.playerByColor('white').offeringDraw = state.wdraw;
    this.playerByColor('black').offeringDraw = state.bdraw;

    this.playerByColor('white').proposingTakeback = state.wtakeback;
    this.playerByColor('black').proposingTakeback = state.btakeback;

    this.setTitle();
    game.setOnGame(d, playedColor, true);

    const delay = (playing && activeColor) ? 0 : 1;
    this.clock.setClock(d, state.wtime / 1000, state.btime / 1000, delay);

    if (this.data.expiration) {
      if (this.data.steps.length > 2) this.data.expiration = undefined;
      else this.data.expiration.movedAt = Date.now();
    }

    this.redraw();
    if (!this.replaying() && playedColor != d.player.color) {
      if (!this.chessground.playPremove()) {
        promotion.cancel(this);
      }
    }
    this.autoScroll();
  };

  onChatLine = (msg) => {
    if(msg.text.includes('declines draw')) {
      this.onDeclineDraw(msg);
    }

    if(msg.text.includes('Takeback declined')) {
      this.onAcceptedTakeback();
    }
    if(msg.text.includes('Takeback accepted')) {
      this.onDeclinedTakeback();
    }
  };

  onOpponentGone = (msg) => {
    game.setIsGone(this.data, this.data.opponent.color, msg.gone);
    this.redraw();

    setTimeout(() => {
      if(this.data.opponent.isGone) {
        this.canClaimVictory = true;
        this.redraw();
      }
    }, msg.claimWinInSeconds * 1000);
  };

  //  A player sends you a challenge or you challenge someone
  onChallenge = (challenge: any) => {
    const c = this.challenge = challenge;
    if(this.isSameClock(c)) {
      this.setRematching(c, true);
    }
  };

  onChallengeCanceled = (challenge: any) => {
    this.challenge = null;
    if(this.isSameClock(challenge)) {
      this.setRematching(challenge, false);
    }
  };

  onChallengeDeclined = (challenge: any) => {
    if(this.isSameClock(challenge)) {
      this.setRematching(challenge, false);
      this.challenge = null;
    }
  };

  private isSameClock = (challenge: any) => {
    return (this.data.clock &&
      challenge.timeControl.limit === this.data.clock.initial &&
      challenge.timeControl.increment === this.data.clock.increment);
  };

  private setRematching = (challenge: any, rematch: boolean) => {
    // @ts-ignore
    if (challenge.challenger.id === this.data.opponent.lichess.id) {
      this.data.opponent.offeringRematch = rematch;
    } else {
      // @ts-ignore
      if (challenge.challenger.id === this.data.player.lichess.id) {
        this.data.player.offeringRematch = rematch;
      }
    }
    this.redraw();
  };

  private onUserMove = (orig: cg.Key, dest: cg.Key, meta: cg.MoveMetadata) => {
    if (!promotion.start(this, orig, dest, meta)) {
      this.sendMove(orig, dest, undefined, meta);
    }
  };

  // @ts-ignore
  private onMove = (_: cg.Key, dest: cg.Key, captured?: cg.Piece) => {
/*    if (captured) {
      sound.capture();
    } else sound.move();*/
  };

  private onPremove = (orig: cg.Key, dest: cg.Key, meta: cg.MoveMetadata) => {
    promotion.start(this, orig, dest, meta);
  };

  private onCancelPremove = () => {
    promotion.cancelPrePromotion(this);
  };

  makeCgHooks = () => ({
    onUserMove: this.onUserMove,
    onMove: this.onMove,
    onNewPiece: sound.move,
    onPremove: this.onPremove,
    onCancelPremove: this.onCancelPremove
  });

  userJump = (ply: Ply): void => {
    this.chessground.selectSquare(null);
    if (ply != this.ply && this.jump(ply)) speech.userJump(this, this.ply);
    else this.redraw();
  };

  jump = (ply: Ply): boolean => {
    ply = Math.max(round.firstPly(this.data), Math.min(round.lastPly(this.data), ply));
    const isForwardStep = ply === this.ply + 1;
    this.ply = ply;
    const s = this.stepAt(ply),
      config: CgConfig = {
        fen: s.fen,
        lastMove: util.uci2move(s.uci),
        check: !!s.check,
        turnColor: this.ply % 2 === 0 ? 'white' : 'black'
      };
    if (this.replaying()) this.chessground.stop();
    else config.movable = {
      color: this.isPlaying() ? this.data.player.color : undefined,
      dests: this.toDests()
    }
    this.chessground.set(config);
    if (s.san && isForwardStep) {
      if (s.san.includes('x')) sound.capture();
      else sound.move();
      if (/[+#]/.test(s.san)) sound.check();
    }
    this.autoScroll();
    return true;
  };

  flipNow = () => {
    this.flip = !this.flip;
    this.chessground.set({
      orientation: ground.boardOrientation(this.data, this.flip)
    });
    this.redraw();
  };

  playerByColor = (c: Color) => this.data[c === this.data.player.color ? 'player' : 'opponent'];

  playerAt = (position: Position) =>
    (this.flip as any) ^ ((position === 'top') as any) ? this.data.opponent : this.data.player;

  setTitle = () => title.set(this);

  setLoading = (v: boolean, duration: number = 3000, rd: boolean = true) => {
    clearTimeout(this.loadingTimeout);
    if (v) {
      this.loading = true;
      this.loadingTimeout = setTimeout(() => {
        this.loading = false;
        if(rd) this.redraw();
      }, duration);
      if(rd) this.redraw();
    } else if (this.loading) {
      this.loading = false;
      if(rd) this.redraw();
    }
  };

  setChessground = (cg: CgApi) => {
    this.chessground = cg;
  };

  isPlaying = () => game.isPlayerPlaying(this.data);

  replaying = (): boolean => this.ply !== round.lastPly(this.data);

  replayEnabledByPref = (): boolean => {
    const d = this.data;
    return d.pref.replay === 2 || (d.pref.replay === 1 && d.game.speed === 'classical');
  };

  isLate = () => this.replaying() && status.playing(this.data);

  stepAt = (ply: Ply) => round.plyStep(this.data, ply);

  reload = (): void => {
    this.setTitle();
    this.redraw();
    this.autoScroll();
    this.setLoading(false);
  };

  isPlayer = () => {
    let userId = this.opts.userId;
    return userId === this.opts.round.white.local.id || userId === this.opts.round.black.local.id;
  };

  isSpectator = () => !this.isPlayer();

  opponentOf = (c: Color) => {
    return (c === 'white' ? 'black' : 'white') as Color;
  };

  isFinishedOrAborted = (status: number) => {
    return status >= 25
  };

  defaultRoundData = (pref: Pref, r: BackendRound) => {
    let c = this.opts.color;
    let p = r[c];
    let o = r[this.opponentOf(c)];
    let d = {
      game: {
        id: r.id,
        status: r.status,
        player: this.opts.color as Color,
        turns: 0, // 每方走一步+1
        startedAtTurn: 0,
        source: r.source as Source,
        speed: r.speed,
        variant: r.variant,
        winner: undefined,
        moveCentis: [],
        initialFen: initialFen,
        importedBy: undefined,
        threefold: false,
        boosted: false,
        rematch: undefined,
        rated: r.rated,
        perf: r.perf.key,
        createdAt: r.createdAt,
        appt: false,
        apptComplete: false
      },
      player: {
        id: p.local.id,
        name: p.local.username,
        title: p.local.title,
        rating: p.local.rating,
        provisional: p.local.provisional ? '1' : '',
        ratingDiff: p.local.ratingDiff,
        head: p.local.head,
        lichess: p.lichess,
        isLichess: p.isLichess,
        spectator: false,
        color: p.color as Color,
        proposingTakeback: p.proposingTakeback,
        offeringRematch: p.offeringRematch,
        offeringDraw: p.offeringDraw,
        ai: null,
        onGame: true,
        isGone: false,
        blurs: undefined,
        hold: undefined,
        checks: undefined,
        engine: false,
        berserk: false,
        version: 0
      } as Player,
      opponent: {
        id: o.local.id,
        name: o.local.username,
        title: o.local.title,
        rating: o.local.rating,
        provisional: o.local.provisional ? '1' : '',
        ratingDiff: o.local.ratingDiff,
        head: o.local.head,
        lichess: o.lichess,
        isLichess: o.isLichess,
        spectator: false,
        color: o.color as Color,
        proposingTakeback: o.proposingTakeback,
        offeringRematch: o.offeringRematch,
        offeringDraw: o.offeringDraw,
        ai: null,
        onGame: true,
        isGone: false,
        blurs: undefined,
        hold: undefined,
        checks: undefined,
        engine: false,
        berserk: false,
        version: 0
      } as Player,
      clock: {
        running: false,
        initial: r.clock.initial,
        increment: r.clock.increment,
        white: r.clock.initial,
        black: r.clock.initial,
        emerg: Math.min(60, Math.max(10, r.clock.initial / 8)),
        showTenths: pref.clockTenths,
        showBar: pref.clockBar,
        moretime: 0
      } as ClockData,
      pref: pref,
      steps: [this.initStep(initialFen)],
      spectator: this.isSpectator(),
      tournament: undefined,
      contest: undefined,
      simul: undefined,
      correspondence: undefined,
      takebackable: true,
      moretimeable: false,
    } as RoundData;

    if(this.expirable(d)) {
      d.expiration = {
        idleMillis: r.gameStart.secondsLeft * 1000,
        millisToMove: this.millisToMove(r.gameStart.speed),
        movedAt: Date.now() - r.gameStart.secondsLeft * 1000
      }
    }

    return d;
  };

  expirable = (d) => {
    return game.bothPlayersHavePlayed(d) && (d.source == 'pool') && game.playable(d);
  };

  updatePlayer = (gameFull: any) => {
    if(!this.isFinishedOrAborted(status.ids[gameFull.state.status])) {
      const opponentColor = this.opponentOf(this.opts.color);
      const player = gameFull[this.opts.color];
      const opponent = gameFull[opponentColor];

      const d = this.data;
      // @ts-ignore
      d.player.lichess.rating = player.rating;
      // @ts-ignore
      d.player.lichess.provisional = player.provisional;
      // @ts-ignore
      d.player.lichess.title = player.title;

      // @ts-ignore
      d.opponent.rating = opponent.rating;
      // @ts-ignore
      d.opponent.provisional = opponent.provisional;
      // @ts-ignore
      d.opponent.title = opponent.title;

      // @ts-ignore
      d.opponent.lichess.rating = opponent.rating;
      // @ts-ignore
      d.opponent.lichess.provisional = opponent.provisional;
      // @ts-ignore
      d.opponent.lichess.title = opponent.title;

      const data = {};
      data[this.opts.color] = {id: player.id, username: player.name, rating:  player.rating, title: player.title, provisional: player.provisional, color: this.opts.color};
      data[opponentColor] = {id: opponent.id, username: opponent.name, rating:  opponent.rating, title: opponent.title, provisional: opponent.provisional, color: opponentColor};
      xhr.updatePlayer(this.gameId, data)
    }
  };

  initStep = (initialFen: string) => {
    return {
      ply: 0,
      fen: initialFen,
      san: '',
      uci: '',
      color: 'white'
    }
  };

  stepsToPlayer = (steps: Step[]) => {
    return (steps[steps.length - 1].ply % 2 === 0 ? 'white' : 'black') as Color;
  };

  stepsToTurns = (steps: Step[]) => {
    return Math.max(steps.length - 1, 0); // 每方走一步+1
  };

  movesToSteps = (initialFen: string, moves: string) => {
    this.chess.load(initialFen);

    let steps: Step[] = [this.initStep(initialFen)];
    if(moves) {
      moves.split(' ').forEach((uci, index) => {
        let mv = this.chess.move(uci, { sloppy: true });
        let step = {
          ply: index + 1,
          fen: this.chess.fen(),
          san: mv.san,
          uci: uci,
          color: (mv.color === 'w') ? 'white' : 'black',
          check: this.chess.in_check(),
          checkmate: this.chess.in_checkmate()
        } as Step;

        steps.push(step);
      });
    }

    return steps;
  };

  toDests = () => {
    const dests = {};
    this.chess.SQUARES.forEach(s => {
      const ms = this.chess.moves({square: s, verbose: true});
      if (ms.length) dests[s] = ms.map(m => m.to);
    });
    return dests as cg.Dests;
  };

  toColor = () => {
    return (this.chess.turn() === 'w') ? 'white' : 'black';
  };

  millisToMove = (speed: string) => {
    let seconds = 0;
    switch (speed) {
      case 'ultraBullet': {
        seconds = 15;
        break;
      }
      case 'bullet': {
        seconds = 20;
        break;
      }
      case 'blitz': {
        seconds = 25;
        break;
      }
      case 'rapid': {
        seconds = 30;
        break;
      }
      default: {
        seconds = 35;
        break;
      }
    }
    return seconds * 1000;
  };

  // @ts-ignore
  sendMove = (orig: cg.Key, dest: cg.Key, prom: cg.Role | undefined, meta: cg.MoveMetadata) => {
    let move = orig + dest;
    if (prom) move += (prom === 'knight' ? 'n' : prom[0]);
    this.apiCtrl.userMove(this.gameId, move);
  };

  abort = () => {
    this.setLoading(true);
    this.apiCtrl.abort(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  offerTakeback = () => {
    this.setLoading(true);
    this.apiCtrl.offerTakeback(this.gameId).then((response) => {
      if(response.ok) {
        this.chessground.cancelPremove();
        promotion.cancel(this);
      }
      this.setLoading(false);
    });
  };

  acceptTakeback = () => {
    this.setLoading(true);
    this.apiCtrl.acceptTakeback(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  declineTakeback = () => {
    this.setLoading(true);
    this.apiCtrl.declineTakeback(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  canOfferDraw = (): boolean =>
    game.drawable(this.data) && (this.lastDrawOfferAtPly || -99) < (this.ply - 20);

  offerDraw = (v: boolean): void => {
    if (this.canOfferDraw()) {
      if (this.drawConfirm) {
        if (v) this.doOfferDraw();
        clearTimeout(this.drawConfirm);
        this.drawConfirm = undefined;
      } else if (v) {
        if (this.data.pref.confirmResign) this.drawConfirm = setTimeout(() => {
          this.offerDraw(false);
        }, 3000);
        else this.doOfferDraw();
      }
    }
  };

  private doOfferDraw = () => {
    this.lastDrawOfferAtPly = this.ply;
    this.setLoading(true);
    this.apiCtrl.offerDraw(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  acceptDraw = () => {
    this.setLoading(true);
    this.apiCtrl.acceptDraw(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  declineDraw = () => {
    this.setLoading(true);
    this.apiCtrl.declineDraw(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  onDeclineDraw = (msg) => {
    let color = (msg.text === 'White declines draw') ? 'black' : 'white';
    this.playerByColor(color as Color).offeringDraw = false;
    this.drawDeclined = true;
    this.redraw();

    setTimeout(() => {
      this.drawDeclined = false;
      this.redraw();
    }, 2000);
  };

  onAcceptedTakeback = () => {

  };

  onDeclinedTakeback = () => {
    this.takebackDeclined = true;
    this.redraw();
    setTimeout(() => {
      this.takebackDeclined = false;
      this.redraw();
    }, 2000);
  };

  forceResignable = (): boolean => {
    const d = this.data;
    return !d.opponent.ai &&
      game.isForceResignable(d) &&
      this.canClaimVictory &&
      !!d.clock &&
      d.opponent.isGone &&
      !game.isPlayerTurn(d) &&
      game.resignable(d);
  };

  resign = (v: boolean): void => {
    if (v) {
      if (this.resignConfirm || !this.data.pref.confirmResign) {
        this.apiCtrl.resign(this.gameId);
        clearTimeout(this.resignConfirm);
      } else {
        this.resignConfirm = setTimeout(() => this.resign(false), 3000);
      }
      this.redraw();
    } else if (this.resignConfirm) {
      clearTimeout(this.resignConfirm);
      this.resignConfirm = undefined;
      this.redraw();
    }
  };

  claimVictory = () => {
    this.setLoading(true);
    this.apiCtrl.claimVictory(this.gameId).then(() => {
      this.setLoading(false);
    });
  };

  createChallenge = (): void => {
    this.data.player.offeringRematch = true;
    this.redraw();

    const clock = this.data.clock;
    if(clock) {
      // @ts-ignore
      this.oAuth2Ctrl.openStream(`/api/challenge/${this.data.opponent.lichess.username}`,
        {
          single: this.oAuth2Ctrl.signal,
          method: 'post',
          body: `clock.limit=${clock.initial}&clock.increment=${clock.increment}&rated=true&keepAliveStream=true`
        },
        _ => {}
      )
    }
  };

  cancelChallenge = () => {
    this.apiCtrl.cancelChallenge(this.challenge.id).then(() => {
      this.data.player.offeringRematch = false;
      this.redraw();
    });
  };

  acceptChallenge = () => {
    this.apiCtrl.acceptChallenge(this.challenge.id).then(() => {
      this.data.opponent.offeringRematch = false;
      this.redraw();
    });
  };

  declineChallenge = () => {
    this.apiCtrl.declineChallenge(this.challenge.id).then(() => {
      this.data.opponent.offeringRematch = false;
      this.redraw();
    });
  };

}
