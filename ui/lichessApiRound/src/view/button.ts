import { h } from 'snabbdom'
import { VNode } from 'snabbdom/vnode'
import { Hooks } from 'snabbdom/hooks'
import * as util from '../util';
import * as game from 'game';
import * as status from 'game/status';
import { game as gameRoute } from 'game/router';
import { RoundData, MaybeVNodes } from '../interfaces';
import RoundController from '../ctrl';

export function standard(
  ctrl: RoundController,
  condition: ((d: RoundData) => boolean) | undefined,
  icon: string,
  hint: string,
  onclick?: () => void
): VNode {
  // disabled if condition callback is provided and is falsy
  const enabled = function() {
    return !condition || condition(ctrl.data);
  };
  return h(`button.fbt.${icon}.${hint}.${Date.now()}`, {
    attrs: {
      disabled: !enabled(),
      title: ctrl.noarg(hint)
    },
    hook: util.bind('click', _ => {
      if (enabled()) onclick ? onclick() : () => {};
    })
  }, [
    h('span', util.justIcon(icon))
  ]);
}

export function cancelTakebackProposition(ctrl: RoundController) {
  return ctrl.data.player.proposingTakeback ? h('div.pending', [
    h('p', ctrl.noarg('takebackPropositionSent')),
    h('button.button.disabled', {
      attrs: { disabled: true },
      hook: util.bind('click', () => {})
    }, ctrl.noarg('cancel'))
  ]) : null;
}

function acceptButton(ctrl: RoundController, action: () => void, i18nKey: string = 'accept') {
  const text = ctrl.noarg(i18nKey);
  return h('a.accept', {
    attrs: {
      'data-icon': 'E',
      title: text
    },
    hook: util.bind('click', action)
  });
}

function declineButton(ctrl: RoundController, action: () => void, i18nKey: string = 'decline') {
  const text = ctrl.noarg(i18nKey);
  return h('a.decline', {
    attrs: {
      'data-icon': 'L',
      title: text
    },
    hook: util.bind('click', action)
  });
}

export function answerOpponentTakebackProposition(ctrl: RoundController) {
  return ctrl.data.opponent.proposingTakeback ? h('div.negotiation.takeback', [
    h('p', ctrl.noarg('yourOpponentProposesATakeback')),
    acceptButton(ctrl, ctrl.acceptTakeback),
    declineButton(ctrl, ctrl.declineTakeback)
  ]) : null;
}

function actConfirm(ctrl: RoundController, f: (v: boolean) => void, transKey: string, icon: string, klass?: string): VNode {
  return h('div.act-confirm.' + transKey, [
    h('button.fbt.yes.' + (klass || ''), {
      attrs: { title: ctrl.noarg(transKey), 'data-icon': icon },
      hook: util.bind('click', () => f(true))
    }),
    h('button.fbt.no', {
      attrs: { title: ctrl.noarg('cancel'), 'data-icon': 'L' },
      hook: util.bind('click', () => f(false))
    })
  ]);
}

export function drawConfirm(ctrl: RoundController): VNode {
  return actConfirm(ctrl, ctrl.offerDraw, 'offerDraw', '2', 'draw-yes');
}

export function cancelDrawOffer(ctrl: RoundController) {
  return ctrl.data.player.offeringDraw ? h('div.pending', [
    h('p', ctrl.noarg('drawOfferSent'))
  ]) : null;
}

export function answerOpponentDrawOffer(ctrl: RoundController) {
  return ctrl.data.opponent.offeringDraw ? h('div.negotiation.draw', [
    h('p', ctrl.noarg('yourOpponentOffersADraw')),
    acceptButton(ctrl, ctrl.acceptDraw),
    declineButton(ctrl, ctrl.declineDraw)
  ]) : null;
}

export function forceResign(ctrl: RoundController) {
  return ctrl.forceResignable() ? h('div.suggestion', [
    h('p', { hook: onSuggestionHook }, ctrl.noarg('opponentLeftChoices')),
    h('button.button', {
      hook: util.bind('click', ctrl.claimVictory)
    }, ctrl.noarg('forceResignation')), // 宣布获胜
    h('button.button.disabled', {
      attrs: { disabled: true },
      hook: util.bind('click', () => {})
    }, ctrl.noarg('forceDraw'))
  ]) : null;
}

export function resignConfirm(ctrl: RoundController): VNode {
  return actConfirm(ctrl, ctrl.resign, 'resign', 'b');
}

export function threefoldClaimDraw(ctrl: RoundController) {
  return ctrl.data.game.threefold ? h('div.suggestion', [
    h('p', {
      hook: onSuggestionHook
    }, ctrl.noarg('threefoldRepetition')),
    h('button.button', {
      hook: util.bind('click', () => {})
    }, ctrl.noarg('claimADraw'))
  ]) : null;
}

export function followUp(ctrl: RoundController): VNode {
  const d = ctrl.data,
    rematchable = !d.game.rematch && (status.finished(d) || status.aborted(d)) && !d.game.boosted,
    newable = status.finished(d) || status.aborted(d),
    rematchZone = (rematchable || d.game.rematch ? rematchButtons(ctrl): []);
  return h('div.follow-up', [
    ...rematchZone,
    newable ? h('a.fbt', {
      attrs: { href: '/lobby' },
    }, ctrl.noarg('newOpponent')) : null,
    analysisButton(ctrl)
  ]);
}

function analysisButton(ctrl: RoundController): VNode | null {
  const d = ctrl.data,
    url = gameRoute(d, d.player.color) + '#' + ctrl.ply;
  return game.replayable(d) ? h('a.fbt', {
    attrs: { href: url },
    hook: util.bind('click', _ => {
      // force page load in case the URL is the same
      if (location.pathname === url.split('#')[0]) location.reload();
    })
  }, ctrl.noarg('analysis')) : null;
}

function rematchButtons(ctrl: RoundController): MaybeVNodes {
  const d = ctrl.data,
    me = !!d.player.offeringRematch,
    them = !!d.opponent.offeringRematch,
    noarg = ctrl.noarg;
  return [
    them ? h('button.rematch-decline', {
      attrs: {
        'data-icon': 'L',
        title: noarg('decline')
      },
      hook: util.bind('click', () => {
        ctrl.declineChallenge();
      })
    },  '') : null,
    h('button.fbt.rematch.white', {
      class: {
        me,
        glowing: them,
      },
      attrs: {
        title: them ? noarg('yourOpponentWantsToPlayANewGameWithYou') : (
          me ? noarg('rematchOfferSent') : '')
      },
      hook: util.bind('click', e => {
        const d = ctrl.data;
        if (d.player.offeringRematch) {
          ctrl.cancelChallenge();
        } else if (d.opponent.offeringRematch) {
          ctrl.acceptChallenge();
        } else if (!(e.target as HTMLElement).classList.contains('disabled')) {
          ctrl.createChallenge();
        }
      }, ctrl.redraw)
    }, [
      me ? util.spinner() : h('span', noarg('rematch'))
    ])
  ];
}

/*
export function watcherFollowUp(ctrl: RoundController): VNode | null {
  const d = ctrl.data,
    content = [
      d.game.rematch ? h('a.fbt.text', {
        attrs: {
          'data-icon': 'v',
          href: `/${d.game.rematch}/${d.opponent.color}`
        }
      }, ctrl.noarg('viewRematch')) : null,
      analysisButton(ctrl)
    ];
  return content.find(x => !!x) ? h('div.follow-up', content) : null;
}*/

export const onSuggestionHook: Hooks = util.onInsert(
  el => window.lichess.pubsub.emit('round.suggestion', el.textContent)
);

