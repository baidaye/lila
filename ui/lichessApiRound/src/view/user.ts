import { h } from 'snabbdom'
import { Player } from 'game';
import { Position } from '../interfaces';
import RoundController from '../ctrl';

export function aiName(ctrl: RoundController, level: number) {
  return ctrl.trans('aiNameLevelAiLevel', 'Stockfish', level);
}

export function userHtml(player: Player, position: Position) {
  const
    rating = player.rating + (player.provisional ? '?' : ''),
    rd = player.ratingDiff,
    ratingDiff = rd === 0 ? h('span', '±0') : (
      rd && rd > 0 ? h('good', '+' + rd) : (
        rd && rd < 0 ? h('bad', '−' + (-rd)) : undefined
      ));
  // @ts-ignore
  const isLichess = player.isLichess;
  // @ts-ignore
  const lichess = player.lichess;
  return h(`div.ruser-${position}.ruser.user-link`, {
    class: {
      online: player.onGame,
      offline: !player.onGame,
      long: player.name.length > 16
    }
  }, [
    h('i.line'),
    h('a.text', {
      class: {ulpt: !isLichess},
      attrs: {
        'data-pt-pos': 's',
        href: (!isLichess ? `/@/${player.name}` : `https://lichess.org/@/${lichess.username}`),
        target: '_blank'
      }
    }, player.title ? [h('span.title',player.title == 'BOT' ? { attrs: {'data-bot': true } } : {}, player.title ), ' ', player.name] : [player.name]),
    h('rating', rating),
    ratingDiff
  ]);
}
