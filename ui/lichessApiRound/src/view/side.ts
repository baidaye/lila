import {h} from 'snabbdom'
import * as status from 'game/status';
import viewStatus from 'game/view/status';
import {Player} from 'game';
import RoundController from '../ctrl';

export default function (ctrl: RoundController) {
  let d = ctrl.data;
  let r = ctrl.opts.round;
  let winner = d.game.winner;
  return h('aside.round__side', [
    h('div.game__meta', [
      h('section', [
        h('div.game__meta__infos', {attrs: {'data-icon': r.perf.icon}}, [
          h('div.header', [
            h('div.setup', [
              r.clock.show + ' • ',
              (d.game.rated ? '有积分' : '无积分') + ' • ',
              r.perf.name
            ]),
            h('time.timeago', {
              hook: {
                insert(vnode) {
                  (vnode.elm as HTMLElement).setAttribute('datetime', '' + r.createdAt);
                }
              }
            }, window.lichess.timeago.format(r.createdAt))
          ])
        ]),
        h('div.game__meta__players', [
          readerPlayer(ctrl.playerByColor('white')),
          readerPlayer(ctrl.playerByColor('black'))
        ])
      ]),
      status.finished(d) || status.aborted(d) ? h('section.status', [
        viewStatus(ctrl),
        winner ? ' • ' + ctrl.trans.noarg(winner + 'IsVictorious') : ''
      ]) : null
    ])
  ])
}

function readerPlayer(player: Player) {
  const baseUrl = $('body').data('asset-url');
  // @ts-ignore
  const isLicess = player.isLichess;
  // @ts-ignore
  const lichess = player.lichess;
  // @ts-ignore
  const head = player.head ? '/image/' + player.head: baseUrl + '/assets/images/head-default-64.png';
  const rating = player.rating + (player.provisional ? '?' : '');
  const rd = player.ratingDiff;
  const ratingDiff = rd === 0 ? h('span', '±0') : (
    rd && rd > 0 ? h('good', '+' + rd) : (
      rd && rd < 0 ? h('bad', '−' + (-rd)) : undefined
    ));
  return h('div.player.color-icon.is.text', {
    class: {white: player.color === 'white'},
    attrs: {'data-icon': player.color === 'white' ? 'K' : 'J'}
  }, [
    h('a.user-link', {
      attrs: {href: (!isLicess ? `/@/${player.name}` : `https://lichess.org/@/${lichess.username}`), target: '_blank'},
      class: {online: player.onGame, offline: !player.onGame, ulpt: !isLicess}
    }, [
      h('div.head-line', [
        h('img.head', {attrs: {src: head}}),
        h('i.line')
      ]),
      player.title ? h('span.title', player.title == 'BOT' ? {attrs: {'data-bot': true}} : {}, player.title) : null,
      h('span.u_name', ` ${player.name}（${rating}）`),
      ratingDiff,
    ])
  ])
}

