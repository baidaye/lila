import {h} from 'snabbdom'
import {VNode} from 'snabbdom/vnode'
import * as promotion from '../promotion';
import * as keyboard from '../keyboard';
import * as gridHacks from './gridHacks'
import renderSide from './side';
import {renderTable} from './table';
import {render as renderGround} from '../ground';
import {read as fenRead} from 'chessground/fen';
import {plyStep} from '../round';
import {Position, MaterialDiff, MaterialDiffSide, CheckCount} from '../interfaces';
import RoundController from '../ctrl';
import * as util from '../util';
import {modal} from './modal';

function wheel(ctrl: RoundController, e: WheelEvent): boolean {
  if (ctrl.isPlaying()) return true;
  e.preventDefault();
  if (e.deltaY > 0) keyboard.next(ctrl);
  else if (e.deltaY < 0) keyboard.prev(ctrl);
  ctrl.redraw();
  return false;
}

const emptyMaterialDiff: MaterialDiff = {
  white: {},
  black: {}
};

function renderMaterial(material: MaterialDiffSide, score: number, position: Position, checks?: number) {
  const children: VNode[] = [];
  let role: string, i: number;
  for (role in material) {
    if (material[role] > 0) {
      const content: VNode[] = [];
      for (i = 0; i < material[role]; i++) content.push(h('mpiece.' + role));
      children.push(h('div', content));
    }
  }
  if (checks) for (i = 0; i < checks; i++) children.push(h('div', h('mpiece.king')));
  if (score > 0) children.push(h('score', '+' + score));
  return h('div.material.material-' + position, children);
}

export function main(ctrl: RoundController): VNode {
  const d = ctrl.data,
    cgState = ctrl.chessground && ctrl.chessground.state,
    topColor = d[ctrl.flip ? 'player' : 'opponent'].color,
    bottomColor = d[ctrl.flip ? 'opponent' : 'player'].color;
  let material: MaterialDiff, score: number = 0;
  if (d.pref.showCaptured) {
    let pieces = cgState ? cgState.pieces : fenRead(plyStep(ctrl.data, ctrl.ply).fen);
    material = util.getMaterialDiff(pieces);
    score = util.getScore(pieces) * (bottomColor === 'white' ? 1 : -1);
  } else material = emptyMaterialDiff;

  const checks: CheckCount = (d.player.checks || d.opponent.checks) ?
    util.countChecks(ctrl.data.steps, ctrl.ply) :
    util.noChecks;

  return h('main.round', [
    renderSide(ctrl),
    h('div.round__app.variant-standard', {
      hook: util.onInsert(gridHacks.start)
    }, [
      h('div.round__app__board.main-board', {
        hook: window.lichess.hasTouchEvents ? undefined :
          util.bind('wheel', (e: WheelEvent) => wheel(ctrl, e), undefined, false)
      }, [
        renderGround(ctrl),
        promotion.view(ctrl)
      ]),
      renderMaterial(material[topColor], -score, 'top', checks[topColor]),
      ...renderTable(ctrl),
      renderMaterial(material[bottomColor], score, 'bottom', checks[bottomColor]),
    ]),
    h('div.round__underboard'),
    h('div.round__underchat'),
    ctrl.connectModal ? connectModal(ctrl) : null
  ]);
}

function connectModal(ctrl: RoundController): VNode {
  return modal({
    class: 'connectModal',
    onClose() {
      ctrl.closeConnectModal();
    },
    content: [
      h('h2', '连接已断开'),
      h('p', '与服务器已断开连接，请检查网络。'),
      h('div.actions', [
        h('button.button.small', {
          hook: util.bind('click', ctrl.reconnect)
        }, '重连'),
        h('button.button.button-red.small', {
          hook: util.bind('click', ctrl.closeConnectModal)
        }, '关闭')
      ]),
    ]
  });
}
