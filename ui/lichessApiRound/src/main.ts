import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import {menuHover} from 'common/menuHover';
import {Chessground} from 'chessground';
import {RoundOpts} from './interfaces';
import RoundController from './ctrl';
import {main as view} from './view/main';

export function start(opts: RoundOpts) {

  const patch = init([klass, attributes]);

  let vnode: VNode, ctrl: RoundController;

  function redraw() {
    vnode = patch(vnode, view(ctrl));
  }

  ctrl = new RoundController(opts, redraw);

  const blueprint = view(ctrl);
  opts.element.innerHTML = '';
  vnode = patch(opts.element, blueprint);

  window.addEventListener('resize', redraw); // col1 / col2+ transition

  ctrl.isPlaying() && menuHover();
}

window.Chessground = Chessground;
