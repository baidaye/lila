import StudentCtrl from './ctrl';

interface Handlers {
  [key: string]: any;
}

export interface Socket {
  send: SocketSend;
  receive(type: string, data: any): boolean;
}

export function make(send: SocketSend, ctrl: StudentCtrl): Socket {
  const handlers: Handlers = {
    start() {
      ctrl.trainInfo();
    },
    stop() {
      ctrl.trainInfo();
    },
    kick() {
      location.reload();
    },
    taskCreate() {
      ctrl.getTasks();
    },
    taskChangeStatus() {
      ctrl.getTasks();
    },
    taskChangeProgress() {
      ctrl.getTasks();
    },
    gameTaskCreate() {
      ctrl.getTasks();
    },
    gameTaskStart(_) {
      ctrl.getTasks();
    },
    gameTaskFinish() {
      ctrl.getTasks();
    }
  };

  return {
    receive(type: string, data: any): boolean {
      const handler = handlers[type];
      if (handler) handler(data);
      return true;
    },
    send
  };
}
