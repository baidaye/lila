import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import StudentCtrl from '../ctrl';
import {bind} from '../util';

export default function(ctrl: StudentCtrl): VNode {
  return h('div.trainCourse-student__top', [
    h('div.trainCourseTop', [
      h('div.left', [
        h('div.title', ctrl.trainCourse.name),
        h('div.time', `${ctrl.trainCourse.timeBegin} ~ ${ctrl.trainCourse.timeEnd}`),
        h('div.status', {
          class: {
            created: (ctrl.trainCourse.status.id === 'created'),
            started: (ctrl.trainCourse.status.id === 'started'),
            stopped: (ctrl.trainCourse.status.id === 'stopped')
          }
        }, ctrl.trainCourse.status.name)
      ]),
      h('div.action', [
        ctrl.trainCourse.status.id !== 'stopped' && ctrl.opts.signed ? h(`button.button.button-red.small.sign.${ctrl.opts.signed}`, {
          hook: bind('click', () => {
            ctrl.signOut();
          })
        }, '退  出') : null,
        ctrl.trainCourse.status.id !== 'stopped' && !ctrl.opts.signed ? h(`button.button.button-green.small.sign.${ctrl.opts.signed}`, {
          hook: bind('click', () => {
            ctrl.signIn();
          })
        }, '签  到') : null
      ])
    ])
  ]);
}


