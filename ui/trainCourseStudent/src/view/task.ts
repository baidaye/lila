import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import {spinner} from '../util';
import StudentCtrl from '../ctrl';

export default function (ctrl: StudentCtrl): VNode {
  return h('div.trainCourse-student__task', [
    h('div.title', '任务列表'),
    h('table.slist', [
      h('thead', [
        h('tr', [
          h('th', '任务名称'),
          h('th', '任务状态'),
          h('th', '进度'),
          h('th', '操作')
        ])
      ]),
      h('tbody', ctrl.taskLoading ? h('tr', [
        h('td.empty', {attrs: {colspan: 4}}, [spinner()])
      ]) : !ctrl.tasks || ctrl.tasks.length == 0 ? h('tr', [
        h('td.empty', {attrs: {colspan: 4}}, '暂无任务')
      ]) : ctrl.tasks.map((task) => {
        return h(`tr.${task.id}`, [
          h('td', task.name),
          h('td', {
            class: {
              training: (task.status.id === 'train'),
              complete: (task.status.id === 'finished'),
              canceled: (task.status.id === 'canceled'),
              uncomplete: (task.status.id === 'created')
            }
          }, task.status.name),
          h('td', `${task.progress}%`),
          h('td.action', [
            h(`a.button.button-empty.small`, { attrs: { target: '_blank', href: (task.itemType.id === 'trainGame' ? `/ttask/${task.id}/doTask` : `/ttask/${task.id}`) } }, '进入')
          ])
        ]);
      }))
    ])
  ]);
}
