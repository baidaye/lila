import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import StudentCtrl from '../ctrl';
import readerTop from './top'
import readerSide from './side'
import readerTask from './task'
import {bind} from '../util';

export default function (ctrl: StudentCtrl): VNode {
  return h('main.page.page-small.trainCourse-student', [
    readerTop(ctrl),
    readerSide(ctrl),
    readerTask(ctrl),
    ctrl.trainCourse.status.id !== 'stopped' && !ctrl.opts.signed ? signModal(ctrl) : null
  ]);
}

export function signModal(ctrl: StudentCtrl): VNode | undefined {
  return h('div#modal-overlay', [
    h('div#modal-wrap.trainCourseSign__modal', [
      h('div.title', ctrl.trainCourse.name),
      h('div.time', `${ctrl.trainCourse.timeBegin} ~ ${ctrl.trainCourse.timeEnd}`),
      h('div.action', [
        h(`button.button.button-green.sign`, {
          hook: bind('click', () => {
            ctrl.signIn();
          })
        }, '签  到')
      ])
    ])
  ]);
}
