import {h} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import StudentCtrl from '../ctrl';
import {onInsert} from '../util';

export default function(ctrl: StudentCtrl): VNode {
  return h('div.trainCourse-student__side', [
    h('div.chat', [
      h('section.mchat', {
        hook: onInsert(_ => {
          if (ctrl.opts.chat.instance) ctrl.opts.chat.instance.destroy();
          ctrl.opts.chat.parseMoves = true;
          window.lichess.makeChat(ctrl.opts.chat, chat => {
            ctrl.opts.chat.instance = chat;
          });
        })
      })
    ])
  ]);
}
