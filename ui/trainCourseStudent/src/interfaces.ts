import {VNode} from 'snabbdom/vnode'
import {Pref} from '../../round/src/interfaces';
import {Task} from '../../ttask/src/interfaces';
import {TrainCourse} from '../../trainCourseCoach/src/interfaces';
export type MaybeVNode = VNode | string | null | undefined;
export type MaybeVNodes = MaybeVNode[]

export interface TrainCourseOpts {
  element: HTMLElement;
  socketSend: SocketSend;
  i18n: any;
  userId: string;
  trainCourse: TrainCourse;
  tasks: Task[];
  signed: boolean;
  pref: Pref;
  chat: any;
}

export type Redraw = () => void;

