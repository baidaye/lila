const headers = {
  'Accept': 'application/vnd.lichess.v3+json'
};

export function signIn(id: string) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/student/signIn`,
    headers: headers
  });
}

export function signOut(id: string) {
  return $.ajax({
    method: 'post',
    url: `/trainCourse/${id}/student/signOut`,
    headers: headers
  });
}

export function trainInfo(id: string) {
  return $.ajax({
    url: `/trainCourse/${id}/info`,
    headers: headers
  });
}

export function getTasks(id: string, userId: string) {
  return $.ajax({
    url: `/ttask/list/trainCourse?metaId=${id}&userId=${userId}&excludeTrainGame=false`,
    headers: headers
  });
}
