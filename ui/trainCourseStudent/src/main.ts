import {init} from 'snabbdom';
import {VNode} from 'snabbdom/vnode'
import klass from 'snabbdom/modules/class';
import attributes from 'snabbdom/modules/attributes';
import throttle from 'common/throttle';
import {Chessground} from 'chessground';
import * as chat from 'chat';
import StudentCtrl from './ctrl';
import {TrainCourseOpts} from './interfaces';

export const patch = init([klass, attributes]);

import view from './view/main';

export function start(opts: TrainCourseOpts) {

  let vnode: VNode;

  let redraw = throttle(100, () => {
    vnode = patch(vnode, view(ctrl));
  });

  const ctrl = new StudentCtrl(opts, redraw);

  const blueprint = view(ctrl);
  vnode = patch(opts.element, blueprint);

  return {
    socketReceive: ctrl.socket.receive,
    redraw: ctrl.redraw
  };
}

// that's for the rest of lichess to access chessground
// without having to include it a second time
window.Chessground = Chessground;
window.LichessChat = chat;

