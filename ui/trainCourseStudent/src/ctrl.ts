import {TrainCourseOpts, Redraw} from './interfaces';
import {Task} from '../../ttask/src/interfaces';
import {TrainCourse} from '../../trainCourseCoach/src/interfaces';
import {make as makeSocket, Socket} from './socket';
import * as xhr from './xhr'

export default class StudentCtrl {

  opts: TrainCourseOpts;
  socket: Socket;
  trainCourse: TrainCourse;
  tasks: Task[];
  taskLoading: boolean = false;

  constructor(opts: TrainCourseOpts, readonly redraw: Redraw) {
    this.opts = opts;
    this.trainCourse = opts.trainCourse;
    this.tasks = opts.tasks;
    this.socket = makeSocket(this.opts.socketSend, this);
  }

  trainInfo = () => {
    xhr.trainInfo(this.trainCourse.id).then((data) => {
      this.trainCourse = data;
      this.redraw();
    });
  };

  signIn = () => {
    xhr.signIn(this.trainCourse.id).then((res) => {
      if(res.error) {
        alert(res.error);
        return;
      }

      this.opts.signed = true;
      this.redraw();

      alert('签到成功，请注意新任务！');
    });
  };

  signOut = () => {
    if(confirm('退出后将无法继续接收新任务，是否继续？')) {
      xhr.signOut(this.trainCourse.id).then(() => {
        this.opts.signed = false;
        this.redraw();

        location.href = '/'
      });
    }
  };

  getTasks = window.lichess.debounce(() => {
    this.taskLoading = true;
    this.redraw();
    xhr.getTasks(this.trainCourse.id, this.opts.userId).then((data) => {
      this.tasks = data;
      this.taskLoading = false;
      this.redraw();
    });
  }, 500);

}

