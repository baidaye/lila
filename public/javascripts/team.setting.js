$(function () {

  let $setting = $('.setting');
  $setting.find('.tabs > div').not('.disabled').click(function () {
    let active = $(this).data('tab');
    $setting.find('.tabs > div').removeClass('active');
    $setting.find('.panels > .panel').removeClass('active');
    $setting.find('.panels > .panel.' + active).addClass('active');
    $(this).addClass('active');
    location.hash = active;
  });

  setTabActive();
  function setTabActive() {
    let hash = location.hash;
    if(!$setting.find('.tabs > div[data-tab="' + hash + '"]').hasClass('disabled')) {
      if(hash) {
        hash = hash.replace('#', '');
        $setting.find('.tabs > div').removeClass('active');
        $setting.find('.tabs > div[data-tab="' + hash + '"]').addClass('active');
        $setting.find('.panels > .panel').removeClass('active');
        $setting.find('.panels > .panel.' + hash).addClass('active');
      }
    }
  }

  $('a.tag-add').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        let $form = $('.tag');
        $form.find('#form3-typ').change(function () {
          let v = $(this).val();
          if (v === 'single_choice' || v === 'multiple_choice') {
            $('#form3-value').prop('required', true).parent().removeClass('none')
          } else {
            $('#form3-value').prop('required', false).parent().addClass('none')
          }
        });
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  $('a.tag-edit').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  $('a.tag-delete').click(function (e) {
    e.preventDefault();
    if (confirm('删除后成员对应的标签也将删除，是否继续？')) {
      $.ajax({
        method: 'post',
        url: $(this).attr('href'),
        success: function () {
          location.reload();
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
    }
    return false;
  });

  //---------------------------------------------------------------------------------------------

  $('a.campus-add').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        autocomplete();
        campusSubmit();
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  $('a.campus-edit').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        autocomplete();
        campusSubmit();
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  $('a.campus-delete').click(function (e) {
    e.preventDefault();
    if (confirm('确认删除？')) {
      $.ajax({
        method: 'post',
        url: $(this).attr('href'),
        success: function () {
          location.reload();
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
    }
    return false;
  });

});

function autocomplete() {
  let $from = $('.campus').find('form');
  let $input = $from.find('.member-autocomplete');
  lichess.loadCssPath('autocomplete');
  lichess.loadScript('javascripts/vendor/typeahead.jquery.min.js').done(function() {
    $input.typeahead({
      minLength: 2,
    }, {
      hint: true,
      highlight: false,
      source: function(query, _, runAsync) {
        let teamId = $input.data('id');
        let q = query.trim();
        if (q) {
          $.ajax({
            url: `/team/${teamId}/member/autocomplete?q=${q}`,
            cache: true,
            success: function(res) {
              if (res.length === 10) res.push(null);
              runAsync(res);
            }
          });
        }
      },
      limit: 10,
      displayKey: 'displayName',
      templates: {
        empty: '<div class="empty">匹配不到成员</div>',
        pending: lichess.spinnerHtml,
        suggestion: function(o) {
          return `<div class = "member">${o.displayName}</div>`;
        }
      }
    });
    $input.on('typeahead:select', function(e, sel) {
      $from.find('#form3-admin').val(sel.userId);
    });
    $input.on('input propertychange',function(){
      console.log(11111111111);
      $from.find('#form3-admin').val('');
    })
  });
}

function campusSubmit() {
  $('.campus').find('form').submit(function (e) {
    e.preventDefault();
    let $form = $(this);
    let campusId = $form.find('input[name=campusId]').val();
    let userId = $form.find('#form3-admin').val();
    let teamId = $form.find('input[name="adminName"]').data('id');
    if(!userId) {
      doSubmit();
    } else {
      $.get(`/team/${teamId}/member/canBeAdmin?userId=${userId}&campusId=${campusId ? campusId: ''}`).then(function (res) {
        if(res.ok) {
          doSubmit()
        } else {
          alert('添加的管理员不是俱乐部成员，或者已经是俱乐部管理员');
        }
      });
    }
    return false;
  });

  function doSubmit() {
    let $form = $('.campus').find('form');
    let userMark = $form.find('input[name="adminName"]').val();
    if(!userMark) {
      $form.find('#form3-admin').val('');
    }
    $.ajax({
      method: 'POST',
      url: $form.attr('action'),
      data: $form.serialize(),
      success: function() {
        location.reload();
      },
      error: function(res) {
        alert(res);
      }
    });
  }
}
