$(function() {

  $('body').removeClass('coords-in').addClass('coords-out');

  let $page = $('.patterns-kingposRank');
  setDesc();
  $page.find('input[name="color"]').change(function() {
    find();
  });

  $page.find('input[name^="checkerRole"]').change(function() {
    let $this = $(this);
    let $cks = $this.parents('.single').find('input:checked').not(this);
    if($cks.length > 0) {
      $cks.prop('checked', false);
    }
    find();
  });

  function find() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let role = $page.find('input[name^="checkerRole"]:checked').val();
    return $.ajax({
      url: `/patterns/${patternsType}/kingpos/rank/find?color=${color}&checkerRole[0]=${role ? role : ''}`
    }).then(d => {
      setPage(true, d);
      setUrl();
    }).fail(function(err) {
      if(err.status === 404) {
        setPage(false, {});
        setUrl();
        alert('没有更多了~');
      } else {
        alert('发生错误')
      }
    });
  }

  function setPage(success, data) {
    setDesc();

    let $squares = $page.find('.squares');
    $squares.find('td').each(function () {
      $(this)
        .removeClass()
        .find('a')
        .text('')
        .attr('href', ``);
    });

    if(success) {
      let patternsType = $page.find('input[name="patternsType"]').val();
      let color = $page.find('input[name="color"]:checked').val();
      let role = $page.find('input[name^="checkerRole"]:checked').val();
      $.each(data, function (_, d) {
        $squares
          .find(`td[data-square=${d.kingSquare}]`)
          .addClass(squareClass(d.percentValue))
          .find('a')
          .text(d.percent)
          .attr('href', `/patterns/${patternsType}/kingpos/rank/feature?kingSquare=${d.kingSquare}&color=${color}&checkerRole[0]=${role ? role : ''}`);
      });

      function squareClass(percent) {
        let cls = '';
        if (percent >= 15) cls = "s1";
        else if (percent >= 12) cls = "s2";
        else if (percent >= 10) cls = "s3";
        else if (percent >= 8) cls = "s4";
        else if (percent >= 6) cls = "s5";
        else if (percent >= 4) cls = "s6";
        else if (percent >= 2) cls = "s7";
        else if (percent >= 1) cls = "s8";
        else if (percent >= 0.7) cls = "s9";
        else if (percent >= 0.4) cls = "s10";
        else if (percent >= 0.2) cls = "s11";
        else if (percent >= 0.1) cls = "s12";
        else if (percent >= 0.08) cls = "s13";
        else if (percent >= 0.05) cls = "s14";
        else cls = "s15";
        return cls;
      }
    }
  }

  function setDesc() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let checkerRole = $page.find('input[name^="checkerRole"]:checked').next('label').text();
    let label = (patternsType === 'single' || patternsType === 'double') ? '将杀' : '逼和';
    let desc = `${color === 'white' ? '白' : '黑'}棋${ checkerRole ? `${checkerRole}` : ''}${label}，按${color === 'white' ? '黑' : '白'}王位置分布`;
    $page.find('.kingposDesc').text(desc);
  }

  function setUrl() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let role = $page.find('input[name^="checkerRole"]:checked').val();
    history.replaceState(null, '', `/patterns/${patternsType}/kingpos/rank?color=${color}&checkerRole[0]=${role ? role : ''}`);
  }

  setGround();
  function setGround() {
    let $board = $page.find('.cg-wrap');
    $board.removeClass('parse-fen-manual');
    let mainGround = $board.data('chessground');
    let fen = $board.data('fen');
    let color = $board.data('color');
    let coord = !!$board.data('coordinates');
    let config = {
      fen: fen,
      orientation: color,
      movable: {
        free: false,
        color: null,
        dests: {}
      },
      highlight: {
        check: true,
        lastMove: true
      },
      drawable: { enabled: false, visible: false },
      resizable: true,
      coordinates: coord,
      addPieceZIndex: false
    };

    if (mainGround) mainGround.set(config);
    else $board.data('chessground', Chessground($board[0], config));
  }

});
