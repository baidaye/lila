$(function() {

  let $page = $('.patterns-rank');

  const maxOrder = 999999;
  const emptyFen = '8/8/8/8/8/8/8/8 w - -';

  const orderMap = {
    single_white_asc: 1,
    single_white_desc: 1489,
    single_black_asc: 1,
    single_black_desc: 1487,
    double_white_asc: 1,
    double_white_desc: 924,
    double_black_asc: 1,
    double_black_desc: 890,
    stalemate_white_asc: 1,
    stalemate_white_desc: 130,
    stalemate_black_asc: 1,
    stalemate_black_desc: 139
  };

  setUrl();

  $page.find('button.prev').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  $page.find('button.next').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  let flipped = false;
  $page.find('button.flip').click(function() {
    let $board = $page.find('.main-board .cg-wrap');
    let orient = $board.data('color');
    $board.addClass('parse-fen-manual');
    if(orient === 'white') {
      $board.data('color', 'black');
    } else {
      $board.data('color', 'white');
    }
    flipped = orient === 'white';
    next();
  });

  $page.find('input[name="color"]').change(function() {
    $page.find('input[name="sort"]').trigger('change');
  });

  $page.find('input[name="sort"]').change(function() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let sort = $page.find('input[name="sort"]:checked').val();

    let orderField = `${patternsType}_${color}_${sort}`;
    let currOrder = orderMap[orderField];
    let prevOrder, nextOrder;
    if(sort === 'asc') {
      prevOrder = '';
      nextOrder = orderMap[orderField] + 1;
    } else {
      prevOrder = orderMap[orderField] - 1;
      nextOrder = '';
    }

    let $prev = $page.find('button.prev');
    let $next = $page.find('button.next');

    $page.find('#form3-order').val(currOrder);

    if(prevOrder) {
      $prev.removeClass('disabled').prop('disabled', false).data('id', prevOrder);
    } else {
      $prev.addClass('disabled').prop('disabled', true).data('id', '');
    }

    if(nextOrder) {
      $next.removeClass('disabled').prop('disabled', false).data('id', nextOrder);
    } else {
      $next.addClass('disabled').prop('disabled', true).data('id', '');
    }

    next();
  });

  $page.find('#form3-order').on('change paste keyup', function() {
    next();
  });

  function next() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let order = $page.find('#form3-order').val();
    let color = $page.find('input[name="color"]:checked').val();
    let sort = $page.find('input[name="sort"]:checked').val();
    return $.ajax({
      url: `/patterns/${patternsType}/rank/next?color=${color}&order=${order}&sort=${sort}`
    }).then(d => {
      setPage(true, d);
      setUrl();
    }).fail(function(err) {
      if(err.status === 404) {
        setPage(false, {});
        setUrl();
      } else {
        alert('发生错误')
      }
    });
  }

  function setPage(success, data) {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let $board = $page.find('.cg-wrap');
    let $boardBtm = $page.find('.patterns__btm');
    let $order = $page.find('#form3-order');
    let sort = $page.find('input[name="sort"]:checked').val();

    let $prev = $page.find('button.prev');
    let $next = $page.find('button.next');

    let curr = data.curr;
    let prev = sort === 'asc' ? data.prev : data.next;
    let next = sort === 'asc' ? data.next : data.prev;
    success = success && curr;
    if(success) {
      $order.val(curr.order);
      $board.data('color', flipped ? 'black' : 'white');
      $board.data('fen', curr.simplifiedFen);
      $board.addClass('parse-fen-manual');
      $boardBtm.find('.patternsOp').text(curr.patternsOp).attr('href', `/patterns/${patternsType}/rank/feature?patternsOp=${curr.patternsOp}&color=${curr.win}&flipped=${flipped}`);
      $boardBtm.find('.percent').text(curr.percent);

      if(prev) {
        $prev.removeClass('disabled').prop('disabled', false).data('id', prev.order);
      } else {
        $prev.addClass('disabled').prop('disabled', true).data('id', '');
      }

      if(next) {
        $next.removeClass('disabled').prop('disabled', false).data('id', next.order);
      } else {
        $next.addClass('disabled').prop('disabled', true).data('id', '');
      }
    } else {
      $board.data('color', 'white');
      $board.data('fen', emptyFen);
      $board.addClass('parse-fen-manual');
      $boardBtm.find('.patternsOp').text('- -').attr('href', ``);
      $boardBtm.find('.percent').text('- -');

      $prev.addClass('disabled').prop('disabled', true).data('id', '');
      $next.addClass('disabled').prop('disabled', true).data('id', '');
    }

    setGround();
  }

  function setUrl() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let sort = $page.find('input[name="sort"]:checked').val();
    let order = $page.find('#form3-order').val();
    history.replaceState(null, '', `/patterns/${patternsType}/rank?color=${color}&sort=${sort}&order=${order ? order : ''}`);
  }

  setGround();
  function setGround() {
    let $board = $page.find('.cg-wrap');
    $board.removeClass('parse-fen-manual');
    let mainGround = $board.data('chessground');
    let fen = $board.data('fen');
    let color = $board.data('color');
    let coord = !!$board.data('coordinates');
    let is3d = $board.hasClass('is3d');
    let config = {
      fen: fen,
      orientation: color,
      movable: {
        free: false,
        color: null,
        dests: {}
      },
      highlight: {
        check: true,
        lastMove: true
      },
      drawable: { enabled: false, visible: false },
      resizable: true,
      coordinates: coord,
      addPieceZIndex: is3d
    };

    if (mainGround) mainGround.set(config);
    else $board.data('chessground', Chessground($board[0], config));
  }

});
