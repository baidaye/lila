function onMemberOpen(reader) {

  memberAdvance();
  let $modal = $('.member-modal');
  let $search = $modal.find('.member-search');

  $search.submit(function (e) {
    e.preventDefault();
    search();
    return false;
  });

  function search() {
    $.get({
      method: 'GET',
      url: $search.attr('action'),
      data: $search.serialize(),
      success: function (members) {
        reader ? reader(members) : defaultRender(members);
      },
      error: function (e) {
        handleError(e);
      }
    });
  }

  if($modal.length > 0) {
    $search.find('.flatpickr').flatpickr();
    search();
  }

  let defaultRender = (members) => {
    let $table = $modal.find('.member-table tbody');
    $table.empty();
    if (members && members.length > 0) {
      let html = members.map((mem, i) => {
        let headSrc = mem.head ? '/image/' + mem.head : $('body').data('asset-url') + '/assets/images/head-default-64.png';
        return `
          <tr>
            <td class="check">
              <input type="checkbox" name="members[${i}]" value="${mem.userId}">
            </td>
            <td>
              <a class="offline user-link ulpt" href="/@/${mem.username}">
                <div class="head-line">
                  <img class="head" src=${headSrc}>
                  <i class="line"></i>
                </div>
                <span class="u_name">${mem.username}</span>
              </a>
            </td>
            <td>${mem.mark}</td>
            <td>${mem.level}</td>
          </tr>`;
      });
      $table.html(html);
    } else {
      $table.html(`<tr><td colspan="4" class="no-more">没有找到.</td></tr>`);
    }

    let $list = $table.find('tr');
    $modal.find('#member_chk_all').click(function () {
      let isChecked = $(this).is(':checked');
      $list.find('input[name^="members"]').prop('checked', false);
      if(isChecked) {
        $list.find('input[name^="members"]').prop('checked', true);
      }
    });
  };

}

function memberAdvance() {
  let $search = $('.member-search');

  $search.find('.show-search').click(function (e) {
    let $this = $(this);
    if($this.hasClass('expanded')) {
      $this.removeClass('expanded').html('显示高级搜索<i data-icon="R"></i>');
      $search.find('.tr-tag').addClass('none');
      $search.find('input[name="advance"]').val("false");
    } else {
      $this.addClass('expanded').html('隐藏高级搜索<i data-icon="S"></i>');
      $search.find('.tr-tag').removeClass('none');
      $search.find('input[name="advance"]').val("true");
    }
  });
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
