let $jq = $.noConflict(true);
$jq(function () {

  let $tree = $jq('.dbtree');
  $tree.jstree({
    'types': {
      '#': {
        'max_depth': 10
      },
      'default': {
        'max_children': 100
      }
    },
    'core': {
      'strings': {
        'New node': '新建文件夹'
      },
      'worker': false,
      'data': {
        'url': '/resource/gamedb/tree/load',
        'data': function (node) {
          return {selected: $jq('input[name="selected"]').val()};
        }
      },
      'check_callback': function (operation, node, node_parent, node_position, more) {
        if(more && more.dnd && node_parent.id === '#') {
          return false;
        }
        return true;
      },
    },
    'plugins': ['types', 'unique', 'dnd', 'contextmenu'],
    'contextmenu': {
      'select_node': false,
      'items': function (node) {
        return contextMenu(node);
      }
    }
  }).on('create_node.jstree', function (e, data) {
      createNode(data);
    })
    .on('rename_node.jstree', function (e, data) {
      renameNode(data);
    })
    .on('delete_node.jstree', function (e, data) {
      deleteNode(data);
    })
    .on('move_node.jstree', function (e, data) {
      moveNode(data);
    })
    .on('changed.jstree', function (e, data) {
      if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
        let selected = data.node.id;
        location.href = `/resource/gamedb?selected=${selected}`;
      }
    });

  function contextMenu(node) {
    let defaultContextmenu = $jq.jstree.defaults.contextmenu.items();
    defaultContextmenu.create.label = '新建目录';
    defaultContextmenu.create.icon = 'icon-create';
    defaultContextmenu.rename.label = '修改目录';
    defaultContextmenu.rename.icon = 'icon-rename';
    defaultContextmenu.remove.label = '删除目录';
    defaultContextmenu.remove.icon = 'icon-remove';

    delete defaultContextmenu.ccp;
    if (node.parent === '#') {
      delete defaultContextmenu.rename;
      delete defaultContextmenu.remove;
    }
    return defaultContextmenu;
  }

  function createNode(data) {
    $.post('/resource/gamedb/tree/create', {
      'parent': data.node.parent,
      'name': data.node.text,
      'sort': (data.position + 1)
    })
      .done(function (d) {
        data.instance.set_id(data.node, d.id);
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  function renameNode(data) {
    if (data.text === data.old || data.node.id.length !== 8) {
      return false;
    }
    $.post(`/resource/gamedb/tree/${data.node.id}/rename`, {'name': data.text})
      .done(function (d) {
        data.instance.refresh();
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  function deleteNode(data) {
    if (confirm('删除目录后所有子目录和对局将一并删除，是否确认操作？')) {
      $.post(`/resource/gamedb/tree/${data.node.id}/remove`)
        .fail(function () {
          data.instance.refresh();
        });
    } else {
      data.instance.refresh();
    }
  }

  function moveNode(data) {
    let parent = data.new_instance.get_node(data.node.parent);
    let children = parent.children;
    $.post(`/resource/gamedb/tree/${data.node.id}/move`, {'parent': data.parent, 'children': children})
      .done(function (d) {
        data.instance.refresh();
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  $jq('.tree-create').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    sel = ref.create_node(sel, {"type":"file"});
    if(sel) {
      ref.edit(sel);
    }
  });

  $jq('.tree-rename').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    ref.edit(sel);
  });

  $jq('.tree-delete').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    ref.delete_node(sel);
  });

  $jq('.gamedb').find('.btn-create').click(function (e) {
    e.preventDefault();
    let gamedbId = $jq('input[name="selected"]').val();
    $.ajax({
      url: `/resource/gamedb/rel/create?gamedbId=${gamedbId}`
    }).then(function (html) {

      $.modal($jq(html));
      let $md = $jq('.gamedbrel-create');
      $md.find('.tabs-horiz span').click(function () {
        let $this = $jq(this);
        $md.find('.tabs-horiz span').removeClass("active");
        $md.find('.tabs-content div').removeClass("active");
        let cls = $this.attr('class');
        $this.addClass('active');
        $md.find('.tabs-content div.' + cls).addClass('active');
        $md.find('input[name=tab]').val(cls);
      });

      $md.find('input[type=file]').on('change', function () {
        let file = this.files[0];
        if (!file) return;
        let reader = new FileReader();
        reader.onload = function (e1) {
          let r = e1.target.result;
          $md.find('textarea').val(r);
        };
        reader.readAsText(file);
      });

      $jq('.cancel').click(function () {
        $.modal.close();
      });

      $('#form3-tags').tagsInput({
        'height': '40px',
        'width': '100%',
        'interactive': true,
        'defaultText': '添加',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 10,
        'placeholderColor': '#666666'
      });

      create($md);
      return false
    });

  });

  function create($md) {
    $md.find('form').submit(function (e) {
      e.preventDefault();
      let $form = $md.find('.form3');
      if (!$form.find('#form3-game').val() && !$form.find('#form3-pgn').val() && !$form.find('#form3-chapter').val()) {
        alert('输入一种PGN获取方式');
        return false;
      }
      let gamedbId = $jq('input[name="selected"]').val();
      $.ajax({
        method: 'POST',
        url: `/resource/gamedb/rel/create?gamedbId=${gamedbId}`,
        data: $form.serialize()
      }).then(function (res) {
        location.reload();
      }, function (err) {
        handleError(err);
      });
      return false;
    });
  }

  let $list = $(".infinitescroll");
  $jq('select.action').change(function() {
    let $this = $jq(this);
    let action = $this.val();
    if (!action) return;

    let ids = [];
    $list.find('.selected').each(function () {
      return ids.push($jq(this).attr('data-id'));
    });

    if (ids.length === 0) {
      return;
    }

    if (action === 'gameRelDelete') {
      if (confirm('删除 ' + ids.length + ' 个资源？')) {
        let url = '/resource/gamedb/rel/delete?ids=' + ids.join(',');
        $jq.post(url).done(function () {
          location.reload();
        });
      } else {
        $this.val('');
      }
    }

    else if (action === 'gameRelEdit') {
      showEdit('gamedb', ids);
    }

    else if (action === 'shareTo') {
      shareTo(ids);
    }
  });

  function showEdit(action, ids) {
    $jq.ajax({
      url: `/resource/${action}/rel/edit?relId=${ids[0]}`,
      success: function (html) {
        $.modal($jq(html));
        $jq('.cancel').click(function () {
          $jq('select.action').val('');
          $.modal.close();
        });

        $('#form3-tags').tagsInput({
          'height': '40px',
          'width': '100%',
          'interactive': true,
          'defaultText': '添加',
          'removeWithBackspace': true,
          'minChars': 0,
          'maxChars': 10,
          'placeholderColor': '#666666'
        });

        let $form = $jq('.gamedbrel-edit').find('form');
        $form.submit(function (e) {
          e.preventDefault();
          $jq.ajax({
            method: 'POST',
            url: `/resource/${action}/rel/update?relId=${ids[0]}`,
            data: $form.serialize(),
            success: function () {
              $.modal.close();
              location.reload();
            },
            error: function (res) {
              handleError(res);
            }
          });
          return false;
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
  }

  function shareTo(ids) {
    $jq.ajax({
      url: `/resource/gamedb/share?ids=${ids.join(',')}`,
      success: function (html) {
        $.modal($jq(html));
        $jq('.cancel').click(function () {
          $jq('select.action').val('');
          $.modal.close();
        });
        let $form = $jq('.gamedbrel-share').find('form');

        $form.find('#form3-clazzId').change(function () {
          let clazzId = $(this).val();
          let $tb = $form.find('.stu-list tbody');
          if(clazzId) {
            $jq.get(`/clazz/student/load?classId=${clazzId}`)
              .fail(function(e) { handleError(e); })
              .done(function(stus) {
                $tb.empty();
                if(stus) {
                  if(stus.length > 0) {
                    let html = stus.map(function (stu, i) {
                      let headSrc = stu.head ? '/image/' + stu.head : $('body').data('asset-url') + '/assets/images/head-default-64.png';
                      return `
                      <tr>
                        <td class="check">
                          <input type="checkbox" name="students[${i}]" value="${stu.userId}">
                        </td>
                        <td>
                          <a class="offline user-link ulpt" href="/@/${stu.username}">
                            <div class="head-line">
                              <img class="head" src=${headSrc}>
                              <i class="line"></i>
                            </div>
                            <span class="u_name">${stu.mark}</span>
                          </a>
                        </td>
                      </tr>`;
                    });
                    $tb.html(html);
                  } else {
                    $tb.html(`<tr><td class="no-more">没有找到.</td></tr>`);
                  }

                  $form.find('.stu-search #ck-all').change(function() {
                    let isChecked = $(this).is(':checked');
                    $tb.find('input[name^="students"]').prop('checked', isChecked);
                  });

                  $form.find('.stu-search .search').on('input propertychange', function() {
                    let txt = $(this).val();
                    if($.trim(txt) !== ''){
                      $tb.find('tr').not('tr:contains("' + txt + '")').css('display', 'none');
                      $tb.find('tr').filter('tr:contains("' + txt + '")').css('display', 'table-row');
                    } else {
                      $tb.find('tr').css('display', 'table-row');
                    }
                  });
                }
              });
          } else {
            $tb.html(`<tr><td class="no-more">没有找到.</td></tr>`);
          }
        });
        $form.find('#form3-clazzId').trigger('change');

        $form.submit(function (e) {
          e.preventDefault();
          $jq.ajax({
            method: 'POST',
            url: '/resource/gamedb/share',
            data: $form.serialize(),
            success: function () {
              $.modal.close();
              location.reload();
            },
            error: function (res) {
              handleError(res);
            }
          });
          return false;
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
  }

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if (typeof json.error === 'string') {
          alert(json.error);
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }

});

