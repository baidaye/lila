$(function() {

  let $page = $('.oAuth2List');

  $page.find('.slist .cancel').click(function () {
    if(confirm('是否确认取消lichess.org授权？')) {
      let $this = $(this);
      let state = $this.data('state');
      let token = $this.data('token');
      revokeAccessToken(state, token);
    }
  });

  function revokeAccessToken(state, accessToken) {
    fetch(`https://lichess.org/api/token`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    }).then(() => {
      fetch(`/lichess/oauth/setUnActive?state=${state}`, {
        method: 'POST'
      }).then(() => {
        location.reload();
      });
    })
  }

  $page.find('.create').click(function () {

    const PKCE_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';
    const lichessHost = 'https://lichess.org';
    const clientId = 'haichess.com';
    const redirectUrl = `${location.protocol}//${location.host}/lichess/oauth/callback`;
    const scopes = ['board:play', 'challenge:write', 'challenge:read'];

    function base64urlEncode(value) {
      let base64 = btoa(value);
      base64 = base64.replace(/\+/g, '-');
      base64 = base64.replace(/\//g, '_');
      base64 = base64.replace(/=/g, '');
      return base64;
    }

    async function generatePKCECodes() {
      const output = new Uint32Array(96);
      crypto.getRandomValues(output);
      const codeVerifier = base64urlEncode(Array
        .from(output)
        .map((num) => PKCE_CHARSET[num % PKCE_CHARSET.length])
        .join(''));

      return crypto
        .subtle
        .digest('SHA-256', (new TextEncoder()).encode(codeVerifier))
        .then((buffer) => {
          let hash = new Uint8Array(buffer);
          let binary = '';
          let hashLength = hash.byteLength;
          for (let i = 0; i < hashLength; i++) {
            binary += String.fromCharCode(hash[i]);
          }
          return binary;
        })
        .then(base64urlEncode)
        .then((codeChallenge) => ({ codeChallenge, codeVerifier }));
    }

    function generateState() {
      const output = new Uint32Array(32);
      crypto.getRandomValues(output);
      return Array
        .from(output)
        .map((num) => PKCE_CHARSET[num % PKCE_CHARSET.length])
        .join('');
    }

    async function redirectLichessOAuth2() {
      const {codeChallenge, codeVerifier} = await generatePKCECodes();
      const state = generateState();
      let authUrl = `${lichessHost}/oauth`;
      let url = authUrl
        + `?response_type=code&`
        + `client_id=${encodeURIComponent(clientId)}&`
        + `redirect_uri=${encodeURIComponent(redirectUrl)}&`
        + `scope=${encodeURIComponent(scopes.join(' '))}&`
        + `state=${state}&`
        + `code_challenge=${encodeURIComponent(codeChallenge)}&`
        + `code_challenge_method=S256`;

      // store {codeVerifier, state} to backend
      return $.ajax({
        method: 'post',
        url: '/lichess/oauth/create',
        data: {state, verifierCode: codeVerifier, scopes: scopes},
        headers: {
          'Accept': 'application/vnd.lichess.v3+json'
        }
      }).then(() => {
        location.replace(url);
      });
    }

    redirectLichessOAuth2();
  });

});
