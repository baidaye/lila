$(function() {

  let $page = $('.capsule-role');

  registerMemberRoles();

  function registerMemberRoles() {

    $page.find('#form3-visibility').change(function () {
      let $this = $(this);
      let url = $this.parents('.form3').data('href');
      let visibility = $this.val();
      $.ajax({
        method: 'post',
        url: url,
        data: {visibility: visibility},
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          handleError(res);
        }
      });
    });

    $page.find('.addMember').click(function () {
      let url = $(this).data('href');
      let username = $page.find('#username').val();
      if(!username.trim()) {
        return;
      }

      $.ajax({
        method: 'post',
        url: url,
        data: {username: username},
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          handleError(res);
        }
      });
    });

    $page.find('.changeOwner').click(function () {
      let $this = $(this);
      let url = $this.data('href');
      let username = $page.find('#username').val();
      if(!username.trim()) {
        return;
      }

      let cfm = $this.attr('title').replace('xxxx', username);
      if(!confirm(cfm)) {
        return;
      }

      $.ajax({
        method: 'post',
        url: url,
        data: {username: username},
        success: function () {
          $.modal.close();
          location.href = '/resource/capsule/mineList';
        },
        error: function (res) {
          handleError(res);
        }
      });
    });

  }

});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
