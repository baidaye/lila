$(function() {

  $('#form3-tags').tagsInput({
    "height": "40px",
    "width": "100%",
    "interactive": true,
    "defaultText": "添加",
    "removeWithBackspace": true,
    "minChars": 0,
    "maxChars": 10,
    "placeholderColor": "#666666"
  });

  $('.dragsort').dragsort({
    dragSelector: '.dragable',
    placeHolderTemplate: '<a class="placeHolder"></a>',
    dragEnd: function() {
      let ids = [];
      $('.dragsort .dragable').each(function () {
        ids.push($(this).data('id'));
      });

      let capsuleId = $('main').data('id');
      let $waiting = $('.capsule-puzzles .waiting');
      $waiting.removeClass('none');
      $.ajax({
        method: 'post',
        url: `/resource/capsule/${capsuleId}/puzzle/reorder?puzzleIds=` + ids.join(','),
        success: function(response) {
          $('.dragsort .dragable').each(function (i) {
            $(this).find('.no').text(i + 1);
          });
          setTimeout(function () {
            $waiting.addClass('none');
          }, 200);
        },
        error: function(res) {
          handleError(res);
        }
      });
    }
  });

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if(typeof json.error === 'string') {
          alert(json.error);
        } else if(res.status === 400) {
          for(let key in json.error) {
            $(`.${key}Error`).text(json.error[key][0]);
          }
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }

});
