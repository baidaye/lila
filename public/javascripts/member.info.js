$(function() {

    let $component = $('.component');
    $component.find('.tabs-horiz span').click(function () {
        let $this = $(this);
        $component.find('.tabs-horiz span').removeClass("active");
        $component.find('.tabs-content div').removeClass("active");

        let cls = $this.data('tab');
        $this.addClass('active');
        $component.find('.tabs-content div.' + cls).addClass('active');
        location.hash = cls;
    });
    setTabActive();

    function setTabActive() {
        let hash = location.hash;
        if(hash) {
            hash = hash.replace('#', '');
            $component.find('.tabs-horiz > span').removeClass('active');
            $component.find('.tabs-horiz > span[data-tab="' + hash + '"]').addClass('active');
            $component.find('.tabs-content > div').removeClass('active');
            $component.find('.tabs-content > div.' + hash).addClass('active');
        }
    }

    $('a.member-intro').click(function(e) {
        e.preventDefault();
        lichess.memberIntro();
        return false;
    });

    $('a.modal-exchange-open').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            success: function (html) {
                $.modal($(html));
                $('.cancel').click(function () {
                    $.modal.close();
                });

                let $input = $('.modal-exchange form .code input');
                $input.on('change keyup paste', (e) => {
                    let el = e.target;
                    el.value = el.value.toUpperCase();
                });

                exchangeSubmit();
            },
            error: function (res) {
                alert(res.responseText);
            }
        });
        return false;
    });

    function exchangeSubmit() {
        let $form = $('.modal-exchange form');
        $form.submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (res) {
                    $.modal.close();
                    location.reload();
                },
                error: function (res) {
                    handleError(res);
                }
            });
            return false;
        });
    }

    function handleError(res) {
        let json = res.responseJSON;
        if (json) {
            if (json.error) {
                if (typeof json.error === 'string') {
                    alert(json.error);
                } else alert(JSON.stringify(json.error));
            } else alert(res.responseText);
        } else alert('发生错误');
    }

});
