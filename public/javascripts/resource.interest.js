$(function () {

  if($('main').data('notaccept') == true) {
    window.lichess.memberIntro();
  }

  $('.interest_form').submit(function (e) {
    if($('main').data('notaccept') == true) {
      e.preventDefault();
      window.lichess.memberIntro();
      return false;
    }
  });
  parseAllFen();
  window.lichess.pubsub.on('content_loaded', parseAllFen);
});

function parseAllFen() {
  $('.resource .infinitescroll .mini-board').each(function () {
    parseFen($(this));
  });
}


function parseFen($el) {
  let $this = $el.removeClass('parse-fen-manual');
  let color = $this.data('color');
  let ground = $this.data('chessground');
  let greenSquare = $this.data('green-square');
  let redSquare = $this.data('red-square');
  let yellowSquare = $this.data('yellow-square');

  let autoShapes = [];
  if(greenSquare) {
    autoShapes.push({
      brush: 'green',
      orig: greenSquare
    })
  }
  if(redSquare) {
    autoShapes.push({
      brush: 'red',
      orig: redSquare
    })
  }
  if(yellowSquare) {
    autoShapes.push({
      brush: 'yellow',
      orig:yellowSquare
    })
  }

  let config = {
    viewOnly: true,
    coordinates: false,
    fen: $this.data('fen'),
    orientation: color,
    drawable: {
      enabled: false,
      autoShapes: autoShapes
    }
  };
  if (ground) ground.set(config);
  else $this.data('chessground', Chessground($this[0], config));
}
