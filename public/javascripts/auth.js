$(function() {

  $('#signup_form input[name=agree]').change(function () {
    if($(this).is(':checked')){
      $('#signup_form button[type=submit]').prop('disabled', false).removeClass('disabled');
    } else {
      $('#signup_form button[type=submit]').prop('disabled', true).addClass('disabled');
    }
  });

  let $auth = $('.auth');
  $auth.find('.tabs > div').not('.disabled').click(function () {
    let active = $(this).data('tab');
    $auth.find('.tabs > div').removeClass('active');
    $auth.find('.panels > .panel').removeClass('active');
    $auth.find('.panels > .panel.' + active).addClass('active');
    $(this).addClass('active');

    location.hash = active;
  });

  setTabActive();
  function setTabActive() {
    let hash = location.hash;
    if(!$auth.find('.tabs > div[data-tab="' + hash + '"]').hasClass('disabled')) {
      if(hash) {
        hash = hash.replace('#', '');
        $auth.find('.tabs > div').removeClass('active');
        $auth.find('.tabs > div[data-tab="' + hash + '"]').addClass('active');
        $auth.find('.panels > .panel').removeClass('active');
        $auth.find('.panels > .panel.' + hash).addClass('active');
      }
    }
  }

});

