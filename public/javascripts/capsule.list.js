$(function() {

  let $page = $('.capsule-list');

  registerForm();

  function registerForm() {
    let $form = $page.find('.search_form');
    let $emptyTag = $form.find(".tag-group.emptyTag input[type='checkbox']");
    let $tags = $form.find(".tag-group input[type='checkbox']");

    $tags.not($emptyTag).click(function () {
      $emptyTag.prop('checked', false);
      $form.submit();
    });

    $emptyTag.click(function () {
      $tags.not($emptyTag).prop('checked', false);
      $form.submit();
    });

    $form.find("#form3-enabled").change(function () {
      $form.submit();
    });
  }

});
