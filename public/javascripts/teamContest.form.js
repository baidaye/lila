$(function () {

  dateFormat();
  let $form = $('.contest__form');

  $form.find('.single-uploader').find('input[type=\'file\']').ajaxSingleUpload({
    'name': 'logo',
    'action': '/contest/team/imageUpload'
  });

  $form.find('#form3-basics_typ').change(function () {
    let o = $form.find('#form3-basics_organizer');
    let v = $(this).val();
    let d = $form.data(v);
    let teamId = getQueryVariable("team");
    let selectedId = $('input[name=organizerSelected]').val();
    selectedId = selectedId ? selectedId : teamId;

    o.empty();
    for (let i = 0; i < d.length; i++) {
      let selected = selectedId && selectedId == d[i].id;
      o.append("<option value = '" + d[i].id + "' " + (selected ? 'selected' : '') + " >" + d[i].name + "</option>");
    }
    o.trigger('change');
    $form.find('#form3-conditions_maxTeamers').trigger('change');
  });

  $form.find('#form3-basics_organizer').change(function () {
    let typ = $form.find('#form3-basics_typ').val();
    let val = $(this).val();
    let dataArr = $form.data(typ);
    let teamRatedArr = dataArr.filter(n => n.id == val && n.teamRated);
    if (teamRatedArr.length > 0) {
      $('#form3-basics_teamRated').parents('.form-check').removeClass('none');
    } else {
      $('#form3-basics_teamRated').parents('.form-check').addClass('none');
    }
  });
  $form.find('#form3-basics_typ').trigger('change');

  $form.find('#form3-basics_rule').change(function () {
    let v = $(this).val();
    $.get(`/contest/team/rule/info?id=${v}`).then(function (data) {
      $form.find('.button-generate').data('maxRound', data.setup.maxRound);
      $form.find('#form3-rounds_rounds').attr('max', data.setup.maxRound).val(data.setup.defaultRound);
      $form.find('#form3-conditions_maxTeamers').attr('max', data.setup.maxTeamers).val(data.setup.defaultTeamers);
      $form.find('#form3-conditions_maxPerTeam').attr('max', data.setup.maxPerTeam).val(data.setup.defaultTeamers);
      $form.find('#form3-conditions_formalPlayers').attr('max', data.setup.maxPlayers);
      $form.find('#form3-conditions_substitutePlayers').attr('max', data.setup.maxPlayers);

      if(v === 'dual') {
        $form.find('div[data-tab="rounds"]').addClass('disabled');
        $form.find('.button-generate').trigger('click');
      } else {
        $form.find('div[data-tab="rounds"]').removeClass('disabled');
      }
    });
  });

  $form.find('#form3-basics_startsAt').change(function () {
    if($form.find('#form3-basics_rule').val() == 'dual') {
      $form.find('.button-generate').trigger('click');
    }
  });

  $form.find('#form3-conditions_maxTeamers').change(function () {
    if($form.find('#form3-basics_typ').val() === 'team-inner') {
      let v = $(this).val();
      $form.find('#form3-conditions_maxPerTeam').val(v).attr('readonly', true);
    } else $form.find('#form3-conditions_maxPerTeam').attr('readonly', false);
  });
  $form.find('#form3-conditions_maxTeamers').trigger('change');

  $form.find('.tabs > div').click(function () {
    let $this = $(this);
    if($this.hasClass('disabled')) return;

    $form.find('.tabs > div').removeClass('active');
    $form.find('.panel').removeClass('active');
    $form.find('.panel.' + $this.data('tab')).addClass('active');
    $this.addClass('active');
  });
  $form.find('.flatpickr').flatpickr(datePickOption());

  // 根据 比赛开始时间、比赛轮次、间隔时间 生成轮次
  $form.find('.button-generate').on('click', function () {
    let $this = $(this);
    let $rounds = $form.find('.round-generate');
    let number = $form.find('#form3-rounds_rounds').val();
    let maxRound = $this.data('maxRound');

    if (number > maxRound) {
      alert(`轮次数量不大于${maxRound}轮`);
      return;
    }
    if ($rounds.find('div').length > 0 && $form.find('#form3-basics_rule').val() !== 'dual') {
      if (!window.confirm('该操作将重置现有轮次设置，是否继续？')) return;
    }

    let startsAt = $form.find('#form3-basics_startsAt').val();
    let spaceDay = $form.find('#form3-rounds_spaceDay').val();
    let spaceHour = $form.find('#form3-rounds_spaceHour').val();
    let spaceMinute = $form.find('#form3-rounds_spaceMinute').val();

    let spaceMi = spaceDay * 24 * 60 * 60 * 1000 + spaceHour * 60 * 60 * 1000 + spaceMinute * 60 * 1000;
    let startsAtMi = new Date(startsAt).getTime();

    $rounds.empty();
    for (let i = 0; i < number; i++) {
      let sd = startsAtMi + (spaceMi * i);
      let el = buildElement(i, new Date(sd).format("yyyy-MM-dd hh:mm"));
      $rounds.append(el);
    }
    $rounds.find('.flatpickr').flatpickr(datePickOption());
  });

  $form.find('#form3-basics_position').change(function () {
    let text = $(this).find("option:selected").text();
    if (text === '初始局面') {
      $('.board-link').addClass('none');
      $('.position-paste').addClass('none');
    } else if (text === '输入FEN') {
      $('.board-link').removeClass('none');
      $('.position-paste').removeClass('none');
      $('.position-paste').val('');
      $('.starts-position').find('.preview').empty();
    } else if (text === '载入局面') {
      if ($('main').data('notaccept') == true) {
        window.lichess.memberIntro();
        return false;
      }
      $('.board-link').removeClass('none');
      $('.position-paste').removeClass('none');
      $('.position-paste').val('');
      $('.starts-position').find('.preview').empty();
      loadSituation();
    } else {
      $('.board-link').removeClass('none');
      $('.position-paste').addClass('none');
      validateFen($(this).val());
    }
  });

  $form.find('.position-paste').on('input propertychange', function () {
    validateFen($(this).val())
  });

  function loadSituation() {
    let ts = this;
    let fen = '';
    $.ajax({
      url: `/resource/situationdb/select?mustStandard=true`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });
      window.lichess.pubsub.emit('content_loaded');

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-selector').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            },
            'plugins': ['search']
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                search();
              }
            });

          let to = null;
          $form.find('.search input[name="q"]').keyup(function () {
            let q = $(this).val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });

          $form.find('#btn-search').click(function () {
            search();
          });

          let $emptyTag = $form.find('.search_form')
            .find('.tag-group.emptyTag')
            .find('input[type="checkbox"]');

          let $tags = $form.find('.search_form')
            .find('.tag-group')
            .find('input[type="checkbox"]');

          $tags.not($emptyTag).click(function () {
            $emptyTag.prop('checked', false);
            search();
          });

          $emptyTag.click(function () {
            $tags.not($emptyTag).prop('checked', false);
            search();
          });

          let $situations = $form.find('.situations');

          function search() {
            let situation = $tree.jstree(true).get_selected(null)[0];
            $.ajax({
              url: `/resource/situationdb/rel/list?selected=${situation}`,
              data: $form.serialize()
            }).then(function (rels) {
              $situations.empty();

              if (!rels || rels.length === 0) {
                let noMore =
                  `<div class="no-more">
                                    <i data-icon="4">
                                    <p>没有更多了</p>
                                  </div>`;
                $situations.html(noMore);
              } else {
                let situations =
                  rels.map(function (rel) {
                    let tags = '';
                    if (rel.tags && rel.tags.length > 0) {
                      rel.tags.map(function (tag) {
                        tags = `<span>${tag}</span>`;
                      });
                    }
                    return `<a class="paginated" target="_blank" data-id="${rel.id}">
                                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                                <cg-helper>
                                                  <cg-container>
                                                    <cg-board></cg-board>
                                                  </cg-container>
                                                </cg-helper>
                                              </div>
                                              <div class="btm">
                                                <label>${rel.name ? rel.name : ''}</label>
                                                <div class="tags">${tags}</div>
                                              </div>
                                            </a>`;
                  });

                $situations.html(situations);
                registerEvent();
                window.lichess.pubsub.emit('content_loaded')
              }
            })
          }

          function registerEvent() {
            let $boards = $situations.find('.paginated');
            $boards.off('click');
            $boards.click(function (e) {
              e.preventDefault();
              let $this = $(this);
              $boards.removeClass('selected');
              $this.addClass('selected');
              fen = $this.find('.mini-board').data('fen');
              return false;
            });
          }

          registerEvent();

          $form.submit(function (e) {
            e.preventDefault();
            $('.position-paste').val(fen);
            validateFen(fen);
            $.modal.close();
            return false;
          });
        })
      });
      return false
    });
  };

  function validateFen(fen) {
    let $position = $form.find('.starts-position');
    let $board = $position.find('.preview');
    if (fen) {
      $.ajax({
        url: '/setup/validate-fen?strict=0',
        data: {
          fen: fen
        },
        success: function (data) {
          $board.html(data);
          $position.removeClass('is-invalid');
          $position.find('a.board-link').attr('href', '/editor/' + fen);
          let $option = $('#form3-basics_position').find('option:selected');
          let dataId = $option.data('id');
          if (dataId === 'option-load-fen' || dataId === 'option-load-situation') {
            $option.val(fen);
          }
          lichess.pubsub.emit('content_loaded');
        },
        error: function () {
          $board.empty();
          $position.addClass('is-invalid');
        }
      });
    }
  }

  function buildElement(index, sd) {
    return `<div class="form-split">
                    <div class="form-group form-half">
                        <label class="form-label" for="form3-rounds_list_${index}_startsAt">第 ${index + 1} 轮</label>
                        <input id="form3-rounds_list_${index}_startsAt" 
                               name="rounds.list[${index}].startsAt"
                               class="form-control flatpickr flatpickr-input" 
                               value="${sd}"
                               data-enable-time="true" 
                               data-time_24h="true"
                               type="hidden">
                    </div>
                </div>`
  }

});

function getQueryVariable(variable) {
  let query = window.location.search.substring(1);
  let vars = query.split("&");
  for (let i = 0; i < vars.length; i++) {
    let pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return false;
}

function datePickOption() {
  return {
    time_24hr: true,
    altFormat: 'Y-m-d H:i'
  }
}

function dateFormat() {
  Date.prototype.format = function (fmt) {
    let o = {
      "M+": this.getMonth() + 1,               //月份
      "d+": this.getDate(),                    //日
      "h+": this.getHours(),                   //小时
      "m+": this.getMinutes(),                 //分
      "s+": this.getSeconds(),                 //秒
      "q+": Math.floor((this.getMonth() + 3) / 3), //季度
      "S": this.getMilliseconds()              //毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  };
}
