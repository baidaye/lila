$(function() {

  let $root = $(".infinitescroll");
  let $form = $(".error_search_form");

  registerEvent();
  lichess.pubsub.on('content_loaded', registerEvent);

  $form.find('input[name="time"]').change(function () {
    let time = $form.find('input[name="time"]:checked').val();
    let sTime = getDay(parseInt(time));
    let eTime = getDay(0);

    $form.find("#form3-sTime").val(sTime);
    $form.find("#form3-eTime").val(eTime);
  });
  $form.find('input[name="time"]').trigger('change');

  $('select.select').change(function() {
    $root.find('.paginated').removeClass('selected');
    switch ($(this).val()) {
      case 'all':
        $root.find('.paginated').addClass('selected');
        break;
    }

  });

  $('select.action').change(function() {
    let $this = $(this);
    let action = $this.val();
    if (!action) return;

    let ids = [];
    $root.find('.selected').each(function() {
      return ids.push($(this).attr('data-id'));
    });

    if (ids.length === 0) return;
    if (action === 'delete') {
      if (confirm('删除 ' + ids.length + ' 个错题？')) {
        let url = $form.attr('action');
        url = url.substring(0, url.lastIndexOf("/")) + '/delete?ids=' + ids.join(',');
        $form.attr('action', url).attr('method', 'post');
        $form.submit();
      } else {
        $this.val('');
      }
    } else if (action === 'toCapsule') {
      showCapsule();
    }

  });

  function showCapsule() {
    let ids = [];
    $root.find('.selected').each(function() {
      return ids.push($(this).attr('data-puzzleId'));
    });
    if(ids.length === 0) {
      alert('至少选择1项');
      return;
    }

    $.ajax({
      url: '/resource/capsule/mine',
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $('select.action').val('');
          $.modal.close();
        });

        let $md = $('.modal-capsule');
        $md.find('.tabs-horiz span').click(function () {
          let $this = $(this);
          $md.find('.tabs-horiz span').removeClass("active");
          $md.find('.tabs-content div').removeClass("active");

          let cls = $this.attr('class');
          $this.addClass('active');
          $md.find('.tabs-content div.' + cls).addClass('active');
        });

        $('.modal-capsule form').submit(function(e) {
          e.preventDefault();
          let $form = $(this);
          let $tabs = $form.find('.tabs-horiz');
          let active = $tabs.find('.active').data('id');

          let url = `/resource/capsule/puzzle/createAndAdd?puzzleIds=` + ids.join(',');
          if(active === 'old') {
            let capsuleId = $form.find('input[name="capsule"]:checked').val();
            if(!capsuleId) return false;
            url = `/resource/capsule/${capsuleId}/puzzle/add?puzzleIds=` + ids.join(',');
          }

          $.ajax({
            method: 'POST',
            url: url,
            data: {
              name: $form.find('input[name="capsuleName"]').val()
            },
            success: function(response) {
              $.modal.close();
              location.reload();
            },
            error: function(res) {
              alert(res.responseText);
            }
          });
          return false;
        });
      },
      error: function(res) {
        if (res.responseText.indexOf('名称已经存在') > 0) {
          $form.find('input[name="capsuleName"]').parent().append('<p class="error">名称已经存在</p>');
        } else {
          alert(res.responseText);
        }
      }
    });
  }

  $form.find(".tag-group input[type='checkbox']").click(function () {
    $form.submit()
  });

  $form.find("#form3-order").change(function () {
    $form.submit()
  });

});

function registerEvent() {
  let $board = $(".infinitescroll").find('.paginated');
  $board.off('click');
  $board.off('dblclick');
  $board.click(function(e) {
    e.preventDefault();
    $(this).toggleClass('selected');
    return false;
  });
  $board.dblclick(function(e) {
    e.preventDefault();
    window.open($(this).data('href'));
    return false;
  });

}

function getDay(day) {
  let today = new Date();
  let mill = today.getTime() + 1000 * 60 * 60 * 24 * day;
  today.setTime(mill);

  let tYear = today.getFullYear();
  let tMonth = today.getMonth();
  let tDate = today.getDate();
  tMonth = doHandleMonth(tMonth + 1);
  tDate = doHandleMonth(tDate);
  return tYear + "-" + tMonth + "-" + tDate;
}

function doHandleMonth(month){
  let m = month;
  if(month.toString().length == 1){
    m = '0' + month;
  }
  return m;
}

