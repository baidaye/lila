$(function () {
  applyShowPage();
  applyJoinPage();
  applyCalendarPage();
});

function applyShowPage() {
  let $show = $('.clockIn-show');
  if($show.length > 0) {
    applyClazzChange();
    applyExport();
    applySorter();
    applyCalendar();
  }

  function applyClazzChange() {
    $show.find('#clazzId').change(function () {
      let $this = $(this);
      let clazzId = $this.val();
      let clockInTaskId = $show.find('.clockIn-show__members').data('id');
      if(clockInTaskId) {
        location.href = `${location.pathname}?clockInTaskId=${clockInTaskId}${clazzId ? '&clazzId=' + clazzId : ''}`
      } else location.href = `${location.pathname}${clazzId ? '?clazzId=' + clazzId : ''}`
    });
  }

  function applyExport() {
    let $members = $show.find('.clockIn-show__members');
    $members.find('.export').click(function () {
      let $table = $members.find('table');
      let $thead = $table.find('thead');
      let $tbody = $table.find('tbody');

      let data = [];

      // 标题
      let title = $show.find('.clockIn-show__top').text() + $show.find('.clockIn-show__members .members-top .clockInDate').text();

      // 表头
      let head = $thead.find('tr th').map(function (_, th) {
        return th.innerText;
      }).toArray();
      data.push(head.slice(0, head.length - 1));

      // 内容
      let body = $tbody.find('tr').map(function (_, tr) {
        let arr = $(tr).find('td').map(function (_, td) {
          return td.innerText.replace('\n', ' ').replace('\n', ' ');
        }).toArray();
        return [arr];
      }).toArray();

      body = body.map(function (arr) {
        return arr.slice(0, arr.length - 1);
      });
      data = data.concat(body);

      let csvContent = 'data:text/csv;charset=utf-8,\uFEFF' + data.map(e => e.join(',')).join('\n');
      let encodedUri = encodeURI(csvContent);
      let link = document.createElement('a');
      link.setAttribute('href', encodedUri);
      link.setAttribute('download', `${title}.csv`);
      document.body.appendChild(link);
      link.click();
    });
  }

  function applySorter() {
    let $members = $show.find('.clockIn-show__members');
    let $list = $members.find('.slist');
    $list.find('.withSorter .up').click(() => {
      $list.find('.withSorter .up').addClass('active');
      $list.find('.withSorter .down').removeClass('active');
      sort();
    });

    $list.find('.withSorter .down').click(() => {
      $list.find('.withSorter .down').addClass('active');
      $list.find('.withSorter .up').removeClass('active');
      sort();
    });

    let sort = () => {
      let arr = [];
      $list.find('tbody tr').each((_, el) => {
        arr.push(el);
      });
      $list.find('tbody').empty();

      for (let i = arr.length - 1; i >= 0; i--) {
        $list.find('tbody').append(arr[i]);
      }
    }
  }

  function applyCalendar() {
    let $zabutoCalendar = $show.find('.zabutoCalendar');

    let attr = $zabutoCalendar.data('attr');
    let events = attr.map(item => {
      let html = item.canceled ? `<div class="dot"></div>` : `<div class="badge">${item.num}</div>`;
      return {
        date: item.date,
        classname: `task ${item.completed ? 'completed' : ''} ${item.canceled ? 'canceled' : ''} ${item.selected ? 'selected' : ''}`,
        markup: `
          <div class="day-wrap" title="${item.taskName}">
            <div>[day]</div>
            ${html}
          </div>`
      }
    });

    $zabutoCalendar.zabuto_calendar({
      language: 'cn',
      classname: 'slist',
      navigation_markup: {
        prev: '<i data-icon="左"></i>',
        next: '<i data-icon="右"></i>'
      },
      events: events
    });

    $zabutoCalendar.on('zabuto:calendar:day', function (e) {
      let date = e.value;
      let data = attr.find(d => d.date === date);
      if(data && !data.canceled) {
        location.href = `/team/clockIn/showOfDate?clockInTaskId=${data.clockInTaskId}`;
      }
    });
  }
}

function applyJoinPage() {
  let $join = $('.clockIn-join');
  if($join.length > 0) {
    applyCalendar($join);
  }
}

function applyCalendarPage() {
  let $calendar = $('.clockIn-calendar');
  if($calendar.length > 0) {
    applyCalendar($calendar);
  }
}

function applyCalendar($page) {
  let $zabutoCalendar = $page.find('.zabutoCalendar');

  let attr = $zabutoCalendar.data('attr');
  let events = attr.map(item => {
    return {
      date: item.date,
      classname: `task ${item.completed ? 'completed' : ''} ${item.canceled ? 'canceled' : ''} `,
      markup: `
        <div class="day-wrap" title="${item.taskName} ${!item.canceled ?  '[' + item.nb + '/' + item.total + ']': ''}">
          <div>[day]</div>
          <div class="dot"></div>
        </div>`
    }
  });

  $zabutoCalendar.zabuto_calendar({
    language: 'cn',
    classname: 'slist',
    navigation_markup: {
      prev: '<i data-icon="左"></i>',
      next: '<i data-icon="右"></i>'
    },
    events: events
  });

  $zabutoCalendar.on('zabuto:calendar:day', function (e) {
    let date = e.value;
    let data = attr.find(d => d.date === date);
    if(data && !data.canceled && data.joined) {
      window.open(`/ttask/${data.taskId}`);
    }
  });
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
