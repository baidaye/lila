$(function () {

  let $coach = $('.coach');

  let $relations = $('.relations');
  $relations.find('.tabs > div').not('.disabled').click(function () {
    let active = $(this).data('tab');
    $relations.find('.tabs > div').removeClass('active');
    $relations.find('.panels > .panel').removeClass('active');
    $relations.find('.panels > .panel.' + active).addClass('active');
    $(this).addClass('active');
    location.hash = active;
  });

  setTabActive();

  function setTabActive() {
    let hash = location.hash;
    if (!$relations.find('.tabs > div[data-tab="' + hash + '"]').hasClass('disabled')) {
      if (hash) {
        hash = hash.replace('#', '');
        $relations.find('.tabs > div').removeClass('active');
        $relations.find('.tabs > div[data-tab="' + hash + '"]').addClass('active');
        $relations.find('.panels > .panel').removeClass('active');
        $relations.find('.panels > .panel.' + hash).addClass('active');
      }
    }
  }

  $coach.find('a.modal-alert').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        registerChangeCoach();
        registerEditCoach();
        onMemberOpen();
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  function registerChangeCoach() {
    let $form = $('.modal-content.changeCoachModal form');
    search($form);
    $form.submit(function (e) {
      e.preventDefault();
      let team = $('main').data('id');
      let coach = $form.find('input[name="coach"]:checked').val();
      location.href = `/team/${team}/coach?coach=${coach}`;
    });
  }

  function registerEditCoach() {
    let $modal = $('.editCoachModal');
    $modal.find('.editCoachSave').click(function (e) {
      e.preventDefault();
      let data = {'newCoach': $('#form3-newCoach').val()};
      $.ajax({
        method: 'post',
        url: $modal.find('form').attr('action'),
        data: data,
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          handleError(res);
        }
      });
      return false;
    });
  }

  $coach.find(".form-check-input input[type='checkbox']").click(function () {
    let $this = $(this);
    let v = $this.is(':checked');
    let id = $this.data('id');
    let key = id.key;

    let data = {
      role: (v === true) ? 'w' : 'r'
    };
    data[`${key}Id`] = id.resourceId;

    let upKey = key[0].toUpperCase() + key.substr(1);
    $.ajax({
      method: 'post',
      url: `/team/${id.teamId}/coach/set${upKey}Role?coach=${id.coachId}`,
      data: data,
      success: function () {
        location.reload();
      },
      error: function (res) {
        $this.prop('checked', !v);
        handleError(res);
      }
    });

  });

  function search($root) {
    let $lst = $root.find('.slist');
    $root.find('.search').on('input propertychange', function () {
      let txt = $(this).val();
      if ($.trim(txt) !== '') {
        $lst.find('tbody tr').not('tr:contains("' + txt + '")').css('display', 'none');
        $lst.find('tbody tr').filter('tr:contains("' + txt + '")').css('display', 'table-row');
      } else {
        $lst.find('tbody tr').css('display', 'table-row');
      }
    });
  }

  search($relations.find('.clazzs'));
  search($relations.find('.students'));
  search($relations.find('.studies'));
  search($relations.find('.capsules'));
  search($relations.find('.openingdbs'));
});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
