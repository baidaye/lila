$(function() {

    const A4height = 297;   // A4高度297mm
    const A4Width = 210;    // A4宽带210mm
    const A4Margin = 10;    // 默认边距10mm
    const a4PageHeight = Math.floor(mm2px(A4height));
    const a4PageWidth = Math.floor(mm2px(A4Width));
    const chromeMargin = Math.floor(mm2px(A4Margin));
    const marginPageHeight = a4PageHeight - chromeMargin * 2; // 去除边距后A4纸的内容高度
    const marginPageWidth = a4PageWidth - chromeMargin * 2;   // 去除边距后A4纸的内容高度

    $('head').append(`<style media="print">@page{size:21cm 29.7cm;background-color:#fff;margin:0;}</style>`);
    let $page = $('.capsule-print');
    let $setting = $page.find('.setting');
    let $list = $page.find('.capsule-list');

    $setting.find('input[name="no"]').click(function(){
        let v = $setting.find('input[name="no"]:checked').val();
        if(v == 1) {
            $list.find('.no').removeClass('none');
        } else {
            $list.find('.no').addClass('none');
        }
        setLayout();
    });

    $setting.find('input[name="color"]').click(function(){
        let v = $setting.find('input[name="color"]:checked').val();
        if(v == 1) {
            $list.find('.color').removeClass('none');
        } else {
            $list.find('.color').addClass('none');
        }
        setLayout();
    });

    $setting.find('input[name="orientation"]').click(function(){
        setLayout();
    });

    $setting.find('input[name="rating"]').click(function(){
        let v = $setting.find('input[name="rating"]:checked').val();
        if(v == 1) {
            $list.find('.rating').removeClass('none');
        } else {
            $list.find('.rating').addClass('none');
        }
        setLayout();
    });

    $setting.find('input[name="line"]').click(function(){
        let v = $setting.find('input[name="line"]:checked').val();
        if(v == 1) {
            $list.find('.lines').removeClass('none');
        } else {
            $list.find('.lines').addClass('none');
        }
        setLayout();
    });

    $setting.find('input[name="nums"]').click(function(){
        setLayout();
    });

    $setting.find('input[name="coord"]').click(function(){
        setLayout();
    });

    $setting.find('input[name="title"]').on('input propertychange', function () {
        let title = $(this).val();
        $list.find('.title').text(title);
    });

    $('.do-print').click(function () {
        $('.print-area').print({
            copyBodyClass: true
        });
    });

    function uciToLastMove(uci) {
        return uci && [uci.substr(0, 2), uci.substr(2, 2)];
    }

    function setLayout() {
        let showNo = $setting.find('input[name="no"]:checked').val() == 1;
        let showColor = $setting.find('input[name="color"]:checked').val() == 1;
        let showRating = $setting.find('input[name="rating"]:checked').val() == 1;
        let showLines = $setting.find('input[name="line"]:checked').val() == 1;

        let v = $setting.find('input[name="nums"]:checked').val();
        let rows = Number.parseInt(v.split('*')[0]);
        let cols = Number.parseInt(v.split('*')[1]);

        let puzzles = $('main').data('puzzles');
        if(puzzles.length > 0) {
            let arrs = grouped(puzzles, rows * cols);
            let pages = pagesHtml(arrs, rows * cols, rows, cols);
            $list.empty();
            $list.html(pages);
        }

        $list.find('.page').innerWidth(marginPageWidth);
        $list.find('.page').innerHeight(a4PageHeight);

        const defaultGap = 35;
        const defaultPaddingRight = 17;

        const titleHeight = 28;
        const blockTopHeight = (showNo || showColor || showRating) ? 19 : 5;
        const blockLinesHeight = showLines ? 80 : defaultGap;

        const boardSizeOfPageHeight = (marginPageHeight - titleHeight - (blockTopHeight + blockLinesHeight) * rows) / rows;
        const boardSizeOfPageWidth = (marginPageWidth - defaultGap * (cols - 1) - defaultPaddingRight) / cols;

        const boardSize = Math.floor(Math.min(boardSizeOfPageHeight, boardSizeOfPageWidth));
        const fixSize = (rows === 3 && cols === 3) ? 4 : 5; // 3x3 -> fix=4 页面正常，其它fix=5页面正常
        $list.find('.mini-board').width(boardSize - fixSize);

        $list.find('.cg-wrap').each(function() {
            let $board = $(this).removeClass('parse-fen-manual');
            let mainGround = $board.data('chessground');
            let fen = $board.data('fen');
            let color = $board.data('color');
            let coord = !!$board.data('coordinates');
            let lastMove = $board.data('lastmove');
            let config = {
                fen: fen,
                orientation: color,
                lastMove: uciToLastMove(lastMove),
                movable: {
                    free: false,
                    color: null,
                    dests: {}
                },
                highlight: {
                    check: true,
                    lastMove: true
                },
                drawable: { enabled: false, visible: false },
                resizable: false,
                coordinates: coord
            };

            if (mainGround) mainGround.set(config);
            else $board.data('chessground', Chessground($board[0], config));
        });

        //lichess.pubsub.emit('content_loaded');
    }

    function pagesHtml(arrs, size, rows, cols) {
        let title = $setting.find('input[name="title"]').val();
        let pagesHtml = '';
        if(arrs && arrs.length > 0) {
            arrs.forEach(function(puzzles, i) {
                pagesHtml += pageHtml(puzzles, size, rows, cols, title, i + 1, arrs.length);
            });
        }
        return pagesHtml;
    }

    function pageHtml(puzzles, size, rows, cols, title, page, total) {
        let listHtml = '';

        if(puzzles.length < size) {
            let fills = size - puzzles.length;
            for (let i = 0; i < fills; i++) {
                puzzles.push({});
            }
        }

        let arrs = grouped(puzzles, cols);
        if(arrs && arrs.length > 0) {
            let showLines = $setting.find('input[name="line"]:checked').val() == 1;
            arrs.forEach(function(puzzles, i) {
                let puzzlesHtmlx = puzzlesHtml(puzzles, rows, cols);
                listHtml += `<div class="list ${(!showLines && i < arrs.length - 1) ? 'mgbtm' : ''}">${puzzlesHtmlx}</div>`;
            });
        }
        return `<div class="page">
                    <div class="page-header" style="padding-top: ${chromeMargin}px">
                        <div class="title">${title}</div>
                        <div class="link">haichess.com</div>
                    </div>
                    ${listHtml}
                    <div class="page-footer" style="height: ${chromeMargin}px;line-height: ${chromeMargin}px;">- 第${page}/${total}页 - </div>
                </div>`;
    }

    function puzzlesHtml(puzzles, rows, cols) {
        let noNoneClass = $setting.find('input[name="no"]:checked').val() == 1 ? '' : 'none';
        let colorNoneClass = $setting.find('input[name="color"]:checked').val() == 1 ? '' : 'none';
        let ratingNoneClass = $setting.find('input[name="rating"]:checked').val() == 1 ? '' : 'none';
        let lineNoneClass = $setting.find('input[name="line"]:checked').val() == 1 ? '' : 'none';
        let orientation = $setting.find('input[name="orientation"]:checked').val();
        let coord = $setting.find('input[name="coord"]:checked').val() == 1;

        let puzzlesHtml = '';
        if(puzzles && puzzles.length > 0) {
            puzzles.forEach(function(puzzle) {
                let html = '';
                if(Object.keys(puzzle).length !== 0) {
                    html = puzzleHtml(puzzle, noNoneClass, colorNoneClass, ratingNoneClass, lineNoneClass, orientation, coord, rows, cols)
                } else {
                    html =
                        `<div class="block block-empty">
                            <div class="top ${(rows>= 4 || cols >= 4) ? 'small' : ''}">
                                <span class="item no">&nbsp;&nbsp;</span>
                                <span class="item color">&nbsp;&nbsp;</span>
                                <span class="item rating">&nbsp;&nbsp;</span>
                            </div>
                            <a class="puzzle">
                                <div class="mini-board cg-wrap parse-fen-manual is2d" data-color="white" data-fen="8/8/8/8/8/8/8/8 w - -"></div>
                            </a>
                            <div class="lines ${lineNoneClass}">
                                <div class="line"></div>
                                <div class="line"></div>
                            </div>
                        </div>`;
                }
                puzzlesHtml += html;
            });
        }
        return puzzlesHtml;
    }

    function puzzleHtml(puzzle, noNoneClass, colorNoneClass, ratingNoneClass, lineNoneClass, orientation, coord, rows, cols) {

        return `<div class="block">
                    <div class="top ${(rows>= 4 || cols >= 4) ? 'small' : ''}">
                        <span class="item no ${noNoneClass}">No.${puzzle.no}</span>
                        <span class="item color ${colorNoneClass}">${puzzle.colorLabel}</span>
                        <span class="item rating  ${ratingNoneClass}">难度：${puzzle.rating}</span>
                    </div>
                    <a class="puzzle" target="_blank" data-id="${puzzle.id}" href="/training/${puzzle.id}?showNextPuzzle=false&amp;rated=false">
                        <div class="mini-board cg-wrap parse-fen-manual is2d" data-color="${orientation === 'auto' ? puzzle.color : orientation}" data-fen="${puzzle.fen}" data-lastmove="${puzzle.lastMove}" data-coordinates="${coord ? true : false}"></div>
                    </a>
                    <div class="lines ${lineNoneClass}">
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div>`;
    }

    function grouped(arr, size) {
        const arrNum = Math.ceil(arr.length / size, 10);
        let index = 0;
        let resIndex = 0;
        const result = [];
        while (index < arrNum) {
            result[index] = arr.slice(resIndex, size + resIndex);
            resIndex += size;
            index++;
        }
        return result;
    }

    setLayout();
});

function getDPI() {
    let dpi = 0;
    if (window.screen.deviceXDPI) {
        dpi = window.screen.deviceXDPI;
    } else {
        let tmpNode = document.createElement('div');
        tmpNode.style.cssText = 'width:1in;height:1in;position:absolute;left:0px;top:0px;z-index:99;visibility:hidden';
        document.body.appendChild(tmpNode);
        dpi = parseInt(tmpNode.offsetWidth);
        tmpNode.parentNode.removeChild(tmpNode);
    }
    return dpi;
}

// 1 英寸 = 25.4 毫米
function px2mm(value) {
    let inch = value / getDPI();
    return inch * 25.4;
}

function mm2px(value) {
    let inch = value / 25.4;
    return inch * getDPI();
}
