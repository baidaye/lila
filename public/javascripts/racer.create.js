$(function () {

    let $form = $('.racer__form');

    $form.find('#form3-typ').change(function () {
        let o = $form.find('#form3-organizer');
        let v = $(this).val();
        let d = $form.data(v);
        let teamId = getQueryVariable("team");
        let selectedId = $('input[name=organizerSelected]').val();
        selectedId = selectedId ? selectedId : teamId;

        o.empty();
        for (let i = 0; i < d.length; i++) {
            let selected = selectedId && selectedId == d[i].id;
            o.append("<option value = '" + d[i].id + "' " + (selected ? 'selected' : '') + " >" + d[i].name + "</option>");
        }
        o.trigger('change');
    });
    $form.find('#form3-typ').trigger('change');

    $form.find('.tabs-horiz span').click(function () {
        let $this = $(this);
        $form.find('.tabs-horiz span').removeClass("active");
        $form.find('.tabs-content div').removeClass("active");

        let active = $this.data('tab');
        $this.addClass('active');
        $form.find('.tabs-content div.' + active).addClass('active');
    });

    $form.find('#form3-duration, #form3-roundTime, #form3-restTime').change(function () {
        let counts = calcRoundCount();
        let roundTime = $form.find('#form3-roundTime').find('option:selected').text();
        let restTime = $form.find('#form3-restTime').find('option:selected').text();
        $('.roundTime').text(`共${counts}轮，每轮${roundTime}，休息${restTime}`);

        let rounds = [];
        for (let i = 1; i <= counts; i++) {
            let html = roundRangesHtml(i);
            rounds.push(html);
        }

        let $rounds = $form.find('.puzzle tbody');
        $rounds.empty();
        if(rounds.length > 0) {
            $rounds.append(rounds.join(''));
        }
        registerCapsuleClick();
        registerResetClick();
        validPuzzleInput();
    });

    function roundRangesHtml(no) {
        let html = `
            <tr data-id="${no}">
                <td>
                    ${no}
                    <input type="hidden" name="roundSetting[${no - 1}].no" value="${no}"/>
                </td>
                <td><input type="number" min="0" max="60" name="roundSetting[${no - 1}].ranges.num1" value="20" required="true"/></td>
                <td><input type="number" min="0" max="60" name="roundSetting[${no - 1}].ranges.num2" value="20" required="true"/></td>
                <td><input type="number" min="0" max="60" name="roundSetting[${no - 1}].ranges.num3" value="20" required="true"/></td>
                <td>
                    <a class="button small modal-alert" href="/resource/capsule/mineOfChoose">指定列表</a>
                    <a class="button button-empty small reset">恢复默认</a>
                </td>
            </tr>
        `;
        return html;
    }

    function roundCapsuleHtml(no, id, name, ids, len) {
        let html = `
            <tr data-id="${no}">
                <td>
                    ${no}
                    <input type="hidden" name="roundSetting[${no - 1}].no" value="${no}"/>
                </td>
                <td colspan="3">
                    <a target="_blank", class="capsuleName" href="/resource/capsule/${id}/puzzle/list">${name} ${len}题</a>
                    <input type="hidden" name="roundSetting[${no - 1}].capsule.id" value="${id}"/>
                    <input type="hidden" name="roundSetting[${no - 1}].capsule.name" value="${name}"/>
                    <input type="hidden" name="roundSetting[${no - 1}].capsule.puzzles" value="${ids}"/>
                </td>
                <td>
                    <a class="button small modal-alert" href="/resource/capsule/mineOfChoose">指定列表</a>
                    <a class="button button-empty small reset">恢复默认</a>
                </td>
            </tr>
        `;
        return html;
    }

    function calcRoundCount() {
        let duration = $form.find('#form3-duration').val();
        let roundTime = $form.find('#form3-roundTime').val();
        let restTime = $form.find('#form3-restTime').val();
        let roundCount = (parseInt(duration) + parseInt(restTime)) / (parseInt(roundTime) + parseInt(restTime));
        return Math.floor(roundCount);
    }

    $form.find('#form3-duration').trigger('change');

    function registerResetClick() {
        $form.find('a.reset').click(function(e) {
            e.preventDefault();
            let no = $(this).parents('tr').data('id');
            $(this).parents('tr').replaceWith(roundRangesHtml(no));

            registerCapsuleClick();
            registerResetClick();
            validPuzzleInput();
            return false;
        });
    }

    function registerCapsuleClick() {
        $form.find('a.modal-alert').click(function(e) {
            e.preventDefault();
            let $tr = $(this).parents('tr');
            $.ajax({
                url: $(this).attr('href'),
                success: function(html) {
                    $.modal($(html));
                    $('.cancel').click(function () {
                        $.modal.close();
                    });
                    registerCapsules($tr.data('id'));
                },
                error: function(res) {
                    alert(res.responseText);
                }
            });
            return false;
        });
    }


    function registerCapsules(no) {

        let $modal = $('.modal-capsule');

        $modal.find('.tabs-horiz span').click(function () {
            let $this = $(this);
            $modal.find('.tabs-horiz span').removeClass("active");
            $modal.find('.tabs-content div').removeClass("active");

            let cls = $this.data('tab');
            $this.addClass('active');
            $modal.find('.tabs-content div.' + cls).addClass('active');
        });

        $modal.find('.form3').submit(function (e) {
            e.preventDefault();
            let capsuleJson = $modal.find('input[name="capsuleRd"]:checked').parents('tr').data('attr');
            let trHtml = roundCapsuleHtml(no, capsuleJson.id, capsuleJson.name, capsuleJson.ids, capsuleJson.len);
            let $rounds = $form.find('.puzzle tbody');
            $rounds.find(`tr[data-id="${no}"]`).replaceWith(trHtml);
            registerCapsuleClick();
            registerResetClick();
            validPuzzleInput();
            $.modal.close();
            return false;
        });

        function filter($tab) {
            let $filter = $tab.find('.capsule-filter');
            let name = $filter.find('.capsule-filter-search').val();
            let tags = [];
            $filter.find('.capsule-filter-tag').find('input:checked').each(function() {
                tags.push(this.value);
            });

            let $lst = $tab.find('.capsule-list');
            let arr = [];
            $lst.find('tr').each(function () {
                arr.push($(this).data('attr'));
            });

            let filterIds = arr.filter(function (n) {
                return (!name || n.name.indexOf(name) > -1) && (tags.length === 0 || tags.filter(t => n.tags.includes(t)).length > 0)
            }).map(n => n.id);

            $lst.find('tr').addClass('none');
            filterIds.forEach(function (id) {
                $lst.find('#rd_' + id).parents('tr').removeClass('none');
            });
        }

        $modal.find(' .capsule-filter-search').on('input propertychange', function() {
            let $tab = $(this).parents('.tabs-contentx');
            filter($tab);
        });

        $modal.find('.capsule-filter-tag input').on('click', function() {
            let $tab = $(this).parents('.tabs-contentx');
            filter($tab);
        });
    }

    $form.submit(function (e) {
        let errors = [];
        $form.find('.puzzle tbody tr').each(function() {
            let $tr = $(this);
            if ($tr.find('.capsuleName').length === 0) {
                let no = $tr.data('id');
                let total = 0;
                $tr.find('input[type="number"]').each(function() {
                    total += parseInt($(this).val());
                });
                if(total <= 0 || total > 60) {
                    errors.push(`第${no}轮，轮次题目数量需大于0个，小于60个!`);
                }
            }
        });
        if (errors.length > 0) {
            alert(errors.join('\n'));
            e.preventDefault();
        }
    });

    function validPuzzleInput() {
        $form.find('.puzzle input[type="number"]').on('input propertychange', function() {
            let $tr = $(this).parents('tr');
            let total = 0;
            $tr.find('input[type="number"]').each(function() {
                total += parseInt($(this).val());
            });
            if(total <= 0 || total > 60) {
                $(this).prop('title', '轮次题目数量需大于0个，小于60个!');
                $(this).addClass('invalid');
            } else {
                $(this).prop('title', '');
                $(this).removeClass('invalid');
            }
        })
    }

});


function getQueryVariable(variable) {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        if (pair[0] === variable) {
            return pair[1];
        }
    }
    return false;
}
