$(function() {

  let $page = $('.oAuth2Callback');

  const clientId = 'haichess.com';
  const haichessHost = `${location.protocol}//${location.host}`;
  const redirectUrl = `${haichessHost}/lichess/oauth/callback`;
  const lichessApiEndpoint = 'https://lichess.org/api';
  const codeVerifier = $('main').data('verifiercode');
  const code = extractParamFromUrl(location.href, 'code');
  const state = extractParamFromUrl(location.href, 'state');

  if($page.length) {
    oAuthCallback();
  }

  function oAuthCallback() {
    obtainAccessToken()
      .then((response) => response.json())
      .then((data) => {
        if(!data.error) {
          getAndSetAuthInfo(data.access_token, data.expires_in);
        } else { applyError(data); }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert(error);
      });
  }

  function obtainAccessToken() {
    const body =
      `grant_type=authorization_code&`
      + `code=${encodeURIComponent(code)}&`
      + `redirect_uri=${encodeURIComponent(redirectUrl)}&`
      + `client_id=${encodeURIComponent(clientId)}&`
      + `code_verifier=${codeVerifier}`;

    return fetch(`${lichessApiEndpoint}/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body,
    })
  }

  function getAndSetAuthInfo(accessToken, expiresIn) {
    fetch(`${lichessApiEndpoint}/account`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    }).then((response) => response.json())
      .then((data) => {
        if(!data.error) {
          setAuthInfo(accessToken, expiresIn, data);
        } else { applyError(data); }
    });
  }

  function setAuthInfo(accessToken, expiresIn, account) {
    const body =
        `accessToken=${accessToken}&`
      + `expiresIn=${expiresIn}&`
      + `userId=${encodeURIComponent(account.id)}&`
      + `username=${encodeURIComponent(account.username)}&`;

    fetch(`/lichess/oauth/setAuthInfo?state=${state}`, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      body: body,
    }).then((response) => {
      if(response.ok) {
        applyAccount(account);
      }
    });
  }

  function applyAccount(account) {
    let $h1 = $page.find('h1');
    let $form = $page.find('.form3');
    let $spinner = $form.find('.spinner');
    let $button = $form.find('.button');
    let $username = `<div class="form-group"><label>lichess.org 账号：</label><span>${account.username}</span></div>`;
    let $p = `<p>修改授权设置，可以访问：<a href="/lichess/oauth/list">lichess.org 登录授权</a></p>`;

    $h1.text('lichess.org 用户授权成功！');
    $form.prepend($p);
    $form.prepend($username);
    $spinner.addClass('none');
    $button.removeClass('disabled').attr('href', '/lobby');
  }

  function applyError(error) {
    let $h1 = $page.find('h1');
    let $form = $page.find('.form3');
    let $spinner = $form.find('.spinner');
    let $button = $form.find('.button');
    let $errorCode = `<div class="form-group is-invalid">错误码：${error.error}</div>`;
    let $errorDescription = `<div class="form-group is-invalid">错误信息：${error.error_description}</div>`;

    $h1.text('lichess.org 用户授权失败！');
    $form.prepend($errorDescription);
    $form.prepend($errorCode);
    $spinner.addClass('none');
    $button.removeClass('disabled').attr('href', '/lobby');
  }

  function extractParamFromUrl(url, param) {
    let queryString = url.split('?');
    if (queryString.length < 2) {
      return '';
    }

    // Account for hash URLs that SPAs usually use.
    queryString = queryString[1].split('#');

    const parts = queryString[0]
      .split('&')
      .reduce((a, s) => a.concat(s.split('=')), []);

    if (parts.length < 2) {
      return '';
    }

    const paramIdx = parts.indexOf(param);
    return decodeURIComponent(paramIdx >= 0 ? parts[paramIdx + 1] : '');
  }

});
