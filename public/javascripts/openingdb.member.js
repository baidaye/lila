$(function() {

  let $page = $('.openingdb-member');

  $page.find('#form3-visibility').change(function () {
    let $this = $(this);
    let url = $this.parents('.form3').data('href');
    let visibility = $this.val();
    $.ajax({
      method: 'post',
      url: url,
      data: {visibility: visibility},
      success: function () {
        $.modal.close();
        location.reload();
      },
      error: function (res) {
        handleError(res);
      }
    });
  });

/*
  $page.find('.btn-add').click(function () {
    let openingdbId = $(this).data('id');
    let username = $page.find('.member-add').find('input[name="username"]').val();
    if(!username) return;
    $.ajax({
      method: 'POST',
      url: `/resource/openingdb/${openingdbId}/member/add`,
      data: {'username': username}
    }).then(function (res) {
      location.reload();
    }, function (err) {
      handleError(err);
    });
  });
*/

  $('a.modal-alert').click(function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        memberChooseModal();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });

  function memberChooseModal() {
    let $modal = $('.modal-member-choose');
    let $transfer = $modal.find('.transfer');
    let $leftPanel = $transfer.find('.left');
    let $leftList = $leftPanel.find('.transfer-panel-list table tbody');
    let oldMemberIds = $modal.find('input[name=members]').val();
    let checkedMemberIds = oldMemberIds ? oldMemberIds.split(',') : [];

    let onChange = (checkeds) => {
      checkedMemberIds = checkeds;
    };

    search();
    transfer(onChange);

    function search() {
      let $searchForm = $modal.find('.member-search');

      $searchForm.find('input[name=username]').on('input propertychange', function () {
        let username = $(this).val();
        let arr = [];
        $leftList.find('input').each(function () {
          arr.push($(this).data('attr'));
        });

        let filterIds = arr.filter(n => {
          return (
            (!username || n.name.includes(username))
          )
        }).map(n => n.id);

        $leftList.find('tr').addClass('none');
        filterIds.forEach(function (id) {
          $leftList.find('#chk_' + id).parents('tr').removeClass('none');
        });
      });
    }

    let $transferForm = $modal.find('.member-choose-transfer');
    $transferForm.submit(function(e) {
      e.preventDefault();

      let url = $(this).attr('action');
      $.ajax({
        method: 'POST',
        url: url,
        data: { "members": checkedMemberIds.join(',') },
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
        }
      });
    })
  }

});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
