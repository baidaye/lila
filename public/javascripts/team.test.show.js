$(function () {

  let $page = $('.test-show');
  $page.find('a.modal-alert').click(function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        onSendOpen();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });

  function onSendOpen() {
    onMemberOpen();

    let $modal = $('.member-modal');
    let $form = $modal.find('.form-send');
    $form.find('.flatpickr').flatpickr({enableTime: true, 'time_24hr': true});

    $form.submit((e) => {
      e.preventDefault();
      let $checkeds = $form.find('tbody input[name^="members"]:checked');
      let checkedIds = [];
      $checkeds.each((i, el) => {
        checkedIds.push(el.value);
      });

      if(!checkedIds.length) {
        alert('至少选择一名学员');
        return false;
      }

      $.ajax({
        method: 'post',
        url: $form.attr('action'),
        data: $form.serialize(),
        success: (res) => {
          if(res && res.ok) {
            let successHtml =
              `<h2>发送成功</h2>
                <div>
                  <p>成功发送给<span class="success">${checkedIds.length}</span>名学员</p>
                  <table class="slist">
                    <thead>
                      <th>账号</th>
                      <th>备注（姓名）</th>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
                <div class="form-actions">
                  <button class="button button-empty cancel">关闭</button>
                </div>`;

            let $successHtml = $(successHtml);
            $checkeds.each((_, el) => {
              let $tr = $(el).parents('tr');
              let $td1 = $tr.find('td:eq(1)');
              let $td2 = $tr.find('td:eq(2)');
              let $successTr = $('<tr></tr>').append($td1).append($td2);
              $successHtml.find('tbody').append($successTr);
            });
            $modal.addClass('send-success').html($successHtml);

            $('.cancel').click(function () {
              location.reload();
              $.modal.close();
            });
          } else {
            alert(res.error);
          }
        },
        error: function (res) {
          handleError(res);
        }
      });
      return false;
    });
  }

  $page.find('.test-remark').on('change keyup paste', window.lichess.debounce((e) => {
    $.post(`/team/test/stu/${$(e.target).data('id')}/remark`, {data: e.target.value});
  }, 500));

});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
