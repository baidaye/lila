$(function () {

  memberAdvance();

  let $list = $('.members');
  $list.find('input[name="mem_chk_all"]').click(function () {
    let isChecked = $(this).is(':checked');
    $list.find('input[name^="mems"]').prop('checked', false);
    if(isChecked) {
      $list.find('input[name^="mems"]').prop('checked', true);
    }
  });

  $('a.btn-giving').click(function (e) {
    e.preventDefault();
    let mems = $list.find('tbody').find('input[name^="mems"]:checked');
    if(mems.length === 0) {
      alert('至少选择一名学员');
      return;
    }

    if(mems.length > 50) {
      alert('最多选择50名学员');
      return;
    }

    $.ajax({
      url: $(this).attr('href'),
      data: $list.find('form').serialize(),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        loadCardCount();
        cardCount();
        submit();
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  function cardCount() {
    let $form = $('.batchGivingModal form');
    $form.find('#form3-cardLevel').change(function () {
      loadCardCount();
    });
    $form.find('#form3-days').change(function () {
      loadCardCount();
    });
  }

  function loadCardCount() {
    let $form = $('.batchGivingModal form');
    let memberCount = $form.find('input[name="memberCount"]').val();
    $.ajax({
      url: `/member/card/cardCount`,
      data: $form.serialize(),
      success: function (res) {
        $form.find('.num').text(res.count);
        if(res.count < memberCount) {
          $form.find('.submit').prop('disabled', true).addClass('disabled');
          $form.find('.count-invalid').removeClass('none');
        } else {
          $form.find('.submit').prop('disabled', false).removeClass('disabled');
          $form.find('.count-invalid').addClass('none');
        }
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
  }

  function submit() {
    let $form = $('.batchGivingModal form');
    $form.submit(function (e) {
      e.preventDefault();
      let cardLevel= $form.find('#form3-cardLevel').val();
      let days= $form.find('#form3-days').val();
      $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (res) {
          $.modal.close();
          giveSuccess(res, cardLevel, days);
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
      return false;
    });
  }

  function giveSuccess(logIds, cardLevel, days) {
    $.ajax({
      url: '/member/card/batchGivingSuccess',
      data: {
        logIds: logIds.join(','),
        cardLevel: cardLevel,
        days: days
      },
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
          location.reload();
        });
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
  }

});
