$(function () {

  lichess.pubsub.on('content_loaded', function () {
    applyModal();
    closeSubmit();
  });
  applyModal();
  closeSubmit();

  function applyModal() {
    $('a.modal-alert').not('.disabled').off('click').on('click', function (e) {
      e.preventDefault();
      $.ajax({
        url: $(this).attr('href'),
        success: function (html) {
          $.modal($(html));
          $('.cancel').click(function () {
            $.modal.close();
          });

          applyAddModal();
          applyResetPasswordModal();
          applyUseCardModal();
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
      return false;
    });
  }

  function applyAddModal() {
    let $modal = $('.team-account-add');
    if($modal.length > 0) {
      let $form = $modal.find('form');

      loadCardCount();
      $form.find('#form3-cardLevel').change(function () {
        loadCardCount();
      });
      $form.find('#form3-days').change(function () {
        loadCardCount();
      });

      $form.submit(function (e) {
        e.preventDefault();
        $form.find(`.error-usernames`).addClass('none');
        $form.find(`.error-password`).addClass('none');
        $form.find(`.error-cardLevel`).addClass('none');
        $.ajax({
          method: 'post',
          url: $modal.find('form').attr('action'),
          data: $form.serialize(),
          success: function () {
            $.modal.close();
            location.reload();
          },
          error: function (res) {
            handleError(res);
          }
        });
        return false;
      });
    }
  }

  function applyResetPasswordModal() {
    let $modal = $('.team-account-password');
    if($modal.length > 0) {
      let $form = $modal.find('form');

      $form.off('submit').submit(function (e) {
        e.preventDefault();
        $form.find(`.error-password`).addClass('none');
        $.ajax({
          method: 'post',
          url: $modal.find('form').attr('action'),
          data: $form.serialize(),
          success: function () {
            $.modal.close();
            alert('操作成功！');
            location.reload();
          },
          error: function (res) {
            handleError(res);
          }
        });
        return false;
      });
    }
  }

  function applyUseCardModal() {
    let $modal = $('.team-account-card');
    if($modal.length > 0) {
      let $form = $modal.find('form');

      loadCardCount();
      $form.find('#form3-cardLevel').change(function () {
        loadCardCount();
      });
      $form.find('#form3-days').change(function () {
        loadCardCount();
      });

      $form.off('submit').submit(function (e) {
        e.preventDefault();
        $form.find(`.error-cardLevel`).addClass('none');
        $.ajax({
          method: 'post',
          url: $modal.find('form').attr('action'),
          data: $form.serialize(),
          success: function () {
            $.modal.close();
            alert('操作成功！');
            location.reload();
          },
          error: function (res) {
            handleError(res);
          }
        });
        return false;
      });
    }
  }

  function loadCardCount() {
    let $form = $('.modal-content form');
    let cardLevel = $form.find('#form3-cardLevel').val();
    let $days = $form.find('#form3-days');
    let $daysForever = $form.find('.days-forever');
    let $cardCount = $form.find('.card-count');

    if(cardLevel === 'general') {
      $cardCount.addClass('none');
      $days.addClass('none');
      $daysForever.removeClass('none');
    } else {
      $cardCount.removeClass('none');
      $days.removeClass('none');
      $daysForever.addClass('none');

      $.ajax({
        url: `/member/card/cardCount`,
        data: $form.serialize(),
        success: function (res) {
          $cardCount.text(`共 ${res.count} 张`);
        },
        error: function (res) {
          handleError(res);
        }
      });
    }
  }

  function closeSubmit() {
    $('.member-close').each(function () {
      let $form = $(this);
      $form.off('submit').on('submit', function (e) {
        e.preventDefault();
        let cfm = $form.find('.button').attr('title');
        if(confirm(cfm)) {
          $.ajax({
            method: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (res) {
              if (res.ok) {
                location.reload();
              } else {
                alert(res.err)
              }
            },
            error: function (res) {
              alert(res.responseText);
            }
          });
        }
        return false;
      });
    });
  }

});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if(res.status === 400) {
      for(let key in json) {
        $(`.error-${key}`).removeClass('none').text(json[key][0]);
      }
    } else {
      if (json.error) {
        if(typeof json.error === 'string') {
          alert(json.error);
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    }
  } else alert('发生错误');
}
