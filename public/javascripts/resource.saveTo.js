$(function() {


  function showSaveTo(pgn, source, rel) {
    if(!pgn) {
      alert("PGN内容不能为空");
      return;
    }

    $.ajax({
      method: 'post',
      url: `/resource/saveTo?source=${source}&rel=${rel}`,
      data: JSON.stringify({pgn}),
      contentType: 'application/json; charset=utf-8',
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });

      let $saveTo = $('.modal-content.saveTo');

      $saveTo.find('.tabs-horiz span').not('.disabled').click(function (e) {
        let $this = $(e.target);
        $saveTo.find('.tabs-horiz span').removeClass("active");
        $saveTo.find('.tabs-content div').removeClass("active");

        let cls = $this.data('tab');
        $this.addClass('active');
        $saveTo.find('.tabs-content div.' + cls).addClass('active');
      });

      ($saveTo.find('#form3-tags,#form3-puzzleTag')).tagsInput({
        'height': '40px',
        'width': '100%',
        'interactive': true,
        'defaultText': '添加',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 10,
        'placeholderColor': '#666666'
      });

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.modal-content.saveTo .gamedb').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/gamedb/tree/load'
              },
              'check_callback': true
            }
          })
            .on('changed.jstree', function (_, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                $form.find('#form3-gamedbId').val(data.node.id);
              }
            });

          $form.submit(function (e) {
            e.preventDefault();
            let gamedbId = $form.find('#form3-gamedbId').val();

            $.ajax({
              method: 'POST',
              url: `/resource/gamedb/rel/create?gamedbId=${gamedbId}`,
              data: $form.serialize()
            }).then(function (res) {
              if(res && res.ok) {
                alert(`保存成功！\r\n请到 "资源 > 对局 > 对局数据库" 中查看`);
              }
              $.modal.close();
            }, function (err) {
              handleError(err);
            });
            return false;
          });
        })
      });

      window.lichess.pubsub.on('embed.move.pgn', setPgn);

      let $pgnInput = $saveTo.find('#form3-pgn');
      let pgnChangeTrigger = true;
      function setPgn(pgn) {
        pgnChangeTrigger = false;
        $pgnInput.val(pgn);
        setTimeout(() => {
          pgnChangeTrigger = true;
        }, 210);
      }

      let $puzzleForm = $saveTo.find('.puzzle form');
      $puzzleForm.submit(function (e) {
        e.preventDefault();
        $.ajax({
          method: 'POST',
          url: `/resource/puzzle/saveTo`,
          data: $puzzleForm.serialize()
        }).then(function (res) {
          if(res && res.ok) {
            alert(`保存成功！\r\n请到 "资源 > 战术题 > 导入的战术题" 中查看`);
          }
          $.modal.close();
        }, function (err) {
          handleError(err);
        });
        return false;
      });

      let $openingdbForm = $saveTo.find('.openingdb form');
      $openingdbForm.submit(function (e) {
        e.preventDefault();
        let openingdbId = $openingdbForm.find('input[name=openingdbId]:checked').val();
        if(!openingdbId) {
          alert('请选择一个开局库');
          return;
        }

        $.ajax({
          method: 'POST',
          url: `/resource/openingdb/${openingdbId}/addNodes`,
          data: $openingdbForm.serialize()
        }).then(function (res) {
          if(res) {
            alert(`保存成功！\r\n请到 "资源 > 开局" 中查看`);
          }
          $.modal.close();
        }, function (err) {
          handleError(err);
        });
        return false;
      });

      return false
    }, function (err) {
      if (err.status === 406) {
        window.lichess.memberIntro();
      } else {
        handleError(err);
      }
    });

    function handleError(res) {
      let json = res.responseJSON;
      if (json) {
        if (json.error) {
          if (typeof json.error === 'string') {
            alert(json.error);
          } else alert(JSON.stringify(json.error));
        } else alert(res.responseText);
      } else alert('请检查PGN格式');
    }
  }

  $('.resourceSaveTo').click(function (e) {
    e.preventDefault();
    if($(this).hasClass('notAccept')) {
      lichess.memberIntro();
    } else {
      let pgn = $(this).data('simplepgn');
      let source = $(this).data('source');
      let rel = $(this).data('rel');
      showSaveTo(pgn, source, rel);
    }
  });

  window.lichess.pubsub.on('pgn.saveTo', data => {
    // pgn, source, rel
    showSaveTo(data.pgn, data.source, data.rel);
  });

});
