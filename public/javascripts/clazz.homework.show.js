$(function () {
  let $page = $('.homework-show');
  if($page.length) {
    document.addEventListener("visibilitychange", () => {
      if (!document.hidden) {
        let id = $('main').data('id');
        let isStudentSeeing = $('main').data('is-student');
        let showFirstComplete = $page.find('#show-first-complete').is(':checked');
        if (isStudentSeeing) {
          location.href = `/clazz/homework/show?id=${id}&showPuzzleFirstComplete=${showFirstComplete}`
        }
      }
    });

    $page.find('#show-first-complete').change(function () {
      let showFirstComplete = $(this).is(':checked');
      let homeworkId = $(this).data('id');
      location.href = `/clazz/homework/show?id=${homeworkId}&showPuzzleFirstComplete=${showFirstComplete}`;
    });

    chessMove($page);
    lazyLoadCapsulePuzzle($page);
    fromPositionModal($page);
    fromPgnModal($page);
    notAcceptModal($page);
    showDeadlineModal();
  }
});


function showDeadlineModal() {
  let id = $('main').data('id');
  let isDeadline = $('main').data('is-deadline');
  let isStudent = $('main').data('is-student');
  let isDeadlineShowed = $.cookie(`isDeadlineShowed:${id}`);
  if(isDeadline && isStudent && !isDeadlineShowed) {
    $.modal(
      `<div class="modal-content modal-deadline">
        <h2>提示</h2>
        <div class="tips" data-icon="">课后练已过期，继续练习不再更新进度</div>
        <button class="button cancel">确定</button>
    </div>`
    );
    $.cookie(`isDeadlineShowed:${id}`, true, { expires: 1 });

    $('.cancel').click(function () {
      $.modal.close();
    });
  }
}


