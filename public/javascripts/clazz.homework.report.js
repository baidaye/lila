$(function () {

  let theme = $('body').hasClass('light') ? 'light' : 'dark';

  let $page = $('.homework-report');

  if($page.length) {

    resetURL();

    // 训练目标 tooltip
    $page.find('.tooltip-tags').each(function () {
      let $this = $(this);
      $this.powerTip({
        placement: 'se',
        popupId: 'tagsTooltip',
        intentPollInterval: 100,
        mouseOnToPopup: true,
        closeDelay: 200
      }).data('powertip', $this.find('.tags'));
    });

    // 完成情况总览
    $page.find('.completeRateDistribute-chart').each(function () {
      let $this = $(this);
      let completeRateDistributeChart = echarts.init(this, theme);
      let completeRateDistributeChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}%'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          className: 'crd-tooltip',
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let stus;
            if (param.data[2] && param.data[2].length > 0) {
              stus = param.data[2].map(function (stu) {
                return `<tr><td>${stu.u}</td><td>${stu.r}%</td></tr>`;
              }).join('');
            } else {
              stus = '<tr><td>无</td></tr>'
            }

            return `<table class="crd-tooltip"><tbody>${stus}</tbody></table>`;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series'),
        }]
      };

      completeRateDistributeChart.setOption(completeRateDistributeChartOption);
    });

    // 指定战术题
    $page.find('.practice-part-capsulePuzzle').each(function () {
      let $capsulePuzzle = $(this);
      capsulePuzzleArea($capsulePuzzle, theme);
    });

    // 打谱 - 饼图
    $page.find('.replayGame-chart').each(function () {
      let $this = $(this);
      let replayGameChart = echarts.init(this, theme);
      let replayGameChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        tooltip: {
          trigger: 'item',
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}：${param.value}人（${param.percent}%）`;
            if (param.data.students && param.data.students.length > 0) {
              let stus = param.data.students.map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>------------------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        color: ['#90ed7d', '#f7a35c'],
        series: [
          {
            name: '完成率',
            type: 'pie',
            radius: '65%',
            center: ['50%', '50%'],
            data: $this.data('series')
          }
        ]
      };

      replayGameChart.setOption(replayGameChartOption);
    });

    // 记谱 - 柱状图
    $page.find('.recallGame-chart').each(function () {
      let $this = $(this);
      let recallGameChart = echarts.init(this, theme);
      let recallGameChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}回合'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}人'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}回合：${param.data[1]}人`;
            if (param.data[2] && param.data[2].length > 0) {
              let stus = param.data[2].map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>-----------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series'),
        }]
      };

      recallGameChart.setOption(recallGameChartOption);
    });

    // 棋谱记录 - 柱状图
    $page.find('.distinguishGame-chart').each(function () {
      let $this = $(this);
      let distinguishGameChart = echarts.init(this, theme);
      let distinguishGameChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}步'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}人'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}步：${param.data[1]}人`;
            if (param.data[2] && param.data[2].length > 0) {
              let stus = param.data[2].map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>-----------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series')
        }]
      };

      distinguishGameChart.setOption(distinguishGameChartOption);
    });

    // 指定位置对局 - 柱状图
    $page.find('.fromPosition-chart').each(function () {
      let $this = $(this);
      let fromPositionChart = echarts.init(this, theme);
      let fromPositionChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}局'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}人'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}局：${param.data[1]}人`;
            if (param.data[2] && param.data[2].length > 0) {
              let stus = param.data[2].map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>-----------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series'),
        }]
      };

      fromPositionChart.setOption(fromPositionChartOption);
    });

    // 指定PGN对局 - 柱状图
    $page.find('.fromPgn-chart').each(function () {
      let $this = $(this);
      let fromPgnChart = echarts.init(this, theme);
      let fromPgnChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}局'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}人'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}局：${param.data[1]}人`;
            if (param.data[2] && param.data[2].length > 0) {
              let stus = param.data[2].map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>-----------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series'),
        }]
      };

      fromPgnChart.setOption(fromPgnChartOption);
    });

    // 指定开局库对局 - 柱状图
    $page.find('.fromOpeningdb-chart').each(function () {
      let $this = $(this);
      let fromOpeningdbChart = echarts.init(this, theme);
      let fromOpeningdbChartOption = {
        grid: {
          left: '40',
          right: '10',
          top: '10',
          bottom: '25',
        },
        xAxis: {
          type: "category",
          axisLabel: {
            formatter: '{value}局'
          }
        },
        yAxis: {
          show: true,
          type: 'value',
          axisLabel: {
            fontSize: 9,
            formatter: '{value}人'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        },
        tooltip: {
          textStyle: {
            fontSize: 10
          },
          formatter: function (param) {
            let html = `${param.name}局：${param.data[1]}人`;
            if (param.data[2] && param.data[2].length > 0) {
              let stus = param.data[2].map(function (stu) {
                return `${stu}<br/>`;
              });
              return html + '<br/>-----------------<br/>' + stus.join('');
            }
            return html;
          }
        },
        series: [{
          type: 'bar',
          barWidth: 40,
          label: {
            show: true,
            fontSize: 9,
            formatter: '{@[1]}人'
          },
          data: $this.data('series'),
        }]
      };

      fromOpeningdbChart.setOption(fromOpeningdbChartOption);
    });

    chessMove($page);
    puzzleSortReload($page);
    completeRateSortReload($page);
    puzzleFirstMoveTip($page);
    showCoinModal($page);
  }

});

function resetURL() {
  history.replaceState(null, '', '/clazz/homework/refreshReport' + location.search);
}

function capsulePuzzleArea($capsulePuzzle, theme) {

  $capsulePuzzle.find('.firstRightRateDistribute-chart').each(function () {
    let $this = $(this);
    let firstRightDistributeChart = echarts.init(this, theme);
    let firstRightDistributeChartOption = {
      title: {
        left: 'center',
        text: '学员首次正确率',
      },
      grid: {
        top: '40',
        left: '40',
        right: '20',
        bottom: '70'
      },
      xAxis: {
        type: "category",
        axisLabel: {
          formatter: '{value}%'
        }
      },
      yAxis: {
        show: true,
        type: 'value',
        axisLabel: {
          fontSize: 9,
          formatter: '{value}'
        },
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        }
      },
      tooltip: {
        className: 'crd-tooltip',
        textStyle: {
          fontSize: 10
        },
        formatter: function (param) {
          let stus;
          if (param.data[2] && param.data[2].length > 0) {
            stus = param.data[2].map(function (stu) {
              return `<tr><td>${stu.u}</td><td>${stu.r}%</td></tr>`;
            }).join('');
          } else {
            stus = '<tr><td>无</td></tr>'
          }

          return `<table class="crd-tooltip"><tbody>${stus}</tbody></table>`;
        }
      },
      series: [{
        type: 'bar',
        barWidth: 40,
        label: {
          show: true,
          fontSize: 9,
          formatter: '{@[1]}人'
        },
        data: $this.data('series'),
      }]
    };

    firstRightDistributeChart.setOption(firstRightDistributeChartOption);
  });

  let puzzleAllChart;
  let puzzleAllChartOption;
  $capsulePuzzle.find('.puzzle-all-chart').each(function () {
    const lineDataMap = {
      '完成率': {
        symbol: 'circle',
        color: '#7cb5ec'
      },
      '正确率': {
        symbol: 'rect',
        color: '#90ed7d'
      },
      '首次正确率': {
        symbol: 'triangle',
        color: '#e4d354'
      }
    };
    let $chart = $(this);
    let $pzs = $capsulePuzzle.find('.puzzles>li');
    let xData = $chart.data('xaxis').map(x => `第${x}题`);
    let currentDataIndex = 0;
    puzzleAllChart = echarts.init(this, theme);
    puzzleAllChartOption = {
      title: {
        left: 'center',
        text: '题目完成情况',
      },
      legend: {
        bottom: 20,
        data: [
          {
            name: '完成率',
            icon: 'circle'
          },
          {
            name: '正确率',
            icon: 'rect'
          },
          {
            name: '首次正确率',
            icon: 'triangle'
          }
        ]
      },
      tooltip: {
        trigger: 'axis',
        textStyle: {
          fontSize: 10
        },
        formatter: function (params) {
          let name = params[0].name;
          let dataIndex = params[0].dataIndex;
          let tooltip = name + '<br/>';
          params.forEach(function (item, i) {
            if (i < params.length - 1) {
              tooltip += item.marker + item.seriesName + '：' + item.value + '%<br/>';
            } else {
              tooltip += item.marker + item.seriesName + '：' + item.value + '%';
            }
          });

          if (dataIndex != currentDataIndex) {
            chartPointChange(dataIndex);
            currentDataIndex = dataIndex;
          }
          return tooltip;
        }
      },
      grid: {
        top: '40',
        left: '40',
        right: '20',
        bottom: '70'
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: xData
      },
      yAxis: {
        show: true,
        type: 'value',
        axisLabel: {
          fontSize: 9,
          formatter: '{value}%'
        },
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        }
      },
      series: $chart.data('series').map(function (item, i) {
        item.data = item.data.map(function (d) {
          return {value: d, symbolSize: 4};
        });
        return {...item, ...lineDataMap[item.name]}
      })
    };
    puzzleAllChart.setOption(puzzleAllChartOption);

    puzzleAllChart.on('click', function (params) {
      chartPointChange(params.dataIndex);
    });

    function chartPointChange(dataIndex) {
      $pzs.not('.none').addClass('none');
      $pzs.eq(dataIndex).removeClass('none');
      setPuzzleChartsSelect(dataIndex);
    }
  });
  setPuzzleChartsSelect(0);
  function setPuzzleChartsSelect(index) {
    if (puzzleAllChartOption && puzzleAllChartOption.series) {
      let series = puzzleAllChartOption.series.map(function (serie) {
        serie.data.forEach(function (d, i) {
          if (i === index) {
            d.symbolSize = 10;
          } else {
            d.symbolSize = 4;
          }
        });
        return serie;
      });
      puzzleAllChartOption.series = series;
      puzzleAllChart.setOption(puzzleAllChartOption);
    }
  }

  // 指定战术题 - 柱状图
  $capsulePuzzle.find('.puzzle-chart').each(function () {
    let $this = $(this);
    let puzzleChart = echarts.init(this, theme);
    let puzzleChartOption = {
      grid: {
        left: '40',
        right: '10',
        top: '10',
        bottom: '25',
      },
      tooltip: {
        trigger: 'item',
        textStyle: {
          fontSize: 10
        },
        formatter: function (param) {
          let html = `${param.name}：${param.data[1]}%`;
          if (param.data[2] && param.data[2].length > 0) {
            let stus = param.data[2].map(function (stu) {
              return `${stu}<br/>`;
            });
            return html + '<br/>--------------------<br/>' + stus.join('');
          }
          return html;
        }
      },
      xAxis: {
        type: "category",
        data: ["完成率", "正确率", "首次正确率"]
      },
      yAxis: {
        show: true,
        type: 'value',
        axisLabel: {
          fontSize: 9,
          formatter: '{value}%'
        },
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        }
      },
      series: [{
        type: 'bar',
        barWidth: 40,
        label: {
          show: true,
          fontSize: 9,
          formatter: '{@[1]}%'
        },
        data: $this.data('series')
      }]
    };

    puzzleChart.setOption(puzzleChartOption);
  });

  // 指定战术题 - 上一题
  $capsulePuzzle.find('.puzzles .prev').click(function () {
    let $puzzle = $(this).parents('li');
    let $prevPuzzle = $puzzle.prev('li');
    if ($prevPuzzle.length > 0) {
      $puzzle.addClass('none');
      $prevPuzzle.removeClass('none');
    }
    let index = $capsulePuzzle.find('.puzzles>li').not('.none').index();
    setPuzzleChartsSelect(index);
  });

  // 指定战术题 - 下一题
  $capsulePuzzle.find('.puzzles .next').click(function () {
    let $puzzle = $(this).parents('li');
    let $nextPuzzle = $puzzle.next('li');
    if ($nextPuzzle.length > 0) {
      $puzzle.addClass('none');
      $nextPuzzle.removeClass('none');
    }
    let index = $capsulePuzzle.find('.puzzles>li').not('.none').index();
    setPuzzleChartsSelect(index);
  });
}

function puzzleFirstMoveTip($page) {
  let $m1 = $page.find('#puzzle-first-move-num span');
  let $m2 = $page.find('#puzzle-first-move-num:hidden').find('span');
  $.each([$m1, $m2], function (i, $jq) {
    $jq.each(function () {
      let $this = $(this);
      $this.powerTip({
        placement: 'se',
        popupId: 'firstMoveTip',
        intentPollInterval: 100,
        mouseOnToPopup: true,
        closeDelay: 200
      }).data('powertip', function () {
        let attr = $this.data('attr');
        let stus = '';
        if (attr && attr.length > 0) {
          stus += '<ul>';
          attr.map(function (stu) {
            stus += `<li>${stu}</li>`;
          });
        }
        return stus;
      });
    });
  });
}

function puzzleSortReload($page) {
  $page.find('.puzzleSort input').click(function () {
    let sort = $(this).val();
    let id = $('main').data('id');
    let $completeRateSorter = $page.find('.common-slist').find('.sorter .active');
    let completeRateSortType = $completeRateSorter.length ? $completeRateSorter.data('sortType') : '';
    location.href = `/clazz/homework/report?id=${id}&puzzleSort=${sort}&completeRateSortType=${completeRateSortType}`;
  });
}

function completeRateSortReload($page) {
  let $common = $page.find('.common-slist');
  $common.find('.sorter a').click(function () {
    let $this = $(this);
    let id = $('main').data('id');
    let puzzleSort = $page.find('.puzzleSort input:checked').val();
    let v = $this.hasClass('active') ? '' : $this.data('sortType');
    location.href = `/clazz/homework/report?id=${id}&puzzleSort=${puzzleSort ? puzzleSort : ''}&completeRateSortType=${v}`;
  });
}

function showCoinModal($page) {
  $page.find('a.modal-task-coin').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        let $form = $('.homework-coin-edit .form3');
        $form.submit(function (e) {
          e.preventDefault();
          $.ajax({
            method: 'post',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function () {
              $.modal.close();
              setTimeout(function () {
                location.reload();
              }, 500);
            },
            error: function (res) {
              handleError(res);
            }
          });
          return false;
        });
      }
    });
    return false;
  });
}
