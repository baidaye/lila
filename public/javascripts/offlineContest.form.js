$(function () {
    dateFormat();
    let $form = $('.contest__form');

    $form.find('.single-uploader').find('input[type=\'file\']').ajaxSingleUpload({
        'name': 'logo',
        'action': '/contest/off/imageUpload'
    });

    $form.find('#form3-typ').change(function () {
        let o = $form.find('#form3-organizer');
        let v = $(this).val();
        let d = $form.data(v);
        let teamId = getQueryVariable("team");
        let selectedId = $('input[name=organizerSelected]').val();
        selectedId = selectedId ? selectedId : teamId;

        o.empty();
        for (let i = 0; i < d.length; i++) {
            let selected = selectedId && selectedId == d[i].id;
            o.append("<option value = '" + d[i].id + "' " + (selected ? 'selected' : '') + " >" + d[i].name + "</option>");
        }
        o.trigger('change');
    });

    $form.find('#form3-organizer').change(function () {
        let tpy = $form.find('#form3-typ').val();
        let val = $(this).val();
        let dataArr = $form.data(tpy);
        let teamRatedArr = dataArr.filter(n => n.id == val && n.teamRated);
        if(teamRatedArr.length > 0) {
            $('#form3-teamRated').parents('.form-check').removeClass('none');
        } else {
            $('#form3-teamRated').parents('.form-check').addClass('none');
        }
    });
    $form.find('#form3-typ').trigger('change');


    $form.find('#form3-rule').change(function () {
        let v = $(this).val();
        $.get(`/contest/rule/info?id=${v}`).then(function (data) {
            //  "id" -> r.id,
            //  "name" -> r.name,
            //  "setup"
            //      "formField" -> r.setup.formField,
            //      "maxRound" -> r.setup.maxRound,
            //      "defaultRound" -> r.setup.defaultRound,
            //      "minPlayer" -> r.setup.minPlayer,
            //      "maxPlayer" -> r.setup.maxPlayer,
            //      "defaultPlayer" -> r.setup.defaultPlayer,
            //      "btsss"
            //          "id" -> btss.id,
            //          "name" -> btss.name

            let $btss = $form.find('.btss-wrap');
            let $btssList = $btss.find('.btss-list');
            let $btssHidden = $btss.find('.btss-hidden');
            $btssList.empty();
            $btssHidden.empty();
            data.setup.btsss.forEach(function (btss, index) {
                $btssList.append(`<span class="btss">${btss.name}</span>`);
                $btssHidden.append(`<input type="hidden" name="${data.setup.formField}[${index}]" value="${btss.id}">`);
            });
            $btss.children('label').text(data.name);
            $form.find('#form3-rounds')
              .attr('max', data.setup.maxRound)
              .val(data.setup.defaultRound);
        });
    });

});



function getQueryVariable(variable) {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return false;
}

function datePickOption() {
    return {
        time_24hr: true,
        altFormat: 'Y-m-d H:i'
    }
}

function dateFormat() {
    Date.prototype.format = function (fmt) {
        let o = {
            "M+": this.getMonth() + 1,                 //月份
            "d+": this.getDate(),                    //日
            "h+": this.getHours(),                   //小时
            "m+": this.getMinutes(),                 //分
            "s+": this.getSeconds(),                 //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds()             //毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };
}
