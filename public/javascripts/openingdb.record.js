$(function () {
  let $page = $('.openingdb-record');
  chessMove($page);

  function chessMove($page) {
    $page.find('.moves').each(function() {
      let $moves = $(this);
      $moves.find('move span:not(.disabled)').click(function () {
        let fen = $(this).data('fen');
        let $board = $(this).parents('tr').find('.td-board .mini-board');
        Chessground($board[0], {
          coordinates: false,
          resizable: false,
          drawable: {enabled: false, visible: false},
          viewOnly: true,
          fen: fen
        });
        $board.attr('data-fen', fen);

        $moves.find('move span.active').removeClass('active');
        $(this).addClass('active');
      });
    });
  }

  let $pgnInput = $page.find('#form3-pgn');
  let $pgnPreview = $page.find('#board_iframe');
  let pgnChangeTrigger = true;
  function setPgn(pgn) {
    pgnChangeTrigger = false;
    $pgnInput.val(pgn);
    setTimeout(() => {
      pgnChangeTrigger = true;
    }, 210);
  }

  let validatePgn = lichess.debounce(function() {
    let pgn = $pgnInput.val();
    if (pgn) {
      $pgnPreview.prop('src', `/analysis/pgnEmbed?urlPgn=${encodeURIComponent(pgn)}`);
    } else {
      $pgnPreview.prop('src', `/analysis/pgnEmbed`);
    }
  }, 200);
  $pgnInput.on('change paste', validatePgn);

  window.lichess.pubsub.on('embed.move.pgn', setPgn);

});
