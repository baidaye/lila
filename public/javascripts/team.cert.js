$(function() {

    $('a.cert-modal').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            success: function(html) {
                $.modal($(html));
                $('.cancel').click(function () {
                    $.modal.close();
                });

                let $modal = $('.modal-content');
                let $form = $modal.find('form');
                $form.submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: $form.attr('action'),
                        data: $form.serialize()
                    }).then(function() {
                        location.reload();
                        $.modal.close();
                    }, function (err) {
                        handleError(err)
                    });
                });
            },
            error: function(res) {
                handleError(res);
            }
        });
        return false;
    });

    function handleError(res) {
        let json = res.responseJSON;
        if (json) {
            if (json.error) {
                if (typeof json.error === 'string') {
                    alert(json.error);
                } else alert(JSON.stringify(json.error));
            } else alert(res.responseText);
        } else alert('发生错误');
    }

});
