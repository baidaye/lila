$(function() {
  let $form = $('.orderForm');
  let product = lichess.buy.product;
  let vmData = {
    productId: product.id,
    itemCode: product.defaultItem,
    item: {},
    count: 1,
    price: 0.00,
    discountPrice: 0.00,
    totalPrice: 0.00,
    priceAfterSilver: 0.00,
    afterPromotionPrice: 0.00,
    points: 0,
    maxPoints: 0,
    pointsAmount: 0.00,
    inviteUserAmount: 0.00,
    inviteUser: '',
    payPrice: 0.00
  };

  registerItemChange();

  function registerItemChange() {
    $form.find('input[name=itemCode]').change(function() {
      let $item = $(this);
      if($item.is(':checked')){
        let itemCode = $item.val();
        let item = product.items[itemCode];

        registerCountChange();
        if(item.isPoint) {
          $form.find('input[name=points]')
            .attr('disabled', false)
            .parents('tr')
            .removeClass('none');
          registerPointsChange();
        } else {
          $form.find('input[name=points]')
            .attr('disabled', true)
            .parents('tr')
            .addClass('none');
        }

        if(item.isInviteUser) {
          $form.find('input[name=inviteUser]')
            .attr('disabled', false)
            .parents('tr')
            .removeClass('none');
          registerInviteUserChange();
        } else {
          $form.find('input[name=inviteUser]')
            .attr('disabled', true)
            .parents('tr')
            .addClass('none');
        }

        if(item.discountDesc) {
          $form.find('div.note')
            .text(item.discountDesc)
            .removeClass('none');
        } else {
          $form.find('div.note')
            .addClass('none');
        }

        vmData.itemCode = itemCode;
        requestPrice();
      }
    }).trigger('change');
  }

  function registerCountChange() {
    $form.find('input[name=count]').blur(function() {
      if($form.find('input[name=count]').val() != vmData.count) {
        requestPrice();
      }
    });
  }

  function registerPointsChange() {
    $form.find('input[name=points]').blur(function() {
      if($form.find('input[name=points]').val() != vmData.points) {
        $form.find('input[name=isPointsChange]').val(true);
        requestPrice(true);
      }
    });
  }

  function registerInviteUserChange() {
    $form.find('select[name=inviteUser]').change(function() {
      if($form.find('select[name=inviteUser]').val() != vmData.inviteUser) {
        requestPrice();
      }
    });
  }

  $form.keypress(function (e) {
    if(e.keyCode === 13) {
      return false;
    }
  });

  function renderPrice() {
    let $price =  $form.find('.price');
    $price.find('.number').text(vmData.discountPrice);
    $price.find('.del-price').text(vmData.price);

    if(vmData.price === vmData.discountPrice) {
      $price.find('del').addClass('none');
    } else {
      $price.find('del').removeClass('none');
    }

    //---------
    let $payPrice =  $form.find('.payPrice');
    $payPrice.find('.number').text(vmData.payPrice);
    $payPrice.find('.del-price').text(vmData.totalPrice);
    if(vmData.payPrice === vmData.totalPrice) {
      $payPrice.find('del').addClass('none');
    } else {
      $payPrice.find('del').removeClass('none');
    }

    //----------
    $form.find('input[name=points]').val(vmData.points);
    $form.find('.inviteUser .minusAmount .number')
      .text(vmData.inviteUserAmount ? (vmData.inviteUserAmount == 0 ? '0.00' : vmData.inviteUserAmount) : '0.00');
  }

  function requestPrice(isPointsChange = false) {
    $form.find(`div.formError`).text('');
    $form.find('.topay')
      .addClass('disabled')
      .prop('disabled', true);

    $.ajax({
      url: '/member/buy/calcPrice',
      type: 'post',
      data: {
        "productTyp": $form.find('input[name=productTyp]').val(),
        "productId": vmData.productId,
        "itemCode": vmData.itemCode,
        "count": $form.find('input[name=count]').val(),
        "points": $form.find('input[name=points]').val(),
        "isPointsChange": isPointsChange,
        "inviteUser": $form.find('select[name=inviteUser]').val()
      },
      success: function(res) {
        vmData.count = res.count;
        vmData.price = res.price;
        vmData.discountPrice = res.discountPrice;
        vmData.totalPrice = res.totalPrice;
        vmData.priceAfterSilver = res.priceAfterSilver;
        vmData.afterPromotionPrice = res.afterPromotionPrice;
        vmData.points = res.points;
        vmData.maxPoints = res.maxPoints;
        vmData.pointsAmount = res.pointsAmount;
        vmData.inviteUserAmount = res.inviteUserAmount;
        vmData.inviteUser = res.inviteUser;
        vmData.payPrice = res.payPrice;
        renderPrice();

        $form.find('.topay')
          .removeClass('disabled')
          .prop('disabled', false);
      },
      error: function(err) {
        handleError(err);
      }
    });
  }

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if(typeof json.error === 'string') {
          alert(json.error);
        } else if(res.status === 400) {
          for(let key in json.error) {
            $(`.${key}Error`).text(json.error[key][0]);
          }
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }

  $form.find('input[name=productId]').trigger('change');
});
