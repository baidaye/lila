$(function() {

  let $page = $('.patternsCheckmate1-map');

  $page.find('.resetProgress').click(function () {
    if(confirm('请注意: 您的进度将被清空！')) {
      $.ajax({
        method: 'post',
        url: '/patterns/checkmate1/resetProgress',
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          handleError(res);
        }
      });
    }
  });

});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
