$(function () {

  const A4height = 297;   // A4高度297mm
  const A4Width = 210;    // A4宽带210mm
  const A4Margin = 10;    // 默认边距10mm
  const a4PageHeight = Math.floor(mm2px(A4height));
  const a4PageWidth = Math.floor(mm2px(A4Width));
  const chromeMargin = Math.floor(mm2px(A4Margin));
  const marginPageHeight = a4PageHeight - chromeMargin * 2; // 去除边距后A4纸的内容高度
  const marginPageWidth = a4PageWidth - chromeMargin * 3 / 2;   // 去除边距后A4纸的内容高度

  $('head').append(`<style media="print">@page{size:21cm 29.7cm;background-color:#fff;margin:0;}</style>`);
  let $page = $('.game-print');
  let $setting = $page.find('.setting');
  let $printArea = $page.find('.game-print-area');

  $printArea.find('.page').innerWidth(marginPageWidth);
  $printArea.find('.page').innerHeight(a4PageHeight);
  $printArea.find('.page-header').css({'padding-top': `${chromeMargin}px`});
  $printArea.find('.page-footer').css({'height': `${chromeMargin}px`, 'line-height': `${chromeMargin}px`});

  $setting.find('input[type="checkbox"]').click(function (e) {
    let checked = e.target.checked;
    let tagType = $(this).data('id');
    let $tag = $printArea.find(`.toggle-${tagType}`);
    if(checked) {
      $tag.removeClass('none');
    } else {
      $tag.addClass('none');
    }

    // 设置
    if(tagType === 'inline') {
      if(checked) {
        $printArea.find('.turn-inline').removeClass('none');
        $printArea.find('.turn-tables').addClass('none');
        $printArea.find('.page-footer').addClass('none');
        $printArea.find('.page-footer-inline').removeClass('none');
      } else {
        $printArea.find('.turn-inline').addClass('none');
        $printArea.find('.turn-tables').removeClass('none');
        $printArea.find('.page-footer').removeClass('none');
        $printArea.find('.page-footer-inline').addClass('none');
      }
    }
  });

  $setting.find('input[type="text"]').on('input propertychange', function () {
    let val = $(this).val();
    let tagType = $(this).data('id');
    let $tag = $printArea.find(`.toggle-${tagType}`);
    if(tagType === 'title') {
      $tag.text(val);
    } else {
      $tag.find('td').eq(1).text(val);
    }
  });

  $('.do-print').click(function () {
    $printArea.print({
      copyBodyClass: true,
      delayPrint: true
    });
  });
});

function getDPI() {
  let dpi = 0;
  if (window.screen.deviceXDPI) {
    dpi = window.screen.deviceXDPI;
  } else {
    let tmpNode = document.createElement('div');
    tmpNode.style.cssText = 'width:1in;height:1in;position:absolute;left:0px;top:0px;z-index:99;visibility:hidden';
    document.body.appendChild(tmpNode);
    dpi = parseInt(tmpNode.offsetWidth);
    tmpNode.parentNode.removeChild(tmpNode);
  }
  return dpi;
}

// 1 英寸 = 25.4 毫米
function px2mm(value) {
  let inch = value / getDPI();
  return inch * 25.4;
}

function mm2px(value) {
  let inch = value / 25.4;
  return inch * getDPI();
}
