(function ($) {
    $.fn.drawer = function (options) {
        let settings = $.extend({
            side: 'left',
            width: ''
        }, options);

        let drawer = this;
        let $cw = drawer.find('.drawer-content-wrapper');
        let $mk = drawer.find('.drawer-mask');
        let $ft = drawer.find('.drawer-footer');
        let $tg = drawer.find('.drawer-toggle');
        let $hd = drawer.find('.drawer-hidden');
        let width = settings.width === '' ? drawerWidth() : settings.width;
        let translate = 'translateX(' + (settings.side === 'right' ? 100 : 100 * -1) + '%)';
        let rside = reverseSide();

        drawer.isOpen = !!$tg.data('drawer-open');
        drawer.addClass('drawer-' + settings.side);
        $ft.css('display', 'none');
        $cw.css({
            'width': width,
            'transform': translate
        });
        $tg.css(rside, "-1.2em");
        $tg.css('border-' + rside, $tg.css('border-bottom'));
        drawer.css('display', '');

        function toggleMenu(e) {
            e.preventDefault();
            e.stopPropagation();

            if (drawer.isOpen) {
                drawer.close();
            } else {
                drawer.open();
            }
            return false;
        }

        function reverseSide() {
            if (settings.side === 'left') {
                return 'right';
            } else {
                return 'left';
            }
        }

        function drawerWidth() {
            const windowWidth = $(window).width();
            if (windowWidth >= 1440) {
                return '25%';
            }
            if (windowWidth >= 1260) {
                return '30%';
            }
            if (windowWidth >= 1120 && windowWidth < 1260) {
                return '35%';
            }
            if (windowWidth >= 980 && windowWidth < 1260) {
                return '40%';
            }
            if (windowWidth >= 800 && windowWidth < 980) {
                return '45%';
            }
            if (windowWidth >= 500 && windowWidth < 980) {
                return '60%';
            }
            if (windowWidth < 500) {
                return '80%';
            }
        }

        drawer.open = function () {
            $cw.css('transform', '');
            drawer.addClass('drawer-open');
            $ft.css('display', '');
            drawer.isOpen = true;

            $mk.on('click touchend', toggleMenu);
        };

        drawer.close = function () {
            drawer.isOpen = false;

            $ft.css('display', 'none');
            $cw.css('transform', translate);
            drawer.removeClass('drawer-open');
            $mk.off('click touchend', toggleMenu);
        };

        $tg.on('click touchend', toggleMenu);
        $hd.on('click touchend', toggleMenu);
        return drawer;
    };
})(jQuery);
