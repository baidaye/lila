$(function() {

  let $page = $('.study-index');
  let $form = $page.find('.box__top-tags');

  let $emptyTag = $form.find('.tag-group.emptyTag').find('input[type="checkbox"]');
  let $tags = $form.find('.tag-group').find('input[type="checkbox"]');

  $tags.not($emptyTag).click(function () {
    $emptyTag.prop('checked', false);
    $form.submit();
  });

  $emptyTag.click(function () {
    $tags.not($emptyTag).prop('checked', false);
    $form.submit();
  });

});

