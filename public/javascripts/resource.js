$(function() {

  let $root = $(".infinitescroll");
  let $form = $(".search_form");

  registerEvent();
  lichess.pubsub.on('content_loaded', registerEvent);

  $('select.select').change(function() {
    $root.find('.paginated').removeClass('selected');
    switch ($(this).val()) {
      case 'all':
        $root.find('.paginated').addClass('selected');
        break;
    }
  });

  $('select.action').change(function() {
    let $this = $(this);
    let action = $this.val();
    if (!action) return;

    let ids = [];
    $root.find('.selected').each(function() {
      return ids.push($(this).data('id'));
    });

    if (ids.length === 0) {
      $this.val('');
      alert('至少选择1项');
      return;
    }

    if (action === 'delete') {
      if (confirm('删除 ' + ids.length + ' 个资源？')) {
        let url = $form.attr('action');
        url = url.substring(0, url.lastIndexOf("/")) + '/delete?ids=' + ids.join(',');
        $form.attr('action', url).attr('method', 'post');
        $form.submit();
      } else {
        $this.val('');
      }
    }

    else if (action === 'unlike') {
      if(confirm('取消收藏 ' + ids.length + ' 个资源？')) {
        let url = $form.attr('action');
        url = url.substring(0, url.lastIndexOf("/")) + '/unlike?ids=' + ids.join(',');
        $form.attr('action', url).attr('method', 'post');
        $form.submit();
      } else {
        $this.val('');
      }
    }

    else if (action === 'likeInterest') {
      if(confirm('收藏 ' + ids.length + ' 个题目？')) {
        let url = $form.data('likehref');
        url = url + '?ids=' + ids.join(',');
        $.post(url).then(() => {
          alert('收藏 ' + ids.length + ' 个题目成功');
          location.reload();
        });
      } else {
        $this.val('');
      }
    }

    else if (action === 'unlikeInterest') {
      if(confirm('取消收藏 ' + ids.length + ' 个题目？')) {
        let url = $form.data('unlikehref');
        url = url + '?ids=' + ids.join(',');
        $form.attr('action', url).attr('method', 'post');
        $form.submit();
      } else {
        $this.val('');
      }
    }

    else if (action === 'copyTo') {
      let source = $form.find('input[name="gameSource"]').val();
      showCopyOrMoveTo(source, 'copy', ids);
    }

    else if (action === 'moveTo') {
      let source = $form.find('input[name="gameSource"]').val();
      showCopyOrMoveTo(source, 'move', ids);
    }

    else if (action === 'toCapsule') {
      showCapsule();
    }

    else if (action === 'toPrint') {
      let $selected = $root.find('.selected');
      let gameId = $selected.data('gameid');
      let title = $selected.find('.rel .title').text().trim();
      if(!gameId) gameId = ids[0];
      window.open(`/${gameId}/print${title ? `?defaultTitle=${title}` : ''}`);
    }

    else if (action === 'toStudy') {
      let $tmpForm = $('#tmp-form');
      let gameId = $root.find('.selected').data('gameid');
      if(!gameId) gameId = ids[0];

      $tmpForm.find('input[name="gameId"]').val(gameId);
      $tmpForm.attr('action', '/study/as');
      $tmpForm.submit();
    }

    else if (action === 'toRecall') {
      let gameId = $root.find('.selected').data('gameid');
      if(!gameId) gameId = ids[0];
      window.open(`/recall?create=true&tab=game&gameId=${gameId}`);
    }

    else if (action === 'toDistinguish') {
      let gameId = $root.find('.selected').data('gameid');
      if(!gameId) gameId = ids[0];
      window.open(`/distinguish?create=true&tab=game&gameId=${gameId}`);
    }

    else if (action === 'gamedbToRecall') {
      let relId = $root.find('.selected').data('id');
      let gameId = $root.find('.selected').data('gameid');
      window.open(`/recall?create=true&tab=gamedb&gameId=${relId}:${gameId}`);
    }

    else if (action === 'gamedbToDistinguish') {
      let relId = $root.find('.selected').data('id');
      let gameId = $root.find('.selected').data('gameid');
      window.open(`/distinguish?create=true&tab=gamedb&gameId=${relId}:${gameId}`);
    }

    else if (action === 'ascSort') {
      sortCapsule(1, ids.join(','));
    }

    else if (action === 'descSort') {
      sortCapsule(-1, ids.join(','));
    }

    setTimeout(function () {
      $this.val('');
    }, 1000);
  });

  let $emptyTag = $form.filter('.imported_form, .liked_form, .gamedb_form, .situation_form, .shared_form')
      .find('.tag-group.emptyTag')
      .find('input[type="checkbox"]');

  let $tags = $form.filter('.imported_form, .liked_form, .gamedb_form, .situation_form, .shared_form')
      .find('.tag-group')
      .find('input[type="checkbox"]');

  $tags.not($emptyTag).click(function () {
    $emptyTag.prop('checked', false);
    $form.submit();
  });

  $emptyTag.click(function () {
    $tags.not($emptyTag).prop('checked', false);
    $form.submit();
  });

  $form.find('.tag-group.single input[type="checkbox"]').click(function () {
    let $this = $(this);
    let $cks = $this.parents('.single').find('input:checked').not(this);
    if($cks.length > 0) {
      $cks.prop('checked', false);
    }
  });

  $form.find("#form3-order").change(function () {
    $form.submit()
  });

  function sortCapsule(sort, ids) {
    let url = $form.attr('action');
    url = url.substring(0, url.lastIndexOf("/")) + '/sort?puzzleIds=' + ids;
    $.ajax({
      method: 'POST',
      url: url,
      data: {
        sort: sort
      },
      success: function() {
        $.modal.close();
        location.reload();
      },
      error: function(res) {
        alert(res.responseText);
      }
    });
  }

  function loadCount(tab) {
    let url = `/resource/puzzle/${tab}Count`+ location.search;
    $.ajax({
      url: url,
      success: function(res) {
        $form.find('.countWaiting').addClass('none');
        $form.find('.count').text(res.count).removeClass('none');
      },
      error: function(res) {
        alert(res.responseText);
      }
    });
  }

  let active = $form.data('id');
  if(active) {
    loadCount(active);
  }

});

function showCapsule() {
  let ids = [];
  $(".infinitescroll").find('.selected').each(function() {
    return ids.push($(this).attr('data-id'));
  });
  if(ids.length === 0) {
    alert('至少选择1项');
    return;
  }

  $.ajax({
    url: '/resource/capsule/mine',
    success: function(html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $('select.action').val('');
        $.modal.close();
      });

      let $md = $('.modal-capsule');
      $md.find('.tabs-horiz span').click(function () {
        let $this = $(this);
        $md.find('.tabs-horiz span').removeClass("active");
        $md.find('.tabs-content div').removeClass("active");

        let cls = $this.attr('class');
        $this.addClass('active');
        $md.find('.tabs-content div.' + cls).addClass('active');
      });

      $('.modal-capsule form').submit(function(e) {
        e.preventDefault();
        let $form = $(this);
        let $tabs = $form.find('.tabs-horiz');
        let active = $tabs.find('.active').data('id');

        let url = `/resource/capsule/puzzle/createAndAdd?puzzleIds=` + ids.join(',');
        if(active === 'mine' || active === 'member') {
          let capsuleId = $form.find('input[name="capsule"]:checked').val();
          if(!capsuleId) return false;
          url = `/resource/capsule/${capsuleId}/puzzle/add?puzzleIds=` + ids.join(',');
        }

        $.ajax({
          method: 'POST',
          url: url,
          data: {
            name: $form.find('input[name="capsuleName"]').val()
          },
          success: function(response) {
            $.modal.close();
            location.reload();
          },
          error: function(res) {
            if (res.responseText.indexOf('名称已经存在') > 0) {
              $form.find('input[name="capsuleName"]').parent().append('<p class="error">名称已经存在</p>');
            } else {
              alert(res.responseText);
            }
          }
        });
        return false;
      });
    },
    error: function(res) {
      alert(res.responseText);
    }
  });
}

function showCopyOrMoveTo(source, action, ids) {
  let gamedbId = $('input[name="selected"]').val();
  $.ajax({
    url: `/resource/gamedb/rel/copyOrMove?source=${source}&action=${action}&frGamedb=${gamedbId}&ids=${ids.join(',')}`,
    success: function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $('select.action').val('');
        $.modal.close();
      });

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.modal-content').find('form');
          let $tree = $form.find('.cp-dbtree');
          $tree.jstree({
            'types': {
              '#': {
                'max_depth': 10
              },
              'default': {
                'max_children': 100
              }
            },
            'core': {
              'strings': {
                'New node': '新建文件夹'
              },
              'worker': false,
              'data': {
                'url': '/resource/gamedb/tree/load'
              },
              'check_callback': true
            },
            'plugins': ['types', 'unique', 'contextmenu'],
            'contextmenu': {
              'select_node': false,
              'items': function (node) {
                return contextMenu(node);
              }
            }
          }).on('create_node.jstree', function (e, data) {
              createNode(data);
            })
            .on('rename_node.jstree', function (e, data) {
              renameNode(data);
            })
            .on('delete_node.jstree', function (e, data) {
              deleteNode(data);
            })
          .on('changed.jstree', function (e, data) {
            if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
              $form.find('input[name="toGamedb"]').val(data.node.id)
            }
          });

          function contextMenu(node) {
            let defaultContextmenu = $jq.jstree.defaults.contextmenu.items();
            defaultContextmenu.create.label = '新建目录';
            defaultContextmenu.create.icon = 'icon-create';
            defaultContextmenu.rename.label = '修改目录';
            defaultContextmenu.rename.icon = 'icon-rename';
            defaultContextmenu.remove.label = '删除目录';
            defaultContextmenu.remove.icon = 'icon-remove';

            delete defaultContextmenu.ccp;
            if (node.parent === '#') {
              delete defaultContextmenu.rename;
              delete defaultContextmenu.remove;
            }
            return defaultContextmenu;
          }

          function createNode(data) {
            $jq.post('/resource/gamedb/tree/create', {
              'parent': data.node.parent,
              'name': data.node.text,
              'sort': (data.position + 1)
            })
              .done(function (d) {
                data.instance.set_id(data.node, d.id);
              })
              .fail(function () {
                data.instance.refresh();
              });
          }

          function renameNode(data) {
            if (data.text === data.old || data.node.id.length !== 8) {
              return false;
            }
            $jq.post(`/resource/gamedb/tree/${data.node.id}/rename`, {'name': data.text})
              .done(function (d) {
                data.instance.refresh();
              })
              .fail(function () {
                data.instance.refresh();
              });
          }

          function deleteNode(data) {
            if (confirm('删除目录后所有子目录和对局将一并删除，是否确认操作？')) {
              $jq.post(`/resource/gamedb/tree/${data.node.id}/remove`)
                .fail(function () {
                  data.instance.refresh();
                });
            } else {
              data.instance.refresh();
            }
          }

          $form.submit(function (e) {
            e.preventDefault();
            $.ajax({
              method: 'POST',
              url: '/resource/gamedb/rel/copyOrMove',
              data: $form.serialize(),
              success: function () {
                $.modal.close();
                location.reload();
              },
              error: function (res) {
                handleError(res);
              }
            });
            return false;
          });
        });
      });
    },
    error: function (res) {
      handleError(res);
    }
  });
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}

function registerEvent() {
  let $board = $(".infinitescroll").find('.paginated');
  $board.off('click');
  $board.off('dblclick');
  $board.click(function(e) {
    e.preventDefault();
    $(this).toggleClass('selected');
    return false;
  });
  $board.dblclick(function(e) {
    e.preventDefault();
    window.open($(this).attr("data-href"));
    return false;
  });
}

