$(function() {

    lichess.pubsub.on('content_loaded', modalClick);

    modalClick();
    function modalClick() {
        $('a.member-rating').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('href'),
                success: function(html) {
                    $.modal($(html));
                    $('.cancel').click(function () {
                        $.modal.close();
                    });
                },
                error: function(res) {
                    alert(res.responseText);
                }
            });
            return false;
        });
    }

    $('.rating-export').click(function () {
        let $listArea = $('.list_area');
        let $table = $listArea.find('.slist');
        let $thead = $table.find('thead');
        let $tbody = $table.find('tbody');

        let data = [];

        // 标题
        let title = $('.distribution_action h1').text();

        // 表头
        let head = $thead.find('tr th').map(function (_, th) {
            return th.innerText;
        }).toArray();
        data.push(head.slice(0, head.length - 1));

        // 内容
        let body = $tbody.find('tr').map(function (_, tr) {
            let arr = $(tr).find('td').map(function (_, td) {
                return td.innerText.replace('\n', ' ').replace('\n', ' ');
            }).toArray();
            return [arr];
        }).toArray();

        body = body.map(function (arr) {
            return arr.slice(0, arr.length - 1);
        });
        data = data.concat(body);

        let csvContent = "data:text/csv;charset=utf-8,\uFEFF"
          + data.map(e => e.join(',')).join('\n');
        let encodedUri = encodeURI(csvContent);
        let link = document.createElement("a");
        link.setAttribute('href', encodedUri);
        link.setAttribute('download', `${title}.csv`);
        document.body.appendChild(link);
        link.click();
    });

});
