$(function () {

  let $form = $('.test-create-form');

  $form.find('.tabs-horiz span').click(function () {
    let $this = $(this);
    $form.find('.tabs-horiz span').removeClass("active");
    $form.find('.tabs-content div').removeClass("active");

    let active = $this.data('tab');
    $this.addClass('active');
    $form.find('.tabs-content div.' + active).addClass('active');
  });

  $form.find('a.modal-alert').click(function(e) {
    e.preventDefault();

    $.ajax({
      url: $(this).attr('href'),
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        registerCapsules();
      },
      error: function(res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  let $capsules = $form.find('.capsules');
  remove();

  function remove() {
    $capsules.find('tbody tr').each(function () {
      let $tr = $(this);
      $tr.find('.remove').click(function () {
        $tr.remove();
        resetName();
        calcTotal();
        resetHref();
      });
    });
  }

  function resetName() {
    $capsules.find('tbody tr').each(function (i) {
      let $tr = $(this);
      $tr.find('input').each(function() {
        let name = $(this).attr('name');
        $(this).attr('name', name.replace(/items.capsules\[\d+\]/g, 'items.capsules[' + i + ']'));
      });
    });
  }

  function calcTotal() {
    let all = [];
    $capsules.find('tbody tr').each(function (i) {
      let puzzles = $(this).find('td.total').data('puzzles');
      all = all.concat(puzzles);
    });
    const set = new Set(all);
    $capsules.find('.item-header .count').text(set.size);
  }

  function resetHref() {
    let capsuleIds = [];
    $capsules.find('tbody tr').each(function (i) {
      let $tr = $(this);
      $tr.find('input').each(function() {
        capsuleIds.push($(this).val());
      });
    });
    $capsules.find('.add-capsule').attr('href', `/resource/capsule/mineOfChoose?ids=${capsuleIds.join(',')}`);
    $capsules.find('input[name="capsuleIds"]').val(capsuleIds.join(','));
  }

  function registerCapsules() {
    let $modal = $('.modal-capsule');

    $modal.find('.tabs-horiz span').click(function () {
      let $this = $(this);
      $modal.find('.tabs-horiz span').removeClass("active");
      $modal.find('.tabs-content div').removeClass("active");

      let cls = $this.data('tab');
      $this.addClass('active');
      $modal.find('.tabs-content div.' + cls).addClass('active');
    });

    $modal.find('.form3').submit(function (e) {
      e.preventDefault();
      let capsuleIds = [];
      $modal.find('input[name="capsuleRd"]:checked').each(function () {
        capsuleIds.push($(this).val());
      });

      if(capsuleIds.length === 0) {
        $.modal.close();
        return false;
      }

      if(capsuleIds.length > 50) {
        alert('最多添加50个战术题列表');
        return false;
      }

      $.get(`/resource/capsule/puzzle/statistics?ids=${capsuleIds.join(',')}`, list => {
        if(list) {
          let $tb = $capsules.find('tbody');
          $tb.empty();
          list.forEach((item) => {
            let tr = trHtml(item);
            let $tr = $(tr);
            $tb.append($tr);
          });
          remove();
          resetName();
          calcTotal();
          resetHref();
        }

        $.modal.close();
      });
      return false;
    });

    function trHtml(capsule) {
      let html = `
          <tr>
              <td>
                  <a target="_blank" class="capsuleName" href="/resource/capsule/${capsule.id}/puzzle/list">${capsule.name}</a>
                  <input type="hidden" name="items.capsules[0]" value="${capsule.id}">
              </td>
              <td class="total" data-puzzles="[${capsule.statistics.puzzles}]">${orNA(capsule.statistics.total)}</td>
              <td>${orNA(capsule.statistics.avgRating)}</td>
              <td>${orNA(capsule.statistics.minRating)}</td>
              <td>${orNA(capsule.statistics.maxRating)}</td>
              <td><a class="button button-empty small remove" title="移除后点击“保存”生效">移除</a></td>
          </tr>
        `;
      return html;
    }

    function orNA(v) {
      return v === 0 ? 'NA' : v;
    }

    function filter($tab) {
      let $filter = $tab.find('.capsule-filter');
      let name = $filter.find('.capsule-filter-search').val();
      let tags = [];
      $filter.find('.capsule-filter-tag').find('input:checked').each(function() {
        tags.push(this.value);
      });

      let $lst = $tab.find('.capsule-list');
      let arr = [];
      $lst.find('tr').each(function () {
        arr.push($(this).data('attr'));
      });

      let filterIds = arr.filter(function (n) {
        return (!name || n.name.indexOf(name) > -1) && (tags.length === 0 || tags.filter(t => n.tags.includes(t)).length > 0)
      }).map(n => n.id);

      $lst.find('tr').addClass('none');
      filterIds.forEach(function (id) {
        $lst.find('#rd_' + id).parents('tr').removeClass('none');
      });
    }

    $modal.find(' .capsule-filter-search').on('input propertychange', function() {
      let $tab = $(this).parents('.tabs-contentx');
      filter($tab);
    });

    $modal.find('.capsule-filter-tag input').on('click', function() {
      let $tab = $(this).parents('.tabs-contentx');
      filter($tab);
    });
  }

});

