$(function () {
    dateFormat();
    applyTab();
    applyModal();
    applyPlayerSort();
    applyPairing();
    applyPublishPairing();
    applySetRounds();
    applyRoundRobinPairing();
    applyPrint();

    $(window).scrollTop($.cookie('haichess-offContest-scrollTop'));
    $(window).scroll(function() {
        $.cookie('haichess-offContest-scrollTop', document.documentElement.scrollTop);
    });
});

function applyTab() {
    let $page = $('.contest-show');
    let $flow = $page.find('.flow');
    let $tabNav = $flow.find('.tab-nav');
    let $tabPanel = $flow.find('.tab-panel');
    let $navs = $tabNav.find('.nav');
    let $panels = $tabPanel.find('.panel');
    $navs.not('.disabled').click(function () {
        let $this = $(this);
        let active = $this.data('tab');
        $navs.removeClass('active');
        $this.addClass('active');

        $panels.removeClass('active');
        $panels.filter(`.${active}`).addClass('active');
        location.hash = active;
    });

    setTabActive();
    function setTabActive() {
        let hash = location.hash;
        if(!$navs.has(`[data-tab="${hash}"]`).hasClass('disabled')) {
            if(hash) {
                hash = hash.replace('#', '');
                $navs.removeClass('active');
                $navs.filter(`[data-tab="${hash}"]`).addClass('active');

                $panels.removeClass('active');
                $panels.filter(`.${hash}`).addClass('active');
            }
        }
    }

    setHScroll();
    function setHScroll() {
        let hash = location.hash;
        let $navScroll = $tabNav.find('.nav-scroll');
        let $navScrollHorizontal = $tabNav.find('.nav-scroll-horizontal');
        let $navScrollBars = $navScroll.find('.nav-bars');
        let navWidth = $navScrollHorizontal.width();
        let barsWidth = $navScrollBars.width();

        let minWidth = 0;
        let maxWidth = Math.floor(barsWidth - navWidth);
        let tick = 95;
        let page = Math.max(Math.floor(navWidth / tick), 1) * tick;
        if(maxWidth > 0) {
            let defaultTranslateX = 0;
            let currentRound = $page.data('currentround');
            if(hash && hash.indexOf('round') > -1 && hash.indexOf('roundEdit') === -1) {
                let roundNo = parseInt(hash.replace('#round', ''));
                defaultTranslateX = Math.max(maxWidth * -1, (roundNo - 1) * tick * -1);
            } else if(currentRound) {
                let roundNo = parseInt(currentRound);
                defaultTranslateX = Math.max(maxWidth * -1, (roundNo - 1) * tick * -1);
            }
            $navScrollBars.css('transform', `translateX(${defaultTranslateX}px)`);

            $navScroll.find('.nav-scroll-prev').click(function () {
                let nowTransform = $navScrollBars[0].style.transform;
                let nb = parseInt(nowTransform.match(/^translateX\((-?\d+)px\)$/)[1]);
                let newNb = Math.min(minWidth, nb + page);
                $navScrollBars.css('transform', `translateX(${newNb}px)`);
            });

            $navScroll.find('.nav-scroll-next').click(function () {
                let nowTransform = $navScrollBars[0].style.transform;
                let nb = parseInt(nowTransform.match(/^translateX\((-?\d+)px\)$/)[1]);
                let newNb = Math.max(maxWidth * -1, nb - page);
                $navScrollBars.css('transform', `translateX(${newNb}px)`);
            });
        } else {
            $navScroll.find('.nav-scroll-prev').addClass('none');
            $navScroll.find('.nav-scroll-next').addClass('none');
        }
    }
}

function applyModal() {
    $('a.modal-alert').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            success: function(html) {
                $.modal($(html));
                $('.cancel').click(function () {
                    $.modal.close();
                });

                playerChooseModal();
                playerExternalModal();
                playerForbiddenModal();
                roundSwapModal();
                manualPairingModal();
                manualAbsentModal();
            },
            error: function(res) {
                handleError(res);
            }
        });
        return false;
    });
}

function playerChooseModal() {
    let $modal = $('.modal-player-choose');
    let $transfer = $modal.find('.transfer');
    let $leftPanel = $transfer.find('.left');
    let $leftList = $leftPanel.find('.transfer-panel-list table tbody');
    let oldPlayerIds = $modal.find('input[name=players]').val();
    let checkedPlayerIds = oldPlayerIds ? oldPlayerIds.split(',') : [];

    let onChange = (checkeds) => {
        checkedPlayerIds = checkeds;
    };

    search();
    memberAdvance();
    transfer(onChange);

    function search() {
        let $searchForm = $modal.find('.member-search');

        $searchForm.find('#member_name').keydown((e) => {
            if(e.keyCode === 13){
                e.preventDefault();
                $searchForm.find('.search').trigger('click');
            }
        });

        $searchForm.submit(function(e) {
            e.preventDefault();

            let username = $searchForm.find('#form3-username').val();
            let campus = $searchForm.find('#form3-campus').val();
            let clazzId = $searchForm.find('#form3-clazzId').val();
            let teamRatingMin = $searchForm.find('#form3-teamRatingMin').val();
            let teamRatingMax = $searchForm.find('#form3-teamRatingMax').val();
            let level = $searchForm.find('#form3-level').val();
            let sex = $searchForm.find('#form3-sex').val();
            let singleTags = $searchForm.data('single-tags');
            let rangeTags = $searchForm.data('range-tags');
            if(!singleTags) {
                singleTags = [];
            }
            if(!rangeTags) {
                rangeTags = [];
            }

            let arr = [];
            $leftList.find('input').each(function () {
                arr.push($(this).data('attr'));
            });

            let filterSingleTag = (n) => {
                return singleTags.reduce((prev, field, index) => {
                    let tagValue = $searchForm.find(`#form3-fields_${index}_fieldValue`).val();
                    return prev && (!tagValue || (n[field] && n[field].includes(tagValue)));
                }, true);
            };

            let filterRangeTag = (n) => {
                return rangeTags.reduce((prev, field, index) => {
                    let tagValueMin = $searchForm.find(`#form3-rangeFields_${index}_min`).val();
                    let tagValueMax = $searchForm.find(`#form3-rangeFields_${index}_max`).val();
                    return prev && (!tagValueMin || (n[field] && n[field] >= tagValueMin)) && (!tagValueMax || (n[field] && n[field] <= tagValueMax));
                }, true);
            };

            let filterIds = arr.filter(n => {
                return (
                  (!username || n.name.includes(username)) &&
                  (!campus || (n.campus && n.campus.includes(campus))) &&
                  (!clazzId || (n.clazz && n.clazz.includes(clazzId))) &&
                  (!teamRatingMin || n.teamRating >= teamRatingMin) &&
                  (!teamRatingMax || n.teamRating <= teamRatingMax) &&
                  (!level || n.level === level) &&
                  (!sex || n.sex === sex) &&
                  filterSingleTag(n) &&
                  filterRangeTag(n)
                )
            }).map(n => n.id);

            $leftList.find('tr').addClass('none');
            filterIds.forEach(function (id) {
                $leftList.find('#chk_' + id).parents('tr').removeClass('none');
            });

            return false;
        });
    }

    let $transferForm = $modal.find('.player-choose-transfer');
    $transferForm.submit(function(e) {
        e.preventDefault();
        let url = $(this).attr('action');
        $.ajax({
            method: 'POST',
            url: url,
            data: { "players": checkedPlayerIds.join(',') },
            success: function() {
                location.reload();
            },
            error: function(res) {
                handleError(res);
            }
        });
    })
}

function playerExternalModal() {
    let $form = $('.player-external').find('form')
    $form.submit(function(e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: $form.serialize(),
            success: function() {
                location.reload();
            },
            error: function(res) {
                handleError(res);
                location.reload();
            }
        });
        return false;
    });
}

function manualAbsentModal() {
    let $modal = $('.contest-absent');
    let onChange = (rights, lefts) => {
        $modal.find('input[name=joins]').val(lefts.join(','));
        $modal.find('input[name=absents]').val(rights.join(','));
    };
    transfer(onChange);
}

function playerForbiddenModal() {
    let $modal = $('.contest-forbidden');
    let $form = $modal.find('form');

    let onChange = (checkeds) => {
        $modal.find('input[name=playerIds]').val(checkeds.join(','));
    };
    transfer(onChange);

    $form.submit(function(e) {
        e.preventDefault();
        let players = $modal.find('input[name=playerIds]').val();
        if(!players) {
            alert('请选择回避棋手');
            return false;
        }
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: $form.serialize(),
            success: function() {
                location.reload();
            },
            error: function(res) {
                handleError(res);
            }
        });
        return false;
    });
}

function roundSwapModal() {
    let $modal = $('.contest-swap');
    let $form = $modal.find('form');
    let $left = $form.find('.contest-swap-rounds-left');
    let $right = $form.find('.contest-swap-rounds-right');
    $left.find('input[name="contest-swap-rd"]').change(function () {
        let roundId = $(this).val();
        $right.find('.contest-swap-round').addClass('none');
        $right.find(`div[data-id="${roundId}"]`).removeClass('none');

        $left.find('.radio').removeClass('active');
        $(this).parent('.radio').addClass('active');
    });

    $form.submit(function(e) {
        e.preventDefault();
        let roundId = $form.find('input[name="contest-swap-rd"]:checked').val();
        if(!roundId) {
            alert('请选择要选择的轮次');
            return;
        }

        let url = $(this).attr('action');
        $.ajax({
            method: 'POST',
            url: url,
            data: { 'roundId': roundId },
            success: function() {
                location.reload();
            },
            error: function(res) {
                handleError(res);
            }
        });
        return false;
    });
}

function manualPairingModal() {
    let $mp = $('.manual-pairing');
    let $lst = $mp.find('.manual-list .slist');
    $mp.find('.manual-filter-search').on('input propertychange', function() {
        let txt = $(this).val();
        if($.trim(txt) !== ''){
            $lst.find('tbody tr').not('tr:contains("' + txt + '")').css('display', 'none');
            $lst.find('tbody tr').filter('tr:contains("' + txt + '")').css('display', 'table-row');
        } else {
            $lst.find('tbody tr').css('display', 'table-row');
        }
    });

    $mp.find('.form3').submit(function(e) {
        e.preventDefault();
        let t = $lst.find('input:checked').val();
        if(!t) {
            alert('请选择交换棋手');
            return false;
        }

        let source = JSON.parse($mp.find('input[name="source"]').val());
        let target = JSON.parse(t);
        let url = $(this).attr('action');
        $.ajax({
            method: 'POST',
            url: url,
            data: { 'source.isBye': source.isBye, 'source.board': source.board, 'source.color': source.color, 'source.player': source.player,
                'target.isBye': target.isBye, 'target.board': target.board, 'target.color': target.color, 'target.player': target.player },
            success: function() {
                location.reload();
            },
            error: function(res) {
                handleError(res);
            }
        });
        return false;
    });
}

function applyPlayerSort() {
    let $page = $('.contest-show');
    let $enterTable =  $page.find('.enter .slist').not('.unsortable');
    let $waiting = $page.find('.waiting');
    $enterTable.tableDnD({
        onDragClass: 'dragClass',
        onDrop: function(t, r) {
            let playerIds = [];
            $enterTable.find('tbody tr').each(function() {
                playerIds.push($(this).data('id'));
            });
            let contestId = $('main').data('id');

            $waiting.removeClass('none');
            $.ajax({
                method: 'POST',
                url: `/contest/off/${contestId}/reorderPlayer`,
                data: {'playerIds': playerIds.join(',')},
                success: function() {
                    setTimeout(function () {
                        $enterTable.find('tbody tr').each(function(i) {
                            $(this).find('td').first().text(i + 1);
                        });
                        $waiting.addClass('none');
                    }, 500);
                },
                error: function(res) {
                    handleError(res);
                }
            });
        }
    });
}

function applyPairing() {

    $('.pairing').parent('form').submit(function(e) {
        e.preventDefault();
        if (confirm('确认生成对战表？')) {
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                success: function() {
                    location.reload();
                },
                error: function(res) {
                    handleError(res);
                    location.reload();
                }
            });
        }
        return false;
    });
}

function applyPublishPairing() {

    $('.publish-result').parent('form').submit(function(e) {
        e.preventDefault();
        if (confirm('确认发布成绩？')) {
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                success: function() {
                    location.reload();
                },
                error: function(res) {
                    handleError(res);
                    location.reload();
                }
            });
        }
        return false;
    });

}

function applySetRounds() {
    let $page = $('.contest-show');
    let $roundEdit = $page.find('.roundEdit');
    let $roundsForm = $roundEdit.find('.form3');
    let $waiting = $roundEdit.find('.waiting');

    function validRoundNum(nbPlayers) {
        let $baseInfo = $page.find('.head__info');
        let rule = $baseInfo.find('input[name="rule"]').val();
        let rounds = $roundsForm.find('input[name="rounds"]').val();

        let robinRoundsByPlayer = () => {
            if (nbPlayers === 0) {
                return rounds;
            } else {
                if (nbPlayers % 2 === 0) {
                    return nbPlayers - 1;
                } else
                    return nbPlayers;
            }
        };

        let robinRoundsByPlayerOfRule = () => {
            if(rule === 'round-robin') {
                return robinRoundsByPlayer();
            } else if(rule === 'db-round-robin') {
                return robinRoundsByPlayer() * 2;
            } else {
                return -1;
            }
        };

        let r = robinRoundsByPlayerOfRule();
        if(r !== parseInt(rounds)) {
            $roundsForm.find('.roundNumError').removeClass('none');
        } else {
            $roundsForm.find('.roundNumError').addClass('none');
        }
    }

    $roundsForm.find('input[name="rounds"]').change(function () {
        $roundsForm.submit();
    });

    $roundsForm.submit(function(e) {
        e.preventDefault();
        let $this = $(this);
        $waiting.removeClass('none');
        $roundsForm.find('.otherError').text('').addClass('none');
        $roundsForm.find('input[name="rounds"]').next('.error').text('').addClass('none');
        $.ajax({
            method: 'POST',
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function(response) {
                validRoundNum(response.nbPlayers);
                setTimeout(() => {
                    $waiting.addClass('none');
                }, 500);
            },
            error: function(res) {
                if(res.status === 400) {
                    let errors = res.responseJSON;
                    if(errors.global) {
                        $roundsForm.find('.otherError').text(errors.global).removeClass('none');
                    } else if(errors.rounds) {
                        $roundsForm.find('input[name="rounds"]').next('.error').text(errors.rounds).removeClass('none');
                    } else handleError(res);
                } else handleError(res);
                setTimeout(() => {
                    $waiting.addClass('none');
                }, 500);
            }
        });
        return false;
    });
}

function applyRoundRobinPairing() {
    let $page = $('.contest-show');
    let $roundEdit = $page.find('.roundEdit');
    let $roundsForm = $roundEdit.find('.form3');
    let $roundErrors = $roundsForm.find('.global-error li');
    let $btn = $roundEdit.find('.pairingRoundRobin');

    $btn.not('.disabled').click(function (e) {
        e.preventDefault();
        let errors = [];
        $roundErrors.each(function () {
            let $this = $(this);
            if(!$this.hasClass('none')) {
                errors.push($this.text());
            }
        });

        if(errors.length) {
            alert(errors.join('；'));
            return;
        }

        if(confirm('循环赛将对所有轮次进行编排且仅能进行一次，是否继续操作？')) {
            $btn.text('正在编排, 请稍后...').addClass('disabled');
            let url = $(this).attr('href');
            $.ajax({
                method: 'POST',
                url: url,
                success: function() {
                    location.reload();
                },
                error: function(res) {
                    handleError(res);
                }
            });
        }
    });
}

function applyPrint() {

    //$('head').append('<style media="print">@page{size:auto; margin:50px 100px;}</style>');
    $('.contest-show').find('.head-line').addClass('no-print');

    $('.print-players').click(function () {
        let $printArea = $(this).parents('.enter').find('.print-area');
        $printArea.find('.printTitle').removeClass('none');
        $printArea.find('.slist').addClass('print-list');
        setTimeout(function() {
            $printArea.find('.printTitle').addClass('none');
            $printArea.find('.slist').removeClass('print-list');
        }, 1000);
        $printArea.print();
    });

    $('.print-board-sheet').click(function () {
        let $printArea = $('.print-area.round' + $(this).data('id'));
        $printArea.find('.printTitle .cName .subName').text('对战表');
        $printArea.find('.printTitle').removeClass('none');
        $printArea.find('.slist').addClass('print-list');
        $printArea.find('.slist .result b').addClass('no-print');
        setTimeout(function() {
            $printArea.find('.printTitle').addClass('none');
            $printArea.find('.slist').removeClass('print-list');
            $printArea.find('.slist .result b').removeClass('no-print');
        }, 1000);
        $printArea.print();
    });

    $('.print-round-score').click(function () {
        let $printArea = $('.print-area.round' + $(this).data('id'));
        $printArea.find('.printTitle .cName .subName').text('轮次成绩');
        $printArea.find('.printTitle').removeClass('none');
        $printArea.find('.slist').addClass('print-list');
        setTimeout(function() {
            $printArea.find('.printTitle').addClass('none');
            $printArea.find('.slist').removeClass('print-list');
        }, 1000);
        $printArea.print();
    });

    $('.print-score').click(function () {
        let $printArea = $(this).parents('.score').find('.print-area');
        $printArea.find('.printTitle').removeClass('none');
        $printArea.find('.slist').addClass('print-list');
        setTimeout(function() {
            $printArea.find('.printTitle').addClass('none');
            $printArea.find('.slist').removeClass('print-list');
        }, 1000);
        $printArea.print();
    });

    $('.export-score').click(function () {
        let $score = $(this).parents('.score');
        let $title = $score.find('.printTitle');
        let $table = $score.find('table');
        let $thead = $table.find('thead');
        let $tbody = $table.find('tbody');

        let data = [];

        // 标题
        let title = $title.text();

        // 表头
        let head = $thead.find('tr th').map(function (_, th) {
            return th.innerText;
        }).toArray();
        data.push(head);

        // 内容
        let body = $tbody.find('tr').map(function (_, tr) {
            let arr = $(tr).find('td').map(function (_, td) {
                return td.innerText.replace('\n', ' ').replace('\n', ' ');
            }).toArray();
            return [arr];
        }).toArray();

        data = data.concat(body);

        let csvContent = "data:text/csv;charset=utf-8,\uFEFF"
          + data.map(e => e.join(',')).join('\n');
        let encodedUri = encodeURI(csvContent);
        let link = document.createElement("a");
        link.setAttribute('href', encodedUri);
        link.setAttribute('download', `${title}.csv`);
        document.body.appendChild(link);
        link.click();
    });
}

function dateFormat() {
    Date.prototype.format = function (fmt) {
        let o = {
            "M+": this.getMonth() + 1,                 //月份
            "d+": this.getDate(),                    //日
            "h+": this.getHours(),                   //小时
            "m+": this.getMinutes(),                 //分
            "s+": this.getSeconds(),                 //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds()             //毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };
}

function handleError(res) {
    let json = res.responseJSON;
    if (json) {
        if (json.error) {
            if(typeof json.error === 'string') {
                alert(json.error);
            } else alert(JSON.stringify(json.error));
        } else alert(res.responseText);
    } else alert('发生错误');
}
