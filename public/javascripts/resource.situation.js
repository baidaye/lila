let $jq = $.noConflict(true);
$jq(function () {

  if($jq('main').data('notaccept') == true) {
    window.lichess.memberIntro();
  }

  let $tree = $jq('.dbtree');
  $tree.jstree({
    'types': {
      '#': {
        'max_depth': 10
      },
      'default': {
        'max_children': 100
      }
    },
    'core': {
      'strings': {
        'New node': '新建文件夹'
      },
      'worker': false,
      'data': {
        'url': '/resource/situationdb/tree/load',
        'data': function (node) {
          return {selected: $jq('input[name="selected"]').val()};
        }
      },
      'check_callback': function (operation, node, node_parent, node_position, more) {
        if(more && more.dnd && node_parent.id === '#') {
          return false;
        }
        return true;
      },
    },
    'plugins': ['types', 'unique', 'dnd', 'contextmenu'],
    'contextmenu': {
      'select_node': false,
      'items': function (node) {
        return contextMenu(node);
      }
    }
  }).on('create_node.jstree', function (e, data) {
      createNode(data);
    })
    .on('rename_node.jstree', function (e, data) {
      renameNode(data);
    })
    .on('delete_node.jstree', function (e, data) {
      deleteNode(data);
    })
    .on('move_node.jstree', function (e, data) {
      moveNode(data);
    })
    .on('changed.jstree', function (e, data) {
      if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
        let selected = data.node.id;
        location.href = `/resource/situationdb?selected=${selected}`;
      }
    });

  function contextMenu(node) {
    let defaultContextmenu = $jq.jstree.defaults.contextmenu.items();
    defaultContextmenu.create.label = '新建目录';
    defaultContextmenu.create.icon = 'icon-create';
    defaultContextmenu.rename.label = '修改目录';
    defaultContextmenu.rename.icon = 'icon-rename';
    defaultContextmenu.remove.label = '删除目录';
    defaultContextmenu.remove.icon = 'icon-remove';

    delete defaultContextmenu.ccp;
    if (node.parent === '#') {
      delete defaultContextmenu.rename;
      delete defaultContextmenu.remove;
    }
    return defaultContextmenu;
  }

  function createNode(data) {
    if($jq('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return false;
    }

    $.post('/resource/situationdb/tree/create', {
      'parent': data.node.parent,
      'name': data.node.text,
      'sort': (data.position + 1)
    })
      .done(function (d) {
        data.instance.set_id(data.node, d.id);
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  function renameNode(data) {
    if($jq('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return false;
    }

    if (data.text === data.old || data.node.id.length !== 8) {
      return false;
    }
    $.post(`/resource/situationdb/tree/${data.node.id}/rename`, {'name': data.text})
      .done(function (d) {
        data.instance.refresh();
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  function deleteNode(data) {
    if($jq('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return false;
    }

    if (confirm('删除目录后所有子目录和对局将一并删除，是否确认操作？')) {
      $.post(`/resource/situationdb/tree/${data.node.id}/remove`)
        .fail(function () {
          data.instance.refresh();
        });
    } else {
      data.instance.refresh();
    }
  }

  function moveNode(data) {
    if($jq('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return false;
    }

    let parent = data.new_instance.get_node(data.node.parent);
    let children = parent.children;
    $.post(`/resource/situationdb/tree/${data.node.id}/move`, {'parent': data.parent, 'children': children})
      .done(function (d) {
        data.instance.refresh();
      })
      .fail(function () {
        data.instance.refresh();
      });
  }

  $('.tree-create').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    sel = ref.create_node(sel, {"type":"file"});
    if(sel) {
      ref.edit(sel);
    }
  });

  $('.tree-rename').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    ref.edit(sel);
  });

  $('.tree-delete').click(function () {
    let ref = $jq('.dbtree').jstree(true),
      sel = ref.get_selected();
    if(!sel.length) { return false; }
    ref.delete_node(sel);
  });

  $('.situationdb').find('.btn-create').click(function (e) {
    e.preventDefault();
    if($jq('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return false;
    }

    let situationdbId = $('input[name="selected"]').val();
    let fen = $(this).data('fen');
    $.ajax({
      url: `/resource/situationdb/rel/create?situationdbId=${situationdbId}&fen=${fen ? fen : ''}`
    }).then(function (html) {
      $.modal($(html), '', function () {
        history.replaceState(null, '', delParam('fen'));
      });
      $('.cancel').click(function () {
        $.modal.close();
      });

      let $md = $('.situationdbrel-create');
      let $form = $md.find('form');

      $form.find('#form3-tags').tagsInput({
        'height': '40px',
        'width': '100%',
        'interactive': true,
        'defaultText': '添加',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 10,
        'placeholderColor': '#666666'
      });

      lichess.pubsub.emit('content_loaded');
      $form.find('#form3-fen').on('input propertychange', function () {
        validateFen($form, $(this).val())
      });

      $form.find('input[name="standard"]').click(function () {
        validateFen($form, $form.find('#form3-fen').val());
      });

      let $tree = $jq('.situationdbrel-create').find('form').find('.dbtree');
      $tree.jstree({
        'core': {
          'worker': false,
          'data': {
            'url': '/resource/situationdb/tree/load'
          },
          'check_callback': true
        },
        'plugins': [ 'types', 'unique' ]
      })
      .on('changed.jstree', function (e, data) {
        if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
          $form.data('id', data.node.id);
          console.log($form.data('id'));
        }
      });

      create($md);
      return false
    });

  });

  function create($md) {
    $md.find('form').submit(function (e) {
      e.preventDefault();
      let $form = $md.find('.form3');
      let modalSituationdbId = $form.data('id');
      let situationdbId = modalSituationdbId ? modalSituationdbId : $('input[name="selected"]').val();
      $.ajax({
        method: 'POST',
        url: `/resource/situationdb/rel/create?situationdbId=${situationdbId}`,
        data: $form.serialize()
      }).then(function (res) {
        if(modalSituationdbId) {
          location.href = `/resource/situationdb?selected=${modalSituationdbId}`;
        } else {
          location.href = `/resource/situationdb?selected=${situationdbId}`;
        }
      }, function (err) {
        handleError(err);
      });
      return false;
    });
  }

  let $list = $(".infinitescroll");
  $('select.action').change(function() {
    let $this = $(this);

    let action = $this.val();
    if (!action) return;

    let ids = [];
    $list.find('.selected').each(function () {
      return ids.push($(this).attr('data-id'));
    });

    if (ids.length === 0) {
      return;
    }

    let fen = $list.find('.selected').find('.mini-board').data('fen');

    if (action === 'situationRelDelete') {
      if (confirm('删除 ' + ids.length + ' 个资源？')) {
        let url = '/resource/situationdb/rel/delete?ids=' + ids.join(',');
        $.post(url).done(function () {
          location.reload();
        });
      } else {
        $this.val('');
      }
    }

    else if (action === 'situationRelEdit') {
      showEdit(ids);
    }

    else if (action === 'situationRelCopyTo') {
      showCopyOrMoveTo('copy', ids);
    }

    else if (action === 'situationRelMoveTo') {
      showCopyOrMoveTo('move', ids);
    }

    else if (action === 'situationRelToAnalysis') {
      window.open(`/analysis/standard/${fen}`);
    }

    else if (action === 'situationRelToStudy') {
      let $tmpForm = $('#tmp-form');
      $tmpForm.find('input[name="fen"]').val(fen);
      $tmpForm.submit();
    }

    else if (action === 'situationRelDemonstrate') {
      window.open(`/demonstrate?fen=${fen}`);
    }

    else if (action === 'situationRelToEditor') {
      window.open(`/editor/${fen}`);
    }

    else if (action === 'situationRelToPlayWithAi') {
      window.open(`/lobby?fen=${fen}#ai`);
    }

    else if (action === 'situationRelToPlayWithFriend') {
      window.open(`/lobby?fen=${fen}#friend`);
    }

  });

  function showEdit(ids) {
    $jq.ajax({
      url: `/resource/situationdb/rel/edit?relId=${ids[0]}`,
      success: function (html) {
        $.modal($jq(html));
        $jq('.cancel').click(function () {
          $jq('select.action').val('');
          $.modal.close();
        });

        let $form = $('.situationdbrel-edit').find('form');
        $form.find('#form3-tags').tagsInput({
          'height': '40px',
          'width': '100%',
          'interactive': true,
          'defaultText': '添加',
          'removeWithBackspace': true,
          'minChars': 0,
          'maxChars': 10,
          'placeholderColor': '#666666'
        });
        lichess.pubsub.emit('content_loaded');

        $form.find('#form3-fen').on('input propertychange', function () {
          validateFen($form, $(this).val());
        });

        $form.find('input[name="standard"]').click(function () {
          validateFen($form, $form.find('#form3-fen').val());
        });

        $form.submit(function (e) {
          e.preventDefault();
          $jq.ajax({
            method: 'POST',
            url: `/resource/situationdb/rel/update?relId=${ids[0]}`,
            data: $form.serialize(),
            success: function () {
              $.modal.close();
              location.reload();
            },
            error: function (res) {
              handleError(res);
            }
          });
          return false;
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
  }

  function validateFen($form, fen) {
    let $board = $form.find('.preview');
    let $fen = $form.find('#form3-fen');
    let isStandard = $form.find('input[name="standard"]:checked').val();
    if (fen) {
      let validPlayable = isStandard === "true";
      $.ajax({
        url: `/setup/validate-fen?strict=0&playable=${validPlayable}`,
        data: {
          fen: fen
        },
        success: function(data) {
          $board.html(data);
          $fen.parent('.form-group').removeClass('is-invalid');
          $fen[0].setCustomValidity('');
          lichess.pubsub.emit('content_loaded');
        },
        error: function() {
          $board.empty();
          $fen.parent('.form-group').addClass('is-invalid');
          $fen[0].setCustomValidity('FEN 格式无效');
        }
      });
    }

  }

  function showCopyOrMoveTo(action, ids) {
    let situationdbId = $('input[name="selected"]').val();
    $.ajax({
      url: `/resource/situationdb/rel/copyOrMove?action=${action}&frSituationdb=${situationdbId}&ids=${ids.join(',')}`,
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $('select.action').val('');
          $.modal.close();
        });

        window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
          window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
            let $jq = $.noConflict(true);
            let $form = $jq('.modal-content').find('form');
            let $tree = $form.find('.cp-dbtree');
            $tree.jstree({
              'types': {
                '#': {
                  'max_depth': 10
                },
                'default': {
                  'max_children': 100
                }
              },
              'core': {
                'strings': {
                  'New node': '新建文件夹'
                },
                'worker': false,
                'data': {
                  'url': '/resource/situationdb/tree/load'
                },
                'check_callback': true
              },
              'plugins': ['types', 'unique', 'contextmenu'],
              'contextmenu': {
                'select_node': false,
                'items': function (node) {
                  return contextMenu(node);
                }
              }
            }).on('create_node.jstree', function (e, data) {
                createNode(data);
              })
              .on('rename_node.jstree', function (e, data) {
                renameNode(data);
              })
              .on('delete_node.jstree', function (e, data) {
                deleteNode(data);
              })
              .on('changed.jstree', function (e, data) {
                if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                  $form.find('input[name="toSituationdb"]').val(data.node.id)
                }
              });

            function contextMenu(node) {
              let defaultContextmenu = $jq.jstree.defaults.contextmenu.items();
              defaultContextmenu.create.label = '新建目录';
              defaultContextmenu.create.icon = 'icon-create';
              defaultContextmenu.rename.label = '修改目录';
              defaultContextmenu.rename.icon = 'icon-rename';
              defaultContextmenu.remove.label = '删除目录';
              defaultContextmenu.remove.icon = 'icon-remove';

              delete defaultContextmenu.ccp;
              if (node.parent === '#') {
                delete defaultContextmenu.rename;
                delete defaultContextmenu.remove;
              }
              return defaultContextmenu;
            }

            $form.submit(function (e) {
              e.preventDefault();
              $.ajax({
                method: 'POST',
                url: '/resource/situationdb/rel/copyOrMove',
                data: $form.serialize(),
                success: function () {
                  $.modal.close();
                  location.reload();
                },
                error: function (res) {
                  handleError(res);
                }
              });
              return false;
            });
          });
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
  }

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if (typeof json.error === 'string') {
          alert(json.error);
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }

  function delParam(paramKey) {
    let url = window.location.href;
    let urlParam = window.location.search.substr(1);
    let beforeUrl = url.substr(0, url.indexOf("?"));
    let nextUrl = "";
    let arr = new Array();
    if (urlParam !== "") {
      let urlParamArr = urlParam.split("&");
      for (var i = 0; i < urlParamArr.length; i++) {
        let paramArr = urlParamArr[i].split("=");
        if (paramArr[0] !== paramKey) {
          arr.push(urlParamArr[i]);
        }
      }
    }
    if (arr.length > 0) {
      nextUrl = "?" + arr.join("&");
    }
    url = beforeUrl + nextUrl;
    return url;
  }

  $list.find('.paginated').click(function(e) {
    e.preventDefault();
    let $selected = $list.find('.paginated.selected');
    let $mustStandard = $('select.action').find('option.mustStandard');
    if($selected.length < 1) {
      $mustStandard.removeClass("none");
    } else {
      let standard = $selected.first().data('standard');
      if(!standard) {
        $mustStandard.addClass("none");
      } else {
        $mustStandard.removeClass("none");
      }
    }
    return false;
  });

  $('.situationdb .relSortBy').change(function () {
    let sortBy = $(this).val();
    let selected = $('input[name="selected"]').val();
    $.post(`/resource/situationdb/${selected}/relSortBy?sortBy=${sortBy}`)
      .done(function () {
        location.reload();
      })
      .fail(function (err) {
        alert(err);
      });
  });

});

