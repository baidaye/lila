$(function () {
  $('.banner').unslider({
    speed: 500,
    delay: 5000,
    keys: false,
    dots: true,
    arrows: true,
    fluid: false
  });


  $('a.member-accept').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        $('.flatpickr').flatpickr();
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  $('a.member-quit').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        let $form = $('.team-quit-modal form');
        $form.submit(function (e) {
          e.preventDefault();
          $.ajax({
            method: 'post',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (res) {
              $.modal.close();
              location.reload();
            },
            error: function (res) {
              if(res.status === 400) {
                if(res.responseJSON && res.responseJSON.password) {
                  alert(res.responseJSON.password.join('\n'));
                } else {
                  alert(res.responseText);
                }
              } else {
                alert(res.responseText);
              }
            }
          });
          return false;
        });
      },
      error: function (res) {
        alert(res.responseText);
      }
    });
    return false;
  });

});
