$(function() {

  let $page = $('.patterns-pieceRank');

  const emptyFen = '8/8/8/8/8/8/8/8 w - -';

  setUrl();

  if($('main').data('notaccept') == true) {
    window.lichess.memberIntro();
    return;
  }

  $page.find('button.prev').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  $page.find('button.next').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  $page.find('#form3-order').on('change paste keyup', () => {
    next();
  });

  let flipped = false;
  $page.find('button.flip').click(function() {
    let $board = $page.find('.main-board .cg-wrap');
    let orient = $board.data('color');
    $board.addClass('parse-fen-manual');
    if(orient === 'white') {
      $board.data('color', 'black');
    } else {
      $board.data('color', 'white');
    }
    flipped = orient === 'white';
    next();
  });

  $page.find('.fen-line .button').click(function(e) {
    let $this = $(this);
    let $board = $page.find('.cg-wrap');
    let fen = $this.data('fen');

    $board.data('fen', fen);
    $board.addClass('parse-fen-manual');
    $page.find('.patterns__btm').find('.fen').val(fen);

    $page.find('.fen-line .button').removeClass('active');
    $this.addClass('active');

    setGround();
  });

  $page.find('input[name="color"]').change(function() {
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('input[name^="checkerRole"]').change(function() {
    let $this = $(this);
    let $cks = $this.parents('.single').find('input:checked').not(this);
    if($cks.length > 0) {
      $cks.prop('checked', false);
    }
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('input[name^="fixCheckerRole"]').change(function() {
    let $this = $(this);
    let $cks = $this.parents('.single').find('input:checked').not(this);
    if($cks.length > 0) {
      $cks.prop('checked', false);
    }
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('input[name^="controllerRole"]').change(function() {
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('input[name="pinner"]').change(function() {
    let $this = $(this);
    let $cks = $this.parents('.single').find('input:checked').not(this);
    if($cks.length > 0) {
      $cks.prop('checked', false);
    }
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('.btnSituation').click(function() {
    showCreateSituation();
  });

  function next() {
    if($('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return;
    }

    let patternsType = $page.find('input[name="patternsType"]').val();
    let order = $page.find('#form3-order').val();
    let color = $page.find('input[name="color"]:checked').val();
    let checkerRole = $page.find('input[name^="checkerRole"]:checked').val();
    let fixCheckerRole = $page.find('input[name^="fixCheckerRole"]:checked').val();
    let controllerRoles = getControllerRoles();
    let pinner = $page.find('input[name="pinner"]:checked').val();

    return $.ajax({
      url: `/patterns/${patternsType}/piece/rank/next?color=${color}&checkerRole=${checkerRole ? checkerRole : ''}&fixCheckerRole=${fixCheckerRole ? fixCheckerRole : ''}${controllerRoles}&pinner=${pinner ? pinner : ''}&order=${order}`
    }).then(d => {
      setPage(true, d);
      setUrl();
    }).fail(function(err) {
      if(err.status === 404) {
        setPage(false, {});
        setUrl();
      } else if(err.status === 406) {
        setPage(false, {});
        setUrl();
        window.lichess.memberIntro();
      } else {
        alert('发生错误')
      }
    });
  }

  function getControllerRoles() {
    let param = '';
    $page.find('input[name^="controllerRole"]:checked').each(function (index) {
      let $this = $(this);
      param += `&controllerRole[${index}]=${$this.val()}`;
    });
    return param;
  }

  function setPage(success, data) {
    let $board = $page.find('.cg-wrap');
    let $patternsOp = $page.find('.patternsOp');
    let $fenLine = $page.find('.fen-line');
    let $boardBtm = $page.find('.patterns__btm');
    let $order = $page.find('#form3-order');
    let color = $page.find('input[name="color"]:checked').val();

    let $prev = $page.find('button.prev');
    let $next = $page.find('button.next');

    let curr = data.curr;
    let prev = data.prev;
    let next = data.next;
    success = success && curr;
    if(success) {
      let patternsType = $page.find('input[name="patternsType"]').val();
      let currentFenType = $fenLine.find('.active').data('fentype');
      $order.val(curr.order);
      $board.data('fen', curr[currentFenType]);
      $board.data('color', flipped ? 'black' : 'white');
      $board.addClass('parse-fen-manual');
      $boardBtm.find('.fen').val(curr[currentFenType]);
      $boardBtm.find('.btnSituation').removeClass('disabled').prop('disabled', false);
      $patternsOp.text(curr.patternsOp).attr('href', `/patterns/${patternsType}/rank/feature?patternsOp=${curr.patternsOp}&color=${color}&flipped=${flipped}`);

      if(prev) {
        $prev.removeClass('disabled').prop('disabled', false).data('id', prev.order);
      } else {
        $prev.addClass('disabled').prop('disabled', true).data('id', '');
      }

      if(next) {
        $next.removeClass('disabled').prop('disabled', false).data('id', next.order);
      } else {
        $next.addClass('disabled').prop('disabled', true).data('id', '');
      }
    } else {
      $board.data('fen', emptyFen);
      $board.addClass('parse-fen-manual');
      $board.data('color', 'white');
      $boardBtm.find('.fen').val(emptyFen);
      $boardBtm.find('.btnSituation').addClass('disabled').prop('disabled', true);
      $patternsOp.text('- -').attr('href', ``);

      $prev.addClass('disabled').prop('disabled', true).data('id', '');
      $next.addClass('disabled').prop('disabled', true).data('id', '');
    }

    $fenLine.find('button').each(function () {
      let $this = $(this);
      let fenType = $this.data('fentype');
      if(success) {
        $this.data('fen', curr[fenType])
          .removeClass('disabled')
          .prop('disabled', false);
      } else {
        $this.data('fen', '')
          .addClass('disabled')
          .prop('disabled', true);
      }
    });

    setGround();
  }

  function setUrl() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    let checkerRole = $page.find('input[name^="checkerRole"]:checked').val();
    let fixCheckerRole = $page.find('input[name^="fixCheckerRole"]:checked').val();
    let controllerRoles = getControllerRoles();
    let pinner = $page.find('input[name="pinner"]:checked').val();
    let order = $page.find('#form3-order').val();
    history.replaceState(null, '', `/patterns/${patternsType}/piece/rank?color=${color}&checkerRole=${checkerRole ? checkerRole : ''}&fixCheckerRole=${fixCheckerRole ? fixCheckerRole : ''}${controllerRoles}&pinner=${pinner ? pinner : ''}&order=${order ? order : ''}`);
  }

  setGround();
  function setGround() {
    let $board = $page.find('.cg-wrap');
    $board.removeClass('parse-fen-manual');
    let mainGround = $board.data('chessground');
    let fen = $board.data('fen');
    let color = $board.data('color');
    let coord = !!$board.data('coordinates');
    let is3d = $board.hasClass('is3d');
    let config = {
      fen: fen,
      orientation: color,
      movable: {
        free: false,
        color: null,
        dests: {}
      },
      highlight: {
        check: true,
        lastMove: true
      },
      drawable: { enabled: false, visible: false },
      resizable: true,
      coordinates: coord,
      addPieceZIndex: is3d
    };

    if (mainGround) mainGround.set(config);
    else $board.data('chessground', Chessground($board[0], config));
  }

  function showCreateSituation() {
    let fen = $page.find('.patterns__btm').find('.fen').val();
    $.ajax({
      url: `/resource/situationdb/rel/createForEditor?looksLegit=true&fen=${fen}`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });

      $('.situation-create').find('form').find('#form3-tags').tagsInput({
        'height': '40px',
        'width': '100%',
        'interactive': true,
        'defaultText': '添加',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 10,
        'placeholderColor': '#666666'
      });

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-create').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            }
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                $form.find('#form3-situationdbId').val(data.node.id);
              }
            });

          $form.submit(function (e) {
            e.preventDefault();
            let situationdbId = $form.find('#form3-situationdbId').val();
            $.ajax({
              method: 'POST',
              url: `/resource/situationdb/rel/create?situationdbId=${situationdbId}`,
              data: $form.serialize()
            }).then(function (res) {
              $.modal.close();
            }, function (err) {
              handleError(err);
            });
            return false;
          });
        })
      });
      return false
    });
  }

});
