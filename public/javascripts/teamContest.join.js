$(function () {
  applyModal();
  applyPlayerSort();
  applyPlayerToggle();
  applyPlayerRemove();
  applyPlayerResetName();
  applySubmit();
});

function applyModal() {
  $('a.modal-alert').click(function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        playerChooseModal();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function applyPlayerSort() {
  let $page = $('.contest-join');
  let $table = $page.find('.contest-join-list').not('.unsortable');
  let $tbody = $table.find('tbody');
  $table.tableDnD({
    onDragClass: 'dragClass',
    onDrop: function(t, r) {
      applyPlayerResetName();
      applyPlayerResetFormal();
    }
  });
}

function playerChooseModal() {
  let $page = $('.contest-join');
  let $modal = $('.modal-player-choose');

  let $transfer = $modal.find('.transfer');
  let $leftPanel = $transfer.find('.left');
  let $leftList = $leftPanel.find('.transfer-panel-list table tbody');
  let $rightPanel = $transfer.find('.right');
  let $rightList = $rightPanel.find('.transfer-panel-list table tbody');

  let isLeader = $modal.data('leader');
  let maxPlayer = $modal.data('maxPlayer');
  let teamRated = $page.data('teamRated');
  let formalPlayers = $page.data('formalPlayers');
  let substitutePlayers = $page.data('substitutePlayers');
  let $tbody = $page.find('.contest-join-list tbody');

  applyOld();
  search();
  memberAdvance();
  transfer((checkeds) => {});

  function applyOld() {
    if(isLeader) {
      let ids = $page.find('#form3-leaders').val();
      if(ids) {
        ids.split(',').forEach(id => {
          apply(id);
        });
      }
    } else {
      $tbody.find('tr').each(function (_, el) {
        let id = $(el).data('id');
        apply(id);
      });
    }

    function apply(id) {
      let $line = $leftList.find(`#chk_${id}`).parents('tr');
      $line.clone().appendTo($rightList);
      $line.remove();
    }
  }

  function search() {
    let $searchForm = $modal.find('.member-search');

    $searchForm.find('#member_name').keydown((e) => {
      if(e.keyCode === 13){
        e.preventDefault();
        $searchForm.find('.search').trigger('click');
      }
    });

    $searchForm.submit(function(e) {
      e.preventDefault();
      let username = $searchForm.find('#form3-username').val();
      let campus = $searchForm.find('#form3-campus').val();
      let clazzId = $searchForm.find('#form3-clazzId').val();
      let teamRatingMin = $searchForm.find('#form3-teamRatingMin').val();
      let teamRatingMax = $searchForm.find('#form3-teamRatingMax').val();
      let level = $searchForm.find('#form3-level').val();
      let sex = $searchForm.find('#form3-sex').val();
      let singleTags = $searchForm.data('single-tags');
      let rangeTags = $searchForm.data('range-tags');
      if(!singleTags) {
        singleTags = [];
      }
      if(!rangeTags) {
        rangeTags = [];
      }

      let arr = [];
      $leftList.find('input').each(function () {
        arr.push($(this).data('attr'));
      });

      let filterSingleTag = (n) => {
        return singleTags.reduce((prev, field, index) => {
          let tagValue = $searchForm.find(`#form3-fields_${index}_fieldValue`).val();
          return prev && (!tagValue || (n[field] && n[field].includes(tagValue)));
        }, true);
      };

      let filterRangeTag = (n) => {
        return rangeTags.reduce((prev, field, index) => {
          let tagValueMin = $searchForm.find(`#form3-rangeFields_${index}_min`).val();
          let tagValueMax = $searchForm.find(`#form3-rangeFields_${index}_max`).val();
          return prev && (!tagValueMin || (n[field] && n[field] >= tagValueMin)) && (!tagValueMax || (n[field] && n[field] <= tagValueMax));
        }, true);
      };

      let filterIds = arr.filter(n => {
        return (
          (!username || n.name.includes(username)) &&
          (!campus || (n.campus && n.campus.includes(campus))) &&
          (!clazzId || (n.clazz && n.clazz.includes(clazzId))) &&
          (!teamRatingMin || n.teamRating >= teamRatingMin) &&
          (!teamRatingMax || n.teamRating <= teamRatingMax) &&
          (!level || n.level === level) &&
          (!sex || n.sex === sex) &&
          filterSingleTag(n) &&
          filterRangeTag(n)
        )
      }).map(n => n.id);

      $leftList.find('tr').addClass('none');
      filterIds.forEach(function (id) {
        $leftList.find('#chk_' + id).parents('tr').removeClass('none');
      });

      return false;
    });
  }

  let $transferForm = $modal.find('.player-choose-transfer');
  $transferForm.submit(function(e) {
    e.preventDefault();

    let arr = [];
    $rightList.find('input').each(function () {
      let $this = $(this);
      let attr = $this.data('attr');
      let $link = $this.parents('tr').find('td.name');
      let $linkClone = $link.clone();
      $linkClone.find('.u_name').text(attr.username);
      arr.push({...attr, ...{link: $linkClone}});
    });

    if(arr.length > maxPlayer) {
      alert(`选择的棋手数量超过最大限制（${maxPlayer}）`);
      return false;
    }

    if(isLeader) {
      let playerIds = arr.map(d => d.id).join(',');
      let playerNames = arr.map(d => d.name).join(',');
      $page.find('#form3-leaders').val(playerIds);
      $page.find('#form3-leaderNames').val(playerNames);
    } else {
      let trHtmls = arr.sort(function(a, b) {
        return b.rating - a.rating;
      }).map((d, i) => {
        let no = i + 1;
        let formal = no <= formalPlayers;
        let trHtml =
          `<tr id="tr-${d.id}" data-id="${d.id}">
              <td>${no}</td>
              <td>${d.link.html()}</td>
              <td>${d.name}</td>
              ${teamRated ? (`<td>${d.teamRating ? d.teamRating : '-'}</td>`) : '-'}
              <td>${d.rating}</td>
              <td><a class="button button-empty small ${formal ? 'formal' : 'substitute'} toggle" title="点击切换">${formal ? '正式' : '替补'}</a></td>
              <td>
                <a class="button button-empty button-red small remove">移除</a>
                <input type="hidden" name="players[${i}].no" value="${no}">
                <input type="hidden" name="players[${i}].userId" value="${d.id}">
                <input type="hidden" name="players[${i}].formal" value="${formal}">
                <input type="hidden" name="players[${i}].rating" value="${d.rating}">
                ${ teamRated && d.teamRating ? (`<input type="hidden" name="players[${i}].teamRating" value="${d.teamRating}">`) : '' }
              </td>
          </tr>`;
        return trHtml;
      });

      $tbody.empty();
      $tbody.html(trHtmls);
      applyPlayerToggle();
      applyPlayerRemove();
      applyPlayerResetName();
      applyPlayerSort();
    }
    $.modal.close();
  })
}

function applyPlayerToggle() {
  let $page = $('.contest-join');
  let $tbody = $page.find('.contest-join-list tbody');
  $tbody.find('.toggle').click(function () {
    let $this = $(this);
    let formal = $this.hasClass('formal');
    if(formal) {
      $this.removeClass('formal').addClass('substitute').text('替补');
      $this.parents('tr').find('input[name$="formal"]').val(!formal);
    } else {
      $this.removeClass('substitute').addClass('formal').text('正式');
      $this.parents('tr').find('input[name$="formal"]').val(!formal);
    }
  });
}

function applyPlayerRemove() {
  let $page = $('.contest-join');
  let $tbody = $page.find('.contest-join-list tbody');
  $tbody.find('.remove').click(function () {
    $(this).parents('tr').remove();
    applyPlayerResetName();
  });
}

function applyPlayerResetName() {
  let $page = $('.contest-join');
  let $tbody = $page.find('.contest-join-list tbody');
  $tbody.find('tr').each(function (i) {
    let no = i + 1;
    let $item = $(this);
    $item.find('input').each(function() {
      let name = $(this).attr('name');
      $(this).attr('name', name.replace(/players\[\d+\]/g, 'players[' + i + ']'));
    });
    $item.find('input[name$="no"]').val(no);
    $item.filter('tr[id^="tr"]').find("td:first-child").text(no)
  });
}

function applyPlayerResetFormal() {
  let $page = $('.contest-join');
  let $tbody = $page.find('.contest-join-list tbody');
  let formalPlayers = $page.data('formalPlayers');
  $tbody.find('tr').each(function (i) {
    let $item = $(this);
    let no = $item.find('input[name$="no"]').val();
    let formal = no <= formalPlayers;

    $item.find('input[name$="formal"]').val(formal);
    $item.filter('tr[id^="tr"]')
      .find("td .toggle")
      .text(formal ? '正式' : '替补')
      .removeClass(formal ? 'substitute' : 'formal')
      .addClass(formal ? 'formal' : 'substitute')
  });
}

function applySubmit() {
  let $page = $('.contest-join');
  let $form = $page.find('.contest-join-form');
  let $table = $page.find('.contest-join-list');
  let $tbody = $table.find('tbody');

  let formalPlayers = $page.data('formalPlayers');
  let substitutePlayers = $page.data('substitutePlayers');

  $form.find('a[name="staging"]').click(function (e) {
    e.preventDefault();
    let staging = $(e.target).attr('value') === 'true';
    let validLeaders = () => {
      let ids = $page.find('#form3-leaders').val();
      let arr = ids.split(',');
      if(!ids || arr.length === 0 || arr.length > 2) {
        alert('领队数量错误');
        return false;
      }
      return true;
    };

    let validPlayers = () => {
      let formalCount = 0;
      let substituteCount = 0;
      $tbody.find('tr').each(function (_, el) {
        let $this = $(this);
        let formal = $this.find('input[name$="formal"]').val() === 'true';

        if(formal) formalCount++;
        else substituteCount++;
      });

      if(formalCount !== formalPlayers) {
        alert('正式棋手数量错误');
        return false;
      }

      if(substituteCount > substitutePlayers) {
        alert('替补棋手数量错误');
        return false;
      }
      return true;
    };

    if(!validLeaders() || !validPlayers()) {
      return false;
    }

    let $button = $(this);
    let buttonTxt = $button.text();
    $button.prop('disabled', true)
      .addClass('disabled')
      .text('正在发送请求');

    let url = $form.attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: $form.serialize() + `&staging=${staging}`,
      success: function() {
        if(staging) {
          alert('已暂存。');
        } else {
          alert('报名成功，等待管理员审核。');
        }
        location.href = `/contest/team/${$form.data('id')}`
      },
      error: function(res) {
        handleError(res);
        $button
          .prop('disabled', false)
          .removeClass('disabled')
          .text(buttonTxt);
      }
    });
  });
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else if (json.global){
      alert(json.global[0]);
    } else alert(res.responseText);
  } else alert('发生错误');
}
