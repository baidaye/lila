$(function () {

    let $page = $('.homework-form');
    registerModal();
    registerItems();
    registerCapsules();
    registerReplayGame();
    registerRecallGame();
    registerDistinguishGame();
    registerFromPosition();

    $page.find('input').on('input propertychange', function () {
        $(this).removeClass('invalid');
    });

    function registerModal() {
        $page.find('a.modal-alert,div.modal-alert').click(function(e) {
            e.preventDefault();
            let $this = $(this);
            let href = $this.attr('href');
            href = href ? href : $this.attr('data-href');
            $.ajax({
                url: href,
                success: function(html) {
                    $.modal($(html));
                    $('.cancel').click(function () {
                        $.modal.close();
                    });
                    registerItems();
                    registerThemePuzzle(e);
                    registerPuzzleRush();
                    registerCoordTrain();
                    registerCapsules();
                    registerReplayGame();
                    registerDistinguishGame();
                    registerRecallGame();
                    registerDeadlineAt();
                },
                error: function(res) {
                    alert(res.responseText);
                }
            });
            return false;
        });
    }

    function registerItems() {
        $('.modal-item .form3').submit(function (e) {
            e.preventDefault();

            let $form = $(this);
            let $table = $page.find('.items table tbody');
            let items = [];
            $form.find('.items input:checked').each(function () {
                let attr = $(this).data('attr');
                items.push(attr);
            });

            let max = $('.btn-item-add').data('max');
            if(items.length > max) {
                alert('最多选择' + max + '项');
                return false;
            }

            let itemCond = function(item, index, clazzId, courseId) {
                function condHtml(code) {
                   return `<div class="modal-alert selected" data-href="/clazz/homework/${code}?clazzId=${clazzId}&courseId=${courseId}">
                                <div class="tags"></div>
                                <input type="hidden" id="form3-common_${index}_extra_cond" name="common[${index}].extra.cond">
                            </div>`;
                }

                let html = '';
                switch (item.id) {
                    case 'themePuzzleNumber':
                    case 'themePuzzleNumber2':
                    case 'themePuzzleNumber3':
                    case 'themePuzzleNumber4':
                        html = condHtml('themePuzzle');
                        break;
                    case 'customNumber':
                        html = condHtml('puzzleRush');
                        break;
                    case 'coordTrain':
                        html = condHtml('coordTrain');
                        break;
                }
                return html;
            };

            let itemsHtml = items.map(function (item, index) {
                let old = $table.find(`#${item.id}`);
                if(old.length > 0) {
                    return old;
                } else {
                    let clazzId = $page.data('clazzid');
                    let courseId = $page.data('courseid');
                    let html =
                        `<tr id="${item.id}">
                            <td class="item-name"><label>${item.name}</label></td>
                            <td class="bit"><strong>${item.isNumber ? '' : '+'}</strong></td>
                            <td class="ipt">
                                <input type="number" name="common[${index}].num" placeholder="${item.isNumber ? '数量' : '分'}">
                                <input type="hidden" name="common[${index}].item" value="${item.id}">
                            </td>
                            <td class="cond">${itemCond(item, index, clazzId, courseId)}</td>
                            <td class="rm"><a class="remove">移除</a></td>
                         </tr>`;
                    return html;
                }
            });

            $table.empty();
            $table.html(itemsHtml);
            remove();
            resetName();
            setLink();
            registerModal();
            $.modal.close();
            return false;
        });

        function remove() {
            $page.find('.items table').find('.remove').click(function () {
                $(this).parents('tr').remove();
                resetName();
                setLink()
            });
        }

        function resetName() {
            $page.find('.items table tr').each(function (i, e) {
                let $item = $(this);
                $item.find('input').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/common\[\d+\]/g, 'common[' + i + ']'));
                });
            });
        }

        function setLink() {
            let ids = [];
            $page.find('.items input[name$=item]').each(function() {
                ids.push($(this).val());
            });
            let href = $('.btn-item-add').attr('href');
            href = href.split('=')[0] + '=' + ids.join(',');
            $('.btn-item-add').attr('href', href);
        }

        remove();
    }

    function registerThemePuzzle(modalEvent) {
        let $form = $('.modal-themePuzzle form');
        $form.submit(function (e) {
            e.preventDefault();
            let ratingMin = $form.find('#form3-ratingMin').val();
            let ratingMax = $form.find('#form3-ratingMax').val();
            let stepsMin = $form.find('#form3-stepsMin').val();
            let stepsMax = $form.find('#form3-stepsMax').val();

            let label = '';
            if(ratingMin) {
                label += `<span>&gt${ratingMin}分</span>`
            }
            if(ratingMax) {
                label += `<span>&lt${ratingMax}分</span>`
            }
            if(stepsMin) {
                label += `<span>&gt${stepsMin}步</span>`
            }
            if(stepsMax) {
                label += `<span>&lt${stepsMax}步</span>`
            }

            let tags = [];
            $form.find('span input:checked').each(function () {
                let text = $(this).parent('span').find('label').text();
                tags.push(text);
            });
            let tagHtml = tags.map(function (tag) {
                return `<span>${tag}</span>`
            });
            label += tagHtml.join('');

            let data = $form.serialize();
            let clazzId = $page.data('clazzid');
            let courseId = $page.data('courseid');
            let $themePuzzleNumber = $(modalEvent.target).parents('tr');

            let $cond = $themePuzzleNumber.find('.cond');
            $cond.find('input').val(data);
            $cond.find('.selected').attr('data-href', `/clazz/homework/themePuzzle?clazzId=${clazzId}&courseId=${courseId}&${data}`);
            $cond.find('.tags').html(label);
            if(label) {
                $cond.find('.selected').addClass('has');
            } else $cond.find('.selected').removeClass('has');

            $.modal.close();
            return false;
        });

        loadCount();
        $form.find('.btn-search').click(function () {
            loadCount();
        });

        function loadCount() {
            $.ajax({
                url: `/resource/puzzle/themeOrCount`,
                data: $form.serialize(),
                success: function(res) {
                    $form.find('.countWaiting').addClass('none');
                    $form.find('.count').text(res.count).removeClass('none');
                },
                error: function(res) {
                    alert(res.responseText);
                }
            });
        }
    }

    function registerPuzzleRush() {
        let $form = $('.modal-puzzleRush form');
        $form.submit(function (e) {
            e.preventDefault();
            let ratingMin = $form.find('#form3-ratingMin').val();
            let ratingMax = $form.find('#form3-ratingMax').val();
            let stepsMin = $form.find('#form3-stepsMin').val();
            let stepsMax = $form.find('#form3-stepsMax').val();
            let color = $form.find('#form3-color').val();
            let phase = $form.find('#form3-phase').val();
            let selector = $form.find('#form3-selector').val();
            let minutes = $form.find('#form3-minutes').val();
            let limit = $form.find('#form3-limit').val();

            let label = '';
            if(ratingMin) {
                label += `<span>&gt${ratingMin}分</span>`
            }
            if(ratingMax) {
                label += `<span>&lt${ratingMax}分</span>`
            }
            if(stepsMin) {
                label += `<span>&gt${stepsMin}步</span>`
            }
            if(stepsMax) {
                label += `<span>&lt${stepsMax}步</span>`
            }
            if(color) {
                let text = $form.find('#form3-color').find('option:selected').text();
                label += `<span>${text}</span>`
            }
            if(phase) {
                let text = $form.find('#form3-phase').find('option:selected').text();
                label += `<span>${text}</span>`
            }
            if(selector) {
                let text = $form.find('#form3-selector').find('option:selected').text();
                label += `<span>${text}</span>`
            }
            if(minutes) {
                label += `<span>${minutes}分钟</span>`
            }
            if(limit) {
                label += `<span>错题&lt${limit}个</span>`
            }

            let data = $form.serialize();
            let clazzId = $page.data('clazzid');
            let courseId = $page.data('courseid');
            $page.find('.items').find('#customNumber').each(function() {
                let $cond = $(this).find('.cond');
                $cond.find('input').val(data);
                $cond.find('.selected').attr('data-href', `/clazz/homework/puzzleRush?clazzId=${clazzId}&courseId=${courseId}&${data}`);
                $cond.find('.tags').html(label);
                if(label) {
                    $cond.find('.selected').addClass('has');
                } else $cond.find('.selected').removeClass('has');
            });

            $.modal.close();
            return false;
        });
    }

    function registerCoordTrain() {
        let $form = $('.modal-coordTrain form');
        $form.submit(function (e) {
            e.preventDefault();
            let label = '';
            let mode = $form.find('#form3-mode').val();
            let orient = $form.find('#form3-orient').val();
            let coordShow = $form.find('#form3-coordShow').val();
            let limitTime = $form.find('#form3-limitTime').val();
            if(mode) {
                let text = $form.find('#form3-mode').find('option:selected').text();
                label += `<span>${text}</span>`
            }
            if(orient) {
                let text = $form.find('#form3-orient').find('option:selected').text();
                label += `<span>${text}</span>`
            }
            if(coordShow === "1") {
                label += `<span>显示棋盘坐标</span>`
            } else {
                label += `<span>不显示棋盘坐标</span>`
            }
            if(limitTime === "1") {
                label += `<span>限制时间</span>`
            } else {
                label += `<span>不限时间</span>`
            }

            let data = $form.serialize();
            let clazzId = $page.data('clazzid');
            let courseId = $page.data('courseid');
            $page.find('.items').find('#coordTrain').each(function() {
                let $cond = $(this).find('.cond');
                $cond.find('input').val(data);
                $cond.find('.selected').attr('data-href', `/clazz/homework/coordTrain?clazzId=${clazzId}&courseId=${courseId}&${data}`);
                $cond.find('.tags').html(label);
                if(label) {
                    $cond.find('.selected').addClass('has');
                } else $cond.find('.selected').removeClass('has');
            });

            $.modal.close();
            return false;
        });
    }

    function registerCapsules() {
        $('.modal-capsule .form3').submit(function (e) {
            e.preventDefault();
            let $form = $(this);
            let ids = [];
            $form.find('.capsule-list input:checked').each(function () {
                ids.push($(this).val());
            });

            let max = $('.btn-capsule-add').data('max');
            if($page.find('.capsules .capsule').length + ids.length > max) {
                alert('最多添加' + max + '项');
                return false;
            }

            $.get('/resource/capsule/infos?ids=' + ids.join(','), function (response) {
                let html = response.map(function(capsule) {
                    let puzzles = capsule.puzzles;
                    let puzzlesHtml = '';
                    if(puzzles && puzzles.length > 0) {
                        puzzles.forEach(function(puzzle) {
                            puzzlesHtml += puzzleHtml(puzzle);
                        });
                    }
                    return capsuleHtml(capsule, puzzlesHtml);
                });

                $page.find('.capsules').append(html);
                lichess.pubsub.emit('content_loaded');

                remove();
                resetName();
                $.modal.close();
            });
            return false;
        });

        function remove() {
            $page.find('.capsules .remove').click(function () {
                $(this).parents('.capsule').remove();
                resetName();
            });
        }

        function resetName() {
            $page.find('.capsules .capsule').each(function (i, e) {
                let $capsule = $(this);

                $capsule.find('.capsule-head input').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/capsules\[\d+\]/g, 'capsules[' + i + ']'));
                });

                $capsule.find('.capsule-puzzles .puzzle').each(function(j, e) {
                    $(this).find('input').each(function() {
                        let name = $(this).attr('name');
                        $(this).attr('name', name.replace(/capsules\[\d+\]/g, 'capsules[' + i + ']').replace(/puzzles\[\d+\]/g, 'puzzles[' + j + ']'));
                    });
                });
            });
        }

        function capsuleHtml(capsule, puzzles) {
          return `<div class="capsule">
                   <div class="capsule-head">
                        <label>${capsule.name}</label>
                        <a class="remove">移除</a>
                        <input type="hidden" name="practice.capsules[0].id" value="${capsule.id}">
                        <input type="hidden" name="practice.capsules[0].name" value="${capsule.name}">
                    </div>
                    <div class="capsule-puzzles">${puzzles}</div> 
                </div>`;
        }

        function puzzleHtml(puzzle) {
            let lines = encodeURI(puzzle.lines);
            return `<div class="puzzle">
                        <span class="mini-board cg-wrap parse-fen is2d" data-color="${puzzle.color}" data-fen="${puzzle.fen}" data-lastmove="${puzzle.lastMove}"></span>
                        <input type="hidden" name="practice.capsules[0].puzzles[0].id" value="${puzzle.id}">
                        <input type="hidden" name="practice.capsules[0].puzzles[0].fen" value="${puzzle.fen}">
                        <input type="hidden" name="practice.capsules[0].puzzles[0].color" value="${puzzle.color}">
                        <input type="hidden" name="practice.capsules[0].puzzles[0].lastMove" value="${puzzle.lastMove}">
                        <input type="hidden" name="practice.capsules[0].puzzles[0].lines" value="${lines}">
                    </div>`;
        }

        remove();

        function filter() {
            let $filter = $('.capsule-filter');
            let name = $filter.find('.capsule-filter-search').val();
            let tags = [];
            $filter.find('.capsule-filter-tag').find('input:checked').each(function() {
                tags.push(this.value);
            });

            let $lst = $('.capsule-list');
            let arr = [];
            $lst.find('tr').each(function () {
                arr.push($(this).data('attr'));
            });

            let filterIds = arr.filter(function (n) {
                return (!name || n.name.indexOf(name) > -1) && (tags.length === 0 || tags.every(t => n.tags.includes(t)))
            }).map(n => n.id);

            $lst.find('tr').addClass('none');
            filterIds.forEach(function (id) {
                $lst.find('#chk_' + id).parents('tr').removeClass('none');
            });
        }

        $('.capsule-filter .capsule-filter-search').on('input propertychange', function() {
            filter();
        });

        $('.capsule-filter .capsule-filter-tag input').on('click', function() {
            filter();
        });
    }

    function registerReplayGame() {
        let $md = $('.modal-replay');
        let $form = $md.find('form');
        $md.find('.tabs-horiz span').click(function () {
            let $this = $(this);
            $md.find('.tabs-horiz span').removeClass("active");
            $md.find('.tabs-content div').removeClass("active");

            let cls = $this.attr('class');
            $this.addClass('active');
            $md.find('.tabs-content div.' + cls).addClass('active');
            $md.find('input[name=tab]').val(cls);
        });

        $form.submit(function (e) {
            e.preventDefault();

            let max = $('.btn-replay-add').data('max');
            if($page.find('.replayGames table>tbody>tr').length >= max) {
                alert('最多添加' + max + '项');
                return false;
            }

            let chapterInfoLink = '';
            let chapterLink = $form.find('.study-content .chapter .chapter-list input:checked').val();
            if(chapterLink) {
                chapterInfoLink = `${chapterLink}/info`;
            }

            let courseWareInfoLink = '';
            let courseWareLink = '';
            let courseWareData = $form.find('.courseWare-content .courseWare-list input:checked').val();
            if(courseWareData) {
                let olClassId = courseWareData.split(':')[0];
                let courseWareId = courseWareData.split(':')[1];
                courseWareLink = `/olclass/${olClassId}#${courseWareId}`;
                courseWareInfoLink = `/olclass/${olClassId}/courseWare/info?courseWareId=${courseWareId}`;
            }

            if(!chapterLink && !courseWareLink) {
                alert('输入一种PGN获取方式');
                return false;
            }


            let infoLink = chapterInfoLink ? chapterInfoLink : courseWareInfoLink;
            let link = chapterLink ? chapterLink : courseWareLink;
            if(infoLink) {
                $.get(infoLink, function (response) {
                    let moves = moveTable(response);
                    let $html = $(replayHtml(response, moves, link));
                    $page.find('.replayGames table>tbody').append($html);
                    $html.find('.mini-board').each(function () {
                        let $board = $(this);
                        Chessground(this, {
                            coordinates: false,
                            resizable: false,
                            drawable: { enabled: false, visible: false },
                            viewOnly: true,
                            fen: $board.data('fen')
                        });
                    });
                    //lichess.pubsub.emit('content_loaded');

                    remove();
                    resetName();
                    chessMove();
                    $.modal.close();
                }).fail(function(err) {
                    if(err.responseJSON.error) {
                        alert(err.responseJSON.error);
                    } else alert('加载失败');
                });
            }
            return false;
        });

        studyCheck($form);
        courseWareGet($form);

        function remove() {
            $page.find('.replayGames table>tbody .remove').click(function () {
                $(this).parents('tr').remove();
                resetName();
            });
        }

        function replayHtml(chapter, moves, chapterLink) {
            return `<tr>
                       <td class="td-board">
                            <span class="mini-board cg-wrap parse-fen is2d" data-fen="${chapter.root}"></span>
                       </td>
                       <td>
                            <a href="${chapterLink}"><label>${chapter.name}</label></a>
                            <div class="moves">${moves}</div>
                       </td>
                       <td>
                            <input type="hidden" name="practice.replayGames[0].chapterLink" value="${chapterLink}">
                            <input type="hidden" name="practice.replayGames[0].name" value="${chapter.name}">
                            <input type="hidden" name="practice.replayGames[0].root" value="${chapter.root}">
                            <textarea class="none" name="practice.replayGames[0].pgn">${chapter.pgn}</textarea>
                            <a class="remove">移除</a>
                        </td>
                    </tr>`;
        }

        function resetName() {
            $page.find('.replayGames table>tbody>tr').each(function (i, e) {
                $(this).find('input,textarea').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/replayGames\[\d+\]/g, 'replayGames[' + i + ']'));
                });

                $(this).find('move').each(function (j, e) {
                    $(this).find('input').each(function() {
                        let name = $(this).attr('name');
                        $(this).attr('name', name.replace(/moves\[\d+\]/g, 'moves[' + j + ']'));
                    });
                });
            });
        }

        function moveTable(chapter) {
            let html = '';
            let moves = chapter.moves;
            if(moves && moves.length > 0) {
                moves.forEach(function(move) {
                    let white = move.white ? move.white.san : '...';
                    let black = move.black ? move.black.san : '';
                    let whiteFen = move.white ? move.white.fen : '';
                    let blackFen = move.black ? move.black.fen : '';
                    let whiteClass = move.white ? '' : 'disabled';
                    let blackClass = move.black ? '' : 'disabled';

                    let whiteHidden = move.white ?
                        `
                        <input type="hidden" name="practice.replayGames[0].moves[0].white.san" value="${move.white.san}">
                        <input type="hidden" name="practice.replayGames[0].moves[0].white.uci" value="${move.white.uci}">
                        <input type="hidden" name="practice.replayGames[0].moves[0].white.fen" value="${move.white.fen}">
                        ` : '';

                    let blackHidden = move.black ?
                        `
                        <input type="hidden" name="practice.replayGames[0].moves[0].black.san" value="${move.black.san}">
                        <input type="hidden" name="practice.replayGames[0].moves[0].black.uci" value="${move.black.uci}">
                        <input type="hidden" name="practice.replayGames[0].moves[0].black.fen" value="${move.black.fen}">
                        ` : '';

                    html += `<move>
                                <index>${move.index}. <input type="hidden" name="practice.replayGames[0].moves[0].index" value="${move.index}"></index>
                                <span class = "${whiteClass}" data-fen="${whiteFen}">${white}${whiteHidden}</span>
                                <span class = "${blackClass}" data-fen="${blackFen}">${black}${blackHidden}</span>
                            </move>`;
                });
            }
            return html;
        }

        function chessMove() {
            $page.find('.moves move span:not(.disabled)').click(function() {
                let fen = $(this).data('fen');
                let $board = $(this).parents('tr').find('.td-board .mini-board');
                Chessground($board[0], {
                    coordinates: false,
                    resizable: false,
                    drawable: { enabled: false, visible: false },
                    viewOnly: true,
                    fen: fen
                });
                $board.attr('data-fen', fen);

                $page.find('.moves move span.active').removeClass('active');
                $(this).addClass('active');
            });
        }

        remove();
        chessMove();
    }

    function registerFromPosition() {
        let register = function() {
            $page.find('.fromPositions .tb>tbody').find('.mini-board').each(function () {
                let $board = $(this);
                Chessground(this, {
                    coordinates: false,
                    resizable: false,
                    drawable: { enabled: false, visible: false },
                    viewOnly: true,
                    fen: $board.data('fen')
                });
            });
            //lichess.pubsub.emit('content_loaded');

            $page.find('.fromPositions .tb>tbody').find('input[name$=fen]').on('input propertychange', function () {
                let fen = $(this).val();
                let $tr = $(this).parents('.form').parents('tr');
                let $board = $tr.find('td:first');
                let $fen = $tr.find('input[name$=fen');

                validateFen(fen, $board, $fen);
            });

            $page.find('.fromPositions .tb>tbody').find('select[name$=isAi]').on('change', function () {
                let isAi = $(this).val();
                let $tr = $(this).parents('.form').parents('tr');
                let $aiTr = $tr.find('select[name$=aiLevel').parent('td').parent('tr');
                if(isAi == 1) {
                    $aiTr.removeClass('none');
                } else {
                    $aiTr.addClass('none');
                }
            });
            remove();
        };

        let remove = function() {
            $page.find('.fromPositions .tb>tbody').find('.remove').click(function () {
                $(this).parents('tr').remove();
                resetName();
            });
        };

        let resetName = function() {
            $page.find('.fromPositions .tb>tbody>tr').each(function (i, e) {
                $(this).find('input,select').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/fromPositions\[\d+\]/g, 'fromPositions[' + i + ']'));
                });
            });
        };

        let validateFen = function(fen, $board, $fen) {
            if (fen) {
                $.ajax({
                    url: '/setup/validate-fen?strict=0',
                    data: {
                        fen: fen
                    },
                    success: function(data) {
                        $board.html(data);
                        $fen[0].setCustomValidity('');
                        lichess.pubsub.emit('content_loaded');
                    },
                    error: function() {
                        $fen[0].setCustomValidity('FEN 格式无效');
                        $board.empty();
                    }
                });
            }
        };

        let registerLoadSituation = function() {
            $page.find('.fromPositions .tb>tbody').find('.loadSituation').click(function () {
                let $tr = $(this).parents('tr');
                loadSituation($tr);
            });
        };

        let loadSituation = function($tr) {
            if($('main').data('notaccept') == true) {
                window.lichess.memberIntro();
                return false;
            }

            let fen = '';
            $.ajax({ url: `/resource/situationdb/select?mustStandard=true` }).then(function (html) {
                $.modal($(html));
                $('.cancel').click(function () {
                    $.modal.close();
                });
                window.lichess.pubsub.emit('content_loaded');

                window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
                    window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                        let $jq = $.noConflict(true);
                        let $form = $jq('.situation-selector').find('form');
                        let $tree = $form.find('.dbtree');
                        $tree.jstree({
                            'core': {
                                'worker': false,
                                'data': {
                                    'url': '/resource/situationdb/tree/load'
                                },
                                'check_callback': true
                            },
                            'plugins' : [ 'search' ]
                        })
                          .on('changed.jstree', function (e, data) {
                              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                                  search();
                              }
                          });

                        let to = null;
                        $form.find('.search input[name="q"]').keyup(function () {
                            let q = $(this).val();
                            if (to) clearTimeout(to);
                            to = setTimeout(function () {
                                $tree.jstree(true).search(q, false, true);
                            }, 500);
                        });

                        $form.find('#btn-search').click(function () {
                            search();
                        });

                        let $emptyTag = $form.find('.search_form')
                          .find('.tag-group.emptyTag')
                          .find('input[type="checkbox"]');

                        let $tags = $form.find('.search_form')
                          .find('.tag-group')
                          .find('input[type="checkbox"]');

                        $tags.not($emptyTag).click(function () {
                            $emptyTag.prop('checked', false);
                            search();
                        });

                        $emptyTag.click(function () {
                            $tags.not($emptyTag).prop('checked', false);
                            search();
                        });

                        let $situations = $form.find('.situations');

                        function search() {
                            let situation = $tree.jstree(true).get_selected(null)[0];
                            $.ajax({
                                url: `/resource/situationdb/rel/list?selected=${situation}`,
                                data: $form.serialize()
                            }).then(function (rels) {
                                $situations.empty();

                                if(!rels || rels.length === 0) {
                                    let noMore =
                                      `<div class="no-more">
                                    <i data-icon="4">
                                    <p>没有更多了</p>
                                  </div>`;
                                    $situations.html(noMore);
                                } else {
                                    let situations =
                                      rels.map(function (rel) {
                                          let tags = '';
                                          if(rel.tags && rel.tags.length > 0) {
                                              rel.tags.map(function (tag) {
                                                  tags = `<span>${tag}</span>`;
                                              });
                                          }
                                          return `<a class="paginated" target="_blank" data-id="${rel.id}">
                                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                                <cg-helper>
                                                  <cg-container>
                                                    <cg-board></cg-board>
                                                  </cg-container>
                                                </cg-helper>
                                              </div>
                                              <div class="btm">
                                                <label>${rel.name ? rel.name : ''}</label>
                                                <div class="tags">${tags}</div>
                                              </div>
                                            </a>`;
                                      });

                                    $situations.html(situations);
                                    registerEvent();
                                    window.lichess.pubsub.emit('content_loaded')
                                }
                            })
                        }

                        function registerEvent() {
                            let $boards = $situations.find('.paginated');
                            $boards.off('click');
                            $boards.click(function(e) {
                                e.preventDefault();
                                let $this = $(this);
                                $boards.removeClass('selected');
                                $this.addClass('selected');
                                fen = $this.find('.mini-board').data('fen');
                                return false;
                            });
                        }
                        registerEvent();

                        $form.submit(function (e) {
                            e.preventDefault();
                            let $board = $tr.find('td.td-board');
                            let $fen = $tr.find('input[name$=fen');
                            $fen.val(fen);
                            validateFen(fen, $board, $fen);
                            $.modal.close();
                            return false;
                        });
                    })
                });
                return false
            });
        };

        registerLoadSituation();
        register();
        resetName();
        remove();
    }

    function registerRecallGame() {
        let $md = $('.modal-recall');
        let $form = $md.find('form');
        $md.find('.tabs-horiz span').click(function () {
            let $this = $(this);
            $md.find('.tabs-horiz span').removeClass("active");
            $md.find('.tabs-content div').removeClass("active");

            let cls = $this.attr('class');
            $this.addClass('active');
            $md.find('.tabs-content div.' + cls).addClass('active');
            $md.find('input[name=tab]').val(cls);
        });

        window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
            window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                let $jq = $.noConflict(true);
                let $tree = $jq($md).find('.dbtree');
                $tree.jstree({
                    'core': {
                        'worker': false,
                        'data' : {
                            'url' : '/resource/gamedb/tree/loadAll',
                            'data' : function (node) {
                                return { 'selected': node.id };
                            }
                        },
                    },
                    'plugins' : [ 'search' ]
                })
                  .on('changed.jstree', function (e, data) {
                      if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                          if(data.node.original.type === 'file') {
                              let id = data.node.id;
                              $md.find('input[name="gamedb"]').val(id.split(':')[1]);
                          }
                      }
                  });

                let to = null;
                $md.find('.gamedb .search input[name="q"]').keyup(function () {
                    let q = $(this).val();
                    if (to) clearTimeout(to);
                    to = setTimeout(function () {
                        $tree.jstree(true).search(q, false, true);
                    }, 500);
                });
            });
        });

        $form.submit(function(e) {
            e.preventDefault();
            let max = $('.btn-recall-add').data('max');
            if($page.find('.recallGames table>tbody>tr').length >= max) {
                alert('最多添加' + max + '项');
                return false;
            }

            let chapterLink = $form.find('.study-content .chapter .chapter-list input:checked').val();
            if(chapterLink) {
                chapterLink = location.protocol + '//' + location.host + chapterLink;
                $form.find('#form3-chapter').val(chapterLink);
            }

            $form.find('#form3-courseWare').val($form.find('.courseWare-content .courseWare-list input:checked').val());

            if(!$form.find('#form3-game').val()
              && !$form.find('#form3-gamedb').val()
              && !$form.find('#form3-pgn').val()
              && !$form.find('#form3-chapter').val()
              && !$form.find('#form3-courseWare').val()
            ) {
                alert('输入一种PGN获取方式');
                return false;
            }

            $.ajax({
                method: 'POST',
                url: '/recall/pgn',
                data: $form.serialize()
            }).then(function(res) {
                let color = $form.find('#form3-color').val();
                let orient = $form.find('#form3-orient').val();
                let turns = $form.find('#form3-turns').val();
                let courseName = $('input[name=courseName]').val();
                let htmlRaw = recallHtml(res.name, res.fen, res.pgn, res.moves, color, orient, turns, courseName);
                let $html = $(htmlRaw);
                $page.find('.recallGames table>tbody').append($html);
                $html.find('.mini-board').each(function () {
                    let $board = $(this);
                    Chessground(this, {
                        coordinates: false,
                        resizable: false,
                        drawable: { enabled: false, visible: false },
                        viewOnly: true,
                        fen: $board.data('fen')
                    });
                });
                //lichess.pubsub.emit('content_loaded');

                remove();
                resetName();
                chessMove();
                $.modal.close();
            }, function (err) {
                handleError(err)
            });
            return false;
        });

        studyCheck($form);
        courseWareGet($form);

        $md.find('input[type=file]').on('change', function() {
            let file = this.files[0];
            if (!file) return;
            let reader = new FileReader();
            reader.onload = function(e) {
                $md.find('textarea').val(e.target.result);
            };
            reader.readAsText(file);
        });

        function remove() {
            $page.find('.recallGames table>tbody .remove').click(function () {
                $(this).parents('tr').remove();
                resetName();
            });
        }

        function resetName() {
            $page.find('.recallGames table>tbody>tr').each(function (i, e) {
                $(this).find('input,textarea').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/recallGames\[\d+\]/g, 'recallGames[' + i + ']'));
                });

                $(this).find('move').each(function (j, e) {
                    $(this).find('input').each(function() {
                        let name = $(this).attr('name');
                        $(this).attr('name', name.replace(/moves\[\d+\]/g, 'moves[' + j + ']'));
                    });
                });
            });
        }

        function recallHtml(name, fen, pgn, moves, color, orient, turns, courseName) {

            let colorName = function () {
                if(color === 'white') return '白方';
                else if(color === 'black') return '黑方';
                else return '双方'
            };

            let orientName = function () {
                if(orient === 'white') return '白方';
                else if(orient === 'black') return '黑方';
                else return ''
            };

            let turnsName = function () {
                if(!turns) return '所有';
                else return turns;
            };

            let colorHtml = function () {
                if(color !== 'all') return `<input type="hidden" name="practice.recallGames[0].color" value="${color}">`;
                else return ``;
            };

            let turnsHtml = function () {
                if(turns) return `<input type="hidden" name="practice.recallGames[0].turns" value="${turns}">`;
                else return ``;
            };

            // let richPgn = function () {
            //     if(pgn) {
            //         return pgn.split('\n\n')[1].replace(new RegExp('\n',"gm"), '<br/>');
            //     }
            //     return '';
            // };

            let movesHtml = function() {
                let html = '';
                if(moves && moves.length > 0) {
                    moves.forEach(function(move) {
                        let white = move.white ? move.white.san : '...';
                        let black = move.black ? move.black.san : '';
                        let whiteFen = move.white ? move.white.fen : '';
                        let blackFen = move.black ? move.black.fen : '';
                        let whiteClass = move.white ? '' : 'disabled';
                        let blackClass = move.black ? '' : 'disabled';

                        let whiteHidden = move.white ?
                            `
                        <input type="hidden" name="practice.recallGames[0].moves[0].white.san" value="${move.white.san}">
                        <input type="hidden" name="practice.recallGames[0].moves[0].white.uci" value="${move.white.uci}">
                        <input type="hidden" name="practice.recallGames[0].moves[0].white.fen" value="${move.white.fen}">
                        ` : '';

                        let blackHidden = move.black ?
                            `
                        <input type="hidden" name="practice.recallGames[0].moves[0].black.san" value="${move.black.san}">
                        <input type="hidden" name="practice.recallGames[0].moves[0].black.uci" value="${move.black.uci}">
                        <input type="hidden" name="practice.recallGames[0].moves[0].black.fen" value="${move.black.fen}">
                        ` : '';

                        html += `<move>
                                <index>${move.index}. <input type="hidden" name="practice.recallGames[0].moves[0].index" value="${move.index}"></index>
                                <span class = "${whiteClass}" data-fen="${whiteFen}">${white}${whiteHidden}</span>
                                <span class = "${blackClass}" data-fen="${blackFen}">${black}${blackHidden}</span>
                            </move>`;
                    });
                }
                return html;
            };

            let html = `<tr>
                       <td class="td-board">
                            <span class="mini-board cg-wrap parse-fen is2d" data-fen="${fen}"></span>
                       </td>
                       <td>
                            <div class="meta">
                                <span>${name}</span><br/>
                                <span>棋色：${colorName()}</span>
                                &nbsp;&nbsp;
                                <span>棋盘方向：${orientName()}</span>
                                &nbsp;&nbsp;
                                <span>回合数：${turnsName()}</span>
                            </div>
                            <div class="moves">${movesHtml()}</div>
                       </td>
                       <td>
                            <input type="hidden" name="practice.recallGames[0].name" value="${name}">
                            <input type="hidden" name="practice.recallGames[0].root" value="${fen}">
                            <textarea class="none" name="practice.recallGames[0].pgn">${pgn}</textarea>
                            <input type="hidden" name="practice.recallGames[0].orient" value="${orient}">
                            ${colorHtml()}
                            ${turnsHtml()}
                            <input type="hidden" name="practice.recallGames[0].title" value="${courseName}">
                            <a class="remove">移除</a>
                        </td>
                    </tr>`;
            return html;
        }

        function chessMove() {
            $page.find('.moves move span:not(.disabled)').click(function() {
                let fen = $(this).data('fen');
                let $board = $(this).parents('tr').find('.td-board .mini-board');
                Chessground($board[0], {
                    coordinates: false,
                    resizable: false,
                    drawable: { enabled: false, visible: false },
                    viewOnly: true,
                    fen: fen
                });
                $board.attr('data-fen', fen);

                $page.find('.moves move span.active').removeClass('active');
                $(this).addClass('active');
            });
        }

        resetName();
        remove();
        chessMove();
    }

    function registerDistinguishGame() {
        let $md = $('.modal-distinguish');
        let $form = $md.find('form');
        $md.find('.tabs-horiz span').click(function () {
            let $this = $(this);
            $md.find('.tabs-horiz span').removeClass("active");
            $md.find('.tabs-content div').removeClass("active");

            let cls = $this.attr('class');
            $this.addClass('active');
            $md.find('.tabs-content div.' + cls).addClass('active');
            $md.find('input[name=tab]').val(cls);
        });

        window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
            window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
                let $jq = $.noConflict(true);
                let $tree = $jq($md).find('.dbtree');
                $tree.jstree({
                    'core': {
                        'worker': false,
                        'data' : {
                            'url' : '/resource/gamedb/tree/loadAll',
                            'data' : function (node) {
                                return { 'selected': node.id };
                            }
                        },
                    },
                    'plugins' : [ 'search' ]
                })
                  .on('changed.jstree', function (e, data) {
                      if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                          if(data.node.original.type === 'file') {
                              let id = data.node.id;
                              $md.find('input[name="gamedb"]').val(id.split(':')[1]);
                          }
                      }
                  });

                let to = null;
                $md.find('.gamedb .search input[name="q"]').keyup(function () {
                    let q = $(this).val();
                    if (to) clearTimeout(to);
                    to = setTimeout(function () {
                        $tree.jstree(true).search(q, false, true);
                    }, 500);
                });
            });
        });

        $form.submit(function(e) {
            e.preventDefault();
            let max = $('.btn-distinguish-add').data('max');
            if($page.find('.distinguishGames table>tbody>tr').length >= max) {
                alert('最多添加' + max + '项');
                return false;
            }

            let chapterLink = $form.find('.study-content .chapter .chapter-list input:checked').val();
            if(chapterLink) {
                chapterLink = location.protocol + '//' + location.host + chapterLink;
                $form.find('#form3-chapter').val(chapterLink);
            }

            $form.find('#form3-courseWare').val($form.find('.courseWare-content .courseWare-list input:checked').val());

            if(!$form.find('.classic input:checked').val()
              && !$form.find('#form3-game').val()
              && !$form.find('#form3-gamedb').val()
              && !$form.find('#form3-pgn').val()
              && !$form.find('#form3-chapter').val()
              && !$form.find('#form3-courseWare').val()
            ) {
                alert('输入一种PGN获取方式');
                return false;
            }

            $.ajax({
                method: 'POST',
                url: '/distinguish/pgn',
                data: $form.serialize()
            }).then(function(res) {
                let orient = $form.find('#form3-orientation').val();
                let rightTurns = $form.find('#form3-rightTurns').val();
                let turns = $form.find('#form3-turns').val();
                let courseName = $('input[name=courseName]').val();
                let htmlRaw = distinguishHtml(res.name, res.fen, res.pgn, res.moves, orient, rightTurns, turns, courseName);
                let $html = $(htmlRaw);
                $page.find('.distinguishGames table>tbody').append($html);
                $html.find('.mini-board').each(function () {
                    let $board = $(this);
                    Chessground(this, {
                        coordinates: false,
                        resizable: false,
                        drawable: { enabled: false, visible: false },
                        viewOnly: true,
                        fen: $board.data('fen')
                    });
                });
                //lichess.pubsub.emit('content_loaded');

                remove();
                resetName();
                chessMove();
                $.modal.close();
            }, function (err) {
                handleError(err)
            });
            return false;
        });

        studyCheck($form);

        courseWareGet($form);

        $md.find('input[type=file]').on('change', function() {
            let file = this.files[0];
            if (!file) return;
            let reader = new FileReader();
            reader.onload = function(e) {
                $md.find('textarea').val(e.target.result);
            };
            reader.readAsText(file);
        });

        function remove() {
            $page.find('.distinguishGames table>tbody .remove').click(function () {
                $(this).parents('tr').remove();
                resetName();
            });
        }

        function resetName() {
            $page.find('.distinguishGames table>tbody>tr').each(function (i, e) {
                $(this).find('input,textarea').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace(/distinguishGames\[\d+\]/g, 'distinguishGames[' + i + ']'));
                });

                $(this).find('move').each(function (j, e) {
                    $(this).find('input').each(function() {
                        let name = $(this).attr('name');
                        $(this).attr('name', name.replace(/moves\[\d+\]/g, 'moves[' + j + ']'));
                    });
                });
            });
        }

        function distinguishHtml(name, fen, pgn, moves, orient, rightTurns, turns, courseName) {

            let orientName = function () {
                if(orient === 'white') return '白方';
                else if(orient === 'black') return '黑方';
                else return ''
            };

            let turnsName = function () {
                if(!turns) return '所有';
                else return turns;
            };

            let movesHtml = function() {
                let html = '';
                if(moves && moves.length > 0) {
                    moves.forEach(function(move) {
                        let white = move.white ? move.white.san : '...';
                        let black = move.black ? move.black.san : '';
                        let whiteFen = move.white ? move.white.fen : '';
                        let blackFen = move.black ? move.black.fen : '';
                        let whiteClass = move.white ? '' : 'disabled';
                        let blackClass = move.black ? '' : 'disabled';

                        let whiteHidden = move.white ?
                            `
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].white.san" value="${move.white.san}">
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].white.uci" value="${move.white.uci}">
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].white.fen" value="${move.white.fen}">
                        ` : '';

                        let blackHidden = move.black ?
                            `
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].black.san" value="${move.black.san}">
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].black.uci" value="${move.black.uci}">
                        <input type="hidden" name="practice.distinguishGames[0].moves[0].black.fen" value="${move.black.fen}">
                        ` : '';

                        html += `<move>
                                <index>${move.index}. <input type="hidden" name="practice.distinguishGames[0].moves[0].index" value="${move.index}"></index>
                                <span class = "${whiteClass}" data-fen="${whiteFen}">${white}${whiteHidden}</span>
                                <span class = "${blackClass}" data-fen="${blackFen}">${black}${blackHidden}</span>
                            </move>`;
                    });
                }
                return html;
            };

            let html = `<tr>
                       <td class="td-board">
                            <span class="mini-board cg-wrap parse-fen is2d" data-fen="${fen}"></span>
                       </td>
                       <td>
                            <div class="meta">
                                <span>${name}</span><br/>
                                <span>棋盘方向：${orientName()}</span>
                                &nbsp;&nbsp;
                                <span>走棋步数：${rightTurns}/${turnsName()}</span>
                            </div>
                            <div class="moves">${movesHtml()}</div>
                       </td>
                       <td>
                            <input type="hidden" name="practice.distinguishGames[0].name" value="${name}">
                            <input type="hidden" name="practice.distinguishGames[0].root" value="${fen}">
                            <textarea class="none" name="practice.distinguishGames[0].pgn">${pgn}</textarea>
                            <input type="hidden" name="practice.distinguishGames[0].orientation" value="${orient}">
                            <input type="hidden" name="practice.distinguishGames[0].rightTurns" value="${rightTurns}">
                            <input type="hidden" name="practice.distinguishGames[0].turns" value="${turns}">
                            <input type="hidden" name="practice.distinguishGames[0].title" value="${courseName}">
                            <a class="remove">移除</a>
                        </td>
                    </tr>`;
            return html;
        }

        function chessMove() {
            $page.find('.moves move span:not(.disabled)').click(function() {
                let fen = $(this).data('fen');
                let $board = $(this).parents('tr').find('.td-board .mini-board');
                Chessground($board[0], {
                    coordinates: false,
                    resizable: false,
                    drawable: { enabled: false, visible: false },
                    viewOnly: true,
                    fen: fen
                });
                $board.attr('data-fen', fen);

                $page.find('.moves move span.active').removeClass('active');
                $(this).addClass('active');
            });
        }

        resetName();
        remove();
        chessMove();
    }

    function registerDeadlineAt() {
        let $form = $('.modal-deadline').find('form');
        $form.find('.flatpickr').flatpickr({disableMobile: 'true', 'time_24hr': true});
    }

    $('a.btn-position-add').click(function(e) {
        e.preventDefault();
        let max = $(this).data('max');
        if($page.find('.fromPositions .tb>tbody>tr').length >= max) {
            alert('最多添加' + max + '项');
            return false;
        }

        let addPosition = function() {
            let html = positionHtml();
            $page.find('.fromPositions .tb>tbody').append(html);
        };

        let positionHtml = function() {
            const initialFen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
            return `<tr>
                       <td class="td-board"><span class="mini-board cg-wrap parse-fen is2d" data-fen="${initialFen}"></span></td>
                       <td>
                            <table class="form">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span>FEN</span>
                                            <a class="loadSituation">载入局面</a>
                                        </td>
                                        <td><input type="text" name="practice.fromPositions[0].fen" value="${initialFen}"></td>
                                    </tr>
                                    <tr>
                                        <td>基本用时</td>
                                        <td>
                                            <select name="practice.fromPositions[0].clockTime">
                                                <option value="0.0">0 分钟</option>
                                                <option value="0.25">¼ 分钟</option>
                                                <option value="0.5">½ 分钟</option>
                                                <option value="0.75">¾ 分钟</option>
                                                <option value="1.0">1 分钟</option>
                                                <option value="1.5">1.5 分钟</option>
                                                <option value="2.0">2 分钟</option>
                                                <option value="3.0">3 分钟</option>
                                                <option value="4.0">4 分钟</option>
                                                <option value="5.0" selected="selected">5 分钟</option>
                                                <option value="6.0">6 分钟</option>
                                                <option value="7.0">7 分钟</option>
                                                <option value="10.0">10 分钟</option>
                                                <option value="15.0">15 分钟</option>
                                                <option value="20.0">20 分钟</option>
                                                <option value="25.0">25 分钟</option>
                                                <option value="30.0">30 分钟</option>
                                                <option value="40.0">40 分钟</option>
                                                <option value="50.0">50 分钟</option>
                                                <option value="60.0">60 分钟</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>每步棋加时</td>
                                        <td>
                                            <select name="practice.fromPositions[0].clockIncrement">
                                                <option value="0" selected="selected">0 秒</option>
                                                <option value="1">1 秒</option>
                                                <option value="2">2 秒</option>
                                                <option value="3">3 秒</option>
                                                <option value="4">4 秒</option>
                                                <option value="5">5 秒</option>
                                                <option value="6">6 秒</option>
                                                <option value="7">7 秒</option>
                                                <option value="10">10 秒</option>
                                                <option value="15">15 秒</option>
                                                <option value="20">20 秒</option>
                                                <option value="25">25 秒</option>
                                                <option value="30">30 秒</option>
                                                <option value="40">40 秒</option>
                                                <option value="50">50 秒</option>
                                                <option value="60">60 秒</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>棋色</td>
                                        <td>
                                            <select name="practice.fromPositions[0].color">
                                                <option value="" selected="selected">随机</option>
                                                <option value="white">白</option>
                                                <option value="black">黑</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>对局数</td>
                                        <td><input type="number" name="practice.fromPositions[0].num" value="1"></td>
                                    </tr>
                                    <tr>
                                        <td>允许人机对弈</td>
                                        <td>
                                            <select name="practice.fromPositions[0].isAi">
                                                <option value="1" selected="selected">是</option>
                                                <option value="0">否</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>引擎级别</td>
                                        <td>
                                            <select name="practice.fromPositions[0].aiLevel">
                                                <option value="1" selected="selected">A.I.1（600 分）</option>
                                                <option value="2">A.I.2（800 分）</option>
                                                <option value="3">A.I.3（1000 分）</option>
                                                <option value="4">A.I.4（1200 分）</option>
                                                <option value="5">A.I.5（1400 分）</option>
                                                <option value="6">A.I.6（1600 分）</option>
                                                <option value="7">A.I.7（1800 分）</option>
                                                <option value="8">A.I.8（2000 分）</option>
                                                <option value="9">A.I.9（2500 分）</option>
                                                <option value="10">A.I.10（3000 分）</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                       </td>
                       <td><a class="remove">移除</a></td>
                    </tr>`;
        };

        addPosition();
        registerFromPosition();
        return false;
    });

    $('.goBack').click(function() {
        window.history.back();
    });

    function studyCheck($form) {
        let $studyContent = $form.find('.study-content');
        let $study = $studyContent.find('.study');
        let $studyTab = $study.find('.study-tab');
        let $studyTabHeader = $studyTab.find('.study-tab-header');
        let $studyTabContent = $studyTab.find('.study-tab-content');

        let activeStudy = 'mine';
        $studyTabHeader.find('.study-tab-item').click(function () {
            let $this = $(this);
            $this.addClass('active');
            $this.siblings('.active').removeClass('active');
            activeStudy = $this.data('id');
            readerStudy(1);
            readerTags();
        });

        let $studyList = $studyTabContent.find('.study-list tbody');
        let $chapter = $studyContent.find('.chapter');
        let $chapterList = $chapter.find('.chapter-list');
        let $pgn = $studyContent.find('.chapter-pgn');

        let $pager = $study.find('.pager');
        $pager.find('a.prev').click(function () {
            if(!$(this).hasClass('disabled')) {
                let page = $pager.data('page');
                readerStudy(page - 1);
            }
        });

        $pager.find('a.next').click(function () {
            if(!$(this).hasClass('disabled')) {
                let page = $pager.data('page');
                readerStudy(page + 1);
            }
        });

        $study.find('.study-search-btn').click(function(e){
            readerStudy(1);
        });

        let $tagGroups = $studyTabContent.find('.study-search-tags');
        let $emptyTag = $tagGroups.find('.tag-group.emptyTag');
        let $otherTag = $tagGroups.find('.tag-group.otherTag');
        let $emptyTagCbx = $emptyTag.find('input[type="checkbox"]');
        let $otherTagCbx = $otherTag.find('input[type="checkbox"]');

        function readerStudy(page) {
            let q = $study.find('.study-search input').val();
            if(q && q.trim()) {
                $emptyTagCbx.prop('checked', false);
                $otherTagCbx.prop('checked', false);
            }

            let tags = [];
            $otherTag.find('input[type="checkbox"]:checked').each(function () {
                tags.push($(this).val());
            });
            let tagData = {
                emptyTag: $emptyTagCbx.is(':checked') ? 'on' : 'off',
                tags: tags
            };

            $studyList.html(lichess.spinnerHtml);
            loadStudy(activeStudy, page, q, tagData).then(function (response) {
                if(response && response.paginator) {
                    let paginator = response.paginator;
                    $studyList.empty();
                    let lis = paginator.currentPageResults.map(function (data) {
                        let title = data.owner.title ? `<span class="title">${data.owner.title}</span>&nbsp;` : '';
                        let owner =
                            activeStudy !== 'mine' ? `<td>
                                <span class="user-link ulpt" data-href="/@/${data.owner.name}">
                                     ${title}
                                     <span class="u_name">${data.owner.name}</span>
                                </span>
                            </td>` : '';

                        return `<tr>
                            <td><a data-id=${data.id}>${data.name}</a></td>
                            ${owner}
                            <td>${data.visibility.name}</td>
                        </tr>`;
                    });
                    if (lis.length === 0) {
                        $studyList.html('<tr><td>您没有可用的研习.</td></tr>');
                    } else $studyList.html(lis.join(''));

                    if(paginator.currentPage <= 1) {
                        $pager.find('a.prev').addClass('disabled');
                    } else {
                        $pager.find('a.prev').removeClass('disabled');
                    }
                    if(paginator.currentPageResults.length < paginator.maxPerPage) {
                        $pager.find('a.next').addClass('disabled');
                    } else {
                        $pager.find('a.next').removeClass('disabled');
                    }
                    $pager.data('page', paginator.currentPage);
                    onStudyClick()
                }
            });
        }

        function onTagsClick() {
            $emptyTagCbx = $emptyTag.find('input[type="checkbox"]');
            $otherTagCbx = $otherTag.find('input[type="checkbox"]');
            $otherTagCbx.click(function () {
                $emptyTagCbx.prop('checked', false);
                $study.find('.study-search input[type="text"]').val('');
                readerStudy(1);
            });

            $emptyTagCbx.click(function () {
                $otherTagCbx.prop('checked', false);
                $study.find('.study-search  input[type="text"]').val('');
                readerStudy(1);
            });
        }
        onTagsClick();

        function readerTags() {
            loadTags(activeStudy).then(function (response) {
                $emptyTag.empty();
                $otherTag.empty();
                if(response && response.length > 0) {
                    $.each(response, function (i, d) {
                        let $tag =
                        `<span>
                            <input id="tags-${d}" name="tags[${i}]" type="checkbox" value="${d}">
                            <label for="tags-${d}">${d}</label>
                        </span>`;
                        $otherTag.append($tag);
                    });
                    $emptyTag.html(
                       `<span>
                            <input id="emptyTag" name="emptyTag" type="checkbox">
                            <label for="emptyTag">无标签</label>
                        </span>`
                    );
                    onTagsClick();
                }
            });
        }

        function onStudyClick() {
            $studyList.find('a').click(function () {
                let studyId = $(this).data('id');
                readerChapter(studyId);
            });
        }

        function readerChapter(studyId) {
            loadChapter(studyId).then(function (response) {
                if(response) {
                    $chapter.find('.study-nav').html(`回到：<a class="backStudy">${response.name}</a>`);
                    $chapterList.empty();
                    let lis = response.chapters.map(function (data, i) {
                        let checked = i === 0 ? 'checked' : '';
                        return `<li>
                                    <span>
                                        <input type="radio" id="rd-chapter-${data.id}" name="rd-chapter" value="/study/${response.id}/${data.id}" ${checked}>
                                        <label for="rd-chapter-${data.id}">${data.name}</label>
                                    </span>
                                    <a class="viewPGN" data-pgn="${toPGN(data.turns.moves)}" data-chapter="${data.name}">PGN</a>
                                </li>`;
                    });

                    if (lis.length === 0) {
                        $chapterList.html("没有可用的章节.");
                    } else $chapterList.html(lis.join(''));
                    $study.addClass('none');
                    $chapter.removeClass('none');

                    $chapter.find('.backStudy').click(function () {
                        $chapter.addClass('none');
                        $study.removeClass('none');
                        $chapterList.empty();
                    });

                    $chapter.find('.viewPGN').click(function () {
                        let pgn = $(this).data('pgn');
                        let chapter = $(this).data('chapter');
                        $pgn.find('.pgn').empty();
                        if(!pgn) {
                            $pgn.find('.pgn').html('<div class="empty">- 空PGN -</div>');
                        } else {
                            $pgn.find('.pgn').text(pgn);
                        }
                        $pgn.find('.study-nav').html(`回到：<a class="backStudy">${response.name}</a> / <a class="backChapter">${chapter}</a>`);

                        $chapter.addClass('none');
                        $pgn.removeClass('none');
                        $pgn.find('.backStudy').click(function () {
                            $pgn.addClass('none');
                            $study.removeClass('none');
                        });

                        $pgn.find('.backChapter').click(function () {
                            $pgn.addClass('none');
                            $chapter.removeClass('none');
                        });
                    });
                }
            });
        }

        function toPGN(moves) {
            if(moves && moves.length > 0) {
                let lst = moves.map(function (move) {
                    let str = `${move.index}. `;
                    if(move.white) {
                        str += move.white.san;
                    } else str += '...';
                    str += ' ';
                    if(move.black) {
                        str += move.black.san;
                    }
                    return str;
                });
                return lst.join(' ');
            } else return "";
        }

        function loadStudy(channel, page, q, tagData) {
            let url = q && q.trim() ? `/study/search?channel=${channel}&page=${page}&q=${q}` : `/study/${channel}/popular?page=${page}`;
            return $.ajax({
                url: url,
                data: tagData,
                headers: {
                    'Accept': 'application/vnd.lichess.v3+json'
                }
            });
        }

        function loadTags(channel) {
            return $.ajax({
                url: `/study/tags/${channel}`,
                headers: {
                    'Accept': 'application/vnd.lichess.v3+json'
                }
            });
        }

        function loadChapter(studyId) {
            return $.ajax({
                url: `/study/${studyId}/chapters`,
                headers: {
                    'Accept': 'application/vnd.lichess.v3+json'
                }
            });
        }

        onStudyClick();
    }

    function courseWareGet($form) {
        let $cw = $form.find('.courseWare');
        let $select = $cw.find('select');
        let $list = $cw.find('.courseWare-list');
        $select.change(function () {
            $.ajax({
                url: `/clazz/homework/courseWares?courseId=${$select.val()}`,
                headers: {
                    'Accept': 'application/vnd.lichess.v3+json'
                }
            }).then(function (response) {
                $list.empty();
                if(response && response.length > 0) {
                    let html = response.map(function (c) {
                        return `<li>
                                <input type="radio" id="${c.id}" name="courseWare" value="${c.olClassId}:${c.id}">
                                <span>${c.order}.</span>
                                <label for="${c.id}">${c.name}</label>
                            </li>`;
                    });
                    $list.html(html.join(''));
                } else {
                    $list.html('<li>您没有可用的课件.</li>');
                }
            });
        });
    }

    $page.find('.homework-form-content').submit(function (e) {
        let $form = $(this);
        let $extra = $form.find('input[id$="extra_cond"]');
        let isEmpty = false;
        $extra.each(function () {
            let val = $(this).val();
            if(!val || val === 'ratingMin=&ratingMax=&stepsMin=&stepsMax=') {
                isEmpty = true;
            }
        });
        if (isEmpty) {
            alert('项目条件不完整!');
            e.preventDefault();
            return false;
        }
    });

});

function handleError(res) {
    let json = res.responseJSON;
    if (json) {
        if (json.error) {
            if(typeof json.error === 'string') {
                alert(json.error);
            } else alert(JSON.stringify(json.error));
        } else alert(res.responseText);
    } else alert('发生错误');
}


