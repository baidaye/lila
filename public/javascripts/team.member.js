$(function () {

  memberAdvance();

  lichess.pubsub.on('content_loaded', function () {
    regModal();
    kickOrCloseSubmit();
  });

  regModal();
  kickOrCloseSubmit();
  function regModal() {
    $('a.member-edit,a.member-accept').off('click').on('click', function (e) {
      e.preventDefault();
      $.ajax({
        url: $(this).attr('href'),
        success: function (html) {
          $.modal($(html));
          $('.cancel').click(function () {
            $.modal.close();
          });
          $('.flatpickr').flatpickr();
          editSubmit();
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
      return false;
    });
  }

  function editSubmit() {
    let $form = $('.member-editform');
    $form.submit(function (e) {
      e.preventDefault();
      $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          alert(res.responseText);
        }
      });
      return false;
    });
  }

  function kickOrCloseSubmit() {
    $('.member-kick, .member-close').each(function () {
      let $form = $(this);
      $form.off('submit').on('submit', function (e) {
        e.preventDefault();
        let cfm = $form.find('.button').attr('title');
        if(confirm(cfm)) {
          $.ajax({
            method: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (res) {
              if (res.ok) {
                location.reload();
              } else {
                alert(res.err)
              }
            },
            error: function (res) {
              alert(res.responseText);
            }
          });
        }
        return false;
      });
    });
  }

  $('.sorter a').click(function () {
    let $this = $(this);
    let sortField = $this.data('sortField');
    let sortType = $this.data('sortType');

    let $sortField = $('#form3-sortField');
    let $sortType = $('#form3-sortType');

    let oldSortField = $sortField.val();
    let oldSortType = $sortType.val();

    if (sortField === oldSortField && sortType === oldSortType) {
      $sortField.val('');
      $sortType.val('');
    } else {
      $sortField.val(sortField);
      $sortType.val(sortType);
    }

    $('.member-search').submit();
  });

});
