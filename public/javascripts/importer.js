$(function() {

  let $page = $('.resource .imported_form');

  $page.find('a.modal-alert').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        applyPuzzleModal();
        applyPgnModal();
      },
      error: function (res) {
        handleError(res);
      }
    });
    return false;
  });


  function applyPuzzleModal() {
    let $modal = $('.puzzle-import');
    let $form = $modal.find('form');

    if($modal.length > 0) {
      $modal.find('#form3-puzzleTag').tagsInput({
        "height": "40px",
        "width": "100%",
        "interactive": true,
        "defaultText": "添加",
        "removeWithBackspace": true,
        "minChars": 0,
        "maxChars": 10,
        "placeholderColor": "#666666"
      });

      if (window.FileReader) {
        $modal.find('input[type=file]').on('change', function() {
          let file = this.files[0];
          if (!file) return;
          let maxSize = 2 * 1024 * 1024;
          if(file.size > maxSize) {
            alert('最大支持2M文件上传');
            return;
          }
          let reader = new FileReader();
          reader.onload = function(e) {
            $modal.find('textarea').val(e.target.result);
          };
          reader.readAsText(file);
        });
      } else $modal.find('.upload').remove();

      $form.submit(function (e) {
        e.preventDefault();
        $.ajax({
          method: 'POST',
          url: '/import/puzzle',
          data: $modal.find('form').serialize()
        }).then(function() {
          location.reload();
          $.modal.close();
        }, function (err) {
          handleError(err)
        });
      });
    }
  }

  function applyPgnModal() {
    let $modal = $('.pgn-import');
    let $form = $modal.find('form');
    if($modal.length > 0) {
      $modal.find('#form3-puzzleTag').tagsInput({
        "height": "40px",
        "width": "100%",
        "interactive": true,
        "defaultText": "添加",
        "removeWithBackspace": true,
        "minChars": 0,
        "maxChars": 10,
        "placeholderColor": "#666666"
      });

      if (window.FileReader) {
        $modal.find('input[type=file]').on('change', function() {
          let file = this.files[0];
          if (!file) return;
          let maxSize = 2 * 1024 * 1024;
          if(file.size > maxSize) {
            alert('最大支持2M文件上传');
            return;
          }
          let reader = new FileReader();
          reader.onload = function(e) {
            $modal.find('textarea').val(e.target.result);
          };
          reader.readAsText(file);
        });
      } else $modal.find('.upload').remove();

      $form.submit(function (e) {
        e.preventDefault();
        $.ajax({
          method: 'POST',
          url: '/import/pgn',
          data: $modal.find('form').serialize()
        }).then(function() {
          location.reload();
          $.modal.close();
        }, function (err) {
          handleError(err)
        });
      });
    }
  }

  function handleError(res) {
    let json = res.responseJSON;
    if (json) {
      if (json.error) {
        if (typeof json.error === 'string') {
          alert(json.error);
        } else alert(JSON.stringify(json.error));
      } else alert(res.responseText);
    } else alert('发生错误');
  }
});
