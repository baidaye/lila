$(function() {

/*  var $variant = $('#form3-variant');
  var $position = $('.form3 .position');
  function showPosition() {
    $position.toggleNone($variant.val() == 1);
  };
  $variant.on('change', showPosition);
  showPosition();*/

  let $form = $('.tour__form');

  $form.find('.conditions a.show').on('click', function() {
    $(this).remove();
    $form.find('.conditions').addClass('visible');
  });

  $(".tour__form .flatpickr").flatpickr({
    time_24hr: true,
    minDate: 'today',
    maxDate: new Date(Date.now() + 1000 * 3600 * 24 * 31),
    dateFormat: 'Z',
    altInput: true,
    altFormat: 'Y-m-d h:i K'
  });

  $form.find('#form3-position').change(function() {
    let text = $(this).find("option:selected").text();
    if(text === '初始局面') {
      $form.find('.board-link').addClass('none');
      $form.find('.position-paste').addClass('none');
    } else if(text === '输入FEN') {
      $('.board-link').removeClass('none');
      $('.position-paste').removeClass('none');
      $('.position-paste').val('');
      $('.starts-position').find('.preview').empty();
    } else if(text === '载入局面') {
      if($('main').data('notaccept') == true) {
        window.lichess.memberIntro();
        return false;
      }

      $('.board-link').removeClass('none');
      $('.position-paste').removeClass('none');
      $('.position-paste').val('');
      $('.starts-position').find('.preview').empty();
      loadSituation();
    } else {
      $form.find('.board-link').removeClass('none');
      $form.find('.position-paste').addClass('none');
      validateFen($(this).val());
    }
  });

  function validateFen(fen) {
    let $position = $form.find('.starts-position');
    let $board = $position.find('.preview');
    if (fen) {
      $.ajax({
        url: '/setup/validate-fen?strict=0',
        data: {
          fen: fen
        },
        success: function(data) {
          $board.html(data);
          $position.removeClass('is-invalid');
          $position.find('a.board-link').attr('href', '/editor/' + fen);
          let $option = $('#form3-position').find('option:selected');
          let dataId = $option.data('id');
          if(dataId === 'option-load-fen' || dataId === 'option-load-situation') {
            $option.val(fen);
          }
          lichess.pubsub.emit('content_loaded');
        },
        error: function() {
          $board.empty();
          $position.addClass('is-invalid');
        }
      });
    }
  }

  $form.find('.position-paste').on('input propertychange', function () {
    validateFen($(this).val())
  });

  function loadSituation() {
    let ts = this;
    let fen = '';
    $.ajax({
      url: `/resource/situationdb/select?mustStandard=true`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });
      window.lichess.pubsub.emit('content_loaded');

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-selector').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            },
            'plugins' : [ 'search' ]
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                search();
              }
            });

          let to = null;
          $form.find('.search input[name="q"]').keyup(function () {
            let q = $(this).val();
            if (to) clearTimeout(to);
            to = setTimeout(function () {
              $tree.jstree(true).search(q, false, true);
            }, 500);
          });

          $form.find('#btn-search').click(function () {
            search();
          });

          let $emptyTag = $form.find('.search_form')
            .find('.tag-group.emptyTag')
            .find('input[type="checkbox"]');

          let $tags = $form.find('.search_form')
            .find('.tag-group')
            .find('input[type="checkbox"]');

          $tags.not($emptyTag).click(function () {
            $emptyTag.prop('checked', false);
            search();
          });

          $emptyTag.click(function () {
            $tags.not($emptyTag).prop('checked', false);
            search();
          });

          let $situations = $form.find('.situations');
          function search() {
            let situation = $tree.jstree(true).get_selected(null)[0];
            $.ajax({
              url: `/resource/situationdb/rel/list?selected=${situation}`,
              data: $form.serialize()
            }).then(function (rels) {
              $situations.empty();

              if(!rels || rels.length === 0) {
                let noMore =
                  `<div class="no-more">
                                    <i data-icon="4">
                                    <p>没有更多了</p>
                                  </div>`;
                $situations.html(noMore);
              } else {
                let situations =
                  rels.map(function (rel) {
                    let tags = '';
                    if(rel.tags && rel.tags.length > 0) {
                      rel.tags.map(function (tag) {
                        tags = `<span>${tag}</span>`;
                      });
                    }
                    return `<a class="paginated" target="_blank" data-id="${rel.id}">
                                              <div class="mini-board cg-wrap parse-fen is2d" data-color="white" data-fen="${rel.fen}">
                                                <cg-helper>
                                                  <cg-container>
                                                    <cg-board></cg-board>
                                                  </cg-container>
                                                </cg-helper>
                                              </div>
                                              <div class="btm">
                                                <label>${rel.name ? rel.name : ''}</label>
                                                <div class="tags">${tags}</div>
                                              </div>
                                            </a>`;
                  });

                $situations.html(situations);
                registerEvent();
                window.lichess.pubsub.emit('content_loaded')
              }
            })
          }

          function registerEvent() {
            let $boards = $situations.find('.paginated');
            $boards.off('click');
            $boards.click(function(e) {
              e.preventDefault();
              let $this = $(this);
              $boards.removeClass('selected');
              $this.addClass('selected');
              fen = $this.find('.mini-board').data('fen');
              return false;
            });
          }
          registerEvent();

          $form.submit(function (e) {
            e.preventDefault();
            $('.position-paste').val(fen);
            validateFen(fen);
            $.modal.close();
            return false;
          });
        })
      });
      return false
    });
  };

});
