$(function() {

  let $page = $('.timetable');

  $page.find('.course').not('.stopped').click(function () {
    let $this = $(this);
    $page.find('.course').not($this).removeClass('active');
    if ($this.hasClass('active')) {
      $this.removeClass('active');
    } else {
      $this.addClass('active');
    }
    handleActionButton();
  });

  $page.find('.course').dblclick(function () {
    let $this = $(this);
    let id = $this.data('id');
    let type = $this.data('type');
    let clazz = $this.data('clazz');
    let index = $this.data('index');
    let homework = $this.data('homework');

    if(type === 'trainCourse') {
      location.href = `/trainCourse/${id}/coach`;
    } else {
      if(homework) {
        location.href = `/clazz/homework/report?id=${clazz}@${id}@${index}`;
      } else {
        location.href = `/clazz/homework/create?clazzId=${clazz}&courseId=${id}`;
      }
    }
  });

  registerModal();
  function registerModal() {
    $page.find('.box__top .action .button').click(function(e) {
      e.preventDefault();
      let $this = $(this);
      let $active = $page.find('.course.active');
      let id = $active.data('id');
      let type = $active.data('type');
      let url = $this.attr('href').replace('#id#', id);
      if(type === 'trainCourse') {
        url = $this.data('traincoursehref').replace('idval', id);
      }

      $.ajax({
        url: url,
        success: function(html) {
          $.modal($(html));
          $('.cancel').click(function () {
            $.modal.close();
          });

          onOpenCourseUpdateModal();
          onOpenTrainCourseFromModal();
          onOpenTrainCourseRemoveModal();
        },
        error: function(res) {
          if (res.status === 400) alert(res.responseText);
        }
      });
      return false;
    });
  }

  function onOpenTrainCourseFromModal() {
    let $modal = $('.modal-train-fm');
    let $form = $modal.find('.form3');
    let $transfer = $modal.find('.transfer');
    let $leftPanel = $transfer.find('.left');
    let $leftList = $leftPanel.find('.transfer-panel-list table tbody');

    let onChange = (checkeds) => {
      $modal.find('input[name=studentIds]').val(checkeds.join(','));
    };
    transfer(onChange);

    let filter = (clazzId, stus) => {
      let allIds = [];
      $leftList.find('input').each(function () {
        allIds.push($(this).val());
      });

      let filterIds = [];
      if(clazzId === 'all') {
        filterIds = allIds;
      } else if(clazzId === 'none') {
        filterIds = allIds.filter(id => {
          return !stus.includes(id);
        });
      } else {
        filterIds = stus;
      }

      $leftList.find('tr').addClass('none');
      filterIds.forEach(function (id) {
        $leftList.find('#chk_' + id).parents('tr').removeClass('none');
      });
    };

    $form.find('.clazz-select').change(function() {
      let clazzId = $(this).val();
      let stus = $form.find('.clazz-select').find('option:selected').data('attr');
      filter(clazzId, stus);
    });

    $form.find('.flatpickr').flatpickr({disableMobile: 'true', 'time_24hr': true});
    $form.submit(function (e) {
      e.preventDefault();

      let studentIds = $modal.find('input[name=studentIds]').val();
      if(!studentIds) {
        alert('请选择学员');
        return false;
      }

      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        data: $form.serialize(),
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
        }
      });
      return false;
    });

    let toMinutes = (time) => {
      let hours = parseInt(time.split(':')[0]);
      let minutes = parseInt(time.split(':')[1]);
      return hours * 60 + minutes;
    }
  }

  function onOpenTrainCourseRemoveModal() {
    let $modal = $('.modal-train-remove');
    let $form = $modal.find('.form3');
    $form.submit(function (e) {
      e.preventDefault();

      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
        }
      });
      return false;
    });
  }

  function onOpenCourseUpdateModal() {
    let $modal = $('.course-update');
    $modal.find('.flatpickr').flatpickr({disableMobile: 'true'});
    $modal.find('form').submit(function(e) {
      e.preventDefault();
      let $form = $(this);
      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        data: {
          'date': $form.find('#form3-date').val(),
          'timeBegin': $form.find('#form3-timeBegin').val(),
          'timeEnd': $form.find('#form3-timeEnd').val()
        },
        success: function() {
          location.reload();
        },
        error: function(res) {
          alert(res.responseText);
        }
      });
      return false;
    });
  }

});

function handleActionButton() {
  let $active = $('.course.active');
  let $action = $('.action .clazzAction');
  let editable = $active.hasClass('editable');
  if(editable) {
    let clazzType = $active.data('type');
    $active.addClass('disabled').prop('disabled', true);
    if ($active.length === 1) {
      if (clazzType === 'train' || clazzType === 'trainCourse') {
        $action.addClass('disabled').prop('disabled', true);
        $action.not('.action-postpone').removeClass('disabled').prop('disabled', false);
      } else {
        $action.removeClass('disabled').prop('disabled', false);
      }
    } else {
      $action.addClass('disabled').prop('disabled', true);
    }
  } else {
    $action.addClass('disabled').prop('disabled', true);
  }
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
