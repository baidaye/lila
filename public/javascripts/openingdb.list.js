$(function() {

  let $page = $('.openingdb-list');
  let $form = $page.find('.box__top-tags');

  let $emptyTag = $form.find('.tag-group.emptyTag').find('input[type="checkbox"]');
  let $tags = $form.find('.tag-group').find('input[type="checkbox"]');

  $tags.not($emptyTag).click(function () {
    $emptyTag.prop('checked', false);
    $form.submit();
  });

  $emptyTag.click(function () {
    $tags.not($emptyTag).prop('checked', false);
    $form.submit();
  });

  $page.find('a.create').click(function(e) {
    e.preventDefault();
    let $this = $(this);
    let href = $this.attr('href');
    if($this.data('notaccept') == true) {
      location.href = '/resource/openingdb/toBuyCreate';
    } else {
      location.href = href;
    }
  });

  $page.find('a.modal-alert').click(function(e) {
    e.preventDefault();
    let $this = $(this);
    let href = $this.attr('href');
    $.ajax({
      url: href,
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        onSystemSetting();
      },
      error: function(res) {
        alert(res.responseText);
      }
    });
    return false;
  });

  function onSystemSetting() {
    let $form = $('.openingdb-system-setting .form3');
    $form.find('#form3-tags').tagsInput({
      'height': '40px',
      'width': '100%',
      'interactive': true,
      'defaultText': '添加',
      'removeWithBackspace': true,
      'minChars': 0,
      'maxChars': 10,
      'placeholderColor': '#666666'
    });
  }

});

