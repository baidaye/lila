$(function () {

  dateFormat();
  applyTab();
  applyModal();
  applySetAutoPairing();
  applyPlayerSort();
  applyPairing();
  applyPublishPairing();
  applyCancelPublishPairing();
  applySetRoundStartTime();
  applySetRounds();
  applyRoundRobinPairing();
  applyPlayerExport();
  applyScoreExport();

  $(window).scrollTop($.cookie('haichess-contest-scrollTop'));
  $(window).scroll(function() {
    $.cookie('haichess-contest-scrollTop', document.documentElement.scrollTop);
  });

});

function applyTab() {
  let $page = $('.contest-show');
  let $flow = $page.find('.flow');
  let $tabNav = $flow.find('.tab-nav');
  let $tabPanel = $flow.find('.tab-panel');
  let $navs = $tabNav.find('.nav');
  let $panels = $tabPanel.find('.panel');
  $navs.not('.disabled').click(function () {
    let $this = $(this);
    let active = $this.data('tab');
    $navs.removeClass('active');
    $this.addClass('active');

    $panels.removeClass('active');
    $panels.filter(`.${active}`).addClass('active');
    location.hash = active;
  });

  setTabActive();
  function setTabActive() {
    let hash = location.hash;
    if(!$navs.has(`[data-tab="${hash}"]`).hasClass('disabled')) {
      if(hash) {
        hash = hash.replace('#', '');
        $navs.removeClass('active');
        $navs.filter(`[data-tab="${hash}"]`).addClass('active');

        $panels.removeClass('active');
        $panels.filter(`.${hash}`).addClass('active');
      }
    }
  }

  setHScroll();
  function setHScroll() {
    let hash = location.hash;
    let $navScroll = $tabNav.find('.nav-scroll');
    let $navScrollHorizontal = $tabNav.find('.nav-scroll-horizontal');
    let $navScrollBars = $navScroll.find('.nav-bars');
    let navWidth = $navScrollHorizontal.width();
    let barsWidth = $navScrollBars.width();

    let minWidth = 0;
    let maxWidth = Math.floor(barsWidth - navWidth);
    let tick = 95;
    let page = Math.max(Math.floor(navWidth / tick), 1) * tick;
    if(maxWidth > 0) {
      let defaultTranslateX = 0;
      let currentRound = $page.data('currentround');
      if(hash && hash.indexOf('round') > -1 && hash.indexOf('roundEdit') === -1) {
        let roundNo = parseInt(hash.replace('#round', ''));
        defaultTranslateX = Math.max(maxWidth * -1, (roundNo - 1) * tick * -1);
      } else if(currentRound) {
        let roundNo = parseInt(currentRound);
        defaultTranslateX = Math.max(maxWidth * -1, (roundNo - 1) * tick * -1);
      }
      $navScrollBars.css('transform', `translateX(${defaultTranslateX}px)`);

      $navScroll.find('.nav-scroll-prev').click(function () {
        let nowTransform = $navScrollBars[0].style.transform;
        let nb = parseInt(nowTransform.match(/^translateX\((-?\d+)px\)$/)[1]);
        let newNb = Math.min(minWidth, nb + page);
        $navScrollBars.css('transform', `translateX(${newNb}px)`);
      });

      $navScroll.find('.nav-scroll-next').click(function () {
        let nowTransform = $navScrollBars[0].style.transform;
        let nb = parseInt(nowTransform.match(/^translateX\((-?\d+)px\)$/)[1]);
        let newNb = Math.max(maxWidth * -1, nb - page);
        $navScrollBars.css('transform', `translateX(${newNb}px)`);
      });
    } else {
      $navScroll.find('.nav-scroll-prev').addClass('none');
      $navScroll.find('.nav-scroll-next').addClass('none');
    }
  }

}

function applyModal() {
  $('a.modal-alert').click(function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function(html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });

        inviteModal();
        playerChooseModal();
        playerForbiddenModal();
        manualAbsentModal();
        manualPairingModal();
        roundSwapModal();
        apptSetBoardTimeModal();
        applySetBoardSignModal();
        apptSetModal();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function applySetAutoPairing() {
  $('#autoPairing').change(function() {
    let checked = $(this).prop("checked");
    let cfm = checked ? '是否自动编排和发布成绩？' : '是否取消自动编排和发布成绩？';
    if (confirm(cfm)) {
      $.ajax({
        method: 'POST',
        url: $(this).parents('form').attr('action'),
        data: { 'autoPairing': checked ? '1' : '0' },
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
        }
      });
    } else $(this).prop("checked", !checked);
  });
}

function applyPlayerSort() {
  let $page = $('.contest-show');
  let $flow = $page.find('.flow');
  let $enterTable = $flow.find('.enter .slist').not('.unsortable');
  let $waiting = $('.contest-show .waiting');
  $enterTable.tableDnD({
    onDragClass: 'dragClass',
    onDrop: function(t, r) {
      let playerIds = [];
      $enterTable.find('tbody tr').each(function() {
        playerIds.push($(this).data('id'));
      });
      let contestId = $('main').data('id');

      $waiting.removeClass('none');
      $.ajax({
        method: 'POST',
        url: `/contest/${contestId}/reorderPlayer`,
        data: {'playerIds': playerIds.join(',')},
        success: function() {
          setTimeout(function () {
            $enterTable.find('tbody tr').each(function(i) {
              $(this).find('td').first().text(i + 1);
            });
            $waiting.addClass('none');
          }, 500);
        },
        error: function(res) {
          handleError(res);
        }
      });
    }
  });
}

function applyPairing() {
  $('.pairing').parent('form').submit(function(e) {
    e.preventDefault();
    if (confirm('新生成的对战表将覆盖本轮原有对战表，是否继续？')) {
      let id = $(this).data('contest-id');
      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
          location.reload();
        }
      });
    }
    return false;
  });
}

function applyPublishPairing() {
  let $btn = $('.publish-pairing');
  $btn.parent('form').submit(function(e) {
    e.preventDefault();
    if (confirm('发布对战表后将无法继续调整本轮对战表，是否继续？')) {
      $btn.prop('disabled', true).addClass('disabled').text('正在发布对战表...');
      let id = $(this).data('contest-id');
      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        success: function() {
          setTimeout(() => {
            location.reload();
          }, 1000)
        },
        error: function(res) {
          handleError(res);
          location.reload();
        }
      });
    }
    return false;
  });
}

function applyCancelPublishPairing() {
  let $btn = $('.cancel-publish-pairing');
  $btn.parent('form').submit(function(e) {
    e.preventDefault();
    if (confirm('撤回已发布的对战表后您可以重新编排并再次发布对战表，是否继续？')) {
      $btn.prop('disabled', true).addClass('disabled').text('正在撤回对战表...');
      $.ajax({
        method: 'POST',
        url: $(this).attr('action'),
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
          location.reload();
        }
      });
    }
    return false;
  });
}

function applySetRoundStartTime() {
  $('input[name="roundStartsTime"]').flatpickr({
    time_24hr: true,
    minuteIncrement: 1,
    altFormat: 'Y-m-d H:i',
    onClose: function (d1, d2) {
      if (confirm('是否更新轮次开始时间？')) {
        let contestId = $('input[name="roundStartsTime"]').data('contest-id');
        let roundNo = $('input[name="roundStartsTime"]').data('round-no');
        let startsAt = $('input[name="roundStartsTime"]').val();
        $.ajax({
          method: 'POST',
          url: '/contest/' + contestId + '/round/startsTime?rno=' + roundNo,
          data: { 'startsAt': startsAt },
          success: function() {
            location.reload();
          },
          error: function(res) {
            handleError(res);
          }
        });
      }
    }
  });
}

function applySetRounds() {
  let $page = $('.contest-show');
  let $roundEdit = $page.find('.roundEdit');
  let $roundsForm = $roundEdit.find('.form3');
  let $rounds = $roundEdit.find('.roundEdit-list tbody');
  let $waiting = $roundEdit.find('.waiting');

  let maxRound = $roundEdit.find('input[name="basics-maxRound"]').val();
  let startsAt = $roundEdit.find('input[name="basics-startsAt"]').val();
  let spaceDay = $roundEdit.find('input[name="rounds-spaceDay"]').val();
  let spaceHour = $roundEdit.find('input[name="rounds-spaceHour"]').val();
  let spaceMinute = $roundEdit.find('input[name="rounds-spaceMinute"]').val();

  let spaceMi = spaceDay * 24 * 60 * 60 * 1000 + spaceHour * 60 * 60 * 1000 + spaceMinute* 60 * 1000;
  let startsAtMi = new Date(startsAt).getTime();

  $rounds.find('.flatpickr').change(function () {$roundsForm.submit(); }).flatpickr(datePickOption());

  applyControl();
  function applyControl() {

    let $trs = $rounds.find('.round-line');
    let len = $trs.length;
    let rm = '<a class="rm" title="移除">-</a>';
    let ad = '<a class="ad" title="添加">+</a>';

    let canEdit = $roundEdit.find('input[name="canEdit"]').val() === "true";
    $trs.each(function (i, _) {
      let $ct = $(this).find('.control');
      $ct.empty();
      if(canEdit) {
        if (len === 1) {
          $ct.append(ad);
        } else {
          if (i === len - 1) {
            $ct.append(rm);
            if (len < maxRound) {
              $ct.append(ad);
            }
          }
        }
      }
    });
    registerListener();
  }

  function registerListener() {
    $rounds.find('.ad').click(function () {
      let $trs = $rounds.find('.round-line');
      let len = $trs.length;
      let $prev = $trs.eq(len - 1);
      let prevDate = $prev.find('.flatpickr').val();
      let sd = new Date(prevDate).getTime() + spaceMi;
      let $el = $(buildElement(len, new Date(sd).format("yyyy-MM-dd hh:mm")));
      $rounds.append($el);
      applyControl();
      $el.find('.flatpickr')
        .change(function () {
          $roundsForm.submit();
        }).flatpickr(datePickOption());
      $roundsForm.submit();
    });

    $rounds.find('.rm').click(function () {
      $(this).parents('.round-line').remove();
      applyControl();
      $roundsForm.submit();
    });
  }

  function buildElement(index, sd) {
    return `<tr class="round-line">
        <td class="no"><span>第${index + 1}轮</span></td>
        <td><input name="list[${index}]" value="${sd}" data-enable-time="true" data-time_24h="true" class="form-control flatpickr flatpickr-input" type="text" readonly="readonly"></td>
        <td class="control"></td>
        <td class="error"></td>
      </tr>`;
  }

  function datePickOption() {
    return {
      time_24hr: true,
      altFormat: 'Y-m-d H:i'
    }
  }

  function validRoundNum(nbPlayers) {
    let $baseInfo = $page.find('.head__info');
    let rule = $baseInfo.find('input[name="rule"]').val();
    let rounds = $rounds.find('.round-line').length;

    let robinRoundsByPlayer = () => {
      if (nbPlayers === 0) {
        return rounds;
      } else {
        if (nbPlayers % 2 === 0) {
          return nbPlayers - 1;
        } else
          return nbPlayers;
      }
    };

    let robinRoundsByPlayerOfRule = () => {
      if(rule === 'round-robin') {
        return robinRoundsByPlayer();
      } else if(rule === 'db-round-robin') {
        return robinRoundsByPlayer() * 2;
      } else {
        return -1;
      }
    };

    let r = robinRoundsByPlayerOfRule();
    if(r !== rounds) {
      $roundsForm.find('.roundNumError').removeClass('none');
    } else {
      $roundsForm.find('.roundNumError').addClass('none');
    }
  }

  $roundsForm.submit(function(e) {
    e.preventDefault();
    let $this = $(this);
    $waiting.removeClass('none');
    $rounds.find('.round-line .error').text('');
    $roundsForm.find('.otherError').text('').addClass('none');
    $.ajax({
      method: 'POST',
      url: $this.attr('action'),
      data: $this.serialize(),
      success: function(response) {
        //location.reload();
        validRoundNum(response.nbPlayers);
        $waiting.addClass('none');
      },
      error: function(res) {
        if(res.status === 400) {
          let errors = res.responseJSON;
          if(errors.global) {
            $roundsForm.find('.otherError').text(errors.global).removeClass('none');
          } else {
            for (let key in errors) {
              let index = key.match(/^list\[(\d{1,2})\]$/)[1];
              $rounds.find('.round-line').eq(index).find('.error').text(errors[key]);
              $roundsForm.find('.otherError').text(errors[key]).removeClass('none');
            }
          }
        } else handleError(res);
        $waiting.addClass('none');
      }
    });
    return false;
  });
}

function applyRoundRobinPairing() {
  let $page = $('.contest-show');
  let $roundEdit = $page.find('.roundEdit');
  let $roundsForm = $roundEdit.find('.form3');
  let $roundErrors = $roundsForm.find('.global-error li');
  let $btn = $roundEdit.find('.pairingRoundRobin');

  $btn.not('.disabled').click(function (e) {
    e.preventDefault();
    let errors = [];
    $roundErrors.each(function () {
      let $this = $(this);
      if(!$this.hasClass('none')) {
        errors.push($this.text());
      }
    });

    if(errors.length) {
      alert(errors.join('；'));
      return;
    }

    if(confirm('循环赛将对所有轮次进行编排且仅能进行一次，是否继续操作？')) {
      $btn.text('正在编排, 请稍后...').addClass('disabled');
      let url = $(this).attr('href');
      $.ajax({
        method: 'POST',
        url: url,
        success: function() {
          location.reload();
        },
        error: function(res) {
          handleError(res);
        }
      });
    }
  });
}

function inviteModal() {
  let $modal = $('.contest-invite');
  $modal.find('.user-autocomplete').each(function() {
    let opts = {
      focus: 1,
      friend: $(this).data('friend'),
      tag: $(this).data('tag')
    };
    if ($(this).attr('autofocus')) lichess.userAutocomplete($(this), opts);
    else $(this).one('focus', function() {
      lichess.userAutocomplete($(this), opts);
    });
  });

  $('.contest-invite .form3').submit(function(e) {
    e.preventDefault();
    let username = $modal.find('input[name="username"]').val();
    let id = $modal.find('input[name="contestId"]').val();
    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { "username": username },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function playerChooseModal() {
  let $page = $('.contest-show');
  let $modal = $('.modal-player-choose');
  let $transfer = $modal.find('.transfer');
  let $leftPanel = $transfer.find('.left');
  let $leftList = $leftPanel.find('.transfer-panel-list table tbody');
  let oldPlayerIds = $modal.find('input[name=players]').val();
  let checkedPlayerIds = oldPlayerIds ? oldPlayerIds.split(',') : [];

  let onChange = (checkeds) => {
    checkedPlayerIds = checkeds;
    //$modal.find('input[name=players]').val(checkeds.join(','));
  };

  search();
  memberAdvance();
  transfer(onChange);

  function search() {
    let $searchForm = $modal.find('.member-search');

    $searchForm.find('#member_name').keydown((e) => {
      if(e.keyCode === 13){
        e.preventDefault();
        $searchForm.find('.search').trigger('click');
      }
    });

    $searchForm.submit(function(e) {
      e.preventDefault();

      let username = $searchForm.find('#form3-username').val();
      let campus = $searchForm.find('#form3-campus').val();
      let clazzId = $searchForm.find('#form3-clazzId').val();
      let teamRatingMin = $searchForm.find('#form3-teamRatingMin').val();
      let teamRatingMax = $searchForm.find('#form3-teamRatingMax').val();
      let level = $searchForm.find('#form3-level').val();
      let sex = $searchForm.find('#form3-sex').val();
      let singleTags = $searchForm.data('single-tags');
      let rangeTags = $searchForm.data('range-tags');
      if(!singleTags) {
        singleTags = [];
      }
      if(!rangeTags) {
        rangeTags = [];
      }

      let arr = [];
      $leftList.find('input').each(function () {
        arr.push($(this).data('attr'));
      });

      let filterSingleTag = (n) => {
        return singleTags.reduce((prev, field, index) => {
          let tagValue = $searchForm.find(`#form3-fields_${index}_fieldValue`).val();
          return prev && (!tagValue || (n[field] && n[field].includes(tagValue)));
        }, true);
      };

      let filterRangeTag = (n) => {
        return rangeTags.reduce((prev, field, index) => {
          let tagValueMin = $searchForm.find(`#form3-rangeFields_${index}_min`).val();
          let tagValueMax = $searchForm.find(`#form3-rangeFields_${index}_max`).val();
          return prev && (!tagValueMin || (n[field] && n[field] >= tagValueMin)) && (!tagValueMax || (n[field] && n[field] <= tagValueMax));
        }, true);
      };

      let filterIds = arr.filter(n => {
        return (
          (!username || n.name.includes(username)) &&
          (!campus || (n.campus && n.campus.includes(campus))) &&
          (!clazzId || (n.clazz && n.clazz.includes(clazzId))) &&
          (!teamRatingMin || n.teamRating >= teamRatingMin) &&
          (!teamRatingMax || n.teamRating <= teamRatingMax) &&
          (!level || n.level === level) &&
          (!sex || n.sex === sex) &&
          filterSingleTag(n) &&
          filterRangeTag(n)
        )
      }).map(n => n.id);

      $leftList.find('tr').addClass('none');
      filterIds.forEach(function (id) {
        $leftList.find('#chk_' + id).parents('tr').removeClass('none');
      });

      return false;
    });
  }

  let $transferForm = $modal.find('.player-choose-transfer');
  $transferForm.submit(function(e) {
    e.preventDefault();

    // let maxPlayers = $page.find('input[name="maxPlayers"]');
    // if(checkedPlayerIds.length > maxPlayers) {
    //   alert(`选择的棋手数量超过最大限制（${maxPlayers}）`);
    //   return false;
    // }

    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { "players": checkedPlayerIds.join(',') },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
  })
}

function playerForbiddenModal() {
  let $modal = $('.contest-forbidden');
  let $form = $modal.find('form');

  let onChange = (checkeds) => {
    $modal.find('input[name=playerIds]').val(checkeds.join(','));
  };
  transfer(onChange);

  $form.submit(function(e) {
    e.preventDefault();
    let players = $modal.find('input[name=playerIds]').val();
    if(!players) {
      alert('请选择回避棋手');
      return false;
    }
    $.ajax({
      method: 'POST',
      url: $(this).attr('action'),
      data: $form.serialize(),
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function manualAbsentModal() {
  let $modal = $('.contest-absent');
  let $form = $modal.find('form');
  let joins = [];
  let absents = [];

  let onChange = (rights, lefts) => {
    joins = lefts;
    absents = rights;
  };
  transfer(onChange);

  $form.submit(function(e) {
    e.preventDefault();
    let id = $form.find('input[name="contestId"]').val();
    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { 'joins': joins, 'absents': absents },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function roundSwapModal() {
  let $modal = $('.contest-swap');
  let $form = $modal.find('form');
  let $left = $form.find('.contest-swap-rounds-left');
  let $right = $form.find('.contest-swap-rounds-right');
  $left.find('input[name="contest-swap-rd"]').change(function () {
    let roundId = $(this).val();
    $right.find('.contest-swap-round').addClass('none');
    $right.find(`div[data-id="${roundId}"]`).removeClass('none');

    $left.find('.radio').removeClass('active');
    $(this).parent('.radio').addClass('active');
  });

  $form.submit(function(e) {
    e.preventDefault();
    let roundId = $form.find('input[name="contest-swap-rd"]:checked').val();
    if(!roundId) {
      alert('请选择要选择的轮次');
      return;
    }

    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { 'roundId': roundId },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function manualPairingModal() {
  let $modal = $('.manual-pairing');
  let $lst = $modal.find('.manual-list .slist');
  $modal.find('.manual-filter-search').on('input propertychange', function() {
    let txt = $(this).val();
    if($.trim(txt) != ''){
      $lst.find('tbody tr').not('tr:contains("' + txt + '")').css('display', 'none');
      $lst.find('tbody tr').filter('tr:contains("' + txt + '")').css('display', 'table-row');
    } else {
      $lst.find('tbody tr').css('display', 'table-row');
    }
  });

  $modal.find('.form3').submit(function(e) {
    e.preventDefault();
    let id = $modal.find('input[name="contestId"]').val();
    let source = JSON.parse($modal.find('input[name="source"]').val());
    let target = JSON.parse($lst.find('input:checked').val());
    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { 'source.isBye': source.isBye, 'source.board': source.board, 'source.color': source.color, 'source.player': source.player,
        'target.isBye': target.isBye, 'target.board': target.board, 'target.color': target.color, 'target.player': target.player },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function apptSetBoardTimeModal() {
  let $modal = $('.board-time');
  $modal.find('.flatpickr').flatpickr({time_24hr: true});
}

function apptSetModal() {
  let $modal = $('.contest-appt');
  let $form = $modal.find('form');
  $modal.find('.flatpickr').flatpickr({time_24hr: true});
  $form.submit(function(e) {
    e.preventDefault();
    let url = $(this).attr('action');
    $.ajax({
      method: 'POST',
      url: url,
      data: { 'startsAt': $modal.find('#apptStartsAt').val() },
      success: function() {
        location.reload();
      },
      error: function(res) {
        handleError(res);
      }
    });
    return false;
  });
}

function applySetBoardSignModal() {
  let $modal = $('.board-sign');
  $modal.find('.btn-sign').click(function () {
    let $this = $(this);
    let signed = $this.hasClass('disabled');
    if (!signed) {
      $.ajax({
        method: 'POST',
        url: $this.data('href'),
        success: function() {
          $this
            .addClass('button-green')
            .addClass('disabled')
            .text('已准备（签到）');
        },
        error: function(res) {
          handleError(res);
        }
      });
    }
  });

  $modal.find('form').submit(function(e) {
    e.preventDefault();
    $.modal.close();
    return false;
  });
}

function applyPlayerExport() {
  $('.player-export').click(function () {
    let $table = $('.flow').find('.enter .players');
    let $thead = $table.find('thead');
    let $tbody = $table.find('tbody');

    let data = [];

    // 标题
    let title = $('.contest-name .text').text() + ' 报名表';

    // 表头
    let head = $thead.find('tr th').map(function (_, th) {
      return th.innerText;
    }).toArray();
    data.push(head.slice(0, head.length - 1));

    // 内容
    let body = $tbody.find('tr').map(function (_, tr) {
      let arr = $(tr).find('td').map(function (_, td) {
        return td.innerText.replace('\n', ' ').replace('\n', ' ');
      }).toArray();
      return [arr];
    }).toArray();

    body = body.map(function (arr) {
      return arr.slice(0, arr.length - 1);
    });
    data = data.concat(body);

    let csvContent = "data:text/csv;charset=utf-8,\uFEFF"
      + data.map(e => e.join(',')).join('\n');
    let encodedUri = encodeURI(csvContent);
    let link = document.createElement("a");
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `${title}.csv`);
    document.body.appendChild(link);
    link.click();
  });
}

function applyScoreExport() {
  $('.export-score').click(function () {
    let $score = $(this).parents('.score');
    let $table = $score.find('table');
    let $thead = $table.find('thead');
    let $tbody = $table.find('tbody');

    let data = [];

    // 标题
    let title = $('.contest-name .text').text() + ' 成绩册';

    // 表头
    let head = $thead.find('tr th').map(function (_, th) {
      return th.innerText;
    }).toArray();
    data.push(head.slice(0, head.length - 1));

    // 内容
    let body = $tbody.find('tr').map(function (_, tr) {
      let arr = $(tr).find('td').map(function (_, td) {
        return td.innerText.replace('\n', ' ').replace('\n', ' ');
      }).toArray();
      return [arr];
    }).toArray();

    body = body.map(function (arr) {
      return arr.slice(0, arr.length - 1);
    });
    data = data.concat(body);

    let csvContent = "data:text/csv;charset=utf-8,\uFEFF"
      + data.map(e => e.join(',')).join('\n');
    let encodedUri = encodeURI(csvContent);
    let link = document.createElement("a");
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `${title}.csv`);
    document.body.appendChild(link);
    link.click();
  });
}

function dateFormat() {
  Date.prototype.format = function (fmt) {
    let o = {
      "M+": this.getMonth() + 1,                 //月份
      "d+": this.getDate(),                    //日
      "h+": this.getHours(),                   //小时
      "m+": this.getMinutes(),                 //分
      "s+": this.getSeconds(),                 //秒
      "q+": Math.floor((this.getMonth() + 3) / 3), //季度
      "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  };
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
