$(function () {
  let $page = $('.task-info');

  if($page.length) {
    document.addEventListener("visibilitychange", () => {
      if (!document.hidden) {
        location.reload();
      }
    });

    $page.find('a.task-planAt').click(function (e) {
      e.preventDefault();
      $.ajax({
        url: $(this).attr('href'),
        success: function (html) {
          $.modal($(html));
          $('.cancel').click(function () {
            $.modal.close();
          });

          let $form = $('.modal-planAt .form-planAt');
          let fp = $form.find('.flatpickr');
          fp.flatpickr({
            enableTime: true,
            time_24hr: true,
            minDate: fp.data('min-date'),
            maxDate: fp.data('max-date')
          });
          $form.submit(function (e) {
            e.preventDefault();
            $.ajax({
              method: 'post',
              url: $form.attr('action'),
              data: $form.serialize(),
              success: function () {
                $.modal.close();
                location.reload();
              },
              error: function (res) {
                handleError(res);
              }
            });
            return false;
          });

        },
        error: function (res) {
          handleError(res);
        }
      });
      return false;
    });

    chessMove($page);
    lazyLoadCapsulePuzzle($page);
    fromPositionModal($page);
    fromPgnModal($page);
  }

});

function fromPositionModal($page) {
  $page.find('a.fromPositionModal.isAi').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
    return false;
  });
}

function fromPgnModal($page) {
  $page.find('a.fromPgnModal.isAi').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
      },
      error: function (res) {
        handleError(res);
      }
    });
    return false;
  });
}

function notAcceptModal($page) {
  $page.find('a.notAccept').click(function (e) {
    e.preventDefault();
    lichess.memberIntro();
  })
}


function chessMove($page) {
  $page.find('.moves').each(function() {
    let $moves = $(this);
    $moves.find('move span:not(.disabled)').click(function () {
      let fen = $(this).data('fen');
      let $board = $(this).parents('tr').find('.td-board .mini-board');
      Chessground($board[0], {
        coordinates: false,
        resizable: false,
        drawable: {enabled: false, visible: false},
        viewOnly: true,
        fen: fen
      });
      $board.attr('data-fen', fen);

      $moves.find('move span.active').removeClass('active');
      $(this).addClass('active');
    });
  });
}

function lazyLoadCapsulePuzzle($page) {
  let $capsulePuzzle = $page.find('.capsulePuzzle');
  let $puzzles = $capsulePuzzle.find('.puzzles .puzzle');

  let puzzleNum = $puzzles.length;
  let n = 0;

  lazyLoad();
  window.onscroll = function () {
    lazyLoad();
  };
  function lazyLoad () {
    let seeHeight = document.documentElement.clientHeight;
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    for (let i = n; i < puzzleNum; i++) {
      if ($puzzles[i].offsetTop < seeHeight + scrollTop) {
        let $board = $($puzzles[i]).find('.parse-fen-manual');
        parseCapsulePuzzle($board);
        n = i + 1;
      }
    }
  }
}

function parseCapsulePuzzle($el) {
  let $this = $el.removeClass('parse-fen-manual');
  if($this.length) {
    let fen = $this.data('fen');
    let lastMove = $this.data('lastmove');
    let color = $this.data('color');
    let ground = $this.data('chessground');
    let playable = !!$this.data('playable');
    let solution = $this.data('solution');
    let solutionColor = $this.data('solution-color');
    let config = {
      coordinates: false,
      viewOnly: true,
      resizable: false,
      fen: fen,
      lastMove: uciToLastMove(lastMove),
      drawable: {
        enabled: false,
        visible: playable,
        autoShapes: solution ? [{
          orig: solution.split(' ')[0],
          dest: solution.split(' ')[1],
          brush: solutionColor ? solutionColor : 'green'
        }] : []
      }
    };

    if (color) config.orientation = color;
    if (ground) ground.set(config);
    else $this.data('chessground', Chessground($this[0], config));
  }
}

function uciToLastMove(uci) {
  return uci && [uci.substr(0, 2), uci.substr(2, 2)];
}

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if(typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
