$(function () {

    let $page = $('.racer-history');

    $page.find('.history-list .paginated').click(function () {
        let $this = $(this);
        let $expand = $this.find('.expand');
        let $rel = $this.next('tr[rel="' + $this.prop('id') + '"]');
        if($expand.hasClass('expanded')) {
            $expand.removeClass('expanded');
            $rel.addClass('none');
        } else {
            $expand.addClass('expanded');
            $rel.removeClass('none');
        }
    });

});
