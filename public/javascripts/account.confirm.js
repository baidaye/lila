$(function() {

    let $confirm = $('.account-confirm');
    $confirm.find('.tabs > div').not('.disabled').click(function () {
        let active = $(this).data('tab');
        $confirm.find('.tabs > div').removeClass('active');
        $confirm.find('.panels > .panel').removeClass('active');
        $confirm.find('.panels > .panel.' + active).addClass('active');
        $(this).addClass('active');
        location.hash = active;
    });

    setTabActive();
    function setTabActive() {
        let hash = location.hash;
        if(!$confirm.find('.tabs > div[data-tab="' + hash + '"]').hasClass('disabled')) {
            if(hash) {
                hash = hash.replace('#', '');
                $confirm.find('.tabs > div').removeClass('active');
                $confirm.find('.tabs > div[data-tab="' + hash + '"]').addClass('active');
                $confirm.find('.panels > .panel').removeClass('active');
                $confirm.find('.panels > .panel.' + hash).addClass('active');
            }
        }
    }

});

