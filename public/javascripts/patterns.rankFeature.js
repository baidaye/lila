$(function() {

  let $page = $('.patterns-rankFeature');

  const maxOrder = 999999;
  const emptyFen = '8/8/8/8/8/8/8/8 w - -';
  let flipped = $page.find('button.flip').data('flipped');

  setUrl();

  if($('main').data('notaccept') == true) {
    window.lichess.memberIntro();
  }

  $page.find('button.prev').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  $page.find('button.next').click(function() {
    let $order = $page.find('#form3-order');
    $order.val($(this).data('id'));
    $order.trigger('change');
  });

  $page.find('button.flip').click(function() {
    let $board = $page.find('.main-board .cg-wrap');
    let orient = $board.data('color');
    $board.addClass('parse-fen-manual');
    if(orient === 'white') {
      $board.data('color', 'black');
    } else {
      $board.data('color', 'white');
    }
    flipped = orient === 'white';
    setUrl();
    setGround();
  });

  $page.find('.fen-line .button').click(function(e) {
    let $this = $(this);
    let $board = $page.find('.cg-wrap');
    let fen = $this.data('fen');

    $board.data('fen', fen);
    $board.addClass('parse-fen-manual');
    $page.find('.patterns__btm').find('.fen').val(fen);

    $page.find('.fen-line .button').removeClass('active');
    $this.addClass('active');

    setGround();
  });

  $page.find('input[name="color"]').change(function() {
    $page.find('.cpn-editor')
      .removeClass('white')
      .removeClass('black')
      .addClass($(this).val() === 'white' ? 'black' : 'white');
    $page.find('#form3-order').val('');
    next();
  });

  $page.find('#form3-patternsOpOfCpn').on('change paste keyup', function(e) {
    e.target.value = e.target.value.toUpperCase();
    cpnApply($page.find('#form3-patternsOpOfCpn').val());
  });

  $page.find('#form3-order').on('change paste keyup', function() {
    next();
  });

  $page.find('.op-random').click(function() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let color = $page.find('input[name="color"]:checked').val();
    $.get({ url: `/patterns/${patternsType}/randomOp?color=${color === 'white'}`}).then(function(cpn) {
      $page.find('#form3-patternsOpOfCpn').val(cpn);
      cpnApply(cpn);
      search();
    });
  });

  $page.find('.op-search').click(function() {
    search();
  });

  $page.find('.btnSituation').click(function() {
    showCreateSituation();
  });

  function search() {
    let cpn = $page.find('#form3-patternsOpOfCpn').val();
    $page.find('#form3-patternsOp').val(cpn);
    $page.find('#form3-order').val('');
    next();
  }

  function next() {
    if($('main').data('notaccept') == true) {
      window.lichess.memberIntro();
      return;
    }

    let oldOrder = $page.find('#form3-order').data('id');
    let order = $page.find('#form3-order').val();
    if(order === oldOrder) {
      return;
    }

    let patternsType = $page.find('input[name="patternsType"]').val();
    let patternsOp = $page.find('#form3-patternsOp').val();
    let color = $page.find('input[name="color"]:checked').val();

    return $.ajax({
      url: `/patterns/${patternsType}/rank/feature/next?patternsOp=${patternsOp}&color=${color}&order=${order}`
    }).then(d => {
      setPage(true, d);
      setUrl();
    }).fail(function(err) {
      if(err.status === 404) {
        setPage(false, {});
        setUrl();
      } else if(err.status === 406) {
        setPage(false, {});
        setUrl();
        window.lichess.memberIntro();
      } else {
        alert('发生错误')
      }
    });
  }

  function setPage(success, data) {
    let $board = $page.find('.cg-wrap');
    let $gameFrom = $page.find('.gameFrom');
    let $fenLine = $page.find('.fen-line');
    let $boardBtm = $page.find('.patterns__btm');
    let $order = $page.find('#form3-order');

    let $prev = $page.find('button.prev');
    let $next = $page.find('button.next');

    let curr = data.curr;
    let prev = data.prev;
    let next = data.next;
    success = success && curr;
    if(success) {
      let patternsType = $page.find('input[name="patternsType"]').val();
      let currentFenType = $fenLine.find('.active').data('fentype');
      $order.val(curr.order);
      $board.data('fen', curr[currentFenType]);
      $board.data('color', flipped ? 'black' : 'white');
      $board.addClass('parse-fen-manual');
      $gameFrom.find('a').text(`#${curr.gameId}`).attr('href', `/${curr.gameId}/white/external?ply=${curr.ply}`);
      $boardBtm.find('.fen').val(curr[currentFenType]);
      $boardBtm.find('.btnSituation').removeClass('disabled').prop('disabled', false);

      if(prev) {
        $prev.removeClass('disabled').prop('disabled', false).data('id', prev.order);
      } else {
        $prev.addClass('disabled').prop('disabled', true).data('id', '');
      }

      if(next) {
        $next.removeClass('disabled').prop('disabled', false).data('id', next.order);
      } else {
        $next.addClass('disabled').prop('disabled', true).data('id', '');
      }
    } else {
      $board.data('fen', emptyFen);
      $board.addClass('parse-fen-manual');
      $board.data('color', 'white');
      $boardBtm.find('.fen').val(emptyFen);
      $gameFrom.find('a').text('#- -').attr('href', ``);
      $boardBtm.find('.btnSituation').addClass('disabled').prop('disabled', true);

      $prev.addClass('disabled').prop('disabled', true).data('id', '');
      $next.addClass('disabled').prop('disabled', true).data('id', '');
    }

    $fenLine.find('button').each(function () {
      let $this = $(this);
      let fenType = $this.data('fentype');
      if(success) {
        $this.data('fen', curr[fenType])
          .removeClass('disabled')
          .prop('disabled', false);
      } else {
        $this.data('fen', '')
          .addClass('disabled')
          .prop('disabled', true);
      }
    });

    setGround();
  }

  function setUrl() {
    let patternsType = $page.find('input[name="patternsType"]').val();
    let patternsOp = $page.find('#form3-patternsOp').val();
    let color = $page.find('input[name="color"]:checked').val();
    let order = $page.find('#form3-order').val();
    history.replaceState(null, '', `/patterns/${patternsType}/rank/feature?patternsOp=${patternsOp}&color=${color}&flipped=${flipped}&order=${order ? order : ''}`);
  }

  setGround();
  function setGround() {
    let $board = $page.find('.cg-wrap');
    $board.removeClass('parse-fen-manual');
    let mainGround = $board.data('chessground');
    let fen = $board.data('fen');
    let color = $board.data('color');
    let coord = !!$board.data('coordinates');
    let is3d = $board.hasClass('is3d');
    let config = {
      fen: fen,
      orientation: color,
      movable: {
        free: false,
        color: null,
        dests: {}
      },
      highlight: {
        check: true,
        lastMove: true
      },
      drawable: { enabled: false, visible: false },
      resizable: true,
      coordinates: coord,
      addPieceZIndex: is3d
    };

    if (mainGround) mainGround.set(config);
    else $board.data('chessground', Chessground($board[0], config));
  }

  cpnApply($page.find('#form3-patternsOpOfCpn').val());
  cpnEditor();
  function cpnEditor() {
    let $editor = $page.find('.cpn-editor');
    let $board = $editor.find('.cpn-editor-board');
    let $spare = $editor.find('.cpn-editor-spare');

    let $selected;
    //let $dragging;
    // if(!$selected) {
    //   $selected = $spare.find('.editor-piece-pointer').parent().addClass('selected');
    // }

    $spare.find('.editor-piece').on('click', function () {
      let $this = $(this);

      $spare.find('.editor-piece').removeClass('selected');
      $this.addClass('selected');

      $selected = $this;
      //$dragging = null;
      setCursor();
    });

    $board.find('.dropzone').on('click', function () {
      let $this = $(this);
      if($selected) {
        let role = $selected.find('.piece-piece-inner').data('role');
        switch (role) {
          case 'pointer':
            break;
          case 'trash': {
            let $piece = $this.children('.editor-piece');
            if($piece.length > 0) {
              $piece.remove();
            }
            break;
          }
          default: {
            let oldRole = $this.find('.piece-piece-inner') ? $this.find('.piece-piece-inner').data('role') : '';
            if(role === oldRole) {
              $this.empty();
            } else {
              $piece = $selected.clone().removeClass('selected');
              // $piece.on('dragstart', function () {
              //   if(canDrag()) {
              //     $dragging = $(this);
              //   }
              // });
              $this.html($piece);
            }
          }
        }
        setPatternsOp();
      }
    });
    //
    // $board.find('.editor-piece').on('dragstart', function () {
    //   if(canDrag()) {
    //     $dragging = $(this);
    //   }
    // });
    //
    // $board.find('.dropzone').on('dragover', function (e) {
    //   e.preventDefault();
    // });
    //
    // $board.find('.dropzone').on('drop', function () {
    //   let $this = $(this);
    //   if(canDrag() && $dragging) {
    //     $this.html($dragging);
    //   }
    // });
    //
    // function canDrag() {
    //   if($selected) {
    //     let role = $selected.find('.piece-piece-inner').data('role');
    //     if (role === 'pointer') {
    //       return true;
    //     }
    //   }
    //   return false;
    // }

    function setCursor() {
      let cursor;
      if($selected) {
        let role = $selected.find('.piece-piece-inner').data('role');
        let color = $page.find('input[name="color"]:checked').val();

        switch (role) {
          case 'pointer':
            cursor = 'pointer';
            break;
          case 'trash':
            let url = window.lichess.assetUrl(`cursors/trash.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          case 'king': {
            let url = window.lichess.assetUrl(`cursors/${color}-${role}.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          }
          case 'P': {
            let url = window.lichess.assetUrl(`cursors/patterns-p.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          }
          case 'O': {
            let url = window.lichess.assetUrl(`cursors/patterns-o.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          }
          case 'X': {
            let url = window.lichess.assetUrl(`cursors/patterns-x.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          }
          case 'Z': {
            let url = window.lichess.assetUrl(`cursors/patterns-z.cur`);
            cursor = 'url(' + url + '), default !important';
            break;
          }
        }
      } else cursor = 'auto';

     $editor.css('cssText', `cursor: ${cursor}`);
    }

    function setPatternsOp() {
      let cpn = cpnCollect();
      $page.find('#form3-patternsOpOfCpn').val(cpn);
    }
  }

  function cpnApply(cpn) {
    if(cpn) {
      let $editor = $page.find('.cpn-editor');
      let $board = $editor.find('.cpn-editor-board');
      let $spare = $editor.find('.cpn-editor-spare');

      let cpnArray = cpn.toLowerCase().split('');
      $.each(cpnArray, function (i, v) {
        let $piece = $spare.find(`.editor-piece-${v}`).parent().clone();
        $board.find(`.square-${i + 1}`).html($piece);
      });

    }
  }

  function cpnCollect() {
    let $editor = $page.find('.cpn-editor');
    let $board = $editor.find('.cpn-editor-board');

    let patternsOp = '';
    for (let i = 1; i <= 8; i++) {
      let $piece = $board.find(`.square-${i} .piece-piece-inner`);
      if($piece.length) {
        patternsOp += $piece.data('role');
      }
    }
    return patternsOp;
  }

  function showCreateSituation() {
    let fen = $page.find('.patterns__btm').find('.fen').val();
    $.ajax({
      url: `/resource/situationdb/rel/createForEditor?looksLegit=true&fen=${fen}`
    }).then(function (html) {
      $.modal($(html));
      $('.cancel').click(function () {
        $.modal.close();
      });

      $('.situation-create').find('form').find('#form3-tags').tagsInput({
        'height': '40px',
        'width': '100%',
        'interactive': true,
        'defaultText': '添加',
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 10,
        'placeholderColor': '#666666'
      });

      window.lichess.loadScript('javascripts/vendor/jquery-1.11.2.min.js').then(() => {
        window.lichess.loadScript('javascripts/vendor/jstree/jstree.min.js').then(() => {
          let $jq = $.noConflict(true);
          let $form = $jq('.situation-create').find('form');
          let $tree = $form.find('.dbtree');
          $tree.jstree({
            'core': {
              'worker': false,
              'data': {
                'url': '/resource/situationdb/tree/load'
              },
              'check_callback': true
            }
          })
            .on('changed.jstree', function (e, data) {
              if (data && data.node && data.action === 'select_node' && data.event && data.event.type === 'click') {
                $form.find('#form3-situationdbId').val(data.node.id);
              }
            });

          $form.submit(function (e) {
            e.preventDefault();
            let situationdbId = $form.find('#form3-situationdbId').val();
            $.ajax({
              method: 'POST',
              url: `/resource/situationdb/rel/create?situationdbId=${situationdbId}`,
              data: $form.serialize()
            }).then(function (res) {
              $.modal.close();
            }, function (err) {
              handleError(err);
            });
            return false;
          });
        })
      });
      return false
    });
  }

});
