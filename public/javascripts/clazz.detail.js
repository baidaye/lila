$(function () {

  let $page = $('.detail');

  let $tabs = $page.find('.tabs');
  $tabs.find('.header > div').not('.disabled').click(function () {
    $tabs.find('.header > div').removeClass('active');
    $tabs.find('.panels div').removeClass('active');

    $(this).addClass('active');
    let activeTab = $(this).data('tab');
    $tabs.find('.panels').find('.' + activeTab).addClass('active');
    location.hash = activeTab;
  });

  setTabActive();

  function setTabActive() {
    let hash = location.hash;
    if (!$tabs.find('.header div[data-tab="' + hash + '"]').hasClass('disabled')) {
      if (hash) {
        hash = hash.replace('#', '');
        $tabs.find('.header > div').removeClass('active');
        $tabs.find('.header > div[data-tab="' + hash + '"]').addClass('active');
        $tabs.find('.panels div').removeClass('active');
        $tabs.find('.panels div.' + hash).addClass('active');

        let courseId = $page.data('course');
        if (courseId && hash === 'courses') {
          setTimeout(function () {
            let $course = $page.find('#' + courseId).first();
            let scrollTop = $course.offset().top;
            $(window).scrollTop(scrollTop);
          }, 500);
        }
      }
    }
  }

  $page.find('#attendDate').change(function () {
    let val = $(this).val();
    let txt = $(this).find("option:selected").text();
    let href = $('.modal-attend').attr('href');
    if(href) {
      href = href.split('?')[0] + '?courseId=' + val;
      $('.modal-attend').attr('href', href);
    }

    $.ajax({
      url: $(this).data('href') + "?courseId=" + val,
      success: function (attends) {
        $('th.dateTime').text(txt);
        let $tb = $page.find('.attend .slist tbody');
        $tb.find('tr').each(function () {
          $(this).find('td.absent').text('-');
          $(this).find('td.nb').text('-');
        });
        if (attends) {
          attends.forEach(function (attend) {
            let absent = '-';
            if (attend.absent != null) {
              absent = attend.absent ? '缺勤' : '出勤';
            }
            let nb = attend.nb ? attend.nb + '天' : '0天';
            $tb.find('tr.' + attend.userId).find('td.absent').text(absent);
            $tb.find('tr.' + attend.userId).find('td.nb').text(nb);
          });
        } else {
          alert('发生错误');
        }
      }
    });
  });

  $page.find('.showHis').click(function () {
    let $this = $(this);
    let $history = $page.find('.courses .slist tr.history');
    if ($this.is(':checked')) {
      $history.addClass('none');
    } else {
      $history.removeClass('none');
    }
  });
  $page.find('.showHis').trigger('click');

  $page.find('.courses .course').click(function () {
    let $this = $(this);
    let $expand = $this.find('.expand');
    let $rel = $this.next('tr[rel="' + $this.prop('id') + '"]');
    if ($expand.hasClass('expanded')) {
      $expand.removeClass('expanded');
      $rel.addClass('none');
    } else {
      $expand.addClass('expanded');
      $rel.removeClass('none');
    }
  });

  $('a.modal-alert').click(function (e) {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('href'),
      success: function (html) {
        $.modal($(html));
        $('.cancel').click(function () {
          $.modal.close();
        });
        attendSetModal();
        attendStudentModal();
        onMemberOpen();
        editCoach();
        courseAppendCoach();
      },
      error: function (res) {
        handleError(res);
      }
    });
    return false;
  });

  function attendSetModal() {
    let $modal = $('.attendSet');
    let onChange = (checkeds) => {
      $modal.find('input[name=absents]').val(checkeds.join(','));
    };
    transfer(onChange);

    $modal.find('.attendSubmit').click(function () {
      $.ajax({
        method: 'post',
        url: $modal.find('.form3').attr('action'),
        data: {absents: $modal.find('input[name=absents]').val()},
        success: function () {
          $.modal.close();
          $page.find('#attendDate').trigger('change');
        },
        error: function (res) {
          handleError(res);
        }
      });
    });
  }

  function attendStudentModal() {
    let $modal = $('.attendStudentSet');
    let $courses = $modal.find('.courses');
    $courses.find('.course').click(function () {
      let absent = $(this).find('input[name=absent]').val();
      let nextAbsent = 0;
      let className = '';
      if (absent == -1) {
        nextAbsent = 0;
        className = 'absent';
      } else if (absent == 0) {
        nextAbsent = 1;
        className = 'unabsent';
      } else if (absent == 1) {
        nextAbsent = -1;
        className = '';
      }
      $(this).removeClass('absent').removeClass('unabsent').addClass(className);
      $(this).find('input[name=absent]').val(nextAbsent);
    });

    $modal.find('.attendStudentSubmit').click(function () {
      let arr = [];
      $courses.find('.course').each(function () {
        let absent = $(this).find('input[name=absent]').val();
        let course = $(this).data('id');
        arr.push(course + ':' + absent);
      });
      $.ajax({
        method: 'post',
        url: $modal.find('.form3').attr('action'),
        data: {courseAttends: arr.join(',')},
        success: function () {
          $.modal.close();
          $page.find('#attendDate').trigger('change');
        },
        error: function (res) {
          handleError(res);
        }
      });
    });
  }

  function editCoach() {
    let $modal = $('.editCoachModal');
    $modal.find('.editCoachSave').click(function (e) {
      e.preventDefault();
      let data = {'newCoach': $('#form3-newCoach').val()};
      $.ajax({
        method: 'post',
        url: $modal.find('form').attr('action'),
        data: data,
        success: function () {
          $.modal.close();
          location.reload();
        },
        error: function (res) {
          handleError(res);
        }
      });
      return false;
    });
  }

  function courseAppendCoach() {
    let $modal = $('.course-append');
    if($modal.length > 0) {
      $('.flatpickr').flatpickr();
      let $form = $('form');
      $form.submit(function (e) {
        e.preventDefault();
        $.ajax({
          method: 'post',
          url: $modal.find('form').attr('action'),
          data: $form.serialize(),
          success: function () {
            $.modal.close();
            location.reload();
          },
          error: function (res) {
            handleError(res);
          }
        });
        return false;
      });
    }
  }

  $page.find('.olClass-open').change(function () {
    let courseId = $(this).data('id');
    let c = $(this)[0].checked;
    $.ajax({
      method: 'post',
      url: `/olclass/${courseId}/open?o=${c}`,
      success: function () {
      },
      error: function (res) {
        handleError(res);
      }
    });
  });
});

function handleError(res) {
  let json = res.responseJSON;
  if (json) {
    if (json.error) {
      if (typeof json.error === 'string') {
        alert(json.error);
      } else alert(JSON.stringify(json.error));
    } else alert(res.responseText);
  } else alert('发生错误');
}
