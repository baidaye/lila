package views.html.resource

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

object bits {

  val tmpForm =
    postForm(id := "tmp-form")(
      form3.hidden("gameId", ""),
      form3.hidden("name", "")
    )

}
