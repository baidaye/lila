package views.html.resource.capsule

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.resource.Capsule
import lila.puzzle.Puzzle
import play.api.libs.json.{ JsArray, Json }
import controllers.rt_resource.routes

object print {

  private val dataLastMove = attr("data-lastmove")
  private val dataPuzzles = attr("data-puzzles")
  private val bool = List("1" -> "是", "0" -> "否")
  private val nums = List("2*2" -> "2行2列", "3*3" -> "3行3列", "4*3" -> "4行3列", "4*4" -> "4行4列")
  private val orientations = List("auto" -> "按题目", "white" -> "白方", "black" -> "黑方")

  def apply(capsule: Capsule, puzzles: List[Puzzle])(implicit ctx: Context) =
    views.html.base.layout(
      title = "打印设置",
      moreCss = frag(
        cssTag("capsule")
      ),
      moreJs = frag(
        printTag,
        jsTag("capsule.print.js")
      )
    ) {
        main(cls := "page-small capsule-print", dataId := capsule.id, dataPuzzles := puzzleJson(puzzles))(
          div(cls := "box box-pad setting")(
            h1("打印设置"),
            table(
              tr(th("每页题目数"), td(boolRadio("nums", nums, "3*3"))),
              tr(th("题目视角"), td(boolRadio("orientation", orientations, "auto"))),
              tr(th("显示编号"), td(boolRadio("no", bool, "1"))),
              tr(th("显示先行"), td(boolRadio("color", bool, "1"))),
              tr(th("显示难度"), td(boolRadio("rating", bool, "1"))),
              tr(th("显示答题线"), td(boolRadio("line", bool, "1"))),
              tr(th("显示棋盘坐标"), td(boolRadio("coord", bool, "1"))),
              tr(th("标题"), td(st.input(tpe := "text", name := "title", value := capsule.name))),
              tr(
                th,
                td(
                  button(cls := "button do-print")("打印"),
                  div(cls := "tip")(
                    div("注：1.如果点击打印后未正常显示棋盘，请在弹出窗口中找到“更多设置”，勾选“背景图形”选项；"),
                    div(nbsp, nbsp, "2.如果打印预览时出现棋盘跨页的情况，可以再弹出窗口中找到“更多设置”，调整自定义“缩放”比例。")
                  )
                )
              )
            )
          ),
          br,
          div(cls := "box box-pad")(
            div(cls := "print-area")(
              div(cls := "capsule-list")()
            )
          )
        )
      }

  private def puzzleJson(puzzles: List[Puzzle]) =
    JsArray(
      puzzles.zipWithIndex.map {
        case (p, i) => {
          Json.obj(
            "no" -> (i + 1),
            "id" -> p.id,
            "color" -> p.color.name,
            "colorLabel" -> p.color.fold("白方", "黑方"),
            "lastMove" -> p.initialUci,
            "fen" -> p.fenAfterInitialMove,
            "rating" -> p.rating.toInt
          )
        }
      }
    ).toString

  private def boolRadio(name: String, options: List[(String, String)], check: String) = {
    div(cls := s"radio-group $name")(
      options.map {
        case (k, v) => {
          span(cls := "radio")(
            st.input(tpe := "radio", st.id := s"${name}_$k", st.name := name, st.value := k.toString, (check == k).option(checked)),
            nbsp,
            label(cls := "radio-label", `for` := s"${name}_$k")(v)
          )
        }
      }
    )
  }

}
