package views.html.resource.capsule

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.resource.{ Capsule, CapsuleStatus, CapsuleVisibility, CapsuleMember }
import views.html.resource.puzzle.emptyTag
import controllers.rt_resource.routes

object list {

  def mine(form: Form[_], capsules: List[Capsule], tags: Set[String])(implicit ctx: Context) = {
    base(form, capsules, tags, "mine")
  }

  def member(form: Form[_], capsules: List[Capsule], tags: Set[String])(implicit ctx: Context) = {
    base(form, capsules, tags, "member")
  }

  def teamManager(form: Form[_], capsules: List[Capsule], tags: Set[String])(implicit ctx: Context) = {
    base(form, capsules, tags, "teamManager")
  }

  private def base(form: Form[_], capsules: List[Capsule], tags: Set[String], tab: String)(implicit ctx: Context) = {
    val call: play.api.mvc.Call = tab match {
      case "mine" => routes.Capsule.mineList()
      case "member" => routes.Capsule.memberList()
      case "teamManager" => routes.Capsule.teamManagerList()
      case _ => routes.Capsule.mineList()
    }

    views.html.base.layout(
      title = "战术题列表",
      moreJs = frag(
        infiniteScrollTag,
        jsTag("capsule.list.js")
      ),
      moreCss = cssTag("capsule")
    ) {
        main(cls := "page-menu capsule-list")(
          st.aside(cls := "page-menu__menu subnav")(
            views.html.resource.puzzle.menuLinks("capsule")
          ),
          div(cls := "box")(
            div(cls := "tabs-horiz capsule-list-tab")(
              span(cls := List("active" -> (tab == "mine")))(a(href := routes.Capsule.mineList())("我的列表")),
              span(cls := List("active" -> (tab == "member")))(a(href := routes.Capsule.memberList())("参与列表")),
              span(cls := List("active" -> (tab == "teamManager")))(a(href := routes.Capsule.teamManagerList())("俱乐部授权"))
            ),
            div(cls := "tabs-content")(
              div(cls := "active")(
                st.form(cls := "search_form capsule_form", action := s"${call.url}", method := "GET")(
                  table(
                    td(colspan := 4)(
                      div(cls := "tag-groups")(
                        emptyTag(form),
                        form3.tags(form, "tags", tags)
                      )
                    ),
                    tab == "mine" option tr(
                      td(cls := "label")(label("状态")),
                      td(cls := "fixed")(form3.select(form("status"), CapsuleStatus.selects, "全部".some)),
                      td(cls := "label")(label("可见范围")),
                      td(cls := "fixed")(form3.select(form("visibility"), CapsuleVisibility.selects, "全部".some))
                    ),
                    tab == "member" option tr(
                      td(cls := "label")(label("状态")),
                      td(cls := "fixed")(form3.select(form("status"), CapsuleStatus.selects, "全部".some)),
                      td(cls := "label")(label("角色")),
                      td(cls := "fixed")(form3.select(form("role"), CapsuleMember.Role.memberSelects, "全部".some))
                    ),
                    tab == "teamManager" option tr(
                      td(cls := "label")(label("状态")),
                      td(cls := "fixed")(form3.select(form("status"), CapsuleStatus.selects, "全部".some)),
                      td,
                      td
                    ),
                    tr(
                      td(cls := "label")(label("名称")),
                      td(form3.input(form("name"))(placeholder := "名称")),
                      td,
                      td
                    ),
                    tr(
                      td(colspan := 4)(
                        div(cls := "action")(
                          a(cls := "button button-green", dataIcon := "O", href := routes.Capsule.createForm)("创建列表"),
                          submitButton(cls := "button")("搜索")
                        )
                      )
                    )
                  )
                ),
                table(cls := "slist")(
                  thead(
                    tr(
                      th("名称"),
                      th("题目数量"),
                      tab == "mine" option th("可见范围"),
                      tab == "member" option th("角色"),
                      th("状态"),
                      th("上次更新"),
                      th("操作")
                    )
                  ),
                  if (capsules.nonEmpty) {
                    tbody(
                      capsules.map { capsule =>
                        tr(
                          td(capsule.name),
                          td(capsule.total),
                          tab == "mine" option td(capsule.visibility.name),
                          tab == "member" option td(ctx.userId.map(userId => capsule.roleOf(userId).map(_.name))),
                          td(capsule.status.name),
                          td(momentFromNow(capsule.updatedAt)),
                          td(style := "display:flex;")(
                            (tab == "mine" && !capsule.nonEmpty) option button(cls := "button button-empty small disabled")("做题"),
                            (tab == "mine" && capsule.nonEmpty) option a(cls := "button button-empty small", href := controllers.routes.Puzzle.capsulePuzzle(capsule.id))("做题"),
                            ((tab == "member" && ctx.userId.??(capsule.isReader)) || tab == "teamManager") option a(cls := "button button-empty small", href := routes.Capsule.puzzleCapsule(capsule.id))("查看"),
                            (tab == "mine" || (tab == "member" && ctx.userId.??(capsule.isWriter))) option a(cls := "button button-empty small", href := routes.Capsule.updateForm(capsule.id))("修改"),
                            a(cls := "button button-empty small", href := routes.Capsule.print(capsule.id))("打印"),
                            tab == "mine" option postForm(action := routes.Capsule.clone(capsule.id))(
                              submitButton(cls := "button button-empty small")("复制")
                            ),
                            tab == "mine" option a(cls := "button button-empty small", href := routes.Capsule.memberRoles(capsule.id))("权限"),
                            tab == "mine" option postForm(action := routes.Capsule.remove(capsule.id))(
                              submitButton(cls := "button button-empty button-red small confirm", title := "删除后不可恢复，是否继续操作？")("删除")
                            ),
                            tab == "member" option postForm(action := routes.Capsule.quitMember(capsule.id))(
                              submitButton(cls := "button button-empty button-red small confirm", title := "是否确认退出此列表？")("退出")
                            )
                          )
                        )
                      }
                    )
                  } else {
                    tbody(
                      tr(
                        td(colspan := 5)("暂无记录")
                      )
                    )
                  }
                )
              )
            )
          )
        )
      }
  }

}
