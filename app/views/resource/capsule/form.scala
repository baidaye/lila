package views.html.resource.capsule

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import views.html.resource.puzzle.actions
import lila.resource.{ Capsule, CapsuleStatus, CapsuleVisibility }
import lila.puzzle.Puzzle
import lila.user.User
import controllers.rt_resource.routes

object form {

  def create(form: Form[_])(implicit ctx: Context) =
    views.html.base.layout(
      title = "创建列表",
      moreCss = frag(
        cssTag("capsule")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("capsule.form.js")
      )
    ) {
        main(cls := "page-menu")(
          st.aside(cls := "page-menu__menu subnav")(
            views.html.resource.puzzle.menuLinks("capsule")
          ),
          div(cls := "box box-pad")(
            h1("创建列表"),
            postForm(cls := "form3", action := routes.Capsule.create())(
              form3.group(form("name"), "名称")(form3.input(_)),
              form3.group(form("tags"), "标签")(form3.input(_)),
              form3.group(form("desc"), "描述")(form3.textarea(_)()),
              form3.actions(
                a(cls := "cancel", href := routes.Capsule.mineList())("取消"),
                submitButton(cls := List("button text" -> true), dataIcon := "E")("保存")
              )
            )
          )
        )
      }

  private val dataLastMove = attr("data-lastmove")
  def update(form: Form[_], capsule: Capsule, puzzles: List[Puzzle])(implicit ctx: Context) =
    views.html.base.layout(
      title = "修改列表",
      moreCss = frag(
        cssTag("resource"),
        cssTag("capsule")
      ),
      moreJs = frag(
        tagsinputTag,
        dragsortTag,
        jsTag("resource.js"),
        jsTag("capsule.form.js")
      )
    ) {
        main(cls := "page-menu", dataId := capsule.id)(
          st.aside(cls := "page-menu__menu subnav")(
            views.html.resource.puzzle.menuLinks("capsule")
          ),
          div(
            div(cls := "box box-pad capsule-update")(
              h1("修改列表"),
              postForm(cls := "form3 small", action := routes.Capsule.update(capsule.id))(
                form3.split(
                  form3.group(form("name"), "名称", half = true)(form3.input(_)),
                  form3.group(form("status"), "锁定题目", half = true, help = frag("锁定题目的列表， 题目和题目顺序将不能修改。").some)(form3.select(_, CapsuleStatus.selects))
                ),
                form3.group(form("tags"), "标签")(form3.input(_)),
                form3.group(form("desc"), "描述")(form3.textarea(_)()),
                form3.actions(
                  a(cls := "cancel", href := routes.Capsule.mineList())("取消"),
                  submitButton(cls := List("button text" -> true), dataIcon := "E")("保存")
                )
              ),
              br,
              div(cls := "capsule-puzzles")(
                div(cls := "waiting none")(spinner),
                st.form(cls := "search_form nopan", action := s"${routes.Capsule.puzzleCapsule(capsule.id)}#results", method := "GET")(
                  table(
                    tr(
                      td(cls := "is-gold", dataIcon := "")("拖动题目可以调整顺序"),
                      puzzles.nonEmpty && capsule.active option td(cls := "action capsule")(
                        actions(List("delete", "ascSort", "descSort"))
                      )
                    )
                  )
                ),
                div(cls := "infinitescroll now-playing list dragsort")(
                  puzzles.zipWithIndex.map {
                    case (p, i) => {
                      a(cls := List("paginated" -> true, "dragable" -> capsule.active), target := "_blank", dataId := p.id, title := "拖动可调整顺序", dataHref := controllers.routes.Puzzle.show(p.id, false, false))(
                        div(
                          cls := "mini-board cg-wrap parse-fen is2d",
                          dataColor := p.color.name,
                          dataFen := p.fenAfterInitialMove,
                          dataLastMove := p.initialUci
                        )(cgWrapContent),
                        div(cls := "btm")(
                          span("编号：", label(cls := "no")(i + 1)),
                          span("难度：", if (p.isImport) "NA" else p.intRating)
                        )
                      )
                    }
                  }
                )
              )
            )
          )
        )
      }

  def memberRoles(capsule: Capsule, users: List[User], markMap: Map[String, Option[String]], form: Form[_])(implicit ctx: Context) =
    views.html.base.layout(
      title = "列表权限",
      moreCss = frag(
        cssTag("capsule")
      ),
      moreJs = frag(
        jsTag("capsule.role.js")
      )
    ) {
        val visibilitySelects = if (ctx.me.??(_.hasTeam)) CapsuleVisibility.selects else CapsuleVisibility.privateSelects
        main(cls := "page-menu")(
          st.aside(cls := "page-menu__menu subnav")(
            views.html.resource.puzzle.menuLinks("capsule")
          ),
          div(cls := "box box-pad capsule-role")(
            h1(s"${capsule.name} 列表权限"),
            div(cls := "form3", dataHref := routes.Capsule.setVisibility(capsule.id))(
              form3.split(
                form3.group(form("visibility"), "可见范围", half = true)(form3.select(_, visibilitySelects))
              ),
              form3.split(
                div(cls := "form-group form-half")(
                  input(cls := "user-autocomplete", id := "username", name := "username", placeholder := "输入“添加成员”或“转移所有者”账号", autofocus, required, dataTag := "span")
                ),
                div(cls := "form-group form-half user-autocomplete-actions")(
                  button(cls := "button addMember", dataHref := routes.Capsule.addMember(capsule.id))("添加成员"),
                  button(cls := "button changeOwner", dataHref := routes.Capsule.changeOwner(capsule.id), title := s"${capsule.name} 列表所有权 将转给 xxxx，此操作不可恢复，请谨慎执行！")("转移所有者")
                )
              ),
              div(cls := "form-group")(
                table(cls := "slist")(
                  thead(
                    tr(
                      th("账号"),
                      th("备注（姓名）"),
                      th("角色"),
                      th("操作")
                    )
                  ),
                  tbody(
                    capsule.members.sorted.map {
                      case (userId, member) => {
                        val user = users.find(_.id == userId) err s"can not find user $userId"
                        tr(
                          td(userLink(user, withBadge = false)),
                          td(userMark(user, markMap)),
                          td(member.role.name),
                          td(cls := "action")(
                            !member.isOwner option frag(
                              member.isReader option postForm(action := routes.Capsule.setMemberRole(capsule.id, userId))(
                                submitButton(cls := "button button-empty small", name := "role", value := "w")("设为贡献者")
                              ),
                              member.isWriter option postForm(action := routes.Capsule.setMemberRole(capsule.id, userId))(
                                submitButton(cls := "button button-empty small", name := "role", value := "r")("设为参与者")
                              ),
                              postForm(action := routes.Capsule.removeMember(capsule.id, userId))(
                                submitButton(cls := "button button-empty small button-red confirm", title := "是否确认移除成员？")("移除")
                              )
                            )
                          )
                        )
                      }
                    }
                  )
                )
              ),
              div(cls := "form-group")(
                ul(
                  li("贡献者：可修改列表信息，包括题目、顺序，以及标签等元数据；"),
                  li("参与者：仅有列表使用权限，不能修改；"),
                  li("锁定题目后，列表中的题目和顺序不能修改，可避免误操作。")
                )
              ),
              form3.actions(
                span,
                a(cls := "button small", href := routes.Capsule.mineList())("返回")
              )
            )
          )
        )
      }

  def userMark(u: lila.user.User, markMap: Map[String, Option[String]]): String = {
    markMap.get(u.id).fold(none[String]) { m => m } | u.realNameOrUsername
  }

}
