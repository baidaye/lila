package views.html.resource.capsule

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.resource.Capsule
import play.api.libs.json.Json
import controllers.rt_resource.routes

object modal {

  val dataAttr = attr("data-attr")

  val dataTab = attr("data-tab")

  // 使用战术题列表
  def capsuleModal(
    mine: List[Capsule],
    member: List[Capsule],
    teamManager: List[Capsule],
    mineTags: Set[String],
    memberTags: Set[String],
    teamManagerTags: Set[String],
    checkedIds: List[Capsule.ID]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content modal-capsule none")(
      h2("选择战术题列表"),
      postForm(cls := "form3")(
        div(cls := "tabs-horiz")(
          span(dataTab := "mine", cls := "mine active")("我的列表"),
          span(dataTab := "member", cls := "member")("参与列表"),
          span(dataTab := "teamManager", cls := "teamManager")("俱乐部授权")
        ),
        div(cls := "tabs-content")(
          div(cls := "tabs-contentx mine active")(
            capsuleContent(mine, mineTags, checkedIds, "mine")
          ),
          div(cls := "tabs-contentx member")(
            capsuleContent(member, memberTags, checkedIds, "member")
          ),
          div(cls := "tabs-contentx teamManager")(
            capsuleContent(teamManager, teamManagerTags, checkedIds, "teamManager")
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确认", klass = "small")
        )
      )
    )
  )

  // 使用战术题列表
  def capsuleModalForTeamTest(
    mine: List[Capsule],
    mineTags: Set[String],
    checkedIds: List[Capsule.ID]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content modal-capsule none")(
      h2("选择战术题列表"),
      postForm(cls := "form3")(
        div(cls := "tabs-horiz")(
          span(dataTab := "mine", cls := "mine active")("我的列表")
        ),
        div(cls := "tabs-content")(
          div(cls := "tabs-contentx mine active")(
            capsuleContent(mine, mineTags, checkedIds, "mine")
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确认", klass = "small")
        )
      )
    )
  )

  private def capsuleContent(capsules: List[Capsule], tags: Set[String], checkedIds: List[Capsule.ID], tab: String) = frag(
    div(cls := "capsule-filter")(
      div(cls := "capsule-filter-tag tag-group")(
        tags.toSeq.map { tag =>
          val _id = s"${tab}_ckb_$tag"
          span(
            st.input(st.id := _id, tpe := "checkbox", value := tag),
            label(`for` := _id)(tag)
          )
        }
      ),
      input(tpe := "text", cls := "capsule-filter-search", placeholder := "搜索")
    ),
    div(cls := "capsule-scroll")(
      (tab == "mine" && capsules.isEmpty) option div(cls := "no-more")(
        "您还没有创建战术题列表，现在就去", nbsp, a(target := "_blank", href := controllers.rt_resource.routes.Capsule.create())("创建"), nbsp, "吧"
      ),
      (tab != "mine" && capsules.isEmpty) option div(cls := "no-more")("没有可用的战术题列表."),
      capsules.nonEmpty option table(cls := "capsule-list")(
        capsules.map { capsule =>
          val json = Json.obj(
            "id" -> capsule.id,
            "name" -> capsule.name,
            "tags" -> capsule.tags,
            "ids" -> capsule.ids.mkString(","),
            "len" -> capsule.total
          )
          tr(dataId := capsule.id, dataAttr := json.toString())(
            td(
              input(tpe := "checkbox", id := s"rd_${capsule.id}", name := "capsuleRd", value := capsule.id, checkedIds.contains(capsule.id) option checked),
              nbsp,
              label(`for` := s"rd_${capsule.id}")(capsule.name)
            ),
            td(capsule.total)
          )
        }
      )
    )
  )

  // 将战术题添加到战术题列表
  def capsuleModal(mineCapsules: List[Capsule], memberCapsules: List[Capsule])(implicit ctx: Context) = frag(
    div(cls := "modal-content modal-capsule none")(
      h2("添加到战术题列表"),
      postForm(cls := "form3")(
        div(cls := "tabs-horiz")(
          span(dataId := "mine", cls := "mine active")("我的列表"),
          span(dataId := "member", cls := "member")("参与列表"),
          span(dataId := "new", cls := "new")("快速建立新列表")
        ),
        div(cls := "tabs-content")(
          div(cls := "mine active")(
            div(cls := "capsule-scroll")(
              mineCapsules.isEmpty option div(cls := "no-more")("没有可用的战术题列表."),
              table(cls := "capsule-list")(
                mineCapsules.map { capsule =>
                  tr(
                    td(
                      input(tpe := "radio", id := capsule.id, name := "capsule", value := capsule.id),
                      nbsp,
                      label(`for` := capsule.id)(capsule.name)
                    ),
                    td(capsule.total)
                  )
                }
              )
            ),
            br,
            div(cls := "is-gold", dataIcon := "")("每个列表最多添加60道战术题")
          ),
          div(cls := "member")(
            div(cls := "capsule-scroll")(
              mineCapsules.isEmpty option div(cls := "no-more")("没有可用的战术题列表."),
              table(cls := "capsule-list")(
                memberCapsules.map { capsule =>
                  tr(
                    td(
                      input(tpe := "radio", id := capsule.id, name := "capsule", value := capsule.id),
                      nbsp,
                      label(`for` := capsule.id)(capsule.name)
                    ),
                    td(capsule.total)
                  )
                }
              )
            ),
            br,
            div(cls := "is-gold", dataIcon := "")("每个列表最多添加60道战术题")
          ),
          div(cls := "new")(
            div(cls := "name-label")(strong("战术题列表名称：")),
            st.input(name := "capsuleName", cls := "form-control", minlength := 2, maxlength := 20)
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

}
