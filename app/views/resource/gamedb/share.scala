package views.html.resource.gamedb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.game.Pov
import lila.resource.{ GameShare, GameShareRel }
import lila.user.User
import play.api.data.Form
import views.html.game.bits
import views.html.resource.puzzle.emptyTag
import views.html.search.bits.dateMinMax
import controllers.rt_resource.routes

object share {

  def apply(
    form: Form[_],
    pager: Paginator[GameShare],
    shareBys: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = "分享的对局",
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        infiniteScrollTag
      ),
      moreCss = cssTag("resource")
    ) {
        main(cls := "page-menu resource shared")(
          st.aside(cls := "page-menu__menu")(
            div(cls := "resource-nav subnav")(
              views.html.resource.game.menuLinks("shared")
            )
          ),
          div(cls := "page-menu__content box")(
            st.form(
              cls := "search_form shared_form",
              action := s"${routes.GameShare.relPage(1)}#results",
              method := "GET"
            )(
                table(
                  tr(
                    th("分享人"),
                    td(
                      form3.select(form("shareBy"), shareBys.map(u => u.id -> userMark(u, markMap)), "".some)
                    )
                  ),
                  tr(
                    th("分享时间"),
                    td(
                      div(cls := "half")(form3.input(form("dateMin"), klass = "flatpickr")(dateMinMax: _*)),
                      div(cls := "half")(form3.input(form("dateMax"), klass = "flatpickr")(dateMinMax: _*))
                    )
                  ),
                  tr(
                    th(
                      ctx.me.??(_.isCoachOrTeam) option a(href := routes.GameShare.logPage(1))("分享记录")
                    ),
                    td(button(cls := "button")("搜索"))
                  )
                )
              ),
            paginate(pager, shareBys, markMap)
          )
        )
      }

  private def paginate(pager: Paginator[GameShare], shareBys: List[User], markMap: Map[String, Option[String]])(implicit ctx: Context) =
    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了~")
    )
    else div(cls := "infinitescroll shares")(
      pager.currentPageResults.map { share =>
        div(cls := "paginated grid")(shareWidget(share, shareBys.find(_.id == share.createBy) err s"can not find user ${share.createBy}", markMap))
      },
      pagerNext(pager, np => addQueryParameter(s"${routes.GameShare.logPage(1)}", "page", np))
    )

  def shareWidget(share: GameShare, shareBy: User, markMap: Map[String, Option[String]])(implicit ctx: Context) = frag(
    a(cls := "overlay", href := routes.GameShare.rels(share.id)),
    div(cls := "share-info")(
      div(
        div(cls := "shareBy")(
          userIdLink(share.createBy.some, withOnline = false, withBadge = false, text = userMark(shareBy, markMap).some),
          span(cls := "time")(share.createAt.toString("yyyy-MM-dd HH:mm"))
        ),
        div(cls := "remark")(share.remark)
      )
    )
  )

  def games(
    form: Form[_],
    share: GameShare,
    list: List[GameShareRel.WithGame],
    tags: Set[String],
    shareByMark: Option[String]
  )(implicit ctx: Context) = views.html.base.layout(
    title = "分享的对局",
    moreJs = frag(
      flatpickrTag,
      delayFlatpickrStart,
      jsTag("resource.js"),
      jsTag("resource.gamedb.js")
    ),
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("resource")
    )
  ) {
      main(cls := "page-menu resource shared")(
        st.aside(cls := "page-menu__menu")(
          div(cls := "resource-nav subnav")(
            views.html.resource.game.menuLinks("shared")
          )
        ),
        div(cls := "page-menu__content box")(
          st.form(
            cls := "search_form shared_form",
            action := s"${routes.GameShare.rels(share.id)}#results",
            method := "GET"
          )(
              form3.hidden("gameSource", "share"),
              form3.hidden("shareId", share.id),
              div(cls := "share-info shadow")(
                div(cls := "shareLine")(
                  div(cls := "shareBy")(
                    userIdLink(share.createBy.some, withOnline = false, withBadge = false, text = shareByMark),
                    span(cls := "time")(share.createAt.toString("yyyy-MM-dd HH:mm"))
                  ),
                  a(href := routes.GameShare.relPage(1))("返回")
                ),
                div(cls := "remark")(share.remark)
              ),
              table(
                tr(
                  td(cls := "tag-groups")(
                    emptyTag(form),
                    form3.tags(form, "tags", tags)
                  )
                ),
                tr(
                  td(cls := "action")(
                    list.nonEmpty option frag(
                      select(cls := "select")(
                        option(value := "")("选择"),
                        option(value := "all")("选中所有"),
                        option(value := "none")("取消选择")
                      ),
                      select(cls := "action")(
                        option(value := "")("操作"),
                        option(value := "toPrint")("打印"),
                        ctx.me.??(_.hasResource) option option(value := "copyTo")("复制到"),
                        option(value := "toStudy")("转研习"),
                        option(value := "toRecall")("转记谱"),
                        option(value := "toDistinguish")("转棋谱记录")
                      )
                    )
                  )
                )
              )
            ),
          views.html.resource.bits.tmpForm,
          div(cls := "games infinitescroll")(
            gameWidgets(list, user = ctx.me, ownerLink = true, linkBlank = true, showFullMoves = true)
          )
        )
      )
    }

  private def userMark(u: lila.user.User, markMap: Map[String, Option[String]]): String = {
    markMap.get(u.id).fold(none[String]) { m => m } | u.username
  }

  private val separator = " • "
  private val dataGameId = attr("data-gameId")
  private val dataName = attr("data-name")
  def gameWidgets(
    games: Seq[GameShareRel.WithGame],
    user: Option[lila.user.User] = None,
    ownerLink: Boolean = false,
    mini: Boolean = false,
    linkBlank: Boolean = false,
    showFullMoves: Boolean = false,
    showCheckbox: Boolean = false
  )(implicit ctx: Context): Frag = games map { gwr =>
    val g = gwr.game
    val fromPlayer = user flatMap g.player
    val firstPlayer = fromPlayer | g.firstPlayer
    st.article(cls := "game-row paginated", dataId := gwr.rel.id, dataGameId := gwr.rel.gameId, dataName := gwr.rel.name, dataHref := gameLink(g, firstPlayer.color, ownerLink))(
      a(cls := "game-row__overlay", linkBlank option (target := "_blank"), href := gameLink(g, firstPlayer.color, ownerLink)),
      div(cls := "game-row__board")(
        gameFen(Pov(g, firstPlayer), withLink = false, withTitle = false)
      ),
      div(cls := "game-row__infos")(
        div(cls := "rel")(
          div(cls := "title")(gwr.rel.name),
          gwr.rel.tags.map { tags =>
            div(cls := "tags")(
              tags.map { tag =>
                span(tag)
              }
            )
          }
        ),
        div(cls := "header", dataIcon := bits.gameIcon(g))(
          div(cls := "header__text")(
            strong(
              if (g.imported) frag(
                span("导入"),
                g.pgnImport.flatMap(_.user).map { user =>
                  frag(" ", trans.by(userIdLink(user.some, None, withOnline = false, withBadge = false)))
                },
                separator,
                if (g.variant.exotic) bits.variantLink(g.variant, g.variant.name.toUpperCase)
                else g.variant.name.toUpperCase
              )
              else frag(
                views.html.game.widgets.showClock(g),
                separator,
                g.perfType.fold(chess.variant.FromPosition.name)(_.name),
                separator,
                if (g.rated) trans.rated.txt() else trans.casual.txt()
              )
            ),
            g.pgnImport.flatMap(_.date).fold(momentFromNow(g.createdAt))(frag(_)),
            g.tournamentId map { tourId =>
              frag(separator, tournamentLink(tourId))
            },
            g.contestId map { contestId =>
              frag(separator, contestLink(contestId, g.contestIsTeamOrFalse))
            },
            g.simulId map { simulId =>
              frag(separator, views.html.simul.bits.link(simulId))
            }
          )
        ),
        div(cls := "versus")(
          views.html.game.widgets.gamePlayer(g.variant, g.whitePlayer),
          div(cls := "swords", dataIcon := "U"),
          views.html.game.widgets.gamePlayer(g.variant, g.blackPlayer)
        ),
        div(cls := "result")(
          if (g.isBeingPlayed) trans.playingRightNow() else {
            if (g.finishedOrAborted)
              span(cls := g.winner.flatMap(w => fromPlayer.map(p => if (p == w) "win" else "loss")))(
                gameEndStatus(g),
                g.winner.map { winner =>
                  frag(
                    ", ",
                    winner.color.fold(trans.whiteIsVictorious(), trans.blackIsVictorious())
                  )
                }
              )
            else g.turnColor.fold(trans.whitePlays(), trans.blackPlays())
          }
        ),
        if (g.turns > 0) {
          val len = 1 + (16 - 1) / 2
          val pgnMoves = g.pgnMoves take len

          div(cls := "opening")(
            (!g.fromPosition ?? g.opening) map { opening =>
              strong(opening.opening.ecoName)
            },
            div(cls := "pgn")(
              pgnMoves.grouped(2).zipWithIndex map {
                case (Vector(w, b), i) => s"${i + 1}. $w $b"
                case (Vector(w), i) => s"${i + 1}. $w"
                case _ => ""
              } mkString " ",
              g.turns > len option s" ... ${1 + (g.turns - 1) / 2} moves "
            )
          )
        } else frag(br, br),
        g.metadata.analysed option
          div(cls := "metadata text", dataIcon := "")(trans.computerAnalysisAvailable()),
        g.pgnImport.flatMap(_.user).map { user =>
          div(cls := "metadata")("PGN import by ", userIdLink(user.some, withOnline = false, withBadge = false))
        }
      )
    )
  }
}
