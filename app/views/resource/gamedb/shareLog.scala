package views.html.resource.gamedb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.resource.{ GameDBRel, GameShare }
import lila.user.User
import play.api.data.Form
import views.html.search.bits.dateMinMax
import controllers.rt_resource.routes

object shareLog {

  def apply(form: Form[_], pager: Paginator[GameShare])(implicit ctx: Context) =
    views.html.base.layout(
      title = "分享记录",
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        infiniteScrollTag
      ),
      moreCss = cssTag("resource")
    ) {
        main(cls := "page-menu resource shared")(
          st.aside(cls := "page-menu__menu")(
            div(cls := "resource-nav subnav")(
              views.html.resource.game.menuLinks("shared")
            )
          ),
          div(cls := "page-menu__content box")(
            st.form(
              cls := "search_form shared_form",
              action := s"${routes.GameShare.logPage(1)}#results",
              method := "GET"
            )(
                table(
                  tr(
                    th("分享时间"),
                    td(
                      div(cls := "half")(form3.input(form("dateMin"), klass = "flatpickr")(dateMinMax: _*)),
                      div(cls := "half")(form3.input(form("dateMax"), klass = "flatpickr")(dateMinMax: _*))
                    )
                  ),
                  tr(
                    th,
                    td(button(cls := "button")("搜索"))
                  )
                )
              ),
            table(cls := "slist")(
              thead(
                tr(
                  th("分享时间"),
                  th("对局数量"),
                  th("目标班级"),
                  th("学员数量"),
                  th("操作")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => routes.GameShare.logPage(np).url),
                  pager.currentPageResults.map { share =>
                    tr(cls := "paginated")(
                      td(share.createAt.toString("yyyy-MM-dd HH:mm")),
                      td(share.rels.size),
                      td(share.clazzId.map(clazzId => if (clazzId == "others") frag("-") else clazzLinkById(clazzId))),
                      td(share.students.size),
                      td(
                        a(href := routes.GameShare.logInfo(share.id))("查看详情")
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 5)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  def info(share: GameShare, students: List[User], rels: List[GameDBRel], markMap: Map[String, Option[String]])(implicit ctx: Context) =
    views.html.base.layout(
      title = "分享详情",
      moreCss = cssTag("resource")
    ) {
        main(cls := "page-menu resource shared")(
          st.aside(cls := "page-menu__menu")(
            div(cls := "resource-nav subnav")(
              views.html.resource.game.menuLinks("shared")
            )
          ),
          div(cls := "page-menu__content box box-pad")(
            h1("分享详情"),
            table(cls := "shareInfo")(
              tr(
                th("分享时间："),
                td(share.createAt.toString("yyyy-MM-dd HH:mm"))
              ),
              tr(
                th("分享说明："),
                td(share.remark)
              ),
              tr(
                th("班级信息："),
                td(share.clazzId.map(clazzId => if (clazzId == "others") frag("-") else clazzLinkById(clazzId)))
              ),
              tr(
                th("分享目标："),
                td(
                  ul(
                    students.map(stu =>
                      li(userLink(stu, withOnline = false, withBadge = false, text = userMark(stu, markMap).some)))
                  )
                )
              ),
              tr(
                th("对局列表："),
                td(
                  ul(
                    rels.map(rel =>
                      li(a(href := controllers.routes.Round.watcher(rel.gameId, "white"))(rel.name)))
                  )
                )
              )
            )
          )
        )
      }

  private def userMark(u: lila.user.User, markMap: Map[String, Option[String]]): String = {
    markMap.get(u.id).fold(none[String]) { m => m } | u.username
  }

}
