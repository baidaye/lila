package views.html.resource.gamedb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.data.Form
import play.api.libs.json.JsArray
import lila.hub.lightClazz.{ ClazzId, ClazzName }
import controllers.rt_resource.routes
import lila.user.User

object modal {

  def createModal(gamedbId: String, form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content gamedbrel-create none")(
      h2("新建对局"),
      postForm(cls := "form3", action := routes.GameDB.createGame(gamedbId))(
        form3.hidden("tab", "pgn"),
        form3.group(form("name"), raw("名称"))(form3.input(_)(required)),
        form3.group(form("tags"), "标签")(form3.input(_)),
        div(cls := "tabs-horiz")(
          span(cls := "pgn active")("PGN"),
          span(cls := "chapter")("研习章节链接"),
          span(cls := "game")("对局链接")
        ),
        div(cls := "tabs-content")(
          div(cls := "pgn active")(
            form3.textarea(form("pgn"))(rows := 5, placeholder := "粘贴PGN文本"),
            form3.group(form("pgnFile"), raw("上传PGN文件"), klass = "upload") { f =>
              form3.file.pgn(f.name)
            }
          ),
          div(cls := "chapter")(
            form3.input(form("chapter"))(placeholder := "章节URL"),
            div("例：https://haichess.com/study/eKF8yUkP/mJyWJTu6")
          ),
          div(cls := "game")(
            form3.input(form("game"))(placeholder := "对局URL"),
            div("例：https://haichess.com/XTtL9RRY"),
            div("或：https://haichess.com/XTtL9RRY/white")
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  def editModal(gamedbRelId: String, form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content gamedbrel-edit none")(
      h2("修改对局"),
      postForm(cls := "form3")(
        form3.group(form("name"), raw("名称"))(form3.input(_)(required)),
        form3.group(form("tags"), "标签")(form3.input(_)),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  val dataTree = attr("data-tree")
  def copyOrMoveModal(source: String, action: String, frGamedb: Option[String], idList: List[String], treeJson: JsArray)(implicit ctx: Context) = {
    div(cls := "modal-content gamedbrel-cp none")(
      h2("复制/移动(", idList.size, ")个对局到"),
      postForm(cls := "form3")(
        div(cls := "cp-dbtree", dataTree := treeJson.toString),
        form3.hidden("source", source),
        form3.hidden("action", action),
        form3.hidden("frGamedb", frGamedb | ""),
        form3.hidden("toGamedb", frGamedb | ""),
        form3.hidden("rels", idList.mkString(",")),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  }

  def shareModal(form: Form[_], idList: List[String], clazzs: List[(ClazzId, ClazzName)])(implicit ctx: Context) = {
    div(cls := "modal-content gamedbrel-share none")(
      h2("分享(", idList.size, ")个对局"),
      postForm(cls := "form3")(
        form3.hidden("rels", idList.mkString(",")),
        form3.group(form("clazzId"), raw("班级"), half = true)(form3.select(_, clazzs)),
        form3.group(form("students"), raw("学员"), klass = "nolabel")(f =>
          div(cls := "stu-panel")(
            div(cls := "stu-search")(
              div(cls := "ck-wrap")(
                input(id := "ck-all", tpe := "checkbox"), nbsp,
                label(`for` := "ck-all", "全选")
              ),
              input(tpe := "text", cls := "search", placeholder := "搜索")
            ),
            div(cls := "stu-list")(
              table(cls := "stu-table")(
                tbody()
              )
            )
          )),
        form3.group(form("isShareTag"), raw("是否分享标签"))(
          form3.radio2(_, List(true -> "是", false -> "否"), "true".some)
        ),
        form3.group(form("remark"), raw("分享说明"))(form3.textarea(_)(rows := 2)),
        p(cls := "info", dataIcon := "")("请注意：对局分享不可撤销，请谨慎操作！"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  }

}
