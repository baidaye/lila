package views.html.resource.situationdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import play.api.data.Form
import views.html.resource.puzzle.emptyTag
import lila.resource.SituationDBRel
import controllers.rt_resource.routes

object home {

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    a(activeCls("situationdb"), href := routes.SituationDB.home())("局面数据库")
  }

  def apply(
    form: Form[_],
    pager: Paginator[SituationDBRel],
    tags: List[String],
    selected: String,
    fen: Option[String] = None,
    relSortBy: Option[lila.resource.SituationDB.SortBy]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = "局面数据库",
      moreJs = frag(
        infiniteScrollTag,
        jsAt("javascripts/vendor/jquery-1.11.2.min.js"),
        jsAt("javascripts/vendor/jstree/jstree.js"),
        tagsinputTag,
        jsTag("resource.js"),
        jsTag("resource.situation.js"),
        fen.map { _ =>
          embedJsUnsafe {
            """$(function() { setTimeout(function() { $("a.btn-create").trigger('click'); }, 500) });"""
          }
        }
      ),
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("resource")
      )
    ) {
        main(cls := "page-menu resource situationdb", dataNotAccept := s"${!(ctx.me.isDefined && ctx.me.??(_.hasResource))}")(
          st.aside(cls := "page-menu__menu")(
            div(cls := "resource-nav subnav")(
              menuLinks("situationdb")
            )
          ),
          div(cls := "page-menu__content box")(
            div(cls := "situation-content")(
              div(cls := "situation-dir")(
                div(cls := "tree-actions")(
                  button(cls := "button button-green small tree-create")("新建"),
                  button(cls := List("button button-blue small tree-rename" -> true, "disabled" -> ctx.userId.contains(selected)), ctx.userId.contains(selected) option disabled)("修改"),
                  button(cls := List("button button-red small tree-delete" -> true, "disabled" -> ctx.userId.contains(selected)), ctx.userId.contains(selected) option disabled)("删除")
                ),
                div(cls := "dbtree")
              ),
              div(cls := "situation-situations")(
                st.form(
                  cls := "search_form situation_form",
                  action := s"${routes.SituationDB.home()}#results",
                  method := "GET"
                )(
                    form3.hidden("selected", selected),
                    table(
                      tr(
                        td(cls := "tag-groups")(
                          emptyTag(form),
                          form3.tags(form, "tags", tags.toSet)
                        )
                      ),
                      tr(
                        td(cls := "search_create")(
                          div(
                            form3.select(form("standard"), List("" -> "全部", true -> "标准局面", false -> "非标局面")),
                            form3.input(form("name"))(placeholder := "名称"),
                            button(cls := "button", dataIcon := "y")
                          ),
                          a(href := "", dataFen := fen, cls := "button button-green btn-create", style := "display:inline-block", dataIcon := "O")
                        )
                      ),
                      tr(
                        td(
                          pager.nbResults > 0 option div(cls := "div-action")(
                            div(
                              !ctx.userId.has(selected) option select(cls := "relSortBy")(
                                lila.resource.SituationDB.SortBy.all.map { sort =>
                                  option(value := sort.id, relSortBy.has(sort) option st.selected)(sort.name)
                                }
                              )
                            ),
                            div(cls := "action")(
                              select(cls := "select")(
                                option(value := "")("选择"),
                                option(value := "all")("选中所有"),
                                option(value := "none")("取消选择")
                              ),
                              select(cls := "action")(
                                option(value := "")("操作"),
                                option(value := "situationRelEdit")("修改"),
                                option(value := "situationRelDelete")("删除"),
                                ctx.me.??(_.hasResource) option option(value := "situationRelCopyTo")("复制到"),
                                ctx.me.??(_.hasResource) option option(value := "situationRelMoveTo")("移动到"),
                                ctx.me.??(_.hasResource) option option(value := "situationRelDemonstrate")("转演示"),
                                option(cls := "mustStandard", value := "situationRelToAnalysis")("转分析"),
                                option(cls := "mustStandard", value := "situationRelToStudy")("转研习"),
                                option(value := "situationRelToEditor")("棋盘编辑器"),
                                option(cls := "mustStandard", value := "situationRelToPlayWithAi")("和机器下棋"),
                                option(cls := "mustStandard", value := "situationRelToPlayWithFriend")("和朋友下棋")
                              )
                            )
                          )
                        )
                      )
                    )
                  ),
                postForm(id := "tmp-form", action := "/study/as")(
                  form3.hidden("fen", "")
                ),
                paginate(pager, form)
              )
            )
          )
        )
      }

  private def paginate(pager: Paginator[SituationDBRel], form: Form[_])(implicit ctx: Context) = {
    var url = s"${routes.SituationDB.home()}"
    url = if (url.contains("?")) url else url.concat("?a=1")
    form.data.foreach {
      case (key, value) =>
        url = url.concat("&").concat(key).concat("=").concat(value)
    }

    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := "now-playing list situations infinitescroll")(
      pager.currentPageResults.map { situation =>
        miniSituation(situation)
      },
      pagerNext(pager, np => addQueryParameter(url, "page", np))
    )
  }

  val dataStandard = attr("data-standard")
  private def miniSituation(situation: SituationDBRel) = {
    a(cls := "paginated", target := "_blank", id := situation.id, dataId := situation.id, dataStandard := situation.standard,
      dataHref := (if (situation.standard) s"${controllers.routes.UserAnalysis.parse(situation.fen)}" else s"${controllers.rt_demonstrate.routes.Demonstrate.home()}?fen=${situation.fen}"))(
        div(
          cls := "mini-board cg-wrap parse-fen is2d",
          dataColor := "white",
          dataFen := situation.fen
        )(cgWrapContent),
        div(cls := "btm")(
          label(situation.name),
          div(cls := "tags")(
            situation.tags.map { tags =>
              tags.map(span(_))
            }
          )
        )
      )
  }

}
