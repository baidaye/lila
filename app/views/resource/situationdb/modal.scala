package views.html.resource.situationdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.data.Form
import play.api.libs.json.JsArray
import views.html.resource.puzzle.emptyTag
import lila.resource.SituationDBRel
import controllers.rt_resource.routes

object modal {

  val dataTree = attr("data-tree")

  def createModal(situationdbId: String, form: Form[_], fen: Option[String])(implicit ctx: Context) =
    div(cls := "modal-content situationdbrel-create none")(
      h2("新建局面"),
      postForm(cls := "form3", action := routes.SituationDB.create(situationdbId))(
        div(cls := "clm2")(
          div(cls := "left")(
            form3.group(form("standard"), raw("标准局面"), half = true)(
              form3.radio2(_, List(true -> "是", false -> "否"), "true".some)
            ),
            fen.map { _ =>
              form3.group(form("dbId"), "目录选择")(_ => {
                div(cls := "scroller")(
                  div(cls := "dbtree")
                )
              })
            },
            form3.group(form("fen"), "FEN")(f =>
              frag(
                form3.input(f)(placeholder := "在此处粘贴FEN棋谱"),
                span(cls := "preview")(
                  div(
                    cls := "mini-board cg-wrap parse-fen is2d",
                    dataColor := "white",
                    dataFen := form("fen").value
                  )(cgWrapContent)
                )
              ))
          ),
          div(cls := "right")(
            form3.group(form("name"), raw("名称"))(form3.input(_)),
            form3.group(form("tags"), "标签")(form3.input(_)()),
            form3.group(form("instruction"), "局面说明")(form3.textarea(_)(rows := 2))
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  def editModal(situationdbRelId: String, form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content situationdbrel-edit none")(
      h2("修改局面"),
      postForm(cls := "form3")(
        div(cls := "clm2")(
          div(cls := "left")(
            form3.group(form("standard"), raw("标准局面"), half = true)(
              form3.radio2(_, List(true -> "是", false -> "否"), "true".some)
            ),
            form3.group(form("fen"), "FEN")(f =>
              frag(
                form3.input(f)(placeholder := "在此处粘贴FEN棋谱"),
                span(cls := "preview")(
                  div(
                    cls := "mini-board cg-wrap parse-fen is2d",
                    dataColor := "white",
                    dataFen := form("fen").value
                  )(cgWrapContent)
                )
              ))
          ),
          div(cls := "right")(
            form3.group(form("name"), raw("名称"))(form3.input(_)),
            form3.group(form("tags"), "标签")(form3.input(_)()),
            form3.group(form("instruction"), "局面说明")(form3.textarea(_)(rows := 2))
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  def moveModal(action: String, frSituationdb: Option[String], idList: List[String], treeJson: JsArray)(implicit ctx: Context) = {
    div(cls := "modal-content situationdbrel-cp none")(
      h2("复制/移动(", idList.size, ")个局面到"),
      postForm(cls := "form3")(
        div(cls := "cp-dbtree", dataTree := treeJson.toString),
        form3.hidden("action", action),
        form3.hidden("frSituationdb", frSituationdb | ""),
        form3.hidden("toSituationdb", frSituationdb | ""),
        form3.hidden("rels", idList.mkString(",")),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  }

  def selectModal(
    situations: List[SituationDBRel],
    tags: Set[String],
    mustStandard: Boolean,
    form: Form[_]
  )(implicit ctx: Context) =
    div(cls := "modal-content situation-selector none")(
      h2("载入局面"),
      postForm(cls := "form3")(
        div(cls := "situation-content")(
          div(cls := "situation-dir")(
            div(cls := "dbtree")
          ),
          div(cls := "situation-situations")(
            div(cls := "search_form")(
              table(
                tr(
                  td(cls := "tag-groups")(
                    emptyTag(form),
                    form3.tags(form, "tags", tags)
                  )
                ),
                tr(
                  td(
                    form3.select2(form("standard"), (if (mustStandard) "true".some else none), List("" -> "全部", true -> "标准局面", false -> "非标局面")),
                    form3.input(form("name"))(placeholder := "名称"),
                    a(cls := "button", dataIcon := "y", id := "btn-search")
                  )
                )
              )
            ),
            div(cls := "situations")(
              if (situations.isEmpty) {
                div(cls := "no-more")(
                  iconTag("4"),
                  p("没有更多了")
                )
              } else {
                situations.map { situation =>
                  a(cls := "paginated", target := "_blank", dataId := situation.id)(
                    div(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := situation.fen
                    )(cgWrapContent),
                    div(cls := "btm")(
                      label(situation.name),
                      div(cls := "tags")(
                        situation.tags.map { tags =>
                          tags.map(span(_))
                        }
                      )
                    )
                  )
                }
              }
            )
          )
        ),
        form3.actions(
          a(cls := "cancel situation-cancel")("取消"),
          form3.submit("确定", icon = none, klass = "small")
        )
      )
    )

}
