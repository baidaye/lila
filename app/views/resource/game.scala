package views.html.resource

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.game.Game
import lila.user.User
import play.api.data.Form
import play.mvc.Call
import views.html.resource.puzzle.emptyTag
import controllers.rt_resource.routes

object game {

  def imported(form: Form[_], user: User, pager: Paginator[Game], tags: Set[String])(implicit ctx: Context) = layout(
    title = "导入的对局",
    active = "imported",
    form = form,
    user = user,
    pager = pager,
    call = routes.Resource.gameImported()
  ) {
      st.form(
        cls := "search_form imported_form",
        action := s"${routes.Resource.gameImported()}#results",
        method := "GET"
      )(
          form3.hidden("gameSource", "import"),
          table(
            tr(
              td(cls := "tag-groups")(
                emptyTag(form),
                form3.tags(form, "tags", tags)
              )
            ),
            tr(
              td(cls := "action")(
                a(cls := "button modal-alert", href := controllers.routes.Importer.pgnImportForm())("导入"),
                pager.nbResults > 0 option frag(
                  select(cls := "select")(
                    option(value := "")("选择"),
                    option(value := "all")("选中所有"),
                    option(value := "none")("取消选择")
                  ),
                  select(cls := "action")(
                    option(value := "")("操作"),
                    option(value := "toPrint")("打印"),
                    option(value := "delete")("删除"),
                    ctx.me.??(_.hasResource) option option(value := "copyTo")("复制到"),
                    ctx.me.??(_.hasResource) option option(value := "moveTo")("移动到"),
                    option(value := "toStudy")("转研习"),
                    option(value := "toRecall")("转记谱"),
                    option(value := "toDistinguish")("转棋谱记录")
                  )
                )
              )
            )
          )
        )
    }

  def importedModal(form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content pgn-import none")(
      h2("导入PGN"),
      postForm(cls := "form3 import", action := controllers.routes.Importer.pgnImport())(
        form3.group(form("gameTag"), "自定义标签")(form3.input(_)),
        form3.group(form("pgn"), labelContent = frag(trans.pasteThePgnStringHere(), "（", a(href := staticUrl("static/对局批量导入.pgn"))("样例"), "）"), help = raw("最多导入20盘").some)(form3.textarea(_)(rows := 5, maxlength := s"${2 * 1024 * 1024}")),
        form3.group(form("pgnFile"), raw("或者上传一个PGN文件"), klass = "upload") { f =>
          form3.file.pgn(f.name)
        },
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("导入", "/".some, klass = "small")
        )
      )
    )

  def search(form: Form[_], user: User, pager: Paginator[Game])(implicit ctx: Context) = layout(
    title = "高级搜索",
    active = "search",
    form = form,
    user = user,
    pager = pager,
    call = routes.Resource.gameSearch()
  ) {
      views.html.search.user(user, form, routes.Resource.gameSearch())
    }

  private[resource] def layout(
    title: String,
    active: String,
    form: Form[_],
    user: User,
    pager: Paginator[Game],
    call: Call
  )(formFrag: Frag)(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreJs = frag(
      active == "search" option flatpickrTag,
      active == "search" option jsTag("search.js"),
      active == "imported" option frag(
        tagsinputTag,
        jsTag("importer.js")
      ),
      infiniteScrollTag,
      jsTag("resource.js")
    ),
    moreCss = frag(
      active == "search" option cssTag("user.show.search"),
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      active == "imported" option cssTag("importer"),
      cssTag("resource")
    )
  ) {
      var url = call.url.concat("?a=1")
      form.data.foreach {
        case (key, value) =>
          url = url.concat("&").concat(key).concat("=").concat(value)
      }

      main(cls := "page-menu resource")(
        st.aside(cls := "page-menu__menu")(
          div(cls := "resource-nav subnav")(
            menuLinks(active)
          )
        ),
        div(cls := "page-menu__content box")(
          formFrag,
          views.html.resource.bits.tmpForm,
          paginate(pager, call, active, form, user)
        )
      )
    }

  def paginate(pager: Paginator[Game], call: Call, active: String, form: Form[_], user: User)(implicit ctx: Context) = {
    var url = call.url.concat("?a=1")
    form.data.foreach {
      case (key, value) =>
        url = url.concat("&").concat(key).concat("=").concat(value)
    }

    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := "games infinitescroll")(
      pagerNext(pager, np => addQueryParameter(url, "page", np)) | div(cls := "none"),
      views.html.game.widgets(pager.currentPageResults, user = user.some,
        ownerLink = true, linkBlank = true, showFullMoves = active == "imported" || active == "gmmedb", showCheckbox = active == "imported" || active == "gmmedb")
    )
  }

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    val accept = ctx.me.??(_.hasResource)
    frag(
      a(activeCls("imported"), href := routes.Resource.gameImported())("导入的对局"),
      a(activeCls("shared"), href := routes.GameShare.relPage())("分享的对局"),
      accept option frag(
        a(activeCls("gamedb"), href := routes.GameDB.home())("对局数据库", views.html.member.bits.vTip),
        a(activeCls("search"), href := routes.Resource.gameSearch())("高级搜索", views.html.member.bits.vTip)
      ),
      !accept option frag(
        a(cls := "gamedb disabled")("对局数据库", views.html.member.bits.vTip),
        a(cls := "search disabled")("高级搜索", views.html.member.bits.vTip)
      )
    )
  }
}
