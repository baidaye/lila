package views.html.resource.interest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.interest.Source
import controllers.rt_resource.routes

object bits {

  def layout(
    title: String,
    active: Source
  )(topFrag: Frag)(pageFrag: Frag)(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreJs = frag(
      infiniteScrollTag,
      jsTag("resource.js"),
      jsTag("resource.interest.js")
    ),
    moreCss = frag(
      cssTag("resource")
    )
  ) {
      main(cls := "page-menu resource", dataNotAccept := s"${!(ctx.me.isDefined && ctx.me.??(_.hasResource))}")(
        st.aside(cls := "page-menu__menu subnav")(
          menuLinks(active)
        ),
        div(cls := "box")(
          topFrag,
          pageFrag
        )
      )
    }

  def menuLinks(active: Source)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active.id).option("active")
    frag(
      a(activeCls(Source.AnnihilationStar.id), href := routes.Interest.home(Source.AnnihilationStar.id))("吃星星"),
      a(activeCls(Source.AnnihilationPiece.id), href := routes.Interest.home(Source.AnnihilationPiece.id))("消灭对手"),
      a(activeCls(Source.CaptureKing.id), href := routes.Interest.home(Source.CaptureKing.id))("单子攻王"),
      a(activeCls(Source.UnprotectedPieces.id), href := routes.Interest.home(Source.UnprotectedPieces.id))("未保护棋子"),
      a(activeCls(Source.UnprotectedSquares.id), href := routes.Interest.home(Source.UnprotectedSquares.id))("未保护格子")
    )
  }

}
