package views.html.resource.interest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import play.api.data.Form
import lila.interest.{ InterestData, DataForm, Source }
import views.html.resource.puzzle.actions
import controllers.rt_resource.routes

object home {

  val dataLikeHref = attr("data-likeHref")
  val redSquare = attr("data-red-square")
  val greenSquare = attr("data-green-square")
  val yellowSquare = attr("data-yellow-square")

  def apply(sc: Source, form: Form[_], pager: Paginator[InterestData])(implicit ctx: Context) =
    views.html.resource.interest.bits.layout(
      title = s"兴趣-${sc.name}",
      active = sc
    ) {
      st.form(
        cls := "search_form interest_form",
        action := routes.Interest.page(sc.id),
        dataLikeHref := routes.Interest.like(sc.id),
        method := "GET"
      )(
          table(
            tr(
              th(label("棋色")),
              td(
                form3.tagsRadio(form("color"), DataForm.colorChoices, "white".some)
              )
            ),
            sc == Source.AnnihilationStar || sc == Source.AnnihilationPiece || sc == Source.CaptureKing option tr(
              th(label("棋子")),
              td(
                form3.tagsRadio(form("role"), DataForm.roleChoices.takeRight(4), "n".some)
              )
            ),
            sc == Source.UnprotectedPieces || sc == Source.UnprotectedSquares option tr(
              th(label("阶段")),
              td(
                form3.tagsRadio(form("phase"), DataForm.phaseChoices, "".some)
              )
            ),
            tr(
              th(label(sc match {
                case Source.UnprotectedPieces => "棋子数"
                case Source.UnprotectedSquares => "格子数"
                case _ => "步数"
              })),
              td(
                div(cls := "half")(form3.input3(form("stepMin"), vl = "1".some, "number")(st.placeholder := s"1 ~ ${sc.maxStep}")),
                div(cls := "half")(form3.input3(form("stepMax"), vl = s"${sc.maxStep}".some, "number")(st.placeholder := s"1 ~ ${sc.maxStep}"))
              )
            ),
            tr(
              th,
              td(globalError(form))
            ),
            tr(
              th,
              td(cls := "action")(
                submitButton(cls := "button")("搜索")
              )
            )
          ),
          table(
            tr(
              td(cls := "total")("共", div(cls := "count")(pager.nbResults), "题"),
              td(cls := "action")(
                pager.nbResults > 0 option actions(List("likeInterest"))
              )
            )
          )
        )
    } {
      paginate(sc, form, pager)
    }

  private[resource] def paginate(sc: Source, form: Form[_], pager: Paginator[InterestData])(implicit ctx: Context) = {
    var url = s"${routes.Interest.page(sc.id, 1)}?a=1"
    form.data.foreach {
      case (key, value) =>
        url = url.concat("&").concat(key).concat("=").concat(value)
    }

    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := s"now-playing list infinitescroll ${sc.id}")(
      pager.currentPageResults.map { p =>
        val bottomColor = if (sc == Source.UnprotectedPieces || sc == Source.UnprotectedSquares) !p.color else p.color
        a(cls := "paginated", target := "_blank", dataId := p.id, dataHref := controllers.rt_interest.routes.Interest.show(sc.id, p.id))(
          div(cls := "board-wrap")(
            sc == Source.UnprotectedSquares option {
              div(cls := "board-mask", style := s"${bottomColor.fold(p.color.fold("top", "bottom"), p.color.fold("bottom", "top"))}:0")
            },
            div(
              cls := List(
                s"mini-board cg-wrap is2d star-${p.color.name}" -> true,
                "parse-fen" -> (sc.id == Source.AnnihilationPiece.id || sc.id == Source.AnnihilationStar.id),
                "parse-fen-manual" -> (sc.id == Source.CaptureKing.id || sc.id == Source.UnprotectedPieces.id || sc.id == Source.UnprotectedSquares.id)
              ),
              dataColor := bottomColor.name,
              dataFen := p.fen,
              redSquare := (if (sc == Source.CaptureKing) { p.kingSquare | "" } else ""),
              greenSquare := (if (sc == Source.CaptureKing) p.square else ""),
              yellowSquare := (if (sc == Source.UnprotectedPieces) { p.colorKingSquare() | "" } else "")
            )(cgWrapContent)
          )
        )
      },
      pagerNext(pager, np => addQueryParameter(url, "page", np))
    )
  }

}
