package views.html.resource

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._

object puzzleMachineAdvisePage {

  def apply()(implicit ctx: Context) = views.html.base.layout(
    title = "机标题库介绍",
    moreCss = cssTag("resource")
  )(
      main(cls := "page-small box box-pad puzzleMachineAdvise")(
        h1("机标题库介绍"),
        div(cls := "s1")(
          div(cls := "s2")(
            "机标题库收集题目共155万道，分为16个段（A段-P段），每段10万道题（最后1段5万），由引擎分析机器进行标注分类。"
          )
        ),
        div(cls := "s1")(
          div(cls := "s2")(
            div(cls := "h2")(b("与主题搜索功能区别：")),
            ul(
              li("1）标签分类不完全相同：原有主题搜索功能题库由人工进行分类标注，机标题库全部由机器标注，某些题目可能不够典型。"),
              li("2）支持的变例不同：原有主题搜索收录题目，答案可能会存在多种；机标题库，除了最后1步将杀，中间过程都是唯一答案。"),
              li("3）机标题库是静态的，难度分数不会虽时间进行更新，而主题搜索题库中的题目的难度分数会随着用户做对或做错进行更新。")
            )
          )
        ),
        div(cls := "s1")(
          div(cls := "s2")(
            div(cls := "h2")(b("一些需要解释或不易理解的标签：")),
            ol(
              li(b("均势："), "Equality，从败局中翻盘，确保和棋或均势 (eval ≤ 200cp)。"),
              li(b("优势："), "Advantage，获得局面上的优势 (200cp ≤ eval ≤ 600cp)。"),
              li(b("胜势："), "Crushing，发现对手严重错误，获得制胜的优势 (eval ≥ 600cp)。"),
              li(b("过门(过渡招)："), "Intermezzo，在走一步预想的棋之前，先插入一个对手必须处理的招法，在走预想的棋，包含了过门和过渡招。"),
              li(b("安静一招："), "Quiet move，一步不打将也不吃子的棋，为之后的进攻做好准备。"),
              li(b("开线(清理)："), "Clearance，通常是在保持先手的情况下，通过一步棋，清理一个格子，打开直线或斜线，为将来的战术实施做准备。"),
              li(b("悬子："), "Hanging piece，局面中，对手有棋子处于无保护或缺少足够保护的情况。此类题目一般难度不高，适合初级棋手入门学习。")
            )
          )
        )
      )
    )
}

