package views.html.resource.openingdb

import play.mvc.Call
import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.opening.OpeningDB
import lila.opening.OpeningDBOrder.OpeningDBOrders
import controllers.rt_resource.routes

object list {

  def minePage(pager: Paginator[OpeningDB], form: Form[_], isCreateAccept: Boolean)(implicit ctx: Context) = {
    val call: Call = routes.OpeningDB.minePage(1)
    bits.layout(
      title = "我的开局库",
      active = "mine",
      page = "openingdb-list",
      moreJs = frag(
        infiniteScrollTag,
        tagsinputTag,
        jsTag("openingdb.list.js")
      ),
      moreCss = cssTag("openingdb.list")
    )(
        div(cls := "box__top")(
          div(cls := "box__top-main")(
            searchForm(form("text").value, call),
            newForm(isCreateAccept)
          ),
          div(cls := "box__top-tip")("若未建立过，金牌、银牌会员可免费新建1个")
        )
      )(
          table(cls := "openingdb-list-table")(
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNext(pager, np => addQueryParameter(call.url, "page", np)),
                pager.currentPageResults.map { o =>
                  tr(cls := "paginated")(
                    td(cls := "main")(
                      div(cls := "name")(
                        a(cls := "text", href := routes.OpeningDB.show(o.id))(o.name),
                        span(cls := "readWriteTag")("读/写")
                      ),
                      descFrag(o),
                      div(cls := "actions")(
                        a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开"),
                        a(cls := "button small", target := "_blank", href := routes.OpeningDB.recordPage(o.id, 1))("开局查询"),
                        ctx.userId.??(o.isOwner) && (isGranted(_.Team) || isGranted(_.Coach)) option a(cls := "button button-green small", target := "_blank", href := routes.OpeningDB.members(o.id))("授权管理")
                      )
                    ),
                    td(cls := "info")(
                      table(
                        tbody(
                          tr(
                            td("着法数："),
                            td(o.nodes, "/", OpeningDB.maxNode)
                          ),
                          tr(
                            td("更新日期："),
                            td(momentFromNow(o.updatedAt))
                          )
                        )
                      )
                    )
                  )
                }
              )
            } else tr(td(colspan := 2)("没有更多了"))
          )
        )
  }

  def memberPage(pager: Paginator[OpeningDB], form: Form[_])(implicit ctx: Context) = {
    val call: Call = routes.OpeningDB.memberPage(1)
    bits.layout(
      title = "参与开局库",
      active = "member",
      page = "openingdb-list",
      moreJs = frag(
        infiniteScrollTag,
        tagsinputTag,
        jsTag("openingdb.list.js")
      ),
      moreCss = cssTag("openingdb.list")
    )(
        div(cls := "box__top")(
          div(cls := "box__top-main")(
            searchForm(form("text").value, call)
          )
        )
      )(
          table(cls := "openingdb-list-table")(
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNext(pager, np => addQueryParameter(call.url, "page", np)),
                pager.currentPageResults.map { o =>
                  tr(cls := "paginated")(
                    td(cls := "main")(
                      div(cls := "name")(
                        a(cls := "text", href := routes.OpeningDB.show(o.id))(o.name),
                        span(cls := "readWriteTag")(if (ctx.userId.??(o.canWrite)) "读/写" else "只读")
                      ),
                      descFrag(o),
                      div(cls := "actions")(
                        a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开"),
                        a(cls := "button small", target := "_blank", href := routes.OpeningDB.recordPage(o.id, 1))("开局查询")
                      )
                    ),
                    td(cls := "info")(
                      table(
                        tbody(
                          tr(
                            td("着法数："),
                            td(o.nodes, "/", OpeningDB.maxNode)
                          ),
                          tr(
                            td("更新日期："),
                            td(momentFromNow(o.updatedAt))
                          )
                        )
                      )
                    )
                  )
                }
              )
            } else tr(td(colspan := 2)("没有更多了"))
          )
        )
  }

  def visiblePage(pager: Paginator[OpeningDB], form: Form[_])(implicit ctx: Context) = {
    val call: Call = routes.OpeningDB.visiblePage(1)
    bits.layout(
      title = "授权开局库",
      active = "visible",
      page = "openingdb-list",
      moreJs = frag(
        infiniteScrollTag,
        tagsinputTag,
        jsTag("openingdb.list.js")
      ),
      moreCss = cssTag("openingdb.list")
    )(
        div(cls := "box__top")(
          div(cls := "box__top-main")(
            searchForm(form("text").value, call)
          )
        )
      )(
          table(cls := "openingdb-list-table")(
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNext(pager, np => addQueryParameter(call.url, "page", np)),
                pager.currentPageResults.map { o =>
                  tr(cls := "paginated")(
                    td(cls := "main")(
                      div(cls := "name")(
                        a(cls := "text", href := routes.OpeningDB.show(o.id))(o.name),
                        span(cls := "readWriteTag")("只读")
                      ),
                      descFrag(o),
                      div(cls := "actions")(
                        a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开"),
                        a(cls := "button small", target := "_blank", href := routes.OpeningDB.recordPage(o.id, 1))("开局查询")
                      )
                    ),
                    td(cls := "info")(
                      table(
                        tbody(
                          tr(
                            td("着法数："),
                            td(o.nodes, "/", OpeningDB.maxNode)
                          ),
                          tr(
                            td("更新日期："),
                            td(momentFromNow(o.updatedAt))
                          )
                        )
                      )
                    )
                  )
                }
              )
            } else tr(td(colspan := 2)("没有更多了"))
          )
        )
  }

  def systemBuyPage(pager: Paginator[OpeningDB], mineOrders: OpeningDBOrders, tags: Set[String], form: Form[_])(implicit ctx: Context) = {
    val call: Call = routes.OpeningDB.systemBuyPage(1)
    bits.layout(
      title = "已购开局库",
      active = "systemBuy",
      page = "openingdb-list",
      moreJs = frag(
        infiniteScrollTag,
        tagsinputTag,
        jsTag("openingdb.list.js")
      ),
      moreCss = cssTag("openingdb.list")
    )(
        div(cls := "box__top")(
          div(cls := "box__top-main")(
            searchForm(form("text").value, call)
          ),
          tags.nonEmpty option st.form(cls := "box__top-tags tag-groups", action := call.url, method := "get")(
            emptyTag(form),
            form3.tags(form, "tags", tags)
          )
        )
      )(
          table(cls := "openingdb-list-table")(
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNext(pager, np => addQueryParameter(call.url, "page", np)),
                pager.currentPageResults.map { o =>
                  tr(cls := "paginated")(
                    td(cls := "main")(
                      div(cls := "name")(
                        a(cls := "text", href := routes.OpeningDB.show(o.id))(o.name),
                        span(cls := "readWriteTag")("只读")
                      ),
                      descFrag(o),
                      div(cls := "actions")(
                        a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开")
                      )
                    ),
                    td(cls := "info")(
                      table(
                        tbody(
                          tr(
                            td("着法数："),
                            td(o.nodes)
                          ),
                          mineOrders.lastOrder(o) map { order =>
                            tr(
                              td("到期日期："),
                              td(if (order.isForever) "永久" else order.expireAt.toString("yyyy-MM-dd"))
                            )
                          }
                        )
                      )
                    )
                  )
                }
              )
            } else tr(td(colspan := 2)("没有更多了"))
          )
        )
  }

  def systemPage(pager: Paginator[OpeningDB], mineOrders: OpeningDBOrders, tags: Set[String], form: Form[_])(implicit ctx: Context) = {
    val call: Call = routes.OpeningDB.systemPage(1)
    bits.layout(
      title = "系统开局库",
      active = "system",
      page = "openingdb-list",
      moreJs = frag(
        infiniteScrollTag,
        tagsinputTag,
        jsTag("openingdb.list.js")
      ),
      moreCss = cssTag("openingdb.list")
    )(
        div(cls := "box__top")(
          div(cls := "box__top-main")(
            searchForm(form("text").value, call)
          ),
          tags.nonEmpty option st.form(cls := "box__top-tags tag-groups", action := call.url, method := "get")(
            emptyTag(form),
            form3.tags(form, "tags", tags)
          )
        )
      )(
          table(cls := "openingdb-list-table")(
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNext(pager, np => addQueryParameter(call.url, "page", np)),
                pager.currentPageResults.map { o =>
                  val canRead = ctx.me.??(u => mineOrders.canRead(o, u.id) || o.systemFree(u))
                  tr(cls := "paginated")(
                    td(cls := "main")(
                      div(cls := "name")(
                        if (canRead) a(cls := "text", href := routes.OpeningDB.show(o.id))(o.name) else span(cls := "text")(o.name),
                        canRead option span(cls := "readWriteTag")("只读")
                      ),
                      descFrag(o),
                      div(cls := "actions")(
                        canRead option a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开"),
                        !canRead option frag(
                          a(cls := "button button-green small", target := "_blank", href := routes.OpeningDB.toBuySystem(o.id, lila.member.Days.Day7.id.some))("1元试用"),
                          a(cls := "button button-green small", target := "_blank", href := routes.OpeningDB.toBuySystem(o.id, lila.member.Days.Forever.id.some))(s"${o.roundPrice}元购买", systemDiscount(ctx))
                        ),
                        isGranted(_.SuperAdmin) option a(cls := "button button-green small modal-alert", href := routes.OpeningDB.systemSetting(o.id))("设置")
                      )
                    ),
                    td(cls := "info")(
                      table(
                        tbody(
                          tr(
                            td("着法数："),
                            td(o.nodes)
                          ),
                          o.system option tr(
                            td("已售："),
                            td(o.sells | 0)
                          )
                        )
                      )
                    )
                  )
                }
              )
            } else tr(td(colspan := 2)("没有更多了"))
          )
        )
  }

  private def searchForm(text: Option[String], call: Call) =
    st.form(cls := "search", action := call.toString, method := "get")(
      input(st.name := "text", value := text, st.placeholder := "搜索“名称”或“描述”"),
      submitButton(cls := "button", dataIcon := "y")
    )

  private def newForm(isCreateAccept: Boolean) =
    div(cls := "box__top__actions")(
      a(href := routes.OpeningDB.createForm(), cls := "button button-green text create", dataIcon := "O", dataNotAccept := !isCreateAccept)("新建开局库")
    )

  private def emptyTag(form: Form[_]) = {
    val tName = "emptyTag"
    val tLabel = "无标签"
    div(cls := "tag-group emptyTag")(
      span(
        st.input(
          st.id := tName,
          name := tName,
          tpe := "checkbox",
          value := form(tName).value,
          form(tName).value.contains("on") option checked
        ),
        label(`for` := tName)(tLabel)
      )
    )
  }

  private def descFrag(o: OpeningDB) =
    div(cls := "desc")(
      div(cls := "tags")(
        o.tags.map { tag =>
          span(tag)
        }
      ),
      lila.common.String.html.richText(o.desc)
    )

  def systemDiscount(implicit ctx: Context) = ctx.me match {
    case Some(me) => {
      if (me.isGold) frag("（8折）")
      else if (me.isSilver) frag("（9折）")
      else frag()
    }
    case None => frag()
  }

}
