package views.html.resource.openingdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.member.{ JsonView, Product }
import lila.user.User
import play.api.data.Form
import play.api.libs.json.Json

object createBuy {

  def apply(
    me: User,
    product: Product,
    discounts: List[(String, String)],
    existsInviteOrder: Boolean,
    form: Form[_]
  )(implicit ctx: Context) =
    bits.layout(
      title = "新建开局库-购买",
      active = "mine",
      page = "openingdb-create-buy",
      moreJs = frag(
        jsTag("buy.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.buy=${
          safeJsonValue(Json.obj(
            "member" -> JsonView.memberJson(me),
            "product" -> JsonView.productJson(product)
          ))
        }""")
      ),
      moreCss = cssTag("openingdb.buy")
    ) { h1(s"购买 ${product.name}") } {
        views.html.member.buyOne.form(me, product, discounts, existsInviteOrder, None, form)
      }

}
