package views.html.resource.openingdb

import lila.api.Context
import play.api.data.Form
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.opening.OpeningDB
import controllers.rt_resource.routes

object form {

  def create(form: Form[_])(implicit ctx: Context) =
    bits.layout(
      title = "新建开局库",
      active = "mine",
      page = "openingdb-form",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("openingdb.form")
      ),
      moreJs = jsTag("openingdb.form.js")
    )(h1("新建开局库")) {
        postForm(cls := "form3", dataNotAccept := s"${!(ctx.me.isDefined && ctx.me.??(_.hasResource))}", action := routes.OpeningDB.create())(
          form3.group(form("name"), "名称")(form3.input(_)),
          form3.group(form("desc"), "描述")(form3.textarea(_)(rows := 5)),
          form3.group(form("initialFen"), raw("起始位置"), klass = "starts-position none") { field =>
            val fieldVal = field.value
            val url = fieldVal.fold(controllers.routes.Editor.index)(f => controllers.routes.Editor.load(f)).url
            frag(
              div(cls := "group-child")(
                st.select(st.id := form3.id(field), name := field.name, cls := "form-control")(
                  option(value := chess.StartingPosition.initial.fen, fieldVal.has(chess.StartingPosition.initial.fen) option selected)(chess.StartingPosition.initial.name),
                  option(value := fieldVal, fieldVal.??(f => !chess.StartingPosition.allWithInitial.exists(_.fen == f)) option selected, dataId := "option-load-fen")("输入FEN"),
                  option(dataId := "option-load-situation")("载入局面"),
                  chess.StartingPosition.categories.map { categ =>
                    optgroup(attr("label") := categ.name)(
                      categ.positions.map { v =>
                        option(value := v.fen, fieldVal.has(v.fen) option selected)(v.fullName)
                      }
                    )
                  }
                )
              ),
              div(cls := "group-child")(
                input(
                  cls := List("form-control position-paste" -> true, "none" -> fieldVal.??(f => chess.StartingPosition.allWithInitial.exists(_.fen == f))),
                  placeholder := "在此处粘贴FEN棋谱", value := fieldVal
                )
              ),
              div(cls := "group-child")(
                a(cls := List("board-link" -> true, "none" -> fieldVal.has(chess.StartingPosition.initial.fen)), target := "_blank", href := url)(
                  div(cls := "preview")(
                    fieldVal.map { f =>
                      (chess.format.Forsyth << f).map { situation =>
                        div(
                          cls := "mini-board cg-wrap parse-fen is2d",
                          dataColor := situation.color.name,
                          dataFen := f
                        )(cgWrapContent)
                      }
                    }
                  )
                )
              )
            )
          },
          form3.actions(
            a(cls := "cancel", href := routes.Capsule.mineList())("取消"),
            submitButton(cls := List("button text" -> true), dataIcon := "E")("保存")
          )
        )
      }

  def update(opening: OpeningDB, form: Form[_])(implicit ctx: Context) =
    bits.layout(
      title = "修改开局库",
      active = "mine",
      page = "openingdb-form",
      moreCss = cssTag("openingdb.form"),
      moreJs = jsTag("openingdb.form.js")
    )(h1("修改开局库")) {
        postForm(cls := "form3", action := routes.OpeningDB.update(opening.id))(
          form3.group(form("name"), "名称")(form3.input(_)),
          form3.group(form("desc"), "描述")(form3.textarea(_)(rows := 5)),
          form3.actions(
            a(cls := "cancel", href := routes.Capsule.mineList())("取消"),
            submitButton(cls := List("button text" -> true), dataIcon := "E")("保存")
          )
        )
      }

}
