package views.html.resource.openingdb

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.opening.OpeningDB
import lila.user.User
import views.html.resource.capsule.form.userMark
import controllers.rt_resource.routes

object modal {

  def systemSettingModal(openingdb: OpeningDB, form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content openingdb-system-setting none")(
      h2(openingdb.name),
      postForm(cls := "form3", action := routes.OpeningDB.systemSetting(openingdb.id))(
        form3.group(form("price"), "价格（元）")(form3.input(_, "number")),
        form3.group(form("freeRole"), "免费授权")(_ => form3.tagsWithKv(form, "freeRoles", OpeningDB.FreeRole.choices)),
        form3.group(form("tags"), "标签")(form3.input(_)()),
        form3.group(form("sells"), "已售数量")(form3.input(_, "number")),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  def memberChoose(openingdb: OpeningDB, all: List[User], members: List[User], markMap: Map[String, Option[String]])(implicit ctx: Context) = frag(
    div(cls := "modal-content member-modal modal-member-choose none")(
      h2("选择成员"),
      st.form(cls := "member-search small")(
        input(name := "username", placeholder := "账号/备注（姓名）")
      ),
      postForm(cls := "form3 member-choose-transfer", action := routes.OpeningDB.memberChoose(openingdb.id))(
        form3.hidden("members", ""),
        div(cls := "player-transfer")(
          views.html.base.transfer[User, User](all, List.empty[User]) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.id}", value := left.id)),
              td(cls := "name")(userMark(left, markMap))
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.id}", value := right.id)),
              td(cls := "name")(userMark(right, markMap))
            )
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

}
