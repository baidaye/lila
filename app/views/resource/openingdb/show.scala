package views.html.resource.openingdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.opening.OpeningDB
import play.api.libs.json._

object show {

  def apply(openingdb: OpeningDB, openingdbJson: JsObject, nodePathMapJson: Option[JsObject], roundJson: JsObject, prefJson: JsObject)(implicit ctx: Context) = {
    views.html.base.layout(
      title = openingdb.name,
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("openingdb.show")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("resource.saveTo.js"),
        jsAt(s"compiled/lichess.openingdb${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.openingdb=${
          safeJsonValue(Json.obj(
            "i18n" -> i18nJsObject(translations),
            "userId" -> ctx.userId,
            "isCoachOrTeam" -> ctx.me.??(_.isCoachOrTeam),
            "isMemberOrCoachOrTeam" -> ctx.me.??(_.isMemberOrCoachOrTeam),
            "openingdb" -> openingdbJson,
            "nodePathMap" -> nodePathMapJson,
            "round" -> roundJson,
            "pref" -> prefJson
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "openingdb")
      }
  }

  private val translations = List(
    // also uses gameOver
    trans.depthX,
    trans.usingServerAnalysis,
    trans.loadingEngine,
    trans.cloudAnalysis,
    trans.goDeeper,
    trans.showThreat,
    trans.inLocalBrowser,
    trans.toggleLocalEvaluation,
    // ceval menu
    trans.computerAnalysis,
    trans.enable,
    trans.bestMoveArrow,
    trans.evaluationGauge,
    trans.infiniteAnalysis,
    trans.removesTheDepthLimit,
    trans.multipleLines,
    trans.cpus,
    trans.memory,
    // explorer (also uses gameOver, checkmate, stalemate, draw, variantEnding)
    trans.openingExplorerAndTablebase,
    trans.openingExplorer,
    trans.xOpeningExplorer,
    trans.move,
    trans.games,
    trans.variantLoss,
    trans.variantWin,
    trans.insufficientMaterial,
    trans.capture,
    trans.pawnMove,
    trans.close,
    trans.winning,
    trans.unknown,
    trans.losing,
    trans.drawn,
    trans.timeControl,
    trans.averageElo,
    trans.database,
    trans.recentGames,
    trans.topGames,
    trans.whiteDrawBlack,
    trans.averageRatingX,
    trans.masterDbExplanation,
    trans.mateInXHalfMoves,
    trans.nextCaptureOrPawnMoveInXHalfMoves,
    trans.noGameFound,
    trans.maybeIncludeMoreGamesFromThePreferencesMenu,
    trans.winPreventedBy50MoveRule,
    trans.lossSavedBy50MoveRule,
    trans.allSet
  )

}

