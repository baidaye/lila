package views.html.resource.openingdb

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.opening.{ OpeningDB, OpeningDBRecord }
import org.joda.time.DateTime
import controllers.rt_resource.routes

object record {

  private val moveTag = tag("move")
  private val indexTag = tag("index")
  def apply(openingdb: OpeningDB, form: Form[_], pager: Paginator[OpeningDBRecord])(implicit ctx: Context) =
    bits.layout(
      title = "开局查询",
      active = "mine",
      page = "openingdb-record",
      moreJs = frag(
        flatpickrTag,
        infiniteScrollTag,
        delayFlatpickrStart,
        jsTag("openingdb.record.js")
      ),
      moreCss = cssTag("openingdb.record")
    ) {
        div(
          h1(openingdb.name),
          st.form(
            cls := "openingdb_record_form",
            action := s"${routes.OpeningDB.recordPage(openingdb.id, 1)}#results",
            method := "GET"
          )(
              table(
                tr(
                  th(label("开局名称")),
                  td(form3.input(form("name"))),
                  th(label("来源")),
                  td(form3.select(form("source"), OpeningDBRecord.Source.choices, "全部".some))
                ),
                tr(
                  th(label("保存时间")),
                  td(form3.input2(form("dateMin"), vl = DateTime.now.minusMonths(3).toString("yyyy-MM-dd").some, klass = "flatpickr")),
                  th(label("至")),
                  td(form3.input2(form("dateMax"), vl = DateTime.now.toString("yyyy-MM-dd").some, klass = "flatpickr"))
                )
              ),
              div(cls := "pgn-area")(
                label("PGN"),
                div(
                  iframe(
                    id := "board_iframe",
                    src := controllers.routes.UserAnalysis.pgnEmbed(form("pgn").value),
                    st.frameborder := 0,
                    width := "100%",
                    height := "200px"
                  ),
                  form3.textarea(form("pgn"))(rows := 3, st.placeholder := "在此处粘贴PGN棋谱（以此PGN为准）")
                )
              ),
              div(cls := "action")(
                submitButton(cls := "button")("搜索")
              )
            )
        )
      } {
        table(cls := "slist")(
          if (pager.nbResults > 0) {
            tbody(cls := "infinitescroll")(
              pagerNext(pager, np => addQueryParameter(routes.OpeningDB.recordPage(openingdb.id, 1).toString, "page", np)),
              pager.currentPageResults.map { record =>
                val virtualOpeningDB = record.virtualOpeningDB
                val moves = virtualOpeningDB.toTurns()
                tr(cls := "paginated")(
                  td(cls := "td-board")(
                    a(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := virtualOpeningDB.lastFen.value,
                      href := routes.OpeningDB.show(record.oid),
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(cls := "td-infos")(
                    div(cls := "infos")(
                      div(cls := "info")(
                        div(cls := "name")(record.shortName),
                        div(cls := "desc")(record.name),
                        div(cls := "date")(record.createdAt.toString("yyyy-MM-dd HH:mm"))
                      ),
                      div(cls := "actions")(
                        a(cls := "button small", href := s"${controllers.routes.UserAnalysis.pgnPage(record.pgn.some)}#explorer", target := "_blank")("开局浏览器"),
                        postForm(st.action := routes.OpeningDB.recordRemove(openingdb.id, record.id))(
                          button(cls := "button button-red small confirm", title := "删除仅从列表中删除，不影响关联的开局库，是否继续？")("删除")
                        )
                      )
                    ),
                    div(cls := "moves")(
                      moves.map { move =>
                        moveTag(
                          indexTag(move.number, "."),
                          move.white.map { w =>
                            span(dataFen := w.fen.value)(w.move.san)
                          } getOrElse span(cls := "disabled")("..."),
                          move.black.map { b =>
                            span(dataFen := b.fen.value)(b.move.san)
                          } getOrElse span(cls := "disabled")
                        )
                      }
                    )
                  )
                )
              }
            )
          } else tr(td(colspan := 2)("没有更多了"))
        )
      }

}
