package views.html.resource.openingdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.opening.OpeningDB
import lila.opening.OpeningDBOrder.OpeningDBOrders
import controllers.rt_resource.routes

object cantRead {

  def apply(o: OpeningDB, mineOrders: OpeningDBOrders)(implicit ctx: Context) = {
    views.html.base.layout(
      title = o.name,
      moreCss = cssTag("openingdb.cantRead")
    ) {
        val canRead = ctx.me.??(u => mineOrders.canRead(o, u.id) || o.systemFree(u))
        val isBuyForever = ctx.me.??(u => o.isBuyForever(u, mineOrders))
        main(cls := "box box-pad page-small openingdb-cantRead")(
          h1("没有访问权限"),
          div(cls := "desc")(
            h2(o.name),
            br,
            lila.common.String.html.richText(o.desc)
          ),
          hr,
          div(
            h3(b("可能的原因：")),
            ol(
              li("> 你不是开局库的创建者；"),
              li("> 你不是开局库的参与者；"),
              li("> 开局库的可见范围内不包括你；"),
              li("> 你与开局库创建者失去教练学员或俱乐部成员关系；"),
              li("> 你失去了金牌会员或银牌会员角色。")
            )
          ),
          br,
          div(cls := "buy")(
            a(cls := "button small", href := routes.OpeningDB.minePage(1))("返回"),
            canRead option a(cls := "button small", href := routes.OpeningDB.show(o.id))("打开"),
            o.system option frag(
              !isBuyForever option a(cls := "button button-green small", href := routes.OpeningDB.toBuySystem(o.id, lila.member.Days.Day7.id.some))("1元试用"),
              !isBuyForever option a(cls := "button button-green small", href := routes.OpeningDB.toBuySystem(o.id, lila.member.Days.Forever.id.some))("购买", list.systemDiscount(ctx))
            )
          )
        )
      }
  }

}

