package views.html.resource.openingdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.opening.OpeningDB
import lila.opening.OpeningDB.Visibility
import lila.user.User
import play.api.data.Form
import views.html.resource.capsule.form.userMark
import controllers.rt_resource.routes

object members {

  val dataTab = attr("data-tab")

  def apply(openingdb: OpeningDB, users: List[User], markMap: Map[String, Option[String]], form: Form[_])(implicit ctx: Context) =
    bits.layout(
      title = s"${openingdb.name} 授权管理",
      active = "mine",
      page = "openingdb-member",
      moreJs = frag(
        transferTag,
        jsTag("openingdb.member.js")
      ),
      moreCss = cssTag("openingdb.list")
    ) { h1(s"${openingdb.name} 授权管理") } {
        val visibilitySelects = if (ctx.me.??(_.isTeam)) Visibility.teamSelects else if (ctx.me.??(_.isCoach)) Visibility.coachSelects else Visibility.privateSelects
        div(cls := "members active")(
          div(cls := "top")(
            div(cls := "form3", dataHref := routes.OpeningDB.setVisibility(openingdb.id))(
              form3.split(
                form3.group(form("visibility"), "可见范围", half = true)(form3.select(_, visibilitySelects))
              )
            ),
            div(cls := "actions")(
              a(cls := "button button-green modal-alert", href := routes.OpeningDB.memberChoose(openingdb.id))("添加")
            )
          ),
          table(cls := "slist")(
            thead(
              tr(
                th("账号"),
                th("备注（姓名）"),
                th("角色"),
                th("权限"),
                th("操作")
              )
            ),
            openingdb.members.sorted.map {
              case (userId, member) => {
                val user = users.find(_.id == userId) err s"can not find user $userId"
                tr(
                  td(userLink(user, withBadge = false)),
                  td(userMark(user, markMap)),
                  td(member.role.name),
                  td(if (member.role.canWrite) "读/写" else "只读"),
                  td(cls := "actions")(
                    !member.isOwner option frag(
                      member.isReader && user.isCoach option postForm(action := routes.OpeningDB.setMemberRole(openingdb.id, userId))(
                        submitButton(cls := "button button-empty small", name := "role", value := "w")("设为贡献者")
                      ),
                      member.isWriter option postForm(action := routes.OpeningDB.setMemberRole(openingdb.id, userId))(
                        submitButton(cls := "button button-empty small", name := "role", value := "r")("设为参与者")
                      ),
                      postForm(action := routes.OpeningDB.removeMember(openingdb.id, userId))(
                        submitButton(cls := "button button-empty small button-red confirm", title := "是否确认移除成员？")("移除")
                      )
                    )
                  )
                )
              }
            }
          ),
          div(
            h3(strong("说明：")),
            ul(
              li("1、仅可以授权给系统付费用户；"),
              li("2、教练可以授权给自己的学员，只读；"),
              li("3、俱乐部可以授权给俱乐部成员及教练，只读/读写。")
            )
          )
        )
      }

}
