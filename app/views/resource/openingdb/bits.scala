package views.html.resource.openingdb

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.game.PgnSetup
import controllers.rt_resource.routes
import lila.opening.OpeningDB

object bits {

  def layout(
    title: String,
    active: String,
    page: String,
    moreCss: Frag = emptyFrag,
    moreJs: Frag = emptyFrag
  )(topFrag: Frag)(mainFrag: Frag)(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreJs = moreJs,
      moreCss = moreCss
    ) {
      main(cls := s"page-menu $page")(
        st.aside(cls := "page-menu__menu subnav")(
          menuLinks(active)
        ),
        div(cls := "page-menu__content box box-pad")(
          topFrag,
          mainFrag
        )
      )
    }

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("mine"), href := routes.OpeningDB.minePage(1))("我的开局库"),
      a(activeCls("member"), href := routes.OpeningDB.memberPage(1))("参与开局库", views.html.member.bits.vTip),
      a(activeCls("visible"), href := routes.OpeningDB.visiblePage(1))("已授权开局库", views.html.member.bits.vTip),
      a(activeCls("systemBuy"), href := routes.OpeningDB.systemBuyPage(1))("已购开局库"),
      a(activeCls("system"), href := routes.OpeningDB.systemPage(1))("系统开局库")
    )
  }

  def showOpeningdb(pgnSetup: PgnSetup) = {
    div(cls := "openingdb", style := "margin-top: .5em;")(
      div("开局库：", a(href := routes.OpeningDB.show(pgnSetup.pgnOpeningDB.opening.id), target := "_blank")(pgnSetup.pgnOpeningDB.opening.name)),
      div(
        div(label("白方："), span(if (pgnSetup.openingdbExcludeWhiteBlunder | true) "排除错误着法" else "不排除错误着法", "、", if (pgnSetup.openingdbExcludeWhiteJscx | true) "排除少见着法" else "不排除少见着法")),
        div(label("黑方："), span(if (pgnSetup.openingdbExcludeBlackBlunder | true) "排除错误着法" else "不排除错误着法", "、", if (pgnSetup.openingdbExcludeBlackJscx | true) "排除少见着法" else "不排除少见着法")),
        div(label("可以提前脱谱：", if (pgnSetup.openingdbCanOff | true) "是" else "否"))
      )
    )
  }

}
