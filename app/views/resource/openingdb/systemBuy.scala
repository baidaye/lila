package views.html.resource.openingdb

import play.api.libs.json.Json
import lila.api.Context
import play.api.data.Form
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.member.{ JsonView, Product }
import lila.user.User
import controllers.rt_resource.routes

object systemBuy {

  def apply(
    me: User,
    product: Product,
    discounts: List[(String, String)],
    existsInviteOrder: Boolean,
    defaultItemCode: Option[String],
    form: Form[_]
  )(implicit ctx: Context) =
    bits.layout(
      title = "系统开局库-购买",
      active = "mine",
      page = "openingdb-system-buy",
      moreJs = frag(
        jsTag("buy.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.buy=${
          safeJsonValue(Json.obj(
            "member" -> JsonView.memberJson(me),
            "product" -> JsonView.productJson(product)
          ))
        }""")
      ),
      moreCss = cssTag("openingdb.buy")
    ) { h1(s"购买 ${product.name}") } {
        views.html.member.buyOne.form(me, product, discounts, existsInviteOrder, defaultItemCode, form)
      }

}
