package views.html.resource

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.puzzle.Puzzle
import lila.resource.{ Capsule, Sorting, ThemeQuery }
import play.api.data.Form
import play.mvc.Call
import lila.hub.PuzzleHub
import controllers.rt_resource.routes

object puzzle {

  def imported(form: Form[_], pager: Paginator[Puzzle], tags: Set[String])(implicit ctx: Context) = layout(
    title = "导入的战术题",
    active = "imported",
    form = form,
    pager = pager,
    call = routes.Resource.puzzleImported(),
    showCheckbox = true
  ) {
      st.form(
        cls := "search_form imported_form",
        action := s"${routes.Resource.puzzleImported()}#results",
        method := "GET"
      )(
          table(
            tr(
              td(cls := "tag-groups")(
                emptyTag(form),
                form3.tags(form, "tags", tags)
              )
            ),
            tr(
              td(cls := "action")(
                a(href := controllers.routes.UserAnalysis.index, cls := "button")("录入"),
                a(cls := "button modal-alert", href := controllers.routes.Importer.puzzleImportForm())("导入"),
                pager.nbResults > 0 option actions(List("delete", "toCapsule"))
              )
            )
          )
        )
    }

  def importedModal(form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content puzzle-import none")(
      h2("导入战术题"),
      postForm(cls := "form3 import", action := controllers.routes.Importer.puzzleImport())(
        form3.group(form("puzzleTag"), "自定义标签")(form3.input(_)),
        form3.group(form("hasLastMove"), raw("格式"))(form3.select(_, List(
          false -> "局面 + 答案",
          true -> "原始局面 + 对方初始着法 + 答案"
        ))),
        form3.group(form("pgn"), labelContent = frag(trans.pasteThePgnStringHere(), "（", a(href := staticUrl("static/战术题批量导入.pgn"))("样例"), "）"), help = raw("最多导入20盘").some)(
          form3.textarea(_)(rows := 5, maxlength := s"${2 * 1024 * 1024}", placeholder := "#样例# 局面 + 答案\n[FEN \"8/2r4p/8/P3N3/1P1p4/7P/4Kpk1/5R2 b - - 7 58\"]\n58... Rc2+ 59. Kd3 Kxf1\n\n#样例# 原始局面 + 对方初始着法 + 答案\n[FEN \"8/2r4p/8/P3N3/1P1p4/7P/3K1pk1/5R2 w - - 6 58\"]\n58. Ke2 Rc2+ 59. Kd3 Kxf1")
        ),
        form3.group(form("pgnFile"), raw("或者上传一个PGN文件"), klass = "upload") { f =>
          form3.file.pgn(f.name)
        },
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("导入", "/".some, klass = "small")
        )
      )
    )

  def emptyTag(form: Form[_]) = {
    val tName = "emptyTag"
    val tLabel = "无标签"
    div(cls := "tag-group emptyTag")(
      span(
        st.input(
          st.id := tName,
          name := tName,
          tpe := "checkbox",
          value := form(tName).value,
          form(tName).value.contains("on") option checked
        ),
        label(`for` := tName)(tLabel)
      )
    )
  }

  def theme(form: Form[_], pager: Paginator[Puzzle], tags: Set[String])(implicit ctx: Context) = layout(
    title = "主题搜索",
    active = "theme",
    form = form,
    pager = pager,
    call = routes.Resource.puzzleTheme()
  ) {
      st.form(
        cls := "search_form theme_form",
        action := s"${routes.Resource.puzzleTheme()}#results",
        method := "GET",
        dataId := "theme"
      )(
          table(
            tr(
              th(label("题号范围")),
              td(
                div(cls := "half")(form3.input3(form("idMin"), vl = PuzzleHub.minThemeId.toString.some, "number")(required, st.placeholder := s"${PuzzleHub.minThemeId} ~ ${PuzzleHub.maxThemeId}")),
                div(cls := "half")(form3.input3(form("idMax"), vl = PuzzleHub.maxThemeId.toString.some, "number")(required, st.placeholder := s"${PuzzleHub.minThemeId} ~ ${PuzzleHub.maxThemeId}"))
              )
            ),
            tr(
              th(label("难度范围")),
              td(
                div(cls := "half")(form3.input(form("ratingMin"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}")),
                div(cls := "half")(form3.input(form("ratingMax"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}"))
              )
            ),
            tr(
              th(label("答案步数")),
              td(
                div(cls := "half")(form3.input(form("stepsMin"), "number")(st.placeholder := "1 ~ 10")),
                div(cls := "half")(form3.input(form("stepsMax"), "number")(st.placeholder := "1 ~ 10"))
              )
            ),
            tr(
              th(label("黑白")),
              td(
                form3.tagsWithKv(form, "pieceColor", ThemeQuery.pieceColor, klass = "single")
              )
            ),
            tr(
              th(label("阶段")),
              td(
                form3.tagsWithKv(form, "phase", ThemeQuery.phase, klass = "single")
              )
            ),
            tr(
              th(label("目的")),
              td(
                form3.tagsWithKv(form, "moveFor", ThemeQuery.moveFor, klass = "single")
              )
            ),
            tr(
              th(label("子力")),
              td(
                form3.tagsWithKv(form, "strength", ThemeQuery.strength)
              )
            ),
            tr(
              th(label("局面")),
              td(
                form3.tagsWithKv(form, "chessGame", ThemeQuery.chessGame)
              )
            ),
            tr(
              th(label("技战术")),
              td(
                form3.tagsWithKv(form, "subject", ThemeQuery.subject)
              )
            ),
            tr(
              th(label("综合")),
              td(
                div(cls := "tag-group-wrap")(
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveThemeMateIn, klass = "single"),
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveTheme, ThemeQuery.ResComprehensiveThemeMateIn.length, ThemeQuery.comprehensive.length)
                )
              )
            ),
            /*            tr(
              th(label("标签")),
              td(
                form3.tags(form, "tags", tags)
              )
            ),*/
            tr(
              th(label("排序")),
              td(
                form3.select(form("order"), Sorting.orders)
              )
            ),
            tr(
              th,
              td(cls := "action")(
                submitButton(cls := "button")("搜索")
              )
            )
          ),
          table(
            tr(
              td(cls := "total")("共", div(cls := "countWaiting")(spinner), div(cls := "count none")("-"), "题"),
              td(cls := "action")(
                pager.nbResults > 0 option actions(List("toCapsule"))
              )
            )
          )
        )
    }

  val idRang = "ABCDEFGHIJKLMNOP".split("").zipWithIndex.map {
    case (c, i) => (i -> s"$c 段")
  }

  def machine(form: Form[_], pager: Paginator[Puzzle])(implicit ctx: Context) = layout(
    title = "机标题库",
    active = "machine",
    form = form,
    pager = pager,
    call = routes.Resource.puzzleMachine()
  ) {
      st.form(
        cls := "search_form machine_form",
        action := s"${routes.Resource.puzzleMachine()}#results",
        method := "GET",
        dataId := "machine"
      )(
          table(
            tr(
              th(label("题号区间")),
              td(
                div(cls := "half")(form3.select(form("idRange"), idRang)),
                div(cls := "half")(
                  a(target := "_blank", cls := "advise", href := routes.Resource.puzzleMachineAdvise)("题库介绍")
                )
              )
            ),
            tr(
              th(label("题号范围")),
              td(
                div(cls := "half")(form3.input3(form("idMin"), vl = "1".some, "number")(st.placeholder := s"1 ~ 100000")),
                div(cls := "half")(form3.input3(form("idMax"), vl = "100000".some, "number")(st.placeholder := s"1 ~ 100000"))
              )
            ),
            tr(
              th(label("难度范围")),
              td(
                div(cls := "half")(form3.input(form("ratingMin"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}")),
                div(cls := "half")(form3.input(form("ratingMax"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}"))
              )
            ),
            tr(
              th(label("黑白")),
              td(
                form3.tagsWithKv(form, "pieceColor", ThemeQuery.pieceColor, klass = "single")
              )
            ),
            tr(
              th(label("阶段")),
              td(
                form3.tagsWithKv(form, "phase", ThemeQuery.phase, klass = "single")
              )
            ),
            tr(
              th(label("答案长度")),
              td(
                form3.tagsWithKv(form, "moves", ThemeQuery.moves, klass = "single")
              )
            ),
            tr(
              th(label("目的")),
              td(
                form3.tagsWithKv(form, "moveFor", ThemeQuery.moveForMachine, klass = "single")
              )
            ),
            tr(
              th(label("子力")),
              td(
                form3.tagsWithKv(form, "strength", ThemeQuery.strength)
              )
            ),
            tr(
              th(label("技战术")),
              td(
                form3.tagsWithKv(form, "subject", ThemeQuery.subjectMachine)
              )
            ),
            tr(
              th(label("综合")),
              td(
                div(cls := "tag-group-wrap")(
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveMachineMateIn, klass = "single"),
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveMachineMate, ThemeQuery.ResComprehensiveMachineMateIn.length, ThemeQuery.comprehensiveMachine.length, klass = "single"),
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveMachineEndgame, (ThemeQuery.ResComprehensiveMachineMateIn ++ ThemeQuery.ResComprehensiveMachineMate).length, ThemeQuery.comprehensiveMachine.length, klass = "single"),
                  form3.tagsWithKv(form, "comprehensive", ThemeQuery.ResComprehensiveMachineRemaind, (ThemeQuery.ResComprehensiveMachineMateIn ++ ThemeQuery.ResComprehensiveMachineMate ++ ThemeQuery.ResComprehensiveMachineEndgame).length, ThemeQuery.comprehensiveMachine.length)
                )
              )
            ),
            tr(
              th(label("排序")),
              td(
                form3.select(form("order"), Sorting.machineOrders)
              )
            ),
            tr(
              th,
              td(cls := "action")(
                submitButton(cls := "button")("搜索")
              )
            )
          ),
          table(
            tr(
              td(cls := "total")("共", div(cls := "countWaiting")(spinner), div(cls := "count none")("-"), "题"),
              td(cls := "action")(
                pager.nbResults > 0 option actions(List("toCapsule"))
              )
            )
          )
        )
    }

  def capsule(pager: Paginator[Puzzle], capsule: Capsule)(implicit ctx: Context) = layout(
    title = "战术题列表",
    active = "capsule",
    form = lila.resource.DataForm.capsule.puzzleOrder,
    pager = pager,
    call = routes.Capsule.puzzleCapsule(capsule.id)
  ) {
      st.form(
        cls := "search_form capsule_puzzle_form",
        action := s"${routes.Capsule.puzzleCapsule(capsule.id)}#results",
        method := "GET"
      )(
          table(
            tr(th("名称："), td(capsule.name)),
            capsule.tags.nonEmpty option tr(th("标签："), td(div(cls := "tags")(capsule.tags.map(span(_))))),
            capsule.desc.map(d => tr(th("描述："), td(d)))
          )
        )
    }

  /*  def targetUrl(active: String, puzzleId: Int, url: String) = {
    s"${routes.Puzzle.themePuzzle(puzzleId)}?${url.substring(url.indexOf("\\?") + 1)}"
  }*/

  private val dataLastMove = attr("data-lastmove")

  private[resource] def miniPuzzle(p: lila.puzzle.Puzzle, active: String, url: String, showCheckbox: Boolean = false) = {
    // targetUrl(active, p.id, url)
    a(cls := "paginated", target := "_blank", dataId := p.id, dataHref := controllers.routes.Puzzle.show(p.id, false, false))(
      div(
        cls := "mini-board cg-wrap parse-fen is2d",
        dataColor := p.color.name,
        dataFen := p.fenAfterInitialMove,
        dataLastMove := p.initialUci
      )(cgWrapContent),
      div(cls := "btm")(
        span((active != "imported") option label("难度：", if (p.isImport) "NA" else p.perf.glicko.rating.toInt))
      /*showCheckbox option input(tpe := "checkbox", id := s"cbx-${p.id}", name := "cbx-puzzle", value := p.id)*/
      )
    )
  }

  private[resource] def paginate(pager: Paginator[Puzzle], call: Call, active: String, form: Form[_], showCheckbox: Boolean = false)(implicit ctx: Context) = {
    var url = if (call.url.contains("?")) call.url else call.url.concat("?a=1")
    form.data.foreach {
      case (key, value) =>
        url = url.concat("&").concat(key).concat("=").concat(value)
    }

    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := "now-playing list infinitescroll")(
      pager.currentPageResults.map { p =>
        miniPuzzle(p, active, url, showCheckbox)
      },
      pagerNext(pager, np => addQueryParameter(url, "page", np))
    )
  }

  private[resource] def layout(
    title: String,
    active: String,
    form: Form[_],
    pager: Paginator[Puzzle],
    call: Call,
    showCheckbox: Boolean = false
  )(topFrag: Frag)(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreJs = frag(
      infiniteScrollTag,
      jsTag("resource.js"),
      active == "imported" option frag(
        tagsinputTag,
        jsTag("importer.js")
      )
    ),
    moreCss = frag(
      cssTag("resource"),
      active == "imported" option cssTag("importer")
    )
  ) {
      main(cls := "page-menu resource")(
        st.aside(cls := "page-menu__menu subnav")(
          menuLinks(active)
        ),
        div(cls := "box")(
          topFrag,
          paginate(pager, call, active, form, showCheckbox)
        )
      )
    }

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    val accept = ctx.me.??(_.hasResource)
    val acceptMachine = ctx.me.??(_.isCoachOrTeam)
    frag(
      a(activeCls("imported"), href := routes.Resource.puzzleImported())("导入的战术题"),
      accept option frag(
        a(activeCls("theme"), href := routes.Resource.puzzleTheme())("主题搜索", views.html.member.bits.vTip),
        acceptMachine option a(activeCls("machine"), href := s"${routes.Resource.puzzleMachine()}?idRange=0&idMin=1&idMax=100000")("机标题库"),
        a(activeCls("capsule"), href := routes.Capsule.mineList())("战术题列表", views.html.member.bits.vTip)
      ),
      !accept option frag(
        a(cls := "theme disabled")("主题搜索", views.html.member.bits.vTip),
        a(cls := "capsule disabled")("战术题列表", views.html.member.bits.vTip)
      )
    )
  }

  def actions(as: List[String]) = frag(
    select(cls := "select")(
      option(value := "")("选中的（单击）"),
      option(value := "all")("选中所有"),
      option(value := "none")("取消选择")
    ),
    select(cls := "action")(
      option(value := "")("操作"),
      as.contains("delete") option option(value := "delete")("删除"),
      as.contains("unlike") option option(value := "unlike")("取消收藏"),
      as.contains("likeInterest") option option(value := "likeInterest")("收藏"),
      as.contains("unlikeInterest") option option(value := "unlikeInterest")("取消收藏"),
      as.contains("toCapsule") option option(value := "toCapsule")("添加到列表"),
      as.contains("ascSort") option option(value := "ascSort")("按难度正序"),
      as.contains("descSort") option option(value := "descSort")("按难度倒序")
    )
  )

}
