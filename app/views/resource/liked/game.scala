package views.html.resource.liked

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.game.Game
import lila.user.User
import play.api.data.Form
import views.html.resource.puzzle.emptyTag
import views.html.resource.game.{ paginate => gamePaginate }
import controllers.rt_resource.routes

object game {

  def apply(form: Form[_], pager: Paginator[Game], tags: Set[String], user: User)(implicit ctx: Context) =
    views.html.resource.liked.bits.layout(
      title = "收藏-对局",
      active = "gameLiked",
      form = form
    ) {
      st.form(
        cls := "search_form liked_form",
        action := s"${routes.Resource.gameLiked()}#results",
        method := "GET"
      )(
          form3.hidden("gameSource", "like"),
          table(
            tr(
              td(cls := "tag-groups")(
                emptyTag(form),
                form3.tags(form, "tags", tags)
              )
            ),
            tr(
              td(cls := "action")(
                select(cls := "select")(
                  option(value := "")("选择"),
                  option(value := "all")("选中所有"),
                  option(value := "none")("取消选择")
                ),
                select(cls := "action")(
                  option(value := "")("操作"),
                  option(value := "toPrint")("打印"),
                  ctx.me.??(_.hasResource) option option(value := "copyTo")("复制到"),
                  ctx.me.??(_.hasResource) option option(value := "moveTo")("移动到"),
                  option(value := "toStudy")("转研习"),
                  option(value := "toRecall")("转记谱"),
                  option(value := "toDistinguish")("转棋谱记录"),
                  option(value := "unlike")("取消收藏")
                )
              )
            )
          )
        )
    } {
      gamePaginate(pager, routes.Resource.gameLiked(), "gameLiked", form, user)
    }

}
