package views.html.resource.liked

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.data.Form
import lila.interest.Source
import controllers.rt_resource.routes

object bits {

  def layout(
    title: String,
    active: String,
    form: Form[_]
  )(topFrag: Frag)(pageFrag: Frag)(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreJs = frag(
      infiniteScrollTag,
      jsTag("resource.js"),
      List("g1Liked", "g2Liked", "g3Liked", "g4Liked", "g5Liked").contains(active) option jsTag("resource.interest.js")
    ),
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("resource")
    )
  ) {
      main(cls := "page-menu resource")(
        st.aside(cls := "page-menu__menu subnav")(
          menuLinks(active)
        ),
        div(cls := "box")(
          topFrag,
          pageFrag
        )
      )
    }

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("puzzleLiked"), href := routes.Resource.puzzleLiked())("战术题"),
      a(activeCls("gameLiked"), href := routes.Resource.gameLiked())("对局"),
      a(activeCls("g1Liked"), href := routes.Interest.likePage(Source.AnnihilationStar.id, 1))("吃星星"),
      a(activeCls("g2Liked"), href := routes.Interest.likePage(Source.AnnihilationPiece.id, 1))("消灭对手"),
      a(activeCls("g3Liked"), href := routes.Interest.likePage(Source.CaptureKing.id, 1))("单子攻王"),
      a(activeCls("g4Liked"), href := routes.Interest.likePage(Source.UnprotectedPieces.id, 1))("未保护棋子"),
      a(activeCls("g5Liked"), href := routes.Interest.likePage(Source.UnprotectedSquares.id, 1))("未保护格子")
    )
  }

}
