package views.html.resource.liked

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import play.api.data.Form
import lila.interest.{ InterestData, Source }
import views.html.resource.puzzle.{ actions => puzzleActions, emptyTag => puzzleEmptyTag }
import controllers.rt_resource.routes
import views.html.resource.interest.home.{ greenSquare, redSquare, yellowSquare }

object interest {

  val dataUnlikeHref = attr("data-unlikeHref")

  def apply(sc: Source, form: Form[_], pager: Paginator[InterestData], tags: Set[String])(implicit ctx: Context) =
    views.html.resource.liked.bits.layout(
      title = s"收藏-${sc.name}",
      active = s"${sc.id}Liked",
      form = form
    ) {
      st.form(
        cls := "search_form liked_form",
        action := routes.Interest.likePage(sc.id, 1),
        dataUnlikeHref := routes.Interest.unlike(sc.id),
        method := "GET"
      )(
          table(
            tr(
              td(cls := "tag-groups")(
                puzzleEmptyTag(form),
                form3.tags(form, "tags", tags)
              )
            ),
            tr(
              td(cls := "action")(
                pager.nbResults > 0 option frag(
                  puzzleActions(List("unlikeInterest"))
                )
              )
            )
          )
        )
    } {
      paginate(sc, form, pager)
    }

  private[resource] def paginate(sc: Source, form: Form[_], pager: Paginator[InterestData])(implicit ctx: Context) = {
    var url = s"${routes.Interest.likePage(sc.id, 1)}?a=1"
    form.data.foreach {
      case (key, value) =>
        url = url.concat("&").concat(key).concat("=").concat(value)
    }

    if (pager.currentPageResults.isEmpty) div(cls := "no-more")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := s"now-playing list infinitescroll ${sc.id}")(
      pager.currentPageResults.map { p =>
        val bottomColor = if (sc == Source.UnprotectedPieces || sc == Source.UnprotectedSquares) !p.color else p.color
        a(cls := "paginated", target := "_blank", dataId := p.id, dataHref := controllers.rt_interest.routes.Interest.show(sc.id, p.id))(
          div(cls := "board-wrap")(
            sc == Source.UnprotectedSquares option {
              div(cls := "board-mask", style := s"${bottomColor.fold(p.color.fold("top", "bottom"), p.color.fold("bottom", "top"))}:0")
            },
            div(
              cls := List(
                s"mini-board cg-wrap is2d star-${p.color.name}" -> true,
                "parse-fen" -> (sc.id == Source.AnnihilationPiece.id || sc.id == Source.AnnihilationStar.id),
                "parse-fen-manual" -> (sc.id == Source.CaptureKing.id || sc.id == Source.UnprotectedPieces.id || sc.id == Source.UnprotectedSquares.id)
              ),
              dataColor := bottomColor.name,
              dataFen := p.fen,
              redSquare := (if (sc == Source.CaptureKing) { p.kingSquare | "" } else ""),
              greenSquare := (if (sc == Source.CaptureKing) p.square else ""),
              yellowSquare := (if (sc == Source.UnprotectedPieces) { p.colorKingSquare() | "" } else "")
            )(cgWrapContent)
          )
        )
      },
      pagerNext(pager, np => addQueryParameter(url, "page", np))
    )
  }

}
