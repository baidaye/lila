package views.html.resource.liked

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.puzzle.Puzzle
import play.api.data.Form
import lila.resource.Sorting
import views.html.resource.puzzle.{ paginate => puzzlePaginate, emptyTag => puzzleEmptyTag, actions => puzzleActions }
import controllers.rt_resource.routes

object puzzle {

  def apply(form: Form[_], pager: Paginator[Puzzle], tags: Set[String])(implicit ctx: Context) =
    views.html.resource.liked.bits.layout(
      title = "收藏-战术题",
      active = "puzzleLiked",
      form = form
    ) {
      st.form(
        cls := "search_form liked_form",
        action := s"${routes.Resource.puzzleLiked()}#results",
        method := "GET"
      )(
          table(
            tr(
              td(cls := "tag-groups")(
                puzzleEmptyTag(form),
                form3.tags(form, "tags", tags)
              )
            ),
            tr(
              td(cls := "action")(
                pager.nbResults > 0 option frag(
                  form3.select(form("order"), Sorting.orders),
                  puzzleActions(List("unlike", "toCapsule"))
                )
              )
            )
          )
        )
    } {
      puzzlePaginate(pager, routes.Resource.puzzleLiked(), "puzzleLiked", form, true)
    }

}
