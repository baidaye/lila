package views.html.resource

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.opening.OpeningDB
import play.api.data.Form
import chess.format.Forsyth
import controllers.rt_resource.routes

object modal {

  private val dataTab = attr("data-tab")

  def saveTo(pgn: String, initialFen: String, gameForm: Form[_], openingdbForm: Form[_], puzzleForm: Form[_], openingdbs: List[OpeningDB])(implicit ctx: Context) = {
    val isInitialFen = initialFen == Forsyth.initial
    div(cls := "modal-content saveTo none")(
      h2("保存到"),
      div(
        iframe(
          id := "iframe-embed",
          src := controllers.routes.UserAnalysis.pgnEmbed(pgn.some, true),
          st.frameborder := 0,
          width := "100%"
        ),
        textarea(cls := "form-control", id := "form3-pgn", rows := 3, readonly, placeholder := "以此PGN为准")(pgn)
      ),
      div(
        div(cls := "tabs-horiz")(
          span(dataTab := "gamedb", cls := "active")("对局库"),
          isInitialFen option span(dataTab := "openingdb")("开局库"),
          !isInitialFen option span(dataTab := "puzzle")("战术题")
        ),
        div(cls := "tabs-content")(
          div(cls := "gamedb active")(
            postForm(cls := "form3", action := routes.GameDB.createGame(""))(
              form3.hidden(gameForm("tab")),
              form3.hidden(gameForm("pgn")),
              div(cls := "clm2")(
                div(cls := "left")(
                  form3.group(gameForm("gamedbId"), "目录选择")(f => {
                    frag(
                      form3.hidden(f, ctx.userId),
                      div(cls := "scroller")(
                        div(cls := "dbtree")
                      )
                    )
                  })
                ),
                div(cls := "right")(
                  form3.group(gameForm("name"), raw("名称"))(form3.input(_)(required)),
                  form3.group(gameForm("tags"), "标签")(form3.input(_)())
                )
              ),
              form3.actions(
                a(cls := "cancel")("取消"),
                form3.submit("保存", klass = "small")
              )
            )
          ),
          isInitialFen option div(cls := "openingdb")(
            postForm(cls := "form3")(
              form3.hidden(openingdbForm("tab")),
              form3.hidden(openingdbForm("pgn")),
              form3.hidden(openingdbForm("source")),
              form3.hidden(openingdbForm("rel")),
              div(cls := "clm2")(
                div(cls := "left")(
                  div(cls := "scroller")(
                    if (openingdbs.nonEmpty) {
                      val field = openingdbForm("openingdbId")
                      val defaultValue = openingdbs.filterNot(_.isFull).headOption.map(_.id)
                      div(cls := "radio-group")(
                        openingdbs.map { openingdb =>
                          val id = openingdb.id
                          val check = (field.value.fold(defaultValue)(_.some)).has(id)

                          span(cls := "radio")(
                            st.input(
                              check.option(checked),
                              st.id := s"${field.name}_${id}",
                              tpe := "radio",
                              st.name := field.name,
                              st.value := id,
                              openingdb.isFull option disabled
                            ),
                            label(cls := "radio-label", `for` := s"${field.name}_${id}")(openingdb.name, openingdb.isFull option ("（已满）"))
                          )
                        }
                      )
                    } else div("赶快去创建自己的开局库吧，", a(href := controllers.rt_resource.routes.OpeningDB.minePage(1))("点这里"))
                  )
                ),
                div(cls := "right")(
                  form3.group(openingdbForm("shortName"), raw("开局名称"))(form3.input(_)(placeholder := "选填")),
                  form3.group(openingdbForm("name"), "开局说明")(form3.input(_)(placeholder := "选填")),
                  div(cls := "group")(
                    h3("注意："),
                    ul(
                      li("1、保存会在选中开局库中添加新的着法，不覆盖已有的着法；"),
                      li("2、超过20回合的着法，自动截断。")
                    )
                  )
                )
              ),
              form3.actions(
                a(cls := "cancel")("取消"),
                form3.submit("保存", klass = s"small${openingdbs.isEmpty.?? { "disabled" }}", isDisable = openingdbs.isEmpty)
              )
            )
          ),
          !isInitialFen option div(cls := "puzzle")(
            postForm(cls := "form3", action := routes.Resource.saveToPuzzle)(
              form3.hidden(puzzleForm("tab")),
              form3.hidden(puzzleForm("pgn")),
              form3.group(puzzleForm("puzzleTag"), "标签")(form3.input(_)()),
              form3.group(puzzleForm("hasLastMove"), "格式")(form3.select(_, List(
                false -> "局面 + 答案",
                true -> "原始局面 + 对方初始着法 + 答案"
              ))),
              p(
                "规则：棋谱中第1步棋为对手着法，第2步棋为答案起始，每步棋为唯一正确答案"
              ),
              form3.actions(
                a(cls := "cancel")("取消"),
                form3.submit("保存", klass = "small")
              )
            )
          )
        )
      )
    )
  }

}
