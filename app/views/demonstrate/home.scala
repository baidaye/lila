package views.html.demonstrate

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json._

object home {

  def apply(fen: Option[String], pref: JsObject)(implicit ctx: Context) = {
    views.html.base.layout(
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("demonstrate")
      ),
      moreJs = frag(
        jsAt(s"compiled/lichess.demonstrate${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.demonstrate=${
          safeJsonValue(Json.obj(
            "data" -> Json.obj(
              "fen" -> fen,
              "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource))
            ),
            "pref" -> pref,
            "userId" -> ctx.userId
          ))
        }""")
      ),
      title = "挂盘演示",
      zoomable = true
    ) {
        main(cls := "demonstrate")()
      }
  }
}

