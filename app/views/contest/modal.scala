package views.html.contest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import org.joda.time.DateTime
import lila.user.User
import lila.team.{ Campus, Tag, Team }
import lila.contest.{ AllPlayerWithUser, Board, Contest, Forbidden, ManualPairingSource, Player, PlayerWithUser, Round }
import play.api.data.Form
import controllers.rt_contest.routes

object modal {

  val dataSingleTags = attr("data-single-tags")
  val dataRangeTags = attr("data-range-tags")

  def invite(c: Contest)(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-invite none")(
      h2("邀请棋手"),
      postForm(cls := "form3", action := routes.Contest.invite(c.id))(
        p(cls := "info", dataIcon := (""))("请邀请您认识并且希望参与这个比赛的棋手"),
        div(cls := "input-wrapper")(
          form3.hidden("contestId", c.id),
          input(cls := "user-autocomplete", placeholder := "按用户账号搜索", name := "username", required, dataTag := "span")
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("邀请", klass = "small")
        )
      )
    )
  )

  def playerChoose(
    form: Form[_],
    contest: Contest,
    clazzs: List[(String, String)],
    teamOption: Option[Team],
    tags: List[Tag],
    campuses: List[Campus],
    allPlayers: List[AllPlayerWithUser],
    players: List[PlayerWithUser]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content member-modal modal-player-choose none")(
      h2("选择棋手"),
      st.form(
        cls := "member-search small",
        dataSingleTags := s"""["${tags.filterNot(_.typ.range).map(_.field).mkString("\",\"")}"]""",
        dataRangeTags := s"""["${tags.filter(_.typ.range).map(_.field).mkString("\",\"")}"]"""
      )(
          table(
            tr(
              td(cls := "label")(label("主办方")),
              td(colspan := 3)(
                contest.typ match {
                  case Contest.Type.Public | Contest.Type.TeamInner => teamLinkById(contest.organizer, false)
                  case Contest.Type.ClazzInner => clazzLinkById(contest.organizer)
                }
              )
            ),
            tr(
              td(cls := "label")(label("账号/备注")),
              td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
              td,
              td
            ),
            (contest.typ == Contest.Type.Public || contest.typ == Contest.Type.TeamInner) option teamOption.map { team =>
              tr(
                td(cls := "label")(label("校区")),
                td(cls := "fixed")(form3.select(form("campus"), campuses.filter(c => views.html.team.bits.CanReadCampus(team, c.id)).map(c => c.id -> c.name), "".some)),
                td(cls := "label")(label("班级")),
                td(cls := "fixed")(form3.select(form("clazzId"), clazzs, "".some))
              )
            },
            tr(
              td(cls := "label")(label("等级分")),
              td(cls := "fixed")(form3.input(form("teamRatingMin"), typ = "number")(placeholder := "俱乐部等级分（最小）")),
              td(cls := "label")(label("至")),
              td(cls := "fixed")(form3.input(form("teamRatingMax"), typ = "number")(placeholder := "俱乐部等级分（最大）"))
            ),
            tr(
              td(cls := "label")(label("棋协级别")),
              td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
              td(cls := "label")(label("性别")),
              td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
            ),
            tags.nonEmpty option tr(
              td(colspan := 4)(
                a(cls := "show-search")("显示高级搜索", iconTag("R"))
              )
            ),
            tags.filterNot(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
            },
            tags.filter(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
            },
            tr(
              td(colspan := 4)(
                div(cls := "action")(
                  div, submitButton(cls := "button small")("查询")
                )
              )
            )
          )
        ),
      postForm(cls := "form3 player-choose-transfer", action := routes.Contest.playerChoose(contest.id))(
        form3.hidden("players", players.map(_.userId).mkString(",")),
        div(cls := "player-transfer")(
          views.html.base.transfer[AllPlayerWithUser, PlayerWithUser](allPlayers, players) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.userId, dataAttr := left.json.toString())),
              td(cls := "name")(left.markOrUsername)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.userId, dataAttr := right.json.toString())),
              td(cls := "name")(right.markOrUsername)
            )
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def playerForbidden(contest: Contest, players: List[PlayerWithUser], forbidden: Option[Forbidden])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-forbidden none")(
      h2("回避设置"),
      postForm(cls := "form3", action := routes.Contest.forbiddenApply(contest.id, forbidden.map(_.id)))(
        form3.hidden("playerIds", forbidden.??(_.playerIds.mkString(","))),
        div(cls := "fname")(input(name := "name", value := forbidden.map(_.name), required, minlength := 2, maxlength := 20, placeholder := "组名")),
        views.html.base.transfer[PlayerWithUser, PlayerWithUser](
          leftOptions = players.filterNot(p => forbidden.??(_.playerIds.contains(p.playerId))),
          rightOptions = players.filter(p => forbidden.??(_.playerIds.contains(p.playerId))),
          leftLabel = "参赛棋手",
          rightLabel = "回避棋手",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.playerId, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.markOrUsername)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.playerId, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.markOrUsername)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def manualAbsent(c: Contest, r: Round, players: List[PlayerWithUser])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-absent none")(
      h2("弃权设置"),
      postForm(cls := "form3", action := routes.Contest.manualAbsent(c.id, r.no))(
        form3.hidden("contestId", c.id),
        views.html.base.transfer[PlayerWithUser, PlayerWithUser](
          leftOptions = players.filterNot(_.player.absent),
          rightOptions = players.filter(p => p.player.manualAbsent && !p.player.absentOr),
          leftLabel = "参赛棋手",
          rightLabel = "弃权棋手",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.id, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.markOrUsername)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.id, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.markOrUsername)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def roundSwap(c: Contest, r: Round, rounds: List[Round], boards: List[Board], players: List[PlayerWithUser])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-swap none")(
      h2("交换轮次"),
      postForm(cls := "form3", action := routes.Contest.roundSwap(c.id, r.no))(
        div(cls := "contest-swap-source")(s"第${r.no}轮 ${r.actualStartsAt.toString("yyyy-HH-dd HH:mm")}"),
        div(cls := "contest-swap-rounds")(
          div(cls := "contest-swap-rounds-left")(
            rounds.take(c.actualRound).filterNot(_.id == r.id).map { round =>
              val rno = round.no
              div(cls := "contest-swap-round")(
                div(cls := List("radio" -> true, "active" -> (round.no == r.no + 1), "disabled" -> (round.no <= r.no || !r.isPairing)))(
                  st.input(tpe := "radio", st.name := "contest-swap-rd", st.id := s"contest-swap-rd_${round.id}", st.value := round.id, (round.no <= r.no || !r.isPairing) option disabled, (round.no == r.no + 1) option st.checked), nbsp,
                  label(`for` := s"contest-swap-rd_${round.id}")(s"第${rno}轮")
                ),
                players.find(_.player.isBye(rno)) match {
                  case Some(pwu) => div(cls := "byeUser")(userSpan(pwu.user, text = pwu.markOrUsername.some, withBadge = false), s"(${pwu.player.no}) 轮空")
                  case None => frag()
                }
              )
            }
          ),
          div(cls := "contest-swap-rounds-right")(
            rounds.take(c.actualRound).filterNot(_.id == r.id).map { round =>
              val rno = round.no
              div(dataId := s"${round.id}", cls := List("contest-swap-round" -> true, "disabled" -> (round.no <= r.no || !r.isPairing), "none" -> (round.no != r.no + 1)))(
                table(cls := "slist contest-swap-boards")(
                  tbody(
                    round.isOverPairing option boards.filter(_.roundNo == rno).map { board =>
                      val white = findPlayer(board.whitePlayer.no, players)
                      val black = findPlayer(board.blackPlayer.no, players)
                      tr(
                        td(cls := "no")(s"#${board.no}"),
                        td(cls := "player")(userLink(white.user, text = white.markOrUsername.some, withBadge = false), s"(${white.no})"),
                        td("Vs."),
                        td(cls := "player")(userLink(black.user, text = black.markOrUsername.some, withBadge = false), s"(${black.no})")
                      )
                    },
                    players.filter(_.player.noBoard(rno)).sortWith((p1, p2) => p1.player.roundOutcomeSort(rno) > p2.player.roundOutcomeSort(rno)).map {
                      playerWithUser =>
                        {
                          val player = playerWithUser.player
                          tr(
                            td(cls := "no")("-"),
                            td(cls := "player")(userLink(playerWithUser.user, text = playerWithUser.markOrUsername.some, withBadge = false), s"(${player.no})"),
                            td(player.roundOutcomeFormat(rno)),
                            td("-")
                          )
                        }
                    }
                  )
                )
              )
            }
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )
  )

  def manualPairing(c: Contest, r: Round, boards: List[Board], players: List[PlayerWithUser], source: ManualPairingSource)(implicit ctx: Context) = frag(
    div(cls := "modal-content manual-pairing none")(
      h2("与...交换"),
      postForm(cls := "form3", action := routes.Contest.manualPairing(c.id, r.no))(
        p(cls := "is-gold", dataIcon := "")("手动调整对阵可能会影响后续匹配，请谨慎操作！"),
        form3.hidden("contestId", c.id),
        !source.isBye option form3.hidden("source", s"""{"isBye":0, "board": "${source.board_.id}", "color": ${source.color_.fold(1, 0)}}"""),
        source.isBye option form3.hidden("source", s"""{"isBye":1, "player": "${source.player_.id}"}"""),
        div(cls := "manual-source")(
          table(cls := "slist")(
            tbody(
              !source.isBye option tr(
                td(source.board_.no),
                td("#", nbsp, source.board_.player(source.color_).no),
                td(source.board_.player(source.color_).userId),
                td(source.color_.fold("白方", "黑方"))
              ),
              source.isBye option tr(
                td("-"),
                td("-"),
                td(source.player_.userId),
                td("-")
              )
            )
          )
        ),
        div(cls := "manual-filter")(
          input(tpe := "text", cls := "manual-filter-search", placeholder := "搜索")
        ),
        div(cls := "manual-list")(
          table(cls := "slist")(
            thead(
              tr(
                th("台号"),
                th("白方"),
                th("黑方")
              )
            ),
            tbody(
              boards.map { board =>
                {
                  val white = findPlayer(board.whitePlayer.no, players)
                  val black = findPlayer(board.blackPlayer.no, players)
                  tr(
                    td(board.no),
                    td(cls := List("white" -> true, "disabled" -> (source.board.??(_.is(board)) && source.color ?? (_.name == "white"))))(
                      label(`for` := white.id, cls := "user-label")("#", white.no, nbsp, white.markOrUsername),
                      nbsp,
                      input(tpe := "radio", id := white.id, name := "user-radio", value := s"""{"isBye": 0, "board": "${board.id}", "color": 1}""", (source.board.??(_.is(board)) && source.color ?? (_.name == "white")) option disabled)
                    ),
                    td(cls := List("black" -> true, "disabled" -> (source.board.??(_.is(board)) && source.color ?? (_.name == "black"))))(
                      label(`for` := black.id)("#", black.no, nbsp, black.markOrUsername),
                      nbsp,
                      input(tpe := "radio", id := black.id, name := "user-radio", value := s"""{"isBye": 0, "board": "${board.id}", "color": 0}""", (source.board.??(_.is(board)) && source.color ?? (_.name == "black")) option disabled)
                    )
                  )
                }
              },
              players.filter(_.player.isBye(r.no)).map { player =>
                tr(title := "轮空")(
                  td("-"),
                  td(cls := List("white" -> true, "disabled" -> source.isBye))(
                    label(`for` := player.id, cls := "user-label")("#", player.no, nbsp, player.markOrUsername),
                    nbsp,
                    input(tpe := "radio", id := player.id, name := "user-radio", value := s"""{"isBye": 1, "player": "${player.id}"}""", source.isBye option disabled)
                  ),
                  td(cls := List("black" -> true))("-")
                )
              }
            )
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def manualResult(c: Contest, r: Round, b: Board)(implicit ctx: Context) = frag(
    div(cls := "modal-content manual-result none")(
      h2("设置成绩"),
      postForm(cls := "form3", action := routes.Contest.manualResult(c.id, b.id))(
        select(name := "result")(
          Board.Result.all.map { r =>
            option(value := r.id)(s"${r.id}（${r.name}）")
          }
        ),
        p("手工设置过比赛成绩，必须先发布成绩，在下一轮才能进行编排"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def scoreDetail(no: Round.No, player: Player, players: List[PlayerWithUser], boards: List[Board])(implicit ctx: Context) = {
    val pwu = findPlayer(player.no, players)
    div(cls := "modal-content score-detail none")(
      h2(s"（${player.no}）${pwu.markOrUsername} 详情"),
      table(cls := "slist")(
        thead(
          tr(
            th("轮次"),
            th("序号"),
            th("白方"),
            th("结果"),
            th("黑方"),
            th("序号"),
            th("操作")
          )
        ),
        tbody(
          (1 to no).toList.map { n =>
            boards.find(_.roundNo == n).map { board =>
              val color = ctx.me.fold("white")(u => board.colorOfByUserId(u.id).name)
              val white = findPlayer(board.whitePlayer.no, players)
              val black = findPlayer(board.blackPlayer.no, players)
              tr(
                td(n),
                td(white.no),
                td(white.markOrUsername),
                td(strong(board.resultFormat)),
                td(black.markOrUsername),
                td(black.no),
                td(
                  a(target := "_blank", href := controllers.routes.Round.watcher(board.id, color))("查看")
                )
              )
            } getOrElse {
              val p = findPlayer(player.no, players)
              tr(
                td(n),
                td("-"),
                td(p.markOrUsername),
                td(strong(player.roundOutcomeFormat(n))),
                td("-"),
                td("-"),
                td("-")
              )
            }
          }
        )
      )
    )
  }

  def setBoardTime(contest: Contest, round: Round, board: Board)(implicit ctx: Context) = frag(
    div(cls := "modal-content board-time none")(
      h2("时间设置"),
      postForm(cls := "form3", action := routes.Contest.setBoardTime(board.id))(
        div(cls := "form-group")(
          st.input(
            name := "startsAt",
            value := board.startsAt.toString("yyyy-MM-dd HH:mm"),
            cls := "form-control flatpickr",
            dataEnableTime := true,
            datatime24h := true
          )(
              dataMinDate := DateTime.now.plusMinutes(3).toString("yyyy-MM-dd HH:mm"),
              dataMaxDate := maxTimeLimit(contest, round)
            )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  val dataSigned = attr("data-signed")
  def setBoardSign(
    contest: Contest,
    round: Round,
    board: Board,
    whiteUser: User,
    blackUser: User,
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content board-sign none")(
      h2("准备比赛（签到）设置"),
      postForm(cls := "form3")(
        div(cls := "form-group")(
          label(cls := "form-label")(s"第 ${round.no} 轮"),
          table(cls := "slist")(
            tbody(
              tr(
                td(s"#${board.whitePlayer.no}"),
                td(markOrUsername(markMap, whiteUser)),
                td(
                  a(
                    cls := List("button button-empty small btn-sign" -> true, "button-green disabled" -> board.whitePlayer.signed),
                    dataHref := s"${routes.Contest.setBoardSign(board.id, board.whitePlayer.userId.some)}",
                    dataSigned := (if (board.whitePlayer.signed) "1" else "0")
                  )(if (board.whitePlayer.signed) "已准备（签到）" else "代准备（签到）")
                )
              ),
              tr(
                td(s"#${board.blackPlayer.no}"),
                td(markOrUsername(markMap, blackUser)),
                td(
                  a(
                    cls := List("button button-empty small btn-sign" -> true, "button-green disabled" -> board.blackPlayer.signed),
                    dataHref := s"${routes.Contest.setBoardSign(board.id, board.blackPlayer.userId.some)}",
                    dataSigned := (if (board.blackPlayer.signed) "1" else "0")
                  )(if (board.blackPlayer.signed) "已准备（签到）" else "代准备（签到）")
                )
              )
            )
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确认", klass = "small")
        )
      )
    )
  )

  private def markOrUsername(markMap: Map[String, Option[String]], user: User): String = {
    val mark = markMap.get(user.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | user.username
  }

  private def maxTimeLimit(contest: Contest, round: Round) = {
    if (!contest.appt) {
      contest.finishAt.plusYears(1).toString("yyyy-MM-dd HH:mm")
    } else {
      round.actualStartsAt.plusMinutes(contest.roundSpace).minusMinutes(contest.apptDeadline | 0).toString("yyyy-MM-dd HH:mm")
    }
  }

  private def findPlayer(no: Player.No, players: List[PlayerWithUser]): PlayerWithUser =
    players.find(_.player.no == no) err s"can not find player：$no"
}
