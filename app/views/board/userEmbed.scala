package views.html.board

import lila.app.templating.Environment._
import lila.app.ui.EmbedConfig
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsObject, Json }
import views.html.base.layout.{ bits => layout }

object userEmbed {

  import EmbedConfig.implicits._

  def apply(data: JsObject, mini: Boolean = false)(implicit config: EmbedConfig) = frag(
    layout.doctype,
    layout.htmlTag(config.lang)(
      head(
        layout.charset,
        layout.viewport,
        layout.metaCsp(basicCsp withNonce config.nonce),
        st.headTitle("haichess.com embed board"),
        layout.pieceSprite(lila.pref.PieceSet.default),
        cssTagWithTheme("analyse.embed", config.bg),
        mini option raw("""<style>@media only screen and (max-width: 499px) {.analyse { grid-template-columns: minmax(150px, calc(100vh - 2.5rem)) minmax(150px, 1fr); }}</style>""")
      ),
      body(
        cls := s"highlight ${config.bg} ${config.board}",
        dataDev := (!isProd).option("true"),
        dataAssetUrl := assetBaseUrl,
        dataAssetVersion := assetVersion.value,
        dataTheme := config.bg,
        attr("data-socket-domain") := socketDomain
      )(
          div(cls := "is2d")(
            main(cls := "analyse")
          ),
          jQueryTag,
          if (isProd)
            jsAt(s"compiled/lichess.site.min.js")
          else frag(
            jsAt(s"compiled/lichess.deps.js"),
            jsAt(s"compiled/lichess.site.js")
          ),
          jsTag("vendor/mousetrap.js"),
          jsAt("compiled/util.js"),
          jsAt("compiled/trans.js"),
          jsAt("compiled/embed-analyse.js"),
          analyseTag,
          embedJsUnsafe(s"""lichess.startEmbeddedAnalyse(${
            safeJsonValue(Json.obj(
              "data" -> data,
              "embed" -> true,
              "embedMoveable" -> true,
              "i18n" -> views.html.board.userAnalysisI18n(withCeval = false, withExplorer = false)
            ))
          })""", config.nonce)
        )
    )
  )

}
