package views.html.board

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import controllers.routes

object editor {

  def apply(
    sit: chess.Situation,
    fen: String,
    positionsJson: String,
    animationDuration: scala.concurrent.duration.Duration
  )(implicit ctx: Context) = views.html.base.layout(
    title = trans.boardEditor.txt(),
    moreJs = frag(
      tagsinputTag,
      jsAt(s"compiled/lichess.editor${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""var data=${safeJsonValue(bits.jsData(sit, fen, animationDuration))};data.positions=$positionsJson;
LichessEditor(document.getElementById('board-editor'), data);""")
    ),
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("editor")
    ),
    chessground = false,
    zoomable = true,
    openGraph = lila.app.ui.OpenGraph(
      title = "Chess board editor",
      url = s"$netBaseUrl${routes.Editor.index.url}",
      description = "Load opening positions or create your own chess position on a chess board editor"
    ).some
  )(main(id := "board-editor")(
      div(cls := "board-editor")(
        div(cls := "spare"),
        div(cls := "main-board")(chessgroundBoard),
        div(cls := "spare")
      )
    ))

  def situationCreate(form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content situation-create none")(
      h2("保存到局面库"),
      postForm(cls := "form3")(
        div(cls := "clm2")(
          div(cls := "left")(
            form3.group(form("situationdbId"), "目录选择")(f => {
              frag(
                form3.hidden(f, ctx.userId),
                div(cls := "scroller")(
                  div(cls := "dbtree")
                )
              )
            }),
            form3.hidden(form("fen"))
          ),
          div(cls := "right")(
            form3.group(form("standard"), raw("标准局面"), half = true)(
              form3.radio2(_, List(true -> "是", false -> "否"), "true".some)
            ),
            form3.group(form("name"), raw("名称"))(form3.input(_)),
            form3.group(form("tags"), "标签")(form3.input(_)()),
            form3.group(form("instruction"), "局面说明")(form3.textarea(_)(rows := 2))
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

}
