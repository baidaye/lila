package views.html.tournament

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.tournament.Tournament
import lila.tournament.Status
import controllers.routes

object mine {

  def apply(pager: lila.common.paginator.Paginator[Tournament])(implicit ctx: Context) =
    views.html.base.layout(
      title = "我创建的锦标赛",
      moreCss = cssTag("tournament.home"),
      moreJs = infiniteScrollTag
    ) {
        main(cls := "box page-small")(
          h1("我创建的锦标赛"),
          table(cls := "slist")(
            thead(
              tr(
                th(colspan := 2, cls := "large"),
                th("状态"),
                th("开始时间"),
                th("持续时间"),
                th("棋手")
              )
            ),
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pager.nextPage.map { np =>
                  tr(th(cls := "pager none")(
                    a(rel := "next", href := routes.Tournament.mine(np))("Next")
                  ))
                },
                pager.currentPageResults.map { t =>
                  tr(cls := List("paginated" -> true, "tour-scheduled" -> t.isScheduled))(
                    td(cls := "icon")(iconTag(tournamentIconChar(t))),
                    td(cls := "header")(
                      a(href := routes.Tournament.show(t.id))(
                        span(cls := "name")(t.fullName),
                        span(cls := "setup")(
                          t.clock.show,
                          " • ",
                          if (t.variant.exotic) t.variant.name else t.perfType.map(_.name),
                          !t.position.initial option frag(" • ", "从常见开局开始的锦标赛"),
                          " • ",
                          t.mode.fold("不计算等级分", "计算等级分")
                        )
                      )
                    ),

                    td(cls := "status")(t.status match {
                      case Status.Created => "新建"
                      case Status.Started => "比赛中"
                      case Status.Finished => "已结束"
                    }),
                    td(t.startsAt.toString("yyyy-MM-dd HH:mm")),
                    td(cls := "duration")(t.durationString),
                    td(cls := "text", dataIcon := "r")(t.nbPlayers)
                  )
                }
              )
            } else {
              tbody(
                tr(
                  td(colspan := 6)("暂无记录")
                )
              )
            }
          )
        )
      }

}
