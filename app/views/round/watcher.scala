package views.html
package round

import play.api.libs.json.Json
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.game.Pov
import lila.contest.Board
import controllers.routes

object watcher {

  private val dataGameIds = attr("data-gameids")
  def apply(
    pov: Pov,
    data: play.api.libs.json.JsObject,
    tour: Option[lila.tournament.TourMiniView],
    contest: Option[lila.hub.actorApi.contest.ContestBoard],
    contestPovs: List[lila.game.actorApi.BoardWithPov],
    markMap: Map[String, Option[String]],
    simul: Option[lila.simul.Simul],
    cross: Option[lila.game.Crosstable.WithMatchup],
    userTv: Option[lila.user.User] = None,
    chatOption: Option[lila.chat.UserChat.Mine],
    bookmarked: Boolean
  )(implicit ctx: Context) = {

    val chatJson = chatOption map { c =>
      chat.json(
        c.chat,
        name = trans.spectatorRoom.txt(),
        timeout = c.timeout,
        withNote = ctx.isAuth,
        public = true,
        palantir = ctx.me.exists(_.canPalantir)
      )
    }

    bits.layout(
      variant = pov.game.variant,
      title = gameVsText(pov.game, withRatings = true),
      moreJs = frag(
        roundNvuiTag,
        roundTag,
        embedJsUnsafe(s"""lichess=window.lichess||{};customWS=true;onload=function(){
LichessRound.boot(${
          safeJsonValue(Json.obj(
            "data" -> data,
            "i18n" -> jsI18n(pov.game),
            "chat" -> chatJson
          ))
        })}""")
      ),
      openGraph = povOpenGraph(pov).some,
      chessground = false
    )(
        main(cls := "round")(
          st.aside(cls := "round__side")(
            bits.side(pov, data, tour, contest, simul, userTv, bookmarked),
            chatOption.map(_ => chat.frag)
          ),
          bits.roundAppPreload(pov, false),
          div(cls := "round__underboard")(bits.crosstable(cross, pov.game)),
          div(cls := "round__underchat")(bits underchat pov.game),
          contestPovs.nonEmpty option div(cls := "round__contestTv", dataGameIds := s"""["${contestPovs.map { _.pov.gameId }.mkString("\",\"")}"]""")(
            h2("本轮全部对局：", span(cls := "finished")(contestPovs.count(_.pov.game.finished)), "/", contestPovs.length),
            div(cls := "now-playing")(
              contestPovs.map(x => views.html.game.mini(x.pov, x.board.some, active = pov.gameId == x.pov.gameId, markMap = markMap))
            )
          )
        )
      )
  }

  def crawler(pov: Pov, initialFen: Option[chess.format.FEN], pgn: chess.format.pgn.Pgn)(implicit ctx: Context) =
    bits.layout(
      variant = pov.game.variant,
      title = gameVsText(pov.game, withRatings = true),
      openGraph = povOpenGraph(pov).some,
      chessground = false
    )(frag(
        main(cls := "round")(
          st.aside(cls := "round__side")(
            game.side(pov, initialFen, none, contest = none, simul = none, userTv = none, bookmarked = false),
            div(cls := "for-crawler")(
              h1(titleGame(pov.game)),
              p(describePov(pov)),
              div(cls := "pgn")(pgn.render)
            )
          ),
          div(cls := "round__board main-board")(chessground(pov))
        )
      ))
}
