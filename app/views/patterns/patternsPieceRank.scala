package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.{ DataForm, PatternsPieceRank }
import lila.patterns.PatternsOp.PatternsType
import play.api.data.Form
import bits.dataCoord
import bits.dataFenType
import controllers.routes

object patternsPieceRank {

  def apply(patternsType: PatternsType, form: Form[_], pieceRankNeighbor: PatternsPieceRank.WithNeighbor)(implicit ctx: Context) =
    views.html.base.layout(
      title = "将杀子 - 将杀模式",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("patterns")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("patterns.pieceRank.js")
      ),
      zoomable = true
    ) {
        main(cls := "patterns patterns-pieceRank", dataNotAccept := s"${!ctx.me.??(_.hasPatterns)}")(
          form3.hidden("patternsType", patternsType.id),
          form3.hidden(form("kingSquare")),
          div(cls := "patterns__board")(
            pieceRankNeighbor.curr.fold(div(cls := "main-board")(bits.emptyBoard)) { pieceRank =>
              div(cls := "main-board")(
                div(
                  cls := List("cg-wrap parse-fen-manual" -> true, "is2d" -> ctx.pref.is2d, "is3d" -> ctx.pref.is3d),
                  dataColor := "white",
                  dataFen := pieceRank.simplifiedCheckmateFen,
                  dataCoord := ctx.pref.coords > 0
                )(cgWrapContent)
              )
            }
          ),
          div(cls := "patterns__search")(
            table(cls := "search__table")(
              tr(
                th(label(s"${patternsType.label1}方")),
                td(
                  form3.tagsRadio(form("color"), DataForm.colorChoices)
                )
              ),
              patternsType != PatternsType.Stalemate option tr(
                th(label(
                  patternsType match {
                    case PatternsType.Single => "将杀子"
                    case PatternsType.Double => "将杀子1"
                    case PatternsType.Stalemate => frag()
                  }
                )),
                td(
                  form3.tagsRadio(form("checkerRole"), DataForm.checkerChoices)
                )
              ),
              patternsType match {
                case PatternsType.Single | PatternsType.Stalemate => frag()
                case PatternsType.Double => {
                  tr(
                    th(label("将杀子2")),
                    td(
                      form3.tagsRadio(form("fixCheckerRole"), DataForm.fixCheckerChoices)
                    )
                  )
                }
              },
              patternsType match {
                case PatternsType.Single | PatternsType.Stalemate => {
                  tr(
                    th(label("控制子")),
                    td(
                      form3.tagsWithKv(form, "controllerRole", DataForm.controllerChoices, klass = "single")
                    )
                  )
                }
                case PatternsType.Double => frag()
              },
              patternsType match {
                case PatternsType.Single | PatternsType.Stalemate => {
                  tr(
                    th(label("带牵制")),
                    td(
                      form3.tagsRadio(form("pinner"), DataForm.pinnerChoices)
                    )
                  )
                }
                case PatternsType.Double => frag()
              }
            )
          ),
          div(cls := "patterns__control")(
            div(cls := "control1")(
              pieceRankNeighbor.curr.fold(
                a(cls := "patternsOp")("- -")
              ) { pieceRank =>
                  a(cls := "patternsOp", href := s"/patterns/${patternsType.id}/rank/feature?patternsOp=${pieceRank.patternsOp}&color=${pieceRank.win.name}")(pieceRank.patternsOp)
                },
              button(cls := "button button-empty flip", dataIcon := "B", title := "翻转棋盘")("翻转棋盘")
            ),
            pieceRankNeighbor.curr.fold(
              div(cls := "fen-line")(
                button(cls := "button button-empty small disabled active", dataFenType := "simplifiedCheckmateFen", disabled)(s"简化${patternsType.label1}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalCheckmateFen", disabled)(s"原始${patternsType.label1}"),
                patternsType != PatternsType.Stalemate option button(cls := "button button-empty small disabled", dataFenType := "simplifiedFen", disabled)(s"简化${patternsType.label2}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalFen", disabled)(s"原始${patternsType.label2}")
              )
            ) { pieceRank =>
                div(cls := "fen-line")(
                  button(cls := "button button-empty small active", dataFenType := "simplifiedCheckmateFen", dataFen := pieceRank.simplifiedCheckmateFen)(s"简化${patternsType.label1}"),
                  button(cls := "button button-empty small", dataFenType := "originalCheckmateFen", dataFen := pieceRank.originalCheckmateFen)(s"原始${patternsType.label1}"),
                  patternsType != PatternsType.Stalemate option button(cls := "button button-empty small", dataFenType := "simplifiedFen", dataFen := pieceRank.simplifiedFen)(s"简化${patternsType.label2}"),
                  button(cls := "button button-empty small", dataFenType := "originalFen", dataFen := pieceRank.originalFen)(s"原始${patternsType.label2}")
                )
              },
            div(cls := "control")(
              button(cls := List("button prev" -> true, "disabled" -> !pieceRankNeighbor.hasPrev), !pieceRankNeighbor.hasPrev option disabled, dataId := pieceRankNeighbor.prevOrderOrEmpty)("上一个"),
              form3.input2(form("order"), vl = form("order").value.fold(pieceRankNeighbor.currOrderOrEmpty) { v => if (v.isEmpty) pieceRankNeighbor.currOrderOrEmpty else v }.some, typ = "number")(dataId := pieceRankNeighbor.currOrderOrEmpty),
              button(cls := List("button next" -> true, "disabled" -> !pieceRankNeighbor.hasNext), !pieceRankNeighbor.hasNext option disabled, dataId := pieceRankNeighbor.nextOrderOrEmpty)("下一个")
            )
          ),
          pieceRankNeighbor.curr.fold(
            div(cls := "patterns__btm")(
              input(value := "8/8/8/8/8/8/8/8 w - -", readonly, cls := "form-control fen"),
              button(cls := "button small disabled btnSituation", disabled)("保存")
            )
          ) { pieceRank =>
              div(cls := "patterns__btm")(
                input(value := pieceRank.simplifiedCheckmateFen, readonly, cls := "form-control fen"),
                button(cls := "button small btnSituation")("保存")
              )
            }
        )
      }

}
