package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import controllers.rt_patterns.routes

object about {

  def intro()(implicit ctx: Context) =
    layout(title = "介绍 - 将杀模式", active = "intro") {
      div(cls := "page-menu__content box box-pad")(
        div(cls := "page patterns-about")(
          h1(cls := "page-title")("将杀模式及CPN"),
          div(cls := "body")(raw(
            s"""
          <div>
             <p><strong>系统中全部使用OP模式，更加精确。</strong></p>
             <h2>一、模式编码</h2>
             <div>
                <p>将杀模式是对将杀局面中，被将杀的王及状态的抽象描述，采用 8 位编码，从败方王上方格子开始，每一位按顺时针方向对应王周围的 1 个格子，编码位数与王周围格子的对应关系如下图：</p>
                <img class="scale", src=${staticUrl("images/patterns/about_01.png")}>
                <p>编码的每一位，表示为一个英文字母，含义如下：</p>
                <ul.ul-nolist>
                  <li><strong>Z</strong> 位置在棋盘边缘以外，实际不存在的格子</li>
                  <li><strong>X</strong> 被胜方将杀子（最终完成将杀的棋子）占有，直接攻击攻击，或王走到后依然被将杀子攻击</li>
                  <li><strong>A</strong> 被败方棋子占据，或被胜方控制（被将杀子以外的棋子攻击），任取 O 或 P</li>
                  <li><strong>O</strong> 被胜方控制（被将杀子以外的棋子攻击）</li>
                  <li><strong>P</strong> 被败方棋子占据</li>
                </ul>

                <p>将杀模式可以分为 2 个层次：A 模式 和 OP 模式。</p>
                <ul.ul-nolist>
                  <li><strong>A 模式</strong> — 每一位使用 ZXA 三个字母之一表示，如：ZZXAAAXZ。编码优先级顺序：Z > X > A</li>
                  <li><strong>OP模式</strong> — 每一位使用 ZXOP 四个字母之一，其中“O/P”是对 A 模式中对应字母“A”的替换表示，如：ZZXPOPXZ，即对上面例子的一种替换。编码优先级顺序：Z > X > O > P</li>
                </ul>
                <p>因此，对同一个局面，可以使用 A 模式或 OP 模式进行编码。A 模式覆盖 OP 模式，属于更上一层。</p>

                <p>下面是一个具体的例子（简化过的将杀局面）：</p>
                <img class="scale", src=${staticUrl("images/patterns/about_02.png")}>
                <p>模式编码的解释：</p>
                <ul.ul-nolist>
                  <li>白棋为胜方，白后为将杀子；黑棋为败方，黑王在 e8 格。</li>
                  <li>第 1、2 位，在棋盘外的格子，表示为 Z</li>
                  <li>第 3 位 f8 格，正在被白棋将杀子 后攻击，表示为 X</li>
                  <li>第 4 位 f7 格，被黑棋兵占据，OP 模式表示为 P，A 模式 为 A</li>
                  <li>第 5 位 e7 格，棋盘上被黑棋象占据，同时被白车攻击，形成牵制，依据优先级规则，OP 模式表示为 O，A 模式表示为 A</li>
                  <li>第 6 位 d7 格，被黑棋后占据，OP 模式表示为 P，A 模式 为 A</li>
                  <li>第 7 位 d8 格，棋盘上为空格，此时如果是黑方走棋，王走到 d8，依然被白后攻击，因此标识为 X。如果 d8 格被白棋控制，或有黑棋棋子，按优先级规则，依然表示为 X。</li>
                  <li>第 8 位，在棋盘外的格子，表示为 Z</li>
                </ul>
             </div>
          </div>
          <br/>
          <div>
            <h2>二、模式说明</h2>
            <div>
              <h3>1、黑白兼容</h3>
              <p>本书中定义的模式编码，以败方王为中心，可以描述白王或黑王被将杀的情况。实际使用中，对于相同的模式编码，白棋和黑棋被将杀，不同子力配合出现的概率可能会有很大的差异。</p>
              <h3>2、单子与双子（双将）兼容</h3>
              <p>可能出现一个子或两个子同时将杀的情况，模式编码可以同时支持。</p>
              <h3>3、棋子、距离兼容</h3>
              <p>模式编码中，不特指实现将杀或控制王使用的棋子，也不描述棋子之间的距离，因此可以兼容各种情况。</p>
              <p>需要注意，子力在距离王不同的距离上，对应的模式会有差异，以后为例：</p>
              <img class="scale", src=${staticUrl("images/patterns/about_03.png")}>
              <h3>4、王的位置</h3>
              <p>可以根据模式编码中 Z 的个数判断王的位置：</p>
              <ul.ul-nolist>
                <li>5 个 Z，为角落，如：ZZZZXAXZ，表示王在 h8 格；</li>
                <li>3 个 Z，为棋盘边缘（不含角落），如：ZZXXXXXZ，表示王在 b8~g8 底线；</li>
                <li>不含 Z，王在 b2 至 g7 的正方形内，如：XXXAXAXX。</li>
              </ul>
              <h3>5、模式与真实局面</h3>
              <p>因为有了字母编码的优先级顺序，因此，可以保证，对任意的一个真实完成将杀的局面，都可以唯一的编码为对应的 A 模式或 OP 模式。</p>
              <p>而任意一个可能的模式编码，不一定可以找到真实的局面，如：A 模式编码 ZZZZXAXZ。</p>
              <img class="scale", src=${staticUrl("images/patterns/about_04.png")}>
              <p>经过推演和计算计算机程序验证，可以对应到真实对局的 A 模式共 166 种：单子完成将杀的 120种，双子将杀的 122 种，其中有 76 种编码是单子、双子共有的。</p>
              <img class="scale", src=${staticUrl("images/patterns/about_05.png")}>
              <p>本书中列举了实现模式对应的子力，有些模式或实现模式的将杀子，是棋色特有的，不做单独说明。</p>
              <h3>6、模式对称性</h3>
              <p>王周围的 8 个格子天然具有一定的对称性，因此模式编码也会具有这种对称性，在使用模式或阅读本书时，可以注意这种对称性，加深理解。</p>
            </div>
          </div>
          """
          ))
        )
      )
    }

  def eBook()(implicit ctx: Context) =
    layout(title = "电子书 - 将杀模式", active = "eBook") {
      div(cls := "page-menu__content page patterns-about")(
        div(cls := "box box-pad")(
          h1(cls := "page-title")("《国际象棋将杀模式》"),
          div(cls := "body")(
            p("将杀是对局中最激动人心的时刻，对将杀局面的分析，可以让初、中级棋手领略国际象棋的精妙，并做到攻防兼备，既不错过进攻的时机，也不给对手偷杀的机会，同时加强对棋子作用、棋子之间关系的理解。"),
            p("规则与真实的将杀局面是源与流的关系，模式可以看作是支流。通过推演及计算机软件计算，理论上166个模式就覆盖了所有的国际象棋将杀局面。本书中不仅列举了这些模式，还给出了可能实现该模式的将杀子。"),
            img(cls := "screenshot")(src := staticUrl("images/patterns/eBook_intro.png")),
            div(cls := "qrcode")(
              img(src := staticUrl("images/patterns/eBook_qrcode_intro.png")),
              div(cls := "right")(
                div(cls := "code")("百度网盘提取码：1234"),
                a(href := "https://pan.baidu.com/s/1AWRGxZr58pzQqFUgTIW1Ag?pwd=1234")("扫码或点击链接，免费下载")
              )
            )
          )
        ),
        div(cls := "box box-pad")(
          h1(cls := "page-title")("《基于模式的一步杀》"),
          div(cls := "body")(
            p("本书共1392道题，全部为来自真实对局的一步杀，与《国际象棋 将杀模式》完全对应，两本书可以当作习题和答案对照阅读。有些极少出现的模式没有对应的题目。"),
            p("选题最主要的依据是简化局面出现的频率，从多样性的角度有人为调整，如：牵制、闪击、兵升变、反杀等等，在一步杀局面下各种场景都有覆盖。"),
            p("为阅读方便，保留了模式信息，下面是题目和对应的答案；对于多种可行走法的情况，在答案后加“*”；题目中绿色格子标识对手上一步棋。"),
            img(cls := "screenshot")(src := staticUrl("images/patterns/eBook_checkmate1.png")),
            div(cls := "qrcode")(
              img(src := staticUrl("images/patterns/eBook_qrcode_checkmate1.png")),
              div(cls := "right")(
                div(cls := "code")("百度网盘提取码：1234"),
                a(href := "https://pan.baidu.com/s/130G0_HIdZPjo6wD9wp6lqg?pwd=1234")("扫码或点击链接，免费下载")
              )
            )
          )
        )
      )
    }

  def layout(title: String, active: String)(content: Frag)(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = cssTag("patterns.about")
    ) {
        main(cls := "page-menu")(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(active)
          ),
          content
        )
      }

  def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("intro"), href := routes.Patterns.intro())("将杀模式介绍"),
      a(activeCls("eBook"), href := routes.Patterns.eBook())("电子书下载")
    )
  }

}
