package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.PatternsKingposRank
import lila.patterns.DataForm
import lila.patterns.PatternsOp.PatternsType
import play.api.data.Form
import controllers.routes
import views.html.patterns.bits.dataCoord

object kingposRank {

  def apply(patternsType: PatternsType, form: Form[_], kingposRanks: List[PatternsKingposRank])(implicit ctx: Context) =
    views.html.base.layout(
      title = "王的位置 - 将杀模式",
      moreCss = cssTag("patterns"),
      moreJs = frag(
        jsTag("patterns.kingposRank.js")
      ),
      zoomable = true
    ) {
        main(cls := "patterns patterns-kingposRank")(
          form3.hidden("patternsType", patternsType.id),
          div(cls := "patterns__board")(
            div(cls := "main-board")(
              div(
                cls := "cg-wrap parse-fen-manual is2d",
                dataColor := "white",
                dataFen := "8/8/8/8/8/8/8/8 w - -",
                dataCoord := ctx.pref.coords > 0
              )(cgWrapContent),
              div(cls := "board-mask")(
                table(cls := "squares")(
                  List(8, 7, 6, 5, 4, 3, 2, 1).map { rank =>
                    tr(
                      List("a", "b", "c", "d", "e", "f", "g", "h").map { file =>
                        val sq = s"$file$rank"
                        val pkr = findSquare(sq, kingposRanks)
                        td(cls := List(sq -> true, squareClass(pkr) -> true), bits.dataSquare := sq)(
                          a(href := s"/patterns/${patternsType.id}/kingpos/rank/feature?kingSquare=$sq&color=${form("color").value | ""}&checkerRole[0]=${form("checkerRole[0]").value | ""}")(squarePercent(pkr))
                        )
                      }
                    )
                  }
                )
              )
            )
          ),
          div(cls := "patterns__search")(
            table(cls := "search__table")(
              tr(
                th(label(s"${patternsType.label1}方")),
                td(
                  form3.tagsRadio(form("color"), DataForm.colorChoices)
                )
              ),
              patternsType match {
                case PatternsType.Single => {
                  tr(
                    th(label("将杀子")),
                    td(
                      form3.tagsWithKv(form, "checkerRole", DataForm.checkerChoices, klass = "single")
                    )
                  )
                }
                case PatternsType.Double | PatternsType.Stalemate => frag()
              },
              tr(
                th, td(cls := "kingposDesc")
              ),
              tr(
                th, td("点击棋盘，进入模式快闪")
              )
            )
          ),
          div(cls := "patterns__control"),
          div(cls := "patterns__btm")
        )
      }

  private def findSquare(square: String, kingposRanks: List[PatternsKingposRank]): Option[PatternsKingposRank] = {
    kingposRanks.find(_.kingSquare == square)
  }

  private def squarePercent(pkrOption: Option[PatternsKingposRank]) = {
    pkrOption.map(_.percentString) | ""
  }

  private def squareClass(pkrOption: Option[PatternsKingposRank]) = {
    pkrOption.map { pkr =>
      val percent = pkr.percent * 100
      if (percent >= 15D) "s1"
      else if (percent >= 12D) "s2"
      else if (percent >= 10D) "s3"
      else if (percent >= 8D) "s4"
      else if (percent >= 6D) "s5"
      else if (percent >= 4D) "s6"
      else if (percent >= 2D) "s7"
      else if (percent >= 1D) "s8"
      else if (percent >= 0.7D) "s9"
      else if (percent >= 0.4D) "s10"
      else if (percent >= 0.2D) "s11"
      else if (percent >= 0.1D) "s12"
      else if (percent >= 0.08D) "s13"
      else if (percent >= 0.05D) "s14"
      else "s15"
    } | ""
  }

}
