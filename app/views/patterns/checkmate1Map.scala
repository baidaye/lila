package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.{ PatternsCheckmate1Chapter, PatternsCheckmate1Progress }
import controllers.rt_patterns.routes

object checkmate1Map {

  def apply(progress: PatternsCheckmate1Progress)(implicit ctx: Context) =
    views.html.base.layout(
      title = "基于模式的一步杀 - 将杀模式",
      moreJs = frag(
        jsTag("patterns.checkmate1.map.js")
      ),
      moreCss = cssTag("patternsCheckmate1.map"),
      zoomable = true
    ) {
        main(cls := "patternsCheckmate1-map")(
          div(cls := "patternsCheckmate1-map__side")(
            i(cls := "fat"),
            h1("一步杀"),
            h2("基于模式的一步杀"),
            div(cls := "progress")(
              div(cls := "text")(s"进度：${progress.progressPercent}%"),
              div(cls := "bar", style := s"width: ${progress.progressPercent}%;")
            ),
            div(cls := "actions")(
              a(cls := "resetProgress")("重置我的进度")
            )
          ),
          div(cls := "patternsCheckmate1-map__chapter")(
            div(cls := "chapters-area")(
              h2(cls := "title")("白棋杀王"),
              div(cls := "chapters")(
                PatternsCheckmate1Chapter.whites.map { ch =>
                  chapter(progress, ch)
                }
              )
            ),
            div(cls := "chapters-area")(
              h2(cls := "title")("黑棋杀王"),
              div(cls := "chapters")(
                PatternsCheckmate1Chapter.blacks.map { ch =>
                  chapter(progress, ch)
                }
              )
            )
          )
        )
      }

  def chapter(progress: PatternsCheckmate1Progress, ch: PatternsCheckmate1Chapter) = {
    a(cls := "chapter ongoing", href := routes.Patterns.checkmate1Chapter(ch.id))(
      span(cls := "ribbon-wrapper")(
        span(cls := "ribbon ongoing")(s"${progress.progressOf(ch)} / ${ch.total}")
      ),
      img(src := staticUrl("images/learn/guillotine.svg")),
      div(cls := "text")(
        h3(ch.name),
        p(cls := "subtitle")("")
      )
    )
  }

}
