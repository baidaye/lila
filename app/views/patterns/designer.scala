package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import controllers.rt_patterns.routes

object designer {

  def apply(
    sit: chess.Situation,
    fen: String,
    color: String,
    animationDuration: scala.concurrent.duration.Duration
  )(implicit ctx: Context) = views.html.base.layout(
    title = "将杀模式设计器 - 将杀模式",
    moreJs = frag(
      tagsinputTag,
      jsAt(s"compiled/lichess.editor${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""var data=${safeJsonValue(views.html.board.bits.jsData(sit, fen, animationDuration, "/patterns/designer/"))};data.patternsDesigner=true;data.patternsDesignerColor=`$color`;
LichessEditor(document.getElementById('board-editor'), data);""")
    ),
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("editor")
    ),
    chessground = false,
    zoomable = true
  )(main(id := "board-editor")(
      div(cls := "board-editor")(
        div(cls := "spare"),
        div(cls := "main-board")(chessgroundBoard),
        div(cls := "spare")
      )
    ))

}
