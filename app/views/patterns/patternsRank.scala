package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.PatternsRank
import lila.patterns.DataForm
import lila.patterns.PatternsOp.PatternsType
import play.api.data.Form
import bits.dataCoord
import controllers.routes

object patternsRank {

  def apply(patternsType: PatternsType, form: Form[_], rankWithNeighbor: PatternsRank.WithNeighbor)(implicit ctx: Context) =
    views.html.base.layout(
      title = "将杀模式排行 - 将杀模式",
      moreCss = cssTag("patterns"),
      moreJs = frag(
        jsTag("patterns.rank.js")
      ),
      zoomable = true
    ) {
        val sort = form("sort").value | "asc"
        val prev = if (sort == "asc") rankWithNeighbor.prev else rankWithNeighbor.next
        val next = if (sort == "asc") rankWithNeighbor.next else rankWithNeighbor.prev

        main(cls := "patterns patterns-rank")(
          form3.hidden("patternsType", patternsType.id),
          div(cls := "patterns__board")(
            rankWithNeighbor.curr.fold(div(cls := "main-board")(bits.emptyBoard)) { pr =>
              div(cls := "main-board")(
                div(
                  cls := List("cg-wrap parse-fen-manual" -> true, "is2d" -> ctx.pref.is2d, "is3d" -> ctx.pref.is3d),
                  dataColor := "white",
                  dataFen := pr.simplifiedFen,
                  dataCoord := ctx.pref.coords > 0
                )(cgWrapContent)
              )
            }
          ),
          div(cls := "patterns__search")(
            table(cls := "search__table")(
              tr(
                th(label(s"${patternsType.label1}方")),
                td(
                  form3.tagsRadio(form("color"), DataForm.colorChoices)
                )
              ),
              tr(
                th(label("排序")),
                td(
                  form3.tagsRadio(form("sort"), DataForm.sortChoices)
                )
              )
            )
          ),
          div(cls := "patterns__control")(
            div(cls := "control1")(
              button(cls := "button button-empty flip", dataIcon := "B", title := "翻转棋盘")("翻转棋盘")
            ),
            div(cls := "control")(
              button(cls := List("button prev" -> true, "disabled" -> prev.isEmpty), prev.isEmpty option disabled, dataId := (prev.map(_.order.toString) | ""))("上一个"),
              form3.input2(form("order"), vl = form("order").value.fold(rankWithNeighbor.currOrderOrEmpty) { v => if (v.isEmpty) rankWithNeighbor.currOrderOrEmpty else v }.some, typ = "number")(dataId := rankWithNeighbor.currOrderOrEmpty),
              button(cls := List("button next" -> true, "disabled" -> next.isEmpty), next.isEmpty option disabled, dataId := (next.map(_.order.toString) | ""))("下一个")
            )
          ),
          rankWithNeighbor.curr.fold(emptyBtm) { pr =>
            div(cls := "patterns__btm")(
              a(cls := "patternsOp", href := s"/patterns/${patternsType.id}/rank/feature?patternsOp=${pr.patternsOp}&color=${pr.win.name}&flipped=false")(pr.patternsOp),
              span(cls := "percent")(pr.percentString)
            )
          }
        )
      }

  val emptyBtm = div(cls := "patterns__btm")(
    a(cls := "patternsOp")("- -"),
    span(cls := "percent")("- -")
  )

}
