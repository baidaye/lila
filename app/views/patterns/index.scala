package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.PatternsOp.PatternsType
import controllers.rt_patterns.routes

object index {

  def apply()(implicit ctx: Context) =
    views.html.base.layout(
      title = "将杀模式",
      moreJs = frag(),
      moreCss = cssTag("patterns"),
      zoomable = true
    ) {
        val notAccept = !ctx.me.??(_.hasPatterns)
        main(cls := "patterns-index", dataNotAccept := s"$notAccept")(
          div(cls := "patterns-index__side")(
            i(cls := "fat"),
            h1("将杀模式"),
            p(a(href := routes.Patterns.intro())("将杀模式介绍"))
          ),
          div(cls := "patterns-index__main")(
            div(cls := "items-area")(
              div(cls := "items")(
                a(cls := "item ongoing", href := routes.Patterns.designerIndex())(h3("将杀设计器")),
                if (notAccept) a(cls := "item ongoing disabled")(
                  div(cls := "item-inner")(
                    h3("基于模式的一步杀", views.html.member.bits.vTip)
                  )
                )
                else a(cls := "item ongoing", href := routes.Patterns.checkmate1Map())(h3("基于模式的一步杀", views.html.member.bits.vTip)),
                a(cls := "item ongoing", href := routes.Patterns.eBook())(h3("电子书免费下载"))
              )
            ),
            div(cls := "items-area")(
              h2(cls := "title")("单子将杀模式"),
              div(cls := "items")(
                a(cls := "item ongoing", href := s"${routes.Patterns.patternsRank(PatternsType.Single.id)}?color=white&sort=asc")(h3("模式排行")),
                a(cls := "item ongoing", href := s"${routes.Patterns.kingposRank(PatternsType.Single.id)}?color=white")(h3("王的位置")),
                if (notAccept) a(cls := "item ongoing disabled")(
                  div(cls := "item-inner")(
                    h3("将杀子", views.html.member.bits.vTip)
                  )
                )
                else a(cls := List("item ongoing" -> true, "disabled" -> notAccept), href := s"${routes.Patterns.patternsPieceRank(PatternsType.Single.id)}?color=white&checkerRole=q&pinner=false")(h3("将杀子", views.html.member.bits.vTip))
              )
            ),
            div(cls := "items-area")(
              h2(cls := "title")("双子将杀模式"),
              div(cls := "items")(
                a(cls := "item ongoing", href := s"${routes.Patterns.patternsRank(PatternsType.Double.id)}?color=white&sort=asc")(
                  h3("模式排行（双子）")
                ),
                a(cls := "item ongoing", href := s"${routes.Patterns.kingposRank(PatternsType.Double.id)}?color=white")(
                  h3("王的位置（双子）")
                ),
                if (notAccept) a(cls := "item ongoing disabled")(
                  div(cls := "item-inner")(
                    h3("将杀子（双子）", views.html.member.bits.vTip)
                  )
                )
                else a(cls := List("item ongoing" -> true, "disabled" -> notAccept), href := s"${routes.Patterns.patternsPieceRank(PatternsType.Double.id)}?color=white&checkerRole=q&fixCheckerRole=q")(h3("将杀子（双子）", views.html.member.bits.vTip))
              )
            ),
            div(cls := "items-area")(
              h2(cls := "title")("逼和模式"),
              div(cls := "items")(
                a(cls := "item ongoing", href := s"${routes.Patterns.patternsRank(PatternsType.Stalemate.id)}?color=white&sort=asc")(
                  h3("模式排行（逼和）")
                ),
                a(cls := "item ongoing", href := s"${routes.Patterns.kingposRank(PatternsType.Stalemate.id)}?color=white")(
                  h3("王的位置（逼和）")
                ),
                if (notAccept) a(cls := "item ongoing disabled")(
                  div(cls := "item-inner")(
                    h3("控制子（逼和）", views.html.member.bits.vTip)
                  )
                )
                else a(cls := List("item ongoing" -> true, "disabled" -> notAccept), href := s"${routes.Patterns.patternsPieceRank(PatternsType.Stalemate.id)}?color=white&controllerRole[0]=q&pinner=false")(h3("控制子（逼和）", views.html.member.bits.vTip))
              )
            )
          )
        )
      }

}
