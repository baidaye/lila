package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.{ DataForm, PatternsKingposRankFeature }
import lila.patterns.PatternsOp.PatternsType
import play.api.data.Form
import bits.dataCoord
import bits.dataFenType
import controllers.routes

object kingposRankFeature {

  def apply(patternsType: PatternsType, form: Form[_], featureWithNeighbor: PatternsKingposRankFeature.WithNeighbor)(implicit ctx: Context) =
    views.html.base.layout(
      title = "王的位置 - 将杀模式",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("patterns")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("patterns.kingposRankFeature.js")
      ),
      zoomable = true
    ) {
        val flipped = form("flipped").value | "false"
        main(cls := "patterns patterns-kingposRankFeature", dataNotAccept := s"${!ctx.me.??(_.hasPatterns)}")(
          form3.hidden("patternsType", patternsType.id),
          form3.hidden(form("kingSquare")),
          div(cls := "patterns__board")(
            featureWithNeighbor.curr.fold(div(cls := "main-board")(bits.emptyBoard)) { feature =>
              div(cls := "main-board")(
                div(
                  cls := List("cg-wrap parse-fen-manual" -> true, "is2d" -> ctx.pref.is2d, "is3d" -> ctx.pref.is3d),
                  dataColor := (if (flipped == "true") "black" else "white"),
                  dataFen := feature.simplifiedCheckmateFen,
                  dataCoord := ctx.pref.coords > 0
                )(cgWrapContent)
              )
            }
          ),
          div(cls := "patterns__search")(
            table(cls := "search__table")(
              tr(
                th(label(s"${patternsType.label1}方")),
                td(
                  form3.tagsRadio(form("color"), DataForm.colorChoices)
                )
              ),
              patternsType match {
                case PatternsType.Single => {
                  tr(
                    th(label("将杀子")),
                    td(
                      form3.tagsWithKv(form, "checkerRole", DataForm.checkerChoices, klass = "single")
                    )
                  )
                }
                case PatternsType.Double | PatternsType.Stalemate => frag()
              },
              tr(
                th,
                td(
                  div(cls := "kingPosBoard")(
                    div(cls := "mini-board cg-wrap is2d", dataFen := featureWithNeighbor.curr.map(_.kingSquareFen) | "8/8/8/8/8/8/8/8 w - -")
                  )
                )
              )
            )
          ),
          div(cls := "patterns__control")(
            div(cls := "control1")(
              featureWithNeighbor.curr.fold(
                a(cls := "patternsOp")("- -")
              ) { feature =>
                  a(cls := "patternsOp", href := s"/patterns/${patternsType.id}/rank/feature?patternsOp=${feature.patternsOp}&color=${feature.win.name}&flipped=false")(feature.patternsOp)
                },
              button(cls := "button button-empty flip", dataIcon := "B", bits.dataFlipped := flipped, title := "翻转棋盘")("翻转棋盘")
            ),
            featureWithNeighbor.curr.fold(
              div(cls := "fen-line")(
                button(cls := "button button-empty small disabled active", dataFenType := "simplifiedCheckmateFen", disabled)(s"简化${patternsType.label1}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalCheckmateFen", disabled)(s"原始${patternsType.label1}"),
                patternsType != PatternsType.Stalemate option button(cls := "button button-empty small disabled", dataFenType := "simplifiedFen", disabled)(s"简化${patternsType.label2}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalFen", disabled)(s"原始${patternsType.label2}")
              )
            ) { feature =>
                div(cls := "fen-line")(
                  button(cls := "button button-empty small active", dataFenType := "simplifiedCheckmateFen", dataFen := feature.simplifiedCheckmateFen)(s"简化${patternsType.label1}"),
                  button(cls := "button button-empty small", dataFenType := "originalCheckmateFen", dataFen := feature.originalCheckmateFen)(s"原始${patternsType.label1}"),
                  patternsType != PatternsType.Stalemate option button(cls := "button button-empty small", dataFenType := "simplifiedFen", dataFen := feature.simplifiedFen)(s"简化${patternsType.label2}"),
                  button(cls := "button button-empty small", dataFenType := "originalFen", dataFen := feature.originalFen)(s"原始${patternsType.label2}")
                )
              },
            div(cls := "control")(
              button(cls := List("button prev" -> true, "disabled" -> !featureWithNeighbor.hasPrev), !featureWithNeighbor.hasPrev option disabled, dataId := featureWithNeighbor.prevOrderOrEmpty)("上一个"),
              form3.input2(form("order"), vl = form("order").value.fold(featureWithNeighbor.currOrderOrEmpty) { v => if (v.isEmpty) featureWithNeighbor.currOrderOrEmpty else v }.some, typ = "number")(dataId := featureWithNeighbor.currOrderOrEmpty),
              button(cls := List("button next" -> true, "disabled" -> !featureWithNeighbor.hasNext), !featureWithNeighbor.hasNext option disabled, dataId := featureWithNeighbor.nextOrderOrEmpty)("下一个")
            )
          ),
          featureWithNeighbor.curr.fold(
            div(cls := "patterns__btm")(
              input(value := "8/8/8/8/8/8/8/8 w - -", readonly, cls := "form-control fen"),
              button(cls := "button small disabled btnSituation", disabled)("保存")
            )
          ) { feature =>
              div(cls := "patterns__btm")(
                input(value := feature.simplifiedCheckmateFen, readonly, cls := "form-control fen"),
                button(cls := "button small btnSituation")("保存")
              )
            }
        )
      }

}
