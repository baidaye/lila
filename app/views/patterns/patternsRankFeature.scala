package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.patterns.{ DataForm, PatternsRankFeature }
import lila.patterns.PatternsOp.PatternsType
import play.api.data.Form
import bits.dataCoord
import bits.dataFenType
import bits.dataRole
import controllers.routes

object patternsRankFeature {

  def apply(patternsType: PatternsType, form: Form[_], rankFeatureWithNeighbor: PatternsRankFeature.WithNeighbor)(implicit ctx: Context) =
    views.html.base.layout(
      title = "将杀模式排行 - 将杀模式",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("patterns")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("patterns.rankFeature.js")
      ),
      zoomable = true
    ) {
        val flipped = form("flipped").value | "false"
        main(cls := "patterns patterns-rankFeature", dataNotAccept := s"${!ctx.me.??(_.hasPatterns)}")(
          form3.hidden("patternsType", patternsType.id),
          div(cls := "patterns__board")(
            rankFeatureWithNeighbor.curr.fold(div(cls := "main-board")(bits.emptyBoard)) { feature =>
              div(cls := "main-board")(
                div(
                  cls := List("cg-wrap parse-fen-manual" -> true, "is2d" -> ctx.pref.is2d, "is3d" -> ctx.pref.is3d),
                  dataColor := (if (flipped == "true") "black" else "white"),
                  dataFen := feature.simplifiedCheckmateFen,
                  dataCoord := ctx.pref.coords > 0
                )(cgWrapContent)
              )
            }
          ),
          div(cls := "patterns__search")(
            table(cls := "search__table")(
              tr(
                th(label(s"${patternsType.label1}方")),
                td(
                  form3.tagsRadio(form("color"), DataForm.colorChoices)
                )
              ),
              tr(
                th(label("CPN")),
                td(
                  div(cls := "cpn-input")(
                    form3.input2(form("patternsOpOfCpn"), vl = form("patternsOp").value),
                    form3.hidden(form("patternsOp")),
                    button(cls := "button button-empty op-random", dataIcon := "P", title := "随机")("随机")
                  )
                )
              ),
              tr(
                th,
                td(
                  div(cls := s"cpn-editor ${form("color").value.map(c => if (c == "white") "black" else "white") | "white"}")(
                    div(cls := "cpn-editor-board")(
                      div(cls := s"editor-square dropzone square-8"),
                      div(cls := s"editor-square dropzone square-1"),
                      div(cls := s"editor-square dropzone square-2"),
                      div(cls := s"editor-square dropzone square-7"),
                      div(cls := s"editor-square square-king")(
                        div(cls := "editor-piece")(div(cls := "piece-piece-inner editor-piece-king", dataRole := "king"))
                      ),
                      div(cls := s"editor-square dropzone square-3"),
                      div(cls := s"editor-square dropzone square-6"),
                      div(cls := s"editor-square dropzone square-5"),
                      div(cls := s"editor-square dropzone square-4")
                    ),
                    div(cls := "cpn-editor-spare")(
                      div(cls := "editor-piece")(div(cls := "piece-piece-inner editor-piece-z", dataRole := "Z", draggable := "true")),
                      div(cls := "editor-piece")(div(cls := "piece-piece-inner editor-piece-p", dataRole := "P", draggable := "true")),
                      div(cls := "editor-piece")(div(cls := "piece-piece-inner editor-piece-o", dataRole := "O", draggable := "true")),
                      patternsType != PatternsType.Stalemate option div(cls := "editor-piece")(div(cls := "piece-piece-inner editor-piece-x", dataRole := "X", draggable := "true"))
                    ),
                    div(cls := "cpn-editor-desc")(
                      div(cls := "editor-desc")("棋盘边缘"),
                      div(cls := "editor-desc")("败方棋子"),
                      div(cls := "editor-desc")("胜方棋子"),
                      patternsType != PatternsType.Stalemate option div(cls := "editor-desc")("将杀子")
                    )
                  )
                )
              ),
              tr(
                td(colspan := 2)(
                  button(cls := "button op-search", dataIcon := "y", title := "搜索")("搜索")
                )
              )
            )
          ),
          div(cls := "patterns__control")(
            div(cls := "control1")(
              rankFeatureWithNeighbor.curr.fold(
                div(cls := "gameFrom")("来自对局", a("#- -"))
              ) { pr =>
                  div(cls := "gameFrom")("来自对局", a(href := routes.Round.watcherOfExternal(pr.gameId, pr.win.name, pr.ply))("#", pr.gameId))
                },
              button(cls := "button button-empty flip", dataIcon := "B", bits.dataFlipped := flipped, title := "翻转棋盘")("翻转棋盘")
            ),
            rankFeatureWithNeighbor.curr.fold(
              div(cls := "fen-line")(
                button(cls := "button button-empty small disabled active", dataFenType := "simplifiedCheckmateFen", disabled)(s"简化${patternsType.label1}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalCheckmateFen", disabled)(s"原始${patternsType.label1}"),
                patternsType != PatternsType.Stalemate option button(cls := "button button-empty small disabled", dataFenType := "simplifiedFen", disabled)(s"简化${patternsType.label2}"),
                button(cls := "button button-empty small disabled", dataFenType := "originalFen", disabled)(s"原始${patternsType.label2}")
              )
            ) { feature =>
                div(cls := "fen-line")(
                  button(cls := "button button-empty small active", dataFenType := "simplifiedCheckmateFen", dataFen := feature.simplifiedCheckmateFen)(s"简化${patternsType.label1}"),
                  button(cls := "button button-empty small", dataFenType := "originalCheckmateFen", dataFen := feature.originalCheckmateFen)(s"原始${patternsType.label1}"),
                  patternsType != PatternsType.Stalemate option button(cls := "button button-empty small", dataFenType := "simplifiedFen", dataFen := feature.simplifiedFen)(s"简化${patternsType.label2}"),
                  button(cls := "button button-empty small", dataFenType := "originalFen", dataFen := feature.originalFen)(s"原始${patternsType.label2}")
                )
              },
            div(cls := "control")(
              button(cls := List("button prev" -> true, "disabled" -> !rankFeatureWithNeighbor.hasPrev), !rankFeatureWithNeighbor.hasPrev option disabled, dataId := rankFeatureWithNeighbor.prevOrderOrEmpty)("上一个"),
              form3.input2(form("order"), vl = form("order").value.fold(rankFeatureWithNeighbor.currOrderOrEmpty) { v => if (v.isEmpty) rankFeatureWithNeighbor.currOrderOrEmpty else v }.some, typ = "number")(dataId := rankFeatureWithNeighbor.currOrderOrEmpty),
              button(cls := List("button next" -> true, "disabled" -> !rankFeatureWithNeighbor.hasNext), !rankFeatureWithNeighbor.hasNext option disabled, dataId := rankFeatureWithNeighbor.nextOrderOrEmpty)("下一个")
            )
          ),
          rankFeatureWithNeighbor.curr.fold(
            div(cls := "patterns__btm")(
              input(value := "8/8/8/8/8/8/8/8 w - -", readonly, cls := "form-control fen"),
              button(cls := "button small disabled btnSituation", disabled)("保存")
            )
          ) { feature =>
              div(cls := "patterns__btm")(
                input(value := feature.simplifiedCheckmateFen, readonly, cls := "form-control fen"),
                button(cls := "button small btnSituation")("保存")
              )
            }
        )
      }

}
