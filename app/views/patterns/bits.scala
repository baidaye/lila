package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

object bits {

  val dataCoord = attr("data-coordinates")
  val dataFenType = attr("data-fenType")
  val dataSquare = attr("data-square")
  val dataRole = attr("data-role")
  val dataFlipped = attr("data-flipped")

  def emptyBoard(implicit ctx: Context) =
    div(
      cls := List("cg-wrap parse-fen-manual" -> true, "is2d" -> ctx.pref.is2d, "is3d" -> ctx.pref.is3d),
      dataColor := "white",
      dataFen := "8/8/8/8/8/8/8/8 w - -",
      dataCoord := ctx.pref.coords > 0
    )(cgWrapContent)

}
