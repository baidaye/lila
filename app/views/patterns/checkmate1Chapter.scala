package views.html.patterns

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.patterns.{ PatternsCheckmate1Chapter, PatternsCheckmate1Typ }
import play.api.libs.json.{ JsArray, JsObject, Json }

object checkmate1Chapter {

  def apply(chapter: PatternsCheckmate1Chapter, progress: JsObject, checkmate1: JsObject, pref: JsObject)(implicit ctx: Context) =
    views.html.base.layout(
      title = "基于模式的一步杀 - 将杀模式",
      moreCss = cssTag("patternsCheckmate1.chapter"),
      moreJs = frag(
        jsAt(s"compiled/lichess.patternsCheckmate1${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.patternsCheckmate1=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "notAccept" -> !ctx.me.??(_.hasPatterns),
            "pref" -> pref,
            "data" -> Json.obj(
              "chapter" -> chapter.id,
              "progress" -> progress,
              "checkmate1" -> checkmate1
            )
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "patternsCheckmate1-chapter")
      }

}
