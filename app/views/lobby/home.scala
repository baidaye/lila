package views.html.lobby

import play.api.libs.json.{ JsObject, Json }
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.game.Pov
import lila.lichessApi.OAuth2
import controllers.routes

object home {

  def apply(
    data: JsObject,
    playban: Option[lila.playban.TempBan],
    currentGame: Option[lila.app.mashup.Preload.CurrentGame],
    blindGames: List[Pov], // only in blind mode,
    lichessOAuth2: Option[OAuth2],
    nbRounds: Int
  )(implicit ctx: Context) = views.html.base.layout(
    title = "",
    fullTitle = Some {
      s"${trans.freeOnlineChess.txt()}"
    },
    moreJs = frag(
      flatpickrTag,
      jsAt(s"compiled/lichess.lobby${isProd ?? (".min")}.js", defer = true),
      embedJsUnsafe(
        s"""lichess=window.lichess||{};customWS=true;lichess_lobby=${
          safeJsonValue(Json.obj(
            "data" -> data.add("perfs" -> ctx.me.map(u => {
              Json.obj(
                "ultraBullet" -> u.perfs.ultraBullet.intRating,
                "bullet" -> u.perfs.bullet.intRating,
                "blitz" -> u.perfs.blitz.intRating,
                "rapid" -> u.perfs.rapid.intRating,
                "classical" -> u.perfs.classical.intRating,
                "correspondence" -> u.perfs.correspondence.intRating
              )
            })),
            "playban" -> playban.map { pb =>
              Json.obj(
                "minutes" -> pb.mins,
                "remainingSeconds" -> (pb.remainingSeconds + 3)
              )
            },
            "oAuth2" -> lichessOAuth2.map { o =>
              lila.lichessApi.Env.current.jsonView.oAuth2(o)
            },
            "pools" -> lila.pool.PoolList.toJson,
            "lichessApiAccept" -> ctx.me.??(_.isMemberOrCoachOrTeam),
            "isMemberOrCoachOrTeam" -> ctx.me.??(_.isMemberOrCoachOrTeam),
            "i18n" -> i18nJsObject(translations)
          ))
        }"""
      )
    ),
    moreCss = cssTag("lobby"),
    chessground = false,
    openGraph = lila.app.ui.OpenGraph(
      image = staticUrl("images/large_tile.png").some,
      title = "The best free, adless Chess server",
      url = netBaseUrl,
      description = trans.siteDescription.txt()
    ).some,
    deferJs = true
  ) {
      val lichessApiAccept = ctx.me.??(_.isMemberOrCoachOrTeam)
      frag(
        div(cls := "lobby__spinner none")(spinner),
        main(cls := List("lobby" -> true, "lobby-nope" -> (playban.isDefined || currentGame.isDefined)))(
          div(cls := "lobby__table")(
            div(cls := "lobby__start")(
              ctx.blind option h2("Play"),
              a(href := routes.Setup.hookForm, cls := List(
                "button button-metal config_hook" -> true,
                "disabled" -> (playban.isDefined || currentGame.isDefined || ctx.isBot)
              ), trans.createAGame()),
              a(href := (routes.Setup.friendForm(none).toString + "?appt=false"), cls := List(
                "button button-metal config_friend" -> true,
                "disabled" -> currentGame.isDefined
              ), trans.playWithAFriend()),
              ctx.isAuth option a(href := routes.Setup.friendForm(none), cls := List(
                "button button-metal config_friend_appt" -> true,
                "disabled" -> currentGame.isDefined
              ), "和朋友约棋"),
              a(href := routes.Setup.aiForm, cls := List(
                "button button-metal config_ai" -> true,
                "disabled" -> currentGame.isDefined
              ), trans.playWithTheMachine())
            ),
            div(cls := "lobby__counters")(
              ctx.blind option h2("Counters"),
              a(id := "nb_connected_players", href := ctx.noBlind.option(routes.User.list.toString))(trans.nbPlayers(nbPlayersPlaceholder)),
              a(id := "nb_games_in_play", href := ctx.noBlind.option(routes.Tv.games.toString))(
                trans.nbGamesInPlay.plural(nbRounds, strong(nbRounds.localize))
              )
            )
          ),
          currentGame.map(bits.currentGameInfo) orElse
            playban.map(bits.playbanInfo) getOrElse {
              if (ctx.blind) blindLobby(blindGames)
              else bits.lobbyApp
            },
          ctx.isAuth && lichessApiAccept option div(cls := "lobby__third")(
            div(cls := "chk")(
              input(tpe := "checkbox", id := "lichessLogin", lichessOAuth2.isDefined option checked),
              label(`for` := "lichessLogin")("在lichess.org上匹配")
            ),
            div(cls := "uname")(
              lichessOAuth2.map(o => o.lichessUser.map(u => a(href := routes.LichessApi.oAuthList())(s"（${u.username}）")))
            )
          ),
          div(cls := "lobby__pools")(
            div(cls := "lpools")(
              lila.pool.PoolList.all.map { poolConfig =>
                div(cls := List("disabled" -> (lichessOAuth2.isDefined && !poolConfig.lichessApi && lichessApiAccept)), dataId := poolConfig.clock.show)(
                  div(cls := "clock")(poolConfig.clock.show),
                  div(cls := "perf")(poolConfig.perfType.name)
                )
              },
              div(cls := List("custom" -> true, "disabled" -> (lichessOAuth2.isDefined && lichessApiAccept)), dataId := "custom")("自定义")
            )
          )
        )
      )
    }

  private val translations = List(
    trans.realTime,
    trans.correspondence,
    trans.nbGamesInPlay,
    trans.player,
    trans.time,
    trans.joinTheGame,
    trans.cancel,
    trans.casual,
    trans.rated,
    trans.variant,
    trans.mode,
    trans.list,
    trans.graph,
    trans.filterGames,
    trans.youNeedAnAccountToDoThat,
    trans.oneDay,
    trans.nbDays,
    trans.aiNameLevelAiLevel,
    trans.yourTurn,
    trans.rating,
    trans.createAGame,
    trans.quickPairing,
    trans.lobby,
    trans.custom,
    trans.anonymous
  )

  private val nbPlayersPlaceholder = strong("--,---")
}
