package views.html.errors

import lila.api.Context
import play.api.data.Form
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.errors.{ DataForm, PuzzleErrors }
import lila.hub.PuzzleHub
import controllers.routes

object puzzle {

  def apply(form: Form[_], pager: Paginator[PuzzleErrors])(implicit ctx: Context) = bits.layout(
    title = "错题库-战术题",
    active = "puzzle",
    form = form,
    pager = pager,
    call = routes.Errors.puzzle(1)
  ) {
      st.form(
        cls := "error_search_form",
        action := s"${routes.Errors.puzzle(1)}#results",
        method := "GET"
      )(
          table(
            tr(
              th(label("难度范围")),
              td(
                div(cls := "half")("从 ", form3.input(form("ratingMin"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}")),
                div(cls := "half")("到 ", form3.input(form("ratingMax"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}"))
              )
            ),
            tr(
              th(label("时间")),
              td(
                div(cls := "half")("从 ", form3.input(form("sTime"), klass = "flatpickr")),
                div(cls := "half")("到 ", form3.input(form("eTime"), klass = "flatpickr"))
              )
            ),
            tr(
              th,
              td(
                form3.radio2(form("time"), DataForm.timeChoices, (6 * 30 * -1).toString.some)
              )
            ),
            tr(
              th(label("来源")),
              td(
                form3.radio2(form("source"), DataForm.sourceChoices, "".some)
              )
            ),
            tr(
              th(label("棋色")),
              td(
                form3.radio2(form("color"), DataForm.colorChoices, "".some)
              )
            ),
            tr(
              th(label("排序")),
              td(form3.select(form("sort"), DataForm.sortChoices, klass = "sort"))
            ),
            tr(
              th,
              td(cls := "action")(
                submitButton(cls := "button")("搜索")
              )
            )
          ),
          table(
            tr(
              td(
                span(cls := "is-gold keep-info", dataIcon := "")("错题最多保留半年，如果需要长期保留，请添加到收藏。")
              ),
              td(cls := "action")(
                pager.nbResults > 0 option bits.actions(List("delete", "toCapsule"))
              )
            )
          )
        )
    }

}
