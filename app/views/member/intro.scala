package views.html.member

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.MemberLevel
import scala.util.Random
import controllers.routes

object intro {

  def apply(goldCardNumber: Int, silverCardNumber: Int)(implicit ctx: Context) = {
    val cm = Map(
      MemberLevel.Gold.code -> goldCardNumber,
      MemberLevel.Silver.code -> silverCardNumber
    )

    div(cls := "modal-content member-intro none")(
      div(cls := "modal-header")(
        h3("会员升级"),
        span(cls := "close", dataIcon := "L")
      ),
      div(cls := "levels")(
        MemberLevel.all map { level =>
          val permissions = level.permissions
          div(cls := s"level ${level.code}")(
            div(cls := "level-title")(
              img(cls := "icon", src := staticUrl(s"/images/icons/${level.code}.svg")),
              div(
                div(cls := "name")(level.name),
                div(cls := "desc")(level.desc)
              )
            ),
            div(cls := "level-permission")(
              table(
                tr(
                  th("战术训练"),
                  td(
                    if (permissions.puzzle == Int.MaxValue) "无限制"
                    else frag(permissions.puzzle, "/天")
                  )
                ),
                tr(
                  th("主题战术"),
                  td(
                    if (permissions.themePuzzle == Int.MaxValue) "无限制"
                    else frag(permissions.themePuzzle, "/天")
                  )
                ),
                tr(
                  th("战术冲刺"),
                  td(
                    if (permissions.puzzleRush == Int.MaxValue) "无限制"
                    else frag(permissions.puzzleRush, "/天")
                  )
                ),
                tr(
                  th("战术题累计"),
                  td(
                    if (permissions.puzzleTotal == Int.MaxValue) "无限制"
                    else frag(permissions.puzzleTotal, "/天")
                  )
                ),
                tr(
                  th("与电脑对弈"),
                  td(
                    if (permissions.playWithAi == Int.MaxValue) "无限制"
                    else frag(permissions.playWithAi, "/天")
                  )
                ),
                tr(
                  th("引擎分析"),
                  td(
                    if (permissions.analyse == Int.MaxValue) "无限制"
                    else frag(permissions.analyse, "/天")
                  )
                ),
                tr(
                  th("资源管理"),
                  td(
                    if (permissions.resource) "有" else "无"
                  )
                ),
                tr(
                  th("任务直通车"),
                  td(
                    if (permissions.throughTrain) "有" else "无"
                  )
                ),
                tr(
                  th("将杀模式"),
                  td(
                    if (permissions.patterns) "有" else "无"
                  )
                ),
                tr(
                  th("开局库"),
                  td(if (permissions.createOpeningdb == 0) "无" else s"免费${permissions.createOpeningdb}个")
                ),
                tr(
                  th("增值服务"),
                  td(
                    if (permissions.discount == BigDecimal(1.0)) "全价" else s"${permissions.discount * 10}折"
                  )
                )
              )
            ),
            div(cls := "level-foot")(
              if (level.prices.year.equals(0)) frag(
                div(cls := "price invisible")(
                  span(cls := "symbol")("￥"),
                  span(cls := "number")(level.prices.month.setScale(1).toString),
                  nbsp, "/月"
                ),
                div(cls := "buy")(span(cls := "btn free disabled")("免费")),
                div(cls := "useCard")
              )
              else frag(
                div(cls := "price")(
                  span(cls := "symbol")("￥"),
                  span(cls := "number")(level.prices.month.setScale(1).toString),
                  nbsp, "/月"
                ),
                div(cls := "buy")(
                  a(cls := List("btn toBuy" -> true, "disabled" -> (isGranted(_.Coach) || isGranted(_.Team))), (!isGranted(_.Coach) && !isGranted(_.Team)) option (href := routes.Member.toBuy(level.code.some)))("购买")
                ),
                div(cls := "useCard")(
                  cm.exists(d => d._1 == level.code && d._2 > 0) && !isGranted(_.Coach) && !isGranted(_.Team) option a(href := s"${routes.Member.info}?q=${Random.nextInt()}#card")(cm.find(d => d._1 == level.code).map(_._2) | 0, " 张待用卡")
                )
              )
            )
          )
        }
      ) /*,
      div(cls := "ad")("通过认证教练或俱乐部购买，可享受团购优惠！")*/
    )
  }

}
