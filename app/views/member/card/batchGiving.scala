package views.html.member.card

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import play.api.data.Form
import lila.team.{ Campus, Member, Team }
import controllers.routes

object batchGiving {

  def team(
    form: Form[_],
    team: Team,
    members: List[User],
    tags: List[lila.team.Tag],
    campuses: List[Campus],
    markMap: Map[String, Option[String]],
    hideGivingInMonth: Boolean,
    advance: Boolean
  )(implicit ctx: Context) =
    bits.layout(
      title = "批量赠卡",
      active = "batchGiving",
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        memberAdvanceTag,
        jsTag("member.batchGiving.js")
      )
    ) {
        frag(
          div(cls := "box__top")(
            h1("批量赠卡")
          ),
          st.form(rel := "nofollow", cls := "member-search", action := s"${routes.MemberCard.teamBatchGiving()}#results", method := "GET")(
            table(
              tr(
                td(cls := "label")(label("账号/备注（姓名）")),
                td(cls := "fixed")(form3.input(form("username"))),
                td(cls := "label")(label("校区")),
                td(cls := "fixed")(form3.select(form("campus"), campuses.map(c => c.id -> c.name)))
              ),
              tr(
                td(cls := "label")(label("角色")),
                td(cls := "fixed")(form3.select(form("role"), Member.Role.list, "".some)),
                td(cls := "label")(label("年龄")),
                td(cls := "fixed")(form3.input(form("age"), typ = "number"))
              ),
              tr(
                td(cls := "label")(label("棋协级别")),
                td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
                td(cls := "label")(label("性别")),
                td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
              ),
              tr(
                td(cls := "label")(label("会员类型")),
                td(cls := "fixed")(form3.select(form("memberLevel"), lila.user.MemberLevel.all.map(d => d.code -> d.name), "全部".some)),
                td(cls := "label"),
                td(cls := "fixed left")(
                  st.input(st.id := "hideGivingInMonth", name := "hideGivingInMonth", tpe := "checkbox", value := "true", hideGivingInMonth option checked),
                  label(`for` := "hideGivingInMonth")("隐藏最近1个月内赠过卡的学员")
                )
              ),
              tags.nonEmpty option tr(
                td(colspan := 4)(
                  form3.hidden("advance", advance.toString),
                  a(cls := List("show-search" -> true, "expanded" -> advance))(if (advance) frag("隐藏高级搜索", iconTag("S")) else frag("显示高级搜索", iconTag("R")))
                )
              ),
              tags.filterNot(_.typ.range).zipWithIndex.map {
                case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), advance)
              },
              tags.filter(_.typ.range).zipWithIndex.map {
                case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), advance)
              },
              tr(
                td(colspan := 4)(
                  div(cls := "action")(
                    div(a(cls := "button button-green btn-giving", href := routes.MemberCard.batchGivingModal())("转赠会员卡"), nbsp, span("最多选择50名学员")),
                    submitButton(cls := "button")(trans.search())
                  )
                )
              )
            )
          ),
          memberTable(form, members, markMap)
        )
      }

  def coach(
    form: Form[_],
    members: List[User],
    clazzes: List[(String, String)],
    markMap: Map[String, Option[String]],
    clazzId: String,
    username: Option[String],
    memberLevel: Option[String],
    hideGivingInMonth: Boolean
  )(implicit ctx: Context) =
    bits.layout(
      title = "批量赠卡",
      active = "batchGiving",
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        memberAdvanceTag,
        jsTag("member.batchGiving.js")
      )
    ) {
        frag(
          div(cls := "box__top")(
            h1("批量赠卡")
          ),
          st.form(rel := "nofollow", cls := "member-search", action := s"${routes.MemberCard.coachBatchGiving()}#results", method := "GET")(
            table(
              tr(
                td(cls := "label")(label("账号/备注（姓名）")),
                td(cls := "fixed")(form3.input2(form("username"), username)),
                td(cls := "label")(label("班级")),
                td(cls := "fixed")(form3.select2(form("clazzId"), clazzId.some, clazzes))
              ),
              tr(
                td(cls := "label")(label("会员类型")),
                td(cls := "fixed")(form3.select2(form("memberLevel"), memberLevel, lila.user.MemberLevel.all.map(d => d.code -> d.name), "全部".some)),
                td(cls := "label"),
                td(cls := "fixed left")(
                  st.input(st.id := "hideGivingInMonth", name := "hideGivingInMonth", tpe := "checkbox", value := "true", hideGivingInMonth option checked),
                  label(`for` := "hideGivingInMonth")("隐藏最近1个月内赠过卡的学员")
                )
              ),
              tr(
                td(colspan := 4)(
                  div(cls := "action")(
                    div(a(cls := "button button-green btn-giving", href := routes.MemberCard.batchGivingModal())("转赠会员卡"), nbsp, span("最多选择50名学员")),
                    submitButton(cls := "button")(trans.search())
                  )
                )
              )
            )
          ),
          memberTable(form, members, markMap)
        )
      }

  def memberTable(form: Form[_], members: List[User], markMap: Map[String, Option[String]])(implicit ctx: Context) =
    table(cls := "slist members")(
      thead(
        tr(
          th(input(tpe := "checkbox", name := "mem_chk_all")),
          th("账号"),
          th("备注（姓名）"),
          th("会员类型"),
          th("有效期至")
        )
      ),
      if (members.nonEmpty) {
        tbody(cls := "infinitescroll")(
          st.form(
            members.zipWithIndex.map {
              case (user, index) => {
                val lvWithExpire = user.memberOrDefault.lvWithExpire
                tr(cls := "paginated")(
                  td(input(tpe := "checkbox", name := s"mems[$index]", value := user.id)),
                  td(userLink(user, withBadge = false)),
                  td(views.html.team.bits.userMark(user, markMap)),
                  td(lvWithExpire.level.name),
                  td(lvWithExpire.expireNote)
                )
              }
            }
          )
        )
      } else {
        tbody(
          tr(
            td(colspan := 5)("暂无记录")
          )
        )
      }
    )

  def batchGivingModal(form: Form[_], members: List[User], markMap: Map[String, Option[String]])(implicit ctx: Context) =
    div(cls := "modal-content batchGivingModal none")(
      h2("转赠会员卡"),
      postForm(cls := "form3", action := routes.MemberCard.batchGivingApply())(
        form3.group(form("mems"), raw(s"已选学员（${members.size} 人）："))(_ =>
          members.zipWithIndex.map {
            case (user, index) =>
              div(cls := "mem")(
                span(views.html.team.bits.userMark(user, markMap)),
                form3.hidden(s"mems[$index]", user.id), ","
              )
          }),
        form3.split(
          form3.group(form("cardLevel"), raw("会员卡类型"), half = true)(form3.select2(_, lila.user.MemberLevel.Gold.code.some, lila.user.MemberLevel.choices)),
          form3.group(form("days"), raw("使用期限"), half = true)(form3.select2(_, lila.member.Days.Year1.id.some, lila.member.Days.choices2))
        ),
        form3.group(form("num"), raw("有效卡数量："))(_ => frag(
          form3.hidden("memberCount", members.size.toString),
          span(cls := "num")("0"), span(" 张")
        )),
        div(cls := "form-group is-invalid count-invalid none")(
          "会员卡数量不足，请重新选择"
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("转赠卡", klass = "small", isDisable = true)
        )
      )
    )

  def batchGivingSuccess(
    level: String,
    days: String,
    members: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    div(cls := "modal-content batchGivingSuccessModal none")(
      h2(s"成功赠送${members.size}张会员卡"),
      postForm(cls := "form3")(
        div(cls := "form-group")(
          label("会员卡类型："),
          div(cls := "content")(level)
        ),
        div(cls := "form-group")(
          label("使用期限："),
          div(cls := "content")(days)
        ),
        div(cls := "form-group")(
          label("学员列表："),
          div(cls := "content")(
            members.map { mu =>
              span(views.html.team.bits.userMark(mu, markMap), ", ")
            }
          )
        ),
        form3.actions(
          a(cls := "cancel")("关闭"),
          a(href := routes.MemberCard.givingLogPage(1))("查看详情")
        )
      )
    )

}
