package views.html.member

import lila.user.{ Member, MemberLevel, User }
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.member.{ ExchangeCard, MemberCard, MemberLevelLog, MemberPointsLog, Order }
import lila.member.MemberLevelLog.MemberLevelChangeType
import lila.user.MemberLevel.{ Gold, Silver }
import scala.math.BigDecimal.RoundingMode
import play.api.data.Form
import controllers.routes

object info {

  private val dataTab = attr("data-tab")

  def apply(
    u: lila.user.User,
    orders: List[Order],
    cards: List[MemberCard],
    levelChangeLogs: List[MemberLevelLog],
    levelPointsLogs: List[MemberPointsLog],
    levelPointsLogOrders: List[Order],
    exchangeCards: List[ExchangeCard]
  )(implicit ctx: Context) = views.html.account.layout(
    title = s"${u.username} - 会员信息",
    active = "member",
    evenMoreJs = frag(
      jsTag("member.info.js")
    ),
    evenMoreCss = cssTag("member")
  ) {
      val member = u.memberOrDefault
      div(cls := "box box-pad account member-info")(
        div(cls := "member-line")(
          userSpan(u, cssClass = "large".some, withPowerTip = false),
          div(member.points, nbsp, "积分")
        ),
        div(cls := "levels")(
          MemberLevel.nofree.map { l =>
            level(l, member)
          }
        ),
        div(cls := "member-actions")(
          a(cls := "button small modal-exchange-open", href := routes.Member.exchangeUseModal())("兑换码")
        ),
        div(cls := "component")(
          div(cls := "tabs-horiz")(
            span(dataTab := "levelLogs", cls := "active")(s"续费记录(${levelChangeLogs.length})"),
            span(dataTab := "card")(s"会员卡(${cards.filterNot(_.isUsed).length})"),
            span(dataTab := "exchange")(s"兑换码记录(${exchangeCards.length})"),
            span(dataTab := "coupon")("优惠券(0)"),
            span(dataTab := "points")(s"积分记录(${levelPointsLogs.length})"),
            span(dataTab := "orders")(s"历史订单(${orders.length})")
          ),
          div(cls := "tabs-content")(
            div(cls := "levelLogs active")(
              table(cls := "slist")(
                thead(
                  tr(
                    th("会员信息"),
                    th("操作时间"),
                    th("原到期时间"),
                    th("现到期时间"),
                    th("续费方式"),
                    th("备注"),
                    th("操作")
                  )
                ),
                tbody(
                  levelChangeLogs.map { log =>
                    tr(
                      td(log.desc),
                      td(momentFromNow(log.createAt)),
                      td(log.oldExpireAt.map(_.toString("yyyy-MM-dd HH:mm")) | "-"),
                      td(log.newExpireAt.map(_.toString("yyyy-MM-dd HH:mm")) | "-"),
                      td(log.typ.name),
                      td(log.note | "-"),
                      td(
                        log.typ match {
                          case MemberLevelChangeType.Buy => {
                            log.orderId.map { orderId =>
                              a(href := routes.MemberOrder.info(orderId), target := "_blank")("详情")
                            } getOrElse "-"
                          }
                          case _ => "-"
                        }
                      )
                    )
                  }
                )
              )
            ),
            div(cls := "card")(
              table(cls := "slist")(
                thead(
                  tr(
                    th("卡号"),
                    th("卡类型"),
                    th("使用期限"),
                    th("有效期至"),
                    th("操作")
                  )
                ),
                tbody(
                  cards.map { card =>
                    tr(
                      td(card.id),
                      td(card.level.name),
                      td(card.days.name),
                      td(card.expireAt.toString("yyyy-MM-dd HH:mm")),
                      td(
                        card.isAvailable option frag(
                          u.memberLevelWithExpire.gtThat(card.level) option button(cls := "button button-empty disabled", disabled)("金牌到期可用"),
                          !u.memberLevelWithExpire.gtThat(card.level) option postForm(action := routes.MemberCard.use(card.id))(
                            button(cls := "button button-empty confirm", title := "确认使用？")("使用")
                          )
                        ),
                        card.isUsed option button(cls := "button button-empty disabled", disabled)("已使用"),
                        card.expired option button(cls := "button button-empty disabled", disabled)("已过期")
                      )
                    )
                  }
                )
              )
            ),
            div(cls := "exchange")(
              table(cls := "slist")(
                thead(
                  tr(
                    th("卡号"),
                    th("兑换码"),
                    th("卡类型"),
                    th("使用期限"),
                    th("有效期至"),
                    th("使用时间")
                  )
                ),
                tbody(
                  exchangeCards.map { card =>
                    tr(
                      td(card.id),
                      td(card.code),
                      td(card.level.name),
                      td(card.days.name),
                      td(card.expireAt.toString("yyyy-MM-dd HH:mm")),
                      td(card.usedAt.map(_.toString("yyyy-MM-dd HH:mm")))
                    )
                  }
                )
              )
            ),
            div(cls := "coupon")(),
            div(cls := "points")(
              table(cls := "slist")(
                thead(
                  tr(
                    th("时间"),
                    th("信息"),
                    th("积分")
                  )
                ),
                tbody(
                  levelPointsLogs.map { log =>
                    val order = log.orderId match {
                      case Some(orderId) => levelPointsLogOrders.find(_.id == orderId)
                      case None => None
                    }
                    tr(
                      td(cls := "time")(momentFromNow(log.createAt)),
                      td(cls := "typ")(
                        log.typ match {
                          case MemberPointsLog.PointsType.OrderPay | MemberPointsLog.PointsType.OrderPayRebate =>
                            order.map { o =>
                              frag(
                                a(href := routes.MemberOrder.info(o.id), target := "_blank")(log.typ.name),
                                nbsp, "（", userIdLink(o.createBy.some, withBadge = false), "）"
                              )
                            }
                          case MemberPointsLog.PointsType.OrderInviteRebate =>
                            order.map { o =>
                              frag(
                                log.typ.name,
                                o.teamId.map(t => frag(nbsp, "（", teamLinkById(t, false), "）"))
                              )
                            }
                          case MemberPointsLog.PointsType.BackendGiven => frag(log.typ.name)
                        }
                      ),
                      td(cls := List("diff" -> true, "minus" -> (log.diff < 0)))(
                        if (log.diff < 0) { log.diff } else { "+" + log.diff }
                      )
                    )
                  }
                )
              )
            ),
            div(cls := "orders")(
              table(cls := "slist")(
                thead(
                  tr(
                    th("订单编号"),
                    th("商品描述"),
                    th("支付金额"),
                    th("订单状态"),
                    th("备注"),
                    th("创建时间"),
                    th("操作")
                  )
                ),
                tbody(
                  orders.map { order =>
                    tr(
                      td(order.id),
                      td(order.descWithCount),
                      td(label(cls := "symbol")("￥"), span(cls := "number")(order.payAmount.setScale(2, RoundingMode.DOWN).toString())),
                      td(order.status.name),
                      td(order.note | "-"),
                      td(momentFromNow(order.createAt)),
                      td(a(href := routes.MemberOrder.info(order.id), target := "_blank")("详情"))
                    )
                  }
                )
              )
            )
          )
        )
      )
    }

  def level(ml: MemberLevel, member: Member) = {
    val levelWithExpireOption = member.levels.get(ml.code)
    div(cls := List("level" -> true, s"bg-${ml.code}" -> true))(
      div(cls := "header")(
        div(cls := "title")(
          span(cls := "name")(ml.name),
          ml == member.lv option span(cls := "current")("当前"),
          levelWithExpireOption.??(_.expired) option span(cls := "expired")("已过期")
        ),
        (ml.id > member.lv.id || (ml == member.lv && !member.lvWithExpire.isForever)) option {
          a(cls := s"btn-vip ${ml.code}", href := routes.Member.toBuy(ml.code.some))(levelWithExpireOption.map(_ => "续费") | "加入")
        }
      ),
      levelWithExpireOption.map { levelWithExpire =>
        div(cls := "note")(
          "有效期至：", levelWithExpire.expireNote
        )
      } | {
        div(cls := "note")(
          span("加入", ml.name, "，享受", a(cls := "member-intro")("更多"), "功能"),
          (!member.isGoldAvailable && member.isSilverAvailable && ml == Gold) option {
            span(cls := "tip")("（", "您现在是", Silver.name, "加入时首先计算差价", "）")
          }
        )
      }
    )
  }

  def exchangeUseModal(form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content modal-exchange none")(
      h2("会员权益兑换"),
      postForm(cls := "form3", action := routes.Member.exchangeUse())(
        form3.group(form("code"), raw("请输入兑换码"), half = true)(_ =>
          div(cls := "code")(
            form3.input(form("code1"))(required, placeholder := "xxxx"),
            span(cls := "divide")("-"),
            form3.input(form("code2"))(required, placeholder := "xxxx"),
            span(cls := "divide")("-"),
            form3.input(form("code3"))(required, placeholder := "xxxx")
          )),
        div(cls := "form-group is-invalid none")(),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )

}
