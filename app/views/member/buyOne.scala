package views.html.member

import lila.api.Context
import play.api.data.{ Field, Form }
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import lila.member.{ DataForm, PayWay, Product, ProductType }
import controllers.routes

object buyOne {

  def form(
    me: User,
    product: Product,
    discounts: List[(String, String)],
    existsInviteOrder: Boolean,
    defaultItemCode: Option[String],
    form: Form[_]
  )(implicit ctx: Context) = {
    val defaultItem = product.itemList.find(_.code == defaultItemCode.getOrElse(product.defaultItem)) | product.itemList.head
    postForm(cls := "orderForm", action := routes.MemberOrder.toPay)(
      form3.hidden(name = "productTyp", value = product.typ.id),
      form3.hidden(name = "productId", value = product.id),
      table(cls := "orderForm")(
        product.itemList.size > 1 option tr(
          th("商品名称"),
          td(product.name)
        ),
        tr(
          th(product.itemName),
          td(
            div(cls := "items")(
              form3.radio2(form("itemCode"), product.itemList.map { item =>
                item.code -> item.name
              }, defaultItem.code.some)
            )
          )
        ),
        tr(
          th("单价"),
          td(
            div(cls := "price")(
              div(
                label(cls := "symbol")("￥"),
                span(cls := "number")("-.--"),
                del("[", label(cls := "symbol")("￥"), span(cls := "del-price")("-.--"), "]")
              ),
              div(cls := List("note" -> true, "none" -> defaultItem.discountDesc.isEmpty))(defaultItem.discountDesc | "")
            )
          )
        ),
        if (product.fixOne) {
          form3.hidden(name = "count", value = "1")
        } else tr(
          th("数量"),
          td(
            div(cls := "count")(
              input(tpe := "number", min := 1, max := 999, step := "1", name := "count", value := 1)
            ),
            div(cls := "countError formError")
          )
        ),
        tr(cls := List("none" -> existsInviteOrder))(
          th("邀请码"),
          td(
            div(cls := "inviteUser")(
              form3.select(form("inviteUser"), discounts, default = if (discounts.size > 1) "".some else none),
              div(cls := "minusAmount")(
                label(cls := "symbol")("优惠：￥"),
                span(cls := "number")("0.00"),
                span("，首次支付有效")
              )
            ),
            div(cls := "inviteUserError formError")
          )
        ),
        tr(
          th("积分抵扣"),
          td(
            div(cls := "points")(
              input(tpe := "number", min := 0, max := Math.min(DataForm.MaxPoints, me.memberOrDefault.points), step := "1", name := "points", value := Math.min(DataForm.MaxPoints, me.memberOrDefault.points)),
              input(tpe := "hidden", name := "isPointsChange", value := false),
              label(s"共 ${me.memberOrDefault.points} 积分（1积分=1人民币）")
            ),
            div(cls := "pointsError formError")
          )
        ),
        tr(
          th("待支付"),
          td(
            div(cls := "payPrice")(
              label(cls := "symbol")("￥"),
              span(cls := "number")("-.--"),
              del("[", label(cls := "symbol")("￥"), span(cls := "del-price")("-.--"), "]")
            )
          )
        ),
        tr(
          th("支付方式"),
          td(
            views.html.member.buyOne.payWay(form("payWay"), PayWay.choices, PayWay.Alipay.id.some),
            small(cls := "form-help")(
              "目前仅支持支付宝，支付中如果遇到问题，请更换浏览器重试。"
            )
          )
        ),
        tr(
          th,
          td(
            button(cls := "button button-green topay")("去支付")
          )
        )
      )
    )
  }

  def payWay(
    field: Field,
    options: Iterable[(String, String)],
    defaultValue: Option[String] = None
  ): Frag =
    div(cls := "radio-group payRadio")(
      options.toSeq.map {
        case (value, _) => {
          val check = field.value.fold(defaultValue)(_.some).has(value)
          div(cls := "radio")(
            st.input(
              check.option(checked),
              st.id := s"${field.name}_$value",
              tpe := "radio",
              st.name := field.name,
              st.value := value
            ),
            label(cls := "radio-label", `for` := s"${field.name}_$value")(
              img(cls := s"icon $value", `for` := s"${field.name}_$value", src := staticUrl(s"images/pay/$value.png"))
            )
          )
        }
      }
    )

}
