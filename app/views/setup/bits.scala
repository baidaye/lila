package views.html.setup

import play.api.data.{ Field, Form }
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.setup.ValidPgn
import org.joda.time.DateTime
import lila.opening.OpeningDB
import controllers.routes

private object bits {

  val prefix = "sf_"

  def fenInput(field: Field, strict: Boolean, validFen: Option[lila.setup.ValidFen])(implicit ctx: Context) = {
    val url = field.value.fold(routes.Editor.index)(routes.Editor.load).url
    div(cls := "fen_position optional_config")(
      frag(
        a(cls := "board_editor")(
          span(cls := "preview")(
            iframe(
              id := "board_iframe",
              src := routes.UserAnalysis.fenEmbed(validFen.map(_.fen.value)),
              st.frameborder := 0,
              width := "100%",
              height := "200px"
            )
          )
        ),
        div(cls := "fen_form", dataValidateUrl := s"""${routes.Setup.validateFen()}${strict.??("?strict=1")}""")(
          form3.input(field)(st.placeholder := s"${trans.pasteTheFenStringHere.txt()}（以此FEN为准）"),
          a(cls := "button button-empty", dataIcon := "m", title := trans.boardEditor.txt(), href := url)
        )
      )
    )
  }

  def pgnInput(field: Field, validPgn: Option[ValidPgn])(implicit ctx: Context) = {
    div(cls := "pgn_position optional_config")(
      frag(
        div(cls := "board_editor")(
          div(cls := "preview")(
            iframe(
              id := "board_iframe",
              src := routes.UserAnalysis.pgnEmbed(validPgn.map(_.pgn)),
              st.frameborder := 0,
              width := "100%",
              height := "200px"
            )
          )
        ),
        div(cls := "pgn_form", dataValidateUrl := s"""${routes.Setup.validatePgn()}""")(
          form3.textarea(field)(rows := 3, st.placeholder := "在此处粘贴PGN棋谱（以此PGN为准）")
        )
      )
    )
  }

  def openingInput(
    form: Form[_],
    mineOpeningdbs: List[OpeningDB],
    memberOpeningdbs: List[OpeningDB],
    visibleOpeningdbs: List[OpeningDB],
    systemOpeningdbs: List[OpeningDB]
  )(implicit ctx: Context) = {
    val dataTab = attr("data-tab")
    val openingdbId = form("openingdbId").value.orElse((mineOpeningdbs ++ memberOpeningdbs ++ visibleOpeningdbs ++ systemOpeningdbs).filter(_.notEmpty).headOption.map(_.id))
    div(cls := "opening_position optional_config")(
      div(cls := "openingdb_choose")(
        div(cls := "tabs-horiz")(
          span(dataTab := "mine", cls := "active")("我的"),
          span(dataTab := "member")("参与的"),
          span(dataTab := "visible")("已授权"),
          span(dataTab := "system")("已购")
        ),
        div(cls := "tabs-content")(
          div(cls := "list mine active")(
            openingdbs(mineOpeningdbs, openingdbId) {
              div(cls := "empty")("赶快去创建自己的开局库吧，", a(href := controllers.rt_resource.routes.OpeningDB.minePage(1))("点这里"))
            }
          ),
          div(cls := "list member")(
            openingdbs(memberOpeningdbs, openingdbId) {
              div(cls := "empty")("没有可用的开局库")
            }
          ),
          div(cls := "list visible")(
            openingdbs(visibleOpeningdbs, openingdbId) {
              div(cls := "empty")("没有可用的开局库")
            }
          ),
          div(cls := "list system")(
            openingdbs(systemOpeningdbs, openingdbId) {
              div(cls := "empty")("没有可用的开局库")
            }
          )
        )
      ),
      div(cls := "openingdb_setting")(
        table(
          tbody(
            tr(
              td("排除错误着法"),
              td(
                span(
                  input(tpe := "checkbox", st.id := s"openingdbExcludeWhiteBlunder", name := "openingdbExcludeWhiteBlunder", value := "true", form("openingdbExcludeWhiteBlunder").value.orElse("true".some).has("true") option checked),
                  label(cls := "checkbox-label", `for` := "openingdbExcludeWhiteBlunder")("白方")
                )
              ),
              td(
                span(
                  input(tpe := "checkbox", st.id := s"openingdbExcludeBlackBlunder", name := "openingdbExcludeBlackBlunder", value := "true", form("openingdbExcludeBlackBlunder").value.orElse("true".some).has("true") option checked),
                  label(cls := "checkbox-label", `for` := "openingdbExcludeBlackBlunder")("黑方")
                )
              )
            ),
            tr(
              td("排除少见着法"),
              td(
                span(
                  input(tpe := "checkbox", st.id := s"openingdbExcludeWhiteJscx", name := "openingdbExcludeWhiteJscx", value := "true", form("openingdbExcludeWhiteJscx").value.orElse("true".some).has("true") option checked),
                  label(cls := "checkbox-label", `for` := "openingdbExcludeWhiteJscx")("白方")
                )
              ),
              td(
                span(
                  input(tpe := "checkbox", st.id := s"openingdbExcludeBlackJscx", name := "openingdbExcludeBlackJscx", value := "true", form("openingdbExcludeBlackJscx").value.orElse("true".some).has("true") option checked),
                  label(cls := "checkbox-label", `for` := "openingdbExcludeBlackJscx")("黑方")
                )
              )
            ),
            tr(
              td("可以提前脱谱"),
              td(
                span(cls := "radio")(
                  st.input(
                    tpe := "radio",
                    st.id := s"openingdb_off_true",
                    st.name := "openingdbCanOff",
                    st.value := "true",
                    form("openingdbCanOff").value.has("true") option checked
                  ),
                  label(cls := "radio-label", `for` := "openingdb_off_true")("是")
                )
              ),
              td(
                span(cls := "radio")(
                  st.input(
                    tpe := "radio",
                    st.id := s"openingdb_off_false",
                    st.name := "openingdbCanOff",
                    st.value := "false",
                    form("openingdbCanOff").value.orElse("false".some).has("false") option checked
                  ),
                  label(cls := "radio-label", `for` := "openingdb_off_false")("否")
                )
              )
            )
          )
        )
      )
    )
  }

  def openingdbs(openingdbs: List[OpeningDB], checkedId: Option[String])(emptyFrag: Frag)(implicit ctx: Context) =
    if (openingdbs.exists(_.notEmpty)) {
      table(cls := "slist")(
        tbody(
          openingdbs.filter(_.notEmpty).map { openingdb =>
            val check = checkedId.has(openingdb.id)
            tr(
              td(
                span(cls := "radio")(
                  st.input(
                    check.option(checked),
                    st.id := s"openingdb_${openingdb.id}",
                    tpe := "radio",
                    st.name := "openingdbId",
                    st.value := openingdb.id
                  ),
                  label(cls := "radio-label", `for` := s"openingdb_${openingdb.id}")(openingdb.name)
                )
              ),
              td("着法数：", openingdb.nodes)
            )
          }
        )
      )
    } else emptyFrag

  def renderVariant(form: Form[_], variants: List[SelectChoice])(implicit ctx: Context) =
    div(cls := "variant label_select")(
      renderLabel(form("variant"), "开局方式"),
      renderSelect(form("variant"), variants.filter {
        case (id, _, _) => ctx.noBlind || lila.game.Game.blindModeVariants.exists(_.id.toString == id)
      })
    )

  def renderSelect(
    field: Field,
    options: Seq[SelectChoice],
    compare: (String, String) => Boolean = (a, b) => a == b,
    selectedValue: Option[String] = None
  ) = select(id := s"$prefix${field.id}", name := field.name)(
    options.map {
      case (value, name, title) => {
        val tv = if (selectedValue.isDefined) selectedValue else field.value
        option(
          st.value := value,
          st.title := title,
          tv.exists(v => compare(v, value)) option selected
        )(name, (value == "21" || value == "22") option "（会员）")
      }
    }
  )

  def renderRadios(field: Field, options: Seq[SelectChoice]) =
    st.group(cls := "radio")(
      options.map {
        case (key, name, hint) => div(
          input(
            `type` := "radio",
            id := s"$prefix${field.id}_${key}",
            st.name := field.name,
            value := key,
            field.value.has(key) option checked
          ),
          label(
            cls := "required",
            title := hint,
            `for` := s"$prefix${field.id}_$key"
          )(name)
        )
      }
    )

  def renderInput(field: Field) =
    input(name := field.name, value := field.value, `type` := "hidden")

  def renderLabel(field: Field, content: Frag) =
    label(`for` := s"$prefix${field.id}")(content)

  def renderTimeMode(form: Form[_], config: lila.setup.BaseConfig)(implicit ctx: Context) =
    div(cls := "time_mode_config optional_config")(
      div(cls := "label_select")(
        renderLabel(form("timeMode"), trans.timeControl()),
        renderSelect(form("timeMode"), translatedTimeModeChoices)
      ),
      if (ctx.blind) frag(
        div(cls := "time_choice")(
          renderLabel(form("time"), trans.minutesPerSide()),
          renderSelect(form("time"), clockTimeChoices, (a, b) => a.replace(".0", "") == b)
        ),
        div(cls := "increment_choice")(
          renderLabel(form("increment"), trans.incrementInSeconds()),
          renderSelect(form("increment"), clockIncrementChoices)
        )
      )
      else frag(
        div(cls := "time_choice slider")(
          trans.minutesPerSide(),
          ": ",
          span(chess.Clock.Config(~form("time").value.map(x => (x.toDouble * 60).toInt), 0).limitString.toString),
          renderInput(form("time"))
        ),
        div(cls := "increment_choice slider")(
          trans.incrementInSeconds(),
          ": ",
          span(form("increment").value),
          renderInput(form("increment"))
        )
      ),
      div(cls := "correspondence")(
        if (ctx.blind) div(cls := "days_choice")(
          renderLabel(form("days"), trans.daysPerTurn()),
          renderSelect(form("days"), corresDaysChoices)
        )
        else div(cls := "days_choice slider")(
          trans.daysPerTurn(),
          ": ",
          span(form("days").value),
          renderInput(form("days"))
        )
      )
    )

  def renderAppt(form: Form[_], appt: Boolean)(implicit ctx: Context) = {
    val now = DateTime.now
    div(cls := "appt_config")(
      div(cls := "label_select")(
        renderLabel(form("appt"), "自由约棋"),
        renderSelect(form("appt"), apptChoices, selectedValue = (if (appt) "1".some else "0".some))
      ),
      div(cls := "appt_pick none")(
        form3.input2(form("apptStartsAt"), vl = now.plusMinutes(5).toString("yyyy-MM-dd HH:mm").some, klass = "flatpickr")(
          dataEnableTime := true,
          datatime24h := true,
          placeholder := "比赛时间"
        )(dataMinDate := now.plusMinutes(1).toString("yyyy-MM-dd HH:mm"), dataMaxDate := (now plusWeeks 2).toString("yyyy-MM-dd HH:mm")),
        form3.textarea(form("apptMessage"), vl = "".some)(placeholder := "留言")
      )
    )
  }

  val dataRandomColorVariants =
    attr("data-random-color-variants") := lila.game.Game.variantsWhereWhiteIsBetter.map(_.id).mkString(",")

  val dataAnon = attr("data-anon")
  val dataMin = attr("data-min")
  val dataMax = attr("data-max")
  val dataValidateUrl = attr("data-validate-url")
  val dataResizable = attr("data-resizable")
  val dataCoord = attr("data-coordinates")
  val dataType = attr("data-type")
}
