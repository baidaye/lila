package views.html.analyse

import play.api.libs.json.Json
import chess.variant.Crazyhouse
import bits.dataPanel
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.Lang
import lila.common.String.html.safeJsonValue
import lila.game.Pov
import lila.contest.Board
import controllers.routes

object replay {

  private val dataSimplePgn = attr("data-simplepgn")
  private val dataGameIds = attr("data-gameids")
  private val dataSource = attr("data-source")
  private val dataRel = attr("data-rel")

  private[analyse] def titleOf(pov: Pov)(implicit lang: Lang) =
    s"${playerText(pov.game.whitePlayer)} vs ${playerText(pov.game.blackPlayer)}: ${pov.game.opening.fold(trans.analysis.txt())(_.opening.ecoName)}"

  def apply(
    pov: Pov,
    data: play.api.libs.json.JsObject,
    initialFen: Option[chess.format.FEN],
    simplePgn: String,
    pgn: String,
    analysis: Option[lila.analyse.Analysis],
    analysisStarted: Boolean,
    contest: Option[lila.hub.actorApi.contest.ContestBoard],
    contestPovs: List[lila.game.actorApi.BoardWithPov],
    markMap: Map[String, Option[String]],
    simul: Option[lila.simul.Simul],
    cross: Option[lila.game.Crosstable.WithMatchup],
    userTv: Option[lila.user.User],
    chatOption: Option[lila.chat.UserChat.Mine],
    bookmarked: Boolean
  )(implicit ctx: Context) = {

    import pov._

    val chatJson = chatOption map { c =>
      views.html.chat.json(
        c.chat,
        name = trans.spectatorRoom.txt(),
        timeout = c.timeout,
        withNote = ctx.isAuth,
        public = true,
        palantir = ctx.me.exists(_.canPalantir)
      )
    }
    val pgnLinks = div(
      a(dataIcon := "x", cls := "text", href := s"${routes.Game.exportOne(game.id)}?literate=1")(trans.downloadAnnotated()),
      a(dataIcon := "x", cls := "text", href := s"${routes.Game.exportOne(game.id)}?evals=0&clocks=0")(trans.downloadRaw()),
      game.isPgnImport option a(dataIcon := "x", cls := "text", href := s"${routes.Game.exportOne(game.id)}?imported=1")(trans.downloadImported()),
      a(dataIcon := "4", cls := "text resourceSaveTo", dataSimplePgn := simplePgn, dataSource := "game", dataRel := pov.game.id)("保存PGN", views.html.member.bits.vTip)
    //ctx.noBlind option a(dataIcon := "=", cls := "text embed-howto", target := "_blank")(trans.embedInYourWebsite())
    )

    bits.layout(
      title = titleOf(pov),
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("analyse.round"),
        pov.game.variant == Crazyhouse option cssTag("analyse.zh"),
        ctx.blind option cssTag("round.nvui")
      ),
      moreJs = frag(
        jsTag("resource.saveTo.js"),
        analyseTag,
        analyseNvuiTag,
        tagsinputTag,
        embedJsUnsafe(s"""lichess=lichess||{};lichess.analyse=${
          safeJsonValue(Json.obj(
            "data" -> data,
            "i18n" -> jsI18n(),
            "userId" -> ctx.userId,
            "chat" -> chatJson,
            "explorer" -> Json.obj(
              "endpoint" -> explorerEndpoint,
              "tablebaseEndpoint" -> tablebaseEndpoint
            )
          ))
        }""")
      ),
      openGraph = povOpenGraph(pov).some
    )(frag(
        main(cls := "analyse")(
          st.aside(cls := "analyse__side")(
            views.html.game.side(pov, initialFen, none, contest, simul = simul, userTv = userTv, bookmarked = bookmarked),
            div(cls := "game__tags")(
              input(id := "game-tags", placeholder := "添加标签")
            )
          ),
          chatOption.map(_ => views.html.chat.frag),
          div(cls := "analyse__board main-board")(chessgroundBoard),
          div(cls := "analyse__tools")(div(cls := "ceval")),
          div(cls := "analyse__controls"),
          !ctx.blind option frag(
            div(cls := "analyse__underboard")(
              div(cls := "analyse__underboard__panels")(
                div(cls := "active"),
                game.analysable option div(cls := "computer-analysis")(
                  if (analysis.isDefined || analysisStarted) div(id := "acpl-chart")
                  else postForm(
                    cls := s"future-game-analysis${ctx.isAnon ?? " must-login"}",
                    action := routes.Analyse.requestAnalysis(gameId)
                  )(
                      submitButton(cls := "button text")(
                        span(cls := "is3 text", dataIcon := "")(trans.requestAComputerAnalysis())
                      )
                    )
                ),
                div(cls := "fen-pgn")(
                  div(
                    strong("FEN"),
                    input(readonly, spellcheck := false, cls := "copyable autoselect analyse__underboard__fen")
                  ),
                  div(cls := "pgn-options")(
                    strong("PGN"),
                    pgnLinks
                  ),
                  div(cls := "pgn")(pgn)
                ),
                div(cls := "move-times")(
                  game.turns > 1 option div(id := "movetimes-chart")
                ),
                cross.map { c =>
                  div(cls := "ctable")(
                    views.html.game.crosstable(pov.player.userId.fold(c)(c.fromPov), pov.gameId.some)
                  )
                }
              ),
              div(cls := "analyse__underboard__menu")(
                game.analysable option
                  span(
                    cls := "computer-analysis",
                    dataPanel := "computer-analysis",
                    title := analysis.map { a => s"Provided by ${usernameOrId(a.providedBy)}" }
                  )(trans.computerAnalysis()),
                !game.isPgnImport option frag(
                  game.turns > 1 option span(dataPanel := "move-times")(trans.moveTimes()),
                  cross.isDefined option span(dataPanel := "ctable")(trans.crosstable())
                ),
                span(dataPanel := "fen-pgn")(raw("FEN &amp; PGN"))
              )
            ),
            contestPovs.nonEmpty option div(cls := "analyse__contestTv", dataGameIds := s"""["${contestPovs.map { _.pov.gameId }.mkString("\",\"")}"]""")(
              h2("本轮全部对局：", span(cls := "finished")(contestPovs.count(_.pov.game.finished)), "/", contestPovs.length),
              div(cls := "now-playing")(
                contestPovs.map(x => views.html.game.mini(x.pov, x.board.some, active = pov.gameId == x.pov.gameId, markMap = markMap))
              )
            )
          )
        ),
        if (ctx.blind) div(cls := "blind-content none")(
          h2("PGN downloads"),
          pgnLinks
        )
      ))
  }
}
