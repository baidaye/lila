package views.html
package auth

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import play.api.data.Form
import controllers.routes

object signupConfirm {

  def apply(user: User, cellphoneForm: Form[_], emailForm: Form[_], emailConfirmEnabled: Boolean, active: Option[String] = None)(implicit ctx: Context) =
    views.html.base.layout(
      title = "验证",
      moreJs = frag(
        smsCaptchaTag,
        jsTag("auth.js")
      ),
      moreCss = frag(
        cssTag("auth")
      )
    ) {
        main(cls := "auth auth-signup-confirm box box-pad")(
          div(cls := "tabs")(
            div(bits.dataTab := "cellphone", cls := List("active" -> (active.isEmpty || active.??(_ === "cellphone"))))("手机验证"),
            emailConfirmEnabled option { div(bits.dataTab := "email", cls := List("active" -> active.??(_ === "email")))("邮箱验证") }
          ),
          div(cls := "panels")(
            div(cls := List("panel cellphone" -> true, "active" -> (active.isEmpty || active.??(_ === "cellphone"))))(
              postForm(cls := "form3", dataSmsrv := true, attr("data-sms-signup") := true, action := routes.Auth.cellphoneConfirm)(
                views.html.base.smsCaptcha(cellphoneForm),
                form3.action(form3.submit("确认"))
              )
            ),
            emailConfirmEnabled option {
              div(cls := List("panel email" -> true, "active" -> active.??(_ === "email")))(
                postForm(cls := "form3", action := routes.Auth.emailConfirm)(
                  form3.group(emailForm("email"), "电子邮箱")(form3.input2(_, typ = "email", vl = user.email.map(_.value))(required)),
                  form3.action(form3.submit("确认"))
                )
              )
            }
          )
        )
      }
}
