package views.html.auth

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import controllers.routes

object checkYourEmail {

  def apply()(implicit ctx: Context) =
    views.html.base.layout(
      title = "查收邮件",
      moreCss = cssTag("email-confirm")
    ) {
        main(cls := s"page-small box box-pad email-confirm anim")(
          h1(cls := "is-green text", dataIcon := "E")(trans.checkYourEmail()),
          p(trans.weHaveSentYouAnEmailClickTheLink()),
          h2("没接收到邮件？"),
          ol(
            li(h3(trans.ifYouDoNotSeeTheEmailCheckOtherPlaces())),
            li(
              h3("稍等一下"), br,
              "邮件可以需要一段时间到达，这取决于您的邮箱提供商。"
            ),
            li(
              h3("始终没有收到？"), br,
              "确认邮箱地址是正确的吗？", br,
              "您等待超过5分钟了码？", br,
              "如果是这样，",
              a(href := routes.Auth.emailConfirmHelp)("请前往这个页面进行处理")
            )
          )
        )
      }
}
