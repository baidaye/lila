package views.html
package auth

import play.api.data.Form

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

import controllers.routes

object login {

  def apply(usernameForm: Form[_], cellphoneForm: Form[_], referrer: Option[String], active: Option[String] = None)(implicit ctx: Context) = views.html.base.layout(
    title = trans.signIn.txt(),
    moreJs = frag(
      smsCaptchaTag,
      jsTag("auth.js")
    ),
    moreCss = cssTag("auth")
  ) {
      main(cls := "auth auth-login box box-pad")(
        h1(trans.signIn()),
        div(cls := "tabs")(
          div(bits.dataTab := "username", cls := List("active" -> (active.isEmpty || active.??(_ === "username"))))("账号密码"),
          div(bits.dataTab := "cellphone", cls := List("active" -> active.??(_ === "cellphone")))("手机验证")
        ),
        div(cls := "panels")(
          div(cls := List("panel username" -> true, "active" -> (active.isEmpty || active.??(_ === "username"))))(
            postForm(
              cls := "form3",
              action := s"${routes.Auth.authenticateByUsername}${referrer.?? { ref => s"?referrer=${java.net.URLEncoder.encode(ref, "US-ASCII")}" }}"
            )(
                form3.globalError(usernameForm),
                auth.bits.formFields(usernameForm("username"), usernameForm("password")),
                form3.submit(trans.signIn())
              )
          ),
          div(cls := List("panel cellphone" -> true, "active" -> active.??(_ === "cellphone")))(
            postForm(cls := "form3", dataSmsrv := false, dataSmsev := true,
              action := s"${routes.Auth.authenticateByCellphone}${referrer.?? { ref => s"?referrer=${java.net.URLEncoder.encode(ref, "US-ASCII")}" }}")(
                views.html.base.smsCaptcha(cellphoneForm),
                form3.submit(trans.signIn())
              )
          )
        ),
        div(cls := "alternative")(
          a(href := s"${routes.Auth.signup()}${referrer.?? { ref => s"?referrer=${java.net.URLEncoder.encode(ref, "US-ASCII")}" }}")(trans.signUp()),
          a(href := routes.Auth.passwordReset())(trans.passwordReset())
        )
      )
    }
}
