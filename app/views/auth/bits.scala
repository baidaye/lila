package views.html
package auth

import play.api.data.{ Form, Field }

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User

import controllers.routes

object bits {

  val dataTab = attr("data-tab")

  def formFields(username: Field, password: Field)(implicit ctx: Context) = frag(
    form3.group(username, trans.usernameOrEmail()) { f =>
      frag(
        form3.input(f)(autofocus, required),
        p(cls := "error exists none")(trans.usernameAlreadyUsed())
      )
    },
    form3.password(password, trans.password())
  )

  def passwordReset(cellphoneWithCaptcha: (Form[_], lila.common.Captcha), emailWithCaptcha: (Form[_], lila.common.Captcha), active: Option[String] = None, ok: Option[Boolean] = None)(implicit ctx: Context) =
    views.html.base.layout(
      title = trans.passwordReset.txt(),
      moreCss = cssTag("auth"),
      moreJs = frag(
        captchaTag,
        smsCaptchaTag,
        jsTag("auth.js")
      )
    ) {

        main(cls := "auth auth-signup box box-pad")(
          h1(
            ok.map { r => span(cls := (if (r) "is-green" else "is-red"), dataIcon := (if (r) "E" else "L")) },
            trans.passwordReset()
          ),
          div(cls := "tabs")(
            div(bits.dataTab := "cellphone", cls := List("active" -> (active.isEmpty || active.??(_ === "cellphone"))))("手机验证"),
            div(bits.dataTab := "email", cls := List("active" -> active.??(_ === "email")))("邮箱验证")
          ),
          div(cls := "panels")(
            div(cls := List("panel cellphone" -> true, "active" -> (active.isEmpty || active.??(_ === "cellphone"))))(
              postForm(cls := "form3", dataSmsrv := false, dataSmsev := true, action := routes.Auth.passwordResetByCellphoneApply)(
                views.html.base.smsCaptcha(cellphoneWithCaptcha._1),
                views.html.base.captcha(cellphoneWithCaptcha._1, cellphoneWithCaptcha._2),
                form3.action(form3.submit("重置密码"))
              )
            ),
            div(cls := List("panel email" -> true, "active" -> active.??(_ === "email")))(
              postForm(cls := "form3", action := routes.Auth.passwordResetByEmailApply)(
                form3.group(emailWithCaptcha._1("email"), trans.email())(form3.input(_, typ = "email")(autofocus)),
                views.html.base.captcha(emailWithCaptcha._1, emailWithCaptcha._2),
                form3.action(form3.submit(trans.emailMeALink()))
              )
            )
          )
        )
      }

  def passwordResetByEmailSent(email: String)(implicit ctx: Context) =
    views.html.base.layout(
      title = trans.passwordReset.txt()
    ) {
        main(cls := "page-small box box-pad")(
          h1(cls := "is-green text", dataIcon := "E")(trans.checkYourEmail()),
          p(trans.weHaveSentYouAnEmailTo(email)),
          p(trans.ifYouDoNotSeeTheEmailCheckOtherPlaces())
        )
      }

  def passwordResetConfirm(u: User, token: String, form: Form[_], ok: Option[Boolean] = None)(implicit ctx: Context) =
    views.html.base.layout(
      title = s"${u.username} - ${trans.changePassword.txt()}",
      moreCss = cssTag("form3")
    ) {
        main(cls := "page-small box box-pad")(
          (ok match {
            case Some(true) => h1(cls := "is-green text", dataIcon := "E")
            case Some(false) => h1(cls := "is-red text", dataIcon := "L")
            case _ => h1
          })(u.username, " - ", trans.changePassword()),
          postForm(cls := "form3", action := routes.Auth.passwordResetConfirmApply(token))(
            form3.hidden(form("token")),
            form3.passwordModified(form("newPasswd1"), trans.newPassword())(autofocus),
            form3.password(form("newPasswd2"), trans.newPasswordAgain()),
            form3.globalError(form),
            form3.action(form3.submit(trans.changePassword()))
          )
        )
      }

  def tor()(implicit ctx: Context) =
    views.html.base.layout(
      title = "Tor exit node"
    ) {
      main(cls := "page-small box box-pad")(
        h1(cls := "text", dataIcon := "2")("Ooops"),
        p("对不起，你不能通过TOR注册海棋！"),
        p("作为匿名用户，您可以对局、战术训练和使用所有Hachess功能。")
      )
    }
}
