package views.html.olclass

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json._

object show {

  def apply(
    title: String,
    sdkAppId: Long,
    userSig: String,
    tagTypes: JsArray,
    olClassJson: JsObject,
    courseWareJson: JsArray,
    prefJson: JsObject,
    roundJson: JsObject,
    clazzJson: JsObject,
    courseJson: JsObject,
    coachJson: JsObject,
    studentsJson: JsArray,
    chatOption: Option[lila.chat.UserChat.Mine]
  )(implicit ctx: Context) = {
    views.html.base.layout(
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("olclass.show")
      ),
      moreJs = frag(
        tagsinputTag,
        jsTag("resource.saveTo.js"),
        jsAt(s"compiled/lichess.olclass${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.olclass=${
          safeJsonValue(Json.obj(
            "i18n" -> i18nJsObject(translations),
            "userId" -> ctx.userId,
            "sdkAppId" -> sdkAppId,
            "userSig" -> userSig,
            "tagTypes" -> tagTypes,
            "olClass" -> olClassJson,
            "courseWares" -> courseWareJson,
            "pref" -> prefJson,
            "round" -> roundJson,
            "clazz" -> clazzJson,
            "course" -> courseJson,
            "coach" -> coachJson,
            "students" -> studentsJson,
            "chat" -> chatOption.map { c =>
              views.html.chat.json(c.chat, name = "聊天互动", timeout = c.timeout, public = true)
            },
            "explorer" -> Json.obj(
              "endpoint" -> explorerEndpoint,
              "tablebaseEndpoint" -> tablebaseEndpoint
            )
          ))
        }""")
      ),
      title = title,
      zoomable = true
    ) {
        main(cls := "olclass")
      }
  }

  private val translations = List(
    // also uses gameOver
    trans.depthX,
    trans.usingServerAnalysis,
    trans.loadingEngine,
    trans.cloudAnalysis,
    trans.goDeeper,
    trans.showThreat,
    trans.inLocalBrowser,
    trans.toggleLocalEvaluation,
    // ceval menu
    trans.computerAnalysis,
    trans.enable,
    trans.bestMoveArrow,
    trans.evaluationGauge,
    trans.infiniteAnalysis,
    trans.removesTheDepthLimit,
    trans.multipleLines,
    trans.cpus,
    trans.memory,
    // explorer (also uses gameOver, checkmate, stalemate, draw, variantEnding)
    trans.openingExplorerAndTablebase,
    trans.openingExplorer,
    trans.xOpeningExplorer,
    trans.move,
    trans.games,
    trans.variantLoss,
    trans.variantWin,
    trans.insufficientMaterial,
    trans.capture,
    trans.pawnMove,
    trans.close,
    trans.winning,
    trans.unknown,
    trans.losing,
    trans.drawn,
    trans.timeControl,
    trans.averageElo,
    trans.database,
    trans.recentGames,
    trans.topGames,
    trans.whiteDrawBlack,
    trans.averageRatingX,
    trans.masterDbExplanation,
    trans.mateInXHalfMoves,
    trans.nextCaptureOrPawnMoveInXHalfMoves,
    trans.noGameFound,
    trans.maybeIncludeMoreGamesFromThePreferencesMenu,
    trans.winPreventedBy50MoveRule,
    trans.lossSavedBy50MoveRule,
    trans.allSet
  )

}

