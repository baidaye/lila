package views.html.distinguish

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.data.Form
import lila.distinguish.{ DataForm, Distinguish }
import org.joda.time.DateTime
import lila.common.String.html.richText
import controllers.routes

object modal {

  def createForm(form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content distinguish-create none")(
      h2("新建棋谱记录"),
      postForm(cls := "form3")(
        form3.hidden("tab", "classic"),
        form3.group(form("name"), raw("名称"))(form3.input2(_, s"棋谱记录 ${DateTime.now.toString("yyyy.MM.dd HH:mm")}".some)),
        form3.split(
          form3.group(form("orientation"), raw("棋盘方向"), half = true)(form3.select(_, DataForm.colorChoices)),
          form3.group(form("turns"), raw("走棋步数"), half = true)(form3.input(_, typ = "number")(placeholder := "不填写表示所有走棋"))
        ),
        div(cls := "tabs-horiz")(
          span(cls := "classic active")("经典对局"),
          span(cls := "gamedb")("选择对局"),
          span(cls := "pgn")("PGN"),
          span(cls := "chapter")("研习章节链接"),
          span(cls := "game")("对局链接")
        ),
        div(cls := "tabs-content")(
          div(cls := "classic active")(
            table(cls := "games")(
              tbody(
                Distinguish.Classic.classics.zipWithIndex.map {
                  case (g, index) => {
                    tr(
                      td(s"${index + 1}."),
                      td(label(`for` := g.gameId)(richText(g.title))),
                      td(
                        st.input(
                          tpe := "radio",
                          id := g.gameId,
                          st.name := "classic",
                          st.value := g.gameId
                        )
                      )
                    )
                  }
                }
              )
            )
          ),
          div(cls := "gamedb")(
            form3.hidden(form("gamedb")),
            div(cls := "search")(
              input(st.name := "q", st.placeholder := "搜索")
            ),
            div(cls := "dbtree")
          ),
          div(cls := "pgn")(
            form3.textarea(form("pgn"))(rows := 5, placeholder := "粘贴PGN文本"),
            form3.group(form("pgnFile"), raw("上传PGN文件"), klass = "upload") { f =>
              form3.file.pgn(f.name)
            }
          ),
          div(cls := "chapter")(
            form3.input(form("chapter"))(placeholder := "章节URL"),
            div("例：https://haichess.com/study/eKF8yUkP/mJyWJTu6")
          ),
          div(cls := "game")(
            form3.input(form("game"))(placeholder := "对局URL"),
            div("例：https://haichess.com/XTtL9RRY"),
            div("或：https://haichess.com/XTtL9RRY/white")
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )

  def editForm(distinguish: Distinguish, goTo: String, form: Form[_])(implicit ctx: Context) =
    div(cls := "modal-content distinguish-edit none")(
      h2("编辑棋谱记录"),
      postForm(cls := "form3", action := routes.Distinguish.update(distinguish.id, goTo))(
        form3.group(form("name"), raw("名称"))(form3.input(_)),
        form3.split(
          form3.group(form("orientation"), raw("棋盘方向"), half = true)(form3.select(_, DataForm.colorChoices)),
          form3.group(form("turns"), raw("走棋步数"), half = true)(form3.input(_, typ = "number")(placeholder := "不填写表示所有回走棋"))
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
}
