package views.html.distinguish

import lila.api.Context
import play.api.libs.json.{ JsObject, Json }
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.data.Form
import controllers.routes

object show {

  def apply(
    data: JsObject,
    pref: JsObject,
    home: Boolean = false,
    create: Option[Boolean] = None,
    tab: Option[String] = None,
    gameId: Option[String] = None,
    fr: Option[String] = None
  )(implicit ctx: Context) = views.html.base.layout(
    title = "棋谱记录",
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("distinguish")
    ),
    moreJs = frag(
      jsAt(s"compiled/lichess.distinguish${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""lichess=lichess||{};lichess.distinguish=${
        safeJsonValue(Json.obj(
          "userId" -> ctx.userId,
          "home" -> home,
          "create" -> create,
          "tab" -> tab,
          "gameId" -> gameId,
          "data" -> data,
          "pref" -> pref,
          "fr" -> fr
        ))
      }""")
    ),
    chessground = false,
    zoomable = true
  ) {
      main(cls := "distinguish")(
        st.aside(cls := "distinguish__side"),
        div(cls := "distinguish__board main-board")(chessgroundBoard),
        div(cls := "distinguish__tools"),
        div(cls := "distinguish__controls")
      )
    }

}
