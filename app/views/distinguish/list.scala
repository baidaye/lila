package views.html.distinguish

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.distinguish.Distinguish
import controllers.routes

object list {

  def apply(pager: Paginator[Distinguish])(implicit ctx: Context) =
    views.html.base.layout(
      title = "棋谱记录记录",
      moreCss = cssTag("distinguish.list"),
      moreJs = infiniteScrollTag
    ) {
        main(cls := "box page-small")(
          h1("棋谱记录"),
          table(cls := "slist")(
            thead(
              tr(
                th("名称"),
                th("棋色"),
                th("走棋步数"),
                th("创建日期")
              )
            ),
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNextTable(pager, np => routes.Distinguish.page(np).url),
                pager.currentPageResults.map { distinguish =>
                  tr(cls := "paginated")(
                    td(a(href := routes.Distinguish.show(distinguish.id))(distinguish.name)),
                    td(distinguish.orientation.map(_.fold("白棋", "黑棋")) | "双方"),
                    td(distinguish.turns.map(_.toString) | "所有"),
                    td(momentFromNow(distinguish.createAt))
                  )
                }
              )
            } else {
              tbody(
                tr(
                  td(colspan := 4)("暂无记录")
                )
              )
            }
          )
        )
      }

}
