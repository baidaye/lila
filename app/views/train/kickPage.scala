package views.html.train

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

object kickPage {

  def apply()(implicit ctx: Context) =
    views.html.base.layout(title = "访问被拒绝") {
      main(cls := "box box-pad")(
        h1("访问权限不足"),
        p("您不在训练课学员列表中，或已被移除。")
      )
    }

}
