package views.html.train

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsObject, Json, JsArray }
import lila.train.TrainCourse

object showStudent {

  def apply(
    trainCourse: TrainCourse,
    trainCourseJson: JsObject,
    chatOption: Option[lila.chat.UserChat.Mine],
    signed: Boolean,
    taskJson: JsArray
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = trainCourse.name,
      moreCss = cssTag("trainCourseStudent"),
      moreJs = frag(
        jsAt(s"compiled/lichess.trainCourseStudent${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.trainCourseStudent=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "trainCourse" -> trainCourseJson,
            "tasks" -> taskJson,
            "chat" -> chatOption.map { c =>
              views.html.chat.json(c.chat, name = "聊天互动", timeout = c.timeout, public = true)
            },
            "signed" -> signed
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "trainCourse-student")
      }

}
