package views.html.train

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.train.TrainCourse
import lila.resource.ThemeQuery
import play.api.libs.json.{ JsArray, JsObject, Json }

object showCoach {

  def apply(
    trainCourse: TrainCourse,
    trainCourseJson: JsObject,
    teamJson: Option[JsObject],
    studentsJson: JsArray,
    chatOption: Option[lila.chat.UserChat.Mine],
    trainGameJson: JsArray,
    clazzJson: JsArray,
    classicGameJson: JsArray
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = trainCourse.name,
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("ttask"),
        cssTag("trainCourseCoach")
      ),
      moreJs = frag(
        jsAt(s"compiled/lichess.trainCourseCoach${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.trainCourseCoach=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
            "trainCourse" -> trainCourseJson,
            "clazzes" -> clazzJson,
            "students" -> studentsJson,
            "chat" -> chatOption.map { c =>
              views.html.chat.json(c.chat, name = "聊天互动", timeout = c.timeout, public = true)
            },
            "themePuzzle" -> views.html.task.bits.themePuzzleJson(),
            "gameTasks" -> trainGameJson,
            "classicGames" -> classicGameJson,
            "team" -> teamJson
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "trainCourse-coach")
      }

}
