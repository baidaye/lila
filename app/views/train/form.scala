package views.html.train

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.coach.StudentWithUser
import play.api.data.Form
import lila.clazz.Clazz
import lila.user.User
import play.api.mvc.Call
import lila.train.TrainCourse
import org.joda.time.DateTime
import controllers.rt_trainCourse.routes

object form {

  def remove(id: String)(implicit ctx: Context) = frag(
    div(cls := "modal-content modal-train-remove none")(
      h2("删除课程"),
      postForm(cls := "form3", action := routes.TrainCourse.remove(id))(
        h3("说明：删除课程后不可恢复，是否继续？"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("删除", klass = "small")
        )
      )
    )
  )

  def create(
    form: Form[_],
    clazzes: List[Clazz],
    allStudents: List[StudentWithUser],
    trainStudents: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    fm("新建训练课", routes.TrainCourse.create(), form, clazzes, allStudents, trainStudents, markMap)

  def update(
    tc: TrainCourse,
    form: Form[_],
    clazzes: List[Clazz],
    allStudents: List[StudentWithUser],
    trainStudents: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    fm("修改训练课", routes.TrainCourse.update(tc.id), form, clazzes, allStudents, trainStudents, markMap)

  def fm(
    title: String,
    call: Call,
    form: Form[_],
    clazzes: List[Clazz],
    allStudents: List[StudentWithUser],
    trainStudents: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    val ids = trainStudents.map(_.id)
    frag(
      div(cls := "modal-content modal-train-fm none")(
        h2(title),
        postForm(cls := "form3", action := call)(
          form3.split(
            form3.group(form("name"), raw("名称"), half = true)(form3.input(_)(required)),
            form3.group(form("max"), raw("人数限制"), half = true)(
              form3.input(_, typ = "number")
            )
          ),
          form3.split(
            form3.group(form("startAt"), raw("开始时间"), half = true)(
              form3.input(_, klass = "flatpickr")(dataEnableTime := true, datatime24h := true, dataMinDate := DateTime.now.toString("yyyy-MM-dd HH:mm"))
            ),
            form3.group(form("duration"), raw("持续时间"), half = true)(
              form3.select(_, lila.train.DataForm.duration)
            )
          ),
          form3.hidden("studentIds", ids.mkString(",")),
          div(cls := "player-transfer")(
            form3.split(
              div(cls := "form-group form-half")(
                st.select(cls := "form-control clazz-select")(
                  option(value := "all")("全部"),
                  option(value := "none", dataAttr := s"""["${clazzes.flatMap(_.studentIds).distinct.mkString("\",\"")}"]""")("无班级"),
                  clazzes.map { clazz =>
                    option(value := clazz.id, dataAttr := s"""["${clazz.studentIds.mkString("\",\"")}"]""")(clazz.name)
                  }
                )
              )
            ),
            views.html.base.transfer[StudentWithUser, User](allStudents.filter(s => !ids.contains(s.userId)), trainStudents, chkAll = true) { left =>
              tr(
                td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.userId, dataId := left.userId)),
                td(cls := "name")(userMark(left.user, markMap))
              )
            } { right =>
              tr(
                td(input(tpe := "checkbox", id := s"chk_${right.id}", value := right.id, dataId := right.id)),
                td(cls := "name")(userMark(right, markMap))
              )
            }
          ),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )
  }

  private def userMark(u: User, markMap: Map[String, Option[String]]): String = {
    markMap.get(u.id).fold(none[String]) { m => m } | u.realNameOrUsername
  }

}
