package views.html
package study

import play.api.mvc.Call
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.user.User
import lila.study.Order
import lila.study.Study.WithChaptersAndLiked
import play.api.data.{ Field, Form }
import controllers.routes

object list {

  def all(pag: Paginator[WithChaptersAndLiked], order: Order, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = s"所有研习",
    active = "all",
    order = order,
    pag = pag,
    url = o => routes.Study.all(o),
    form = form,
    tags = tags
  )("所有研习")

  def byOwner(pag: Paginator[WithChaptersAndLiked], order: Order, owner: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = s"被${owner.titleUsername}创建的研习",
    active = "owner",
    order = order,
    pag = pag,
    url = o => routes.Study.byOwner(owner.username, o),
    form = form,
    tags = tags
  )(frag(userLink(owner), "的研习"))

  def mine(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = s"我的研习",
    active = "mine",
    order = order,
    pag = pag,
    url = o => routes.Study.mine(o),
    form = form,
    tags = tags
  )("我的研习")

  def mineFavorites(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = "收藏的研习",
    active = "favorites",
    order = order,
    pag = pag,
    url = o => routes.Study.mineFavorites(o),
    form = form,
    tags = tags
  )("收藏的研习")

  def mineMember(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = s"参与的研习",
    active = "member",
    order = order,
    pag = pag,
    url = o => routes.Study.mineMember(o),
    form = form,
    tags = tags
  )("参与的研习")

  def minePublic(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = "我的公共",
    active = "public",
    order = order,
    pag = pag,
    url = o => routes.Study.minePublic(o),
    form = form,
    tags = tags
  )("我的公共")

  def minePrivate(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = "我的私有",
    active = "private",
    order = order,
    pag = pag,
    url = o => routes.Study.minePrivate(o),
    form = form,
    tags = tags
  )("我的私有")

  def mineAuthTo(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = "我的授权",
    active = "authTo",
    order = order,
    pag = pag,
    url = o => routes.Study.mineAuthTo(o),
    form = form,
    tags = tags
  )("我的授权")

  def mineAuth(pag: Paginator[WithChaptersAndLiked], order: Order, me: User, form: Form[_], tags: Set[String])(implicit ctx: Context) = layout(
    title = "被授权的",
    active = "auth",
    order = order,
    pag = pag,
    url = o => routes.Study.mineAuth(o),
    form = form,
    tags = tags
  )("被授权的")

  def search(active: String, pag: Paginator[WithChaptersAndLiked], text: String)(implicit ctx: Context) =
    views.html.base.layout(
      title = text,
      moreCss = cssTag("study.index"),
      wrapClass = "full-screen-force",
      moreJs = infiniteScrollTag
    ) {
        main(cls := "page-menu")(
          menu(active, Order.default),
          main(cls := "page-menu__content study-index box")(
            div(cls := "box__top")(
              div(cls := "box__top-main")(
                searchForm("所有研习", text, active),
                bits.newForm()
              )
            ),
            paginate(pag, routes.Study.search(text))
          )
        )
      }

  private[study] def paginate(pager: Paginator[WithChaptersAndLiked], url: Call)(implicit ctx: Context) =
    if (pager.currentPageResults.isEmpty) div(cls := "nostudies")(
      iconTag("4"),
      p("没有更多了")
    )
    else div(cls := "studies list infinitescroll")(
      pager.currentPageResults.map { s =>
        div(cls := "study paginated")(bits.widget(s))
      },
      pagerNext(pager, np => addQueryParameter(url.url, "page", np))
    )

  private[study] def menu(active: String, order: Order)(implicit ctx: Context) =
    st.aside(cls := "page-menu__menu subnav")(
      a(cls := active.active("all"), href := routes.Study.all(order.key))("所有研习"),
      ctx.me.map { bits.authLinks(_, active, order) }
    )

  private[study] def searchForm(placeholder: String, value: String, active: String) =
    form(cls := "search", action := routes.Study.search(), method := "get")(
      input(name := "q", st.placeholder := placeholder, st.value := value),
      input(name := "channel", st.`type` := "hidden", st.value := active),
      submitButton(cls := "button", dataIcon := "y")
    )

  private def layout(
    title: String,
    active: String,
    order: Order,
    pag: Paginator[WithChaptersAndLiked],
    url: controllers.Study.ListUrl,
    form: Form[_],
    tags: Set[String]
  )(titleFrag: Frag)(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreCss = cssTag("study.index"),
    wrapClass = "full-screen-force",
    moreJs = frag(
      infiniteScrollTag,
      jsTag("study.index.js")
    )
  ) {
      main(cls := "page-menu")(
        menu(active, order),
        main(cls := "page-menu__content study-index box")(
          div(cls := "box__top")(
            div(cls := "box__top-main")(
              searchForm(title, "", active),
              views.html.base.bits.mselect(
                "orders",
                span(order.name),
                (if (active == "all") Order.allButOldest else Order.all) map { o =>
                  a(href := url(o.key), cls := (order == o).option("current"))(o.name)
                }
              ),
              bits.newForm()
            ),
            tags.nonEmpty option st.form(cls := "box__top-tags tag-groups", action := url(order.key), method := "get")(
              emptyTag(form),
              form3.tags(form, "tags", tags)
            )
          ),
          paginate(pag, url(order.key))
        )
      )
    }

  def emptyTag(form: Form[_]) = {
    val tName = "emptyTag"
    val tLabel = "无标签"
    div(cls := "tag-group emptyTag")(
      span(
        st.input(
          st.id := tName,
          name := tName,
          tpe := "checkbox",
          value := form(tName).value,
          form(tName).value.contains("on") option checked
        ),
        label(`for` := tName)(tLabel)
      )
    )
  }
}
