package views.html
package study

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User

import controllers.routes

object bits {

  def newButton()(implicit ctx: Context) = ctx.isAuth option
    postForm(cls := "new-study", action := routes.Study.create)(
      submitButton(cls := "button")("创建研习")
    )

  def newForm()(implicit ctx: Context) =
    postForm(cls := "new-study", action := routes.Study.create)(
      submitButton(cls := "button button-green", dataIcon := "O", title := "创建研习")
    )

  def authLinks(me: User, active: String, order: lila.study.Order)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("auth"), href := routes.Study.mineAuth(order.key))("被授权的"),
      a(activeCls("mine"), href := routes.Study.mine(order.key))("我的研习"),
      a(activeCls("public"), href := routes.Study.minePublic(order.key))("我的公共"),
      a(activeCls("authTo"), href := routes.Study.mineAuthTo(order.key))("我的授权"),
      a(activeCls("private"), href := routes.Study.minePrivate(order.key))("我的私有"),
      a(activeCls("member"), href := routes.Study.mineMember(order.key))("参与的研习"),
      a(activeCls("favorites"), href := routes.Study.mineFavorites(order.key))("收藏的研习")
    )
  }

  def widget(s: lila.study.Study.WithChaptersAndLiked, tag: Tag = h2)(implicit ctx: Context) = frag(
    a(cls := "overlay", href := routes.Study.show(s.study.id.value)),
    div(cls := "top", dataIcon := "4")(
      div(
        tag(cls := "study-name")(s.study.name.value),
        span(
          !s.study.isPublic option frag(
            iconTag("a")(cls := "private", ariaTitle("Private")),
            " "
          ),
          iconTag(if (s.liked) "" else ""),
          " ",
          s.study.likes.value,
          " • ",
          usernameOrId(s.study.ownerId),
          " • ",
          momentFromNow(s.study.createdAt)
        )
      )
    ),
    div(cls := "body")(
      ol(cls := "chapters")(
        s.chapters.map { name =>
          li(cls := "text", dataIcon := "K")(name.value)
        }
      ),
      ol(cls := "members")(
        s.study.members.members.values.take(4).map { m =>
          li(cls := "text", dataIcon := (if (m.canContribute) "" else "v"))(usernameOrId(m.id))
        } toList
      )
    )
  )

  def streamers(streams: List[lila.streamer.Stream]) =
    streams.nonEmpty option div(cls := "streamers none")(
      streams.map { s =>
        views.html.streamer.bits.contextual(s.streamer.userId)
      }
    )
}
