package views.html.study

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.study.{ Chapter, Study }
import controllers.routes

object cloneChapter {

  private def studyButton(s: Study.IdName) =
    submitButton(name := "as", value := s.id.value, cls := "submit button")(s.name.value)

  def apply(study: Study, chapter: Chapter, owner: List[Study.IdName], contrib: List[Study.IdName])(implicit ctx: Context) =
    views.html.site.message(
      title = s"复制 ${chapter.name.value} 到",
      icon = Some("4"),
      back = true,
      moreCss = cssTag("study.create").some
    ) {
        div(cls := "study-create")(
          postForm(action := routes.Study.cloneChapterApply(study.id.value, chapter.id.value))(
            p(submitButton(name := "as", value := "study", cls := "submit button large new text", dataIcon := "4")("创建研习")),
            div(cls := "studies")(
              div(
                h2("我的研习"),
                owner map studyButton
              ),
              div(
                h2("我参与的研习"),
                contrib map studyButton
              )
            )
          )
        )
      }
}
