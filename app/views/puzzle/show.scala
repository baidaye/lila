package views.html.puzzle

import play.api.libs.json.{ JsObject, Json }
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.resource.ThemeQuery
import lila.puzzle.{ ThemeRound, ThemeShow }
import lila.hub.PuzzleHub
import controllers.routes

object show {

  def apply(
    puzzle: lila.puzzle.Puzzle,
    data: JsObject,
    pref: JsObject,
    themeShow: Option[ThemeShow],
    near10Themes: Option[List[ThemeRound]],
    notAccept: Boolean = false,
    fr: Option[String] = None
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = trans.training.txt(),
      moreCss = cssTag("puzzle"),
      moreJs = frag(
        jsTag("vendor/sparkline.min.js"),
        tagsinputTag,
        drawerTag,
        jsAt(s"compiled/lichess.puzzle${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""
lichess = lichess || {};
lichess.puzzle = ${
          safeJsonValue(Json.obj(
            "data" -> data.add("notAccept" -> notAccept),
            "pref" -> pref,
            "i18n" -> bits.jsI18n(),
            "fr" -> fr
          ))
        }""")
      ),
      chessground = false,
      openGraph = lila.app.ui.OpenGraph(
        image = cdnUrl(routes.Export.puzzlePng(puzzle.id).url).some,
        title = s"Chess tactic #${puzzle.id} - ${puzzle.color.name.capitalize} to play",
        url = s"$netBaseUrl${routes.Puzzle.show(puzzle.id).url}",
        description = s"Haichess tactic trainer: " + puzzle.color.fold(
          trans.findTheBestMoveForWhite,
          trans.findTheBestMoveForBlack
        ).txt() + s" Played by ${puzzle.attempts} players."
      ).some,
      zoomable = true
    ) {
        frag(
          themeShow.isDefined option themeSearch(themeShow, near10Themes),
          main(cls := "puzzle")(
            st.aside(cls := "puzzle__side")(
              div(cls := "puzzle__side__metas")(spinner)
            ),
            div(cls := "puzzle__board main-board")(chessgroundBoard),
            div(cls := "puzzle__tools"),
            div(cls := "puzzle__controls"),
            div(cls := "puzzle__history")
          )
        )
      }

  def themeSearch(themeShow: Option[ThemeShow], near10ThemesOption: Option[List[ThemeRound]])(implicit ctx: Context): Frag =
    themeShow map { ts =>
      drawer(ts.showDrawer, "主题搜索") {
        st.form(
          cls := "search_form",
          action := s"${routes.Puzzle.themePuzzle(ts.history.map(_.puzzleId) | 100000, false, true, true)}#results",
          method := "GET"
        )(
            form3.hidden("next", "true"),
            form3.hidden("saveQuery", "true"),
            form3.hidden("showDrawer", "false"),
            ts.rnf option div(cls := "theme-rnf")("没有搜索到，换个主题吧~"),
            near10ThemesOption.map { near10Themes =>
              near10Themes.nonEmpty option div(cls := "search-his")(
                st.select(st.id := "", cls := "form-control")(
                  option("- 历史搜索记录 -"),
                  near10Themes.map { theme =>
                    option(value := theme.search)(theme.toTags.mkString("，"))
                  },
                  option(disabled)(if (near10Themes.nonEmpty) "- 最多显示近十条 -" else "- 无搜索记录 -")
                )
              )
            },
            table(
              tr(
                th(label("难度范围")),
                td(
                  div(cls := "half")("从 ", form3.input(ts.searchForm("ratingMin"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}")),
                  div(cls := "half")("到 ", form3.input(ts.searchForm("ratingMax"), "number")(st.placeholder := s"${PuzzleHub.minRating} ~ ${PuzzleHub.maxRating}"))
                )
              ),
              tr(
                th(label("答案步数")),
                td(
                  div(cls := "half")("从 ", form3.input(ts.searchForm("stepsMin"), "number")(st.placeholder := "1 ~ 10")),
                  div(cls := "half")("到 ", form3.input(ts.searchForm("stepsMax"), "number")(st.placeholder := "1 ~ 10"))
                )
              ),
              tr(
                th(label("黑白")),
                td(
                  form3.tagsWithKv(ts.searchForm, "pieceColor", ThemeQuery.pieceColor)
                )
              ),
              tr(
                th(label("阶段")),
                td(
                  form3.tagsWithKv(ts.searchForm, "phase", ThemeQuery.phase)
                )
              ),
              tr(
                th(label("目的")),
                td(
                  form3.tagsWithKv(ts.searchForm, "moveFor", ThemeQuery.moveFor)
                )
              ),
              tr(
                th(label("子力")),
                td(
                  form3.tagsWithKv(ts.searchForm, "strength", ThemeQuery.strength)
                )
              ),
              tr(
                th(label("局面")),
                td(
                  form3.tagsWithKv(ts.searchForm, "chessGame", ThemeQuery.chessGame)
                )
              ),
              tr(
                th(label("技战术")),
                td(
                  form3.tagsWithKv(ts.searchForm, "subject", ThemeQuery.subject)
                )
              ),
              tr(
                th(label("综合")),
                td(
                  form3.tagsWithKv(ts.searchForm, "comprehensive", ThemeQuery.comprehensive)
                )
              ) /*,
              tr(
                th(label("标签")),
                td(
                  form3.tags(ts.searchForm, "tags", ts.markTags)
                )
              )*/
            ),
            /*            div(cls := "themeHistory")(
              h2("做题历史"),
              table(
                tbody(
                  ts.history map { record =>
                    tr(
                      td(cls := "tags")(
                        record.toTags map { tg =>
                          span(tg)
                        }
                      ),
                      td(cls := "actions")(
                        a(cls := "continue", dataHref := routes.Puzzle.themePuzzleHistoryUri(record.id))("继续"),
                        a(cls := "restart", dataHref := routes.Puzzle.themePuzzleHistoryUri(record.id))("重开"),
                        a(cls := "remove", dataHref := routes.Puzzle.themePuzzleHistoryRemove(record.id))("删除")
                      )
                    )
                  }
                )
              )
            ),*/
            div(cls := "drawer-footer")(
              div(cls := "drawer-footer-btn")(
                a(cls := "cancel drawer-hidden")("取消"),
                submitButton(cls := "button button-empty")("开始练习")
              )
            )
          )
      }
    }

}
