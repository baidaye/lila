package views.html.teamContest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import lila.clazz.Clazz
import lila.team.{ Campus, Tag, Team }
import lila.teamContest.{ AllPlayerWithUser, Forbidden, PlayerWithUser, TeamBoard, TeamContest, TeamPaired, TeamPlayer, TeamRound, Teamer }
import play.api.data.Form
import controllers.rt_contest.routes
import org.joda.time.DateTime

object modal {

  val dataSingleTags = attr("data-single-tags")
  val dataRangeTags = attr("data-range-tags")
  val dataLeader = attr("data-leader")
  val dataMaxPlayer = attr("data-max-player")

  def playerChoose(
    leader: Boolean,
    form: Form[_],
    contest: TeamContest,
    team: Option[Team],
    campuses: List[Campus],
    tags: List[Tag],
    clazzes: List[Clazz],
    allPlayers: List[AllPlayerWithUser]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content member-modal modal-player-choose none", dataMaxPlayer := (if (leader) 2 else contest.maxPlayers), dataLeader := leader)(
      h2(s"选择${if (leader) "领队" else "棋手"}"),
      st.form(
        cls := "member-search small",
        dataSingleTags := s"""["${tags.filterNot(_.typ.range).map(_.field).mkString("\",\"")}"]""",
        dataRangeTags := s"""["${tags.filter(_.typ.range).map(_.field).mkString("\",\"")}"]"""
      )(
          table(
            tr(
              td(cls := "label")(label("主办方")),
              td(cls := "organizer")(colspan := 3)(
                contest.typ match {
                  case TeamContest.Type.TeamInner => teamLinkById(contest.organizer, false)
                  case TeamContest.Type.FederationInner => federationLinkById(contest.organizer)
                }
              )
            ),
            tr(
              td(cls := "label")(label("账号/备注")),
              td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
              td,
              td
            ),
            tr(
              td(cls := "label")(label("校区")),
              td(cls := "fixed")(form3.select(form("campus"), campuses.map(c => c.id -> c.name), "".some)),
              td(cls := "label")(label("班级")),
              td(cls := "fixed")(form3.select(form("clazzId"), clazzes.map(c => c.id -> c.name), "".some))
            ),
            tr(
              td(cls := "label")(label("等级分")),
              td(cls := "fixed")(form3.input(form("teamRatingMin"), typ = "number")(placeholder := "俱乐部等级分（最小）")),
              td(cls := "label")(label("至")),
              td(cls := "fixed")(form3.input(form("teamRatingMax"), typ = "number")(placeholder := "俱乐部等级分（最大）"))
            ),
            tr(
              td(cls := "label")(label("棋协级别")),
              td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
              td(cls := "label")(label("性别")),
              td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
            ),
            tags.nonEmpty option tr(
              td(colspan := 4)(
                a(cls := "show-search")("显示高级搜索", iconTag("R"))
              )
            ),
            tags.filterNot(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
            },
            tags.filter(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
            },
            tr(
              td(colspan := 4)(
                div(cls := "action")(
                  div, submitButton(cls := "button small")("查询")
                )
              )
            )
          )
        ),
      postForm(cls := "form3 player-choose-transfer")(
        div(cls := "player-transfer")(
          views.html.base.transfer[AllPlayerWithUser, PlayerWithUser](allPlayers, Nil) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.userId, dataAttr := left.json(contest.perfLens).toString())),
              td(cls := "name")(userSpan(left.user, text = left.markOrUsername.some, withBadge = false))
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.userId, dataAttr := right.json(contest.perfLens).toString())),
              td(cls := "name")(userSpan(right.user, text = right.markOrUsername.some, withBadge = false))
            )
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )
  )

  def showTeamer(contest: TeamContest, teamer: Teamer, pwus: List[PlayerWithUser])(implicit ctx: Context) = {
    div(cls := "modal-content modal-teamer-show none")(
      h2(s"${teamer.name} 详情"),
      div(cls := "teamer")(
        div(cls := "pair")(
          h3("领队："),
          div(cls := "leaders")(
            teamer.leaders.map { leader =>
              userIdLink(leader.some, withBadge = false)
            }
          )
        ),
        table(cls := "slist")(
          thead(
            tr(
              th("序号"),
              th("账号"),
              th("备注（姓名）"),
              contest.teamRated option th("俱乐部等级分"),
              th("等级分"),
              th("正式/替补")
            )
          ),
          tbody(
            if (pwus.nonEmpty) {
              pwus.map { pwu =>
                tr(st.id := pwu.userId, dataId := pwu.userId, cls := List("mine" -> ctx.me.?? { user => pwu.userId == user.id }))(
                  td(pwu.no),
                  td(userSpan(pwu.user, withBadge = false)),
                  td(pwu.markOrUsername),
                  contest.teamRated option td(pwu.player.teamRating.map(_.toString) | "-"),
                  td(pwu.player.rating),
                  td(cls := List("formal" -> pwu.player.formal, "substitute" -> !pwu.player.formal))(if (pwu.player.formal) "正式" else "替补")
                )
              }
            } else {
              tr(td(colspan := (if (contest.teamRated) 6 else 5))("还没有选择棋手."))
            }
          )
        )
      )
    )
  }

  def manualAbsent(contest: TeamContest, round: TeamRound, teamers: List[Teamer])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-absent none")(
      h2("弃权设置"),
      postForm(cls := "form3", action := routes.TeamContest.manualAbsent(contest.id, round.no))(
        form3.hidden("contestId", contest.id),
        views.html.base.transfer[Teamer, Teamer](
          leftOptions = teamers.filterNot(_.absent),
          rightOptions = teamers.filter(t => t.manualAbsent && !t.absentOr),
          leftLabel = "参赛队伍",
          rightLabel = "弃权队伍",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.id}", value := left.id, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.name)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.id}", value := right.id, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.name)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def setBoardTime(contest: TeamContest, round: TeamRound, board: TeamBoard)(implicit ctx: Context) = frag(
    div(cls := "modal-content board-time none")(
      h2("时间设置"),
      postForm(cls := "form3", action := routes.TeamContest.setBoardTime(contest.id, board.id))(
        div(cls := "form-group")(
          st.input(
            name := "startsAt",
            value := board.startsAt.toString("yyyy-MM-dd HH:mm"),
            cls := "form-control flatpickr",
            dataEnableTime := true,
            datatime24h := true
          )(
              dataMinDate := DateTime.now.plusMinutes(3).toString("yyyy-MM-dd HH:mm"),
              dataMaxDate := contest.finishAt.plusYears(1).toString("yyyy-MM-dd HH:mm")
            )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  val dataSigned = attr("data-signed")
  def setBoardSign(
    contest: TeamContest,
    round: TeamRound,
    paired: TeamPaired,
    board: TeamBoard,
    whiteUser: User,
    blackUser: User,
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content board-sign none")(
      h2("准备比赛（签到）设置"),
      postForm(cls := "form3")(
        div(cls := "form-group")(
          label(cls := "form-label")(s"第 ${round.no} 轮"),
          table(cls := "slist")(
            tbody(
              tr(
                td(s"#${board.whitePlayer.no}"),
                td(markOrUsername(markMap, whiteUser)),
                td(
                  ctx.me.??(me => contest.isCreator(me) || show.isPlayerLeader(board.whitePlayer.teamerId, paired)) option a(
                    cls := List("button button-empty small btn-sign" -> true, "button-green disabled" -> board.whitePlayer.signed),
                    dataHref := s"${routes.TeamContest.setBoardSign(contest.id, board.id, board.whitePlayer.userId.some)}",
                    dataSigned := (if (board.whitePlayer.signed) "1" else "0")
                  )(if (board.whitePlayer.signed) "已准备（签到）" else "代准备（签到）")
                )
              ),
              tr(
                td(s"#${board.blackPlayer.no}"),
                td(markOrUsername(markMap, blackUser)),
                td(
                  ctx.me.??(me => contest.isCreator(me) || show.isPlayerLeader(board.blackPlayer.teamerId, paired)) option a(
                    cls := List("button button-empty small btn-sign" -> true, "button-green disabled" -> board.blackPlayer.signed),
                    dataHref := s"${routes.TeamContest.setBoardSign(contest.id, board.id, board.blackPlayer.userId.some)}",
                    dataSigned := (if (board.blackPlayer.signed) "1" else "0")
                  )(if (board.blackPlayer.signed) "已准备（签到）" else "代准备（签到）")
                )
              )
            )
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确认", klass = "small")
        )
      )
    )
  )

  def teamerScoreDetail(no: TeamRound.No, teamer: Teamer, teamers: List[Teamer], paireds: List[TeamPaired])(implicit ctx: Context) = {
    div(cls := "modal-content score-detail none")(
      h2(s"（${teamer.no}）${teamer.name} 详情"),
      table(cls := "slist")(
        thead(
          tr(
            th("轮次"),
            th("序号"),
            th("白方"),
            th("结果"),
            th("黑方"),
            th("序号")
          )
        ),
        tbody(
          (1 to no).toList.map { n =>
            paireds.find(_.roundNo == n).map { paired =>
              val white = findTeamer(paired.whiteTeamer.id, teamers)
              val black = findTeamer(paired.blackTeamer.id, teamers)
              tr(
                td(n),
                td(white.no),
                td(show.teamerFrag(white)),
                td(strong(paired.resultFormat)),
                td(show.teamerFrag(black)),
                td(black.no)
              )
            } getOrElse {
              tr(
                td(n),
                td(teamer.no),
                td(show.teamerFrag(teamer)),
                td(strong(teamer.roundOutcomeFormat(n))),
                td("-"),
                td("-"),
                td("-")
              )
            }
          }
        )
      )
    )
  }

  def playerScoreDetail(no: TeamRound.No, player: TeamPlayer, teamer: Teamer, pwus: List[PlayerWithUser], boards: List[TeamBoard])(implicit ctx: Context) = {
    val pwu = findPlayer(player.id, pwus)
    div(cls := "modal-content score-detail none")(
      h2(s"（${player.no}）${pwu.markOrUsername} 详情"),
      table(cls := "slist")(
        thead(
          tr(
            th("轮次"),
            th("序号"),
            th("白方"),
            th("结果"),
            th("黑方"),
            th("序号"),
            th("操作")
          )
        ),
        tbody(
          (1 to no).toList.map { n =>
            boards.find(_.roundNo == n).map { board =>
              val color = ctx.me.fold("white")(u => board.colorOfByUserId(u.id).name)
              val white = findPlayer(board.whitePlayer.id, pwus)
              val black = findPlayer(board.blackPlayer.id, pwus)
              tr(
                td(n),
                td(s"${board.whitePlayer.teamerNo}.${white.no}"),
                td(white.markOrUsername),
                td(strong(board.resultFormat(false))),
                td(black.markOrUsername),
                td(s"${board.blackPlayer.teamerNo}.${black.no}"),
                td(a(target := "_blank", href := controllers.routes.Round.watcher(board.id, color))("查看"))
              )
            } getOrElse {
              val p = findPlayer(player.id, pwus)
              tr(
                td(n),
                td(s"${teamer.no}.${p.no}"),
                td(p.markOrUsername),
                td(strong(player.roundOutcomeFormat(n))),
                td("-"),
                td("-"),
                td("-")
              )
            }
          }
        )
      )
    )
  }

  def manualResult(contest: TeamContest, round: TeamRound, board: TeamBoard)(implicit ctx: Context) = frag(
    div(cls := "modal-content manual-result none")(
      h2("设置成绩"),
      postForm(cls := "form3", action := routes.TeamContest.manualResult(contest.id, board.id))(
        select(name := "result")(
          TeamBoard.Result.all.map { r =>
            option(value := r.id)(s"${r.id}（${r.name}）")
          }
        ),
        p("手工设置过比赛成绩，必须先发布成绩，在下一轮才能进行编排"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small btn-manual-result")
        )
      )
    )
  )

  def replaceFormal(contest: TeamContest, board: TeamBoard, teamer: Teamer, player: TeamPlayer, boards: List[TeamBoard], pwus: List[PlayerWithUser])(implicit ctx: Context) = {
    val boardPlayerIds = boards.flatMap(_.players.map(_.id))
    val pwu = findPlayer(player.id, pwus)
    div(cls := "modal-content replace-formal none")(
      h2(s"替换#${board.pairedNo}.${board.no}台棋手"),
      postForm(cls := "form3", action := routes.TeamContest.replaceFormal(contest.id, board.id, player.id))(
        div(cls := "form-group")(
          label(cls := "form-label")("原棋手："),
          span(s"#${pwu.player.no} ", pwu.markOrUsername)
        ),
        div(cls := "form-group")(
          label(cls := "form-label")("选择上场棋手："),
          div(cls := "manual-source")(
            table(cls := "slist")(
              thead(
                th,
                th("序号"),
                th("账号")
              ),
              tbody(
                pwus.filterNot(pwu => boardPlayerIds.contains(pwu.id)).map { pwu =>
                  tr(
                    td(input(tpe := "radio", name := "target", id := pwu.id, value := pwu.id)),
                    td("#", nbsp, pwu.no),
                    td(userLink(pwu.user, text = pwu.markOrUsername.some, withBadge = false))
                  )
                }
              )
            )
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  }

  def teamerForbidden(contest: TeamContest, teamers: List[Teamer], forbidden: Option[Forbidden])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-forbidden none")(
      h2("回避设置"),
      postForm(cls := "form3", action := routes.TeamContest.forbiddenApply(contest.id, forbidden.map(_.id)))(
        form3.hidden("teamerIds", forbidden.??(_.teamerIds.mkString(","))),
        div(cls := "fname")(input(name := "name", value := forbidden.map(_.name), required, minlength := 2, maxlength := 20, placeholder := "组名")),
        views.html.base.transfer[Teamer, Teamer](
          leftOptions = teamers.filterNot(p => forbidden.??(_.teamerIds.contains(p.id))),
          rightOptions = teamers.filter(p => forbidden.??(_.teamerIds.contains(p.id))),
          leftLabel = "参赛队伍",
          rightLabel = "回避队伍",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.id}", value := left.id, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.name)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.id}", value := right.id, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.name)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  private def findTeamer(id: Teamer.ID, teamers: List[Teamer]): Teamer =
    teamers.find(_.id == id) err s"can not find teamer：$id"

  private def findPlayer(id: TeamPlayer.ID, pwus: List[PlayerWithUser])(implicit ctx: Context): PlayerWithUser =
    pwus.find(pwu => pwu.player.id == id) err s"can not find player：$id"

  private def markOrUsername(markMap: Map[String, Option[String]], user: User): String = {
    val mark = markMap.get(user.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | user.username
  }

}
