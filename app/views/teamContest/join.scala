package views.html.teamContest

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.teamContest.{ PlayerWithUser, TeamContest, Teamer }
import controllers.rt_contest.routes

object join {

  val dataFormalPlayer = attr("data-formal-players")
  val dataSubstitutePlayer = attr("data-substitute-players")
  val teamRated = attr("data-team-rated")

  def apply(contest: TeamContest, teamer: Option[Teamer], pwus: List[PlayerWithUser], form: Form[_])(implicit ctx: Context) = {
    views.html.base.layout(
      title = s"${contest.fullName} 报名",
      moreCss = cssTag("teamContest.join"),
      moreJs = frag(
        tableDnDTag,
        transferTag,
        memberAdvanceTag,
        jsTag("teamContest.join.js")
      )
    ) {
        main(cls := "page-small contest-join", dataFormalPlayer := contest.formalPlayers, dataSubstitutePlayer := contest.substitutePlayers, teamRated := contest.teamRated)(
          div(cls := "box box-pad")(
            h1(s"${contest.fullName} 报名"),
            postForm(cls := "form3 contest-join-form", dataId := contest.id, action := routes.TeamContest.join(contest.id, teamer.map(_.id)))(
              form3.group(form("name"), raw("队名"), help = raw("最长10个字符").some)(form3.input(_)(required)),
              form3.group(form("leaderNames"), div(label("领队"), a(cls := "button button-empty small modal-alert player-choose", href := routes.TeamContest.playerChooseForm(contest.id, true, None))("选择")), help = raw("1-2名").some)(form3.input(_)(required, readonly := true)),
              form3.hidden(form("leaders")),
              form3.group(form("players"), div(label("棋手"), a(cls := "button button-empty small modal-alert player-choose", href := routes.TeamContest.playerChooseForm(contest.id, false, teamer.map(_.id)))("选择")), help = raw(s"正式棋手${contest.formalPlayers}名，替补棋手最多${contest.substitutePlayers}名").some)(_ =>
                table(cls := "slist contest-join-list")(
                  thead(
                    tr(
                      th("序号"),
                      th("账号"),
                      th("备注（姓名）"),
                      contest.teamRated option th("俱乐部等级分"),
                      th("等级分"),
                      th("正式/替补"),
                      th("操作")
                    )
                  ),
                  tbody(
                    if (pwus.nonEmpty) {
                      pwus.zipWithIndex.map {
                        case (pwu, index) => {
                          tr(st.id := s"tr-${pwu.userId}", dataId := pwu.userId, cls := List("mine" -> ctx.me.?? { user => pwu.userId == user.id }))(
                            td(pwu.no),
                            td(userSpan(pwu.user, withBadge = false)),
                            td(pwu.markOrUsername),
                            contest.teamRated option td(pwu.player.teamRating.map(_.toString) | "-"),
                            td(pwu.player.rating),
                            td(a(cls := List("button button-empty small toggle" -> true, "formal" -> pwu.player.formal, "substitute" -> !pwu.player.formal))(if (pwu.player.formal) "正式" else "替补")),
                            td(
                              form3.hidden(s"players[$index].no", pwu.no.toString),
                              form3.hidden(s"players[$index].userId", pwu.userId),
                              form3.hidden(s"players[$index].formal", pwu.player.formal.toString),
                              form3.hidden(s"players[$index].rating", pwu.player.rating.toString),
                              contest.teamRated option pwu.player.teamRating.map(r => form3.hidden(s"players[$index].teamRating", r.toString)),
                              a(cls := "button button-empty button-red small remove")("移除")
                            )
                          )
                        }
                      }
                    } else {
                      tr(td(colspan := (if (contest.teamRated) 7 else 6))("还没有选择棋手."))
                    }
                  )
                )),
              form3.globalError(form),
              form3.actions(
                a(href := routes.TeamContest.show(contest.id))("取消"),
                a(cls := "button button-empty", name := "staging", value := "true")("暂存"),
                a(cls := "button", name := "staging", value := "false")("报名")
              )
            )
          )
        )
      }
  }

}
