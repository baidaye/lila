package views.html.teamContest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.TeamFederation
import lila.teamContest.TeamContest.Rule
import lila.teamContest.{ DataForm, TeamContest, Btsss }
import play.api.data.Form
import play.api.libs.json._
import controllers.rt_contest.routes

object form {

  val dataTeamInner = attr("data-team-inner")
  val dataFederationInner = attr("data-federation-inner")
  val dataMaxRound = attr("data-max-round")

  def create(form: Form[_], rule: Rule, teams: List[lila.team.Team], federations: List[TeamFederation])(implicit ctx: Context) =
    layout(
      form,
      rule,
      teams,
      federations,
      "创建团体赛",
      routes.TeamContest.create()
    )

  def update(contest: TeamContest, form: Form[_], teams: List[lila.team.Team], federations: List[TeamFederation])(implicit ctx: Context) =
    layout(
      form,
      contest.rule,
      teams,
      federations,
      "编辑团体赛",
      routes.TeamContest.update(contest.id),
      contest.some
    )

  def view(contest: TeamContest, form: Form[_], teams: List[lila.team.Team], federations: List[TeamFederation])(implicit ctx: Context) =
    layout(
      form,
      contest.rule,
      teams,
      federations,
      "查看团体赛",
      routes.TeamContest.update(contest.id),
      contest.some,
      "返回".some
    )

  def clone(contest: TeamContest, form: Form[_], teams: List[lila.team.Team], federations: List[TeamFederation])(implicit ctx: Context) =
    layout(
      form,
      contest.rule,
      teams,
      federations,
      "复制团体赛",
      routes.TeamContest.create()
    )

  private val dataTab = attr("data-tab")
  private def errorTabActive(form: Form[_], tabName: String): Boolean =
    if (form.errors.isEmpty) false
    else form.errors.head.key.startsWith(tabName)

  private def layout(
    form: Form[_],
    rule: Rule,
    teams: List[lila.team.Team],
    federations: List[TeamFederation],
    title: String,
    url: play.api.mvc.Call,
    contest: Option[TeamContest] = None,
    cancelName: Option[String] = None
  )(implicit ctx: Context) = views.html.base.layout(
    title = title,
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("teamContest.form")
    ),
    moreJs = frag(
      flatpickrTag,
      singleUploaderTag,
      singleFileUploaderTag,
      jsTag("teamContest.form.js")
    )
  )(main(cls := "page-small", dataNotAccept := s"${!ctx.me.??(_.isTeam)}")(
      div(
        cls := "contest__form box box-pad",
        dataTeamInner := JsArray(teams.map(t => Json.obj("id" -> t.id, "name" -> t.name, "teamRated" -> t.ratingSettingOrDefault.open))).toString,
        dataFederationInner := JsArray(federations.map(c => Json.obj("id" -> c.id, "name" -> c.name, "teamRated" -> false))).toString
      )(
          h1(title),
          postForm(cls := "form3", action := url)(
            div(cls := "tabs")(
              div(dataTab := "basics", cls := List("active" -> (form.errors.isEmpty || form.errors.headOption ?? (_.key.isEmpty) || errorTabActive(form, "basics"))))("1.基本信息"),
              div(dataTab := "rounds", cls := List("active" -> errorTabActive(form, "rounds"), "disabled" -> (rule == Rule.Dual)))("2.轮次设置"),
              div(dataTab := "conditions", cls := List("active" -> errorTabActive(form, "conditions")))("3.报名设置")
            ),
            div(cls := "panels")(
              div(cls := List("panel basics" -> true, "active" -> (form.errors.isEmpty || form.errors.headOption ?? (_.key.isEmpty) || errorTabActive(form, "basics"))))(
                div(cls := "top")(
                  form3.group(form("basics.logo"), raw("Logo"), klass = "logo")(form3.singleImage(_, "上传LOGO"))
                ),
                form3.split(
                  form3.group(form("basics.name"), raw("比赛名称"), half = true)(form3.input(_)),
                  form3.group(form("basics.groupName"), raw("组别（可选）"), half = true)(form3.input(_))
                ),
                form3.split(
                  {
                    def available = ctx.me.?? { user =>
                      List(
                        TeamContest.Type.TeamInner -> user.hasTeam,
                        TeamContest.Type.FederationInner -> (user.hasTeam && federations.nonEmpty)
                      ).filter(_._2).map { t => t._1.id -> t._1.name }
                    }
                    val baseField = form("basics.typ")
                    val field = ctx.req.queryString get "team" flatMap (_.headOption) match {
                      case None => baseField
                      case Some(team) => baseField.copy(value = "team-inner".some)
                    }
                    form3.group(field, raw("比赛类型"), half = true)(form3.select(_, available))
                  },
                  form3.group(form("basics.organizer"), raw("报名范围"), half = true)(f => frag(
                    form3.select(f, teams.map(t => t.id -> t.name)),
                    form3.hidden("organizerSelected", f.value | "")
                  ))
                ),
                form3.split(
                  div(cls := "form-group form-half form-flex")(
                    form3.checkbox(form("basics.rated"), raw("计算等级分"), help = raw("对局将会计分<br>并影响棋手的等级分").some),
                    form3.checkbox(form("basics.teamRated"), raw("记录俱乐部等级分"), help = raw("影响棋手所在俱乐部的等级分").some, klass = "none")
                  ),
                  form3.group(form("basics.variant"), raw("比赛项目"), half = true)(form3.select(_, translatedVariantChoices.map(x => x._1 -> x._2)))
                ),
                form3.group(form("basics.position"), raw("起始位置"), klass = "starts-position") { field =>
                  val fieldVal = field.value
                  val url = fieldVal.fold(controllers.routes.Editor.index)(f => controllers.routes.Editor.load(f)).url
                  frag(
                    div(cls := "group-child")(
                      st.select(st.id := form3.id(field), name := field.name, cls := "form-control")(
                        option(value := chess.StartingPosition.initial.fen, fieldVal.has(chess.StartingPosition.initial.fen) option selected)(chess.StartingPosition.initial.name),
                        option(value := fieldVal, fieldVal.??(f => !chess.StartingPosition.allWithInitial.exists(_.fen == f)) option selected, dataId := "option-load-fen")("输入FEN"),
                        option(dataId := "option-load-situation")("载入局面"),
                        chess.StartingPosition.categories.map { categ =>
                          optgroup(attr("label") := categ.name)(
                            categ.positions.map { v =>
                              option(value := v.fen, fieldVal.has(v.fen) option selected)(v.fullName)
                            }
                          )
                        }
                      )
                    ),
                    div(cls := "group-child")(
                      input(
                        cls := List("form-control position-paste" -> true, "none" -> fieldVal.??(f => chess.StartingPosition.allWithInitial.exists(_.fen == f))),
                        placeholder := "在此处粘贴FEN棋谱", value := fieldVal
                      )
                    ),
                    div(cls := "group-child")(
                      a(cls := List("board-link" -> true, "none" -> fieldVal.has(chess.StartingPosition.initial.fen)), target := "_blank", href := url)(
                        div(cls := "preview")(
                          fieldVal.map { f =>
                            (chess.format.Forsyth << f).map { situation =>
                              div(
                                cls := "mini-board cg-wrap parse-fen is2d",
                                dataColor := situation.color.name,
                                dataFen := f
                              )(cgWrapContent)
                            }
                          }
                        )
                      )
                    )
                  )
                },
                form3.group(form("basics.rule"), raw("赛制"), half = true)(form3.select(_, TeamContest.Rule.list)),
                form3.split(
                  form3.group(form("basics.clockTime"), raw("基本用时"), half = true)(form3.select(_, DataForm.clockTimeChoices)),
                  form3.group(form("basics.clockIncrement"), raw("每步棋加时"), half = true)(form3.select(_, DataForm.clockIncrementChoices))
                ),
                form3.split(
                  form3.group(form("basics.startsAt"), raw("比赛开始时间"), half = true)(form3.flatpickr(_)),
                  form3.group(form("basics.finishAt"), raw("比赛结束时间"), half = true)(form3.flatpickr(_))
                )
              ),
              div(cls := List("panel rounds" -> true, "active" -> errorTabActive(form, "rounds")))(
                form3.split(
                  form3.group(form("#"), raw("轮次间隔"), half = true)(_ => div(cls := "round-space")(
                    form3.groupNoLabel(form("rounds.spaceDay"))(form3.select(_, DataForm.roundSpaceDayChoices)),
                    form3.groupNoLabel(form("rounds.spaceHour"))(form3.select(_, DataForm.roundSpaceHourChoices)),
                    form3.groupNoLabel(form("rounds.spaceMinute"))(form3.select(_, DataForm.roundSpaceMinuteChoices))
                  ))
                ),
                form3.split(
                  form3.group(form("rounds.rounds"), raw("轮次"), half = true)(form3.input(_, typ = "number")),
                  div(cls := "form-group form-half")(
                    label(cls := "form-label")(nbsp),
                    div(cls := "form-control")(
                      contest.isEmpty || contest.??(_.isCreated) option a(cls := "button button-generate", dataMaxRound := rule.setup.maxRound)("生成轮次时间")
                    )
                  )
                ),
                div(
                  label(cls := "form-label")("轮次开始时间"),
                  div(cls := "round-generate")(
                    (0 until rule.setup.maxRound) map { i =>
                      if (form(s"rounds.list[$i].startsAt").value.isDefined) {
                        form3.split(
                          form3.group(form(s"rounds.list[$i].startsAt"), raw(s"第 ${i + 1} 轮"), half = true)(form3.flatpickr(_))
                        )
                      } else frag()
                    }
                  )
                )
              ),
              div(cls := List("panel conditions" -> true, "active" -> errorTabActive(form, "conditions")))(
                form3.split(
                  form3.group(form("conditions.deadline"), raw("报名截止时间"), half = true)(form3.select(_, DataForm.deadlineMinuteChoices)),
                  form3.group(form("conditions.enterShow"), raw("报名信息显示"), half = true)(form3.select(_, TeamContest.EnterShow.list))
                ),
                form3.split(
                  form3.group(form("conditions.maxTeamers"), raw("比赛限报队伍总数"), half = true)(form3.input(_, typ = "number")),
                  form3.group(form("conditions.maxPerTeam"), raw("每俱乐部限报队伍数"), half = true)(form3.input(_, typ = "number"))
                ),
                form3.split(
                  form3.group(form("conditions.formalPlayers"), raw("每队正式棋手人数"), half = true)(form3.input(_, typ = "number")),
                  form3.group(form("conditions.substitutePlayers"), raw("每队替补棋手人数"), half = true)(form3.input(_, typ = "number"))
                ),
                form3.split(
                  form3.group(form("conditions.canLateMinute"), raw("允许迟到时间（分钟）"), half = true)(form3.input(_, typ = "number")),
                  div(cls := "form-group form-half")(
                    label(cls := "form-label")("破同分规则"),
                    div(
                      div(cls := "btss-wrap")(
                        //label(rule.name),
                        div(cls := "btss-list")(
                          Btsss.formShow.list.map { btss =>
                            span(cls := "btss")(btss.name)
                          }
                        )
                      )
                    )
                  )
                )
              )
            ),
            form3.globalError(form),
            form3.actions(
              a(href := routes.TeamContest.ownerPage(None, "", 1))(cancelName | "取消"),
              contest.isEmpty || contest.??(_.isCreated) option form3.submit("保存并预览", icon = "g".some)
            )
          )
        )
    ))

}
