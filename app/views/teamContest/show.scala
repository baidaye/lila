package views.html.teamContest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.teamContest.{ Btsss, Forbidden, PlayerWithUser, TeamBoard, TeamBtss, TeamContest, TeamPaired, TeamPlayer, TeamPlayerBtss, TeamPlayerScoreSheet, TeamRound, TeamScoreSheet, Teamer }
import lila.teamContest.TeamContest.Status
import lila.user.User
import controllers.rt_contest.routes

object show {

  private val dataTab = attr("data-tab")
  private val dataCurrentRound = attr("data-currentRound")

  def apply(
    contest: TeamContest,
    rounds: List[TeamRound],
    teamers: List[Teamer],
    players: List[PlayerWithUser],
    paireds: List[TeamPaired],
    boards: List[TeamBoard],
    forbiddens: List[Forbidden],
    teamRating: Map[User.ID, Double],
    scoreSheets: List[TeamScoreSheet],
    playerScoreSheets: List[TeamPlayerScoreSheet]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = s"团体赛 ${contest.name}",
      moreCss = cssTag("teamContest.show"),
      moreJs = frag(
        tableDnDTag,
        cookieTag,
        flatpickrTag,
        transferTag,
        memberAdvanceTag,
        jsTag("teamContest.show.js")
      )
    ) {
        main(cls := "page-small contest-show", dataId := contest.id, dataCurrentRound := contest.currentRound)(
          div(cls := "box box-pad head")(
            div(cls := "head__info")(
              baseInfo(contest)
            ),
            div(cls := "head__board")(
              (chess.StartingPosition.initial.fen != contest.position.fen) option
                (chess.format.Forsyth << contest.position.fen).map { situation =>
                  span(
                    cls := s"mini-board cg-wrap parse-fen is2d ${contest.variant.key}",
                    dataColor := situation.color.name,
                    dataFen := contest.position.fen
                  )(cgWrapContent)
                }
            ),
            div(cls := "head__enter")(
              findMyBoard(boards).map { b =>
                val color = ctx.me.fold("white")(u => b.colorOfByUserId(u.id).name)
                a(cls := "button glowing enter", href := controllers.routes.Round.watcher(b.id, color))("比赛已经开始，点击进入")
              } getOrElse {
                val enabled = enterButtonStatus(contest, teamers)
                st.form(st.action := routes.TeamContest.joinForm(contest.id, None), method := "GET")(
                  submitButton(cls := List("button button-green enter" -> true, "disabled" -> !enabled), !enabled option disabled)(enterButtonText(contest, teamers))
                )
              }
            ),
            isCreator(contest) option creatorActions(contest, teamers)
          ),
          div(cls := "box box-pad flow")(
            div(cls := "tab-nav")(
              div(cls := "nav-bars nav-fix")(
                div(dataTab := "rule", cls := List("nav" -> true, "active running" -> isRuleTabActive(contest)))("竞赛规则"),
                isCreator(contest) || isTeamerCreatorOrLeader(teamers) option div(dataTab := "enter", cls := List("nav" -> true, "active running" -> isEnterTabActive(contest, teamers)))("报名管理"),
                (!isCreator(contest) && showEnterSheet(contest, teamers, players)) option div(dataTab := "enterSheet", cls := List("nav" -> true))("报名表"),

                (isCreator(contest) && contest.rounds > 1 && contest.rule.flow.forbidden) option div(dataTab := "forbidden", cls := List("nav" -> true, "disabled" -> isForbiddenTabDisabled(contest)))("回避设置"),
                (isCreator(contest) && !contest.autoPairing && contest.rule.flow.roundEdit) option div(dataTab := "roundEdit", cls := List("nav" -> true, "active running" -> isRoundEditTabActive(contest, rounds), "disabled" -> isRoundEditTabDisabled(contest)))("循环赛编排")
              ),
              div(cls := "nav-scroll")(
                button(cls := "button button-empty nav-scroll-prev", dataIcon := "左"),
                div(cls := "nav-scroll-horizontal")(
                  div(cls := "nav-bars")(
                    contest.roundList.map { rno =>
                      val rd = findRound(rno, rounds)
                      div(dataTab := s"round$rno", cls := List("nav" -> true, "active running" -> isRoundTabActive(contest, rno, rounds), "disabled" -> isRoundTabDisabled(contest, rno, rounds)))(
                        div(s"第${rno}轮"),
                        div(rd.actualStartsAt.toString("MM-dd HH:mm"))
                      )
                    }
                  )
                ),
                button(cls := "button button-empty nav-scroll-next", dataIcon := "右")
              ),
              div(cls := "nav-bars nav-fix")(
                div(dataTab := "score", cls := List("nav" -> true, "active running" -> isScoreTabActive(contest), "disabled" -> isScoreTabDisabled(contest)))("成绩册")
              )
            ),
            div(cls := "tab-panel")(
              div(cls := List("panel rule" -> true, "active" -> isRuleTabActive(contest)))(rule(contest, rounds)),
              isCreator(contest) || isTeamerCreatorOrLeader(teamers) option div(cls := List("panel enter" -> true, "active" -> isEnterTabActive(contest, teamers)))(
                enter(contest, rounds, teamers)
              ),
              (!isCreator(contest) && showEnterSheet(contest, teamers, players)) option div(cls := List("panel enterSheet" -> true))(
                enterSheet(contest, teamers)
              ),
              (isCreator(contest) && contest.rounds > 1 && contest.rule.flow.forbidden) option div(cls := List("panel forbidden" -> true))(forbiddenTab(contest, forbiddens, teamers)),
              (isCreator(contest) && !contest.autoPairing && contest.rule.flow.roundEdit) option div(cls := List("panel roundEdit" -> true, "active" -> isRoundEditTabActive(contest, rounds)))(
                roundEditTab(contest, rounds, teamers, players)
              ),
              contest.roundList.map { rno =>
                div(cls := List(s"panel round round$rno" -> true, "active" -> isRoundTabActive(contest, rno, rounds)))(
                  round(contest, rounds, teamers, players, paireds, boards, rno)
                )
              },
              div(cls := List("panel score" -> true, "active" -> isScoreTabActive(contest)))(
                score(contest, rounds, teamers, players, scoreSheets, playerScoreSheets, teamRating)
              )
            )
          )
        )
      }

  private def baseInfo(contest: TeamContest)(implicit ctx: Context) =
    table(
      tr(
        td(
          img(cls := "logo", src := contest.logo.fold(staticUrl("images/icons/contest.svg")) { l => dbImageUrl(l) })
        ),
        td(
          div(cls := "contest-name")(
            span(cls := "text")(contest.fullName), nbsp,
            isCreator(contest) option frag(
              a(cls := "clone", href := routes.TeamContest.clone(contest.id), title := "复制当前比赛")("复制"), nbsp,
              contest.isOverPublished option a(cls := "view", href := routes.TeamContest.viewForm(contest.id), title := "查看比赛设置")("查看"), nbsp,
              contest.isOverPublished && !contest.isFinishedOrCanceled option a(cls := "view", href := routes.TeamContest.show(contest.id), title := "刷新比赛状态")("刷新")
            )
          ),
          div(cls := "organizer")(
            "主办方：",
            (contest.createdByTeam map { teamId =>
              teamLinkById(contest.organizer, false)
            }) getOrElse (
              contest.typ match {
                case TeamContest.Type.TeamInner => teamLinkById(contest.organizer, false)
                case TeamContest.Type.FederationInner => federationLinkById(contest.organizer)
              }
            )
          )
        )
      ),
      tr(
        td(contest.status.name),
        td(contest.typ.name, contest.typ == TeamContest.Type.FederationInner option frag("（", federationLinkById(contest.organizer), "）"))
      ),
      tr(
        td,
        td(contest.rule.name, nbsp, contest.rounds, "轮", form3.hidden("rule", contest.rule.id))
      ),
      tr(
        td,
        td(contest.variant.name, nbsp, contest.clock.toString)
      ),
      tr(
        td,
        td("比赛时间：", contest.startsAt.toString("yyyy-MM-dd HH:mm"), " 至 ", contest.finishAt.toString("yyyy-MM-dd HH:mm"))
      ),
      tr(
        td,
        td("报名截止：", contest.deadlineAt.toString("yyyy-MM-dd HH:mm"))
      ),
      tr(
        td,
        td("报名队数：", contest.nbTeamers, " / ", contest.maxTeamers, form3.hidden("maxTeamers", contest.nbTeamers.toString), form3.hidden("maxTeamers", contest.maxTeamers.toString))
      )
    )

  private def rule(contest: TeamContest, rounds: List[TeamRound])(implicit ctx: Context) = frag(
    div(cls := "s1")(
      div(cls := "h1")("一、竞赛通用规则"),
      div(cls := "s2")(
        div(cls := "h2")("1、公平竞赛规则"),
        ul(
          li("1.1 棋手必须进行公平的竞赛，禁止使用电脑引擎分析，或由其他人进行辅助"),
          li("1.2 裁判：主办方有权对比赛任何一盘棋做反作弊调查与裁决"),
          li("1.3 仲裁：棋手对任何一盘棋的结果或裁决有异议，可联系主办方进行仲裁")
        )
      ),
      div(cls := "s2")(
        div(cls := "h2")("2、文明竞赛规则"),
        ul(
          li("2.1 棋手应按时参加比赛"),
          li("2.2 积极争取好的比赛结果，不故意输棋或故意拖延走棋时间"),
          li("2.3 比赛中，禁止出现不文明、不道德的语言或行为")
        )
      ),
      div(cls := "s2")(
        div(cls := "h2")("3、比赛受到不可抗拒因素或者国家政策影响，主办方有权修改比赛时间调整赛事进程")
      )
    ),
    div(cls := "s1")(
      div(cls := "h1")(s"二、", contest.groupName | "", "组别规则"),
      div(cls := "s2")(
        div(cls := "h2")(s"1、比赛时间：自 ${contest.startsAt.toString("yyyy-MM-dd HH:mm")} 至 ${contest.finishAt.toString("yyyy-MM-dd HH:mm")}")
      ),
      div(cls := "s2")(
        div(cls := "h2")(s"2、比赛项目：${contest.variant.name}")
      ),
      div(cls := "s2")(
        div(cls := "h2")(s"3、用时：每方 ${contest.clock.limitString} 分钟，每步棋加 ${contest.clock.incrementSeconds} 秒")
      ),
      div(cls := "s2")(
        div(cls := "h2")(s"4、比赛采用${contest.rule.name}赛制，共 ${contest.rounds} 轮"),
        ul(
          rounds.zipWithIndex.map {
            case (r, i) => li("第 ", i + 1, " 轮：", r.startsAt.toString("yyyy-MM-dd HH:mm"))
          }
        ),
        div(cls := "h2")(b("注："), "最终轮次和时间根据报名队数，以最终编排结果为准")
      ),
      div(cls := "s2")(
        div(cls := "h2")(s"5、迟到：比赛开始 ${contest.canLateMinute} 分钟仍未开棋的，视为弃权，判负")
      ),
      div(cls := "s2")(
        div(cls := "h2")("6、破同分规则：", Btsss.formShow.list.map(_.name).mkString("、"))
      )
    ),
    div(cls := "s1")(
      div(cls := "h1")("三、报名要求"),
      div(cls := "s2")(
        div(cls := "h2 flex")(s"1、比赛类型为${contest.typ.name}，", contest.typ match {
          case TeamContest.Type.TeamInner => div(cls := "contest-typ")("仅", nbsp, teamLinkById(contest.organizer, false), nbsp, " 可以报名")
          case TeamContest.Type.FederationInner => div(cls := "contest-typ")("仅", nbsp, federationLinkById(contest.organizer), nbsp, " 联盟成员可以报名")
        }),
        div(cls := "h2")(s"2、每俱乐部限报${contest.maxPerTeam}队"),
        div(cls := "h2")(s"3、每队正式棋手${contest.formalPlayers}人，替补棋手最多${contest.substitutePlayers}人")
      )
    ),
    div(cls := "s1")(
      div(cls := "h1")("四、免责条款"),
      div(cls := "s2")(
        "比赛组织、管理由竞赛主办方负责，包括但不限于比赛信息咨询、报名、裁判、仲裁、编排、结果发布、奖励等，如需要问题，请直接联系主办方；对以上具体比赛服务，haichess.com 仅提供系统服务，不对具体的问题负责，如果在比赛中遇到技术问题，可随时联系客服协助解决。"
      )
    )
  )

  private def enter(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer])(implicit ctx: Context) = {
    val mine = ctx.userId.??(userId => teamers.filter(_.isCreatorOrLeader(userId)))
    val sentOrRefuseds = teamers.filter(_.sentOrRefused)
    val joineds = teamers.filter(_.joined)
    frag(
      isTeamerCreatorOrLeader(teamers) option div(
        div(cls := "top")(
          h3("我的参赛队")
        ),
        table(cls := "slist requests")(
          thead(
            tr(
              th("队名"),
              th("俱乐部"),
              th("领队"),
              th("报名时间"),
              th("状态"),
              th("操作")
            )
          ),
          tbody(
            mine.map { teamer =>
              tr(
                td(teamerFrag(teamer)),
                td(teamLinkById(teamer.teamId)),
                td(cls := "leaders")(
                  teamer.leaders.map { leader =>
                    userIdLink(leader.some, withOnline = false, withBadge = false)
                  }
                ),
                td(momentFromNow(teamer.createdAt)),
                td(teamer.status.name),
                td(cls := "actions")(
                  a(cls := "button button-empty small modal-alert teamer-show", href := routes.TeamContest.showTeamer(contest.id, teamer.id))("详情"),
                  teamer.stagingOrRefused option a(cls := "button button-empty small", href := routes.TeamContest.joinForm(contest.id, teamer.id.some))("继续报名"),
                  teamer.joined option {
                    if (teamer.absentOr) {
                      button(cls := "button button-red button-empty small disabled", disabled)("已退赛")
                    } else {
                      postForm(st.action := routes.TeamContest.quit(contest.id, teamer.id))(
                        submitButton(cls := "button button-red button-empty small quit confirm", title := "退赛后将不能继续参与剩余轮次，是否继续？")("退赛")
                      )
                    }
                  }
                )
              )
            }
          )
        )
      ),
      isCreator(contest) && sentOrRefuseds.nonEmpty option div(
        div(cls := "top")(
          h3("参赛队报名")
        ),
        table(cls := "slist requests")(
          thead(
            tr(
              th("队名"),
              th("俱乐部"),
              th("领队"),
              th("报名时间"),
              th("状态"),
              th("操作")
            )
          ),
          tbody(
            sentOrRefuseds.map { teamer =>
              tr(
                td(teamerFrag(teamer)),
                td(teamLinkById(teamer.teamId)),
                td(cls := "leaders")(
                  teamer.leaders.map { leader =>
                    userIdLink(leader.some, withOnline = false, withBadge = false)
                  }
                ),
                td(momentFromNow(teamer.createdAt)),
                td(teamer.status.name),
                td(cls := "actions")(
                  a(cls := "button button-empty small modal-alert teamer-show", href := routes.TeamContest.showTeamer(contest.id, teamer.id))("详情"),
                  postForm(cls := "process-request", action := routes.TeamContest.joinProcess(teamer.contestId, teamer.id))(
                    button(name := "process", value := "accept", cls := List("button button-empty button-green small confirm teamer-accept" -> true, "disabled" -> (teamer.processed || contest.isTeamerFull)), (teamer.processed || contest.isTeamerFull) option disabled, title := "是否确认接受？")(if (contest.isTeamerFull) "名额已满" else "接受"),
                    button(name := "process", value := "decline", cls := List("button button-empty button-red small confirm teamer-decline" -> true, "disabled" -> teamer.processed), teamer.processed option disabled, title := "是否确认拒绝？")("拒绝")
                  )
                )
              )
            }
          )
        )
      ),
      isCreator(contest) option div(cls := "waiting none")(spinner),
      isCreator(contest) option div(
        div(cls := "top")(
          h3("已审核报名"),
          contest.isOverPublished option a(cls := "button small teamer-export")("导出报名表")
        ),
        table(cls := List("slist teamers" -> true, "unsortable" -> (!contest.isPublished && !(contest.isEnterStopped && rounds.exists(r => r.no == contest.currentRound && r.isCreated)))))(
          thead(
            tr(
              th("序号"),
              th("队名"),
              th("俱乐部"),
              th("领队"),
              th("操作")
            )
          ),
          tbody(
            if (joineds.nonEmpty) {
              joineds.map { teamer =>
                tr(st.id := teamer.id, dataId := teamer.id, cls := List("mine" -> ctx.me.?? { user => teamer.createdBy == user.id || teamer.leaders.contains(user.id) }))(
                  td(teamer.no),
                  td(teamerFrag(teamer)),
                  td(teamLinkById(teamer.teamId)),
                  td(cls := "leaders")(
                    teamer.leaders.map { leader =>
                      userIdLink(leader.some, withOnline = false, withBadge = false)
                    }
                  ),
                  td(cls := "actions")(
                    a(cls := "button button-empty small modal-alert teamer-show", href := routes.TeamContest.showTeamer(contest.id, teamer.id))("详情"),
                    contest.teamerRemoveable && findRound(contest.currentRound, rounds).isCreated option postForm(action := routes.TeamContest.removeTeamer(teamer.contestId, teamer.id))(
                      button(cls := "button button-empty button-red small confirm teamer-remove", title := "是否确认移除？")("移除")
                    ),
                    contest.teamerKickable option postForm(action := routes.TeamContest.toggleKickPlayer(teamer.contestId, teamer.id))(
                      button(
                        cls := List("button button-empty small confirm player-kick" -> true, "button-red" -> !teamer.absentOr, "button-green" -> teamer.absentOr),
                        title := (if (teamer.absentOr) "是否确认恢复比赛" else "是否确认禁赛")
                      )(if (teamer.absentOr) "恢复" else "禁赛")
                    )
                  )
                )
              }
            } else {
              tr(td(colspan := "5")("没有报名队伍."))
            }
          )
        )
      )
    )
  }

  private def enterButtonText(contest: TeamContest, teamers: List[Teamer])(implicit ctx: Context): String = {
    contest.status match {
      case TeamContest.Status.Published =>
        contest.isTeamerFull match {
          case true => "名额已满"
          case false => {
            findTeamer(teamers).size >= contest.maxPerTeam match {
              case true => "报名次数已满"
              case false => "报名"
            }
          }
        }
      case _ => contest.status.name
    }
  }

  private def enterButtonStatus(contest: TeamContest, teamers: List[Teamer])(implicit ctx: Context) =
    if (ctx.me.??(_.isTeam)) {
      contest.status match {
        case TeamContest.Status.Published => !contest.isTeamerFull && findTeamer(teamers).size < contest.maxPerTeam && canEnter(contest)
        case _ => false
      }
    } else false

  private def canEnter(contest: TeamContest)(implicit ctx: Context) = {
    !contest.isTeamInner || (contest.isTeamInner && ctx.me.??(_.belongTeamId.has(contest.organizer)))
  }

  private def creatorActions(contest: TeamContest, teamers: List[Teamer])(implicit ctx: Context) = {
    val autoPairingFrag = postForm(st.action := routes.TeamContest.autoPairing(contest.id))(
      span(cls := "form-check-input", title := (if (contest.autoPairing) "当前为自动模式，点击切换为手动模式" else "当前为手动模式，点击切换为自动模式"))(
        st.input(
          st.id := "autoPairing",
          tpe := "checkbox",
          cls := "form-control cmn-toggle",
          contest.autoPairing option checked
        ),
        label(`for` := "autoPairing")
      )
    )

    val cancelFrag = postForm(st.action := routes.TeamContest.cancel(contest.id))(
      submitButton(cls := "button button-red button-empty cancel confirm", title := "取消比赛后，比赛将立即终止（正在进行的对局不受影响），是否继续？")("取消")
    )

    frag(
      div(cls := "head__action")(
        contest.status match {
          case Status.Created => frag(
            postForm(st.action := routes.TeamContest.publish(contest.id))(
              submitButton(
                cls := List("button button-green publish confirm" -> true, "disabled" -> contest.shouldEnterStop), contest.shouldEnterStop option disabled,
                title := (if (contest.shouldEnterStop) "已经过了报名截止时间" else "发布比赛后将无法继续编辑比赛信息，是否继续？")
              )("发布")
            ),
            a(cls := "button button-empty update", href := routes.TeamContest.updateForm(contest.id), title := "编辑比赛")("编辑"),
            postForm(st.action := routes.TeamContest.remove(contest.id))(
              submitButton(cls := "button button-red button-empty remove confirm", title := "删除比赛将不可恢复，是否继续？")("删除")
            )
          )
          case Status.Published => frag(
            !contest.autoPairing option postForm(st.action := routes.TeamContest.enterStop(contest.id))(
              submitButton(cls := "button button-red button-empty enterStop confirm", title := "报名截止后棋手无法继续报名比赛（管理员可以手动调整报名表，并进入第1轮比赛编排），是否继续？")("报名截止")
            ),
            cancelFrag
          )
          case Status.EnterStopped => frag(
            autoPairingFrag,
            cancelFrag
          )
          case Status.Started => frag(
            autoPairingFrag,
            cancelFrag
          )
          case Status.Finished | Status.Canceled => frag()
        }
      ),
      div(cls := "head__action_info")(
        contest.isCreated && contest.shouldEnterStop option div(dataIcon := "", cls := "warn")("当前时间已经超过报名截止时间，您可以返回“编辑”页面，重新设置比赛“开始时间”后再进行操作。")
      )
    )
  }

  private def enterSheet(contest: TeamContest, teamers: List[Teamer])(implicit ctx: Context) =
    table(cls := "slist")(
      thead(
        tr(
          th("序号"),
          th("队名"),
          th("俱乐部"),
          th("领队"),
          th("操作")
        )
      ),
      tbody(
        if (teamers.exists(_.joined)) {
          teamers.filter(_.joined).map { teamer =>
            tr(
              td(teamer.no),
              td(teamerFrag(teamer)),
              td(teamLinkById(teamer.teamId)),
              td(cls := "leaders")(
                teamer.leaders.map { leader =>
                  userIdLink(leader.some, withOnline = false, withBadge = false)
                }
              ),
              td(a(cls := "button button-empty small modal-alert teamer-show", href := routes.TeamContest.showTeamer(contest.id, teamer.id))("详情"))
            )
          }
        } else {
          tr(td(colspan := "5")("没有报名队伍."))
        }
      )
    )

  private def showEnterSheet(contest: TeamContest, teamers: List[Teamer], players: List[PlayerWithUser])(implicit ctx: Context) =
    contest.enterShowOrDefault == TeamContest.EnterShow.All || (contest.enterShowOrDefault == TeamContest.EnterShow.Player && (players.exists(p => ctx.userId.has(p.userId)) || teamers.exists(t => ctx.userId.exists(u => t.leaders.contains(u)))))

  private def isForbiddenTabDisabled(contest: TeamContest)(implicit ctx: Context) = contest.isCreated

  private def forbiddenTab(contest: TeamContest, forbiddens: List[Forbidden], teamers: List[Teamer])(implicit ctx: Context) = frag(
    div(cls := "forbidden-actions")(
      (isCreator(contest) && (contest.isPublished || contest.isEnterStopped || contest.isStarted)) option
        a(cls := "button small modal-alert", href := routes.TeamContest.forbiddenCreateForm(contest.id))("新建回避组")
    ),
    table(cls := "slist")(
      thead(
        tr(
          th("组名"),
          th("队伍"),
          contest.isEnterStopped || contest.isStarted option th("操作")
        )
      ),
      tbody(
        forbiddens.map { forbidden =>
          tr(
            td(forbidden.name),
            td(forbidden.withTeamer(teamers).map(_._2.name).mkString(", ")),
            contest.isEnterStopped || contest.isStarted option td(cls := "action")(
              postForm(action := routes.TeamContest.removeForbidden(contest.id, forbidden.id))(
                button(cls := "button button-empty small button-red confirm", title := "确认删除？")("删除")
              )
            )
          )
        }
      )
    )
  )

  private def roundEditTab(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer], players: List[PlayerWithUser])(implicit ctx: Context) = {
    val existsQuitOrKick = teamers.exists(_.quitOrKick)
    frag(
      postForm(cls := "form3", action := routes.TeamContest.setRoundRobin(contest.id))(
        div(cls := "waiting none")(spinner),
        table(cls := "roundEdit-main")(
          form3.hidden("canEdit", ((contest.isEnterStopped || contest.isStarted) && !contest.roundRobinPairingOr).toString),
          form3.hidden("basics-maxRound", contest.rule.setup.maxRound.toString),
          form3.hidden("basics-startsAt", contest.startsAt.toString("yyyy-MM-dd HH:mm")),
          form3.hidden("rounds-spaceDay", (contest.roundSpace / (24 * 60)).toString),
          form3.hidden("rounds-spaceHour", (contest.roundSpace % (24 * 60) / 60).toString),
          form3.hidden("rounds-spaceMinute", (contest.roundSpace % (24 * 60) % 60).toString),
          tbody(
            tr(
              th("轮次间隔："),
              td(s"${contest.roundSpace / (24 * 60)}天，${contest.roundSpace % (24 * 60) / 60}小时，${contest.roundSpace % (24 * 60) % 60}分钟")
            ),
            tr(
              th,
              td(
                table(cls := "roundEdit-list")(
                  tbody(
                    rounds.zipWithIndex.map {
                      case (round, index) => {
                        tr(cls := "round-line")(
                          td(cls := "no")(
                            span(s"第${round.no}轮")
                          ),
                          td(input(name := s"list[$index]", value := round.startsAt.toString("yyyy-MM-dd HH:mm"), dataEnableTime := true, datatime24h := true, cls := "form-control flatpickr")),
                          td(cls := "control")(
                            a(cls := "rm", title := "移除")("-"),
                            a(cls := "ad", title := "添加")("+")
                          ),
                          td(cls := "error")
                        )
                      }
                    }
                  )
                )
              )
            )
          )
        ),
        ul(cls := "global-error error")(
          li(dataIcon := "", cls := List("roundNumError" -> true, "none" -> (contest.robinRoundsByTeamerOfRule == rounds.size)))("报名队伍和轮次不匹配"),
          li(dataIcon := "", cls := List("otherError" -> true, "none" -> !existsQuitOrKick))(existsQuitOrKick option "当前报名队伍中存在“禁赛”或“退赛”状态，您可以“移除”或者“恢复”禁赛队伍。")
        ),
        (isCreator(contest) && (contest.isEnterStopped || contest.isStarted) && !contest.roundRobinPairingOr) option form3.actions(
          a(cls := "button small pairingRoundRobin", href := routes.TeamContest.pairingRoundRobin(contest.id))("循环赛编排")
        )
      )
    )
  }

  val dataContestId = attr("data-contest-id")
  val dataRoundNo = attr("data-round-no")
  private def round(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer], players: List[PlayerWithUser], paireds: List[TeamPaired], boards: List[TeamBoard], rno: Int)(implicit ctx: Context) = {
    val round = findRound(rno, rounds)
    val roundPaireds = paireds.filter(_.roundNo == rno)
    val roundBoards = boards.filter(_.roundNo == rno)
    val noBoardTeamers = teamers.filter(_.noBoard(rno)).sortWith((p1, p2) => p1.roundOutcomeSort(rno) > p2.roundOutcomeSort(rno))
    frag(
      (isCreator(contest) && !contest.autoPairing && contest.currentRound == rno) option div(
        div(cls := "round-actions")(
          ((contest.isEnterStopped || contest.isStarted) && (round.isCreated || (contest.isRoundRobin && round.isPairing)) && players.nonEmpty && contest.rule.flow.manualAbsent) option a(cls := "button small modal-alert absent", href := routes.TeamContest.manualAbsentForm(contest.id, rno))("弃权设置"),
          ((contest.isEnterStopped || contest.isStarted) && (round.isCreated || round.isPairing) && teamers.nonEmpty && contest.rule.flow.roundPairing) option postForm(st.action := routes.TeamContest.pairing(contest.id, rno), dataContestId := contest.id)(
            submitButton(cls := "button small pairing", title := "新生成的对战表将覆盖本轮原有对战表，是否继续？")("生成对战表")
          ),
          ((contest.isEnterStopped || contest.isStarted) && round.isPairing) option postForm(st.action := routes.TeamContest.publishPairing(contest.id, rno), dataContestId := contest.id)(
            submitButton(cls := "button small publish-pairing", title := "发布对战表后将无法继续调整本轮对战表，是否继续？")("发布对战表")
          ),
          ((contest.isEnterStopped || contest.isStarted) && contest.rule.flow.cancelPairingPublish && round.isPublished && roundBoards.forall(_.canCancel)) option postForm(st.action := routes.TeamContest.cancelPublishPairing(contest.id, rno), dataContestId := contest.id)(
            submitButton(cls := "button small cancel-publish-pairing", title := "撤回已发布的对战表后您可以重新编排并再次发布对战表，是否继续？")("撤回对战表")
          ),
          ((contest.isEnterStopped || contest.isStarted) && round.isFinished) option postForm(st.action := routes.TeamContest.publishResult(contest.id, rno))(
            submitButton(cls := "button small publish-result confirm", title := "发布成绩后将无法继续调整比赛成绩，是否继续？")("发布成绩")
          ),
          (contest.isEnterStopped || contest.isStarted) && (round.isCreated || round.isPairing) option div(cls := "round-starts")(
            label("本轮开始时间："),
            st.input(cls := "flatpickr", dataEnableTime := true, datatime24h := true, dataContestId := contest.id, dataRoundNo := rno, name := "roundStartsTime", value := round.actualStartsAt.toString("yyyy-MM-dd HH:mm"))
          )
        )
      ),
      table(cls := "slist")(
        thead(
          tr(
            th,
            th(cls := "no")("台号"),
            th(cls := "no")("序号"),
            th(cls := "name")("白方"),
            th(cls := "score")("积分"),
            th(cls := "board-result")("结果"),
            th(cls := "score")("积分"),
            th(cls := "name")("黑方"),
            th(cls := "no")("序号"),
            th("操作")
          )
        ),
        tbody(
          round.isOverPairing option roundPaireds.map {
            paired =>
              {
                val white = findTeamer(paired.whiteTeamer.no, teamers)
                val black = findTeamer(paired.blackTeamer.no, teamers)
                frag(
                  tr(cls := List("expandable" -> true, "mine" -> ctx.me.?? { user => paired.isLeader(user.id) }), dataId := paired.id)(
                    td(cls := "expand expanded", dataIcon := "右"),
                    td(cls := "no")(s"#${paired.no}"),
                    td(cls := "no")(white.no),
                    td(cls := "name")(teamerFrag(white)),
                    td(cls := "score")(white.roundScore(rno, contest.isRoundRobin)),
                    td(cls := "board-result")(paired.resultShow),
                    td(cls := "score")(black.roundScore(rno, contest.isRoundRobin)),
                    td(cls := "name")(teamerFrag(black)),
                    td(cls := "no")(black.no),
                    td()
                  ),
                  tr(rel := paired.id)(
                    td(cls := "td-paired-players", colspan := "10")(
                      pairedPlayers(contest, round, paired, boards, players)
                    )
                  )
                )
              }
          },
          noBoardTeamers.map { teamer =>
            {
              val noBoardPlayers = players.filter(_.player.teamerId == teamer.id).filter(_.player.noBoard(rno)).sortWith((p1, p2) => p1.player.roundOutcomeSort(rno) > p2.player.roundOutcomeSort(rno))
              frag(
                tr(cls := List("expandable" -> true, "mine" -> ctx.me.?? { user => teamer.isCreatorOrLeader(user.id) }), dataId := teamer.id)(
                  td(cls := "expand expanded", dataIcon := "右"),
                  td(cls := "no")("-"),
                  td(cls := "no")(teamer.no),
                  td(cls := "name")(teamerFrag(teamer)),
                  td(cls := "score")(teamer.roundScore(rno, contest.isRoundRobin)),
                  td(cls := "board-result")(teamer.roundOutcomeFormat(rno)),
                  td(cls := "score")("-"),
                  td(cls := "name")("-"),
                  td(cls := "no")("-"),
                  td("-")
                ),
                tr(rel := teamer.id)(
                  td(cls := "td-paired-players", colspan := "10")(
                    teamerWhitePlayers(contest, round, teamer, boards, noBoardPlayers)
                  )
                )
              )
            }
          }
        )
      )
    )
  }

  private def pairedPlayers(contest: TeamContest, round: TeamRound, paired: TeamPaired, boards: List[TeamBoard], players: List[PlayerWithUser])(implicit ctx: Context) = {
    val rno = round.no
    val pairedBoards = boards.filter(b => b.pairedId == paired.id)
    table(cls := "slist table-paired-players")(
      tbody(
        round.isOverPairing option pairedBoards.map { board =>
          val leftColor = if (board.no % 2 == 0) chess.Black else chess.White
          val rightColor = if (board.no % 2 == 0) chess.White else chess.Black
          val leftPlayer = findPlayer(board.player(leftColor).id, players)
          val leftBoardPlayer = board.player(leftColor)
          val rightPlayer = findPlayer(board.player(rightColor).id, players)
          val rightBoardPlayer = board.player(rightColor)
          val color = ctx.me.fold("white")(u => board.colorOfByUserId(u.id).name)
          val reverse = leftColor == chess.Black
          tr(cls := List("mine" -> ctx.me.?? { user => board.containsByUser(user.id) }))(
            td(cls := "expand"),
            td(cls := "no")(s"#${paired.no}.${board.no}"),
            td(cls := "no")(s"${leftBoardPlayer.teamerNo}.${leftBoardPlayer.no}"),
            td(cls := "name")(
              playerFrag(leftPlayer, leftColor, contest, round, board, paired)
            ),
            td(cls := "score")(leftPlayer.player.roundScore(rno, contest.isRoundRobin)),
            td(cls := "board-result")(
              div(cls := "nowrap")(board.resultShow(reverse)),
              (board.isCreated && round.startsAt != board.startsAt) option div(cls := "board-time")(board.startsAt.toString("MM-dd HH:mm"))
            ),
            td(cls := "score")(rightPlayer.player.roundScore(rno, contest.isRoundRobin)),
            td(cls := "name")(
              playerFrag(rightPlayer, rightColor, contest, round, board, paired)
            ),
            td(cls := "no")(s"${rightBoardPlayer.teamerNo}.${rightBoardPlayer.no}"),
            td(
              contest.isOverPublished && round.isOverPublished option frag(
                round.isOverPublished option a(cls := "button button-empty small", href := controllers.routes.Round.watcher(board.id, color))(if (ctx.me.??(u => board.containsByUser(u.id))) "进入" else "观看")
              ),
              nbsp,
              (isCreator(contest) && (contest.isEnterStopped || contest.isStarted) && (round.isPublished || round.isStarted) && board.isCreated) option
                a(cls := "button button-empty small modal-alert", href := routes.TeamContest.setBoardTimeForm(contest.id, board.id))("设置时间"),
              ((isCreator(contest) || ctx.userId.??(paired.isLeader)) && (contest.isEnterStopped || contest.isStarted) && (round.isPublished || round.isStarted) && (board.isCreated || board.isStarted)) option
                a(cls := "button button-empty small modal-alert", href := routes.TeamContest.setBoardSignForm(contest.id, board.id))("准备比赛（签到）"),
              (isCreator(contest) && !contest.autoPairing && contest.isStarted && board.isFinished) option
                a(cls := "button button-empty small modal-alert manual-result", href := routes.TeamContest.manualResultForm(contest.id, board.id))("设置成绩")
            )
          )
        }
      )
    )
  }

  private def teamerWhitePlayers(contest: TeamContest, round: TeamRound, teamer: Teamer, boards: List[TeamBoard], players: List[PlayerWithUser])(implicit ctx: Context) = {
    val rno = round.no
    table(cls := "slist table-paired-players")(
      tbody(
        players.filterNot(_.player.roundIsSubstitute(rno)).map { playerWithUser =>
          val player = playerWithUser.player
          val result = player.roundOutcomeFormat(rno)
          tr(title := result, cls := List("mine" -> ctx.me.?? { user => player.userId == user.id }))(
            td(cls := "expand"),
            td(cls := "no")("-"),
            td(cls := "no")(s"${teamer.no}.${player.no}"),
            td(cls := "name")(userLink(playerWithUser.user, text = playerWithUser.markOrUsername.some, withTitle = false, withBadge = false)),
            td(cls := "score")(player.roundScore(rno, contest.isRoundRobin)),
            td(cls := "board-result")(result),
            td(cls := "score")("-"),
            td(cls := "name")("-"),
            td(cls := "no")("-"),
            td("-")
          )
        }
      )
    )
  }

  private def score(
    contest: TeamContest,
    rounds: List[TeamRound],
    teamers: List[Teamer],
    players: List[PlayerWithUser],
    scoreSheets: List[TeamScoreSheet],
    playerScoreSheets: List[TeamPlayerScoreSheet],
    teamRating: Map[User.ID, Double]
  )(implicit ctx: Context) = {
    //val boardPlayerIds = players.filter(_.player.hasBoard).map(_.id)
    val maxPublishedResultRoundNo = rounds.findRight(_.isPublishResult).map(_.no) | contest.currentRound
    frag(
      isCreator(contest) option div(cls := "score-actions")(
        (!contest.autoPairing && contest.isStarted && contest.allRoundFinished) option postForm(st.action := routes.TeamContest.publishScoreAndFinish(contest.id))(
          submitButton(cls := "button small publish-result confirm", title := "是否确认发布成绩并结束比赛？")("发布成绩并结束比赛")
        ),
        a(cls := "button small export-score")("导出成绩册")
      ),
      div(cls := "tabs-score")(
        div(cls := "tabs-score-head")(
          div(cls := "radio-group")(
            span(cls := "radio")(
              st.input(tpe := "radio", st.id := "rd-score-teamer", st.name := "rd-score", st.value := "score-teamer", checked),
              label(cls := "radio-label", `for` := "rd-score-teamer")("团体")
            ),
            span(cls := "radio")(
              st.input(tpe := "radio", st.id := "rd-score-player", st.name := "rd-score", st.value := "score-player"),
              label(cls := "radio-label", `for` := "rd-score-player")("棋手")
            )
          )
        ),
        div(cls := "tabs-score-body")(
          table(cls := "slist score-teamer")(
            thead(
              tr(
                th("名次"),
                th("序号"),
                th("队伍"),
                th("俱乐部"),
                th("胜/和/负"),
                th("积分（场分）"),
                TeamBtss.default.filterNot(_.id == "no").map(btss => th(btss.name)),
                th("操作")
              )
            ),
            tbody(
              scoreSheets.map { scoreSheet =>
                {
                  val teamer = findTeamer(scoreSheet.teamerNo, teamers)
                  tr(
                    td(if ((isCreator(contest) || contest.isFinishedOrCanceled) && scoreSheet.cancelled) "-" else scoreSheet.rank),
                    td(teamer.no),
                    td(teamerFrag(teamer)),
                    td(teamLinkById(teamer.teamId)),
                    td("`", span(teamer.countWin(maxPublishedResultRoundNo, contest.isRoundRobin)), " / ", span(teamer.countDraw(maxPublishedResultRoundNo, contest.isRoundRobin)), " / ", span(teamer.countLoss(maxPublishedResultRoundNo, contest.isRoundRobin))),
                    td(strong(scoreSheet.score)),
                    scoreSheet.btssScores.filterNot(_.btss.id == "no").map(btss => td(btss.score)),
                    td(cls := "actions")(
                      (isCreator(contest) && !contest.autoPairing && contest.isStarted && contest.allRoundFinished) option postForm(action := routes.TeamContest.cancelScore(scoreSheet.contestId, scoreSheet.id))(
                        submitButton(cls := List("button button-empty button-red small confirm score-cancel" -> true, "disabled" -> scoreSheet.cancelled), title := "取消比赛成绩将不可恢复，是否继续？", scoreSheet.cancelled option disabled)(if (scoreSheet.cancelled) "已取消" else "取消成绩")
                      ),
                      a(cls := "button button-empty small modal-alert score-detail", href := routes.TeamContest.teamerScoreDetail(contest.id, scoreSheet.roundNo, scoreSheet.teamerId))("详情")
                    )
                  )
                }
              }
            )
          ),
          table(cls := "slist score-player none")(
            thead(
              tr(
                th("名次"),
                th("序号"),
                th("棋手"),
                th("队伍"),
                contest.teamRated option th("俱乐部等级分"),
                th("积分"),
                TeamPlayerBtss.default.filterNot(_.id == "no").map(btss => th(btss.name)),
                th("操作")
              )
            ),
            tbody(
              playerScoreSheets.map { scoreSheet =>
                {
                  val playerWithUser = findPlayerByUser(scoreSheet.playerUid, players)
                  val teamer = findTeamer(playerWithUser.player.teamerId, teamers)
                  tr(cls := List("mine" -> ctx.me.?? { user => playerWithUser.userId == user.id }))(
                    td(if ((isCreator(contest) || contest.isFinishedOrCanceled) && scoreSheet.cancelled) "-" else scoreSheet.rank),
                    td(s"${teamer.no}.${scoreSheet.playerNo}"),
                    td(userLink(playerWithUser.user, text = playerWithUser.markOrUsername.some, withBadge = false)),
                    td(teamerFrag(teamer)),
                    contest.teamRated option td(
                      playerWithUser.player.teamRating.map(_.toString) | "-",
                      teamRating.get(playerWithUser.player.userId).map { diff => frag("（", span(cls := List("diff" -> true, "minus" -> (diff < 0)))(if (diff < 0) { diff } else { "+" + diff }), "）") }
                    ),
                    td(strong(scoreSheet.score)),
                    scoreSheet.btssScores.filterNot(_.btss.id == "no").map(btss => td(if (btss.btss == TeamPlayerBtss.VictRate) Math.round(btss.score) else btss.score, if (btss.btss == TeamPlayerBtss.VictRate) "%" else "")),
                    td(cls := "actions")(
                      scoreSheet.cancelled option button(cls := "button button-empty button-red small disabled", scoreSheet.cancelled option disabled)("已取消"),
                      a(cls := "button button-empty small modal-alert score-detail", href := routes.TeamContest.playerScoreDetail(contest.id, scoreSheet.roundNo, scoreSheet.playerUid))("详情")
                    )
                  )
                }
              }
            )
          )
        )
      )
    )
  }

  private def isRuleTabActive(contest: TeamContest)(implicit ctx: Context) = contest.isCreated || (contest.isPublished && !isCreator(contest))
  private def isEnterTabActive(contest: TeamContest, teamers: List[Teamer])(implicit ctx: Context) = contest.isPublished && isCreator(contest)
  private def isRoundEditTabActive(contest: TeamContest, rounds: List[TeamRound])(implicit ctx: Context) = {
    (contest.isEnterStopped || contest.isStarted) && !contest.roundRobinPairingOr
  }
  private def isRoundTabActive(contest: TeamContest, r: Int, rounds: List[TeamRound])(implicit ctx: Context) = {
    val rd = findRound(r, rounds)
    !contest.isFinishedOrCanceled && !contest.allRoundFinished && contest.currentRound == r && (
      contest.autoPairing match {
        case true => rd.isPublished || rd.isStarted
        case false => contest.isEnterStopped || contest.isStarted
      }
    ) && (
        contest.isRoundRobin match {
          case true => contest.roundRobinPairingOr
          case false => true
        }
      )
  }
  private def isScoreTabActive(contest: TeamContest)(implicit ctx: Context) = contest.isFinished || contest.isCanceled || contest.allRoundFinished
  private def isRoundTabDisabled(contest: TeamContest, r: Int, rounds: List[TeamRound])(implicit ctx: Context) = {
    val rd = findRound(r, rounds)
    ((rd.isCreated || rd.isPairing) && !(!contest.autoPairing && contest.currentRound == r) || contest.isCreated || contest.isPublished) && !contest.isRoundRobin
  }
  private def isScoreTabDisabled(contest: TeamContest)(implicit ctx: Context) = !contest.isOverStarted

  private def isRoundEditTabDisabled(contest: TeamContest)(implicit ctx: Context) = contest.isCreated

  private def isCreator(contest: TeamContest)(implicit ctx: Context): Boolean = ctx.me ?? contest.isCreator

  private def isLeader(teamers: List[Teamer])(implicit ctx: Context): Boolean =
    ctx.userId.?? { userId => teamers.exists(_.isLeader(userId)) }

  private def isTeamerCreator(teamers: List[Teamer])(implicit ctx: Context): Boolean =
    ctx.userId.?? { userId => teamers.exists(_.isCreator(userId)) }

  private def isTeamerCreatorOrLeader(teamers: List[Teamer])(implicit ctx: Context): Boolean =
    ctx.userId.?? { userId => teamers.exists(_.isCreatorOrLeader(userId)) }

  private def findTeamer(teamers: List[Teamer])(implicit ctx: Context): List[Teamer] =
    ctx.me.?? { user =>
      teamers.filter(_.isCreator(user.id))
    }

  private def findTeamer(id: Teamer.ID, teamers: List[Teamer]): Teamer =
    teamers.find(_.id == id) err s"can not find teamer：$id"

  private def findTeamer(no: Teamer.No, teamers: List[Teamer]): Teamer =
    teamers.find(_.no == no) err s"can not find teamer：$no"

  private def findMyBoard(boards: List[TeamBoard])(implicit ctx: Context): Option[TeamBoard] =
    ctx.me.?? { user =>
      boards.find(b => b.containsByUser(user.id) && b.isStarted)
    }

  private def isPlayer(players: List[PlayerWithUser])(implicit ctx: Context): Boolean =
    ctx.userId.?? { userId => players.exists(_.is(userId)) }

  private def findPlayer(players: List[PlayerWithUser])(implicit ctx: Context): Option[PlayerWithUser] =
    ctx.me.?? { user =>
      players.find(pwu => pwu.player.userId == user.id)
    }

  private def findPlayer(id: TeamPlayer.ID, players: List[PlayerWithUser])(implicit ctx: Context): PlayerWithUser =
    players.find(pwu => pwu.player.id == id) err s"can not find player：$id"

  private def findPlayerByUser(userId: User.ID, players: List[PlayerWithUser])(implicit ctx: Context): PlayerWithUser =
    players.find(pwu => pwu.userId == userId) err s"can not find player：$userId"

  private def findPlayers(teamerId: Teamer.ID, players: List[PlayerWithUser])(implicit ctx: Context): List[PlayerWithUser] =
    players.filter(_.player.teamerId == teamerId)

  private def findRound(no: TeamRound.No, rounds: List[TeamRound]): TeamRound =
    rounds.find(_.no == no) err s"can not find round：$no"

  private def playerFrag(pwu: PlayerWithUser, color: chess.Color, contest: TeamContest, round: TeamRound, board: TeamBoard, paired: TeamPaired)(implicit ctx: Context) =
    div(cls := s"teamer-player color-icon is ${color.name} text")(
      userSpan(pwu.user, text = pwu.markOrUsername.some, withTitle = false, withBadge = false),
      ((contest.isEnterStopped || contest.isStarted) && (round.isPublished || round.isStarted) && board.canReplace && !board.isSigned(pwu.id) && isPlayerLeader(pwu.player.teamerId, paired)) option
        a(cls := "button button-empty small modal-alert button-replace-formal", dataIcon := "替", title := "替换棋手", href := routes.TeamContest.replaceFormal(contest.id, board.id, pwu.id))
    )

  def isPlayerLeader(playerTeamerId: Teamer.ID, paired: TeamPaired)(implicit ctx: Context) = {
    val isWhiteLeader = ctx.userId.??(userId => paired.isWhiteLeader(userId))
    val isBlackLeader = ctx.userId.??(userId => paired.isBlackLeader(userId))
    (isWhiteLeader && paired.whiteTeamer.id == playerTeamerId) || (isBlackLeader && paired.blackTeamer.id == playerTeamerId)
  }

  def teamerFrag(teamer: Teamer) = div(cls := "teamerFrag")(
    img(cls := "teamer-icon")(src := staticUrl("images/contest/teamer-icon.svg")),
    span(cls := "teamer-name", title := teamer.name)(teamer.name)
  )

}
