package views.html.user.show

import play.api.data.Form

import lila.api.Context
import lila.app.mashup.UserInfo.Angle
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.app.mashup.UserInfo
import lila.game.Game
import lila.user.User

import controllers.routes

object page {

  def activity(
    u: User,
    activities: Vector[lila.activity.ActivityView],
    info: UserInfo,
    social: lila.app.mashup.UserInfo.Social
  )(implicit ctx: Context) = views.html.base.layout(
    title = s"${u.username} : ${trans.activity.activity.txt()}",
    openGraph = lila.app.ui.OpenGraph(
      image = staticUrl("images/large_tile.png").some,
      title = u.titleUsernameWithBestRating,
      url = s"$netBaseUrl${routes.User.show(u.username).url}",
      description = describeUser(u)
    ).some,
    moreJs = moreJs(info),
    moreCss = frag(
      cssTag("user.show"),
      isGranted(_.UserSpy) option cssTag("mod.user")
    ),
    robots = u.count.game >= 10
  ) {
      main(cls := "page-menu", dataUsername := u.username)(
        st.aside(cls := "page-menu__menu")(side(u, info.ranks, none)),
        div(cls := "page-menu__content box user-show")(
          header(u, info, Angle.Activity, social),
          div(cls := "angle-content")(views.html.activity(u, activities))
        )
      )
    }

  def games(
    u: User,
    info: UserInfo,
    games: Paginator[Game],
    filters: lila.app.mashup.GameFilterMenu,
    searchForm: Option[Form[_]],
    social: lila.app.mashup.UserInfo.Social
  )(implicit ctx: Context) = views.html.base.layout(
    title = s"${u.username} : ${userGameFilterTitleNoTag(u, info.nbs, filters.current)}${if (games.currentPage == 1) "" else " - page " + games.currentPage}",
    moreJs = moreJs(info, filters.current.name == "search"),
    moreCss = frag(
      cssTag("user.show"),
      filters.current.name == "search" option cssTag("user.show.search"),
      isGranted(_.UserSpy) option cssTag("mod.user")
    ),
    robots = u.count.game >= 10
  ) {
      main(cls := "page-menu", dataUsername := u.username)(
        st.aside(cls := "page-menu__menu")(side(u, info.ranks, none)),
        div(cls := "page-menu__content box user-show")(
          header(u, info, Angle.Games(searchForm), social),
          div(cls := "angle-content")(gamesContent(u, info.nbs, games, filters, filters.current.name))
        )
      )
    }

  private def moreJs(info: UserInfo, withSearch: Boolean = false)(implicit ctx: Context) = frag(
    infiniteScrollTag,
    jsAt("compiled/user.js"),
    info.ratingChart.map { ratingChart =>
      frag(
        jsTag("chart/ratingHistory.js"),
        embedJsUnsafe(s"lichess.ratingHistoryChart($ratingChart);")
      )
    },
    withSearch option flatpickrTag,
    withSearch option frag(jsTag("search.js")),
    isGranted(_.UserSpy) option jsAt("compiled/user-mod.js")
  )

  def disabled(u: User)(implicit ctx: Context) =
    views.html.base.layout(title = u.username, robots = false) {
      main(cls := "box box-pad")(
        h1(u.username),
        p(trans.thisAccountIsClosed())
      )
    }

  def forbidden(
    u: User,
    blocked: Boolean,
    followable: Boolean,
    rel: Option[lila.relation.Relation],
    mark: Option[String]
  )(implicit ctx: Context) = {
    val title = s"${u.username}的个人动态是受保护的"
    views.html.base.layout(
      title = title
    ) {
      main(cls := "box box-pad page-small")(
        h1(title),
        div(
          div(cls := "upt__info")(
            div(cls := "upt__info__top")(
              div(cls := "left")(
                userLink(u, text = mark, withPowerTip = false),
                views.html.user.mini.markArea(u, mark),
                a(cls := "btn-mark-edit", dataIcon := "写", st.title := "修改备注")("修改备注")
              )
            ),
            if (u.engine && !ctx.me.has(u) && !isGranted(_.UserSpy)) {
              div(cls := "upt__info__warning")(trans.thisPlayerUsesChessComputerAssistance())
            } else {
              div(cls := "upt__info__ratings")(u.best8Perfs map { showPerfRating(u, _) })
            }
          ),
          ctx.userId map { myId =>
            frag(
              (myId != u.id && u.enabled) option div(cls := "upt__actions btn-rack")(
                a(dataIcon := "1", cls := "btn-rack__btn", st.title := trans.watchGames.txt(), href := routes.User.tv(u.username))("观看"),
                !blocked option frag(
                  a(dataIcon := "c", cls := "btn-rack__btn", st.title := trans.chat.txt(), href := routes.Message.convo(u.username))(trans.chat.txt()),
                  a(dataIcon := "U", cls := "btn-rack__btn", st.title := trans.challengeToPlay.txt(), href := s"${routes.Lobby.home()}?user=${u.username}#friend")(trans.challengeToPlay.txt())
                ),
                views.html.relation.mini(u.id, blocked, followable, rel)
              )
            )
          }
        ),
        br,
        p("或者可以让他们修改", a(href := routes.Pref.form("privacy"))("隐私设置")),
        br,
        embedJsUnsafe {
          """if (document.referrer) document.write('<a class="button text" data-icon="I" href="' + document.referrer + '">返回</a>');"""
        }
      )
    }
  }

  private val dataUsername = attr("data-username")
}
