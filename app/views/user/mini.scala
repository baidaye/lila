package views.html.user

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User

import controllers.routes

object mini {

  def apply(
    u: User,
    playing: Option[lila.game.Pov],
    blocked: Boolean,
    followable: Boolean,
    rel: Option[lila.relation.Relation],
    isStudent: Boolean,
    mark: Option[String],
    ping: Option[Int],
    teamRating: Option[Int],
    crosstable: Option[lila.game.Crosstable]
  )(implicit ctx: Context) = frag(
    div(cls := "upt__info")(
      div(cls := "upt__info__top")(
        div(cls := "left")(
          userLink(u, text = mark, withPowerTip = false),
          markArea(u, mark),
          a(cls := "btn-mark-edit", dataIcon := "写", title := "修改备注")
        //          u.profileOrDefault.countryInfo map { c =>
        //            val hasRoomForNameText = u.username.size + c.shortName.size < 20
        //            span(
        //              cls := "upt__info__top__country",
        //              title := (!hasRoomForNameText).option(c.name)
        //            )(
        //                img(cls := "flag", src := staticUrl(s"images/flags/${c.code}.png")),
        //                hasRoomForNameText option c.shortName
        //              )
        //          }
        ),
        teamRating map { r => div(cls := "upt__info__teamRating")(r) }
      //        ping map bits.signalBars
      ),
      if (u.engine && !ctx.me.has(u) && !isGranted(_.UserSpy))
        div(cls := "upt__info__warning")(trans.thisPlayerUsesChessComputerAssistance())
      else
        div(cls := "upt__info__ratings")(u.best8Perfs map { showPerfRating(u, _) })
    ),
    ctx.userId map { myId =>
      frag(
        (myId != u.id && u.enabled) option div(cls := "upt__actions btn-rack")(
          a(dataIcon := "1", cls := "btn-rack__btn", title := trans.watchGames.txt(), href := routes.User.tv(u.username)),
          (isStudent && u.isMember) option a(dataIcon := "车", cls := "btn-rack__btn", title := "教学直通车", href := routes.Coach.throughTrain(1, s"$myId@${u.id}", None)),
          !blocked option frag(
            a(dataIcon := "U", cls := "btn-rack__btn", title := trans.challengeToPlay.txt(), href := s"${routes.Lobby.home()}?user=${u.username}#friend"),
            a(dataIcon := "c", cls := "btn-rack__btn", title := trans.chat.txt(), href := routes.Message.convo(u.username))
          ),
          views.html.relation.mini(u.id, blocked, followable, rel)
        ),
        crosstable.flatMap(_.nonEmpty) map { cross =>
          a(
            cls := "upt__score",
            href := s"${routes.User.games(u.username, "me")}#games",
            title := trans.nbGames.pluralTxt(cross.nbGames, cross.nbGames.localize)
          )(trans.yourScore(raw(s"""<strong>${cross.showScore(myId)}</strong> - <strong>${~cross.showOpponentScore(myId)}</strong>""")))
        }
      )
    },
    isGranted(_.UserSpy) option div(cls := "upt__mod")(
      span(
        trans.nbGames.plural(u.count.game, u.count.game.localize),
        " ", momentFromNowOnce(u.createdAt)
      ),
      (u.lameOrTroll || u.disabled) option span(cls := "upt__mod__marks")(mod.userMarks(u, None))
    ),
    (!ctx.pref.isBlindfold) ?? playing map { pov =>
      frag(
        gameFen(pov),
        div(cls := "upt__game-legend")(
          i(dataIcon := pov.game.perfType.map(_.iconChar.toString), cls := "text")(
            pov.game.clock.map(_.config.show)
          ),
          playerText(pov.opponent, withRating = true)
        )
      )
    }
  )

  val dataUsername = attr("data-username")
  def markArea(u: User, oldMark: Option[String])(implicit ctx: Context) = div(cls := "mark-area none")(
    st.input(name := "mark", minlength := 0, maxlength := 20, value := oldMark, placeholder := "备注"),
    a(cls := "btn-mark-save", dataUsername := u.username, href := routes.Relation.mark(u.id))("保存")
  )

}
