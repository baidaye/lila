package views.html
package account

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.pref.PrefCateg

object bits {

  def categName(categ: lila.pref.PrefCateg)(implicit ctx: Context): String = categ match {
    case PrefCateg.GameDisplay => trans.gameDisplay.txt()
    case PrefCateg.ChessClock => trans.chessClock.txt()
    case PrefCateg.GameBehavior => trans.gameBehavior.txt()
    case PrefCateg.Privacy => trans.privacy.txt()
  }

  def checkYourEmail(email: String)(implicit ctx: Context) =
    views.html.base.layout(
      title = "邮箱验证"
    ) {
      main(cls := "page-small box box-pad")(
        h1(cls := "is-green text", dataIcon := "E")("请查看您的电子邮件"),
        p("我们给您的邮箱（", email, "）发了邮箱验证的链接。请点击链接来验证您的邮箱。"),
        p("如果您没有收到我们的邮件, 请检查您的其他收件箱, 例如垃圾箱, 广告, 社交等.")
      )
    }
}
