package views.html
package account

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import play.api.data.Form
import controllers.routes

object confirm {

  val dataTab = attr("data-tab")

  def apply(user: User, cellphoneForm: Form[_], emailForm: Form[_], tp: Option[String] = None, active: Option[String] = None)(implicit ctx: Context) =
    account.layout(
      title = s"${user.username} - 账号验证",
      active = "confirm",
      evenMoreJs = frag(
        smsCaptchaTag,
        jsTag("account.confirm.js")
      )
    ) {
        div(cls := "account account-confirm box box-pad")(
          h1("账号验证"),
          div(cls := "tabs")(
            div(dataTab := "cellphone", cls := List("active" -> (active.isEmpty || active.??(_ === "cellphone"))))("手机绑定"),
            div(dataTab := "email", cls := List("active" -> active.??(_ === "email")))("邮箱绑定")
          ),
          div(cls := "panels")(
            div(cls := List("panel cellphone" -> true, "active" -> (active.isEmpty || active.??(_ === "cellphone"))))(
              tp.??(_ == "cp") || !user.cpExistsAndConfirmed option postForm(cls := "form3", dataSmsrv := true, action := routes.Account.cellphoneConfirm)(
                views.html.base.smsCaptcha(cellphoneForm),
                form3.actions(
                  a(href := s"${routes.Account.confirm()}#cellphone")("取消"),
                  form3.submit("确认")
                )
              ),
              !tp.??(_ == "cp") option user.cellphone.map { cp =>
                div(cls := "show")(
                  span(cls := "fill")(cp.toString),
                  a(cls := "edit", href := s"${routes.Account.confirm()}?tp=cp#cellphone")("[修改]")
                )
              }
            ),
            div(cls := List("panel email" -> true, "active" -> active.??(_ === "email")))(
              tp.??(_ == "em") || !user.emExistsAndConfirmed option postForm(cls := "form3", action := routes.Account.emailSend)(
                form3.group(emailForm("email"), "电子邮箱")(form3.input2(_, typ = "email", vl = user.email.map(_.value))(required)),
                form3.actions(
                  a(href := s"${routes.Account.confirm()}#email")("取消"),
                  form3.submit("确认")
                )
              ),
              !tp.??(_ == "em") option user.email.map { em =>
                div(cls := "show")(
                  span(cls := "fill")(em.conceal),
                  a(cls := "edit", href := s"${routes.Account.confirm()}?tp=em#email")("[修改]")
                )
              }
            )
          )
        )
      }
}
