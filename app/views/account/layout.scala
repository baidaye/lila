package views.html.account

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

import controllers.routes

object layout {

  def apply(
    title: String,
    active: String,
    evenMoreCss: Frag = emptyFrag,
    evenMoreJs: Frag = emptyFrag
  )(body: Frag)(implicit ctx: Context): Frag = views.html.base.layout(
    title = title,
    moreCss = frag(cssTag("account"), evenMoreCss),
    moreJs = frag(jsTag("account.js"), evenMoreJs)
  ) {
      def activeCls(c: String) = cls := active.activeO(c)
      main(cls := "account page-menu")(
        st.nav(cls := "page-menu__menu subnav")(
          lila.pref.PrefCateg.all.map { categ =>
            a(activeCls(categ.slug), href := routes.Pref.form(categ.slug))(
              bits.categName(categ)
            )
          },
          a(activeCls("kid"), href := routes.Account.kid())(
            trans.kidMode()
          ),
          div(cls := "sep"),
          a(activeCls("editProfile"), href := routes.Account.profile(None))(
            trans.editProfile()
          ),
          a(activeCls("member"), href := routes.Member.info)("会员信息"),
          ctx.me.??(_.notImported) option a(activeCls("coach-certify"), href := routes.Coach.certify)("教练认证"),
          isGranted(_.Coach) option a(activeCls("coach-profile"), href := routes.Coach.edit)("教练资料"),
          div(cls := "sep"),
          a(activeCls("password"), href := routes.Account.passwd())("修改密码"),
          ctx.me.??(_.notImported) option a(activeCls("confirm"), href := routes.Account.confirm())("账号验证"),
          a(activeCls("security"), href := routes.Account.security())("安全会话"),
          ctx.me.??(_.isMemberOrCoachOrTeam) option a(activeCls("thirdOAuth2"), href := routes.LichessApi.oAuthList())("lichess.org 授权"),
          div(cls := "sep"),
          ctx.me.??(_.notImported) option a(activeCls("close"), href := routes.Account.close())(
            trans.closeAccount()
          )
        ),
        div(cls := "page-menu__content")(body)
      )
    }
}
