package views.html.base

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

object transfer {

  def apply[L, R](leftOptions: List[L], rightOptions: List[R], leftLabel: String = "备选", rightLabel: String = "已选", search: Boolean = false, chkAll: Boolean = false)(leftFrag: L => Frag)(rightFrag: R => Frag)(implicit ctx: Context) = {
    div(cls := "transfer")(
      div(cls := "transfer-panel left")(
        div(cls := "transfer-panel-head")(
          chkAll option span(
            input(tpe := "checkbox", id := "transfer_chk_all"),
            label(`for` := "transfer_chk_all")("全部"), " "
          ),
          s"$leftLabel(", span(cls := "count")(leftOptions.size), ")"
        ),
        search option div(cls := "transfer-panel-search")(
          input(tpe := "text", cls := "transfer-search", placeholder := "搜索")
        ),
        div(cls := "transfer-panel-list")(
          table(cls := "transfer-table")(
            tbody(
              leftOptions.map { option =>
                leftFrag(option)
              }
            )
          )
        )
      ),
      div(cls := "transfer-buttons")(
        button(cls := "button small arrow-left disabled", dataIcon := "I", disabled),
        button(cls := "button small arrow-right disabled", dataIcon := "H", disabled)
      ),
      div(cls := "transfer-panel right")(
        div(cls := "transfer-panel-head")(
          chkAll option span(
            input(tpe := "checkbox", id := "transfer_chk_all"),
            label(`for` := "transfer_chk_all")("全部"), " "
          ),
          s"$rightLabel(", span(cls := "count")(rightOptions.size), ")"
        ),
        search option div(cls := "transfer-panel-search")(
          input(tpe := "text", cls := "transfer-search", placeholder := "搜索")
        ),
        div(cls := "transfer-panel-list")(
          table(cls := "transfer-table")(
            tbody(
              rightOptions.map { option =>
                rightFrag(option)
              }
            )
          )
        )
      )
    )
  }

}
