package views.html.base

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ Campus, CampusRepo, TeamRepo }
import controllers.routes

object topnav {

  private def linkTitle(url: String, name: Frag)(implicit ctx: Context) =
    if (ctx.blind) h3(name) else a(cls := "nav-toggle", href := url)(name)

  def apply()(implicit ctx: Context) =
    st.nav(id := "topnav", cls := List("mobileMode" -> ctx.isMobileBrowser, s"${ctx.pref.navMode}Mode" -> true))(
      st.section(
        linkTitle(routes.Home.home.toString, frag(
          span(cls := "play")(trans.play()),
          span(cls := "home")("haichess.com")
        )),
        div(role := "group")(
          /*        if (ctx.noBot) a(href := "/lobby?any#hook")(trans.createAGame())
          else a(href := "/lobby?any#friend")(trans.playWithAFriend()),*/
          if (ctx.noBot) a(href := routes.Lobby.home)(trans.createAGame())
          else a(href := routes.Lobby.home)(trans.playWithAFriend()),
          ctx.noBot option frag(
            a(href := routes.Tournament.home())(trans.tournaments()),
            ctx.isAuth option a(href := controllers.rt_contest.routes.Contest.home)("个人赛"),
            ctx.isAuth option a(href := controllers.rt_contest.routes.TeamContest.home)("团体赛"),
            a(href := routes.Simul.home)(trans.simultaneousExhibitions()),
            a(href := routes.Racer.home)("战术竞速")
          ),
          a(href := routes.Tv.games)("当前对局")
        )
      ),
      st.section(
        linkTitle(routes.Puzzle.home.toString, trans.learnMenu()),
        div(role := "group")(
          ctx.noBot option frag(
            a(href := routes.Puzzle.home)("战术训练"),
            a(href := routes.Puzzle.themePuzzleHome)("主题战术"),
            a(href := routes.PuzzleRush.show)("战术冲刺"),
            a(href := controllers.rt_patterns.routes.Patterns.index)("将杀模式", views.html.member.bits.vTip),
            a(href := routes.Practice.index)("练习")
          ),
          a(href := routes.Study.allDefault(1))("研习"),
          ctx.isAuth option a(href := routes.Recall.home())("记谱"),
          /*,
          a(href := routes.Coach.allDefault(1))(trans.coaches())*/
          ctx.noBot option frag(
            a(href := routes.CoordTrain.home)("坐标训练"),
            a(href := routes.Distinguish.home)("棋谱记录")
          )
        )
      ),
      st.section(
        linkTitle(routes.Learn.index.toString, "趣味"),
        div(role := "group")(
          a(href := routes.Learn.index)(trans.chessBasics()),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.AnnihilationStar.id))("吃星星"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.AnnihilationPiece.id))("消灭对手"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.CaptureKing.id))("单子攻王"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.UnprotectedPieces.id))("未保护棋子"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.UnprotectedSquares.id))("未保护格子"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.DualRooksAndBishops.id))("双车捉双象（PC）"),
          a(href := controllers.rt_interest.routes.Interest.home(lila.interest.Source.PawnsOnly.id))("小兵大战（PC）")
        )
      ),
      /*    st.section(
        linkTitle(routes.Tv.index.toString, trans.watch()),
        div(role := "group")(
          a(href := routes.Tv.index)("嗨棋 TV"),
          a(href := routes.Tv.games)(trans.currentGames()),
          a(href := routes.Streamer.index())("主播"),
          a(href := routes.Relay.index())("广播"),
          ctx.noBot option a(href := routes.Video.index)(trans.videoLibrary())
        )
      ),
      st.section(
        linkTitle(routes.User.list.toString, trans.community()),
        div(role := "group")(
          a(href := routes.User.list)(trans.players()),
          NotForKids(frag(
            a(href := routes.Team.home())(trans.teams()),
            a(href := routes.ForumCateg.index)(trans.forum())
          ))
        )
      ),*/
      st.section(
        linkTitle(routes.UserAnalysis.index.toString, trans.tools()),
        div(role := "group")(
          a(href := routes.UserAnalysis.index)(trans.analysis()),
          a(href := controllers.rt_demonstrate.routes.Demonstrate.home)("挂盘演示"),
          a(href := s"${routes.UserAnalysis.index}#explorer")(trans.openingExplorer()),
          a(href := routes.Editor.index)(trans.boardEditor()),
          a(href := routes.Search.index())(trans.advancedSearch()),
          ctx.me.map { me =>
            isGranted(_.Coach) || me.hasTeam option a(href := controllers.rt_contest.routes.OffContest.home())("比赛编排")
          }
        )
      ),
      st.section(
        linkTitle(controllers.rt_resource.routes.Resource.puzzleLiked().toString, "资源"),
        div(role := "group")(
          a(href := controllers.rt_resource.routes.Resource.puzzleLiked(1))("收藏"),
          a(href := routes.Errors.puzzle(1))("错题库"),
          a(href := controllers.rt_resource.routes.Resource.puzzleImported())("战术题", views.html.member.bits.vTip),
          a(href := controllers.rt_resource.routes.OpeningDB.minePage(1))("开局", views.html.member.bits.vTip),
          a(href := controllers.rt_resource.routes.Resource.gameImported())("对局", views.html.member.bits.vTip),
          a(href := controllers.rt_resource.routes.SituationDB.home())("局面", views.html.member.bits.vTip),
          a(href := controllers.rt_resource.routes.Interest.home(lila.interest.Source.AnnihilationStar.id))("趣味", views.html.member.bits.vTip)
        )
      ),
      ctx.me.map { me =>
        st.section(
          linkTitle(routes.User.gamesAll(me.username).toString, "我的"),
          div(role := "group")(
            a(href := routes.User.gamesAll(me.username))("动态"),
            a(href := controllers.rt_task.routes.TTask.current(1))("任务"),
            a(href := routes.Message.home())("信箱"),
            a(href := routes.Relation.following(me.username))("好友"),
            a(href := controllers.rt_klazz.routes.Clazz.current())("班级"),
            me.belongTeamId map { teamId =>
              a(href := controllers.rt_team.routes.Team.show(teamId))("俱乐部")
            } getOrElse a(href := controllers.rt_team.routes.Team.all())("俱乐部"),
            /*a(href := routes.ForumCateg.index)("讨论区")*/
            a(href := routes.Insight.index(me.username))("数据洞察", views.html.member.bits.vTip),
            /*a(href := routes.ForumCateg.index)("讨论区")*/
            ctx.me.map { me =>
              isGranted(_.Coach) || isGranted(_.Team) option a(href := s"${controllers.rt_team.routes.TeamCooperator.cooperates()}")("邀请")
            }
          )
        )
      },
      ctx.me.map { me =>
        isGranted(_.Coach) option st.section(
          linkTitle(routes.Coach.showById(me.id).toString, "教练"),
          div(role := "group")(
            a(href := routes.Coach.showById(me.id))("个人主页"),
            a(href := controllers.rt_klazz.routes.Clazz.current())("班级"),
            a(href := routes.Coach.approvedStuList())("学员"),
            a(href := controllers.rt_klazz.routes.Course.timetable(0))("课程表"),
            isGranted(_.Coach) option a(href := s"${routes.MemberCard.page(1)}?source=coach")("会员卡")
          )
        )
      },
      ctx.me.map { me =>
        frag(
          me.hasTeam option st.section(
            linkTitle(controllers.rt_team.routes.Team.show(me.teamIdValue).toString, "俱乐部"),
            div(role := "group")(
              a(href := controllers.rt_team.routes.TeamMember.members(me.teamIdValue, 1))("成员"),
              isGranted(_.Team) option a(href := s"${controllers.rt_team.routes.Team.clazzTable(me.teamIdValue)}?campus=${Campus.defaultId(me.teamIdValue)}")("班级"),
              TeamRepo.byId(me.teamIdValue).awaitSeconds(3) map { team =>
                frag(
                  if (!team.ratingSettingOrDefault.open) a(href := s"${controllers.rt_team.routes.Team.setting(team.id)}#rating")("等级分") else a(href := controllers.rt_team.routes.TeamRating.ratingDistribution(team.id))("等级分"),
                  if (!team.coinSettingOrDefault.open) a(href := s"${controllers.rt_team.routes.Team.setting(team.id)}#coin")("积分") else a(href := controllers.rt_team.routes.TeamCoin.coinMembers(team.id))("积分")
                )
              },
              isGranted(_.Team) option a(href := controllers.rt_team.routes.TeamClockIn.current(1, me.teamIdValue, None))("打卡任务"),
              isGranted(_.Team) option a(href := controllers.rt_team.routes.TeamTest.current())("战术测评"),
              a(href := controllers.rt_team.routes.Team.setting(me.teamIdValue))("设置"),
              isGranted(_.Team) option a(href := s"${routes.MemberCard.page(1)}?source=team")("会员卡"),
              isGranted(_.Team) option a(href := controllers.rt_team.routes.TeamFederation.mineList())("联盟")
            )
          ),
          !me.hasTeam && isGranted(_.TeamCampus) option {
            me.headCampusAdmin.map { campusId =>
              st.section(
                linkTitle(controllers.rt_team.routes.Team.show(Campus.toTeamId(campusId)).toString, "俱乐部"),
                div(role := "group")(
                  a(href := s"${controllers.rt_team.routes.TeamMember.members(Campus.toTeamId(campusId), 1)}?campus=$campusId")("成员"),
                  a(href := s"${controllers.rt_team.routes.Team.clazzTable(Campus.toTeamId(campusId))}?campus=$campusId")("班级")
                )
              )
            }
          }
        )
      }
    )
}
