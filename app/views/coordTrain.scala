package views.html

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.libs.json.{ JsObject, Json }
import lila.common.String.html.safeJsonValue
import lila.pref.Pref.Color
import controllers.routes

object coordTrain {

  def apply(scores: JsObject, pref: JsObject, auto: Boolean = false, fr: Option[String] = None)(implicit ctx: Context) = views.html.base.layout(
    title = "坐标训练",
    moreCss = cssTag("coordTrain"),
    moreJs = frag(
      jsTag("vendor/sparkline.min.js"),
      jsAt(s"compiled/lichess.coordTrain${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""lichess=lichess||{};lichess.coordTrain=${
        safeJsonValue(Json.obj(
          "userId" -> ctx.userId,
          "scores" -> scores,
          "pref" -> pref,
          "auto" -> auto,
          "fr" -> fr
        ))
      }""")
    ),
    zoomable = true
  ) {
      main(cls := "coord-train")(
        st.aside(cls := "coord-train__side"),
        div(cls := "coord-train__board main-board")(chessgroundBoard),
        div(cls := "coord-train__table"),
        div(cls := "coord-train__progress")(
          div(cls := "progress_bar")
        )
      )
    }

  /*def home(scoreOption: Option[lila.coordTrain.Score])(implicit ctx: Context) =
    views.html.base.layout(
      title = trans.coordinates.coordinateTraining.txt(),
      moreCss = cssTag("coordTrain"),
      moreJs = frag(
        jsTag("vendor/sparkline.min.js"),
        jsAt("compiled/coordinate.js")
      ),
      openGraph = lila.app.ui.OpenGraph(
        title = "Chess board coordinates trainer",
        url = s"$netBaseUrl${routes.CoordTrain.home.url}",
        description = "Knowing the chessboard coordinates is a very important chess skill. A square name appears on the board and you must click on the correct square."
      ).some,
      zoomable = true
    )(
        main(
          id := "trainer",
          cls := "coord-trainer training init",
          attr("data-color-pref") := ctx.pref.coordColorName,
          attr("data-score-url") := ctx.isAuth.option(routes.Coordinate.score().url)
        )(
            div(cls := "coord-trainer__side")(
              div(cls := "box")(
                h1(trans.coordinates.coordinates()),
                if (ctx.isAuth) scoreOption.map { score =>
                  div(cls := "scores")(scoreCharts(score))
                }
                else div(cls := "register")(
                  p(trans.toTrackYourProgress()),
                  p(cls := "signup")(
                    a(cls := "button", href := routes.Auth.signup)(trans.signUp())
                  )
                )
              ),
              form(cls := "color buttons", action := routes.Coordinate.color)(
                st.group(cls := "radio")(
                  List(Color.BLACK, Color.RANDOM, Color.WHITE).map { id =>
                    div(
                      input(
                        tpe := "radio",
                        st.id := s"coord_color_$id",
                        name := "color",
                        value := id,
                        (id == ctx.pref.coordColor) option checked
                      ),
                      label(`for` := s"coord_color_$id", cls := s"color color_$id", title := (if (id == 1) "白方" else if (id == 2) "随机" else if (id == 3) "黑方" else ""))(i)
                    )
                  }
                )
              ),
              form(cls := "coord_show buttons")(
                st.group(cls := "radio")(
                  List(0 -> "不显示坐标", 1 -> "显示坐标").map {
                    case (id, name) => {
                      div(
                        input(
                          tpe := "radio",
                          st.id := s"coord_show_$id",
                          st.name := "coordShow",
                          value := id,
                          (id == 0) option checked
                        ),
                        label(`for` := s"coord_show_$id")(name)
                      )
                    }
                  }
                )
              )
            ),
            div(cls := "coord-trainer__board main-board")(
              div(cls := "next_coord", id := "next_coord0"),
              div(cls := "next_coord", id := "next_coord1"),
              div(cls := "next_coord", id := "next_coord2"),
              chessgroundBoard
            ),
            div(cls := "coord-trainer__table")(
              div(cls := "explanation")(
                p(trans.coordinates.knowingTheChessBoard()),
                ul(
                  li(trans.coordinates.mostChessCourses()),
                  li(trans.coordinates.talkToYourChessFriends()),
                  li(trans.coordinates.youCanAnalyseAGameMoreEffectively())
                ),
                p(trans.coordinates.aSquareNameAppears())
              ),
              button(cls := "start button button-fat")(trans.coordinates.startTraining())
            ),
            div(cls := "coord-trainer__score")(0),
            div(cls := "coord-trainer__progress")(div(cls := "progress_bar"))
          )
      )

  def scoreCharts(score: lila.coordTrain.Score)(implicit ctx: Context) = frag(
    List((trans.coordinates.averageScoreAsWhiteX, score.white), (trans.coordinates.averageScoreAsBlackX, score.black)).map {
      case (averageScoreX, s) => div(cls := "chart_container")(
        s.nonEmpty option frag(
          p(averageScoreX(raw(s"""<strong>${"%.2f".format(s.sum.toDouble / s.size)}</strong>"""))),
          div(cls := "user_chart", attr("data-points") := safeJsonValue(Json toJson s))
        )
      )
    }
  )*/
}
