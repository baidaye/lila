package views.html.task

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.Team
import lila.task.TTask
import lila.task.TTask.Source
import org.joda.time.DateTime
import play.api.data.Form
import controllers.routes

object info {

  private val dataIsStudent = attr("data-is-student")

  def apply(task: TTask, team: Option[Team])(implicit ctx: Context) =
    views.html.base.layout(
      title = "任务详情",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("ttask")
      ),
      moreJs = frag(
        flatpickrTag,
        tagsinputTag,
        jsTag("resource.saveTo.js"),
        jsTag("task.info.js")
      )
    ) {
        main(cls := "box box-pad page-small task-info", dataIsStudent := ctx.me.??(task.belongTo))(
          h1(task.name),
          table(cls := "desc-table")(
            tr(
              th("任务来源"),
              td(a(href := sourceLink(task))(task.sourceRel.name), " ", strong(task.sourceRel.source.name))
            ),
            task.hasTag option tr(
              th("条件"),
              td(bits.buildCondTags(task.itemType, task.item))
            ),
            tr(
              th("附言"),
              td(task.remark | "无")
            ),
            tr(
              th("状态"),
              td(cls := List(
                "complete" -> task.isFinished,
                "training" -> task.isTrain,
                "uncomplete" -> task.isCreated,
                "canceled" -> (task.isExpired || task.isCanceled)
              ))(task.status.name)
            ),
            tr(
              th("进度"),
              td(task.progress, "%")
            ),
            (task.hasCoin && task.isFinished) option {
              team.map { team =>
                task.coinDiff.map { coinDiff =>
                  tr(
                    th(team.coinSettingOrDefault.name),
                    td("+", coinDiff)
                  )
                }
              }
            },
            (task.hasCoin && !task.isFinished) option {
              team.map { team =>
                task.coinRule.map { coinRule =>
                  tr(
                    th(team.coinSettingOrDefault.name),
                    td("+", coinRule, s"（完成本次任务后应得${team.coinSettingOrDefault.name}）")
                  )
                }
              }
            },
            task.deadlineAt.map { da =>
              tr(
                th("截止时间"),
                td(da.toString("yyyy-MM-dd HH:mm"))
              )
            },
            tr(
              th("计划时间"),
              td(task.planAt.map { sa =>
                div(
                  sa.toString("yyyy-MM-dd HH:mm"),
                  nbsp, nbsp,
                  !task.isDeadlined && ctx.me.??(task.belongTo) option a(cls := "modal-alert task-planAt", href := controllers.rt_task.routes.TTask.updatePlanAtModal(task.id))("修改")
                )
              }.getOrElse(if (!task.isDeadlined && ctx.me.??(task.belongTo)) a(cls := "modal-alert task-planAt", href := controllers.rt_task.routes.TTask.updatePlanAtModal(task.id))("添加到日程表") else "-"))
            ),
            ctx.me.??(task.belongTo) option tr(
              th,
              td(toFinishButton(task, false, true))
            )
          ),
          div(cls := "flat")(bits.flatTask(task))
        )
      }

  def toFinishButton(task: TTask, small: Boolean, flat: Boolean)(implicit ctx: Context) = {
    a(
      cls := List(
        "button button-green " -> true,
        "small" -> small,
        "toLink" -> flat,
        "disabled" -> !task.canSolve,
        "fromPositionModal" -> (task.canSolve && task.itemType == TTask.TTaskItemType.FromPositionItem),
        "fromPgnModal" -> (task.canSolve && (task.itemType == TTask.TTaskItemType.FromPgnItem)),
        "isAi" -> (task.canSolve && (task.item.fromPosition.?? { fp => fp.firstUnComplete.??(_.ai) } || task.item.fromPgn.?? { fp => fp.firstUnComplete.??(_.ai) }))
      ),
      style := (if (small) "margin-left: 1em;" else ""),
      href := (if (task.canSolve) { controllers.rt_task.routes.TTask.doTask(task.id).toString } else "")
    )("去完成")
  }

  private def sourceLink(task: TTask): String =
    task.sourceRel.source match {
      case Source.Homework => controllers.rt_klazz.routes.Homework.show(task.sourceRel.id).toString
      case Source.TrainCourse => controllers.rt_trainCourse.routes.TrainCourse.show(task.sourceRel.id).toString
      case Source.TeamClockIn => controllers.rt_team.routes.TeamClockIn.joinOf(task.sourceRel.id).toString
      case Source.ThroughTrain => routes.Coach.throughTrain(1, task.sourceRel.id, None).toString
      case Source.TeamTest => (task.item.teamTest.map { tt => controllers.rt_team.routes.TeamTest.testStuShow(tt.stuTestId).toString } | "")
    }

  def formPositionModal(fen: String, limit: Int, increment: Int, aiLevel: Option[Int], color: Option[String], canTakeback: Option[Boolean])(implicit ctx: Context) =
    div(cls := "modal-formPosition none")(
      a(cls := "button", target := "_blank", href := routes.Setup.customAi(limit, increment, aiLevel | 1, fen.some, None, color, canTakeback, None, None, None, None, None, None))("和机器下棋"),
      a(cls := "button", target := "_blank", href := routes.Setup.customFriend(limit, increment, fen.some, None, color, None, canTakeback))("和朋友下棋")
    )

  def formPgnModal(
    pgn: String,
    limit: Int,
    increment: Int,
    aiLevel: Option[Int],
    color: Option[String],
    canTakeback: Option[Boolean],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean]
  )(implicit ctx: Context) =
    div(cls := "modal-formPgn none")(
      a(cls := "button", target := "_blank", href := routes.Setup.customAi(limit, increment, aiLevel | 1, None, pgn.some, color, canTakeback,
        openingdbId, openingdbExcludeWhiteBlunder, openingdbExcludeBlackBlunder, openingdbExcludeWhiteJscx, openingdbExcludeBlackJscx, openingdbCanOff))("和机器下棋"),
      a(cls := "button", target := "_blank", href := routes.Setup.customFriend(limit, increment, None, pgn.some, color, None, canTakeback))("和朋友下棋")
    )

  def updatePlanAtModal(task: TTask, form: Form[_])(implicit ctx: Context) = {
    val now = DateTime.now
    div(cls := "modal-content modal-planAt none")(
      h2("计划时间"),
      postForm(cls := "form3 form-planAt", action := controllers.rt_task.routes.TTask.updatePlanAt(task.id))(
        form3.group(form("planAt"), raw(""))(f =>
          form3.input3(f, vl = (task.planAt | now.plusMinutes(3)).toString("yyyy-MM-dd HH:mm").some, klass = "flatpickr")(
            dataEnableTime := true,
            datatime24h := true
          )(dataMinDate := now.toString("yyyy-MM-dd HH:mm"), dataMaxDate := (task.deadlineAt | now.plusDays(30)).toString("yyyy-MM-dd HH:mm"))),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  }

}
