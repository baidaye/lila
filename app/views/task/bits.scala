package views.html.task

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.task.{ CoordTrainItem, GameItem, PuzzleRushItem, TTask, TTaskItem, ThemePuzzleItem }
import lila.task.TTask.{ Source, TTaskItemType }
import lila.resource.ThemeQuery
import play.api.libs.json.{ JsArray, Json }
import controllers.routes
import views.html.analyse.replay.{ dataRel, dataSource }

object bits {

  private val dataLastmove = attr("data-lastmove")
  private val moveTag = tag("move")
  private val indexTag = tag("index")

  def themePuzzleJson() =
    Json.obj(
      "pieceColor" -> JsArray(
        ThemeQuery.pieceColor.map { pieceColor =>
          Json.obj(
            "id" -> pieceColor._1,
            "name" -> pieceColor._2
          )
        }
      ),
      "phase" -> JsArray(
        ThemeQuery.phase.map { phase =>
          Json.obj(
            "id" -> phase._1,
            "name" -> phase._2
          )
        }
      ),
      "moveFor" -> JsArray(
        ThemeQuery.moveFor.map { moveFor =>
          Json.obj(
            "id" -> moveFor._1,
            "name" -> moveFor._2
          )
        }
      ),
      "strength" -> JsArray(
        ThemeQuery.strength.map { strength =>
          Json.obj(
            "id" -> strength._1,
            "name" -> strength._2
          )
        }
      ),
      "chessGame" -> JsArray(
        ThemeQuery.chessGame.map { chessGame =>
          Json.obj(
            "id" -> chessGame._1,
            "name" -> chessGame._2
          )
        }
      ),
      "subject" -> JsArray(
        ThemeQuery.subject.map { subject =>
          Json.obj(
            "id" -> subject._1,
            "name" -> subject._2
          )
        }
      ),
      "comprehensive" -> JsArray(
        ThemeQuery.comprehensive.map { comprehensive =>
          Json.obj(
            "id" -> comprehensive._1,
            "name" -> comprehensive._2
          )
        }
      )
    )

  def buildCondTags(itemType: TTaskItemType, item: TTaskItem): Frag =
    itemType match {
      case TTaskItemType.ThemePuzzleItem =>
        item.themePuzzle.map { tp =>
          buildThemePuzzleTags(tp.themePuzzle)
        }
      case TTaskItemType.PuzzleRushItem => {
        item.puzzleRush.map { pr =>
          buildRushTags(pr.puzzleRush)
        }
      }
      case TTaskItemType.CoordTrainItem => {
        item.coordTrain.map { ct =>
          buildCoordTrainTags(ct.coordTrain)
        }
      }
      case TTaskItemType.GameItem => {
        item.game.map { gm =>
          buildGameTags(gm.gameItem)
        }
      }
      case _ => emptyFrag
    }

  def buildThemePuzzleTags(themePuzzle: ThemePuzzleItem) = {
    div(cls := "tags")(
      themePuzzle.buildTags().map(span(_))
    )
  }

  def buildRushTags(puzzleRush: PuzzleRushItem) = {
    div(cls := "tags")(
      puzzleRush.buildTags().map(span(_))
    )
  }

  def buildCoordTrainTags(coordTrain: CoordTrainItem) = {
    div(cls := "tags")(
      coordTrain.buildTags().map(span(_))
    )
  }

  def buildGameTags(game: GameItem) = {
    div(cls := "tags")(
      game.buildTags().map(span(_))
    )
  }

  def flatTask(task: TTask, withHeader: Boolean = false, frag: Frag = emptyFrag)(implicit ctx: Context) =
    task.itemType match {
      case TTask.TTaskItemType.CapsulePuzzleItem => capsulePuzzle(task)
      case TTask.TTaskItemType.ReplayGameItem => replayGame(task)
      case TTask.TTaskItemType.RecallGameItem => recallGame(task)
      case TTask.TTaskItemType.DistinguishGameItem => distinguishGame(task)
      case TTask.TTaskItemType.FromPositionItem => fromPositionGame(task)
      case TTask.TTaskItemType.FromPgnItem => fromPgnGame(task)
      case TTask.TTaskItemType.FromOpeningdbItem => fromOpeningdbGame(task)
      case _ => div().some
    }

  def capsulePuzzle(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag, showPuzzleFirstComplete: Boolean = false)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.capsulePuzzle.map { capsulePuzzle =>
      div(cls := "task-flat capsulePuzzle")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          div(cls := "puzzles")(
            capsulePuzzle.puzzles.map { p =>
              val puzzle = p.puzzle
              val firstWrongSolution = showPuzzleFirstComplete.?? { p.firstWrongMoveUci.??(_.mkString(" ")) }
              val link = controllers.rt_task.routes.TTask.doTask(task.id, puzzle.id.toString.some).toString
              div(cls := "puzzle")(
                linkTag(
                  cls := "mini-board cg-wrap parse-fen-manual is2d",
                  dataColor := puzzle.color.name,
                  dataFen := puzzle.fen,
                  dataLastmove := puzzle.lastMove,
                  dataPlayable := "1",
                  dataSolution := firstWrongSolution,
                  dataSolutionColor := "red",
                  target := "_blank",
                  href := link
                )(cgWrapContent),
                div(cls := "result")(
                  if (p.isTry) {
                    showPuzzleFirstComplete match {
                      case true => { // 关注首次完成情况
                        p.isFirstComplete match {
                          case true => div(cls := "complete flex")(
                            linkTag(target := "_blank", href := link)("正确")
                          )
                          case false => div(cls := "error flex")(
                            linkTag(target := "_blank", href := link)("错误"),
                            !p.isFirstMoveRight option p.firstWrongMove.map(span(cls := "error")(_))
                          )
                        }
                      }
                      case false => { // 不关注首次完成情况
                        p.isComplete match {
                          case true => div(cls := "complete flex")(
                            linkTag(target := "_blank", href := link)("正确")
                          )
                          case false => div(cls := "error flex")(
                            linkTag(target := "_blank", href := link)("错误")
                          )
                        }
                      }
                    }
                  } else linkTag(cls := "uncomplete", target := "_blank", href := link)("做题")
                )
              )
            }
          )
        )
      )
    }
  }

  def replayGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.replayGame.map { replayGame =>
      div(cls := "task-flat replayGames")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              replayGame.replayGames.map { r =>
                val replayGame = r.replayGame
                val link = controllers.rt_task.routes.TTask.doTask(task.id, replayGame.chapterLink.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := replayGame.root,
                      target := "_blank",
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    linkTag(cls := List("complete" -> r.result.??(_.win), "uncomplete" -> !r.result.??(_.win)), target := "_blank", href := link)(
                      strong(replayGame.name, if (r.result.??(_.win)) "（已完成）" else "（未完成）")
                    ),
                    div(cls := "moves")(
                      replayGame.moves.map { move =>
                        moveTag(
                          indexTag(move.index, "."),
                          move.white.map { w =>
                            span(dataFen := w.fen)(w.san)
                          } getOrElse span(cls := "disabled")("..."),
                          move.black.map { b =>
                            span(dataFen := b.fen)(b.san)
                          } getOrElse span(cls := "disabled")
                        )
                      }
                    ),
                    div(cls := "actions")(
                      saveTo(task, replayGame.simplePgn)
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def recallGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.recallGame.map { recallGame =>
      div(cls := "task-flat recallGames")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              recallGame.recallGames.map { r =>
                val recallGame = r.recallGame
                val link = controllers.rt_task.routes.TTask.doTask(task.id, recallGame.hashMD5.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := recallGame.color.fold("white")(_.name),
                      dataFen := recallGame.root,
                      target := "_blank",
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    linkTag(cls := List("complete" -> r.result.??(_.win), "uncomplete" -> !r.result.??(_.win)), target := "_blank", href := link)(
                      strong(
                        recallGame.name.map { name =>
                          frag(span(name), br)
                        },
                        span("棋色：", recallGame.color.map { c => if (c.name == "white") "白方" else "黑方" } | "双方"),
                        nbsp, nbsp,
                        recallGame.orient.map { orient =>
                          frag(
                            span("棋盘方向：", if (orient == "white") "白方" else "黑方"),
                            nbsp, nbsp
                          )
                        },
                        span("回合数：", r.result.fold(0)(_.turns), "/", recallGame.turns.map(_.toString) | "所有"),
                        if (r.result.??(_.win)) "（已完成）" else "（未完成）"
                      )
                    ),
                    recallGame.moves.isEmpty || recallGame.moves.??(_.isEmpty) option div(cls := "pgn")(
                      strong(
                        recallGame.pgn.split("\r\n\r\n")(1)
                      )
                    ),
                    recallGame.moves.map { moves =>
                      moves.nonEmpty option div(cls := "moves")(
                        moves.map { move =>
                          moveTag(
                            indexTag(move.index, "."),
                            move.white.map { w =>
                              span(dataFen := w.fen)(w.san)
                            } getOrElse span(cls := "disabled")("..."),
                            move.black.map { b =>
                              span(dataFen := b.fen)(b.san)
                            } getOrElse span(cls := "disabled")
                          )
                        }
                      )
                    },
                    div(cls := "actions")(
                      recallGame.simplePgn.map(pgn => saveTo(task, pgn))
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def distinguishGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.distinguishGame.map { distinguishGame =>
      div(cls := "task-flat distinguishGames")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              distinguishGame.distinguishGames.map { r =>
                val distinguishGame = r.distinguishGame
                val link = controllers.rt_task.routes.TTask.doTask(task.id, distinguishGame.hashMD5.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := distinguishGame.orient | "white",
                      dataFen := distinguishGame.root,
                      target := "_blank",
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    linkTag(cls := List("complete" -> r.result.??(_.win), "uncomplete" -> !r.result.??(_.win)), target := "_blank", href := link)(
                      strong(
                        span(distinguishGame.name), br,
                        span("棋盘方向：", distinguishGame.orient.map { c => if (c == "white") "白方" else "黑方" }),
                        nbsp, nbsp,
                        span("走棋步数：", r.result.fold(0)(_.turns), "/", distinguishGame.rightTurns, "/", distinguishGame.turns.map(_.toString) | "所有"),
                        if (r.result.??(_.win)) "（已完成）" else "（未完成）"
                      )
                    ),
                    distinguishGame.moves.isEmpty || distinguishGame.moves.??(_.isEmpty) option div(cls := "pgn")(
                      strong(
                        distinguishGame.pgn.split("\r\n\r\n")(1)
                      )
                    ),
                    distinguishGame.moves.map { moves =>
                      moves.nonEmpty option div(cls := "moves")(
                        moves.map { move =>
                          moveTag(
                            indexTag(move.index, "."),
                            move.white.map { w =>
                              span(dataFen := w.fen)(w.san)
                            } getOrElse span(cls := "disabled")("..."),
                            move.black.map { b =>
                              span(dataFen := b.fen)(b.san)
                            } getOrElse span(cls := "disabled")
                          )
                        }
                      )
                    },
                    div(cls := "actions")(
                      distinguishGame.simplePgn.map(pgn => saveTo(task, pgn))
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def fromPositionGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.fromPosition.map { fromPosition =>
      div(cls := "task-flat fromPositions")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              fromPosition.fromPositions.map { f =>
                val fromPosition = f.fromPosition
                val result = f.result
                val link = controllers.rt_task.routes.TTask.doTask(task.id, fromPosition.hashMD5.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := List("mini-board cg-wrap parse-fen is2d fromPositionModal" -> true, "isAi" -> f.ai),
                      dataColor := "white",
                      dataFen := fromPosition.fen,
                      target := (if (f.ai) "_self" else "_blank"),
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    div(
                      fromPosition.clock.show,
                      nbsp, nbsp,
                      fromPosition.color.map { c => span(c.fold("执白", "执黑")) } | span("随机"),
                      fromPosition.isAi.??(isAi => isAi) option fromPosition.aiLevel.map { lv => frag(nbsp, nbsp, span(s"A.I.${lv}级")) },
                      fromPosition.chessStatus.map { s =>
                        frag(
                          nbsp, nbsp, "对局结果：",
                          s match {
                            case "all" => "不限定"
                            case "win" => "胜"
                            case "winOrDraw" => "胜或和"
                          }
                        )
                      },
                      nbsp, nbsp,
                      label("完成进度："),
                      strong(
                        span(s"${f.completeSize()}/${fromPosition.num}"),
                        linkTag(cls := List("fromPositionModal" -> true, "complete" -> f.isComplete, "uncomplete" -> !f.isComplete, "isAi" -> f.ai), target := (if (f.ai) "_self" else "_blank"), href := link)(if (f.isComplete()) "（已完成）" else "（未完成）")
                      )
                    ),
                    result.map { result =>
                      ul(cls := "games")(
                        result.map { g =>
                          val complete = f.sameChessStatus(g)
                          li(
                            a(target := "_blank", href := routes.Round.watcher(g.gameId, "white"))(
                              span(g.white),
                              nbsp,
                              iconTag("U"),
                              nbsp,
                              span(g.black),
                              g.resultText.map(t => span(cls := List("complete" -> complete, "error" -> !complete))(s"（$t）"))
                            )
                          )
                        }
                      )
                    }
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def fromPgnGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.fromPgn.map { fromPgn =>
      div(cls := "task-flat fromPgns")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              fromPgn.fromPgns.map { f =>
                val fromPgn = f.fromPgn
                val result = f.result
                val link = controllers.rt_task.routes.TTask.doTask(task.id, fromPgn.hashMD5.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := List("mini-board cg-wrap parse-fen is2d fromPgnModal" -> true, "isAi" -> f.ai),
                      dataColor := "white",
                      dataFen := fromPgn.pgnSetup.lastFen,
                      target := (if (f.ai) "_self" else "_blank"),
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    div(
                      fromPgn.clock.show,
                      nbsp, nbsp,
                      fromPgn.color.map { c => span(c.fold("执白", "执黑")) } | span("随机"),
                      fromPgn.isAi.??(isAi => isAi) option fromPgn.aiLevel.map { lv => frag(nbsp, nbsp, span(s"A.I.${lv}级")) },
                      fromPgn.chessStatus.map { s =>
                        frag(
                          nbsp, nbsp, "对局结果：",
                          s match {
                            case "all" => "不限定"
                            case "win" => "胜"
                            case "winOrDraw" => "胜或和"
                          }
                        )
                      },
                      nbsp, nbsp,
                      label("完成进度："),
                      strong(
                        span(s"${f.completeSize()}/${fromPgn.num}"),
                        linkTag(cls := List("fromPgnModal" -> true, "complete" -> f.isComplete, "uncomplete" -> !f.isComplete, "isAi" -> f.ai), target := (if (f.ai) "_self" else "_blank"), href := link)(if (f.isComplete()) "（已完成）" else "（未完成）")
                      )
                    ),
                    fromPgn.opening match {
                      case Left(pgn) => {
                        val moves = fromPgn.pgnSetup.pgnOpeningDB.toTurns()
                        div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.number, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen.value)(w.move.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen.value)(b.move.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                      case Right(openingdb) => views.html.resource.openingdb.bits.showOpeningdb(fromPgn.pgnSetup)
                    },
                    result.map { result =>
                      ul(cls := "games")(
                        result.map { g =>
                          val complete = f.sameChessStatus(g)
                          li(
                            a(target := "_blank", href := routes.Round.watcher(g.gameId, "white"))(
                              span(g.white),
                              nbsp,
                              iconTag("U"),
                              nbsp,
                              span(g.black),
                              g.resultText.map(t => span(cls := List("complete" -> complete, "error" -> !complete))(s"（$t）"))
                            )
                          )
                        }
                      )
                    }
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def fromOpeningdbGame(task: TTask, withHeader: Option[Int] = None, actionFrag: Frag = emptyFrag)(implicit ctx: Context) = {
    val isTaskOwner = ctx.userId.??(task.userIds.contains(_))
    val linkTag = if (isTaskOwner) tag("a") else tag("span")
    task.item.fromOpeningdb.map { fromOpeningdb =>
      div(cls := "task-flat fromOpeningdbs")(
        withHeader.map { index =>
          div(cls := "task-header")(
            h3(s"$index、", task.name, isTaskOwner option toFinishButton(task)),
            div(cls := "task-header-action")(actionFrag)
          )
        },
        div(cls := "task-body")(
          table(cls := "table-board")(
            tbody(
              fromOpeningdb.fromOpeningdbs.map { f =>
                val fromOpeningdb = f.fromOpeningdb
                val result = f.result
                val link = controllers.rt_task.routes.TTask.doTask(task.id, fromOpeningdb.hashMD5.toString.some).toString
                tr(
                  td(cls := "td-board")(
                    linkTag(
                      cls := List("mini-board cg-wrap parse-fen is2d fromOpeningdbModal" -> true, "isAi" -> f.ai, "notAccept" -> (!task.isFree && !ctx.me.??(_.isMemberOrCoachOrTeam))),
                      dataColor := "white",
                      dataFen := fromOpeningdb.pgnSetup.lastFen,
                      target := (if (f.ai) "_self" else "_blank"),
                      href := link
                    )(cgWrapContent)
                  ),
                  td(
                    div(
                      fromOpeningdb.clock.show,
                      nbsp, nbsp,
                      fromOpeningdb.color.map { c => span(c.fold("执白", "执黑")) } | span("随机"),
                      fromOpeningdb.isAi.??(isAi => isAi) option fromOpeningdb.aiLevel.map { lv => frag(nbsp, nbsp, span(s"A.I.${lv}级")) },
                      fromOpeningdb.chessStatus.map { s =>
                        frag(
                          nbsp, nbsp, "对局结果：",
                          s match {
                            case "all" => "不限定"
                            case "win" => "胜"
                            case "winOrDraw" => "胜或和"
                          }
                        )
                      },
                      nbsp, nbsp,
                      label("完成进度："),
                      strong(
                        span(s"${f.completeSize()}/${fromOpeningdb.num}"),
                        linkTag(cls := List("fromOpeningdbModal" -> true, "complete" -> f.isComplete, "uncomplete" -> !f.isComplete, "isAi" -> f.ai, "notAccept" -> (!task.isFree && !ctx.me.??(_.isMemberOrCoachOrTeam))), target := (if (f.ai) "_self" else "_blank"), href := link)(if (f.isComplete()) "（已完成）" else "（未完成）")
                      )
                    ),
                    fromOpeningdb.opening match {
                      case Left(pgn) => {
                        val moves = fromOpeningdb.pgnSetup.pgnOpeningDB.toTurns()
                        div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.number, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen.value)(w.move.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen.value)(b.move.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                      case Right(openingdb) => views.html.resource.openingdb.bits.showOpeningdb(fromOpeningdb.pgnSetup)
                    },
                    result.map { result =>
                      ul(cls := "games")(
                        result.map { g =>
                          val complete = f.sameChessStatus(g)
                          li(
                            a(target := "_blank", href := routes.Round.watcher(g.gameId, "white"))(
                              span(g.white),
                              nbsp,
                              iconTag("U"),
                              nbsp,
                              span(g.black),
                              g.resultText.map(t => span(cls := List("complete" -> complete, "error" -> !complete))(s"（$t）"))
                            )
                          )
                        }
                      )
                    }
                  )
                )
              }
            )
          )
        )
      )
    }
  }

  def toFinishButton(task: TTask)(implicit ctx: Context) = {
    a(
      cls := List(
        "button button-green" -> true,
        "disabled" -> !task.canPlay,
        "notAccept" -> (task.canPlay && !task.isFree && !ctx.me.??(_.isMemberOrCoachOrTeam)),
        "fromPositionModal" -> (task.canPlay && task.itemType == TTask.TTaskItemType.FromPositionItem),
        "fromPgnModal" -> (task.canPlay && (task.itemType == TTask.TTaskItemType.FromPgnItem)),
        "isAi" -> (task.canPlay && (task.item.fromPosition.?? { fp => fp.firstUnComplete.??(_.ai) } || task.item.fromPgn.?? { fp => fp.firstUnComplete.??(_.ai) }))
      ),
      style := "margin-left:1em;font-size:0.8rem;",
      href := (if (task.canPlay) { controllers.rt_task.routes.TTask.doTask(task.id).toString } else "")
    )("去完成", !task.isFree option views.html.member.bits.vTip)
  }

  private val dataSimplePgn = attr("data-simplepgn")
  private val dataSource = attr("data-source")
  private val dataRel = attr("data-rel")

  private def saveTo(task: TTask, pgn: String)(implicit ctx: Context) = {
    button(dataIcon := "4", cls := List("button small resourceSaveTo" -> true), style := "margin: .5em 0", dataSimplePgn := pgn,
      dataSource := (
        task.sourceRel.source match {
          case Source.Homework => "homework"
          case _ => "task"
        }
      ),
      dataRel := task.id)("保存PGN", views.html.member.bits.vTip)
  }

}
