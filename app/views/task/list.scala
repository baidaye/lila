package views.html.task

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.task.TTask
import play.api.data.Form
import play.api.mvc.Call
import controllers.rt_task.routes

object list {

  def current(pager: Paginator[TTask], form: Form[_])(implicit ctx: Context) =
    layout(
      title = "当前任务",
      active = "current",
      statusArray = TTask.Status.current,
      pager = pager,
      form = form,
      call = routes.TTask.current()
    )

  def history(pager: Paginator[TTask], form: Form[_])(implicit ctx: Context) =
    layout(
      title = "历史任务",
      active = "history",
      statusArray = TTask.Status.history,
      pager = pager,
      form = form,
      call = routes.TTask.history()
    )

  private def layout(
    title: String,
    active: String,
    statusArray: List[TTask.Status],
    pager: Paginator[TTask],
    form: Form[_],
    call: Call
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = cssTag("task"),
      moreJs = frag(
        infiniteScrollTag
      )
    ) {
        main(cls := "page-menu page-small task-list")(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(active)
          ),
          div(cls := "page-menu__content box")(
            h1(title),
            st.form(
              rel := "nofollow",
              cls := "box__pad task-search",
              action := s"$call#results",
              method := "GET"
            )(
                table(
                  tr(
                    td(cls := "label")(label("任务类型")),
                    td(cls := "fixed")(form3.select(form("source"), TTask.Source.selects, "全部".some)),
                    td(cls := "label")(label("任务状态")),
                    td(cls := "fixed")(form3.select(form("status"), statusArray.map(s => s.id -> s.name), "全部".some))
                  ),
                  tr(
                    td(colspan := 4)(
                      div(cls := "action")(
                        div, submitButton(cls := "button")("查询")
                      )
                    )
                  )
                )
              ),
            table(cls := "slist")(
              thead(
                tr(
                  th("任务名称"),
                  th("截止时间"),
                  th("发布时间"),
                  th("进度"),
                  th("详情")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => addQueryParameter(call.url, "page", np)),
                  pager.currentPageResults.map { task =>
                    tr(cls := "paginated")(
                      td(task.name),
                      td(task.deadlineAt.map(_.toString("MM月dd日 HH:mm")) | "-"),
                      td(task.createdAt.toString("MM月dd日 HH:mm")),
                      td(task.progress, "%"),
                      td(
                        a(cls := "button button-empty small", href := routes.TTask.info(task.id))("详情")
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 6)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  private def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("current"), href := routes.TTask.current())("当前"),
      a(activeCls("history"), href := routes.TTask.history())("历史")
    )
  }

}
