package views.html.coach

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsArray, JsObject, Json }
import lila.user.User
import lila.resource.ThemeQuery

object throughTrain {

  def apply(studentJson: JsObject, taskJson: JsObject, teamJson: Option[JsObject], classicGameJson: JsArray)(implicit ctx: Context) =
    views.html.base.layout(
      title = "教学直通车",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("ttask"),
        cssTag("throughTrain")
      ),
      moreJs = frag(
        flatpickrTag,
        jsAt(s"compiled/lichess.throughTrain${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.throughTrain=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
            "student" -> studentJson,
            "taskPager" -> taskJson,
            "themePuzzle" -> views.html.task.bits.themePuzzleJson(),
            "classicGames" -> classicGameJson,
            "team" -> teamJson
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "throughTrain")
      }

}
