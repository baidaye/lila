package views.html

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import controllers.routes

object redirectTo {

  def apply(to: String)(implicit ctx: Context) = {
    views.html.base.layout(
      title = "正在跳转",
      moreJs = frag(embedJsUnsafe(s"""setTimeout(function(){location.href='$to'}, 1000)"""))
    ) {
        main(cls := "redirectTo")(
          h3(style := "text-align:center;margin-top:2em;font-size:1.5em;")("正在跳转... ", a(href := to)(to))
        )
      }
  }

}
