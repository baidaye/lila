package views.html
package game

import chess.format.Forsyth
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.game.Pov
import lila.hub.actorApi.contest.MiniBoard
import controllers.routes

object mini {

  private val dataTime = attr("data-time")
  private val dataLive = attr("data-live")
  private val dataTurnColor = attr("data-turncolor")
  private val dataLastmove = attr("data-lastmove")

  def apply(
    pov: Pov,
    board: Option[MiniBoard] = None,
    active: Boolean = false,
    ownerLink: Boolean = false,
    tv: Boolean = false,
    withTitle: Boolean = true,
    withLink: Boolean = true,
    withLive: Boolean = true,
    markMap: Map[String, Option[String]] = Map.empty[String, Option[String]]
  )(implicit ctx: Context): Tag = {
    val game = pov.game
    val isLive = game.isBeingPlayed
    val cssClass = isLive ?? ("live mini-board-" + game.id)
    val variant = game.variant.key
    val tag = if (withLink) a else span
    div(cls := List("mini-game" -> true, "active" -> active))(
      board.map { b => span(cls := "board-no")(b.boardName) },
      renderPlayer(!pov, board, markMap),
      tag(
        href := withLink.option(gameLink(game, pov.color, ownerLink, tv)),
        title := withTitle.option(gameTitle(game, pov.color)),
        cls := s"mini-board cg-wrap parse-fen is2d $cssClass $variant",
        dataLive := isLive.option(game.id),
        dataColor := pov.color.name,
        dataFen := Forsyth.exportBoard(game.board),
        dataTurnColor := game.turnColor.name,
        dataLastmove := ~game.lastMoveKeys
      )(cgWrapContent),
      renderPlayer(pov, board, markMap)
    )
  }

  private def boardNo(b: MiniBoard, c: chess.Color) = {
    val player = c.fold(b.whitePlayer, b.blackPlayer)
    player.teamerNo.fold(s"${player.no}")(tno => s"$tno.${player.no}")
  }

  private def renderPlayer(pov: Pov, board: Option[MiniBoard], markMap: Map[String, Option[String]] = Map.empty[String, Option[String]]) =
    span(cls := "mini-game__player")(
      span(cls := "mini-game__user")(
        board.map { b => strong("#", boardNo(b, pov.color)) },
        playerUsername(pov.player, text = pov.player.userId.?? { uid => userMark(uid, markMap) })
      ),
      if (pov.game.finished) renderResult(pov)
      else pov.game.clock.map { renderClock(_, pov.color) }
    )

  private def renderResult(pov: Pov) =
    span(cls := "mini-game__result")(
      pov.game.winnerColor.fold(span(cls := "draw")("½")) { c =>
        if (c == pov.color) span(cls := "win")("1") else span(cls := "loss")("0")
      }
    )

  private def renderClock(clock: chess.Clock, color: chess.Color) = {
    val s = clock.remainingTime(color).roundSeconds
    span(
      cls := s"mini-game__clock mini-game__clock--${color.name}",
      dataTime := s
    )(
        f"${s / 60}:${s % 60}%02d"
      )
  }

  private def userMark(userId: String, markMap: Map[String, Option[String]]) = {
    markMap.get(userId).fold(none[String]) { m => m }
  }
}
