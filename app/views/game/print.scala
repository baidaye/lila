package views.html
package game

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import lila.game.{ Game, Player }
import chess.format.FEN
import chess.format.pgn.{ Pgn, TagType, Tags, Turn, Tag => PgnTag }
import lila.user.UserContext

object print {

  val firstPageSize = 75
  val firstPageColSize = (firstPageSize / 3)

  val secondPageSize = 90
  val secondPageColSize = (secondPageSize / 3)

  def apply(game: Game, pgn: Pgn, initialFen: Option[FEN], defaultTitle: Option[String] = None)(implicit ctx: Context) = {
    val tags = pgn.tags
    val turns = pgn.turns
    val firstPageTurns = turns.take(firstPageSize)
    val firstPageCol1Turns = firstPageTurns.take(firstPageColSize)
    val firstPageCol2Turns = firstPageTurns.slice(firstPageColSize, firstPageColSize * 2)
    val firstPageCol3Turns = firstPageTurns.slice(firstPageColSize * 2, firstPageColSize * 3)
    val secondPageTurns = turns.slice(firstPageSize, firstPageSize + secondPageSize)
    val secondPageCol1Turns = secondPageTurns.take(secondPageColSize)
    val secondPageCol2Turns = secondPageTurns.slice(secondPageColSize, secondPageColSize * 2)
    val secondPageCol3Turns = secondPageTurns.slice(secondPageColSize * 2, secondPageColSize * 3)
    val title = defaultTitle | buildTitle(game) //s"${tags(_.White) | "?"} Vs. ${tags(_.Black) | "?"}"
    views.html.base.layout(
      title = title,
      moreCss = frag(
        cssTag("game.print")
      ),
      moreJs = frag(
        printTag,
        jsTag("game.print.js")
      )
    ) {
        main(cls := "page-small game-print")(
          div(cls := "box box-pad")(
            h1("打印设置"),
            div(cls := "setting")(
              table(cls := "tag-table")(
                tr(td(chk("title", "标题", true)), td(st.input(tpe := "text", dataId := "title", name := "title", value := title))),
                trTagChk(tags, PgnTag.White, true),
                trTagChk(tags, PgnTag.Black, true),
                trTagChk(tags, PgnTag.Result, true),
                trTagChk(tags, PgnTag.Event, false),
                trTagChk(tags, PgnTag.Site, false),
                trTagChk(tags, PgnTag.Round, false),
                trTagChk(tags, PgnTag.Date, false),
                tr(td(chk("inline", "棋谱无格式", false)), td)
              ),
              div(cls := "action")(
                button(cls := "button do-print")("打印"),
                div(cls := "tip")(
                  h3(strong("注：")),
                  ul(
                    li("1.如果点击打印后未正常显示棋盘，请在弹出窗口中找到“更多设置”，勾选“背景图形”选项；"),
                    li("2.如果打印预览时出现棋盘跨页的情况，可以再弹出窗口中找到“更多设置”，调整自定义“缩放”比例。")
                  )
                )
              )
            )
          ),
          br,
          div(cls := "box box-pad")(
            div(cls := "game-print-area")(
              div(cls := "page")(
                div(cls := "page-header")(
                  div(cls := "toggle-title")(title),
                  div(cls := "link")("haichess.com")
                ),
                div(cls := "page-body")(
                  div(cls := "tag-board")(
                    table(cls := "tag-table")(
                      trTag(tags, PgnTag.White, true),
                      trTag(tags, PgnTag.Black, true),
                      trTag(tags, PgnTag.Result, true),
                      trTag(tags, PgnTag.Event, false),
                      trTag(tags, PgnTag.Site, false),
                      trTag(tags, PgnTag.Round, false),
                      trTag(tags, PgnTag.Date, false)
                    ),
                    div(cls := "board-area")(
                      initialFen map { fen =>
                        span(
                          cls := s"mini-board cg-wrap parse-fen is2d",
                          dataColor := "white",
                          dataFen := fen.value
                        )(cgWrapContent)
                      }
                    )
                  ),
                  div(cls := "turn-inline none")(
                    turns.mkString(" ")
                  ),
                  turnTables(firstPageCol1Turns, firstPageCol2Turns, firstPageCol3Turns)
                ),
                div(cls := "page-footer page-footer-inline none")(s"- 第 1/1 页 -"),
                div(cls := "page-footer")(s"- 第 1/${if (secondPageTurns.nonEmpty) 2 else 1} 页 -")
              ),
              secondPageTurns.nonEmpty option div(cls := "page")(
                div(cls := "page-header"),
                div(cls := "page-body")(
                  turnTables(secondPageCol1Turns, secondPageCol2Turns, secondPageCol3Turns)
                ),
                div(cls := "page-footer")("- 第 2/2 页 -")
              )
            )
          )
        )
      }
  }

  private def buildTitle(g: Game)(implicit ctx: Context) = {
    val playerVs = s"${gamePlayer(g.variant, g.whitePlayer)} Vs. ${gamePlayer(g.variant, g.blackPlayer)}"
    if (g.imported) {
      s"导入${widgets.separator}来自${g.pgnImport.flatMap(_.user) | lila.user.User.anonymous}${widgets.separator}${g.variant.name}${widgets.separator}$playerVs"
    } else {
      s"${showClock(g)}${widgets.separator}${g.perfType.fold(chess.variant.FromPosition.name)(_.name)}${widgets.separator}${if (g.rated) trans.rated.txt() else trans.casual.txt()}${widgets.separator}$playerVs"
    }
  }

  private def gamePlayer(variant: chess.variant.Variant, player: Player)(implicit ctx: UserContext) = {
    player.playerUser map { playerUser =>
      if (player.isLichess | false) player.name | lila.user.User.anonymous else playerUser.id
    } getOrElse {
      player.aiLevel map { level =>
        aiName(level, false)
      } getOrElse {
        player.nameSplit.map(_._1) | lila.user.User.anonymous
      }
    }
  }

  private def showClock(game: Game)(implicit ctx: Context) =
    game.clock.map { clock =>
      clock.config.show
    } getOrElse {
      game.daysPerTurn.map { days => s"${days}天" }.getOrElse("∞")
    }

  private def turnTables(col1: List[Turn], col2: List[Turn], col3: List[Turn]) =
    div(cls := "turn-tables")(
      turnTable(col1),
      turnTable(col2),
      turnTable(col3)
    )

  private def turnTable(turns: List[Turn]) =
    turns.nonEmpty option table(cls := "turn-table")(
      thead(
        tr(
          th(cls := "index")("回合"),
          th(cls := "san")("白方"),
          th(cls := "san")("黑方")
        )
      ),
      tbody(
        turns.map { turn =>
          tr(
            td(cls := "index")(turn.number),
            td(cls := "san white")(turn.white.map(_.san) | "..."),
            td(cls := "san black")(turn.black.map(_.san))
          )
        }
      )
    )

  private def trTagChk(tags: Tags, tag: TagType, isChecked: Boolean) =
    tr(td(chk(tag.name, tagNameOrDefault(tag), isChecked)), td(st.input(tpe := "text", dataId := tag.name, name := tag.name, value := tags(_ => tag))))

  private def trTag(tags: Tags, tag: TagType, isChecked: Boolean) =
    tr(cls := List(s"toggle-${tag.name}" -> true, "none" -> !isChecked))(td(tagNameOrDefault(tag)), td(tags(_ => tag)))

  private def tagNameOrDefault(tag: TagType) = tagNameMap.getOrElse(tag.name, tag.name)

  private def chk(key: String, name: String, isChecked: Boolean) =
    div(cls := "checkbox-group")(
      st.input(tpe := "checkbox", st.id := key, dataId := key, isChecked option checked), nbsp,
      label(cls := "radio-label", `for` := key)(name)
    )

  val acceptTags = List(PgnTag.White, PgnTag.Black, PgnTag.Result, PgnTag.Site, PgnTag.Event, PgnTag.Round, PgnTag.Date)
  val tagNameMap =
    Map(
      "White" -> "白方",
      "Black" -> "黑方",
      "Result" -> "结果",
      "Site" -> "地点",
      "Event" -> "赛事",
      "Round" -> "轮次",
      "Date" -> "日期"
    )

}
