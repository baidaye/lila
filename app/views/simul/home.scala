package views.html.simul

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import controllers.routes

object home {

  def apply(
    opens: List[lila.simul.Simul],
    starteds: List[lila.simul.Simul],
    finisheds: List[lila.simul.Simul]
  )(implicit ctx: Context) = views.html.base.layout(
    moreCss = cssTag("simul.list"),
    moreJs = embedJsUnsafe(s"""$$(function() {
  lichess.StrongSocket.defaults.params.flag = 'simul';
  lichess.pubsub.on('socket.in.reload', () => {
    $$('.simul-list__content').load('${routes.Simul.homeReload()}', () => lichess.pubsub.emit('content_loaded'));
  });
});"""),
    title = "车轮战",
    openGraph = lila.app.ui.OpenGraph(
      title = "车轮战",
      url = s"$netBaseUrl${routes.Simul.home}",
      description = "车轮战涉及到一个人同时和几位棋手下棋。"
    ).some
  ) {
      main(cls := "page-menu simul-list")(
        st.aside(cls := "page-menu__menu simul-list__help")(
          p("车轮战涉及到一个人同时和几位棋手下棋。"),
          img(src := staticUrl("images/fischer-simul.jpg"), alt := "Simul IRL with Bobby Fischer")(
            em("[1964] ", trans.aboutSimulImage()),
            p(trans.aboutSimulRealLife()),
            p(trans.aboutSimulRules()),
            p(trans.aboutSimulSettings())
          )
        ),
        div(cls := "page-menu__content simul-list__content")(
          homeInner(opens, starteds, finisheds)
        )
      )
    }
}
