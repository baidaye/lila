package views.html.simul

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import controllers.routes

object form {

  def apply(form: Form[lila.simul.SimulForm.Setup], teams: lila.hub.lightTeam.TeamIdsWithNames)(implicit ctx: Context) = {

    import lila.simul.SimulForm._

    views.html.base.layout(
      title = "主持新车轮战",
      moreCss = cssTag("simul.form")
    ) {
        main(cls := "box box-pad page-small simul-form")(
          h1("主持新车轮战"),
          postForm(cls := "form3", action := routes.Simul.create())(
            globalError(form),
            form3.group(form("variant"), "如果您选择几个变体，每个棋手都要选择下哪一种。", klass = "none") { f =>
              div(cls := "variants")(
                views.html.setup.filter.renderCheckboxes(form, "variants", form.value.map(_.variants.map(_.toString)).getOrElse(Nil), translatedVariantChoicesWithVariants)
              )
            },
            form3.split(
              form3.group(form("clockTime"), raw("基本用时"), help = raw("棋手越多，您需要的时间可能就越多。").some, half = true)(form3.select(_, clockTimeChoices)),
              form3.group(form("clockIncrement"), raw("每步棋加时"), half = true)(form3.select(_, clockIncrementChoices))
            ),
            form3.split(
              form3.group(form("clockExtra"), "主持人附加时间", help = raw("您可以给您的时钟多加点时间以帮助您应对车轮战。").some, half = true)(
                form3.select(_, clockExtraChoices)
              ),
              form3.group(form("color"), raw("主持人每局棋色"), half = true)(form3.select(_, colorChoices))
            ),
            form3.split(
              teams.nonEmpty ?? {
                form3.group(form("team"), raw("仅俱乐部成员"), half = true)(form3.select(_, List(("", "没有限制")) ::: teams))
              },
              form3.checkbox(form("joinAnyTime"), raw("允许中途加入比赛"), help = raw("开启后，参与者可以中途申请加入对局").some, half = true)
            ),
            form3.group(form("text"), raw("描述"), help = frag("您想告诉参与者的任何事情").some)(form3.textarea(_)(rows := 2)),
            form3.actions(
              a(href := routes.Simul.home())(trans.cancel()),
              form3.submit("主持新车轮战", icon = "g".some)
            )
          )
        )
      }
  }
}
