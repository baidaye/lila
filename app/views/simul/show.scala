package views.html.simul

import play.api.libs.json.Json

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue

import controllers.routes

object show {

  def apply(
    sim: lila.simul.Simul,
    socketVersion: lila.socket.Socket.SocketVersion,
    data: play.api.libs.json.JsObject,
    chatOption: Option[lila.chat.UserChat.Mine],
    stream: Option[lila.streamer.Stream],
    team: Option[lila.team.Team]
  )(implicit ctx: Context) = views.html.base.layout(
    moreCss = cssTag("simul.show"),
    title = sim.fullName,
    moreJs = frag(
      jsAt(s"compiled/lichess.simul${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""lichess.simul=${
        safeJsonValue(Json.obj(
          "data" -> data,
          "i18n" -> bits.jsI18n(),
          "socketVersion" -> socketVersion.value,
          "userId" -> ctx.userId,
          "chat" -> chatOption.map { c =>
            views.html.chat.json(c.chat, name = "聊天室", timeout = c.timeout, public = true)
          }
        ))
      }""")
    )
  ) {
      main(cls := "simul")(
        st.aside(cls := "simul__side")(
          div(cls := "simul__meta")(
            div(cls := "game-infos")(
              div(cls := "header")(
                iconTag("f"),
                div(
                  span(cls := "clock")(sim.clock.config.show),
                  div(cls := "setup")(sim.variants.map(_.name).mkString(", "), " • ", "不计算等级分")
                )
              ),
              "主持人附加时间", "：", sim.clock.hostExtraMinutes, "分钟",
              br,
              "主持人每局棋色", "：", sim.color match {
                case Some("white") => "白方"
                case Some("black") => "黑方"
                case _ => "随机选色"
              }
            ),
            div(cls := "author")(
              "来自：", userIdLink(sim.hostId.some, withOnline = false, withBadge = false), nbsp, momentFromNow(sim.createdAt)
            ),
            team map { t =>
              frag(
                br, "您必须在俱乐部", a(href := controllers.rt_team.routes.Team.show(t.id))(t.name), "中"
              )
            }
          ),
          stream.map { s =>
            views.html.streamer.bits.contextual(s.streamer.userId)
          },
          chatOption.isDefined option views.html.chat.frag
        ),
        div(cls := "simul__main box")(spinner)
      )
    }
}
