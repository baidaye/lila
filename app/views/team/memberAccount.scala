package views.html.team

import lila.api.Context
import play.api.data.Form
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.team.{ MemberOptionWithUser, Team }
import lila.user.User
import controllers.rt_team.routes

object memberAccount {

  def apply(
    form: Form[_],
    team: Team,
    pager: Paginator[MemberOptionWithUser]
  )(implicit ctx: Context) =
    bits.layout(
      title = s"${team.name} 学生账号",
      evenMoreJs = frag(
        jsTag("team.memberAccount.js")
      )
    ) {
        main(cls := "page-menu")(
          bits.menu("memberAccount".some),
          div(cls := "page-menu__content box member")(
            h1(a(href := routes.Team.show(team.id))(team.name), nbsp, em("学生账号")),
            st.form(
              rel := "nofollow",
              cls := "box__pad member-search",
              action := s"${routes.TeamMemberAccount.memberAccount(team.id)}#results",
              method := "GET"
            )(
                table(
                  tr(
                    td(cls := "label")(label("账号/备注（姓名）")),
                    td(cls := "fixed")(form3.input(form("username"))),
                    td(cls := "label")(label("账号状态")),
                    td(cls := "fixed")(form3.select(form("enabled"), List(true -> "正常", false -> "关闭"), "全部".some))
                  ),
                  tr(
                    td(cls := "label")(label("会员类型")),
                    td(cls := "fixed")(form3.select(form("level"), lila.user.MemberLevel.allChoices, "全部".some)),
                    td(cls := "label"),
                    td(cls := "fixed")
                  ),
                  tr(
                    td(colspan := 2)(
                      a(cls := "button button-green modal-alert", href := routes.TeamMemberAccount.memberAccountAddModal(team.id), dataIcon := "O")("建立新的学生账号")
                    ),
                    td(colspan := 2)(
                      div(cls := "action")(
                        div, submitButton(cls := "button")("查询")
                      )
                    )
                  )
                )
              ),
            table(cls := "slist member-list")(
              thead(
                tr(
                  th("账号"),
                  th("备注（姓名）"),
                  th("会员类型"),
                  th("有效期至"),
                  th("创建时间"),
                  th("状态"),
                  th("操作")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => nextPageUrl(form, team, np)),
                  pager.currentPageResults.map { mu =>
                    tr(cls := "paginated")(
                      td(userLink(mu.user, withBadge = false)),
                      td(mu.markOrName),
                      td(mu.user.memberLevel.name),
                      td(mu.user.memberLevelWithExpire.expireNote),
                      td(mu.user.createdAt.toString("yyyy-MM-dd HH:mm")),
                      td(mu.user.enabledName),
                      td(style := "display:flex;")(
                        a(cls := List("button button-empty small modal-alert" -> true, "disabled" -> !mu.user.enabled), href := (if (mu.user.enabled) { s"${routes.TeamMemberAccount.memberAccountUseCardModal(team.id, mu.user.id)}" } else ""))("使用会员卡"),
                        a(cls := List("button button-empty small modal-alert" -> true, "disabled" -> !mu.user.enabled), href := (if (mu.user.enabled) { s"${routes.TeamMemberAccount.memberAccountResetPasswordModal(team.id, mu.user.id)}" } else ""))("重置密码"),
                        if (mu.user.enabled) {
                          postForm(cls := "member-close", action := routes.TeamMemberAccount.memberAccountClose(team.id, mu.user.id))(
                            button(cls := "button button-empty button-red small", title := "关闭账号后用户相关信息将被删除，无法继续登录且无法恢复，是否确认操作？")("关闭")
                          )
                        } else {
                          a(cls := "button button-empty button-red disabled small")("关闭")
                        }
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 6)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  def nextPageUrl(form: Form[_], team: Team, np: Int)(implicit ctx: Context) = {
    var url: String = routes.TeamMemberAccount.memberAccount(team.id, np).url
    form.data.foreach {
      case (key, value) => url = url.concat("&").concat(key).concat("=").concat(value)
    }
    url
  }

  def addModal(form: Form[_], team: Team)(implicit ctx: Context) =
    div(cls := "modal-content team-account-add none")(
      h2("新建学生账号"),
      postForm(cls := "form3", action := routes.TeamMemberAccount.memberAccountAddApply(team.id))(
        form3.group(
          form("usernames"),
          raw("账号列表"),
          help = div(strong("账号规则："), "中文或字母开头，只允许中文、字母、数字、下划线和连字符", br, strong("注意："), "用户账号或将展示在等级分排名、对局、班级等区域。请使用文明词汇，不要填写敏感信息。").some
        )(f =>
            frag(
              form3.textarea(f)(rows := 7, maxlength := s"${20 * lila.team.DataForm.memberAccountData.MemberAccountMaxSize}", placeholder := s"#样例#\nzhanghao01\nzhanghao02\nzhanghao03\n……\nzhanghao${lila.team.DataForm.memberAccountData.MemberAccountMaxSize}\n最多${lila.team.DataForm.memberAccountData.MemberAccountMaxSize}个账号"),
              p(cls := "error error-usernames none")
            )),
        form3.group(form("password"), "初始密码", help = raw("6-20位英文数字下划线。请尽量提高密码强度，默认密码可能导致账号被盗").some)(f =>
          frag(
            form3.input(f)(required),
            p(cls := "error error-password none")
          )),
        form3.split(
          form3.group(form("cardLevel"), raw("会员类型"), help = div(cls := "card-count none")("共_张").some, half = true)(f =>
            frag(
              form3.select(f, lila.user.MemberLevel.allChoices),
              p(cls := "error error-cardLevel none")
            )),
          form3.group(form("days"), raw("使用期限"), half = true) { f =>
            frag(
              div(cls := "form-control days-forever")("永久"),
              form3.select(f, lila.team.DataForm.memberAccountData.accountDaysChoices, klass = "none")
            )
          }
        ),
        div(cls := "form-group")(
          strong("注意："),
          ul(cls := "note")(
            li("1、学生账号，由俱乐部管理员进行管理，用户名+密码 登录；"),
            li("2、学生账号不能退出俱乐部，不能认证教练；"),
            li("3、账号需按", a(href := controllers.routes.Page.tos)("《用户协议》"), "合法使用。")
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )

  def resetPasswordModal(form: Form[_], team: Team, member: User)(implicit ctx: Context) =
    div(cls := "modal-content team-account-password none")(
      h1("重置密码"),
      postForm(cls := "form3", action := routes.TeamMemberAccount.memberAccountResetPasswordApply(team.id, member.id))(
        form3.group(form("password"), "新密码", help = raw("6-20位英文数字下划线").some) { f =>
          frag(
            form3.input(f)(required),
            p(cls := "error error-password none")
          )
        },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )

  def useCardModal(form: Form[_], team: Team, member: User)(implicit ctx: Context) =
    div(cls := "modal-content team-account-card none")(
      h2("使用会员卡"),
      postForm(cls := "form3", action := routes.TeamMemberAccount.memberAccountUseCardApply(team.id, member.id))(
        form3.group(form("username"), raw("账号："), half = true)(_ =>
          member.username),
        form3.group(form("cardLevel"), raw("会员类型"), help = div(cls := "card-count none")("共_张").some, half = true)(f =>
          frag(
            form3.select(f, lila.user.MemberLevel.choices),
            p(cls := "error error-cardLevel none")
          )),
        form3.group(form("days"), raw("使用期限"), half = true)(f =>
          form3.select(f, lila.team.DataForm.memberAccountData.accountDaysChoices, klass = "none")),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
}
