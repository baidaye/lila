package views.html.team

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.Team
import lila.common.Region
import controllers.rt_team.routes

object forms {

  def create(form: Form[_], captcha: lila.common.Captcha)(implicit ctx: Context) =
    bits.layout(title = trans.newTeam.txt(), evenMoreJs = frag(captchaTag, provinceCascadeTag)) {
      main(cls := "page-menu page-small")(
        bits.menu("form".some),
        div(cls := "page-menu__content box box-pad")(
          h1(trans.newTeam()),
          if (ctx.me.??(u => !u.cpExistsAndConfirmed)) a(href := s"${controllers.routes.Account.confirm}#cellphone")("请先绑定手机")
          else {
            frag(
              p(cls := "is-gold", dataIcon := "")(nbsp, nbsp, "每账号同一时间仅可建立1个俱乐部"),
              postForm(cls := "form3", action := routes.Team.create())(
                form3.globalError(form),
                form3.group(form("name"), "名称")(form3.input(_)),
                form3.group(form("open"), "加入政策") { f =>
                  form3.select(f, Seq(0 -> "需要管理员确认", 1 -> "对所有人开放"))
                },
                form3.split(
                  form3.group(form("province"), "省份", half = true) { f =>
                    form3.select(f, Region.Province.provinces, default = "".some)
                  },
                  form3.group(form("city"), "城市", half = true) { f =>
                    val empty = form3.select(f, List.empty, default = "".some)
                    form("province").value.fold(empty) { v =>
                      form3.select(f, Region.City.citys(v), default = "".some)
                    }
                  }
                ),
                form3.group(form("description"), "简要介绍")(form3.textarea(_)(rows := 10)),
                views.html.base.captcha(form, captcha),
                div(cls := "form-group")(
                  p(cls := "is-gold", dataIcon := "")(
                    nbsp, nbsp,
                    "您将建立一个棋友之间互助、交流、学习的团队。需要您注意的是：作为俱乐部的管理员，您可以引导成员进行热烈、友好的互动，但如果出现违规信息，您和涉及的成员都会受到相应的处罚。请及时检查、清理互动区的信息，如果发现无法处理的情况，请联系客服。祝您和俱乐部成员能充分利用平台提供的功能，提升学习效率，享受国际象棋的乐趣！"
                  )
                ),
                form3.actions(
                  a(href := routes.Team.home(1))(trans.cancel()),
                  form3.submit(trans.newTeam())
                )
              )
            )
          }
        )
      )
    }

  def update(t: Team, form: Form[_])(implicit ctx: Context) = {
    bits.layout(title = (t.name + " 资料"), evenMoreJs = frag(
      provinceCascadeTag,
      singleUploaderTag,
      multiUploaderTag,
      jsTag("team.edit.js")
    )) {
      main(cls := "page-small box box-pad")(
        h1(a(href := routes.Team.show(t.id))(t.name), nbsp, em("资料")),
        p("编号", "：", t.id),
        postForm(cls := "form3 edit", action := routes.Team.update(t.id))(
          div(cls := "top")(
            form3.group(form("logo"), raw("Logo"), klass = "logo")(form3.singleImage(_, "上传LOGO"))
          ),
          t.certified option form3.hidden(form("name")),
          !t.certified option form3.group(form("name"), "名称", help = frag("认证后将不可修改").some)(form3.input(_)),
          t.certified option form3.split(
            form3.group(form(""), "省份：", half = true) { _ =>
              frag(
                form3.hidden(form("province")),
                strong(t.provinceName)
              )
            },
            form3.group(form(""), "城市：", half = true) { _ =>
              frag(
                form3.hidden(form("city")),
                strong(t.cityName)
              )
            }
          ),
          !t.certified option form3.split(
            form3.group(form("province"), "省份", half = true) { f =>
              form3.select(f, Region.Province.provinces, default = "".some)
            },
            form3.group(form("city"), "城市", half = true) { f =>
              val empty = form3.select(f, List.empty, default = "".some)
              form("province").value.fold(empty) { v =>
                form3.select(f, Region.City.citys(v), default = "".some)
              }
            }
          ),
          form3.group(form("description"), "简要介绍")(form3.textarea(_)(rows := 10)),
          form3.group(form("envPicture"), raw("俱乐部环境（最多上传5张，宽度大于750像素，3:2比例显示最佳）"))(_ => form3.multiImage(form, "envPicture", 5, "点击上传")),
          form3.actions(
            a(href := routes.Team.show(t.id), style := "margin-left:20px")("取消"),
            form3.submit("保存")
          )
        )
      )
    }
  }

}
