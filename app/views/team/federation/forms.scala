package views.html.team.federation

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.TeamFederation
import controllers.rt_team.routes

object forms {

  def create(form: Form[_])(implicit ctx: Context) =
    bits.layout("建立联盟") {
      main(cls := "page-menu page-small")(
        bits.menu("create", 0),
        div(cls := "page-menu__content box box-pad federation-create")(
          h1("建立联盟"),
          postForm(cls := "form3", action := routes.TeamFederation.create())(
            p(cls := "is-gold", dataIcon := "")(nbsp, nbsp, s"每个俱乐部可建立${TeamFederation.MAX_CREATE}个联盟"),
            form3.group(form("name"), "名称")(form3.input(_)),
            form3.group(form("open"), "加入政策") { f =>
              form3.select(f, Seq(false -> "需要管理员确认", true -> "对所有人开放"))
            },
            form3.group(form("visibility"), "成员可见范围") { f =>
              form3.select(f, TeamFederation.Visibility.selects)
            },
            form3.group(form("description"), "简要介绍")(form3.textarea(_)(rows := 10)),
            div(cls := "form-group")(
              ul(
                li("1、联盟成员俱乐部，具有平等的权限；"),
                li("2、发起、参与 联盟团体赛；"),
                li("3、共享资源。")
              )
            ),
            form3.globalError(form),
            form3.actions(
              a(href := routes.TeamFederation.mineList())("取消"),
              form3.submit("建立联盟")
            )
          )
        )
      )
    }

  def update(form: Form[_], federation: TeamFederation)(implicit ctx: Context) =
    views.html.base.layout(
      title = s"${federation.name} 资料",
      moreJs = frag(
        singleUploaderTag,
        jsTag("team.federation.js")
      ),
      moreCss = cssTag("team")
    ) {
        main(cls := "box box-pad page-small federation-update")(
          h1(a(href := routes.TeamFederation.show(federation.id))(federation.name), nbsp, em("资料")),
          postForm(cls := "form3", action := routes.TeamFederation.update(federation.id))(
            p("编号", "：", federation.id),
            div(cls := "top")(
              form3.group(form("logo"), raw("Logo"), klass = "logo")(form3.singleImage(_, "上传LOGO"))
            ),
            form3.group(form("description"), "简要介绍")(form3.textarea(_)(rows := 10)),
            form3.globalError(form),
            form3.actions(
              a(href := routes.TeamFederation.mineList())("取消"),
              form3.submit("保存")
            )
          )
        )
      }

  def setting(form: Form[_], federation: TeamFederation)(implicit ctx: Context) =
    views.html.base.layout(
      title = s"${federation.name} 设置",
      moreCss = cssTag("team")
    ) {
        main(cls := "box box-pad page-small federation-setting")(
          div(cls := "box__top")(
            h1(a(href := routes.TeamFederation.show(federation.id))(federation.name), nbsp, em("设置")),
            postForm(cls := "box__top__actions", action := routes.TeamFederation.disable(federation.id))(
              submitButton(cls := "button button-red button-empty small confirm", st.title := "关闭联盟后将不可恢复")("关闭")
            )
          ),
          postForm(cls := "form3", action := routes.TeamFederation.setting(federation.id))(
            form3.group(form("open"), "加入政策") { f =>
              form3.select(f, Seq(false -> "需要管理员确认", true -> "对所有人开放"))
            },
            form3.group(form("visibility"), "成员可见范围") { f =>
              form3.select(f, TeamFederation.Visibility.selects)
            },
            form3.globalError(form),
            form3.actions(
              a(href := routes.TeamFederation.mineList())("取消"),
              form3.submit("保存")
            )
          )
        )
      }

}
