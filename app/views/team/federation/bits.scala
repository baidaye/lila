package views.html.team.federation

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.TeamFederationRequestRepo
import controllers.rt_team.routes

object bits {

  val baseUrl2 = s"//${lila.api.Env.current.Net.AssetDomain}"
  val defaultLogo: String = s"${baseUrl2}/assets/images/club-default.png"
  def logoUrl(logo: Option[String]) = logo.fold(defaultLogo) { l =>
    s"$baseUrl2/image/$l"
  }

  def layout(
    title: String,
    evenMoreCss: Frag = emptyFrag,
    evenMoreJs: Frag = emptyFrag
  )(body: Frag)(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = frag(
        cssTag("team"),
        evenMoreCss
      ),
      moreJs = frag(
        infiniteScrollTag,
        evenMoreJs
      )
    )(body)

  def menu(active: String, requestCount: Int)(implicit ctx: Context) = {
    val requestCount = ctx.me.??(_.teamId.fold(0) { teamId => TeamFederationRequestRepo.countByTeam(teamId) awaitSeconds (3) })
    st.nav(cls := "page-menu__menu subnav")(
      (requestCount > 0) option
        a(cls := List("active" -> (active == "requests")), href := routes.TeamFederation.requestList())(
          requestCount, " 个加入请求"
        ),
      a(cls := List("active" -> (active == "mineList")), href := routes.TeamFederation.mineList())("参与的联盟"),
      a(cls := List("active" -> (active == "joinList")), href := routes.TeamFederation.joinList(""))("加入联盟"),
      a(cls := List("active" -> (active == "create")), href := routes.TeamFederation.createForm())("建立联盟")
    )
  }

}
