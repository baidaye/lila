package views.html.team.federation

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ TeamFederation, TeamFederationMember, TeamFederationRequest }
import lila.common.String.html.richText
import controllers.rt_team.routes

object show {

  def apply(
    federation: TeamFederation,
    myRequest: Option[TeamFederationRequest],
    myMember: Option[TeamFederationMember],
    rwts: List[TeamFederationRequest.WithTeam],
    mwts: List[TeamFederationMember.WithTeam]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = federation.name,
      moreCss = cssTag("team")
    ) {
        main(cls := "box federation-show")(
          div(cls := "box__top")(
            div(cls := "subject")(
              img(cls := "logo", src := bits.logoUrl(federation.logo)),
              div(cls := "name")(federation.name)
            ),
            div(
              if (federation.disabled) span(cls := "closed")("已关闭")
              else span(federation.nbMembers, "位成员")
            )
          ),
          (ctx.userId.??(federation.isOwner) || federation.enabled) option div(cls := "federation-show__content")(
            st.section(cls := "federation-show__meta")(
              div("编号", "：", strong(federation.id)),
              div("管理员", "：", userIdLink(federation.teamOwnerId.some, withBadge = false))
            ),
            st.section(cls := "federation-show__actions")(
              div(cls := "join")(
                (ctx.me.??(_.isTeam) && federation.enabled && myMember.isEmpty) option frag(
                  if (myRequest.isDefined) strong("您的加入请求正在由联盟管理员审核")
                  else a(cls := "button small member-join", href := routes.TeamFederation.joinForm(federation.id))("加入联盟")
                ),
                myMember.map { m =>
                  !m.isOwner option postForm(action := routes.TeamFederation.quit(federation.id))(
                    button(cls := "button small button-red confirm", title := "确认退出联盟")("退出联盟")
                  )
                }
              ),
              div(cls := "actions")(
                myMember.??(_.isOwner) option div(cls := "creator-action")(
                  a(href := routes.TeamFederation.settingForm(federation.id), cls := "button button-empty small", dataIcon := "%")("设置"),
                  a(href := routes.TeamFederation.updateForm(federation.id), cls := "button button-empty small", dataIcon := "m")("资料")
                )
              )
            ),
            st.section(cls := "federation-show__desc")(
              div(cls := "description")(richText(federation.description))
            ),
            (federation.isAllVisible || (!federation.isAllVisible && myMember.isDefined)) option st.section(cls := "federation-show__members")(
              h2(cls := "text")("联盟成员"),
              table(cls := "slist")(
                thead(
                  tr(
                    th("俱乐部"),
                    th("管理员"),
                    th("加入时间"),
                    myMember.??(_.isOwner) option th("操作")
                  )
                ),
                tbody(
                  if (mwts.nonEmpty) {
                    mwts.map { mwt =>
                      tr(
                        td(teamLink(mwt.team)),
                        td(userIdLink(mwt.team.createdBy.some, withBadge = false)),
                        td(momentFromNow(mwt.member.createdAt)),
                        myMember.??(_.isOwner) option td(cls := "action")(
                          (!mwt.member.isOwner) option postForm(action := routes.TeamFederation.kick(federation.id, mwt.member.id))(
                            button(cls := "button button-empty button-red small confirm", title := "确认移除该成员？")("移除")
                          )
                        )
                      )
                    }
                  } else tr(td(colspan := "4")("没有找到~"))
                )
              )
            )
          )
        )
      }

}
