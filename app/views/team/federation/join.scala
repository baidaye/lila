package views.html.team.federation

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.richText
import lila.team.{ TeamFederation, TeamFederationRequest }
import controllers.rt_team.routes

object join {

  def joinForm(form: Form[_], federation: TeamFederation)(implicit ctx: Context) =
    bits.layout(title = "加入联盟") {
      main(cls := "page-menu federation-join")(
        bits.menu("joinList", 0),
        div(cls := "page-menu__content box box-pad")(
          h1(s"加入 ${federation.name}"),
          p(style := "margin:2em 0")(richText(federation.description)),
          postForm(cls := "form3", action := routes.TeamFederation.join(federation.id))(
            form3.group(form("message"), raw("消息"))(form3.textarea(_)()),
            p(cls := "is-gold", dataIcon := "")(b(s"同一俱乐部最多加入${TeamFederation.MAX_JOIN}个联盟。")),
            form3.globalError(form),
            form3.actions(
              a(href := routes.TeamFederation.show(federation.id))("取消"),
              form3.submit("加入")
            )
          )
        )
      )
    }

  def requestList(rwts: List[TeamFederationRequest.WithTeam])(implicit ctx: Context) =
    table(cls := "slist")(
      thead(
        tr(
          th("俱乐部"),
          th("管理员"),
          th("消息"),
          th("申请时间"),
          th("操作")
        )
      ),
      tbody(
        if (rwts.nonEmpty) {
          rwts.map { rwt =>
            tr(
              td(teamLink(rwt.team, cssClass = "medium".some)),
              td(userIdLink(rwt.team.createdBy.some, withBadge = false)),
              td(richText(rwt.request.message)),
              td(momentFromNow(rwt.request.createdAt)),
              td(cls := "process")(
                postForm(cls := "process-request", action := routes.TeamFederation.joinProcess(rwt.request.id))(
                  button(cls := "button button-empty button-green small confirm", title := "确认接受申请？", name := "process", value := "accept")("接受"),
                  button(cls := "button button-empty button-red small confirm", title := "确认拒绝申请？", name := "process", value := "decline")("拒绝")
                )
              )
            )
          }
        } else tr(td(colspan := "5")("没有找到~"))
      )
    )

}
