package views.html.team.federation

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ TeamFederation, TeamFederationRequest }
import controllers.rt_team.routes

object list {

  def mineList(list: List[TeamFederation.WithMember])(implicit ctx: Context) =
    bits.layout(title = "参与的联盟") {
      main(cls := "page-menu federation-list")(
        bits.menu("mineList", 0),
        div(cls := "page-menu__content box")(
          h1("参与的联盟"),
          table(cls := "slist slist-pad")(
            if (list.nonEmpty) tbody(
              list.map { fwm =>
                val f = fwm.federation
                val m = fwm.member
                tr(
                  td(
                    div(cls := List("subject" -> true))(
                      img(cls := "logo", src := bits.logoUrl(f.logo)),
                      a(cls := "name", href := routes.TeamFederation.show(f.id))(f.name),
                      f.disabled option span(cls := "tag closed")("已关闭"),
                      span(cls := "tag")(if (m.isOwner) "盟主" else "成员")
                    ),
                    shorten(f.description, 200)
                  ),
                  td(cls := "info")(
                    p(f.nbMembers, "位成员")
                  )
                )
              }
            )
            else emptyTr()
          )
        )
      )
    }

  def joinList(list: List[TeamFederation.WithRequestAndMember], text: String = "")(implicit ctx: Context) =
    bits.layout(title = "加入联盟") {
      main(cls := "page-menu federation-list")(
        bits.menu("joinList", 0),
        div(cls := "page-menu__content box")(
          div(cls := "box__top")(
            st.form(cls := "search", action := routes.TeamFederation.joinList(""), method := "get")(
              input(st.name := "text", st.placeholder := "搜索 “完整名称” 或 “编号”", st.value := text),
              submitButton(cls := "button", dataIcon := "y")
            )
          ),
          table(cls := "slist slist-pad")(
            if (list.nonEmpty) tbody(
              list.map { fwr =>
                val f = fwr.federation
                val r = fwr.request
                val m = fwr.member
                tr(
                  td(
                    div(cls := List("subject" -> true))(
                      img(cls := "logo", src := bits.logoUrl(f.logo)),
                      a(cls := "name", href := routes.TeamFederation.show(f.id))(f.name),
                      f.disabled option span(cls := "tag closed")("已关闭"),
                      m.map { m => span(cls := "tag")(if (m.isOwner) "盟主" else "成员") },
                      r.map { r => span(cls := "tag")("已申请") }
                    ),
                    shorten(f.description, 200)
                  ),
                  td(cls := "info")(
                    p(f.nbMembers, "位成员")
                  )
                )
              }
            )
            else emptyTr()
          )
        )
      )
    }

  def requestList(rwts: List[TeamFederationRequest.WithTeam])(implicit ctx: Context) =
    bits.layout(title = "申请列表") {
      main(cls := "page-menu federation-requests")(
        bits.menu("requests", 0),
        div(cls := "page-menu__content box")(
          h1("申请列表"),
          join.requestList(rwts)
        )
      )
    }

  private def emptyTr()(implicit ctx: Context) = tbody(
    tr(td(colspan := "2")("没有找到~"))
  )

}
