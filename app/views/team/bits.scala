package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ Member, MemberWithUser, Team, Campus }
import controllers.rt_team.routes

object bits {

  def menu(currentTab: Option[String])(implicit ctx: Context) = ~currentTab |> { tab =>
    st.nav(cls := "page-menu__menu subnav")(
      (ctx.teamNbRequests > 0) option
        a(cls := tab.active("requests"), href := routes.Team.requests())(
          ctx.teamNbRequests, " 个加入请求"
        ),
      ctx.me.map { u =>
        frag(
          !u.hasBelongTeam option a(cls := tab.active("all"), href := routes.Team.all())("加入俱乐部"),
          !u.hasBelongTeam option a(cls := tab.active("form"), href := routes.Team.createForm())("建立俱乐部"),
          u.teamId.map { teamId =>
            frag(
              a(cls := tab.active("memberAccount"), href := routes.TeamMemberAccount.memberAccount(teamId))("学生账号"),
              a(cls := tab.active("member"), href := routes.TeamMember.members(teamId))("成员"),
              u.isTeam option a(cls := tab.active("coach"), href := routes.TeamCoach.coach(teamId))("教练")
            )
          },
          !u.hasTeam option {
            u.headCampusAdmin.map { campusId =>
              frag(
                a(cls := tab.active("member"), href := s"${routes.TeamMember.members(Campus.toTeamId(campusId), 1)}?campus=$campusId")("成员")
              )
            }
          }
        )
      }
    )
  }

  private[team] def teamTr(t: Team)(implicit ctx: Context) =
    tr(cls := "paginated")(
      td(
        div(cls := List("subject" -> true))(
          teamLink(t, cssClass = "medium".some),
          t.disabled option span(cls := "closed")("已关闭") /*,
          a(cls := "button button-empty", href := routes.Team.join(t.id))("加入俱乐部")*/
        ),
        shorten(t.description, 200)
      ),
      td(cls := "info")(
        p(trans.nbMembers.plural(t.nbMembers, t.nbMembers.localize))
      )
    )

  private[team] def teamWithMemberTr(twm: Team.TeamWithMember)(implicit ctx: Context) = {
    val t = twm.team
    tr(cls := "paginated")(
      td(
        div(cls := List("subject" -> true))(
          teamLink(t, cssClass = "medium".some),
          t.disabled option span(cls := "closed")("已关闭")
        ),
        shorten(t.description, 200)
      ),
      td(cls := "info")(
        p(trans.nbMembers.plural(t.nbMembers, t.nbMembers.localize)),
        p(
          t.ratingSettingOrDefault.open option {
            val link = if (ctx.userId.??(t.isCreator)) routes.TeamRating.ratingDistribution(t.id) else routes.TeamRating.memberRatingDistribution(t.id, twm.member.id)
            frag("等级分：", a(cls := "rating", href := link)(twm.member.intRating.map(_.toString) | "暂无"))
          }, nbsp, b(twm.member.roles)
        )
      )
    )
  }

  private[team] def layout(
    title: String,
    evenMoreCss: Frag = emptyFrag,
    evenMoreJs: Frag = emptyFrag,
    openGraph: Option[lila.app.ui.OpenGraph] = None
  )(body: Frag)(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = frag(
        cssTag("team"),
        evenMoreCss
      ),
      moreJs = frag(
        infiniteScrollTag,
        evenMoreJs
      ),
      openGraph = openGraph
    )(body)

  private[team] def markByUserId(userId: String, markMap: Map[String, Option[String]]): Option[String] = {
    markMap.get(userId).fold(none[String]) { m => m }
  }

  private[team] def mark(m: Member, markMap: Map[String, Option[String]]): Option[String] = {
    markMap.get(m.user).fold(none[String]) { m => m }
  }

  def userMark(u: lila.user.User, markMap: Map[String, Option[String]]): String = {
    markMap.get(u.id).fold(none[String]) { m => m } | u.realNameOrUsername
  }

  def userViewName(mwu: MemberWithUser, markMap: Map[String, Option[String]]): String = {
    mark(mwu.member, markMap).fold(mwu.user.realNameOrUsername)(m => m)
  }

  def Owner(team: Team)(implicit ctx: Context) =
    ctx.me.?? { me => team.isCreator(me.id) }

  def CanReadCampus(team: Team, campusId: String)(implicit ctx: Context) =
    ctx.me.?? { me => team.isCreator(me.id) || me.isCampusAdmin(campusId) }

}
