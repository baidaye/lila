package views.html.team

import play.api.data.{ Field, Form }
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import scalatags.Text.all.attr
import lila.common.paginator.Paginator
import lila.team.{ Campus, Member, MemberWithUser, Tag, Team }
import controllers.rt_team.routes

object member {

  def apply(
    form: Form[_],
    team: Team,
    pager: Paginator[MemberWithUser],
    tags: List[Tag],
    campuses: List[Campus],
    markMap: Map[String, Option[String]],
    advance: Boolean
  )(implicit ctx: Context) =
    bits.layout(
      title = s"${team.name} 成员",
      evenMoreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        memberAdvanceTag,
        jsTag("team.member.js")
      )
    ) {
        main(cls := "page-menu")(
          bits.menu("member".some),
          div(cls := "page-menu__content box member")(
            h1(a(href := routes.Team.show(team.id))(team.name), nbsp, em("成员")),
            st.form(
              rel := "nofollow",
              cls := "box__pad member-search",
              action := s"${routes.TeamMember.members(team.id)}#results",
              method := "GET"
            )(
                table(
                  tr(
                    td(cls := "label")(label("账号/备注（姓名）")),
                    td(cls := "fixed")(form3.input(form("username"))),
                    td(cls := "label")(label("校区")),
                    td(cls := "fixed")(form3.select(form("campus"), campuses.filter(c => bits.CanReadCampus(team, c.id)).map(c => c.id -> c.name), if (bits.Owner(team)) "".some else None))
                  ),
                  tr(
                    td(cls := "label")(label("角色")),
                    td(cls := "fixed")(form3.select(form("role"), Member.Role.list, "".some)),
                    td(cls := "label")(label("年龄")),
                    td(cls := "fixed")(form3.input(form("age"), typ = "number"))
                  ),
                  tr(
                    td(cls := "label")(label("棋协级别")),
                    td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
                    td(cls := "label")(label("性别")),
                    td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
                  ),
                  tags.nonEmpty option tr(
                    td(colspan := 4)(
                      form3.hidden("advance", advance.toString),
                      a(cls := List("show-search" -> true, "expanded" -> advance))(if (advance) frag("隐藏高级搜索", iconTag("S")) else frag("显示高级搜索", iconTag("R")))
                    )
                  ),
                  tags.filterNot(_.typ.range).zipWithIndex.map {
                    case (t, i) => buildSearchField(t, form(s"fields[$i]"), advance)
                  },
                  tags.filter(_.typ.range).zipWithIndex.map {
                    case (t, i) => buildRangeSearchField(t, form(s"rangeFields[$i]"), advance)
                  },
                  tr(
                    td(colspan := 4)(
                      div(cls := "action")(
                        div, submitButton(cls := "button")("查询")
                      )
                    )
                  )
                ),
                form3.hidden(form("sortField")),
                form3.hidden(form("sortType"))
              ),
            table(cls := "slist member-list")(
              thead(
                tr(
                  th(withSorter("账号", "username", form)),
                  th(withSorter("备注（姓名）", "markname", form)),
                  th("角色"),
                  th("校区"),
                  th(withSorter("等级分", "rating", form)),
                  th("性别"),
                  th("年龄"),
                  th("级别"),
                  tags.filter(_.isVisitable).map { tag =>
                    th(tag.label)
                  },
                  th("操作")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => nextPageUrl(form, team, np)),
                  pager.currentPageResults.map { mu =>
                    tr(cls := "paginated")(
                      td(userLink(mu.user, withBadge = false)),
                      td(mu.markOrName),
                      td(mu.member.roles),
                      td(campuses.find(_.id == mu.member.campus).map(_.name) | "-"),
                      td(mu.member.rating.map(_.intValue.toString) | "-"),
                      td(mu.profile.ofSex.map(_.name) | "-"),
                      td(mu.profile.age.map(_.toString) | "-"),
                      td(mu.profile.ofLevel.name),
                      tags.filter(_.isVisitable).map { tag =>
                        td(mu.member.tagValue(tag.field))
                      },
                      td(
                        /*a(cls := "button button-empty small", href := routes.Message.convo(mu.user.username))("发消息"),*/
                        a(cls := "button button-empty small member-edit", href := routes.TeamMember.editMemberModal(team.id, mu.member.id))("编辑"),
                        if (mu.user.notImported) {
                          !mu.member.isManager option postForm(cls := "member-kick inline", action := routes.TeamMember.kick(team.id))(
                            input(tpe := "hidden", name := "userId", value := mu.user.id),
                            button(title := "移除成员后不可恢复，是否继续？", cls := "button button-empty button-red small button-no-upper")("移除")
                          )
                        } else {
                          mu.user.enabled option postForm(cls := "member-close inline", action := routes.TeamMemberAccount.memberAccountClose(team.id, mu.user.id))(
                            button(cls := "button button-empty button-red small", title := "关闭账号后用户相关信息将被删除，无法继续登录且无法恢复，是否确认操作？")("关闭")
                          )
                        }
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 3)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  val dataSortType = attr("data-sort-type")
  val dataSortField = attr("data-sort-field")
  def withSorter(lb: String, filedName: String, form: Form[_]) = div(cls := "withSorter")(
    label(lb),
    span(cls := "sorter")(
      a(cls := List("up" -> true, "active" -> (form("sortField").value.has(filedName) && form("sortType").value.has("up"))), dataSortField := filedName, dataSortType := "up", dataIcon := "升"),
      a(cls := List("down" -> true, "active" -> (form("sortField").value.has(filedName) && form("sortType").value.has("down"))), dataSortField := filedName, dataSortType := "down", dataIcon := "降")
    )
  )

  def nextPageUrl(form: Form[_], team: Team, np: Int)(implicit ctx: Context) = {
    var url: String = routes.TeamMember.members(team.id, np).url
    form.data.foreach {
      case (key, value) => url = url.concat("&").concat(key).concat("=").concat(value)
    }
    url
  }

  def buildSearchField(tag: Tag, form: Field, advance: Boolean)(implicit ctx: Context) = {
    tag.typ match {
      case lila.team.Tag.Type.Text => tr(cls := List("tr-tag" -> true, "none" -> !advance))(
        td(cls := "label")(label(`for` := form3.id(form(tag.field)))(tag.label)),
        td(cls := "single")(colspan := 3)(
          form3.input(form("fieldValue")),
          form3.hidden(form("fieldName"), tag.field.some)
        )
      )
      case lila.team.Tag.Type.SingleChoice => tr(cls := List("tr-tag" -> true, "none" -> !advance))(
        td(cls := "label")(label(`for` := form3.id(form(tag.field)))(tag.label)),
        td(cls := "single")(colspan := 3)(
          form3.select(form("fieldValue"), tag.toChoice, default = "".some),
          form3.hidden(form("fieldName"), tag.field.some)
        )
      )
      case _ => frag()
    }
  }

  def buildRangeSearchField(tag: Tag, form: Field, advance: Boolean)(implicit ctx: Context) = {
    tag.typ match {
      case lila.team.Tag.Type.Number => tr(cls := List("tr-tag" -> true, "none" -> !advance))(
        td(cls := "label")(label(`for` := form3.id(form(tag.field)))(tag.label)),
        td(cls := "range")(colspan := 3)(
          form3.hidden(form("fieldName"), tag.field.some),
          div(cls := "half")(form3.input(form("min"), typ = "number")(placeholder := "从")),
          div(cls := "half")(form3.input(form("max"), typ = "number")(placeholder := "到"))
        )
      )
      case lila.team.Tag.Type.Date => tr(cls := List("tr-tag" -> true, "none" -> !advance))(
        td(cls := "label")(label(`for` := form3.id(form(tag.field)))(tag.label)),
        td(cls := "range")(colspan := 3)(
          form3.hidden(form("fieldName"), tag.field.some),
          div(cls := "half")(form3.input(form("min"), klass = "flatpickr")(placeholder := "从")),
          div(cls := "half")(form3.input(form("max"), klass = "flatpickr")(placeholder := "到"))
        )
      )
      case _ => frag()
    }
  }

  def edit(mu: MemberWithUser, tags: List[Tag], campuses: List[Campus], markOption: Option[String], form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content none")(
      h2(markOption | mu.user.username),
      postForm(cls := "form3 member-editform", action := routes.TeamMember.editMemberApply(mu.member.team, mu.member.id))(
        form3.group(form("mark"), "备注（姓名）")(form3.input(_)),
        form3.group(form("campus"), "校区")(form3.select(_, campuses.map(c => c.id -> c.name))),
        tags.zipWithIndex.map {
          case (t, i) => buildEditField(mu.member, t, form(s"fields[$i]"))
        },
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def buildEditField(m: Member, tag: Tag, form: Field)(implicit ctx: Context) = {
    val vl = m.tagsIfEmpty.tagMap.get(tag.field) ?? (_.value)
    tag.typ match {
      case lila.team.Tag.Type.Text => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.Number => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl, typ = "number"),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.Date => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl, klass = "flatpickr"),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.SingleChoice => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.select2(f, vl, tag.toChoice, default = "".some),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
    }
  }

}
