package views.html.team

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ Certification, Team }
import controllers.rt_team.routes

object cert {

  def apply(t: Team, form: Form[_])(implicit ctx: Context) = {
    val cert = t.certification
    bits.layout(title = (t.name + " 认证"), evenMoreJs = frag(
      smsCaptchaTag,
      singleUploaderTag,
      jsTag("team.certification.js")
    )) {
      main(cls := "page-small box box-pad")(
        h1(a(href := routes.Team.show(t.id))(t.name), nbsp, em("认证")),
        postForm(cls := "form3", dataSmsrv := false, action := routes.TeamCertification.certificationSend(t.id))(
          form3.group(form("addr"), "详细地址")(form3.input(_)),
          form3.group(form("members"), "俱乐部人数")(form3.input(_)),
          form3.group(form("org"), "注册单位名称")(form3.input(_)),
          form3.group(form("businessLicense"), "营业执照")(form3.singleImage(_)),
          form3.group(form("leader"), "负责人")(form3.input(_)),
          views.html.base.smsCaptcha(form, "手机号码", showCode = editable(cert)),
          (cert.isEmpty || cert.??(!_.approved)) option form3.group(form("cooperator"), "邀请人账号")(form3.input(_)),
          form3.group(form("message"), "补充信息")(form3.textarea(_)(rows := 6)),
          form3.action(form3.submit(submitName(cert), klass = if (submitDisabled(cert)) "disabled" else "", isDisable = submitDisabled(cert)))
        )
      )
    }
  }

  def editable(cert: Option[Certification]): Boolean = cert match {
    case None => true
    case Some(c) => c.status match {
      case Certification.Status.Applying => false
      case Certification.Status.Approved => false
      case Certification.Status.Rejected => true
    }
  }

  def submitName(cert: Option[Certification]): String = cert match {
    case None => "提交审核"
    case Some(c) => c.status match {
      case Certification.Status.Applying => "审核中"
      case Certification.Status.Approved => "已通过"
      case Certification.Status.Rejected => "提交审核"
    }
  }

  def submitDisabled(cert: Option[Certification]): Boolean = cert match {
    case None => false
    case Some(c) => c.status match {
      case Certification.Status.Applying => true
      case Certification.Status.Approved => true
      case Certification.Status.Rejected => false
    }
  }

}
