package views.html.team.clockIn

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.task.TTaskTemplate
import lila.team.{ ClockInMember, ClockInSetting, ClockInTask, Team }
import views.html.team.clockIn.calendar.buildCalendarData
import controllers.rt_team.routes

object join {

  def apply(
    team: Team,
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = s"打卡任务",
      moreCss = frag(
        cssAt("javascripts/vendor/calendar/zabuto_calendar.css"),
        cssTag("team.clockIn")
      ),
      moreJs = frag(
        jsAt("javascripts/vendor/calendar/zabuto_calendar.min.js"),
        jsTag("team.clockIn.js")
      )
    )(
        main(cls := "page-small box box-pad clockIn-join")(
          div(cls := "box__top")(
            h1("打卡任务"),
            clockInSetting.isJoined(ctx.userId) option div(cls := "box__top__actions")(
              span(cls := "joined")("已报名")
            )
          ),
          table(cls := "desc-table")(
            tr(
              th("任务名称"),
              td(clockInSetting.name)
            ),
            tr(
              th("类型"),
              td(
                div(cls := "itemType")(taskTemplate.itemType.name),
                taskTemplate.item.fromPosition.map { fromPosition =>
                  div(cls := "boards")(
                    fromPosition.fromPositions.map { f =>
                      val fromPosition = f.fromPosition
                      div(
                        div(
                          cls := List("mini-board cg-wrap parse-fen is2d" -> true, "isAi" -> f.ai),
                          dataColor := "white",
                          dataFen := fromPosition.fen
                        )(cgWrapContent),
                        div(cls := "btm")(
                          fromPosition.clock.show,
                          nbsp, nbsp,
                          fromPosition.color.map { c => span(c.fold("执白", "执黑")) } | span("随机"),
                          fromPosition.isAi.??(isAi => isAi) option fromPosition.aiLevel.map { lv => frag(nbsp, nbsp, span(s"A.I.${lv}级")) }
                        )
                      )
                    }
                  )
                }
              )
            ),
            tr(
              th("每日", if (taskTemplate.isNumber) "数量" else "分数"),
              td(taskTemplate.total, if (taskTemplate.isNumber) "" else "（分）")
            ),
            clockInSetting.hasCoin option (
              clockInSetting.coinRule.map { coinRule =>
                tr(
                  th(team.coinSettingOrDefault.name),
                  td(s"+${coinRule}")
                )
              }
            ),
            tr(
              th("报名人数"),
              td(clockInSetting.nb)
            )
          ),
          div(cls := "calendar clockIn-calendar")(
            div(cls := "calendar-top")(
              label(cls := "title", "任务日历"),
              !clockInSetting.isJoined(ctx.userId) option postForm(cls := "form3", action := routes.TeamClockIn.join(clockInSetting.id))(
                button(cls := "button button-green small")("报名")
              ),
              clockInSetting.isJoined(ctx.userId) option postForm(cls := "form3", action := routes.TeamClockIn.leave(clockInSetting.id))(
                button(cls := "button button-red small confirm", title := "取消后不能继续进行打卡任务，是否确认？")("取消")
              )
            ),
            div(cls := "zabutoCalendar", dataAttr := buildCalendarData(clockInSetting.isJoined(ctx.userId), taskTemplate, clockInTasks, members))
          )
        )
      )

}
