package views.html.team.clockIn

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ ClockInMember, ClockInSetting, ClockInTask, Team }
import lila.task.TTaskTemplate
import lila.user.User
import lila.hub.lightClazz.{ ClazzId, ClazzName }
import play.api.libs.json.{ JsArray, Json }
import controllers.rt_team.routes

object show {

  def apply(
    team: Team,
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember],
    users: List[User],
    markMap: Map[String, Option[String]],
    clazzes: List[(ClazzId, ClazzName)],
    clazzId: Option[ClazzId]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = s"${clockInSetting.name} 详情",
      moreCss = frag(
        cssAt("javascripts/vendor/calendar/zabuto_calendar.css"),
        cssTag("team.clockIn")
      ),
      moreJs = frag(
        jsAt("javascripts/vendor/calendar/zabuto_calendar.min.js"),
        jsTag("team.clockIn.js")
      )
    )(
        main(cls := "page-small box box-pad clockIn-show")(
          h1(cls := "clockIn-show__top")(s"${clockInSetting.name} 详情"),
          readerSide(team, clockInSetting, taskTemplate, members),
          readerCalendar(clockInSetting, taskTemplate, None, clockInTasks, members),
          readerMember(clockInSetting, members, users, markMap, clazzes, clazzId)
        )
      )

  def showOfDate(
    team: Team,
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    clockInTask: ClockInTask,
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember],
    users: List[User],
    markMap: Map[String, Option[String]],
    clazzes: List[(ClazzId, ClazzName)],
    clazzId: Option[ClazzId]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = s"${clockInSetting.name} 详情",
      moreCss = frag(
        cssAt("javascripts/vendor/calendar/zabuto_calendar.css"),
        cssTag("team.clockIn")
      ),
      moreJs = frag(
        jsAt("javascripts/vendor/calendar/zabuto_calendar.min.js"),
        jsTag("team.clockIn.js")
      )
    )(
        main(cls := "page-small box box-pad clockIn-show")(
          h1(cls := "clockIn-show__top")(
            s"${clockInSetting.name} 详情"
          ),
          readerSide(team, clockInSetting, taskTemplate, members),
          readerCalendar(clockInSetting, taskTemplate, clockInTask.some, clockInTasks, members),
          readerMemberOfDate(clockInSetting, clockInTask, members, users, markMap, clazzes, clazzId)
        )
      )

  private def readerSide(team: Team, clockInSetting: ClockInSetting, taskTemplate: TTaskTemplate, members: List[ClockInMember]) =
    div(cls := "clockIn-show__side")(
      table(cls := "desc-table")(
        tr(
          th("类型"),
          td(taskTemplate.itemType.name)
        ),
        tr(
          th("每日", if (taskTemplate.isNumber) "数量" else "分数"),
          td(taskTemplate.total, if (taskTemplate.isNumber) "" else "（分）")
        ),
        clockInSetting.hasCoin option (
          clockInSetting.coinRule.map { coinRule =>
            tr(
              th(team.coinSettingOrDefault.name),
              td(s"+${coinRule}")
            )
          }
        ),
        tr(
          th("报名人数"),
          td(clockInSetting.nb)
        ),
        tr(
          th("打卡总计"),
          td(members.count(_.isComplete))
        )
      )
    )

  private def readerCalendar(
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    currentClockInTask: Option[ClockInTask],
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember]
  ) =
    div(cls := "clockIn-show__calendar")(
      div(cls := "calendar-top")(
        label(cls := "title", "任务日历"),
        currentClockInTask.map { _ =>
          a(cls := "button button-empty small", href := routes.TeamClockIn.show(clockInSetting.id, None))("累计数据")
        }
      ),
      div(cls := "zabutoCalendar", dataAttr := buildCalendarData(clockInSetting, taskTemplate, currentClockInTask, clockInTasks, members))
    )

  private def classSelect(clazzes: List[(ClazzId, ClazzName)], selected: Option[String]) = {
    st.select(st.id := "clazzId", cls := "form-control")(
      option(value := "")("全部"),
      clazzes map {
        case (value, name) => option(
          st.value := value,
          selected.has(value) option st.selected
        )(name)
      }
    )
  }

  def buildCalendarData(
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    currentClockInTask: Option[ClockInTask],
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember]
  ) = {
    JsArray(
      clockInTasks.map { clockInTask =>
        val mems = members.filter { member => clockInTask.dateString == member.dateString }
        Json.obj(
          "date" -> clockInTask.date.toString("yyyy-MM-dd"),
          "clockInTaskId" -> clockInTask.id,
          "taskName" -> taskTemplate.name,
          "canceled" -> clockInTask.isCanceled,
          "selected" -> currentClockInTask.??(_.id == clockInTask.id),
          "num" -> mems.count(_.isComplete)
        )
      }
    ).toString
  }

  private def readerMember(
    clockInSetting: ClockInSetting,
    members: List[ClockInMember],
    users: List[User],
    markMap: Map[String, Option[String]],
    clazzes: List[(ClazzId, ClazzName)],
    clazzId: Option[ClazzId]
  ) = {
    val list = members.groupBy(_.userId).mapValues(_.count(_.isComplete)).toList.sortBy(-_._2)
    div(cls := "clockIn-show__members")(
      div(cls := "members-top")(
        div(cls := "left")(
          label(cls := "title", "累计数据"),
          classSelect(clazzes, clazzId)
        ),
        a(cls := "button small export")("导出列表")
      ),
      table(cls := "slist")(
        thead(
          tr(
            th("排名"),
            th("备注（姓名）"),
            th(withSorter("累计打卡")),
            th("操作")
          )
        ),
        if (list.nonEmpty) {
          tbody(
            list.zipWithIndex.map {
              case (member, index) => {
                tr(
                  td(index + 1),
                  td(userMark(member._1, users, markMap)),
                  td(member._2),
                  td(
                    a(cls := "button button-empty small", target := "_blank", href := routes.TeamClockIn.calendar(clockInSetting.id, member._1))("详情")
                  )
                )
              }
            }
          )
        } else {
          tbody(
            tr(
              td(colspan := 4)("暂无记录")
            )
          )
        }
      )
    )
  }

  private def readerMemberOfDate(
    clockInSetting: ClockInSetting,
    clockInTask: ClockInTask,
    members: List[ClockInMember],
    users: List[User],
    markMap: Map[String, Option[String]],
    clazzes: List[(ClazzId, ClazzName)],
    clazzId: Option[ClazzId]
  ) = {
    val list = members.filter(_.dateString == clockInTask.dateString).sortBy(-_.nb)
    div(cls := "clockIn-show__members", dataId := clockInTask.id)(
      div(cls := "members-top")(
        div(cls := "left")(
          label(cls := "title clockInDate", clockInTask.dateString),
          classSelect(clazzes, clazzId)
        ),
        a(cls := "button export small")("导出列表")
      ),
      table(cls := "slist")(
        thead(
          tr(
            th("排名"),
            th("备注（姓名）"),
            th(withSorter("当天数量")),
            th("操作")
          )
        ),
        if (list.nonEmpty) {
          tbody(
            list.zipWithIndex.map {
              case (member, index) => {
                tr(
                  td(index + 1),
                  td(userMark(member.userId, users, markMap)),
                  td(cls := List("completed" -> member.isComplete))(member.nb),
                  td(
                    a(cls := "button button-empty small", target := "_blank", href := routes.TeamClockIn.calendar(clockInSetting.id, member.userId))("详情")
                  )
                )
              }
            }
          )
        } else {
          tbody(
            tr(
              td(colspan := 4)("暂无记录")
            )
          )
        }
      )
    )
  }

  private def userMark(userId: User.ID, users: List[User], markMap: Map[String, Option[String]]): String = {
    val user = users.find(_.id == userId) err s"can not find user $userId"
    markMap.get(userId).fold(none[String]) { m => m } | user.realNameOrUsername
  }

  private def withSorter(lb: String) =
    div(cls := "withSorter")(
      label(lb),
      span(cls := "sorter")(
        a(cls := List("up" -> true, "active" -> false), dataIcon := "升"),
        a(cls := List("down" -> true, "active" -> true), dataIcon := "降")
      )
    )

}
