package views.html.team.clockIn

import play.api.mvc.Call
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.team.{ ClockInSetting, Team }
import controllers.rt_team.routes

object list {

  def current(team: Team, status: Option[ClockInSetting.Status], pager: Paginator[ClockInSetting])(implicit ctx: Context) =
    layout(
      title = "当前任务",
      active = "current",
      team = team,
      status = status,
      pager = pager,
      call = routes.TeamClockIn.current(1, team.id, None)
    )

  def history(team: Team, status: Option[ClockInSetting.Status], pager: Paginator[ClockInSetting])(implicit ctx: Context) =
    layout(
      title = "历史任务",
      active = "history",
      team = team,
      status = status,
      pager = pager,
      call = routes.TeamClockIn.history(1, team.id, None)
    )

  private def layout(
    title: String,
    active: String,
    team: Team,
    status: Option[ClockInSetting.Status],
    pager: Paginator[ClockInSetting],
    call: Call
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = cssTag("team.clockIn"),
      moreJs = frag(
        infiniteScrollTag
      )
    ) {
        main(cls := "page-menu page-small clockIn-home")(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(active, team)
          ),
          div(cls := "page-menu__content box")(
            div(cls := "box__top")(
              h1(title),
              div(cls := "box__top__actions")(
                a(cls := "button button-green", href := routes.TeamClockIn.createForm(team.id), dataIcon := "O")("建立新的打卡")
              )
            ),
            table(cls := "slist")(
              thead(
                tr(
                  th("任务名称"),
                  th("参与人数"),
                  th("创建时间"),
                  th("状态"),
                  th("操作")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => addQueryParameter(call.url, "page", np)),
                  pager.currentPageResults.map { item =>
                    tr(cls := "paginated")(
                      td(a(href := routes.TeamClockIn.show(item.id, None))(item.name)),
                      td(item.nb),
                      td(momentFromNow(item.createdAt)),
                      td(item.status.name),
                      td(style := "display:flex;")(
                        a(cls := "button button-empty small", href := routes.TeamClockIn.show(item.id, None))("详情"),
                        item.editable option a(cls := "button button-empty small", href := routes.TeamClockIn.updateForm(item.id))("编辑"),
                        //a(cls := "button button-empty small", href := routes.TeamClockIn.cloneForm(item.id))("复制"),
                        item.canPublish option postForm(action := routes.TeamClockIn.publish(item.id))(
                          button(cls := "button button-empty button-green small confirm", st.title := "是否确认发布任务？")("发布")
                        ),
                        item.canClose option postForm(action := routes.TeamClockIn.close(item.id))(
                          button(cls := "button button-empty button-red small confirm", st.title := "是否确认关闭任务？")("关闭")
                        )
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 4)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  private def menuLinks(active: String, team: Team)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("current"), href := routes.TeamClockIn.current(1, team.id, None))("当前"),
      a(activeCls("history"), href := routes.TeamClockIn.history(1, team.id, None))("历史")
    )
  }

}
