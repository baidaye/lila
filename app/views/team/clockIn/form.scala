package views.html.team.clockIn

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsArray, JsObject, Json }
import lila.resource.ThemeQuery
import lila.team.Team

object form {

  def create(team: Team)(implicit ctx: Context) =
    fm(team, "新建打卡任务", None, None, None)

  def update(team: Team, settingJson: JsObject, taskJson: JsArray, taskTplJson: JsObject)(implicit ctx: Context) =
    fm(team, "修改打卡任务", settingJson.some, taskJson.some, taskTplJson.some)

  def fm(team: Team, title: String, settingJson: Option[JsObject], taskJson: Option[JsArray], taskTplJson: Option[JsObject])(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssAt("javascripts/vendor/calendar/zabuto_calendar.css"),
        cssTag("ttask"),
        cssTag("team.clockIn.form")
      ),
      moreJs = frag(
        flatpickrTag,
        jsAt("javascripts/vendor/calendar/zabuto_calendar.min.js"),
        jsAt(s"compiled/lichess.clockIn${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.clockIn=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
            "team" -> Json.obj(
              "id" -> team.id,
              "name" -> team.name,
              "coinSetting" -> Json.obj(
                "open" -> team.coinSettingOrDefault.open,
                "name" -> team.coinSettingOrDefault.name,
                "singleVal" -> team.coinSettingOrDefault.singleVal
              )
            ),
            "clockInSetting" -> settingJson,
            "clockInTask" -> taskJson,
            "task" -> taskTplJson,
            "themePuzzle" -> views.html.task.bits.themePuzzleJson(),
            "classicGames" -> lila.distinguish.Distinguish.Classic.toJson
          ))
        }""")
      )
    )(
        main(cls := "page-small box clockIn")
      )

}

