package views.html.team.clockIn

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.task.TTaskTemplate
import lila.team.{ ClockInMember, ClockInSetting, ClockInTask }
import lila.user.User
import play.api.libs.json._
import controllers.rt_team.routes

object calendar {

  def apply(
    user: User,
    markOption: Option[String],
    clockInSetting: ClockInSetting,
    taskTemplate: TTaskTemplate,
    clockInTasks: List[ClockInTask],
    members: List[ClockInMember]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = s"打卡任务",
      moreCss = frag(
        cssAt("javascripts/vendor/calendar/zabuto_calendar.css"),
        cssTag("team.clockIn")
      ),
      moreJs = frag(
        jsAt("javascripts/vendor/calendar/zabuto_calendar.min.js"),
        jsTag("team.clockIn.js")
      )
    )(
        main(cls := "page-small box clockIn-calendar")(
          h1(clockInSetting.name, "打卡明细", ctx.userId.?? { clockInSetting.isCreator } option s" · ${markOption | user.realNameOrUsername}"),
          div(cls := "zabutoCalendar", dataAttr := buildCalendarData(true, taskTemplate, clockInTasks, members)),
          clockInCount(members) > 1 option div(cls := "thumb", dataIcon := "h", s"赞！已连续打卡${clockInCount(members)}天")
        )
      )

  def clockInCount(members: List[ClockInMember]) = {
    members.filter(_.date.isBeforeNow).foldRight((0 -> false)) {
      case (member, (count, stop)) => if (member.isComplete && !stop) { (count + 1 -> false) } else (count -> true)
    }
  }._1

  def buildCalendarData(joined: Boolean, taskTemplate: TTaskTemplate, clockInTasks: List[ClockInTask], members: List[ClockInMember]) = {
    JsArray(
      clockInTasks.map { clockInTask =>
        val memberOption = members.find { member => clockInTask.dateString == member.dateString }
        Json.obj(
          "date" -> clockInTask.date.toString("yyyy-MM-dd"),
          "total" -> taskTemplate.total,
          "taskId" -> memberOption.??(_.taskId),
          "taskName" -> taskTemplate.name,
          "nb" -> memberOption.??(_.nb),
          "completed" -> memberOption.??(_.isComplete),
          "canceled" -> clockInTask.isCanceled,
          "joined" -> joined
        )
      }
    ).toString
  }

  //calendars(clockInTasks, members)
  def calendars(clockInTasks: List[ClockInTask], members: List[ClockInMember]) =
    div(cls := "calendars")(
      clockInTasks.map { clockInTask =>
        members.find { member => clockInTask.dateString == member.dateString }.map { member =>
          div(cls := "calendar")(
            a(cls := "date", href := controllers.rt_task.routes.TTask.info(member.taskId))(clockInTask.dateString),
            div(cls := List("num" -> true, "completed" -> member.isComplete))(member.nb)
          )
        } getOrElse div(cls := "calendar canceled")(
          div(cls := "date")(clockInTask.dateString),
          div(clockInTask.status.name)
        )
      }
    )

}
