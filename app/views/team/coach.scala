package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ Campus, MemberWithUser, Team }
import lila.study.Study
import lila.clazz.Clazz
import lila.resource.Capsule
import play.api.data.Form
import controllers.rt_team.routes
import lila.opening.OpeningDB

object coach {

  val dataTab = attr("data-tab")

  def apply(
    team: Team,
    coach: Option[MemberWithUser],
    campuses: List[Campus],
    students: List[MemberWithUser],
    studies: List[Study],
    capsules: List[Capsule],
    openingdbs: List[OpeningDB],
    clazzs: List[Clazz],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    bits.layout(
      title = s"${team.name} 教练",
      evenMoreJs = frag(
        flatpickrTag,
        jsTag("team.member.async.js"),
        jsTag("team.coach.js")
      )
    ) {
        main(cls := "page-menu", dataId := team.id)(
          bits.menu("coach".some),
          div(cls := "page-menu__content box box-pad coach")(
            h1(a(href := routes.Team.show(team.id))(team.name), nbsp, em("教练")),
            div(cls := "top")(
              coach.map { mu =>
                div(cls := "left")(
                  div(cls := "currentCoach")(
                    a(cls := "modal-alert changeCoach", href := routes.TeamCoach.changeCoachModal(team.id, mu.userId))(bits.userViewName(mu, markMap), iconTag("u"))
                  ),
                  a(cls := "button button-red modal-alert remove", href := routes.TeamCoach.removeCoachModal(team.id, mu.userId))("移除教练（角色）")
                )
              } getOrElse {
                div(cls := "noneTip")("您还没有添加俱乐部教练")
              },
              a(cls := "button button-green modal-alert add", href := routes.TeamCoach.addCoachModal(team.id), dataIcon := "O")("添加教练（角色）")
            ),
            div(cls := "relations")(
              div(cls := "tabs")(
                div(dataTab := "campuses", cls := "active")("授课校区"),
                div(dataTab := "clazzs")("授课班级"),
                div(dataTab := "students")("学员关系"),
                div(dataTab := "studies")("研习权限"),
                div(dataTab := "capsules")("列表权限"),
                div(dataTab := "openingdbs")("开局库权限")
              ),
              div(cls := "panels")(
                div(cls := "panel campuses active")(
                  div(cls := "top")(
                    div(cls := "actions")(
                      coach.map { cu =>
                        a(cls := "button button-green small modal-alert", href := routes.TeamCoach.addCoachCampusModal(team.id, cu.userId))("添加校区")
                      }
                    )
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("名称"),
                        th("管理员"),
                        th("地址"),
                        th("操作")
                      )
                    ),
                    coach.map { c =>
                      if (campuses.nonEmpty) {
                        tbody(
                          campuses.filter(_.coach.??(_.contains(c.userId))).map { campus =>
                            tr(
                              td(campus.name),
                              td(campus.admin.map(adm => userIdLink(adm.some, withBadge = false, text = bits.markByUserId(adm, markMap))) | "-"),
                              td(campus.addr | "-"),
                              td(
                                postForm(action := routes.TeamCoach.removeCoachCampus(team.id, c.userId))(
                                  button(name := "campusId", title := "移除后不可恢复，是否继续？", cls := "button button-empty button-red small confirm", value := campus.id)("移除")
                                )
                              )
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 4)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                ),
                div(cls := "panel clazzs")(
                  div(cls := "top")(
                    div(cls := "actions")(),
                    input(tpe := "text", cls := "search", placeholder := "搜索")
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("班级名称"),
                        th("班型"),
                        th("上课时间"),
                        th("学员"),
                        th("操作")
                      )
                    ),
                    coach.map { _ =>
                      if (clazzs.nonEmpty) {
                        tbody(
                          clazzs.map { clazz =>
                            tr(
                              td(a(target := "_blank", href := controllers.rt_klazz.routes.Clazz.detail(clazz.id))(clazz.name)),
                              td(clazz.clazzType.name),
                              td(
                                clazz.clazzType match {
                                  case Clazz.ClazzType.Week => {
                                    clazz.weekClazz.map { wc =>
                                      ul(cls := "course")(
                                        wc.weekCourse.map { c =>
                                          li(c.toString)
                                        }
                                      )
                                    }
                                  }
                                  case Clazz.ClazzType.Train => {
                                    clazz.trainClazz.map { tc =>
                                      ul(cls := "course")(
                                        tc.trainCourse.map { c =>
                                          li(c.toString)
                                        }
                                      )
                                    }
                                  }
                                }
                              ),
                              td(clazz.studentCount),
                              td(a(cls := "button button-empty small modal-alert", href := controllers.rt_klazz.routes.Clazz.editCoachModal(clazz.id))("更换"))
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 6)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                ),
                div(cls := "panel students")(
                  div(cls := "top")(
                    div(cls := "actions")(
                      coach.map { cu =>
                        frag(
                          a(cls := "button button-green small modal-alert", href := routes.TeamCoach.addCoachStuModal(team.id, cu.userId))("添加学员"),
                          postForm(action := routes.TeamCoach.removeCoachStus(team.id, cu.userId))(
                            button(title := "移除成员后不可恢复，是否继续？", cls := "button button-red small confirm")("全部移除")
                          )
                        )
                      }
                    ),
                    input(tpe := "text", cls := "search", placeholder := "搜索")
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("账号"),
                        th("备注（姓名）"),
                        th("校区"),
                        th("级别"),
                        th("加入时间"),
                        th("操作")
                      )
                    ),
                    coach.map { c =>
                      if (students.nonEmpty) {
                        tbody(
                          students.map { mu =>
                            tr(
                              td(userLink(mu.user)),
                              td(bits.userViewName(mu, markMap)),
                              td(campuses.find(_.id == mu.member.campus).map(_.name) | "-"),
                              td(mu.profile.ofLevel.name),
                              td(momentFromNow(mu.member.date)),
                              td(
                                postForm(action := routes.TeamCoach.removeCoachStu(team.id, c.userId))(
                                  button(name := "userId", title := "移除成员后不可恢复，是否继续？", cls := "button button-empty button-red small confirm", value := mu.userId)("移除")
                                )
                              )
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 6)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                ),
                div(cls := "panel studies")(
                  div(cls := "top")(
                    div(cls := "actions")(
                      coach.map { cu =>
                        postForm(action := routes.TeamCoach.removeCoachStudys(team.id, cu.userId))(
                          button(title := "确认移除？", cls := "button button-red small confirm")("全部移除")
                        )
                      }
                    ),
                    input(tpe := "text", cls := "search", placeholder := "搜索")
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("研习名称"),
                        th("贡献者权限"),
                        th("操作")
                      )
                    ),
                    coach.map { c =>
                      if (studies.nonEmpty) {
                        tbody(
                          studies.map { study =>
                            val write = study.members.containsAndHasWriteRole(c.userId)
                            tr(
                              td(a(target := "_blank", href := controllers.routes.Study.show(study.id.value))(study.name)),
                              td(coachRadio(team.id, c.userId, study.id.value, "study", write)),
                              td(
                                postForm(action := routes.TeamCoach.removeCoachStudy(team.id, c.userId))(
                                  button(name := "studyId", title := "确认移除？", cls := "button button-empty button-red small confirm", value := study.id.value)("移除")
                                )
                              )
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 3)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                ),
                div(cls := "panel capsules")(
                  div(cls := "top")(
                    div(cls := "actions")(
                      coach.map { cu =>
                        postForm(action := routes.TeamCoach.removeCoachCapsules(team.id, cu.userId))(
                          button(title := "确认移除？", cls := "button button-red small confirm")("全部移除")
                        )
                      }
                    ),
                    input(tpe := "text", cls := "search", placeholder := "搜索")
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("列表名称"),
                        th("类型"),
                        th("贡献者权限"),
                        th("操作")
                      )
                    ),
                    coach.map { c =>
                      if (capsules.nonEmpty) {
                        tbody(
                          capsules.map { capsule =>
                            val writer = capsule.members.isWriter(c.userId)
                            tr(
                              td(a(target := "_blank", href := controllers.rt_resource.routes.Capsule.updateForm(capsule.id))(capsule.name)),
                              td(capsule.itemType.name),
                              td(coachRadio(team.id, c.userId, capsule.id, "capsule", writer)),
                              td(
                                postForm(action := routes.TeamCoach.removeCoachCapsule(team.id, c.userId))(
                                  button(name := "capsuleId", title := "确认移除？", cls := "button button-empty button-red small confirm", value := capsule.id)("移除")
                                )
                              )
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 3)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                ),
                div(cls := "panel openingdbs")(
                  div(cls := "top")(
                    div(cls := "actions")(
                      coach.map { cu =>
                        postForm(action := routes.TeamCoach.removeCoachOpeningdbs(team.id, cu.userId))(
                          button(title := "确认移除？", cls := "button button-red small confirm")("全部移除")
                        )
                      }
                    ),
                    input(tpe := "text", cls := "search", placeholder := "搜索")
                  ),
                  table(cls := "slist")(
                    thead(
                      tr(
                        th("列表名称"),
                        th("贡献者权限"),
                        th("操作")
                      )
                    ),
                    coach.map { c =>
                      if (openingdbs.nonEmpty) {
                        tbody(
                          openingdbs.map { openingdb =>
                            val writer = openingdb.members.isWriter(c.userId)
                            tr(
                              td(a(target := "_blank", href := controllers.rt_resource.routes.OpeningDB.show(openingdb.id))(openingdb.name)),
                              td(coachRadio(team.id, c.userId, openingdb.id, "openingdb", writer)),
                              td(
                                postForm(action := routes.TeamCoach.removeCoachOpeningdb(team.id, c.userId))(
                                  button(name := "openingdbId", title := "确认移除？", cls := "button button-empty button-red small confirm", value := openingdb.id)("移除")
                                )
                              )
                            )
                          }
                        )
                      } else {
                        tbody(
                          tr(
                            td(colspan := 3)("暂无记录")
                          )
                        )
                      }
                    } getOrElse frag()
                  )
                )
              )
            )
          )
        )
      }

  private def coachRadio(teamId: String, coachId: String, resourceId: String, key: String, check: Boolean): Frag =
    span(cls := "form-check-input")(
      st.input(
        tpe := "checkbox",
        cls := "cmn-toggle",
        st.id := s"$coachId:$resourceId",
        dataId := s"""{"teamId":"$teamId","coachId":"$coachId","resourceId":"$resourceId","key":"${key}"}""",
        st.name := s"$coachId:$resourceId",
        check option checked
      ),
      label(`for` := s"$coachId:$resourceId")
    )

  def changeCoachModal(team: Team, coach: lila.user.User.ID, coaches: List[MemberWithUser], markMap: Map[String, Option[String]])(implicit ctx: Context) = {
    frag(
      div(cls := "modal-content changeCoachModal none")(
        h2("选择教练"),
        postForm(cls := "form3")(
          div(cls := "coach-panel")(
            div(cls := "coach-search")(
              input(tpe := "text", cls := "search", placeholder := "搜索")
            ),
            div(cls := "coach-list")(
              if (coaches.isEmpty) {
                p("没有添加过教练")
              } else {
                table(cls := "slist coach-table")(
                  tbody(
                    coaches.map { mu =>
                      tr(
                        td(
                          st.input(tpe := "radio", st.name := "coach", st.id := mu.userId, st.value := mu.userId, (coach == mu.userId) option checked), nbsp,
                          a(href := routes.TeamCoach.coach(team.id, mu.userId.some))
                        ),
                        td(userLink(mu.user, withBadge = false)),
                        td(bits.userViewName(mu, markMap)),
                        td(mu.user.profileOrDefault.currentLevel.label)
                      )
                    }
                  )
                )
              }
            )
          ),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("确定", klass = "small")
          )
        )
      )
    )
  }

  def remove(
    team: Team,
    coach: MemberWithUser,
    students: List[String],
    studies: List[Study],
    capsules: List[Capsule],
    clazzs: List[Clazz],
    coachRoleCampuses: List[Campus],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    frag(
      div(cls := "modal-content removeModal none")(
        h2("移除教练（角色）"),
        postForm(cls := "form3", action := routes.TeamCoach.removeCoachApply(team.id, coach.userId))(
          div(cls := "info")(
            div(strong("账号："), userLink(coach.user)),
            div(strong("备注（姓名）："), bits.userViewName(coach, markMap))
          ),
          div(cls := "warn")(
            coachRoleCampuses.nonEmpty option div(">> 教练仍然存在授课校区 <<"),
            clazzs.nonEmpty option div(">> 教练仍然在班级中担任教练 <<"),
            students.nonEmpty option div(">> 教练与俱乐部学员存在师生关系 <<"),
            studies.nonEmpty option div(">> 教练具有访问俱乐部研习的权限 <<"),
            capsules.nonEmpty option div(">> 教练具有访问俱乐部列表的权限 <<")
          ),
          div(cls := "tip")(
            h3("请注意："),
            ul(
              li("1、移除教练（角色）后，该用户成为俱乐部普通成员，如果教练离职，请到成员页面进行移除。"),
              li("2、移除教练角色前需要先清理该教练与学员之间的师生关系，和俱乐部所有资源的权限，并且不能是正在开课班级的教练。")
            )
          ),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("确认", klass = (if (isDisable(students, studies, clazzs)) "small disabled" else "small"), isDisable = isDisable(students, studies, clazzs))
          )
        )
      )
    )
  }

  private def isDisable(
    students: List[String],
    studies: List[Study],
    clazzs: List[Clazz]
  ) = clazzs.nonEmpty || students.nonEmpty || studies.nonEmpty

  def add(team: Team, spareMembers: List[MemberWithUser], markMap: Map[String, Option[String]])(implicit ctx: Context) = frag(
    div(cls := "modal-content addModal none")(
      h2("添加教练（角色）"),
      postForm(cls := "form3", action := routes.TeamCoach.addCoachApply(team.id))(
        div(cls := "coach-list")(
          if (spareMembers.isEmpty) {
            p(cls := "empty")("待添加教练必须已经认证并加入俱乐部.")
          } else {
            table(cls := "slist coach-table")(
              tbody(
                spareMembers.zipWithIndex.map {
                  case (mwu, index) => {
                    tr(
                      td(st.input(tpe := "radio", st.name := "coach", st.id := mwu.userId, st.value := mwu.userId, (index == 0) option checked)),
                      td(userLink(mwu.user, withBadge = false)),
                      td(bits.userViewName(mwu, markMap)),
                      td(mwu.user.profileOrDefault.currentLevel.label)
                    )
                  }
                }
              )
            )
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def addCampusModal(team: Team, coach: String, campuses: List[Campus], markMap: Map[String, Option[String]])(implicit ctx: Context) = frag(
    div(cls := "modal-content addCampusModal none")(
      h2("添加授课校区"),
      postForm(cls := "form3", action := routes.TeamCoach.addCoachCampusApply(team.id, coach))(
        div(cls := "campus-list")(
          if (campuses.isEmpty) {
            p(cls := "empty")("还没有建立校区.")
          } else {
            val coachCampuses = campuses.filterNot(_.coach.??(_.contains(coach)))
            if (coachCampuses.isEmpty) {
              p(cls := "empty")("没有可选校区.")
            } else {
              table(cls := "slist campus-table")(
                tbody(
                  coachCampuses.zipWithIndex.map {
                    case (campus, index) => {
                      tr(
                        td(st.input(tpe := "radio", st.name := "campusId", st.id := campus.id, st.value := campus.id, (index == 0) option checked)),
                        td(campus.name),
                        td(campus.admin.map(adm => userIdLink(adm.some, withBadge = false, text = bits.markByUserId(adm, markMap))) | "-"),
                        td(campus.addr | "-")
                      )
                    }
                  }
                )
              )
            }
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def addStuModal(
    form: Form[_],
    team: Team,
    coach: String,
    campuses: List[Campus],
    tags: List[lila.team.Tag]
  )(implicit ctx: Context) = {
    div(cls := "modal-content member-modal none")(
      h2("添加学员"),
      div(cls := "member-panel")(
        st.form(cls := "member-search small", action := routes.TeamMember.loadCoachMember(team.id, coach))(
          table(
            tr(
              td(cls := "label")(label("账号/备注")),
              td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
              td(cls := "label")(label("校区")),
              td(cls := "fixed")(
                form3.select(form("campus"), campuses.map(c => c.id -> c.name))
              )
            ),
            tr(
              td(cls := "label")(label("角色")),
              td(cls := "fixed")(form3.select(form("role"), List(lila.team.Member.Role.Trainee).map(r => r.id -> r.name))),
              td(cls := "label")(label("年龄")),
              td(cls := "fixed")(form3.input(form("age"), typ = "number"))
            ),
            tr(
              td(cls := "label")(label("棋协级别")),
              td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
              td(cls := "label")(label("性别")),
              td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
            ),
            tags.nonEmpty option tr(
              td(colspan := 4)(
                a(cls := "show-search")("显示高级搜索", iconTag("R"))
              )
            ),
            tags.filterNot(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
            },
            tags.filter(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
            },
            tr(
              td,
              td(colspan := 3)(
                div(cls := "action")(
                  div, submitButton(cls := "button small")("查询")
                )
              )
            )
          )
        ),
        postForm(cls := "form3", action := routes.TeamCoach.addCoachStuApply(team.id, coach))(
          div(cls := "member-list")(
            table(cls := "slist member-table")(
              thead(
                th(input(tpe := "checkbox", id := "member_chk_all"), label(`for` := "member_chk_all")),
                th("账号"),
                th("备注（姓名）"),
                th("级别")
              ),
              tbody()
            )
          ),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )
  }

}
