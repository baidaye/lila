package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.clazz.Clazz
import lila.team.{ Campus, MemberWithUser, Team }
import play.api.data.Form
import controllers.rt_team.routes

object clazzTable {

  val timeRange = List("上午", "下午", "晚上")
  def apply(
    form: Form[_],
    team: Team,
    clazzs: List[Clazz.ClazzWithCoach],
    campuses: List[Campus],
    coaches: List[MemberWithUser],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    val canReadCampus = campuses.filter(c => bits.CanReadCampus(team, c.id))
    val campusOrDefault = form("campus").value.map(c => if (c == "") Campus.defaultId(team.id) else c).orElse(Campus.defaultId(team.id).some)
    views.html.base.layout(
      title = "班级管理",
      moreJs = frag(
        jsTag("team.clazz.js")
      ),
      moreCss = cssTag("team")
    ) {
        main(cls := "box clazzTable")(
          div(cls := "box__top")(
            st.form(cls := "searchForm", action := routes.Team.clazzTable(team.id))(
              div(label("校区："), form3.select(form("campus"), canReadCampus.map(c => c.id -> c.name), "所有".some)),
              div(label("教练："), form3.select(form("coach"), coaches.map(mu => mu.user.id -> bits.userViewName(mu, markMap)), "所有".some))
            ),
            a(cls := "button button-green", href := controllers.rt_klazz.routes.Clazz.createForm(campusOrDefault), dataIcon := "O")("建立新的班级")
          ),
          table(cls := "tb")(
            thead(
              tr(
                th(style := "width: 5em;"),
                th("周一"),
                th("周二"),
                th("周三"),
                th("周四"),
                th("周五"),
                th("周六"),
                th("周日")
              )
            ),
            tbody(
              timeRange.map { t =>
                tr(
                  td(t),
                  List(1, 2, 3, 4, 5, 6, 7).map { week =>
                    td(
                      clazzs.filter(_.exists(week, t)).map { cwc =>
                        val clazz = cwc.clazz
                        val courseList = cwc.distinctCourse(week, t)
                        courseList.map { course =>
                          a(cls := "course", target := "_blank", style := "color:" + clazz.color, href := controllers.rt_klazz.routes.Clazz.detail(clazz.id))(
                            div(cls := "time-index", title := "上课时间", dataIcon := "p")(
                              span(cls := "time")(course.timeBegin, "-", course.timeEnd),
                              clazz.isWeek option span(cls := "index", title := "最近已完成的课时")("[", course.index + "/" + clazz.times, "]")
                            ),
                            div(cls := "clazz-name", title := clazz.name, dataIcon := "班")(clazz.name),
                            div(cls := "coach", title := bits.userMark(cwc.coach, markMap), dataIcon := "教")(bits.userMark(cwc.coach, markMap))
                          )
                        }
                      }
                    )
                  }
                )
              }
            )
          )
        )
      }
  }

}
