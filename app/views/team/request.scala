package views.html.team

import lila.api.Context
import play.api.data.{ Field, Form }
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ Campus, Request, Team }
import lila.common.String.html.richText
import controllers.rt_team.routes

object request {

  def requestForm(team: lila.team.Team, campuses: List[Campus], tags: List[lila.team.Tag], form: Form[_], captcha: lila.common.Captcha)(implicit ctx: Context) = {
    val title = s"${trans.joinTeam.txt()} ${team.name}"
    views.html.base.layout(
      title = title,
      moreCss = cssTag("team"),
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        infiniteScrollTag,
        captchaTag
      )
    ) {
        main(cls := "page-menu page-small")(
          bits.menu("requests".some),
          div(cls := "page-menu__content box box-pad")(
            h1(title),
            p(style := "margin:2em 0")(richText(team.description)),
            postForm(cls := "form3", action := routes.TeamMember.requestCreate(team.id))(
              team.requestTagTip option frag(
                form3.group(form("campus"), "校区")(form3.select(_, campuses.map(c => c.id -> c.name))),
                tags.filter(_.isRequest).zipWithIndex.map {
                  case (tag, i) => buildAcceptField(tag, form(s"fields[$i]"))
                }
              ),
              form3.group(form("message"), raw("消息"))(form3.textarea(_)()),
              p(cls := "is-gold", dataIcon := "")(b("同一时间只能加入一个俱乐部。")),
              p(cls := "is-gold", dataIcon := "")(b("加入俱乐部，将同步您的姓名、性别、出生年份和当前级别给俱乐部。")),
              p(cls := "is-gold", dataIcon := "")(b("认证俱乐部有权访问您的训练数据，并为您分配班级、指定教练。")),
              views.html.base.captcha(form, captcha),
              form3.globalError(form),
              form3.actions(
                a(href := routes.Team.show(team.slug))(trans.cancel()),
                form3.submit(trans.joinTeam())
              )
            )
          )
        )
      }
  }

  def accept(
    team: Team,
    request: Request,
    requestUser: Option[lila.user.User],
    referrer: String,
    tags: List[lila.team.Tag],
    campuses: List[Campus],
    form: Form[_]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content none")(
      h2("接受请求"),
      p("如果不想默认添加标签，您可以", a(href := routes.Team.setting(team.id))("设置")),
      postForm(cls := "form3 member-editform", style := "text-align:left;", action := routes.TeamMember.acceptMemberApply(team.id, request.id, referrer))(
        form3.group(form("mark"), "备注（姓名）")(form3.input2(_, vl = requestUser.fold(none[String]) { u => u.realNameOrUsername.some })),
        team.ratingSettingOrDefault.open option form3.group(form("rating"), "初始等级分")(form3.input2(_, typ = "number", vl = team.ratingSettingOrDefault.defaultRating.toString.some)),
        form3.group(form("campus"), "校区")(form3.select2(_, vl = request.campus, campuses.map(c => c.id -> c.name))),
        tags.zipWithIndex.map {
          case (t, i) => buildAcceptField(t, form(s"fields[$i]"), request.some)
        },
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存并接受", klass = "small")
        )
      )
    )
  )

  def buildAcceptField(tag: lila.team.Tag, form: Field, request: Option[Request] = None)(implicit ctx: Context) = {
    tag.typ match {
      case lila.team.Tag.Type.Text => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl = request.??(_.tagFieldValue(tag.field)))(tag.isRequired option required),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.Number => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl = request.??(_.tagFieldValue(tag.field)), typ = "number")(tag.isRequired option required),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.Date => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.input2(f, vl = request.??(_.tagFieldValue(tag.field)), klass = "flatpickr")(tag.isRequired option required),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
      case lila.team.Tag.Type.SingleChoice => form3.group(form("fieldValue"), raw(tag.label))(f => frag(
        form3.select2(f, vl = request.??(_.tagFieldValue(tag.field)), tag.toChoice, default = if (tag.isRequired) none else "".some),
        form3.hidden(form("fieldName"), tag.field.some)
      ))
    }
  }

}
