package views.html.team.test

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.team.{ Team, Campus, Member, TestStudent, TestTemplate, Tag }
import lila.user.User
import play.api.data.Form
import org.joda.time.DateTime
import controllers.rt_team.routes

object show {

  def apply(form: Form[_], tpl: TestTemplate, pager: Paginator[TestStudent.WithUser], markMap: Map[String, Option[String]])(implicit ctx: Context) =
    views.html.base.layout(
      title = s"测评-${tpl.name}",
      moreJs = frag(
        flatpickrTag,
        infiniteScrollTag,
        jsTag("team.member.async.js"),
        jsTag("team.test.show.js")
      ),
      moreCss = cssTag("team.test")
    ) {
        main(cls := "page-small test-show")(
          div(cls := "box box-pad test-tpl")(
            h1(
              a(href := routes.TeamTest.current(), dataIcon := "I"), "测评详情"
            ),
            div(cls := "test-desc")(
              div(cls := "desc-header")(
                div(cls := "title")(tpl.name),
                !tpl.isCanceled option a(cls := "button button-empty", href := routes.TeamTest.updateForm(tpl.id))("编辑")
              ),
              div(cls := "desc-body")(
                table(cls := "desc-table")(
                  tr(
                    td(cls := "label")("状态"),
                    td(tpl.status.name),
                    td(cls := "label")("限时"),
                    td(tpl.limitTime, "（分钟）")
                  ),
                  tr(
                    td(cls := "label")("题目数量"),
                    td(tpl.nbQ),
                    td(cls := "label")("达标数量"),
                    td(tpl.passedQ)
                  ),
                  tr(
                    td(cls := "label")("重试次数"),
                    td(colspan := 3)(tpl.maxRetry)
                  ),
                  tr(
                    td(cls := "label")("测评说明"),
                    td(colspan := 3)(tpl.desc | "无")
                  )
                )
              )
            )
          ),
          div(cls := "box box-pad test-students")(
            st.form(cls := "search_form", action := routes.TeamTest.show(1, tpl.id), method := "GET")(
              table(
                tr(
                  th(label("账号/备注（姓名）")),
                  td(form3.input(form("username"))),
                  th(label("状态")),
                  td(form3.select(form("status"), lila.team.DataForm.testData.testStuStatus))
                ),
                tr(
                  th(label("是否通过")),
                  td(form3.select(form("passed"), lila.team.DataForm.testData.booleanChoices)),
                  th,
                  td
                )
              ),
              div(cls := "action")(
                !tpl.isCanceled option a(href := routes.TeamTest.sendTestModal(tpl.id), cls := "button button-green modal-alert")("发送测评任务"),
                submitButton(cls := "button")("搜索")
              )
            ),
            table(cls := "slist test-list")(
              thead(
                tr(
                  th("账号"),
                  th("备注（姓名）"),
                  th("截止时间"),
                  th("完成状态"),
                  th("是否通过"),
                  th("成绩"),
                  th("处理说明（学员不可见）")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pager.currentPageResults.map { stu =>
                    tr(
                      td(userLink(stu.user, withBadge = false)),
                      td(markOrRealName(markMap, stu.user)),
                      td(stu.testStu.deadline.toString("MM-dd HH:mm")),
                      td(stu.testStu.status.name),
                      td(
                        stu.testStu.isPassed.map { passed =>
                          if (passed) "是" else "否"
                        } | "-"
                      ),
                      td(
                        stu.testStu.isFinished match {
                          case true => frag(
                            a(target := "_blank", cls := "finish", href := routes.TeamTest.testStuScore(stu.testStu.id))(stu.testStu.rightPuzzles), " / ", stu.testStu.allPuzzles
                          )
                          case false => frag("- / ", stu.testStu.allPuzzles)
                        },
                        nbsp,
                        stu.testStu.isFinished option a(target := "_blank", href := routes.TeamTest.testStuScore(stu.testStu.id))("详情")
                      ),
                      td(cls := "remark")(
                        input(cls := "form-control test-remark", dataId := stu.testStu.id, value := stu.testStu.remark, placeholder := "0~100字", min := 0, max := 100)
                      )
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 7)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  private def markOrRealName(markMap: Map[String, Option[String]], user: User): String = {
    val mark = markMap.get(user.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | user.realNameOrUsername
  }

  def sendModal(
    form: Form[_],
    tpl: TestTemplate,
    team: Team,
    campuses: List[Campus],
    tags: List[Tag]
  )(implicit ctx: Context) =
    div(cls := "modal-content member-modal send-modal none")(
      h2("发送测评任务"),
      div(cls := "member-panel")(
        st.form(cls := "member-search small", action := controllers.rt_team.routes.TeamMember.loadMember(team.id))(
          table(
            tr(
              td(cls := "label")(label("账号/备注")),
              td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
              td(cls := "label")(label("校区")),
              td(cls := "fixed")(
                form3.select(form("campus"), campuses.map(c => c.id -> c.name))
              )
            ),
            tr(
              td(cls := "label")(label("角色")),
              td(cls := "fixed")(form3.select2(form("role"), vl = "trainee".some, Member.Role.list)),
              td(cls := "label")(label("年龄")),
              td(cls := "fixed")(form3.input(form("age"), typ = "number"))
            ),
            tr(
              td(cls := "label")(label("棋协级别")),
              td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
              td(cls := "label")(label("性别")),
              td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
            ),
            tags.nonEmpty option tr(
              td(colspan := 4)(
                a(cls := "show-search")("显示高级搜索", iconTag("R"))
              )
            ),
            tags.filterNot(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
            },
            tags.filter(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
            }
          ),
          table(
            tr(
              td(colspan := 4)(
                div(cls := "action")(
                  div, submitButton(cls := "button small")("查询")
                )
              )
            )
          )
        ),
        postForm(cls := "form3 form-send", action := routes.TeamTest.sendTest(tpl.id))(
          form3.split(
            form3.group(form("deadline"), raw("截止时间"), half = true, help = raw("超过截止时间后，学员的测评任务将失效").some)(f => {
              val defaultTime = DateTime.now.plusDays(7).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toString
              form3.input2(f, vl = defaultTime.some, klass = "flatpickr")(dataMinDate := DateTime.now.plusMinutes(3).toString("yyyy-MM-dd HH:mm"))
            })
          ),
          div(cls := "member-list")(
            table(cls := "slist member-table")(
              thead(
                th(input(tpe := "checkbox", id := "member_chk_all"), label(`for` := "member_chk_all")),
                th("账号"),
                th("备注（姓名）"),
                th("级别")
              ),
              tbody()
            )
          ),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("发送", klass = "small")
          )
        )
      )
    )

}
