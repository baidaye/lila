package views.html.team.test

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.team.TestTemplate
import controllers.rt_team.routes

object list {

  def current(pager: Paginator[TestTemplate])(implicit ctx: Context) = layout(
    title = "当前测评",
    active = "current",
    pager = pager
  ) { tpl =>
    div(cls := "action")(
      a(cls := "button button-empty small", href := routes.TeamTest.show(1, tpl.id))("详情"),
      a(cls := "button button-empty small", href := routes.TeamTest.updateForm(tpl.id))("编辑"),
      postForm(action := routes.TeamTest.close(tpl.id))(
        button(cls := "button button-empty button-red small confirm", title := "是否确认关闭测评？")("关闭")
      )
    )
  }

  def history(pager: Paginator[TestTemplate])(implicit ctx: Context) = layout(
    title = "历史测评",
    active = "history",
    pager = pager
  ) { tpl =>
    div(cls := "action")(
      a(cls := "button button-empty small", href := routes.TeamTest.show(1, tpl.id))("详情"),
      postForm(action := routes.TeamTest.recover(tpl.id))(
        button(cls := "button button-empty button-green small confirm", title := "是否确认恢复测评？")("恢复")
      )
    )
  }

  private def layout(
    title: String,
    active: String,
    pager: Paginator[TestTemplate]
  )(action: TestTemplate => Frag)(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreJs = frag(
        flatpickrTag,
        infiniteScrollTag
      ),
      moreCss = cssTag("team.test")
    ) {
        main(cls := "page-menu page-small test-list")(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(active)
          ),
          div(cls := "page-menu__content box")(
            div(cls := "box__top")(
              h1("战术测评"),
              div(cls := "box__top__actions")(
                a(
                  href := routes.TeamTest.createForm(),
                  cls := "button button-green text",
                  dataIcon := "O"
                )("建立新的测评")
              )
            ),
            table(cls := "slist test-list")(
              thead(
                tr(
                  th("测评名称"),
                  th("参与人数"),
                  th("创建时间"),
                  th("状态"),
                  th("操作")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => routes.TeamTest.current(np).url),
                  pager.currentPageResults.map { tpl =>
                    tr(cls := "paginated")(
                      td(
                        a(href := routes.TeamTest.show(1, tpl.id))(tpl.name)
                      ),
                      td(tpl.nbMembers),
                      td(momentFromNow(tpl.createdAt)),
                      td(tpl.status.name),
                      td(action(tpl))
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 5)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }

  private def menuLinks(active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("current"), href := routes.TeamTest.current())("当前"),
      a(activeCls("history"), href := routes.TeamTest.history())("历史")
    )
  }

}
