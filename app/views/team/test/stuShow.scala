package views.html.team.test

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.TestStudent
import controllers.rt_team.routes

object stuShow {

  def apply(testStu: TestStudent)(implicit ctx: Context) =
    views.html.base.layout(
      title = s"测评-${testStu.tpl.name}",
      moreCss = cssTag("team.test.student")
    ) {
        main(cls := "box box-pad page-small test-student-show")(
          div(cls := "test-desc")(
            div(cls := "desc-header")(
              div(cls := "title")(testStu.tpl.name)
            ),
            div(cls := "desc-body")(
              table(cls := "desc-table")(
                tr(
                  td(cls := "label")("状态"),
                  td(
                    testStu.status match {
                      case TestStudent.Status.Finished => if (testStu.isPassed.getOrElse(false)) "已通过" else "未通过"
                      case _ => testStu.status.name
                    }
                  ),
                  td(cls := "label")("限时"),
                  td(testStu.tpl.limitTime, "（分钟）")
                ),
                tr(
                  td(cls := "label")("题目数量"),
                  td(testStu.tpl.nbQ),
                  td(cls := "label")("达标数量"),
                  td(testStu.tpl.passedQ)
                ),
                tr(
                  td(cls := "label")("截止时间"),
                  td(testStu.deadline.toString("HH-dd HH:mm")),
                  td(cls := "label")("重试次数"),
                  td(testStu.tpl.maxRetry)
                ),
                tr(
                  td(cls := "label")("测评说明"),
                  td(colspan := 3)(testStu.tpl.desc | "无")
                )
              )
            )
          ),
          testStu.status match {
            case TestStudent.Status.Created => a(cls := "button button-green button-block", href := routes.TeamTest.testStuAnswer(testStu.id))("开始答题")
            case TestStudent.Status.Started => a(cls := "button button-block", href := routes.TeamTest.testStuAnswer(testStu.id))("继续答题")
            case TestStudent.Status.Finished => a(cls := "button button-block", href := routes.TeamTest.testStuScore(testStu.id))("查看详情")
            case TestStudent.Status.Expired => button(cls := "button button-block disabled")(s"开始答题（${testStu.status.name}）")
          }
        )
      }

}
