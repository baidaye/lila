package views.html.team.test

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsObject, Json }
import lila.team.TestStudent

object stuAnswer {

  def apply(testStu: TestStudent, pref: JsObject, data: JsObject)(implicit ctx: Context) =
    views.html.base.layout(
      title = s"测评-${testStu.tpl.name}",
      moreCss = cssTag("team.test.student"),
      moreJs = frag(
        jsAt(s"compiled/lichess.teamTest${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.teamTest=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "data" -> data,
            "pref" -> pref
          ))
        }""")
      ),
      zoomable = true
    ) {
        main(cls := "teamTest")
      }

}
