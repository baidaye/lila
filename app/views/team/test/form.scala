package views.html.team.test

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.TestTemplate
import play.api.data.Form
import lila.resource.Capsule
import lila.resource.CapsuleWithPuzzleStatistics
import play.mvc.Call
import controllers.rt_team.routes

object form {

  val dataTab = attr("data-tab")
  val dataPuzzles = attr("data-puzzles")

  def create(
    form: Form[_],
    capsules: List[CapsuleWithPuzzleStatistics] = Nil,
    nb: Int = 0
  )(implicit ctx: Context) =
    fm(form, "建立测评", routes.TeamTest.createForm(), None, capsules, nb)(ctx)

  def update(form: Form[_], tpl: TestTemplate, capsules: List[CapsuleWithPuzzleStatistics] = Nil, nb: Int = 0)(implicit ctx: Context) =
    fm(form, "编辑测评", routes.TeamTest.updateForm(tpl.id), tpl.some, capsules, nb)(ctx)

  def fm(
    form: Form[_],
    title: String,
    call: Call,
    tpl: Option[TestTemplate] = None,
    capsules: List[CapsuleWithPuzzleStatistics] = Nil,
    nb: Int = 0
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = title,
      moreCss = cssTag("team.test"),
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        jsTag("team.test.form.js")
      )
    )(
        main(cls := "page-small test-create")(
          div(cls := "box box-pad test-create-form")(
            h1(title),
            postForm(cls := "form3", action := call.toString)(
              div(cls := "tabs-horiz")(
                span(dataTab := "basic", cls := "active")("基本信息"),
                span(dataTab := "items")("题库设置")
              ),
              div(cls := "tabs-content")(
                div(cls := "basic active")(
                  form3.split(
                    form3.group(form("name"), raw("测评名称"), half = true)(form3.input(_)),
                    form3.group(form("limitTime"), raw("限时（分钟）"), half = true, help = frag("最大120分钟").some)(form3.input(_, typ = "number"))
                  ),
                  form3.split(
                    form3.group(form("nbQ"), raw("题目数量"), half = true, help = frag("小于等于题库当中题目数量").some)(form3.input(_, typ = "number")),
                    form3.group(form("passedQ"), raw("达标数量"), half = true, help = frag("小于等于题目数量").some)(form3.input(_, typ = "number"))
                  ),
                  form3.split(
                    form3.group(form("rule"), raw("出题模式"), half = true)(form3.select(_, TestTemplate.Rule.choices)),
                    form3.group(form("maxRetry"), raw("允许重试次数"), half = true, help = frag("重试次数等于0时，将不会允许重试").some)(form3.input3(_, vl = "1".some, typ = "number"))
                  ),
                  form3.group(form("desc"), raw("测评说明"), help = frag("展示给学员，最多500个字").some)(form3.textarea(_)(rows := 5))
                ),
                div(cls := "items")(
                  div(cls := "item capsules")(
                    form3.hidden("capsuleIds", capsules.map(_.id).mkString(",")),
                    div(cls := "item-header")(
                      div(cls := "total")(
                        "共", span(cls := "count")(nb), "题（已去重，不支持答案中含“重试”或者“多种解题方式”的题目）"
                      ),
                      div(cls := "actions")(
                        a(href := s"${controllers.rt_resource.routes.Capsule.mineOfChooseForTeamTest()}?ids=${capsules.map(_.id).mkString(",")}", cls := "button button-green small modal-alert add-capsule")("添加战术题列表")
                      )
                    ),
                    div(cls := "item-body")(
                      table(cls := "slist")(
                        thead(
                          tr(
                            th("列表名称"),
                            th("题目数量"),
                            th("平均难度"),
                            th("最低难度"),
                            th("最高难度"),
                            th("操作")
                          )
                        ),
                        tbody(
                          capsules.zipWithIndex map {
                            case (capsule, index) => {
                              tr(
                                td(
                                  a(target := "_blank", cls := "capsuleName", href := s"/resource/capsule/${capsule.id}/puzzle/list")(capsule.name),
                                  form3.hidden(form("items")("capsules[" + index + "]"))
                                ),
                                td(cls := "total", dataPuzzles := capsule.puzzlesJson)(orNA(capsule.statistics.total)),
                                td(orNA(capsule.statistics.avgRating)),
                                td(orNA(capsule.statistics.minRating)),
                                td(orNA(capsule.statistics.maxRating)),
                                td(
                                  a(cls := "button button-empty small remove", st.title := "移除后点击“保存”生效")("移除")
                                )
                              )
                            }
                          }
                        )
                      )
                    )
                  ),
                  div(cls := "maxItemTip", dataIcon := "")("最多添加50个战术题列表")
                )
              ),
              form3.globalError(form),
              form3.actions(
                a(href := routes.TeamTest.current())("取消"),
                form3.submit("保存")
              )
            )
          )
        )
      )

  def orNA(v: Int) = if (v == 0) "NA" else v.toString

}
