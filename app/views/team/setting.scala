package views.html.team

import lila.api.Context
import play.api.data.Form
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import lila.team.DataForm.teamData.RatingTeamSetting
import lila.team.{ Campus, EloRating, Tag, Team }
import controllers.rt_team.routes

object setting {

  val dataTab = attr("data-tab")

  def apply(
    t: Team,
    campuses: List[Campus],
    tags: List[Tag],
    basicForm: Form[_],
    ratingForm: Form[_],
    coinForm: Form[_],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    bits.layout(
      title = t.name + " 设置",
      evenMoreJs = frag(jsTag("team.setting.js"))
    ) {
        main(cls := "page-small box box-pad setting")(
          h1(a(href := routes.Team.show(t.id))(t.name), nbsp, em("设置")),
          div(cls := "tabs")(
            div(dataTab := "basic", cls := "active")("基本设置"),
            div(dataTab := "campus")("校区管理"),
            div(dataTab := "rating")("内部等级分"),
            div(dataTab := "coin")("内部积分"),
            div(dataTab := "tags")("自定义标签")
          ),
          div(cls := "panels")(
            div(cls := "panel basic active")(
              div(cls := "actions")(
                a(cls := "button button-empty small", href := routes.Team.updateForm(t.id))("资料"),
                a(cls := "button button-empty small", href := routes.TeamCertification.certification(t.id))("认证"),
                a(cls := "button button-empty small", href := routes.Team.changeOwner(t.id))("转移管理员"),
                (isGranted(_.ManageTeam) || ctx.me ?? (u => t.isCreator(u.id))) && t.enabled option frag(
                  postForm(cls := "inline", action := routes.Team.disable(t.id))(
                    submitButton(dataIcon := "q", st.title := "关闭后将不可恢复", cls := "text button button-red button-empty small confirm")("关闭俱乐部")
                  )
                )
              ),
              postForm(cls := "form3", action := routes.Team.basicSettingApply(t.id))(
                form3.group(basicForm("open"), "加入政策") { f =>
                  form3.select(f, Seq(0 -> "需要管理员确认", 1 -> "对所有人开放"))
                },
                form3.group(basicForm("tagTip"), "接受邀请时补录信息") { f =>
                  form3.select(f, Seq(0 -> "否", 1 -> "是"))
                },
                form3.group(basicForm("requestTagTip"), "申请加入时填写信息") { f =>
                  form3.select(f, Seq(0 -> "否", 1 -> "是"))
                },
                form3.actions(
                  a(href := routes.Team.show(t.id))("取消"),
                  form3.submit("应用")
                )
              )
            ),
            div(cls := "panel campus")(
              div(cls := "actions")(
                a(cls := "button small campus-add", href := routes.TeamCampus.addCampusModal(t.id))("添加校区")
              ),
              table(cls := "slist")(
                thead(
                  tr(
                    th("名称"),
                    th("管理员"),
                    th("地址"),
                    th("简介"),
                    th("操作")
                  )
                ),
                tbody(
                  if (campuses.isEmpty) {
                    tr(td(colspan := 4)("- 暂无 -"))
                  } else {
                    campuses.map { c =>
                      tr(
                        td(c.name),
                        td(c.admin.map(adm => userIdLink(adm.some, withBadge = false, text = bits.markByUserId(adm, markMap))) | "-"),
                        td(c.addr | "-"),
                        td(cls := "intro", title := c.intro)(c.intro | "-"),
                        td(
                          a(cls := "button button-empty small campus-edit", href := routes.TeamCampus.editCampusModal(t.id, c.id))("编辑"),
                          c.deletable option a(cls := "button button-empty button-red small campus-delete", href := routes.TeamCampus.removeCampus(t.id, c.id))("删除")
                        )
                      )
                    }
                  }
                )
              )
            ),
            div(cls := "panel rating")(
              div(cls := "actions")(
                a(target := "_blank", href := routes.TeamRating.ratingRule)("计分规则"),
                nbsp, "•", nbsp,
                a(target := "_blank", href := routes.TeamRating.ratingAdvise)("使用建议")
              ),
              postForm(cls := "form3", action := routes.Team.ratingSettingApply(t.id))(
                form3.split(
                  form3.group(ratingForm("ratingSetting.defaultRating"), raw(s"默认初始等级分（<=${EloRating.max}）"), half = true)(form3.input(_, typ = "number")),
                  form3.group(ratingForm("ratingSetting.minRating"), raw("最低等级分（<= 600 且 >=0）"), half = true)(form3.input(_, typ = "number")(step := 25))
                ),
                form3.split(
                  form3.group(ratingForm("ratingSetting.k"), raw("发展系数K"), half = true, help = raw("表示棋手的稳定性").some)(f => form3.select(f, RatingTeamSetting.kList))
                ),
                h3(b("满足以下条件的线上对局（俱乐部内会员间），自动计算内部等级分")),
                form3.split(
                  form3.group(ratingForm("ratingSetting.turns"), raw("回合数大于"), half = true)(f => frag(form3.input(f, typ = "number"))),
                  form3.group(ratingForm("ratingSetting.minutes"), raw("单个棋手用时大于等于（分钟）"), half = true)(f => frag(form3.input(f, typ = "number")))
                ),
                form3.checkbox(ratingForm("ratingSetting.coachSupport"), raw("允许教练创建班级比赛（含线上和线下），计算俱乐部等级分"), klass = if (t.certified) "" else "none"),
                form3.checkbox(ratingForm("ratingSetting.open"), raw("开启俱乐部内部等级分")),
                form3.actions(
                  a(href := routes.Team.show(t.id))("取消"),
                  form3.submit("应用")
                )
              )
            ),
            div(cls := "panel coin")(
              postForm(cls := "form3", action := routes.Team.coinSettingApply(t.id))(
                form3.split(
                  form3.group(coinForm("coinSetting.name"), raw("积分名称"), half = true)(f => form3.input(f)(required, placeholder := "XX币、XX豆...，最长4个字")),
                  form3.group(coinForm("coinSetting.singleVal"), raw("单次积分上限"), half = true)(f => form3.input(f, typ = "number"))
                ),
                form3.checkbox(coinForm("coinSetting.open"), raw("开启俱乐部积分")),
                form3.actions(
                  a(href := routes.Team.show(t.id))("取消"),
                  form3.submit("应用")
                )
              )
            ),
            div(cls := "panel tags")(
              div(cls := "actions")(
                a(cls := "button small tag-add", href := routes.TeamTag.addTagModal(t.id))("添加标签")
              ),
              table(cls := "slist")(
                thead(
                  tr(
                    th("类型"),
                    th("名称"),
                    th("列表可见"),
                    th("申请时填写"),
                    th("申请时必填"),
                    th("可选值"),
                    th("操作")
                  )
                ),
                tbody(
                  if (tags.isEmpty) {
                    tr(td(colspan := 4)("- 暂无 -"))
                  } else {
                    tags.map { tag =>
                      tr(
                        td(tag.typ.name),
                        td(tag.label),
                        td(if (tag.isVisitable) "是" else "否"),
                        td(if (tag.isRequest) "是" else "否"),
                        td(if (tag.isRequired) "是" else "否"),
                        td(style := "max-width: 260px;")(tag.value.map {
                          span(_)
                        }.getOrElse(frag("-"))),
                        td(
                          tag.editable option a(cls := "button button-empty small tag-edit", href := routes.TeamTag.editTagModal(t.id, tag._id))("编辑"),
                          tag.editable option a(cls := "button button-empty small button-red tag-delete", href := routes.TeamTag.removeTag(t.id, tag._id))("删除")
                        )
                      )
                    }
                  }
                )
              )
            )
          )
        )
      }
  }

  def addCampus(teamId: String, form: Form[_], sort: Int)(implicit ctx: Context) = frag(
    div(cls := "modal-content campus none")(
      h2("添加校区"),
      postForm(cls := "form3", action := routes.TeamCampus.addCampusApply(teamId))(
        form3.group(form("name"), raw("校区名称"))(form3.input(_)),
        form3.split(
          form3.group(form("sort"), raw("排序"), half = true)(form3.input2(_, vl = s"$sort".some, typ = "number")),
          form3.group(form("admin"), raw("校区管理员"), half = true)(f => frag(
            st.input(cls := "form-control member-autocomplete", st.name := "adminName", dataId := teamId, placeholder := "搜索俱乐部成员（账号或备注）"),
            form3.hidden(f)
          ))
        ),
        form3.group(form("addr"), raw("校区地址（选填）"))(form3.input(_)),
        form3.group(form("intro"), raw("校区介绍（选填）"))(form3.textarea(_)()),
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def editCampus(c: Campus, adminMark: Option[String], form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content campus none")(
      h2("编辑校区"),
      postForm(cls := "form3", action := routes.TeamCampus.editCampusApply(c.team, c.id))(
        form3.hidden("campusId", c.id),
        form3.group(form("name"), raw("校区名称"))(form3.input(_)),
        form3.split(
          form3.group(form("sort"), raw("排序"), half = true)(form3.input(_, typ = "number")),
          form3.group(form("admin"), raw("校区管理员"), half = true)(f => frag(
            st.input(cls := "form-control member-autocomplete", st.name := "adminName", dataId := c.team, st.value := adminMark, placeholder := "搜索俱乐部成员（账号或备注）"),
            form3.hidden(f)
          ))
        ),
        form3.group(form("addr"), raw("校区地址（选填）"))(form3.input(_)),
        form3.group(form("intro"), raw("校区介绍（选填）"))(form3.textarea(_)()),
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def addTag(teamId: String, form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content tag none")(
      h2("添加标签"),
      postForm(cls := "form3", action := routes.TeamTag.addTagApply(teamId))(
        form3.group(form("typ"), raw("类型"))(form3.select(_, Tag.Type.list)),
        form3.group(form("label"), raw("名称"))(form3.input(_)(required)),
        form3.group(form("value"), raw("可选值"), help = raw("多个值时使用逗号分隔（如：A,B,C,D）").some, klass = "none")(form3.textarea(_)()),
        form3.split(
          form3.group(form("sort"), raw("排序"), half = true)(form3.input2(_, vl = "1".some, typ = "number")),
          form3.group(form("visitable"), raw("在成员列表中是否可见"), half = true)(form3.radio2(_, List(true -> "是", false -> "否"), "false".some))
        ),
        form3.split(
          form3.group(form("request"), raw("申请加入时是否填写"), half = true)(form3.radio2(_, List(true -> "是", false -> "否"), "false".some)),
          form3.group(form("required"), raw("申请加入时是否必填"), half = true)(form3.radio2(_, List(true -> "是", false -> "否"), "false".some))
        ),
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def editTag(tag: Tag, form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content tag none")(
      h2("编辑标签"),
      postForm(cls := "form3", action := routes.TeamTag.editTagApply(tag.team, tag._id))(
        form3.group(form("typ"), raw("类型"))(f => frag(
          "：",
          tag.typ.name,
          form3.hidden(f)
        )),
        form3.group(form("label"), raw("名称"))(form3.input(_)(required)),
        tag.typ.hasValue option form3.group(form("value"), raw("可选值"), help = raw("多个值时使用逗号分隔（如：A,B,C,D）").some)(form3.textarea(_)()),
        form3.split(
          form3.group(form("sort"), raw("排序"), half = true)(form3.input(_, typ = "number")),
          form3.group(form("visitable"), raw("在成员列表中是否可见"), half = true)(form3.radio(_, List(true -> "是", false -> "否")))
        ),
        form3.split(
          form3.group(form("request"), raw("申请加入时是否填写"), half = true)(form3.radio(_, List(true -> "是", false -> "否"))),
          form3.group(form("required"), raw("申请加入时是否必填"), half = true)(form3.radio(_, List(true -> "是", false -> "否")))
        ),
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

}
