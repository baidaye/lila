package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.team.{ Campus, MemberWithUser, Team, TeamCoin }
import lila.clazz.HomeworkV2Student
import org.joda.time.DateTime
import play.api.data.Form
import play.mvc.Call
import controllers.rt_team.routes

object coin {

  def members(
    form: Form[_],
    team: Team,
    campuses: List[Campus],
    pager: Paginator[MemberWithUser],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    val coinName = team.coinSettingOrDefault.name
    views.html.base.layout(
      title = "俱乐部积分",
      moreCss = cssTag("team"),
      moreJs = frag(
        infiniteScrollTag,
        jsTag("team.coin.members.js")
      )
    ) {
        main(cls := "page-menu team-coin", dataId := team.id)(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(team, "coinMembers")
          ),
          div(cls := "page-menu__content box box-pad members")(
            div(
              h1(team.coinSettingOrDefault.name),
              st.form(cls := "box__top__actions", action := s"${routes.TeamCoin.coinMembers(team.id)}#results", method := "GET")(
                form3.hidden("id", team.id),
                table(
                  tr(
                    td(form3.input(form("q"))(placeholder := "账号/备注（姓名）")),
                    td(form3.select(form("campus"), campuses.map(c => c.id -> c.name), "全部".some)),
                    td(submitButton(cls := "button")("查询")),
                    td(a(cls := "button coin-export")("导出"))
                  )
                )
              ),
              table(cls := "slist")(
                thead(
                  tr(
                    th("账号"),
                    th("备注（姓名）"),
                    th(coinName),
                    th("操作")
                  )
                ),
                if (pager.nbResults > 0) {
                  tbody(cls := "infinitescroll")(
                    pagerNextTable(pager, np => nextPageUrl(form, team, np)),
                    pager.currentPageResults.zipWithIndex.map {
                      case (mu, _) =>
                        tr(cls := "paginated")(
                          td(userLink(mu.user)),
                          td(bits.userViewName(mu, markMap)),
                          td(mu.member.coinOrZero),
                          td(
                            a(cls := "button button-empty small member-coin", href := routes.TeamCoin.coinMemberModal(team.id, mu.member.id))("编辑"),
                            a(cls := "button button-empty small", href := routes.TeamCoin.coinMember(team.id, mu.member.id))("详情")
                          )
                        )
                    }
                  )
                } else {
                  tbody(
                    tr(
                      td(colspan := 4)("暂无记录")
                    )
                  )
                }
              )
            )
          )
        )
      }
  }

  def memberForTeam(
    team: Team,
    mwu: MemberWithUser,
    isOwnerOrCoach: Boolean,
    pager: Paginator[TeamCoin],
    markOption: Option[String]
  )(implicit ctx: Context) = {
    views.html.base.layout(
      title = "俱乐部积分",
      moreCss = cssTag("team"),
      moreJs = frag(
        infiniteScrollTag,
        jsTag("team.coin.members.js")
      )
    ) {
        main(cls := "page-menu team-coin", dataId := team.id)(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(team, "coinMembers")
          ),
          div(cls := "page-menu__content box box-pad member")(
            memberContent(team, mwu, isOwnerOrCoach, pager, markOption)
          )
        )
      }
  }

  def memberForSelf(
    team: Team,
    mwu: MemberWithUser,
    isOwnerOrCoach: Boolean,
    pager: Paginator[TeamCoin],
    markOption: Option[String]
  )(implicit ctx: Context) = {
    views.html.base.layout(
      title = "俱乐部积分",
      moreCss = cssTag("team"),
      moreJs = frag(
        infiniteScrollTag,
        jsTag("team.coin.members.js")
      )
    ) {
        main(cls := "team-coin", dataId := team.id)(
          div(cls := "box box-pad member")(
            memberContent(team, mwu, isOwnerOrCoach, pager, markOption)
          )
        )
      }
  }

  def memberContent(
    team: Team,
    mwu: MemberWithUser,
    isOwnerOrCoach: Boolean,
    pager: Paginator[TeamCoin],
    markOption: Option[String]
  ) = frag(
    div(cls := "box__top")(
      h1(team.coinSettingOrDefault.name),
      div(cls := "box__top__actions")(
        h2(markOption | mwu.user.username, s"（${mwu.member.coinOrZero}）"),
        isOwnerOrCoach option a(cls := "button button-empty member-coin", href := routes.TeamCoin.coinMemberModal(team.id, mwu.member.id))("编辑")
      )
    ),
    table(cls := "slist")(
      thead(
        tr(
          th("时间"),
          th("信息"),
          th(s"原${team.coinSettingOrDefault.name}"),
          th(s"现${team.coinSettingOrDefault.name}"),
          th("变化"),
          th("类型")
        )
      ),
      if (pager.nbResults > 0) {
        tbody(cls := "infinitescroll")(
          pagerNextTable(pager, np => routes.TeamCoin.coinMember(team.id, mwu.member.id, np).url),
          pager.currentPageResults.map { teamCoin =>
            tr(cls := "paginated")(
              td(teamCoin.createAt.toString("yyyy-MM-dd HH:mm")),
              td(teamCoin.typ match {
                case TeamCoin.Typ.TTask => teamCoin.metaData.taskId.fold(frag(teamCoin.noteOrDefault)) { taskId => a(href := controllers.rt_task.routes.TTask.info(taskId))(teamCoin.noteOrDefault) }
                case TeamCoin.Typ.Homework => teamCoin.metaData.homeworkId.fold(frag(teamCoin.noteOrDefault)) { homeworkId => a(href := controllers.rt_klazz.routes.Homework.show(HomeworkV2Student.makeId(homeworkId, mwu.member.user)))(teamCoin.noteOrDefault) }
                case _ => teamCoin.noteOrDefault
              }),
              td(teamCoin.oldCoin),
              td(teamCoin.newCoin),
              td(cls := List("rating" -> true, "minus" -> (teamCoin.diffCoin < 0)))(if (teamCoin.diffCoin < 0) { teamCoin.diffCoin } else { "+" + teamCoin.diffCoin }),
              td(teamCoin.typ.name)
            )
          }
        )
      } else {
        tbody(
          tr(
            td(colspan := 6)("暂无记录")
          )
        )
      }
    )
  )

  def logs(
    form: Form[_],
    team: Team,
    campuses: List[Campus],
    pager: Paginator[TeamCoin],
    users: List[lila.user.User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) = {
    views.html.base.layout(
      title = "俱乐部积分",
      moreCss = cssTag("team"),
      moreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        infiniteScrollTag
      )
    ) {
        val call: Call = routes.TeamCoin.coinLogs(team.id)
        var url = if (call.url.contains("?")) call.url else call.url.concat("?q=1")
        form.data.foreach {
          case (key, value) =>
            url = url.concat("&").concat(key).concat("=").concat(value)
        }
        main(cls := "page-menu team-coin", dataId := team.id)(
          st.aside(cls := "page-menu__menu subnav")(
            menuLinks(team, "coinLogs")
          ),
          div(cls := "page-menu__content box box-pad logs")(
            div(cls := "box__top")(
              h1(team.coinSettingOrDefault.name, " 台账"),
              st.form(cls := "coin_search_form", action := s"$call#results", method := "GET")(
                table(
                  tr(
                    th(label("操作时间")),
                    td(form3.input2(form("dateMin"), vl = DateTime.now.minusMonths(3).toString("yyyy-MM-dd").some, klass = "flatpickr")),
                    th(label("至")),
                    td(form3.input2(form("dateMax"), vl = DateTime.now.toString("yyyy-MM-dd").some, klass = "flatpickr"))
                  ),
                  tr(
                    th(label("校区")),
                    td(form3.select(form("campus"), campuses.map(c => c.id -> c.name), "全部".some)),
                    th(label("类型")),
                    td(form3.select(form("typ"), lila.team.TeamCoin.Typ.selects, "全部".some))
                  ),
                  tr(
                    th(label("学员")),
                    td(form3.input(form("member"))),
                    th(label("操作人")),
                    td(form3.input(form("operator")))
                  )
                ),
                div(cls := "action")(
                  submitButton(cls := "button")("搜索")
                )
              )
            ),
            table(cls := "slist")(
              thead(
                tr(
                  th("时间"),
                  th("学员"),
                  th("校区"),
                  th("变化"),
                  th("类型"),
                  th("信息"),
                  th("操作人")
                )
              ),
              if (pager.nbResults > 0) {
                tbody(cls := "infinitescroll")(
                  pagerNextTable(pager, np => addQueryParameter(url, "p", np)),
                  pager.currentPageResults.map { teamCoin =>
                    tr(cls := "paginated")(
                      td(teamCoin.createAt.toString("yyyy-MM-dd HH:mm")),
                      td(users.find(_.id == teamCoin.userId).map(user => userLink(user, withBadge = false, text = bits.userMark(user, markMap).some))),
                      td(campuses.find(_.id == teamCoin.campusId).map(_.name) | teamCoin.campusId),
                      td(cls := List("rating" -> true, "minus" -> (teamCoin.diffCoin < 0)))(if (teamCoin.diffCoin < 0) { teamCoin.diffCoin } else { "+" + teamCoin.diffCoin }),
                      td(teamCoin.typ.name),
                      td(teamCoin.noteOrDefault),
                      td(userIdLink(teamCoin.createBy.some, withBadge = false))
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 7)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }
  }

  val dataMinusMax = attr("data-minus-max")
  val dataSingleMax = attr("data-single-max")
  def coinEdit(team: Team, mu: MemberWithUser, markOption: Option[String], form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content none")(
      h2(markOption | mu.user.username),
      postForm(cls := "form3 member-coin-modal", action := routes.TeamCoin.coinMemberApply(team.id, mu.member.id))(
        form3.split(
          form3.group(form("operate"), "操作", half = true)(form3.select(_, TeamCoin.Operate.selects)),
          form3.group(form("coin"), s"${team.coinSettingOrDefault.name}（当前：${mu.member.coinOrZero}）", half = true)(
            form3.input(_, typ = "number")(dataMinusMax := Math.min(team.coinSettingOrDefault.singleVal, mu.member.coinOrZero), dataSingleMax := team.coinSettingOrDefault.singleVal)
          )
        ),
        form3.group(form("note"), "原因说明")(form3.textarea(_)()),
        form3.globalError(form),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  private def nextPageUrl(form: Form[_], team: Team, np: Int)(implicit ctx: Context) = {
    var url: String = routes.TeamCoin.coinMembers(team.id, np).url
    form.data.foreach {
      case (key, value) => url = url.concat("&").concat(key).concat("=").concat(value)
    }
    url
  }

  private def menuLinks(team: Team, active: String)(implicit ctx: Context) = {
    def activeCls(c: String) = cls := (c == active).option("active")
    frag(
      a(activeCls("coinMembers"), href := routes.TeamCoin.coinMembers(team.id, 1))("学员积分"),
      a(activeCls("coinLogs"), href := routes.TeamCoin.coinLogs(team.id, 1))("积分台账")
    )
  }

}
