package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.common.String.html.richText
import lila.team.Team
import controllers.rt_team.routes

object list {

  def search(text: String, teams: Paginator[Team])(implicit ctx: Context) = list(
    name = trans.search.txt() + " \"" + text + "\"",
    teams = teams,
    nextPageUrl = n => routes.Team.search(text, n).url,
    tab = "all",
    search = text
  )

  def all(teams: Paginator[Team])(implicit ctx: Context) = list(
    name = trans.teams.txt(),
    teams = teams,
    nextPageUrl = n => routes.Team.all(n).url,
    tab = "all"
  )

  def mine(teams: List[Team.TeamWithMember])(implicit ctx: Context) =
    bits.layout(title = trans.myTeams.txt()) {
      main(cls := "team-list page-menu")(
        bits.menu("mine".some),
        div(cls := "page-menu__content box")(
          h1(trans.myTeams()),
          table(cls := "slist slist-pad")(
            if (teams.size > 0) tbody(teams.map(bits.teamWithMemberTr(_)))
            else noTeam()
          )
        )
      )
    }

  private def noTeam()(implicit ctx: Context) = tbody(
    tr(td(colspan := "2")(
      br,
      trans.noTeamFound()
    ))
  )

  private def list(
    name: String,
    teams: Paginator[lila.team.Team],
    nextPageUrl: Int => String,
    tab: String,
    search: String = ""
  )(implicit ctx: Context) =
    bits.layout(title = "%s - page %d".format(name, teams.currentPage)) {
      main(cls := "team-list page-menu")(
        bits.menu("all".some),
        div(cls := "page-menu__content box")(
          div(cls := "box__top")(
            st.form(cls := "search", action := routes.Team.search(), method := "get")(
              input(st.name := "text", st.placeholder := "搜索 “完整名称” 或 “编号”", st.value := search),
              submitButton(cls := "button", dataIcon := "y")
            )
          ),
          table(cls := "slist slist-pad")(
            if (teams.nbResults > 0) tbody(cls := "infinitescroll")(
              pagerNext(teams, nextPageUrl),
              tr,
              teams.currentPageResults map { bits.teamTr(_) }
            )
            else noTeam()
          )
        )
      )
    }

  def allRequests(list: List[lila.team.RequestWithUser])(implicit ctx: Context) = {
    val title = s"${list.size} 加入请求"
    bits.layout(
      title = title,
      evenMoreJs = frag(
        flatpickrTag,
        delayFlatpickrStart,
        memberAdvanceTag,
        jsTag("team.member.js")
      )
    ) {
        main(cls := "page-menu")(
          bits.menu("requests".some),
          div(cls := "page-menu__content box box-pad")(
            h1(title),
            requests(list, none)
          )
        )
      }
  }

  private[team] def requests(requests: List[lila.team.RequestWithUser], t: Option[lila.team.Team])(implicit ctx: Context) = {
    val url = t.fold(routes.Team.requests())(te => routes.Team.show(te.id)).toString
    table(cls := "slist requests @if(t.isEmpty){all}else{for-team} datatable")(
      tbody(
        requests.map { request =>
          tr(
            td(userLink(request.user)),
            /*t.isEmpty option td(teamLink(request.team)),*/
            request.team.requestTagTip option td(request.campus.map(c => frag(c.name, ", ")), request.request.requiredFieldValues(request.tags)),
            request.team.requestTagTip option td(request.request.unRequiredFieldValues(request.tags)),
            td(richText(request.message)),
            td(momentFromNow(request.date)),
            td(cls := "process")(
              postForm(cls := "process-request", action := routes.TeamMember.requestProcess(request.id))(
                input(tpe := "hidden", name := "url", value := url),
                button(name := "process", cls := "button button-empty button-red", value := "decline")(trans.decline()),
                if (request.team.tagTip) {
                  a(cls := "button button-green member-accept", href := routes.TeamMember.acceptMemberModal(request.teamId, request.id, url))(trans.accept())
                } else {
                  button(name := "process", cls := "button button-green", value := "accept")(trans.accept())
                }
              )
            )
          )
        }
      )
    )
  }

}
