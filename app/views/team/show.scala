package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.richText
import lila.team.{ Member, Team }
import play.api.data.Form
import controllers.rt_team.routes

object show {

  def apply(
    t: Team,
    member: Option[Member],
    members: List[lila.team.MemberWithUser],
    info: lila.app.mashup.TeamInfo,
    requestOrBelong: Boolean,
    error: Option[String] = None
  )(implicit ctx: Context) =
    bits.layout(
      title = t.name,
      evenMoreJs = frag(
        unsliderTag,
        flatpickrTag,
        jsTag("team.show.js")
      ),
      openGraph = lila.app.ui.OpenGraph(
        title = s"${t.name} team",
        url = s"$netBaseUrl${routes.Team.show(t.id).url}",
        description = shorten(t.description, 152)
      ).some
    )(
        main(cls := "box team-show")(
          div(cls := "box__top")(
            div(cls := "subject")(
              teamLink(t, cssClass = "large".some)
            ),
            div(
              if (t.disabled) span(cls := "closed")("已关闭")
              else trans.nbMembers.plural(t.nbMembers, strong(t.nbMembers.localize))
            )
          ),
          (info.mine || t.enabled) option div(cls := "team-show__content")(
            st.section(cls := "team-show__meta")(
              div("编号", "：", strong(t.id)),
              div("地址", "：", strong(cls := "location")(t.location)),
              div(trans.teamLeader(), "：", userIdLink(t.createdBy.some, withBadge = false)),
              member.map { m =>
                frag(
                  t.ratingSettingOrDefault.open option div("等级分", "：", a(href := (if (ctx.userId.??(t.isCreator)) routes.TeamRating.ratingDistribution(t.id) else routes.TeamRating.memberRatingDistribution(t.id, m.id)))(strong(m.intRating.map(_.toString) | "暂无"))),
                  t.coinSettingOrDefault.open option div(s"${t.coinSettingOrDefault.name}", "：", a(href := routes.TeamCoin.coinMember(m.team, m.id, 1))(strong(m.coin.map(_.toString) | "暂无")))
                )
              }
            ),
            div(cls := "team-show__members")(
              !info.bestUserIds.isEmpty option st.section(cls := "best-members")(
                h2(trans.teamBestPlayers()),
                ol(cls := "userlist best_players")(
                  info.bestUserIds.map { userId =>
                    li(userIdLink(userId.some))
                  }
                )
              ),
              (info.mine && !info.coachIds.isEmpty) option st.section(cls := "coach-members")(
                h2("教练"),
                ol(cls := "userlist")(
                  info.coachIds.map { userId =>
                    li(userIdLink(userId.some))
                    //li(a(cls := "user-link", href := routes.Coach.showById(userId))(userId))
                  }
                )
              ),
              st.section(cls := "recent-members")(
                h2("在线成员"),
                ol(cls := "userlist")(
                  members.map { mwu =>
                    li(userLink(mwu.user, isPlaying = mwu.isPlaying))
                  }
                )
              )
            ),
            st.section(cls := "team-show__desc")(
              div(cls := "description")(richText(t.description)),
              t.envPicture.isDefined option div(cls := "banner")(
                ul(cls := "items")(
                  t.envPictureOrDefault.map { image =>
                    li(img(src := dbImageUrl(image)))
                  }
                )
              ),
              /*
            info.createdByMe && t.certified option div(cls := "invites")(
              h2("邀请教练"),
              inviteList(info.invites, t, error)
            ),
            */
              info.hasRequests option div(cls := "requests")(
                h2(info.requests.size, "加入请求"),
                views.html.team.list.requests(info.requests, t.some)
              )
            ),
            st.section(cls := "team-show__actions")(
              div(cls := "join")(
                (t.enabled && !info.mine && !info.invite.isDefined) option frag(
                  if (info.requestedByMe) strong("您的加入请求正在由俱乐部管理员审核")
                  else joinButton(t, requestOrBelong)
                ),
                info.invite map { ivt =>
                  postForm(cls := "process-invite", action := routes.TeamMember.inviteProcess(t.id, ivt.id))(
                    p(strong(ivt.message)),
                    button(name := "process", cls := "button button-green", value := "accept")(trans.accept()),
                    button(name := "process", cls := "button button-empty button-red", value := "decline")(trans.decline())
                  )
                },
                (info.mine && !info.createdByMe && !member.??(_.isManager) && ctx.me.??(_.notImported)) option
                  a(cls := "button button-empty button-red member-quit", href := routes.TeamMember.quitModal(t.id))(trans.quitTeam.txt())
              ),
              div(cls := "actions")(
                (info.createdByMe || isGranted(_.Admin)) option frag(
                  div(cls := "creator-action")(
                    a(href := routes.Team.setting(t.id), cls := "button button-empty text", dataIcon := "%")(trans.settings()),
                    a(href := routes.Team.updateForm(t.id), cls := "button button-empty text", dataIcon := "m")("资料")
                  ),
                  div(cls := "creator-action")(
                    t.enabled option a(href := routes.TeamCertification.certification(t.id), cls := "button button-empty text", dataIcon := "证")("认证"),
                    a(href := routes.TeamMember.members(t.id, 1), cls := "button button-empty text", dataIcon := "r")("成员")
                  ),
                  t.enabled option div(cls := "creator-action")(
                    a(href := s"${controllers.routes.Tournament.form()}?team=${t.id}", cls := "button button-empty text", dataIcon := "g")("锦标赛"),
                    a(href := s"${controllers.rt_contest.routes.Contest.createForm(None)}?team=${t.id}", cls := "button button-empty text", dataIcon := "赛")("比赛")
                  )
                )
              )
            ),
            info.tasks.nonEmpty option frag(
              st.section(cls := "team-show__task")(
                h2(dataIcon := "任", cls := "text")("打卡任务"),
                table(cls := "slist")(
                  tbody(
                    info.tasks.map { settingWithTask =>
                      tr(
                        td(
                          a(href := controllers.rt_team.routes.TeamClockIn.join(settingWithTask.setting.id))(
                            span(cls := "name")(settingWithTask.setting.name),
                            span(cls := "setup")(s"  每天 ${settingWithTask.tpl.total}（数量）")
                          )
                        ),
                        td(cls := "infos")(
                          settingWithTask.setting.period.name, br,
                          momentFromNowOnce(settingWithTask.setting.createdAt)
                        ),
                        td(cls := "text", dataIcon := "r")(settingWithTask.setting.nb)
                      )
                    }
                  )
                )
              )
            ),
            info.featuredTours.nonEmpty option frag(
              st.section(cls := "team-show__tour team-tournaments")(
                h2(dataIcon := "赛", cls := "text")("锦标赛/比赛"),
                tour.widget(info.featuredTours)
              )
            ) /*,
          info.mine option st.section(cls := "team-show__forum")(
            h2(dataIcon := "d", cls := "text")(
              a(href := teamForumUrl(t.id))(trans.forum()),
              " (", info.forumNbPosts, ")"
            ),
            info.forumPosts.take(10).map { post =>
              st.article(
                p(cls := "meta")(
                  a(href := routes.ForumPost.redirect(post.postId))(post.topicName),
                  em(
                    userIdLink(post.userId, withOnline = false),
                    " ",
                    momentFromNow(post.createdAt)
                  )
                ),
                p(shorten(post.text, 200))
              )
            },
            a(cls := "more", href := teamForumUrl(t.id))(t.name, trans.forum(), "»")
          )*/
          )
        )
      )

  private def inviteList(invites: List[lila.team.InviteWithUser], t: lila.team.Team, error: Option[String] = None)(implicit ctx: Context) = frag(
    postForm(action := routes.TeamMember.invite(t.id))(
      div(cls := "user-invite")(
        label(`for` := "username")("搜索："),
        input(cls := "user-autocomplete", id := "username", name := "username",
          placeholder := "用户账号", autofocus, required, dataTag := "span"),
        submitButton(cls := "button confirm", dataIcon := "E")
      ),
      error.map {
        badTag(_)
      }
    ),
    table(cls := "slist")(
      thead(
        tr(
          th("教练"),
          th("邀请时间"),
          th("邀请消息")
        )
      ),
      tbody(
        invites.map { invite =>
          tr(
            td(userLink(invite.user)),
            td(momentFromNow(invite.date)),
            td(richText(invite.message))
          )
        }
      )
    )
  )

  private def joinButton(t: Team, requestOrBelong: Boolean)(implicit ctx: Context) = st.form(method := "get", cls := "inline", action := routes.TeamMember.join(t.id))(
    submitButton(cls := List("button button-green" -> true, "disabled" -> requestOrBelong), requestOrBelong option disabled, title := (if (requestOrBelong) "您已经加入其它俱乐部，或正在申请加入其它俱乐部" else ""))(trans.joinTeam())
  )

  private def joinAt(url: String)(implicit ctx: Context) =
    a(cls := "button button-green", href := url)(trans.joinTeam())

  def quitModal(team: Team, form: Form[_])(implicit ctx: Context) = frag(
    div(cls := "modal-content team-quit-modal none")(
      h2("退出俱乐部"),
      postForm(cls := "quit", action := routes.TeamMember.quit(team.id))(
        form3.group(form("password"), "登录密码", help = raw("输入登录密码以确认操作").some)(f =>
          frag(
            form3.input(f)(required),
            p(cls := "error error-password none")
          )),
        p("退出以后，与俱乐部关联的信息将被清除，请谨慎操作！"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确认", klass = "small")
        )
      )
    )
  )
}
