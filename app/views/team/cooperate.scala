package views.html.team

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.{ TeamCooperate, TeamCooperates }
import play.api.data.Form
import controllers.rt_team.routes

object cooperate {

  def apply(form: Form[_], teamCooperates: TeamCooperates, enabledTyp: Option[TeamCooperate.EnabledTyp])(implicit ctx: Context) = {
    views.html.base.layout(
      title = "我的邀请",
      moreCss = cssTag("team"),
      moreJs = frag(
        infiniteScrollTag,
        jsTag("team.cooperate.js")
      )
    ) {
        main(cls := "team-cooperate")(
          div(cls := "box box-pad")(
            div(cls := "box__top")(
              h1("我的邀请")
            ),
            table(cls := "cooperate-desc")(
              tbody(
                tr(
                  td(cls := "label")("正在积分俱乐部："),
                  td(teamCooperates.enabledTeams().size)
                ),
                tr(
                  td(cls := "label")("邀请累计积分："),
                  td(a(href := s"${controllers.routes.Member.info()}#points")(teamCooperates.points))
                )
              )
            ),
            div(cls := "cooperate-header")(
              div(cls := "left")(
                strong("说明："),
                ul(
                  li("1、邀请俱乐部认证，5年内、未关闭，满足积分要求"),
                  li("2、满足积分要求俱乐部 >=3，开始积分")
                )
              ),
              form3.select(form("typ"), TeamCooperate.EnabledTyp.selects, "全部".some)
            ),
            table(cls := "slist")(
              thead(
                tr(
                  th("俱乐部名称"),
                  th("管理员账号"),
                  th("俱乐部状态"),
                  th("认证时间"),
                  th("积分截止时间"),
                  th("满足要求")
                )
              ),
              if (teamCooperates.nonEmpty) {
                tbody(
                  teamCooperates.processCooperates(enabledTyp).map { twi =>
                    tr(
                      td(teamLink(twi.team, withIcon = false)),
                      td(userIdLink(twi.team.createdBy.some, withOnline = false)),
                      td(twi.team.enabledName),
                      td(twi.cooperate.certifyTime.toString("yyyy-MM-dd HH:mm")),
                      td(twi.cooperate.deadlineAt.toString("yyyy-MM-dd HH:mm")),
                      td(cls := List("enabled" -> twi.manzhuyaoqiu, "disabled" -> !twi.manzhuyaoqiu))(if (twi.manzhuyaoqiu) "是" else "否")
                    )
                  }
                )
              } else {
                tbody(
                  tr(
                    td(colspan := 6)("暂无记录")
                  )
                )
              }
            )
          )
        )
      }
  }

}
