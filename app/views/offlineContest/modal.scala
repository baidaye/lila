package views.html.offlineContest

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.offlineContest.{ OffBoard, OffContest, OffForbidden, OffManualPairingSource, OffPlayer, OffRound }
import lila.team.{ Campus, EloRating, Tag, Team }
import views.html.contest.modal.{ dataRangeTags, dataSingleTags }
import play.api.data.Form
import controllers.rt_contest.routes

object modal {

  def playerChoose(
    form: Form[_],
    contest: OffContest,
    clazzs: List[(String, String)],
    teamOption: Option[Team],
    tags: List[Tag],
    campuses: List[Campus],
    allPlayers: List[OffPlayer.AllPlayerWithUser],
    players: List[OffPlayer.PlayerWithUser]
  )(implicit ctx: Context) = frag(
    div(cls := "modal-content member-modal modal-player-choose none")(
      h2("选择棋手"),
      st.form(
        cls := "member-search small",
        dataSingleTags := s"""["${tags.filterNot(_.typ.range).map(_.field).mkString("\",\"")}"]""",
        dataRangeTags := s"""["${tags.filter(_.typ.range).map(_.field).mkString("\",\"")}"]"""
      )(
          table(
            tr(
              td(cls := "label")(label("主办方")),
              td(colspan := 3)(
                contest.typ match {
                  case OffContest.Type.Public | OffContest.Type.TeamInner => teamLinkById(contest.organizer, false)
                  case OffContest.Type.ClazzInner => clazzLinkById(contest.organizer)
                }
              )
            ),
            tr(
              td(cls := "label")(label("账号/备注")),
              td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
              td,
              td
            ),
            (contest.typ == OffContest.Type.Public || contest.typ == OffContest.Type.TeamInner) option teamOption.map { team =>
              tr(
                td(cls := "label")(label("校区")),
                td(cls := "fixed")(form3.select(form("campus"), campuses.filter(c => views.html.team.bits.CanReadCampus(team, c.id)).map(c => c.id -> c.name), "".some)),
                td(cls := "label")(label("班级")),
                td(cls := "fixed")(form3.select(form("clazzId"), clazzs, "".some))
              )
            },
            tr(
              td(cls := "label")(label("等级分")),
              td(cls := "fixed")(form3.input(form("teamRatingMin"), typ = "number")(placeholder := "俱乐部等级分（最小）")),
              td(cls := "label")(label("至")),
              td(cls := "fixed")(form3.input(form("teamRatingMax"), typ = "number")(placeholder := "俱乐部等级分（最大）"))
            ),
            tr(
              td(cls := "label")(label("棋协级别")),
              td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
              td(cls := "label")(label("性别")),
              td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
            ),
            tags.nonEmpty option tr(
              td(colspan := 4)(
                a(cls := "show-search")("显示高级搜索", iconTag("R"))
              )
            ),
            tags.filterNot(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
            },
            tags.filter(_.typ.range).zipWithIndex.map {
              case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
            },
            tr(
              td(colspan := 4)(
                div(cls := "action")(
                  div, submitButton(cls := "button small")("查询")
                )
              )
            )
          )
        ),
      postForm(cls := "form3 player-choose-transfer", action := routes.OffContest.playerChoose(contest.id))(
        form3.hidden("players", players.map(_.userId).mkString(",")),
        div(cls := "player-transfer")(
          views.html.base.transfer[OffPlayer.AllPlayerWithUser, OffPlayer.PlayerWithUser](allPlayers, players) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.userId, dataAttr := left.json.toString())),
              td(cls := "name")(left.realName)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.userId, dataAttr := right.json.toString())),
              td(cls := "name")(right.realName)
            )
          }
        ),
        form3.actions(
          a(cls := "cancel small")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def externalPlayer(c: OffContest)(implicit ctx: Context) = frag(
    div(cls := "modal-content player-external none")(
      h2("临时棋手"),
      postForm(cls := "form3", action := routes.OffContest.externalPlayer(c.id))(
        table(
          tr(
            th("姓名："),
            td(input(st.name := "username", minlength := 2, maxlength := 20, required))
          ),
          c.teamRated option tr(
            th("等级分："),
            td(input(st.name := "teamRating", tpe := "number", min := 0, max := EloRating.max, value := 1500, required))
          )
        ),
        p("注：如果等级分为0，参与对局不影响对手等级分。"),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def playerForbidden(contest: OffContest, players: List[OffPlayer.PlayerWithUser], forbidden: Option[OffForbidden])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-forbidden none")(
      h2("回避设置"),
      postForm(cls := "form3", action := routes.OffContest.forbiddenApply(contest.id, forbidden.map(_.id)))(
        form3.hidden("playerIds", forbidden.??(_.playerIds.mkString(","))),
        div(cls := "fname")(input(name := "name", value := forbidden.map(_.name), required, minlength := 2, maxlength := 20, placeholder := "组名")),
        views.html.base.transfer[OffPlayer.PlayerWithUser, OffPlayer.PlayerWithUser](
          leftOptions = players.filterNot(p => forbidden.??(_.playerIds.contains(p.playerId))),
          rightOptions = players.filter(p => forbidden.??(_.playerIds.contains(p.playerId))),
          leftLabel = "参赛棋手",
          rightLabel = "回避棋手",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.playerId, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.realName)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.playerId, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.realName)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def manualAbsent(contest: OffContest, round: OffRound, players: List[OffPlayer.PlayerWithUser])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-absent none")(
      h2("弃权设置"),
      postForm(cls := "form3", action := routes.OffContest.manualAbsent(contest.id, round.no))(
        form3.hidden("joins", players.filterNot(_.player.absent).map(_.playerId).mkString(",")),
        form3.hidden("absents", players.filter(pwu => pwu.player.manualAbsent && !pwu.player.absentOr).map(_.playerId).mkString(",")),
        views.html.base.transfer[OffPlayer.PlayerWithUser, OffPlayer.PlayerWithUser](
          leftOptions = players.filterNot(_.player.absent),
          rightOptions = players.filter(pwu => pwu.player.manualAbsent && !pwu.player.absentOr),
          leftLabel = "参赛棋手",
          rightLabel = "弃权棋手",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.id, dataAttr := left.json.toString())),
              td(cls := "name")(nbsp, strong("#", left.no), nbsp, left.realName)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.id, dataAttr := right.json.toString())),
              td(cls := "name")(nbsp, strong("#", right.no), nbsp, right.realName)
            )
          },
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def manualPairing(c: OffContest, r: OffRound, boards: List[OffBoard], players: List[OffPlayer.PlayerWithUser], source: OffManualPairingSource)(implicit ctx: Context) = frag(
    div(cls := "modal-content manual-pairing none")(
      h2("与...交换"),
      postForm(cls := "form3", action := routes.OffContest.manualPairing(c.id, r.no))(
        p(cls := "is-gold", dataIcon := "")("手动调整对阵可能会影响后续匹配，请谨慎操作！"),
        form3.hidden("contestId", c.id),
        !source.isBye option form3.hidden("source", s"""{"isBye":0, "board": "${source.board_.id}", "color": ${source.color_.fold(1, 0)}}"""),
        source.isBye option form3.hidden("source", s"""{"isBye":1, "player": "${source.player_.id}"}"""),
        div(cls := "manual-source")(
          table(cls := "slist")(
            tbody(
              !source.isBye option {
                val player = findPlayer(source.board_.player(source.color_).no, players)
                tr(
                  td(source.board_.no),
                  td("#", player.no),
                  td(player.realName),
                  td(source.color_.fold("白方", "黑方"))
                )
              },
              source.isBye option {
                val player = findPlayer(source.player_.no, players)
                tr(
                  td("-"),
                  td("-"),
                  td(player.realName),
                  td("-")
                )
              }
            )
          )
        ),
        div(cls := "manual-filter")(
          input(tpe := "text", cls := "manual-filter-search", placeholder := "搜索")
        ),
        div(cls := "manual-list")(
          table(cls := "slist")(
            thead(
              tr(
                th("台号"),
                th("白方"),
                th("黑方")
              )
            ),
            tbody(
              boards.map { board =>
                {
                  val white = findPlayer(board.whitePlayer.no, players)
                  val black = findPlayer(board.blackPlayer.no, players)
                  tr(
                    td(board.no),
                    td(cls := List("white" -> true, "disabled" -> (source.board.??(_.is(board)) && source.color ?? (_.name == "white"))))(
                      label(`for` := white.playerId, cls := "user-label")("#", white.no, nbsp, white.realName),
                      nbsp,
                      input(tpe := "radio", id := white.playerId, name := "user-radio", value := s"""{"isBye": 0, "board": "${board.id}", "color": 1}""", (source.board.??(_.is(board)) && source.color ?? (_.name == "white")) option disabled)
                    ),
                    td(cls := List("black" -> true, "disabled" -> (source.board.??(_.is(board)) && source.color ?? (_.name == "black"))))(
                      label(`for` := black.playerId)("#", black.no, nbsp, black.realName),
                      nbsp,
                      input(tpe := "radio", id := black.playerId, name := "user-radio", value := s"""{"isBye": 0, "board": "${board.id}", "color": 0}""", (source.board.??(_.is(board)) && source.color ?? (_.name == "black")) option disabled)
                    )
                  )
                }
              },
              players.filter(_.player.isBye(r.no)).map { pwu =>
                tr(title := "轮空")(
                  td("-"),
                  td(cls := List("white" -> true, "disabled" -> source.isBye))(
                    label(`for` := pwu.playerId, cls := "user-label")("#", pwu.no, nbsp, pwu.realName),
                    nbsp,
                    input(tpe := "radio", id := pwu.playerId, name := "user-radio", value := s"""{"isBye": 1, "player": "${pwu.playerId}"}""", source.isBye option disabled)
                  ),
                  td(cls := List("black" -> true))("-")
                )
              }
            )
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  private def findPlayer(no: OffPlayer.No, players: List[OffPlayer.PlayerWithUser]): OffPlayer.PlayerWithUser =
    players.find(_.player.no == no) err s"can not find player：$no"

  def manualResult(c: OffContest, r: OffRound, b: OffBoard)(implicit ctx: Context) = frag(
    div(cls := "modal-content manual-result none")(
      h2("设置成绩"),
      postForm(cls := "form3", action := routes.OffContest.manualResult(c.id, b.id))(
        select(name := "result")(
          OffBoard.Result.all.map { r =>
            option(value := r.id)(s"${r.id}（${r.name}）")
          }
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("保存", klass = "small")
        )
      )
    )
  )

  def roundSwap(c: OffContest, r: OffRound, rounds: List[OffRound], boards: List[OffBoard], players: List[OffPlayer.PlayerWithUser])(implicit ctx: Context) = frag(
    div(cls := "modal-content contest-swap none")(
      h2("交换轮次"),
      postForm(cls := "form3", action := routes.OffContest.roundSwap(c.id, r.no))(
        div(cls := "contest-swap-source")(s"第${r.no}轮"),
        div(cls := "contest-swap-rounds")(
          div(cls := "contest-swap-rounds-left")(
            rounds.take(c.actualRound).filterNot(_.id == r.id).map { round =>
              val rno = round.no
              div(cls := "contest-swap-round")(
                div(cls := List("radio" -> true, "active" -> (round.no == r.no + 1), "disabled" -> (round.no <= r.no || !r.isPairing)))(
                  st.input(tpe := "radio", st.name := "contest-swap-rd", st.id := s"contest-swap-rd_${round.id}", st.value := round.id, (round.no <= r.no || !r.isPairing) option disabled, (round.no == r.no + 1) option st.checked), nbsp,
                  label(`for` := s"contest-swap-rd_${round.id}")(s"第${rno}轮")
                ),
                players.find(_.player.isBye(rno)) match {
                  case Some(pwu) => div(cls := "byeUser")(pwu.realName, s"(${pwu.player.no}) 轮空")
                  case None => frag()
                }
              )
            }
          ),
          div(cls := "contest-swap-rounds-right")(
            rounds.take(c.actualRound).filterNot(_.id == r.id).map { round =>
              val rno = round.no
              div(dataId := s"${round.id}", cls := List("contest-swap-round" -> true, "disabled" -> (round.no <= r.no || !r.isPairing), "none" -> (round.no != r.no + 1)))(
                table(cls := "slist contest-swap-boards")(
                  tbody(
                    round.isOverPairing option boards.filter(_.roundNo == rno).map { board =>
                      val white = findPlayer(board.whitePlayer.no, players)
                      val black = findPlayer(board.blackPlayer.no, players)
                      tr(
                        td(cls := "no")(s"#${board.no}"),
                        td(cls := "player")(white.realName, s"(${white.no})"),
                        td("Vs."),
                        td(cls := "player")(black.realName, s"(${black.no})")
                      )
                    },
                    players.filter(_.player.noBoard(rno)).sortWith((p1, p2) => p1.player.roundOutcomeSort(rno) > p2.player.roundOutcomeSort(rno)).map {
                      playerWithUser =>
                        {
                          val player = playerWithUser.player
                          tr(
                            td(cls := "no")("-"),
                            td(cls := "player")(playerWithUser.realName, s"(${player.no})"),
                            td(player.roundOutcomeFormat(rno)),
                            td("-")
                          )
                        }
                    }
                  )
                )
              )
            }
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("确定", klass = "small")
        )
      )
    )
  )

  def scoreDetail(no: OffRound.No, player: OffPlayer, players: List[OffPlayer.PlayerWithUser], boards: List[OffBoard])(implicit ctx: Context) = {
    val pwu = findPlayer(player.no, players)
    div(cls := "modal-content score-detail none")(
      h2(s"（${player.no}）${pwu.realName} 详情"),
      table(cls := "slist")(
        thead(
          tr(
            th("轮次"),
            th("序号"),
            th("白方"),
            th("结果"),
            th("黑方"),
            th("序号")
          )
        ),
        tbody(
          (1 to no).toList.map { n =>
            boards.find(_.roundNo == n).map { board =>
              val color = ctx.me.fold("white")(u => board.colorOfById(u.id).name)
              val white = findPlayer(board.whitePlayer.no, players)
              val black = findPlayer(board.blackPlayer.no, players)
              tr(
                td(n),
                td(white.no),
                td(white.realName),
                td(strong(board.resultFormat)),
                td(black.realName),
                td(black.no)
              )
            } getOrElse {
              val p = findPlayer(player.no, players)
              tr(
                td(n),
                td("-"),
                td(p.realName),
                td(strong(player.roundOutcomeFormat(n))),
                td("-"),
                td("-")
              )
            }
          }
        )
      )
    )
  }

}
