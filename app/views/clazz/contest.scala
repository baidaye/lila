package views.html.clazz

import play.api.data.Form
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.team.Team
import lila.contest.DataForm
import lila.clazz.{ Clazz, Course }
import controllers.rt_klazz.routes

object contest {

  def apply(clazz: Clazz, course: Course, team: Option[Team], form: Form[_])(implicit ctx: Context) = views.html.base.layout(
    title = "创建随堂赛",
    moreCss = frag(
      cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
      cssTag("clazz")
    ),
    moreJs = frag(
      flatpickrTag,
      jsTag("clazz.contest.js")
    )
  )(
      main(cls := "page-small", dataNotAccept := s"${!(ctx.me.isDefined && ctx.me.??(_.hasResource))}")(
        div(cls := "contest__form box box-pad")(
          h1("创建随堂赛"),
          postForm(cls := "form3", action := routes.Clazz.contestApply(clazz.id, course.id))(
            form3.group(form("name"), raw("比赛名称"), half = true)(form3.input(_)),
            form3.split(
              div(cls := "form-half form-flex")(
                form3.checkbox(form("rated"), raw("计算等级分"), help = raw("对局将会计分<br/>并影响棋手的等级分").some, klass = "rate"),
                team.?? { t => t.ratingSetting.?? { rs => rs.open && rs.coachSupport } } option form3.checkbox(form("teamRated"), raw("记录俱乐部等级分"), help = raw("影响棋手所在俱乐部的等级分").some)
              )
            ),
            form3.group(form("position"), raw("起始位置"), klass = "starts-position") { field =>
              val fieldVal = field.value
              val url = fieldVal.fold(controllers.routes.Editor.index)(f => controllers.routes.Editor.load(f)).url
              frag(
                div(cls := "group-child")(
                  st.select(st.id := form3.id(field), name := field.name, cls := "form-control")(
                    option(value := chess.StartingPosition.initial.fen, fieldVal.has(chess.StartingPosition.initial.fen) option selected)(chess.StartingPosition.initial.name),
                    option(value := fieldVal, fieldVal.??(f => !chess.StartingPosition.allWithInitial.exists(_.fen == f)) option selected, dataId := "option-load-fen")("输入FEN"),
                    option(dataId := "option-load-situation")("载入局面"),
                    chess.StartingPosition.categories.map { categ =>
                      optgroup(attr("label") := categ.name)(
                        categ.positions.map { v =>
                          option(value := v.fen, fieldVal.has(v.fen) option selected)(v.fullName)
                        }
                      )
                    }
                  )
                ),
                div(cls := "group-child")(
                  input(
                    cls := List("form-control position-paste" -> true, "none" -> fieldVal.??(f => chess.StartingPosition.allWithInitial.exists(_.fen == f))),
                    placeholder := "在此处粘贴FEN棋谱", value := fieldVal
                  )
                ),
                div(cls := "group-child")(
                  a(cls := List("board-link" -> true, "none" -> fieldVal.has(chess.StartingPosition.initial.fen)), target := "_blank", href := url)(
                    div(cls := "preview")(
                      fieldVal.map { f =>
                        (chess.format.Forsyth << f).map { situation =>
                          div(
                            cls := "mini-board cg-wrap parse-fen is2d",
                            dataColor := situation.color.name,
                            dataFen := f
                          )(cgWrapContent)
                        }
                      }
                    )
                  )
                )
              )
            },
            form3.split(
              form3.group(form("clockTime"), raw("基本用时"), half = true)(form3.select(_, DataForm.clockTimeChoices)),
              form3.group(form("clockIncrement"), raw("每步棋加时"), half = true)(form3.select(_, DataForm.clockIncrementChoices))
            ),
            form3.group(form("startsAt"), raw("比赛开始时间"), half = true)(form3.flatpickr(_)),
            form3.globalError(form),
            form3.actions(
              a(href := s"${routes.Clazz.detail(clazz.id)}#courses")("取消"),
              form3.submit("保存并发布", icon = "g".some)
            )
          )
        )
      )
    )

}
