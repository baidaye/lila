package views.html.clazz.homework

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.clazz.{ Course, HomeworkV2 }
import controllers.rt_klazz.routes

object notStart {

  def apply(course: Course)(implicit ctx: Context) =
    views.html.base.layout(
      title = "课后练",
      moreCss = cssTag("homework"),
      moreJs = frag(
        embedJsUnsafe("""$(function() { $('.go-back').click(function() { history.go(-1); })})""")
      )
    ) {
        main(cls := "box box-pad page-small homework-notFound")(
          h1("没有开始"),
          br,
          div(cls := "message")(
            s"本节课还未开始，您可以在【${course.dateTimeStr}】后访问此页面完成课后练。"
          ),
          br, br, br, br,
          a(cls := "button button-flat go-back")("返回")
        )
      }

}
