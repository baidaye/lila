package views.html.clazz.homework

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.libs.json._
import lila.user.User
import lila.task.{ TTask, TTaskTemplate }
import lila.clazz.HomeworkV2Report.PuzzleSort
import lila.clazz.{ Clazz, Course, HomeworkV2, HomeworkV2Report, HomeworkV2ReportView, HomeworkV2Student, Student }
import lila.team.Team
import play.api.data.Form
import controllers.rt_klazz.routes

object report {

  private val dataLastmove = attr("data-lastmove")
  private val moveTag = tag("move")
  private val indexTag = tag("index")
  private val dataAttr = attr("data-attr")
  private val dataXAxis = attr("data-xaxis")
  private val dataSeries = attr("data-series")
  private val dataAvailable = attr("data-available")
  private val dataUpdateAt = attr("data-updateat")

  def apply(
    team: Option[Team],
    clazz: Clazz,
    course: Course,
    hwt: HomeworkV2.WithTask,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort] = None,
    completeRateSortType: Option[String] = None,
    students: List[Student],
    users: List[User],
    markMap: Map[String, Option[String]]
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = "课后练报告",
      moreJs = frag(
        echartsTag,
        echartsThemeTag,
        jsTag("task.info.js"),
        jsTag("clazz.homework.report.js")
      ),
      moreCss = cssTag("homework")
    ) {
        val homework = hwt.homework
        main(cls := "box box-pad page-small homework-report", dataUpdateAt := hrv.updateAt.getMillis, dataAvailable := homework.available, dataId := hwt.id)(
          div(cls := "box__top")(
            h1("课后练报告"),
            div(cls := "uptime")(
              span("更新时间："),
              span(hrv.updateAt.toString("yyyy-MM-dd HH:mm")),
              nbsp, nbsp,
              a(href := s"${routes.Homework.refreshReport(hwt.id)}&puzzleSort=${puzzleSort.map(_.id) | ""}")("更新")
            )
          ),
          table(cls := "homework__course")(
            tbody(
              tr(
                td(
                  label("班级："),
                  a(href := routes.Clazz.detail(clazz.id))(strong(clazz.name))
                ),
                td(
                  label("上课时间："),
                  strong(course.courseFormatTime)
                )
              ),
              tr(
                td(
                  label("课节："),
                  strong(course.index)
                ),
                td(
                  label("截止时间："),
                  strong(homework.deadlineAt.map(_.toString("yyyy-MM-dd HH:mm")))
                )
              )
            )
          ),
          div(cls := "report")(
            div(cls := "part completeRateDistribute")(
              h3(cls := "part-title")("完成情况总览"),
              {
                val series = JsArray(
                  hrv.completeRateDistribute.map {
                    case (distributeRate, rateMap) => {
                      JsArray(Seq(
                        JsNumber(distributeRate),
                        JsNumber(rateMap.size),
                        JsArray(
                          rateMap.map {
                            case (student, studentRate) => {
                              Json.obj(
                                "u" -> markByUserId(student, users, markMap),
                                "r" -> studentRate
                              )
                            }
                          }
                        )
                      ))
                    }
                  }
                )
                div(cls := "completeRateDistribute-warp")(
                  div(cls := "completeRateDistribute-chart", dataSeries := series.toString)
                )
              }
            ),
            div(cls := "part common")(
              h3(cls := "part-title")("训练目标"),
              div(
                table(cls := "slist common-slist")(
                  thead(
                    tr(
                      th(cls := "task-user")("学员"),
                      hwt.commonOrEmpty.map { homeworkV2RealTaskTpl =>
                        val taskTpl = homeworkV2RealTaskTpl.taskTpl
                        th(cls := "task-item")(
                          div(cls := "task-item-withtag")(
                            span(cls := "task-name", title := taskTpl.name)(taskTpl.itemType.name, if (!taskTpl.isNumber) "+" else ""),
                            taskTpl.hasTag option div(cls := "tooltip-tags", dataIcon := "")(
                              div(cls := "tags-wrap")(
                                views.html.task.bits.buildCondTags(taskTpl.itemType, taskTpl.item)
                              )
                            )
                          )
                        )
                      },
                      th,
                      th(cls := "task-rate")(withSorter("完成率", completeRateSortType)),
                      (hwt.homework.hasCoin || hwt.homework.hasCoinZero) option team.map(t => th(cls := "task-coin")(t.coinSettingOrDefault.name)),
                      th(cls := "task-action")
                    )
                  )
                ),
                div(cls := "common-scroll")(
                  table(cls := "slist common-slist")(
                    tbody(
                      hrv.sortedCommon(students.map(_.userId), completeRateSortType).map {
                        case (userId, list) => {
                          val user = users.find(_.id == userId) err s"can not find user $userId"
                          val completeRate = hrv.completeRate.map(_.getOrElse(userId, 0.0D)).getOrElse(0.0D)
                          val coinDiff = hrv.coinDiff.fold(0)(_.getOrElse(userId, 0))
                          tr(
                            td(cls := "task-user")(userLink(user, withBadge = false, text = markOrRealName(markMap, user).some)),
                            list.map { com =>
                              val report = com.report
                              td(cls := "task-item")(div(span(cls := List("complete" -> report.completed, "uncomplete" -> !report.completed))(report.num), span("/"), span(report.total)))
                            },
                            td,
                            td(cls := "task-rate")(s"$completeRate%"),
                            (hwt.homework.hasCoin || hwt.homework.hasCoinZero) option td(cls := "task-coin")(
                              if (hwt.homework.overDeadline | false) a(cls := "modal-alert modal-task-coin", href := routes.Homework.updateCoinModal(HomeworkV2Student.makeId(homework.id, userId)))(if (hwt.homework.hasCoinZero && coinDiff == 0) "手动" else s"+$coinDiff")
                              else span(title := "课后练截止后可手动设置")(s"+$coinDiff")
                            ),
                            td(cls := "task-action")(a(target := "_blank", href := routes.Homework.show(HomeworkV2Student.makeId(homework.id, userId)), title := "查看详情")("详情"))
                          )
                        }
                      }.toList
                    )
                  )
                )
              )
            ),
            hwt.hasPractice option div(cls := "part practice")(
              h3(cls := "part-title")("练习任务"),
              hwt.practiceOrEmpty.zipWithIndex.map {
                case (homeworkV2RealTaskTpl, index) => {
                  val taskTpl = homeworkV2RealTaskTpl.taskTpl
                  homeworkV2RealTaskTpl.taskItem match {
                    case TTask.TTaskItemType.CapsulePuzzleItem => readerCapsulePuzzle(taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.ReplayGameItem => readerReplayGame(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.RecallGameItem => readerRecallGame(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.DistinguishGameItem => readerDistinguishGame(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.FromPositionItem => readerFromPosition(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.FromPgnItem => readerFromPgn(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case TTask.TTaskItemType.FromOpeningdbItem => readerFromOpeningdb(clazz, taskTpl, hrv, puzzleSort, users, markMap, index)
                    case _ => emptyFrag
                  }
                }
              }
            )
          )
        )
      }

  val dataSortType = attr("data-sort-type")
  def withSorter(lb: String, completeRateSortType: Option[String]) = div(cls := "withSorter")(
    label(lb),
    span(cls := "sorter")(
      a(cls := List("up" -> true, "active" -> completeRateSortType.has("up")), dataSortType := "up", dataIcon := "升序"),
      a(cls := List("down" -> true, "active" -> completeRateSortType.has("down")), dataSortType := "down", dataIcon := "降序")
    )
  )

  private def readerCapsulePuzzle(
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.capsulePuzzles.find(_.taskTpl.id == taskTpl.id).map { reportCapsulePuzzle =>
      val puzzlesWithReport = puzzleSort match {
        case Some(sort) => sort match {
          case PuzzleSort.No => reportCapsulePuzzle.report
          case PuzzleSort.FirstRightRate => reportCapsulePuzzle.report.sortBy(_.report.firstMoveRightRate.rate)
          case PuzzleSort.RightRate => reportCapsulePuzzle.report.sortBy(_.report.rightRate.rate)
          case _ => reportCapsulePuzzle.report
        }
        case None => reportCapsulePuzzle.report
      }

      div(cls := "practice-part practice-part-capsulePuzzle")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        {
          val series = JsArray(
            reportCapsulePuzzle.firstRightRateDistribute.map {
              case (distributeRate, userRates) => {
                JsArray(Seq(
                  JsNumber(distributeRate),
                  JsNumber(userRates.size),
                  JsArray(
                    userRates.map { userRate =>
                      Json.obj(
                        "u" -> markByUserId(userRate.student, users, markMap),
                        "r" -> userRate.rate
                      )
                    }
                  )
                ))
              }
            }
          )

          div(cls := "firstRightRateDistribute-chart", dataSeries := series.toString)
        },
        div(cls := "puzzleSort")(
          strong("排序："),
          div(cls := "radio-group")(
            HomeworkV2Report.PuzzleSort.selector.map {
              case (id, name) => {
                span(cls := "radio")(
                  st.input(tpe := "radio", st.id := s"puzzleSort_$id", st.value := id, st.name := s"puzzleSort_${taskTpl.id}", ((puzzleSort.isEmpty && HomeworkV2Report.PuzzleSort(id) == HomeworkV2Report.PuzzleSort.No) || puzzleSort.contains(HomeworkV2Report.PuzzleSort(id))) option checked),
                  nbsp, label(cls := "radio-label", `for` := s"puzzleSort_$id")(name)
                )
              }
            }
          )
        ),
        {
          val xAxis = JsArray(puzzlesWithReport.zipWithIndex.map {
            case (_, i) => JsNumber(i + 1)
          })

          val series = JsArray(
            List(
              Json.obj(
                "name" -> "完成率",
                "type" -> "line",
                "data" -> JsArray(puzzlesWithReport.map { pz =>
                  JsNumber(pz.report.completeRate.rate)
                })
              ),
              Json.obj(
                "name" -> "正确率",
                "type" -> "line",
                "data" -> JsArray(puzzlesWithReport.map { pz =>
                  JsNumber(pz.report.rightRate.rate)
                })
              ),
              Json.obj(
                "name" -> "首次正确率",
                "type" -> "line",
                "data" -> JsArray(puzzlesWithReport.map { pz =>
                  JsNumber(pz.report.firstMoveRightRate.rate)
                })
              )
            )
          )
          div(cls := "puzzle-all-chart", dataXAxis := xAxis.toString, dataSeries := series.toString)
        },
        ul(cls := "puzzles")(
          puzzlesWithReport.zipWithIndex.map {
            case (pz, i) => {
              val puzzle = pz.puzzle
              val report = pz.report
              val completes = report.completeRate.students.map { student =>
                markByUserId(student, users, markMap)
              }.mkString("\",\"")
              val rights = report.rightRate.students.map { student =>
                markByUserId(student, users, markMap)
              }.mkString("\",\"")
              val firstMoveRights = report.firstMoveRightRate.students.map { student =>
                markByUserId(student, users, markMap)
              }.mkString("\",\"")

              li(cls := List("none" -> (i > 0)))(
                table(
                  tr(
                    td(cls := "td-chart")(
                      div(cls := "puzzle-chart", dataSeries := s"""[[0,${report.completeRate.rate},["$completes"]],[1,${report.rightRate.rate},["$rights"]],[2,${report.firstMoveRightRate.rate},["$firstMoveRights"]]]""")
                    ),
                    td(cls := "td-board")(
                      a(
                        cls := "mini-board cg-wrap parse-fen is2d",
                        dataColor := puzzle.color.name,
                        dataFen := puzzle.fen,
                        dataLastmove := puzzle.lastMove,
                        target := "_blank",
                        href := controllers.routes.Puzzle.show(puzzle.id)
                      )(cgWrapContent),
                      div(cls := "controls")(
                        button(cls := List("button button-empty small prev" -> true, "disabled" -> (i == 0)))("上一题"),
                        span("第", strong(i + 1), "题"),
                        button(cls := List("button button-empty small next" -> true, "disabled" -> (i == puzzlesWithReport.size - 1)))("下一题")
                      )
                    ),
                    td(
                      div(cls := "first-move")(
                        label("第一步正确走法"),
                        ul(cls := "right")(
                          report.rightMoveDistribute.map { mn =>
                            val rightMoveStudents = mn.students match {
                              case None => ""
                              case Some(rights) => {
                                rights.map { student =>
                                  markByUserId(student, users, markMap)
                                }.mkString("\",\"")
                              }
                            }
                            li(id := "puzzle-first-move-num")(
                              span(dataAttr := s"""["$rightMoveStudents"]""")(mn.move, nbsp, nbsp, mn.num)
                            )
                          }
                        )
                      ),
                      div(cls := "first-move")(
                        label("第一步其他走法"),
                        ul(cls := "wrong")(
                          report.wrongMoveDistribute.take(6).map { mn =>
                            val wrongMoveStudents = mn.students match {
                              case None => ""
                              case Some(wrongs) => {
                                wrongs.map { student =>
                                  markByUserId(student, users, markMap)
                                }.mkString("\",\"")
                              }
                            }
                            li(id := "puzzle-first-move-num")(
                              span(dataAttr := s"""["$wrongMoveStudents"]""")(mn.move, nbsp, nbsp, mn.num)
                            )
                          }
                        )
                      )
                    )
                  )
                )
              )
            }
          }
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerReplayGame(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.replayGames.find(_.taskTpl.id == taskTpl.id).map { reportReplayGame =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "replayGames")(
          table(
            tbody(
              reportReplayGame.report.map { rg =>
                val replayGame = rg.replayGame
                val completeStudents = rg.students match {
                  case None => ""
                  case Some(completes) => {
                    completes.map { student =>
                      markByUserId(student, users, markMap)
                    }.mkString("\",\"")
                  }
                }

                val uncompleteStudents = {
                  rg.students match {
                    case None => clazz.studentIds.toSet
                    case Some(completes) => clazz.studentIds.toSet -- completes.toSet
                  }
                } map { student =>
                  markByUserId(student, users, markMap)
                } mkString ("\",\"")

                val series = s"""[{"name":"完成","value":${rg.complete},"students":["$completeStudents"]}, {"name":"未完成","value":${hrv.size - rg.complete},"students":["$uncompleteStudents"]}]"""
                tr(
                  td(cls := "td-chart")(
                    div(cls := "replayGame-chart", dataSeries := series)
                  ),
                  td(cls := "td-board")(
                    a(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := replayGame.root,
                      target := "_blank",
                      href := replayGame.chapterLink
                    )(cgWrapContent)
                  ),
                  td(
                    div(cls := "replayGame-header")(
                      a(cls := "title", title := replayGame.name)(href := replayGame.chapterLink, target := "_blank")(replayGame.name)
                    ),
                    div(cls := "moves")(
                      replayGame.moves.map { move =>
                        moveTag(
                          indexTag(move.index, "."),
                          move.white.map { w =>
                            span(dataFen := w.fen)(w.san)
                          } getOrElse span(cls := "disabled")("..."),
                          move.black.map { b =>
                            span(dataFen := b.fen)(b.san)
                          } getOrElse span(cls := "disabled")
                        )
                      }
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerRecallGame(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.recallGames.find(_.taskTpl.id == taskTpl.id).map { reportRecallGame =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "recallGames")(
          table(
            tbody(
              reportRecallGame.report.map { rg =>
                val recallGame = rg.recallGame
                val turnsDistribute = rg.turnsDistribute
                val series = JsArray(
                  turnsDistribute.map { r =>
                    JsArray(Seq(
                      JsString(r.turns.toString),
                      JsNumber(r.num),
                      JsArray(
                        r.students.fold(List.empty[JsString]) { students =>
                          students.map { student =>
                            JsString(markByUserId(student, users, markMap))
                          }
                        }
                      )
                    ))
                  }
                )
                tr(
                  td(cls := "td-chart")(
                    div(cls := "recallGame-chart", dataSeries := series.toString)
                  ),
                  td(cls := "td-board")(
                    span(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := recallGame.root,
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(
                    div(
                      div(cls := "recallGame-header")(
                        recallGame.name.map { name =>
                          frag(span(cls := "title", title := name)(name), br)
                        },
                        span("棋色：", recallGame.color.map { c => if (c.name == "white") "白方" else "黑方" } | "双方"),
                        nbsp, nbsp,
                        recallGame.orient.map { orient =>
                          frag(
                            span("棋盘方向：", if (orient == "white") "白方" else "黑方"),
                            nbsp, nbsp
                          )
                        },
                        span("回合数：", recallGame.turns.map(_.toString) | "所有")
                      ),
                      recallGame.moves.isEmpty || recallGame.moves.??(_.isEmpty) option div(cls := "pgn")(
                        strong(
                          recallGame.pgn.split("\r\n\r\n")(1)
                        )
                      ),
                      recallGame.moves.map { moves =>
                        moves.nonEmpty option div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.index, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen)(w.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen)(b.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerDistinguishGame(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.distinguishGames.find(_.taskTpl.id == taskTpl.id).map { reportDistinguishGame =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "distinguishGames")(
          table(
            tbody(
              reportDistinguishGame.report.map { dg =>
                val distinguishGame = dg.distinguishGame
                val turnsDistribute = dg.turnsDistribute
                val series = JsArray(
                  turnsDistribute.map { r =>
                    JsArray(Seq(
                      JsString(r.turns.toString),
                      JsNumber(r.num),
                      JsArray(
                        r.students.fold(List.empty[JsString]) { students =>
                          students.map { student =>
                            JsString(markByUserId(student, users, markMap))
                          }
                        }
                      )
                    ))
                  }
                )

                tr(
                  td(cls := "td-chart")(
                    div(cls := "distinguishGame-chart", dataSeries := series.toString)
                  ),
                  td(cls := "td-board")(
                    span(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := distinguishGame.root,
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(
                    div(
                      div(cls := "distinguishGame-header")(
                        span(cls := "title", title := distinguishGame.name)(distinguishGame.name), br,
                        span("棋盘方向：", distinguishGame.orient.map { c => if (c == "white") "白方" else "黑方" }),
                        nbsp, nbsp,
                        span("走棋步数：", distinguishGame.rightTurns, "/", distinguishGame.turns.map(_.toString) | "所有")
                      ),
                      distinguishGame.moves.map { moves =>
                        moves.nonEmpty option div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.index, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen)(w.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen)(b.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                    )
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerFromPosition(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.fromPositions.find(_.taskTpl.id == taskTpl.id).map { reportFromPosition =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "fromPositions")(
          table(
            tbody(
              reportFromPosition.report.map { fp =>
                val fromPosition = fp.fromPosition
                val roundDistribute = fp.roundDistribute
                val series = JsArray(
                  roundDistribute.map { r =>
                    JsArray(Seq(
                      JsString(r.rounds.toString),
                      JsNumber(r.num),
                      JsArray(
                        r.students.fold(List.empty[JsString]) { students =>
                          students.map { student =>
                            JsString(markByUserId(student, users, markMap))
                          }
                        }
                      )
                    ))
                  }
                )

                tr(
                  td(cls := "td-chart")(
                    div(cls := "fromPosition-chart", dataSeries := series.toString)
                  ),
                  td(cls := "td-board")(
                    span(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := fromPosition.fen,
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(
                    span("对局数：", fromPosition.num, "，时钟：", fromPosition.clock.show, "，棋色：", fromPosition.color.map(c => c.fold("白", "黑")) | "随机", fromPosition.isAi.??(isAi => isAi) option fromPosition.aiLevel.map { lv => frag("，", s"A.I${lv}级") })
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerFromPgn(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.fromPgns.find(_.taskTpl.id == taskTpl.id).map { reportFromPgn =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "fromPgns")(
          table(
            tbody(
              reportFromPgn.report.map { fp =>
                val fromPgn = fp.fromPgn
                val roundDistribute = fp.roundDistribute
                val series = JsArray(
                  roundDistribute.map { r =>
                    JsArray(Seq(
                      JsString(r.rounds.toString),
                      JsNumber(r.num),
                      JsArray(
                        r.students.fold(List.empty[JsString]) { students =>
                          students.map { student =>
                            JsString(markByUserId(student, users, markMap))
                          }
                        }
                      )
                    ))
                  }
                )

                tr(
                  td(cls := "td-chart")(
                    div(cls := "fromPgn-chart", dataSeries := series.toString)
                  ),
                  td(cls := "td-board")(
                    span(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := fromPgn.pgnSetup.lastFen,
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(
                    span("对局数：", fromPgn.num, "，时钟：", fromPgn.clock.show, "，棋色：", fromPgn.color.map(c => c.fold("白", "黑")) | "随机", fromPgn.isAi.??(isAi => isAi) option fromPgn.aiLevel.map { lv => frag("，", s"A.I${lv}级") }),
                    fromPgn.opening match {
                      case Left(pgn) => {
                        val moves = fromPgn.pgnSetup.pgnOpeningDB.toTurns()
                        div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.number, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen.value)(w.move.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen.value)(b.move.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                      case Right(openingdb) => views.html.resource.openingdb.bits.showOpeningdb(fromPgn.pgnSetup)
                    }
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def readerFromOpeningdb(
    clazz: Clazz,
    taskTpl: TTaskTemplate,
    hrv: HomeworkV2ReportView,
    puzzleSort: Option[HomeworkV2Report.PuzzleSort],
    users: List[User],
    markMap: Map[String, Option[String]],
    index: Int
  ) = {
    hrv.practice.fromOpeningdbs.find(_.taskTpl.id == taskTpl.id).map { reportFromOpeningdb =>
      div(cls := "practice-part")(
        h3(cls := "practice-title")(s"${index + 1}、", taskTpl.name),
        div(cls := "fromOpeningdbs")(
          table(
            tbody(
              reportFromOpeningdb.report.map { fp =>
                val fromOpeningdb = fp.fromOpeningdb
                val roundDistribute = fp.roundDistribute
                val series = JsArray(
                  roundDistribute.map { r =>
                    JsArray(Seq(
                      JsString(r.rounds.toString),
                      JsNumber(r.num),
                      JsArray(
                        r.students.fold(List.empty[JsString]) { students =>
                          students.map { student =>
                            JsString(markByUserId(student, users, markMap))
                          }
                        }
                      )
                    ))
                  }
                )

                tr(
                  td(cls := "td-chart")(
                    div(cls := "fromOpeningdb-chart", dataSeries := series.toString)
                  ),
                  td(cls := "td-board")(
                    span(
                      cls := "mini-board cg-wrap parse-fen is2d",
                      dataColor := "white",
                      dataFen := fromOpeningdb.pgnSetup.lastFen,
                      target := "_blank"
                    )(cgWrapContent)
                  ),
                  td(
                    span("对局数：", fromOpeningdb.num, "，时钟：", fromOpeningdb.clock.show, "，棋色：", fromOpeningdb.color.map(c => c.fold("白", "黑")) | "随机", fromOpeningdb.isAi.??(isAi => isAi) option fromOpeningdb.aiLevel.map { lv => frag("，", s"A.I${lv}级") }),
                    fromOpeningdb.opening match {
                      case Left(pgn) => {
                        val moves = fromOpeningdb.pgnSetup.pgnOpeningDB.toTurns()
                        div(cls := "moves")(
                          moves.map { move =>
                            moveTag(
                              indexTag(move.number, "."),
                              move.white.map { w =>
                                span(dataFen := w.fen.value)(w.move.san)
                              } getOrElse span(cls := "disabled")("..."),
                              move.black.map { b =>
                                span(dataFen := b.fen.value)(b.move.san)
                              } getOrElse span(cls := "disabled")
                            )
                          }
                        )
                      }
                      case Right(openingdb) => views.html.resource.openingdb.bits.showOpeningdb(fromOpeningdb.pgnSetup)
                    }
                  )
                )
              }
            )
          )
        )
      )
    }
  }.getOrElse(emptyFrag)

  private def markOrRealName(markMap: Map[String, Option[String]], user: User): String = {
    val mark = markMap.get(user.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | user.realNameOrUsername
  }

  private def markByUserId(userId: User.ID, users: List[User], markMap: Map[String, Option[String]]): String = {
    val u = users.find(_.id == userId)
    markMap.get(userId) match {
      case Some(markOption) => markOption match {
        case Some(mark) => mark
        case None => u.map(_.realNameOrUsername) | userId
      }
      case None => u.map(_.realNameOrUsername) | userId
    }
  }

  def coinEditModal(
    form: Form[_],
    team: Team,
    student: Option[User],
    studentMark: Option[String],
    shwt: HomeworkV2Student.WithTask
  )(implicit ctx: Context) = {
    frag(
      div(cls := "modal-content homework-coin-edit none")(
        h2(s"设置 ${team.coinSettingOrDefault.name}"),
        postForm(cls := "form3", action := routes.Homework.updateCoin(shwt.id))(
          div(cls := "user")(student.map(userLink(_, text = studentMark, withBadge = false)) | shwt.studentId),
          div(cls := "completeRate")(strong("完成率："), shwt.progressFormat),
          form3.group(form("coinDiff"), team.coinSettingOrDefault.name)(form3.input2(_, vl = shwt.homework.coinDiffOrZero.toString.some)),
          form3.actions(
            a(cls := "cancel small")("取消"),
            button(cls := "button small")("保存")
          )
        )
      )
    )
  }

}
