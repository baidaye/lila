package views.html.clazz.homework

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.richText
import lila.clazz.{ Clazz, Course, HomeworkV2Student }
import lila.task.TTask
import lila.user.User
import lila.team.Team
import controllers.rt_klazz.routes

object show {

  private val dataLastmove = attr("data-lastmove")
  private val moveTag = tag("move")
  private val indexTag = tag("index")
  private val dataIsStudent = attr("data-is-student")
  private val dataIsDeadline = attr("data-is-deadline")

  def apply(
    shwt: HomeworkV2Student.WithTask,
    team: Option[Team],
    clazz: Clazz,
    course: Course,
    coach: Option[User],
    student: Option[User],
    studentMark: Option[String],
    showPuzzleFirstComplete: Boolean = false
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = "课后练",
      moreJs = frag(
        cookieTag,
        tagsinputTag,
        jsTag("resource.saveTo.js"),
        jsTag("task.info.js"),
        jsTag("clazz.homework.show.js")
      ),
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("homework")
      )
    ) {
        val isStudent = ctx.userId.??(shwt.homework.studentId == _)
        main(cls := "box box-pad page-small homework-show task-info", dataId := shwt.id, dataIsStudent := isStudent, dataIsDeadline := shwt.isDeadlined)(
          div(cls := "box__top")(
            h1("课后练"),
            div(cls := "top-rigth")(
              table(
                tr(
                  td(student.map(stu => userLink(stu, text = studentMark, withBadge = false))),
                  td("完成进度："),
                  td(shwt.progressFormat)
                ),
                ((shwt.homework.hasCoin || shwt.homework.hasCoinZero) && (shwt.isFinished || shwt.isDeadlined)) option {
                  team.map { team =>
                    tr(
                      td,
                      td(team.coinSettingOrDefault.name, "："),
                      td("+", shwt.homework.coinDiff | 0, " / ", shwt.homework.coinRule | 0)
                    )
                  }
                },
                (shwt.homework.hasCoin && !(shwt.isFinished || shwt.isDeadlined)) option {
                  team.map { team =>
                    shwt.homework.coinRule.map { coinRule =>
                      tr(
                        td,
                        td(team.coinSettingOrDefault.name, "："),
                        td("+", coinRule, "（最多）")
                      )
                    }
                  }
                },
                tr(
                  td,
                  td,
                  td(a(target := "_blank", href := routes.Homework.rule())("统计规则"))
                )
              )
            )
          ),
          table(cls := "homework__course")(
            tbody(
              tr(
                coach.map { c =>
                  td(
                    label("教练："),
                    a(href := controllers.routes.Coach.showById(c.id))(strong(c.realNameOrUsername))
                  )
                },
                td
              ),
              tr(
                td(
                  label("班级："),
                  a(href := routes.Clazz.detail(clazz.id))(strong(clazz.name))
                ),
                td(
                  label("上课时间："),
                  strong(course.courseFormatTime)
                )
              ),
              tr(
                td(
                  label("课节："),
                  strong(course.index)
                ),
                td(
                  label("截止时间："),
                  strong(cls := "deadlineAt")(shwt.homework.deadlineAt.toString("yyyy-MM-dd HH:mm"))
                )
              )
            )
          ),
          div(cls := "info")(
            div(cls := "part split")(
              div(cls := "half")(
                h3(cls := "part-title")("课节总结"),
                div(
                  shwt.homework.summary.map(richText(_)).getOrElse(span(cls := "nil")("无."))
                )
              ),
              div(cls := "half")(
                h3(cls := "part-title")("预习内容"),
                div(
                  shwt.homework.prepare.map(richText(_)).getOrElse(span(cls := "nil")("无."))
                )
              )
            ),
            shwt.common.map { common =>
              div(cls := "part common")(
                h3(cls := "part-title")("训练目标"),
                table(cls := "slist")(
                  thead(
                    tr(
                      th("练习项目"),
                      th("本次目标"),
                      th("当前"),
                      th("状态"),
                      isStudent option th
                    )
                  ),
                  tbody(
                    common.tasks.map { homeworkV2RealTask =>
                      val taskItem = homeworkV2RealTask.taskItem
                      val task = homeworkV2RealTask.task
                      tr(
                        td(taskItem.name, views.html.task.bits.buildCondTags(task.itemType, task.item)),
                        td(task.formatTotal),
                        td(task.num),
                        td(
                          if (isStudent) {
                            if (task.canPlay) a(cls := List("complete" -> task.isComplete, "uncomplete" -> !task.isComplete), target := "_blank", href := controllers.rt_task.routes.TTask.doTask(task.id))(s"${if (task.isComplete) "已" else "未"}完成")
                            else span(cls := "uncomplete")("未完成")
                          } else {
                            span(cls := List("complete" -> task.isComplete, "uncomplete" -> !task.isComplete))(s"${if (task.isComplete) "已" else "未"}完成")
                          }
                        ),
                        isStudent option td(
                          if (task.canPlay)
                            a(cls := "button button-green small", target := "_blank", href := controllers.rt_task.routes.TTask.doTask(task.id))(if (task.isComplete) "继续训练" else "去训练")
                          else a(cls := "button button-green small disabled")("去训练")
                        )
                      )
                    }
                  )
                )
              )
            },
            shwt.practice.map { practice =>
              div(cls := "part practice")(
                h3(cls := "part-title")("练习任务"),
                div(cls := "practice-part")(
                  practice.tasks.zipWithIndex.map {
                    case (homeworkV2RealTask, index) => {
                      val task = homeworkV2RealTask.task
                      task.itemType match {
                        case TTask.TTaskItemType.CapsulePuzzleItem => views.html.task.bits.capsulePuzzle(task, withHeader = (index + 1).some, actionFrag = capsulePuzzleAction(shwt, task, showPuzzleFirstComplete), showPuzzleFirstComplete)
                        case TTask.TTaskItemType.ReplayGameItem => views.html.task.bits.replayGame(task, withHeader = (index + 1).some)
                        case TTask.TTaskItemType.RecallGameItem => views.html.task.bits.recallGame(task, withHeader = (index + 1).some)
                        case TTask.TTaskItemType.DistinguishGameItem => views.html.task.bits.distinguishGame(task, withHeader = (index + 1).some)
                        case TTask.TTaskItemType.FromPositionItem => views.html.task.bits.fromPositionGame(task, withHeader = (index + 1).some)
                        case TTask.TTaskItemType.FromPgnItem => views.html.task.bits.fromPgnGame(task, withHeader = (index + 1).some)
                        case TTask.TTaskItemType.FromOpeningdbItem => views.html.task.bits.fromOpeningdbGame(task, withHeader = (index + 1).some)
                        case _ => div().some
                      }
                    }
                  }
                )
              )
            }
          )
        )
      }

  def capsulePuzzleAction(shwt: HomeworkV2Student.WithTask, task: TTask, showPuzzleFirstComplete: Boolean) =
    task.item.capsulePuzzle.map { capsulePuzzle =>
      frag(
        div(cls := "switch-warp")(
          label(`for` := "show-first-complete")("首次完成情况", nbsp),
          div(cls := "switch")(
            st.input(
              tpe := "checkbox",
              st.id := "show-first-complete",
              dataId := shwt.id,
              cls := "cmn-toggle",
              showPuzzleFirstComplete option checked
            ),
            label(`for` := "show-first-complete")
          )
        ),
        div(cls := "rightRate")(
          label("正确率："),
          span(if (showPuzzleFirstComplete) capsulePuzzle.puzzleFirstRightRate else capsulePuzzle.puzzleRightRate, "%")
        )
      )
    }

}
