package views.html.clazz.homework

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.clazz.{ Clazz, Course, HomeworkV2 }
import controllers.rt_klazz.routes

object modal {

  def clone(homeworkV2: HomeworkV2, clazz: Clazz, course: Course, clazzes: List[Clazz])(implicit ctx: Context) = {
    frag(
      div(cls := "modal-content none")(
        h2(s"复制“${homeworkV2.name | HomeworkV2.makeName(clazz.name, course.index)}”到..."),
        postForm(cls := "form3", action := routes.Homework.clone(homeworkV2.id, "clazzId", "courseId"))(
          div(cls := "names")(
            strong(clazz.name)
          ),
          div(cls := "courses")(),
          br,
          p("注意：本次课后练原有内容将被覆盖。"),
          form3.actions(
            a(cls := "cancel small")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )
  }

}
