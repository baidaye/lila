package views.html.clazz.homework

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.clazz.{ Clazz, Course }
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsObject, Json }

object form {

  def clone(
    clazz: Clazz,
    course: Course,
    teamJson: Option[JsObject],
    clazzJson: JsObject,
    courseJson: JsObject,
    nextCourseJson: Option[JsObject],
    homeworkJson: JsObject,
    prevHomeworkJson: Option[JsObject],
    cloneHomeworkJson: Option[JsObject],
    isTeamManager: Boolean
  )(implicit ctx: Context) =
    fm(
      clazz,
      course,
      teamJson,
      clazzJson,
      courseJson,
      nextCourseJson,
      homeworkJson,
      prevHomeworkJson,
      cloneHomeworkJson,
      isTeamManager
    )

  def apply(
    clazz: Clazz,
    course: Course,
    teamJson: Option[JsObject],
    clazzJson: JsObject,
    courseJson: JsObject,
    nextCourseJson: Option[JsObject],
    homeworkJson: JsObject,
    prevHomeworkJson: Option[JsObject],
    cloneHomeworkJson: Option[JsObject],
    isTeamManager: Boolean
  )(implicit ctx: Context) =
    fm(
      clazz,
      course,
      teamJson,
      clazzJson,
      courseJson,
      nextCourseJson,
      homeworkJson,
      prevHomeworkJson,
      cloneHomeworkJson,
      isTeamManager
    )

  def fm(
    clazz: Clazz,
    course: Course,
    teamJson: Option[JsObject],
    clazzJson: JsObject,
    courseJson: JsObject,
    nextCourseJson: Option[JsObject],
    homeworkJson: JsObject,
    prevHomeworkJson: Option[JsObject],
    cloneHomeworkJson: Option[JsObject],
    isTeamManager: Boolean
  )(implicit ctx: Context) =
    views.html.base.layout(
      title = "新建课后练",
      moreCss = frag(
        cssAt("javascripts/vendor/jstree/themes/default/style.min.css"),
        cssTag("homework")
      ),
      moreJs = frag(
        flatpickrTag,
        jsAt(s"compiled/lichess.homework${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.homework=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
            "isTeamManager" -> isTeamManager,
            "team" -> teamJson,
            "clazz" -> clazzJson,
            "course" -> courseJson,
            "nextCourse" -> nextCourseJson,
            "homework" -> homeworkJson,
            "prevHomework" -> prevHomeworkJson,
            "cloneHomework" -> cloneHomeworkJson,
            "limit" -> Json.obj(
              "common" -> 6,
              "practice" -> 20
            ),
            "sourceRel" -> Json.obj(
              "id" -> s"${clazz.id}@${course.id}@${course.index}",
              "name" -> s"${clazz.name} 第${course.index}次课 课后练",
              "source" -> "homework"
            ),
            "themePuzzle" -> views.html.task.bits.themePuzzleJson(),
            "classicGames" -> lila.distinguish.Distinguish.Classic.toJson
          ))
        }""")
      )
    ) {
        main(cls := "box box-pad page-small homework-form")
      }

}
