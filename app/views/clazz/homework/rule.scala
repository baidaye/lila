package views.html.clazz.homework

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._

object rule {

  def apply()(implicit ctx: Context) = views.html.base.layout(
    title = "统计规则",
    moreCss = cssTag("homework")
  )(
      main(cls := "page-small box box-pad homework-rule")(
        h1("课后练进度统计规则"),
        div(cls := "mg1")(
          div("1. 只有在", span(cls := "bold")("截止时间"), "之前完成，才会统计进度；过了截止时间，题目状态及所有进度状态都不再更新；"),
          div("2. 课后练会自动统计完成进度，未完成的会有", span(cls := "yellow")("橙色字体"), "提示，", span(cls := "red")("红色字体"), "表示题目错误，也不算完成，已完成的有", span(cls := "green")("绿色字体"), "提示；"),
          div("3. 进度统计规则："),
          div(cls := "indent1")(
            div("1) 完成率 = ∑任务权重 * 任务完成度；"),
            div("2) 任务权重：训练目标 或 练习-任务，权重平均分配，如：课后练中设置2个目标，3个练习任务，则每一项权重为 20%；"),
            div("3) 任务完成度：单项 目标或任务的完成比例，如：在训练目标中选择战术训练，数量5，实际完成4个，则 此项的完成度为 4/5 = 0.8。")
          )
        ),
        div(cls := "mg1")(
          div(cls := "bold mg0")("【注意事项】"),
          div(cls := "indent1")(
            div("1.	训练目标部分的战术训练（不指定题目），与练习下 战术题（指定题目），为两个不同功能，分开统计；"),
            div("2.	请在", span(cls := "bold")("截至时间"), "之前完成，过期后不再统计进度；"),
            div("3.	战术训练、主题战术只有", span(cls := "bold")("作对"), "了才会计数，", span(cls := "bold")("重复题目只记1次"), "；"),
            div("4.	主题战术、坐标训练等功能，", span(cls := "bold")("教练设置好的条件，不要修改，"), "条件不满足教练要求，不会计数。"),
            div("5.	如果发现统计不准确，建议在电脑上使用chrome浏览器，如果问题依然存在，可联系客服，反馈问题（信息包括：账号，课后练地址或对应的班级、日期、对应的错误截图或视频）。系统内，可发消息给", a(href := "/inbox/Haichess客服")("Haichess客服"), "。")
          )
        ),
        div(cls := "mg1")(
          div(cls := "bold mg0")("【示例】"),
          div(cls := "indent1")("一次课后练的示例，如下图。"),
          div(cls := "imgs indent1 mg1")(
            div(cls := "warp")(
              img(cls := "img")(src := staticUrl("images/homework/规则图1.png")),
              div(cls := "center bold")("课后练图1")
            ),
            div(cls := "warp")(
              img(cls := "img")(src := staticUrl("images/homework/规则图2.png")),
              div(cls := "center bold")("课后练图2")
            )
          ),
          div(cls := "indent1 mg0")(
            div(span(cls := "bold")("完成率计算过程：")),
            div(cls := "indent2")(
              div("1、本次课后练，共2个训练目标，3个练习任务，共5项，每项权重20%；"),
              div(
                table(cls := "table-indent2")(
                  tr(
                    td(cls := "nopad")("2、完成率"),
                    td(" = "),
                    td("1/5 * 20%"),
                    td("#战术训练")
                  ),
                  tr(
                    td,
                    td(" + "),
                    td("5/5 * 20%"),
                    td("#主题战术")
                  ),
                  tr(
                    td,
                    td(" + "),
                    td("4/6 * 20%"),
                    td("#战术题")
                  ),
                  tr(
                    td,
                    td(" + "),
                    td("0/1 * 20%"),
                    td("#棋谱记录")
                  ),
                  tr(
                    td,
                    td(" + "),
                    td("1/1 * 20%"),
                    td("#指定起始位置对局")
                  ),
                  tr(
                    td,
                    td(" = "),
                    td("57.3%"),
                    td
                  )
                )
              )
            )
          )
        )
      )
    )
}

