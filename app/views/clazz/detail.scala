package views.html.clazz

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User
import lila.clazz.{ Clazz, Course, CourseRelation, InviteWithUser, StudentWithUser }
import lila.clazz.Clazz.ClazzWithCoach
import lila.contest.Contest
import lila.team.CampusWithTeam
import controllers.rt_klazz.routes

object detail {

  private val dataTab = attr("data-tab")
  private val dataCourse = attr("data-course")

  def apply(
    clazzWithCoach: ClazzWithCoach,
    team: Option[CampusWithTeam],
    courseRelations: List[CourseRelation],
    allContests: List[Contest],
    studentWithUsers: List[StudentWithUser],
    inviteWithUsers: List[InviteWithUser],
    attends: Map[User.ID, (Option[Boolean], Int)],
    recentlyCourse: Option[Course],
    isTeamManager: Boolean = false,
    courseId: Option[String] = None,
    error: Option[String] = None
  )(implicit ctx: Context) = {
    views.html.base.layout(
      title = "班级详情",
      moreJs = frag(
        flatpickrTag,
        transferTag,
        jsTag("team.member.async.js"),
        jsTag("clazz.detail.js")
      ),
      moreCss = cssTag("clazz")
    ) {
        main(cls := "box box-pad page-small detail", dataCourse := courseId)(
          bits.clazzInfo(clazzWithCoach, team, isTeamManager = isTeamManager),
          div(cls := "tabs relations")(
            div(cls := "header")(
              div(dataTab := "students", cls := "active")("学员"),
              div(dataTab := "courses")("课节"),
              (clazzWithCoach.isCoach(ctx.me) || isTeamManager) && recentlyCourse.isDefined option div(dataTab := "attend")("出勤情况")
            ),
            div(cls := "panels")(
              div(cls := "students active")(studentTab(clazzWithCoach, studentWithUsers, inviteWithUsers, isTeamManager, error)),
              div(cls := "courses")(courseTab(clazzWithCoach, courseRelations, allContests, recentlyCourse, isTeamManager, courseId)),
              (clazzWithCoach.isCoach(ctx.me) || isTeamManager) option {
                recentlyCourse map { rc =>
                  attendTab(clazzWithCoach, studentWithUsers, courseRelations, rc, attends, isTeamManager)
                }
              }
            )
          )
        )
      }
  }

  def studentTab(clazzWithCoach: ClazzWithCoach, studentWithUsers: List[StudentWithUser], inviteWithUsers: List[InviteWithUser], isTeamManager: Boolean, error: Option[String])(implicit ctx: Context) = {
    val clazz = clazzWithCoach.clazz
    frag(
      (isTeamManager || clazzWithCoach.isCoach(ctx.me)) && !clazz.stopped option div(cls := "student-top")(
        postForm(cls := "invite", action := routes.Student.invite(clazz.id))(
          div(cls := "user-invite")(
            label(`for` := "username")("邀请学员："),
            input(cls := "user-autocomplete", id := "username", name := "username", placeholder := "用户账号", autofocus, required, dataTag := "span"),
            submitButton(cls := "button", dataIcon := "E")
          ),
          error.map {
            badTag(_)
          }
        ),
        a(cls := "button modal-alert", href := routes.Student.addStuModal(clazz.id))("添加学员")
      ),
      div(cls := "tables")(
        (isTeamManager || clazz.isCoach(ctx.me)) && inviteWithUsers.nonEmpty option frag(
          table(cls := "slist invited")(
            thead(
              tr(
                th("账号"),
                th("备注（姓名）"),
                th("级别"),
                th("状态"),
                th("操作")
              )
            ),
            tbody(
              inviteWithUsers.map { iwu =>
                tr(dataId := iwu.userId)(
                  td(userLink(iwu.user)),
                  td(iwu.realName),
                  td(iwu.levelLabel),
                  td(iwu.invite.statusLabel),
                  td(
                    a(cls := "button button-empty small", href := controllers.routes.Message.convo(iwu.user.username))("发消息"),
                    !clazz.stopped && iwu.invite.isInvited && !iwu.invite.expired option postForm(style := "display:initial", action := routes.Student.cancelInvite(iwu.id))(
                      button(cls := "button button-empty button-red small confirm remove", title := "确认取消？")("取消邀请")
                    ),
                    !clazz.stopped && iwu.invite.isInvited && iwu.invite.expired option postForm(style := "display:initial", action := routes.Student.invite(clazz.id))(
                      form3.hidden("username", iwu.user.username),
                      button(cls := "button button-empty button-green small")("重新邀请")
                    ),
                    !clazz.stopped && iwu.invite.isInvited && iwu.invite.expired option postForm(style := "display:initial", action := routes.Student.cancelInvite(iwu.id))(
                      button(cls := "button button-empty button-red small confirm remove", title := "确认移除？")("移除")
                    )
                  )
                )
              }
            )
          ),
          br
        ),
        table(cls := "slist joined")(
          thead(
            tr(
              th("账号"),
              th("备注（姓名）"),
              th("级别"),
              (isTeamManager || clazz.isCoach(ctx.me)) option th("操作")
            )
          ),
          tbody(
            if (studentWithUsers.isEmpty) {
              tr(cls := "empty")(
                td(colspan := (if (clazz.isCoach(ctx.me)) 5 else 3))("还没有学员")
              )
            } else {
              studentWithUsers.map { swu =>
                tr(dataId := swu.userId)(
                  td(userLink(swu.user)),
                  td(swu.realName),
                  td(swu.levelLabel),
                  (isTeamManager || clazz.isCoach(ctx.me)) option td(
                    a(cls := "button button-empty small", href := controllers.routes.Message.convo(swu.user.username))("发消息"),
                    !clazz.stopped option postForm(style := "display:initial", action := routes.Student.remove(clazz.id, swu.userId))(
                      button(cls := "button button-empty button-red small confirm remove", title := "确认移除")("移除")
                    )
                  )
                )
              }
            }
          )
        )
      )
    )
  }

  def courseTab(clazzWithCoach: ClazzWithCoach, courseRelations: List[CourseRelation], allContests: List[Contest], recentlyCourse: Option[Course], isTeamManager: Boolean, courseId: Option[String])(implicit ctx: Context) = {
    val clazz = clazzWithCoach.clazz
    val isCoach = clazz.isCoach(ctx.me)
    val isStudent = clazz.isStudent(ctx.me)
    val lastThreeCourse = recentlyCourse.map { c =>
      val index = if (c.index < 3) 1 else c.index - 2
      courseRelations.find(_.course.index == index).map(_.course).err(s"can not find course index $index")
    }
    frag(
      div(cls := "action")(
        div(cls := "right")(
          input(tpe := "checkbox", cls := "showHis", id := "showHis"),
          label(`for` := "showHis")("隐藏历史课节")
        ),
        (clazz.isCoach(ctx.me) || isTeamManager) && clazz.isWeek && !clazz.stopped option a(cls := "button modal-alert btn-course-append", href := routes.Course.appendCourse(clazz.id))("追加课节")
      ),
      table(cls := "slist")(
        thead(
          tr(
            th,
            th(cls := "index")("课节"),
            th(cls := "time")("上课时间"),
            th
          )
        ),
        tbody(
          courseRelations.map { courseRelation =>
            val course = courseRelation.course
            val olClassOption = courseRelation.olClass
            val homeworkOption = courseRelation.homework
            val shwtOption = courseRelation.shwt
            val contests = allContests.filter(c => c.meta.??(_.courseId ?? (_ == course.id))).sortBy(_.createdAt).reverse
            val absentOption = courseRelation.attend.map(_.absent)
            frag(
              tr(id := course.id, cls := List("course" -> true, "history" -> (lastThreeCourse.??(_.dateTime.isAfter(course.dateTime)))))(
                td(cls := List("expand" -> true, "expanded" -> courseId.contains(course.id)), dataIcon := "右"),
                td(s"第${course.index}节"),
                td(course.courseFormatTime, course.stopped option (span(cls := "stopped")("（停课）"))),
                td(
                  span(cls := "wa", dataIcon := "课")("课件（", olClassOption.fold(0) { c => if (c.opened) 1 else 0 }, "）"),
                  span(cls := "sa", dataIcon := "奖")("随堂赛（", contests.length, "）"),
                  span(cls := "ke", dataIcon := "写")("课后练（", homeworkOption.fold(0) { h => if (h.isPublished) 1 else 0 }, "）"),
                  (clazz.isShowAttend && isStudent) option absentOption.map { absent =>
                    span(dataIcon := (if (absent) "L" else "E"), cls := List("qn" -> true, "yes" -> !absent, "no" -> absent))(
                      if (absent) "缺勤" else "出勤"
                    )
                  }
                )
              ),
              tr(rel := course.id, cls := List("rel" -> true, "none" -> !courseId.contains(course.id)))(
                td,
                td(colspan := 3)(
                  div(cls := "entries")(
                    div(cls := "entry")(
                      i(dataIcon := "课", title := "课件"),
                      div(cls := "content")(
                        isCoach || isTeamManager option div(cls := "coach")(
                          olClassOption.map { olClass =>
                            frag(
                              a(target := "_blank", cls := "button button-empty small", href := controllers.rt_olclass.routes.OlClass.show(course.id))("进入课件"),
                              isCoach option div(cls := "switch-warp")(
                                span(cls := "plain")("关闭/开放课件"),
                                div(cls := "switch")(
                                  st.input(
                                    tpe := "checkbox",
                                    st.id := s"olClass-open-${course.id}",
                                    cls := "cmn-toggle olClass-open",
                                    dataId := course.id,
                                    olClass.opened option checked
                                  ),
                                  label(`for` := s"olClass-open-${course.id}", title := (if (olClass.opened) "关闭课件" else "开放课件"))
                                )
                              )
                            )
                          } getOrElse frag(
                            olClassOption.isEmpty option span(cls := "plain")("- 无课件 -"),
                            isCoach && ctx.me.??(_.isCoach) option st.form(method := "post", action := controllers.rt_olclass.routes.OlClass.create(course.id))(
                              button(cls := "button button-empty button-green small")("新建课件")
                            )
                          )
                        ),
                        isStudent option div(cls := "student")(
                          olClassOption.map { olClass =>
                            if (olClass.opened) {
                              a(target := "_blank", cls := "button button-empty small", href := controllers.rt_olclass.routes.OlClass.show(course.id))("进入课件")
                            } else span(cls := "plain")("- 无课件 -")
                          } getOrElse span(cls := "plain")("- 无课件 -")
                        )
                      )
                    ),
                    div(cls := "entry")(
                      i(dataIcon := "奖", title := "随堂赛"),
                      div(cls := "content")(
                        div(cls := "coach")(
                          contests.isEmpty option span(cls := "plain")("- 无随堂赛 -"),
                          isCoach && ctx.me.??(_.isCoach) option a(cls := "button button-empty button-green small", href := routes.Clazz.contestForm(course.clazz, course.id))("新建随堂赛")
                        ),
                        div(cls := "student")(
                          ul(cls := "contests")(
                            contests.map { contest =>
                              li(
                                span(cls := "contestName")(contest.fullName),
                                a(target := "_blank", cls := "button button-empty small", href := controllers.rt_contest.routes.Contest.show(contest.id))("进入比赛")
                              )
                            }
                          )
                        )
                      )
                    ),
                    div(cls := "entry")(
                      i(dataIcon := "写", title := "课后练"),
                      div(cls := "content")(
                        isCoach || isTeamManager option div(cls := "coach")(
                          homeworkOption.map { homework =>
                            frag(
                              if (homework.hasContent) frag(
                                span(cls := "plain")("截止时间：", homework.deadline),
                                a(target := "_blank", cls := "button button-empty small", href := routes.Homework.createForm(course.clazz, course.id))("查看课后练", if (homework.isPublished) "（已发布）" else "（未发布）")
                              )
                              else frag(
                                span(cls := "plain")("- 无课后练 -"),
                                isCoach && !clazz.stopped && !homework.hasContent option a(cls := "button button-empty button-green small", href := routes.Homework.createForm(course.clazz, course.id))("新建课后练")
                              ),
                              homework.isPublished option a(target := "_blank", cls := "button button-empty small", href := routes.Homework.refreshReport(homework.id))("课后练报告")
                            )
                          } getOrElse frag(
                            span(cls := "plain")("- 无课后练 -"),
                            isCoach && !clazz.stopped option a(cls := "button button-empty button-green small", href := routes.Homework.createForm(course.clazz, course.id))("新建课后练")
                          )
                        ),
                        isStudent option div(cls := "student")(
                          shwtOption.map { shwt =>
                            val homework = shwt.homework
                            frag(
                              span(cls := "plain")("截止时间：", homework.deadline),
                              span(cls := "plain")("完成率：", shwt.progressFormat),
                              course.dateEndTime.isAfterNow option span(cls := "plain warn")("(等待课程结束后可以完成)"),
                              !course.dateEndTime.isAfterNow option a(target := "_blank", cls := "button button-empty small", href := routes.Homework.show(homework.id))("完成课后练")
                            )
                          } getOrElse span(cls := "plain")("- 无课后练 -")
                        )
                      )
                    )
                  )
                )
              )
            )
          }
        )
      )
    )
  }

  def attendTab(clazzWithCoach: ClazzWithCoach, studentWithUsers: List[StudentWithUser], courseRelations: List[CourseRelation], rc: Course, attends: Map[User.ID, (Option[Boolean], Int)], isTeamManager: Boolean)(implicit ctx: Context) = {
    val clazz = clazzWithCoach.clazz
    div(cls := "attend")(
      div(cls := "action")(
        div(
          label("日期："),
          st.select(st.id := "attendDate", dataHref := s"/clazz/${clazz.id}/attends")(
            courseRelations.map(x => x.course.id -> s"${x.course.dateTime.toString("yyyy年MM月dd日 HH:mm")}（${x.course.weekFormat}）").map {
              case (value, name) => option(st.value := value.toString, (rc.id == value) option selected)(name)
            }
          )
        ),
        !clazz.stopped && clazz.isCoach(ctx.me) option a(cls := "button modal-alert modal-attend", href := routes.Clazz.attendSetModal(clazz.id, rc.id))("批量设置缺勤")
      ),
      table(cls := "slist")(
        thead(
          tr(
            th("账号"),
            th("备注（姓名）"),
            th(cls := "dateTime")(rc.dateTime.toString("yyyy年MM月dd日 HH:mm"), s"（${rc.weekFormat}）"),
            th("累计缺勤"),
            th("操作")
          )
        ),
        tbody(
          studentWithUsers.map { swu =>
            val attend = attends.find(_._1 == swu.userId)
            tr(cls := swu.userId)(
              td(userLink(swu.user)),
              td(swu.realName),
              td(cls := "absent")(attend.map(_._2._1.map(absent => if (absent) "缺勤" else "出勤") | "-") | "-"),
              td(cls := "nb")(attend.map(_._2._2.toString + "天") | "-"),
              td(
                a(cls := "button button-empty small modal-alert modal-attend-suudent", href := routes.Clazz.attendStudentModal(clazz.id, swu.userId))("详情")
              )
            )
          }
        )
      )
    )
  }

  def attendSetModal(clazz: Clazz, course: Course, students: List[StudentWithUser], absents: List[String])(implicit ctx: Context) = frag(
    div(cls := "modal-content attendSet none")(
      h2("批量缺勤设置"),
      postForm(cls := "form3", action := routes.Clazz.attendSet(clazz.id, course.id))(
        h3(course.courseFormatTime),
        br,
        form3.hidden("absents", absents.mkString(",")),
        views.html.base.transfer[StudentWithUser, StudentWithUser](
          leftOptions = students.filterNot(s => absents.contains(s.userId)),
          rightOptions = students.filter(s => absents.contains(s.userId)),
          leftLabel = "备选学员",
          rightLabel = "缺勤学员",
          search = true
        ) { left =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${left.userId}", value := left.userId, dataAttr := left.json.toString())),
              td(cls := "name")(left.realName)
            )
          } { right =>
            tr(
              td(input(tpe := "checkbox", id := s"chk_${right.userId}", value := right.userId, dataAttr := right.json.toString())),
              td(cls := "name")(right.realName)
            )
          },
        br,
        p("注：除选中为缺勤的学员外，其他学员将统一设置为正常出勤。"),
        form3.actions(
          a(cls := "cancel small")("取消"),
          a(cls := "button small attendSubmit")("保存")
        )
      )
    )
  )

}

