package views.html.clazz

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.train.TrainCourse
import lila.clazz.{ Clazz, Course }
import org.joda.time.{ DateTime, Period, PeriodType }
import controllers.rt_klazz.routes

object course {

  val dataTrainCourseHref = attr("data-trainCourseHref")
  def apply(
    firstDay: DateTime,
    lastDay: DateTime,
    courseMap: Map[(String, String), List[Course.WithClazz]],
    trainCourseMap: Map[(String, String), List[TrainCourse]],
    week: Int
  )(implicit ctx: Context) = {
    val dataClazz = attr("data-clazz")
    val timeRange = List("上午", "下午", "晚上")
    val dateRange: Seq[String] = (0 to new Period(firstDay.getMillis, lastDay.getMillis, PeriodType.days()).getDays) map { i =>
      firstDay.plusDays(i).toString("M月d日")
    }

    views.html.base.layout(
      title = "课程表",
      moreJs = frag(
        flatpickrTag,
        transferTag,
        jsTag("clazz.course.js")
      ),
      moreCss = frag(
        cssTag("course"),
        cssTag("trainCourse")
      )
    ) {
        main(cls := "box page-small timetable")(
          div(cls := "box__top")(
            div(cls := "btn-group")(
              a(cls := "button prev", href := routes.Course.timetable(week - 1))("上一周"),
              a(cls := "button next", href := routes.Course.timetable(week + 1))("下一周"),
              a(cls := "button today", href := routes.Course.timetable(0))("今天")
            ),
            div(cls := "btn-group action")(
              button(cls := "button button-empty trainCourse", href := controllers.rt_trainCourse.routes.TrainCourse.createModal(), dataTrainCourseHref := controllers.rt_trainCourse.routes.TrainCourse.createModal())("新建训练课"),
              button(cls := "button button-empty clazzAction action-update disabled", disabled, href := routes.Course.updateModal("#id#", week), dataTrainCourseHref := controllers.rt_trainCourse.routes.TrainCourse.updateModal("idval"))("修改课时"),
              button(cls := "button button-empty clazzAction action-stop disabled", disabled, href := routes.Course.stopModal("#id#", week), dataTrainCourseHref := controllers.rt_trainCourse.routes.TrainCourse.removeModal("idval"))("停课一次"),
              button(cls := "button button-empty clazzAction action-postpone disabled", disabled, href := routes.Course.postponeModal("#id#", week))("顺延")
            )
          ),
          table(cls := "course-list")(
            thead(
              tr(
                th(rowspan := 2, style := "width: 3em;"),
                th("周一"),
                th("周二"),
                th("周三"),
                th("周四"),
                th("周五"),
                th("周六"),
                th("周日")
              ),
              tr(
                dateRange map { d =>
                  th(d)
                }
              )
            ),
            tbody(
              timeRange map { t =>
                tr(
                  td(t),
                  dateRange.map { d =>
                    val courseListOption = courseMap.get(d, t)
                    val trainCourseListOption = trainCourseMap.get(d, t)
                    td(
                      courseListOption.map { courseList =>
                        courseList.map { courseWithClazz =>
                          courseInfo(courseWithClazz.clazz, courseWithClazz.course)
                        }
                      },
                      trainCourseListOption.map { trainCourseList =>
                        trainCourseList.map { trainCourse =>
                          trainCourseInfo(trainCourse)
                        }
                      }
                    )
                  }
                )
              }
            )
          )
        )
      }
  }

  val dataClazz = attr("data-clazz")
  val dataIndex = attr("data-index")
  val dataHomework = attr("data-homework")
  private def courseInfo(clazz: Clazz, course: Course) =
    div(
      cls := List("course" -> true, "stopped" -> (course.stopped || clazz.stopped), "editable" -> (course.editable && !course.stopped && !clazz.stopped)),
      dataId := course.id,
      dataIndex := course.index,
      dataHomework := course.homework,
      dataClazz := clazz.id,
      dataType := clazz.clazzType.id,
      style := "color:" + clazz.color
    )(
        div(cls := "nowrap")(course.timeBegin, "-", course.timeEnd),
        div(cls := "nowrap ellipsis", title := clazz.name)(clazz.name),
        div(cls := "nowrap")("第" + course.index + "/" + clazz.times + "节")
      )

  private def trainCourseInfo(trainCourse: TrainCourse) =
    div(
      cls := List("course" -> true, "editable" -> !trainCourse.expired),
      dataId := trainCourse.id,
      dataType := "trainCourse"
    )(
        div(cls := "nowrap")(trainCourse.timeBegin, "-", trainCourse.timeEnd),
        div(cls := "nowrap")("训练课"),
        div(cls := "nowrap ellipsis")(trainCourse.name)
      )

}
