package views.html.clazz

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.clazz.{ Clazz, Invite }
import lila.clazz.Clazz.ClazzWithCoach
import lila.team.CampusWithTeam
import controllers.rt_klazz.routes

object accept {

  def apply(clazzWithCoach: ClazzWithCoach, invite: Option[Invite], team: Option[CampusWithTeam], error: Option[String])(implicit ctx: Context) = {
    val clazz = clazzWithCoach.clazz
    views.html.base.layout(
      title = "接受邀请",
      moreCss = cssTag("clazz")
    ) {
        main(cls := "box box-pad page-small detail")(
          bits.clazzInfo(clazzWithCoach, team, invite = invite),
          div(cls := "accept-box")(
            invite.map { it =>
              frag(
                it.status match {
                  case Invite.Status.Invited => if (it.expired) message("邀请已过期") else forms(clazz)
                  case Invite.Status.Joined => message("学员已经加入")
                  case Invite.Status.Refused => message("邀请已经拒绝")
                },
                error.map {
                  badTag(_)
                }
              )
            } getOrElse {
              message("邀请不存在或已被取消")
            }
          )
        )
      }
  }

  def forms(clazz: Clazz) = div(
    p(strong(
      "加入班级之后，您将成为教练的学员，有权访问教练设置了学员权限的资源；同时，教练可以看到您的个人信息、动态、数据洞察和课后练等信息。"
    )),
    div(cls := "actions")(
      postForm(action := routes.Student.accept(clazz.id))(
        submitButton(cls := "text button", dataIcon := "E")("接受")
      ),
      postForm(action := routes.Student.refused(clazz.id))(
        submitButton(cls := "text button button-red", dataIcon := "L")("拒绝")
      )
    )
  )

  def message(message: String) = h2(message)

}

