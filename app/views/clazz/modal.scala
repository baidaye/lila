package views.html.clazz

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import play.api.data.Form
import lila.user.User
import lila.clazz.Clazz.ClazzWithCoach
import lila.clazz.{ Clazz, Course, CourseAttend }
import lila.team.{ Campus, CampusWithTeam, Member, MemberWithUser, Team }
import controllers.rt_klazz.routes

object modal {

  object clazz {

    def setting(clazz: Clazz, form: Form[_])(implicit ctx: Context) = frag(
      div(cls := "modal-content clazz-setting none")(
        h2("班级设置"),
        postForm(cls := "form3", action := routes.Clazz.setting(clazz.id))(
          form3.group(form("showAttend"), raw("学员显示出勤情况"))(form3.radio(_, List(true -> "是", false -> "否"))),
          form3.actions(
            a(cls := "cancel")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )

    def attendStudentModal(
      clazz: Clazz,
      courses: List[Course],
      attends: List[CourseAttend],
      user: User,
      mark: Option[String],
      isTeamManager: Boolean
    )(implicit ctx: Context) = {
      frag(
        div(cls := "modal-content attendStudentSet none")(
          h2("出勤情况详情"),
          postForm(cls := "form3", action := routes.Clazz.attendStudentSet(clazz.id, user.id))(
            div(cls := "names")(
              strong(clazz.name),
              strong(mark | user.realNameOrUsername)
            ),
            div(cls := "courses")(
              courses.map { course =>
                val absent = attends.find(_.course == course.id).map { a => if (a.absent) 0 else 1 } | -1
                div(dataId := course.id, cls := List("course" -> true, "absent" -> attends.exists(attend => attend.course == course.id && attend.absent), "unabsent" -> attends.exists(attend => attend.course == course.id && !attend.absent)))(
                  form3.hidden("absent", absent.toString),
                  div(course.dateTimeStr),
                  div(course.weekFormat)
                )
              }
            ),
            br,
            p("注：单击日期修改出勤状态，绿色为正常出勤，橙色为未出勤，白色为未设置。"),
            !isTeamManager option form3.actions(
              a(cls := "cancel small")("取消"),
              a(cls := "button small attendStudentSubmit")("保存")
            )
          )
        )
      )
    }

    def editCoachModal(
      form: Form[_],
      cwc: ClazzWithCoach,
      coaches: List[MemberWithUser],
      markMap: Map[String, Option[String]]
    )(implicit ctx: Context) = {
      frag(
        div(cls := "modal-content editCoachModal none")(
          h2("更换教练"),
          postForm(cls := "form3", action := routes.Clazz.editCoach(cwc.id))(
            form3.group(form("name"), "班级名称")(_ => cwc.clazz.name),
            form3.group(form("oldCoach"), "原教练")(_ => cwc.mark | cwc.coach.realNameOrUsername),
            form3.group(form("newCoach"), raw("新教练")) { f =>
              form3.select(f, coaches.map(mu => mu.user.id -> bits.userMark(mu, markMap)))
            },
            br,
            p("提示： 修改教练后，原教练将不能再进入班级，请谨慎操作。"),
            form3.actions(
              a(cls := "cancel small")("取消"),
              a(cls := "button small editCoachSave")("保存")
            )
          )
        )
      )
    }

    def addStuModal(
      form: Form[_],
      clazz: Clazz,
      teamOption: Option[Team],
      campusesOption: Option[List[Campus]],
      tagsOption: Option[List[lila.team.Tag]]
    )(implicit ctx: Context) =
      div(cls := "modal-content member-modal none")(
        h2("添加学员"),
        div(cls := "member-panel")(
          st.form(cls := "member-search small", action := teamOption.map { team => controllers.rt_team.routes.TeamMember.loadClazzMember(team.id, clazz.id) } getOrElse { controllers.routes.Coach.loadStudent(clazz.id) })(
            teamOption.map { team =>
              table(
                tr(
                  td(cls := "label")(label("账号/备注")),
                  td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
                  td(cls := "label")(label("校区")),
                  td(cls := "fixed")(
                    campusesOption.map { campuses =>
                      form3.select(form("campus"), campuses.map(c => c.id -> c.name))
                    }
                  )
                ),
                tr(
                  td(cls := "label")(label("角色")),
                  td(cls := "fixed")(form3.select(form("role"), List(Member.Role.Trainee).map(r => r.id -> r.name))),
                  td(cls := "label")(label("年龄")),
                  td(cls := "fixed")(form3.input(form("age"), typ = "number"))
                ),
                tr(
                  td(cls := "label")(label("棋协级别")),
                  td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some)),
                  td(cls := "label")(label("性别")),
                  td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
                ),
                tagsOption.map { tags =>
                  frag(
                    tr(
                      td(colspan := 4)(
                        a(cls := "show-search")("显示高级搜索", iconTag("R"))
                      )
                    ),
                    tags.filterNot(_.typ.range).zipWithIndex.map {
                      case (t, i) => views.html.team.member.buildSearchField(t, form(s"fields[$i]"), false)
                    },
                    tags.filter(_.typ.range).zipWithIndex.map {
                      case (t, i) => views.html.team.member.buildRangeSearchField(t, form(s"rangeFields[$i]"), false)
                    }
                  )
                }
              )
            } getOrElse {
              table(
                tr(
                  td(cls := "label")(label("账号/备注")),
                  td(cls := "fixed")(form3.input(form("username"))(placeholder := "账号/备注（姓名）")),
                  td(cls := "label")(label("棋协级别")),
                  td(cls := "fixed")(form3.select(form("level"), lila.user.FormSelect.Level.levelWithRating, "".some))
                ),
                tr(
                  td(cls := "label")(label("年龄")),
                  td(cls := "fixed")(form3.input(form("age"), typ = "number")),
                  td(cls := "label")(label("性别")),
                  td(cls := "fixed")(form3.select(form("sex"), lila.user.FormSelect.Sex.list, "".some))
                )
              )
            },
            table(
              tr(
                td(colspan := 4)(
                  div(cls := "action")(
                    div, submitButton(cls := "button small")("查询")
                  )
                )
              )
            )
          ),
          postForm(cls := "form3", action := routes.Student.addStu(clazz.id))(
            div(cls := "member-list")(
              table(cls := "slist member-table")(
                thead(
                  th(input(tpe := "checkbox", id := "member_chk_all"), label(`for` := "member_chk_all")),
                  th("账号"),
                  th("备注（姓名）"),
                  th("级别")
                ),
                tbody()
              )
            ),
            form3.actions(
              a(cls := "cancel small")("取消"),
              form3.submit("保存", klass = "small")
            )
          )
        )
      )
  }

  object course {

    def update(course: Course, form: Form[_], week: Int)(implicit ctx: Context) = frag(
      div(cls := "modal-content course-update none")(
        h2("修改课时"),
        postForm(cls := "form3", action := routes.Course.update(course._id, week))(
          form3.split(
            div(cls := "form-third info")(
              course.date.toString("M月d日"),
              "（" + course.weekFormat + "）"
            ),
            div(cls := "form-third info")(course.timeBegin),
            div(cls := "form-third info")(course.timeEnd)
          ),
          form3.split(
            form3.group(form("date"), raw("日期"), klass = "form-third")(form3.input(_, klass = "flatpickr")(dataMinDate := "today")),
            form3.group(form("timeBegin"), raw("开始时间"), klass = "form-third")(form3.timeFlatpickr(_)),
            form3.group(form("timeEnd"), raw("结束时间"), klass = "form-third")(form3.timeFlatpickr(_))
          ),
          form3.actions(
            a(cls := "cancel")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )

    def stop(id: String, week: Int)(implicit ctx: Context) = frag(
      div(cls := "modal-content none")(
        h2("停课一次"),
        postForm(cls := "form3", action := routes.Course.stop(id, week))(
          h3("说明：本次课取消，课节直接跳过。"),
          form3.actions(
            a(cls := "cancel")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )

    def postpone(id: String, week: Int)(implicit ctx: Context) = frag(
      div(cls := "modal-content none")(
        h2("顺延"),
        postForm(cls := "form3", action := routes.Course.postpone(id, week))(
          h3("说明：本次及后续课节统一做顺延，保证与实际发生一致。"),
          form3.actions(
            a(cls := "cancel")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    )

    def appendCourse(
      clazzWithCoach: ClazzWithCoach,
      team: Option[CampusWithTeam],
      startDate: String,
      form: Form[_]
    )(implicit ctx: Context) = {
      val clazz = clazzWithCoach.clazz
      val minDate = clazz.endDate.plusDays(1).toString("yyyy-MM-dd")
      div(cls := "modal-content course-append none")(
        h2("追加课节"),
        postForm(cls := "form3", action := routes.Course.appendCourse(clazz.id))(
          div(cls := "detail")(
            bits.clazzInfo(clazzWithCoach, team)
          ),
          br,
          form3.group(form("date"), raw("开始日期"), help = frag("开始日期必须大于当前最后的课时的时间").some)(form3.input2(_, startDate.some, klass = "flatpickr")(dataMinDate := startDate)),
          form3.group(form("times"), raw("课节数"), help = frag(s"所有课节数不能大于${Clazz.MAX_COURSE}").some)(form3.input(_, "number")),
          p("说明：追加的课节从设置的“开始时间”开始，按照班级原有设置的规则向后添加。"),
          br,
          form3.actions(
            a(cls := "cancel")("取消"),
            form3.submit("保存", klass = "small")
          )
        )
      )
    }

  }

}
