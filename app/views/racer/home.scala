package views.html.racer

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.racer.RacerRace
import controllers.routes

object home {

  def apply(list: List[RacerRace.WithRound])(implicit ctx: Context) =
    views.html.base.layout(
      moreCss = cssTag("racer-home"),
      moreJs = embedJsUnsafe("""$(function() {
$('a.joinLobby').on('click', function(e) {
  let $btn = $(this);
  if($btn.hasClass('disabled')) {
    e.preventDefault();
    return false;
  }
  window.onbeforeunload = function(event) {
    $btn.addClass('disabled');
  }
});});"""),
      title = "战术竞速赛"
    ) {
        main(cls := "page page-small racer-home box box-pad")(
          div(cls := "box__top")(
            h1("战术竞速赛"),
            div(cls := "box__top__actions")(
              ctx.me.?? { u => u.isCoach || u.hasTeam } option a(cls := "button button-empty", href := routes.Racer.history(1))("历史房间"),
              ctx.me.?? { u => u.isCoach || u.hasTeam } option a(cls := "button button-green", href := routes.Racer.createForm, dataIcon := "O")("新建竞速赛")
            )
          ),
          div(cls := "racer-home__races")(
            a(cls := "button button-green button-fat joinLobby", href := routes.Racer.lobby)(i(cls := "car")(0), "加入公开赛"),
            list.map { rwr =>
              a(cls := "button button-fat", href := routes.Racer.show(rwr.race.id))(
                div(cls := "info")(
                  div(cls := "organizer")(
                    rwr.race.typ match {
                      case RacerRace.Type.TeamInner => frag(label("俱乐部房间："), rwr.race.name)
                      case RacerRace.Type.ClazzInner => frag(label("班级房间："), rwr.race.name)
                      case _ => rwr.race.name
                    }
                  ),
                  div(span(rwr.round.playerSize, "/", rwr.race.maxPlayers, "人 "), rwr.race.timeBetween)
                ),
                i(cls := "car")(0), "进入房间"
              )
            }
          ),
          div(cls := "racer-home__about")(
            a(href := routes.Racer.help())("关于战术竞速赛")
          )
        )
      }
}
