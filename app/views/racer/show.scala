package views.html.racer

import play.api.libs.json._
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.racer.{ RacerRace, RacerRound, RoundSettingChoiceRule }
import controllers.routes

object show {

  def apply(race: RacerRace, data: JsObject, pref: JsObject)(implicit ctx: Context) =
    views.html.base.layout(
      moreCss = frag(cssTag("racer")),
      moreJs = frag(
        jsAt(s"compiled/lichess.racer${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.racer=${
          safeJsonValue(Json.obj(
            "data" -> data,
            "pref" -> pref
          ))
        }""")
      ),
      title = "战术竞速赛",
      zoomable = true,
      playing = true,
      chessground = false
    ) {
        main(
          div(cls := "racer racer-app racer--play")(
            div(cls := "racer__board main-board"),
            div(cls := "racer__side")
          )
        )
      }

  def setting(race: RacerRace, rounds: List[RacerRound])(implicit ctx: Context) = frag(
    div(cls := "modal-content clazz-setting none")(
      h2("房间设置"),
      postForm(cls := "form3", action := routes.Racer.remove(race.id))(
        table(cls := "slist")(
          tbody(
            rounds.map { round =>
              val setting = race.settingOf(round.no)
              tr(
                td(
                  if (round.isFinished) {
                    a(target := "_blank", href := routes.Racer.round(race.id, round.no))(s"第${round.no}轮")
                  } else s"第${round.no}轮"
                ),
                td(round.playerSize, "人"),
                td(setting.rule match {
                  case RoundSettingChoiceRule.Range => setting.ranges.map { ranges =>
                    frag(ranges.num1, " / ", ranges.num2, " / ", ranges.num3)
                  }
                  case RoundSettingChoiceRule.Capsule => setting.capsule.map { capsule =>
                    a(target := "_blank", href := controllers.rt_resource.routes.Capsule.puzzleCapsule(capsule.id))(capsule.name)
                  }
                }),
                td("共", setting.size, "题")
              )
            }
          )
        ),
        form3.actions(
          a(cls := "cancel")("取消"),
          form3.submit("关闭房间", icon = "L".some, klass = s"button-red small ${if (race.canDelete) "" else "disabled"}", isDisable = !race.canDelete)
        )
      )
    )
  )

}
