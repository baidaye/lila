package views.html.racer

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.paginator.Paginator
import lila.racer.{ RacerRace, RoundSettingChoiceRule }
import controllers.routes

object history {

  def apply(pager: Paginator[RacerRace.WithRounds])(implicit ctx: Context) =
    views.html.base.layout(
      title = "历史房间",
      moreCss = cssTag("racer-home"),
      moreJs = frag(
        infiniteScrollTag,
        jsTag("racer.history.js")
      )
    ) {
        main(cls := "box page-small racer-history")(
          h1(a(href := routes.Racer.home, dataIcon := "I", cls := "text"), nbsp, "历史房间"),
          table(cls := "slist history-list")(
            thead(
              tr(
                th,
                th("房间名称"),
                th("开始时间"),
                th("持续时间"),
                th("轮次"),
                th("最大人数")
              )
            ),
            if (pager.nbResults > 0) {
              tbody(cls := "infinitescroll")(
                pagerNextTable(pager, np => routes.Racer.history(np).url),
                pager.currentPageResults.map { rwr =>
                  val race = rwr.race
                  frag(
                    tr(id := race.id, cls := "paginated")(
                      td(cls := List("expand" -> true, "expanded" -> false), dataIcon := "右"),
                      td(race.name),
                      td(momentFromNow(race.createdAt)),
                      td(race.durationText),
                      td(race.roundCount),
                      td(rwr.maxPlayer)
                    ),
                    tr(rel := race.id, cls := List("rel" -> true, "none" -> true))(
                      td,
                      td(colspan := 5, cls := "nopan")(
                        table(cls := "round-list")(
                          tbody(
                            rwr.rounds.map { round =>
                              val setting = race.settingOf(round.no)
                              tr(
                                td(
                                  a(target := "_blank", href := routes.Racer.round(race.id, round.no))(s"第${round.no}轮")
                                ),
                                td(round.playerSize, "人"),
                                td(setting.rule match {
                                  case RoundSettingChoiceRule.Range => setting.ranges.map { ranges =>
                                    frag(ranges.num1, " / ", ranges.num2, " / ", ranges.num3)
                                  }
                                  case RoundSettingChoiceRule.Capsule => setting.capsule.map { capsule =>
                                    a(target := "_blank", href := controllers.rt_resource.routes.Capsule.puzzleCapsule(capsule.id))(capsule.name)
                                  }
                                }),
                                td("共", setting.size, "题")
                              )
                            }
                          )
                        )
                      )
                    )
                  )
                }
              )
            } else {
              tbody(
                tr(
                  td(colspan := 6)("暂无记录")
                )
              )
            }
          )
        )
      }

}
