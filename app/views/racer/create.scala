package views.html.racer

import play.api.libs.json._
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.racer.{ DataForm, RacerRace }
import play.api.data.Form
import lila.clazz.Clazz
import org.joda.time.DateTime
import lila.resource.Capsule
import controllers.routes

object create {

  val dataTab = attr("data-tab")
  val dataTeamInner = attr("data-team-inner")
  val dataClazzInner = attr("data-clazz-inner")
  def apply(form: Form[_], teams: List[lila.team.Team], clazzs: List[(Clazz, Boolean)])(implicit ctx: Context) = views.html.base.layout(
    title = "新建战术竞速",
    moreCss = cssTag("racer-create"),
    moreJs = frag(
      flatpickrTag,
      delayFlatpickrStart,
      jsTag("racer.create.js")
    )
  )(main(cls := "page-small racer-create")(
      div(
        cls := "racer__form box box-pad",
        dataTeamInner := JsArray(teams.map(t => Json.obj("id" -> t.id, "name" -> t.name))).toString,
        dataClazzInner := JsArray(clazzs.map(c => Json.obj("id" -> c._1.id, "name" -> c._1.name))).toString
      )(
          h1("新建战术竞速"),
          postForm(cls := "form3", action := routes.Racer.create)(
            div(cls := "tabs-horiz")(
              span(dataTab := "basic", cls := "active")("基本信息"),
              span(dataTab := "puzzle")("题目设置")
            ),
            div(cls := "tabs-content")(
              div(cls := "basic active")(
                form3.split(
                  form3.group(form("name"), raw("房间名称"), half = true)(form3.input(_)(required)),
                  form3.group(form("maxPlayers"), raw("人数上限"), half = true)(form3.input(_, typ = "number"))
                ),
                form3.split(
                  {
                    def available = ctx.me.?? { user =>
                      List(
                        RacerRace.Type.TeamInner -> user.hasTeam,
                        RacerRace.Type.ClazzInner -> isGranted(_.Coach, user)
                      ).filter(_._2).map { t => t._1.id -> t._1.name }
                    }
                    val baseField = form("typ")
                    val field = ctx.req.queryString get "team" flatMap (_.headOption) match {
                      case None => baseField
                      case Some(_) => baseField.copy(value = "team-inner".some)
                    }
                    form3.group(field, raw("比赛类型"), half = true)(form3.select(_, available))
                  },
                  form3.group(form("organizer"), raw("主办方"), half = true)(f => frag(
                    form3.select(f, teams.map(t => t.id -> t.name)),
                    form3.hidden("organizerSelected", f.value | "")
                  ))
                ),
                form3.split(
                  form3.group(form("startedAt"), raw("开始时间"), half = true)(form3.flatpickr(_)),
                  form3.group(form("duration"), raw("开放时间"), half = true)(form3.select(_, DataForm.durationChoices))
                ),
                form3.split(
                  form3.group(form("roundTime"), raw("每轮比赛时间"), half = true)(form3.select(_, DataForm.roundTimeChoices)),
                  form3.group(form("restTime"), raw("轮次休息时间"), half = true)(form3.select(_, DataForm.restTimeChoices))
                )
              ),
              div(cls := "puzzle")(
                div(cls := "roundTime")("共n轮，每轮n分钟，休息n秒"),
                table(cls := "slist")(
                  thead(
                    tr(
                      th("轮次"),
                      th("500-1000分"),
                      th("1000-1500分"),
                      th("1500-2000分"),
                      th("操作")
                    )
                  ),
                  tbody()
                ),
                div(
                  br, br,
                  h3(strong("说明：")),
                  ul(
                    li("1、每轮题目数<=60；"),
                    li("2、题目分3个段：500-1000分，1000-1500分，1500-2000分，可以指定每段题目数；"),
                    li("3、可以选择一个战术题列表；"),
                    li("4、请根据棋手能力和轮次时长进行选择。")
                  )
                )
              )
            ),
            form3.globalError(form),
            form3.actions(
              a(href := routes.Racer.home())(trans.cancel()),
              form3.submit("发布", icon = "g".some)
            )
          )
        )
    ))

}
