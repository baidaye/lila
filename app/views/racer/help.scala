package views.html.racer

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import controllers.routes

object help {

  def apply()(implicit ctx: Context) = views.html.base.layout(
    title = "战术竞速赛",
    moreCss = cssTag("racer-help")
  )(
      main(cls := "page-small box box-pad racer-help")(
        h1("战术竞速赛"),
        div(cls := "s1")(
          div(cls := "block-img")(
            img(src := staticUrl("images/racer/checkered-flag.svg"))
          ),
          p(a(href := routes.Racer.home())("与其他棋手竞赛，看谁做题又快有准。"))
        ),
        div(cls := "s1")(
          div(cls := "h1")("比赛规则"),
          div(cls := "s2")(
            p("每走对1步棋得1分，目标是在90秒之内尽量多得分。分数最高的棋手赢得比赛。"),
            p("一场比赛中，您将使用相同的棋色。"),
            p("一场比赛中，共60道题目，所有棋手均使用相同的题目。前面的题目相对简单，后面的相对较难。")
          )
        ),
        div(cls := "s1")(
          div(cls := "h1")("竞赛模式"),
          div(cls := "s2")(
            div(cls := "h2")("公开赛"),
            p("进入比赛并创建房间，最多等待30秒，如果没有其他棋手加入，比赛直接开始。"),
            p("30秒内，如果有其他棋手加入，可共同进行比赛。")
          ),
          div(cls := "s2")(
            div(cls := "h2")("俱乐部/班级房间"),
            p("俱乐部管理员或教练可以建立俱乐部或班级的房间，限定参赛棋手范围。"),
            p("每个房间最多容纳20名棋手，创建者可根据需要单独指定。"),
            p("房间可以提前创建，指定开放时间、轮次休息时间等。"),
            p("适合俱乐部/教练在线上组织活动。")
          )
        ),
        div(cls := "s1")(
          div(cls := "h1")("加速器"),
          div(cls := "s2")(
            p("每走对1步棋，可以给加速器充能。加速器充满，将获得奖励加分，并增加下次获得的奖励分。"),
            div(
              strong("奖励分:"),
              ul(
                li("• 5 步棋：+1 分"),
                li("• 12 步棋：+2 分"),
                li("• 20 步棋：+3 分"),
                li("• 30 步棋：+4 分"),
                li("• 之后每10步棋 +4 分")
              )
            ),
            p("如果出现走错的棋，加速器需要重新进行充能。")
          )
        ),
        div(cls := "s1")(
          div(cls := "h1")("跳过（使用魔法）"),
          div(cls := "s2")(
            p("每场比赛中，有1次跳过（使用魔法）的机会，直接按正确着法走棋。"),
            p("最好有策略的进行使用，保持加速器的持续充能。")
          )
        )
      )
    )
}

