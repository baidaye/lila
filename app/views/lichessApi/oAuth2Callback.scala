package views.html.lichessApi

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import lila.lichessApi.OAuth2
import controllers.routes

object oAuth2Callback {

  private val dataVerifierCode = attr("data-verifierCode")

  def apply(o: OAuth2)(implicit ctx: Context) =
    views.html.base.layout(
      title = "lichess.org 用户授权",
      moreJs = jsTag("lichessApi.oAuth2Callback.js"),
      moreCss = cssTag("lichessApiOAuth")
    ) {
        main(cls := "page-small box box-pad oAuth2Callback", dataVerifierCode := o.verifierCode)(
          h1("lichess.org 用户授权..."),
          postForm(cls := "form3")(
            spinner,
            div(cls := "form-actions center")(
              a(cls := "button small disabled")("返回大厅")
            )
          )
        )
      }

  /*
   private def successPage(o: OAuth2, fail: Option[OAuth2.FailResponse], form: Form[_])(implicit ctx: Context) =
      fail.isEmpty option o.lichessUser.map { account =>
        postForm(cls := "form3", action := routes.LichessApi.keepLongTerm())(
          h1("lichess.org 用户授权成功！"),
          form3.group(form("username"), frag("lichess.org 账号："))(_ => strong(account.username)),
          form3.hidden("state", o.id),
          form3.checkbox(form("longTerm"), raw("保持授权"), help = raw("下次匹配无需再次授权").some),
          p("修改授权设置，可以访问：", a("lichess.org 登录授权")),
          div(cls := "form-actions center")(
            button(cls := "button small")("保存并返回大厅")
          )
        )
      }

    private def failPage(o: OAuth2, fail: Option[OAuth2.FailResponse])(implicit ctx: Context) =
      fail.map { fail =>
        postForm(cls := "form3")(
          h1("lichess.org 用户授权失败！"),
          div(cls := "form-group is-invalid")("错误码：", fail.error),
          div(cls := "form-group is-invalid")("错误信息：", fail.error_description),
          p("请稍后再试！"),
          div(cls := "form-actions center")(
            a(cls := "button small", href := routes.Lobby.home)("返回大厅")
          )
        )
      }
  */
}
