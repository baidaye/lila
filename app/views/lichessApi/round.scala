package views.html.lichessApi

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import lila.common.String.html.safeJsonValue
import play.api.libs.json.{ JsObject, JsValue, Json }
import views.html.round.jsI18n.{ baseTranslations, realtimeTranslations }
import controllers.routes

object round {

  def apply(user: JsObject, pref: JsObject, oAuth2: Option[JsObject], round: JsValue, color: chess.Color)(implicit ctx: Context) =
    views.html.base.layout(
      title = "在lichess.org上对局",
      moreJs = frag(
        jsAt(s"compiled/lichess.apiRound${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.apiRound=${
          safeJsonValue(Json.obj(
            "userId" -> ctx.userId,
            "user" -> user,
            "pref" -> pref,
            "oAuth2" -> oAuth2,
            "round" -> round,
            "color" -> color.name,
            "i18n" -> i18nJsObject { baseTranslations ++ realtimeTranslations }
          ))
        }""")
      ),
      moreCss = frag(
        cssTag("round"),
        cssTag("lichessApiRound")
      ),
      chessground = false,
      zoomable = true,
      playing = true,
      robots = false,
      deferJs = false,
      csp = defaultCsp.withPeer.some,
      openGraph = None
    )(
        main(cls := "round apiRound")(
          st.aside(cls := "round__side"),
          div(cls := "round__app")(
            div(cls := "round__app__board main-board"),
            div(cls := "round__app__table"),
            div(cls := "ruser ruser-top user-link")(i(cls := "line"), a(cls := "text")),
            div(cls := "ruser ruser-bottom user-link")(i(cls := "line"), a(cls := "text")),
            div(cls := "rclock rclock-top preload")(div(cls := "time")(nbsp)),
            div(cls := "rclock rclock-bottom preload")(div(cls := "time")(nbsp)),
            div(cls := "rmoves")(div(cls := "moves")),
            div(cls := "rcontrols")(i(cls := "ddloader"))
          )
        )
      )
}
