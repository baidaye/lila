package views.html.lichessApi

import lila.api.Context
import lila.app.ui.ScalatagsTemplate._
import lila.app.templating.Environment._
import lila.lichessApi.OAuth2
import controllers.routes

object oAuth2List {

  private val dataToken = attr("data-token")
  private val dataState = attr("data-state")

  def apply(list: List[OAuth2])(implicit ctx: Context) =
    views.html.account.layout(
      title = "lichess.org 用户授权",
      active = "oAuth2List",
      evenMoreJs = jsTag("lichessApi.oAuth2List.js"),
      evenMoreCss = cssTag("lichessApiOAuth")
    ) {
        main(cls := "page-small box oAuth2List")(
          div(cls := "box__top")(
            h1("lichess.org 用户授权"),
            div(cls := "box__top__actions")(
              button(cls := "button button-green create")("新建")
            )
          ),
          table(cls := "slist")(
            thead(
              tr(
                th("账号"),
                th("授权范围"),
                th("到期时间"),
                th("创建时间"),
                th("状态"),
                th("操作")
              )
            ),
            tbody(
              list.map { oAuth =>
                tr(
                  td(oAuth.lichessUser.map(_.username) | "-"),
                  td(oAuth.scopes.map(_.name).mkString("，")),
                  td(cls := List("expireAt" -> true, "expired" -> oAuth.expired))(oAuth.expireAt.map(_.toString("yyyy-MM-dd HH:mm")) | "-"),
                  td(momentFromNow(oAuth.createdAt)),
                  td(cls := List("atv" -> true, "active" -> oAuth.active))(if (oAuth.active) "可用" else "停用"),
                  td(
                    if (oAuth.active && !oAuth.expired) {
                      button(cls := "button button-empty button-red small cancel", dataToken := oAuth.accessToken, dataState := oAuth.id)("取消授权")
                    } else button(cls := "button button-empty button-red small disabled", disabled)("取消授权")
                  )
                )
              }
            )
          )
        )
      }

}
