package views.html.interest

import lila.api.Context
import play.api.libs.json.{ JsObject, Json }
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import lila.interest.Source
import play.api.data.Form

object showVariant {

  def apply(
    source: Source,
    variant: String,
    pref: JsObject,
    form: Form[_]
  )(implicit ctx: Context) = views.html.base.layout(
    title = s"趣味-${source.name}",
    moreCss = frag(
      cssTag("interestVariant")
    ),
    moreJs = frag(
      drawerTag,
      fairyStockfishTag,
      jsAt(s"compiled/lichess.interestVariant${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""lichess=lichess||{};lichess.interestVariant=${
        safeJsonValue(Json.obj(
          "userId" -> ctx.userId,
          "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
          "pref" -> pref,
          "variant" -> variant,
          "source" -> Json.obj(
            "id" -> source.id,
            "name" -> source.name,
            "desc" -> source.desc
          )
        ))
      }""")
    ),
    zoomable = true
  ) {
      frag(
        themeSearch(source, variant, form),
        main(cls := "interest")(
          st.aside(cls := "interest__side"),
          div(cls := "interest__board main-board")(chessgroundBoard),
          div(cls := "interest__table")
        )
      )
    }

  def rookColorChoices = List("white" -> "白棋双车", "black" -> "黑棋双车")
  def colorChoices = List("white" -> "白棋引擎", "black" -> "黑棋引擎")
  def levelChoices = List("2-1000" -> "1", "4-2000" -> "2", "6-5000" -> "3", "17-8000" -> "4", "20-20000" -> "5")
  def situationChoices = List("8pawn" -> "八兵", "3pawn1" -> "三兵1", "3pawn2" -> "三兵2", "3pawn3" -> "三兵3", "custom" -> "自定义")
  def themeSearch(source: Source, variant: String, form: Form[_])(implicit ctx: Context): Frag =
    drawer(false, source.name) {
      st.form(
        cls := "search_form",
        action := s"${controllers.rt_interest.routes.Interest.showVariant(source.id, variant)}",
        method := "GET"
      )(
          table(
            source == Source.DualRooksAndBishops option tr(
              th(label("棋子")),
              td(
                form3.tagsRadio(form("rookColor"), rookColorChoices)
              )
            ),
            tr(
              th(label("棋色")),
              td(
                form3.tagsRadio(form("color"), colorChoices)
              )
            ),
            tr(
              th(label("级别")),
              td(
                form3.tagsRadio(form("level"), levelChoices)
              )
            ),
            source == Source.PawnsOnly option tr(
              th(label("局面")),
              td(
                form3.tagsRadio(form("situation"), situationChoices)
              )
            ),
            source == Source.DualRooksAndBishops option {
              form3.hidden(form("fen"))
            },
            tr(
              th,
              td(
                span(
                  cls := "mini-board cg-wrap parse-fen is2d init-fen",
                  dataColor := "white",
                  dataFen := form("fen").value
                )(cgWrapContent)
              )
            ),
            source == Source.PawnsOnly option tr(
              th,
              td(cls := List("td-fen" -> true, "transparent" -> !form("situation").value.has("custom")))(
                form3.input(form("fen"))
              )
            )
          ),
          div(cls := "search-action")(
            source == Source.DualRooksAndBishops option a(cls := "button button-empty small random")("随机局面"),
            a(cls := "button button-empty small reset")("重置"),
            submitButton(cls := "button small")("开始练习")
          )
        )
    }

}
