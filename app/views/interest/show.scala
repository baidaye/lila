package views.html.interest

import lila.api.Context
import play.api.libs.json.{ JsObject, Json }
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue
import play.api.data.Form
import lila.interest.{ ID, Source, DataForm }

object show {

  def apply(
    lastId: ID,
    source: Source,
    form: Form[_],
    pref: JsObject,
    data: JsObject,
    nf: Boolean
  )(implicit ctx: Context) = views.html.base.layout(
    title = s"趣味-${source.name}",
    moreCss = frag(
      cssTag("interest")
    ),
    moreJs = frag(
      drawerTag,
      tagsinputTag,
      jsAt(s"compiled/lichess.interest${isProd ?? (".min")}.js"),
      embedJsUnsafe(s"""lichess=lichess||{};lichess.interest=${
        safeJsonValue(Json.obj(
          "userId" -> ctx.userId,
          "notAccept" -> !(ctx.me.isDefined && ctx.me.??(_.hasResource)),
          "data" -> data,
          "pref" -> pref,
          "nf" -> nf,
          "source" -> Json.obj(
            "id" -> source.id,
            "name" -> source.name,
            "desc" -> source.desc
          )
        ))
      }""")
    ),
    zoomable = true
  ) {
      frag(
        themeSearch(source, lastId, form),
        main(cls := "interest")(
          st.aside(cls := "interest__side"),
          div(cls := "interest__board main-board")(chessgroundBoard),
          div(cls := "interest__table")
        )
      )
    }

  def themeSearch(source: Source, lastId: ID, form: Form[_])(implicit ctx: Context): Frag =
    drawer(false, source.name) {
      st.form(
        cls := "search_form",
        action := s"${controllers.rt_interest.routes.Interest.show(source.id, lastId)}",
        method := "GET"
      )(
          form3.hidden("next", "true"),
          table(
            tr(
              th(label("棋色")),
              td(
                form3.tagsRadio(form("color"), DataForm.colorChoices)
              )
            ),
            source == Source.AnnihilationStar || source == Source.AnnihilationPiece || source == Source.CaptureKing option tr(
              th(label("棋子")),
              td(
                form3.tagsRadio(form("role"), DataForm.roleChoices, "".some)
              )
            ),
            source == Source.UnprotectedPieces || source == Source.UnprotectedSquares option tr(
              th(label("阶段")),
              td(
                form3.tagsRadio(form("phase"), DataForm.phaseChoices, "".some)
              )
            ),
            source == Source.AnnihilationStar || source == Source.AnnihilationPiece option tr(
              th(label("步数")),
              td(
                form3.tagsRadio(form("step"), DataForm.stepChoices)
              )
            ),
            source == Source.CaptureKing option tr(
              th(label("步数")),
              td(
                form3.tagsRadio(form("step"), DataForm.kingStepChoices)
              )
            ),
            source == Source.UnprotectedPieces option tr(
              th(label("棋子数")),
              td(
                form3.tagsRadio(form("step"), DataForm.pieceNumChoices)
              )
            ),
            source == Source.UnprotectedSquares option tr(
              th(label("格子数")),
              td(
                form3.tagsRadio(form("step"), DataForm.squareNumChoices)
              )
            ),
            tr(
              th(label("视角")),
              td(
                form3.tagsRadio(form("orientation"), DataForm.orientationChoices)
              )
            ),
            tr(
              th,
              td(
                form3.checkbox(form("autoNext"), "自动下一题", help = frag("得到3星后自动进入下一题").some, klass = "autoNext")
              )
            )
          ),
          div(cls := "search-action")(
            a(cls := "button button-empty small reset")("重置"),
            submitButton(cls := "button small")("开始练习")
          )
        )
    }

}
