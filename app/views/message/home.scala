package views.html.message

import play.api.libs.json._
import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.common.String.html.safeJsonValue

object home {

  def apply(json: JsObject)(implicit ctx: Context) = {
    views.html.base.layout(
      moreCss = cssTag("message"),
      moreJs = frag(
        jsAt(s"compiled/lichess.message${isProd ?? (".min")}.js"),
        embedJsUnsafe(s"""lichess=lichess||{};lichess.message=${
          safeJsonValue(Json.obj(
            "data" -> json,
            "userId" -> ctx.userId
          ))
        }""")
      ),
      title = "信箱"
    ) {
        main(cls := "box message-app")()
      }
  }
}

