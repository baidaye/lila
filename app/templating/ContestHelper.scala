package lila.app
package templating

import lila.app.ui.ScalatagsTemplate._
import lila.contest.Contest
import lila.contest.Env.{ current => contestEnv }
import lila.teamContest.Env.{ current => teamContestEnv }
import controllers.rt_contest.routes

trait ContestHelper { self: I18nHelper with DateHelper with UserHelper =>

  def contestLink(contestId: String, isTeam: Boolean): Frag = a(
    dataIcon := "赛",
    cls := "text",
    href := (if (isTeam) routes.TeamContest.show(contestId) else routes.Contest.show(contestId))
  )(contestIdToName(contestId, isTeam))

  def contestLink(contestId: String, contestName: String): Frag = a(
    dataIcon := "赛",
    cls := "text",
    href := routes.Contest.show(contestId)
  )(contestName)

  def contestIdToName(id: String, isTeam: Boolean) =
    if (isTeam) {
      teamContestEnv.cached name id getOrElse "团体赛"
    } else contestEnv.cached name id getOrElse "比赛"

  def contestIconChar(c: Contest): String = c.perfType.fold('g')(_.iconChar).toString

}
