package controllers.rt_trainCourse

import lila.app._
import play.api.libs.json._
import lila.api.Context
import play.api.mvc.Result
import lila.train.{ TrainCourseStudent => TrainCourseStudentMode, TrainCourseStudentRepo, TrainCourse => TrainCourseModel }
import lila.user.UserRepo
import views.html

object TrainCourse extends controllers.LilaController {

  private val env = Env.train
  private val api = env.api
  private val forms = env.form
  private val jsonView = env.jsonView
  private val memberSelector = Env.team.memberSelector

  def websocket(id: String, apiVersion: Int) = SocketOption[JsValue] { implicit ctx =>
    getSocketSri("sri").?? { sri =>
      env.socketHandler.join(id, sri, ctx.me, lila.common.ApiVersion(apiVersion))
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      if (trainCourse.isCreator(me)) {
        Redirect(controllers.rt_trainCourse.routes.TrainCourseCoach.show(id)).fuccess
      } else if (trainCourse.belongTo(me)) {
        Redirect(controllers.rt_trainCourse.routes.TrainCourseStudent.show(id)).fuccess
      } else notFound
    }
  }

  def createModal = AuthBody { implicit ctx => me =>
    for {
      clazzes <- Env.clazz.api.coachClazzs(me.id)
      allStudents <- Env.coach.studentApi.mineStudentsWithUser(me.id)
      markMap <- Env.relation.markApi.getMarks(me.id)
    } yield {
      Ok(html.train.form.create(forms.createDefault, clazzes.filterNot(_.stopped), allStudents, Nil, markMap))
    }
  }

  def create = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.createOrUpdate.bindFromRequest.fold(
      err => BadRequest(errorsAsJson(err)).fuccess,
      data => api.create(data, me.id) inject jsonOkResult
    )
  }

  def updateModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { tc =>
      Owner(tc) {
        for {
          clazzes <- Env.clazz.api.coachClazzs(me.id)
          allStudents <- Env.coach.studentApi.mineStudentsWithUser(me.id)
          trainStudents <- UserRepo.byIds(tc.studentIds)
          markMap <- Env.relation.markApi.getMarks(me.id)
        } yield {
          Ok(html.train.form.update(tc, forms.updateOf(tc), clazzes, allStudents, trainStudents, markMap))
        }
      }
    }
  }

  def update(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api byId id) { tc =>
      Owner(tc) {
        implicit def req = ctx.body
        forms.createOrUpdate.bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          data => {
            if (tc.canUpdate) {
              api.update(tc, data, me.id) inject jsonOkResult
            } else fuccess(Forbidden(jsonError("Authorization failed")))
          }
        )
      }
    }
  }

  def removeModal(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(api byId id) { tc =>
      Owner(tc) {
        Ok(html.train.form.remove(id)).fuccess
      }
    }
  }

  def remove(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api byId id) { tc =>
      Owner(tc) {
        api.remove(tc) inject jsonOkResult
      }
    }
  }

  def info(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.OwnerOrBelong(trainCourse) {
        fuccess {
          Ok(jsonView.trainCourse(trainCourse)) as JSON
        }
      }
    }
  }

  private[controllers] def OwnerOrBelong(trainCourse: TrainCourseModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    ctx.me match {
      case Some(me) => {
        trainCourse.isCreator(me) match {
          case true => f
          case false => {
            TrainCourseStudentRepo.byId(TrainCourseStudentMode.makeId(trainCourse.id, me.id)) flatMap {
              case Some(student) => if (student.isKicked) KickResult else f
              case None => ForbiddenResult
            }
          }
        }
      }
      case None => ForbiddenResult
    }

  private[controllers] def Belong(trainCourse: TrainCourseModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    ctx.me match {
      case Some(me) => {
        TrainCourseStudentRepo.byId(TrainCourseStudentMode.makeId(trainCourse.id, me.id)) flatMap {
          case Some(student) => if (student.isKicked) KickResult else f
          case None => ForbiddenResult
        }
      }
      case None => ForbiddenResult
    }

  private[controllers] def Owner(trainCourse: TrainCourseModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => trainCourse.isCreator(me))) f
    else ForbiddenResult

  private def ForbiddenResult(implicit ctx: Context) = Forbidden(views.html.site.message.authFailed).fuccess
  private def KickResult(implicit ctx: Context) = Forbidden(views.html.train.kickPage()).fuccess

}
