package controllers.rt_trainCourse

import lila.app._
import lila.chat.Chat
import lila.train.{ TrainCourseStudent => TrainCourseStudentMode, TrainCourseStudentRepo }
import lila.task.{ TTaskRepo, TTask => TTaskModel }
import views.html

object TrainCourseStudent extends controllers.LilaController {

  private val env = Env.train
  private val api = env.api
  private val forms = env.form
  private val jsonView = env.jsonView

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Belong(trainCourse) {
        for {
          signed <- TrainCourseStudentRepo.signed(trainCourse.id, me.id)
          chat <- Env.chat.api.userChat.cached.findMine(Chat.Id(id), ctx.me).map(some)
          _ <- chat ?? { c => Env.user.lightUserApi.preloadMany(c.chat.userIds) }
          tasks <- TTaskRepo.findBySource(TTaskModel.Source.TrainCourse, id, me.id)
          taskJson = Env.task.jsonView.list(tasks)
        } yield Ok(html.train.showStudent(trainCourse, jsonView.trainCourse(trainCourse), chat, signed, taskJson))
      }
    }
  }

  def signIn(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Belong(trainCourse) {
        TrainCourseStudentRepo.list(trainCourse.id, List(TrainCourseStudentMode.Status.Free, TrainCourseStudentMode.Status.Train)).flatMap { list =>
          if (list.size > trainCourse.max) Ok(jsonError("目前参与训练人数超过限制，请稍后再试")).fuccess
          else api.signIn(trainCourse, me.id) inject jsonOkResult
        }
      }
    }
  }

  def signOut(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Belong(trainCourse) {
        api.signOut(trainCourse, me.id) inject jsonOkResult
      }
    }
  }

}
