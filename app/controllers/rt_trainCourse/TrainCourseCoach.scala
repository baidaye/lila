package controllers.rt_trainCourse

import lila.app._
import lila.chat.Chat
import lila.task.TTaskRepo
import lila.train.{ TrainCourseStudentRepo, TrainCourseStudent => TrainCourseStudentMode }
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json._
import views.html

object TrainCourseCoach extends controllers.LilaController {

  private val env = Env.train
  private val api = env.api
  private val forms = env.form
  private val jsonView = env.jsonView

  def show(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          clazzes <- Env.clazz.api.coachClazzs(me.id)
          students <- TrainCourseStudentRepo.list(id)
          studentJson <- jsonView.students(students, markMap)
          teamOption <- me.belongTeamId.??(Env.team.api.ownerOrCoach(_, me.id))
          teamJson = Env.team.api.teamJson(teamOption)
          chat <- Env.chat.api.userChat.cached.findMine(Chat.Id(id), ctx.me).map(some)
          _ <- chat ?? { c => Env.user.lightUserApi.preloadMany(c.chat.userIds) }
          tasks <- TTaskRepo.trainGameTasks(id)
          trainGameJson <- Env.task.jsonView.trainGameTasks(tasks, markMap)
          classicGameJson = lila.distinguish.Distinguish.Classic.toJson
        } yield Ok(html.train.showCoach(trainCourse, jsonView.trainCourse(trainCourse), teamJson, studentJson, chat, trainGameJson, clazzJson(clazzes), classicGameJson))
      }
    }
  }

  private def clazzJson(clazzes: List[lila.clazz.Clazz]) = JsArray(
    clazzes.filterNot(_.stopped).map(clazz =>
      Json.obj(
        "id" -> clazz.id,
        "name" -> clazz.name,
        "studentIds" -> clazz.studentIds
      ))
  )

  def students(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          students <- TrainCourseStudentRepo.list(id)
          studentJson <- jsonView.students(students, markMap)
        } yield Ok(studentJson)
      }
    }
  }

  def players(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          students <- TrainCourseStudentRepo.list(id, List(TrainCourseStudentMode.Status.Free, TrainCourseStudentMode.Status.Train))
          studentJson <- jsonView.players(me.id, students, markMap)
        } yield Ok(studentJson)
      }
    }
  }

  def start(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        api.start(trainCourse) inject jsonOkResult
      }
    }
  }

  def stop(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        api.stop(trainCourse) inject jsonOkResult
      }
    }
  }

  def unAddStudents(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          allStudents <- Env.coach.studentApi.mineStudentsWithUser(me.id)
          unAddStus = allStudents.filter(s => !trainCourse.studentIds.contains(s.userId)).map(_.user)
          studentJson = jsonView.unAddStudentJson(unAddStus, markMap)
        } yield Ok(studentJson)
      }
    }
  }

  def addStudents(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        implicit val req = ctx.body
        Form(single("students" -> list(lila.user.DataForm.historicalUsernameField))).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          studentIds => {
            api.addStudents(trainCourse, studentIds) inject jsonOkResult
          }
        )
      }
    }
  }

  def kick(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api byId id) { trainCourse =>
      TrainCourse.Owner(trainCourse) {
        implicit val req = ctx.body
        Form(single("userId" -> lila.user.DataForm.historicalUsernameField)).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          userId => api.kick(trainCourse, userId) inject jsonOkResult
        )
      }
    }
  }

}
