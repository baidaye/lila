package controllers

import lila.api.Context
import lila.app._
import lila.member.ExchangeCard.CardStatus
import lila.member.{ JsonView, OrderDirector, ProductType }
import views._

object Member extends LilaController {

  private def env = Env.member

  def intro = Auth { implicit ctx => me =>
    for {
      goldCardNumber <- env.memberCardApi.goldCardNumber(me.id)
      silverCardNumber <- env.memberCardApi.silverCardNumber(me.id)
    } yield Ok(html.member.intro(goldCardNumber, silverCardNumber))
  }

  def info = Auth { implicit ctx => me =>
    for {
      orders <- env.orderApi.mine(me.id)
      cards <- env.memberCardApi.mine(me.id)
      levelChangeLogs <- env.memberLevelLogApi.mine(me.id)
      levelPointsLogs <- env.memberPointsLogApi.mine(me.id)
      levelPointsLogOrders <- {
        val orderIds = levelPointsLogs.map(_.orderId).filter(_.isDefined).map(_.get)
        env.orderApi.byIds(orderIds)
      }
      exchangeCards <- env.exchangeCardApi.mine(me.id)
    } yield Ok(html.member.info(me, orders, cards, levelChangeLogs, levelPointsLogs, levelPointsLogOrders, exchangeCards))
  }

  def toBuy(level: Option[String]) = Auth { implicit ctx => me =>
    memberDiscounts.flatMap { discounts =>
      env.orderApi.existsInviteOrder(me.id, ProductType.VirtualMember.id).map { eio =>
        Ok(html.member.buy(me, env.form.order(me, discounts), discounts, level, eio))
      }
    }
  }

  def calcPrice = AuthBody { implicit ctx => me =>
    memberDiscounts flatMap { discounts =>
      implicit val req = ctx.body
      env.form.calcPrice(me, discounts).bindFromRequest.fold(
        jsonFormError,
        data => {
          env.orderApi.existsInviteOrder(me.id, data.productTyp) map { eio =>
            val res = OrderDirector.calcPrice(data, eio, me)
            Ok(JsonView.priceJson(res)) as JSON
          }
        }
      )
    }
  }

  private[controllers] def memberDiscounts(implicit ctx: Context) = {
    Env.team.api.mineCertifyTeam(ctx.me) flatMap { teams =>
      if (teams.isEmpty) {
        Env.coach.studentApi.mineCertifyCoach(ctx.me) map { users =>
          users.map { u =>
            u.id -> u.username
          }
        }
      } else {
        fuccess {
          teams.map { t =>
            t.createdBy -> t.name
          }
        }
      }
    }
  }

  def exchangeUseModal() = Auth { implicit ctx => me =>
    Ok(html.member.info.exchangeUseModal(env.form.exchangeUse)).fuccess
  }

  def exchangeUse() = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    env.form.exchangeUse.bindFromRequest.fold(
      err => jsonFormError(err),
      data => {
        env.exchangeCardApi.byCode(data.toCode) flatMap {
          case Some(card) => {
            card.status match {
              case CardStatus.Create => {
                env.exchangeCardApi.mine(me.id).flatMap { list =>
                  val useIn1Year = list.exists(_.useIn1Year)
                  if (useIn1Year) fuccess {
                    Forbidden(jsonError("本账号一年内使用过兑换码")) as JSON
                  }
                  else {
                    env.exchangeCardApi.use(me, card) inject jsonOkResult
                  }
                }
              }
              case CardStatus.Used => fuccess {
                Forbidden(jsonError("兑换码已使用")) as JSON
              }
              case CardStatus.Expired => fuccess {
                Forbidden(jsonError("兑换码已过期")) as JSON
              }
            }
          }
          case None => fuccess {
            Forbidden(jsonError("兑换码不存在")) as JSON
          }
        }
      }
    )
  }

}
