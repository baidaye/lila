package controllers

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.duration._
import lila.api.Context
import lila.app._
import lila.chat.Chat
import lila.common.paginator.{ Paginator, PaginatorJson }
import lila.common.{ IpAddress, HTTPRequest }
import lila.study.JsonView.JsData
import lila.study.Study.WithChapter
import lila.study.Study.WithChaptersAndLiked
import lila.study.{ Chapter, Order, Study => StudyModel }
import lila.tree.Node.partitionTreeJsonWriter
import views._

object Study extends LilaController {

  type ListUrl = String => Call

  private def env = Env.study

  def restoreIndex = Secure(_.SuperAdmin) { req => me =>
    Env.studySearch.api.restoreAll(1)
    fuccess(jsonOkResult)
  }

  def search(text: String, channel: String, page: Int) = OpenBody { implicit ctx =>
    Reasonable(page) {
      if (text.trim.isEmpty) {
        //all,mine,public,private,member,auth,favorites
        channel match {
          case "all" => Redirect(routes.Study.all(Order.default.key, page)).fuccess
          case "mine" => Redirect(routes.Study.mine(Order.default.key, page)).fuccess
          case "public" => Redirect(routes.Study.minePublic(Order.default.key, page)).fuccess
          case "private" => Redirect(routes.Study.minePrivate(Order.default.key, page)).fuccess
          case "authTo" => Redirect(routes.Study.mineAuthTo(Order.default.key, page)).fuccess
          case "member" => Redirect(routes.Study.mineMember(Order.default.key, page)).fuccess
          case "auth" => Redirect(routes.Study.mineAuth(Order.default.key, page)).fuccess
          case "favorites" => Redirect(routes.Study.mineFavorites(Order.default.key, page)).fuccess
          case _ => Redirect(routes.Study.all(Order.default.key, page)).fuccess
        }
      } else {
        for {
          coaches <- Env.coach.studentApi.mineCoach(ctx.me)
          teamOwners <- Env.team.api.mineTeamOwner(ctx.me)
          pag <- Env.studySearch(ctx.me)(text, channel, coaches.toList, teamOwners.toList, page)
          res <- negotiate(
            html = Ok(html.study.list.search(channel, pag, text)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    }
  }

  def allDefault(page: Int) = all(Order.Popular.key, page)

  def all(o: String, page: Int) = OpenBody { implicit ctx =>
    Reasonable(page) {
      Order(o) match {
        case Order.Oldest => Redirect(routes.Study.allDefault(page)).fuccess
        case order => {
          implicit def req = ctx.body
          def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
          searchForm.fold(
            fail => Ok(html.study.list.all(Paginator.empty[WithChaptersAndLiked], order, fail, Set.empty[String])).fuccess,
            data => {
              for {
                mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
                mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
                //tags <- env.pager.allTags(ctx.me, mineCoaches, mineTeamOwners)
                pag <- env.pager.all(ctx.me, mineCoaches, mineTeamOwners, data, order, page)
                res <- negotiate(
                  html = Ok(html.study.list.all(pag, order, searchForm, Set.empty[String])).fuccess,
                  api = _ => apiStudies(pag)
                )
              } yield res
            }
          )
        }
      }
    }
  }

  def byOwnerDefault(username: String, page: Int) = byOwner(username, Order.default.key, page)

  def byOwner(username: String, order: String, page: Int) = Open { implicit ctx =>
    lila.user.UserRepo.named(username).flatMap {
      _.fold(notFound(ctx)) { owner =>
        env.pager.byOwner(owner, ctx.me, Order(order), page) flatMap { pag =>
          negotiate(
            html = Ok(html.study.list.byOwner(pag, Order(order), owner, lila.study.DataForm.pagerSearch, Set.empty[String])).fuccess,
            api = _ => apiStudies(pag)
          )
        }
      }
    }
  }

  def mine(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.mine(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          tags <- env.pager.mineTags(me)
          pag <- env.pager.mine(me, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.mine(pag, Order(order), me, searchForm, tags)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def minePublic(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.minePublic(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          tags <- env.pager.minePublicTags(me)
          pag <- env.pager.minePublic(me, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.minePublic(pag, Order(order), me, searchForm, tags)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def minePrivate(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.minePrivate(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          tags <- env.pager.minePrivateTags(me)
          pag <- env.pager.minePrivate(me, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.minePrivate(pag, Order(order), me, searchForm, tags)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def mineAuthTo(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.mineAuthTo(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          tags <- env.pager.mineAuthToTags(me)
          pag <- env.pager.mineAuthTo(me, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.mineAuthTo(pag, Order(order), me, searchForm, tags)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def mineMember(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.mineMember(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          //tags <- env.pager.mineMemberTags(me)
          pag <- env.pager.mineMember(me, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.mineMember(pag, Order(order), me, searchForm, Set.empty[String])).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def mineFavorites(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.mineFavorites(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
          mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
          tags <- env.pager.mineFavoritesTags(me)
          pag <- env.pager.mineFavorites(me, mineCoaches, mineTeamOwners, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.mineFavorites(pag, Order(order), me, searchForm, tags)).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def mineAuth(order: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = lila.study.DataForm.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.study.list.mineAuth(Paginator.empty[WithChaptersAndLiked], Order(order), me, fail, Set.empty[String])).fuccess,
      data => {
        for {
          mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
          mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
          //tags <- env.pager.mineAuthTags(me, mineCoaches, mineTeamOwners)
          pag <- env.pager.mineAuth(me, mineCoaches, mineTeamOwners, data, Order(order), page)
          res <- negotiate(
            html = Ok(html.study.list.mineAuth(pag, Order(order), me, searchForm, Set.empty[String])).fuccess,
            api = _ => apiStudies(pag)
          )
        } yield res
      }
    )
  }

  def tags(channel: String) = Auth { implicit ctx => me =>
    {
      channel match {
        case "all" => fuccess(Set.empty[String])
        case "mine" => env.pager.mineTags(me)
        case "public" => env.pager.minePublicTags(me)
        case "private" => env.pager.minePrivateTags(me)
        case "authTo" => env.pager.mineAuthToTags(me)
        case "member" => env.pager.mineMemberTags(me)
        case "auth" => {
          for {
            mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
            mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
            tags <- env.pager.mineAuthTags(me, mineCoaches, mineTeamOwners)
          } yield tags
        }
        case "favorites" => {
          for {
            mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
            mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
            tags <- env.pager.mineFavoritesTags(me)
          } yield tags
        }
        case _ => fuccess(Set.empty[String])
      }
    }.map(tags => Ok(JsArray(tags.map(tag => JsString(tag)).toSeq)))
  }

  def apiStudies(pager: Paginator[StudyModel.WithChaptersAndLiked]) = {
    implicit val pagerWriter = Writes[StudyModel.WithChaptersAndLiked] { s =>
      Env.study.jsonView.pagerData(s)
    }
    Ok(Json.obj(
      "paginator" -> PaginatorJson(pager)
    )).fuccess
  }

  def apiStudies2(pager: Paginator[StudyModel]) = {
    implicit val pagerWriter = Writes[StudyModel] { s =>
      Env.study.jsonView.pagerData2(s)
    }
    Ok(Json.obj(
      "paginator" -> PaginatorJson(pager)
    )).fuccess
  }

  private def orRelay(id: String, chapterId: Option[String] = None)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (HTTPRequest isRedirectable ctx.req) Env.relay.api.getOngoing(lila.relay.Relay.Id(id)) flatMap {
      _.fold(f) { relay =>
        fuccess(Redirect {
          chapterId.fold(routes.Relay.show(relay.slug, relay.id.value)) { c =>
            routes.Relay.chapter(relay.slug, relay.id.value, c)
          }
        })
      }
    }
    else f

  private def showQuery(query: Fu[Option[WithChapter]])(implicit ctx: Context): Fu[Result] =
    OptionFuResult(query) { oldSc =>
      CanViewResult(oldSc.study) {
        for {
          (sc, data) <- getJsonData(oldSc)
          res <- negotiate(
            html = for {
              chat <- chatOf(sc.study)
              sVersion <- env.version(sc.study.id)
              streams <- streamsOf(sc.study)
            } yield Ok(html.study.show(sc.study, data, chat, sVersion, streams)),
            api = _ => chatOf(sc.study).map { chatOpt =>
              Ok(
                Json.obj(
                  "study" -> data.study.add("chat" -> chatOpt.map { c =>
                    lila.chat.JsonView.mobile(
                      chat = c.chat,
                      writeable = ctx.userId.??(sc.study.canChat)
                    )
                  }),
                  "analysis" -> data.analysis
                )
              )
            }
          )
        } yield res
      }
    } map NoCache

  private[controllers] def getJsonData(sc: WithChapter)(implicit ctx: Context): Fu[(WithChapter, JsData)] = for {
    chapters <- Env.study.chapterRepo.orderedMetadataByStudy(sc.study.id)
    (study, resetToChapter) <- Env.study.api.resetIfOld(sc.study, chapters)
    chapter = resetToChapter | sc.chapter
    _ <- Env.user.lightUserApi preloadMany study.members.ids.toList
    _ = if (HTTPRequest isSynchronousHttp ctx.req) Env.study.studyRepo.incViews(study)
    pov = UserAnalysis.makePov(chapter.root.fen.some, chapter.setup.variant)
    analysis <- chapter.serverEval.exists(_.done) ?? Env.analyse.analyser.byId(chapter.id.value)
    division = analysis.isDefined option env.serverEvalMerger.divisionOf(chapter)
    baseData = Env.round.jsonView.userAnalysisJson(pov, ctx.pref, chapter.root.fen.some, chapter.setup.orientation,
      owner = false,
      me = ctx.me,
      division = division)
    studyJson <- Env.study.jsonView(study, chapters, chapter, ctx.me)
  } yield WithChapter(study, chapter) -> JsData(
    study = studyJson,
    analysis = baseData.add(
      "treeParts" -> partitionTreeJsonWriter.writes {
        lila.study.TreeBuilder(chapter.root, chapter.setup.variant)
      }.some
    ).add("analysis" -> analysis.map { lila.study.ServerEval.toJson(chapter, _) })
  )

  def show(id: String) = Open { implicit ctx =>
    orRelay(id) {
      showQuery(env.api byIdWithChapter id)
    }
  }

  def chapter(id: String, chapterId: String) = Open { implicit ctx =>
    orRelay(id, chapterId.some) {
      showQuery(env.api.byIdWithChapter(id, chapterId))
    }
  }

  def chapterMeta(id: String, chapterId: String) = Open { implicit ctx =>
    env.chapterRepo.byId(chapterId).map {
      _.filter(_.studyId.value == id) ?? { chapter =>
        Ok(env.jsonView.chapterConfig(chapter))
      }
    }
  }

  def chapterInfo(id: String, chapterId: String) = Auth { implicit ctx => me =>
    env.api.byIdWithChapter(id, chapterId) flatMap {
      _.fold(notFound) {
        case WithChapter(study, chapter) => {
          if (study.isPublic || study.members.contains(me.id)) {
            fuccess(Ok(env.jsonView.chapterInfo(study, chapter)))
          } else {
            fuccess(Unauthorized(jsonError("您无权使用这个研习.")))
          }
        }
      }
    }
  }

  private[controllers] def chatOf(study: lila.study.Study)(implicit ctx: Context) = {
    !ctx.kid && // no public chats for kids
      ctx.me.fold(true) { // anon can see public chats
        Env.chat.panic.allowed
      }
  } ?? Env.chat.api.userChat.findMine(Chat.Id(study.id.value), ctx.me).map(some)

  def websocket(id: String, apiVersion: Int) = SocketOption[JsValue] { implicit ctx =>
    get("sri") ?? { sri =>
      env.api byId id flatMap {
        _.filter(canView) ?? { study =>
          env.socketHandler.join(
            studyId = id,
            sri = lila.socket.Socket.Sri(sri),
            user = ctx.me,
            getSocketVersion,
            apiVersion
          ) map some
        }
      }
    }
  }

  def createAs = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    lila.study.DataForm.importGame.form.bindFromRequest.fold(
      err => Redirect(routes.Study.byOwnerDefault(me.username)).fuccess,
      data => for {
        owner <- env.studyRepo.recentByOwner(me.id, 50)
        contrib <- env.studyRepo.recentByContributor(me.id, 50)
        res <- if (owner.isEmpty && contrib.isEmpty) createStudy(data, me)
        else Ok(html.study.create(data, owner, contrib)).fuccess
      } yield res
    )
  }

  def create = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    lila.study.DataForm.importGame.form.bindFromRequest.fold(
      err => Redirect(routes.Study.byOwnerDefault(me.username)).fuccess,
      data => createStudy(data, me)
    )
  }

  private def createStudy(data: lila.study.DataForm.importGame.Data, me: lila.user.User)(implicit ctx: Context) =
    env.api.importGame(lila.study.StudyMaker.ImportGame(data), me) flatMap {
      _.fold(notFound) { sc =>
        Redirect(routes.Study.show(sc.study.id.value)).fuccess
      }
    }

  def delete(id: String) = Auth { implicit ctx => me =>
    env.api.byIdAndOwner(id, me) flatMap {
      _ ?? env.api.delete
    } inject Redirect(routes.Study.mine("hot"))
  }

  def clearChat(id: String) = Auth { implicit ctx => me =>
    env.api.isOwner(id, me) flatMap {
      _ ?? Env.chat.api.userChat.clear(Chat.Id(id))
    } inject Redirect(routes.Study.show(id))
  }

  def importPgn(id: String) = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    lila.study.DataForm.importPgn.form.bindFromRequest.fold(
      jsonFormError,
      data => env.api.importPgns(me, StudyModel.Id(id), data.toChapterDatas, sticky = data.sticky)
    )
  }

  def embed(id: String, chapterId: String) = Action.async { implicit req =>
    reqToCtx(req) flatMap { ctx =>
      env.api.byIdWithChapter(id, chapterId).map(_.filter(sc => canView(sc.study)(ctx))) flatMap {
        _.fold(embedNotFound) {
          case WithChapter(study, chapter) => {
            for {
              chapters <- env.chapterRepo.idNames(study.id)
              studyJson <- env.jsonView(study.copy(
                members = lila.study.StudyMembers(Map.empty) // don't need no members
              ), List(chapter.metadata), chapter, none)
              setup = chapter.setup
              initialFen = chapter.root.fen.some
              pov = UserAnalysis.makePov(initialFen, setup.variant)
              baseData = Env.round.jsonView.userAnalysisJson(pov, lila.pref.Pref.default, initialFen, setup.orientation, owner = false, me = none)
              analysis = baseData ++ Json.obj(
                "treeParts" -> partitionTreeJsonWriter.writes {
                  lila.study.TreeBuilder.makeRoot(chapter.root, setup.variant)
                }
              )
              data = lila.study.JsonView.JsData(study = studyJson, analysis = analysis)
              result <- negotiate(
                html = Ok(html.study.embed(study, chapter, chapters, data)).fuccess,
                api = _ => Ok(Json.obj("study" -> data.study, "analysis" -> data.analysis)).fuccess
              )
            } yield result
          }
        }
      }
    }
  }

  private def embedNotFound(implicit req: RequestHeader): Fu[Result] =
    fuccess(NotFound(html.study.embed.notFound))

  private val CloneLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
    credits = 10 * 3,
    duration = 24 hour,
    name = "clone study per user",
    key = "clone_study.user"
  )

  private val CloneLimitPerIP = new lila.memo.RateLimit[IpAddress](
    credits = 20 * 3,
    duration = 24 hour,
    name = "clone study per IP",
    key = "clone_study.ip"
  )

  def cloneStudy(id: String) = Auth { implicit ctx => me =>
    CanClone {
      OptionFuResult(env.api.byId(id)) { study =>
        CanViewResult(study) {
          Ok(html.study.clone(study)).fuccess
        }
      }
    }
  }

  def cloneApply(id: String) = Auth { implicit ctx => me =>
    CanClone {
      implicit val default = ornicar.scalalib.Zero.instance[Fu[Result]](notFound)
      val cost = if (isGranted(_.Coach) || me.hasTitle) 1 else 3
      CloneLimitPerUser(me.id, cost = cost) {
        CloneLimitPerIP(HTTPRequest lastRemoteAddress ctx.req, cost = cost) {
          OptionFuResult(env.api.byId(id)) { prev =>
            CanViewResult(prev) {
              env.api.clone(me, prev) map { study =>
                Redirect(routes.Study.show((study | prev).id.value))
              }
            }
          }
        }
      }
    }
  }

  def cloneChapter(id: String, chapterId: String) = Auth { implicit ctx => me =>
    CanClone {
      OptionFuResult(env.api.byIdWithChapter(id, chapterId)) { studyWithChapter =>
        val study = studyWithChapter.study
        CanViewResult(study) {
          if (lila.study.Settings.UserSelection.allows(study.settings.cloneable, study, me.id.some)) {
            for {
              owner <- env.studyRepo.recentByOwner(me.id, 50)
              contrib <- env.studyRepo.recentByContributor(me.id, 50)
            } yield Ok(html.study.cloneChapter(study, studyWithChapter.chapter, owner, contrib))
          } else fuccess(Forbidden(html.site.message.uncloneStudy(study.ownerId)))
        }
      }
    }
  }

  def cloneChapterApply(id: String, chapterId: String) = AuthBody { implicit ctx => me =>
    CanClone {
      implicit val default = ornicar.scalalib.Zero.instance[Fu[Result]](notFound)
      val cost = if (isGranted(_.Coach) || me.hasTitle) 1 else 3
      CloneLimitPerUser(me.id, cost = cost) {
        CloneLimitPerIP(HTTPRequest lastRemoteAddress ctx.req, cost = cost) {
          OptionFuResult(env.api.byIdWithChapter(id, chapterId)) { studyWithChapter =>
            val study = studyWithChapter.study
            CanViewResult(study) {
              if (lila.study.Settings.UserSelection.allows(study.settings.cloneable, study, me.id.some)) {
                implicit val req = ctx.body
                lila.study.DataForm.cloneChapter.form.bindFromRequest.fold(
                  err => Redirect(routes.Study.byOwnerDefault(me.username)).fuccess,
                  data => env.api.cloneChapter(data, study, studyWithChapter.chapter, me) flatMap {
                    _.fold(notFound) { sc =>
                      Redirect(routes.Study.chapter(sc.study.id.value, sc.chapter.id.value)).fuccess
                    }
                  }
                )
              } else fuccess(Forbidden(html.site.message.uncloneStudy(study.ownerId)))
            }
          }
        }
      }
    }
  }

  private val PgnRateLimitGlobal = new lila.memo.RateLimit[String](
    credits = 30,
    duration = 1 minute,
    name = "export study PGN global",
    key = "export.study_pgn.global"
  )

  def pgn(id: String) = Open { implicit ctx =>
    OnlyHumans {
      PgnRateLimitGlobal("-", msg = HTTPRequest.lastRemoteAddress(ctx.req).value) {
        OptionFuResult(env.api byId id) { study =>
          CanViewResult(study) {
            lila.mon.export.pgn.study()
            env.pgnDump(study) map { pgns =>
              Ok(pgns.mkString("\n\n\n")).withHeaders(
                CONTENT_TYPE -> pgnContentType,
                CONTENT_DISPOSITION -> ("attachment; filename=" + (env.pgnDump filename study))
              )
            }
          }
        }
      }
    }
  }

  def chapterPgn(id: String, chapterId: String) = Open { implicit ctx =>
    OnlyHumans {
      env.api.byIdWithChapter(id, chapterId) flatMap {
        _.fold(notFound) {
          case WithChapter(study, chapter) => CanViewResult(study) {
            lila.mon.export.pgn.studyChapter()
            Ok(env.pgnDump.ofChapter(study, chapter).toString).withHeaders(
              CONTENT_TYPE -> pgnContentType,
              CONTENT_DISPOSITION -> ("attachment; filename=" + (env.pgnDump.filename(study, chapter)))
            ).fuccess
          }
        }
      }
    }
  }

  def multiBoard(id: String, page: Int) = Open { implicit ctx =>
    OptionFuResult(env.api byId id) { study =>
      CanViewResult(study) {
        env.multiBoard.json(study, page, getBool("playing")) map { json =>
          Ok(json) as JSON
        }
      }
    }
  }

  private def CanViewResult(study: StudyModel)(f: => Fu[Result])(implicit ctx: lila.api.Context) =
    if (canView(study)) f
    else negotiate(
      html = fuccess(Unauthorized(html.site.message.privateStudy(study.ownerId))),
      api = _ => fuccess(Unauthorized(jsonError("This study is now private")))
    )

  /*  private def CanCloneResult(study: StudyModel)(f: => Fu[Result])(implicit ctx: lila.api.Context) =
    if (canClone(study)) f
    else negotiate(
      html = fuccess(Unauthorized(html.site.message.privateStudy(study.ownerId))),
      api = _ => fuccess(Unauthorized(jsonError("This study is now private")))
    )*/

  private def canView(study: StudyModel)(implicit ctx: lila.api.Context) = {
    study.isPublic || ctx.userId.exists(study.members.contains) || {
      for {
        coachs <- Env.coach.studentApi.mineCoach(ctx.me)
        teamOwners <- Env.team.api.mineTeamOwner(ctx.me)
      } yield { (study.isStudent && study.members.containsIfOne(coachs)) || (study.isTeam && study.members.containsIfOne(teamOwners)) }
    }.awaitSeconds(3)
  }

  private[controllers] def CanClone(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(_.hasStudyClone)) f
    else NotAcceptable(html.member.notAccept()).fuccess
  }

  /*  private def canClone(study: StudyModel)(implicit ctx: lila.api.Context) =
    study.isPublic || ctx.userId.exists(study.members.containsAndHasWriteRole)*/

  private implicit def makeStudyId(id: String): StudyModel.Id = StudyModel.Id(id)
  private implicit def makeChapterId(id: String): Chapter.Id = Chapter.Id(id)

  private[controllers] def streamsOf(study: StudyModel)(implicit ctx: Context): Fu[List[lila.streamer.Stream]] =
    Env.streamer.liveStreamApi.all.flatMap {
      _.streams.filter { s =>
        study.members.members.exists(m => s is m._2.id)
      }.map { stream =>
        (fuccess(ctx.me ?? stream.streamer.is) >>|
          env.isConnected(study.id, stream.streamer.userId)) map { _ option stream }
      }.sequenceFu.map(_.flatten)
    }

  def studyPage(page: Int, text: String, onlyMember: Boolean) = Auth { implicit ctx => me =>
    Reasonable(page) {
      Env.study.api.minePage(
        me,
        page,
        text.trim,
        onlyMember
      ).flatMap { Study.apiStudies2 }
    }
  }

  def studyChapter(studyId: String) = Auth { implicit ctx => me =>
    Env.study.api.byId(studyId) flatMap {
      _.fold(notFound) { study =>
        Env.study.api.studyChapter(study).map { chapters =>
          env.jsonView.studyChapters(study, chapters.filter(c => !c.isGamebook && !c.isPractice))
        }.map { Ok(_) }
      }
    }
  }
}
