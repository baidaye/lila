package controllers.rt_olclass

import lila.app._
import lila.chat.Chat
import chess.format.FEN
import controllers.UserAnalysis
import lila.game.{ GameRepo, Pov }
import chess.variant.Standard
import lila.study.Node
import play.api.libs.json._
import lila.olclass.{ CourseWareRepo, OlClass => OlClassModal }
import lila.api.Context
import play.api.mvc.Result
import lila.tree.Node.partitionTreeJsonWriter

object OlClass extends controllers.LilaController {

  private val env = Env.olclass
  private val jsonView = env.jsonView

  def websocket(id: String, apiVersion: Int) = SocketOption[JsValue] { implicit ctx =>
    getSocketSri("sri").?? { sri =>
      env.socketHandler.join(id, sri, ctx.me, lila.common.ApiVersion(apiVersion))
    }
  }

  def create(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- Env.clazz.courseApi.byId(id)
      clazzOption <- courseOption.?? { course => Env.clazz.api.byId(course.clazz) }
    } yield (clazzOption |@| courseOption).tupled) {
      case (clazz, course) => {
        if (clazz.isCoach(me.id)) {
          env.api.create(clazz.id, course.id, clazz.coach).inject(
            Redirect(controllers.rt_olclass.routes.OlClass.show(id))
          )
        } else controllers.rt_klazz.Clazz.ForbiddenResult
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- Env.clazz.courseApi.byId(id)
      clazzOption <- courseOption.?? { course => Env.clazz.api.byId(course.clazz) }
      olClassOption <- courseOption.?? { course => env.api.find(course.id) }
    } yield (clazzOption |@| courseOption |@| olClassOption).tupled) {
      case (clazz, course, olClass) => {
        controllers.rt_klazz.Clazz.ClazzMemberOrTeamManager(clazz) { _ =>
          if (!olClass.opened && !olClass.isHost(me.id)) {
            controllers.rt_klazz.Clazz.ForbiddenResult
          } else {
            val emptyFen = FEN("8/8/8/8/8/8/8/8 w - -").some
            val pov = UserAnalysis.makePov(emptyFen, Standard)
            for {
              //olClass <- env.api.findOrCreate(clazz.id, course.id, clazz.coach)
              courseWares <- CourseWareRepo.orderedMetadataByOlClass(id)
              coachJson <- controllers.User.lightUserJson(me.id, clazz.coach)
              students <- Env.user.lightUserApi.asyncMany(clazz.studentIds).map(_.filter(_.isDefined).map(_.get))
              studentsLightUser = clazz.studentIds.map(u => students.find(_.id == u) | lila.common.LightUser.fallback(u))
              studentsJson <- controllers.User.lightUsersJson(me.id, studentsLightUser)
              meJson <- controllers.User.lightUserJson(me.id, me.id)
              roundJson <- Env.api.roundApi.userAnalysisJson(pov, ctx.pref, emptyFen, pov.color, owner = false, me = ctx.me)
              chat <- Env.chat.api.userChat.cached.findMine(Chat.Id(id), ctx.me).map(some)
              _ <- chat ?? { c => Env.user.lightUserApi.preloadMany(c.chat.userIds) }
            } yield {
              Ok(views.html.olclass.show(
                title = s"${clazz.name} - 第 ${course.index} 节",
                sdkAppId = env.tlsSig.getSdkAppId(),
                userSig = env.tlsSig.genUserSig(me.id),
                prefJson = jsonView.pref(ctx.pref),
                tagTypes = JsArray(lila.study.PgnTags.sortedTypes.map(t => JsString(t.name)).toSeq),
                olClassJson = jsonView.olClassJs(olClass),
                courseWareJson = jsonView.courseWares(courseWares),
                roundJson = roundJson,
                clazzJson = jsonView.clazz(clazz),
                courseJson = jsonView.course(course),
                coachJson = coachJson,
                studentsJson = studentsJson,
                chatOption = chat
              ))
            }
          }
        }
      }
    }
  }

  def pgn = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    val orientation = get("orientation")
    val initPly = getInt("initPly")
    Env.importer.forms.importForm.bindFromRequest.fold(
      jsonFormError,
      formData => Env.importer.importer.inMemory(formData).fold(
        err => BadRequest(jsonError(err.shows)).fuccess, {
          case (game, fen) => {
            val pov = Pov(game, chess.White)
            val o = orientation.map(x => chess.Color.apply(x) err s"error color name $x") | pov.color
            Env.api.roundApi.userAnalysisJson(pov, ctx.pref, initialFen = fen, orientation = o, owner = false, me = ctx.me).map { data =>
              val root = pgnRoot(formData.pgn)
              Ok(data.add("initPly", initPly) ++ Json.obj(
                "treeParts" -> partitionTreeJsonWriter.writes {
                  lila.study.TreeBuilder(root, Standard)
                }
              ))
            }
          }
        }
      )
    ).map(_ as JSON)
  }

  private def pgnRoot(pgn: String): Node.Root = {
    lila.study.PgnImport(pgn, Nil).toOption.fold(
      Node.Root(
        ply = 0,
        fen = FEN(chess.variant.Variant.default.initialFen),
        check = false,
        clock = none,
        crazyData = none,
        children = Node.emptyChildren
      )
    ) { res => res.root }
  }

  def fen(urlFen: String) = Auth { implicit ctx => me =>
    val decodedFen: Option[FEN] = lila.common.String.decodeUriPath(urlFen)
      .map(_.replace('_', ' ').trim).filter(_.nonEmpty)
      .orElse(get("fen")) map FEN.apply
    val pov = UserAnalysis.makePov(decodedFen, Standard)
    Env.api.roundApi.userAnalysisJson(pov, ctx.pref, decodedFen, pov.color, owner = false, me = ctx.me).map { data =>
      Ok(data)
    }.map(_ as JSON)
  }

  def game(id: String) = Open { implicit ctx =>
    OptionFuResult(GameRepo game id) { game =>
      val pov = Pov(game, chess.White)
      for {
        initialFen <- GameRepo initialFen game.id
        data <- Env.api.roundApi.userAnalysisJson(pov, ctx.pref, initialFen, pov.color, owner = false, me = ctx.me)
      } yield Ok(data) as JSON
    }
  }

  def getOlClass(id: String) = Auth { implicit ctx => me =>
    OptionResult(env.api.find(id)) { oc =>
      Ok(jsonView.olClassJs(oc)) as JSON
    }
  }

  def open(id: String, o: Boolean) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.open(id, o) inject jsonOkResult
      }
    }
  }

  def start(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.start(id) inject jsonOkResult
      }
    }
  }

  def stop(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.stop(id) inject jsonOkResult
      }
    }
  }

  def join(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      env.api.join(id, me.id) inject jsonOkResult
    }
  }

  def leave(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      env.api.leave(id, me.id) inject jsonOkResult
    }
  }

  def muteAll(id: String, m: Boolean) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.muteAll(id, m) inject jsonOkResult
      }
    }
  }

  def addSync(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.addSync(id, userId) inject jsonOkResult
      }
    }
  }

  def removeSync(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.api.removeSync(id, userId) inject jsonOkResult
      }
    }
  }

  def addHandUp(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      env.api.addHandUp(id, me.id) inject jsonOkResult
    }
  }

  def addSpeaker(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      env.api.addSpeaker(id, userId) inject jsonOkResult
    }
  }

  def removeSpeaker(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      env.api.removeSpeaker(id, userId) inject jsonOkResult
    }
  }

  def courseWares(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- Env.clazz.courseApi.byId(id)
      clazzOption <- courseOption.?? { course => Env.clazz.api.byId(course.clazz) }
    } yield (clazzOption |@| courseOption).tupled) {
      case (clazz, course) => {
        controllers.rt_klazz.Clazz.ClazzMemberOrTeamManager(clazz) { _ =>
          CourseWareRepo.orderedMetadataByOlClass(id) map { list =>
            Ok(jsonView.courseWares(list)) as JSON
          }
        }
      }
    }
  }

  def courseWareInfo(id: String, courseWareId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- Env.clazz.courseApi.byId(id)
      clazzOption <- courseOption.?? { course => Env.clazz.api.byId(course.clazz) }
    } yield (clazzOption |@| courseOption).tupled) {
      case (clazz, course) => {
        controllers.rt_klazz.Clazz.ClazzMemberOrTeamManager(clazz) { _ =>
          CourseWareRepo.byIdAndOlClass(id, courseWareId).flatMap {
            _.fold(notFoundJson()) { d =>
              fuccess {
                Ok(env.jsonView.courseWareInfo(d)) as JSON
              }
            }
          }
        }
      }
    }
  }

  def courseWare(id: String, courseWareId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- Env.clazz.courseApi.byId(id)
      clazzOption <- courseOption.?? { course => Env.clazz.api.byId(course.clazz) }
    } yield (clazzOption |@| courseOption).tupled) {
      case (clazz, course) => {
        controllers.rt_klazz.Clazz.ClazzMemberOrTeamManager(clazz) { _ =>
          CourseWareRepo.byIdAndOlClass(id, courseWareId).flatMap {
            _.fold(notFoundJson()) { d =>
              fuccess {
                val variant = chess.variant.Variant.default
                val pov = UserAnalysis.makePov(d.root.fen.some, variant)
                Ok(jsonView.courseWare(d, pov, ctx.pref)) as JSON
              }
            }
          }
        }
      }
    }
  }

  def courseWareCreate(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        implicit val req = ctx.body
        env.form.create(me).bindFromRequest.fold(
          jsonFormError,
          data => env.courseWareApi.addCourseWare(oc, data) inject jsonOkResult
        ).map(_ as JSON)
      }
    }
  }

  def courseWareUpdate(id: String, courseWareId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        implicit val req = ctx.body
        env.form.edit.bindFromRequest.fold(
          jsonFormError,
          data => env.courseWareApi.updateCourseWare(courseWareId, data) inject jsonOkResult
        )
      }
    }
  }

  def courseWareDelete(id: String, courseWareId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        env.courseWareApi.deleteCourseWare(oc, courseWareId) inject jsonOkResult
      }
    }
  }

  def courseWareSort(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        implicit val req = ctx.body
        env.form.sort.bindFromRequest.fold(
          jsonFormError,
          data => env.courseWareApi.sortCourseWare(id, data.ids) inject jsonOkResult
        )
      }
    }
  }

  def courseWareNote(id: String, courseWareId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.find(id)) { oc =>
      Owner(oc) {
        implicit val req = ctx.body
        env.form.note.bindFromRequest.fold(
          jsonFormError,
          data => env.courseWareApi.setNote(courseWareId, data.text) inject jsonOkResult
        )
      }
    }
  }

  private def Owner(oc: OlClassModal)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => oc.isHost(me.id))) f
    else ForbiddenResult
  }

  private def ForbiddenResult(implicit ctx: Context) = Forbidden(views.html.site.message.authFailed).fuccess

}
