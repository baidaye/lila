package controllers.rt_task

import lila.api.Context
import lila.app._
import lila.common.paginator.Paginator
import play.api.mvc.Result
import lila.task.{ TTaskRepo, TTask => TTaskModel, TTaskTemplate => TTaskTemplateModel }
import lila.team.TeamRepo
import views.html

object TTask extends controllers.LilaController {

  private val env = Env.task
  private val forms = env.forms
  private val api = env.api
  private val tplApi = env.tplApi
  private val jsonView = env.jsonView
  private val taskSolve = env.taskSolve

  def create = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.create.bindFromRequest.fold(
      jsonFormError,
      data => api.create(data, me) inject jsonOkResult
    )
  }

  def cancel(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      Owner(task) {
        api.cancel(task) inject jsonOkResult
      }
    }
  }

  def info(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      OwnerOrBelong(task) {
        task.coinTeam.??(TeamRepo.byId) map { team =>
          Ok(html.task.info(task, team))
        }
      }
    }
  }

  def findByPage(page: Int, source: String, metaId: String, userId: String, status: Option[String]) = Auth { implicit ctx => me =>
    val s = status.filter(_.nonEmpty).map { s => TTaskModel.Status(s) }
    TTaskRepo.findByPage(page, TTaskModel.Source(source), metaId, userId, s) map { pager =>
      Ok(jsonView.page(pager)) as JSON
    }
  }

  def findBySource(source: String, metaId: String, userId: String) = Auth { implicit ctx => me =>
    TTaskRepo.findBySource(TTaskModel.Source(source), metaId, userId) map { list =>
      Ok(jsonView.list(list)) as JSON
    }
  }

  def findMine() = Auth { implicit ctx => me =>
    TTaskRepo.findMine(me.id) map { lst =>
      Ok(jsonView.list(lst)) as JSON
    }
  }

  def findMineItem(item: String) = Auth { implicit ctx => me =>
    TTaskRepo.findMine(me.id, List(TTaskModel.TTaskItemType(item))) map { lst =>
      Ok(jsonView.list(lst)) as JSON
    }
  }

  def current(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    val searchForm = forms.search.bindFromRequest
    searchForm.fold(
      err => BadRequest(html.task.list.current(Paginator.empty[TTaskModel], err)).fuccess,
      data => {
        TTaskRepo.findByUserPage(page, me.id, TTaskModel.Status.current, data.sc, data.st).map { pager =>
          Ok(html.task.list.current(pager, searchForm))
        }
      }
    )
  }

  def history(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    val searchForm = forms.search.bindFromRequest
    searchForm.fold(
      err => BadRequest(html.task.list.history(Paginator.empty[TTaskModel], err)).fuccess,
      data => {
        TTaskRepo.findByUserPage(page, me.id, TTaskModel.Status.history, data.sc, data.st).map { pager =>
          Ok(html.task.list.history(pager, searchForm))
        }
      }
    )
  }

  def createTrainGame = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.trainGame.bindFromRequest.fold(
      jsonFormError,
      data => api.createTrainGame(data, me) inject jsonOkResult
    )
  }

  def trainGameTasks(trainCourseId: String) = Auth { implicit ctx => me =>
    TTaskRepo.trainGameTasks(trainCourseId) flatMap { list =>
      Env.relation.markApi.getMarks(me.id) flatMap { markMap =>
        jsonView.trainGameTasks(list, markMap) map { Ok(_) as JSON }
      }
    }
  }

  def doTask(id: String, extraId: Option[String] = None) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      Belong(task) {
        api.doTask(task) inject Redirect(task.link(me, extraId))
      }
    }
  }

  def toTask(id: String, extraId: Option[String] = None) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      OwnerOrBelong(task) {
        Redirect(task.link(me, extraId)).fuccess
      }
    }
  }

  def solveReplayGame(id: String, red: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      Belong(task) {
        taskSolve.handleReplayGame(me.id, id, red) inject Redirect(red)
      }
    }
  }

  def formPositionModal(id: String, fen: String, limit: Int, increment: Int, aiLevel: Option[Int], color: Option[String], canTakeback: Option[Boolean]) = Auth { implicit ctx => me =>
    Ok(html.task.info.formPositionModal(fen, limit, increment, aiLevel, color, canTakeback)).fuccess
  }

  def formPgnModal(
    id: String,
    pgn: String,
    limit: Int,
    increment: Int,
    aiLevel: Option[Int],
    color: Option[String],
    canTakeback: Option[Boolean],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean]
  ) = Auth { implicit ctx => me =>
    Ok(html.task.info.formPgnModal(pgn, limit, increment, aiLevel, color, canTakeback, openingdbId, openingdbExcludeWhiteBlunder, openingdbExcludeBlackBlunder, openingdbExcludeWhiteJscx, openingdbExcludeBlackJscx, openingdbCanOff)).fuccess
  }

  def updatePlanAtModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      Belong(task) {
        Ok(html.task.info.updatePlanAtModal(task, forms.planAtForm)).fuccess
      }
    }
  }

  def updatePlanAt(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api byId id) { task =>
      Belong(task) {
        implicit def req = ctx.body
        forms.planAtForm.bindFromRequest.fold(
          jsonFormError,
          planAt => api.updatePlanAt(task, planAt) inject jsonOkResult
        )
      }
    }
  }

  def createTpl = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.tplCreate.bindFromRequest.fold(
      jsonFormError,
      data => tplApi.create(data, me) map { tpl =>
        Ok(jsonView.taskTemplateInfo(tpl))
      }
    )
  }

  def updateTpl(tplId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(tplApi.byId(tplId)) { tpl =>
      TplOwner(tpl) {
        implicit def req = ctx.body
        forms.tplCreate.bindFromRequest.fold(
          jsonFormError,
          data => tplApi.update(tpl, data, me) map { tpl =>
            Ok(jsonView.taskTemplateInfo(tpl))
          }
        )
      }
    }
  }

  def removeTpl(tplId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(tplApi.byId(tplId)) { tpl =>
      TplOwner(tpl) {
        tplApi.remove(tpl) inject jsonOkResult
      }
    }
  }

  private[controllers] def TplOwner(ttaskTpl: TTaskTemplateModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => ttaskTpl.isCreator(me))) f
    else ForbiddenResult

  private[controllers] def Owner(ttask: TTaskModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => ttask.isCreator(me))) f
    else ForbiddenResult

  private[controllers] def Belong(ttask: TTaskModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => ttask.belongTo(me))) f
    else ForbiddenResult

  private[controllers] def OwnerOrBelong(ttask: TTaskModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => ttask.isCreator(me) || ttask.belongTo(me))) f
    else ForbiddenResult

  private[controllers] def ForbiddenResult(implicit ctx: Context) = Forbidden(views.html.site.message.authFailed).fuccess

}
