package controllers

import lila.app._
import lila.common.HTTPRequest
import lila.importer.{ ImportData, MultiPgn }
import lila.puzzle.PuzzleId
import ornicar.scalalib.Zero
import play.api.libs.json.Json
import play.api.mvc.{ RequestHeader, Result, Results }
import scalaz.{ Failure, Success }
import views._

object Importer extends LilaController {

  private def env = Env.importer
  private def puzzle = Env.puzzle

  def pgnImportForm() = AuthBody { implicit ctx => me =>
    fuccess {
      val pgn = ctx.body.queryString.get("pgn").flatMap(_.headOption).getOrElse("")
      val pgnData = lila.importer.ImportData(pgn, None)
      val pgnForm = env.forms.importForm.fill(pgnData)
      Ok(html.resource.game.importedModal(pgnForm))
    }
  }

  def pgnImport = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    env.forms.importForm.bindFromRequest.fold(
      _ => BadRequest(Json.obj("error" -> "无效的PGN格式")).fuccess,
      data => MultiPgn.split(data.pgn, max = env.batchMaxSize).value.map { onePgn =>
        env.importer(data.copy(pgn = onePgn), ctx.userId) flatMap { game =>
          (ctx.userId ?? Env.game.cached.clearNbImportedByCache) >>
            (data.analyse.isDefined && game.analysable) ?? {
              Env.fishnet.analyser(game, lila.fishnet.Work.Sender(
                userId = ctx.userId,
                ip = HTTPRequest.lastRemoteAddress(ctx.req).some,
                mod = isGranted(_.Hunter) || isGranted(_.Relay),
                system = false
              ))
            }
        } recover {
          case e => {
            controllerLogger.branch("importer").warn(
              s"Imported game validates but can't be replayed:\n$onePgn", e
            )
            0
          }
        } inject (1)
      }.sequenceFu.map { list =>
        jsonOkResult
      }
    )
  }

  import scala.concurrent.duration._
  import lila.common.{ HTTPRequest }
  private val ImportLimitGlobal = new lila.memo.RateLimit[String](
    credits = 30,
    duration = 1 minute,
    name = "api import pgn",
    key = "api.games.global"
  )

  private val ImportLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
    credits = 10,
    duration = 1 minute,
    name = "api import pgn per user",
    key = "api.games.user"
  )

  def rateLimit(id: lila.user.User.ID, req: RequestHeader)(run: => Fu[Result]): Fu[Result] = {
    implicit val limitedDefault = Zero.instance[Fu[Result]](fuccess(Results.TooManyRequest("请求过于频繁，请稍后再试。")))
    ImportLimitGlobal("-", cost = 1) {
      ImportLimitPerUser(id, cost = 1) {
        run
      }
    }
  }

  def pgnImportApp(pgn: String, encode: Option[Boolean] = None) = Auth { implicit ctx => me =>
    rateLimit(me.id, ctx.req) {
      val decodedPgn = if (encode | false) decodePgn(pgn) else pgn
      ImportData(decodedPgn, none).preprocess(me.id.some) match {
        case Success(p) => {
          env.importer.insertAndFinish(p.game) inject Redirect(controllers.rt_resource.routes.Resource.gameImported(1))
        }
        case Failure(e) => {
          BadRequest(s"无效的PGN格式：\r\n\r\n$e").fuccess
        }
      }
    }
  }

  private def decodePgn(pgn: String) = {
    pgn.replaceAll("~", ". ").replaceAll("_", " ")
  }

  def puzzleImportForm() = Auth { implicit ctx => me =>
    fuccess {
      Ok(html.resource.puzzle.importedModal(lila.puzzle.DataForm.fenForm()))
    }
  }

  def puzzleImport = this.synchronized {
    AuthBody { implicit ctx => me =>
      implicit def req = ctx.body
      lila.puzzle.DataForm.fenForm().bindFromRequest.fold(
        _ => BadRequest(Json.obj("error" -> "无效的战术题格式")).fuccess,
        data => (lila.db.Util findNextId (puzzle.puzzleColl)) flatMap { puzzleMaxId =>
          val minImportPuzzleId = lila.puzzle.Puzzle.minImportId
          var puzzleId = minImportPuzzleId atLeast puzzleMaxId
          MultiPgn.split(data.pgn, max = env.batchMaxSize).value.map { onePgn =>
            puzzleId = puzzleId + 1
            data.copy(pgn = onePgn) preprocess (me.id.some) match {
              case Success(p) =>
                puzzle.api.puzzle.insert(p.copy(id = puzzleId)) recover {
                  case e => {
                    controllerLogger.branch("importer").warn(
                      s"Imported puzzle validates but can't be replayed:\n$onePgn", e
                    )
                    0
                  }
                } inject (1)
              case Failure(e) =>
                controllerLogger.branch("importer").warn(
                  s"Imported puzzle validates but can't be replayed:\n$onePgn"
                )
                fufail(e)
            }
          }.sequenceFu.map { list =>
            jsonOkResult
          }
        }
      )
    }
  }

  def masterGame(id: String, orientation: String) = Open { implicit ctx =>
    Env.explorer.importer(id) map {
      _ ?? { game =>
        val url = routes.Round.watcher(game.id, orientation).url
        val fenParam = get("fen").??(f => s"?fen=$f")
        Redirect(s"$url$fenParam")
      }
    }
  }

  def lichessGame(id: String, orientation: String) = Open { implicit ctx =>
    Env.explorer.importer(id, url = s"https://lichess.org/game/export/$id?literate=1".some) map {
      _ ?? { game =>
        val url = routes.Round.watcher(game.id, orientation).url
        val fenParam = get("fen").??(f => s"?fen=$f")
        Redirect(s"$url$fenParam")
      }
    }
  }

  def systemImport(secret: String, gameId: String, puzzleId: PuzzleId) = OpenBody { implicit ctx =>
    if (secret != "B15iz3lcF12OmQj32wQ6M9jpYqLyKBtN") {
      fuccess(Results.Forbidden("Authorization failed"))
    } else {
      implicit def req = ctx.body
      env.forms.importForm.bindFromRequest.fold(
        failure => jsonFormErrorDefaultLang(failure),
        data => env.puzzleGameImporter(gameId, data.pgn) flatMap { game =>
          (data.analyse.isDefined && game.analysable) ?? {
            Env.fishnet.analyser(game, lila.fishnet.Work.Sender(
              userId = None,
              ip = HTTPRequest.lastRemoteAddress(ctx.req).some,
              mod = true,
              system = true
            ))
          } inject Ok(Json.arr(gameId, puzzleId))
        }
      )
    }
  }

  /*
  def sendPgnGame = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    env.forms.importForm.bindFromRequest.fold(
      failure => negotiate(
        html = Ok(html.game.importGame(failure, env.forms.fenForm, true, false)).fuccess,
        api = _ => BadRequest(Json.obj("error" -> "Invalid PGN")).fuccess
      ),
      data => env.importer(data, ctx.userId) flatMap { game =>
        (ctx.userId ?? Env.game.cached.clearNbImportedByCache) >>
          (data.analyse.isDefined && game.analysable) ?? {
            Env.fishnet.analyser(game, lila.fishnet.Work.Sender(
              userId = ctx.userId,
              ip = HTTPRequest.lastRemoteAddress(ctx.req).some,
              mod = isGranted(_.Hunter) || isGranted(_.Relay),
              system = false
            ))
          } inject Redirect(routes.Round.watcher(game.id, "white"))
      } recover {
        case e =>
          controllerLogger.branch("importer").warn(
            s"Imported game validates but can't be replayed:\n${data.pgn}", e
          )
          Redirect(routes.Importer.importGame)
      }
    )
  }
*/

}
