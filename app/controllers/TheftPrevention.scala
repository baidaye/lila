package controllers

import lila.api.Context
import lila.app._
import lila.game.{ Game => GameModel, Pov, AnonCookie }
import lila.security.Granter
import play.api.mvc._

trait TheftPrevention { self: LilaController =>

  def PreventTheft(pov: Pov)(ok: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (isTheft(pov)) fuccess(Redirect(routes.Round.watcher(pov.gameId, pov.color.name)))
    else ok

  def isTheft(pov: Pov)(implicit ctx: Context) = pov.game.isPgnImport || pov.player.isAi || {
    (pov.player.userId, ctx.userId) match {
      case (Some(_), None) => true
      case (Some(playerUserId), Some(userId)) => playerUserId != userId
      case (None, _) =>
        !lila.api.Mobile.Api.requested(ctx.req) &&
          !ctx.req.cookies.get(AnonCookie.name).exists(_.value == pov.playerId)
    }
  }

  def isMyPov(pov: Pov)(implicit ctx: Context) = !isTheft(pov)

  def playablePovForReq(game: GameModel)(implicit ctx: Context) =
    (!game.isPgnImport && game.playable) ?? {
      ctx.userId.flatMap(game.playerByUserId).orElse {
        ctx.req.cookies.get(AnonCookie.name).map(_.value)
          .flatMap(game.player).filterNot(_.hasUser)
      }.filterNot(_.isAi).map { Pov(game, _) }
    }

  lazy val theftResponse = Unauthorized(jsonError(
    "This game requires authentication"
  )) as JSON
}
