package controllers.rt_demonstrate

import lila.app._

object Demonstrate extends controllers.LilaController {

  private val env = Env.demonstrate
  private val jsonView = env.jsonView

  def home() = Auth { implicit ctx => me =>
    val fen = get("fen")
    fuccess(
      Ok(views.html.demonstrate.home(
        fen,
        jsonView.pref(ctx.pref)
      ))
    )
  }

}
