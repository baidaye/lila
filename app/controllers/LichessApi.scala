package controllers

import lila.api.Context
import lila.app._
import lila.game.GameRepo
import lila.lichessApi.{ OAuth2Repo, RoundRepo }
import lila.user.UserRepo
import play.api.mvc.Result
import views.html

object LichessApi extends LilaController {

  private def env = Env.lichessApi
  private def oAuth2Api = env.oAuth2Api
  private def roundApi = env.roundApi
  private def forms = env.forms
  private def jsonView = env.jsonView

  def oAuthCreate() = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    forms.oAuth2Create.bindFromRequest.fold(
      err => jsonFormError(err),
      data => oAuth2Api.create(data, me.id) inject jsonOkResult
    )
  }

  def oAuthCallback(code: String, state: String) = Auth { implicit ctx => me =>
    OptionResult(oAuth2Api.byId(state)) { o =>
      Ok(html.lichessApi.oAuth2Callback(o))
    }
  }

  def setAuthInfo(state: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(oAuth2Api.byId(state)) { o =>
      implicit val req = ctx.body
      forms.oAuth2Update.bindFromRequest.fold(
        err => jsonFormError(err),
        data => oAuth2Api.setAuthInfo(o, data) inject jsonOkResult
      )
    }
  }

  def setUnActive(state: String) = Auth { implicit ctx => me =>
    OptionFuResult(oAuth2Api.byId(state)) { o =>
      OAuth2Repo.setUnActive(state) inject jsonOkResult
    }
  }

  def oAuthList() = Auth { implicit ctx => me =>
    Permiss {
      OAuth2Repo.findList(me.id) map { oAuthList =>
        Ok(html.lichessApi.oAuth2List(oAuthList))
      }
    }
  }

  def toRound(gameId: String) = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      forms.round.bindFromRequest.fold(
        err => jsonFormError(err),
        data => roundApi.create(data, me) flatMap { round =>
          for {
            whiteOption <- UserRepo.byId(round.white.local.id)
            blackOption <- UserRepo.byId(round.black.local.id)
          } yield Ok(jsonView.round(round, whiteOption, blackOption))
        }
      )
    }
  }

  def round(gameId: String, color: String) = Auth { implicit ctx => me =>
    Permiss {
      val c = chess.Color(color) err s"can not apply color $color"
      OptionFuResult(roundApi.byId(gameId)) { round =>
        for {
          oAuth2Option <- oAuth2Api.findActive(me.id)
          whiteOption <- UserRepo.byId(round.white.local.id)
          blackOption <- UserRepo.byId(round.black.local.id)
        } yield Ok(html.lichessApi.round(
          jsonView.user(round, me),
          jsonView.pref(ctx.pref),
          oAuth2Option.map(jsonView.oAuth2),
          jsonView.round(round, whiteOption, blackOption),
          c
        ))
      }
    }
  }

  def updateRoundPlayer(gameId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(roundApi.byId(gameId)) { round =>
      implicit val req = ctx.body
      forms.roundPlayer.bindFromRequest.fold(
        err => jsonFormError(err),
        data => roundApi.updatePlayer(round, data, me.id) inject jsonOkResult
      )
    }
  }

  def finishRound(gameId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(roundApi.byId(gameId)) { round =>
      implicit val req = ctx.body
      forms.roundFinish.bindFromRequest.fold(
        err => jsonFormError(err),
        data => roundApi.finish(round, data, me.id).map {
          case None => jsonOkResult
          case Some(diffs) => Ok(jsonView.diff(diffs)) as JSON
        }
      )
    }
  }

  def redirectedRound(gameId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(roundApi.byId(gameId)) { round =>
      roundApi.setRedirected(round, me.id) inject jsonOkResult
    }
  }

  private[controllers] def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(_.isMemberOrCoachOrTeam)) f
    else NotAcceptable(html.member.notAccept()).fuccess
  }

}
