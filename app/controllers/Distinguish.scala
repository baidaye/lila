package controllers

import lila.api.Context
import lila.app.{ Env, _ }
import lila.distinguish.{ DataForm, Distinguish => DistinguishModel }
import play.api.mvc.Result

object Distinguish extends LilaController {

  private def env = Env.distinguish

  def home = Auth { implicit ctx => me =>
    val create = getBoolOpt("create")
    val tab = get("tab")
    val gameId = get("gameId")
    env.api.history(me.id) flatMap { list =>
      env.jsonView(DistinguishModel.makeSyntheticDistinguish, list).map { data =>
        views.html.distinguish.show(
          data = data,
          pref = env.jsonView.pref(ctx.pref),
          home = true,
          create = create,
          tab = tab,
          gameId = gameId,
          fr = get("fr")
        )
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuOk(env.api.byId(id)) { distinguish =>
      env.api.history(me.id) flatMap { list =>
        env.jsonView(distinguish, list).map { data =>
          views.html.distinguish.show(
            data = data,
            pref = env.jsonView.pref(ctx.pref),
            fr = get("fr")
          )
        }
      }
    }
  }

  def showOfMate(pgn: String) = Auth { implicit ctx => me =>
    env.api.history(me.id) flatMap { list =>
      val rightTurns = getInt("rightTurns")
      val turns = getInt("turns")
      val orientation = get("orientation")
      val title = get("title")
      val hashId = get("hashId")
      env.jsonView(DistinguishModel.makeTemporaryDistinguish(turns, orientation, title), list, pgn.some).map { data =>
        Ok(
          views.html.distinguish.show(
            data = data
              .add("hashId" -> hashId)
              .add("rightTurns" -> rightTurns),
            pref = env.jsonView.pref(ctx.pref),
            fr = get("fr")
          )
        )
      }
    }
  }

  def page(page: Int) = Auth { implicit ctx => me =>
    env.api.page(me.id, page) map { pager =>
      Ok(views.html.distinguish.list(pager))
    }
  }

  def createForm = Auth { implicit ctx => me =>
    Ok(views.html.distinguish.modal.createForm(env.form.create(me))).fuccess
  }

  def create = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    env.form.create(me).bindFromRequest.fold(
      jsonFormError,
      data => {
        env.api.create(data, me.id).map { pr =>
          Ok(jsonOkBody.add("id" -> pr.id.some))
        }
      }
    ).map(_ as JSON)
  }

  def distinguishPgn = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    env.form.create(me).bindFromRequest.fold(
      jsonFormError,
      data => env.api.gamePgn(data, Env.olclass.courseWareApi.fetchPgn).map { result =>
        Ok(
          jsonOkBody
            .add("name" -> result.name.some)
            .add("fen" -> result.fen.some)
            .add("pgn" -> result.pgn.some)
            ++ Env.study.api.pgnToTurns(result.pgn)
        )
      }
    ).map(_ as JSON)
  }

  def editForm(id: String, goTo: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { distinguish =>
      Ok(views.html.distinguish.modal.editForm(distinguish, goTo, env.form.editOf(distinguish))).fuccess
    }
  }

  def update(id: String, goTo: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { distinguish =>
      Owner(distinguish) {
        implicit val req = ctx.body
        env.form.edit.bindFromRequest.fold(
          jsonFormError,
          data => env.api.update(distinguish, data) inject Redirect(goTo)
        )
      }
    }
  }

  def delete(id: String, goTo: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { distinguish =>
      Owner(distinguish) {
        env.api.delete(distinguish) inject Redirect(goTo)
      }
    }
  }

  def finish(id: Option[String], hashId: Option[String]) = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    DataForm.finish.bindFromRequest.fold(
      jsonFormError,
      data => data match {
        case (win, turns) => {
          env.api.finish(id, hashId, win, turns, me.id).map { _ =>
            jsonOkResult
          }
        }
      }
    )
  }

  def createAs = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    env.form.createAs.bindFromRequest.fold(
      err => Redirect(routes.Distinguish.home).fuccess,
      data => env.api.createBy(data.toDistinguish(me.id)).map { pr =>
        Redirect(routes.Distinguish.show(pr.id))
      }
    )
  }

  private def Owner(distinguish: DistinguishModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => distinguish.isCreator(me.id))) f
    else ForbiddenResult
  }

  private def ForbiddenResult(implicit ctx: Context) = Forbidden(views.html.site.message.authFailed).fuccess

}
