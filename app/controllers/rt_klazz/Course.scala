package controllers.rt_klazz

import lila.app._
import lila.clazz.{ UpdateData, Course => CourseModel }
import lila.train.{ TrainCourse => TrainCourseModel }
import org.joda.time.DateTime
import views._

object Course extends controllers.LilaController {

  private val env = Env.clazz
  private val form = env.courseForm

  def timetable(week: Int) = Secure(_.Coach) { implicit ctx => me =>
    val firstDay = DateTime.now.withDayOfWeek(1).plusWeeks(week)
    val lastDay = DateTime.now.withDayOfWeek(7).plusWeeks(week)
    for {
      clazzList <- env.api.byCourseList(me.id)
      courseList <- env.courseApi.weekCourse(firstDay, lastDay, clazzList.map(_.id))
      trainCourseList <- Env.train.api.weekCourse(firstDay, lastDay, me.id)
    } yield {
      val courseMap: Map[(String, String), List[CourseModel.WithClazz]] = courseList.filter(c => clazzList.exists(_.id == c.clazz)).sortBy(_.timeBegin) map { c =>
        CourseModel.WithClazz(c, clazzList.find(c.clazz == _._id).get)
      } groupBy { c =>
        val d = c.course.date.toString("M月d日")
        val t = c.course.period
        (d, t)
      }

      val trainCourseMap: Map[(String, String), List[TrainCourseModel]] = trainCourseList.sortBy(_.timeBegin) groupBy { c =>
        val d = c.date.toString("M月d日")
        val t = c.period
        (d, t)
      }

      Ok(html.clazz.course(firstDay, lastDay, courseMap, trainCourseMap, week))
    }
  }

  def updateModal(id: String, week: Int) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- env.courseApi.byId(id)
      clazzOption <- courseOption.??(c => env.api.byId(c.clazz))
    } yield (courseOption |@| clazzOption).tupled) {
      case (course, clazz) => {
        Clazz.OwnerAndEnable(clazz) {
          Ok(
            html.clazz.modal.course.update(
              course,
              form.updateForm fill UpdateData(course.date, course.timeBegin, course.timeEnd),
              week
            )
          ).fuccess
        }
      }
    }
  }

  def update(id: String, week: Int) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- env.courseApi.byId(id)
      clazzOption <- courseOption.??(c => env.api.byId(c.clazz))
    } yield (courseOption |@| clazzOption).tupled) {
      case (course, clazz) => {
        Clazz.OwnerAndEnable(clazz) {
          implicit def req = ctx.body
          form.updateForm.bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => env.courseApi.update(course, data) inject jsonOkResult
          )
        }
      }
    }
  }

  def stopModal(id: String, week: Int) = Secure(_.Coach) { implicit ctx => me =>
    Ok(html.clazz.modal.course.stop(id, week)).fuccess
  }

  def stop(id: String, week: Int) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- env.courseApi.byId(id)
      clazzOption <- courseOption.??(c => env.api.byId(c.clazz))
    } yield (courseOption |@| clazzOption).tupled) {
      case (course, clazz) => {
        Clazz.OwnerAndEnable(clazz) {
          env.courseApi.stop(course) inject {
            Redirect(routes.Course.timetable(week))
          }
        }
      }
    }
  }

  def postponeModal(id: String, week: Int) = Secure(_.Coach) { implicit ctx => me =>
    Ok(html.clazz.modal.course.postpone(id, week)).fuccess
  }

  def postpone(id: String, week: Int) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      courseOption <- env.courseApi.byId(id)
      clazzOption <- courseOption.??(c => env.api.byId(c.clazz))
    } yield (courseOption |@| clazzOption).tupled) {
      case (course, clazz) => {
        Clazz.OwnerAndEnable(clazz) {
          env.api.byId(course.clazz) flatMap {
            _.fold(notFound(ctx)) { clazz =>
              env.courseApi.postpone(course, clazz) flatMap { lastCourseTime =>
                env.api.updateWeekCourseLastDate(clazz.id, lastCourseTime) inject {
                  Redirect(routes.Course.timetable(week))
                }
              }
            }
          }
        }
      }
    }
  }

  def appendCourseModal(clazzId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byIdWithCoach(clazzId, me)) { cwc =>
      val clazz = cwc.clazz
      Clazz.ClazzOrTeamManager(clazz) { _ =>
        if (clazz.isWeek) {
          val nextFirstDate = clazz.weekClazz.map { weekClazz =>
            weekClazz.copy(times = 1).toCourseFromWeek(clazz.id, clazz.coach, clazz.times + 1, clazz.endDate.plusDays(1)).head.date
          } | clazz.endDate.plusDays(1)

          val appendForm = form.appendForm(clazz) fill lila.clazz.Append(nextFirstDate, 10)
          val startDate = nextFirstDate.toString("yyyy-MM-dd")
          Env.team.api.campusWithTeam(cwc.clazz.teamOrDefault) map { teamOption =>
            Ok(html.clazz.modal.course.appendCourse(cwc, teamOption, startDate, appendForm))
          }
        } else Clazz.ForbiddenResult
      }
    }
  }

  def appendCourse(clazzId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(clazzId)) { clazz =>
      Clazz.ClazzOrTeamManager(clazz) { _ =>
        if (clazz.isWeek) {
          implicit def req = ctx.body
          form.appendForm(clazz).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => env.courseApi.appendCourse(clazz, data, env.api.updateWeekClazz) inject jsonOkResult
          )
        } else Clazz.ForbiddenResult
      }
    }
  }

}
