package controllers.rt_klazz

import lila.app._
import lila.clazz.{ Invite, InviteRepo }
import lila.team.{ Campus, TagRepo, TeamRepo }
import lila.user.UserRepo
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{ JsArray, Json }
import views._

object Student extends controllers.LilaController {

  private val env = Env.clazz

  def invite(clazzId: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(clazzId)) { clazz =>
      Clazz.ClazzOrTeamManager(clazz) { _ =>
        implicit def req = ctx.body
        Form(single(
          "username" -> lila.user.DataForm.historicalUsernameField
        )).bindFromRequest.fold(
          err => RenderError(clazzId, errorsAsJson(err).toString),
          username => {
            if (me.username == username) {
              RenderError(clazzId, "不可以邀请自己")
            } else {
              UserRepo named username flatMap {
                case None => RenderError(clazzId, "邀请学员不存在")
                case Some(u) => {
                  InviteRepo.find(clazzId, u.id) flatMap {
                    case None => env.studentApi.byId(clazzId, u.id).flatMap {
                      case None => env.studentApi.inviteStudent(clazz, u.id) inject Redirect(routes.Clazz.detail(clazzId))
                      case Some(s) => RenderError(clazzId, "学员已经加入")
                    }
                    case Some(invite) => invite.status match {
                      case Invite.Status.Invited => {
                        if (invite.expired) {
                          env.studentApi.reInviteStudent(clazz, u.id) inject Redirect(routes.Clazz.detail(clazzId))
                        } else RenderError(clazzId, "请勿重复邀请")
                      }
                      case Invite.Status.Joined => RenderError(clazzId, "学员已经加入")
                      case Invite.Status.Refused => env.studentApi.invitedAgain(clazz, u.id) inject Redirect(routes.Clazz.detail(clazzId))
                    }
                  }
                }
              }
            }
          }
        )
      }
    }
  }

  private def RenderError(clazzId: String, error: String) = {
    val queryString = Map[String, Seq[String]]("error" -> Seq(error))
    Redirect(routes.Clazz.detail(clazzId).toString, queryString = queryString).fuccess
  }

  def cancelInvite(inviteId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      inviteOption ← InviteRepo.byId(inviteId)
      clazzOption ← inviteOption.?? { i => env.api.byId(i.clazzId) }
    } yield (clazzOption |@| inviteOption).tupled) {
      case (clazz, invite) => {
        Clazz.ClazzOrTeamManager(clazz) { _ =>
          if (invite.isInvited) {
            InviteRepo.remove(inviteId) inject Redirect(routes.Clazz.detail(invite.clazzId))
          } else Clazz.ForbiddenResult
        }
      }
    }
  }

  def remove(clazzId: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(clazzId)) { clazz =>
      Clazz.ClazzOrTeamManager(clazz) { _ =>
        env.studentApi.removeStudent(clazz, userId) inject {
          Redirect(routes.Clazz.detail(clazzId))
        }
      }
    }
  }

  def acceptForm(clazzId: String, error: Option[String]) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byIdWithCoach(clazzId, me)) { clazzWithCoach =>
      InviteRepo.find(clazzId, me.id) flatMap { inviteOption =>
        Env.team.api.campusWithTeam(clazzWithCoach.clazz.teamOrDefault) map { team =>
          html.clazz.accept(clazzWithCoach, inviteOption, team, error)
        }
      }
    }
  }

  def accept(clazzId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      clazzOption ← env.api.byId(clazzId)
      inviteOption ← InviteRepo.find(clazzId, me.id)
    } yield (clazzOption |@| inviteOption).tupled) {
      case (clazz, invite) => {
        if (invite.userId == me.id) {
          if (invite.expired) {
            Redirect(routes.Student.acceptForm(clazzId, "邀请已经过期".some)).fuccess
          } else {
            invite.status match {
              case Invite.Status.Invited => env.studentApi.accept(clazz, me) inject Redirect(routes.Clazz.detail(clazzId))
              case Invite.Status.Joined => Redirect(routes.Student.acceptForm(clazzId, "请勿重复操作".some)).fuccess
              case Invite.Status.Refused => Redirect(routes.Student.acceptForm(clazzId, "邀请已经拒绝".some)).fuccess
            }
          }
        } else Clazz.ForbiddenResult
      }
    }
  }

  def refused(clazzId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      clazzOption ← env.api.byId(clazzId)
      inviteOption ← InviteRepo.find(clazzId, me.id)
    } yield (clazzOption |@| inviteOption).tupled) {
      case (clazz, invite) => {
        if (invite.userId == me.id) {
          if (invite.expired) {
            Redirect(routes.Student.acceptForm(clazzId, "邀请已经过期".some)).fuccess
          } else {
            invite.status match {
              case Invite.Status.Invited => env.studentApi.refused(clazzId, me) inject {
                Redirect(routes.Student.acceptForm(clazzId))
              }
              case Invite.Status.Joined => Redirect(routes.Student.acceptForm(clazzId, "学员已经加入".some)).fuccess
              case Invite.Status.Refused => Redirect(routes.Student.acceptForm(clazzId, "邀请已经拒绝".some)).fuccess
            }
          }
        } else Clazz.ForbiddenResult
      }
    }
  }

  def addStuModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      Clazz.ClazzOrTeamManager(clazz) { isTeamManager =>
        for {
          teamOption <- clazz.team.?? { campusId => TeamRepo.byId(Campus.toTeamId(campusId)) }
          campuses <- clazz.team.?? { campusId => Clazz.getCampuses(teamOption, me, isTeamManager) }
          tags <- clazz.team.?? { campusId => TagRepo.findByTeam(Campus.toTeamId(campusId)) }
        } yield {
          Ok(html.clazz.modal.clazz.addStuModal(
            Env.team.forms.member.memberSearch,
            clazz, teamOption, campuses.some, tags.some
          ))
        }
      }
    }
  }

  def addStu(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      Clazz.ClazzOrTeamManager(clazz) { _ =>
        implicit val req = ctx.body
        Form(single("members" -> list(lila.user.DataForm.historicalUsernameField))).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          stus => {
            val userIds = stus.filterNot(u => clazz.studentIds.contains(u))
            if (userIds.isEmpty) {
              Redirect(routes.Clazz.detail(clazz.id)).fuccess
            } else env.studentApi.addStudents(clazz, userIds) inject Redirect(routes.Clazz.detail(clazz.id))
          }
        )
      }
    }
  }

  def loadStudents(clazzId: String) = Auth { implicit ctx => me =>
    if (clazzId == "others") {
      if (me.isCoachOrTeam) {
        {
          for {
            clazzStudents <- Env.clazz.api.mine(me.id).map(_.filter(c => c.isCoach(me.id))).map(_.flatMap(_.studentIds))
            mineStudents <- Env.coach.studentApi.mineStudents(me.some)
            teamStudents <- Env.team.api.mineMembers(me.some)
          } yield (teamStudents ++ mineStudents -- clazzStudents.toSet)
        } flatMap { userIds =>
          for {
            users <- UserRepo.byIds(userIds)
            markMap <- env.api.userMarks(me.id)
          } yield {
            val data =
              JsArray {
                users.map { user =>
                  Json.obj(
                    "userId" -> user.id,
                    "username" -> user.username,
                    "mark" -> {
                      markMap.get(user.id) match {
                        case None => user.realNameOrUsername
                        case Some(mark) => mark | user.realNameOrUsername
                      }
                    },
                    "head" -> user.head
                  )
                }
              }
            Ok(data)
          }
        }
      } else Clazz.ForbiddenResult
    } else {
      OptionFuResult(env.api.byId(clazzId)) { clazz =>
        Clazz.ClazzOrTeamManager(clazz) { _ =>
          env.studentApi.studentsWithUserById(clazzId, me.id).map { stus =>
            val data =
              JsArray {
                stus.map { stu =>
                  Json.obj(
                    "clazzId" -> stu.clazzId,
                    "userId" -> stu.userId,
                    "username" -> stu.user.username,
                    "mark" -> stu.realName,
                    "head" -> stu.user.head
                  )
                }
              }
            Ok(data)
          }.map(_ as JSON)
        }
      }
    }
  }

}
