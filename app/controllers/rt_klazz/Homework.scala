package controllers.rt_klazz

import lila.app._
import lila.api.Context
import lila.clazz.{ HomeworkV2Repo, HomeworkV2Report }
import lila.task.TTaskRepo
import lila.team.TeamRepo
import lila.user.UserRepo
import play.api.libs.json.{ JsArray, Json }
import play.api.mvc.Result
import views._

object Homework extends controllers.LilaController {

  private val env = Env.clazz
  private val api = env.api
  private val forms = env.homeworkV2Form
  private val courseApi = env.courseApi
  private val studentApi = env.studentApi
  private val homeworkApi = env.homeworkV2Api
  private val homeworkReport = env.homeworkV2Report
  private val jsonView = env.jsonView

  def createForm(clazzId: String, courseId: String) = Auth { implicit ctx => me =>
    Clazz.GrandCoachOrTeamManager {
      OptionFuResult(for {
        clazzOption <- api.byId(clazzId)
        courseOption <- courseApi.byId(courseId)
      } yield (clazzOption |@| courseOption).tupled) {
        case (clazz, course) => {
          Clazz.ClazzOrTeamManager(clazz, true, true) { isTeamManager =>
            for {
              prevCourse <- courseApi.findByCourseIndex(clazzId, course.index - 1)
              nextCourse <- courseApi.findByCourseIndex(clazzId, course.index + 1)
              homework <- homeworkApi.findOrCreate(clazz, course, me.id)
              prevHomework <- prevCourse.?? { course => HomeworkV2Repo.find(course.clazz, course.id, course.index) }
              cloneHomework <- homework.homework.cloneFrom.?? { fromId => HomeworkV2Repo.byId(fromId) }
              teamOption <- me.belongTeamId.??(Env.team.api.ownerOrCoach(_, me.id))
              teamJson = Env.team.api.teamJson(teamOption)
              clazzJson <- jsonView.clazzJson(clazz)
              courseJson = jsonView.courseJson(course)
              nextCourseJson = nextCourse.map(course => jsonView.courseJson(course))
              homeworkJson = jsonView.homeworkWithTaskJson(homework)
              prevHomeworkJson = prevHomework.map { homework => jsonView.homeworkJson(homework) }
              cloneHomeworkJson = cloneHomework.map { homework => jsonView.homeworkJson(homework) }
            } yield Ok(html.clazz.homework.form(clazz, course, teamJson, clazzJson, courseJson, nextCourseJson, homeworkJson, prevHomeworkJson, cloneHomeworkJson, isTeamManager))
          }
        }
      }
    }
  }

  def update(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      homeworkOption <- HomeworkV2Repo.byId(id)
      clazzOption <- homeworkOption.??(h => api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).tupled) {
      case (homework, clazz, course) => {
        Clazz.OwnerAndEnable(clazz) {
          if (homework.isPublished) Clazz.ForbiddenResult
          else {
            implicit def req = ctx.body
            forms.create.bindFromRequest.fold(
              jsonFormError,
              data => homeworkApi.update(homework, clazz, course, data, me) inject jsonOkResult
            )
          }
        }
      }
    }
  }

  def updateAndPublish(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      homeworkOption <- HomeworkV2Repo.byId(id)
      clazzOption <- homeworkOption.??(h => api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).tupled) {
      case (homework, clazz, course) => {
        Clazz.OwnerAndEnable(clazz) {
          if (homework.isPublished) Clazz.ForbiddenResult
          else {
            implicit def req = ctx.body
            forms.create.bindFromRequest.fold(
              jsonFormError,
              data => homeworkApi.updateAndPublish(homework, clazz, course, data, me) inject jsonOkResult
            )
          }
        }
      }
    }
  }

  def clone(id: String, clazzId: String, courseId: String) = Auth { implicit ctx => me =>
    Clazz.GrandCoachOrTeamManager {
      OptionFuResult(for {
        sourceHomeworkOption <- homeworkApi.byId(id)
        clazzOption <- api.byId(clazzId)
        courseOption <- courseApi.byId(courseId)
      } yield (sourceHomeworkOption |@| clazzOption |@| courseOption).tupled) {
        case (sourceHomework, clazz, course) => {
          Clazz.ClazzOrTeamManager(clazz, true, true) { isTeamManager =>
            if (!sourceHomework.homework.isPublished) Clazz.ForbiddenResult
            else homeworkApi.cloneBy(sourceHomework, clazz, course, me.id) map { hw =>
              Ok(jsonOkBody ++ Json.obj("id" -> hw.homework.id)) as JSON
            }
          }
        }
      }
    }
  }

  def revoke(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      homeworkOption <- HomeworkV2Repo.byId(id)
      clazzOption <- homeworkOption.??(h => api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).tupled) {
      case (homework, clazz, course) => {
        Clazz.OwnerAndEnable(clazz) {
          if (!homework.isPublished) Clazz.ForbiddenResult
          else {
            homeworkApi.revoke(homework) inject jsonOkResult
          }
        }
      }
    }
  }

  def revokeValid(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      homeworkOption <- HomeworkV2Repo.byId(id)
      clazzOption <- homeworkOption.??(h => api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).tupled) {
      case (homework, clazz, course) => {
        Clazz.OwnerAndEnable(clazz) {
          if (!homework.isPublished) Clazz.ForbiddenResult
          else {
            TTaskRepo.existsByHomework(homework.id) map { userIds =>
              Ok(Json.obj("exists" -> userIds.nonEmpty, "count" -> userIds.size))
            }
          }
        }
      }
    }
  }

  def updateDeadline(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      homeworkOption <- HomeworkV2Repo.byId(id)
      clazzOption <- homeworkOption.??(h => api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).tupled) {
      case (homework, clazz, _) => {
        Clazz.OwnerAndEnable(clazz) {
          if (!homework.isPublished) Clazz.ForbiddenResult
          else {
            implicit def req = ctx.body
            forms.deadlineForm.bindFromRequest.fold(
              jsonFormError,
              deadlineAt => homeworkApi.setDeadline(homework, deadlineAt) inject jsonOkResult
            )
          }
        }
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    (for {
      shwtOption <- homeworkApi.findStudentById(id)
      clazzOption <- shwtOption.??(shwt => api.byId(shwt.homework.clazzId))
      courseOption <- shwtOption.??(shwt => courseApi.byId(shwt.homework.courseId))
    } yield (shwtOption |@| clazzOption |@| courseOption).tupled).flatMap {
      case None => notFoundHomework(ctx)
      case Some((shwt, clazz, course)) => {
        val homework = shwt.homework
        if (course.dateTime.isAfterNow) notStartHomework(course)(ctx)
        else {
          if (homework.isCreator(me.id) || homework.belongTo(me.id)) {
            for {
              team <- shwt.homework.coinTeam.??(TeamRepo.byId)
              coach <- UserRepo.byId(clazz.coach)
              student <- (me.id != homework.studentId).?? { UserRepo.byId(homework.studentId) }
              studentMark <- (me.id != homework.studentId).?? { api.userMark(me.id, homework.studentId) }
            } yield Ok(html.clazz.homework.show(shwt, team, clazz, course, coach, student, studentMark, getBool("showPuzzleFirstComplete")))
          } else Clazz.ForbiddenResult
        }
      }
    }
  }

  def show2(clazzId: String, courseId: String) = Auth { implicit ctx => me =>
    (for {
      clazzOption <- api.byId(clazzId)
      courseOption <- courseApi.byId(courseId)
      hwtOption <- courseOption.?? { course => homeworkApi.findStudentByCourse(course, me.id) }
    } yield (hwtOption |@| clazzOption |@| courseOption).tupled).flatMap {
      case None => Redirect(s"${routes.Clazz.detail(clazzId)}?courseId=$courseId#courses").fuccess
      case Some((hwt, clazz, course)) => {
        if (course.dateTime.isAfterNow) Redirect(s"${routes.Clazz.detail(clazzId)}?courseId=$courseId#courses").fuccess
        else {
          val homework = hwt.homework
          if (homework.isCreator(me.id) || homework.belongTo(me.id)) {
            for {
              team <- hwt.homework.coinTeam.??(TeamRepo.byId)
              coach <- UserRepo.byId(clazz.coach)
              student <- (me.id != homework.studentId).?? { UserRepo.byId(homework.studentId) }
              studentMark <- (me.id != homework.studentId).?? { api.userMark(me.id, homework.studentId) }
            } yield Ok(html.clazz.homework.show(hwt, team, clazz, course, coach, student, studentMark, getBool("showPuzzleFirstComplete")))
          } else Clazz.ForbiddenResult
        }
      }
    }
  }

  def report(id: String) = Auth { implicit ctx => me =>
    Clazz.GrandCoachOrTeamManager {
      OptionFuResult(for {
        hwtOption <- homeworkApi.byId(id)
        clazzOption <- hwtOption.??(hwt => api.byId(hwt.homework.clazzId))
        courseOption <- hwtOption.??(hwt => courseApi.byId(hwt.homework.courseId))
      } yield (hwtOption |@| clazzOption |@| courseOption).tupled) {
        case (hwt, clazz, course) => {
          Clazz.ClazzOrTeamManager(clazz, false) { _ =>
            if (!hwt.homework.isPublished) Clazz.ForbiddenResult
            else {
              val puzzleSort = get("puzzleSort").map(c => lila.clazz.HomeworkV2Report.PuzzleSort(c))
              val completeRateSortType = get("completeRateSortType")
              for {
                team <- hwt.homework.coinTeam.??(TeamRepo.byId)
                hrv <- homeworkReport.byHomeworkView(hwt)
                students <- studentApi.studentsById(clazz.id)
                users <- UserRepo.byIds(hrv.students)
                userMark <- studentApi.userMarks(me.id)
              } yield Ok(html.clazz.homework.report(team, clazz, course, hwt, hrv, puzzleSort, completeRateSortType, students, users, userMark))
            }
          }
        }
      }
    }
  }

  def refreshReport(id: String) = Auth { implicit ctx => me =>
    Clazz.GrandCoachOrTeamManager {
      OptionFuResult(for {
        hwtOption <- homeworkApi.byId(id)
        clazzOption <- hwtOption.??(h => api.byId(h.homework.clazzId))
      } yield (hwtOption |@| clazzOption).tupled) {
        case (hwt, clazz) => {
          Clazz.ClazzOrTeamManager(clazz, false) { _ =>
            homeworkReport.refreshReport(hwt) inject Redirect(routes.Homework.report(id) + "&puzzleSort=" + (get("puzzleSort") | "") + "&completeRateSortType=" + (get("completeRateSortType") | ""))
          }
        }
      }
    }
  }

  def updateCoinModal(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      shwtOption <- homeworkApi.findStudentById(id)
      teamOption <- shwtOption.??(shwt => shwt.homework.coinTeam.??(TeamRepo.byId))
    } yield (shwtOption |@| teamOption).tupled) {
      case (shwt, team) => {
        if (shwt.homework.overDeadline | false) {
          for {
            student <- UserRepo.byId(shwt.studentId)
            studentMark <- api.userMark(me.id, shwt.studentId)
          } yield Ok(html.clazz.homework.report.coinEditModal(forms.coinDiffForm(team.coinSettingOrDefault.singleVal), team, student, studentMark, shwt))
        } else Forbidden(jsonError("Forbidden")).fuccess
      }
    }
  }

  def updateCoin(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(for {
      shwtOption <- homeworkApi.findStudentById(id)
      teamOption <- shwtOption.??(shwt => shwt.homework.coinTeam.??(TeamRepo.byId))
    } yield (shwtOption |@| teamOption).tupled) {
      case (shwt, team) => {
        if (shwt.homework.overDeadline | false) {
          implicit def req = ctx.body
          forms.coinDiffForm(team.coinSettingOrDefault.singleVal).bindFromRequest.fold(
            jsonFormError,
            coinDiff => fuccess(homeworkApi.updateCoin(shwt.homework, coinDiff)) inject jsonOkResult
          )
        } else Forbidden(jsonError("Forbidden")).fuccess
      }
    }
  }

  def rule() = Auth { implicit ctx => me =>
    Ok(html.clazz.homework.rule()).fuccess
  }

  def homeworkJsonApi(id: String, s: String) = Auth { implicit ctx => me =>
    Clazz.GrandCoachOrTeamManager {
      (for {
        hwtOption <- homeworkApi.byId(id)
        clazzOption <- hwtOption.??(h => api.byId(h.homework.clazzId))
      } yield (hwtOption |@| clazzOption).tupled).flatMap {
        case None => fuccess(Ok(
          Json.obj(
            "puzzles" -> JsArray(),
            "replayGames" -> JsArray(),
            "recallGames" -> JsArray(),
            "distinguishGames" -> JsArray()
          )
        ))
        case Some((hwt, clazz)) => {
          Clazz.ClazzOrTeamManager(clazz) { _ =>
            homeworkReport.byHomeworkView(hwt) map { report =>
              val all = report.practice.capsulePuzzles.flatMap(_.report)
              val puzzleSort = HomeworkV2Report.PuzzleSort.apiSort(s)
              val puzzles = puzzleSort match {
                case HomeworkV2Report.PuzzleSort.No => all
                case HomeworkV2Report.PuzzleSort.CompleteRate => all.sortBy(_.report.completeRate.rate)
                case HomeworkV2Report.PuzzleSort.RightRate => all.sortBy(_.report.rightRate.rate)
                case HomeworkV2Report.PuzzleSort.FirstRightRate => all.sortBy(_.report.firstMoveRightRate.rate)
                case _ => all
              }

              Ok(
                Json.obj(
                  "puzzles" -> JsArray(
                    puzzles.map { pwr =>
                      val puzzle = pwr.puzzle
                      Json.obj(
                        "id" -> puzzle.id,
                        "fen" -> puzzle.fen,
                        "color" -> puzzle.color.name,
                        "lastMove" -> puzzle.lastMove,
                        "completeRate" -> pwr.report.completeRate.rate,
                        "rightRate" -> pwr.report.rightRate.rate,
                        "firstMoveRightRate" -> pwr.report.firstMoveRightRate.rate
                      )
                    }
                  ),
                  "replayGames" -> JsArray(
                    report.practice.replayGames.flatMap { rgs =>
                      rgs.report.map { rpr =>
                        val replayGame = rpr.replayGame
                        Json.obj(
                          "name" -> replayGame.name,
                          "fen" -> replayGame.lastFen,
                          "pgn" -> replayGame.toPGN
                        )
                      }
                    }
                  ),
                  "recallGames" -> JsArray(
                    report.practice.recallGames.flatMap { rgs =>
                      rgs.report.map { rpr =>
                        val recallGame = rpr.recallGame
                        Json.obj(
                          "name" -> (recallGame.name | "-"),
                          "fen" -> recallGame.lastFen,
                          "pgn" -> recallGame.pgn
                        )
                      }
                    }
                  ),
                  "distinguishGames" -> JsArray(
                    report.practice.distinguishGames.flatMap { dgs =>
                      dgs.report.map { dgr =>
                        val distinguishGame = dgr.distinguishGame
                        Json.obj(
                          "name" -> (distinguishGame.name | "-"),
                          "fen" -> distinguishGame.lastFen,
                          "pgn" -> distinguishGame.pgn
                        )
                      }
                    }
                  )
                )
              ) as JSON
            }
          }
        }
      }
    }
  }

  private def notStartHomework(course: lila.clazz.Course)(implicit ctx: Context): Fu[Result] = Ok(html.clazz.homework.notStart(course)).fuccess
  private def notFoundHomework(implicit ctx: Context): Fu[Result] = Ok(html.clazz.homework.notFound()).fuccess

}
