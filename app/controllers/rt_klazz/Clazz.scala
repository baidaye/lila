package controllers.rt_klazz

import lila.app._
import lila.api.Context
import lila.clazz.ClazzForm.CreateData
import lila.clazz.{ OlClassMetaData, OlClassStatus, Clazz => ClazzModel }
import lila.contest.ContestRepo
import lila.team.{ Campus, CampusRepo, MemberRepo, MemberWithUser, TeamRepo }
import lila.user.UserRepo
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json._
import play.api.mvc.{ Result, Results }
import views._

object Clazz extends controllers.LilaController {

  private def env = Env.clazz
  private val form = env.form
  private val courseApi = env.courseApi
  private val courseAttendApi = env.courseAttendApi
  private val studentApi = env.studentApi

  def current = Auth { implicit ctx => me =>
    env.api.current(me) map { list =>
      Ok(html.clazz.list.current(list))
    }
  }

  def history = Auth { implicit ctx => me =>
    env.api.history(me) map { list =>
      Ok(html.clazz.list.history(list))
    }
  }

  def createForm(campusId: Option[String]) = Auth { implicit ctx => me =>
    GrandCoachOrTeamManager {
      for {
        teamOption <- getTeam(campusId, me)
        campuses <- getCampuses(teamOption, me, campusId.isDefined)
        coaches <- getCoaches(teamOption, campuses, campusId)
        markMap <- env.api.userMarks(me.id)
      } yield {
        Ok(html.clazz.form.create(
          form.create(me, none, campusId) fill CreateData.default(none),
          campusId,
          teamOption,
          campuses,
          coaches,
          markMap
        ))
      }
    }
  }

  def create(campusId: Option[String]) = AuthBody { implicit ctx => me =>
    GrandCoachOrTeamManager {
      implicit def req = ctx.body
      form.create(me, none, campusId).bindFromRequest.fold(
        failure => {
          for {
            teamOption <- getTeam(campusId, me)
            campuses <- getCampuses(teamOption, me, campusId.isDefined)
            coaches <- getCoaches(teamOption, campuses, campusId)
            markMap <- env.api.userMarks(me.id)
          } yield {
            Ok(html.clazz.form.create(
              failure,
              campusId,
              teamOption,
              campuses,
              coaches,
              markMap
            ))
          }
        },
        data => {
          val clazz = data.toClazz(me, campusId)
          env.api.create(clazz) map { _ =>
            Redirect(routes.Clazz.detail(clazz.id))
          }
        }
      )
    }
  }

  def editForm(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        val campusId = clazz.team
        for {
          teamOption <- getTeam(campusId, me)
          campuses <- getCampuses(teamOption, me, false)
          coaches <- getCoaches(teamOption, campuses, campusId)
          markMap <- env.api.userMarks(me.id)
        } yield {
          Ok(html.clazz.form.update(
            form.create(me, id.some, none) fill CreateData.byClazz(clazz),
            clazz,
            teamOption,
            campuses,
            coaches,
            markMap
          ))
        }
      }
    }
  }

  def update(id: String) = SecureBody(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        if (clazz.editable) {
          implicit def req = ctx.body
          form.create(me, id.some, none).bindFromRequest.fold(
            failure => {
              val campusId = clazz.team
              for {
                teamOption <- getTeam(campusId, me)
                campuses <- getCampuses(teamOption, me, false)
                coaches <- getCoaches(teamOption, campuses, campusId)
                markMap <- env.api.userMarks(me.id)
              } yield {
                Ok(html.clazz.form.update(
                  failure,
                  clazz,
                  teamOption,
                  campuses,
                  coaches,
                  markMap
                ))
              }
            },
            data => {
              val updater = data.withUpdate(clazz)
              env.api.update(clazz, updater) inject Redirect(routes.Clazz.detail(id))
            }
          )
        } else ForbiddenResult
      }
    }
  }

  def campusCoach(campusId: Option[String]) = AuthBody { implicit ctx => me =>
    for {
      teamOption <- getTeam(campusId, me)
      campuses <- getCampuses(teamOption, me, true)
      coaches <- getCoaches(teamOption, campuses, campusId)
      markMap <- env.api.userMarks(me.id)
    } yield {
      Ok(JsArray(
        coaches.map { coach =>
          Json.obj(
            "id" -> coach.userId,
            "name" -> userMark(coach, markMap)
          )
        }
      )) as JSON
    }
  }

  private def userMark(mwu: MemberWithUser, markMap: Map[String, Option[String]]): String = {
    markMap.get(mwu.userId).fold(none[String]) { m => m } | mwu.user.realNameOrUsername
  }

  private def getTeam(campusId: Option[String], me: lila.user.User): Fu[Option[lila.team.Team]] =
    me.belongTeamId match {
      case Some(teamId) => {
        TeamRepo.byId(teamId).flatMap { teamOption =>
          MemberRepo.byId(teamId, me.id) map {
            case None => none[lila.team.Team]
            case Some(member) => teamOption.filter { team => team.enabled && team.certified && member.isManager }
          }
        }
      }
      case None => fuccess(none[lila.team.Team])
    }

  private[controllers] def getCampuses(team: Option[lila.team.Team], me: lila.user.User, isManager: Boolean): Fu[List[Campus]] =
    team match {
      case None => fuccess(List.empty[Campus])
      case Some(t) =>
        if (isManager) {
          if (t.isCreator(me.id)) {
            CampusRepo.byTeam(t.id)
          } else if (me.isTeamCampusAdmin(t.id)) {
            CampusRepo.byAdmin(t.id, me.id)
          } else fuccess(List.empty[Campus])
        } else {
          CampusRepo.byCoach(t.id, me.id)
        }
    }

  private def getCoaches(team: Option[lila.team.Team], campuses: List[Campus], assignCampusId: Option[String]) =
    team.?? { team =>
      val campusCoaches = campuses.find(c => team.id == c.team && assignCampusId.contains(c.id)).map(c => c.coachIgnoreEmpty.toList) | List.empty[String]
      MemberRepo.coachByTeam(team.id).map {
        _.filter { member =>
          campusCoaches.contains(member.userId)
        }
      }
    }

  def settingModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        val form = Form(single("showAttend" -> boolean)).fill(clazz.isShowAttend)
        Ok(html.clazz.modal.clazz.setting(clazz, form)).fuccess
      }
    }
  }

  def setting(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        implicit val req = ctx.body
        Form(single("showAttend" -> boolean)).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          showAttend => {
            env.api.updateSetting(clazz.copy(showAttend = showAttend.some)) inject Redirect(routes.Clazz.detail(id))
          }
        )
      }
    }
  }

  def stop(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        env.api.stop(id) >>- env.courseApi.removeCalendars(id) /* >> env.courseApi.stopByClazz(id) */ inject {
          Redirect(routes.Clazz.current)
        }
      }
    }
  }

  // 已作废
  def delete(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      Owner(clazz) {
        if (clazz.deletable) {
          env.api.delete(clazz) inject {
            Redirect(routes.Clazz.current)
          }
        } else ForbiddenResult
      }
    }
  }

  def removeCalendars(id: String) = Secure(_.Coach) { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      if (ctx.me.??(me => clazz.isCoach(me.id) && !clazz.isDelete)) {
        env.courseApi.removeCalendars(id) inject {
          Redirect(routes.Clazz.history)
        }
      } else ForbiddenResult
    }
  }

  def detail(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byIdWithCoach(id, me)) { clazzWithCoach =>
      ClazzView(clazzWithCoach.clazz) { isTeamManager =>
        for {
          teamOption <- Env.team.api.campusWithTeam(clazzWithCoach.clazz.teamOrDefault)
          studentWithUsers <- studentApi.studentsWithUser(clazzWithCoach.clazz, me)
          //teamMembers <- studentWithUsers.map(_.userId)
          olClasses <- Env.olclass.api.findByClass(id)
          olClassMetaDatas = olClasses.map(oc => OlClassMetaData(courseId = oc.id, status = OlClassStatus(oc.status.id), opened = oc.opened))
          courseRelations <- courseApi.courseRelations(clazzWithCoach.clazz, olClassMetaDatas, me.id)
          allContests <- ContestRepo.byIds(clazzWithCoach.clazz.contests)
          inviteWithUsers <- studentApi.invitesWithUser(clazzWithCoach.clazz, me)
          rc = courseApi.recentlyCourseWithDefault(courseRelations.map(_.course))
          attends <- rc.?? { rc =>
            courseAttendApi.list(clazzWithCoach.clazz, rc.id)
          }
        } yield Ok(html.clazz.detail(
          clazzWithCoach,
          teamOption,
          courseRelations,
          allContests.filter(_.isCourse),
          studentWithUsers,
          inviteWithUsers,
          attends,
          rc,
          isTeamManager,
          get("courseId"),
          get("error")
        ))
      }
    }
  }

  def attends(id: String, courseId: String) = Auth { implicit ctx => me =>
    OptionFuOk(env.api.byId(id)) { clazz =>
      courseAttendApi.list(clazz, courseId) map { ats =>
        JsArray(
          ats.map {
            case (userId, attend) => {
              Json.obj(
                "userId" -> userId,
                "absent" -> attend._1,
                "nb" -> attend._2
              )
            }
          }.toSeq
        )
      }
    }
  }

  def attendSetModal(id: String, courseId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      clazzOption <- env.api.byId(id)
      courseOption <- courseApi.byId(courseId)
    } yield (clazzOption |@| courseOption).tupled) {
      case (clazz, course) => {
        OwnerAndEnable(clazz) {
          for {
            studentsWithUser <- studentApi.studentsWithUser(clazz, me)
            attends <- courseAttendApi.list(clazz, courseId)
          } yield {
            val absents = attends.filter(x => x._2._1 | false).map(_._1).toList
            Ok(html.clazz.detail.attendSetModal(clazz, course, studentsWithUser, absents))
          }
        }
      }
    }
  }

  def attendSet(id: String, courseId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        implicit val req = ctx.body
        Form(single("absents" -> text)).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          absents => {
            courseApi.byId(courseId) flatMap {
              _.?? { course =>
                courseAttendApi.setAbsents(clazz, course, split(absents)) inject jsonOkResult
              }
            }
          }
        )
      }
    }
  }

  def attendStudentModal(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      ClazzOrTeamManager(clazz) { isTeamManager =>
        for {
          course <- courseApi.clazzCourse(id)
          attends <- courseAttendApi.listByUser(id, userId)
          userOption <- UserRepo.byId(userId)
          markOption <- studentApi.userMark(me.id, userId)
        } yield {
          userOption.?? { user =>
            Ok(html.clazz.modal.clazz.attendStudentModal(clazz, course, attends, user, markOption, isTeamManager))
          }
        }
      }
    }
  }

  def attendStudentSet(id: String, userId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      OwnerAndEnable(clazz) {
        implicit val req = ctx.body
        Form(single("courseAttends" -> text)).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          courseAttends => {
            courseApi.clazzCourse(id) flatMap { courses =>
              courseAttendApi.setStudentAbsents(clazz, userId, courses, split(courseAttends)) inject jsonOkResult
            }
          }
        )
      }
    }
  }

  def editCoachModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byIdWithCoach(id, me)) { cwc =>
      TeamManager(cwc.clazz) {
        for {
          teamOption <- getTeam(cwc.clazz.team, me)
          campuses <- getCampuses(teamOption, me, true)
          coaches <- getCoaches(teamOption, campuses, cwc.clazz.team)
          markMap <- env.api.userMarks(me.id)
        } yield Ok(html.clazz.modal.clazz.editCoachModal(env.form.editCoach fill cwc.clazz.coach, cwc, coaches, markMap))
      }
    }
  }

  def editCoach(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      TeamManager(clazz) {
        implicit val req = ctx.body
        env.form.editCoach.bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          newCoach => {
            if (clazz.studentIds.contains(newCoach)) {
              BadRequest(jsonError("教练已经在学员列表内，可移除后再操作")).fuccess
            } else {
              env.api.updateCoach(clazz, newCoach) inject jsonOkResult
            }
          }
        )
      }
    }
  }

  private def split(str: String) = if (str.isEmpty) Nil else str.split(",").toList

  def contestForm(id: String, courseId: String) = Auth { implicit ctx => me =>
    controllers.rt_contest.Contest.Permiss {
      NoLameOrBot {
        OptionFuResult(for {
          clazzOption <- env.api.byId(id)
          courseOption <- courseApi.byId(courseId)
        } yield (clazzOption |@| courseOption).tupled) {
          case (clazz, course) => {
            TeamRepo.byCampusId(clazz.teamOrDefault).map { team =>
              Ok(html.clazz.contest(clazz, course, team, Env.contest.forms.clazzContestOf(clazz.name, course.index)))
            }
          }
        }
      }
    }
  }

  def contestApply(id: String, courseId: String) = AuthBody { implicit ctx => me =>
    controllers.rt_contest.Contest.Permiss {
      NoLameOrBot {
        OptionFuResult(for {
          clazzOption <- env.api.byId(id)
          courseOption <- courseApi.byId(courseId)
        } yield (clazzOption |@| courseOption).tupled) {
          case (clazz, course) => {
            TeamRepo.byCampusId(clazz.teamOrDefault).flatMap { team =>
              implicit val req = ctx.body
              Env.contest.forms.clazzContest.bindFromRequest.fold(
                err => BadRequest(html.clazz.contest(clazz, course, team, err)).fuccess,
                data => controllers.rt_contest.Contest.ClazzCreateLimitPerUser(me.id, cost = 1) {
                  Env.contest.contestApi.createByClazz(clazz.id, course.id, me, clazz.studentIds, data).flatMap { contest =>
                    env.api.setContest(clazz, contest.id) inject Redirect(s"${controllers.rt_contest.routes.Contest.show(contest.id)}#enter")
                  }
                }(rateLimited)
              )
            }
          }
        }
      }
    }
  }

  def coursesApi(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      ClazzOrTeamManager(clazz) { isTeamManager =>
        for {
          courses <- courseApi.clazzCourse(id)
          recentlyCourse = courseApi.recentlyCourseWithDefault(courses)
        } yield {
          Ok(
            JsArray(
              courses.filter(_.homework).map { course =>
                Json.obj(
                  "id" -> course.id,
                  "index" -> course.index,
                  "dateTime" -> course.courseFormatTime
                )
              }
            )
          ) as JSON
        }
      }
    }
  }

  def allCoursesApi(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.byId(id)) { clazz =>
      ClazzOrTeamManager(clazz) { _ =>
        courseApi.clazzCourse(id) map { courses =>
          Ok(
            JsArray(
              courses.map { course =>
                Json.obj(
                  "id" -> course.id,
                  "index" -> course.index,
                  "date" -> course.dateTimeStr,
                  "dateTime" -> course.courseFormatTime,
                  "weekFormat" -> course.weekFormat,
                  "homework" -> course.homework
                )
              }
            )
          ) as JSON
        }
      }
    }
  }

  def allClazzesApi() = Auth { implicit ctx => me =>
    env.api.mine(me.id) map { clazzes =>
      Ok(
        JsArray(
          clazzes.map { clazz =>
            Json.obj(
              "id" -> clazz.id,
              "name" -> clazz.name
            )
          }
        )
      ) as JSON
    }
  }

  implicit val rateLimited = ornicar.scalalib.Zero.instance[Fu[Result]] {
    fuccess(Results.TooManyRequest("Too many requests, try again tomorrow."))
  }

  private[controllers] def TeamManager(clazz: ClazzModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (clazz.isDelete || clazz.stopped) ForbiddenResult
    else {
      ctx.me match {
        case None => ForbiddenResult
        case Some(me) => {
          clazz.team match {
            case Some(campusId) => TeamRepo.byCampusId(campusId).flatMap {
              case Some(team) => {
                if (team.isCreator(me.id) || me.isCampusAdmin(campusId)) f
                else ForbiddenResult
              }
              case None => ForbiddenResult
            }
            case None => ForbiddenResult
          }
        }
      }
    }

  private[controllers] def ClazzView(clazz: ClazzModel)(f: Boolean => Fu[Result])(implicit ctx: Context): Fu[Result] =
    ctx.me match {
      case None => ForbiddenResult
      case Some(me) => {
        clazz.team match {
          case Some(campusId) => TeamRepo.byCampusId(campusId).flatMap {
            case None => {
              if (clazz.belongTo(me.id)) f(false)
              else ForbiddenResult
            }
            case Some(team) => {
              if (team.isCreator(me.id) || me.isCampusAdmin(campusId)) f(true)
              else {
                if (clazz.belongTo(me.id)) f(false)
                else ForbiddenResult
              }
            }
          }
          case None => {
            if (clazz.belongTo(me.id)) f(false)
            else ForbiddenResult
          }
        }
      }
    }

  private[controllers] def ClazzOrTeamManager(clazz: ClazzModel, enable: Boolean = true, ignoreStopped: Boolean = false)(f: Boolean => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (enable && (clazz.isDelete || (!ignoreStopped && clazz.stopped))) ForbiddenResult
    else {
      ctx.me match {
        case None => ForbiddenResult
        case Some(me) => {
          clazz.team match {
            case Some(campusId) => TeamRepo.byCampusId(campusId).flatMap {
              case None => {
                if (clazz.isCoach(me.id)) f(false)
                else ForbiddenResult
              }
              case Some(team) => {
                if (team.isCreator(me.id) || me.isCampusAdmin(campusId)) f(true)
                else {
                  if (clazz.isCoach(me.id)) f(false)
                  else ForbiddenResult
                }
              }
            }
            case None => {
              if (clazz.isCoach(me.id)) f(false)
              else ForbiddenResult
            }
          }
        }
      }
    }

  private[controllers] def ClazzMemberOrTeamManager(clazz: ClazzModel, enable: Boolean = true)(f: Boolean => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (enable && clazz.isDelete) ForbiddenResult
    else {
      ctx.me match {
        case None => ForbiddenResult
        case Some(me) => {
          clazz.team match {
            case Some(campusId) => TeamRepo.byCampusId(campusId).flatMap {
              case None => {
                if (clazz.belongTo(me.id)) f(false)
                else ForbiddenResult
              }
              case Some(team) => {
                if (team.isCreator(me.id) || me.isCampusAdmin(campusId)) f(true)
                else {
                  if (clazz.belongTo(me.id)) f(false)
                  else ForbiddenResult
                }
              }
            }
            case None => {
              if (clazz.belongTo(me.id)) f(false)
              else ForbiddenResult
            }
          }
        }
      }
    }

  private[controllers] def Owner(clazz: ClazzModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => clazz.isCoach(me.id))) f
    else ForbiddenResult

  private[controllers] def OwnerAndEnable(clazz: ClazzModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => clazz.isCoach(me.id) && (!clazz.isDelete && !clazz.stopped))) f
    else ForbiddenResult

  private[controllers] def GrandCoachOrTeamManager[A](f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (isGranted(_.Coach) || isGranted(_.Team) || isGranted(_.TeamCampus)) f
    else ForbiddenResult

  private[controllers] def ForbiddenResult(implicit ctx: Context) = Forbidden(views.html.site.message.authFailed).fuccess

  private def teamList(me: lila.user.User): Fu[List[(String, String)]] =
    Env.team.api.mine(me).map(_.filter(t => t.team.enabled && t.team.certified && t.member.isManager).map(t => t.team.id -> t.team.name))

}
