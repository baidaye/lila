package controllers

import lila.app._

object CoordTrain extends LilaController {

  private def env = Env.coordTrain

  def home = Open { implicit ctx =>
    val mode = get("mode")
    val orient = getInt("orient")
    val coordShow = getInt("coordShow")
    val limitTime = getInt("limitTime")
    val auto = get("auto").??(_.toBoolean)

    val pref = ctx.pref.copy(
      coordTrain = lila.pref.CoordTrain(
        mode = mode | ctx.pref.coordTrain.mode,
        coord = coordShow | ctx.pref.coordTrain.coord,
        color = orient | ctx.pref.coordTrain.color,
        limit = limitTime | ctx.pref.coordTrain.limit
      )
    )

    val scoreMode = lila.coordTrain.Score.Mode(pref.coordTrain.mode)
    ctx.me.fold(fuccess(lila.coordTrain.Score.empty("-", scoreMode))) { me =>
      env.scoreApi.getScore(me.id, scoreMode)
    } map { scores =>
      Ok(views.html.coordTrain(
        env.jsonView.scores(scores),
        env.jsonView.pref(pref),
        auto,
        fr = get("fr")
      ))
    }
  }

  def getScore(mode: String) = Auth { implicit ctx => me =>
    val m = lila.coordTrain.Score.Mode(mode)
    env.scoreApi.getScore(me.id, m).map { scores =>
      Ok(env.jsonView.scores(scores)) as JSON
    }
  }

  def start = AuthBody { implicit ctx => me =>
    implicit val body = ctx.body
    env.forms.start.bindFromRequest.fold(
      err => BadRequest(errorsAsJson(err)).fuccess,
      data => {
        val pref = ctx.pref.copy(
          coordTrain = lila.pref.CoordTrain(
            mode = data.mode,
            coord = data.coord,
            color = data.color,
            limit = data.limit
          )
        )
        Env.pref.api.setPref(pref) inject jsonOkResult
      }
    )
  }

  def finish = AuthBody { implicit ctx => me =>
    implicit val body = ctx.body
    env.forms.score.bindFromRequest.fold(
      err => BadRequest(errorsAsJson(err)).fuccess,
      data => data.isLimit.?? { env.scoreApi.addScore(me.id, data.realMode, data.isWhite, data.score) }.flatMap { _ =>
        env.scoreApi.publish(me.id, data)
        env.scoreApi.getScore(me.id, data.realMode).map { scores =>
          Ok(env.jsonView.scores(scores)) as JSON
        }
      }
    )
  }

  def fetchMove(color: String) = Open { implicit ctx =>
    env.moveApi.fetchNext(color).map { m =>
      Ok(env.jsonView.move(m)) as JSON
    }
  }

  def fetchCombat(color: String, prevId: Option[Int]) = Open { implicit ctx =>
    env.combatApi.fetchNext(color, prevId).map { c =>
      Ok(env.jsonView.combat(c)) as JSON
    }
  }

}
