package controllers

import lila.app._
import play.api.libs.json._
import views._

object Message extends LilaController {

  private val env = Env.message

  def home = Auth { implicit ctx => me =>
    inboxJson(me) map { json =>
      Ok(views.html.message.home(json))
    }
  }

  def convo(username: String, before: Option[Long] = None) = Auth { implicit ctx => me =>
    env.api.convoWith(me, username, before).flatMap {
      case None => {
        negotiate(
          html = Redirect(routes.Message.home).fuccess,
          api = _ => notFoundJson()
        )
      }
      case Some(c) => {
        def newJson = inboxJson(me).map { _ + ("convo" -> env.jsonView.convo(c)) }
        negotiate(
          html = newJson map { json =>
            Ok(views.html.message.home(json))
          },
          api = _ => newJson map { Ok(_) }
        )
      }
    }
  }

  def convoDelete(username: String) = Auth { implicit ctx => me =>
    env.api.delete(me, username) >> inboxJson(me) map { Ok(_) }
  }

  def search(q: String) = Auth { ctx => me =>
    env.search(me, q.trim) flatMap { res =>
      env.jsonView.searchResult(me)(res) map { Ok(_) }
    }
  }

  def websocket(apiVersion: Int) = SocketOption[JsValue] { implicit ctx =>
    getSocketSri("sri") ?? { sri =>
      env.socketHandler(sri, user = ctx.me, apiVersion) map some
    }
  }

  private def inboxJson(me: lila.user.User) =
    env.api.threadsOf(me) flatMap env.jsonView.threads(me) map { threads =>
      Json.obj(
        "me" -> lila.common.LightUser.lightUserWrites.writes(me.light).add("kid" -> me.kid),
        "contacts" -> threads
      )
    }

}
