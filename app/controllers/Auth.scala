package controllers

import ornicar.scalalib.Zero
import play.api.data.{ Form, FormError }
import play.api.libs.json._
import play.api.mvc._
import lila.api.{ BodyContext, Context }
import lila.app._
import lila.common.{ EmailAddress, HTTPRequest, LilaCookie }
import lila.security.{ EmailAddressValidator, FingerPrint }
import lila.user.{ PasswordHasher, UserRepo, User => UserModel }
import UserModel.ClearPassword
import org.joda.time.DateTime
import views._

object Auth extends LilaController {

  private def env = Env.security
  private def api = env.api
  private def forms = env.forms
  private def emailRegisterEnabled = env.emailRegisterSetting.get

  private def authLog(user: String, msg: String) =
    lila.log("auth").info(s"$user $msg")

  def signup = Open { implicit ctx =>
    val referrer = get("referrer").filter(goodReferrer).filterNot(_ contains "/login").filterNot(_ contains "/signup")
    forms.signup.websiteWithCaptcha.map {
      case (form, captcha) => Ok(html.auth.signup(form, captcha)) withCookies {
        referrer.fold(LilaCookie.newSession(ctx.req)) { r => LilaCookie.session(Env.security.api.AccessUri, r) }
      }
    }
  }

  def signupPost = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    Firewall {
      forms.signup.website.bindFromRequest.fold(
        err => {
          err("username").value.foreach(u => authLog(u, s"Signup fail: ${Json stringify errorsAsJson(err)}"))
          env.forms.anyCaptcha map { captcha =>
            BadRequest(html.auth.signup(err, captcha))
          }
        },
        data => HasherRateLimit(data.username, ctx.req) { _ =>
          authLog(data.username, s"fp: ${data.fingerPrint} req:${ctx.req}")
          val passwordHash = Env.user.authenticator passEnc ClearPassword(data.password)
          UserRepo.create(data.username, passwordHash, ctx.blind, none, data.level)
            .flatten(s"No user could be created for ${data.username}")
            .flatMap { user =>
              api.saveSignup(user.id, ctx.mobileApiVersion, data.fingerPrint).map { sessionId =>
                authenticateCookie(sessionId, false) {
                  Redirect(routes.Auth.signupConfirm)
                }
              }
            }
        }
      )
    }
  }

  def signupConfirm = Open { implicit ctx =>
    ctx.me match {
      case Some(me) => {
        if (me.confirmed) Redirect(routes.User.show(me.username)).fuccess
        else Ok(html.auth.signupConfirm(me, forms.signup.cellphoneConfirm(me), forms.signup.emailConfirm(me), emailRegisterEnabled)).fuccess
      }
      case None => {
        api.signupUser(ctx.req).map {
          case None => Redirect(routes.Auth.login)
          case Some(u) => Ok(html.auth.signupConfirm(u, forms.signup.cellphoneConfirm(u), forms.signup.emailConfirm(u), emailRegisterEnabled))
        }
      }
    }
  }

  def cellphoneConfirm = OpenBody { implicit ctx =>
    ctx.me match {
      case Some(me) => {
        if (me.confirmed) Redirect(routes.User.show(me.username)).fuccess
        else doCellphoneConfirm(me)
      }
      case None => {
        api.signupUser(ctx.req).flatMap {
          case None => Redirect(routes.Auth.login).fuccess
          case Some(u) => doCellphoneConfirm(u)
        }
      }
    }
  }

  private def doCellphoneConfirm(user: lila.user.User)(implicit ctx: BodyContext[_]) = {
    implicit val req = ctx.body
    forms.signup.cellphoneConfirm(user).bindFromRequest.fold(
      err => Ok(html.auth.signupConfirm(user, err, forms.signup.emailConfirm(user), emailRegisterEnabled, "cellphone".some)).fuccess,
      data => {
        val cellphone = data.realCellphone
        UserRepo.setCellphoneConfirmed(user.id, cellphone = cellphone).flatMap { _ =>
          val newUser = user.copy(cpConfirmed = true, cellphone = cellphone.some)
          garbageCollect(newUser)
          redirectNewUser(newUser)
        }
      }
    )
  }

  def emailConfirm = OpenBody { implicit ctx =>
    ctx.me match {
      case Some(me) => {
        if (me.confirmed) Redirect(routes.User.show(me.username)).fuccess
        else doEmailConfirm(me)
      }
      case None => {
        api.signupUser(ctx.req).flatMap {
          case None => Redirect(routes.Auth.login).fuccess
          case Some(u) => doEmailConfirm(u)
        }
      }
    }
  }

  private def doEmailConfirm(user: lila.user.User)(implicit ctx: BodyContext[_]) = {
    implicit val req = ctx.body
    forms.signup.emailConfirm(user).bindFromRequest.fold(
      err => Ok(html.auth.signupConfirm(user, forms.signup.cellphoneConfirm(user), err, emailRegisterEnabled, "email".some)).fuccess,
      data => {
        val email = EmailAddress(data)
        val userEmail = lila.security.EmailConfirm.UserEmail(user.username, email)
        EmailConfirmRateLimit(userEmail, ctx.req) {
          UserRepo.setEmail(user.id, email) flatMap { _ =>
            env.emailConfirm.send(user, email) inject {
              Redirect(routes.Auth.checkYourEmail)
            }
          }
        }
      }
    )
  }

  def checkYourEmail = Open { implicit ctx =>
    Ok(html.auth.checkYourEmail()).fuccess
  }

  def emailConfirmHelp = OpenBody { implicit ctx =>
    import lila.security.EmailConfirm.Help._
    ctx.me match {
      case Some(me) => Redirect(routes.User.show(me.username)).fuccess
      case None if get("username").isEmpty =>
        Ok(html.auth.emailConfirmHelp(helpForm, none)).fuccess
      case None =>
        implicit val req = ctx.body
        helpForm.bindFromRequest.fold(
          err => BadRequest(html.auth.emailConfirmHelp(err, none)).fuccess,
          username => getStatus(username) map { status =>
            Ok(html.auth.emailConfirmHelp(helpForm fill username, status.some))
          }
        )
    }
  }

  def signupConfirmEmail(token: String) = Open { implicit ctx =>
    import lila.security.EmailConfirm.Result
    Env.security.emailConfirm.confirm(token) flatMap {
      case Result.NotFound => notFound
      case Result.AlreadyConfirmed(user) if ctx.is(user) =>
        Redirect(routes.User.show(user.username)).fuccess
      case Result.AlreadyConfirmed(_) =>
        Redirect(routes.Auth.login).fuccess
      case Result.JustConfirmed(user) => {
        garbageCollect(user)
        redirectNewUser(user)
      }
    }
  }

  def login = Open { implicit ctx =>
    val referrer = get("referrer").filter(goodReferrer)
    referrer.filterNot(_ contains "/login") ifTrue ctx.isAuth match {
      case Some(url) => Redirect(url).fuccess // redirect immediately if already logged in
      case None => Ok(html.auth.login(forms.login.usernameLogin, forms.login.cellphoneLogin, referrer)).fuccess
    }
  }

  def logout = Open { implicit ctx =>
    val currentSessionId = ~Env.security.api.reqSessionId(ctx.req)
    lila.security.Store.delete(currentSessionId) >>
      Env.push.webSubscriptionApi.unsubscribeBySession(currentSessionId) >>
      Redirect(routes.Auth.login).withCookies(LilaCookie.newSession).fuccess
  }

  private val is2fa = Set("MissingTotpToken", "InvalidTotpToken")

  def authenticateByUsername = OpenBody { implicit ctx =>
    def redirectTo(url: String) = if (HTTPRequest isXhr ctx.req) Ok(s"ok:$url") else Redirect(url)
    Firewall({
      implicit val req = ctx.body
      val referrer = get("referrer")
      forms.login.usernameOrEmailForm.bindFromRequest.fold(
        _ => Unauthorized(html.auth.login(forms.login.usernameLogin, forms.login.cellphoneLogin, referrer)).fuccess,
        usernameOrEmail => HasherRateLimit(usernameOrEmail, ctx.req) { chargeIpLimiter =>
          api.loadLoginForm(usernameOrEmail) flatMap { loginForm =>
            loginForm.bindFromRequest.fold(
              err => {
                chargeIpLimiter(1)
                fuccess {
                  err.errors match {
                    case List(FormError("", List(err), _)) if is2fa(err) => Ok(err)
                    case _ => Unauthorized(html.auth.login(err, forms.login.cellphoneLogin, referrer))
                  }
                }
              },
              result => result.toOption match {
                case None => InternalServerError("Authentication error").fuccess
                case Some(u) =>
                  garbageCollect(u)
                  authenticateUser(u, Some(redirectTo))
              }
            )
          }
        }
      )
    }, redirectTo("/").fuccess)
  }

  def authenticateByCellphone = OpenBody { implicit ctx =>
    def redirectTo(url: String) = if (HTTPRequest isXhr ctx.req) Ok(s"ok:$url") else Redirect(url)
    Firewall({
      implicit val req = ctx.body
      val referrer = get("referrer")
      forms.login.cellphoneLogin.bindFromRequest.fold(
        err => Ok(html.auth.login(forms.login.usernameLogin, err, referrer, "cellphone".some)).fuccess,
        data => {
          val cellphone = data.realCellphone
          UserRepo.byCellphone(cellphone).flatMap {
            case None => Unauthorized(html.auth.login(forms.login.usernameLogin, forms.login.cellphoneLogin.fill(data), referrer, "cellphone".some)).fuccess
            case Some(u) => {
              garbageCollect(u)
              authenticateUser(u, Some(redirectTo))
            }
          }
        }
      )
    }, redirectTo("/").fuccess)
  }

  def authenticateUser(u: UserModel, result: Option[String => Result] = None)(implicit ctx: Context): Fu[Result] = {
    if (u.disabled || u.ipBan) fuccess(Redirect(routes.Home.home))
    else api.saveAuthentication(u.id, ctx.mobileApiVersion) flatMap { sessionId =>
      fuccess {
        val redirectTo = get("referrer").filter(goodReferrer) orElse ctxReq.session.get(api.AccessUri) getOrElse routes.Home.home.url
        result.fold(Redirect(redirectTo))(_(redirectTo))
      } map authenticateCookie(sessionId)
    } recoverWith authRecovery
  }

  private def redirectNewUser(user: UserModel)(implicit ctx: Context) = {
    api.saveAuthentication(user.id, ctx.mobileApiVersion) flatMap { sessionId =>
      api.signupConfirmed(user.id)
      fuccess {
        val redirectTo = get("referrer").filter(goodReferrer) orElse ctxReq.session.get(api.AccessUri) getOrElse s"${routes.User.show(user.username)}"
        Redirect(redirectTo)
      } map { authenticateCookie(sessionId) }
    } recoverWith authRecovery
  }

  private def authRecovery(implicit ctx: Context): PartialFunction[Throwable, Fu[Result]] = {
    case lila.security.SecurityApi.MustConfirm(userId) => api.signupId(userId) map { sessionIdOption =>
      sessionIdOption.fold { Redirect(routes.Auth.login) } { sessionId =>
        authenticateCookie(sessionId, false) {
          Redirect(routes.Auth.signupConfirm)
        }
      }
    }
  }

  // do not allow redirects to external sites,
  // nor redirect back to /mobile (which is shown after logout)
  private def goodReferrer(referrer: String): Boolean = {
    val refRegex = """[\w@/-=?]++""".r
    referrer.nonEmpty &&
      referrer.stripPrefix("/") != "mobile" && {
        (!referrer.contains("//") && refRegex.matches(referrer)) ||
          referrer.startsWith(Env.oAuth.baseUrl)
      }
  }

  private def garbageCollect(user: UserModel)(implicit ctx: Context) =
    Env.security.garbageCollector.delay(user, ctx.req)

  private def authenticateCookie(sessionId: String, cleanAccessUri: Boolean = true)(result: Result)(implicit req: RequestHeader) =
    result.withCookies(
      LilaCookie.withSession {
        _ + (api.sessionIdKey -> sessionId) - cleanAccessUri.?? { api.AccessUri }
      }
    )

  def setFingerPrint(fp: String, ms: Int) = Auth { ctx => me =>
    api.setFingerPrint(ctx.req, FingerPrint(fp)) flatMap {
      _ ?? { hash =>
        !me.lame ?? (for {
          otherIds <- api.recentUserIdsByFingerHash(hash).map(_.filter(me.id!=))
          autoReport <- (otherIds.size >= 2) ?? UserRepo.countEngines(otherIds).flatMap {
            case nb if nb >= 2 && nb >= otherIds.size / 2 => Env.report.api.autoCheatPrintReport(me.id)
            case _ => funit
          }
        } yield ())
      }
    } inject NoContent
  }

  def passwordReset = Open { implicit ctx =>
    for {
      emailWithCaptcha <- forms.passwordReset.passwordResetByEmailWithCaptcha
      cellphoneWithCaptcha <- forms.passwordReset.passwordResetByCellphoneWithCaptcha
    } yield Ok(html.auth.bits.passwordReset(cellphoneWithCaptcha, emailWithCaptcha))
  }

  def passwordResetByEmailApply = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    forms.passwordReset.passwordResetByEmail.bindFromRequest.fold(
      err => {
        for {
          emailCaptcha <- forms.anyCaptcha
          cellphoneWithCaptcha <- forms.passwordReset.passwordResetByCellphoneWithCaptcha
        } yield Ok(html.auth.bits.passwordReset(cellphoneWithCaptcha, (err, emailCaptcha), "email".some, false.some))
      },
      data => {
        UserRepo.enabledWithEmail(data.realEmail.normalize) flatMap {
          case Some((user, storedEmail)) => {
            lila.mon.user.auth.passwordResetRequest("success")()
            Env.security.passwordReset.sendEmail(user, storedEmail) inject Redirect(routes.Auth.passwordResetByEmailSent(storedEmail.conceal))
          }
          case _ => {
            lila.mon.user.auth.passwordResetRequest("no_email")()
            for {
              emailWithCaptcha <- forms.passwordReset.passwordResetByEmailWithCaptcha
              cellphoneWithCaptcha <- forms.passwordReset.passwordResetByCellphoneWithCaptcha
            } yield BadRequest(html.auth.bits.passwordReset(cellphoneWithCaptcha, emailWithCaptcha, "email".some, false.some))
          }
        }
      }
    )
  }

  def passwordResetByCellphoneApply = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    forms.passwordReset.passwordResetByCellphone.bindFromRequest.fold(
      err => {
        for {
          emailWithCaptcha <- forms.passwordReset.passwordResetByEmailWithCaptcha
          cellphoneCaptcha <- forms.anyCaptcha
        } yield Ok(html.auth.bits.passwordReset((err, cellphoneCaptcha), emailWithCaptcha, "cellphone".some, false.some))
      },
      data => {
        val cellphone = data.realCellphone
        UserRepo.byCellphone(cellphone).flatMap {
          case None => {
            for {
              emailWithCaptcha <- forms.passwordReset.passwordResetByEmailWithCaptcha
              cellphoneWithCaptcha <- forms.passwordReset.passwordResetByCellphoneWithCaptcha
            } yield BadRequest(html.auth.bits.passwordReset(cellphoneWithCaptcha, emailWithCaptcha, "cellphone".some, false.some))
          }
          case Some(u) => Env.security.passwordReset.generateToken(u) map { token =>
            Redirect(routes.Auth.passwordResetConfirm(token))
          }
        }
      }
    )
  }

  def passwordResetByEmailSent(email: String) = Open { implicit ctx =>
    fuccess {
      Ok(html.auth.bits.passwordResetByEmailSent(email))
    }
  }

  def passwordResetConfirm(token: String) = Open { implicit ctx =>
    Env.security.passwordReset confirm token flatMap {
      case None => {
        lila.mon.user.auth.passwordResetConfirm("token_fail")()
        notFound
      }
      case Some(user) => {
        authLog(user.username, "Reset password")
        lila.mon.user.auth.passwordResetConfirm("token_ok")()
        fuccess(html.auth.bits.passwordResetConfirm(user, token, forms.passwordReset.passwdReset, none))
      }
    }
  }

  def passwordResetConfirmApply(token: String) = OpenBody { implicit ctx =>
    Env.security.passwordReset confirm token flatMap {
      case None => {
        lila.mon.user.auth.passwordResetConfirm("token_post_fail")()
        notFound
      }
      case Some(user) =>
        implicit val req = ctx.body
        FormFuResult(forms.passwordReset.passwdReset) { err =>
          fuccess(html.auth.bits.passwordResetConfirm(user, token, err, false.some))
        } { data =>
          HasherRateLimit(user.username, ctx.req) { _ =>
            Env.user.authenticator.setPassword(user.id, ClearPassword(data.newPasswd1)) >>
              env.store.disconnect(user.id) >>
              Env.push.webSubscriptionApi.unsubscribeByUser(user) >>
              authenticateUser(user) >>-
              lila.mon.user.auth.passwordResetConfirm("success")()
          }
        }
    }
  }

  private implicit val limitedDefault = Zero.instance[Result](TooManyRequest("请求过于频繁，请稍后再试。"))

  private[controllers] def HasherRateLimit =
    PasswordHasher.rateLimit[Result](enforce = Env.api.Net.RateLimit) _

  private[controllers] def EmailConfirmRateLimit = lila.security.EmailConfirm.rateLimit[Result] _
}
