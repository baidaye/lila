package controllers

import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc.{ Result, Results }
import scala.concurrent.duration._
import chess.format.FEN
import lila.api.{ BodyContext, Context }
import lila.app._
import lila.common.{ HTTPRequest, IpAddress, LilaCookie }
import lila.game.{ AnonCookie, GameRepo, Pov }
import lila.opening.OpeningDBRepo
import lila.setup.Processor.HookResult
import lila.setup.{ AiConfig, FriendConfig, ValidFen, ValidPgn }
import lila.socket.Socket.Sri
import lila.user.UserRepo
import views._

object Setup extends LilaController with TheftPrevention {

  private def env = Env.setup
  private def jsonView = env.jsonView
  private def member = Env.member.memberActiveRecordApi

  private[controllers] val PostRateLimit = new lila.memo.RateLimit[IpAddress](5, 1 minute,
    name = "setup post",
    key = "setup_post",
    enforce = Env.api.Net.RateLimit)

  def aiForm = Open { implicit ctx =>
    if (HTTPRequest isXhr ctx.req) {
      ctx.me match {
        case None => Redirect(routes.Auth.login).fuccess
        case Some(user) => {
          member.isAiPlayAccept(user) flatMap { accept =>
            if (accept) {
              env.forms aiFilled (
                get("fen").map(FEN),
                get("pgn"),
                getInt("limit"),
                getInt("increment"),
                getInt("level"),
                get("openingdbId"),
                getBoolOpt("openingdbExcludeWhiteBlunder"),
                getBoolOpt("openingdbExcludeBlackBlunder"),
                getBoolOpt("openingdbExcludeWhiteJscx"),
                getBoolOpt("openingdbExcludeBlackJscx"),
                getBoolOpt("openingdbCanOff")
              ) flatMap { form =>
                  val validFen = form("fen").value flatMap ValidFen(getBool("strict"))
                  val validPgn = form("pgn").value.flatMap(p => ValidPgn.of(p).orNone)
                  for {
                    mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
                    mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
                    mineList <- OpeningDBRepo.mineList(user.id)
                    memberList <- user.isMemberOrCoachOrTeam.?? { OpeningDBRepo.memberList(user.id, mineCoaches, mineTeamOwners) }
                    visibleList <- user.isMemberOrCoachOrTeam.?? { OpeningDBRepo.visibleList(user.id, mineCoaches, mineTeamOwners) }
                    systemList <- Env.opening.api.systemBuyList(user)
                  } yield {
                    html.setup.forms.ai(
                      form,
                      Env.fishnet.aiPerfApi.intRatings,
                      validFen,
                      validPgn,
                      mineList,
                      memberList,
                      visibleList,
                      systemList
                    )
                  }
                }
            } else fuccess(NotAcceptable("每日试用次数超过上限"))
          }
        }
      }
    } else fuccess {
      Redirect(routes.Lobby.home + "#ai")
    }
  }

  def ai = process(env.forms.ai) { config => implicit ctx =>
    env.processor ai config
  }

  def puzzleAi(puzzleId: String, color: String, fen: String) = Auth { implicit ctx => me =>
    member.isAiPlayAccept(me) flatMap { accept =>
      if (accept) {
        val config = AiConfig.puzzleAi(puzzleId, color, fen, me)
        env.processor.ai(config) map { pov =>
          redirectPov(pov)
        }
      } else Redirect(routes.Lobby.home + "#notAccept").fuccess
    }
  }

  def customAi(
    limit: Int,
    increment: Int,
    aiLevel: Int,
    fen: Option[String],
    pgn: Option[String],
    color: Option[String],
    canTakeback: Option[Boolean],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean]
  ) = Auth { implicit ctx => me =>
    val config = AiConfig.customAi(limit, increment, aiLevel, fen, pgn, color,
      openingdbId, openingdbExcludeWhiteBlunder, openingdbExcludeBlackBlunder, openingdbExcludeWhiteJscx, openingdbExcludeBlackJscx, openingdbCanOff)
    env.processor.ai(config, canTakeback) map { pov =>
      redirectPov(pov)
    }
  }

  def customFriend(limit: Int, increment: Int, fen: Option[String], pgn: Option[String], color: Option[String], rated: Option[Boolean], canTakeback: Option[Boolean]) = Auth { implicit ctx => me =>
    val config = FriendConfig.customFriend(limit, increment, fen, pgn, color, rated)
    import lila.challenge.Challenge._
    val challenge = lila.challenge.Challenge.make(
      variant = config.variant,
      initialFen = if (config.variant.fromPgn) None else config.fen,
      initialPgn = config.pgn map { p =>
        ValidPgn.toPgnSetup(p) err s"can not parse pgn $p"
      },
      timeControl = config.makeClock map { c =>
        TimeControl.Clock(c)
      } orElse config.makeDaysPerTurn.map {
        TimeControl.Correspondence.apply
      } getOrElse TimeControl.Unlimited,
      mode = config.mode,
      appt = config.appt,
      apptStartsAt = config.apptStartsAt,
      apptMessage = config.apptMessage,
      color = config.color.name,
      challenger = Right(me),
      destUser = none,
      rematchOf = none,
      canTakeback = canTakeback
    )
    env.processor.saveFriendConfig(config) >>
      (Env.challenge.api create challenge) flatMap {
        case true => negotiate(
          html = fuccess(Redirect(routes.Round.watcher(challenge.id, "white"))),
          api = _ => Challenge showChallenge challenge
        )
        case false => negotiate(
          html = fuccess(Redirect(routes.Lobby.home)),
          api = _ => fuccess(BadRequest(jsonError("Challenge not created")))
        )
      }
  }

  def friendForm(userId: Option[String]) = Open { implicit ctx =>
    if (HTTPRequest isXhr ctx.req)
      env.forms friendFilled (
        get("fen").map(FEN),
        get("pgn"),
        get("limit").map(_.toInt),
        get("increment").map(_.toInt),
        get("color"),
        getBoolOpt("rated")
      ) flatMap { form =>
          val validFen = form("fen").value flatMap ValidFen(false)
          val validPgn = form("pgn").value.flatMap(p => ValidPgn.of(p).orNone)
          val appt = get("appt") match {
            case None => true
            case Some(_) => false
          }
          userId ?? UserRepo.named flatMap {
            case None => Ok(html.setup.forms.friend(form, none, none, validFen, validPgn, appt)).fuccess
            case Some(user) => Env.challenge.granter(ctx.me, user, none) map {
              case Some(denied) => BadRequest(lila.challenge.ChallengeDenied.translated(denied))
              case None => Ok(html.setup.forms.friend(form, user.some, none, validFen, validPgn, appt))
            }
          }
        }
    else fuccess {
      Redirect(routes.Lobby.home + "#friend")
    }
  }

  def friend(userId: Option[String]) = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    PostRateLimit(HTTPRequest lastRemoteAddress ctx.req) {
      env.forms.friend(ctx).bindFromRequest.fold(
        err => negotiate(
          html = Lobby.renderHome(Results.BadRequest),
          api = _ => jsonFormError(err)
        ),
        config => userId ?? UserRepo.enabledById flatMap { destUser =>
          destUser ?? { Env.challenge.granter(ctx.me, _, config.perfType) } flatMap {
            case Some(denied) =>
              val message = lila.challenge.ChallengeDenied.translated(denied)
              negotiate(
                html = BadRequest(html.site.message.challengeDenied(message)).fuccess,
                api = _ => BadRequest(jsonError(message)).fuccess
              )
            case None =>
              import lila.challenge.Challenge._
              val challenge = lila.challenge.Challenge.make(
                variant = config.variant,
                initialFen = if (config.variant.fromPgn) None else config.fen,
                initialPgn = config.pgn map { p =>
                  ValidPgn.toPgnSetup(p) err s"can not parse pgn $p"
                },
                timeControl = config.makeClock map { c =>
                  TimeControl.Clock(c)
                } orElse config.makeDaysPerTurn.map {
                  TimeControl.Correspondence.apply
                } getOrElse TimeControl.Unlimited,
                mode = config.mode,
                appt = config.appt,
                apptStartsAt = config.apptStartsAt,
                apptMessage = config.apptMessage,
                color = config.color.name,
                challenger = (ctx.me, HTTPRequest sid req) match {
                  case (Some(user), _) => Right(user)
                  case (_, Some(sid)) => Left(sid)
                  case _ => Left("no_sid")
                },
                destUser = destUser,
                rematchOf = none,
                canTakeback = none
              )
              env.processor.saveFriendConfig(config) >>
                (Env.challenge.api create challenge) flatMap {
                  case true => negotiate(
                    html = fuccess(Redirect(routes.Round.watcher(challenge.id, "white"))),
                    api = _ => Challenge showChallenge challenge
                  )
                  case false => negotiate(
                    html = fuccess(Redirect(routes.Lobby.home)),
                    api = _ => fuccess(BadRequest(jsonError("Challenge not created")))
                  )
                }
          }
        }
      )
    }
  }

  def hookForm = Open { implicit ctx =>
    NoBot {
      if (HTTPRequest isXhr ctx.req) NoPlaybanOrCurrent {
        env.forms.hookFilled(timeModeString = get("time")) map { html.setup.forms.hook(_) }
      }
      else fuccess {
        Redirect(routes.Lobby.home + "#hook")
      }
    }
  }

  private def hookResponse(res: HookResult) = res match {
    case HookResult.Created(id) => Ok(Json.obj(
      "ok" -> true,
      "hook" -> Json.obj("id" -> id)
    )) as JSON
    case HookResult.Refused => BadRequest(jsonError("Game was not created"))
  }

  private val hookSaveOnlyResponse = Ok(Json.obj("ok" -> true))

  def hook(sri: String) = OpenBody { implicit ctx =>
    NoBot {
      implicit val req = ctx.body
      PostRateLimit(HTTPRequest lastRemoteAddress ctx.req) {
        NoPlaybanOrCurrent {
          env.forms.hook(ctx).bindFromRequest.fold(
            jsonFormError,
            userConfig => {
              val config = userConfig withinLimits ctx.me
              if (getBool("pool")) env.processor.saveHookConfig(config) inject hookSaveOnlyResponse
              else (ctx.userId ?? Env.relation.api.fetchBlocking) flatMap {
                blocking =>
                  env.processor.hook(config, Sri(sri), HTTPRequest sid req, blocking) map hookResponse
              }
            }
          )
        }
      }
    }
  }

  def like(sri: String, gameId: String) = Open { implicit ctx =>
    NoBot {
      PostRateLimit(HTTPRequest lastRemoteAddress ctx.req) {
        NoPlaybanOrCurrent {
          for {
            config <- env.forms.hookConfig
            game <- GameRepo game gameId
            blocking <- ctx.userId ?? Env.relation.api.fetchBlocking
            hookConfig = game.fold(config)(config.updateFrom)
            sameOpponents = game.??(_.userIds)
            hookResult <- env.processor.hook(hookConfig, Sri(sri), HTTPRequest sid ctx.req, blocking ++ sameOpponents)
          } yield hookResponse(hookResult)
        }
      }
    }
  }

  def filterForm = Open { implicit ctx =>
    env.forms.filterFilled map {
      case (form, filter) => html.setup.filter(form, filter)
    }
  }

  def filter = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    env.forms.filter(ctx).bindFromRequest.fold[Fu[Result]](
      f => {
        controllerLogger.branch("setup").warn(f.errors.toString)
        BadRequest(()).fuccess
      },
      config => JsonOk(env.processor filter config inject config.render)
    )
  }

  def validateFen = Open { implicit ctx =>
    val strict = getBool("strict")
    val playableOption = get("playable")
    get("fen") flatMap ValidFen(strict, playableOption.map(_.toBoolean) | true) match {
      case None => BadRequest.fuccess
      case Some(v) => Ok(html.game.bits.miniBoard(v.fen, v.color)).fuccess
    }
  }

  def validatePgn = Open { implicit ctx =>
    get("pgn") match {
      case Some(pgn) => {
        val vp = ValidPgn.of(pgn)
        vp.success match {
          case true => {
            val pgnSetup = ValidPgn.toPgnSetup(pgn) err s"can not parse pgn $pgn"
            Ok(lila.game.JsonView.initialPgnJson(pgnSetup)).fuccess
          }
          case false => BadRequest(jsonError(vp.message | "invalid pgn")).fuccess
        }
      }
      case None => BadRequest(jsonError("invalid pgn")).fuccess
    }
  }

  private def process[A](form: Context => Form[A])(op: A => BodyContext[_] => Fu[Pov]) =
    OpenBody { implicit ctx =>
      PostRateLimit(HTTPRequest lastRemoteAddress ctx.req) {
        implicit val req = ctx.body
        form(ctx).bindFromRequest.fold(
          err => negotiate(
            html = Lobby.renderHome(Results.BadRequest),
            api = _ => jsonFormError(err)
          ),
          config => op(config)(ctx) flatMap { pov =>
            negotiate(
              html = fuccess(redirectPov(pov)),
              api = apiVersion => Env.api.roundApi.player(pov, apiVersion) map { data =>
                Created(data) as JSON
              }
            )
          }
        )
      }
    }

  private[controllers] def redirectPov(pov: Pov)(implicit ctx: Context) = {
    val redir = Redirect(routes.Round.watcher(pov.gameId, "white"))
    if (ctx.isAuth) redir
    else redir withCookies LilaCookie.cookie(
      AnonCookie.name,
      pov.playerId,
      maxAge = AnonCookie.maxAge.some,
      httpOnly = false.some
    )
  }
}
