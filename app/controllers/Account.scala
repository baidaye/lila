package controllers

import play.api.mvc._
import lila.api.Context
import lila.app._
import lila.common.{ LilaCookie, SmsTemplate }
import lila.user.{ TotpSecret, UserRepo, User => UserModel }
import UserModel.ClearPassword
import lila.memo.UploadRateLimit
import views.html
import play.api.libs.json._

object Account extends LilaController {

  private def env = Env.user
  private def relationEnv = Env.relation
  private def sforms = Env.security.forms

  def profile(referrer: Option[String] = None) = Auth { implicit ctx => me =>
    Ok(html.account.profile(me, referrer, env.forms profileOf me)).fuccess
  }

  def levels(referrer: Option[String] = None) = Auth { implicit ctx => me =>
    Ok(html.account.level(me, referrer, env.forms levelsOf me)).fuccess
  }

  def uploadPicture = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val picture = ctx.body.body.file("file")
      picture match {
        case Some(pic) => env.photographer(me.id, pic, true) map { image =>
          Ok(Json.obj("ok" -> true, "path" -> image.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  def profileApply(referrer: Option[String] = None) = AuthBody { implicit ctx => me =>
    implicit val req: Request[_] = ctx.body
    FormFuResult(env.forms.profile(me)) { err =>
      fuccess(html.account.profile(me, referrer, err))
    } { profile =>
      UserRepo.setProfile(me.id, profile) >>- env.lightUserApi.invalidate(me.id) inject {
        referrer match {
          case None => Redirect(routes.User show me.username)
          case Some(x) => x match {
            case "coach" => Redirect(routes.Coach.certify)
            //case "club" => Redirect(routes.Coach.certification)
          }
        }
      }
    }
  }

  def levelsApply(referrer: Option[String] = None) = AuthBody { implicit ctx => me =>
    implicit val req: Request[_] = ctx.body
    FormFuResult(env.forms.level) { err =>
      fuccess(Json stringify errorsAsJson(err))
    } { lv =>
      UserRepo.setCurrentLevel(me.id, lv) inject Redirect(routes.Account.profile(referrer))
    }
  }

  def info = Auth { implicit ctx => me =>
    negotiate(
      html = notFound,
      api = _ => {
        lila.mon.http.response.accountInfo.count()
        relationEnv.api.countFollowers(me.id) zip
          relationEnv.api.countFollowing(me.id) zip
          Env.pref.api.getPref(me) zip
          Env.round.proxy.urgentGames(me) zip
          Env.challenge.api.countInFor.get(me.id) zip
          Env.playban.api.currentBan(me.id) map {
            case nbFollowers ~ nbFollowing ~ prefs ~ povs ~ nbChallenges ~ playban =>
              Env.current.system.lilaBus.publish(lila.user.User.Active(me), 'userActive)
              Ok {
                import lila.pref.JsonView._
                Env.user.jsonView(me) ++ Json.obj(
                  "prefs" -> prefs,
                  "nowPlaying" -> JsArray(povs take 50 map Env.api.lobbyApi.nowPlaying),
                  "nbFollowing" -> nbFollowing,
                  "nbFollowers" -> nbFollowers,
                  "nbChallenges" -> nbChallenges
                ).add("kid" -> me.kid)
                  .add("troll" -> me.troll)
                  .add("playban" -> playban)
              }
          }
      }.mon(_.http.response.accountInfo.time)
    )
  }

  def nowPlaying = Auth { implicit ctx => me =>
    negotiate(
      html = notFound,
      api = _ => doNowPlaying(me, ctx.req)
    )
  }

  def apiMe = Scoped() { _ => me =>
    Env.api.userApi.extended(me, me.some) map { JsonOk(_) }
  }

  def apiNowPlaying = Scoped() { req => me =>
    doNowPlaying(me, req)
  }

  def apiEmail = Scoped(_.Email.Read) { _ => me =>
    UserRepo email me.id map {
      _ ?? { email =>
        JsonOk(Json.obj("email" -> email.value))
      }
    }
  }

  private def doNowPlaying(me: lila.user.User, req: RequestHeader) =
    Env.round.proxy.urgentGames(me) map { povs =>
      val nb = (getInt("nb", req) | 9) atMost 50
      Ok(Json.obj("nowPlaying" -> JsArray(povs take nb map Env.api.lobbyApi.nowPlaying)))
    }

  def dasher = Auth { implicit ctx => me =>
    negotiate(
      html = notFound,
      api = _ => Env.pref.api.getPref(me) map { prefs =>
        Ok {
          import lila.pref.JsonView._
          lila.common.LightUser.lightUserWrites.writes(me.light) ++ Json.obj(
            "coach" -> isGranted(_.Coach),
            "prefs" -> prefs
          )
        }
      }
    )
  }

  def passwd = Auth { implicit ctx => me =>
    env.forms passwd me map { form =>
      Ok(html.account.passwd(form))
    }
  }

  def passwdApply = AuthBody { implicit ctx => me =>
    controllers.Auth.HasherRateLimit(me.username, ctx.req) { _ =>
      implicit val req = ctx.body
      env.forms passwd me flatMap { form =>
        FormFuResult(form) { err =>
          fuccess(html.account.passwd(err))
        } { data =>
          Env.user.authenticator.setPassword(me.id, ClearPassword(data.newPasswd1)) inject
            Redirect(s"${routes.Account.passwd}?ok=1")
        }
      }
    }
  }

  def confirm = Auth { implicit ctx => me =>
    if (me.notImported) {
      val tp = get("tp")
      Ok(html.account.confirm(me, sforms.confirm.changeCellphone(me), sforms.confirm.changeEmail(me), tp)).fuccess
    } else Forbidden(views.html.site.message.authFailed).fuccess
  }

  def emailSend = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    FormFuResult(sforms.confirm.changeEmail(me)) { err =>
      fuccess(html.account.confirm(me, sforms.confirm.changeCellphone(me), err, tp = "em".some, active = "email".some))
    } { data =>
      val email = Env.security.emailAddressValidator.validate(data.realEmail) err s"Invalid email ${data.email}"
      val newUserEmail = lila.security.EmailConfirm.UserEmail(me.username, email.acceptable)
      controllers.Auth.EmailConfirmRateLimit(newUserEmail, ctx.req) {
        Env.security.emailChange.send(me, newUserEmail.email) inject Redirect(routes.Account.checkYourEmail(newUserEmail.email.conceal))
      }
    }
  }

  def checkYourEmail(email: String) = Open { implicit ctx =>
    Ok(html.account.bits.checkYourEmail(email)).fuccess
  }

  def emailConfirm(token: String) = Open { implicit ctx =>
    Env.security.emailChange.confirm(token).flatMap {
      case None => Redirect(s"${routes.Account.confirm()}?tp=2").fuccess
      case Some(user) => {
        controllers.Auth.authenticateUser(user, result = Some { _ => Redirect(s"${routes.Account.confirm}#email") })
      }
    }
  }

  def cellphoneConfirm = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    sforms.confirm.changeCellphone(me).bindFromRequest.fold(
      err => Ok(html.account.confirm(me, err, sforms.confirm.changeEmail(me), tp = "cp".some, active = "cellphone".some)).fuccess,
      data => {
        val cellphone = data.realCellphone
        UserRepo.setCellphoneConfirmed(me.id, cellphone = cellphone).flatMap { _ =>
          controllers.Auth.authenticateUser(me, result = Some { _ => Redirect(s"${routes.Account.confirm}#cellphone") })
        }
      }
    )
  }

  def close = Auth { implicit ctx => me =>
    sforms.closeAccount(me) map { form =>
      Ok(html.account.close(me, form))
    }
  }

  def closeConfirm = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    sforms.closeAccount(me) flatMap { form =>
      FormFuResult(form) { err =>
        fuccess(html.account.close(me, err))
      } { _ =>
        Env.current.closeAccount(me.id, self = true) inject {
          Redirect(routes.User.show(me.username)) withCookies LilaCookie.newSession
        }
      }
    }
  }

  def kid = Auth { implicit ctx => me =>
    Ok(html.account.kid(me)).fuccess
  }
  def apiKid = Scoped(_.Preference.Read) { _ => me =>
    JsonOk(Json.obj("kid" -> me.kid)).fuccess
  }

  // App BC
  def kidToggle = Auth { ctx => me =>
    UserRepo.setKid(me, !me.kid) inject Ok
  }

  def kidPost = Auth { implicit ctx => me =>
    UserRepo.setKid(me, getBool("v")) inject Redirect(routes.Account.kid)
  }
  def apiKidPost = Scoped(_.Preference.Write) { req => me =>
    UserRepo.setKid(me, getBool("v", req)) inject jsonOkResult
  }

  private def currentSessionId(implicit ctx: Context) =
    ~Env.security.api.reqSessionId(ctx.req)

  def security = Auth { implicit ctx => me =>
    Env.security.api.dedup(me.id, ctx.req) >>
      Env.security.api.locatedOpenSessions(me.id, 50) map { sessions =>
        Ok(html.account.security(me, sessions, currentSessionId))
      }
  }

  def signout(sessionId: String) = Auth { implicit ctx => me =>
    if (sessionId == "all")
      lila.security.Store.closeUserExceptSessionId(me.id, currentSessionId) >>
        Env.push.webSubscriptionApi.unsubscribeByUserExceptSession(me, currentSessionId) inject
        Redirect(routes.Account.security)
    else
      lila.security.Store.closeUserAndSessionId(me.id, sessionId) >>
        Env.push.webSubscriptionApi.unsubscribeBySession(sessionId)
  }

}
