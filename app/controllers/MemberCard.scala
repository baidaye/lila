package controllers

import lila.app._
import lila.api.Context
import lila.security.Permission
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import lila.common.paginator.Paginator
import lila.member.{ JsonView, OrderDirector, MemberCard => MemberCardMode }
import lila.team.{ CampusRepo, TagRepo }
import lila.common.LilaCookie
import lila.user.UserRepo
import play.api.libs.json.{ JsArray, JsString, Json }
import views._

object MemberCard extends LilaController {

  private def env = Env.member
  private val MemberCardSourceCookie = "MemberCardSource"

  def hasSignupGiven(userId: String) = Auth { implicit ctx => me =>
    env.memberCardLogApi.hasSignupGiven(userId) map (bool => Ok(bool.toString))
  }

  def page(page: Int, status: Option[String], level: Option[String]) = Auth { implicit ctx => me =>
    Permiss {
      val st = status.map(s => MemberCardMode.CardStatus(s))
      val lv = level.map(l => lila.user.MemberLevel(l))
      env.memberCardApi.minePage(me.id, page, st, lv) map { pager =>
        get("source") match {
          case None => Ok(html.member.card.mine(pager, st, lv))
          case Some(s) => {
            Ok(html.member.card.mine(pager, st, lv)).withCookies(
              LilaCookie.cookie(MemberCardSourceCookie, s)
            )
          }
        }
      }
    }
  }

  def givingLogPage(page: Int) = AuthBody { implicit ctx => me =>
    Permiss {
      implicit def req = ctx.body
      def searchForm = env.form.cardLogSearch
      searchForm.bindFromRequest.fold(
        err => {
          Ok(html.member.card.givingLogs(Paginator.empty, err)).fuccess
        },
        data => env.memberCardLogApi.minePage(me.id, page, data) map { pager =>
          Ok(html.member.card.givingLogs(pager, searchForm fill data))
        }
      )
    }
  }

  def toBuy = Auth { implicit ctx => me =>
    Permiss {
      Ok(html.member.card.buy(me, env.form.order(me, List.empty))).fuccess
    }
  }

  def calcPrice = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      env.form.calcPrice(me, List.empty).bindFromRequest.fold(
        jsonFormError,
        data => {
          val res = OrderDirector.calcPrice(data, true, me)
          Ok(JsonView.priceJson(res)) as JSON
        }.fuccess
      )
    }
  }

  def givingForm(id: String) = Auth { implicit ctx => me =>
    Permiss {
      OptionResult(env.memberCardApi.byId(id)) { card =>
        Ok(html.member.card.giving(card))
      }
    }
  }

  def validUsername = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      Form(single(
        "username" -> lila.user.DataForm.historicalUsernameField.verifying("必须是您的学员", canGive _)
      )).bindFromRequest.fold(
        err => jsonFormError(err),
        _ => fuccess(jsonOkResult)
      )
    }
  }

  def giving(id: String) = AuthBody { implicit ctx => me =>
    Permiss {
      OptionFuResult(env.memberCardApi.byId(id)) { card =>
        if (card.userId != me.id || !card.isAvailable) {
          Forbidden(views.html.site.message.authFailed).fuccess
        } else {
          implicit val req = ctx.body
          Form(single(
            "username" -> lila.user.DataForm.historicalUsernameField.verifying("必须是您的学员", canGive _)
          )).bindFromRequest.fold(
            err => fuccess(BadRequest(err.toString)),
            username => UserRepo.byId(lila.user.User.normalize(username)) flatMap { memberOption =>
              memberOption.fold(Redirect(routes.MemberCard.page(1, none, none)).fuccess) { member =>
                env.memberCardApi.give(me, member, card) inject Redirect(routes.MemberCard.page(1, none, none))
              }
            }
          )
        }
      }
    }
  }

  def use(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.memberCardApi.byId(id)) { card =>
      if (card.userId != me.id || !card.isAvailable) {
        Forbidden(views.html.site.message.authFailed).fuccess
      } else {
        env.memberCardApi.use(me, card) inject Redirect(routes.Member.info)
      }
    }
  }

  def batchGiving() = Auth { implicit ctx => me =>
    ctx.req.cookies.get(MemberCardSourceCookie).fold(fuccess { Redirect(s"${routes.MemberCard.teamBatchGiving()}?campus=${lila.team.Campus.defaultId(me.teamIdValue)}") }) { cookie =>
      cookie.value match {
        case "team" => fuccess { Redirect(s"${routes.MemberCard.teamBatchGiving()}?campus=${lila.team.Campus.defaultId(me.teamIdValue)}") }
        case "coach" => fuccess { Redirect(routes.MemberCard.coachBatchGiving()) }
      }
    }
  }

  def teamBatchGiving() = AuthBody { implicit ctx => me =>
    Permiss {
      val teamId = me.teamIdValue
      OptionFuResult(Env.team.api.team(teamId)) { team =>
        rt_team.Team.CanWriteCampus(team) {
          (for {
            tags <- TagRepo.findByTeam(teamId)
            campuses <- CampusRepo.byTeam(teamId)
            markMap <- Env.team.api.userMarks(me.id)
          } yield ((tags, campuses), markMap)) flatMap {
            case ((tags, campuses), markMap) => {
              implicit val req = ctx.body
              val form = Env.team.forms.member.memberSearch.bindFromRequest
              val hideGivingInMonth = getBool("hideGivingInMonth")
              form.fold(
                fail => {
                  BadRequest(html.member.card.batchGiving.team(fail, team, List.empty[lila.user.User], tags, campuses, markMap, hideGivingInMonth, getBool("advance"))).fuccess
                },
                data => {
                  env.memberCardLogApi.givedNear28Day(me) flatMap { logs =>
                    Env.team.memberSelector.findMember(team.id, data, tags, markMap) map { members =>
                      val excludes = if (hideGivingInMonth) logs.map(_.newUserId).distinct :+ me.id else List(me.id)
                      val sortedMembers = members.map(_.user).filter(u => !excludes.contains(u.id)).sortWith {
                        case (u1, u2) => {
                          if (u1.memberLevel.id > u2.memberLevel.id) true
                          else if (u1.memberLevel.id < u2.memberLevel.id) false
                          else u1.memberOrDefault.lvWithExpire.expireAt.isBefore(u2.memberOrDefault.lvWithExpire.expireAt)
                        }
                      }
                      Ok(html.member.card.batchGiving.team(form, team, sortedMembers, tags, campuses, markMap, hideGivingInMonth, getBool("advance")))
                    }
                  }
                }
              )
            }
          }
        }
      }
    }
  }

  def coachBatchGiving() = Auth { implicit ctx => me =>
    Permiss {
      (for {
        clazzes <- Env.clazz.api.mine(me.id).map(_.filter(c => c.isCoach(me.id)))
        markMap <- Env.team.api.userMarks(me.id)
      } yield (clazzes, markMap)) flatMap {
        case (clazzes, markMap) => {
          val clazzId = get("clazzId")
          val username = get("username")
          val memberLevel = get("memberLevel")
          val hideGivingInMonth = getBool("hideGivingInMonth")

          (clazzId match {
            case None => clazzes.headOption match {
              case Some(clazz) => coachClazzStudents(me, markMap, clazz.id, username, memberLevel, hideGivingInMonth).map { (clazz.id, _) }
              case None => coachOtherStudents(me, markMap, username, memberLevel, hideGivingInMonth).map { ("others", _) }
            }
            case Some(clsId) =>
              if (clsId === "others") coachOtherStudents(me, markMap, username, memberLevel, hideGivingInMonth).map { (clsId, _) }
              else coachClazzStudents(me, markMap, clsId, username, memberLevel, hideGivingInMonth).map { (clsId, _) }
          }).map {
            case (clsId, students) => {
              val clazzSeq = clazzes.map(c => c.id -> c.name) :+ ("others" -> "其它学员")
              Ok(html.member.card.batchGiving.coach(env.form.memberSearch, students, clazzSeq, markMap, clsId, username, memberLevel, hideGivingInMonth))
            }
          }
        }
      }
    }
  }

  private def coachOtherStudents(
    me: lila.user.User,
    markMap: Map[String, Option[String]],
    username: Option[String],
    memberLevel: Option[String],
    hideGivingInMonth: Boolean
  )(implicit ctx: Context): Fu[List[lila.user.User]] = {
    for {
      mineStudents <- Env.coach.studentApi.mineStudents(me.some)
      teamStudents <- Env.team.api.mineMembers(me.some)
      clazzStudents <- Env.clazz.api.mine(me.id).map(_.filter(c => c.isCoach(me.id))).map(_.flatMap(_.studentIds))
      givedNear28Day <- if (hideGivingInMonth) { env.memberCardLogApi.givedNear28Day(me) } else fuccess(nil)
      excludes = givedNear28Day.map(_.newUserId) :+ me.id
      userIds = teamStudents ++ mineStudents -- clazzStudents.toSet -- excludes.toSet
      students <- Env.clazz.studentApi.clazzStudents(userIds.toList, username, markMap, memberLevel)
    } yield students
  }

  private def coachClazzStudents(
    me: lila.user.User,
    markMap: Map[String, Option[String]],
    clazzId: String,
    username: Option[String],
    memberLevel: Option[String],
    hideGivingInMonth: Boolean
  )(implicit ctx: Context): Fu[List[lila.user.User]] =
    Env.clazz.api.byId(clazzId).flatMap {
      case None => fuccess { List.empty[lila.user.User] }
      case Some(clazz) => {
        for {
          givedNear28Day <- if (hideGivingInMonth) { env.memberCardLogApi.givedNear28Day(me) } else fuccess(nil)
          excludes = givedNear28Day.map(_.newUserId) :+ me.id
          userIds = clazz.studentIds.toSet -- excludes.toSet
          students <- Env.clazz.studentApi.clazzStudents(userIds.toList, username, markMap, memberLevel)
        } yield students
      }
    }

  val maxGiving = 50
  def batchGivingModal() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      Form(single(
        "mems" -> list(lila.user.DataForm.historicalUsernameField)
      )).bindFromRequest.fold(
        err => jsonFormError(err),
        mems => {
          for {
            members <- UserRepo.byIds(mems.take(maxGiving))
            markMap <- Env.team.api.userMarks(me.id)
          } yield Ok(html.member.card.batchGiving.batchGivingModal(env.form.batchGive, members, markMap))
        }
      )
    }
  }

  def batchGivingApply() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      env.form.batchGive.bindFromRequest.fold(
        err => jsonFormError(err),
        data => UserRepo.byIds(data.mems.take(maxGiving)) flatMap { members =>
          env.memberCardApi.cardCount(me.id, data.cardLevel, data.days) flatMap { count =>
            if (count < members.length) {
              fuccess {
                Forbidden(jsonError("会员卡数量不足")) as JSON
              }
            } else {
              env.memberCardApi.batchGive(me, members, data).map { logs =>
                Ok(JsArray(logs.map(log => JsString(log.id)))) as JSON
              }
            }
          }
        }
      )
    }
  }

  def batchGivingSuccess() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      env.form.batchGiveSuccess.bindFromRequest.fold(
        err => jsonFormError(err),
        data => {
          for {
            logs <- env.memberCardLogApi.byIds(data.ids)
            members <- UserRepo.byIds(logs.map(_.newUserId))
            markMap <- Env.team.api.userMarks(me.id)
          } yield Ok(html.member.card.batchGiving.batchGivingSuccess(data.cardLevelName, data.daysName, members, markMap))
        }
      )
    }
  }

  def cardCount() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      env.form.cardCount.bindFromRequest.fold(
        err => jsonFormError(err),
        data => env.memberCardApi.cardCount(me.id, data.cardLevel, data.days).map { count =>
          Ok(Json.obj(
            "count" -> count
          ))
        }
      )
    }
  }

  private def canGive(username: String)(implicit ctx: lila.api.Context): Boolean = {
    for {
      students <- Env.coach.studentApi.mineStudents(ctx.me)
      teamMembers <- Env.team.api.mineMembers(ctx.me)
    } yield {
      val userId = lila.user.User.normalize(username)
      students.contains(userId) || teamMembers.contains(userId)
    }
  } awaitSeconds 3

  private def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (isGranted(Permission.Coach) || isGranted(Permission.Team)) f
    else Forbidden(views.html.site.message.authFailed).fuccess
  }

}
