package controllers

import lila.app._
import views._

object RedirectTo extends LilaController {

  def homeworkShow(id: String) = Auth { implicit ctx => me =>
    Ok(views.html.redirectTo(s"${controllers.rt_klazz.routes.Homework.show(id)}")).fuccess
  }

  def homeworkShow2(clazzId: String, courseId: String) = Auth { implicit ctx => me =>
    Ok(views.html.redirectTo(s"${controllers.rt_klazz.routes.Homework.show2(clazzId, courseId)}")).fuccess
  }

}
