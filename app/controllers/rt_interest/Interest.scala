package controllers.rt_interest

import lila.app._
import lila.api.Context
import lila.interest.{ InterestData, InterestRepo, InterestSearchData, InterestVariantSearchData, InterestLikeRepo, SearchRecordRepo, InterestSearchRepo, ID, Source }
import views.html

object Interest extends controllers.LilaController {

  private val env = Env.interest
  private val jsonView = env.jsonView
  private val form = lila.interest.DataForm

  def home(sc: String) = Open { implicit ctx =>
    val source = Source(sc)
    source match {
      case Source.PawnsOnly => Redirect(routes.Interest.showVariant(sc, "pawnsOnly")).fuccess
      case Source.DualRooksAndBishops => Redirect(routes.Interest.showVariant(sc, "dualRooksAndBishops")).fuccess
      case _ => {
        for {
          last <- SearchRecordRepo.lastId(source, ctx.userId)
          search <- InterestSearchRepo.mine(source, ctx.userId)
        } yield {
          Redirect(s"${routes.Interest.show(sc, last)}?next=true${search.toUrlParam}")
        }
      }
    }
  }

  def show(sc: String, id: ID) = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    val source = Source(sc)
    form.interestSearch(source).bindFromRequest.fold(
      _ => fuccess(BadRequest),
      search => {
        {
          getBool("next") match {
            case true => InterestRepo.next(ctx.me, source, id, search.orDefault(source))
            case false => InterestRepo.byId(id)
          }
        } flatMap {
          case None => renderShow(source, InterestData.default(source), search.orDefault(source), nf = true)
          case Some(data) => renderShow(source, data, search.orDefault(source))
        }
      }
    )
  }

  def showVariant(sc: String, variant: String) = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    val source = Source(sc)
    val searchForm = form.interestVariantSearch(source).bindFromRequest
    searchForm.fold(
      _ => fuccess(BadRequest),
      search => {
        Ok(html.interest.showVariant(source, variant, jsonView.pref(ctx.pref), searchForm.fill(search.orDefault(source))))
          .withHeaders("Cross-Origin-Opener-Policy" -> "same-origin")
          .withHeaders("Cross-Origin-Embedder-Policy" -> "require-corp")
          .fuccess
      }
    )
  }

  def renderShow(source: Source, data: InterestData, search: InterestSearchData, nf: Boolean = false)(implicit ctx: Context) = {
    for {
      lastId <- SearchRecordRepo.lastId(source, ctx.userId)
      jsData <- jsonView.data(data, ctx.me)
    } yield Ok(html.interest.show(
      lastId,
      source,
      form.interestSearch(source).fill(search),
      jsonView.pref(ctx.pref),
      jsData,
      nf
    ))
  }

  def next(sc: String, id: ID) = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    val source = Source(sc)
    form.interestSearch(source).bindFromRequest.fold(
      jsonFormError,
      data => {
        InterestRepo.next(ctx.me, source, id, data).flatMap {
          case None => notFound
          case Some(data) => jsonView.data(data, ctx.me) map { jsData =>
            Ok(jsData)
          }
        }
      }
    )
  }

  def finish(sc: String, id: ID) = OpenBody { implicit ctx =>
    implicit val req = ctx.body
    val source = Source(sc)
    form.finish(source).bindFromRequest.fold(
      jsonFormError,
      data => {
        env.api.finish(id, source, ctx.userId, data) inject jsonOkResult
      }
    )
  }

  def like(sc: String, id: ID) = Auth { implicit ctx => me =>
    InterestLikeRepo.toggle(id, me.id, Source(sc)) inject jsonOkResult
  }

  def setLikeTag(sc: String, id: ID) = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    form.like.bindFromRequest.fold(
      jsonFormError,
      tags => {
        val tagList = tags.split(",").toList
        InterestLikeRepo.setTag(id, me.id, Source(sc), tagList) inject jsonOkResult
      }
    )
  }

  def storeSearch(sc: String) = AuthBody { implicit ctx => me =>
    val source = Source(sc)
    implicit val req = ctx.body
    form.interestSearch(source).bindFromRequest.fold(
      jsonFormError,
      search => {
        InterestSearchRepo.upsert(me.id, source, search) inject jsonOkResult
      }
    )
  }

}
