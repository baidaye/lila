package controllers.rt_resource

import chess.Replay
import chess.format.Forsyth
import chess.format.pgn.{ Reader }
import lila.app._
import lila.api.Context
import lila.app.mashup.GameFilterMenu
import lila.common.paginator.Paginator
import lila.game.GameRepo
import play.api.mvc.Result
import play.api.data._
import play.api.data.Forms._
import views.html

object Resource extends controllers.LilaController {

  private val env = Env.resource
  private val puzzleEnv = Env.puzzle
  private val bookmarkEnv = Env.bookmark
  private val gameEnv = Env.game
  private val openingEnv = Env.opening
  private def userGameSearch = Env.gameSearch.userGameSearch
  private val maxSize = 60

  def puzzleLiked(page: Int) = AuthBody { implicit ctx => me =>
    //Permiss {
    implicit def req = ctx.body
    def searchForm = env.forms.puzzle.liked
    searchForm.bindFromRequest.fold(
      failure => Ok(views.html.resource.liked.puzzle(failure, Paginator.empty, Set())).fuccess,
      data => {
        for {
          tags <- puzzleEnv.resource.likedTags(me.id)
          pager <- puzzleEnv.resource.liked(page, me.id, data)
        } yield {
          Ok(views.html.resource.liked.puzzle(searchForm fill data, pager, tags))
        }
      }
    )
    //}
  }

  def puzzleImported(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = env.forms.puzzle.imported
    searchForm.bindFromRequest.fold(
      failure => Ok(views.html.resource.puzzle.imported(failure, Paginator.empty, Set())).fuccess,
      data => {
        for {
          tags <- puzzleEnv.resource.importedTags(me.id)
          pager <- puzzleEnv.resource.imported(page, me.id, data)
        } yield {
          Ok(views.html.resource.puzzle.imported(searchForm fill data, pager, tags))
        }
      }
    )
  }

  def puzzleTheme(page: Int) = AuthBody { implicit ctx => me =>
    Permiss {
      implicit def req = ctx.body
      def searchForm = env.forms.puzzle.themeAnd
      searchForm.bindFromRequest.fold(
        failure => Ok(views.html.resource.puzzle.theme(failure, Paginator.empty, Set())).fuccess,
        data => {
          for {
            tags <- puzzleEnv.resource.themeTags()
            pager <- puzzleEnv.resource.theme(page, me.id, data)
          } yield {
            Ok(views.html.resource.puzzle.theme(searchForm fill data, pager, tags))
          }
        }
      )
    }
  }

  def puzzleThemeAndCount = AuthBody { implicit ctx => me =>
    Permiss {
      implicit def req = ctx.body
      def searchForm = env.forms.puzzle.themeAnd
      searchForm.bindFromRequest.fold(
        jsonFormError,
        data => {
          puzzleEnv.resource.themeAndCount(data).map { count =>
            Ok(jsonOkBody.add("count" -> count.some)) as JSON
          }
        }
      )
    }
  }

  def puzzleThemeOrCount = AuthBody { implicit ctx => me =>
    Permiss {
      implicit def req = ctx.body
      def searchForm = env.forms.puzzle.themeOr
      searchForm.bindFromRequest.fold(
        jsonFormError,
        data => {
          puzzleEnv.resource.themeOrCount(data).map { count =>
            Ok(jsonOkBody.add("count" -> count.some)) as JSON
          }
        }
      )
    }
  }

  def puzzleMachine(page: Int) = AuthBody { implicit ctx => me =>
    TeamOrCoach {
      implicit def req = ctx.body
      def searchForm = env.forms.puzzle.machine
      searchForm.bindFromRequest.fold(
        failure => Ok(views.html.resource.puzzle.machine(failure, Paginator.empty)).fuccess,
        data => {
          for {
            pager <- puzzleEnv.resource.machine(page, data)
          } yield {
            Ok(views.html.resource.puzzle.machine(searchForm fill data, pager))
          }
        }
      )
    }
  }

  def puzzleMachineCount = AuthBody { implicit ctx => me =>
    TeamOrCoach {
      implicit def req = ctx.body
      def searchForm = env.forms.puzzle.machine
      searchForm.bindFromRequest.fold(
        jsonFormError,
        data => {
          puzzleEnv.resource.machineCount(data).map { count =>
            Ok(jsonOkBody.add("count" -> count.some)) as JSON
          }
        }
      )
    }
  }

  def puzzleMachineAdvise() = Auth { implicit ctx => me =>
    Ok(views.html.resource.puzzleMachineAdvisePage()).fuccess
  }

  def puzzleBatchDelete = AuthBody { implicit ctx => implicit me =>
    val idList = get("ids").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
    puzzleEnv.resource.disableByIds(idList, me) inject Redirect(routes.Resource.puzzleImported(1))
  }

  def puzzleBatchUnlike = AuthBody { implicit ctx => implicit me =>
    //Permiss {
    val idList = get("ids").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
    puzzleEnv.api.tagger.removeByPuzzleIdsAndUser(idList, me.id) inject Redirect(routes.Resource.puzzleLiked(1))
    //}
  }

  def gameLiked(page: Int) = AuthBody { implicit ctx => me =>
    //Permiss {
    implicit def req = ctx.body
    def searchForm = env.forms.game.liked
    searchForm.bindFromRequest.fold(
      failure => Ok(views.html.resource.liked.game(failure, Paginator.empty, Set(), me)).fuccess,
      data => data match {
        case (emptyTag, t) => {
          for {
            tags <- bookmarkEnv.api.bookmarkTags(me.id)
            pager <- bookmarkEnv.api.gamePaginatorByUser(me, page, emptyTag, t)
          } yield {
            Ok(views.html.resource.liked.game(searchForm fill data, pager, tags, me))
          }
        }
      }
    )
    //}
  }

  def gameImported(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = env.forms.game.imported
    searchForm.bindFromRequest.fold(
      failure => Ok(views.html.resource.game.imported(failure, me, Paginator.empty, Set())).fuccess,
      data => data match {
        case (emptyTag, t) => {
          for {
            tags <- GameRepo.importedTags(me.id)
            pager <- GameFilterMenu.imported(me, page, emptyTag, t)
          } yield {
            Ok(views.html.resource.game.imported(searchForm fill data, me, pager, tags))
          }
        }
      }
    )
  }

  def gameSearch(page: Int) = AuthBody { implicit ctx => me =>
    Permiss {
      implicit def req = ctx.body
      userGameSearch(me, page) map { pager =>
        Ok(views.html.resource.game.search(userGameSearch.requestForm, me, pager))
      }
    }
  }

  def gameBatchDelete = AuthBody { implicit ctx => implicit me =>
    val idList = get("ids").??(_.split(",").toList).distinct take maxSize
    GameRepo.removeByIds(idList, me.id) inject Redirect(routes.Resource.gameImported(1))
  }

  def gameBatchUnlike = AuthBody { implicit ctx => implicit me =>
    //Permiss {
    val idList = get("ids").??(_.split(",").toList).distinct take maxSize
    bookmarkEnv.api.removeByGameIdsAndUser(idList, me.id) inject Redirect(routes.Resource.gameLiked(1))
    //}
  }

  def initialFen(pgn: String) = {
    Reader.full(pgn) map lila.opening.PgnParser.evenIncomplete map {
      case replay @ Replay(setup, _, _) => {
        Forsyth >> setup
      }
    }
  }

  def saveToModal(source: String, rel: String) = AuthBody { implicit ctx => me =>
    if (me.isMemberOrCoachOrTeam) {
      implicit def req = ctx.body
      Form(single("pgn" -> nonEmptyText)).bindFromRequest.fold(
        err => jsonFormError(err),
        pgn => {
          def res(initialFen: String) = lila.opening.Env.current.api.canWrittenList(me) map { openings =>
            val gamedbForm = env.forms.gameDBRel.create(me, Env.study.api.byId) fill env.forms.gameDBRel.CreateData(
              tab = "pgn",
              name = "",
              tags = None,
              chapter = None,
              game = None,
              pgn = pgn.some
            )
            val openingdbForm = openingEnv.form.addNodesForm fill lila.opening.DataForm.AddNodesData(
              tab = "openingdb", name = None, shortName = None, pgn = pgn, source = source, rel = rel
            )
            val puzzleForm = lila.puzzle.DataForm.fenForm(false) fill lila.puzzle.DataForm.PuzzleImportData(
              pgn = pgn,
              hasLastMove = false,
              puzzleTag = None
            )
            Ok(html.resource.modal.saveTo(pgn, initialFen, gamedbForm, openingdbForm, puzzleForm, openings))
          }
          initialFen(pgn).fold(_ => res(Forsyth.initial), initialFen => res(initialFen))
        }
      )
    } else NotAcceptable(html.member.notAccept("会员升级")).fuccess
  }

  import play.api.libs.json.Json
  import scalaz.{ Failure, Success }
  def saveToPuzzle = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    lila.puzzle.DataForm.fenForm(false).bindFromRequest.fold(
      _ => BadRequest(Json.obj("error" -> "无效的战术题格式")).fuccess,
      data => (lila.db.Util findNextId (Env.puzzle.puzzleColl)) flatMap { puzzleMaxId =>
        val minImportPuzzleId = lila.puzzle.Puzzle.minImportId
        var puzzleId = minImportPuzzleId atLeast puzzleMaxId
        data.copy(pgn = data.pgn) preprocess (me.id.some, false) match {
          case Success(p) => Env.puzzle.api.puzzle.insert(p.copy(id = puzzleId)) inject jsonOkResult
          case Failure(e) => BadRequest(Json.obj("error" -> "无效的战术题格式")).fuccess
        }
      }
    )
  }

  private[controllers] def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(_.hasResource)) f
    else NotAcceptable(html.member.notAccept()).fuccess
  }

  private[controllers] def TeamOrCoach(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(_.isCoachOrTeam)) f
    else NotAcceptable(html.member.notAccept()).fuccess
  }

}
