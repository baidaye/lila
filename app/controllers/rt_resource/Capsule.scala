package controllers.rt_resource

import lila.api.Context
import lila.app.{ Env, _ }
import play.api.libs.json.{ JsArray, Json }
import play.api.mvc.Result
import lila.common.paginator.Paginator
import lila.resource.{ CapsuleWithPuzzleStatistics, DataForm, PuzzleStatistics, Capsule => CapsuleModel, CapsuleRepo }
import lila.user.UserRepo

object Capsule extends controllers.LilaController {

  def api = Env.resource.capsuleApi
  def puzzleEnv = Env.puzzle
  def puzzleApi = puzzleEnv.api
  def forms = Env.resource.forms
  def jsonView = Env.resource.CapsuleJsonView

  val maxSize = 60

  def mineList = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.capsule.searchForm.bindFromRequest
    searchForm.fold(
      fail => Ok(views.html.resource.capsule.list.mine(fail, List.empty[CapsuleModel], Set.empty[String])).fuccess,
      data => {
        for {
          mine <- CapsuleRepo.mine(data, me.id)
          mineTags <- CapsuleRepo.mineTags(me.id)
        } yield {
          Ok(views.html.resource.capsule.list.mine(searchForm, mine, mineTags))
        }
      }
    )
  }

  def memberList = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.capsule.searchForm.bindFromRequest
    searchForm.fold(
      fail => Ok(views.html.resource.capsule.list.member(fail, List.empty[CapsuleModel], Set.empty[String])).fuccess,
      data => {
        for {
          member <- CapsuleRepo.member(data, me.id)
          memberTags <- CapsuleRepo.memberTags(me.id)
        } yield {
          Ok(views.html.resource.capsule.list.member(searchForm, member, memberTags))
        }
      }
    )
  }

  def teamManagerList = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.capsule.searchForm.bindFromRequest
    searchForm.fold(
      fail => Ok(views.html.resource.capsule.list.teamManager(fail, List.empty[CapsuleModel], Set.empty[String])).fuccess,
      data => {
        for {
          coachTeamIdsByUser <- lila.team.MemberRepo.coachTeamIdsByUser(me.id)
          belongTeamOwners <- lila.team.TeamRepo.ownersOf(coachTeamIdsByUser.toList)
          teamManager <- CapsuleRepo.teamManager(data, belongTeamOwners)
          teamManagerTags <- CapsuleRepo.teamManagerTags(belongTeamOwners)
        } yield {
          Ok(views.html.resource.capsule.list.teamManager(searchForm, teamManager, teamManagerTags))
        }
      }
    )
  }

  def puzzleCapsule(capsuleId: String, page: Int) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(capsuleId)) { capsule =>
      CanRead(capsule) {
        puzzleEnv.resource.capsule(page, capsule.ids) map { pager =>
          Ok(views.html.resource.puzzle.capsule(pager, capsule))
        }
      }
    }
  }

  // 使用
  def mineOfChoose = Auth { implicit ctx => me =>
    for {
      mine <- CapsuleRepo.mine(DataForm.capsule.CapsuleSearchData(), me.id)
      mineTags <- CapsuleRepo.mineTags(me.id)
      member <- CapsuleRepo.member(DataForm.capsule.CapsuleSearchData(), me.id)
      memberTags <- CapsuleRepo.memberTags(me.id)
      coachTeamIdsByUser <- lila.team.MemberRepo.coachTeamIdsByUser(me.id)
      belongTeamOwners <- lila.team.TeamRepo.ownersOf(coachTeamIdsByUser.toList)
      teamManager <- CapsuleRepo.teamManager(DataForm.capsule.CapsuleSearchData(), belongTeamOwners)
      teamManagerTags <- CapsuleRepo.teamManagerTags(belongTeamOwners)
    } yield {
      val ids = get("ids").??(_.split(",").toList)
      Ok(views.html.resource.capsule.modal.capsuleModal(mine, member, teamManager, mineTags, memberTags, teamManagerTags, ids))
    }
  }

  // 使用
  def mineOfChooseForTeamTest = Auth { implicit ctx => me =>
    for {
      mine <- CapsuleRepo.mine(DataForm.capsule.CapsuleSearchData(), me.id)
      mineTags <- CapsuleRepo.mineTags(me.id)
    } yield {
      val ids = get("ids").??(_.split(",").toList)
      Ok(views.html.resource.capsule.modal.capsuleModalForTeamTest(mine, mineTags, ids))
    }
  }

  // 添加
  def mine = Auth { implicit ctx => me =>
    for {
      mine <- CapsuleRepo.mine(DataForm.capsule.CapsuleSearchData(status = Some(true)), me.id)
      member <- CapsuleRepo.member(DataForm.capsule.CapsuleSearchData(status = Some(true), role = "w".some), me.id)
    } yield Ok(views.html.resource.capsule.modal.capsuleModal(mine, member))
  }

  def mineApi = Auth { implicit ctx => me =>
    for {
      mine <- CapsuleRepo.mine(DataForm.capsule.CapsuleSearchData(), me.id)
      mineTags <- CapsuleRepo.mineTags(me.id)
      member <- CapsuleRepo.member(DataForm.capsule.CapsuleSearchData(), me.id)
      memberTags <- CapsuleRepo.memberTags(me.id)
      coachTeamIdsByUser <- lila.team.MemberRepo.coachTeamIdsByUser(me.id)
      belongTeamOwners <- lila.team.TeamRepo.ownersOf(coachTeamIdsByUser.toList)
      teamManager <- CapsuleRepo.teamManager(DataForm.capsule.CapsuleSearchData(), belongTeamOwners)
      teamManagerTags <- CapsuleRepo.teamManagerTags(belongTeamOwners)
    } yield {
      Ok(Json.obj(
        "mine" -> jsonView.listWithTags(mine, mineTags.toList),
        "member" -> jsonView.listWithTags(member, memberTags.toList),
        "teamManager" -> jsonView.listWithTags(teamManager, teamManagerTags.toList)
      )) as JSON
    }
  }

  def puzzlesApi(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanRead(capsule) {
        puzzleApi.puzzle.findMany2(capsule.ids) map { puzzles =>
          Ok(jsonView.puzzleList(toCapsulePuzzle(puzzles))) as JSON
        }
      }
    }
  }

  def infos = Auth { implicit ctx => me =>
    val idList = get("ids").??(_.split(",").toList).distinct take 10
    api.byIds(idList) flatMap { capsules =>
      val puzzleIds = capsules.foldLeft(List.empty[Int]) {
        case (all, capsule) => all ++ capsule.ids
      }
      puzzleApi.puzzle.findMany2(puzzleIds) map { puzzles =>
        Ok(jsonView.capsulesWithPuzzles(capsules, toCapsulePuzzle(puzzles)))
      }
    } map (_ as JSON)
  }

  def createForm = Auth { implicit ctx => me =>
    Ok(views.html.resource.capsule.form.create(forms.capsule.createForm(me))).fuccess
  }

  def create = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.capsule.createForm(me).bindFromRequest.fold(
      fail => Ok(views.html.resource.capsule.form.create(fail)).fuccess,
      data => api.create(data, me) inject Redirect(routes.Capsule.mineList)
    )
  }

  def updateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        puzzleApi.puzzle.findMany2(capsule.ids) map { puzzles =>
          Ok(views.html.resource.capsule.form.update(
            forms.capsule.updateFormOf(me, capsule),
            capsule,
            puzzles
          ))
        }
      }
    }
  }

  def update(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        implicit def req = ctx.body
        forms.capsule.updateForm(me, capsule.id.some).bindFromRequest.fold(
          fail => {
            puzzleApi.puzzle.findMany2(capsule.ids) map { puzzles =>
              Ok(views.html.resource.capsule.form.update(fail, capsule, puzzles))
            }
          },
          data => api.update(capsule, data, me) inject Redirect(routes.Capsule.updateForm(id))
        )
      }
    }
  }

  def remove(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        api.remove(capsule) inject Redirect(routes.Capsule.mineList)
      }
    }
  }

  def clone(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        api.clone(capsule, me) map { c =>
          Redirect(routes.Capsule.update(c.id))
        }
      }
    }
  }

  def createAndAddPuzzle = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    forms.capsule.createForm(me).bindFromRequest.fold(
      fail => BadRequest(jsonError(fail.toString)).fuccess,
      data => {
        val puzzleIds = get("puzzleIds").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
        api.create(data, me) flatMap { capsule =>
          api.addPuzzle(capsule, puzzleIds, me) inject Ok(Json.obj("ok" -> true, "id" -> capsule.id))
        }
      }
    )
  }

  def addPuzzle(id: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        Active(capsule) {
          val puzzleIds = get("puzzleIds").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
          if ((capsule.ids ++ puzzleIds).size > maxSize) {
            BadRequest(jsonError(s"每个列表最多添加${maxSize}道战术题")).fuccess
          } else {
            api.addPuzzle(capsule, puzzleIds, me) inject jsonOkResult
          }
        }
      }
    }
  }

  def delPuzzle(id: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        Active(capsule) {
          val puzzleIds = get("ids").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
          api.delPuzzle(capsule, puzzleIds, me) inject Redirect(routes.Capsule.updateForm(id))
        }
      }
    }
  }

  def reorderPuzzles(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        Active(capsule) {
          val puzzleIds = get("puzzleIds").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
          api.reorderPuzzles(capsule, puzzleIds, me) inject jsonOkResult
        }
      }
    }
  }

  def sortPuzzles(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanContribute(capsule) {
        Active(capsule) {
          implicit val req = ctx.body
          forms.capsule.sortForm.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            sort => {
              val puzzleIds = get("puzzleIds").??(_.split(",").toList).distinct.map(_.toInt) take maxSize
              puzzleApi.puzzle.findMany3(puzzleIds, sort = sort) flatMap { puzzles =>
                api.sortPuzzles(capsule, puzzles.map(_.id), me) inject jsonOkResult
              }
            }
          )
        }
      }
    }
  }

  def print(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      CanRead(capsule) {
        puzzleApi.puzzle.findMany2(capsule.ids) map { puzzles =>
          Ok(views.html.resource.capsule.print(capsule, puzzles))
        }
      }
    }
  }

  def puzzleStatistics = Auth { implicit ctx => me =>
    val ids = get("ids").??(_.split(",").toList)
    listWithStatistics(ids, me) map { list =>
      Ok(
        JsArray(
          list.map { cws =>
            Json.obj(
              "id" -> cws.capsule.id,
              "name" -> cws.capsule.name,
              "statistics" -> Json.obj(
                "puzzles" -> cws.puzzles,
                "total" -> cws.puzzles.size,
                "avgRating" -> cws.statistics.avgRating,
                "minRating" -> cws.statistics.minRating,
                "maxRating" -> cws.statistics.maxRating
              )

            )
          }
        )
      ) as JSON
    }
  }

  def listWithStatistics(ids: List[CapsuleModel.ID], me: lila.user.User): Fu[List[CapsuleWithPuzzleStatistics]] =
    ids.nonEmpty.?? {
      api.byIds(ids) flatMap { capsules =>
        val puzzleIds = capsules.flatMap(_.ids).distinct
        puzzleApi.puzzle.findMany4(puzzleIds) map { puzzles =>
          capsules.map { capsule =>
            val capsulePuzzles = puzzles.filter(p => capsule.ids.contains(p.id))
            val ignoreIptPuzzles = capsulePuzzles.filter(_.notImport)
            val total = ignoreIptPuzzles.size
            val ratings = ignoreIptPuzzles.map(_.intRating)
            val avgRating = if (total == 0) 0 else ratings.sum / total
            val minRating = if (total == 0) 0 else ratings.min
            val maxRating = if (total == 0) 0 else ratings.max
            CapsuleWithPuzzleStatistics(capsule, PuzzleStatistics(capsulePuzzles.size, avgRating, minRating, maxRating), capsulePuzzles.map(_.id))
          }
        }
      }
    }

  private def toCapsulePuzzle(puzzles: List[lila.puzzle.Puzzle]) =
    puzzles.map { puzzle =>
      lila.resource.CapsulePuzzle(
        id = puzzle.id,
        fen = puzzle.fenAfterInitialMove | "",
        color = puzzle.color.name,
        lastMove = puzzle.initialUci,
        lines = lila.puzzle.Line.toJson2(puzzle.lines).toString
      )
    }

  def memberRoles(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        for {
          users <- UserRepo.byIds(capsule.members.ids)
          markMap <- Env.team.api.userMarks(me.id)
        } yield views.html.resource.capsule.form.memberRoles(capsule, users, markMap, forms.capsule.visibilityForm.fill(capsule.visibility.id))
      }
    }
  }

  def setVisibility(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        implicit val req = ctx.body
        forms.capsule.visibilityForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          visibility => api.setVisibility(capsule, visibility, me) inject jsonOkResult
        )
      }
    }
  }

  def changeOwner(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        implicit val req = ctx.body
        forms.capsule.changeOwnerForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          username => UserRepo.byId(lila.user.User.normalize(username)) flatMap { memberOption =>
            memberOption.fold(fuccess(jsonOkResult)) { member =>
              api.changeOwner(capsule, member, me) inject jsonOkResult
            }
          }
        )
      }
    }
  }

  def addMember(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        implicit val req = ctx.body
        forms.capsule.addMemberForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          username => UserRepo.byId(lila.user.User.normalize(username)) flatMap { memberOption =>
            memberOption.fold(fuccess(jsonOkResult)) { member =>
              api.addMember(capsule, member) inject jsonOkResult
            }
          }
        )
      }
    }
  }

  def removeMember(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        UserRepo.byId(memberId) flatMap { memberOption =>
          memberOption.fold(fuccess(Redirect(routes.Capsule.memberRoles(id)))) { member =>
            api.removeMember(capsule, member) inject Redirect(routes.Capsule.memberRoles(id))
          }
        }
      }
    }
  }

  def setMemberRole(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      Owner(capsule) {
        implicit val req = ctx.body
        forms.capsule.setMemberRole.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          role => UserRepo.byId(memberId) flatMap { memberOption =>
            memberOption.fold(fuccess(Redirect(routes.Capsule.memberRoles(id)))) { member =>
              api.setMemberRole(capsule, member, role) inject Redirect(routes.Capsule.memberRoles(id))
            }
          }
        )
      }
    }
  }

  def quitMember(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { capsule =>
      WriterOrReader(capsule) {
        api.quitMember(capsule, me) inject Redirect(routes.Capsule.memberList())
      }
    }
  }

  private def Owner(capsule: CapsuleModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => capsule.isOwner(me.id))) f
    else ForbiddenResult
  }

  private def CanContribute(capsule: CapsuleModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => capsule.isContributor(me.id))) f
    else ForbiddenResult
  }

  private def CanRead(capsule: CapsuleModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    ctx.me match {
      case Some(me) => {
        if (capsule.members.contains(me.id)) f
        else {
          lila.team.MemberRepo.coachTeamIdsByUser(me.id) flatMap { coachTeamIds =>
            UserRepo.byId(capsule.ownerId) flatMap {
              case None => ForbiddenResult
              case Some(owner) => {
                owner.teamId match {
                  case None => ForbiddenResult
                  case Some(teamId) => {
                    if (coachTeamIds.contains(teamId)) f
                    else ForbiddenResult
                  }
                }
              }
            }
          }
        }
      }
      case None => ForbiddenResult
    }

  private def WriterOrReader(capsule: CapsuleModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => capsule.isWriter(me.id) || capsule.isReader(me.id))) f
    else ForbiddenResult
  }

  private def Active(capsule: CapsuleModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (capsule.active) f
    else ForbiddenResult
  }

  private def ForbiddenResult(implicit ctx: Context): Fu[Result] =
    Forbidden(views.html.site.message.authFailed).fuccess

}
