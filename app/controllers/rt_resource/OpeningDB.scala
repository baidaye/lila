package controllers.rt_resource

import lila.app._
import lila.api.Context
import lila.common.paginator.Paginator
import lila.member.{ Days, ProductRepo }
import lila.opening.OpeningDBOrder.OpeningDBOrders
import lila.opening.{ OpeningDBNodeRepo, OpeningDBRecordRepo, OpeningDBRepo }
import chess.format.{ FEN, Forsyth }
import chess.variant.Standard
import lila.user.UserRepo
import play.api.libs.json.Json
import play.api.mvc.Result
import views.html

object OpeningDB extends controllers.LilaController {

  private val api = Env.opening.api
  private val form = Env.opening.form
  private val jsonView = Env.opening.jsonView
  private val memberEnv = Env.member

  def minePage(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => Ok(html.resource.openingdb.list.minePage(Paginator.empty[lila.opening.OpeningDB], fail, false)).fuccess,
      data => {
        for {
          isCreateAccept <- isCreateAccept(me)
          pager <- api.minePage(me, page, data)
        } yield Ok(html.resource.openingdb.list.minePage(pager, searchForm, isCreateAccept))
      }
    )
  }

  def systemPage(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => fuccess {
        Ok(html.resource.openingdb.list.systemPage(Paginator.empty[lila.opening.OpeningDB], lila.opening.OpeningDBOrder.OpeningDBOrders(Nil), Set.empty[String], fail))
      },
      data => {
        for {
          tags <- api.systemTags()
          pager <- api.systemPage(me, page, data)
          orders <- lila.opening.OpeningDBOrderRepo.mine(me.id)
        } yield Ok(html.resource.openingdb.list.systemPage(pager, lila.opening.OpeningDBOrder.OpeningDBOrders(orders), tags, searchForm))
      }
    )
  }

  def systemBuyPage(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => fuccess {
        Ok(html.resource.openingdb.list.systemBuyPage(Paginator.empty[lila.opening.OpeningDB], lila.opening.OpeningDBOrder.OpeningDBOrders(Nil), Set.empty[String], fail))
      },
      data => {
        for {
          orders <- lila.opening.OpeningDBOrderRepo.mine(me.id)
          openingdbIds = orders.filterNot(_.expired).map(_.oid).distinct
          pager <- api.systemBuyPage(me, page, data, openingdbIds)
          tags <- api.systemTags()
        } yield Ok(html.resource.openingdb.list.systemBuyPage(pager, lila.opening.OpeningDBOrder.OpeningDBOrders(orders), tags, searchForm))
      }
    )
  }

  def memberPage(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => fuccess {
        Ok(html.resource.openingdb.list.memberPage(Paginator.empty[lila.opening.OpeningDB], fail))
      },
      data => {
        for {
          mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
          mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
          pager <- if (me.isMemberOrCoachOrTeam) api.memberPage(me, page, data, mineCoaches, mineTeamOwners) else fuccess(Paginator.empty[lila.opening.OpeningDB])
        } yield Ok(html.resource.openingdb.list.memberPage(pager, searchForm))
      }
    )
  }

  def visiblePage(page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.pagerSearch.bindFromRequest
    searchForm.fold(
      fail => fuccess {
        Ok(html.resource.openingdb.list.visiblePage(Paginator.empty[lila.opening.OpeningDB], fail))
      },
      data => {
        for {
          mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
          mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
          pager <- if (me.isMemberOrCoachOrTeam) api.visiblePage(me, page, data, mineCoaches, mineTeamOwners) else fuccess(Paginator.empty[lila.opening.OpeningDB])
        } yield Ok(html.resource.openingdb.list.visiblePage(pager, searchForm))
      }
    )
  }

  def createForm = Auth { implicit ctx => me =>
    isCreateAccept(me) map { accept =>
      if (accept) {
        Ok(views.html.resource.openingdb.form.create(form.createFormOf))
      } else NotAcceptable("用户创建数量超过上限")
    }
  }

  def create = AuthBody { implicit ctx => me =>
    isCreateAccept(me) flatMap { accept =>
      if (accept) {
        implicit def req = ctx.body
        form.createForm.bindFromRequest.fold(
          fail => BadRequest(html.resource.openingdb.form.create(fail)).fuccess,
          data => api.create(me, data) inject Redirect(routes.OpeningDB.minePage(1))
        )
      } else fuccess(NotAcceptable("用户创建数量超过上限"))
    }
  }

  def updateForm(id: String) = Auth { implicit ctx => me =>
    OptionResult(api.byId(id)) { opening =>
      Ok(html.resource.openingdb.form.update(opening, form.updateFormOf(opening)))
    }
  }

  def update(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanWrite(openingdb) {
        implicit def req = ctx.body
        form.updateForm.bindFromRequest.fold(
          fail => BadRequest(html.resource.openingdb.form.update(openingdb, fail)).fuccess,
          data => api.update(me, openingdb, data) inject jsonOkResult
        )
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      val initialFen = FEN(openingdb.initialFen).some
      val pov = controllers.UserAnalysis.makePov(initialFen, Standard)
      for {
        roundJson <- Env.api.roundApi.userAnalysisJson(pov, ctx.pref, initialFen, pov.color, owner = false, me = ctx.me)
        mineOrders <- lila.opening.OpeningDBOrderRepo.mine(me.id)
        mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
        mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
        orders = OpeningDBOrders(mineOrders)
      } yield {
        if (openingdb.canRead(me, orders, mineCoaches, mineTeamOwners)) {
          Ok(html.resource.openingdb.show(
            openingdb,
            jsonView.openingdb(openingdb),
            None,
            roundJson,
            jsonView.pref(ctx.pref)
          ))
        } else {
          Ok(html.resource.openingdb.cantRead(openingdb, orders))
        }
      }
    }
  }

  def showNode(id: String, nodeId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        val data = lila.importer.ImportData(openingdbNode.pgn, none)
        Env.importer.importer.inMemory(data).fold(
          _ => {
            val pov = controllers.UserAnalysis.makePov(None, Standard)
            Env.api.roundApi.userAnalysisJson(pov, ctx.pref, None, chess.White, owner = false, me = ctx.me) map { roundJson =>
              Ok(html.resource.openingdb.show(
                openingdb,
                jsonView.openingdb(openingdb),
                None,
                roundJson,
                jsonView.pref(ctx.pref)
              ))
            }
          }, {
            case (game, fen) => {
              val pov = lila.game.Pov(game, chess.White)
              for {
                roundJson <- Env.api.roundApi.userAnalysisJson(pov, ctx.pref, None, chess.White, owner = false, me = ctx.me)
                nodes <- api.pgnNodes(openingdbNode)
              } yield Ok(html.resource.openingdb.show(
                openingdb,
                jsonView.openingdb(openingdb),
                jsonView.nodesWithPath(nodes).some,
                roundJson,
                jsonView.pref(ctx.pref)
              ))
            }
          }
        )
      }
    }
  }

  def nextNodes(id: String, fen: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      val epd = Forsyth.toEPD(fen)
      OpeningDBNodeRepo.findNextNodes(id, epd) map { nodes =>
        Ok(jsonView.nodesWithDests(nodes)) as JSON
      }
    }
  }

  def transferNodes(id: String, fen: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      val epd = Forsyth.toEPD(fen)
      OpeningDBNodeRepo.transferNodes(id, epd) map { nodes =>
        Ok(jsonView.nodesJson(nodes)) as JSON
      }
    }
  }

  def searchNodes(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      implicit def req = ctx.body
      form.nodeSearch.bindFromRequest.fold(
        fail => BadRequest(jsonError(fail.toString)).fuccess,
        data => OpeningDBNodeRepo.searchNodes(id, data) map { nodes =>
          Ok(jsonView.nodesJson(nodes)) as JSON
        }
      )
    }
  }

  def addNodes(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanWrite(openingdb) {
        implicit def req = ctx.body
        form.addNodesForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          data => {
            if (openingdb.isFull) BadRequest(jsonError(s"max nodes: ${lila.opening.OpeningDB.maxNode}")).fuccess
            else {
              api.addNodes(openingdb, data, me) map { nodes =>
                Ok(jsonView.nodesJson(nodes)) as JSON
              }
            }
          }
        )
      }
    }
  }

  def addNode(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanWrite(openingdb) {
        implicit def req = ctx.body
        form.addNodeForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          data => {
            if (openingdb.isFull) BadRequest(jsonError(s"max nodes: ${lila.opening.OpeningDB.maxNode}")).fuccess
            else {
              if (lila.opening.PgnParser.validMaxTurns(data.pgn)) {
                api.addNode(openingdb, data, me) map { node =>
                  Ok(jsonView.nodeJson(node)) as JSON
                }
              } else BadRequest(jsonError(s"最多保存${lila.opening.OpeningDBNode.maxTurns}个回合")).fuccess
            }
          }
        )
      }
    }
  }

  def deleteNode(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          api.deleteNode(openingdb, openingdbNode) inject jsonOkResult
        }
      }
    }
  }

  def setClean(id: String) = AuthBody { implicit ctx => me =>
    if (me.isMemberOrCoachOrTeam) {
      OptionFuResult(api.byId(id)) { openingdb =>
        CanWrite(openingdb) {
          api.setClean(openingdb, 1, me.id) inject jsonOkResult
        }
      }
    } else NotAcceptable(html.member.notAccept("会员升级")).fuccess
  }

  def copyFrom(fr: String, to: String) = AuthBody { implicit ctx => me =>
    if (me.isMemberOrCoachOrTeam) {
      OptionFuResult(for {
        frOpeningdbOption <- api.byId(fr)
        toOpeningdbOption <- api.byId(to)
      } yield (frOpeningdbOption |@| toOpeningdbOption).tupled) {
        case (frOpeningdb, toOpeningdb) => {
          CanWrite(toOpeningdb) {
            lila.opening.OpeningDBOrderRepo.mine(me.id) flatMap { mineOrders =>
              val orders = OpeningDBOrders(mineOrders)
              if (frOpeningdb.canBeCopy(me, orders)) {
                api.copyFrom(frOpeningdb, toOpeningdb) inject jsonOkResult
              } else Forbidden(jsonError("Forbidden")).fuccess
            }
          }
        }
      }
    } else NotAcceptable(html.member.notAccept("会员升级")).fuccess
  }

  def setNodeName(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          implicit def req = ctx.body
          form.nodeName.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            name => api.setNodeName(openingdb, openingdbNode, name) inject jsonOkResult
          )
        }
      }
    }
  }

  def setNodeShortName(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          implicit def req = ctx.body
          form.nodeShortName.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            shortName => api.setNodeShortName(openingdb, openingdbNode, shortName) inject jsonOkResult
          )
        }
      }
    }
  }

  def setNodeComment(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          implicit def req = ctx.body
          form.nodeComment.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            comment => api.setNodeComment(openingdb, openingdbNode, comment, me) map { comments =>
              import play.api.libs.json.Json
              import lila.tree.Node.commentsWriter
              val j = Json.obj("comments" -> comments)
              Ok(jsonOkBody ++ j) as JSON
            }
          )
        }
      }
    }
  }

  def setNodeShapes(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          implicit def req = ctx.body
          form.nodeShape.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            shapesJson => api.setNodeShapes(openingdb, openingdbNode, shapesJson) map { shapes =>
              import play.api.libs.json.Json
              import lila.tree.Node.shapesWrites
              val j = Json.obj("shapes" -> shapes)
              Ok(jsonOkBody ++ j) as JSON
            }
          )
        }
      }
    }
  }

  def toggleNodeGlyph(id: String, nodeId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbNodeOption <- OpeningDBNodeRepo.byId(nodeId)
    } yield (openingdbOption |@| openingdbNodeOption).tupled) {
      case (openingdb, openingdbNode) => {
        CanWrite(openingdb) {
          implicit def req = ctx.body
          form.nodeGlyph.bindFromRequest.fold(
            fail => BadRequest(jsonError(fail.toString)).fuccess,
            glyphId => {
              api.toggleNodeGlyph(openingdb, openingdbNode, glyphId) map { glyphs =>
                import play.api.libs.json.Json
                import lila.tree.Node.glyphsWriter
                val j = Json.obj("glyphs" -> glyphs)
                Ok(jsonOkBody ++ j) as JSON
              }
            }
          )
        }
      }
    }
  }

  def recordPage(id: String, page: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      for {
        mineOrders <- lila.opening.OpeningDBOrderRepo.mine(me.id)
        mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
        mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
        orders = OpeningDBOrders(mineOrders)
        res <- (
          if (openingdb.canRead(me, orders, mineCoaches, mineTeamOwners)) {
            implicit def req = ctx.body
            val f = form.recordSearchForm.bindFromRequest
            f.bindFromRequest.fold(
              fail => BadRequest(html.resource.openingdb.record(openingdb, fail, Paginator.empty[lila.opening.OpeningDBRecord])).fuccess,
              data => OpeningDBRecordRepo.page(page, openingdb, data) map { pager =>
                Ok(html.resource.openingdb.record(openingdb, f, pager))
              }
            )
          } else {
            Ok(html.resource.openingdb.cantRead(openingdb, orders)).fuccess
          }
        )
      } yield res
    }
  }

  def recordRemove(id: String, recordId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      openingdbRecordOption <- OpeningDBRecordRepo.byId(recordId)
    } yield (openingdbOption |@| openingdbRecordOption).tupled) {
      case (openingdb, openingdbRecord) => {
        (openingdb.isOwner(me.id) || openingdbRecord.creator(me)).?? {
          OpeningDBRecordRepo.remove(recordId) inject Redirect(routes.OpeningDB.recordPage(id, 1))
        }
      }
    }
  }

  def mineApi() = Auth { implicit ctx => me =>
    for {
      mineCoaches <- Env.coach.studentApi.mineCoach(ctx.me)
      mineTeamOwners <- Env.team.api.mineTeamOwner(ctx.me)
      mineList <- OpeningDBRepo.mineList(me.id)
      memberList <- me.isMemberOrCoachOrTeam.?? { OpeningDBRepo.memberList(me.id, mineCoaches, mineTeamOwners) }
      visibleList <- me.isMemberOrCoachOrTeam.?? { OpeningDBRepo.visibleList(me.id, mineCoaches, mineTeamOwners) }
      systemList <- api.systemBuyList(me)
    } yield Ok(jsonView.mineApi(mineList.filter(_.notEmpty), memberList.filter(_.notEmpty), visibleList.filter(_.notEmpty), systemList.filter(_.notEmpty))) as JSON
  }

  def nimamadewen() = Auth { implicit ctx => me =>
    for {
      mineList <- OpeningDBRepo.mineList(me.id)
      systemList <- api.systemBuyList(me, true)
    } yield Ok(jsonView.mineApi2(mineList.filter(_.notEmpty), systemList.filter(_.notEmpty))) as JSON
  }

  def systemSettingModal(id: String) = SecureBody(_.SuperAdmin) { implicit ctx => me =>
    OptionResult(api.byId(id)) { openingdb =>
      val f = form.systemSettingFormOf(openingdb)
      Ok(html.resource.openingdb.modal.systemSettingModal(openingdb, f))
    }
  }

  def systemSetting(id: String) = SecureBody(_.SuperAdmin) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      implicit def req = ctx.body
      val f = form.systemSettingForm.bindFromRequest
      f.bindFromRequest.fold(
        fail => BadRequest(jsonError(fail.toString)).fuccess,
        data => api.systemSetting(openingdb, data) inject Redirect(routes.OpeningDB.systemPage(1))
      )
    }
  }

  def members(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        for {
          users <- UserRepo.byIds(openingdb.members.ids)
          markMap <- Env.team.api.userMarks(me.id)
        } yield html.resource.openingdb.members(openingdb, users, markMap, form.visibilityForm.fill(openingdb.visibility.key))
      }
    }
  }

  def memberChooseForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        for {
          markMap <- Env.team.api.userMarks(me.id)
          members <- UserRepo.byIds(openingdb.members.ids)
          coachStudents <- me.isCoach.?? { Env.coach.studentApi.mineStudentsWithUser(me.id).map(_.map(_.user)) }
          teamMembers <- me.isTeam.?? { lila.team.MemberRepo.allMembersWithUser(me.teamIdValue).map(_.map(_.user)) }
        } yield {
          val memberIds = members.map(_.id)
          val all = (coachStudents ++ teamMembers).filter(_.isMemberOrCoachOrTeam).filterNot { m => memberIds.contains(m.id) }
          Ok(html.resource.openingdb.modal.memberChoose(openingdb, all, members, markMap))
        }
      }
    }
  }

  def memberChoose(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        implicit val req = ctx.body
        form.membersForm.bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          members => {
            val oldMembers = openingdb.members.ids.toList
            val newMembers = split(members).filterNot(oldMembers.contains(_))
            api.addMembers(openingdb, newMembers) inject jsonOkResult
          }
        )
      }
    }
  }

  private def split(str: String) = if (str.isEmpty) Nil else str.split(",").toList

  def addMember(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        implicit val req = ctx.body
        form.addMemberForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          username => {
            for {
              memberOption <- UserRepo.byId(lila.user.User.normalize(username))
              coachStudents <- me.isCoach.?? { Env.coach.studentApi.mineStudentsWithUser(me.id).map(_.map(_.user).filter(_.isMemberOrCoachOrTeam)) }
              teamMembers <- me.isTeam.?? { lila.team.MemberRepo.allMembersWithUser(me.teamIdValue).map(_.map(_.user).filter(_.isMemberOrCoachOrTeam)) }
              res <- memberOption.fold(fuccess(BadRequest(Json.obj("error" -> "用户不存在")))) { member =>
                if (member.isMemberOrCoachOrTeam && (coachStudents ++ teamMembers).contains(member)) {
                  api.addMember(openingdb, member) inject jsonOkResult
                } else BadRequest(Json.obj("error" -> "用户不是会员/俱乐部成员/教练学员")).fuccess
              }
            } yield res
          }
        )
      }
    }
  }

  def removeMember(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        UserRepo.byId(memberId) flatMap { memberOption =>
          memberOption.fold(fuccess(Redirect(routes.OpeningDB.members(id)))) { member =>
            api.removeMember(openingdb, member) inject Redirect(routes.OpeningDB.members(id))
          }
        }
      }
    }
  }

  def setMemberRole(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        implicit val req = ctx.body
        form.setMemberRole.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          role => UserRepo.byId(memberId) flatMap { memberOption =>
            memberOption.fold(fuccess(Redirect(routes.OpeningDB.members(id)))) { member =>
              api.setMemberRole(openingdb, member, role) inject Redirect(routes.OpeningDB.members(id))
            }
          }
        )
      }
    }
  }

  def setVisibility(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { openingdb =>
      CanAuth(openingdb) {
        implicit val req = ctx.body
        form.visibilityForm.bindFromRequest.fold(
          fail => BadRequest(jsonError(fail.toString)).fuccess,
          visibility => api.setVisibility(openingdb, visibility, me) inject jsonOkResult
        )
      }
    }
  }

  def toBuySystem(id: String, d: Option[String]) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      openingdbOption <- api.byId(id)
      productOption <- openingdbOption.?? { openingdb => lila.member.ProductRepo.byAttr("attrs.openingdbId", openingdb.id) }
    } yield (openingdbOption |@| productOption).tupled) {
      case (openingdb, product) => {
        for {
          orders <- lila.opening.OpeningDBOrderRepo.mine(me.id)
          discounts <- controllers.Member.memberDiscounts
          eio <- memberEnv.orderApi.existsInviteOrder(me.id, lila.member.ProductType.SystemOpeningdb.id)
        } yield {
          if (openingdb.isBuyForever(me, OpeningDBOrders(orders))) BadRequest("已经购买过。")
          else {
            val dayExist = d.isEmpty || d.??(List(Days.Day7, Days.Forever).map(_.id).contains(_))
            if (dayExist) {
              val defaultItemCode = d.map(lila.opening.OpeningDB.toProductItemCode(openingdb.id, _))
              val form = memberEnv.form.order(me, discounts)
              Ok(html.resource.openingdb.systemBuy(me, product, discounts, eio, defaultItemCode, form))
            } else BadRequest("憋瞎搞~")
          }
        }
      }
    }
  }

  def toBuyCreate() = Auth { implicit ctx => me =>
    val produceId = "9Hx7pZ3a"
    OptionFuResult(lila.member.ProductRepo.byId(produceId)) { product =>
      for {
        discounts <- controllers.Member.memberDiscounts
        eio <- memberEnv.orderApi.existsInviteOrder(me.id, lila.member.ProductType.SystemOpeningdb.id)
      } yield {
        val form = memberEnv.form.order(me, discounts)
        Ok(html.resource.openingdb.createBuy(me, product, discounts, eio, form))
      }
    }
  }

  private def CanAuth(openingdb: lila.opening.OpeningDB)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => openingdb.isOwner(me.id) && me.isCoachOrTeam)) f
    else fuccess(Forbidden(jsonError("Forbidden")) as JSON)
  }

  private def CanWrite(openingdb: lila.opening.OpeningDB)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => openingdb.canWrite(me.id))) f
    else fuccess(Forbidden(jsonError("Forbidden")) as JSON)
  }

  def product(id: String) = Auth { implicit ctx => me =>
    ProductRepo.byId(id).map { p =>
      Ok(p.toString)
    }
  }

  private def isCreateAccept(user: lila.user.User): Fu[Boolean] = {
    if (user.isCoachOrTeam) fuccess(true)
    else {
      val maxCreateCount = user.memberLevel.permissions.createOpeningdb
      OpeningDBRepo.mineList(user.id) map { list =>
        list.size < maxCreateCount
      }
    }
  }

}
