package controllers.rt_resource

import lila.app._
import lila.common.paginator.Paginator
import lila.game.GameRepo
import lila.resource.GameDBRel
import views.html

object GameDB extends controllers.LilaController {

  private val env = Env.resource
  private val form = env.forms
  private val api = env.gamedbApi

  val maxRelSize = 100

  def home() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      def searchForm = form.gamedb.search
      searchForm.bindFromRequest.fold(
        fail => Ok(html.resource.gamedb.home(fail, me, Paginator.empty, Set.empty[String], me.id)).fuccess,
        data => {
          val selected = get("selected") | me.id
          for {
            //            children <- env.gamedbApi.findAllChildren(me.id, selected)
            //            childrenIds = children.map(_.id)
            rels <- env.gamedbRelApi.findByIds(selected, data, me.id, maxRelSize)
            tags <- env.gamedbRelApi.tagsByUser(me.id)
            pager <- env.gamedbRelApi.page(rels)
          } yield {
            Ok(html.resource.gamedb.home(searchForm fill data, me, pager, tags, selected))
          }
        }
      )
    }
  }

  def loadFolderNode() = Auth { implicit ctx => me =>
    val selected = get("selected") | me.id
    env.gamedbApi.loadFolderNode(me.id, selected) map { json =>
      Ok(json) as JSON
    }
  }

  def loadAllNode() = Auth { implicit ctx => me =>
    val selected = get("selected")
    env.gamedbApi.loadAllNode(me.id, selected) map { json =>
      Ok(json) as JSON
    }
  }

  def createNode() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.gamedb.create.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        data => api.create(data.parent, data.name, data.sort, me.id).map { gameDB =>
          Ok(jsonOkBody.add("id", gameDB.id.some))
        }
      )
    }
  }

  def renameNode(id: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.gamedb.rename.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        name => api.rename(id, name).map { _ =>
          jsonOkResult
        }
      )
    }
  }

  def removeNode(id: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      api.remove(me.id, id).map { _ =>
        jsonOkResult
      }
    }
  }

  def moveNode(id: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.gamedb.move.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        data => data match {
          case (parent, children) => api.move(id, parent, children, me.id).map { _ =>
            jsonOkResult
          }
        }
      )
    }
  }

  def createGameForm(gamedbId: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      Ok(html.resource.gamedb.modal.createModal(
        gamedbId,
        form.gameDBRel.create(
          me,
          Env.study.api.byId
        )
      )).fuccess
    }
  }

  def createGame(gamedbId: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit val req = ctx.body
      form.gameDBRel.create(me, Env.study.api.byId).bindFromRequest.fold(
        jsonFormError,
        data =>
          env.gamedbRelApi.countByDb(gamedbId).flatMap { count =>
            if (count >= maxRelSize) {
              fuccess(BadRequest(jsonError(s"目录最多添加${maxRelSize}个对局")) as JSON)
            } else {
              env.gamedbRelApi.create(gamedbId, data, me.id).map { pr =>
                Ok(jsonOkBody.add("id" -> pr.id.some))
              } map (_ as JSON)
            }
          }
      )
    }
  }

  def editGameForm(relId: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      OptionResult(env.gamedbRelApi.byId(relId)) { rel =>
        Ok(html.resource.gamedb.modal.editModal(
          relId,
          form.gameDBRel.editOf(rel)
        ))
      }
    }
  }

  def updateGame(relId: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      OptionFuResult(env.gamedbRelApi.byId(relId)) { rel =>
        implicit val req = ctx.body
        form.gameDBRel.edit.bindFromRequest.fold(
          jsonFormError,
          data => env.gamedbRelApi.update(relId, data.name, data.tagsSet).map { _ =>
            jsonOkResult
          }
        ).map(_ as JSON)
      }
    }
  }

  def removeGame = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      val idList = get("ids").??(_.split(",").toList).distinct take maxRelSize
      implicit val req = ctx.body
      env.gamedbRelApi.remove(idList, me.id).map { _ =>
        jsonOkResult
      }
    }
  }

  def copyOrMoveModal(source: String, action: String, frGamedb: Option[String]) = Auth { implicit ctx => me =>
    Resource.Permiss {
      val idList = get("ids").??(_.split(",").toList).distinct take maxRelSize
      env.gamedbApi.loadFolderNode(me.id, frGamedb | me.id).map { treeJson =>
        Ok(html.resource.gamedb.modal.copyOrMoveModal(source, action, frGamedb, idList, treeJson))
      }
    }
  }

  def copyOrMove() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit val req = ctx.body
      form.gameDBRel.copyOrMove.bindFromRequest.fold(
        jsonFormError,
        data => {
          env.gamedbRelApi.countByDb(data.toGamedb).flatMap { count =>
            if (count + data.relIds.size > maxRelSize) {
              fuccess(BadRequest(jsonError(s"单个目录最多添加${maxRelSize}个对局")) as JSON)
            } else {
              data.source match {
                case "gamedb" => env.gamedbRelApi.toCopyRels(data).flatMap { rels =>
                  env.gamedbRelApi.copyOrMove(me.id, data, rels, env.gamedbRelApi.remove)
                }
                case "share" => env.gameShareRelApi.toCopyRels(data, me.id).flatMap { rels =>
                  env.gamedbRelApi.copyOrMove(me.id, data, rels, env.gameShareRelApi.remove)
                }
                case "import" => importToCopyRels(data, me.id).flatMap { rels =>
                  env.gamedbRelApi.copyOrMove(me.id, data, rels, GameRepo.removeByIds)
                }
                case "like" => likeToCopyRels(data, me.id).flatMap { rels =>
                  env.gamedbRelApi.copyOrMove(me.id, data, rels, Env.bookmark.api.removeByGameIdsAndUser)
                }
                case _ => funit
              }
            } map (_ => jsonOkResult)
          }
        }
      )
    }
  }

  private def likeToCopyRels(data: form.gameDBRel.CopyOrMoveData, userId: lila.user.User.ID): Fu[List[GameDBRel]] =
    Env.bookmark.api.findByGameIdsAndUser(data.relIds, userId).map { list =>
      list.map { bookmark =>
        GameDBRel.make(
          name = "来自收藏的对局",
          tags = bookmark.t,
          gamedbId = data.toGamedb,
          gameId = bookmark.g,
          userId = userId
        )
      }
    }

  private def importToCopyRels(data: form.gameDBRel.CopyOrMoveData, userId: lila.user.User.ID): Fu[List[GameDBRel]] =
    GameRepo.gamesFromSecondary(data.relIds).map { list =>
      list.map { game =>
        game.pgnImport.map { ipt =>
          GameDBRel.make(
            name = "来自导入的对局",
            tags = ipt.tags,
            gamedbId = data.toGamedb,
            gameId = game.id,
            userId = userId
          )
        }
      }.filter(_.isDefined).map(_.get)
    }

}
