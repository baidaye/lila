package controllers.rt_resource

import lila.app._
import lila.common.paginator.Paginator
import lila.interest.{ InterestLikeRepo, InterestRepo, ResourceSearch, Source }

object Interest extends controllers.LilaController {

  private val env = Env.interest
  private val form = lila.interest.DataForm

  def home(sc: String) = AuthBody { implicit ctx => me =>
    fuccess {
      Redirect(s"${routes.Interest.page(sc, 1)}?q=${ResourceSearch.default(Source(sc)).toUrlParam}")
    }
  }

  def page(sc: String, page: Int) = AuthBody { implicit ctx => me =>
    val source = Source(sc)
    if (me.hasResource) {
      implicit def req = ctx.body
      def searchForm = form.resourceSearch(source)
      searchForm.bindFromRequest.fold(
        fail => Ok(views.html.resource.interest.home(source, fail, Paginator.empty)).fuccess,
        data => {
          for {
            pager <- InterestRepo.resourcePage(source, page, data)
          } yield {
            Ok(views.html.resource.interest.home(source, searchForm fill data, pager))
          }
        }
      )
    } else {
      Ok(views.html.resource.interest.home(source, form.resourceSearch(source), Paginator.empty)).fuccess
    }
  }

  def likePage(sc: String, page: Int) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.likedSearch
    val source = Source(sc)
    searchForm.bindFromRequest.fold(
      fail => Ok(views.html.resource.liked.interest(source, fail, Paginator.empty, Set())).fuccess,
      data => {
        for {
          tags <- InterestLikeRepo.tags(me.id, source)
          pager <- InterestRepo.likePage(source, me.id, page, data)
        } yield {
          Ok(views.html.resource.liked.interest(source, searchForm fill data, pager, tags))
        }
      }
    )
  }

  def like(sc: String) = AuthBody { implicit ctx => implicit me =>
    Resource.Permiss {
      val idList = get("ids").??(_.split(",").toList).distinct.map(_.toInt) take 60
      InterestLikeRepo.likeByIds(idList, me.id, Source(sc)) inject jsonOkResult
    }
  }

  def unlike(sc: String) = AuthBody { implicit ctx => implicit me =>
    val idList = get("ids").??(_.split(",").toList).distinct.map(_.toInt) take 60
    InterestLikeRepo.removeByIds(idList, me.id, Source(sc)) inject Redirect(routes.Interest.likePage(sc, 1))
  }

}
