package controllers.rt_resource

import lila.app._
import lila.user.UserRepo
import views.html

object GameShare extends controllers.LilaController {

  private val env = Env.resource
  private val form = env.forms

  def relPage(page: Int) = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    val searchForm = form.gameShare.logSearch
    searchForm.bindFromRequest.fold(
      _ => fuccess(BadRequest),
      data => for {
        pager <- env.gameShareApi.relPage(me.id, data, page)
        shareUsers <- UserRepo.usersFromSecondary(pager.currentPageResults.map(_.createBy).distinct)
        markMap <- Env.team.api.userMarks(me.id)
      } yield Ok(html.resource.gamedb.share(searchForm fill data, pager, shareUsers, markMap))
    )
  }

  def rels(shareId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.gameShareApi.byId(shareId)) { share =>
      implicit val req = ctx.body
      val searchForm = form.gameShare.search
      searchForm.bindFromRequest.fold(
        _ => fuccess(BadRequest),
        data => for {
          list <- env.gameShareRelApi.list(shareId, data)
          tags <- env.gameShareRelApi.tagsShare(shareId)
          shareByMark <- Env.team.api.userMark(me.id, share.createBy)
        } yield Ok(html.resource.gamedb.share.games(searchForm fill data, share, list, tags, shareByMark))
      )
    }
  }

  def logPage(page: Int) = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    val searchForm = form.gameShare.logSearch
    searchForm.bindFromRequest.fold(
      _ => fuccess(BadRequest),
      data => env.gameShareApi.page(me.id, data, page) map { pager =>
        Ok(html.resource.gamedb.shareLog(searchForm fill data, pager))
      }
    )
  }

  def logInfo(shareId: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.gameShareApi.byId(shareId)) { share =>
      for {
        students <- UserRepo.usersFromSecondary(share.students)
        rels <- env.gamedbRelApi.byIds(share.rels.map(_.id))
        markMap <- Env.team.api.userMarks(me.id)
      } yield Ok(html.resource.gamedb.shareLog.info(share, students, rels, markMap))
    }
  }

  def removeGame = AuthBody { implicit ctx => me =>
    val idList = get("ids").??(_.split(",").toList).distinct take GameDB.maxRelSize
    implicit val req = ctx.body
    env.gameShareRelApi.remove(idList, me.id).map { _ =>
      jsonOkResult
    }
  }

  def shareModal() = Auth { implicit ctx => me =>
    Resource.TeamOrCoach {
      val idList = get("ids").??(_.split(",").toList).distinct take GameDB.maxRelSize
      Env.clazz.api.mine(me.id).map(_.filter(c => c.isCoach(me.id))).map { clazzs =>
        val clazzSeq = clazzs.map(c => c.id -> c.name) :+ ("others" -> "其它学员")
        Ok(html.resource.gamedb.modal.shareModal(form.gameShare.share, idList, clazzSeq))
      }
    }
  }

  def share() = AuthBody { implicit ctx => me =>
    Resource.TeamOrCoach {
      implicit val req = ctx.body
      form.gameShare.share.bindFromRequest.fold(
        jsonFormError,
        data => env.gameShareApi.create(data, me.id).map { _ =>
          jsonOkResult
        }
      ).map(_ as JSON)
    }
  }

}
