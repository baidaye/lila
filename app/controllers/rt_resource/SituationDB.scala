package controllers.rt_resource

import lila.app._
import lila.common.paginator.Paginator
import play.api.libs.json.{ JsArray, JsString, Json }
import views.html

object SituationDB extends controllers.LilaController {

  private val env = Env.resource
  private val form = env.forms

  val maxRelSize = 100

  def home() = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.situationdb.search
    searchForm.bindFromRequest.fold(
      fail => Ok(html.resource.situationdb.home(fail, Paginator.empty, List.empty[String], me.id, none, none)).fuccess,
      data => {
        val selected = get("selected") | me.id
        env.situationdbApi.byId(selected).flatMap { opt =>
          val relSortBy = opt.map(_.relSortBy) | lila.resource.SituationDB.SortBy.default.some
          for {
            rels <- env.situationdbRelApi.findByDB(selected, data, me.id, maxRelSize, relSortBy)
            tags <- env.situationdbRelApi.tagsByUser(me.id)
            pager <- env.situationdbRelApi.page(rels)
          } yield {
            Ok(html.resource.situationdb.home(searchForm fill data, pager, tags.toList, selected, get("fen"), relSortBy))
          }
        }
      }
    )
  }

  def loadFolderNode() = Auth { implicit ctx => me =>
    val selected = get("selected") | me.id
    env.situationdbApi.loadFolderNode(me.id, selected) map { json =>
      Ok(json) as JSON
    }
  }

  def createNode() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.situationdb.create.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        data => env.situationdbApi.create(data.parent, data.name, data.sort, me.id).map { situationdb =>
          Ok(jsonOkBody.add("id", situationdb.id.some))
        }
      )
    }
  }

  def renameNode(id: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.situationdb.rename.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        name => env.situationdbApi.rename(id, name).map { _ =>
          jsonOkResult
        }
      )
    }
  }

  def removeNode(id: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      env.situationdbApi.remove(me.id, id).map { _ =>
        jsonOkResult
      }
    }
  }

  def moveNode(id: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      form.situationdb.move.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        data => data match {
          case (parent, children) => env.situationdbApi.move(id, parent, children, me.id).map { _ =>
            jsonOkResult
          }
        }
      )
    }
  }

  def createForm(situationdbId: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      val fen = get("fen")
      Ok(html.resource.situationdb.modal.createModal(
        situationdbId,
        form.situationdbRel.createOf(fen),
        fen
      )).fuccess
    }
  }

  def createFormForEditor(fen: String, looksLegit: Boolean) = Auth { implicit ctx => me =>
    Resource.Permiss {
      Ok(html.board.editor.situationCreate(
        form.situationdbRel.createOf(fen.some, looksLegit.some)
      )).fuccess
    }
  }

  def create(situationdbId: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit val req = ctx.body
      form.situationdbRel.create.bindFromRequest.fold(
        jsonFormError,
        data =>
          env.situationdbRelApi.countByDb(situationdbId).flatMap { count =>
            if (count >= maxRelSize) {
              fuccess(BadRequest(jsonError(s"目录最多添加${maxRelSize}个局面")) as JSON)
            } else {
              env.situationdbRelApi.create(situationdbId, data, me.id).map { pr =>
                Ok(jsonOkBody.add("id" -> pr.id.some))
              } map (_ as JSON)
            }
          }
      )
    }
  }

  def editForm(relId: String) = Auth { implicit ctx => me =>
    Resource.Permiss {
      OptionResult(env.situationdbRelApi.byId(relId)) { rel =>
        Ok(html.resource.situationdb.modal.editModal(
          relId,
          form.situationdbRel.editOf(rel)
        ))
      }
    }
  }

  def update(relId: String) = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      OptionFuResult(env.situationdbRelApi.byId(relId)) { rel =>
        implicit val req = ctx.body
        form.situationdbRel.edit.bindFromRequest.fold(
          jsonFormError,
          data => env.situationdbRelApi.update(relId, data).map { _ =>
            jsonOkResult
          }
        ).map(_ as JSON)
      }
    }
  }

  def remove = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      val idList = get("ids").??(_.split(",").toList).distinct take maxRelSize
      implicit val req = ctx.body
      env.situationdbRelApi.remove(idList, me.id).map { _ =>
        jsonOkResult
      }
    }
  }

  def copyOrMoveModal(action: String, frSituationDB: Option[String]) = Auth { implicit ctx => me =>
    Resource.Permiss {
      val idList = get("ids").??(_.split(",").toList).distinct take maxRelSize
      env.situationdbApi.loadFolderNode(me.id, frSituationDB | me.id).map { treeJson =>
        Ok(html.resource.situationdb.modal.moveModal(action, frSituationDB, idList, treeJson))
      }
    }
  }

  def copyOrMove() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit val req = ctx.body
      form.situationdbRel.move.bindFromRequest.fold(
        jsonFormError,
        data => {
          env.situationdbRelApi.toCopyRels(data).flatMap { rels =>
            env.situationdbRelApi.countByDb(data.toSituationdb).flatMap { count =>
              if (count + rels.size > maxRelSize) {
                fuccess(BadRequest(jsonError(s"单个目录最多添加${maxRelSize}个局面")) as JSON)
              } else {
                env.situationdbRelApi.copyOrMove(me.id, data, rels, env.situationdbRelApi.remove) map (_ => jsonOkResult)
              }
            }
          }
        }
      )
    }
  }

  def selectModal() = AuthBody { implicit ctx => me =>
    Resource.Permiss {
      implicit def req = ctx.body
      def searchForm = form.situationdb.search
      searchForm.bindFromRequest.fold(
        fail => Ok(html.resource.situationdb.modal.selectModal(Nil, Set.empty[String], false, searchForm)).fuccess,
        data => {
          val selected = get("selected") | me.id
          val mustStandard = getBool("mustStandard")
          val dt = data.copy(standard = if (mustStandard) Some(true) else none)
          env.situationdbApi.byId(selected).flatMap { opt =>
            val relSortBy = opt.map(_.relSortBy) | lila.resource.SituationDB.SortBy.default.some
            for {
              rels <- env.situationdbRelApi.findByDB(selected, dt, me.id, maxRelSize, relSortBy)
              tags <- env.situationdbRelApi.tagsByUser(me.id)
            } yield {
              Ok(html.resource.situationdb.modal.selectModal(rels, tags, mustStandard, searchForm))
            }
          }
        }
      )
    }
  }

  def situationTags() = Auth { implicit ctx => me =>
    env.situationdbRelApi.tagsByUser(me.id) map { tags =>
      Ok(JsArray(tags.toSeq.map(JsString)))
    }
  }

  def rels() = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = form.situationdb.search
    searchForm.bindFromRequest.fold(
      fail => jsonFormError(fail),
      data => {
        val selected = get("selected") | me.id
        env.situationdbApi.byId(selected).flatMap { opt =>
          val relSortBy = opt.map(_.relSortBy) | lila.resource.SituationDB.SortBy.default.some
          env.situationdbRelApi.findByDB(selected, data, me.id, maxRelSize, relSortBy) map { list =>
            Ok(JsArray(
              list.map { rel =>
                Json.obj(
                  "id" -> rel.id,
                  "fen" -> rel.fen,
                  "name" -> rel.name,
                  "tags" -> rel.tags,
                  "score" -> rel.score
                )
              }
            )) as JSON
          }
        }
      }
    )
  }

  def relSortBy(id: String, sortBy: String) = Auth { implicit ctx => me =>
    env.situationdbApi.byId(id).flatMap {
      _.fold(fuccess(jsonOkResult)) { _ =>
        env.situationdbApi.relSortBy(id, lila.resource.SituationDB.SortBy(sortBy)) inject jsonOkResult
      }
    }
  }

}
