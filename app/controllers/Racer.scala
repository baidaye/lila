package controllers

import play.api.libs.json._
import play.api.mvc._
import lila.api.Context
import lila.app._
import lila.puzzle.PuzzleId
import lila.racer.{ RacerRace, RacerRaceRepo, RacerRound, RacerRoundRepo }
import lila.security.Permission

import scala.concurrent.duration._
import views.html

object Racer extends LilaController {

  private def env = Env.racer
  private def member = Env.member.memberActiveRecordApi

  def home = Auth { implicit ctx => me =>
    NoBot {
      Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
        env.api.lobbyList(me, clazzIds).map { list =>
          Ok(html.racer.home(list))
        }
      }
    }
  }

  def history(page: Int) = Auth { implicit ctx => me =>
    NoBot {
      RacerRaceRepo.historyPage(me, page) map { pager =>
        Ok(html.racer.history(pager))
      }
    }
  }

  def lobby() = Auth { implicit ctx => me =>
    member.isTotalPuzzleAccept(me) flatMap { accept =>
      if (accept) {
        env.lobby.join(me.id) map { raceId =>
          Redirect(routes.Racer.show(raceId))
        }
      } else {
        get("referId") match {
          case None => Redirect(routes.Racer.home + "#notAccept").fuccess
          case Some(id) => Redirect(routes.Racer.show(id) + "#notAccept").fuccess
        }
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    env.cache.getRaceO(id) flatMap {
      case None => Redirect(routes.Racer.home).fuccess
      case Some(race) => env.cache.getRoundO(race).flatMap {
        case None => Redirect(routes.Racer.home).fuccess
        case Some(round) => showPage(race, round, me, ctx.pref)
      }
    }
  }

  def round(id: String, no: RacerRound.No) = Auth { implicit ctx => me =>
    OptionFuResult(env.api.raceAndRoundO(id, no)) {
      case (race, round) => showPage(race, round, me, ctx.pref)
    }
  }

  private def showPage(race: RacerRace, round: RacerRound, me: lila.user.User, pref: lila.pref.Pref)(implicit ctx: Context) = {
    for {
      player <- {
        if (race.isPublic && round.joinable(race)) env.api.join(race.id, round.no, me.id)
        else fuccess(lila.racer.RacerPlayer.make(race.id, round.no, me.id))
      }
      round <- env.cache.getRound(race.id, round.no)
      players <- env.cache.getPlayerList(round.playerIds)
      markMap <- env.api.userMarks(me.id)
      classIds <- Env.clazz.api.mineClazzIds(me.id)
      cj = canJoin(race, classIds)
      accept <- member.isTotalPuzzleAccept(me)
    } yield NoCache(Ok(
      html.racer.show(
        race,
        env.jsonView.data(race, round, player, players, markMap, cj, accept),
        env.jsonView.pref(pref)
      )
    ))
  }

  private def canJoin(race: RacerRace, clazzIds: List[String])(implicit ctx: Context) = {
    race.typ match {
      case RacerRace.Type.TeamInner => ctx.me.??(_.belongTeamId.exists(_ == race.organizer))
      case RacerRace.Type.ClazzInner => clazzIds.contains(race.organizer)
      case _ => ctx.isAuth
    }
  }

  def feedback(id: String, no: RacerRound.No, puzzleId: PuzzleId) = AuthBody { implicit ctx => me =>
    OptionFuResult(env.api.raceAndRoundO(id, no)) {
      case (race, round) => {
        if (round.isStarted) {
          implicit val req = ctx.body
          env.forms.feedback.bindFromRequest.fold(
            jsonFormError,
            data => {
              env.api.feedback(race, round, puzzleId, me, data)
              fuccess(jsonOkResult)
            }
          )
        } else fuccess(jsonOkResult)
      }
    }
  }

  def websocket(id: String, no: RacerRound.No, apiVersion: Int) = SocketOption[JsValue] { implicit ctx =>
    getSocketSri("sri").?? { sri =>
      env.socketHandler.join(id, no, sri, ctx.me, getSocketVersion, apiVersion)
    }
  }

  def createForm = Auth { implicit ctx => me =>
    Permiss {
      NoBot {
        for {
          teams <- controllers.rt_contest.Contest.teamList(me)
          clazzs <- controllers.rt_contest.Contest.clazzList(me)
        } yield Ok(html.racer.create(env.forms.createOf(me), teams, clazzs))
      }
    }
  }

  def create = AuthBody { implicit ctx => me =>
    Permiss {
      NoBot {
        implicit val req = ctx.body
        env.forms.create(me).bindFromRequest.fold(
          err => {
            for {
              teams <- controllers.rt_contest.Contest.teamList(me)
              clazzs <- controllers.rt_contest.Contest.clazzList(me)
            } yield Ok(html.racer.create(err, teams, clazzs))
          },
          data => env.api.createInner(me, data) map { race =>
            Redirect(routes.Racer.home())
          }
        )
      }
    }
  }

  def remove(id: String) = Auth { implicit ctx => me =>
    env.cache.getRaceO(id) flatMap {
      case None => Redirect(routes.Racer.home).fuccess
      case Some(race) => if (race.isCreator(me.id) && race.canDelete) {
        env.api.remove(race) inject Redirect(routes.Racer.home)
      } else Redirect(routes.Racer.home).fuccess
    }
  }

  def settingModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(env.cache.getRaceO(id)) { race =>
      if (race.isCreator(me.id)) {
        RacerRoundRepo.getByRace(race.id) flatMap { rounds =>
          Ok(html.racer.show.setting(race, rounds)).fuccess
        }
      } else Forbidden(views.html.site.message.authFailed).fuccess
    }
  }

  private[controllers] def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (isGranted(Permission.Coach) || ctx.me.??(_.hasTeam)) f
    else Forbidden(views.html.site.message.authFailed).fuccess
  }

  //  implicit val limitedDefault = Zero.instance[Fu[Result]](fuccess(Results.TooManyRequest("请求过于频繁，请明日再试")))
  //  private val CreateLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
  //    credits = 10,
  //    duration = 1 days,
  //    name = "racer per user",
  //    key = "racer.user"
  //  )

  def puzzles(id: String, no: Int) = Open { implicit ctx =>
    env.cache.getRaceO(id) flatMap {
      case None => Ok("none").fuccess
      case Some(race) => {
        env.selector.puzzles(chess.Color.black, race.settingOf(no)).map { list =>
          Ok {
            JsArray(list.map { puz => JsString(puz.rating.toString) })
          }
        }
      }
    }
  }

  def help = Open { implicit ctx =>
    Ok(html.racer.help()).fuccess
  }

}
