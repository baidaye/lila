package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import play.api.mvc.Result
import lila.team.{ ClockInMemberRepo, ClockInSetting, ClockInSettingRepo, ClockInTaskRepo, MemberRepo }
import lila.user.UserRepo
import views._

object TeamClockIn extends controllers.LilaController {

  private val teamApi = Env.team.api
  private val clockInApi = Env.team.clockInApi
  private val jsonView = Env.team.clockInJsonView
  private val forms = Env.task.forms

  def current(page: Int, teamId: String, status: Option[String]) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(teamApi.team(teamId)) { team =>
      Team.OwnerAndEnable(team) {
        val st = status.filter(_.nonEmpty).map(s => ClockInSetting.Status(s))
        ClockInSettingRepo.findByPage(page, teamId, ClockInSetting.Status.current, st) map { pager =>
          Ok(html.team.clockIn.list.current(team, st, pager))
        }
      }
    }
  }

  def history(page: Int, teamId: String, status: Option[String]) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(teamApi.team(teamId)) { team =>
      Team.OwnerAndEnable(team) {
        val st = status.filter(_.nonEmpty).map(s => ClockInSetting.Status(s))
        ClockInSettingRepo.findByPage(page, teamId, ClockInSetting.Status.history, st) map { pager =>
          Ok(html.team.clockIn.list.history(team, st, pager))
        }
      }
    }
  }

  def createForm(teamId: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(teamApi.team(teamId)) { team =>
      Team.OwnerAndEnable(team) {
        Ok(html.team.clockIn.form.create(team)).fuccess
      }
    }
  }

  def create(teamId: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(teamApi.team(teamId)) { team =>
      Team.OwnerAndEnable(team) {
        implicit def req = ctx.body
        forms.teamClockInCreate.bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          data => clockInApi.create(team, data, me) inject jsonOkResult
        )
      }
    }
  }

  def updateForm(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      clockInSettingOption <- clockInApi.byId(id)
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
      teamOption <- clockInSettingOption.?? { clockInSetting => teamApi.team(clockInSetting.teamId) }
    } yield (clockInSettingOption |@| taskTemplateOption |@| teamOption).tupled) {
      case (clockInSetting, taskTemplate, team) => {
        Owner(clockInSetting) {
          if (clockInSetting.editable) {
            ClockInTaskRepo.bySetting(clockInSetting.id) map { tasks =>
              Ok(html.team.clockIn.form.update(
                team,
                jsonView.clockInSettingJson(clockInSetting),
                jsonView.tasksJson(tasks),
                Env.task.jsonView.taskTemplateInfo(taskTemplate)
              ))
            }
          } else Forbidden(jsonError("Authorization failed")).fuccess
        }
      }
    }
  }

  def update(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      clockInSettingOption <- clockInApi.byId(id)
      teamOption <- clockInSettingOption.?? { clockInSetting => teamApi.team(clockInSetting.teamId) }
    } yield (clockInSettingOption |@| teamOption).tupled) {
      case (clockInSetting, team) => {
        Owner(clockInSetting) {
          if (clockInSetting.editable) {
            implicit def req = ctx.body
            forms.teamClockInCreate.bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              data => clockInApi.update(clockInSetting, team, data, me) inject jsonOkResult
            )
          } else Forbidden(jsonError("Authorization failed")).fuccess
        }
      }
    }
  }

  def publish(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(clockInApi.byId(id)) { clockInSetting =>
      Owner(clockInSetting) {
        if (clockInSetting.canPublish) {
          clockInApi.publish(clockInSetting) inject Redirect(routes.TeamClockIn.current(1, clockInSetting.teamId, None))
        } else Forbidden(views.html.site.message.authFailed).fuccess
      }
    }
  }

  def close(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(clockInApi.byId(id)) { clockInSetting =>
      Owner(clockInSetting) {
        if (clockInSetting.canClose) {
          clockInApi.close(clockInSetting) inject Redirect(routes.TeamClockIn.history(1, clockInSetting.teamId, None))
        } else Forbidden(views.html.site.message.authFailed).fuccess
      }
    }
  }

  def show(id: String, clazzId: Option[String]) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      clockInSettingOption <- clockInApi.byId(id)
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
      teamOption <- clockInSettingOption.?? { clockInSetting => teamApi.team(clockInSetting.teamId) }
    } yield (clockInSettingOption |@| taskTemplateOption |@| teamOption).tupled) {
      case (clockInSetting, taskTemplate, team) => {
        Owner(clockInSetting) {
          for {
            clockInTasks <- ClockInTaskRepo.bySetting(clockInSetting.id)
            members <- ClockInMemberRepo.bySettingAll(clockInSetting.id)
            teamMembers <- MemberRepo.memberFromSecondary(team.id, members.map(_.userId).distinct)
            filteredTeamMembers = teamMembers.filter(m => clazzId.isEmpty || clazzId.?? { cid => m.clazzIds.exists(_ contains (cid)) })
            userIds = filteredTeamMembers.map(_.user)
            users <- UserRepo.byIds(userIds)
            markMap <- Env.relation.markApi.getMarks(me.id)
            clazzes <- team.clazzIds.??(Env.clazz.api.byIds)
            cls = clazzes.sortBy(_.name).map(c => c.id -> c.name)
          } yield {
            Ok(html.team.clockIn.show(team, clockInSetting, taskTemplate, clockInTasks, members.filter(m => userIds.contains(m.userId)), users, markMap, cls, clazzId))
          }
        }
      }
    }
  }

  def showOfDate(clockInTaskId: String, clazzId: Option[String]) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      clockInTaskOption <- ClockInTaskRepo.byId(clockInTaskId)
      clockInSettingOption <- clockInTaskOption.?? { clockInTask => clockInApi.byId(clockInTask.settingId) }
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
      teamOption <- clockInSettingOption.?? { clockInSetting => teamApi.team(clockInSetting.teamId) }
    } yield (clockInTaskOption |@| clockInSettingOption |@| taskTemplateOption |@| teamOption).tupled) {
      case (clockInTask, clockInSetting, taskTemplate, team) => {
        Owner(clockInSetting) {
          for {
            clockInTasks <- ClockInTaskRepo.bySetting(clockInSetting.id)
            members <- ClockInMemberRepo.bySettingAll(clockInSetting.id)
            teamMembers <- MemberRepo.memberFromSecondary(clockInSetting.teamId, members.map(_.userId).distinct)
            filteredTeamMembers = teamMembers.filter(m => clazzId.isEmpty || clazzId.?? { cid => m.clazzIds.exists(_ contains (cid)) })
            userIds = filteredTeamMembers.map(_.user)
            users <- UserRepo.byIds(userIds)
            markMap <- Env.relation.markApi.getMarks(me.id)
            clazzes <- team.clazzIds.??(Env.clazz.api.byIds)
            cls = clazzes.sortBy(_.name).map(c => c.id -> c.name)
          } yield {
            Ok(html.team.clockIn.show.showOfDate(team, clockInSetting, taskTemplate, clockInTask, clockInTasks, members.filter(m => userIds.contains(m.userId)), users, markMap, cls, clazzId))
          }
        }
      }
    }
  }

  def joinOf(sourceId: String) = Auth { implicit ctx => me =>
    OptionResult(ClockInTaskRepo.bySourceId(sourceId)) { clockInTask =>
      if (clockInTask.isCreator(me.id)) {
        Redirect(routes.TeamClockIn.show(clockInTask.settingId, None))
      } else {
        Redirect(routes.TeamClockIn.join(clockInTask.settingId))
      }
    }
  }

  def joinPage(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      clockInSettingOption <- clockInApi.byId(id)
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
      teamOption <- clockInSettingOption.?? { clockInSetting => teamApi.team(clockInSetting.teamId) }
    } yield (clockInSettingOption |@| taskTemplateOption |@| teamOption).tupled) {
      case (clockInSetting, taskTemplate, team) => {
        for {
          belongTo <- MemberRepo.exists(team.id, me.id)
          clockInTasks <- ClockInTaskRepo.bySetting(clockInSetting.id)
          members <- ClockInMemberRepo.bySetting(clockInSetting.id, me.id)
        } yield {
          if (belongTo) {
            Ok(html.team.clockIn.join(team, clockInSetting, taskTemplate, clockInTasks, members))
          } else Forbidden(views.html.site.message.authFailed)
        }
      }
    }
  }

  def join(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      clockInSettingOption <- clockInApi.byId(id)
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
    } yield (clockInSettingOption |@| taskTemplateOption).tupled) {
      case (clockInSetting, taskTemplate) => {
        MemberRepo.exists(clockInSetting.teamId, me.id) flatMap { belongTo =>
          if (belongTo && clockInSetting.canJoin) {
            clockInApi.join(clockInSetting, taskTemplate, me.id) inject Redirect(routes.TeamClockIn.joinPage(id))
          } else Forbidden(views.html.site.message.authFailed).fuccess
        }
      }
    }
  }

  def leave(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(clockInApi.byId(id)) { clockInSetting =>
      MemberRepo.exists(clockInSetting.teamId, me.id) flatMap { belongTo =>
        if (belongTo) {
          clockInApi.leave(clockInSetting, me.id) inject Redirect(routes.TeamClockIn.joinPage(id))
        } else Forbidden(views.html.site.message.authFailed).fuccess
      }
    }
  }

  def calendar(id: String, userId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      userOption <- UserRepo.byId(userId)
      clockInSettingOption <- clockInApi.byId(id)
      taskTemplateOption <- clockInSettingOption.?? { clockInSetting => Env.task.tplApi.byId(clockInSetting.templateId) }
    } yield (userOption |@| clockInSettingOption |@| taskTemplateOption).tupled) {
      case (user, clockInSetting, taskTemplate) => {
        for {
          markOption <- Env.relation.markApi.getMark(me.id, userId)
          belongTo <- MemberRepo.exists(clockInSetting.teamId, userId)
          clockInTasks <- ClockInTaskRepo.bySetting(clockInSetting.id)
          members <- ClockInMemberRepo.bySetting(clockInSetting.id, userId)
        } yield {
          if (belongTo) {
            Ok(html.team.clockIn.calendar(user, markOption, clockInSetting, taskTemplate, clockInTasks, members))
          } else Forbidden(views.html.site.message.authFailed)
        }
      }
    }
  }

  private[controllers] def Owner(clockInSetting: ClockInSetting)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => clockInSetting.isCreator(me.id))) f
    else Forbidden(views.html.site.message.authFailed).fuccess

}
