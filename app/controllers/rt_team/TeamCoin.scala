package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import lila.user.UserRepo
import lila.common.paginator.Paginator
import lila.team.{ CampusRepo, MemberRepo, TagRepo, TeamCoinRepo, MemberWithUser, TeamCoin => TeamCoinModel }
import views._

object TeamCoin extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms

  def coinMembers(id: String, p: Int = 1) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        (for {
          tags <- TagRepo.findByTeam(id)
          markMap <- api.userMarks(me.id)
        } yield (tags, markMap)) flatMap {
          case (tags, markMap) => {
            implicit val req = ctx.body
            val form = forms.coin.coinMemberSearch.bindFromRequest
            form.fold(
              fail => Ok(html.team.coin.members(fail, team, Nil, Paginator.empty[MemberWithUser], markMap)).fuccess,
              data => data match {
                case (q, campus) => {
                  for {
                    campuses <- CampusRepo.byTeam(id)
                    markMap <- api.userMarks(me.id)
                    pager <- MemberRepo.coinPage(team.id, markMap, p, q, campus)
                  } yield {
                    Ok(html.team.coin.members(form, team, campuses, pager, markMap))
                  }
                }
              }
            )
          }
        }
      }
    }
  }

  def coinMember(id: String, memberId: String, p: Int = 1) = Auth { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        MemberRepo.ownerOrCoach(mwu.team, me.id) flatMap {
          case None => {
            if (me.id == mwu.userId) {
              for {
                pager <- TeamCoinRepo.page(p, mwu.user.id)
                markOption <- api.userMark(me.id, mwu.member.user)
              } yield Ok(html.team.coin.memberForSelf(team, mwu, false, pager, markOption))
            } else Forbidden(views.html.site.message.authFailed).fuccess
          }
          case Some(_) => {
            for {
              pager <- TeamCoinRepo.page(p, mwu.user.id)
              markOption <- api.userMark(me.id, mwu.member.user)
            } yield if (team.isCreator(me.id)) Ok(html.team.coin.memberForTeam(team, mwu, true, pager, markOption)) else Ok(html.team.coin.memberForSelf(team, mwu, true, pager, markOption))
          }
        }
      }
    }
  }

  def coinLogs(id: String, p: Int = 1) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        val form = forms.coin.coinLogSearch.bindFromRequest
        form.fold(
          err => BadRequest(html.team.coin.logs(err, team, Nil, Paginator.empty[lila.team.TeamCoin], Nil, Map.empty[String, Option[String]])).fuccess,
          data => for {
            campuses <- CampusRepo.byTeam(id)
            pager <- TeamCoinRepo.logPage(p, id, data)
            users <- pager.currentPageResults.nonEmpty.??(UserRepo.byIds(pager.currentPageResults.map(_.userId)))
            markMap <- api.userMarks(me.id)
          } yield Ok(html.team.coin.logs(form, team, campuses, pager, users, markMap))
        )
      }
    }
  }

  def coinMemberModal(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        MemberRepo.ownerOrCoach(mwu.team, me.id) flatMap {
          case None => Forbidden(views.html.site.message.authFailed).fuccess
          case Some(_) => {
            api.userMark(me.id, mwu.member.user) flatMap { markOption =>
              Ok(html.team.coin.coinEdit(team, mwu, markOption, forms.coin.coinEditOf(team, mwu))).fuccess
            }
          }
        }
      }
    }
  }

  def coinMemberApply(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        MemberRepo.ownerOrCoach(mwu.team, me.id) flatMap {
          case None => Forbidden(jsonError("Forbidden")).fuccess
          case Some(_) => {
            api.userMark(me.id, mwu.member.user).flatMap { markOption =>
              implicit val req = ctx.body
              val form = forms.coin.coinEdit(team).bindFromRequest
              form.fold(
                err => BadRequest(html.team.coin.coinEdit(team, mwu, markOption, err)).fuccess,
                data => data.realOperate match {
                  case TeamCoinModel.Operate.Plus => {
                    if (data.coin > team.coinSettingOrDefault.singleVal) fuccess {
                      Forbidden(jsonError("超过最大限值")) as JSON
                    }
                    else api.setMemberCoin(team, mwu.member, data.realOperate, data.coin, data.note, me) inject Redirect(routes.TeamCoin.coinMember(team.id, memberId, 1))
                  }
                  case TeamCoinModel.Operate.Minus => {
                    if (data.coin > Math.min(mwu.member.coinOrZero, team.coinSettingOrDefault.singleVal)) fuccess {
                      Forbidden(jsonError("超过最大限值")) as JSON
                    }
                    else api.setMemberCoin(team, mwu.member, data.realOperate, data.coin, data.note, me) inject Redirect(routes.TeamCoin.coinMember(team.id, memberId, 1))
                  }
                }
              )
            }
          }
        }
      }
    }
  }

}
