package controllers.rt_team

import lila.app._
import lila.api.Context
import lila.memo.UploadRateLimit
import lila.team.{ TeamFederationRepo, TeamFederationMemberRepo, TeamFederationRequestRepo, TeamRepo, Team => TeamModel, TeamFederation => TeamFederationModel, TeamFederationMember, TeamFederationRequest }
import ornicar.scalalib.Random
import play.api.libs.json.Json
import play.api.mvc._
import views._

object TeamFederation extends controllers.LilaController {

  private val api = Env.team.federationApi
  private val forms = Env.team.forms

  def mineList() = Secure(_.Team) { implicit ctx => me =>
    val teamId = me.teamIdValue
    for {
      members <- TeamFederationMemberRepo.findByTeam(teamId)
      federations <- TeamFederationRepo.byIds(members.map(_.federationId))
    } yield {
      val fwms = members.sortBy(-_.createdAt.getMillis).map { member =>
        val f = federations.find(_.id == member.federationId) err s"cam not find federation ${member.federationId}"
        TeamFederationModel.WithMember(f, member)
      }
      Ok(html.team.federation.list.mineList(fwms))
    }
  }

  def joinList(text: String) = Secure(_.Team) { implicit ctx => me =>
    val teamId = me.teamIdValue
    for {
      requests <- TeamFederationRequestRepo.findByTeam(teamId)
      members <- TeamFederationMemberRepo.findByTeam(teamId)
      federations <- text.trim.nonEmpty.??(TeamFederationRepo.byIdOrName(text))
    } yield {
      val fwrs = federations.map { federation =>
        val request = requests.find(_.federationId == federation.id)
        val member = members.find(_.federationId == federation.id)
        TeamFederationModel.WithRequestAndMember(federation, request, member)
      }
      Ok(html.team.federation.list.joinList(fwrs, text))
    }
  }

  def requestList() = Secure(_.Team) { implicit ctx => me =>
    val teamId = me.teamIdValue
    for {
      requests <- TeamFederationRequestRepo.findByFederationTeam(teamId)
      teams <- TeamRepo.byOrderedIds(requests.map(_.teamId))
    } yield {
      val rwts = requests.sortBy(-_.createdAt.getMillis).map { request =>
        val t = teams.find(_.id == request.teamId) err s"cam not find team ${request.teamId}"
        TeamFederationRequest.WithTeam(request, t)
      }
      Ok(html.team.federation.list.requestList(rwts))
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api byId id) { federation =>
      val teamId = me.teamId
      for {
        myRequest <- teamId.??(t => TeamFederationRequestRepo.find(federation.id, t))
        myMember <- teamId.??(t => TeamFederationMemberRepo.find(federation.id, t))
        requests <- teamId.??(t => TeamFederationRequestRepo.findByFederationTeam(t))
        members <- TeamFederationMemberRepo.findByFederation(federation.id)
        teams <- TeamRepo.byOrderedIds((members.map(_.teamId) ++ requests.map(_.teamId)).distinct)
      } yield {
        val rwts = requests.sortBy(-_.createdAt.getMillis).map { request =>
          val t = teams.find(_.id == request.teamId) err s"cam not find team ${request.teamId}"
          TeamFederationRequest.WithTeam(request, t)
        }

        val mwts = members.sortBy(-_.createdAt.getMillis).map { member =>
          val t = teams.find(_.id == member.teamId) err s"cam not find team ${member.teamId}"
          TeamFederationMember.WithTeam(member, t)
        }

        Ok(html.team.federation.show(federation, myRequest, myMember, rwts, mwts))
      }
    }
  }

  def createForm = Secure(_.Team) { implicit ctx => me =>
    val teamId = me.teamIdValue
    OptionFuResult(TeamRepo.byId(teamId)) { team =>
      TeamOwnerAndEnable(team) {
        TeamFederationRepo.findByTeam(teamId) map { federations =>
          if (federations.size >= TeamFederationModel.MAX_CREATE) {
            Forbidden(s"每个用户仅可创建${TeamFederationModel.MAX_CREATE}个联盟")
          } else {
            Ok(html.team.federation.forms.create(forms.federation.createOf))
          }
        }
      }
    }
  }

  def create = SecureBody(_.Team) { implicit ctx => implicit me =>
    val teamId = me.teamIdValue
    OptionFuResult(TeamRepo.byId(teamId)) { team =>
      TeamOwnerAndEnable(team) {
        TeamFederationRepo.findByTeam(teamId) flatMap { federations =>
          if (federations.size >= TeamFederationModel.MAX_CREATE) {
            Forbidden(s"每个用户仅可创建${TeamFederationModel.MAX_CREATE}个联盟").fuccess
          } else {
            implicit val req = ctx.body
            forms.federation.create.bindFromRequest.fold(
              err => BadRequest(html.team.federation.forms.create(err)).fuccess,
              data => api.create(data, me) map { f =>
                Redirect(routes.TeamFederation.show(f.id))
              }
            )
          }
        }
      }
    }
  }

  def updateForm(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { federation =>
      OwnerAndEnable(federation) {
        Ok(html.team.federation.forms.update(forms.federation.updateOf(federation), federation)).fuccess
      }
    }
  }

  def update(id: String) = SecureBody(_.Team) { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { federation =>
      OwnerAndEnable(federation) {
        implicit val req = ctx.body
        forms.federation.update.bindFromRequest.fold(
          err => BadRequest(html.team.federation.forms.update(err, federation)).fuccess,
          data => api.update(federation, data) inject Redirect(routes.TeamFederation.show(id))
        )
      }
    }
  }

  def settingForm(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { federation =>
      OwnerAndEnable(federation) {
        Ok(html.team.federation.forms.setting(forms.federation.settingOf(federation), federation)).fuccess
      }
    }
  }

  def setting(id: String) = SecureBody(_.Team) { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { federation =>
      OwnerAndEnable(federation) {
        implicit val req = ctx.body
        forms.federation.setting.bindFromRequest.fold(
          err => BadRequest(html.team.federation.forms.setting(err, federation)).fuccess,
          data => api.setting(federation, data) inject Redirect(routes.TeamFederation.show(id))
        )
      }
    }
  }

  def joinForm(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { federation =>
      Ok(html.team.federation.join.joinForm(forms.federation.join, federation)).fuccess
    }
  }

  def join(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { federation =>
      TeamFederationMemberRepo.findByTeam(me.teamIdValue).flatMap { members =>
        if (members.filter(_.isMember).size >= TeamFederationModel.MAX_JOIN) {
          Forbidden(s"每个用户仅可加入${TeamFederationModel.MAX_JOIN}个联盟").fuccess
        } else {
          implicit val req = ctx.body
          forms.federation.join.bindFromRequest.fold(
            err => BadRequest(html.team.federation.join.joinForm(err, federation)).fuccess,
            message => TeamRepo.byId(me.teamIdValue) flatMap {
              case None => Redirect(routes.TeamFederation.show(id)).fuccess
              case Some(team) => api.joinRequest(federation, message, team, me) inject Redirect(routes.TeamFederation.show(id))
            }
          )
        }
      }
    }
  }

  def joinProcess(requestId: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      requestOption <- TeamFederationRequestRepo.byId(requestId)
      federationOption <- requestOption.?? { r => api.byId(r.federationId) }
    } yield (requestOption |@| federationOption).tupled) {
      case (request, federation) => {
        OwnerAndEnable(federation) {
          implicit val req = ctx.body
          forms.federation.joinProcess.bindFromRequest.fold(
            err => BadRequest(html.team.federation.forms.setting(err, federation)).fuccess,
            process => api.joinProcess(federation, request, me, process == "accept") inject Redirect(routes.TeamFederation.requestList)
          )
        }
      }
    }
  }

  def quit(id: String) = Secure(_.Team) { implicit ctx => me =>
    val teamId = me.teamIdValue
    OptionFuResult(for {
      memberOption <- TeamFederationMemberRepo.find(id, teamId)
      federationOption <- api.byId(id)
    } yield (memberOption |@| federationOption).tupled) {
      case (member, federation) => {
        api.quit(federation, member) inject Redirect(routes.TeamFederation.show(id))
      }
    }
  }

  def kick(id: String, memberId: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      memberOption <- TeamFederationMemberRepo.byId(memberId)
      federationOption <- memberOption.??(m => api.byId(m.federationId))
    } yield (memberOption |@| federationOption).tupled) {
      case (member, federation) => {
        OwnerAndEnable(federation) {
          api.kick(federation, member) inject Redirect(routes.TeamFederation.show(federation.id))
        }
      }
    }
  }

  def disable(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { federation =>
      OwnerAndEnable(federation) {
        api.disable(federation) inject Redirect(routes.TeamFederation.show(federation.id))
      }
    }
  }

  def uploadPicture = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val picture = ctx.body.body.file("file")
      picture match {
        case Some(pic) => api.uploadPicture(Random nextString 16, pic) map { image =>
          Ok(Json.obj("ok" -> true, "path" -> image.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  private[controllers] def TeamOwnerAndEnable(team: TeamModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => team.isCreator(me.id) && team.enabled)) f
    else Forbidden(views.html.site.message.teamNotAvailable).fuccess

  private[controllers] def OwnerAndEnable(federation: TeamFederationModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => federation.isOwner(me.id) && federation.enabled)) f
    else Forbidden(views.html.site.message.teamNotAvailable).fuccess

}
