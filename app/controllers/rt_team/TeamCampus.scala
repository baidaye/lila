package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import lila.user.UserRepo
import lila.team.{ Campus, CampusRepo }
import views.html

object TeamCampus extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms

  def addCampusModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        CampusRepo.findNextSort(id) map { sort =>
          Ok(html.team.setting.addCampus(id, forms.campus.campus, sort))
        }
      }
    }
  }

  def addCampusApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.campus.campus.bindFromRequest.fold(
          err => {
            CampusRepo.findNextSort(id) map { sort =>
              BadRequest(html.team.setting.addCampus(id, err, sort))
            }
          },
          data => api.addCampus(team, me, data) >>
            data.admin.?? { Env.mod.api.addPermission(_, lila.security.Permission.TeamCampus) }
            inject Redirect(s"${routes.Team.setting(id)}#campus")
        )
      }
    }
  }

  def editCampusModal(id: String, campusId: String) = Auth { implicit ctx => me =>
    OptionFuResult(CampusRepo.byId(campusId)) { c =>
      OptionFuResult(api.team(c.team)) { team =>
        Team.OwnerAndEnable(team) {
          campusAdmin(c, me) map { adminMark =>
            Ok(html.team.setting.editCampus(c, adminMark, forms.campus.campusOf(c)))
          }
        }
      }
    }
  }

  def editCampusApply(id: String, campusId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(CampusRepo.byId(campusId)) { c =>
      OptionFuResult(api.team(c.team)) { team =>
        Team.OwnerAndEnable(team) {
          implicit val req = ctx.body
          forms.campus.campus.bindFromRequest.fold(
            err => {
              campusAdmin(c, me) map { adminMark =>
                BadRequest(html.team.setting.editCampus(c, adminMark, err))
              }
            },
            data => {
              val isChangeAdmin = c.admin != data.admin
              api.updateCampus(c, data, isChangeAdmin) >> isChangeAdmin.?? {
                c.admin.?? { Env.mod.api.removePermission(_, lila.security.Permission.TeamCampus) } >>
                  data.admin.?? { Env.mod.api.addPermission(_, lila.security.Permission.TeamCampus) }
              }
            } inject Redirect(s"${routes.Team.setting(c.team)}#campus")
          )
        }
      }
    }
  }

  private def campusAdmin(c: Campus, me: lila.user.User) =
    c.admin match {
      case None => fuccess(none[String])
      case Some(adm) => {
        api.userMark(me.id, adm).flatMap {
          case None => UserRepo.byId(adm).map(_.map(_.username))
          case Some(mark) => fuccess(mark.some)
        }
      }
    }

  def removeCampus(id: String, campusId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(CampusRepo.byId(campusId)) { c =>
      OptionFuResult(api.team(c.team)) { team =>
        Team.OwnerAndEnable(team) {
          if (c.deletable) {
            api.removeCampus(c) inject jsonOkResult
          } else fuccess(Forbidden(jsonError("Forbidden")))
        }
      }
    }
  }

}
