package controllers.rt_team

import lila.app._
import lila.api.Context
import lila.memo.UploadRateLimit
import lila.common.paginator.Paginator
import lila.common.{ HTTPRequest, MaxPerSecond }
import play.api.libs.json.Json
import lila.team.{ CampusRepo, MemberRepo, TagRepo, Team => TeamModel }
import ornicar.scalalib.Random
import lila.user.UserRepo
import play.api.mvc._
import views._

object Team extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms
  private val paginator = Env.team.paginator

  private lazy val teamInfo = Env.current.teamInfo

  def home(page: Int) = Open { implicit ctx =>
    ctx.me.??(api.hasTeams) map {
      case true => Redirect(routes.Team.mine)
      case false => Redirect(routes.Team.all(page))
    }
  }

  def all(page: Int) = Open { implicit ctx =>
    Ok(html.team.list.all(Paginator.empty)).fuccess
  }

  def mine = Auth { implicit ctx => me =>
    api mine me map { html.team.list.mine(_) }
  }

  def search(text: String, page: Int) = OpenBody { implicit ctx =>
    if (text.trim.isEmpty) paginator popularTeams page map { html.team.list.all(_) }
    else paginator.popularTeams(page, text.some) map { teams => html.team.list.search(text, teams) }
  }

  def requests = Auth { implicit ctx => me =>
    Env.team.cached.nbRequests invalidate me.id
    api requestsWithUsers me map { html.team.list.allRequests(_) }
  }

  def show(id: String, page: Int) = Open { implicit ctx =>
    OptionFuOk(api team id) { team =>
      renderTeam(team, page)
    }
  }

  private[controllers] def renderTeam(team: TeamModel, page: Int = 1, error: Option[String] = None)(implicit ctx: Context) = for {
    info <- teamInfo(team, ctx.me)
    member <- ctx.userId.??(userId => MemberRepo.byId(team.id, userId))
    members <- api.onLineMembers(team.id)
    _ <- Env.user.lightUserApi preloadMany info.userIds
    requestOrBelong <- team.open.?? { ctx.userId.??(userId => api.requestOrBelong(userId)) }
  } yield html.team.show(team, member, members, info, requestOrBelong, error)

  def uploadPicture = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val picture = ctx.body.body.file("file")
      picture match {
        case Some(pic) => api.uploadPicture(Random nextString 16, pic) map { image =>
          Ok(Json.obj("ok" -> true, "path" -> image.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  def createForm = Auth { implicit ctx => me =>
    if (me.hasTeam || me.hasBelongTeam) {
      Forbidden("每个用户仅可创建一个俱乐部").fuccess
    } else {
      forms.anyCaptcha map { captcha =>
        Ok(html.team.forms.create(forms.team.create, captcha))
      }
    }
  }

  def create = AuthBody { implicit ctx => implicit me =>
    if (me.hasTeam || me.hasBelongTeam) {
      Forbidden("每个用户仅可创建一个俱乐部").fuccess
    } else {
      if (!me.cpExistsAndConfirmed) Forbidden("请先绑定手机").fuccess
      else {
        implicit val req = ctx.body
        forms.team.create.bindFromRequest.fold(
          err => forms.anyCaptcha map { captcha =>
            BadRequest(html.team.forms.create(err, captcha))
          },
          data => api.create(data, me) ?? {
            _ map { team => Redirect(routes.Team.show(team.id)): Result }
          }
        )
      }
    }
  }

  def updateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) { fuccess(html.team.forms.update(team, forms.team.edit(team))) }
    }
  }

  def update(id: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.team.edit(team).bindFromRequest.fold(
          err => BadRequest(html.team.forms.update(team, err)).fuccess,
          data => api.update(team, data, me) inject Redirect(routes.Team.show(team.id))
        )
      }
    }
  }

  def changeOwnerForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        MemberRepo userIdsByTeam team.id map { userIds =>
          html.team.changeOwner(team, userIds - team.createdBy)
        }
      }
    }
  }

  def changeOwner(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.member.selectMember.bindFromRequest.value ?? { userId =>
          UserRepo.byId(userId).flatMap {
            case None => Redirect(routes.Team.show(team.id)).fuccess
            case Some(u) => {
              if (u.teamId.isEmpty) {
                api.changeOwner(team, userId, me) inject Redirect(routes.Team.show(team.id))
              } else Forbidden(views.html.site.message.teamOwnerCannotChange(userId)).fuccess
            }
          }
        }
      }
    }
  }

  def disable(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        (for {
          campuses <- CampusRepo.byTeam(team.id)
          coaches <- MemberRepo.coachByTeam(team.id)
          members <- MemberRepo.teamMembers(Set(team.id))
        } yield ((campuses, coaches), members)) flatMap {
          case ((campuses, coaches), members) => {
            if (members.nonEmpty || coaches.count(_.member.isCoach) > 0 || campuses.count(_.hasAdmin) > 0) {
              fuccess(views.html.site.message.teamCloseWarn())
            } else {
              (api disable team) >>
                Env.mod.logApi.deleteTeam(me.id, team.name, team.description) inject
                Redirect(routes.Team.all(1))
            }
          }
        }
      }
    }
  }

  //  def close(id: String) = Secure(_.ManageTeam) { implicit ctx => me =>
  //    OptionFuResult(api team id) { team =>
  //      (api delete team) >>
  //        Env.mod.logApi.deleteTeam(me.id, team.name, team.description) inject
  //        Redirect(routes.Team all 1)
  //    }
  //  }

  def setting(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        for {
          tags <- TagRepo.findByTeam(id)
          campuses <- CampusRepo.byTeam(id)
          markMap <- api.userMarks(me.id)
        } yield html.team.setting(
          team,
          campuses,
          tags,
          forms.team.basicSetting(team),
          forms.team.ratingSetting(team),
          forms.team.coinSetting(team),
          markMap
        )
      }
    }
  }

  def basicSettingApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.team.basicSetting(team).bindFromRequest.fold(
          err => {
            for {
              tags <- TagRepo.findByTeam(id)
              campuses <- CampusRepo.byTeam(id)
              markMap <- api.userMarks(me.id)
            } yield BadRequest(html.team.setting(team, campuses, tags, err, forms.team.ratingSetting(team), forms.team.coinSetting(team), markMap))
          },
          data => api.basicSetting(team, data, me) inject Redirect(s"${routes.Team.setting(team.id)}#basic")
        )
      }
    }
  }

  def ratingSettingApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.team.ratingSetting(team).bindFromRequest.fold(
          err => {
            for {
              tags <- TagRepo.findByTeam(id)
              campuses <- CampusRepo.byTeam(id)
              markMap <- api.userMarks(me.id)
            } yield BadRequest(html.team.setting(team, campuses, tags, forms.team.basicSetting(team), err, forms.team.coinSetting(team), markMap))
          },
          data => api.ratingSetting(team, data, me) inject Redirect(s"${routes.Team.setting(team.id)}#rating")
        )
      }
    }
  }

  def coinSettingApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.team.coinSetting(team).bindFromRequest.fold(
          err => {
            for {
              tags <- TagRepo.findByTeam(id)
              campuses <- CampusRepo.byTeam(id)
              markMap <- api.userMarks(me.id)
            } yield BadRequest(html.team.setting(team, campuses, tags, forms.team.ratingSetting(team), forms.team.ratingSetting(team), err, markMap))
          },
          data => api.coinSetting(team, data, me) inject Redirect(s"${routes.Team.setting(team.id)}#coin")
        )
      }
    }
  }

  def clazzTable(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.team(id)) { team =>
      CanWriteCampus(team) {
        implicit val req = ctx.body
        val form = forms.team.clazzTable.bindFromRequest
        form.fold(
          fail => BadRequest(fail.toString).fuccess,
          {
            case (campus, coach) => {
              for {
                clazzs <- Env.clazz.api.teamClazz(team.clazzIds | Nil, campus, coach, me.id)
                campuses <- CampusRepo.byTeam(id)
                coaches <- MemberRepo.coachByTeam(id)
                markMap <- api.userMarks(me.id)
                currentCampus = campuses.find(c => campus.has(c.id))
                campusCoaches = campus.fold(coaches) { _ => coaches.filter(cu => currentCampus.??(_.containsCoach(cu.userId))) }
              } yield Ok(html.team.clazzTable(form, team, clazzs, campuses, campusCoaches, markMap))
            }
          }
        )
      }
    }
  }

  def users(teamId: String) = Action.async { req =>
    import controllers.Api.limitedDefault
    api.team(teamId) flatMap {
      _ ?? { team =>
        controllers.Api.GlobalLinearLimitPerIP(HTTPRequest lastRemoteAddress req) {
          import play.api.libs.iteratee._
          controllers.Api.jsonStream {
            Env.team.memberStream(team, MaxPerSecond(20)) &>
              Enumeratee.map(Env.api.userApi.one)
          } |> fuccess
        }
      }
    }
  }

  private[controllers] def OwnerAndEnable(team: TeamModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => (team.isCreator(me.id) || isGranted(_.ManageTeam)) && team.enabled)) f
    else Forbidden(views.html.site.message.teamNotAvailable).fuccess

  private[controllers] def ManagerAndEnable(team: TeamModel, campusId: Option[String])(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => (team.isCreator(me.id) || campusId.??(me.isCampusAdmin _) || isGranted(_.ManageTeam)) && team.enabled)) f
    else Forbidden(views.html.site.message.teamNotAvailable).fuccess

  private[controllers] def CanWriteCampus(team: TeamModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => (team.isCreator(me.id) || isGranted(_.ManageTeam)) && team.enabled)) f
    else {
      val campusId = get("campus")
      if (ctx.me.??(me => campusId.??(me.isCampusAdmin))) f
      else Forbidden(views.html.site.message.teamNotAvailable).fuccess
    }
  }

}
