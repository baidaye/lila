package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import lila.team.TagRepo
import views.html

object TeamTag extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms

  def addTagModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        Ok(html.team.setting.addTag(id, forms.tag.tagAdd)).fuccess
      }
    }
  }

  def addTagApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.tag.tagAdd.bindFromRequest.fold(
          err => BadRequest(html.team.setting.addTag(id, err)).fuccess,
          data => api.addTag(team, me, data) inject Redirect(s"${routes.Team.setting(id)}#tags")
        )
      }
    }
  }

  def editTagModal(id: String, tagId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TagRepo.byId(tagId)) { tag =>
      OptionFuResult(api team tag.team) { team =>
        Team.OwnerAndEnable(team) {
          Ok(html.team.setting.editTag(tag, forms.tag.tagEditOf(tag))).fuccess
        }
      }
    }
  }

  def editTagApply(id: String, tagId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(TagRepo.byId(tagId)) { tag =>
      OptionFuResult(api team tag.team) { team =>
        Team.OwnerAndEnable(team) {
          implicit val req = ctx.body
          forms.tag.tagEdit.bindFromRequest.fold(
            err => BadRequest(html.team.setting.editTag(tag, err)).fuccess,
            data => api.updateTag(tagId, data) inject Redirect(s"${routes.Team.setting(team.id)}#tags")
          )
        }
      }
    }
  }

  def removeTag(id: String, tagId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(TagRepo.byId(tagId)) { tag =>
      OptionFuResult(api team id) { team =>
        Team.OwnerAndEnable(team) {
          api.removeTag(id, tag) inject jsonOkResult
        }
      }
    }
  }

}
