package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import lila.common.paginator.Paginator
import lila.team.{ MemberRepo, MemberWithUser, TagRepo, TeamRatingRepo }
import views.html

object TeamRating extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms

  def ratingDistributionChart(id: String, clazzId: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.ratingDistribution(team, if (clazzId.isEmpty) none else clazzId.some).map { distributionData =>
          Ok(distributionData.mkString("[", ",", "]")) as JSON
        }
      }
    }
  }

  def ratingDistribution(id: String, p: Int = 1) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        (for {
          tags <- TagRepo.findByTeam(id)
          markMap <- api.userMarks(me.id)
        } yield (tags, markMap)) flatMap {
          case (tags, markMap) => {
            implicit val req = ctx.body
            val form = forms.rating.ratingDistribution.bindFromRequest
            form.fold(
              fail => {
                Ok(html.team.ratingDistribution(fail, team, Nil, Paginator.empty[MemberWithUser], Nil, markMap)).fuccess
              },
              data => data match {
                case (q, clazzId) => {
                  for {
                    clazzs <- team.clazzIds.??(Env.clazz.api.byIds)
                    markMap <- api.userMarks(me.id)
                    pager <- MemberRepo.ratingPage(team.id, markMap, p, q, clazzId)
                    distributionData <- MemberRepo.ratingDistribution(team, clazzId)
                  } yield {
                    val cls = clazzs.sortBy(_.name).map(c => c.id -> c.name)
                    Ok(html.team.ratingDistribution(form, team, cls, pager, distributionData, markMap))
                  }
                }
              }
            )
          }
        }
      }
    }
  }

  def memberRatingModal(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        Team.OwnerAndEnable(team) {
          api.userMark(me.id, mwu.member.user) flatMap { markOption =>
            Ok(html.team.ratingDistribution.ratingEdit(team, mwu, markOption, forms.rating.ratingEditOf(team, mwu))).fuccess
          }
        }
      }
    }
  }

  def memberRatingApply(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        Team.OwnerAndEnable(team) {
          api.userMark(me.id, mwu.member.user) flatMap { markOption =>
            implicit val req = ctx.body
            forms.rating.ratingEdit.bindFromRequest.fold(
              err => BadRequest(
                html.team.ratingDistribution.ratingEdit(team, mwu, markOption, err)
              ).fuccess,
              data => api.setMemberRating(team, mwu.member, data.k, data.rating.doubleValue(), data.note) inject Redirect(routes.TeamRating.ratingDistribution(team.id, 1))
            )
          }
        }
      }
    }
  }

  def memberRatingDistribution(id: String, memberId: String, p: Int = 1) = Auth { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        if (me.belongTeamId == mwu.user.belongTeamId) {
          for {
            distributionData <- MemberRepo.ratingDistribution(team, none)
            historyData <- TeamRatingRepo.historyData(mwu.user.id)
            pager <- TeamRatingRepo.page(p, mwu.user.id)
            markOption <- api.userMark(me.id, mwu.member.user)
          } yield Ok(html.team.memberRatingDistribution(team, mwu, distributionData, historyData, pager, markOption))
        } else Forbidden(views.html.site.message.teamNotAvailable).fuccess
      }
    }
  }

  def ratingRule() = Auth { implicit ctx => me =>
    Ok(views.html.team.ratingRulePage()).fuccess
  }

  def ratingAdvise() = Auth { implicit ctx => me =>
    Ok(views.html.team.ratingAdvisePage()).fuccess
  }

}
