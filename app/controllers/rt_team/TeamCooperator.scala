package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import views._

object TeamCooperator extends controllers.LilaController {

  private val api = Env.team.api
  private val certificationApi = Env.team.certificationApi
  private val forms = Env.team.forms

  def cooperates() = AuthBody { implicit ctx => me =>
    implicit val req = ctx.body
    val form = forms.cooperate.search.bindFromRequest
    val typ = getBoolOpt("typ")
    form.fold(
      err => BadRequest(
        html.team.cooperate(
          form = err,
          teamCooperates = lila.team.TeamCooperates(Nil),
          enabledTyp = typ.map(t => lila.team.TeamCooperate.EnabledTyp(t))
        )
      ).fuccess,
      typ => {
        lila.team.TeamCooperateRepo.findByUserWithTeam(me.id) map { cooperates =>
          Ok(html.team.cooperate(
            form = form,
            teamCooperates = lila.team.TeamCooperates(cooperates),
            enabledTyp = typ.map(t => lila.team.TeamCooperate.EnabledTyp(t))
          ))
        }
      }
    )
  }

  def modSetCooperatorModal(id: String) = Secure(_.ManageTeam) { implicit ctx => me =>
    OptionResult(api team id) { team =>
      Ok(html.team.mod.setCooperator(team, forms.cooperate.setCooperatorForm))
    }
  }

  def modSetCooperatorApply(id: String) = SecureBody(_.ManageTeam) { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      implicit val req = ctx.body
      forms.cooperate.setCooperatorForm.bindFromRequest.fold(
        err => BadRequest(errorsAsJson(err)).fuccess,
        cooperator => certificationApi.setCooperator(team, cooperator) inject jsonOkResult
      )
    }
  }

}
