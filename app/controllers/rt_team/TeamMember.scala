package controllers.rt_team

import lila.app._
import lila.api.Context
import play.api.mvc._
import play.api.libs.json._
import lila.user.UserRepo
import lila.security.Granter
import lila.common.paginator.Paginator
import lila.team.{ CampusRepo, Invite, InviteRepo, Joined, MemberRepo, MemberWithUser, Motivate, RequestRepo, TagRepo, TeamRepo, Team => TeamModel }

import views.html

object TeamMember extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms
  private val memberSelector = Env.team.memberSelector

  def join(id: String) = Auth { implicit ctx => me =>
    Env.clazz.api.myTeamClazzIds(me.id, id) flatMap { clazzIds =>
      api.join(id, me, clazzIds) flatMap {
        case Some(Joined(team)) => Redirect(routes.Team.show(team.id)).fuccess
        case Some(Motivate(team)) => Redirect(routes.TeamMember.requestForm(team.id)).fuccess
        case _ => notFound(ctx)
      }
    }
  }

  def acceptMemberModal(id: String, requestId: String, referrer: String) = Auth { implicit ctx => me =>
    OptionFuResult(api request requestId) { request =>
      OptionFuResult(api team request.team) { team =>
        Team.ManagerAndEnable(team, request.campus) {
          for {
            tags <- TagRepo.findByTeam(request.team)
            campuses <- CampusRepo.byTeam(request.team)
            requestUser <- UserRepo.byId(request.user)
          } yield Ok(html.team.request.accept(team, request, requestUser, referrer, tags, campuses, forms.member.memberAdd(team)))
        }
      }
    }
  }

  def acceptMemberApply(id: String, requestId: String, referrer: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      requestOption ← api request requestId
      teamOption ← requestOption.??(req => TeamRepo.managered(req.team, req.campus, me))
    } yield (teamOption |@| requestOption).tupled) {
      case (team, request) => {
        (for {
          tags <- TagRepo.findByTeam(request.team)
          campuses <- CampusRepo.byTeam(request.team)
          requestUser <- UserRepo.byId(request.user)
        } yield ((tags, campuses), requestUser)).flatMap {
          case ((tags, campuses), requestUser) => {
            implicit val req = ctx.body
            forms.member.memberAdd(team).bindFromRequest.fold(
              err => fuccess(BadRequest(html.team.request.accept(team, request, requestUser, referrer, tags, campuses, err))),
              data => Env.clazz.api.myTeamClazzIds(request.user, team.id) flatMap { clazzIds =>
                api.acceptRequest(
                  team,
                  request,
                  lila.team.MemberTags.byTagList(data.fields),
                  data.mark,
                  data.campus,
                  data.rating,
                  clazzIds,
                  me
                ) inject Redirect(referrer)
              }
            )
          }
        }
      }
    }
  }

  def requestForm(id: String) = Auth { implicit ctx => me =>
    OptionFuOk(api.requestable(id, me)) { team =>
      for {
        tags <- TagRepo.findByTeam(team.id)
        campuses <- CampusRepo.byTeam(team.id)
        captcha <- forms.anyCaptcha
      } yield html.team.request.requestForm(team, campuses, tags, forms.member.requestOf, captcha)
    }
  }

  def requestCreate(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.requestable(id, me)) { team =>
      api.requestOrBelongTeams(me.id) flatMap {
        case (t1, t2) => {
          implicit val req = ctx.body
          forms.member.request(t1.map(_.name), t2.map(_.name)).bindFromRequest.fold(
            err => {
              for {
                tags <- TagRepo.findByTeam(team.id)
                campuses <- CampusRepo.byTeam(team.id)
                captcha <- forms.anyCaptcha
              } yield BadRequest(html.team.request.requestForm(team, campuses, tags, err, captcha))
            },
            setup => api.createRequest(team, setup, me) inject Redirect(routes.Team.show(team.id))
          )
        }
      }
    }
  }

  def requestProcess(requestId: String) = AuthBody { implicit ctx => me =>
    OptionFuRedirectUrl(for {
      requestOption ← api request requestId
      teamOption ← requestOption.??(req => TeamRepo.managered(req.team, req.campus, me))
    } yield (teamOption |@| requestOption).tupled) {
      case (team, request) => {
        implicit val req = ctx.body
        forms.member.processRequest.bindFromRequest.fold(
          _ => fuccess(routes.Team.show(team.id).toString), {
            case (decision, url) =>
              if (team.disabled) fuccess(url)
              else {
                Env.clazz.api.myTeamClazzIds(request.user, team.id) flatMap { clazzIds =>
                  api.processRequest(team, request, decision === "accept", clazzIds, me) inject url
                }
              }
          }
        )
      }
    }
  }

  def invite(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      if (team.disabled) {
        renderInviteMessage(team, Invite.InviteMessage.Disabled)
      } else {
        if (team.certified) {
          implicit val req = ctx.body
          forms.member.inviteForm.bindFromRequest.fold(
            _ => renderInviteMessage(team, Invite.InviteMessage.CoachNotFound),
            username => UserRepo.named(username) flatMap {
              case None => renderInviteMessage(team, Invite.InviteMessage.CoachNotFound)
              case Some(u) => if (Granter(_.Coach)(u)) {
                api.belongsTo(team.id, u.id) flatMap {
                  case true => renderInviteMessage(team, Invite.InviteMessage.Joined)
                  case false => InviteRepo.exists(team.id, u.id) flatMap {
                    case true => renderInviteMessage(team, Invite.InviteMessage.Inviting)
                    case false => RequestRepo.exists(team.id, u.id) flatMap {
                      case true => renderInviteMessage(team, Invite.InviteMessage.Requested)
                      case false => api.createInvite(team, u) inject Redirect(routes.Team.show(team.id))
                    }
                  }
                }
              } else {
                renderInviteMessage(team, Invite.InviteMessage.MustCoach)
              }
            }
          )
        } else {
          Forbidden("team is not certified").fuccess
        }
      }
    }
  }

  private def renderInviteMessage(team: TeamModel, im: Invite.InviteMessage)(implicit ctx: Context) =
    Team.renderTeam(team = team, page = 1, error = im.message.some) map { Ok(_) }

  def inviteProcess(id: String, inviteId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      inviteOption ← api invite inviteId
      teamOption ← inviteOption.??(inv => api team inv.team)
    } yield (teamOption |@| inviteOption).tupled) {
      case (team, invite) => {
        if (team.certified) {
          implicit val req = ctx.body
          forms.member.inviteProcessForm.bindFromRequest.fold(
            _ => Team.renderTeam(team) map { BadRequest(_) },
            process => {
              if (team.disabled) Redirect(routes.Team.show(team.id)).fuccess
              else Env.clazz.api.myTeamClazzIds(me.id, team.id) flatMap { clazzIds =>
                api.processInvite(team, invite, process === "accept", clazzIds, me) inject Redirect(routes.Team.show(team.id))
              }
            }
          )
        } else {
          Forbidden("team is not certified").fuccess
        }
      }
    }
  }

  def quitModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      if (me.belongTeamId.has(team.id)) {
        Env.user.forms.validPwd(me) map { form =>
          Ok(html.team.show.quitModal(team, form))
        }
      } else Forbidden("Forbidden").fuccess
    }
  }

  def quit(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      if (me.belongTeamId.has(team.id) && me.notImported) {
        implicit val req = ctx.body
        Env.user.forms.validPwd(me) flatMap { form =>
          form.bindFromRequest.fold(
            err => jsonFormError(err),
            _ => api.quit(id, me) inject jsonOkResult
          )
        }
      } else Forbidden("Forbidden").fuccess
    }
  }

  def kick(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      implicit val req = ctx.body
      forms.member.kickForm.bindFromRequest.fold(
        err => jsonFormError(err), {
          u =>
            {
              MemberRepo.byId(id, u) flatMap {
                case None => fuccess(jsonOkResult)
                case Some(m) => {
                  Team.ManagerAndEnable(team, m.campus.some) {
                    if (m.isManager) Ok(Json.obj("ok" -> false, "err" -> "管理者不可移除")).fuccess
                    else api.kick(team, u, me) inject jsonOkResult
                  }
                }
              }
            }
        }
      )
    }
  }

  def members(id: String, page: Int = 1) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.CanWriteCampus(team) {
        (for {
          tags <- TagRepo.findByTeam(id)
          campuses <- CampusRepo.byTeam(id)
          markMap <- api.userMarks(me.id)
        } yield ((tags, campuses), markMap)) flatMap {
          case ((tags, campuses), markMap) => {
            implicit val req = ctx.body
            val form = forms.member.memberSearch.bindFromRequest
            form.fold(
              fail => {
                BadRequest(html.team.member(fail, team, Paginator.empty, tags, campuses, markMap, getBool("advance"))).fuccess
              },
              data => {
                memberSelector.teamMembers(team, page, data, tags, markMap) map { pager =>
                  Ok(html.team.member(form, team, pager, tags, campuses, markMap, getBool("advance")))
                }
              }
            )
          }
        }
      }
    }
  }

  def loadMember(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.CanWriteCampus(team) {
        implicit val req = ctx.body
        forms.member.memberSearch.bindFromRequest.fold(
          jsonFormError,
          data => for {
            tags <- TagRepo.findByTeam(id)
            markMap <- Env.relation.markApi.getMarks(me.id)
            members <- memberSelector.findMember(team.id, data, tags, markMap)
          } yield {
            Ok(memberJson(members, markMap)) as JSON
          }
        )
      }
    }
  }

  def loadClazzMember(id: String, cls: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      implicit val req = ctx.body
      forms.member.memberSearch.bindFromRequest.fold(
        jsonFormError,
        data => for {
          clazzOption <- Env.clazz.api.byId(cls)
          tags <- TagRepo.findByTeam(id)
          markMap <- api.userMarks(me.id)
          members <- memberSelector.findMember(team.id, data, tags, markMap)
        } yield {
          clazzOption match {
            case None => Ok(memberJson(members, markMap)) as JSON
            case Some(clazz) => {
              val excludeMembers = members.filterNot(m => clazz.studentIds.contains(m.userId))
              Ok(memberJson(excludeMembers, markMap)) as JSON
            }
          }
        }
      )
    }
  }

  def loadCoachMember(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      implicit val req = ctx.body
      forms.member.memberSearch.bindFromRequest.fold(
        jsonFormError,
        data => for {
          tags <- TagRepo.findByTeam(id)
          markMap <- api.userMarks(me.id)
          members <- memberSelector.findMember(team.id, data, tags, markMap)
          coachStudentIds <- Env.coach.studentApi.mineStudentsById(coach)
        } yield {
          val excludeMembers = members.filterNot(m => coachStudentIds.contains(m.userId))
          Ok(memberJson(excludeMembers, markMap)) as JSON
        }
      )
    }
  }

  private def memberJson(members: List[MemberWithUser], markMap: Map[String, Option[String]]) = {
    JsArray {
      members.map { mu =>
        val user = mu.user
        Json.obj(
          "userId" -> user.id,
          "username" -> user.username,
          "mark" -> {
            markMap.get(user.id) match {
              case None => user.realNameOrUsername
              case Some(mark) => mark | user.realNameOrUsername
            }
          },
          "head" -> user.head,
          "level" -> user.profileOrDefault.currentLevel.label
        )
      }
    }
  }

  def editMemberModal(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        (for {
          tags <- TagRepo.findByTeam(team.id)
          campuses <- CampusRepo.byTeam(team.id)
          markOption <- api.userMark(me.id, mwu.member.user)
        } yield ((tags, campuses), markOption)) flatMap {
          case ((tags, campuses), markOption) => {
            Team.ManagerAndEnable(team, mwu.member.campus.some) {
              Ok(html.team.member.edit(mwu, tags, campuses, markOption, forms.member.memberEditOf(team, mwu, markOption))).fuccess
            }
          }
        }
      }
    }
  }

  def editMemberApply(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(MemberRepo.memberWithUser(memberId)) { mwu =>
      OptionFuResult(api team mwu.team) { team =>
        (for {
          tags <- TagRepo.findByTeam(team.id)
          campuses <- CampusRepo.byTeam(team.id)
          markOption <- api.userMark(me.id, mwu.member.user)
        } yield ((tags, campuses), markOption)) flatMap {
          case ((tags, campuses), markOption) => {
            Team.ManagerAndEnable(team, mwu.member.campus.some) {
              implicit val req = ctx.body
              forms.member.memberEdit(mwu.user).bindFromRequest.fold(
                err => BadRequest(
                  html.team.member.edit(mwu, tags, campuses, markOption, err)
                ).fuccess,
                data => api.updateMember(team, mwu.member, data, me) inject jsonOkResult
              )
            }
          }
        }
      }
    }
  }

  def autocomplete(teamId: String) = Auth { implicit ctx => me =>
    api.findMemberByText(teamId, me.id, get("q")).map { list =>
      Ok(JsArray(
        list.map { mwu =>
          Json.obj(
            "userId" -> mwu.member.user,
            "username" -> mwu.user.username,
            "mark" -> mwu.mark,
            "displayName" -> (mwu.mark | mwu.user.username)
          )
        }
      )) as JSON
    }
  }

  def canBeAdmin(teamId: String, userId: String, campusId: Option[String]) = Open { implicit ctx =>
    for {
      memberOption <- MemberRepo.byId(teamId, userId)
      campuses <- CampusRepo.byTeam(teamId)
    } yield {
      val isAdminRole = memberOption.exists(_.isAdmin)
      val isOriginalAdmin = campuses.exists(campus => campus.admin.contains(userId) && campusId.contains(campus.id))
      val ok = !isAdminRole || (isAdminRole && isOriginalAdmin)
      Ok(Json.obj("ok" -> ok)) as JSON
    }
  }

}
