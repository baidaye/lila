package controllers.rt_team

import lila.app._
import lila.api.Context
import lila.common.paginator.Paginator
import lila.team.{ CampusRepo, MiniPuzzle, TagRepo, TestStudent, TestStudentRepo, TestTemplate, TestTemplateRepo }
import lila.user.UserRepo
import play.api.mvc._
import views._

object TeamTest extends controllers.LilaController {

  private val forms = Env.team.forms
  private val api = Env.team.testApi
  private val jsonView = Env.team.testJsonView
  private val teamApi = Env.team.api
  private val memberSelector = Env.team.memberSelector

  def current(page: Int) = Secure(_.Team) { implicit ctx => me =>
    val status = List(TestTemplate.Status.Created, TestTemplate.Status.Published)
    TestTemplateRepo.page(page, me.teamIdValue, status) map { pager =>
      Ok(html.team.test.list.current(pager))
    }
  }

  def history(page: Int) = Secure(_.Team) { implicit ctx => me =>
    val status = List(TestTemplate.Status.Canceled)
    TestTemplateRepo.page(page, me.teamIdValue, status) map { pager =>
      Ok(html.team.test.list.history(pager))
    }
  }

  def createForm = Secure(_.Team) { implicit ctx => me =>
    fuccess {
      Ok(html.team.test.form.create(forms.test.createOrUpdate(fetchPuzzleCount)))
    }
  }

  def create = SecureBody(_.Team) { implicit ctx => me =>
    implicit def req = ctx.body
    forms.test.createOrUpdate(fetchPuzzleCount).bindFromRequest.fold(
      err => {
        val capsuleIds = err.data.get("capsuleIds").??(_.split(",").toList)
        for {
          capsuleWithStatistics <- controllers.rt_resource.Capsule.listWithStatistics(capsuleIds, me)
          count <- fetchPuzzleCount(capsuleIds)
        } yield BadRequest(html.team.test.form.create(err, capsuleWithStatistics, count))
      },
      data => api.addTemplate(data, me) map { tpl =>
        Redirect(routes.TeamTest.show(1, tpl.id))
      }
    )
  }

  def updateForm(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(api byId id) { tpl =>
      OwnerAndEnable(tpl) {
        val capsuleIds = tpl.capsuleIds
        for {
          capsuleWithStatistics <- controllers.rt_resource.Capsule.listWithStatistics(capsuleIds, me)
          count <- fetchPuzzleCount(capsuleIds)
        } yield {
          Ok(
            html.team.test.form.update(
              forms.test.createOrUpdateOf(tpl, fetchPuzzleCount),
              tpl,
              capsuleWithStatistics,
              count
            )
          )
        }
      }
    }
  }

  def update(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(api byId id) { tpl =>
      OwnerAndEnable(tpl) {
        implicit def req = ctx.body
        forms.test.createOrUpdate(fetchPuzzleCount).bindFromRequest.fold(
          err => {
            val capsuleIds = err.data.get("capsuleIds").??(_.split(",").toList)
            for {
              capsuleWithStatistics <- controllers.rt_resource.Capsule.listWithStatistics(capsuleIds, me)
              count <- fetchPuzzleCount(capsuleIds)
            } yield BadRequest(html.team.test.form.update(err, tpl, capsuleWithStatistics, count))
          },
          data => api.updateTemplate(tpl, data) inject {
            Redirect(routes.TeamTest.show(1, id))
          }
        )
      }
    }
  }

  def close(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(api byId id) { tpl =>
      OwnerAndEnable(tpl) {
        api.close(tpl.id) inject {
          Redirect(routes.TeamTest.current(1))
        }
      }
    }
  }

  def recover(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(api byId id) { tpl =>
      Owner(tpl) {
        api.recover(tpl.id) inject {
          Redirect(routes.TeamTest.history(1))
        }
      }
    }
  }

  def show(page: Int, id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(api byId id) { tpl =>
      Owner(tpl) {
        Env.relation.markApi.getMarks(me.id) flatMap { marks =>
          implicit def req = ctx.body
          forms.test.testStudentSearch.bindFromRequest.fold(
            err => BadRequest(html.team.test.show(err, tpl, Paginator.empty[TestStudent.WithUser], marks)).fuccess,
            data => TestStudentRepo.page(page, id, data, marks) map { pager =>
              html.team.test.show(forms.test.testStudentSearch fill data, tpl, pager, marks)
            }
          )
        }
      }
    }
  }

  def sendTestModal(id: String) = Secure(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      tplOption <- api.byId(id)
      teamOption <- tplOption.?? { tpl => teamApi.team(tpl.teamId) }
    } yield (tplOption |@| teamOption).tupled) {
      case (tpl, team) => {
        OwnerAndEnable(tpl) {
          for {
            campuses <- CampusRepo.byTeam(team.id)
            tags <- TagRepo.findByTeam(team.id)
          } yield {
            Ok(html.team.test.show.sendModal(
              form = forms.member.memberSearch,
              tpl = tpl,
              team = team,
              campuses = campuses,
              tags = tags
            ))
          }
        }
      }
    }
  }

  import play.api.libs.json._
  def sendTest(id: String) = SecureBody(_.Team) { implicit ctx => me =>
    OptionFuResult(for {
      tplOption <- api.byId(id)
      teamOption <- tplOption.?? { tpl => teamApi.team(tpl.teamId) }
    } yield (tplOption |@| teamOption).tupled) {
      case (tpl, team) => {
        OwnerAndEnable(tpl) {
          implicit val req = ctx.body
          forms.test.testInvite.bindFromRequest.fold(
            jsonFormError,
            data => {
              fetchPuzzleCount(tpl.capsuleIds) flatMap { nb =>
                if (tpl.nbQ > 0 && tpl.nbQ <= nb) {
                  api.sendTest(tpl, data, fetchPuzzle, me) inject jsonOkResult
                } else {
                  BadRequest(Json.obj("error" -> "测试题目数量不能为0，并且测试题目数量不能大于题库内题目数量")).fuccess
                }
              }
            }
          )
        }
      }
    }
  }

  def testStuShow(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        Ok(html.team.test.stuShow(testStu)).fuccess
      }
    }
  }

  def testStuAnswer(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        testStu.status match {
          case TestStudent.Status.Created => Ok(html.team.test.stuAnswer(testStu, jsonView.pref(ctx.pref), jsonView.testStuData(testStu))).fuccess
          case TestStudent.Status.Started => Ok(html.team.test.stuAnswer(testStu, jsonView.pref(ctx.pref), jsonView.testStuData(testStu))).fuccess
          case TestStudent.Status.Expired => Redirect(routes.TeamTest.testStuShow(testStuId)).fuccess
          case TestStudent.Status.Finished => Redirect(routes.TeamTest.testStuScore(testStuId)).fuccess
        }
      }
    }
  }

  def testStuScore(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwnerOrCreator(testStu) {
        for {
          stu <- UserRepo.byId(testStu.studentId)
          mark <- Env.relation.markApi.getMark(me.id, testStu.studentId)
          markOrName = stu.?? { mark | _.realNameOrUsername }
        } yield {
          testStu.status match {
            case TestStudent.Status.Created => Redirect(routes.TeamTest.testStuAnswer(testStuId))
            case TestStudent.Status.Started => Redirect(routes.TeamTest.testStuAnswer(testStuId))
            case TestStudent.Status.Expired => Redirect(routes.TeamTest.testStuShow(testStuId))
            case TestStudent.Status.Finished => Ok(html.team.test.stuScore(testStu, jsonView.pref(ctx.pref), jsonView.testStuData(testStu) ++ Json.obj("studentMarkOrName" -> markOrName)))
          }
        }
      }
    }
  }

  def testStuStart(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        api.setStart(testStu) inject jsonOkResult
      }
    }
  }

  def testStuFinish(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        api.setFinish(testStu) inject jsonOkResult
      }
    }
  }

  def testStuFinishResult(testStuId: String) = Auth { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        Ok(jsonView.lightResult(testStu)).fuccess
      }
    }
  }

  def testStuRound(testStuId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        implicit val req = ctx.body
        forms.test.round.bindFromRequest.fold(
          jsonFormError,
          data => {
            api.round(testStu, data) inject jsonOkResult
          }
        )
      }
    }
  }

  def testStuRedo(testStuId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuOwner(testStu) {
        implicit val req = ctx.body
        forms.test.redo.bindFromRequest.fold(
          jsonFormError,
          data => {
            api.redo(testStu, data) inject jsonOkResult
          }
        )
      }
    }
  }

  def testStuRemark(testStuId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(TestStudentRepo.byId(testStuId)) { testStu =>
      StuCreator(testStu) {
        implicit val req = ctx.body
        forms.test.remark.bindFromRequest.fold(
          jsonFormError,
          data => {
            api.setRemark(testStu, data) inject jsonOkResult
          }
        )
      }
    }
  }

  private def fetchPuzzleCount(capsuleIds: List[String]): Fu[Int] =
    Env.resource.capsuleApi.byIds(capsuleIds) flatMap { capsules =>
      val ids = capsules.flatMap(_.ids)
      Env.puzzle.api.puzzle.findMany4Count(ids)
    }

  private def fetchPuzzle(tpl: TestTemplate): Fu[List[MiniPuzzle]] =
    Env.resource.capsuleApi.byIds(tpl.capsuleIds) flatMap { capsules =>
      val ids = capsules.flatMap(_.ids)
      Env.puzzle.api.puzzle.findMany4(ids) map { puzzles =>
        puzzles.map { puzzle =>
          MiniPuzzle(
            id = puzzle.id,
            fen = puzzle.fenAfterInitialMove | puzzle.fen,
            color = puzzle.color,
            rating = puzzle.intRating,
            depth = puzzle.depth,
            lines = lila.puzzle.Line.toJson(puzzle.lines).toString,
            lastMove = puzzle.initialUciOption
          )
        }
      }
    }

  private[controllers] def StuOwnerOrCreator(testStu: TestStudent)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => testStu.isStudent(me.id) || testStu.isCreator(me.id))) f
    else Forbidden(views.html.site.message.authFailed).fuccess

  private[controllers] def StuCreator(testStu: TestStudent)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => testStu.isCreator(me.id))) f
    else Forbidden(views.html.site.message.authFailed).fuccess

  private[controllers] def StuOwner(testStu: TestStudent)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => testStu.isStudent(me.id))) f
    else Forbidden(views.html.site.message.authFailed).fuccess

  private[controllers] def Owner(tpl: TestTemplate)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => tpl.isCreator(me.id))) f
    else Forbidden(views.html.site.message.authFailed).fuccess

  private[controllers] def OwnerAndEnable(tpl: TestTemplate)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => tpl.isCreator(me.id) && !tpl.isCanceled)) f
    else Forbidden(views.html.site.message.authFailed).fuccess

}
