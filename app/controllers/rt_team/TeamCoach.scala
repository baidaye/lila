package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import lila.user.UserRepo
import lila.team.{ CampusRepo, MemberRepo, TagRepo, Member => MemberModel }
import views._

object TeamCoach extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms

  def coach(id: String, coach: Option[lila.user.User.ID] = None) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          coaches <- MemberRepo.coachByTeam(id)
          coachesWithoutOwner = coaches.filter(_.member.isCoach)
          cwu <- coach.fold(fuccess(coachesWithoutOwner.headOption)) { c => MemberRepo.memberWithUser(MemberModel.makeId(id, c)).map(_.filter(_.member.isCoach)) }
          campuses <- cwu.??(_ => CampusRepo.byTeam(id))
          studentIds <- cwu.??(c => Env.coach.studentApi.mineStudents(c.user.some))
          students <- cwu.??(_ => MemberRepo.membersWithUser(id, studentIds.toList))
          studies <- cwu.??(c => Env.study.api.teamMemberStudies(c.userId, me.id))
          capsules <- cwu.??(c => lila.resource.CapsuleRepo.memberFor(me.id, c.userId))
          openingdbs <- cwu.??(c => lila.opening.OpeningDBRepo.memberFor(me.id, c.userId))
          clazzs <- cwu.??(c => Env.clazz.api.byTeams(campuses.map(_.id), c.userId))
          markMap <- cwu.??(_ => api.userMarks(me.id))
        } yield Ok(html.team.coach(team, cwu, campuses, students, studies, capsules, openingdbs, clazzs, markMap))
      }
    }
  }

  def changeCoachModal(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          coaches <- MemberRepo.coachByTeam(id)
          coachesWithoutOwner = coaches.filter(_.member.isCoach)
          markMap <- api.userMarks(me.id)
        } yield Ok(html.team.coach.changeCoachModal(team, coach, coachesWithoutOwner, markMap))
      }
    }
  }

  def removeCoachModal(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          memberOption <- MemberRepo.memberWithUser(MemberModel.makeId(id, coach))
          coachOption = memberOption.filter(_.member.isCoach)
          campuses <- coachOption.??(_ => CampusRepo.byTeam(id))
          coachStudentIds <- coachOption.?? { cu => Env.coach.studentApi.mineStudents(cu.user.some) }
          teamStudentIds <- coachOption.?? { _ => MemberRepo.userIdsByUserIds(id, coachStudentIds.toList) }
          studies <- coachOption.??(_ => Env.study.api.teamMemberStudies(coach, me.id))
          capsules <- coachOption.??(_ => lila.resource.CapsuleRepo.memberFor(me.id, coach))
          clazzs <- coachOption.??(_ => Env.clazz.api.byTeams(campuses.map(_.id), coach))
          coachRoleCampuses <- coachOption.??(_ => CampusRepo.byCoach(id, coach))
          markMap <- coachOption.??(_ => api.userMarks(me.id))
        } yield {
          coachOption match {
            case None => Redirect(routes.TeamCoach.coach(team.id, coach.some))
            case Some(cu) => Ok(html.team.coach.remove(team, cu, teamStudentIds.toList, studies, capsules, clazzs, coachRoleCampuses, markMap))
          }
        }
      }
    }
  }

  def removeCoachApply(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        (for {
          memberOption <- MemberRepo.memberWithUser(MemberModel.makeId(id, coach))
          coachOption = memberOption.filter(_.member.isCoach)
          campuses <- coachOption.??(_ => CampusRepo.byTeam(id))
          coachStudentIds <- coachOption.?? { cu => Env.coach.studentApi.mineStudents(cu.user.some) }
          teamStudentIds <- coachOption.?? { _ => MemberRepo.userIdsByUserIds(id, coachStudentIds.toList) }
          studies <- coachOption.??(_ => Env.study.api.teamMemberStudies(coach, me.id))
          capsules <- coachOption.??(_ => lila.resource.CapsuleRepo.memberFor(me.id, coach))
          clazzs <- coachOption.??(_ => Env.clazz.api.byTeams(campuses.map(_.id), coach))
          coachRoleCampuses <- coachOption.??(_ => CampusRepo.byCoach(id, coach))
        } yield (((((coachOption, teamStudentIds), studies), capsules), clazzs), coachRoleCampuses)).flatMap {
          case (((((coachOption, teamStudentIds), studies), capsules), clazzs), coachRoleCampuses) => coachOption match {
            case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
            case Some(_) => {
              if (clazzs.nonEmpty || teamStudentIds.nonEmpty || studies.nonEmpty || capsules.nonEmpty || coachRoleCampuses.nonEmpty) Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
              else MemberRepo.removeRole(id, coach, MemberModel.Role.Coach) inject Redirect(routes.TeamCoach.coach(team.id))
            }
          }
        }
      }
    }
  }

  def addCoachModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          all <- UserRepo.allCoach()
          members <- MemberRepo.notCoach(id, all.map(_.id))
          markMap <- api.userMarks(me.id)
        } yield Ok(html.team.coach.add(team, members.toList, markMap))
      }
    }
  }

  def addCoachApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.coach.addCoach.bindFromRequest.fold(
          _ => BadRequest("can not apply coach").fuccess,
          coach => api.addCoach(id, coach) inject Redirect(routes.TeamCoach.coach(team.id, coach.some))
        )
      }
    }
  }

  def addCoachCampusModal(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          campuses <- CampusRepo.byTeam(id)
          markMap <- api.userMarks(me.id)
        } yield Ok(html.team.coach.addCampusModal(team, coach, campuses, markMap))
      }
    }
  }

  def addCoachCampusApply(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.coach.addCoachCampus.bindFromRequest.fold(
          _ => BadRequest("can not apply campus").fuccess,
          campusId => CampusRepo.addCoach(campusId, coach) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#campuses")
        )
      }
    }
  }

  def removeCoachCampus(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#campuses").fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.removeCoachCampus.bindFromRequest.fold(
                _ => BadRequest("can not apply campus").fuccess,
                campusId => CampusRepo.removeCoach(campusId, coach) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#campuses")
              )
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

  def addCoachStuModal(id: String, coach: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        for {
          campuses <- CampusRepo.byTeam(id)
          tags <- TagRepo.findByTeam(id)
        } yield {
          Ok(html.team.coach.addStuModal(
            form = Env.team.forms.member.memberSearch,
            team = team,
            coach = coach,
            campuses = campuses,
            tags = tags
          ))
        }
      }
    }
  }

  def addCoachStuApply(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.coach.addCoachStu.bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          stus => {
            (for {
              memberOption <- MemberRepo.memberWithUser(MemberModel.makeId(id, coach))
              coachOption = memberOption.filter(_.member.isCoach)
              coachStudentIds <- coachOption.?? { cu => Env.coach.studentApi.mineStudents(cu.user.some) }
            } yield (coachOption, coachStudentIds)).flatMap {
              case (coachOption, coachStudentIds) => {
                coachOption match {
                  case None => Redirect(routes.TeamCoach.coach(team.id)).fuccess
                  case Some(cu) => {
                    val userIds = stus.filterNot(u => coachStudentIds.contains(u))
                    if (userIds.isEmpty) {
                      Redirect(routes.TeamCoach.coach(team.id)).fuccess
                    } else {
                      Env.coach.studentApi.addStudents(cu.userId, userIds) inject Redirect(s"${routes.TeamCoach.coach(team.id, cu.userId.some)}#students")
                    }
                  }
                }
              }
            }
          }
        )
      }
    }
  }

  def removeCoachStu(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.removeCoachStu.bindFromRequest.fold(
                _ => BadRequest("can not apply student").fuccess,
                userId => Env.coach.studentApi.removeStudent(coach, userId) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#students")
              )
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

  def removeCoachStus(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              Env.coach.studentApi.mineStudents(mu.user.some).flatMap { studentIds =>
                MemberRepo.userIdsByUserIds(id, studentIds.toList).flatMap { memberIds =>
                  Env.coach.studentApi.removeStudents(coach, memberIds.toList) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#students")
                }
              }
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

  def setCoachStudyRole(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.coachStudyRole.bindFromRequest.fold(
                fail => BadRequest(fail.toString).fuccess,
                data => Env.study.api.setRole(me.id, lila.study.Study.Id(data.studyId), coach, data.role) inject jsonOkResult
              )
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

  def removeCoachStudy(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.removeCoachStudy.bindFromRequest.fold(
                _ => BadRequest("can not apply studyId").fuccess,
                studyId => Env.study.api.kick(me.id, lila.study.Study.Id(studyId), coach) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#studies")
              )
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#studies").fuccess
          }
        }
      }
    }
  }

  def removeCoachStudys(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              Env.study.api.teamMemberStudies(coach, me.id).flatMap {
                _.map { study =>
                  Env.study.api.kick(me.id, study.id, coach)
                }.sequenceFu inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#studies")
              }
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#studies").fuccess
          }
        }
      }
    }
  }

  def removeCoachCapsules(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              lila.resource.CapsuleRepo.memberFor(me.id, coach).flatMap { capsules =>
                Env.resource.capsuleApi.removeMembers(capsules, mu.user) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#capsules")
              }
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#capsules").fuccess
          }
        }
      }
    }
  }

  def removeCoachCapsule(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.removeCoachCapsule.bindFromRequest.fold(
                _ => BadRequest("can not apply capsuleId").fuccess,
                capsuleId => Env.resource.capsuleApi.removeMemberById(capsuleId, mu.user) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#capsules")
              )
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#capsules").fuccess
          }
        }
      }
    }
  }

  def setCoachCapsuleRole(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.coachCapsuleRole.bindFromRequest.fold(
                fail => BadRequest(fail.toString).fuccess,
                data => Env.resource.capsuleApi.setMemberRoleById(data.capsuleId, mu.user, data.role) inject jsonOkResult
              )
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

  //--------------------

  def removeCoachOpeningdbs(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              lila.opening.OpeningDBRepo.memberFor(me.id, coach).flatMap { openingdbs =>
                Env.opening.api.removeMembers(openingdbs, mu.user) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#openingdbs")
              }
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#openingdbs").fuccess
          }
        }
      }
    }
  }

  def removeCoachOpeningdb(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.removeCoachOpeningdb.bindFromRequest.fold(
                _ => BadRequest("can not apply openingdbId").fuccess,
                openingdbId => Env.opening.api.removeMemberById(openingdbId, mu.user) inject Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#openingdbs")
              )
            } else Redirect(s"${routes.TeamCoach.coach(team.id, coach.some)}#openingdbs").fuccess
          }
        }
      }
    }
  }

  def setCoachOpeningdbRole(id: String, coach: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        MemberRepo.memberWithUser(MemberModel.makeId(id, coach)).flatMap {
          case None => Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          case Some(mu) => {
            if (mu.member.isCoach) {
              implicit val req = ctx.body
              forms.coach.coachOpeningdbRole.bindFromRequest.fold(
                fail => BadRequest(fail.toString).fuccess,
                data => Env.opening.api.setMemberRoleById(data.openingdbId, mu.user, data.role) inject jsonOkResult
              )
            } else Redirect(routes.TeamCoach.coach(team.id, coach.some)).fuccess
          }
        }
      }
    }
  }

}
