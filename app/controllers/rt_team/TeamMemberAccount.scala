package controllers.rt_team

import lila.app._
import play.api.mvc._
import lila.api.Context
import play.api.libs.json._
import play.api.mvc.Result
import org.joda.time.DateTime
import lila.common.LameName
import lila.common.paginator.Paginator
import lila.user.UserRepo
import lila.user.User.ClearPassword
import lila.team.{ Team => TeamModel }
import views.html

object TeamMemberAccount extends controllers.LilaController {

  private val api = Env.team.api
  private val forms = Env.team.forms
  private val memberSelector = Env.team.memberSelector

  def memberAccount(id: String, page: Int = 1) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        api.userMarks(me.id).flatMap { markMap =>
          implicit val req = ctx.body
          val form = forms.memberAccount.memberAccountSearch.bindFromRequest
          form.fold(
            fail => {
              BadRequest(html.team.memberAccount(fail, team, Paginator.empty)).fuccess
            },
            data => {
              memberSelector.teamMemberAccount(team, page, data, markMap) map { pager =>
                Ok(html.team.memberAccount(form, team, pager))
              }
            }
          )
        }
      }
    }
  }

  def memberAccountAddModal(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        Ok(html.team.memberAccount.addModal(
          form = forms.memberAccount.memberAccountAddOf(team),
          team = team
        )).fuccess
      }
    }
  }

  def memberAccountAddApply(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api team id) { team =>
      Team.OwnerAndEnable(team) {
        implicit val req = ctx.body
        forms.memberAccount.memberAccountAdd(team).bindFromRequest.fold(
          err => BadRequest(errorsAsJson(err)).fuccess,
          data => MemberAccountValid(team, data) {
            val trackId = DateTime.now.getMillis
            controllerLogger.info(s"[学生账号][开始]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId")
            val passwordHash = Env.user.authenticator passEnc ClearPassword(data.password)
            UserRepo.createOfTeam(data.realUsernames, passwordHash, me) flatMap { members =>
              val memberIds = members.map(_.id)
              controllerLogger.info(s"[学生账号][创建账号]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds")
              api.batchJoin(team, members) flatMap { _ =>
                controllerLogger.info(s"[学生账号][加入俱乐部]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds")
                if (data.nofreeLevel) {
                  Env.member.memberCardApi.batchGive2(me, members, lila.member.BatchGive(memberIds, data.cardLevel, data.days)) flatMap { memberWithCards =>
                    val memberCardIds = memberWithCards.map {
                      case (member, card) => (member.id -> card.id)
                    }
                    controllerLogger.info(s"[学生账号][赠送会员卡]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds，cardIds：$memberCardIds")
                    Env.member.memberCardApi.batchUse(memberWithCards) map { _ =>
                      controllerLogger.info(s"[学生账号][使用会员卡]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds，cardIds：$memberCardIds")
                      controllerLogger.info(s"[学生账号][结束]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds")
                      jsonOkResult
                    }
                  }
                } else {
                  controllerLogger.info(s"[学生账号][结束]，teamId：${team.id}，teamName：${team.name}，trackId：$trackId，memberIds：$memberIds")
                  jsonOkResult.fuccess
                }
              }
            }
          }
        )
      }
    }
  }

  def memberAccountResetPasswordModal(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(UserRepo.byId(memberId)) { member =>
      member.imported.fold(notFoundJson("Resource not found")) { ipt =>
        OptionFuResult(api team ipt.teamId) { team =>
          Team.OwnerAndEnable(team) {
            Ok(html.team.memberAccount.resetPasswordModal(
              form = forms.memberAccount.memberAccountResetPassword,
              team = team,
              member = member
            )).fuccess
          }
        }
      }
    }
  }

  def memberAccountResetPasswordApply(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(UserRepo.byId(memberId)) { member =>
      member.imported.fold(notFoundJson("Resource not found")) { ipt =>
        OptionFuResult(api team ipt.teamId) { team =>
          Team.OwnerAndEnable(team) {
            if (member.enabled) {
              implicit val req = ctx.body
              forms.memberAccount.memberAccountResetPassword.bindFromRequest.fold(
                err => BadRequest(errorsAsJson(err)).fuccess,
                newPassword => {
                  Env.user.authenticator.setPassword(member.id, ClearPassword(newPassword)) inject jsonOkResult
                }
              )
            } else fuccess {
              Forbidden(jsonError("用户已停用")) as JSON
            }
          }
        }
      }
    }
  }

  def memberAccountUseCardModal(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(UserRepo.byId(memberId)) { member =>
      member.imported.fold(notFoundJson("Resource not found")) { ipt =>
        OptionFuResult(api team ipt.teamId) { team =>
          Team.OwnerAndEnable(team) {
            Ok(html.team.memberAccount.useCardModal(
              form = forms.memberAccount.memberAccountUseCardOf,
              team = team,
              member = member
            )).fuccess
          }
        }
      }
    }
  }

  def memberAccountUseCardApply(id: String, memberId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(UserRepo.byId(memberId)) { member =>
      member.imported.fold(notFoundJson("Resource not found")) { ipt =>
        OptionFuResult(api team ipt.teamId) { team =>
          Team.OwnerAndEnable(team) {
            if (member.enabled) {
              implicit val req = ctx.body
              forms.memberAccount.memberAccountUseCard.bindFromRequest.fold(
                err => BadRequest(errorsAsJson(err)).fuccess,
                data => {
                  Env.member.memberCardApi.give2(me, member, lila.member.BatchGive(List(member.id), data.cardLevel, data.days)) flatMap {
                    case Some(card) => {
                      controllerLogger.info(s"[学生账号][赠送会员卡]，teamId：${team.id}，teamName：${team.name}，memberId：${member.id}，cardId：${card.id}")
                      Env.member.memberCardApi.use(member, card) map { _ =>
                        controllerLogger.info(s"[学生账号][使用会员卡]，teamId：${team.id}，teamName：${team.name}，memberId：${member.id}，cardId：${card.id}")
                        jsonOkResult
                      }
                    }
                    case None => fuccess { BadRequest(Json.obj("cardLevel" -> JsArray(Seq(JsString("会员卡数量不足"))))) as JSON }
                  }
                }
              )
            } else fuccess {
              Forbidden(jsonError("用户已停用")) as JSON
            }
          }
        }
      }
    }
  }

  def memberAccountClose(id: String, memberId: String) = Auth { implicit ctx => me =>
    OptionFuResult(UserRepo.byId(memberId)) { member =>
      if (member.enabled) {
        member.imported.fold(notFoundJson("Resource not found")) { ipt =>
          OptionFuResult(api team ipt.teamId) { team =>
            Team.OwnerAndEnable(team) {
              Env.current.closeAccount(member.id, self = false) inject jsonOkResult
            }
          }
        }
      } else fuccess {
        Forbidden(jsonError("用户已停用")) as JSON
      }
    }
  }

  private def MemberAccountValid(team: TeamModel, data: lila.team.DataForm.memberAccountData.MemberAccountAdd)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    val ru = data.realUsernames

    val maxUsernameLen = 20

    def validUsernameCount = {
      ru.nonEmpty && ru.size <= lila.team.DataForm.memberAccountData.MemberAccountMaxSize
    }

    def validUsernameLength(username: String) =
      username.getBytes("GBK").length <= maxUsernameLen

    def validUsernameRegex = {
      ru.forall { username =>
        validUsernameLength(username) && lila.user.User.couldBeUsername(username)
      }
    }

    def validUsernameRegexMessage = {
      ru.filterNot { username =>
        validUsernameLength(username) && lila.user.User.couldBeUsername(username)
      }.mkString("，")
    }

    def validSensitive = {
      ru.forall { username =>
        !LameName.sensitive(username)
      }
    }

    def validSensitiveMessage = {
      ru.filter { username =>
        LameName.sensitive(username)
      }.mkString("，")
    }

    def validReserved = {
      ru.forall { username =>
        !LameName.reserved(username)
      }
    }

    def validReservedMessage = {
      ru.filter { username =>
        LameName.reserved(username)
      }.mkString("，")
    }

    def validUsernameExists = UserRepo.existingUsernameIds(ru.toSet).map(_.nonEmpty)

    def validUsernameExistsMessage = {
      UserRepo.byIds(ru.map(lila.user.User.normalize)) map { users =>
        users.map(_.username).mkString("，")
      }
    }

    def validMemberCardCount = ctx.me.?? { me =>
      if (data.nofreeLevel) {
        Env.member.memberCardApi.cardCount(me.id, data.cardLevel, data.days).map(_ >= ru.size)
      } else fuTrue
    }

    if (validUsernameCount) {
      if (validUsernameRegex) {
        if (validSensitive) {
          if (validReserved) {
            validUsernameExists flatMap { usernameExists =>
              if (!usernameExists) {
                validMemberCardCount flatMap { cardCountExists =>
                  if (cardCountExists) f
                  else fuccess { BadRequest(Json.obj("cardLevel" -> JsArray(Seq(JsString("会员卡数量不足"))))) as JSON }
                }
              } else {
                validUsernameExistsMessage.map { validUsernameExistsMessage =>
                  BadRequest(Json.obj("usernames" -> JsArray(Seq(JsString(s"账号已经存在：$validUsernameExistsMessage"))))) as JSON
                }
              }
            }
          } else fuccess { BadRequest(Json.obj("usernames" -> JsArray(Seq(JsString(s"该用户账号已保留：$validReservedMessage"))))) as JSON }
        } else fuccess { BadRequest(Json.obj("usernames" -> JsArray(Seq(JsString(s"账号请避免使用敏感词：$validSensitiveMessage"))))) as JSON }
      } else fuccess { BadRequest(Json.obj("usernames" -> JsArray(Seq(JsString(s"账号格式错误：$validUsernameRegexMessage"))))) as JSON }
    } else fuccess { BadRequest(Json.obj("usernames" -> JsArray(Seq(JsString(s"最多添加1-${lila.team.DataForm.memberAccountData.MemberAccountMaxSize}个账号"))))) as JSON }
  }

}
