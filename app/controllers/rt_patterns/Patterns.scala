package controllers.rt_patterns

import lila.app._
import lila.patterns.{
  PatternsCheckmate1,
  PatternsCheckmate1Chapter,
  PatternsCheckmate1ProgressRepo,
  PatternsCheckmate1Repo,
  PatternsCheckmate1Typ,
  PatternsKingposRank,
  PatternsKingposRankFeature,
  PatternsKingposRankFeatureRepo,
  PatternsKingposRankRepo,
  PatternsOp,
  PatternsPieceRank,
  PatternsPieceRankRepo,
  PatternsRank,
  PatternsRankFeature,
  PatternsRankFeatureRepo,
  PatternsRankRepo
}
import chess.format.Forsyth
import play.api.libs.json._
import views.html

object Patterns extends controllers.LilaController {

  private val env = Env.patterns
  private val forms = env.forms
  private val jsonView = env.jsonView

  def index = Auth { implicit ctx => me =>
    Ok(html.patterns.index()).fuccess
  }

  def intro = Auth { implicit ctx => me =>
    Ok(html.patterns.about.intro()).fuccess
  }

  def eBook = Auth { implicit ctx => me =>
    Ok(html.patterns.about.eBook()).fuccess
  }

  def patternsRank(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.patternsRankSearch.bindFromRequest
    val typ = PatternsOp.PatternsType(patternsType)
    searchForm.fold(
      _ => Ok(html.patterns.patternsRank(typ, searchForm, PatternsRank.EmptyNeighbor)).fuccess,
      data => {
        PatternsRankRepo.next(typ, data) map { rankNeighbor =>
          Ok(html.patterns.patternsRank(typ, searchForm, rankNeighbor))
        }
      }
    )
  }

  def nextPatternsRank(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.patternsRankSearch.bindFromRequest
    searchForm.fold(
      _ => notFoundJson(),
      data => {
        PatternsRankRepo.next(PatternsOp.PatternsType(patternsType), data) map { featureNeighbor =>
          Ok(jsonView.patternsRankNeighbor(featureNeighbor))
        }
      }
    )
  }

  def patternsRankFeature(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.patternsRankFeatureSearch.bindFromRequest
    val typ = PatternsOp.PatternsType(patternsType)

    searchForm.fold(
      _ => Ok(html.patterns.patternsRankFeature(typ, searchForm, PatternsRankFeature.EmptyNeighbor)).fuccess,
      data => {
        if (me.hasPatterns) {
          PatternsRankFeatureRepo.next(typ, data) map { rankFeatureNeighbor =>
            Ok(html.patterns.patternsRankFeature(typ, searchForm, rankFeatureNeighbor))
          }
        } else Ok(html.patterns.patternsRankFeature(typ, searchForm, PatternsRankFeature.EmptyNeighbor)).fuccess
      }
    )
  }

  def nextPatternsRankFeature(patternsType: String) = AuthBody { implicit ctx => me =>
    if (me.hasPatterns) {
      implicit def req = ctx.body
      def searchForm = forms.patternsRankFeatureSearch.bindFromRequest
      searchForm.fold(
        _ => notFoundJson(),
        data => {
          PatternsRankFeatureRepo.next(PatternsOp.PatternsType(patternsType), data) map { featureNeighbor =>
            Ok(jsonView.patternsRankFeatureNeighbor(featureNeighbor))
          }
        }
      )
    } else fuccess(NotAcceptable("仅会员可用"))
  }

  def kingposRank(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.kingposRankSearch.bindFromRequest
    val typ = PatternsOp.PatternsType(patternsType)
    searchForm.fold(
      _ => Ok(html.patterns.kingposRank(typ, searchForm, List.empty[PatternsKingposRank])).fuccess,
      data => {
        PatternsKingposRankRepo.find(typ, data) map { list =>
          Ok(html.patterns.kingposRank(typ, searchForm, list))
        }
      }
    )
  }

  def findKingposRank(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.kingposRankSearch.bindFromRequest
    searchForm.fold(
      _ => notFoundJson(),
      data => PatternsKingposRankRepo.find(PatternsOp.PatternsType(patternsType), data) map { list =>
        Ok(jsonView.kingposRanks(list))
      }
    )
  }

  def kingposRankFeature(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.kingposRankFeatureSearch.bindFromRequest
    val typ = PatternsOp.PatternsType(patternsType)
    if (me.hasPatterns) {
      searchForm.fold(
        _ => Ok(html.patterns.kingposRankFeature(typ, searchForm, PatternsKingposRankFeature.EmptyNeighbor)).fuccess,
        data => PatternsKingposRankFeatureRepo.next(typ, data) map { featureNeighbor =>
          Ok(html.patterns.kingposRankFeature(typ, searchForm, featureNeighbor))
        }
      )
    } else Ok(html.patterns.kingposRankFeature(typ, searchForm, PatternsKingposRankFeature.EmptyNeighbor)).fuccess
  }

  def nextKingposRankFeature(patternsType: String) = AuthBody { implicit ctx => me =>
    if (me.hasPatterns) {
      implicit def req = ctx.body
      def searchForm = forms.kingposRankFeatureSearch.bindFromRequest
      searchForm.fold(
        _ => notFoundJson(),
        data => PatternsKingposRankFeatureRepo.next(PatternsOp.PatternsType(patternsType), data) map { featureNeighbor =>
          Ok(jsonView.kingposRankFeatureNeighbor(featureNeighbor))
        }
      )
    } else fuccess(NotAcceptable("仅会员可用"))
  }

  def patternsPieceRank(patternsType: String) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.patternsPieceRankSearch.bindFromRequest
    val typ = PatternsOp.PatternsType(patternsType)
    if (me.hasPatterns) {
      searchForm.fold(
        _ => Ok(html.patterns.patternsPieceRank(typ, searchForm, PatternsPieceRank.EmptyNeighbor)).fuccess,
        data => PatternsPieceRankRepo.next(typ, data) map { pieceRankNeighbor =>
          Ok(html.patterns.patternsPieceRank(typ, searchForm, pieceRankNeighbor))
        }
      )
    } else Ok(html.patterns.patternsPieceRank(typ, searchForm, PatternsPieceRank.EmptyNeighbor)).fuccess
  }

  def nextPatternsPieceRank(patternsType: String) = AuthBody { implicit ctx => me =>
    if (me.hasPatterns) {
      implicit def req = ctx.body
      def searchForm = forms.patternsPieceRankSearch.bindFromRequest
      searchForm.fold(
        _ => notFoundJson(),
        data => PatternsPieceRankRepo.next(PatternsOp.PatternsType(patternsType), data) map { pieceRankNeighbor =>
          Ok(jsonView.patternsPieceRankNeighbor(pieceRankNeighbor))
        }
      )
    } else fuccess(NotAcceptable("仅会员可用"))
  }

  def randomOp(patternsType: String, color: Boolean) = Auth { implicit ctx => me =>
    fuccess {
      Ok(PatternsOp.random(PatternsOp.PatternsType(patternsType), chess.Color(color)))
    }
  }

  def verifyOp(patternsType: String, op: String) = Auth { implicit ctx => me =>
    fuccess {
      Ok(JsBoolean(PatternsOp.verify(PatternsOp.PatternsType(patternsType), op)))
    }
  }

  def checkmate1Map = Auth { implicit ctx => me =>
    PatternsCheckmate1ProgressRepo.findOrEmpty(me.id) map { progress =>
      Ok(html.patterns.checkmate1Map(progress))
    }
  }

  def checkmate1Chapter(chapter: Int, id: Option[PatternsCheckmate1.ID]) = Auth { implicit ctx => me =>
    PatternsCheckmate1ProgressRepo.findOrEmpty(me.id) flatMap { progress =>
      val ch = PatternsCheckmate1Chapter(chapter)
      (
        if (me.hasPatterns) {
          PatternsCheckmate1Repo.next(ch, progress, progress.lastId(ch), progress.typ, id.orElse(progress.lastId(ch).some))
        } else fuccess { PatternsCheckmate1.empty(progress.lastId(ch), ch.id) }
      ) map { checkmate1 =>
          Ok(html.patterns.checkmate1Chapter(
            PatternsCheckmate1Chapter(chapter),
            jsonView.checkmate1Progress(progress),
            jsonView.checkmate1Chapter(checkmate1),
            jsonView.pref(ctx.pref)
          ))
        }
    }
  }

  def checkmate1ChapterNext(chapter: Int, lastId: PatternsCheckmate1.ID, typ: String) = Auth { implicit ctx => me =>
    if (me.hasPatterns) {
      PatternsCheckmate1ProgressRepo.findOrEmpty(me.id) flatMap { progress =>
        PatternsCheckmate1Repo.next(PatternsCheckmate1Chapter(chapter), progress, lastId, PatternsCheckmate1Typ(typ), None) map { checkmate1 =>
          Ok(jsonView.checkmate1Chapter(checkmate1))
        }
      }
    } else fuccess(NotAcceptable("仅会员可用"))
  }

  def checkmate1ChapterRound(chapter: Int, id: PatternsCheckmate1.ID) = AuthBody { implicit ctx => me =>
    implicit def req = ctx.body
    def searchForm = forms.patternsCheckmateRound.bindFromRequest
    searchForm.fold(
      fail => BadRequest(jsonError(fail.toString)).fuccess,
      data => {
        PatternsCheckmate1ProgressRepo.findOrEmpty(me.id) flatMap { progress =>
          val newProgress = progress.withCheckmate1Result(PatternsCheckmate1Chapter(chapter), id, data.win, data.realOrient, data.realTyp)
          PatternsCheckmate1ProgressRepo.upsert(newProgress) map { _ =>
            Ok(jsonView.checkmate1Progress(newProgress))
          }
        }
      }
    )
  }

  def checkmate1ResetProgress() = Auth { implicit ctx => me =>
    PatternsCheckmate1ProgressRepo.findOrEmpty(me.id) flatMap { progress =>
      PatternsCheckmate1ProgressRepo.resetProgress(progress) inject jsonOkResult
    }
  }

  val emptyFen = "4k3/8/8/8/8/8/8/8 b - -"

  def designerIndex = Auth { implicit ctx => me =>
    fuccess {
      Redirect(routes.Patterns.designerLoad(emptyFen, "white"))
    }
  }

  def designerLoad(urlFen: String, color: String) = Auth { implicit ctx => me =>
    val fenStr =
      lila.common.String.decodeUriPath(urlFen)
        .map(_.replace('_', ' ').trim)
        .filter(_.nonEmpty)
        .orElse(get("fen"))
    fuccess {
      val defaultEmptyFen = emptyFen
      val situation = fenStr.map(_.trim).filter(_.nonEmpty).flatMap(Forsyth.<<<).map(_.situation) | (Forsyth << defaultEmptyFen).get
      Ok(html.patterns.designer(
        sit = situation,
        fen = Forsyth >> situation,
        color = color,
        animationDuration = Env.api.EditorAnimationDuration
      ))
    }
  }

}
