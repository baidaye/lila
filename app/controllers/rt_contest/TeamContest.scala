package controllers.rt_contest

import lila.app._
import play.api.mvc.{ BodyParsers, Result }
import play.api.data.Forms._
import play.api.data.Form
import play.api.libs.json.Json
import lila.api.Context
import lila.memo.UploadRateLimit
import lila.user.UserRepo
import lila.security.Permission
import lila.team.{ CampusRepo, TagRepo, TeamFederation, TeamFederationMemberRepo, TeamFederationRepo, TeamRatingRepo, TeamRepo }
import lila.teamContest.{ ForbiddenRepo, TeamBoardRepo, TeamPairedRepo, TeamPlayerRepo, TeamPlayerScoreSheetRepo, TeamRoundRepo, TeamScoreSheetRepo, TeamerRepo, TeamContest => ContestModel }
import lila.common.Form.numberIn
import lila.contest.DataForm.booleanChoices
import lila.game.GameRepo
import org.joda.time.DateTime
import scala.concurrent.duration._
import ornicar.scalalib.Random
import views._

object TeamContest extends controllers.LilaController {

  private def env = Env.teamContest
  private def api = env.contestApi
  private def roundApi = env.roundApi
  private def forms = env.forms
  private def jsonView = env.jsonView

  def sequencingTest = Auth { implicit ctx => me =>
    api.sequencingTest("YyT0lnUg")
    Ok("OK").fuccess
  }

  def manualFinishGame(gameId: String) = Secure(_.SuperAdmin) { implicit ctx => me =>
    OptionResult(GameRepo.game(gameId)) { game =>
      api.finishGame(game)
      Ok("ok")
    }
  }

  def home = Auth { implicit ctx => me =>
    federationIds(me) flatMap { fIds =>
      env.pager.enter(1, none, me, fIds, "") flatMap { pag =>
        Ok(html.teamContest.list.enter(pag, none, "")).fuccess
      }
    }
  }

  def enterPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    federationIds(me) flatMap { fIds =>
      env.pager.enter(page, status, me, fIds, text) flatMap { pag =>
        Ok(html.teamContest.list.enter(pag, status, text)).fuccess
      }
    }
  }

  def belongPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    env.pager.belong(page, status, me, text) flatMap { pag =>
      Ok(html.teamContest.list.belong(pag, status, text)).fuccess
    }
  }

  def ownerPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    env.pager.owner(page, status, me, text) flatMap { pag =>
      Ok(html.teamContest.list.owner(pag, status, text)).fuccess
    }
  }

  def finishPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
      env.pager.finish(page, status, me, clazzIds, text) flatMap { pag =>
        Ok(html.teamContest.list.finish(pag, status, text)).fuccess
      }
    }
  }

  def ruleInfo(id: String) = Auth { implicit ctx => me =>
    Permiss {
      val r = ContestModel.Rule(id)
      Ok(jsonView.rule(r)).fuccess
    }
  }

  def createForm(rule: Option[String]) = Auth { implicit ctx => me =>
    Permiss {
      NoLameOrBot {
        val rl = rule.map(r => if (r.isEmpty) ContestModel.Rule.Swiss else ContestModel.Rule.apply(r)) | ContestModel.Rule.Swiss
        for {
          teams <- Contest.teamList(me)
          federations <- federations(me)
        } yield Ok(html.teamContest.form.create(forms.contestDefault(me, rl), rl, teams, federations))
      }
    }
  }

  def create() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      for {
        teams <- Contest.teamList(me)
        federations <- federations(me)
        res <- {
          val rl =
            (req.body match {
              case body: play.api.mvc.AnyContent if body.asFormUrlEncoded.isDefined => body.asFormUrlEncoded.get
              case _ => Map.empty[String, Seq[String]]
            }).get("basics.rule").map {
              _.headOption.map { ContestModel.Rule.apply } | ContestModel.Rule.Swiss
            } | ContestModel.Rule.Swiss

          forms.contest(me, None, rl).bindFromRequest.fold(
            err => {
              BadRequest(html.teamContest.form.create(err, rl, teams, federations)).fuccess
            },
            data => CreateLimitPerUser(me.id, cost = 1) {
              val contest = data.toContest(me)
              api.create(contest, data.roundList(contest.id)) map { c =>
                Redirect(routes.TeamContest.show(c.id))
              }
            }(rateLimited)
          )
        }
      } yield res
    }
  }

  def viewForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        for {
          teams <- Contest.teamList(me)
          federations <- federations(me)
          rounds <- TeamRoundRepo.getByContest(id)
        } yield Ok(html.teamContest.form.view(contest, forms.contestOf(me, contest, rounds), teams, federations))
      }
    }
  }

  def updateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          for {
            teams <- Contest.teamList(me)
            federations <- federations(me)
            rounds <- TeamRoundRepo.getByContest(id)
          } yield Ok(html.teamContest.form.update(contest, forms.contestOf(me, contest, rounds), teams, federations))
        }
      }
    }
  }

  def update(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          for {
            teams <- Contest.teamList(me)
            federations <- federations(me)
            rounds <- TeamRoundRepo.getByContest(id)
            res <- {
              implicit val req = ctx.body
              val rl =
                (req.body match {
                  case body: play.api.mvc.AnyContent if body.asFormUrlEncoded.isDefined => body.asFormUrlEncoded.get
                  case _ => Map.empty[String, Seq[String]]
                }).get("basics.rule").map {
                  _.headOption.map { ContestModel.Rule.apply } | ContestModel.Rule.Swiss
                } | ContestModel.Rule.Swiss

              forms.contest(me, id.some, rl).bindFromRequest.fold(
                err => BadRequest(html.teamContest.form.update(contest, err, teams, federations)).fuccess,
                data => {
                  val newContest = data.toContest(me)
                  val rounds = data.roundList(contest.id)
                  api.update(contest, newContest, rounds) inject Redirect(routes.TeamContest.show(id))
                }
              )
            }
          } yield res
        }
      }
    }
  }

  def clone(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        for {
          teams <- Contest.teamList(me)
          federations <- federations(me)
          rounds <- TeamRoundRepo.getByContest(id)
        } yield Ok(html.teamContest.form.clone(contest, forms.contestOf(me, contest, rounds), teams, federations))
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      for {
        rounds <- TeamRoundRepo.getByContest(id)
        markMap <- Env.relation.markApi.getMarks(me.id)
        teamers <- TeamerRepo.getByContest(contest.id)
        players <- api.playersWithUsers(contest, markMap, me)
        paireds <- TeamPairedRepo.getByContest(id)
        boards <- TeamBoardRepo.getByContest(id)
        forbiddens <- ForbiddenRepo.getByContest(id)
        teamRating <- contest.teamRated.?? { TeamRatingRepo.findByContest(id, Some(true)) }
        scoreSheets <- contest.isOverStarted.?? {
          val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round $id-${contest.currentRound}"
          val roundNo = if (round.isPublishResult) round.no else round.no - 1
          TeamScoreSheetRepo.getByRound(id, Math.max(roundNo, 1))
        }
        playerScoreSheets <- contest.isOverStarted.?? {
          val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round $id-${contest.currentRound}"
          val roundNo = if (round.isPublishResult) round.no else round.no - 1
          TeamPlayerScoreSheetRepo.getByRound(id, Math.max(roundNo, 1))
        }
      } yield html.teamContest.show(contest, rounds, teamers, players, paireds, boards, forbiddens, teamRating, scoreSheets, playerScoreSheets)
    }
  }

  def remove(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          api.remove(contest) inject Redirect(routes.TeamContest.home)
        }
      }
    }
  }

  def publish(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          if (contest.shouldEnterStop) ForbiddenResult("操作被拒绝。当前时间已经超过报名截止时间，您可以返回“编辑”页面，重新设置比赛“开始时间”后再进行操作。")
          else api.publish(contest) inject Redirect(routes.TeamContest.show(id))
        }
      }
    }
  }

  def cancel(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        api.cancel(contest) inject Redirect(routes.TeamContest.show(id))
      }
    }
  }

  def enterStop(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Published) {
          fuccess {
            (!contest.autoPairing).?? {
              api.setEnterStop(contest)
            }
          } inject Redirect(routes.TeamContest.show(id))
        }
      }
    }
  }

  def autoPairing(id: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!contest.isPublished && !contest.isEnterStopped && !contest.isStarted) ForbiddenJsonStatus(contest)
        else {
          implicit val req = ctx.body
          Form(single("autoPairing" -> numberIn(booleanChoices))).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            auto => api.setAutoPairing(contest, auto == 1) inject jsonOkResult
          )
        }
      }
    }
  }

  def manualAbsentForm(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || (contest.isRoundRobin && round.isPairing))) ForbiddenJsonStatus2
          else TeamerRepo.getByContest(contest.id) map { teamers =>
            Ok(html.teamContest.modal.manualAbsent(contest, round, teamers))
          }
        }
      }
    }
  }

  def manualAbsent(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || (contest.isRoundRobin && round.isPairing))) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(tuple(
              "joins" -> list(nonEmptyText),
              "absents" -> list(nonEmptyText)
            )).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              {
                case (joins, absents) => {
                  roundApi.manualAbsent(contest, round, joins, absents) inject jsonOkResult
                }
              }
            )
          }
        }
      }
    }
  }

  def pairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || round.isPairing)) ForbiddenJsonStatus2
          else {
            roundApi.pairing(contest, round, api.publishScoreAndFinish).map { succ =>
              if (succ) jsonOkResult
              else BadRequest(jsonError("当前匹配规则下已无法再进行匹配，您可以在成绩册中发布成绩结束比赛"))
            }
          }
        }
      }
    }
  }

  def setRoundRobin(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!(contest.isEnterStopped || contest.isStarted)) ForbiddenJsonStatus(contest)
        else {
          implicit val req = ctx.body
          forms.roundRobinForm(contest).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => {
              api.setRoundRobin(contest, data.list) inject {
                Ok(Json.obj(
                  "ok" -> true,
                  "nbTeamers" -> contest.nbTeamers
                )) as JSON
              }
            }
          )
        }
      }
    }
  }

  def pairingRoundRobin(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!(contest.isEnterStopped || contest.isStarted)) ForbiddenJsonStatus(contest)
        else api.pairingRoundRobin(contest) inject jsonOkResult
      }
    }
  }

  def publishPairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else {
            if (round.actualStartsAt.getMillis < DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0).plusMinutes(lila.contest.Round.beforeStartMinutes).getMillis) {
              BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess
            } else roundApi.publish(contest, round) inject jsonOkResult
          }
        }
      }
    }
  }

  def cancelPublishPairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !contest.rule.flow.cancelPairingPublish || !(contest.isEnterStopped || contest.isStarted) || !round.isPublished) ForbiddenJsonStatus2
          else {
            TeamBoardRepo.getByRound(round.id).flatMap { boards =>
              if (boards.forall(_.canCancel)) {
                roundApi.cancelPublish(contest, round, boards) inject jsonOkResult
              } else ForbiddenJsonResult("轮次中有已经开始或立即开始的对局，您不能做此操作")
            }
          }
        }
      }
    }
  }

  def setRoundStartsTime(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || round.isPairing)) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("startsAt" -> lila.common.Form.futureDateTime)).bindFromRequest.fold(
              _ => BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess,
              st => {
                if (st.getMillis < DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0).plusMinutes(lila.contest.Round.beforeStartMinutes).getMillis) {
                  BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess
                } else if (st.isBefore(contest.startsAt)) {
                  BadRequest(jsonError(s"轮次开始时间必须大于等于比赛开始时间")).fuccess
                } else roundApi.setStartsTime(contest, round, st) inject jsonOkResult
              }
            )
          }
        }
      }
    }
  }

  def publishResult(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- TeamRoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        Owner(contest) {
          if (contest.autoPairing || !contest.isStarted || !round.isFinished) ForbiddenStatus2
          else {
            roundApi.publishResult(contest, round.id, round.no) inject RedirectToShow(contest.id, s"round${round.no}".some)
          }
        }
      }
    }
  }

  def publishScoreAndFinish(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.autoPairing || !contest.isStarted || !contest.allRoundFinished) ForbiddenStatus2
        else {
          api.publishScoreAndFinish(contest) inject RedirectToShow(contest.id, "score".some)
        }
      }
    }
  }

  def cancelScore(id: String, sid: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      scoreSheetOption <- TeamScoreSheetRepo.byId(sid)
    } yield (contestOption |@| scoreSheetOption).tupled) {
      case (contest, scoreSheet) => {
        Owner(contest) {
          if (!contest.isStarted || contest.autoPairing || !contest.allRoundFinished) ForbiddenStatus2
          else api.cancelScore(contest, scoreSheet) inject RedirectToShow(contest.id, "score".some)
        }
      }
    }
  }

  def setBoardTimeForm(id: String, boardId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.isCreated) ForbiddenJsonStatus2
          else Ok(html.teamContest.modal.setBoardTime(contest, round, board)).fuccess
        }
      }
    }
  }

  def setBoardTime(id: String, boardId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.isCreated) ForbiddenJsonStatus2
          else {
            implicit def req = ctx.body
            import lila.common.Form.futureDateTime
            Form(single("startsAt" -> futureDateTime.verifying(
              s"日期必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}分钟）",
              DateTime.now.plusMinutes(lila.contest.Round.beforeStartMinutes - 1).isBefore(_)
            ))).bindFromRequest.fold(
              err => RedirectToShow(contest.id, s"round${round.no}".some).fuccess,
              startsAt => roundApi.setBoardTime(contest, round, board, startsAt) inject RedirectToShow(contest.id, s"round${round.no}".some)
            )
          }
        }
      }
    }
  }

  def setBoardSignForm(id: String, boardId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      pairedOption <- boardOption.?? { b => TeamPairedRepo.byId(b.pairedId) }
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
      whitePlayerOption <- boardOption.?? { b => UserRepo.byId(b.whitePlayer.userId) }
      blackPlayerOption <- boardOption.?? { b => UserRepo.byId(b.blackPlayer.userId) }
    } yield (boardOption |@| contestOption |@| roundOption |@| pairedOption |@| whitePlayerOption |@| blackPlayerOption).tupled) {
      case (board, contest, round, paired, whitePlayer, blackPlayer) => {
        if (ctx.me.??(me => contest.isCreator(me) || isGranted(_.ManageContest) || paired.isLeader(me.id))) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !(board.isCreated || board.isStarted)) ForbiddenJsonStatus2
          else {
            Env.relation.markApi.getMarks(me.id) map { markMap =>
              Ok(html.teamContest.modal.setBoardSign(contest, round, paired, board, whitePlayer, blackPlayer, markMap))
            }
          }
        } else ForbiddenJsonResult("操作权限不足。仅比赛管理员可进行此操作！")
      }
    }
  }

  def setBoardSign(id: String, boardId: String, userId: Option[String] = None) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      pairedOption <- boardOption.?? { b => TeamPairedRepo.byId(b.pairedId) }
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
    } yield (boardOption |@| pairedOption |@| contestOption |@| roundOption).tupled) {
      case (board, paired, contest, round) => {
        val isAgent = (contest.isCreator(me) || paired.isLeader(me.id)) && userId.exists(board.containsByUser)
        if (board.containsByUser(me.id) || isAgent) {
          roundApi.setBoardSign(board, (userId | me.id), isAgent) inject jsonOkResult
        } else ForbiddenJsonResult("操作被拒绝。")
      }
    }
  }

  def manualResultForm(id: String, boardId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isStarted || contest.isFinished) || !board.isFinished) ForbiddenJsonStatus2
          else Ok(html.teamContest.modal.manualResult(contest, round, board)).fuccess
        }
      }
    }
  }

  def manualResult(id: String, boardId: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        Owner(contest) {
          if (contest.autoPairing || !contest.isStarted || !board.isFinished) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("result" -> nonEmptyText.verifying(lila.teamContest.TeamBoard.Result.keys.contains(_)))).bindFromRequest.fold(
              err => RedirectToShow(contest.id, s"round${round.no}".some).fuccess,
              result => {
                roundApi.manualResult(contest, round, board, result) inject RedirectToShow(contest.id, s"round${round.no}".some)
              }
            )
          }
        }
      }
    }
  }

  def replaceFormalForm(id: String, boardId: String, playerId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
      playerOption <- TeamPlayerRepo.byId(playerId)
      teamerOption <- playerOption.?? { p => TeamerRepo.byId(p.teamerId) }
    } yield (contestOption |@| roundOption |@| boardOption |@| playerOption |@| teamerOption).tupled) {
      case (contest, round, board, player, teamer) => {
        if (teamer.isCreatorOrLeader(me.id)) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.isCreated || board.isSigned(playerId)) ForbiddenJsonStatus2
          else {
            for {
              markMap <- Env.relation.markApi.getMarks(me.id)
              boards <- TeamBoardRepo.getByRound(round.id)
              pwus <- api.teamerPlayersWithUsers(contest, teamer, markMap, me)
            } yield Ok(html.teamContest.modal.replaceFormal(contest, board, teamer, player, boards, pwus))
          }
        } else ForbiddenJsonResult("操作权限不足。仅比赛管理员可进行此操作！")
      }
    }
  }

  def replaceFormal(id: String, boardId: String, playerId: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(for {
      boardOption <- TeamBoardRepo.byId(boardId)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
      playerOption <- TeamPlayerRepo.byId(playerId)
      teamerOption <- playerOption.?? { p => TeamerRepo.byId(p.teamerId) }
    } yield (contestOption |@| roundOption |@| boardOption |@| playerOption |@| teamerOption).tupled) {
      case (contest, round, board, player, teamer) => {
        if (teamer.isCreatorOrLeader(me.id)) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.canReplace) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("target" -> nonEmptyText)).bindFromRequest.fold(
              err => RedirectToShow(contest.id, round.no.toString.some).fuccess,
              target => TeamPlayerRepo.byId(target).flatMap {
                case None => RedirectToShow(contest.id, s"round${round.no}".some).fuccess
                case Some(t) => roundApi.replaceFormal(contest, round, board, teamer, player, t) inject RedirectToShow(contest.id, s"round${round.no}".some)
              }
            )
          }
        } else ForbiddenJsonResult("操作权限不足。仅比赛管理员可进行此操作！")
      }
    }
  }

  def playerChooseForm(id: String, leader: Boolean, teamerId: Option[String]) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      if (!contest.chooseable) ForbiddenJsonStatus(contest)
      else {
        val teamId = me.teamIdValue
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          team <- TeamRepo.byId(teamId)
          campuses <- CampusRepo.byTeam(teamId)
          tags <- TagRepo.findByTeam(teamId)
          clazzes <- Env.clazz.api.byTeams(campuses.map(_.id))
          allPlayers <- api.allPlayersWithUsers(contest, markMap, me)
          teamerPlayers <- (!leader).?? { api.myTeamerPlayers(contest, me) }
        } yield {
          val all = if (leader) allPlayers else allPlayers.filterNot { p =>
            teamerPlayers.exists(tp => tp.userId == p.userId && !teamerId.has(tp.teamerId))
          }
          Ok(html.teamContest.modal.playerChoose(leader, Env.team.forms.member.memberSearch, contest, team, campuses, tags, clazzes, all))
        }
      }
    }
  }

  def joinForm(id: String, teamerId: Option[String]) = AuthBody { implicit ctx => implicit me =>
    NoLameOrBot {
      NoPlayban {
        OptionFuResult(api.byId(id)) { contest =>
          Status(contest, ContestModel.Status.Published) {
            if (contest.isTeamInner && !me.belongTeamId.has(contest.organizer)) {
              fuccess(Forbidden(jsonError("俱乐部内部赛不允许非俱乐部成员报名！")) as JSON)
            } else {
              for {
                markMap <- Env.relation.markApi.getMarks(me.id)
                teamerOption <- teamerId.?? { TeamerRepo.byId }
                leaderUsers <- teamerOption.?? { teamer => UserRepo.byIds(teamer.leaders) }
                pwus <- teamerOption.?? { teamer => api.teamerPlayersWithUsers(contest, teamer, markMap, me) }
              } yield Ok(html.teamContest.join(contest, teamerOption, pwus, forms.joinFormOf(contest, teamerOption, markMap, leaderUsers, me)))
            }
          }
        }
      }
    }
  }

  def join(id: String, teamerId: Option[String]) = AuthBody { implicit ctx => implicit me =>
    NoLameOrBot {
      NoPlayban {
        OptionFuResult(api.byId(id)) { contest =>
          StatusJson(contest, ContestModel.Status.Published) {
            implicit val req = ctx.body
            forms.joinForm(contest, teamerId).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              data => {
                val userIds = data.players.map(_.userId)
                TeamPlayerRepo.find(contest.id, userIds).flatMap { players =>
                  if (players.filterNot(p => teamerId.has(p.teamerId)).nonEmpty) {
                    fuccess(Forbidden(jsonError(s"棋手已经在其他队伍中报名（${players.map(_.userId).mkString("，")}）")) as JSON)
                  } else {
                    for {
                      old <- teamerId.?? { TeamerRepo.byId }
                      users <- UserRepo.byIds(userIds)
                      res <- api.joinRequest(contest, old, data, me, users) inject jsonOkResult
                    } yield res
                  }
                }
              }
            )
          }
        }
      }
    }
  }

  def joinProcess(id: String, teamerId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(teamer => api.byId(teamer.contestId))
    } yield (contestOption |@| teamerOption).tupled) {
      case (contest, teamer) => {
        Owner(contest) {
          if (!contest.joinProcessable) ForbiddenStatus(contest)
          else {
            implicit val req = ctx.body
            Form(single("process" -> nonEmptyText)).bindFromRequest.fold(
              _ => Redirect(routes.TeamContest.show(contest.id)).fuccess,
              decision => {
                val accept = decision === "accept"
                if (accept && contest.isTeamerFull) {
                  ForbiddenResult(s"报名名额已满（${contest.maxTeamers}）")
                } else {
                  api.processJoin(contest, teamer, accept) inject RedirectToShow(contest.id, "enter".some)
                }
              }
            )
          }
        }
      }
    }
  }

  def quit(id: String, teamerId: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(teamer => api.byId(teamer.contestId))
      roundOption <- contestOption.??(c => TeamRoundRepo.byId(lila.contest.Round.makeId(c.id, c.currentRound)))
    } yield (contestOption |@| roundOption |@| teamerOption).tupled) {
      case (contest, round, teamer) => {
        if (!contest.quitable || !teamer.isCreatorOrLeader(me.id)) ForbiddenStatus(contest)
        else {
          api.quit(contest, round, teamer, me) inject RedirectToShow(contest.id, "enter".some)
        }
      }
    }
  }

  def toggleKickPlayer(id: String, teamerId: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(teamer => api.byId(teamer.contestId))
      roundOption <- contestOption.??(c => TeamRoundRepo.byId(lila.contest.Round.makeId(c.id, c.currentRound)))
    } yield (contestOption |@| roundOption |@| teamerOption).tupled) {
      case (contest, round, teamer) => {
        Owner(contest) {
          if (!contest.teamerKickable) ForbiddenStatus(contest)
          else api.toggleKickPlayer(contest, round, teamer) inject RedirectToShow(contest.id, "enter".some)
        }
      }
    }
  }

  def showTeamer(id: String, teamerId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(teamer => api.byId(teamer.contestId))
    } yield (contestOption |@| teamerOption).tupled) {
      case (contest, teamer) => {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          pwus <- api.teamerPlayersWithUsers(contest, teamer, markMap, me)
        } yield Ok(html.teamContest.modal.showTeamer(contest, teamer, pwus))
      }
    }
  }

  def teamerScoreDetail(id: String, rno: Int, teamerId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(t => api.byId(t.contestId))
    } yield (contestOption |@| teamerOption).tupled) {
      case (contest, teamer) => {
        for {
          paireds <- TeamPairedRepo.getByTeamer(id, rno, teamer.id)
          teamers <- TeamerRepo.getByContest(contest.id)
        } yield Ok(html.teamContest.modal.teamerScoreDetail(rno, teamer, teamers, paireds))
      }
    }
  }

  def playerScoreDetail(id: String, rno: Int, uid: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      playerOption <- TeamPlayerRepo.find(id, uid)
      teamerOption <- playerOption.??(p => TeamerRepo.byId(p.teamerId))
      contestOption <- playerOption.??(p => api.byId(p.contestId))
    } yield (contestOption |@| playerOption |@| teamerOption).tupled) {
      case (contest, player, teamer) => {
        for {
          markMap <- Env.relation.markApi.getMarks(me.id)
          boards <- TeamBoardRepo.getByUser(id, rno, uid)
          pwus <- api.playersWithUsers(contest, markMap, me)
        } yield Ok(html.teamContest.modal.playerScoreDetail(rno, player, teamer, pwus, boards))
      }
    }
  }

  def reorderTeamer(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        TeamRoundRepo.byId(lila.contest.Round.makeId(contest.id, contest.currentRound)).map(r => r.isEmpty || r.??(_.isCreated)) flatMap { roundAccept =>
          if (contest.status == ContestModel.Status.Published || (contest.status == ContestModel.Status.EnterStopped && roundAccept)) {
            implicit val req = ctx.body
            Form(single("teamerIds" -> nonEmptyText)).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              teamerIds => api.reorderTeamer(teamerIds.split(",").toList) inject jsonOkResult
            )
          } else ForbiddenJsonStatus2
        }
      }
    }
  }

  def removeTeamer(id: String, teamerId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      teamerOption <- TeamerRepo.byId(teamerId)
      contestOption <- teamerOption.??(teamer => api.byId(teamer.contestId))
    } yield (contestOption |@| teamerOption).tupled) {
      case (contest, teamer) => {
        Owner(contest) {
          if (!contest.teamerRemoveable) ForbiddenStatus(contest)
          else api.removeTeamer(contest, teamer) inject RedirectToShow(contest.id, "enter".some)
        }
      }
    }
  }

  def forbiddenCreateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
          TeamerRepo.getJoinedByContest(contest.id) map { teamers =>
            Ok(html.teamContest.modal.teamerForbidden(contest, teamers, none))
          }
        } else ForbiddenJsonStatus(contest)
      }
    }
  }

  def forbiddenUpdateForm(id: String, fid: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      forbiddenOption <- ForbiddenRepo.byId(fid)
    } yield (contestOption |@| forbiddenOption).tupled) {
      case (contest, forbidden) => {
        Owner(contest) {
          if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
            TeamerRepo.getJoinedByContest(contest.id) map { teamers =>
              Ok(html.teamContest.modal.teamerForbidden(contest, teamers, forbidden.some))
            }
          } else ForbiddenJsonStatus(contest)
        }
      }
    }
  }

  def forbiddenApply(id: String, fidOption: Option[String]) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
          implicit val req = ctx.body
          forms.forbidden.bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => fidOption match {
              case None => ForbiddenRepo.upsert(data.toForbidden(id, none)) inject RedirectToShow(contest.id, "forbidden".some)
              case Some(fid) => ForbiddenRepo.byId(fid) flatMap { f =>
                ForbiddenRepo.upsert(data.toForbidden(id, f)) inject RedirectToShow(contest.id, "forbidden".some)
              }
            }
          )
        } else ForbiddenStatus(contest)
      }
    }
  }

  def removeForbidden(id: String, fid: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      forbiddenOption <- ForbiddenRepo.byId(fid)
    } yield (contestOption |@| forbiddenOption).tupled) {
      case (contest, forbidden) => {
        Owner(contest) {
          if (contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
            ForbiddenRepo.remove(forbidden) inject RedirectToShow(contest.id, "forbidden".some)
          } else ForbiddenStatus(contest)
        }
      }
    }
  }

  def uploadPicture = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val picture = ctx.body.body.file("logo")
      picture match {
        case Some(pic) => api.uploadPicture(Random nextString 16, pic) map { image =>
          Ok(Json.obj("ok" -> true, "path" -> image.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  private def RedirectToShow(id: String, hash: Option[String]) =
    Redirect(s"${routes.TeamContest.show(id)}${hash.?? { h => s"#$h" }}")

  private def federationIds(me: lila.user.User)(implicit ctx: Context) = {
    me.teamId.?? { teamId =>
      TeamFederationMemberRepo.findByTeam(teamId) map { members =>
        members.map(_.federationId)
      }
    }
  }

  private def federations(me: lila.user.User)(implicit ctx: Context): Fu[List[TeamFederation]] = {
    me.teamId.?? { teamId =>
      TeamFederationMemberRepo.findByTeam(teamId) flatMap { members =>
        TeamFederationRepo.byIds(members.map(_.federationId))
      }
    }
  }

  private def ForbiddenResult(message: String)(implicit ctx: Context) =
    Forbidden(views.html.site.message.authFailed(message)).fuccess

  private def ForbiddenStatus(contest: ContestModel)(implicit ctx: Context) =
    ForbiddenResult(s"操作被拒绝。比赛当前状态（${contest.status.name}）无法进行此操作！您可以点击“返回”继续其它操作。")

  private def ForbiddenStatus2(implicit ctx: Context) =
    ForbiddenResult(s"操作被拒绝。当前比赛及轮次状态无法进行此操作！您可以点击“返回”继续其它操作。")

  private def Status(contest: ContestModel, status: ContestModel.Status)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (contest.status == status) f
    else ForbiddenStatus(contest)

  private def Owner(contest: ContestModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => contest.isCreator(me) || isGranted(_.ManageContest))) f
    else ForbiddenResult(s"操作权限不足。仅比赛管理员可进行此操作！您可以点击“返回”继续其它操作。")

  private[controllers] def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(_.hasTeam)) f
    else ForbiddenResult(s"操作权限不足。您可以点击“返回”继续其它操作。")
  }

  private def ForbiddenJsonResult(message: String)(implicit ctx: Context) =
    fuccess(Forbidden(jsonError(message)) as JSON)

  private def StatusJson(contest: ContestModel, status: ContestModel.Status)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (contest.status == status) f
    else ForbiddenJsonStatus(contest)
  }

  private def ForbiddenJsonStatus(contest: ContestModel)(implicit ctx: Context): Fu[Result] = {
    ForbiddenJsonResult(s"操作被拒绝。比赛当前状态（${contest.status.name}）无法进行此操作！")
  }

  private def ForbiddenJsonStatus2(implicit ctx: Context): Fu[Result] = {
    ForbiddenJsonResult(s"操作被拒绝。当前比赛及轮次状态无法进行此操作！")
  }

  private def OwnerJson(contest: ContestModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => contest.isCreator(me) || isGranted(_.ManageContest))) f
    else ForbiddenJsonResult("操作权限不足。仅比赛管理员可进行此操作！")
  }

  private def PermissJson(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (isGranted(Permission.Coach) || ctx.me.??(_.hasTeam)) f
    else ForbiddenJsonResult("操作权限不足。")
  }

  private val CreateLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
    credits = 20,
    duration = 24 hour,
    name = "contest per user",
    key = "contest.user"
  )

  private val rateLimited = ornicar.scalalib.Zero.instance[Fu[Result]] {
    fuccess(Redirect(routes.TeamContest.home))
  }

}
