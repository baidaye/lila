package controllers.rt_contest

import lila.app._
import lila.hub.lightClazz._
import lila.hub.lightTeam._
import lila.memo.UploadRateLimit
import ornicar.scalalib.Random
import play.api.mvc.{ BodyParsers, Result }
import play.api.data.Forms._
import play.api.data.Form
import scala.concurrent.duration._
import play.api.libs.json.Json
import lila.api.Context
import lila.common.Form.numberIn
import lila.contest.DataForm.booleanChoices
import lila.user.UserRepo
import lila.security.Permission
import lila.team.{ TeamRatingRepo, TeamRepo }
import org.joda.time.DateTime
import lila.contest.{ BoardRepo, ForbiddenRepo, Invite, InviteRepo, ManualPairingSource, PlayerRepo, Request, RequestRepo, RoundRepo, ScoreSheetRepo, Contest => ContestModel }
import lila.game.GameRepo
import views._

object Contest extends controllers.LilaController {

  private def env = Env.contest
  private def api = env.contestApi
  private def roundApi = env.roundApi
  private def forms = env.forms
  private def jsonView = env.jsonView

  def testPairing(id: String, random: Boolean = false) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      roundApi.roundPairingTest(contest, random).map(_ =>
        Ok("ok"))
    }
  }

  def testScoreSheet(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      roundApi.testScoreSheet(contest).map(_ =>
        Ok("ok"))
    }
  }

  def manualFinishGame(gameId: String) = Secure(_.SuperAdmin) { implicit ctx => me =>
    OptionResult(GameRepo.game(gameId)) { game =>
      api.finishGame(game)
      Ok("ok")
    }
  }

  def home = Auth { implicit ctx => me =>
    Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
      env.pager.all(1, none, me, clazzIds, "") flatMap { pag =>
        Ok(html.contest.list.all(pag, none, "")).fuccess
      }
    }
  }

  def allPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
      env.pager.all(page, status, me, clazzIds, text) flatMap { pag =>
        Ok(html.contest.list.all(pag, status, text)).fuccess
      }
    }
  }

  def belongPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    env.pager.belong(page, status, me, text) flatMap { pag =>
      Ok(html.contest.list.belong(pag, status, text)).fuccess
    }
  }

  def ownerPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    env.pager.owner(page, status, me, text) flatMap { pag =>
      Ok(html.contest.list.owner(pag, status, text)).fuccess
    }
  }

  def finishPage(s: Option[Int], text: String, page: Int) = Auth { implicit ctx => me =>
    val status = s.map(ContestModel.Status(_))
    Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
      env.pager.finish(page, status, me, clazzIds, text) flatMap { pag =>
        Ok(html.contest.list.finish(pag, status, text)).fuccess
      }
    }
  }

  def ruleInfo(id: String) = Auth { implicit ctx => me =>
    Permiss {
      val r = ContestModel.Rule(id)
      Ok(jsonView.rule(r)).fuccess
    }
  }

  def createForm(rule: Option[String]) = Auth { implicit ctx => me =>
    Permiss {
      NoLameOrBot {
        val rl = rule.map(r => if (r.isEmpty) ContestModel.Rule.Swiss else ContestModel.Rule.apply(r)) | ContestModel.Rule.Swiss
        for {
          teams <- teamList(me)
          clazzs <- clazzList(me)
        } yield Ok(html.contest.form.create(forms.contestDefault(me, rl), rl, teams, clazzs))
      }
    }
  }

  /*  def startsPosition(fen: Option[String]) = Auth { implicit ctx => me =>
    Ok(html.contest.form.startingPositionModal(fen)).fuccess
  }*/

  def create() = AuthBody { implicit ctx => me =>
    Permiss {
      implicit val req = ctx.body
      for {
        teams <- teamList(me)
        clazzs <- clazzList(me)
        res <- {
          val rl =
            (req.body match {
              case body: play.api.mvc.AnyContent if body.asFormUrlEncoded.isDefined => body.asFormUrlEncoded.get
              case _ => Map.empty[String, Seq[String]]
            }).get("basics.rule").map {
              _.headOption.map { ContestModel.Rule.apply } | ContestModel.Rule.Swiss
            } | ContestModel.Rule.Swiss

          forms.contest(me, None, rl).bindFromRequest.fold(
            err => BadRequest(html.contest.form.create(err, rl, teams, clazzs)).fuccess,
            data => CreateLimitPerUser(me.id, cost = 1) {
              val contest = data.toContest(me, teams.map(t => t.id -> t.name), clazzs.map(c => c._1.id -> c._1.name))
              api.create(contest, data.roundList(contest.id)) map { c =>
                Redirect(routes.Contest.show(c.id))
              }
            }(rateLimited)
          )
        }
      } yield res
    }
  }

  def viewForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        for {
          teams <- teamList(me)
          clazzs <- clazzList(me)
          rounds <- RoundRepo.getByContest(id)
        } yield Ok(html.contest.form.view(contest, forms.contestOf(me, contest, rounds), teams, clazzs))
      }
    }
  }

  def updateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          for {
            teams <- teamList(me)
            clazzs <- clazzList(me)
            rounds <- RoundRepo.getByContest(id)
          } yield Ok(html.contest.form.update(contest, forms.contestOf(me, contest, rounds), teams, clazzs))
        }
      }
    }
  }

  def update(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          for {
            teams <- teamList(me)
            clazzs <- clazzList(me)
            rounds <- RoundRepo.getByContest(id)
            res <- {
              implicit val req = ctx.body
              val rl =
                (req.body match {
                  case body: play.api.mvc.AnyContent if body.asFormUrlEncoded.isDefined => body.asFormUrlEncoded.get
                  case _ => Map.empty[String, Seq[String]]
                }).get("basics.rule").map {
                  _.headOption.map { ContestModel.Rule.apply } | ContestModel.Rule.Swiss
                } | ContestModel.Rule.Swiss

              forms.contest(me, id.some, rl).bindFromRequest.fold(
                err => BadRequest(html.contest.form.update(contest, err, teams, clazzs)).fuccess,
                data => {
                  val newContest = data.toContest(me, teams.map(t => t.id -> t.name), clazzs.map(c => c._1.id -> c._1.name))
                  val rounds = data.roundList(contest.id)
                  api.update(contest, newContest, rounds) inject Redirect(routes.Contest.show(id))
                }
              )
            }
          } yield res
        }
      }
    }
  }

  def show(id: String) = Auth { implicit ctx => me =>
    OptionFuOk(api.byId(id)) { contest =>
      for {
        rounds <- RoundRepo.getByContest(id)
        players <- api.playersWithUsers(me, contest)
        markMap <- api.userMarks(me.id)
        boards <- BoardRepo.getByContest(id)
        requests <- contest.isCreator(me) ?? api.requestsWithUsers(contest, markMap)
        invites <- api.inviteWithUsers(contest, markMap)
        myRequest <- RequestRepo.find(id, me.id)
        myInvite <- InviteRepo.find(id, me.id)
        forbiddens <- ForbiddenRepo.getByContest(id)
        teamRating <- contest.teamRated.?? { TeamRatingRepo.findByContest(id) }
        scoreSheets <- contest.isOverStarted.?? {
          val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round $id-${contest.currentRound}"
          val roundNo = if (round.isPublishResult) round.no else round.no - 1
          ScoreSheetRepo.getByRound(id, Math.max(roundNo, 1))
        }
      } yield html.contest.show(contest, rounds, players, boards, requests, invites, forbiddens, teamRating, scoreSheets, myRequest, myInvite)
    }
  }

  def scoreDetail(id: String, rno: Int, uid: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      playerOption <- PlayerRepo.find(id, uid)
      contestOption <- playerOption.??(p => api.byId(p.contestId))
    } yield (contestOption |@| playerOption).tupled) {
      case (contest, player) => {
        BoardRepo.getByUser(id, rno, uid).flatMap { boards =>
          api.playersWithUsers(me, contest).map { players =>
            Ok(html.contest.modal.scoreDetail(rno, player, players, boards))
          }
        }
      }
    }
  }

  def remove(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          api.remove(contest) inject Redirect(routes.Contest.home)
        }
      }
    }
  }

  def publish(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Created) {
          if (contest.shouldEnterStop) ForbiddenResult("操作被拒绝。当前时间已经超过报名截止时间，您可以返回“编辑”页面，重新设置比赛“开始时间”后再进行操作。")
          else api.publish(contest) inject Redirect(routes.Contest.show(id))
        }
      }
    }
  }

  def cancel(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        api.cancel(contest) inject Redirect(routes.Contest.show(id))
      }
    }
  }

  def enterStop(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        Status(contest, ContestModel.Status.Published) {
          fuccess {
            (!contest.autoPairing).?? {
              api.setEnterStop(id)
            }
          } inject Redirect(routes.Contest.show(id))
        }
      }
    }
  }

  def clone(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        for {
          teams <- teamList(me)
          clazzs <- clazzList(me)
          rounds <- RoundRepo.getByContest(id)
        } yield Ok(html.contest.form.clone(contest, forms.contestOf(me, contest, rounds), teams, clazzs))
      }
    }
  }

  def joinForm(id: String) = AuthBody { implicit ctx => implicit me =>
    NoLameOrBot {
      NoPlayban {
        OptionFuResult(api.byId(id)) { contest =>
          Status(contest, ContestModel.Status.Published) {
            forms.anyCaptcha.map {
              html.contest.join(contest, forms.joinForm, _)
            }
          }
        }
      }
    }
  }

  def join(id: String) = AuthBody { implicit ctx => implicit me =>
    NoLameOrBot {
      NoPlayban {
        OptionFuResult(api.byId(id)) { contest =>
          Status(contest, ContestModel.Status.Published) {
            implicit val req = ctx.body
            forms.joinForm.bindFromRequest.fold(
              err => forms.anyCaptcha map { captcha =>
                BadRequest(html.contest.join(contest, err, captcha))
              },
              setup => {
                forms.anyCaptcha flatMap { captcha =>
                  PlayerRepo.find(id, me.id) flatMap {
                    case None => {
                      RequestRepo.find(id, me.id) flatMap {
                        case Some(r) => {
                          r.status match {
                            case Request.RequestStatus.Invited => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "邀请已经发出，请勿重新加入".some)).fuccess
                            case Request.RequestStatus.Joined => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已经加入比赛".some)).fuccess
                            case Request.RequestStatus.Refused => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已经被拒绝加入比赛".some)).fuccess
                          }
                        }
                        case None => {
                          InviteRepo.find(id, me.id) flatMap {
                            case Some(iv) => {
                              iv.status match {
                                case Invite.InviteStatus.Invited => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已被邀请加入比赛，请通过申请".some)).fuccess
                                case Invite.InviteStatus.Joined => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已经加入比赛".some)).fuccess
                                case Invite.InviteStatus.Refused => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已拒绝加入比赛".some)).fuccess
                              }
                            }
                            case None => {
                              if (contest.isPlayerFull) {
                                BadRequest(html.contest.join(contest, forms.joinForm, captcha, "参赛名额已满".some)).fuccess
                              } else {
                                api.verdicts(contest, me, teamIdList, clazzIdList) flatMap { v =>
                                  if (v.accepted) {
                                    api.joinRequest(contest, setup.toRequest(contest, me), me) inject Redirect(routes.Contest.show(contest.id))
                                  } else {
                                    BadRequest(html.contest.join(contest, forms.joinForm, captcha, "请核对个人资料是否满足参赛条件".some)).fuccess
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                    case Some(_) => BadRequest(html.contest.join(contest, forms.joinForm, captcha, "您已经加入比赛".some)).fuccess
                  }
                }
              }
            )
          }
        }
      }
    }
  }

  def joinProcess(requestId: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      requestOption <- RequestRepo.byId(requestId)
      userOption <- requestOption.??(req => UserRepo.byId(req.userId))
      contestOption <- requestOption.??(req => api.byId(req.contestId))
    } yield (contestOption |@| requestOption |@| userOption).tupled) {
      case (contest, request, user) => {
        Owner(contest) {
          if (!contest.joinProcessable) ForbiddenStatus(contest)
          else {
            implicit val req = ctx.body
            Form(single("process" -> nonEmptyText)).bindFromRequest.fold(
              _ => Redirect(routes.Contest.show(contest.id)).fuccess,
              decision => {
                val accept = decision === "accept"
                if (accept && contest.isPlayerFull) {
                  ForbiddenResult(s"参赛名额已满（${contest.maxPlayers}）")
                } else {
                  api.processRequest(contest, request, accept, user) inject Redirect(routes.Contest.show(contest.id))
                }
              }
            )
          }
        }
      }
    }
  }

  def quit(id: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- contestOption.??(c => RoundRepo.byId(lila.contest.Round.makeId(c.id, c.currentRound)))
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        if (!contest.quitable) ForbiddenStatus(contest)
        else {
          api.quit(contest, round, me.id) inject Redirect(routes.Contest.show(id))
        }
      }
    }
  }

  def removePlayer(playerId: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      playerOption <- PlayerRepo.byId(playerId)
      contestOption <- playerOption.??(p => api.byId(p.contestId))
      roundOption <- contestOption.??(c => RoundRepo.byId(lila.contest.Round.makeId(c.id, c.currentRound)))
    } yield (contestOption |@| roundOption |@| playerOption).tupled) {
      case (contest, round, player) => {
        Owner(contest) {
          if (!contest.playerRemoveable) ForbiddenStatus(contest)
          else api.removePlayer(contest, playerId) inject Redirect(s"${routes.Contest.show(contest.id)}#enter")
        }
      }
    }
  }

  def toggleKickPlayer(playerId: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      playerOption <- PlayerRepo.byId(playerId)
      contestOption <- playerOption.??(p => api.byId(p.contestId))
      roundOption <- contestOption.??(c => RoundRepo.byId(lila.contest.Round.makeId(c.id, c.currentRound)))
    } yield (contestOption |@| roundOption |@| playerOption).tupled) {
      case (contest, round, player) => {
        Owner(contest) {
          if (!contest.playerKickable) ForbiddenStatus(contest)
          else api.toggleKickPlayer(contest, round, player) inject Redirect(s"${routes.Contest.show(contest.id)}#enter")
        }
      }
    }
  }

  def inviteForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!contest.inviteable) ForbiddenJsonStatus(contest)
        else Ok(html.contest.modal.invite(contest)).fuccess
      }
    }
  }

  def invite(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!contest.inviteable) ForbiddenJsonStatus(contest)
        else {
          implicit def req = ctx.body
          Form(single(
            "username" -> lila.user.DataForm.historicalUsernameField
          )).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            username => UserRepo.named(username) flatMap {
              case None => BadRequest(jsonError("棋手不存在")).fuccess
              case Some(user) => {
                PlayerRepo.find(id, user.id) flatMap {
                  case None => {
                    InviteRepo.find(id, user.id) flatMap {
                      case Some(iv) => {
                        iv.status match {
                          case Invite.InviteStatus.Invited => BadRequest(jsonError("邀请已经发出，请勿重新邀请")).fuccess
                          case Invite.InviteStatus.Joined => BadRequest(jsonError("棋手已经加入比赛")).fuccess
                          case Invite.InviteStatus.Refused => BadRequest(jsonError("棋手已经被拒绝加入比赛")).fuccess
                        }
                      }
                      case None => {
                        RequestRepo.find(id, user.id) flatMap {
                          case Some(r) => {
                            r.status match {
                              case Request.RequestStatus.Invited => BadRequest(jsonError("棋手已经请求加入比赛，请通过申请")).fuccess
                              case Request.RequestStatus.Joined => BadRequest(jsonError("棋手已经加入比赛")).fuccess
                              case Request.RequestStatus.Refused => doInvite(contest, user)
                            }
                          }
                          case None => doInvite(contest, user)
                        }
                      }
                    }
                  }
                  case Some(_) => BadRequest(jsonError("棋手已经加入比赛")).fuccess
                }
              }
            }
          )
        }
      }
    }
  }

  private def doInvite(contest: ContestModel, user: lila.user.User) = {
    if (contest.isPlayerFull) {
      BadRequest(jsonError("参赛名额已满")).fuccess
    } else {
      //      doValidTyp(contest, user) flatMap {
      //        case true => {
      api.invite(
        contest,
        Invite.make(
          contest.id,
          contest.name,
          user.id
        )
      ) inject jsonOkResult
      //        }
      //        case false => BadRequest(jsonError("棋手参赛资格不足")).fuccess
      //      }
    }
  }

  private def doValidTyp(contest: ContestModel, user: lila.user.User): Fu[Boolean] = {
    contest.typ match {
      case ContestModel.Type.Public => teamIdList(user) map (_.contains(contest.organizer))
      case ContestModel.Type.TeamInner => teamIdList(user) map (_.contains(contest.organizer))
      case ContestModel.Type.ClazzInner => clazzIdList(user) map (_.contains(contest.organizer))
    }
  }

  def inviteProcess(inviteId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      inviteOption <- InviteRepo.byId(inviteId)
      userOption <- inviteOption.??(inv => UserRepo.byId(inv.userId))
      contestOption <- inviteOption.??(inv => api.byId(inv.contestId))
    } yield (contestOption |@| inviteOption |@| userOption).tupled) {
      case (contest, invite, user) => {
        if (!contest.inviteProcessable) ForbiddenStatus(contest)
        else {
          if (contest.isPlayerFull) {
            ForbiddenResult("参赛名额已满")
          } else api.processInvite(contest, invite, true, user) inject Redirect(routes.Contest.show(contest.id))
        }
      }
    }
  }

  def inviteRemove(inviteId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      inviteOption <- InviteRepo.byId(inviteId)
      contestOption <- inviteOption.??(inv => api.byId(inv.contestId))
    } yield (contestOption |@| inviteOption).tupled) {
      case (contest, invite) => {
        if (!contest.inviteRemoveable || invite.status == Invite.InviteStatus.Joined) ForbiddenStatus(contest)
        else {
          InviteRepo.remove(inviteId) inject Redirect(routes.Contest.show(contest.id))
        }
      }
    }
  }

  def playerChooseForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (!contest.chooseable) ForbiddenJsonStatus(contest)
        else {
          for {
            clazzs <- api.teamClazzs(contest)
            team <- api.team(contest)
            tags <- api.teamTags(contest)
            campuses <- api.teamCampus(contest)
            allPlayers <- api.allPlayersWithUsers(me, contest)
            players <- api.playersWithUsers(me, contest)
            requests <- RequestRepo.getByContest(contest.id)
            invites <- InviteRepo.getByContest(contest.id)
          } yield {
            val playerUserIds = players.map(_.userId)
            val all = allPlayers.filterNot { p =>
              playerUserIds.contains(p.userId) ||
                invites.exists(i => i.userId == p.userId && (i.status == Invite.InviteStatus.Invited || i.status == Invite.InviteStatus.Joined)) ||
                requests.exists(r => r.userId == p.userId && (r.status == Request.RequestStatus.Invited || r.status == Request.RequestStatus.Joined))
            }
            val pls = if (contest.isEnterStopped || contest.isStarted) Nil else players
            Ok(html.contest.modal.playerChoose(Env.team.forms.member.memberSearch, contest, clazzs, team, tags, campuses, all, pls))
          }
        }
      }
    }
  }

  def playerChoose(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (!contest.chooseable) ForbiddenJsonStatus(contest)
        else {
          implicit val req = ctx.body
          Form(single("players" -> text)).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            players => {
              PlayerRepo.getByContest(contest.id) flatMap { oldMembers =>
                val userIds = split(players)
                val allUserIds = (oldMembers.map(_.userId) ++ userIds).distinct
                if (allUserIds.size > contest.maxPlayers) {
                  ForbiddenJsonResult(s"选择的棋手数量超过最大限制（${contest.maxPlayers}）")
                } else api.setPlayers(contest, oldMembers, split(players)) inject jsonOkResult
              }
            }
          )
        }
      }
    }
  }

  private def split(str: String) = if (str.isEmpty) Nil else str.split(",").toList

  def forbiddenCreateForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
          api.playersWithUsers(me, contest) map { players =>
            Ok(html.contest.modal.playerForbidden(contest, players, none))
          }
        } else ForbiddenJsonStatus(contest)
      }
    }
  }

  def forbiddenUpdateForm(id: String, fid: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      forbiddenOption <- ForbiddenRepo.byId(fid)
    } yield (contestOption |@| forbiddenOption).tupled) {
      case (contest, forbidden) => {
        Owner(contest) {
          if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
            api.playersWithUsers(me, contest) map { players =>
              Ok(html.contest.modal.playerForbidden(contest, players, forbidden.some))
            }
          } else ForbiddenJsonStatus(contest)
        }
      }
    }
  }

  def forbiddenApply(id: String, fidOption: Option[String]) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
          implicit val req = ctx.body
          forms.forbidden.bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => fidOption match {
              case None => ForbiddenRepo.upsert(data.toForbidden(id, none)) inject Redirect(routes.Contest.show(contest.id) + "#forbidden")
              case Some(fid) => ForbiddenRepo.byId(fid) flatMap { f =>
                ForbiddenRepo.upsert(data.toForbidden(id, f)) inject Redirect(routes.Contest.show(contest.id) + "#forbidden")
              }
            }
          )
        } else ForbiddenStatus(contest)
      }
    }
  }

  def removeForbidden(id: String, fid: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      forbiddenOption <- ForbiddenRepo.byId(fid)
    } yield (contestOption |@| forbiddenOption).tupled) {
      case (contest, forbidden) => {
        Owner(contest) {
          if (contest.status == ContestModel.Status.Published || contest.status == ContestModel.Status.EnterStopped || contest.status == ContestModel.Status.Started) {
            ForbiddenRepo.remove(forbidden) inject Redirect(routes.Contest.show(contest.id))
          } else ForbiddenStatus(contest)
        }
      }
    }
  }

  def reorderPlayer(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        RoundRepo.byId(lila.contest.Round.makeId(contest.id, contest.currentRound)).map(r => r.isEmpty || r.??(_.isCreated)) flatMap { roundAccept =>
          if (contest.status == ContestModel.Status.Published || (contest.status == ContestModel.Status.EnterStopped && roundAccept)) {
            implicit val req = ctx.body
            Form(single(
              "playerIds" -> nonEmptyText
            )).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              playerIds => api.reorderPlayerByPlayerIds(playerIds.split(",").toList) inject jsonOkResult
            )
          } else ForbiddenJsonStatus2
        }
      }
    }
  }

  def autoPairing(id: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!contest.isPublished && !contest.isEnterStopped && !contest.isStarted) ForbiddenJsonStatus(contest)
        else {
          implicit val req = ctx.body
          Form(single("autoPairing" -> numberIn(booleanChoices))).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            auto => api.setAutoPairing(contest, auto == 1) inject jsonOkResult
          )
        }
      }
    }
  }

  def setRoundRobin(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!(contest.isEnterStopped || contest.isStarted)) ForbiddenJsonStatus(contest)
        else {
          implicit val req = ctx.body
          forms.roundRobinForm(contest).bindFromRequest.fold(
            err => BadRequest(errorsAsJson(err)).fuccess,
            data => {
              api.setRoundRobin(contest, data.list) inject {
                Ok(Json.obj(
                  "ok" -> true,
                  "nbPlayers" -> contest.nbPlayers
                )) as JSON
              }
            }
          )
        }
      }
    }
  }

  def pairingRoundRobin(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      OwnerJson(contest) {
        if (!(contest.isEnterStopped || contest.isStarted)) ForbiddenJsonStatus(contest)
        else api.pairingRoundRobin(contest) inject jsonOkResult
      }
    }
  }

  def pairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          val random = getBool("random")
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || round.isPairing)) ForbiddenJsonStatus2
          else {
            roundApi.pairing(contest, round, api.publishScoreAndFinish, random).map { succ =>
              if (succ) jsonOkResult
              else BadRequest(jsonError("当前匹配规则下已无法再进行匹配，您可以在成绩册中发布成绩结束比赛"))
            }
          }
        }
      }
    }
  }

  def manualAbsentForm(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || (contest.isRoundRobin && round.isPairing))) ForbiddenJsonStatus2
          else api.playersWithUsers(me, contest) map { players =>
            Ok(html.contest.modal.manualAbsent(contest, round, players))
          }
        }
      }
    }
  }

  def manualAbsent(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || (contest.isRoundRobin && round.isPairing))) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(tuple(
              "joins" -> list(nonEmptyText),
              "absents" -> list(nonEmptyText)
            )).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              data => data match {
                case (joins, absents) => {
                  roundApi.manualAbsent(contest, round, joins, absents) inject jsonOkResult
                }
              }
            )
          }
        }
      }
    }
  }

  def manualPairingBeyForm(id: String, roundId: String, playerId: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      playerOption <- PlayerRepo.byId(playerId)
      roundOption <- RoundRepo.byId(roundId)
    } yield (contestOption |@| playerOption |@| roundOption).tupled) {
      case (contest, player, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else BoardRepo.getByRound(round.id) flatMap { boards =>
            api.playersWithUsers(me, contest).map { players =>
              Ok(html.contest.modal.manualPairing(contest, round, boards, players,
                ManualPairingSource(none, none, player.some, true)))
            }
          }
        }
      }
    }
  }

  def manualPairingNotBeyForm(id: String, boardId: String, color: Boolean) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(boardId)
      contestOption <- api.byId(id)
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else BoardRepo.getByRound(round.id) flatMap { boards =>
            api.playersWithUsers(me, contest).map { players =>
              Ok(html.contest.modal.manualPairing(contest, round, boards, players,
                ManualPairingSource(board.some, chess.Color(color).some, none, false)))
            }
          }
        }
      }
    }
  }

  def manualPairing(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            forms.manualPairingForm.bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              data => roundApi.manualPairing(contest, round, data) inject jsonOkResult
            )
          }
        }
      }
    }
  }

  def manualResultForm(id: String, bid: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(bid)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isStarted || contest.isFinished) || !board.isFinished) ForbiddenJsonStatus2
          else Ok(html.contest.modal.manualResult(contest, round, board)).fuccess
        }
      }
    }
  }

  def manualResult(id: String, bid: String) = AuthBody { implicit ctx => implicit me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(bid)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        Owner(contest) {
          if (contest.autoPairing || !contest.isStarted || !board.isFinished) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("result" -> nonEmptyText.verifying(lila.contest.Board.Result.keys.contains(_)))).bindFromRequest.fold(
              err => Redirect(routes.Contest.show(contest.id)).fuccess,
              result => roundApi.manualResult(contest, round, board, result) inject Redirect(routes.Contest.show(contest.id))
            )
          }
        }
      }
    }
  }

  def roundSwapForm(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else for {
            rounds <- RoundRepo.getByContest(id)
            boards <- BoardRepo.getByContest(id)
            players <- api.playersWithUsers(me, contest)
          } yield Ok(html.contest.modal.roundSwap(contest, round, rounds, boards, players))
        }
      }
    }
  }

  def roundSwap(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("roundId" -> nonEmptyText)).bindFromRequest.fold(
              err => BadRequest(errorsAsJson(err)).fuccess,
              roundId => roundApi.roundSwap(contest, round, roundId) inject jsonOkResult
            )
          }
        }
      }
    }
  }

  def publishPairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !round.isPairing) ForbiddenJsonStatus2
          else {
            if (round.actualStartsAt.getMillis < DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0).plusMinutes(lila.contest.Round.beforeStartMinutes).getMillis) {
              BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess
            } else roundApi.publish(contest, round) inject jsonOkResult
          }
        }
      }
    }
  }

  def cancelPublishPairing(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !contest.rule.flow.cancelPairingPublish || !(contest.isEnterStopped || contest.isStarted) || !round.isPublished) ForbiddenJsonStatus2
          else {
            BoardRepo.getByRound(round.id).flatMap { boards =>
              if (boards.forall(_.canCancel)) {
                roundApi.cancelPublish(contest, round, boards) inject jsonOkResult
              } else ForbiddenJsonResult("轮次中有已经开始或立即开始的对局，您不能做此操作")
            }
          }
        }
      }
    }
  }

  def publishResult(id: String, rno: Int) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        Owner(contest) {
          if (contest.autoPairing || !contest.isStarted || !round.isFinished) ForbiddenStatus2
          else {
            roundApi.publishResult(contest, round.id, round.no) inject Redirect(routes.Contest.show(contest.id))
          }
        }
      }
    }
  }

  def cancelScore(id: String, sid: String) = Auth { implicit ctx => implicit me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      scoreSheetOption <- ScoreSheetRepo.byId(sid)
    } yield (contestOption |@| scoreSheetOption).tupled) {
      case (contest, scoreSheet) => {
        Owner(contest) {
          if (!contest.isStarted || contest.autoPairing || !contest.allRoundFinished) ForbiddenStatus2
          else api.cancelScore(contest, scoreSheet) inject Redirect(routes.Contest.show(id))
        }
      }
    }
  }

  def setRoundStartsTime(id: String, rno: Int) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      contestOption <- api.byId(id)
      roundOption <- RoundRepo.find(id, rno)
    } yield (contestOption |@| roundOption).tupled) {
      case (contest, round) => {
        OwnerJson(contest) {
          if (contest.autoPairing || !(contest.isEnterStopped || contest.isStarted) || !(round.isCreated || round.isPairing)) ForbiddenJsonStatus2
          else {
            implicit val req = ctx.body
            Form(single("startsAt" -> lila.common.Form.futureDateTime)).bindFromRequest.fold(
              _ => BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess,
              st => {
                if (st.getMillis < DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0).plusMinutes(lila.contest.Round.beforeStartMinutes).getMillis) {
                  BadRequest(jsonError(s"轮次开始时间必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}）分钟")).fuccess
                } else if (st.isBefore(contest.startsAt)) {
                  BadRequest(jsonError(s"轮次开始时间必须大于等于比赛开始时间")).fuccess
                } else roundApi.setStartsTime(contest, round, st) inject jsonOkResult
              }
            )
          }
        }
      }
    }
  }

  def publishScoreAndFinish(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(api.byId(id)) { contest =>
      Owner(contest) {
        if (contest.autoPairing || !contest.isStarted || !contest.allRoundFinished) ForbiddenStatus2
        else {
          api.publishScoreAndFinish(contest) inject Redirect(routes.Contest.show(contest.id))
        }
      }
    }
  }

  def setBoardTimeForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(id)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.isCreated) ForbiddenJsonStatus2
          else Ok(html.contest.modal.setBoardTime(contest, round, board)).fuccess
        }
      }
    }
  }

  def setBoardTime(id: String) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(id)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        OwnerJson(contest) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !board.isCreated) ForbiddenJsonStatus2
          else {
            implicit def req = ctx.body
            import lila.common.Form.futureDateTime
            Form(single("startsAt" -> futureDateTime.verifying(
              s"日期必须大于当前时间（+${lila.contest.Round.beforeStartMinutes}分钟）",
              DateTime.now.plusMinutes(lila.contest.Round.beforeStartMinutes - 1).isBefore(_)
            ))).bindFromRequest.fold(
              err => Redirect(routes.Contest.show(contest.id)).fuccess,
              startsAt => roundApi.setBoardTime(contest, round, board, startsAt) inject Redirect(routes.Contest.show(contest.id))
            )
          }
        }
      }
    }
  }

  def uploadPicture = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val picture = ctx.body.body.file("logo")
      picture match {
        case Some(pic) => api.uploadPicture(Random nextString 16, pic) map { image =>
          Ok(Json.obj("ok" -> true, "path" -> image.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  def uploadFile = AuthBody(BodyParsers.parse.multipartFormData) { implicit ctx => implicit me =>
    UploadRateLimit.rateLimit(me.username, ctx.req) {
      val file = ctx.body.body.file("attachments")
      file match {
        case Some(f) => api.uploadFile(Random nextString 16, f) map { dbFile =>
          Ok(Json.obj("ok" -> true, "path" -> dbFile.path))
        } recover {
          case e: lila.base.LilaException => Ok(Json.obj("ok" -> false, "message" -> e.message))
        }
        case _ => fuccess(Ok(Json.obj("ok" -> true)))
      }
    }
  }

  def apiContest(text: String, page: Int) = Auth { implicit ctx => me =>
    Env.clazz.api.mineClazzIds(me.id) flatMap { clazzIds =>
      env.pager.api(page, me, clazzIds, text) map { pag =>
        Ok(env.jsonView.contestPage(pag)) as JSON
      }
    }
  }

  def apiRound(contestId: String) = Auth { implicit ctx => me =>
    RoundRepo.getByContest(contestId) map { rounds =>
      Ok(env.jsonView.rounds(rounds.filter(_.isPublishResult))) as JSON
    }
  }

  def apiBoard(contestId: String, roundId: String) = Auth { implicit ctx => me =>
    BoardRepo.getByRound(roundId) flatMap { boards =>
      Env.relation.markApi.getMarks(me.id) flatMap { marks =>
        env.jsonView.boards(boards.filter(_.isFinished), marks) map { js =>
          Ok(js) as JSON
        }
      }
    }
  }

  def setBoardSignForm(id: String) = Auth { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(id)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
      whitePlayerOption <- boardOption.?? { b => UserRepo.byId(b.whitePlayer.userId) }
      blackPlayerOption <- boardOption.?? { b => UserRepo.byId(b.blackPlayer.userId) }
    } yield (boardOption |@| contestOption |@| roundOption |@| whitePlayerOption |@| blackPlayerOption).tupled) {
      case (board, contest, round, whitePlayer, blackPlayer) => {
        OwnerJson(contest) {
          if (!(contest.isEnterStopped || contest.isStarted) || !(round.isPublished || round.isStarted) || !(board.isCreated || board.isStarted)) ForbiddenJsonStatus2
          else {
            Env.relation.markApi.getMarks(me.id) map { markMap =>
              Ok(html.contest.modal.setBoardSign(contest, round, board, whitePlayer, blackPlayer, markMap))
            }
          }
        }
      }
    }
  }

  def setBoardSign(id: String, userId: Option[String] = None) = AuthBody { implicit ctx => me =>
    OptionFuResult(for {
      boardOption <- BoardRepo.byId(id)
      contestOption <- boardOption.?? { b => api.byId(b.contestId) }
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
    } yield (boardOption |@| contestOption |@| roundOption).tupled) {
      case (board, contest, round) => {
        val isAgent = contest.isCreator(me) && userId.exists(board.contains)
        if (board.contains(me.id) || isAgent) {
          roundApi.setBoardSign(board, (userId | me.id), isAgent) inject jsonOkResult
        } else ForbiddenJsonResult("操作被拒绝。")
      }
    }
  }

  private def ForbiddenResult(message: String)(implicit ctx: Context) =
    Forbidden(views.html.site.message.authFailed(message)).fuccess

  private def ForbiddenStatus(contest: ContestModel)(implicit ctx: Context) =
    ForbiddenResult(s"操作被拒绝。比赛当前状态（${contest.status.name}）无法进行此操作！您可以点击“返回”继续其它操作。")

  private def ForbiddenStatus2(implicit ctx: Context) =
    ForbiddenResult(s"操作被拒绝。当前比赛及轮次状态无法进行此操作！您可以点击“返回”继续其它操作。")

  private def Status(contest: ContestModel, status: ContestModel.Status)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (contest.status == status) f
    else ForbiddenStatus(contest)

  private def Owner(contest: ContestModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] =
    if (ctx.me.??(me => contest.isCreator(me) || isGranted(_.ManageContest))) f
    else ForbiddenResult(s"操作权限不足。仅比赛管理员可进行此操作！您可以点击“返回”继续其它操作。")

  private[controllers] def Permiss(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (isGranted(Permission.Coach) || ctx.me.??(_.hasTeam)) f
    else ForbiddenResult(s"操作权限不足。您可以点击“返回”继续其它操作。")
  }

  private def ForbiddenJsonResult(message: String)(implicit ctx: Context) =
    fuccess(Forbidden(jsonError(message)) as JSON)

  private def StatusJson(contest: ContestModel, status: ContestModel.Status)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (contest.status == status) f
    else ForbiddenJsonStatus(contest)
  }

  private def ForbiddenJsonStatus(contest: ContestModel)(implicit ctx: Context): Fu[Result] = {
    ForbiddenJsonResult(s"操作被拒绝。比赛当前状态（${contest.status.name}）无法进行此操作！")
  }

  private def ForbiddenJsonStatus2(implicit ctx: Context): Fu[Result] = {
    ForbiddenJsonResult(s"操作被拒绝。当前比赛及轮次状态无法进行此操作！")
  }

  private def OwnerJson(contest: ContestModel)(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (ctx.me.??(me => contest.isCreator(me) || isGranted(_.ManageContest))) f
    else ForbiddenJsonResult("操作权限不足。仅比赛管理员可进行此操作！")
  }

  private def PermissJson(f: => Fu[Result])(implicit ctx: Context): Fu[Result] = {
    if (isGranted(Permission.Coach) || ctx.me.??(_.hasTeam)) f
    else ForbiddenJsonResult("操作权限不足。")
  }

  private val CreateLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
    credits = 20,
    duration = 24 hour,
    name = "contest per user",
    key = "contest.user"
  )

  private[controllers] val ClazzCreateLimitPerUser = new lila.memo.RateLimit[lila.user.User.ID](
    credits = 20,
    duration = 24 hour,
    name = "contest per user",
    key = "contest.user"
  )

  private val rateLimited = ornicar.scalalib.Zero.instance[Fu[Result]] {
    fuccess(Redirect(routes.Contest.home))
  }

  private[controllers] def teamIdList(me: lila.user.User): Fu[TeamIdList] =
    Env.team.api.mine(me).map(_.filter(_.team.enabled).map(_.team.id))

  private[controllers] def clazzIdList(me: lila.user.User): Fu[ClazzIdList] =
    Env.clazz.api.mine(me.id).map(_.map(_._id))

  private[controllers] def teamList(me: lila.user.User): Fu[List[lila.team.Team]] =
    Env.team.api.mine(me).map(_.filter(twm => twm.team.enabled && twm.member.isOwner).map(t => t.team))

  private[controllers] def clazzList(me: lila.user.User): Fu[List[(lila.clazz.Clazz, Boolean)]] =
    for {
      clazzs <- Env.clazz.api.mine(me.id).map(_.filter(c => c.isCoach(me.id)))
      teams <- TeamRepo.byCampusIds(clazzs.map(_.teamOrDefault))
    } yield {
      val ratedList = teams.map(t => t.id -> (t.ratingSettingOrDefault.open && t.ratingSettingOrDefault.coachSupport)).toMap
      clazzs.map { c =>
        c -> c.team.?? { campusId =>
          ratedList.get(lila.team.Campus.toTeamId(campusId)) | false
        }
      }
    }

}
