package lila.app
package mashup

import lila.api.Context
import lila.contest.Contest
import lila.game.{ Game, GameRepo, Pov }
import lila.playban.TempBan
import lila.puzzle.PuzzleRush
import lila.streamer.LiveStreams
import lila.study.Study
import lila.timeline.Entry
import lila.tournament.{ Winner }
import lila.tv.Tv
import lila.user.LightUserApi
import lila.user.User
import play.api.libs.json._
import lila.lichessApi.OAuth2

final class Preload(
    tv: Tv,
    leaderboard: Unit => Fu[List[User.LightPerf]],
    rushs: () => Fu[List[(PuzzleRush.Mode, List[(User.ID, Int)])]],
    contests: Option[User] => Fu[List[(Contest, User.ID)]],
    studys: () => Fu[List[Study]],
    tourneyWinners: Fu[List[Winner]],
    timelineEntries: String => Fu[Vector[Entry]],
    liveStreams: () => Fu[LiveStreams],
    dailyPuzzle: lila.puzzle.Daily.Try,
    countRounds: () => Int,
    lobbyApi: lila.api.LobbyApi,
    getPlayban: User.ID => Fu[Option[TempBan]],
    lightUserApi: LightUserApi,
    roundProxyPov: (Game.ID, User) => Fu[Option[Pov]],
    urgentGames: User => Fu[List[Pov]],
    getLastThemePuzzleId: User.ID => Fu[Int],
    getActiveLichessOAuth2: User.ID => Fu[Option[OAuth2]]
) {

  import Preload._

  private type Response = (JsObject, Option[TempBan], Option[Preload.CurrentGame], List[Pov], Option[OAuth2], Int)

  def apply()(implicit ctx: Context): Fu[Response] =
    lobbyApi(ctx) zip
      (ctx.userId ?? getPlayban) zip
      (ctx.userId ?? getActiveLichessOAuth2) zip
      (ctx.blind ?? ctx.me ?? urgentGames) flatMap {
        case (data, povs) ~ playban ~ lichessOAuth2 ~ blindGames => {
          (ctx.me ?? currentGameMyTurn(povs, lightUserApi.sync)).map { currentGame =>
            (data, playban, currentGame, blindGames, lichessOAuth2, countRounds())
          }
        }
      }

  private type HomeResponse = (JsObject, Option[Game], List[User.LightPerf], List[(PuzzleRush.Mode, List[(User.ID, Int)])], List[(Contest, User.ID)], List[Study], Option[lila.puzzle.DailyPuzzle], Int)

  def home()(implicit ctx: Context): Fu[HomeResponse] = {
    lobbyApi.homeData(ctx) zip
      tv.getBestGame zip
      leaderboard(()) zip
      rushs() zip
      contests(ctx.me) zip
      studys() zip
      ctx.userId.fold(fuccess(100000)) { getLastThemePuzzleId } zip
      (ctx.noBot ?? dailyPuzzle()) flatMap {
        case (data, povs) ~ feat ~ leaderboard ~ rushs ~ contests ~ studys ~ lastThemePuzzleId ~ puzzle => {
          lightUserApi.preloadMany {
            rushs.flatMap {
              case (_, list) => list.map(_._1)
            } ::: contests.map(_._2)
          } inject (data, feat, leaderboard, rushs, contests, studys, puzzle, lastThemePuzzleId)
        }
      }
  }

  def currentGameMyTurn(user: User): Fu[Option[CurrentGame]] =
    GameRepo.playingRealtimeNoAi(user, 10).flatMap {
      _.map { roundProxyPov(_, user) }.sequenceFu.map(_.flatten)
    } flatMap {
      currentGameMyTurn(_, lightUserApi.sync)(user)
    }

  private def currentGameMyTurn(povs: List[Pov], lightUser: lila.common.LightUser.GetterSync)(user: User): Fu[Option[CurrentGame]] =
    ~povs.collectFirst {
      case p1 if p1.game.nonAi && p1.game.hasClock && p1.isMyTurn =>
        roundProxyPov(p1.gameId, user) map (_ | p1) map { pov =>
          val opponent = lila.game.Namer.playerText(pov.opponent)(lightUser)
          CurrentGame(
            pov = pov,
            opponent = opponent,
            json = Json.obj(
              "id" -> pov.gameId,
              "color" -> pov.color.name,
              "opponent" -> opponent
            )
          ).some
        }
    }
}

object Preload {

  case class CurrentGame(pov: Pov, json: JsObject, opponent: String)

  case class CurrentLichessGame(id: String, opponent: String)
}
