#!/bin/sh -e

DOMAIN="lichess.org"
#RUN_DIR="/mnt/c/work/install/JetBrains/IntelliJ_IDEA_20180301/workspace/lila/data/ssl"
RUN_DIR="/home/haichess/lila/data/ssl"
CRT_NAME="$RUN_DIR/$DOMAIN.crt"

echo "开始下载证书"
mv $CRT_NAME $CRT_NAME-$(date +%Y-%m-%d_%H-%M)
openssl s_client -showcerts -servername $DOMAIN -connect $DOMAIN:443 > $CRT_NAME
echo "下载证书完成"

mv /home/haichess/lila/data/ssl/lichess.org.crt /home/haichess/lila/data/ssl/lichess.org.crt-$(date +%Y-%m-%d_%H-%M);openssl s_client -showcerts -servername lichess.org -connect lichess.org:443 > /home/haichess/lila/data/ssl/lichess.org.crt

mv /mnt/c/work/install/JetBrains/IntelliJ_IDEA_20180301/workspace/lila/data/ssl/lichess.org.crt /mnt/c/work/install/JetBrains/IntelliJ_IDEA_20180301/workspace/lila/data/ssl/lichess.org.crt-$(date +%Y-%m-%d_%H-%M);openssl s_client -showcerts -servername lichess.org -connect lichess.org:443 > /mnt/c/work/install/JetBrains/IntelliJ_IDEA_20180301/workspace/lila/data/ssl/lichess.org.crt


