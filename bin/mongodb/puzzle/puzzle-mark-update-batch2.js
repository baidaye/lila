db.adm_puzzle_mark_all_new.find({
  "mark.markStatus": "Marked",
  "mark.batch": 2
}).forEach(function(obj) {
  let mark = obj.mark;
  mark.validStatus = "Save";

  db.puzzle.update({"_id": mark.haichessId}, {
    $set: {
      "idHistory.source": "lichess-mark",
      "idHistory.batch": NumberInt(2),
      "mark": mark
    }
  })
})


db.adm_puzzle_mark_all_new.find({"mark.haichessId": {$exists: true}}).forEach(function(obj) {
  let mark = obj.mark;
  db.puzzle.update({"_id": mark.haichessId}, {
    $set: {
      "mark.lichessVote": NumberInt(obj.puzzle.vote)
    }
  })
})


db.puzzle.update({_id: {$lt: 1000000}}, {
  $set: {
    "theme": true
  }
}, {multi: true})











db.puzzle_removed.find({}).forEach(function(p) {
  let new_puzzle = p;

  new_puzzle._id = NumberInt(parseInt(p._id));
  new_puzzle.attempts = NumberInt(parseInt(p.attempts));
  new_puzzle.depth = NumberInt(parseInt(p.depth));

  if(p.idHistory.mark) {
    new_puzzle.idHistory.mark = NumberInt(parseInt(p.idHistory.mark));
  }
  if(p.idHistory.lichess) {
    new_puzzle.idHistory.lichess = NumberInt(parseInt(p.idHistory.lichess));
  }
  if(p.idHistory.version) {
    new_puzzle.idHistory.version = NumberInt(parseInt(p.idHistory.version));
  }
  if(p.idHistory.batch) {
    new_puzzle.idHistory.batch = NumberInt(parseInt(p.idHistory.batch));
  }
  if(p.mark.id) {
    new_puzzle.mark.id = NumberInt(parseInt(p.mark.id));
  }
  if(p.mark.rating) {
    new_puzzle.mark.rating = NumberInt(parseInt(p.mark.rating));
  }
  if(p.mark.lichessVote) {
    new_puzzle.mark.lichessVote = NumberInt(parseInt(p.mark.lichessVote));
  }

  if(p.perf.nb) {
    new_puzzle.perf.nb = NumberInt(parseInt(p.perf.nb));
  }
  if(p.time) {
    new_puzzle.time = NumberInt(parseInt(p.time));
  }
  new_puzzle.vote.up = NumberInt(parseInt(p.vote.up));
  new_puzzle.vote.down = NumberInt(parseInt(p.vote.down));
  new_puzzle.vote.nb = NumberInt(parseInt(p.vote.nb));
  new_puzzle.vote.ratio = NumberInt(parseInt(p.vote.ratio));
  if(p.wins) {
    new_puzzle.wins = NumberInt(parseInt(p.wins));
  }
  if(p.likes) {
    new_puzzle.likes = NumberInt(parseInt(p.likes));
  }

  //console.log(new_puzzle);
  db.puzzle_removed2.insert(new_puzzle);
});
