package lila.message

import lila.db.dsl._
import lila.message.MessageThread.ID
import lila.user.User
import reactivemongo.api.ReadPreference

object MessageThreadRepo {

  import BSONHandlers.ThreadHandler
  import BSONHandlers.LastMessageHandler

  private val coll = Env.current.threadColl

  def byId(id: MessageThread.ID): Fu[Option[MessageThread]] =
    coll.byId[MessageThread](id)

  def insert(thread: MessageThread): Funit =
    coll.insert(thread).void

  def update(id: MessageThread.ID, message: Message, send: MessageSecurity.Send, multi: Boolean): Funit =
    coll.update(
      $id(id),
      $set("lastMessage" -> message.asLast) ++ {
        $pull(
          // unset "deleted by receiver" unless the message is muted
          "del" $in (message.orig :: (!send.mute).option(message.dest).toList)
        )
      }
    ).void

  def delete(threadId: ID, userId: User.ID): Funit =
    coll.update(
      $id(threadId),
      $addToSet("del" -> userId)
    ).void

  def deleteByUser(userId: User.ID): Funit =
    coll.remove($doc("users" -> userId)).void

  def setRead(threadId: MessageThread.ID, dest: User.ID) =
    coll.updateField(
      $id(threadId) ++ $doc("lastMessage.user" $ne dest, "lastMessage.read" -> false),
      "lastMessage.read",
      true
    )

  def threadsOf(me: User): Fu[List[MessageThread]] =
    coll
      .find($doc("users" -> me.id, "del" $ne me.id))
      .sort($sort desc "lastMessage.date")
      .cursor[MessageThread]()
      .list(50)
      .map(prioritize)

  private def prioritize(threads: List[MessageThread]): List[MessageThread] =
    threads.find(_.isPriority) match {
      case None => threads
      case Some(found) => found :: threads.filterNot(_.isPriority)
    }

  def allThreadsOf(userId: User.ID): Fu[List[MessageThread]] =
    coll.find($doc("users" -> userId)).list[MessageThread]()

  def searchThreads(me: User, q: String): Fu[List[MessageThread]] =
    coll.find($doc("users" -> me.id, "del" $ne me.id))
      .sort($sort desc "lastMessage.date")
      //.hint($doc("users" -> 1, "lastMessage.date" -> -1))
      .cursor[MessageThread](ReadPreference.secondaryPreferred)
      .list(5)

  def exists(id: Message.ID): Fu[Boolean] = coll.exists($id(id))

  def existsDestNotDel(orig: User.ID, dest: User.ID): Fu[Boolean] =
    coll.exists(
      $id(MessageThread.id(orig, dest)) ++
        $doc("del" $ne dest)
    )

  def recentThreads(user: User, nb: Int): Fu[List[MessageThread]] =
    coll
      .find($doc("users" -> user.id))
      .sort($sort desc "lastMessage.date")
      .cursor[MessageThread](ReadPreference.secondaryPreferred)
      .list(nb)

}
