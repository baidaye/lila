package lila.message

import lila.user.User
import lila.common.LightUser

case class MessageThread(
    _id: MessageThread.ID,
    user1: User.ID,
    user2: User.ID,
    lastMessage: LastMessage,
    del: Option[List[User.ID]]
) {

  def id = _id

  def users = List(user1, user2)

  def other(userId: User.ID): User.ID = if (user1 == userId) user2 else user1
  def other(user: User): User.ID = other(user.id)
  def other(user: LightUser): User.ID = other(user.id)

  def delBy(userId: User.ID) = del.exists(_ contains userId)

  def isPriority =
    !lastMessage.read && {
      user1 == User.lichessId || user2 == User.lichessId
    }
}

object MessageThread {

  type ID = String

  case class WithMsgs(thread: MessageThread, msgs: List[Message])

  case class WithContact(thread: MessageThread, contact: LightUser)

  case class Unread(thread: MessageThread)

  val idSep = '/'

  def id(u1: User.ID, u2: User.ID): ID =
    sortUsers(u1, u2) match {
      case (user1, user2) => s"$user1$idSep$user2"
    }

  def make(u1: User.ID, u2: User.ID, message: Message, del: Option[List[User.ID]]): MessageThread =
    sortUsers(u1, u2) match {
      case (user1, user2) =>
        MessageThread(
          _id = id(user1, user2),
          user1 = user1,
          user2 = user2,
          lastMessage = message.asLast,
          del = del
        )
    }

  private def sortUsers(u1: User.ID, u2: User.ID): (User.ID, User.ID) =
    if (u1 < u2) (u1, u2) else (u2, u1)
}
