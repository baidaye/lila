package lila.message

import lila.common.LightUser
import lila.relation.Relations

case class MessageConvo(
    contact: LightUser,
    messages: List[Message],
    relations: Relations,
    postable: Boolean
)
