package lila.message

import akka.actor._
import com.typesafe.config.Config
import lila.chat.ChatPanic

final class Env(
    config: Config,
    db: lila.db.Env,
    shutup: ActorSelection,
    notifyApi: lila.notify.NotifyApi,
    blocks: (String, String) => Fu[Boolean],
    follows: (String, String) => Fu[Boolean],
    prefApi: lila.pref.PrefApi,
    spam: lila.security.Spam,
    system: ActorSystem,
    hub: lila.hub.Env,
    isOnline: lila.user.User.ID => Boolean,
    userCache: lila.user.Cached,
    relationApi: lila.relation.RelationApi,
    lightUserApi: lila.user.LightUserApi
) {

  private val CollectionThread = config getString "collection.thread"
  private val CollectionMessage = config getString "collection.message"
  private val SocketSriTtl = config duration "socket.sri.ttl"

  private[message] lazy val threadColl = db(CollectionThread)
  private[message] lazy val messageColl = db(CollectionMessage)

  lazy val jsonView = new JsonView(isOnline, lightUserApi)

  lazy val search = new MessageSearch(
    userCache = userCache,
    markActor = hub.relation,
    lightUserApi = lightUserApi,
    relationApi = relationApi
  )

  lazy val security = new MessageSecurity(
    prefApi = prefApi,
    relationApi = relationApi,
    spam = spam,
    chatPanic = new ChatPanic,
    bus = system.lilaBus
  )

  lazy val notifier = new MessageNotify(
    notifyApi = notifyApi,
    scheduler = system.scheduler
  )

  lazy val api = new MessageApi(
    security = security,
    lightUserApi = lightUserApi,
    relationApi = relationApi,
    jsonView = jsonView,
    notifier = notifier,
    shutup = shutup,
    spam = spam,
    lilaBus = system.lilaBus
  )

  lazy val socket = new MessageSocket(system, SocketSriTtl)

  lazy val socketHandler = new MessageSocketHandler(
    api = api,
    socket = socket,
    hub = lila.hub.Env.current
  )

  def cli =
    new lila.common.Cli {
      def process = {
        case "message" :: "multi" :: orig :: dests :: words =>
          api.cliMultiPost(orig, dests.map(_.toLower).split(',').toIndexedSeq, words mkString " ")
      }
    }

}

object Env {

  lazy val current = "message" boot new Env(
    config = lila.common.PlayApp loadConfig "message",
    db = lila.db.Env.current,
    shutup = lila.hub.Env.current.shutup,
    notifyApi = lila.notify.Env.current.api,
    blocks = lila.relation.Env.current.api.fetchBlocks,
    follows = lila.relation.Env.current.api.fetchFollows,
    prefApi = lila.pref.Env.current.api,
    spam = lila.security.Env.current.spam,
    system = lila.common.PlayApp.system,
    hub = lila.hub.Env.current,
    isOnline = lila.user.Env.current.isOnline,
    userCache = lila.user.Env.current.cached,
    relationApi = lila.relation.Env.current.api,
    lightUserApi = lila.user.Env.current.lightUserApi
  )
}
