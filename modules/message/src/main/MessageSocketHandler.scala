package lila.message

import lila.socket._
import lila.user.User
import lila.common.ApiVersion
import lila.message.MessageSocket._

final class MessageSocketHandler(
    api: MessageApi,
    socket: MessageSocket,
    hub: lila.hub.Env
) {

  private def controller(
    sri: Socket.Sri,
    member: SocketMember,
    user: Option[User]
  ): Handler.Controller = {
    case ("messageSend", o) => {
      for {
        data <- o obj "d"
        dest <- data.str("dest") map lila.user.User.normalize
        text <- data.str("text")
      } yield member.userId.foreach { orig =>
        api.post(orig, dest, text)
      }
    }
    case ("messageRead", o) => o str "d" foreach { dest =>
      member.userId.foreach { orig =>
        api.setRead(orig, dest)
      }
    }
    case ("messageType", o) => o str "d" foreach { dest =>
      member.userId.foreach { orig =>
        socket ! MessageType(orig, dest)
      }
    }
  }

  def apply(
    sri: Socket.Sri,
    user: Option[User],
    apiVersion: ApiVersion
  ): Fu[JsSocketHandler] =
    socket.ask[Connected](Join(sri, user.map(_.id), _)) map {
      case Connected(enum, member) => Handler.iteratee(
        hub,
        controller(sri, member, user),
        member,
        socket,
        sri,
        apiVersion
      ) -> enum
    }
}
