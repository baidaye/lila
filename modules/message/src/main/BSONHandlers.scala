package lila.message

import reactivemongo.bson._
import reactivemongo.bson.Macros
import lila.user.User
import lila.db.dsl._
import lila.db.BSON

object BSONHandlers {

  import lila.db.dsl.BSONJodaDateTimeHandler
  implicit val MessageHandler = Macros.handler[Message]
  implicit val LastMessageHandler = Macros.handler[LastMessage]

  implicit val ThreadHandler = new BSON[MessageThread] {
    def reads(r: BSON.Reader) =
      r.strsD("users") match {
        case List(u1, u2) =>
          MessageThread(
            _id = r.get[String]("_id"),
            user1 = u1,
            user2 = u2,
            lastMessage = r.get[LastMessage]("lastMessage"),
            del = r.getO[List[String]]("del")
          )
        case x => sys error s"Invalid MessageThread users: $x"
      }

    def writes(w: BSON.Writer, t: MessageThread) =
      $doc(
        "_id" -> t.id,
        "users" -> t.users.sorted,
        "lastMessage" -> t.lastMessage,
        "del" -> t.del
      )
  }

}
