package lila.message

import play.api.libs.iteratee._
import play.api.libs.json.{ JsObject, JsValue }
import scala.concurrent.duration._
import scala.concurrent.Promise
import lila.user.User
import lila.socket._

final class MessageSocket(
    system: akka.actor.ActorSystem,
    sriTtl: FiniteDuration
) extends SocketTrouper[MessageSocket.Member](system, sriTtl) with LoneSocket {

  def monitoringName = "message"
  def broomFrequency = 4027 millis

  import MessageSocket._

  system.lilaBus.subscribe(this, 'messageNew, 'relation)

  def receiveSpecific = {

    case Join(sri, userId, promise) =>
      val (enumerator, channel) = Concurrent.broadcast[JsValue]
      val member = Member(channel, userId)
      addMember(sri, member)
      promise success Connected(enumerator, member)

    case MessageNew(dest, data) =>
      firstMemberByUserId(dest) foreach { member =>
        notifyMember("messageNew", data)(member)
      }
    case MessageType(orig, dest) =>
      firstMemberByUserId(dest) foreach { member =>
        notifyMember("messageType", orig)(member)
      }
    case lila.hub.actorApi.relation.Block(from, to) =>
      firstMemberByUserId(to) foreach { member =>
        notifyMember("blockedBy", from)(member)
      }
    case lila.hub.actorApi.relation.UnBlock(from, to) =>
      firstMemberByUserId(to) foreach { member =>
        notifyMember("unblockedBy", from)(member)
      }
  }

}

object MessageSocket {

  case class Member(
      channel: JsChannel,
      userId: Option[User.ID]
  ) extends SocketMember

  case class Join(sri: Socket.Sri, userId: Option[String], promise: Promise[Connected])
  case class Connected(enumerator: JsEnumerator, member: Member)
  case class MessageNew(dest: User.ID, data: JsObject)
  case class MessageType(orig: User.ID, dest: User.ID)
}
