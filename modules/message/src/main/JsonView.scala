package lila.message

import lila.user.User
import lila.common.LightUser
import lila.relation.Relations
import play.api.libs.json._

final class JsonView(
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi
) {

  implicit private val relationsWrites = Json.writes[Relations]

  def threads(me: User)(threads: List[MessageThread]): Fu[JsArray] =
    withContacts(me, threads) map { threads =>
      JsArray(threads map renderThread)
    }

  def convo(c: MessageConvo): JsObject =
    Json.obj(
      "user" -> renderContact(c.contact),
      "messages" -> c.messages.map(renderMsg),
      "relations" -> c.relations,
      "postable" -> c.postable
    )

  def renderMsg(message: Message): JsObject =
    Json
      .obj(
        "text" -> message.text,
        "user" -> message.orig,
        "date" -> message.date
      )

  def renderLastMsg(lastMsg: LastMessage): JsObject =
    Json
      .obj(
        "text" -> lastMsg.text,
        "user" -> lastMsg.orig,
        "date" -> lastMsg.date,
        "read" -> lastMsg.read
      )

  def searchResult(me: User)(res: MessageSearch.Result): Fu[JsObject] =
    withContacts(me, res.threads) map { threads =>
      Json.obj(
        "contacts" -> threads.map(renderThread),
        "friends" -> res.friends,
        "users" -> res.users
      )
    }

  private def withContacts(me: User, threads: List[MessageThread]): Fu[List[MessageThread.WithContact]] =
    lightUserApi.asyncMany(threads.map(_ other me)) map { users =>
      threads.zip(users).map {
        case (thread, userOption) =>
          MessageThread.WithContact(thread, userOption | LightUser.fallback(thread other me))
      }
    }

  private def renderThread(t: MessageThread.WithContact) =
    Json
      .obj(
        "user" -> renderContact(t.contact),
        "lastMessage" -> renderLastMsg(t.thread.lastMessage)
      )

  private def renderContact(user: LightUser): JsObject =
    LightUser
      .lightUserWrites.writes(user)
      .add("online" -> isOnline(user.id))
}
