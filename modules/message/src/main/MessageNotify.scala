package lila.message

import lila.db.dsl._
import akka.actor.Cancellable
import java.util.concurrent.ConcurrentHashMap
import scala.concurrent.duration._
import lila.notify.{ Notification, PrivateMessage }
import lila.common.String.shorten
import lila.notify.Notification.Sender
import lila.user.User

class MessageNotify(
    notifyApi: lila.notify.NotifyApi,
    scheduler: akka.actor.Scheduler
) {

  private val delay = 5 seconds

  private val delayed: ConcurrentHashMap[MessageThread.ID, Cancellable] = new ConcurrentHashMap[MessageThread.ID, Cancellable](256)
  object FunctionConverter {
    implicit def scalaFunctionToJava[From, To](function: (From) => To): java.util.function.Function[From, To] = {
      new java.util.function.Function[From, To] {
        override def apply(input: From): To = function(input)
      }
    }
  }
  import FunctionConverter._

  def onPost(threadId: MessageThread.ID): Unit = schedule(threadId)

  def onRead(threadId: MessageThread.ID, userId: User.ID, contactId: User.ID): Funit = {
    !cancel(threadId) ?? {
      notifyApi.markRead(
        lila.notify.Notification.Notifies(userId),
        $doc("content.type" -> "privateMessage", "content.user" -> contactId)
      )
    }
  }

  def deleteAllBy(threads: List[MessageThread], user: User): Funit =
    threads
      .map { thread =>
        cancel(thread.id)
        notifyApi
          .remove(
            lila.notify.Notification.Notifies(thread other user),
            $doc("content.user" -> user.id)
          )
          .void
      }
      .sequenceFu
      .void

  private def schedule(threadId: MessageThread.ID): Unit = {
    Option(delayed.get(threadId)).foreach { canc =>
      canc.cancel()
      delayed.remove(threadId)
    }
    delayed.put(threadId, scheduler.scheduleOnce(delay) {
      doNotify(threadId)
    })
  }

  private def cancel(threadId: MessageThread.ID): Boolean =
    Option(delayed remove threadId).map(_.cancel()).isDefined

  private def doNotify(threadId: MessageThread.ID): Funit =
    MessageThreadRepo.byId(threadId) flatMap {
      _ ?? { thread =>
        val lastMessage = thread.lastMessage
        val dest = lastMessage.dest
        !thread.delBy(dest) ?? {
          //lila.common.Bus.publish(MessageThread.Unread(thread), "messageUnread")
          notifyApi addNotification Notification.make(
            Sender(lastMessage.orig).some,
            Notification.Notifies(dest),
            PrivateMessage(
              PrivateMessage.SenderId(lastMessage.orig),
              PrivateMessage.Thread(threadId, threadId),
              PrivateMessage.Text(shorten(lastMessage.text, 80))
            )
          )
        }
      }
    }
}
