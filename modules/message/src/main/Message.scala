package lila.message

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class Message(
    _id: Message.ID,
    orig: User.ID,
    dest: User.ID,
    text: String,
    date: DateTime,
    threadId: MessageThread.ID
) {

  def id = _id

  def asLast =
    LastMessage(
      orig = orig,
      dest = dest,
      text = text take 60,
      date = date,
      read = false
    )
}

object Message {

  type ID = String

  def make(
    orig: User.ID,
    dest: User.ID,
    text: String,
    threadId: String
  ): Message = {
    val cleanText = text.trim
    Message(
      _id = Random nextString 32,
      orig = orig,
      dest = dest,
      text = cleanText take 10000,
      date = DateTime.now,
      threadId = threadId
    )
  }
}

case class LastMessage(
    orig: User.ID,
    dest: User.ID,
    text: String,
    date: DateTime,
    read: Boolean
) {
  def unreadBy(userId: User.ID) = !read && orig != userId
}
