package lila.message

import akka.actor.ActorSelection
import lila.common.LightUser
import lila.user.User
import akka.pattern.ask
import makeTimeout.large
import lila.hub.actorApi.relation.GetMarks

class MessageSearch(
    userCache: lila.user.Cached,
    markActor: ActorSelection,
    lightUserApi: lila.user.LightUserApi,
    relationApi: lila.relation.RelationApi
) {

  def apply(me: User, q: String): Fu[MessageSearch.Result] =
    if (me.kid) forKid(me, q)
    else
      MessageThreadRepo.searchThreads(me, q) zip searchFriends(me, q) zip searchUsers(me, q) map {
        case ((threads, friends), users) =>
          MessageSearch
            .Result(
              threads,
              friends.filterNot(f => threads.exists(_.other(me) == f.id)) take 10,
              users.filterNot(u => u.id == me.id || friends.exists(_.id == u.id)) take 10
            )
      }

  private def forKid(me: User, q: String): Fu[MessageSearch.Result] = for {
    threads <- MessageThreadRepo.searchThreads(me, q)
  } yield MessageSearch.Result(threads, Nil, Nil)

  val empty = MessageSearch.Result(Nil, Nil, Nil)

  private def searchFriends(me: User, q: String): Fu[List[LightUser]] =
    relationApi.searchFollowedBy(me, q, 15) flatMap lightUserApi.asyncMany dmap (_.flatten)

  private def searchUsers(me: User, q: String): Fu[List[LightUser]] =
    for {
      userMarkLike <- userMarkLike(me, q)
      userIdLike <- userCache.userIdsLike(q)
      users <- lightUserApi.asyncMany((userMarkLike ++ userIdLike).distinct) dmap (_.flatten)
    } yield users

  private def userMarkLike(me: User, q: String): Fu[List[User.ID]] = {
    userMarks(me.id) map { markMap =>
      markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(q.toLowerCase)
        }
      }.keySet.toList
    }
  }

  private def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]
}

object MessageSearch {

  case class Result(
      threads: List[MessageThread],
      friends: List[LightUser],
      users: List[LightUser]
  )
}
