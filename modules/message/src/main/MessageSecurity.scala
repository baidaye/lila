package lila.message

import scala.concurrent.duration._
import lila.memo.RateLimit
import lila.security.Granter
import lila.shutup.Analyser
import lila.user.{ User, UserRepo }
import ornicar.scalalib.Zero

class MessageSecurity(
    prefApi: lila.pref.PrefApi,
    relationApi: lila.relation.RelationApi,
    spam: lila.security.Spam,
    chatPanic: lila.chat.ChatPanic,
    bus: lila.common.Bus
) {

  import MessageSecurity._

  // 创建会话
  private val CreateLimitPerUser = new RateLimit[User.ID](
    credits = 100,
    duration = 24 hour,
    name = "message create per user",
    key = "message_create.user"
  )

  // 正常聊天
  private val ReplyLimitPerUser = new RateLimit[User.ID](
    credits = 100,
    duration = 1 minute,
    name = "message reply per user",
    key = "message_reply.user"
  )

  object can {

    def post(
      contacts: User.Contacts,
      rawText: String,
      isNew: Boolean,
      unlimited: Boolean = false
    ): Fu[Verdict] = {
      val text = rawText.trim
      if (text.isEmpty) fuccess(Invalid)
      else
        may.post(contacts, isNew) flatMap {
          case false => fuccess(Block)
          case _ =>
            isLimited(contacts, isNew, unlimited) orElse
              isSpam(text) orElse
              isTroll(contacts) orElse
              isDirt(text) getOrElse
              fuccess(Ok)
        } flatMap {
          case mute: Mute =>
            relationApi.fetchFollows(contacts.dest.id, contacts.orig.id) dmap { isFriend =>
              if (isFriend) Ok else mute
            }
          case verdict => fuccess(verdict)
        } addEffect {
          case Dirt =>
            // to update user dirt tag
            logger.warn(s"Dirt Message from ${contacts.orig.id} to ${contacts.dest.id}: $text")
          case Spam =>
            // to update user span tag
            logger.warn(s"Spam Message from ${contacts.orig.id} to ${contacts.dest.id}: $text")
          case _ =>
        }
    }

    private def isLimited(contacts: User.Contacts, isNew: Boolean, unlimited: Boolean): Fu[Option[Verdict]] =
      if (unlimited) fuccess(none)
      else {
        implicit val limitedDefault = Zero.instance[Option[Verdict]](Limit.some)
        if (isNew) {
          fuccess(
            CreateLimitPerUser[Option[Verdict]](contacts.orig.id, 1)(none)
          )
        } else
          fuccess(
            ReplyLimitPerUser[Option[Verdict]](contacts.orig.id, 1)(none)
          )
      }

    // 滥发聊天信息（引诱跳转到其它国象网站）
    private def isSpam(text: String): Fu[Option[Verdict]] =
      spam.detect(text) ?? fuccess(Spam.some)

    // 禁用聊天
    private def isTroll(contacts: User.Contacts): Fu[Option[Verdict]] =
      (contacts.orig.troll && !contacts.dest.troll) ?? fuccess(Troll.some)

    // 脏话屏蔽
    private def isDirt(text: String): Fu[Option[Verdict]] =
      Analyser(text).dirty ?? fuccess(Dirt.some)
  }

  object may {

    def post(orig: User.ID, dest: User.ID, isNew: Boolean): Fu[Boolean] =
      UserRepo.contacts(orig, dest) flatMap {
        _ ?? { post(_, isNew) }
      }

    def post(contacts: User.Contacts, isNew: Boolean): Fu[Boolean] =
      fuccess(contacts.dest.id != User.lichessId) >>&
        fuccess(contacts.orig.confirmed && contacts.dest.confirmed) >>& {
          fuccess(Granter(_.ModMessage)(contacts.orig)) >>| {
            !relationApi.fetchBlocks(contacts.dest.id, contacts.orig.id) >>&
              (create(contacts) >>| reply(contacts)) >>&
              fuccess(chatPanic.allowed(contacts.orig)) >>&
              kidCheck(contacts, isNew)
          }
        }

    // 判断对方的个人设置，从而判断是否可以发起一个新会话
    private def create(contacts: User.Contacts): Fu[Boolean] =
      prefApi.getPref(contacts.dest).flatMap { prefs =>
        prefs.message match {
          case lila.pref.Pref.Message.NEVER => fuccess(false)
          case lila.pref.Pref.Message.FRIEND => relationApi.fetchFollows(contacts.dest.id, contacts.orig.id)
          case lila.pref.Pref.Message.ALWAYS => fuccess(true)
        }
      }

    // Even if the dest prefs disallow it,
    // you can still reply if they recently messaged you,
    // unless they deleted the thread.
    private def reply(contacts: User.Contacts): Fu[Boolean] =
      MessageThreadRepo.existsDestNotDel(contacts.orig.id, contacts.dest.id)

    private def kidCheck(contacts: User.Contacts, isNew: Boolean): Fu[Boolean] =
      if (!isNew || !contacts.hasKid) fuTrue
      else isCloseTo(contacts)
  }

  private def isCloseTo(contacts: User.Contacts) =
    isCoachOf(contacts.orig.id, contacts.dest.id) >>|
      isClazzCoachOf(contacts.orig.id, contacts.dest.id) >>|
      isTeamManagerOf(contacts.orig.id, contacts.dest.id)

  private def isCoachOf(coach: User.ID, student: User.ID) =
    bus.ask[Boolean]('isCoachOf) { lila.hub.actorApi.coach.IsCoachOfUser(coach, student, _) }

  private def isClazzCoachOf(coach: User.ID, student: User.ID) =
    bus.ask[Boolean]('isClazzCoachOf) { lila.hub.actorApi.clazz.IsCoachOfUser(coach, student, _) }

  private def isTeamManagerOf(manager: User.ID, member: User.ID) =
    bus.ask[Boolean]('isTeamManagerOf) { lila.hub.actorApi.team.IsManagerOfUser(manager, member, _) }

}

object MessageSecurity {

  sealed trait Verdict
  sealed trait Reject extends Verdict
  sealed abstract class Send(val mute: Boolean) extends Verdict
  sealed abstract class Mute extends Send(true)

  case object Ok extends Send(false)
  case object Troll extends Mute
  case object Spam extends Mute
  case object Dirt extends Mute
  case object Block extends Reject
  case object Limit extends Reject
  case object Invalid extends Reject
}
