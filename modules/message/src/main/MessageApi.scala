package lila.message

import lila.common.LightUser
import lila.message.MessageSocket.MessageNew
import lila.user.{ User, UserRepo }
import org.joda.time.DateTime
import scala.util.Try

final class MessageApi(
    security: MessageSecurity,
    lightUserApi: lila.user.LightUserApi,
    relationApi: lila.relation.RelationApi,
    jsonView: JsonView,
    notifier: MessageNotify,
    shutup: akka.actor.ActorSelection,
    spam: lila.security.Spam,
    lilaBus: lila.common.Bus
) {

  import MessageApi._

  def threadsOf(me: User): Fu[List[MessageThread]] =
    MessageThreadRepo.threadsOf(me)

  def convoWith(me: User, username: String, beforeMillis: Option[Long] = None): Fu[Option[MessageConvo]] = {
    val userId = User.normalize(username)
    val threadId = MessageThread.id(me.id, userId)
    val before = beforeMillis flatMap { millis =>
      Try(new DateTime(millis)).toOption
    }
    (userId != me.id) ?? lightUserApi.async(userId).flatMap {
      _ ?? { contact =>
        for {
          _ <- setReadBy(threadId, me.id, userId)
          messages <- MessageRepo.threadMessageFor(threadId, me, before)
          relations <- relationApi.fetchRelations(me.id, userId)
          postable <- security.may.post(me.id, userId, isNew = messages.headOption.isEmpty)
        } yield MessageConvo(contact, messages, relations, postable).some
      }
    }
  }

  def recentByForMod(user: User, nb: Int): Fu[List[MessageConvo]] =
    MessageThreadRepo.recentThreads(user, nb).flatMap {
      _.map { thread =>
        MessageRepo.threadMessageFor(thread.id).flatMap { messages =>
          lightUserApi async thread.other(user) map { contact =>
            MessageConvo(
              contact | LightUser.fallback(thread other user),
              messages,
              lila.relation.Relations(none, none),
              postable = false
            )
          }
        }
      }.sequenceFu
    }

  def delete(me: User, username: String): Funit = {
    val threadId = MessageThread.id(me.id, User.normalize(username))
    MessageThreadRepo.delete(threadId, me.id) >>
      MessageRepo.delete(threadId, me.id)
  }

  def setReadBy(threadId: MessageThread.ID, me: User.ID, contactId: User.ID): Funit =
    MessageThreadRepo.setRead(threadId, me) flatMap { res =>
      (res.nModified > 0) ?? notifier.onRead(threadId, me, contactId)
    }

  def setRead(userId: User.ID, contactId: User.ID): Funit = {
    val threadId = MessageThread.id(userId, contactId)
    setReadBy(threadId, userId, contactId)
  }

  def post(
    orig: User.ID,
    dest: User.ID,
    text: String,
    multi: Boolean = false,
    ignoreSecurity: Boolean = false
  ): Fu[PostResult] = {
    val threadId = MessageThread.id(orig, dest)
    val preMessage = Message.make(orig, dest, text, threadId)
    for {
      contactsOption <- UserRepo.contacts(orig, dest)
      contacts = contactsOption err s"Missing convo contact user $orig -> $dest"
      isNew <- !MessageThreadRepo.exists(threadId)
      verdict <- {
        if (ignoreSecurity) fuccess(MessageSecurity.Ok)
        else security.can.post(contacts, text, isNew, unlimited = multi)
      }
      res <- verdict match {
        case MessageSecurity.Invalid => {
          lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "您发送的是无效消息")), 'messageNew)
          fuccess(PostResult.Invalid)
        }
        case MessageSecurity.Limit => {
          lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "发送频率过快")), 'messageNew)
          fuccess(PostResult.Limited)
        }
        case MessageSecurity.Block => {
          lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "1.已被对方拉黑；2.对方隐私设置中开启了私信保护；3.对方开启了儿童模式；4.自方或对方未进行账号验证。")), 'messageNew)
          fuccess(PostResult.Bounced)
        }
        case send: MessageSecurity.Send => {
          send match {
            case mute: MessageSecurity.Mute => {
              mute match {
                case MessageSecurity.Troll => {
                  lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "您已经被禁用消息功能")), 'messageNew)
                  fuccess(PostResult.Bounced)
                }
                case MessageSecurity.Spam => {
                  lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "消息涉嫌引诱跳转到其它网站")), 'messageNew)
                  fuccess(PostResult.Bounced)
                }
                case MessageSecurity.Dirt => {
                  lilaBus.publish(MessageNew(orig, warnMessage(preMessage, "使用了非文明用语")), 'messageNew)
                  fuccess(PostResult.Bounced)
                }
              }
            }
            case MessageSecurity.Ok => {
              val message = if (verdict == MessageSecurity.Spam) preMessage.copy(text = spam.replace(text)) else preMessage
              val msgWrite = MessageRepo.insert(message)
              val threadWrite = {
                if (isNew)
                  MessageThreadRepo.insert(
                    MessageThread.make(orig, dest, message, del = List(
                      multi option orig,
                      send.mute option dest
                    ).flatten.some)
                  )
                else MessageThreadRepo.update(threadId, message, send, multi)
              }
              (msgWrite zip threadWrite) >>- {
                if (!send.mute) {
                  notifier.onPost(threadId)
                  lilaBus.publish(MessageNew(dest, jsonView.renderMsg(message)), 'messageNew)
                  shutup ! lila.hub.actorApi.shutup.RecordPrivateMessage(orig, dest, text)
                }
              } inject PostResult.Success
            }
          }
        }
      }
    } yield res
  }

  private def warnMessage(preMessage: Message, warn: String) = {
    val orig = preMessage.dest
    val dest = preMessage.dest
    jsonView.renderMsg(
      preMessage.copy(
        orig = orig,
        dest = dest,
        text = s"对方无法接收此信息！可能原因：$warn"
      )
    ).add("warn", true)
  }

  def sendPreset(mod: User, user: User, preset: MessagePreset): Fu[PostResult] =
    post(mod.id, user.id, preset.text, multi = true)

  def deleteThreadsBy(user: User): Funit =
    MessageThreadRepo.allThreadsOf(user.id) flatMap { threads =>
      MessageThreadRepo.deleteByUser(user.id)
      MessageRepo.deleteByThread(threads.map(_.id)) >>
        notifier.deleteAllBy(threads, user)
    }

  def sendPresetFromLichess(user: User, preset: MessagePreset): Fu[PostResult] =
    UserRepo.lichess flatten "Missing haichess user" flatMap { sendPreset(_, user, preset) }

  def cliMultiPost(orig: String, dests: Seq[User.ID], text: String): Fu[String] =
    UserRepo named orig flatMap {
      case None => fuccess(s"Unknown sender $orig")
      case Some(sender) => multiPost(sender.id, dests, text) inject "done"
    }

  def multiPost(orig: User.ID, dests: Seq[User.ID], text: String): Funit =
    dests.filterNot(_ == orig).map { dest =>
      post(orig, dest, text, multi = true)
    }.sequenceFu.void
}

object MessageApi {

  sealed trait PostResult

  object PostResult {
    case object Success extends PostResult
    case object Invalid extends PostResult
    case object Limited extends PostResult
    case object Bounced extends PostResult
    case object Mute extends PostResult
  }
}
