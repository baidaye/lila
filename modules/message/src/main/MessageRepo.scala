package lila.message

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime

object MessageRepo {

  import BSONHandlers.MessageHandler

  private val coll = Env.current.messageColl

  def insert(message: Message): Funit =
    coll.insert(message).void

  def delete(threadId: MessageThread.ID, userId: User.ID): Funit =
    coll.update(
      $doc("threadId" -> threadId),
      $addToSet("del" -> userId),
      multi = true
    ).void

  def deleteByThread(threadIds: List[MessageThread.ID]): Funit =
    coll.remove($doc("threadId" $in threadIds)).void

  def threadMessageFor(threadId: MessageThread.ID, me: User, before: Option[DateTime]): Fu[List[Message]] =
    coll.find(
      $doc("threadId" -> threadId, "del" $ne me.id) ++
        before.?? { b =>
          $doc("date" $lt b)
        }
    )
      .sort($sort desc "date")
      .cursor[Message]()
      .list(100)

  def threadMessageFor(threadId: MessageThread.ID): Fu[List[Message]] =
    coll.find($doc("threadId" -> threadId))
      .sort($sort desc "date")
      .cursor[Message]()
      .list(10)

}
