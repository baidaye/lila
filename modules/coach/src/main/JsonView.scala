package lila.coach

import lila.user.User
import lila.common.LightUser
import play.api.libs.json._

final class JsonView(isOnline: User.ID => Boolean) {

  def renderLightUser(user: User, coachId: User.ID, coachName: String, markOption: Option[String]): JsObject = {
    LightUser
      .lightUserWrites.writes(user.light)
      .add("isMember" -> user.isMember)
      .add("online" -> isOnline(user.id))
      .add("mark" -> markOption)
      .add("coachId" -> coachId.some)
      .add("coachName" -> coachName.some)
  }

}

