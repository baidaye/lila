package lila.coach

import lila.coach.Student.Status
import lila.db.dsl._
import lila.hub.actorApi.relation.GetMarks
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import akka.pattern.ask
import akka.actor.ActorSelection
import makeTimeout.large
import lila.user.{ User, UserRepo }
import org.joda.time.DateTime
import reactivemongo.api.ReadPreference

final class StudentApi(coll: Coll, markActor: ActorSelection, notifyApi: NotifyApi) {

  import BsonHandlers.StudentBSONHandler

  def byId(id: String): Fu[Option[Student]] = coll.byId[Student](id)

  def byIds(coachId: User.ID, studentId: User.ID): Fu[Option[Student]] =
    coll.byId[Student](Student.makeId(coachId, studentId))

  def loadStudent(
    coach: User,
    markMap: Map[String, Option[String]],
    username: Option[String] = None,
    sex: Option[String] = None,
    age: Option[Int] = None,
    level: Option[String] = None
  ): Fu[List[User]] = {
    mineStudents(coach.some) flatMap { studentIds =>
      val markUserIds =
        username.?? { txt =>
          markMap.filterValues { markOption =>
            markOption.?? { mark =>
              mark.toLowerCase.contains(txt.toLowerCase)
            }
          }.keySet
        }.intersect(studentIds)

      var $selector = $doc("_id" $in studentIds)
      username.foreach { u =>
        $selector = $selector ++ $or($doc("username" $regex (u, "i")), $doc("_id" $in markUserIds))
      }
      sex.foreach { s =>
        $selector = $selector ++ $doc("profile.sex" -> s)
      }
      age.foreach { a =>
        $selector = $selector ++ $doc("profile.birthyear" -> (DateTime.now.getYear - a))
      }
      level.foreach { l =>
        $selector = $selector ++ $doc("profile.levels" -> $doc("level" -> l, "current" -> 1))
      }

      UserRepo.find($selector)
        .sort($doc("_id" -> 1))
        .list[lila.user.User]()
    }
  }

  def mineCoach(user: Option[User]): Fu[Set[String]] = user.?? { u =>
    coll.distinct[String, Set](
      "coachId",
      $doc(
        "studentId" -> u.id,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      ).some
    )
  }

  def mineCoach(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set](
      "coachId",
      $doc(
        "studentId" -> userId,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      ).some
    )

  def mineCertifyCoach(user: Option[User]): Fu[List[User]] = user.?? { u =>
    for {
      coachs <- coll.distinct[String, Set](
        "coachId",
        $doc(
          "studentId" -> u.id,
          "available" -> true,
          "status" -> Student.Status.Approved.id
        ).some
      )
      users <- UserRepo.byOrderedIds(coachs.toSeq, ReadPreference.secondaryPreferred)
    } yield users.filter(_.isCoach)
  }

  def mineStudents(user: Option[User]): Fu[Set[String]] = user.?? { u =>
    coll.distinct[String, Set](
      "studentId",
      $doc(
        "coachId" -> u.id,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      ).some
    )
  }

  def mineStudentsById(coachId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set](
      "studentId",
      $doc(
        "coachId" -> coachId,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      ).some
    )

  def mineStudentsWithUser(coachId: String): Fu[List[StudentWithUser]] =
    coll.find(
      $doc(
        "coachId" -> coachId,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      )
    ).list().flatMap(withUsers)

  def applyingList(coachId: String): Fu[List[Student]] =
    coll.find(
      $doc(
        "coachId" -> coachId,
        "available" -> true,
        "status" -> Student.Status.Applying.id
      )
    ).sort($doc("createAt" -> -1)).list()

  def approvedList(coachId: String, markMap: Map[String, Option[String]], q: String): Fu[List[StudentWithUser]] = {
    val filterMarkUserIds = q.nonEmpty ?? {
      markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(q.toLowerCase)
        }
      }.keySet
    }

    coll.find(
      $doc(
        "coachId" -> coachId,
        "available" -> true,
        "status" -> Student.Status.Approved.id
      ) ++ q.trim.nonEmpty.?? {
          $or($doc("studentId" $regex (q.trim, "i")), $doc("studentId" $in filterMarkUserIds))
        }
    ).sort($doc("studentId" -> 1)).list() flatMap withUsers
  }

  private def withUsers(students: Seq[Student]): Fu[List[StudentWithUser]] =
    UserRepo.withColl {
      _.byOrderedIds[User, User.ID](students.map(_.studentId))(_.id)
    } map { users =>
      students zip users collect {
        case (student, user) => StudentWithUser(student, user)
      } toList
    }

  def addOrReAdd(coachId: User.ID, studentId: User.ID): Funit = {
    val id = Student.makeId(coachId, studentId)
    byId(id) flatMap {
      case None => add(coachId, studentId) >>- applyNotify(coachId: User.ID, studentId: User.ID)
      case Some(s) => (!s.available || s.status == Student.Status.Decline).?? {
        coll.update(
          $id(id),
          $set(
            "available" -> true,
            "status" -> Student.Status.Applying.id,
            "createAt" -> DateTime.now
          )
        ).void >>- applyNotify(coachId: User.ID, studentId: User.ID)
      }
    }
  }

  def add(coachId: User.ID, studentId: User.ID): Funit =
    coll.insert(Student.make(coachId, studentId)).void

  def join(coachId: String, studentId: String): Funit = {
    val id = Student.makeId(coachId, studentId)
    coll.byId(id).flatMap {
      case None => {
        coll.insert(
          Student(
            id = id,
            coachId = coachId,
            studentId = studentId,
            available = true,
            status = Status.Approved,
            createAt = DateTime.now,
            approvedAt = DateTime.now.some
          )
        ).void
      }
      case Some(stu) => {
        (!stu.available).?? {
          coll.update(
            $id(id),
            $set(
              "available" -> true,
              "approvedAt" -> DateTime.now
            )
          ).void
        } >> (!stu.isApproved).?? {
          coll.update(
            $id(id),
            $set(
              "available" -> true,
              "status" -> Status.Approved.id,
              "approvedAt" -> DateTime.now
            )
          ).void
        }
      }
    }
  }

  def changeCoach(oldCoachId: String, newCoachId: String, clazzStudentIds: List[String], excludeStudentIds: List[String]): Funit = {
    addStudents(newCoachId, clazzStudentIds) >> removeStudents(oldCoachId, excludeStudentIds)
  }

  def addStudents(coachId: String, studentIds: List[String]): Funit = {
    coll.find(
      $doc(
        "coachId" -> coachId,
        "studentId" $in studentIds
      )
    ).list().flatMap { students =>
        coll.update(
          $inIds(students.filterNot(_.available).map(_.id)),
          $set(
            "available" -> true,
            "approvedAt" -> DateTime.now
          ),
          multi = true
        ).void >> coll.update(
            $inIds(students.filterNot(_.isApproved).map(_.id)),
            $set(
              "available" -> true,
              "status" -> Status.Approved.id,
              "approvedAt" -> DateTime.now
            ),
            multi = true
          ).void

        val existsUserIds = students.map(_.studentId)
        val adds = studentIds.filterNot(existsUserIds.contains(_)).map { studentId =>
          Student(
            id = Student.makeId(coachId, studentId),
            coachId = coachId,
            studentId = studentId,
            available = true,
            status = Status.Approved,
            createAt = DateTime.now,
            approvedAt = DateTime.now.some
          )
        }
        adds.nonEmpty.?? {
          coll.bulkInsert(
            documents = adds.map(StudentBSONHandler.write).toStream,
            ordered = true
          ).void
        }
      }
  }

  def approve(id: String, coachId: User.ID, studentId: User.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "status" -> Student.Status.Approved.id,
        "approvedAt" -> DateTime.now
      )
    ).void >>- approveNotify(coachId, studentId)

  def decline(id: String): Funit =
    coll.update(
      $id(id),
      $set("status" -> Student.Status.Decline.id)
    ).void

  def remove(id: String): Funit =
    coll.update(
      $id(id),
      $set("available" -> false)
    ).void

  def removeStudentByUserId(studentId: User.ID): Funit =
    coll.update(
      $doc("studentId" -> studentId),
      $set("available" -> false),
      multi = true
    ).void

  def removeStudent(coachId: User.ID, studentId: User.ID): Funit =
    remove(Student.makeId(coachId, studentId))

  def removeStudents(coachId: User.ID, studentIds: List[User.ID]): Funit =
    coll.update(
      $doc(
        "coachId" -> coachId,
        "studentId" $in studentIds
      ),
      $set("available" -> false),
      multi = true
    ).void

  private def applyNotify(coachId: User.ID, studentId: User.ID): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(studentId).some,
      Notifies(coachId),
      lila.notify.GenericLink(
        url = "/coach/student/list/applying",
        title = "学员申请".some,
        text = s"$studentId 申请成为学员".some,
        icon = "教"
      )
    ))
  }

  private def approveNotify(coachId: User.ID, studentId: User.ID): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(User.lichessId).some,
      Notifies(studentId),
      lila.notify.GenericLink(
        url = s"/coach/$coachId",
        title = "学员申请通过".some,
        text = s"您已经成为${coachId}的学员".some,
        icon = "教"
      )
    ))
  }

  def isMyStudents(me: Option[User], userId: User.ID): Fu[Boolean] = {
    me match {
      case None => fuFalse
      case Some(m) => {
        coll.exists(
          $doc(
            "coachId" -> m.id,
            "studentId" -> userId,
            "available" -> true,
            "status" -> Student.Status.Approved.id
          )
        )
      }
    }
  }

  def userMarks(coachId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(coachId)).mapTo[Map[String, Option[String]]]

}
