package lila.game

import PgnSetup._
import chess.format.Forsyth
import lila.opening.{ OpeningDB, OpeningDBNode, OpeningDBHelper, PgnParser }

case class PgnSetup(
    lastPly: Int,
    initialFen: String,
    opening: EitherOpening,
    openingdbExcludeWhiteBlunder: Option[Boolean] = None,
    openingdbExcludeBlackBlunder: Option[Boolean] = None,
    openingdbExcludeWhiteJscx: Option[Boolean] = None,
    openingdbExcludeBlackJscx: Option[Boolean] = None,
    openingdbCanOff: Option[Boolean] = None
) {

  val pgnOpeningDB = opening match {
    case Left(pgn) => PgnParser.toOpeningDB(pgn, lila.user.User.virtual())
    case Right(openingdbId) => OpeningDB.WithLine(OpeningDBHelper.openingFromCache(openingdbId), Nil)
  }

  def pgn = opening.left.toOption
  def openingdbId = opening.right.toOption

  def notInitialFen = initialFen.some.filter(f => normalizeFen(Forsyth.initial) != normalizeFen(f))

  def normalizeFen(fen: String) = fen.split(" ")(0)

  def source = opening match {
    case Left(_) => "pgn"
    case Right(_) => "openingdb"
  }

  def openingWithLine = opening match {
    case Left(pgn) => pgnOpeningDB
    case Right(openingdbId) => {
      val o = OpeningDBHelper.openingFromCache(openingdbId)
      OpeningDB.WithLine(o, Nil)
    }
  }

  def lastFen = opening match {
    case Left(pgn) => pgnOpeningDB.lastFen.value
    case Right(_) => initialFen
  }

  def nextNodes(fen: String) = opening match {
    case Left(pgn) => pgnOpeningDB.nextNodes(fen)
    case Right(openingdbId) => {
      val nodes = OpeningDBHelper.openingWithNextNodesFromCache(openingdbId, fen).nodes
      val filteredNodes = nodes.filter { node =>
        val glyphs = node.glyphs.toList
        (!(openingdbExcludeWhiteBlunder | true) || glyphs.isEmpty || !(node.piece.is(chess.White) && glyphs.exists(g => g.id == 2 || g.id == 4))) &&
          (!(openingdbExcludeBlackBlunder | true) || glyphs.isEmpty || !(node.piece.is(chess.Black) && glyphs.exists(g => g.id == 2 || g.id == 4))) &&
          (!(openingdbExcludeWhiteJscx | true) || glyphs.isEmpty || !(node.piece.is(chess.White) && glyphs.exists(g => g.id == 202))) &&
          (!(openingdbExcludeBlackJscx | true) || glyphs.isEmpty || !(node.piece.is(chess.Black) && glyphs.exists(g => g.id == 202)))
      }
      filteredNodes
    }
  }

  def nextRandomMove(fen: String) = {
    val nodes = nextNodes(fen)
    scala.util.Random.shuffle(nodes)
      .headOption
      .map(_.move)
  }

}

object PgnSetup {

  type PGN = String
  type OpeningID = String

  type EitherOpening = Either[PGN, OpeningID]

  val defaultFromOpeningPly = 1

}
