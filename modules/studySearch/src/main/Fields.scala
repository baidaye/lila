package lila.studySearch

private[studySearch] object Fields {
  val name = "name"
  val owner = "owner"
  val members = "members"
  val chapterNames = "chapterNames"
  val chapterTexts = "chapterTexts"
  val visibility = "visibility"
  val tags = "tags"
  val rank = "rank"
  val likes = "likes"
  val likers = "likers"
  val favorites = "favorites"
  val favoriters = "favoriters"
  val createdAt = "createdAt"
  val updatedAt = "updatedAt"

  object chapter {
    val name = "name"
  }

}
