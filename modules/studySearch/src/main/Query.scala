package lila.studySearch

import lila.study.Order

// channel
// all,mine,member,public,private,auth,favorites
private[studySearch] case class Query(text: String, channel: String, userId: Option[String], mineCoach: List[String], mineTeamOwner: List[String])

object Query {

  implicit val jsonWriter = play.api.libs.json.Json.writes[Query]
}
