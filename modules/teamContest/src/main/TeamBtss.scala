package lila.teamContest

import lila.teamContest.TeamBtss._

private[teamContest] abstract class TeamBtss(val id: String, val name: String) {

  def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtsss: TeamBtss.TeamerBtssScores): TeamBtss.TeamerBtssScores

}

object TeamBtss {

  case class TeamBtssScore(btss: TeamBtss, score: Double)
  case class TeamerBtssScore(teamer: Teamer, btsss: List[TeamBtssScore] = List.empty) {
    def +(btssScore: TeamBtssScore) = copy(teamer, btsss :+ btssScore)
    def scoreOfBtss(btss: TeamBtss) = btsss.find(_.btss == btss).map(_.score)
  }
  case class TeamerBtssScores(teamers: List[TeamerBtssScore]) {

    def miniTeamer(teamer: Teamer, paired: TeamPaired) =
      paired.teamer(teamer.no) err s"can not find teamer of ${teamer.no}"

    def opponentTeamer(teamer: Teamer, paired: TeamPaired): Teamer = {
      val opponentNo = paired.opponentOf(teamer.no) err s"can not find opponent no of ${teamer.no}"
      (teamers.find(_.teamer.no == opponentNo) err s"can not find opponent of ${teamer.no}").teamer
    }

    def sort: List[TeamerBtssScore] = teamers.sortWith { (teamerBtssScore1, teamerBtssScore2) =>
      {
        val s1 = teamerBtssScore1.teamer.score
        val s2 = teamerBtssScore2.teamer.score
        if (s1 > s2) true
        else if (s1 == s2) {
          teamerBtssScore1.btsss.zip(teamerBtssScore2.btsss).map {
            case (btssScore1, btssScore2) => {
              if (btssScore1.btss == TeamBtss.No) {
                btssScore2.score - btssScore1.score
              } else {
                btssScore1.score - btssScore2.score
              }
            }
          }.foldLeft((true, true)) { // (result, isContinue)
            case (result, diff) => {
              if (result._2) {
                if (diff > 0) {
                  (true, false)
                } else if (diff < 0) {
                  (false, false)
                } else {
                  (true, true)
                }
              } else {
                result
              }
            }
          }._1
        } else false
      }
    }
  }

  case object Event extends TeamBtss("event", "场分") {

    override def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtssScores: TeamerBtssScores): TeamerBtssScores = {
      TeamerBtssScores(
        teamerBtssScores.teamers.map { teamerBtssScore =>
          val teamer = teamerBtssScore.teamer
          val score = teamer.score
          teamerBtssScore + TeamBtssScore(this, score)
        }
      )
    }
  }

  case object Round extends TeamBtss("round", "局分") {

    override def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtssScores: TeamerBtssScores): TeamerBtssScores = {
      TeamerBtssScores(
        teamerBtssScores.teamers.map { teamerBtssScore =>
          val teamer = teamerBtssScore.teamer
          val score =
            paireds.filter(_.containsByNo(teamer.no)).foldLeft(0D) {
              case (s, p) => {
                val miniTeamer = teamerBtssScores.miniTeamer(teamer, p)
                s + miniTeamer.score
              }
            }
          teamerBtssScore + TeamBtssScore(this, score)
        }
      )
    }
  }

  case object Res extends TeamBtss("res", "直胜") {

    override def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtssScores: TeamerBtssScores): TeamerBtssScores = {
      TeamerBtssScores(
        teamerBtssScores.teamers.map { teamerBtssScore =>
          val teamer = teamerBtssScore.teamer
          // 相同分的选手(不包括自己)
          val sameScoreTeamers = teamerBtssScores.teamers.filter { otherPlayerBtssScore =>
            !otherPlayerBtssScore.teamer.is(teamer) &&
              otherPlayerBtssScore.teamer.score == teamer.score &&
              otherPlayerBtssScore.scoreOfBtss(Round).exists(s1 => teamerBtssScore.scoreOfBtss(Round).contains(s1))
          }.map(_.teamer)

          val score =
            paireds.filter(p => p.containsByNo(teamer.no)).foldLeft(0D) {
              case (s, p) => {
                // 对手
                val opponent = teamerBtssScores.opponentTeamer(teamer, p)
                // 相同分对手
                if (sameScoreTeamers.contains(opponent)) {
                  // 选手赢了+1分
                  if (p.isWin(teamer.no)) s + 1
                  // 平局+0.5分
                  //else if (p.isDraw) s + 0.5
                  else s
                } else s
              }
            }
          teamerBtssScore + TeamBtssScore(this, score)
        }
      )
    }
  }

  // 对手没走棋不算分
  case object Vict extends TeamBtss("vict", "黑方胜局数") {

    override def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtssScores: TeamerBtssScores): TeamerBtssScores = {
      TeamerBtssScores(
        teamerBtssScores.teamers.map { teamerBtssScore =>
          val teamer = teamerBtssScore.teamer
          val playerBlackWinCount = boards.count(b => b.blackPlayer.teamerNo == teamer.no && b.blackIsWinner)
          val score = playerBlackWinCount
          teamerBtssScore + TeamBtssScore(this, score)
        }
      )
    }
  }

  case object No extends TeamBtss("no", "序号") {

    override def score(paireds: List[TeamPaired], boards: List[TeamBoard], teamerBtssScores: TeamerBtssScores): TeamerBtssScores = {
      TeamerBtssScores(
        teamerBtssScores.teamers.map { teamerBtssScore =>
          val teamer = teamerBtssScore.teamer
          teamerBtssScore + TeamBtssScore(this, teamer.no)
        }
      )
    }
  }

  val all = List(Event, Round, Vict, Res, No)

  val byId = all map { b => (b.id, b) } toMap

  def apply(id: String): TeamBtss = byId get id err s"Bad TeamBtss $id"

  def list = all.map { r => (r.id -> r.name) }

  def default = List(Round, Res, Vict, No)

}

case class Btsss(list: List[TeamBtss])

object Btsss {

  def default = TeamBtss.default

  def teamer = Btsss(List(Round, Res, Vict))

  def formShow = Btsss(List(Event, Round, Res, Vict))

  def empty = Btsss(Nil)

}
