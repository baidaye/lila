package lila.teamContest

import lila.db.dsl._
import lila.user.User
import reactivemongo.bson._
import org.joda.time.DateTime
import scala.collection.breakOut

object TeamerRepo {

  private[teamContest] lazy val coll = Env.current.teamerColl
  import BSONHandlers.TeamerHandler
  import BSONHandlers.PairedHandler

  type ID = Teamer.ID

  def byId(id: Teamer.ID): Fu[Option[Teamer]] = coll.byId[Teamer](id)

  def byIds(ids: List[Teamer.ID]): Fu[List[Teamer]] = coll.byIds[Teamer](ids)

  def byNos(contestId: TeamContest.ID, nos: List[Teamer.No]): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId) ++ $doc("no" $in nos)).list[Teamer]()

  def byOrderedIds(ids: List[Teamer.ID]): Fu[List[Teamer]] =
    coll.byOrderedIds[Teamer, Teamer.ID](ids)(_.id)

  def findByByeRound(contestId: TeamContest.ID, byeRound: TeamRound.No): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId) ++ $doc("byeRound" -> byeRound)).list[Teamer]()

  def nameExists(contestId: TeamContest.ID, name: String, id: Option[String]): Fu[Boolean] =
    coll.exists(contestQuery(contestId) ++ $doc("name" -> name) ++ id.??(i => $doc("_id" $ne i)))

  def insert(teamer: Teamer): Funit = coll.insert(teamer).void

  def upsert(teamer: Teamer): Funit =
    coll.update($id(teamer.id), teamer, upsert = true).void

  def countByContest(contestId: TeamContest.ID): Fu[Int] =
    coll.countSel(contestQuery(contestId))

  def getByLeaderOrCreatorId(userId: User.ID): Fu[List[Teamer]] =
    coll.find($or($doc("leaders" -> userId), $doc("createdBy" -> userId))).list[Teamer]()

  def getByUserId(contestId: TeamContest.ID, userId: User.ID): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId) ++ $doc("createdBy" -> userId)).list[Teamer]()

  def getByUserIdNear1Year(userId: User.ID): Fu[List[Teamer]] =
    coll.find($doc("createdBy" -> userId, "entryTime" $gt DateTime.now.minusYears(1))).list[Teamer]()

  def getByContest(contestId: TeamContest.ID): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId)).sort($sort asc "no").list[Teamer]()

  def getJoinedByContest(contestId: TeamContest.ID): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId) ++ $doc("status" -> Teamer.Status.Joined.id)).sort($sort asc "no").list[Teamer]()

  def getUnJoinedByContest(contestId: TeamContest.ID): Fu[List[Teamer]] =
    coll.find(contestQuery(contestId) ++ $doc("status" $ne Teamer.Status.Joined.id))
      .sort($sort asc "no")
      .list[Teamer]()

  def remove(id: Teamer.ID): Funit =
    coll.remove($id(id)).void

  def removeByIds(ids: List[Teamer.ID]): Funit =
    coll.remove($inIds(ids)).void

  def setStatus(id: Teamer.ID, status: Teamer.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setJoined(id: Teamer.ID, no: Teamer.No, status: Teamer.Status): Funit =
    coll.update(
      $id(id),
      $set(
        "no" -> no,
        "status" -> status.id,
        "entryTime" -> DateTime.now
      )
    ).void

  def kick(id: Teamer.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "absent" -> true,
        "kick" -> true
      )
    ).void

  def unkick(id: Teamer.ID, isAbsent: Boolean): Funit =
    coll.update(
      $id(id),
      $set(
        "absent" -> isAbsent,
        "quit" -> false,
        "kick" -> false
      )
    ).void

  def quit(id: Teamer.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "absent" -> true,
        "quit" -> true
      )
    ).void

  def quitAll(ids: List[Teamer.ID]): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "absent" -> true,
        "quit" -> true
      ),
      multi = true
    ).void

  // 上轮弃权，本轮恢复->影响所有棋手
  def unAbsentByContest(contestId: TeamContest.ID): Funit =
    // 之前轮次不是禁赛、退赛 的状态，那么取消弃权后可以继续匹配
    coll.update(
      $doc("contestId" -> contestId, "manualAbsent" -> true, "quit" -> false, "kick" -> false),
      $set(
        "absent" -> false,
        "manualAbsent" -> false
      ),
      multi = true
    ).void >>
      // 之前轮次存在（禁赛、退赛）其中一个状态，那么取消弃权后可以不能匹配
      coll.update(
        $doc("contestId" -> contestId, "manualAbsent" -> true, $or("quit" $eq true, "kick" $eq true)),
        $set(
          "manualAbsent" -> false
        ),
        multi = true
      ).void

  // 设置手动弃权状态，unAbsentByContest函数的反向操作，取消编排发布时使用
  def setManualAbsent(ids: List[Teamer.ID]): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "absent" -> true,
        "manualAbsent" -> true
      ),
      multi = true
    ).void

  def findNextNo(contestId: TeamContest.ID): Fu[Int] = {
    coll.find(contestQuery(contestId) ++ $doc("status" -> Teamer.Status.Joined.id), $doc("_id" -> false, "no" -> true))
      .sort($sort desc "no")
      .uno[Bdoc] map {
        _ flatMap { doc => doc.getAs[Int]("no") map (1 + _) } getOrElse 1
      }
  }

  def bulkUpdate(contestId: TeamContest.ID, teamers: List[Teamer]): Funit =
    removeByContest(contestId) >> bulkInsert(teamers).void

  def removeByContest(contestId: TeamContest.ID): Funit =
    coll.remove(contestQuery(contestId)).void

  def bulkInsert(teamers: List[Teamer]): Funit = coll.bulkInsert(
    documents = teamers.map(TeamerHandler.write).toStream,
    ordered = true
  ).void

  def rounds(contest: TeamContest): Fu[List[(Teamer, Map[TeamRound.No, TeamPaired])]] = {
    import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework._
    coll.aggregateList(
      Match($doc("contestId" -> contest.id)),
      List(
        PipelineOperator(
          $doc(
            "$lookup" -> $doc(
              "from" -> Env.current.settings.CollectionPaired,
              "let" -> $doc("tno" -> "$no"),
              "pipeline" -> $arr(
                $doc(
                  "$match" -> $doc(
                    "$expr" -> $doc(
                      "$and" -> $arr(
                        $doc("$eq" -> $arr("$contestId", contest.id)),
                        $doc("$or" -> $arr(
                          $doc("$eq" -> $arr("$$tno", "$whiteTeamer.no")),
                          $doc("$eq" -> $arr("$$tno", "$blackTeamer.no"))
                        ))
                      )
                    )
                  )
                )
              ),
              "as" -> "paireds"
            )
          )
        )
      ),
      maxDocs = 1000
    ).map {
        _.flatMap { doc =>
          val result = for {
            teamer <- TeamerHandler.readOpt(doc)
            paireds <- doc.getAs[List[TeamPaired]]("paireds")
            pairedMap = paireds.map { p =>
              p.roundNo -> p
            }.toMap
          } yield (teamer, pairedMap)
          result
        }(breakOut).toList
      }
  }

  def removeTeamersLastOutcome(contest: TeamContest, teamers: List[Teamer], roundNo: TeamRound.No): Funit =
    teamers.map { teamer =>
      removeTeamerLastOutcome(contest, teamer, roundNo)
    }.sequenceFu.void

  def removeTeamerLastOutcome(contest: TeamContest, teamer: Teamer, roundNo: TeamRound.No): Funit = {
    update(
      teamer.copy(
        outcomes = teamer.removeOutcomeByRound(roundNo),
        roundScores = teamer.removeScoreByRound(roundNo)
      ) |> { p =>
          p.copy(
            score = p.allScore(contest.isRoundRobin)
          )
        }
    )
  }

  def setZeroScoreOutcomes(contestId: TeamContest.ID, roundNo: Int, teamers: List[Teamer.No], outcome: TeamBoard.Outcome): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("no" -> $in(teamers: _*)),
      $set(
        s"outcomes.${roundNo - 1}" -> outcome.id,
        s"roundScores.${roundNo - 1}" -> 0.0
      ),
      multi = true
    ).void

  def setScore(id: Teamer.ID, score: Double): Funit =
    coll.update(
      $id(id),
      $set(
        "score" -> score,
        "points" -> score
      )
    ).void

  def setNo(id: Teamer.ID, no: Teamer.No): Funit = coll.updateField($id(id), "no", no).void

  def update(teamer: Teamer): Funit = {
    coll.update(
      $id(teamer.id),
      teamer
    ).void
  }

  def setCancelScore(id: Teamer.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "cancelled" -> true
      )
    ).void

  def setByeRound(id: Teamer.ID, byeRound: List[TeamRound.No]): Funit =
    coll.update(
      $id(id),
      $set(
        "byeRound" -> byeRound
      )
    ).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

}
