package lila.teamContest

import java.io.{ File, PrintWriter }
import scala.concurrent.blocking
import scala.sys.process._
import scala.sys.process.ProcessLogger
import PairingSystem.{ Bye, ByeOrPending, Pending }

final class SwissPairing(trf: SwissTrf) extends Pairing {

  val sep = System.getProperty("line.separator")

  override def pairing(contest: TeamContest): Fu[List[ByeOrPending]] =
    trf(contest).map { trfContent =>
      logger.info(sep + s"==============SwissPairing TrfContent $contest 第${contest.currentRound}轮 ================")
      logger.info(sep + trfContent)
      val pairResult = invoke(trfContent)
      logger.info(sep + s"==============SwissPairing PairResult $contest 第${contest.currentRound}轮 ================")
      logger.info(sep + pairResult.mkString(sep))
      pairResult
        .drop(1) // first line is the number of pairings
        .map(_ split ' ')
        .collect {
          case Array(p, "0") => Left(Bye(p.toInt))
          case Array(w, b) => Right(Pending(w.toInt, b.toInt))
        }
    }

  override def pairingAll(contest: TeamContest, teamers: List[Teamer]): Fu[Map[Int, List[ByeOrPending]]] =
    fuccess(Map.empty[Int, List[ByeOrPending]])

  private def invoke(trfContent: String): List[String] =
    withTempFile(trfContent) { file =>
      val command = s"java -jar ./bin/javafo.jar ${file.getAbsolutePath}  -p"
      val stdout = new collection.mutable.ListBuffer[String]
      val stderr = new StringBuilder
      val status = blocking {
        command ! ProcessLogger(stdout append _, stderr append _)
      }
      if (status != 0) {
        val error = stderr.toString
        if (error contains "No valid pairing exists") Nil
        else throw SwissPairing.PairingException(error, trfContent)
      } else if (stdout.headOption.??(_.contains("Exception"))) {
        throw SwissPairing.PairingException(stdout.toList.mkString(sep), trfContent)
      } else stdout.toList
    }

  def withTempFile[A](content: String)(f: File => A): A = {
    val file = File.createTempFile("haichess-", "-contest")
    val writer = new PrintWriter(file)
    writer.print(content)
    writer.flush()
    writer.close()
    val res = f(file)
    file.delete()
    res
  }

}

object SwissPairing {

  case class PairingException(val message: String, val trfContent: String) extends lila.base.LilaException

}
