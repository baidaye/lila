package lila.teamContest

import lila.db.dsl._
import reactivemongo.bson._

object ForbiddenRepo {

  private[teamContest] lazy val coll = Env.current.forbiddenColl
  import BSONHandlers.ForbiddenHandler

  def byId(id: String): Fu[Option[Forbidden]] = coll.byId[Forbidden](id)

  def getByContest(contestId: TeamContest.ID): Fu[List[Forbidden]] =
    coll.find(contestQuery(contestId)).list[Forbidden]()

  def upsert(forbidden: Forbidden): Funit = coll.update($id(forbidden.id), forbidden, upsert = true).void

  def remove(forbidden: Forbidden): Funit = coll.remove($id(forbidden.id)).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

}
