package lila.teamContest

import chess.variant.Variant
import chess.Clock.{ Config => ClockConfig }
import chess.{ Mode, Speed, StartingPosition }
import org.joda.time.{ DateTime, Duration, Interval }
import ornicar.scalalib.Random
import lila.user.User
import lila.game.PerfPicker
import lila.rating.PerfType
import TeamContest._

case class TeamContest(
    _id: ID,
    name: String,
    groupName: Option[String],
    logo: Option[String],
    typ: Type,
    organizer: String, // teamid / federationid
    variant: Variant,
    position: StartingPosition,
    mode: Mode, // rated ?
    teamRated: Boolean,
    clock: ClockConfig,
    rule: Rule,
    startsAt: DateTime,
    finishAt: DateTime,
    deadline: Int,
    deadlineAt: DateTime,
    maxTeamers: Int, // 队伍总数
    maxPerTeam: Int, // 每俱乐部限报队伍数
    formalPlayers: Int, // 每队正式棋手人数
    substitutePlayers: Int, // 每队替补棋手人数
    roundSpace: Int,
    rounds: Int,
    teamBtsss: Btsss,
    canLateMinute: Int,
    autoPairing: Boolean,
    enterShow: Option[EnterShow],
    nbTeamers: Int = 0,
    allRoundFinished: Boolean = false, // 所有轮次都结束了
    currentRound: Int = 1, // TeamRound PublishResult 之后更新此字段
    status: Status = Status.Created,
    realFinishAt: Option[DateTime] = None,
    roundRobinPairing: Option[Boolean] = None,
    meta: Option[MetaData] = None,
    createdByTeam: Option[String],
    createdBy: User.ID,
    createdAt: DateTime
) {

  def id = _id

  def fullName = s"$name${groupName.fold("") { " " + _ }}"

  def isFromPosition = variant == chess.variant.FromPosition

  def is(that: TeamContest.ID) = id == that

  def isCreated = status == Status.Created

  def isPublished = status == Status.Published

  def isEnterStopped = status == Status.EnterStopped

  def isStarted = status == Status.Started

  def isFinished = status == Status.Finished

  def isCanceled = status == Status.Canceled

  def isEnterable = isPublished

  def isOverPublished = status >= Status.Published

  def isOverEnterStopped = status >= Status.EnterStopped

  def isOverStarted = status >= Status.Started

  def isFinishedOrCanceled = status == Status.Finished || status == Status.Canceled

  def isTeamInner = typ == Type.TeamInner

  def isRoundRobin = rule == Rule.RoundRobin || rule == Rule.DBRoundRobin

  def isDbRoundRobin = rule == Rule.DBRoundRobin

  def roundRobinPairingOr = roundRobinPairing | false

  def quitable = isPublished || isEnterStopped || isStarted

  def inviteable = isPublished || ((isEnterStopped || isStarted) && !isRoundRobin)

  def inviteRemoveable = isPublished || isEnterStopped || isStarted

  def inviteProcessable = isPublished || ((isEnterStopped || isStarted) && (!isRoundRobin || (isRoundRobin && !roundRobinPairingOr)))

  def teamerKickable = isPublished || isEnterStopped || isStarted

  def teamerRemoveable = isPublished || (isEnterStopped && (isRoundRobin && !roundRobinPairingOr)) || (isStarted && (isRoundRobin && !roundRobinPairingOr))

  def canSetAutoPairing = isEnterStopped || isStarted

  def chooseable = isPublished || ((isEnterStopped || isStarted) && (!isRoundRobin || (isRoundRobin && !roundRobinPairingOr)))

  def joinProcessable = isPublished || ((isEnterStopped || isStarted) && (!isRoundRobin || (isRoundRobin && !roundRobinPairingOr)))

  def secondsToStart = (startsAt.getSeconds - nowSeconds).toInt atLeast 0

  def secondsToFinish = (finishAt.getSeconds - nowSeconds).toInt atLeast 0

  def isRecentlyFinished = isFinished && realFinishAt ?? (d => (nowSeconds - d.getSeconds) < 30 * 60)

  def isRecentlyStarted = isStarted && (nowSeconds - startsAt.getSeconds) < 15

  def shouldEnterStop = deadlineAt.getMillis <= DateTime.now.getMillis

  def shouldStart = startsAt.getMillis <= DateTime.now.getMillis

  def isNowOrSoon = startsAt.isBefore(DateTime.now plusMinutes 15) && !isFinished

  def isDistant = startsAt.isAfter(DateTime.now plusDays 1)

  def durationMillis = finishAt.getMillis - startsAt.getMillis

  def isDeadlined = DateTime.now.isBefore(deadlineAt)

  def duration = new Duration(durationMillis)

  def interval = new Interval(startsAt, duration)

  def overlaps(other: TeamContest) = interval overlaps other.interval

  def clockStatus = secondsToFinish |> { s => "%02d:%02d".format(s / 60, s % 60) }

  def isCreator(user: User) = user.id == createdBy

  def isCreator(userId: User.ID) = userId == createdBy

  def rated = mode.rated

  def enterShowOrDefault = enterShow | EnterShow.default

  def speed = Speed(clock)

  def perfType: Option[PerfType] = PerfPicker.perfType(speed, variant, none)

  def perfLens = PerfPicker.mainOrDefault(speed, variant, none)

  def isTeamerFull = nbTeamers >= maxTeamers

  def maxPlayers = formalPlayers + substitutePlayers

  def guessNbRounds = (nbTeamers - 1) atMost rounds atLeast 2

  def actualNbRounds = if (isFinished) currentRound else guessNbRounds

  def robinRoundsByTeamer =
    if (nbTeamers == 0) rounds
    else if (nbTeamers % 2 == 0) nbTeamers - 1 else nbTeamers

  def robinRoundsByTeamerOfRule =
    rule match {
      case Rule.RoundRobin => robinRoundsByTeamer
      case Rule.DBRoundRobin => robinRoundsByTeamer * 2
      case _ => -1
    }

  def actualRound =
    if (isOverEnterStopped) {
      rule match {
        case Rule.Swiss => rounds
        case Rule.RoundRobin => Math.min(robinRoundsByTeamer, rounds)
        case Rule.DBRoundRobin => Math.min(robinRoundsByTeamer * 2, rounds)
        case Rule.Dual => 1
      }
    } else rounds

  def btsss: List[TeamBtss] = {
    teamBtsss.list :+ TeamBtss.No
  }

  def roundList: List[TeamRound.No] = (1 to actualRound).toList

  def historyRoundList: List[TeamRound.No] = if (currentRound == 1) List.empty[TeamRound.No] else (1 to (currentRound - 1)).toList

  def isAllRoundFinished: Boolean = currentRound >= actualRound

  def setup = s"${clock.show} • ${if (rated) "计算等级分" else "不计算等级分"} • ${variant.name}"

  override def toString = s"$fullName $id（${typ.name}）"

}

object TeamContest {

  type ID = String

  val minTeamers = 2

  def make(
    by: User,
    name: String,
    groupName: Option[String],
    logo: Option[String],
    typ: Type,
    organizer: String,
    variant: Variant,
    position: StartingPosition,
    mode: Mode,
    teamRated: Boolean,
    clock: ClockConfig,
    rule: Rule,
    startsAt: DateTime,
    finishAt: DateTime,
    deadline: Int,
    maxTeamers: Int,
    maxPerTeam: Int,
    formalPlayers: Int,
    substitutePlayers: Int,
    roundSpace: Int,
    rounds: Int,
    canLateMinute: Int,
    autoPairing: Boolean,
    enterShow: Option[EnterShow],
    meta: Option[MetaData]
  ) = TeamContest(
    _id = Random.nextString(8),
    name = name,
    groupName = groupName,
    logo = logo,
    typ = typ,
    organizer = organizer,
    variant = variant,
    position = position,
    mode = mode,
    teamRated = teamRated,
    clock = clock,
    rule = rule,
    startsAt = startsAt,
    finishAt = finishAt,
    deadline = deadline,
    deadlineAt = startsAt.minusMinutes(deadline),
    maxTeamers = maxTeamers,
    maxPerTeam = maxPerTeam,
    formalPlayers = formalPlayers,
    substitutePlayers = substitutePlayers,
    roundSpace = roundSpace,
    rounds = rounds,
    teamBtsss = Btsss.teamer,
    canLateMinute = canLateMinute,
    autoPairing = autoPairing,
    enterShow = enterShow,
    meta = meta,
    createdByTeam = by.teamId,
    createdBy = by.id,
    createdAt = DateTime.now
  )

  sealed abstract class Status(val id: Int, val name: String) extends Ordered[Status] {
    def compare(other: Status) = Integer.compare(id, other.id)

    def is(s: Status): Boolean = this == s

    def is(f: Status.type => Status): Boolean = is(f(Status))
  }

  object Status {

    case object Created extends Status(10, "筹备中")

    case object Published extends Status(20, "报名中")

    case object EnterStopped extends Status(30, "报名截止")

    case object Started extends Status(40, "比赛中")

    case object Finished extends Status(50, "比赛结束")

    case object Canceled extends Status(60, "比赛取消")

    val all = List(Created, Published, EnterStopped, Started, Finished, Canceled)
    val byId = all map { v => (v.id, v) } toMap

    def apply(id: Int): Status = byId get id err s"Bad Status $id"

    val enter = List(Published, EnterStopped)
    val overStarted = List(Started, Finished, Canceled)
    val belong = List(Published, EnterStopped, Started /*, Finished, Canceled*/ )
    val owner = List(Created, Published, EnterStopped, Started /*, Finished, Canceled*/ )
    val finish = List(Finished, Canceled)

    val empty = (none -> "所有")

    def enterSelect = empty :: enter.map(s => (s.id.some -> s.name))

    def belongSelect = empty :: belong.map(s => (s.id.some -> s.name))

    def ownerSelect = empty :: owner.map(s => (s.id.some -> s.name))

    def finishSelect = empty :: finish.map(s => (s.id.some -> s.name))

  }

  sealed abstract class Type(val id: String, val name: String)

  object Type {

    case object TeamInner extends Type("team-inner", "俱乐部内部赛")

    case object FederationInner extends Type("federation-inner", "联盟友谊赛")

    val all = List(TeamInner, FederationInner)

    def apply(id: String) = all.find(_.id == id) err s"Bad Type $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => (r.id -> r.name) }

    def byId = all.map { x => x.id -> x }.toMap
  }

  sealed abstract class Rule(val id: String, val name: String, val setup: RuleSetup, val flow: RuleFlow)

  object Rule {

    case object Swiss extends Rule("swiss", "瑞士制",
      RuleSetup(16, 5, 20, 10, 10),
      RuleFlow(forbidden = true, roundEdit = false, roundPairing = true, manualAbsent = true, cancelPairingPublish = true, roundSwap = false))

    case object RoundRobin extends Rule("round-robin", "单循环",
      RuleSetup(21, 5, 22, 10, 10),
      RuleFlow(forbidden = false, roundEdit = true, roundPairing = false, manualAbsent = true, cancelPairingPublish = false, roundSwap = false))

    case object DBRoundRobin extends Rule("db-round-robin", "双循环",
      RuleSetup(22, 6, 12, 10, 10),
      RuleFlow(forbidden = false, roundEdit = true, roundPairing = false, manualAbsent = true, cancelPairingPublish = false, roundSwap = false))

    case object Dual extends Rule("dual", "两队对抗赛（仅一轮）",
      RuleSetup(1, 1, 2, 2, 100),
      RuleFlow(forbidden = false, roundEdit = false, roundPairing = true, manualAbsent = false, cancelPairingPublish = false, roundSwap = false))

    val all = List(Swiss, RoundRobin, DBRoundRobin, Dual)

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Rule = byId get id err s"Bad Rule $id"

    def list = all.map { r => r.id -> r.name }
  }

  case class RuleSetup(maxRound: Int, defaultRound: Int, maxTeamers: Int, defaultTeamers: Int, maxPlayers: Int)

  case class RuleFlow(forbidden: Boolean, roundEdit: Boolean, roundPairing: Boolean, manualAbsent: Boolean, cancelPairingPublish: Boolean, roundSwap: Boolean)

  sealed abstract class EnterShow(val id: String, val name: String)

  object EnterShow {

    case object Self extends EnterShow("self", "仅自己")

    case object Player extends EnterShow("player", "参赛棋手（管理员+领队+棋手）")

    case object All extends EnterShow("all", "所有人（公开）")

    def default = Player

    val all = List(Self, Player, All)

    def apply(id: String) = all.find(_.id == id) err s"Bad EnterShow $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => (r.id -> r.name) }

    def byId = all.map { x => x.id -> x }.toMap
  }

  case class MetaData(teamId: Option[String] = None, federationId: Option[String] = None)

}
