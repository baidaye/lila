package lila.teamContest

import lila.db.dsl._
import reactivemongo.bson._
import org.joda.time.DateTime

object TeamPairedRepo {

  private[teamContest] lazy val coll = Env.current.pairedColl
  import BSONHandlers.PairedHandler

  def insertMany(roundId: TeamRound.ID, paireds: List[TeamPaired]): Funit =
    coll.remove(roundQuery(roundId)) >> bulkInsert(paireds)

  def insertManyByRounds(roundIds: List[TeamRound.ID], paireds: List[TeamPaired]): Funit =
    coll.remove($doc("roundId" $in roundIds)) >> bulkInsert(paireds)

  def bulkInsert(paireds: List[TeamPaired]): Funit =
    coll.bulkInsert(
      documents = paireds.map(PairedHandler.write).toStream,
      ordered = true
    ).void

  def byId(id: TeamPaired.ID): Fu[Option[TeamPaired]] = coll.byId[TeamPaired](id)

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamPaired]] =
    coll.find(contestQuery(contestId)).list[TeamPaired]()

  def getByContestLteRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamPaired]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $lte no)).list[TeamPaired]()

  def getByContestGteRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamPaired]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $gte no)).list[TeamPaired]()

  def getByTeamer(contestId: TeamContest.ID, no: TeamRound.No, teamerId: Teamer.ID): Fu[List[TeamPaired]] =
    coll.find(
      contestQuery(contestId) ++ $or(
        $doc("whiteTeamer.id" -> teamerId),
        $doc("blackTeamer.id" -> teamerId)
      ) ++ $doc("roundNo" $lte no)
    ).sort($doc("createdAt" -> 1))
      .list[TeamPaired]()

  def getByRound(roundId: TeamRound.ID): Fu[List[TeamPaired]] =
    coll.find(roundQuery(roundId)).list[TeamPaired]()

  def allFinished(id: TeamRound.ID, paireds: Option[Int]): Fu[Boolean] =
    coll.countSel(roundQuery(id) ++ $doc("status" -> TeamPaired.Status.Finished.id)) map { count =>
      paireds.?? { _ == count }
    }

  def setStatusByRound(roundId: TeamRound.ID, status: TeamPaired.Status): Funit =
    coll.update(
      roundQuery(roundId) ++ $doc("status" -> TeamPaired.Status.Created.id),
      $set("status" -> status.id),
      multi = true
    ).void

  def setStatus(id: TeamPaired.ID, status: chess.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setAbsent(id: TeamPaired.ID, color: chess.Color): Funit =
    coll.update(
      $id(id),
      $set(s"${color.name}Player.absent" -> true)
    ).void

  def update(paired: TeamPaired): Funit =
    coll.update($id(paired.id), paired).void

  def remove(id: TeamPaired.ID): Funit = coll.remove($id(id)).void

  def removeByRound(roundId: TeamRound.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def removeByRounds(roundId: TeamRound.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def roundSwap(ids: List[TeamPaired.ID], targetRound: TeamRound): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "roundId" -> targetRound.id,
        "roundNo" -> targetRound.no,
        "startsAt" -> targetRound.actualStartsAt
      ),
      multi = true
    ).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

  def roundQuery(roundId: TeamRound.ID) = $doc("roundId" -> roundId)

}
