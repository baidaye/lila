package lila.teamContest

import lila.user.User
import org.joda.time.DateTime

case class TeamRequest(
    id: String,
    contestId: TeamContest.ID,
    contestName: String,
    status: TeamRequest.Status,
    date: DateTime
) {

}

object TeamRequest {

  type ID = String

  def make(
    contestId: String,
    contestName: String,
    userId: String,
    message: String
  ): TeamRequest = new TeamRequest(
    id = makeId(contestId, userId),
    contestId = contestId,
    contestName = contestName,
    status = Status.Created,
    date = DateTime.now
  )

  private[teamContest] def makeId(contestId: String, userId: String) = userId + "@" + contestId

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "待审核")
    case object Joined extends Status("joined", "已加入")
    case object Refused extends Status("refused", "已拒绝")

    val all = List(Created, Joined, Refused)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): Status = byId get id err s"Bad Status $id"
    def applyByAccept(accept: Boolean): Status = if (accept) Joined else Refused
  }

}

case class RequestWithUser(request: TeamRequest, user: User, mark: Option[String]) {
  def id = request.id
  def date = request.date
  def status = request.status
  def processed = status != TeamRequest.Status.Created
  def profile = user.profileOrDefault
  def markOrUsername = mark | user.username
}
