package lila.teamContest

private[teamContest] abstract class TeamPlayerBtss(val id: String, val name: String) {

  def score(boards: Boards, playerBtsss: TeamPlayerBtss.PlayerBtssScores): TeamPlayerBtss.PlayerBtssScores

}

object TeamPlayerBtss {

  case class TeamPlayerBtssScore(btss: TeamPlayerBtss, score: Double)
  case class PlayerBtssScore(player: TeamPlayer, btsss: List[TeamPlayerBtssScore] = List.empty) {
    def +(btssScore: TeamPlayerBtssScore) = copy(player, btsss :+ btssScore)
    def scoreOfBtss(btss: TeamPlayerBtss) = btsss.find(_.btss == btss).map(_.score)
  }
  case class PlayerBtssScores(players: List[PlayerBtssScore]) {

    def opponentPlayer(player: TeamPlayer, board: TeamBoard) = {
      val opponentId = board.opponentOf(player.id) err s"can not find opponent no of ${player.id}"
      (players.find(_.player.id == opponentId) err s"can not find opponent of ${player.id}").player
    }

    def sort: List[PlayerBtssScore] = players.sortWith { (playerBtssScore1, playerBtssScore2) =>
      {
        val s1 = playerBtssScore1.player.score
        val s2 = playerBtssScore2.player.score
        if (s1 > s2) true
        else if (s1 == s2) {
          playerBtssScore1.btsss.zip(playerBtssScore2.btsss).map {
            case (btssScore1, btssScore2) => {
              if (btssScore1.btss == TeamPlayerBtss.No) {
                btssScore2.score - btssScore1.score
              } else {
                btssScore1.score - btssScore2.score
              }
            }
          }.foldLeft((true, true)) { // (result, isContinue)
            case (result, diff) => {
              if (result._2) {
                if (diff > 0) {
                  (true, false)
                } else if (diff < 0) {
                  (false, false)
                } else {
                  (true, true)
                }
              } else {
                result
              }
            }
          }._1
        } else false
      }
    }
  }

  case object Opponent extends TeamPlayerBtss("opponent", "对手分") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val score =
            boards.filter(_.containsByUser(player.userId)).foldLeft(0D) {
              case (s, b) => s + playerBtssScores.opponentPlayer(player, b).score
            }
          playerBtssScore + TeamPlayerBtssScore(this, score)
        }
      )
    }
  }

  case object Mid extends TeamPlayerBtss("mid", "中间分") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val opponentScores = boards.filter(_.contains(player.id)).map { board =>
            (board, playerBtssScores.opponentPlayer(player, board).score)
          }.sortWith { (opponentScore1, opponentScore2) =>
            opponentScore1._2 < opponentScore2._2
          }

          val score = {
            if (opponentScores.size > 2) {
              // 去掉 最低分 最高分
              opponentScores.drop(1).reverse.drop(1).foldLeft(0D) {
                case (s, (_, os)) => s + os
              }
            } else 0
          }
          playerBtssScore + TeamPlayerBtssScore(this, score)
        }
      )
    }
  }

  // 索伯分（小分）
  case object Sauber extends TeamPlayerBtss("sauber", "小分") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val score =
            boards.filter(_.contains(player.id)).foldLeft(0D) {
              case (s, b) => {
                val opponent = playerBtssScores.opponentPlayer(player, b)
                if (b.isWin(player.id)) s + opponent.score // -赢过的对手分累计
                else if (b.isDraw) s + opponent.score / 2 // -和过的对手分一半
                else s
              }
            }
          playerBtssScore + TeamPlayerBtssScore(this, score)
        }
      )
    }
  }

  // 对手没走棋不算分
  case object Vict extends TeamPlayerBtss("vict", "胜局数") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val score =
            boards.filter(_.contains(player.id)).foldLeft(0D) {
              case (s, b) => {
                val opponent = playerBtssScores.opponentPlayer(player, b)
                val opponentNoStart = opponent.roundOutcome(b.roundNo).??(_ == TeamBoard.Outcome.NoStart)
                if (b.isWin(player.id) && !opponentNoStart) s + 1
                else s
              }
            }
          playerBtssScore + TeamPlayerBtssScore(this, score)
        }
      )
    }
  }

  // 胜率
  case object VictRate extends TeamPlayerBtss("victRate", "胜率") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val playerBoards = boards.filter(_.contains(player.id))
          if (playerBoards.nonEmpty) {
            // 胜率 = 得分率
            val v = player.score / playerBoards.size.toDouble
            val score = Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
            playerBtssScore + TeamPlayerBtssScore(this, score)
          } else {
            playerBtssScore + TeamPlayerBtssScore(this, 0.0)
          }
        }
      )
    }
  }

  case object OpponentAvgRating extends TeamPlayerBtss("opponentAvgRating", "对手平均等级分") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          val playerBoards = boards.filter(_.containsByUser(player.userId))
          val opponentRatings =
            playerBoards.foldLeft(0D) {
              case (s, b) => s + playerBtssScores.opponentPlayer(player, b).rating
            }
          val score = if (opponentRatings == 0) 0D else {
            Math.round(opponentRatings.toDouble / playerBoards.size.toDouble)
          }
          playerBtssScore + TeamPlayerBtssScore(this, score)
        }
      )
    }
  }

  case object No extends TeamPlayerBtss("no", "序号") {

    override def score(boards: Boards, playerBtssScores: PlayerBtssScores): PlayerBtssScores = {
      PlayerBtssScores(
        playerBtssScores.players.map { playerBtssScore =>
          val player = playerBtssScore.player
          playerBtssScore + TeamPlayerBtssScore(this, player.no)
        }
      )
    }
  }

  val all = List(Opponent, Mid, Sauber, Vict, VictRate, OpponentAvgRating, No)

  val byId = all map { b => (b.id, b) } toMap

  def apply(id: String): TeamPlayerBtss = byId get id err s"Bad TeamPlayerBtss $id"

  def list = all.map { r => (r.id -> r.name) }

  def default = List(VictRate, Vict, OpponentAvgRating, No)
}

case class TeamPlayerBtsss(list: List[TeamPlayerBtss])

object TeamPlayerBtsss {

  def default = TeamPlayerBtsss(TeamPlayerBtss.default)

  def empty = TeamPlayerBtsss(Nil)

}
