package lila.teamContest

import lila.db.{ DbImage, Photographer }
import lila.game.actorApi.BoardWithPov
import lila.game.{ Game, GameRepo }
import lila.notify.{ Notification, NotifyApi }
import lila.user.{ User, UserRepo }
import lila.hub.{ Duct, DuctMap }
import lila.hub.actorApi.calendar.{ CalendarsRemove }
import lila.hub.actorApi.contest.{ ContestBoard, MiniBoard, MiniBoardPlayer }
import lila.notify.Notification.{ Notifies, Sender }
import lila.round.actorApi.round.ContestRoundStart
import lila.team.{ Member, MemberRepo, TeamFederationMemberRepo }
import akka.actor.{ ActorSelection, ActorSystem }
import org.joda.time.DateTime
import scala.concurrent.duration._

class TeamContestApi(
    sequencers: DuctMap[_],
    renderer: ActorSelection,
    notifyApi: NotifyApi,
    roundApi: TeamRoundApi,
    roundMap: DuctMap[_],
    reminder: TeamContestReminder,
    pairingDirector: PairingDirector,
    photographer: Photographer,
    isOnline: lila.user.User.ID => Boolean
)(implicit system: ActorSystem) {

  def sequencingTest(contestId: TeamContest.ID) =
    Sequencing(contestId)(byId) { contest =>
      val now = DateTime.now()
      println("--------sequencingTest 调用开始--------", now)
      Thread.sleep(5000)
      val afterNow = DateTime.now()
      println("--------sequencingTest 调用结束--------", now, afterNow)
      funit
    }

  def byId(id: TeamContest.ID): Fu[Option[TeamContest]] = TeamContestRepo.byId(id)

  def fullContestBoard(gameId: Game.ID): Fu[Option[ContestBoard]] =
    fullBoardInfo(gameId).map {
      _.map { c =>
        ContestBoard(
          contestId = c.contest.id,
          contestFullName = c.contest.fullName,
          setup = c.contest.setup,
          isTeam = true,
          teamRated = c.contest.teamRated,
          board = MiniBoard(
            id = c.board.id,
            no = c.board.no,
            roundId = c.round.id,
            roundNo = c.round.no,
            pairedNo = c.board.pairedNo.some,
            status = c.board.status.id,
            startsAt = c.board.startsAt,
            whitePlayer = MiniBoardPlayer(
              no = c.board.whitePlayer.no,
              userId = c.board.whitePlayer.userId,
              teamerNo = c.board.whitePlayer.teamerNo.some,
              signed = c.board.whitePlayer.signed
            ),
            blackPlayer = MiniBoardPlayer(
              no = c.board.blackPlayer.no,
              userId = c.board.blackPlayer.userId,
              teamerNo = c.board.blackPlayer.teamerNo.some,
              signed = c.board.blackPlayer.signed
            )
          )
        )
      }
    }

  def fullBoardInfo(gameId: Game.ID): Fu[Option[TeamBoard.FullInfo]] =
    for {
      boardOption <- TeamBoardRepo.byId(gameId)
      roundOption <- boardOption.?? { b => TeamRoundRepo.byId(b.roundId) }
      contestOption <- boardOption.?? { b => byId(b.contestId) }
    } yield (boardOption |@| roundOption |@| contestOption).tupled map {
      case (board, round, contest) => TeamBoard.FullInfo(board, round, contest)
    }

  def roundGames(boardId: Game.ID, me: Option[User]): Fu[List[BoardWithPov]] =
    me.?? { u =>
      for {
        boardOption <- TeamBoardRepo.byId(boardId)
        contestOption <- boardOption.?? { b => byId(b.contestId) }
        teamers <- contestOption.?? { c => TeamerRepo.getByContest(c.id) }
        canView = canViewGame(contestOption, teamers, u)
        boards <- boardOption.?? { b => canView.??(TeamBoardRepo.getByRound(b.roundId)) }
        games <- canView.??(GameRepo.gamesFromSecondary(boards.map(_.id)))
      } yield boards zip games map {
        case (board, game) => BoardWithPov(MiniBoard(
          id = board.id,
          no = board.no,
          roundId = board.roundId,
          roundNo = board.roundNo,
          pairedNo = board.pairedNo.some,
          status = board.status.id,
          startsAt = board.startsAt,
          whitePlayer = MiniBoardPlayer(
            no = board.whitePlayer.no,
            userId = board.whitePlayer.userId,
            teamerNo = board.whitePlayer.teamerNo.some,
            signed = board.whitePlayer.signed
          ),
          blackPlayer = MiniBoardPlayer(
            no = board.blackPlayer.no,
            userId = board.blackPlayer.userId,
            teamerNo = board.blackPlayer.teamerNo.some,
            signed = board.blackPlayer.signed
          )
        ), lila.game.Pov.white(game))
      }
    }

  private def canViewGame(contest: Option[TeamContest], teamers: List[Teamer], user: User): Boolean = {
    contest.?? { c =>
      c.isCreator(user) || teamers.exists(_.isCreatorOrLeader(user.id))
    }
  }

  def create(contest: TeamContest, rounds: List[TeamRound]): Fu[TeamContest] = {
    TeamContestRepo.insert(contest) >> TeamRoundRepo.bulkInsert(rounds) inject contest
  }

  def update(old: TeamContest, c: TeamContest, rounds: List[TeamRound]): Funit =
    TeamContestRepo.update(
      old.copy(
        name = c.name,
        groupName = c.groupName,
        logo = c.logo,
        typ = c.typ,
        organizer = c.organizer,
        variant = c.variant,
        position = c.position,
        mode = c.mode,
        clock = c.clock,
        rule = c.rule,
        teamRated = c.teamRated,
        startsAt = c.startsAt,
        finishAt = c.finishAt,
        deadline = c.deadline,
        deadlineAt = c.deadlineAt,
        maxTeamers = c.maxTeamers, // 队伍总数
        maxPerTeam = c.maxPerTeam, // 每俱乐部限报队伍数
        formalPlayers = c.formalPlayers, // 每队正式棋手人数
        substitutePlayers = c.substitutePlayers, // 每队替补棋手人数
        roundSpace = c.roundSpace,
        rounds = c.rounds,
        canLateMinute = c.canLateMinute,
        autoPairing = c.autoPairing,
        enterShow = c.enterShow,
        meta = c.meta
      )
    ) >> TeamRoundRepo.bulkUpdate(old.id, rounds).void

  def remove(contest: TeamContest): Funit = {
    lg(contest, none, "删除比赛", none)
    TeamContestRepo.remove(contest.id) >> TeamRoundRepo.removeByContest(contest.id)
  }

  def publish(contest: TeamContest): Funit = {
    lg(contest, none, "发布比赛", none)
    TeamContestRepo.setStatus(contest.id, TeamContest.Status.Published)
  }

  def cancel(contest: TeamContest): Funit = {
    lg(contest, none, "取消比赛", none)
    TeamContestRepo.setStatus(contest.id, TeamContest.Status.Canceled) >>
      finishNotify(contest) >>
      removeCalendar(contest)
  }

  def setEnterStop(contest: TeamContest): Unit = {
    val id = contest.id
    Sequencing(id)(TeamContestRepo.publishedById) { contest =>
      if (contest.autoPairing && contest.nbTeamers < TeamContest.minTeamers) {
        lg(contest, none, "比赛取消", none)
        TeamContestRepo.setStatus(id, TeamContest.Status.Canceled)
      } else {
        lg(contest, none, "比赛报名截止", none)
        TeamContestRepo.setStatus(id, TeamContest.Status.EnterStopped) >>
          removeStagingTeamer(contest) >>- contest.autoPairing.?? {
            TeamRoundRepo.byId(TeamRound.makeId(id, 1)) flatMap {
              case None => {
                TeamContestRepo.setStatus(id, TeamContest.Status.Canceled) >> fufail(s"can not find first round of $contest")
              }
              case Some(r) => {
                pairingByRule(contest.copy(status = TeamContest.Status.EnterStopped), r, publishScoreAndFinish)
              }
            }
          }
      }
    }
  }

  private def removeStagingTeamer(contest: TeamContest) = {
    TeamerRepo.getUnJoinedByContest(contest.id).flatMap { teamers =>
      teamers.nonEmpty.?? {
        lg(contest, none, "比赛报名截止 - 移除临时队伍", s"${teamers}".some)
        TeamerRepo.removeByIds(teamers.map(_.id)) >>
          TeamPlayerRepo.removeByTeamers(teamers.map(_.id))
      }
    }
  }

  def setAutoPairing(contest: TeamContest, auto: Boolean): Funit = {
    lg(contest, none, "比赛自动设置", s"Auto：$auto".some)
    TeamContestRepo.setAutoPairing(contest.id, auto) flatMap { _ =>
      if (auto) {
        TeamRoundRepo.byId(TeamRound.makeId(contest.id, contest.currentRound)).flatMap { roundOption =>
          roundOption.?? { round =>
            val c = contest.copy(autoPairing = true)
            round.status match {
              case TeamRound.Status.Created => (c.isEnterStopped || c.isStarted).?? { delayNextRound(c, round).map { nr => pairingByRule(c, nr, publishScoreAndFinish) } }
              case TeamRound.Status.Pairing => (c.isEnterStopped || c.isStarted).?? { delayNextRound(c, round).map { nr => roundApi.publish(c, nr) } }
              case TeamRound.Status.Published | TeamRound.Status.Started => funit
              case TeamRound.Status.Finished => c.isStarted.?? {
                roundApi.publishResult(c, round.id, round.no).flatMap { nc =>
                  if (nc.allRoundFinished) {
                    publishScoreAndFinish(contest)
                  } else toNextRound(nc, c.currentRound + 1)
                }
              }
              case TeamRound.Status.PublishResult => c.isStarted.?? {
                if (c.allRoundFinished) {
                  publishScoreAndFinish(c)
                } else toNextRound(c, c.currentRound + 1)
              }
            }
          }
        }
      } else funit
    }
  }

  def enterStop: Funit =
    TeamContestRepo.published map { contests =>
      contests foreach { contest =>
        if (contest.shouldEnterStop) {
          setEnterStop(contest)
        }
      }
    }

  def start: Funit =
    TeamContestRepo.enterStopped map { contests =>
      contests foreach { contest =>
        if (contest.shouldStart) {
          setStart(contest.id)
        }
      }
    }

  def setStart(id: TeamContest.ID): Unit =
    Sequencing(id)(TeamContestRepo.enterStoppedById) { contest =>
      lg(contest, none, "比赛开始", none)
      TeamContestRepo.setStatus(id, TeamContest.Status.Started)
    }

  def launch: Funit = {
    launchBoards.flatMap { list =>
      val gameIds = list.filter(_.canStarted).map(_.board.id)
      gameIds.nonEmpty.?? {
        val now = DateTime.now
        for {
          _ <- GameRepo.gameStartBatch(gameIds, now)
          _ <- TeamBoardRepo.gameStartBatch(gameIds, now)
        } yield {
          gameIds.foreach { gameId =>
            roundMap.tell(gameId, ContestRoundStart(now))
          }
        }
      }
    }
  }

  private def launchBoards: Fu[List[TeamBoard.FullInfo]] = for {
    boards ← TeamBoardRepo.pending
    rounds ← boards.nonEmpty ?? { TeamRoundRepo.byOrderedIds(boards.map(_.roundId)) }
    contests <- boards.nonEmpty ?? { TeamContestRepo.byOrderedIds(boards.map(_.contestId)) }
  } yield boards zip rounds zip contests map {
    case ((board, round), contest) => TeamBoard.FullInfo(board, round, contest)
  }

  def remind: Funit =
    remindBoards map { list =>
      list.foreach { info =>
        info.board.players.foreach { player =>
          if (!info.contest.isFinishedOrCanceled && (info.round.isPublished || info.round.isStarted)) {
            TeamBoardRepo.setReminded(info.board.id) >>-
              reminder(
                info,
                player.userId
              )
          }
        }
      }
    }

  private def remindBoards: Fu[List[TeamBoard.FullInfo]] = for {
    boards ← TeamBoardRepo.remindAtSoon
    rounds ← boards.nonEmpty ?? { TeamRoundRepo.byOrderedIds(boards.map(_.roundId)) }
    contests <- boards.nonEmpty ?? { TeamContestRepo.byOrderedIds(boards.map(_.contestId)) }
  } yield boards zip rounds zip contests map {
    case ((board, round), contest) => TeamBoard.FullInfo(board, round, contest)
  }

  def finishGame(game: Game) =
    if (game.contestIsTeamOrFalse) {
      game.contestId.?? { id =>
        Sequencing(id)(byId) { contest =>
          val wp = game.whitePlayer.userId err s"contest game miss player userId $game"
          val bp = game.blackPlayer.userId err s"contest game miss player userId $game"
          TeamBoardRepo.byId(game.id) flatMap {
            _.?? { board =>
              lg(contest, None, "对局结束", s"第${board.roundNo}轮 - #${board.pairedNo}.${board.no} ${board.whitePlayer.userId} Vs. ${board.blackPlayer.userId}".some)
              val currentRoundNo = contest.currentRound
              val currentRoundId = TeamRound.makeId(contest.id, currentRoundNo)

              val result = boardResult(contest, game, board)
              val whiteOutcome = roundApi.whitePlayerOutcome(result)
              val blackOutcome = roundApi.blackPlayerOutcome(result)
              val winner = roundApi.boardWinner(result)

              (for {
                _ <- TeamBoardRepo.finishGame(game, winner, result)
                _ <- TeamPlayerRepo.finishGame(contest, board.roundNo, wp, whiteOutcome)
                _ <- TeamPlayerRepo.finishGame(contest, board.roundNo, bp, blackOutcome)
              } yield ()) >> {
                TeamRoundRepo.byId(currentRoundId) flatMap {
                  _.?? { round =>
                    TeamBoardRepo.onePairedFinished(board.pairedId, contest.formalPlayers) flatMap { isOnePairedFinished =>
                      isOnePairedFinished.?? {
                        roundApi.pairedFinish(contest, board.pairedId) >> {
                          TeamPairedRepo.allFinished(round.id, round.paireds) flatMap { isAllPairedFinished =>
                            isAllPairedFinished.?? {
                              TeamRoundRepo.finish(currentRoundId) >> {
                                {
                                  if (isAllPairedFinished && contest.autoPairing) {
                                    roundApi.publishResult(contest, currentRoundId, currentRoundNo)
                                  } else fuccess(contest)
                                } flatMap { newContest =>
                                  toNextRound(newContest, currentRoundNo + 1) >>
                                    (newContest.allRoundFinished && newContest.autoPairing).?? {
                                      publishScoreAndFinish(newContest)
                                    }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              /*
              for {
                roundOption <- TeamRoundRepo.byId(currentRoundId)
                _ <- TeamBoardRepo.finishGame(game, winner, result)
                _ <- TeamPlayerRepo.finishGame(contest, board.roundNo, wp, whiteOutcome)
                _ <- TeamPlayerRepo.finishGame(contest, board.roundNo, bp, blackOutcome)
                isOnePairedFinished <- TeamBoardRepo.onePairedFinished(board.pairedId, contest.formalPlayers)
                _ <- isOnePairedFinished.?? {
                  roundApi.pairedFinish(contest, board.pairedId)
                }
                isAllPairedFinished <- roundOption.?? { round => TeamPairedRepo.allFinished(round.id, round.paireds) }
                _ <- isAllPairedFinished.?? {
                  TeamRoundRepo.finish(currentRoundId)
                }
                contest <- if (isAllPairedFinished && contest.autoPairing) {
                  roundApi.publishResult(contest, currentRoundId, currentRoundNo)
                } else fuccess(contest)
                _ <- (contest.allRoundFinished && contest.autoPairing).?? {
                  lila.common.Future.delay(3 seconds) {
                    publishScoreAndFinish(contest)
                  }
                }
                res <- isAllPairedFinished.?? {
                  toNextRound(contest, currentRoundNo + 1)
                }
              } yield res*/
            }
          }
        }
      }
    }

  private def boardResult(contest: TeamContest, game: Game, board: TeamBoard): TeamBoard.Result = {
    val whiteSigned = board.whitePlayer.signed
    val blackSigned = board.blackPlayer.signed
    val whiteAbsent = board.whitePlayer.absent | false
    val blackAbsent = board.blackPlayer.absent | false

    if (whiteSigned && blackSigned) {
      game.winnerColor.map {
        case chess.Color.White => TeamBoard.Result.WhiteWin
        case chess.Color.Black => TeamBoard.Result.BlackWin
      } | TeamBoard.Result.Draw
    } else if ((whiteSigned && !blackSigned) || (contest.isRoundRobin && blackAbsent && !whiteAbsent)) {
      TeamBoard.Result.BlackAbsent
    } else if ((!whiteSigned && blackSigned) || (contest.isRoundRobin && whiteAbsent && !blackAbsent)) {
      TeamBoard.Result.WhiteAbsent
    } else if ((!whiteSigned && !blackSigned) || (contest.isRoundRobin && whiteAbsent && blackAbsent)) {
      TeamBoard.Result.AllAbsent
    } else {
      logger.error(s"can not apply board result $contest ${board.id}")
      TeamBoard.Result.AllAbsent
    }
  }

  private def toNextRound(contest: TeamContest, no: TeamRound.No) =
    (!contest.isFinishedOrCanceled && !contest.allRoundFinished).?? {
      nextRound(contest, no, publishScoreAndFinish)
    }

  private def nextRound(contest: TeamContest, no: TeamRound.No, publishScoreAndFinish: TeamContest => Funit): Funit = {
    contest.autoPairing.?? {
      TeamRoundRepo.byId(TeamRound.makeId(contest.id, no)).flatMap { roundOption =>
        roundOption.?? { r =>
          lg(contest, r.some, "进入下一轮", none)
          delayNextRound(contest, r) map { nr =>
            pairingByRule(contest, nr, publishScoreAndFinish)
          }
        }
      }
    }
  }

  private def pairingByRule(contest: TeamContest, round: TeamRound, publishScoreAndFinish: TeamContest => Funit): Funit =
    contest.rule match {
      case TeamContest.Rule.RoundRobin | TeamContest.Rule.DBRoundRobin => {
        if (round.isFirstRound()) {
          pairingRoundRobin(contest).void
        } else {
          contest.autoPairing.?? {
            roundApi.publish(contest, round)
          }
        }
      }
      case TeamContest.Rule.Swiss | TeamContest.Rule.Dual => {
        roundApi.pairing(contest, round, publishScoreAndFinish).void
      }
    }

  private def delayNextRound(contest: TeamContest, round: TeamRound): Fu[TeamRound] = {
    val now = DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0)
    if (round.actualStartsAt.isBefore(now.plusMinutes(TeamRound.beforeStartMinutes))) {
      lg(contest, round.some, "轮次推迟执行", none)
      val st = now.plusMinutes(TeamRound.beforeStartMinutes)
      TeamRoundRepo.setStartsTime(round.id, st) >> {
        TeamBoardRepo.setStartsTimes(round.id, st)
      } inject round.copy(actualStartsAt = st)
    } else fuccess(round)
  }

  def publishScoreAndFinish(contest: TeamContest): Funit = {
    lg(contest, none, "比赛结束", none)
    for {
      _ <- computeAllScoreWithoutCancelled(contest)
      _ <- computeAllPlayerScoreWithoutCancelled(contest)
      _ <- TeamContestRepo.finish(contest)
    } yield finishNotify(contest)
  }

  private def computeAllScoreWithoutCancelled(contest: TeamContest): Funit = {
    TeamScoreSheetRepo.getByContest(contest.id) flatMap { scoreSheets =>
      scoreSheets.exists(_.cancelled).?? {
        scoreSheets.groupBy(_.roundNo).map {
          case (no, list) => {
            val lst = list.map(d => d.teamerNo -> d.rank)
            lg(contest, none, "计算成绩", s"第${no}轮：$lst".some)
            list.filterNot(_.cancelled).zipWithIndex.map {
              case (s, i) => TeamScoreSheetRepo.setRank(s.id, i + 1)
            }.sequenceFu.void
          }
        }.sequenceFu.void >> TeamScoreSheetRepo.setCancelledRank(contest.id)
      }
    }
  }

  private def computeAllPlayerScoreWithoutCancelled(contest: TeamContest): Funit = {
    TeamPlayerScoreSheetRepo.getByContest(contest.id) flatMap { scoreSheets =>
      scoreSheets.exists(_.cancelled).?? {
        scoreSheets.groupBy(_.roundNo).map {
          case (no, list) => {
            val lst = list.map(d => d.playerUid -> d.rank)
            lg(contest, none, "计算成绩", s"第${no}轮：$lst".some)
            list.filterNot(_.cancelled).zipWithIndex.map {
              case (s, i) => TeamPlayerScoreSheetRepo.setRank(s.id, i + 1)
            }.sequenceFu.void
          }
        }.sequenceFu.void >> TeamPlayerScoreSheetRepo.setCancelledRank(contest.id)
      }
    }
  }

  def cancelScore(contest: TeamContest, scoreSheet: TeamScoreSheet): Funit = {
    val teamerId = scoreSheet.teamerId
    lg(contest, none, "取消比赛成绩", s"队伍：${teamerId}".some)
    TeamScoreSheetRepo.setCancelScore(teamerId) >>
      TeamerRepo.setCancelScore(teamerId) >>
      TeamPlayerScoreSheetRepo.setCancelScore(teamerId) >>
      TeamPlayerRepo.setCancelScore(teamerId)
  }

  def setRoundRobin(contest: TeamContest, list: List[DateTime]): Funit = {
    lg(contest, none, "循环赛设置", s"Rounds：${list.size}".some)
    val rounds = list.zipWithIndex map {
      case (startsAt, i) => TeamRound.make(
        no = i + 1,
        contestId = contest.id,
        startsAt = startsAt
      )
    }

    TeamContestRepo.setRounds(contest.id, rounds.size) >>
      TeamRoundRepo.bulkUpdate(contest.id, rounds)
  }

  def pairingRoundRobin(contest: TeamContest): Fu[Boolean] = {
    lg(contest, none, "循环赛编排", none)
    (for {
      rounds <- TeamRoundRepo.getByContest(contest.id)
      teamers <- TeamerRepo.getByContest(contest.id)
      players <- TeamPlayerRepo.getFormalByContest(contest.id)
    } yield (rounds, teamers, players)) flatMap {
      case (rounds, teamers, players) => {
        val firstRound = rounds.minBy(_.no)
        pairingDirector.pairingAll(contest, rounds.take(contest.actualRound), teamers, players).flatMap {
          case None => {
            lg(contest, none, "循环赛编排失败", none, true)
            roundApi.pairingFailed(contest, firstRound, publishScoreAndFinish).inject(false)
          }
          case Some(paireds) => {
            if (paireds.isEmpty) {
              lg(contest, none, "循环赛编排失败", "boards.isEmpty -> force finished!".some, true)
              roundApi.pairingFailed(contest, firstRound, publishScoreAndFinish).inject(false)
            } else {
              val bs = paireds.map(_.teamerNos)
              lg(contest, none, "循环赛编排完成", s"$bs".some)
              TeamContestRepo.setRoundRobinPairing(contest.id) >> contest.autoPairing.?? {
                TeamPlayerRepo.getByContest(contest.id) flatMap { players =>
                  roundApi.autoPublish(contest, firstRound)
                }
              }.inject(true)
            }
          }
        }
      }
    }
  }

  def joinRequest(contest: TeamContest, old: Option[Teamer], data: JoinSetup, me: User, playerUsers: List[User]): Funit = {
    lg(contest, none, "报名申请", s"队伍：${data.name} - ${me.id}".some)
    val teamer = old.map {
      _.copy(
        name = data.name,
        leaders = data.leadersList,
        status = if (data.staging) Teamer.Status.Staging else Teamer.Status.Sent
      )
    } | Teamer.make(
      no = -1,
      contestId = contest.id,
      teamId = me.teamIdValue,
      teamOwner = me.id,
      name = data.name,
      leaders = data.leadersList,
      staging = data.staging,
      createdBy = me.id
    )

    val players = data.players.map { p =>
      val user = playerUsers.find(_.id == p.userId) err s"can not find user ${p.userId}"
      TeamPlayer.make(
        contestId = contest.id,
        no = p.no,
        user = user,
        teamerId = teamer.id,
        perfLens = contest.perfLens,
        teamRating = p.teamRating,
        formal = p.formal
      )
    }

    TeamerRepo.upsert(teamer) >>
      TeamPlayerRepo.bulkUpdate(teamer.id, players) >>
      ((contest.isTeamInner || contest.isCreator(me)) && !data.staging).?? {
        processJoin(contest, teamer, true)
      }
  }

  def processJoin(contest: TeamContest, teamer: Teamer, accept: Boolean): Funit = {
    lg(contest, none, "报名比赛处理", s"队伍：${teamer.name} - ${teamer.id}，Accept: $accept".some)
    if (accept) {
      TeamerRepo.findNextNo(contest.id) flatMap { no =>
        TeamerRepo.setJoined(teamer.id, no, Teamer.Status.Joined) >>
          TeamPlayerRepo.setStatus(teamer.id, Teamer.Status.Joined) >>
          TeamContestRepo.incTeamers(contest.id, +1) >>
          acceptNotify(contest, teamer)
      }
    } else {
      TeamerRepo.setStatus(teamer.id, Teamer.Status.Refused) >>
        TeamPlayerRepo.setStatus(teamer.id, Teamer.Status.Refused)
    }
  }

  def quit(contest: TeamContest, round: TeamRound, teamer: Teamer, me: User): Funit = {
    lg(contest, round.some, "队伍退赛", s"队伍：$teamer 操作者：${me.id}".some)
    TeamerRepo.quit(teamer.id)
  }

  def quitByUser(userId: User.ID): Funit =
    TeamerRepo.getByUserIdNear1Year(userId) flatMap { teamers =>
      TeamContestRepo.byIds(teamers.map(_.contestId).distinct) flatMap { contests =>
        val quitableContestIds = contests.filter(_.quitable).map(_.id)
        val quitableTeamers = teamers.filter(t => quitableContestIds.contains(t.contestId))

        logger.info(s"用户注销，队伍退赛。用户：$userId，队伍：$quitableTeamers")
        TeamerRepo.quitAll(quitableTeamers.map(_.teamId))
      }
    }

  def toggleKickPlayer(contest: TeamContest, round: TeamRound, teamer: Teamer): Funit =
    teamer.absentOr match {
      case true => {
        lg(contest, round.some, "队伍恢复比赛", s"队伍：$teamer，state：[absent-${teamer.absent},quit-${teamer.quit},kick-${teamer.kick},manualAbsent-${teamer.manualAbsent}]".some)
        TeamerRepo.unkick(teamer.id, teamer.manualAbsent)
      }
      case false => {
        lg(contest, round.some, "队伍禁止比赛", s"队伍：$teamer，state：[absent-${teamer.absent},quit-${teamer.quit},kick-${teamer.kick},manualAbsent-${teamer.manualAbsent}]".some)
        TeamerRepo.kick(teamer.id)
      }
    }

  def reorderTeamer(teamerIds: List[String]): Funit =
    TeamerRepo.byOrderedIds(teamerIds) flatMap { teamers =>
      teamers.zipWithIndex.map {
        case (p, i) => TeamerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  private def reorderTeamer(contest: TeamContest): Funit =
    TeamerRepo.getJoinedByContest(contest.id) flatMap { teamers =>
      teamers.zipWithIndex.map {
        case (p, i) => TeamerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  def removeTeamer(contest: TeamContest, teamer: Teamer): Funit = {
    lg(contest, none, "移除队伍", s"队伍：${teamer.name} - ${teamer.id}".some)
    TeamerRepo.remove(teamer.id) >>
      TeamPlayerRepo.removeByTeamer(teamer.id) >>
      teamer.joined.?? {
        reorderTeamer(contest) >> TeamContestRepo.incTeamers(contest.id, -1)
      }
  }

  def playersWithUsers(contest: TeamContest, markMap: Map[String, Option[String]], me: User): Fu[List[PlayerWithUser]] = for {
    teamers ← TeamerRepo.getByContest(contest.id)
    players ← TeamPlayerRepo.getByContest(contest.id)
    users ← UserRepo usersFromSecondary players.map(_.userId)
    members ← contest.typ match {
      case TeamContest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(contest.organizer, players.map(_.userId))
      case TeamContest.Type.FederationInner => {
        val memberIds = players.map { player =>
          val teamer = teamers.find(_.id == player.teamerId) err s"can not find teamer ${player.teamerId}"
          Member.makeId(teamer.teamId, player.userId)
        }
        MemberRepo.memberOptionByOrderedIds(memberIds)
      }
    }
  } yield players zip users zip members map {
    case ((player, user), member) => {
      val mark = markOption(user.id, markMap)
      PlayerWithUser(player, user, member, mark)
    }
  }

  def allPlayersWithUsers(contest: TeamContest, markMap: Map[String, Option[String]], me: User): Fu[List[AllPlayerWithUser]] = for {
    members ← MemberRepo.memberByTeam(me.teamIdValue)
    users ← UserRepo usersFromSecondary members.map(_.user)
  } yield users zip members map {
    case (user, member) => {
      val mark = markOption(user.id, markMap)
      AllPlayerWithUser(user, member.some, mark)
    }
  }

  def teamerPlayersWithUsers(contest: TeamContest, teamer: Teamer, markMap: Map[String, Option[String]], me: User): Fu[List[PlayerWithUser]] = for {
    players ← TeamPlayerRepo.getByTeamer(teamer.id)
    users ← UserRepo usersFromSecondary players.map(_.userId)
    members ← contest.typ match {
      case TeamContest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(contest.organizer, players.map(_.userId))
      case TeamContest.Type.FederationInner => {
        val memberIds = players.map { player =>
          Member.makeId(teamer.teamId, player.userId)
        }
        MemberRepo.memberOptionByOrderedIds(memberIds)
      }
    }
  } yield players zip users zip members map {
    case ((player, user), member) => {
      val mark = markOption(user.id, markMap)
      PlayerWithUser(player, user, member, mark)
    }
  }

  def myTeamerPlayers(contest: TeamContest, me: User): Fu[List[TeamPlayer]] =
    TeamerRepo.getByUserId(contest.id, me.id) flatMap { teamers =>
      TeamPlayerRepo.getByTeamers(teamers.map(_.id))
    }

  private def markOption(userId: User.ID, markMap: Map[String, Option[String]]): Option[String] = {
    markMap.get(userId).fold(none[String]) { m => m }
  }

  private def Sequencing(contestId: TeamContest.ID)(fetch: TeamContest.ID => Fu[Option[TeamContest]])(run: TeamContest => Funit): Unit =
    doSequence(contestId) {
      fetch(contestId) flatMap {
        case Some(t) => run(t)
        case None => fufail(s"Can't run sequenced operation on missing contest $contestId")
      }
    }

  private def doSequence(contestId: TeamContest.ID)(fu: => Funit): Unit =
    sequencers.tell(contestId, Duct.extra.LazyFu(() => fu))

  def uploadPicture(id: String, picture: Photographer.Uploaded, processFile: Boolean = false): Fu[DbImage] =
    photographer(id, picture, processFile)

  private def finishNotify(contest: TeamContest): Funit = {
    def makeNotify(userId: User.ID) =
      Notification.make(
        Sender(User.lichessId).some,
        Notifies(userId),
        lila.notify.GenericLink(
          url = s"/contest/team/${contest.id}",
          title = "团体赛结束".some,
          text = s"团体赛【${contest.fullName}】已经结束".some,
          icon = "赛"
        )
      )

    for {
      teamers <- TeamerRepo.getByContest(contest.id)
      players <- TeamPlayerRepo.getByContest(contest.id)
    } yield {
      val userIds =
        (contest.createdBy +: teamers.flatMap { teamer =>
          teamer.leaders :+ teamer.createdBy
        }) ++ players.map { player =>
          player.userId
        }

      userIds.distinct.foreach { userId =>
        notifyApi.addNotification(makeNotify(userId))
      }
    }
  }

  private def acceptNotify(contest: TeamContest, teamer: Teamer): Funit = {
    TeamPlayerRepo.getByTeamer(teamer.id).flatMap { players =>
      (!contest.isTeamInner && contest.createdBy != teamer.createdBy).?? {
        notifyApi.addNotification(Notification.make(
          Sender(User.lichessId).some,
          Notifies(teamer.createdBy),
          lila.notify.GenericLink(
            url = s"/contest/team/${contest.id}",
            title = "参赛报名通过".some,
            text = s"已经通过【${contest.fullName}】的参赛申请，请组织棋手准时参加比赛。".some,
            icon = "赛"
          )
        ))
      } >> {
        (players.map(_.userId) ++ teamer.leaders).distinct.filterNot(p => p == teamer.createdBy).map { player =>
          notifyApi.addNotification(Notification.make(
            Sender(User.lichessId).some,
            Notifies(player),
            lila.notify.GenericLink(
              url = s"/contest/team/${contest.id}",
              title = "团体赛参赛提醒".some,
              text = s"您的俱乐部已报名参加【${contest.fullName}】团体赛，请准时参加比赛。".some,
              icon = "赛"
            )
          ))
        }.sequenceFu.void
      }
    }
  }

  private def removeCalendar(contest: TeamContest): Funit = {
    TeamRoundRepo.getByContest(contest.id) flatMap { rounds =>
      val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round ${contest.id}-${contest.currentRound}"
      round.isPublished.?? {
        for {
          teamers <- TeamerRepo.getByContest(contest.id)
          boards <- TeamBoardRepo.getByRound(round.id)
        } yield {
          val creatorId = List(s"${round.id}@${contest.createdBy}")
          val boardIds = boards.flatMap { board => board.players.map { player => s"${board.id}@${player.userId}" } }
          val teamerIds = teamers.flatMap { teamer => teamer.leaders.map { leader => s"${round.id}@$leader" } }
          val teamerCreatorId = teamers.map { teamer => s"${round.id}@${teamer.createdBy}" }
          val ids = creatorId ++ boardIds ++ teamerIds ++ teamerCreatorId
          system.lilaBus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
        }
      }
    }
  }

  private def lg(contest: TeamContest, round: Option[TeamRound], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
