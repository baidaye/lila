package lila.teamContest

import play.api.data._
import play.api.data.Forms._
import chess.variant._
import chess.format.Forsyth
import chess.{ Mode, StartingPosition }
import lila.common.Form._
import lila.user.User
import lila.game.Game
import lila.teamContest.TeamContest.Rule
import org.joda.time.{ DateTime, Period, PeriodType }
import ContestSetup._

final class DataForm(val captcher: akka.actor.ActorSelection) extends lila.hub.CaptchedForm {

  import DataForm._

  def contest(user: User, id: Option[TeamContest.ID], rule: Rule) = Form(mapping(
    "basics" -> mapping(
      "name" -> nonEmptyText(minLength = 2, maxLength = 30),
      "groupName" -> optional(nonEmptyText(minLength = 2, maxLength = 30)),
      "logo" -> optional(text(minLength = 5, maxLength = 150)),
      "typ" -> stringIn(TeamContest.Type.list),
      "organizer" -> nonEmptyText(minLength = 6, maxLength = 8),
      "variant" -> text.verifying(v => guessVariant(v).isDefined),
      "position" -> nonEmptyText.verifying("Fen格式错误", validFen _),
      "rated" -> boolean,
      "teamRated" -> boolean,
      "clockTime" -> numberInDouble(clockTimeChoices),
      "clockIncrement" -> numberIn(clockIncrementChoices),
      "rule" -> stringIn(TeamContest.Rule.list),
      "startsAt" -> futureDateTime,
      "finishAt" -> futureDateTime
    )(Basics.apply)(Basics.unapply),
    "conditions" -> mapping(
      "deadline" -> numberIn(deadlineMinuteChoices),
      "enterShow" -> stringIn(TeamContest.EnterShow.list),
      "maxTeamers" -> number(min = TeamContest.minTeamers, max = rule.setup.maxTeamers),
      "maxPerTeam" -> number(min = 1, max = rule.setup.maxTeamers),
      "formalPlayers" -> number(min = 2, max = rule.setup.maxPlayers),
      "substitutePlayers" -> number(min = 1, max = rule.setup.maxPlayers),
      "canLateMinute" -> number(min = 1, max = 30)
    )(Conditions.apply)(Conditions.unapply),
    "rounds" -> mapping(
      "spaceDay" -> numberIn(roundSpaceDayChoices),
      "spaceHour" -> numberIn(roundSpaceHourChoices),
      "spaceMinute" -> numberIn(roundSpaceMinuteChoices),
      "rounds" -> number(min = 1, max = rule.setup.maxRound).verifying("双循环赛制轮次应为偶数", validDbRoundRobinNumber(_, rule)),
      "list" -> list(mapping(
        "startsAt" -> futureDateTime
      )(RoundSetup.apply)(RoundSetup.unapply))
    )(Rounds.apply)(Rounds.unapply)
  )(ContestSetup.apply)(ContestSetup.unapply)
    .verifying("比赛名称重复", !_.validName(id).awaitSeconds(2))
    .verifying("无效的时钟", _.validClock)
    .verifying("请上调时钟或下调允许迟到时间", _.validCanLateTime)
    .verifying("无效的比赛起始时间", _.validFinishAt)
    .verifying(s"至少1个轮次，并且不大于${rule.setup.maxRound}个轮次", _.validRoundNumber)
    .verifying("需要重新生成各轮次时间", _.validRoundSetup)
    .verifying("轮次间隔时间至少1分钟", _.validRoundSpace)
    .verifying("无效的轮次开始时间", _.validRoundStartsAt)
    .verifying("混乱的轮次开始时间", _.validRoundStartsAtBetween))

  def contestDefault(user: User, rule: Rule) = contest(user, None, rule) fill ContestSetup.default(rule)

  def contestOf(user: User, c: TeamContest, rounds: List[TeamRound]) =
    contest(user, c.id.some, c.rule) fill ContestSetup(
      basics = Basics(
        name = c.name,
        groupName = c.groupName,
        logo = c.logo,
        typ = c.typ.id,
        organizer = c.organizer,
        variant = c.variant.id.toString,
        position = c.position.fen,
        rated = c.mode.id == 1,
        teamRated = c.teamRated,
        clockTime = c.clock.limitInMinutes,
        clockIncrement = c.clock.incrementSeconds,
        rule = c.rule.id,
        startsAt = c.startsAt,
        finishAt = c.finishAt
      ),
      conditions = Conditions(
        deadline = c.deadline,
        enterShow = c.enterShow.map(_.id) | TeamContest.EnterShow.default.id,
        maxTeamers = c.maxTeamers, // 队伍总数
        maxPerTeam = c.maxPerTeam, // 每俱乐部限报队伍数
        formalPlayers = c.formalPlayers, // 每队正式棋手人数
        substitutePlayers = c.substitutePlayers, // 每队替补棋手人数
        canLateMinute = c.canLateMinute
      ),
      rounds = Rounds(
        spaceDay = c.roundSpace / (24 * 60),
        spaceHour = c.roundSpace % (24 * 60) / 60,
        spaceMinute = c.roundSpace % (24 * 60) % 60,
        rounds = c.rounds,
        list = rounds.map(r => RoundSetup(r.startsAt))
      )
    )

  def joinForm(contest: TeamContest, teamerId: Option[Teamer.ID] = None) = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 20),
    "leaderNames" -> nonEmptyText,
    "leaders" -> nonEmptyText.verifying("领队数量1-2人", validLeaders _),
    "players" -> list(mapping(
      "no" -> number(min = 1, max = contest.maxPlayers),
      "userId" -> lila.user.DataForm.historicalUsernameField,
      "formal" -> boolean,
      "rating" -> number,
      "teamRating" -> optional(number)
    )(JoinPlayer.apply)(JoinPlayer.unapply)),
    "staging" -> boolean
  )(JoinSetup.apply)(JoinSetup.unapply)
    .verifying("队名已经存在", !_.validName(contest.id, teamerId).awaitSeconds(2))
    .verifying("正式棋手数量错误", _.validFormal(contest))
    .verifying("替补棋手数量错误", _.validSubstitute(contest)))

  def joinFormOf(contest: TeamContest, teamer: Option[Teamer], markMap: Map[String, Option[String]], leaderUsers: List[User], me: User) =
    teamer.map { t =>
      joinForm(contest, t.id.some) fill JoinSetup(
        name = t.name,
        leaderNames = leaderNames(markMap, leaderUsers),
        leaders = t.leaders.mkString(","),
        players = Nil,
        staging = false
      )
    } | (
      joinForm(contest) fill JoinSetup(
        name = s"${me.username} 的队伍",
        leaderNames = me.username,
        leaders = me.id,
        players = Nil,
        staging = false
      )
    )

  private def leaderNames(markMap: Map[String, Option[String]], leaderUsers: List[User]) = {
    leaderUsers.map { leaderUser =>
      markOrUsername(markMap, leaderUser)
    }.mkString(",")
  }

  private def markOrUsername(markMap: Map[String, Option[String]], leaderUser: User): String = {
    val mark = markMap.get(leaderUser.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | leaderUser.realNameOrUsername
  }

  private def validLeaders(leaders: String) = {
    val list = if (leaders.trim.isEmpty) List.empty[String] else leaders.trim.split(",").toList
    list.nonEmpty && list.size <= 2
  }

  def roundRobinForm(contest: TeamContest) = Form(mapping(
    "list" -> list(futureDateTime)
  )(RoundRobinData.apply)(RoundRobinData.unapply)
    .verifying("双循环赛制轮次应为偶数", _.validRoundNum(contest))
    .verifying("轮次开始时间，必须与轮次顺序保持一致", _.validOrder)
    .verifying("当前报名队伍中存在“禁赛”或“退赛”状态，您可以“移除”或者“恢复”禁赛队伍。", d => existsQuitOrKick(contest.id).awaitSeconds(2)))

  private def existsQuitOrKick(contestId: TeamContest.ID) =
    TeamerRepo.getByContest(contestId).map {
      !_.exists(_.quitOrKick)
    }

  def forbidden = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 20),
    "teamerIds" -> nonEmptyText(minLength = 2, maxLength = 600)
  )(ForbiddenData.apply)(ForbiddenData.unapply))

  def forbiddenOf(f: Forbidden) = forbidden fill ForbiddenData(f.name, f.teamerIds.mkString(","))

}

object DataForm {

  val clockTimes: Seq[Double] = Seq(3d, 5d) ++ (10d to 30d by 5d) ++ (40d to 60d by 10d)
  val clockTimeDefault = 5d

  private def formatLimit(l: Double) =
    chess.Clock.Config(l * 60 toInt, 0).limitString + {
      if (l <= 1) " 分钟" else " 分钟"
    }

  val clockTimeChoices = optionsDouble(clockTimes, formatLimit)

  val clockIncrements = (0 to 2 by 1) ++ Seq(3, 5) ++ (10 to 30 by 5) ++ (40 to 60 by 10)
  val clockIncrementDefault = 0
  val clockIncrementChoices = options(clockIncrements, "%d 秒")
  val apptDeadlineMinuteChoices = Seq((10 -> "下轮开始前10分钟"), (30 -> "下轮开始前30分钟"), (60 -> "下轮开始前1小时"), (60 * 24 -> "下轮开始前24小时"))
  val deadlineMinuteChoices = Seq( /*(1 -> "比赛开始前1分钟"),*/ (3 -> "比赛开始前3分钟"), (5 -> "比赛开始前5分钟"), (30 -> "比赛开始前30分钟"), (60 -> "比赛开始前1小时"), (60 * 24 -> "比赛开始前1天"), (60 * 24 * 3 -> "比赛开始前3天"))
  val booleanChoices = Seq((0 -> "否"), (1 -> "是"))
  val canLateMinuteChoices = Seq((1 -> "1 分钟"), (3 -> "3 分钟"), (5 -> "5 分钟"), (10 -> "10 分钟"), (20 -> "20 分钟"), (30 -> "30 分钟"))
  val roundSpaceDayChoices = Seq((0 -> "0 天"), (1 -> "1 天"), (2 -> "2 天"), (7 -> "7 天"))
  val roundSpaceHourChoices = Seq((0 -> "0 小时"), (1 -> "1 小时"), (2 -> "2 小时"), (3 -> "3 小时"), (4 -> "4 小时"))
  val roundSpaceMinuteChoices = Seq((0 -> "0 分钟"), (5 -> "5 分钟"), (10 -> "10 分钟"), (15 -> "15 分钟"), (20 -> "20 分钟"), (30 -> "30 分钟"), (45 -> "45 分钟"))

  val positions = StartingPosition.allWithInitial.map(_.fen)
  val positionChoices = StartingPosition.allWithInitial.map { p =>
    p.fen -> p.fullName
  }
  val positionDefault = StartingPosition.initial.fen

  def startingPosition(fen: String): StartingPosition =
    Thematic.byFen(fen) | StartingPosition(
      eco = "",
      name = "",
      fen = fen,
      wikiPath = "",
      moves = "",
      featurable = false
    )

  val validVariants = List(Standard)

  def guessVariant(from: String): Option[Variant] = validVariants.find { v =>
    v.key == from || parseIntOption(from).exists(v.id ==)
  }

  def validFen(fen: String) = (Forsyth <<< fen).??(f => f.situation.playable(false))

  def validDbRoundRobinNumber(rounds: Int, rule: TeamContest.Rule) = if (rule == TeamContest.Rule.DBRoundRobin) rounds % 2 == 0 else true
}

case class ContestSetup(
    basics: Basics,
    conditions: Conditions,
    rounds: Rounds
) {

  def realRule = TeamContest.Rule(basics.rule)

  def realMode = Mode(basics.rated)

  def realType = TeamContest.Type(basics.typ)

  def realVariant = DataForm.guessVariant(basics.variant) | chess.variant.Standard

  def bool(v: Int) = if (v == 1) true else false

  def clockConfig = chess.Clock.Config((basics.clockTime * 60).toInt, basics.clockIncrement)

  def toContest(user: User) =
    TeamContest.make(
      by = user,
      name = basics.name,
      groupName = basics.groupName,
      logo = basics.logo,
      typ = realType,
      organizer = basics.organizer,
      variant = realVariant,
      position = DataForm.startingPosition(basics.position),
      mode = realMode,
      teamRated = if (realType == TeamContest.Type.TeamInner) basics.teamRated else false,
      clock = clockConfig,
      rule = realRule,
      startsAt = basics.startsAt,
      finishAt = basics.finishAt,
      deadline = conditions.deadline,
      maxTeamers = conditions.maxTeamers, // 队伍总数
      maxPerTeam = conditions.maxPerTeam, // 每俱乐部限报队伍数
      formalPlayers = conditions.formalPlayers, // 每队正式棋手人数
      substitutePlayers = conditions.substitutePlayers, // 每队替补棋手人数
      roundSpace = rounds.toSpaceMinutes,
      rounds = rounds.rounds,
      canLateMinute = conditions.canLateMinute,
      autoPairing = false,
      enterShow = TeamContest.EnterShow(conditions.enterShow).some,
      meta = TeamContest.MetaData(
        teamId = if (realType == TeamContest.Type.TeamInner) basics.organizer.some else None,
        federationId = if (realType == TeamContest.Type.FederationInner) basics.organizer.some else None
      ).some
    )

  def roundList(contestId: TeamContest.ID) = rounds.list.zipWithIndex map {
    case (r, i) => TeamRound.make(
      no = i + 1,
      contestId = contestId,
      startsAt = r.startsAt
    )
  }

  def validName(id: Option[TeamContest.ID]) = TeamContestRepo.nameExists(basics.name.trim, basics.groupName.map(_.trim), id)

  def validClock = basics.validClock

  def validFinishAt = basics.validFinishAt

  def validRoundNumber = rounds.validRoundNumber(realRule)

  def validRoundSetup = rounds.validRoundSetup

  def validRoundSpace = rounds.validRoundSpace

  def validRoundStartsAt = rounds.validRoundStartsAt(basics.startsAt, basics.finishAt)

  def validRoundStartsAtBetween = rounds.validRoundStartsAtBetween

  def validCanLateTime = basics.clockTime > conditions.canLateMinute + 1

}

object ContestSetup {

  def default(rule: Rule) = {
    val now = DateTime.now
    ContestSetup(
      basics = Basics(
        name = "",
        groupName = None,
        logo = None,
        typ = TeamContest.Type.TeamInner.id,
        organizer = "",
        variant = "1",
        position = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        rated = true,
        teamRated = true,
        clockTime = DataForm.clockTimeDefault,
        clockIncrement = DataForm.clockIncrementDefault,
        rule = rule.id,
        startsAt = now.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).plusHours(1),
        finishAt = now.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).plusHours(2)
      ),
      conditions = Conditions(
        deadline = 5,
        enterShow = TeamContest.EnterShow.default.id,
        maxTeamers = rule.setup.defaultTeamers, // 队伍总数
        maxPerTeam = rule.setup.defaultTeamers, // 每俱乐部限报队伍数
        formalPlayers = 4, // 每队正式棋手人数
        substitutePlayers = 2, // 每队替补棋手人数
        canLateMinute = 3
      ),
      rounds = Rounds(
        spaceDay = 0,
        spaceHour = 0,
        spaceMinute = 30,
        rounds = rule.setup.defaultRound,
        list = List.empty
      )
    )
  }

  case class Basics(
      name: String,
      groupName: Option[String],
      logo: Option[String],
      typ: String,
      organizer: String,
      variant: String,
      position: String,
      rated: Boolean,
      teamRated: Boolean,
      clockTime: Double,
      clockIncrement: Int,
      rule: String,
      startsAt: DateTime,
      finishAt: DateTime
  ) {

    def validClock = (clockTime + clockIncrement) > 0

    def validFinishAt = finishAt.isAfter(startsAt)

    def durationMillis = finishAt.getMillis - startsAt.getMillis

    // There are 2 players, and they don't always use all their time (0.8)
    def estimatedGameSeconds: Double = {
      (60 * clockTime + 40 * clockIncrement) * 2 * 0.8
    }
  }

  case class Rounds(
      spaceDay: Int,
      spaceHour: Int,
      spaceMinute: Int,
      rounds: Int,
      list: List[RoundSetup]
  ) {

    def toSpaceMinutes = spaceDay * 24 * 60 + spaceHour * 60 + spaceMinute

    def validRoundNumber(rule: Rule) = list.size > 0 && list.size <= rule.setup.maxRound

    def validRoundSetup = list.size == rounds

    def validRoundSpace = toSpaceMinutes >= 1

    // 校验轮次开始时间小于比赛结束时间
    def validRoundStartsAt(startsAt: DateTime, finishAt: DateTime) = list.forall(d =>
      /*finishAt.getMillis > d.startsAt.getMillis && */ d.startsAt.getMillis >= startsAt.getMillis)

    // 校验下一轮次开始时间大于上一轮开始时间
    def validRoundStartsAtBetween = {
      val arr = list.toArray
      val len = arr.length
      (0 to (len - 1)).forall(i => {
        (i == len - 1) || arr(i).startsAt.isBefore(arr(i + 1).startsAt)
      })
    }
  }

  case class RoundSetup(startsAt: DateTime)

  case class Conditions(
      deadline: Int,
      enterShow: String,
      maxTeamers: Int, // 队伍总数
      maxPerTeam: Int, // 每俱乐部限报队伍数
      formalPlayers: Int, // 每队正式棋手人数
      substitutePlayers: Int, // 每队替补棋手人数
      canLateMinute: Int
  )

}

case class JoinSetup(name: String, leaderNames: String, leaders: String, players: List[JoinPlayer], staging: Boolean) {

  def validName(contestId: String, id: Option[Teamer.ID]) = TeamerRepo.nameExists(contestId, name.trim, id)

  def leadersList = if (leaders.trim.isEmpty) List.empty[String] else leaders.trim.split(",").toList

  def validFormal(contest: TeamContest) = players.count(_.formal) == contest.formalPlayers

  def validSubstitute(contest: TeamContest) = players.filterNot(_.formal).size <= contest.substitutePlayers

}

case class JoinPlayer(no: Int, userId: User.ID, formal: Boolean, rating: Int, teamRating: Option[Int])

case class RoundRobinData(list: List[DateTime]) {

  def validOrder =
    list.zip(list.tail).forall {
      case (prev, next) => prev.isBefore(next)
    }

  def validRoundNum(contest: TeamContest) = if (contest.isDbRoundRobin) list.size % 2 == 0 else true
}

case class ForbiddenData(name: String, teamerIds: String) {

  def toForbidden(contestId: String, forbidden: Option[Forbidden]) = {
    forbidden.fold(
      Forbidden.make(
        name = name,
        contestId = contestId,
        teamerIds = toList
      )
    ) { fb =>
        fb.copy(
          name = name,
          teamerIds = toList
        )
      }
  }

  def toList = teamerIds.split(",").toList

}
