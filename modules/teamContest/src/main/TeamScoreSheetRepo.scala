package lila.teamContest

import lila.db.dsl._

object TeamScoreSheetRepo {

  private[teamContest] lazy val coll = Env.current.scoreSheetColl

  import BSONHandlers.TeamScoreSheetHandler

  def byId(id: TeamScoreSheet.ID): Fu[Option[TeamScoreSheet]] =
    coll.byId[TeamScoreSheet](id)

  def insertMany(scoreSheetList: List[TeamScoreSheet]): Funit =
    coll.bulkInsert(
      documents = scoreSheetList.map(TeamScoreSheetHandler.write).toStream,
      ordered = true
    ).void

  def removeByRound(contestId: TeamContest.ID, no: TeamRound.No): Funit =
    coll.remove(roundQuery(contestId, no)).void

  def getByRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamScoreSheet]] =
    coll.find(roundQuery(contestId, no)).sort($sort asc "rank").list[TeamScoreSheet]()

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamScoreSheet]] =
    coll.find(contestQuery(contestId)).sort($sort asc "rank").list[TeamScoreSheet]()

  def setRank(id: TeamScoreSheet.ID, rank: Int): Funit =
    coll.updateField($id(id), "rank", rank).void

  def setCancelScore(teamerId: Teamer.ID): Funit =
    coll.update(
      $doc("teamerId" -> teamerId),
      $set(
        "cancelled" -> true
      ),
      multi = true
    ).void

  def setCancelledRank(contestId: TeamContest.ID): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("cancelled" -> true),
      $set(
        "rank" -> 10000
      ),
      multi = true
    ).void

  def roundQuery(contestId: TeamContest.ID, no: TeamRound.No) = $doc("contestId" -> contestId, "roundNo" -> no)
  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)
}
