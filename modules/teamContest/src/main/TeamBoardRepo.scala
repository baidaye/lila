package lila.teamContest

import lila.db.dsl._
import lila.game.Game
import reactivemongo.bson._
import org.joda.time.DateTime

object TeamBoardRepo {

  private[teamContest] lazy val coll = Env.current.boardColl
  import BSONHandlers.BoardHandler

  def insertMany(roundId: TeamRound.ID, boardList: List[TeamBoard]): Funit =
    coll.remove(roundQuery(roundId)) >> bulkInsert(boardList)

  def insertManyByRounds(roundIds: List[TeamRound.ID], boardList: List[TeamBoard]): Funit =
    coll.remove($doc("roundId" $in roundIds)) >> bulkInsert(boardList)

  def bulkUpdate(boardList: List[TeamBoard]): Funit =
    coll.remove($inIds(boardList.map(_.id))) >> bulkInsert(boardList)

  def bulkInsert(boardList: List[TeamBoard]): Funit =
    coll.bulkInsert(
      documents = boardList.map(BoardHandler.write).toStream,
      ordered = true
    ).void

  def byId(id: Game.ID): Fu[Option[TeamBoard]] = coll.byId[TeamBoard](id)

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamBoard]] =
    coll.find(contestQuery(contestId)).list[TeamBoard]()

  def getByContestLteRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamBoard]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $lte no)).list[TeamBoard]()

  def getByContestGteRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamBoard]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $gte no)).list[TeamBoard]()

  def getByContestGtRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamBoard]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $gt no)).list[TeamBoard]()

  def getByUser(contestId: TeamContest.ID, no: TeamRound.No, userId: String): Fu[List[TeamBoard]] =
    coll.find(
      contestQuery(contestId) ++ $or(
        $doc("whitePlayer.userId" -> userId),
        $doc("blackPlayer.userId" -> userId)
      ) ++ $doc("roundNo" $lte no)
    ).sort($doc("startsAt" -> 1)).list[TeamBoard]()

  def getByRound(roundId: TeamRound.ID): Fu[List[TeamBoard]] =
    coll.find(roundQuery(roundId)).list[TeamBoard]()

  def setStatus(id: Game.ID, status: chess.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setSign(id: Game.ID, color: chess.Color, isAgent: Boolean): Funit =
    coll.update(
      $id(id),
      $set(
        s"${color.name}Player.sign.isAgent" -> isAgent,
        s"${color.name}Player.sign.signedAt" -> DateTime.now
      )
    ).void

  def setAbsent(id: Game.ID, color: chess.Color): Funit =
    coll.update(
      $id(id),
      $set(
        s"${color.name}Player.absent" -> true
      )
    ).void

  def finishGame(game: Game, winner: Option[chess.Color], result: TeamBoard.Result): Funit = {
    coll.update(
      $id(game.id),
      $set(
        "status" -> game.status.id,
        "turns" -> game.playedTurns.some,
        "result" -> result.id,
        "finishAt" -> DateTime.now
      ) ++ winner.?? { wn =>
          wn.fold(
            $set("whitePlayer.isWinner" -> true),
            $set("blackPlayer.isWinner" -> true)
          )
        }
    ).void
  }

  def onePairedFinished(pairedId: TeamPaired.ID, boards: Int): Fu[Boolean] =
    coll.countSel(pairedQuery(pairedId) ++ $doc("status" $gte chess.Status.Aborted.id)) map { count =>
      count == boards
    }

  def pending: Fu[List[TeamBoard]] =
    coll.find($doc("status" -> chess.Status.Created.id, "startsAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 20)))).list[TeamBoard]()

  // 找到未来1分钟内将要开始的对局
  def remindAtSoon: Fu[List[TeamBoard]] = {
    val doc = $doc("status" -> chess.Status.Created.id, "startsAt" -> ($lte(DateTime.now plusMinutes 1) ++ $gte(DateTime.now)), "reminded" -> false)
    coll.find(doc).list[TeamBoard]()
  }

  def setReminded(id: Game.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "reminded" -> true
      )
    ).void

  def setResult(id: Game.ID, winner: Option[chess.Color], result: TeamBoard.Result): Funit =
    winner.fold {
      coll.update(
        $id(id),
        $unset("whitePlayer.isWinner", "blackPlayer.isWinner") ++ $set("result" -> result.id)
      ).void
    } { color =>
      coll.update(
        $id(id),
        $set(
          "whitePlayer.isWinner" -> (color == chess.Color.White),
          "blackPlayer.isWinner" -> (color == chess.Color.Black),
          "result" -> result.id
        )
      ).void
    }

  def update(board: TeamBoard): Funit =
    coll.update($id(board.id), board).void

  def remove(id: Game.ID): Funit = coll.remove($id(id)).void

  def removeByRound(roundId: TeamRound.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def removeByRounds(roundId: TeamRound.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def setStartsTimeByRound(roundId: TeamRound.ID, st: DateTime): Funit =
    coll.update(
      roundQuery(roundId),
      $set(
        "startsAt" -> st
      ),
      multi = true
    ).void

  def gameStartBatch(ids: List[Game.ID], startsAt: DateTime): Funit =
    coll.update(
      $doc("_id" -> $in(ids: _*)),
      $set(
        "status" -> chess.Status.Started.id,
        "startsAt" -> startsAt
      ),
      multi = true
    ).void

  def setStartsTime(id: Game.ID, st: DateTime): Funit =
    coll.update(
      $id(id),
      $set("startsAt" -> st)
    ).void

  def setStartsTimes(roundId: TeamRound.ID, st: DateTime): Funit =
    coll.update(
      roundQuery(roundId),
      $set("startsAt" -> st),
      multi = true
    ).void

  def roundSwap(ids: List[Game.ID], targetRound: TeamRound): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "roundId" -> targetRound.id,
        "roundNo" -> targetRound.no,
        "startsAt" -> targetRound.actualStartsAt
      ),
      multi = true
    ).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

  def roundQuery(roundId: TeamRound.ID) = $doc("roundId" -> roundId)

  def pairedQuery(pairedId: TeamPaired.ID) = $doc("pairedId" -> pairedId)

}
