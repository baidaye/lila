package lila.teamContest

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import play.api.libs.json.Json
import scala.math.BigDecimal.RoundingMode

case class Teamer(
    _id: Teamer.ID,
    no: Teamer.No,
    contestId: TeamContest.ID,
    teamId: String,
    teamOwner: User.ID,
    name: String,
    leaders: List[User.ID],
    score: Double = 0,
    absent: Boolean = false, // 不参与匹配(quit || kick || manualAbsent)
    quit: Boolean = false, // 主动退赛
    kick: Boolean = false, // 管理员在比赛未开始时退赛
    manualAbsent: Boolean = false, // 管理员设置本轮弃权,当本轮匹配完之后设置=true
    cancelled: Boolean = false, // 取消成绩
    outcomes: List[TeamBoard.Outcome] = Nil,
    roundScores: List[Double] = Nil,
    byeRound: List[TeamRound.No] = Nil,
    entryTime: Option[DateTime] = None,
    status: Teamer.Status,
    createdBy: User.ID,
    createdAt: DateTime
) {

  def id = _id
  def fullNo(teamerNo: Teamer.No) = s"$teamerNo.$no"
  def is(other: Teamer): Boolean = other.id == id
  def absentAnd = quit && kick
  def absentOr = quit || kick
  def quitOrKick = quit || kick
  def quitOrKickOrManualAbsent = quit || kick || manualAbsent
  def joined = status == Teamer.Status.Joined
  def sentOrRefused = status == Teamer.Status.Sent || status == Teamer.Status.Refused
  def processed = joined || status == Teamer.Status.Refused
  def staging = status == Teamer.Status.Staging
  def stagingOrRefused = status == Teamer.Status.Staging || status == Teamer.Status.Refused

  def scoreEqual(sc: Double) = {
    val score1 = BigDecimal(score).setScale(1, RoundingMode.DOWN)
    val score2 = BigDecimal(sc).setScale(1, RoundingMode.DOWN)
    score1.equals(score2)
  }

  // 不包含本轮的累计得分
  def roundScore(rn: TeamRound.No, isRoundRobin: Boolean): Double =
    outcomes.zipWithIndex.foldLeft(0.0) {
      case (s, (o, i)) => if (i < rn - 1) s + scoreByOutcome(o, isRoundRobin) else s
    }

  // 包含本轮的累计得分
  def roundScoreWithCurr(rn: TeamRound.No, isRoundRobin: Boolean): Double =
    outcomes.zipWithIndex.foldLeft(0.0) {
      case (s, (o, i)) => if (i < rn) s + scoreByOutcome(o, isRoundRobin) else s
    }

  def allScore(isRoundRobin: Boolean): Double =
    outcomes.foldLeft(0.0) {
      case (s, o) => s + scoreByOutcome(o, isRoundRobin)
    }

  def outcomeEqual(o: TeamBoard.Outcome, isRoundRobin: Boolean) = {
    if (isRoundRobin) {
      o match {
        case TeamBoard.Outcome.Win => TeamBoard.Outcome.Win.some
        case TeamBoard.Outcome.Loss => TeamBoard.Outcome.Loss.some
        case TeamBoard.Outcome.Draw => TeamBoard.Outcome.Draw.some
        case TeamBoard.Outcome.Bye | TeamBoard.Outcome.NoStart | TeamBoard.Outcome.Quit | TeamBoard.Outcome.Kick | TeamBoard.Outcome.ManualAbsent | TeamBoard.Outcome.Substitute => None
      }
    } else {
      o match {
        case TeamBoard.Outcome.Bye | TeamBoard.Outcome.Win => TeamBoard.Outcome.Win.some
        case TeamBoard.Outcome.Loss => TeamBoard.Outcome.Loss.some
        case TeamBoard.Outcome.Draw => TeamBoard.Outcome.Draw.some
        case TeamBoard.Outcome.NoStart | TeamBoard.Outcome.Quit | TeamBoard.Outcome.Kick | TeamBoard.Outcome.ManualAbsent | TeamBoard.Outcome.Substitute => None
      }
    }
  }

  def countWin(rn: TeamRound.No, isRoundRobin: Boolean) = outcomes.zipWithIndex.count {
    case (o, i) => (i < rn) && outcomeEqual(o, isRoundRobin).has(TeamBoard.Outcome.Win)
  }
  def countDraw(rn: TeamRound.No, isRoundRobin: Boolean) = outcomes.zipWithIndex.count {
    case (o, i) => (i < rn) && outcomeEqual(o, isRoundRobin).has(TeamBoard.Outcome.Draw)
  }
  def countLoss(rn: TeamRound.No, isRoundRobin: Boolean) = outcomes.zipWithIndex.count {
    case (o, i) => (i < rn) && outcomeEqual(o, isRoundRobin).has(TeamBoard.Outcome.Loss)
  }

  // 获得本轮结果
  def roundOutcome(rn: TeamRound.No): Option[TeamBoard.Outcome] =
    outcomes.zipWithIndex.find(_._2 == rn - 1).map(_._1)

  def setOutcomeByRound(rn: TeamRound.No, outcome: TeamBoard.Outcome) =
    if (outcomes.length >= rn) {
      outcomes.zipWithIndex.map {
        case (o, i) => if (rn == i + 1) outcome else o
      }
    } else {
      if (rn - 1 == outcomes.length) outcomes :+ outcome
      else {
        logger.warn(s"设置队伍结果异常：$id，$rn，$outcome")
        outcomes
      }
    }

  def setScoreByRound(rn: TeamRound.No, score: Double) =
    if (roundScores.length >= rn) {
      roundScores.zipWithIndex.map {
        case (s, i) => if (rn == i + 1) score else s
      }
    } else {
      if (rn - 1 == roundScores.length) roundScores :+ score
      else {
        logger.warn(s"设置队伍积分异常：$id，$rn")
        roundScores
      }
    }

  def removeOutcomeByRound(rn: TeamRound.No) =
    if (outcomes.length == rn) {
      outcomes.dropRight(1)
    } else {
      logger.warn(s"删除队伍结果异常：$id，$rn")
      outcomes
    }

  def removeScoreByRound(rn: TeamRound.No) =
    if (roundScores.length == rn) {
      roundScores.dropRight(1)
    } else {
      logger.warn(s"删除队伍积分异常：$id，$rn")
      roundScores
    }

  def roundOutcomeFormat(rn: TeamRound.No): String = {
    import TeamBoard.Outcome._
    roundOutcome(rn).map {
      case Win => "1-0"
      case Loss => "0-1"
      case Draw => "1/2-1/2"
      case Bye => "轮空"
      case NoStart => "未移动"
      case Quit => "退赛"
      case Kick => "退赛"
      case ManualAbsent => "弃权"
      case Substitute => "替补"
    }.orElse(if (isBye(rn)) "轮空".some else None) | "-"
  }

  def roundOutcomeSort(rn: TeamRound.No): Int = {
    import TeamBoard.Outcome._
    roundOutcome(rn).map {
      case Win => 100
      case Loss => 90
      case Draw => 80
      case NoStart => 70
      case Bye => 60
      case ManualAbsent => 40
      case Quit => 20
      case Kick => 10
      case Substitute => 0
    }.orElse(if (isBye(rn)) 60.some else None) | 0
  }

  def scoreByOutcome(o: TeamBoard.Outcome, isRoundRobin: Boolean): Double = {
    import TeamBoard.Outcome._
    o match {
      case Win => 2.0
      case Loss => 0.0
      case Draw => 1.0
      case Bye => if (isRoundRobin) 0.0 else 2.0
      case Quit => 0.0
      case Kick => 0.0
      case ManualAbsent => 0.0
      case _ => 0.0
    }
  }

  def finish(rn: TeamRound.No, o: TeamBoard.Outcome, s: Double, isRoundRobin: Boolean) = {
    copy(
      outcomes = setOutcomeByRound(rn, o),
      roundScores = setScoreByRound(rn, s)
    ) |> { t =>
        t.copy(score = t.allScore(isRoundRobin))
      }
  }

  def isBye(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && o == TeamBoard.Outcome.Bye
    } || byeRound.contains(rn)

  def isManualAbsent(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && o == TeamBoard.Outcome.ManualAbsent
    }

  def isAbsent(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && (o == TeamBoard.Outcome.Quit || o == TeamBoard.Outcome.Kick || o == TeamBoard.Outcome.ManualAbsent || o == TeamBoard.Outcome.Substitute)
    }

  def isAbsentIgnoreManual(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && (o == TeamBoard.Outcome.Quit || o == TeamBoard.Outcome.Kick)
    }

  def noBoard(rn: TeamRound.No): Boolean = isAbsent(rn) || isBye(rn)

  def isCreator(userId: User.ID) = userId == createdBy

  def isLeader(userId: User.ID) = leaders.contains(userId)

  def isCreatorOrLeader(userId: User.ID) = isCreator(userId) || isLeader(userId)

  def json = Json.obj(
    "id" -> id,
    "no" -> no,
    "name" -> s"$no $name"
  )

  override def toString: String = s"$name - $id"

}

object Teamer {

  type ID = String

  type No = Int

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Staging extends Status("staging", "暂存")
    case object Sent extends Status("sent", "待审核")
    case object Joined extends Status("joined", "报名成功")
    case object Refused extends Status("refused", "已拒绝")

    val all = List(Staging, Sent, Joined, Refused)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): Status = byId get id err s"Bad Status $id"
    def applyByAccept(accept: Boolean): Status = if (accept) Joined else Refused
  }

  private[teamContest] def make(
    no: Teamer.No,
    contestId: TeamContest.ID,
    teamId: String,
    teamOwner: User.ID,
    name: String,
    leaders: List[User.ID],
    staging: Boolean,
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    Teamer(
      _id = Random.nextString(8),
      no = no,
      contestId = contestId,
      teamId = teamId,
      teamOwner = teamOwner,
      name = name,
      leaders = leaders,
      status = if (staging) Status.Staging else Status.Sent,
      createdBy = createdBy,
      createdAt = now
    )
  }

}
