package lila.teamContest

import lila.db.dsl._

object TeamPlayerScoreSheetRepo {

  private[teamContest] lazy val coll = Env.current.playerScoreSheetColl

  import BSONHandlers.PlayerScoreSheetHandler

  def byId(id: TeamPlayerScoreSheet.ID): Fu[Option[TeamPlayerScoreSheet]] =
    coll.byId[TeamPlayerScoreSheet](id)

  def insertMany(scoreSheetList: List[TeamPlayerScoreSheet]): Funit =
    coll.bulkInsert(
      documents = scoreSheetList.map(PlayerScoreSheetHandler.write).toStream,
      ordered = true
    ).void

  def removeByRound(contestId: TeamContest.ID, no: TeamRound.No): Funit =
    coll.remove(roundQuery(contestId, no)).void

  def getByRound(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamPlayerScoreSheet]] =
    coll.find(roundQuery(contestId, no)).sort($sort asc "rank").list[TeamPlayerScoreSheet]()

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamPlayerScoreSheet]] =
    coll.find(contestQuery(contestId)).sort($sort asc "rank").list[TeamPlayerScoreSheet]()

  def setRank(id: TeamPlayerScoreSheet.ID, rank: Int): Funit =
    coll.updateField($id(id), "rank", rank).void

  def setCancelScore(teamerId: Teamer.ID): Funit =
    coll.update(
      $doc("teamerId" -> teamerId),
      $set(
        "cancelled" -> true
      ),
      multi = true
    ).void

  def setCancelledRank(contestId: TeamContest.ID): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("cancelled" -> true),
      $set(
        "rank" -> 10000
      ),
      multi = true
    ).void

  def roundQuery(contestId: TeamContest.ID, no: TeamRound.No) = $doc("contestId" -> contestId, "roundNo" -> no)
  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)
}
