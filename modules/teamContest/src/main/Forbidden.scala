package lila.teamContest

import ornicar.scalalib.Random

case class Forbidden(
    _id: String,
    name: String,
    contestId: String,
    teamerIds: List[Teamer.ID]
) {

  def id = _id

  def withTeamer(teamers: List[Teamer]) = {
    val tids = teamers.map(_.id)
    filteredIds(tids).map { teamerId =>
      (teamerId, teamers.find(t => t.id == teamerId) err s"can not find teamer $teamerId")
    }
  }

  def pairs(teamers: List[Teamer]) = {
    val tids = teamers.map(_.id)
    val ftids = filteredIds(tids)
    ftids.flatMap { p1 =>
      ftids.map { p2 =>
        p1 -> p2
      }
    }.filterNot(p => p._1 == p._2) map {
      case (p1, p2) => if (p1 > p2) p2 -> p1 else p1 -> p2
    } toSet
  }

  def filteredIds(tids: List[Teamer.ID]) = teamerIds.filter(tids.contains(_))

}

object Forbidden {

  def make(
    name: String,
    contestId: String,
    teamerIds: List[Teamer.ID]
  ): Forbidden = Forbidden(
    _id = Random nextString 8,
    name = name,
    contestId = contestId,
    teamerIds: List[Teamer.ID]
  )

  case class WithUser(forbidden: Forbidden, teamers: List[Teamer])

}
