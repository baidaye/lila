package lila

package object teamContest extends PackageObject {

  private[teamContest] val logger = lila.log("teamContest")

  private[teamContest] val pairingLogger = logger branch "pairing"

  private[teamContest] type Players = List[teamContest.TeamPlayer]

  private[teamContest] type Boards = List[teamContest.TeamBoard]

  private[teamContest] type Ranking = Map[lila.user.User.ID, Int]

}
