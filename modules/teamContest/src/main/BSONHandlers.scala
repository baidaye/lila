package lila.teamContest

import lila.db.BSON
import lila.db.dsl._
import chess.variant.Variant
import chess.Clock.{ Config => ClockConfig }
import chess.StartingPosition
import reactivemongo.bson._
import org.joda.time.DateTime
import TeamContest.MetaData
import TeamContest.EnterShow
import TeamContest.Rule
import TeamBoard.Sign
import TeamBoard.MiniPlayer
import TeamBtss.TeamBtssScore
import TeamPlayerBtss.TeamPlayerBtssScore

object BSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler

  private[teamContest] implicit val arrayHandler = bsonArrayToListHandler[String]
  private[teamContest] implicit val metaHandler = Macros.handler[TeamContest.MetaData]
  private[teamContest] implicit val enterShowHandler = Macros.handler[TeamContest.EnterShow]

  private[teamContest] implicit val ClockBSONHandler = new BSONHandler[BSONDocument, ClockConfig] {
    def read(doc: BSONDocument) = ClockConfig(
      doc.getAs[Int]("limit").get,
      doc.getAs[Int]("increment").get
    )

    def write(config: ClockConfig) = BSONDocument(
      "limit" -> config.limitSeconds,
      "increment" -> config.incrementSeconds
    )
  }

  private[teamContest] implicit val ColorBSONHandler = new BSONHandler[BSONString, chess.Color] {
    def read(b: BSONString) = chess.Color(b.value) err s"invalid color ${b.value}"
    def write(c: chess.Color) = BSONString(c.name)
  }

  private[teamContest] implicit val ModeBSONHandler = new BSONHandler[BSONInteger, chess.Mode] {
    def read(b: BSONInteger): chess.Mode = chess.Mode.orDefault(b.value)
    def write(x: chess.Mode) = BSONInteger(x.id)
  }

  private[teamContest] implicit val VariantBSONHandler = new BSONHandler[BSONInteger, chess.variant.Variant] {
    def read(b: BSONInteger): chess.variant.Variant = chess.variant.Variant.orDefault(b.value)
    def write(x: chess.variant.Variant) = BSONInteger(x.id)
  }

  private[teamContest] implicit val StatusBSONHandler = new BSONHandler[BSONInteger, chess.Status] {
    def read(bsonInt: BSONInteger): chess.Status = chess.Status(bsonInt.value) err s"No such status: ${bsonInt.value}"
    def write(x: chess.Status) = BSONInteger(x.id)
  }

  private[teamContest] implicit val BtssBSONHandler = new BSONHandler[BSONString, TeamBtss] {
    def read(bsonString: BSONString): TeamBtss = TeamBtss(bsonString.value)
    def write(b: TeamBtss) = BSONString(b.id)
  }

  private[teamContest] implicit val BtssScoreBSONHandler = new BSON[TeamBtss.TeamBtssScore] {
    def reads(r: BSON.Reader): TeamBtss.TeamBtssScore = TeamBtss.TeamBtssScore(
      btss = r.get[TeamBtss]("btss"),
      score = r double "score"
    )
    def writes(w: BSON.Writer, b: TeamBtss.TeamBtssScore) = $doc(
      "btss" -> b.btss,
      "score" -> b.score
    )
  }

  private[teamContest] implicit val TeamPlayerBtssBSONHandler = new BSONHandler[BSONString, TeamPlayerBtss] {
    def read(bsonString: BSONString): TeamPlayerBtss = TeamPlayerBtss(bsonString.value)
    def write(b: TeamPlayerBtss) = BSONString(b.id)
  }

  private[teamContest] implicit val TeamPlayerBtssScoreBSONHandler = new BSON[TeamPlayerBtss.TeamPlayerBtssScore] {
    def reads(r: BSON.Reader): TeamPlayerBtss.TeamPlayerBtssScore = TeamPlayerBtss.TeamPlayerBtssScore(
      btss = r.get[TeamPlayerBtss]("btss"),
      score = r double "score"
    )
    def writes(w: BSON.Writer, b: TeamPlayerBtss.TeamPlayerBtssScore) = $doc(
      "btss" -> TeamPlayerBtssBSONHandler.write(b.btss),
      "score" -> b.score
    )
  }

  private[teamContest] implicit val BtsssBSONHandler = new BSONHandler[BSONArray, Btsss] {
    private val btssBSONHandler = bsonArrayToListHandler[String]

    def read(doc: BSONArray) = Btsss(btssBSONHandler.read(doc).map(TeamBtss(_)))

    def write(btsss: Btsss) = BSONArray(btsss.list.map(_.id).map(BSONString.apply))
  }

  private[teamContest] implicit val PlayerBtssScoreArrayHandler = lila.db.dsl.bsonArrayToListHandler[TeamPlayerBtss.TeamPlayerBtssScore]
  private[teamContest] implicit val PlayerScoreSheetHandler = Macros.handler[TeamPlayerScoreSheet]
  private[teamContest] implicit val TeamBtssScoreArrayHandler = lila.db.dsl.bsonArrayToListHandler[TeamBtss.TeamBtssScore]
  private[teamContest] implicit val TeamScoreSheetHandler = Macros.handler[TeamScoreSheet]

  private[teamContest] implicit val ContestRuleBSONHandler = new BSONHandler[BSONString, TeamContest.Rule] {
    def read(b: BSONString): TeamContest.Rule = TeamContest.Rule(b.value)
    def write(x: TeamContest.Rule) = BSONString(x.id)
  }

  private[teamContest] implicit val ContestStatusBSONHandler = new BSONHandler[BSONInteger, TeamContest.Status] {
    def read(b: BSONInteger): TeamContest.Status = TeamContest.Status(b.value)
    def write(x: TeamContest.Status) = BSONInteger(x.id)
  }

  private[teamContest] implicit val ContestTypeBSONHandler = new BSONHandler[BSONString, TeamContest.Type] {
    def read(b: BSONString): TeamContest.Type = TeamContest.Type(b.value)
    def write(x: TeamContest.Type) = BSONString(x.id)
  }

  private[teamContest] implicit val StartingPositionBSONHandler = new BSONHandler[BSONString, StartingPosition] {
    def read(b: BSONString): StartingPosition = Thematic.byFen(b.value) | StartingPosition(
      eco = "",
      name = "",
      fen = b.value,
      wikiPath = "",
      moves = "",
      featurable = false
    )
    def write(x: StartingPosition) = BSONString(x.fen)
  }

  private[teamContest] implicit val RoundStatusBSONHandler = new BSONHandler[BSONInteger, TeamRound.Status] {
    def read(b: BSONInteger): TeamRound.Status = TeamRound.Status(b.value)
    def write(x: TeamRound.Status) = BSONInteger(x.id)
  }

  private[teamContest] implicit val RequestStatusBSONHandler = new BSONHandler[BSONString, TeamRequest.Status] {
    def read(b: BSONString): TeamRequest.Status = TeamRequest.Status(b.value)
    def write(x: TeamRequest.Status) = BSONString(x.id)
  }

  private[teamContest] implicit val TeamerStatusBSONHandler = new BSONHandler[BSONString, Teamer.Status] {
    def read(b: BSONString): Teamer.Status = Teamer.Status(b.value)
    def write(x: Teamer.Status) = BSONString(x.id)
  }

  private[teamContest] implicit val PairedStatusBSONHandler = new BSONHandler[BSONInteger, TeamPaired.Status] {
    def read(b: BSONInteger): TeamPaired.Status = TeamPaired.Status(b.value)
    def write(x: TeamPaired.Status) = BSONInteger(x.id)
  }

  private[teamContest] implicit val PlayerSignHandler = new BSON[TeamBoard.Sign] {
    def reads(r: BSON.Reader) = TeamBoard.Sign(
      isAgent = r bool "isAgent",
      signedAt = r.get[DateTime]("signedAt")
    )

    def writes(w: BSON.Writer, sign: TeamBoard.Sign) =
      $doc(
        "isAgent" -> sign.isAgent,
        "signedAt" -> sign.signedAt
      )
  }

  private[teamContest] implicit val BoardResultBSONHandler = new BSONHandler[BSONString, TeamBoard.Result] {
    def read(b: BSONString): TeamBoard.Result = TeamBoard.Result(b.value)
    def write(x: TeamBoard.Result) = BSONString(x.id)
  }

  private[teamContest] implicit val miniPlayerHandler = new BSON[TeamPaired.MiniTeamer] {
    def reads(r: BSON.Reader) = TeamPaired.MiniTeamer(
      id = r str "id",
      no = r int "no",
      leaders = r strsD "leaders",
      color = r.get[chess.Color]("color"),
      score = r double "score"
    )

    def writes(w: BSON.Writer, p: TeamPaired.MiniTeamer) =
      $doc(
        "id" -> p.id,
        "no" -> p.no,
        "leaders" -> p.leaders,
        "color" -> p.color,
        "score" -> p.score
      )
  }

  private[teamContest] implicit val OutcomeBSONHandler = new BSONHandler[BSONString, TeamBoard.Outcome] {
    def read(b: BSONString): TeamBoard.Outcome = TeamBoard.Outcome(b.value)
    def write(x: TeamBoard.Outcome) = BSONString(x.id)
  }

  private[teamContest] implicit val BoardSignHandler = Macros.handler[TeamBoard.Sign]
  private[teamContest] implicit val MiniPlayerHandler = Macros.handler[TeamBoard.MiniPlayer]

  private[teamContest] implicit val RequestHandler = Macros.handler[TeamRequest]
  private[teamContest] implicit val TeamerHandler = Macros.handler[Teamer]
  private[teamContest] implicit val PlayerHandler = Macros.handler[TeamPlayer]
  private[teamContest] implicit val PairedHandler = Macros.handler[TeamPaired]
  private[teamContest] implicit val RoundHandler = Macros.handler[TeamRound]
  private[teamContest] implicit val BoardHandler = Macros.handler[TeamBoard]
  private[teamContest] implicit val ForbiddenHandler = Macros.handler[Forbidden]

  private[teamContest] implicit val ContestHandler = new BSON[TeamContest] {
    def reads(r: BSON.Reader) = TeamContest(
      _id = r str "_id",
      name = r str "name",
      groupName = r strO "groupName",
      logo = r strO "logo",
      typ = r.get[TeamContest.Type]("typ"),
      organizer = r str "organizer",
      variant = r.intO("variant").fold[Variant](Variant.default)(Variant.orDefault),
      position = {
        val fen = r.str("position")
        Thematic.byFen(fen) | StartingPosition(
          eco = "",
          name = "",
          fen = fen,
          wikiPath = "",
          moves = "",
          featurable = false
        )
      },
      mode = r.get[chess.Mode]("mode"),
      clock = r.get[ClockConfig]("clock"),
      rule = r.get[TeamContest.Rule]("rule"),
      teamRated = r.boolD("teamRated"),
      startsAt = r.get[DateTime]("startsAt"),
      finishAt = r.get[DateTime]("finishAt"),
      deadline = r int "deadline",
      deadlineAt = r.get[DateTime]("deadlineAt"),
      maxTeamers = r int "maxTeamers",
      maxPerTeam = r int "maxPerTeam",
      formalPlayers = r int "formalPlayers",
      substitutePlayers = r int "substitutePlayers",
      roundSpace = r int "roundSpace",
      rounds = r int "rounds",
      teamBtsss = r.get[Btsss]("teamBtsss"),
      canLateMinute = r int "canLateMinute",
      autoPairing = r bool "autoPairing",
      enterShow = r.getO[TeamContest.EnterShow]("enterShow"),
      nbTeamers = r int "nbTeamers",
      allRoundFinished = r boolD "allRoundFinished",
      currentRound = r int "currentRound",
      status = r.get[TeamContest.Status]("status"),
      realFinishAt = r.getO[DateTime]("realFinishAt"),
      roundRobinPairing = r boolO "roundRobinPairing",
      meta = r.getO[TeamContest.MetaData]("meta"),
      createdByTeam = r strO "createdByTeam",
      createdBy = r str "createdBy",
      createdAt = r.get[DateTime]("createdAt")
    )

    def writes(w: BSON.Writer, o: TeamContest) =
      $doc(
        "_id" -> o.id,
        "name" -> o.name,
        "groupName" -> o.groupName,
        "logo" -> o.logo,
        "typ" -> o.typ.id,
        "organizer" -> o.organizer,
        "variant" -> o.variant.some.filterNot(_.standard).map(_.id),
        "position" -> o.position.fen,
        "mode" -> o.mode.id,
        "clock" -> ClockBSONHandler.write(o.clock),
        "rule" -> o.rule.id,
        "teamRated" -> o.teamRated,
        "startsAt" -> o.startsAt,
        "finishAt" -> o.finishAt,
        "deadline" -> o.deadline,
        "deadlineAt" -> o.deadlineAt,
        "maxTeamers" -> o.maxTeamers,
        "maxPerTeam" -> o.maxPerTeam,
        "formalPlayers" -> o.formalPlayers,
        "substitutePlayers" -> o.substitutePlayers,
        "roundSpace" -> o.roundSpace,
        "rounds" -> o.rounds,
        "teamBtsss" -> o.teamBtsss,
        "canLateMinute" -> o.canLateMinute,
        "autoPairing" -> o.autoPairing,
        "enterShow" -> o.enterShow.map(_.id),
        "nbTeamers" -> o.nbTeamers,
        "allRoundFinished" -> o.allRoundFinished,
        "currentRound" -> o.currentRound,
        "status" -> o.status.id,
        "realFinishAt" -> o.realFinishAt,
        "roundRobinPairing" -> o.roundRobinPairing,
        "meta" -> o.meta,
        "createdByTeam" -> o.createdByTeam,
        "createdAt" -> o.createdAt,
        "createdBy" -> o.createdBy
      )
  }

}
