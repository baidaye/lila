package lila.teamContest

final class PairingSystem(swissPairing: SwissPairing, roundRobinPairing: RoundRobinPairing) {

  import PairingSystem._

  def apply(contest: TeamContest): Fu[List[ByeOrPending]] =
    contest.rule match {
      case TeamContest.Rule.Swiss => swissPairing.pairing(contest)
      case TeamContest.Rule.Dual => swissPairing.pairing(contest)
      case _ => fuccess(Nil)
    }

  def pairingAll(contest: TeamContest, teamers: List[Teamer]): Fu[Map[Int, List[ByeOrPending]]] =
    contest.rule match {
      case TeamContest.Rule.RoundRobin => roundRobinPairing.pairingAll(contest, teamers)
      case TeamContest.Rule.DBRoundRobin => roundRobinPairing.pairingAll(contest, teamers)
      case _ => fuccess(Map.empty[Int, List[ByeOrPending]])
    }

}

object PairingSystem {

  type ByeOrPending = Either[Bye, Pending]

  case class Pending(white: Teamer.No, black: Teamer.No)

  case class Bye(no: Teamer.No)

}
