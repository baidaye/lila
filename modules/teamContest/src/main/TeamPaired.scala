package lila.teamContest

import chess.Color
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import scala.math.BigDecimal.RoundingMode

case class TeamPaired(
    _id: TeamPaired.ID,
    no: TeamPaired.No,
    contestId: TeamContest.ID,
    roundId: TeamRound.ID,
    roundNo: TeamRound.No,
    whiteTeamer: TeamPaired.MiniTeamer,
    blackTeamer: TeamPaired.MiniTeamer,
    status: TeamPaired.Status,
    createdAt: DateTime,
    finishAt: Option[DateTime]
) {

  def id = _id

  def teamers = List(whiteTeamer, blackTeamer)

  def teamerNos = (whiteTeamer.no, blackTeamer.no)

  def teamer(color: Color): TeamPaired.MiniTeamer =
    color.fold(whiteTeamer, blackTeamer)

  def teamer(no: Teamer.No): Option[TeamPaired.MiniTeamer] =
    if (no == whiteTeamer.no) whiteTeamer.some
    else if (no == blackTeamer.no) blackTeamer.some
    else none

  def opponentOf(no: Teamer.No): Option[TeamPaired.No] =
    if (no == whiteTeamer.no) blackTeamer.no.some
    else if (no == blackTeamer.no) whiteTeamer.no.some
    else none

  def colorOf(no: Teamer.No): Option[Color] =
    if (no == whiteTeamer.no) Color.White.some
    else if (no == blackTeamer.no) Color.Black.some
    else none

  def colorOfById(teamerId: Teamer.ID): Color =
    if (teamerId == whiteTeamer.id) Color.White
    else if (teamerId == blackTeamer.id) Color.Black
    else Color.White

  def containsByNo(no: Teamer.No): Boolean =
    whiteTeamer.no == no || blackTeamer.no == no

  def containsById(id: Teamer.ID): Boolean =
    whiteTeamer.id == id || blackTeamer.id == id

  def isWhiteLeader(userId: User.ID): Boolean =
    whiteTeamer.leaders.contains(userId)

  def isBlackLeader(userId: User.ID): Boolean =
    blackTeamer.leaders.contains(userId)

  def isLeader(userId: User.ID): Boolean =
    isWhiteLeader(userId) || isBlackLeader(userId)

  def winner: Option[TeamPaired.MiniTeamer] =
    if (whiteTeamer.scoreDecimal > blackTeamer.scoreDecimal) whiteTeamer.some
    else if (whiteTeamer.scoreDecimal < blackTeamer.scoreDecimal) blackTeamer.some
    else None

  def winnerColor: Option[chess.Color] = winner.map(_.color)

  def isWin(no: Teamer.No) = winner.??(_.no == no)

  def isDraw = winner.isEmpty

  def whiteOutcome = winnerColor.map(_.fold(TeamBoard.Outcome.Win, TeamBoard.Outcome.Loss)) | TeamBoard.Outcome.Draw

  def blackOutcome = winnerColor.map(_.fold(TeamBoard.Outcome.Loss, TeamBoard.Outcome.Win)) | TeamBoard.Outcome.Draw

  def resultShow = status match {
    case TeamPaired.Status.Created => "等待开赛"
    case TeamPaired.Status.Started => "比赛中"
    case _ => resultFormat
  }

  def resultFormat = winnerColor.map(_.fold("2-0", "0-2")) | "1-1"

  def finish(isRoundRobin: Boolean, whitePlayers: List[TeamPlayer], blackPlayers: List[TeamPlayer]): TeamPaired = {
    val roundAllWhitePlayerScore = whitePlayers.foldLeft(0.0) {
      case (all, player) => all + player.roundScoreCurr(roundNo, isRoundRobin)
    }
    val roundAllBlackPlayerScore = blackPlayers.foldLeft(0.0) {
      case (all, player) => all + player.roundScoreCurr(roundNo, isRoundRobin)
    }
    copy(
      whiteTeamer = whiteTeamer.copy(score = roundAllWhitePlayerScore),
      blackTeamer = blackTeamer.copy(score = roundAllBlackPlayerScore),
      status = TeamPaired.Status.Finished,
      finishAt = DateTime.now.some
    )
  }

}

object TeamPaired {

  type ID = String

  type No = Int

  // score 是局分，本轮本队所有队员积分之和
  case class MiniTeamer(id: Teamer.ID, no: Teamer.No, leaders: List[User.ID], color: chess.Color, score: Double) {
    def scoreDecimal = BigDecimal(score).setScale(1, RoundingMode.DOWN)
  }

  def make(
    no: TeamPaired.No,
    contestId: TeamContest.ID,
    roundId: TeamRound.ID,
    roundNo: TeamRound.No,
    whiteTeamer: Teamer,
    blackTeamer: Teamer,
    createdAt: DateTime
  ) = TeamPaired(
    _id = Random nextString 8,
    no = no,
    contestId = contestId,
    roundId = roundId,
    roundNo = roundNo,
    whiteTeamer = TeamPaired.MiniTeamer(whiteTeamer.id, whiteTeamer.no, whiteTeamer.leaders, chess.White, 0),
    blackTeamer = TeamPaired.MiniTeamer(blackTeamer.id, blackTeamer.no, blackTeamer.leaders, chess.Black, 0),
    status = TeamPaired.Status.Created,
    createdAt = DateTime.now,
    finishAt = None
  )

  private[teamContest] sealed abstract class Status(val id: Int, val desc: String) extends Ordered[Status] {
    def compare(other: Status) = Integer.compare(id, other.id)
    def is(s: Status): Boolean = this == s
    def is(f: Status.type => Status): Boolean = is(f(Status))
  }

  private[teamContest] object Status {
    case object Created extends Status(10, "等待开赛")
    case object Started extends Status(20, "比赛中")
    case object Finished extends Status(30, "比赛结束")
    val all = List(Created, Started, Finished)

    val byId = all map { v => (v.id, v) } toMap
    def apply(id: Int): Status = byId get id err s"Bad Status $id"
  }

}
