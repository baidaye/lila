package lila.teamContest

import org.joda.time.DateTime
import lila.game.{ Game, IdGenerator }

final class PairingDirector(pairingSystem: PairingSystem) {

  private[teamContest] def roundPairingTest(contest: TeamContest): Fu[Boolean] =
    pairingSystem(contest)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(false)
        else fuccess(true)
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          false
      }

  private[teamContest] def roundPairing(contest: TeamContest, round: TeamRound, teamers: List[Teamer], players: List[TeamPlayer]): Fu[Option[List[TeamPaired]]] =
    pairingSystem(contest)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          val paireds = toRoundPaireds(contest, round, pendings, teamers)
          for {
            borads <- toRoundBoards(contest, round, paireds, teamers, players)
            _ <- addPaireds(round, paireds)
            _ <- addBoards(round, borads)
            _ <- setTeamerBye(contest, round.no, pendings, teamers, players)
            _ <- TeamPlayerRepo.setSubstituteOutcomes(contest.id, round.no)
            _ <- TeamRoundRepo.setPairedAndStatus(round.id, paireds.size, borads.size, TeamRound.Status.Pairing)
          } yield Some(paireds)
        }
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[TeamPaired])
        case RoundRobinPairing.PairingException(msg) =>
          logger.warn(s"RoundRobinPairing failed, contest: $contest, msg: " + msg)
          Some(List.empty[TeamPaired])
      }

  private[teamContest] def pairingAll(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer], players: List[TeamPlayer]): Fu[Option[List[TeamPaired]]] = {
    logger.info(s"循环赛匹配开始：$contest")
    pairingSystem.pairingAll(contest, teamers)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          val paireds = allRoundPaireds(contest, rounds, teamers, pendings)
          for {
            borads <- toAllRoundBoards(contest, rounds, paireds, teamers, players)
            _ <- addAllPaireds(rounds, paireds)
            _ <- addAllBoards(rounds, borads)
            _ <- setAllTeamerByeRound(contest, rounds, teamers, pendings)
            _ <- TeamRoundRepo.setPairedsAndStatus(rounds.map(_.id), paireds.size / rounds.size, borads.size / rounds.size, TeamRound.Status.Pairing)
            firstRound = rounds.minBy(_.no)
          } yield Some(paireds.filter(_.roundNo == firstRound.no))
        }
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[TeamPaired])
        case RoundRobinPairing.PairingException(msg) =>
          logger.warn(s"RoundRobinPairing failed, contest: $contest, msg: " + msg)
          Some(List.empty[TeamPaired])
      }
  }

  private def addPaireds(round: TeamRound, paireds: List[TeamPaired]): Funit = {
    TeamPairedRepo.removeByRound(round.id) >>
      TeamPairedRepo.insertMany(round.id, paireds)
  }

  private def addAllPaireds(rounds: List[TeamRound], paireds: List[TeamPaired]): Funit = {
    TeamPairedRepo.insertManyByRounds(rounds.map(_.id), paireds)
  }

  private def toRoundPaireds(contest: TeamContest, round: TeamRound, byeOrPendings: List[PairingSystem.ByeOrPending], teamers: List[Teamer]): List[TeamPaired] = {
    byeOrPendings.zipWithIndex.collect {
      case (Right(PairingSystem.Pending(w, b)), i) => {
        val white = teamers.find(_.no == w) err s"cannot find teamer $w"
        val black = teamers.find(_.no == b) err s"cannot find teamer $b"
        TeamPaired.make(
          no = i + 1,
          contestId = contest.id,
          roundId = round.id,
          roundNo = round.no,
          whiteTeamer = white,
          blackTeamer = black,
          createdAt = DateTime.now
        )
      }
    }
  }

  private def allRoundPaireds(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer], byeOrPendingMap: Map[Int, List[PairingSystem.ByeOrPending]]): List[TeamPaired] = {
    rounds.flatMap { round =>
      byeOrPendingMap.get(round.no) match {
        case Some(byeOrPendings) => {
          byeOrPendings.collect { case Right(pending) => pending }.zipWithIndex.map {
            case (pending, i) => {
              val white = teamers.find(_.no == pending.white) err s"cannot find teamer ${pending.white}"
              val black = teamers.find(_.no == pending.black) err s"cannot find teamer ${pending.black}"
              TeamPaired.make(
                no = i + 1,
                contestId = contest.id,
                roundId = round.id,
                roundNo = round.no,
                whiteTeamer = white,
                blackTeamer = black,
                createdAt = DateTime.now
              )
            }
          }
        }
        case None => throw RoundRobinPairing.PairingException(s"allRoundBoards RoundRobinPairing PairError $contest 第${round.no}轮")
      }
    }
  }

  private def setTeamerBye(contest: TeamContest, roundNo: TeamRound.No, byeOrPendings: List[PairingSystem.ByeOrPending], teamers: List[Teamer], players: List[TeamPlayer]): Funit = {
    val newByeNos = byeOrPendings.collect { case Left(bye) => bye.no }
    val newByeTeamers = teamers.filter(p => newByeNos.contains(p.no))
    val oldByeTeamers = teamers.filter(_.roundOutcome(roundNo).contains(TeamBoard.Outcome.Bye))
    val oldByeNos = oldByeTeamers.map(_.no)

    oldByeTeamers.filter(p => !newByeNos.contains(p.no)).map { teamer =>
      TeamerRepo.update(
        teamer.copy(
          outcomes = teamer.removeOutcomeByRound(roundNo),
          roundScores = teamer.removeScoreByRound(roundNo)
        ) |> { t =>
            t.copy(score = t.allScore(contest.isRoundRobin))
          }
      ) >> {
          players.filter(_.teamerId == teamer.id).map { player =>
            TeamPlayerRepo.update(
              player.copy(
                outcomes = player.removeOutcomeByRound(roundNo)
              ) |> { p =>
                  p.copy(score = p.allScore(contest.isRoundRobin))
                }
            )
          }.sequenceFu.void
        }
    }.sequenceFu.void >> {
      newByeTeamers.filter(p => !oldByeNos.contains(p.no)).map { teamer =>
        TeamerRepo.update(
          teamer.copy(
            outcomes = teamer.setOutcomeByRound(roundNo, TeamBoard.Outcome.Bye),
            roundScores = teamer.setScoreByRound(roundNo, teamer.scoreByOutcome(TeamBoard.Outcome.Bye, contest.isRoundRobin))
          ) |> { p =>
              p.copy(score = p.allScore(contest.isRoundRobin))
            }
        ) >> {
            players.filter(_.teamerId == teamer.id).map { player =>
              TeamPlayerRepo.update(
                player.copy(
                  outcomes = player.setOutcomeByRound(roundNo, TeamBoard.Outcome.Bye)
                ) |> { p =>
                    p.copy(score = p.allScore(contest.isRoundRobin))
                  }
              )
            }.sequenceFu.void
          }
      }.sequenceFu.void
    }
  }

  private def setAllTeamerByeRound(contest: TeamContest, rounds: List[TeamRound], teamers: List[Teamer], byeOrPendingMap: Map[Int, List[PairingSystem.ByeOrPending]]): Funit = {
    byeOrPendingMap.foldLeft(Map.empty[Teamer.No, List[TeamRound.No]]) {
      case (teamerByeRound, (roundNo, byeOrPendings)) => {
        val byeNos = byeOrPendings.collect { case Left(bye) => bye.no }
        var newMap = teamerByeRound
        byeNos.foreach { teamerNo =>
          newMap = newMap.get(teamerNo) match {
            case None => newMap + (teamerNo -> List(roundNo))
            case Some(roundNos) => newMap + (teamerNo -> (roundNos :+ roundNo))
          }
        }
        newMap
      }
    }.map {
      case (teamerNo, byeRound) => {
        val teamer = teamers.find(_.no == teamerNo) err s"cannot find teamer $teamerNo"
        TeamerRepo.setByeRound(teamer.id, byeRound) >>
          TeamPlayerRepo.setByeRound(teamer.id, byeRound)
      }
    }.sequenceFu.void
  }

  //--------------------------------------------

  private def addBoards(round: TeamRound, boards: List[TeamBoard]): Funit = {
    TeamBoardRepo.removeByRound(round.id) >>
      TeamBoardRepo.insertMany(round.id, boards)
  }

  private def addAllBoards(rounds: List[TeamRound], boards: List[TeamBoard]): Funit = {
    TeamBoardRepo.insertManyByRounds(rounds.map(_.id), boards)
  }

  private def toRoundBoards(contest: TeamContest, round: TeamRound, paireds: List[TeamPaired], teamers: List[Teamer], players: Players): Fu[List[TeamBoard]] = {
    val totalBoard = contest.formalPlayers * paireds.size
    IdGenerator.games(totalBoard).map { gameIds =>
      toBoards(contest, round, paireds.filter(_.roundNo == round.no), teamers, players, gameIds)
    }
  }

  private def toAllRoundBoards(contest: TeamContest, rounds: List[TeamRound], paireds: List[TeamPaired], teamers: List[Teamer], players: Players): Fu[List[TeamBoard]] = {
    val totalBoard = contest.formalPlayers * paireds.size
    val roundBoard = paireds.size / rounds.size * contest.formalPlayers
    IdGenerator.games(totalBoard).map { gameIds =>
      rounds.zip(gameIds.grouped(roundBoard).toList).flatMap {
        case (round, roundGameIds) => toBoards(contest, round, paireds.filter(_.roundNo == round.no), teamers, players, roundGameIds)
      }
    }
  }

  private def toBoards(contest: TeamContest, round: TeamRound, paireds: List[TeamPaired], teamers: List[Teamer], players: Players, gameIds: List[Game.ID]): List[TeamBoard] = {
    val gameIdArray = gameIds.toArray
    var idIndex = 0
    paireds.flatMap { paired =>
      val firstWhitePlayers = players.filter(_.teamerId == paired.whiteTeamer.id).sortBy(_.no)
      val firstBlackPlayers = players.filter(_.teamerId == paired.blackTeamer.id).sortBy(_.no)
      (firstWhitePlayers zip firstBlackPlayers).zipWithIndex.map {
        case ((firstWhitePlayer, firstBlackPlayer), boardIndex) => {
          val boardNo = boardIndex + 1
          val whitePlayer = if (boardNo % 2 == 0) firstBlackPlayer else firstWhitePlayer
          val blackPlayer = if (boardNo % 2 == 0) firstWhitePlayer else firstBlackPlayer
          val whiteTeamer = teamers.find(_.id == whitePlayer.teamerId) err s"can not find teamer ${whitePlayer.teamerId}"
          val blackTeamer = teamers.find(_.id == blackPlayer.teamerId) err s"can not find teamer ${blackPlayer.teamerId}"
          val board = TeamBoard(
            _id = gameIdArray(idIndex),
            no = boardNo,
            contestId = contest.id,
            roundId = round.id,
            roundNo = round.no,
            pairedId = paired.id,
            pairedNo = paired.no,
            status = chess.Status.Created,
            whitePlayer = TeamBoard.MiniPlayer(whitePlayer.id, whitePlayer.no, whitePlayer.userId, whiteTeamer.id, whiteTeamer.no),
            blackPlayer = TeamBoard.MiniPlayer(blackPlayer.id, blackPlayer.no, blackPlayer.userId, blackTeamer.id, blackTeamer.no),
            startsAt = round.actualStartsAt
          )
          idIndex = idIndex + 1
          board
        }
      }
    }
  }

}

