package lila.teamContest

import lila.common.LightUser
import play.api.libs.json._
import lila.common.paginator.{ Paginator, PaginatorJson }

final class JsonView(lightUserApi: lila.user.LightUserApi) {

  def rule(r: TeamContest.Rule) = {
    Json.obj(
      "id" -> r.id,
      "name" -> r.name,
      "setup" -> Json.obj(
        "maxRound" -> r.setup.maxRound,
        "defaultRound" -> r.setup.defaultRound,
        "maxTeamers" -> r.setup.maxTeamers,
        "defaultTeamers" -> r.setup.defaultTeamers,
        "maxPlayers" -> r.setup.maxPlayers
      )
    )
  }

  def contestPage(pager: Paginator[TeamContest]) = {
    implicit val pagerWriter = Writes[TeamContest] { c => contest(c) }
    Json.obj(
      "paginator" -> PaginatorJson(pager)
    )
  }

  def contest(c: TeamContest) = {
    Json.obj(
      "id" -> c.id,
      "name" -> c.fullName,
      "startsAt" -> c.startsAt.toString("yyyy-MM-dd HH:mm")
    )
  }

  def rounds(list: List[TeamRound]) = {
    JsArray(list.map(r => round(r)))
  }

  def round(r: TeamRound) = {
    Json.obj(
      "id" -> r.id,
      "no" -> r.no,
      "boards" -> r.boards,
      "startsAt" -> r.actualStartsAt.toString("yyyy-MM-dd HH:mm")
    )
  }

  def boards(list: List[TeamBoard], markMap: Map[String, Option[String]]) = {
    val players = list.flatMap(_.players.map(_.userId))
    lightUserApi.asyncMany(players.distinct) map { users =>
      JsArray(
        list.map(b =>
          board(b, users, markMap))
      )
    }
  }

  def board(b: TeamBoard, lightUsers: List[Option[LightUser]], markMap: Map[String, Option[String]]) = {
    val white = lightUsers.find(_.exists(_.id == b.whitePlayer.userId)) match {
      case None => LightUser.fallback(b.whitePlayer.userId)
      case Some(p) => p | LightUser.fallback(b.whitePlayer.userId)
    }
    val black = lightUsers.find(_.exists(_.id == b.blackPlayer.userId)) match {
      case None => LightUser.fallback(b.whitePlayer.userId)
      case Some(p) => p | LightUser.fallback(b.blackPlayer.userId)
    }

    Json.obj(
      "id" -> b.id,
      "no" -> b.no,
      "white" -> markOrUsername(markMap, white),
      "black" -> markOrUsername(markMap, black),
      "result" -> b.resultFormat(false)
    )
  }

  private def markOrUsername(markMap: Map[String, Option[String]], lightUser: LightUser): String = {
    val mark = markMap.get(lightUser.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | lightUser.name
  }

}

