package lila.teamContest

import com.typesafe.config.Config
import lila.hub.{ Duct, DuctMap }
import lila.common.{ AtMost, Every, ResilientScheduler }
import akka.actor._
import lila.hub.actorApi.contest.GetContestBoard

import scala.concurrent.duration._

final class Env(
    config: Config,
    db: lila.db.Env,
    system: ActorSystem,
    roundMap: DuctMap[_],
    hub: lila.hub.Env,
    lightUserApi: lila.user.LightUserApi,
    historyApi: lila.history.HistoryApi,
    notifyApi: lila.notify.NotifyApi,
    clazzApi: lila.clazz.ClazzApi,
    scheduler: lila.common.Scheduler,
    asyncCache: lila.memo.AsyncCache.Builder,
    isOnline: lila.user.User.ID => Boolean
) {

  private implicit val sys = system
  private[teamContest] val settings = new {
    val CollectionContest = config getString "collection.contest"
    val CollectionRound = config getString "collection.round"
    val CollectionBoard = config getString "collection.board"
    val CollectionPlayer = config getString "collection.player"
    val CollectionTeamer = config getString "collection.teamer"
    val CollectionRequest = config getString "collection.request"
    val CollectionPaired = config getString "collection.paired"
    val CollectionScoreSheet = config getString "collection.scoresheet"
    val CollectionPlayerScoreSheet = config getString "collection.playerscoresheet"
    val CollectionForbidden = config getString "collection.forbidden"
    val CollectionImage = config getString "collection.image"
    val SocketTimeout = config duration "socket.timeout"
    val SocketName = config getString "socket.name"
    val ApiActorName = config getString "api_actor.name"
    val SequencerTimeout = config duration "sequencer.timeout"
    val NetBaseUrl = config getString "net.base_url"
    val ActorName = config getString "actor.name"
  }

  import settings._
  private[teamContest] lazy val contestColl = db(CollectionContest)
  private[teamContest] lazy val roundColl = db(CollectionRound)
  private[teamContest] lazy val boardColl = db(CollectionBoard)
  private[teamContest] lazy val teamerColl = db(CollectionTeamer)
  private[teamContest] lazy val playerColl = db(CollectionPlayer)
  private[teamContest] lazy val requestColl = db(CollectionRequest)
  private[teamContest] lazy val pairedColl = db(CollectionPaired)
  private[teamContest] lazy val scoreSheetColl = db(CollectionScoreSheet)
  private[teamContest] lazy val playerScoreSheetColl = db(CollectionPlayerScoreSheet)
  private[teamContest] lazy val forbiddenColl = db(CollectionForbidden)
  private[teamContest] lazy val imageColl = db(CollectionImage)

  lazy val pager = new TeamContestPager
  lazy val forms = new DataForm(captcher = hub.captcher)
  lazy val jsonView = new JsonView(lightUserApi)
  lazy val cached = new Cached

  private val photographer = new lila.db.Photographer(imageColl, "teamContest")

  private val trf: SwissTrf = new SwissTrf(NetBaseUrl)
  private val swissPairing = new SwissPairing(trf)
  private val roundRobinPairing = new RoundRobinPairing()
  private val pairingSystem = new PairingSystem(swissPairing, roundRobinPairing)
  private val pairingDirector = new PairingDirector(pairingSystem)

  private val contestSequencerMap = new DuctMap(
    mkDuct = _ => Duct.extra.lazyFu(5.seconds)(system),
    accessTimeout = SequencerTimeout
  )

  private val roundSequencerMap = new DuctMap(
    mkDuct = _ => Duct.extra.lazyFu(5.seconds)(system),
    accessTimeout = SequencerTimeout
  )

  lazy val roundApi = new TeamRoundApi(
    system = system,
    sequencers = roundSequencerMap,
    roundMap = roundMap,
    notifyApi = notifyApi,
    pairingDirector = pairingDirector
  )

  lazy val contestApi = new TeamContestApi(
    sequencers = contestSequencerMap,
    renderer = hub.renderer,
    notifyApi = notifyApi,
    roundApi = roundApi,
    roundMap = roundMap,
    reminder = new TeamContestReminder(system.lilaBus),
    pairingDirector = pairingDirector,
    photographer = photographer,
    isOnline = isOnline
  )(system)

  // 报名截止
  ResilientScheduler(
    every = Every(3 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 30 seconds
  ) { contestApi.enterStop }(system)

  // 比赛开始
  ResilientScheduler(
    every = Every(1 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 30 seconds
  ) { contestApi.start }(system)

  // Round开始
  ResilientScheduler(
    every = Every(1 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 31 seconds
  ) { roundApi.start }(system)

  // Game开始
  ResilientScheduler(
    every = Every(1 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 32 seconds
  ) { contestApi.launch }(system)

  // Game开始-通知
  ResilientScheduler(
    every = Every(10 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 30 seconds
  ) { contestApi.remind }(system)

  system.actorOf(Props(new Actor {
    def receive = {
      case GetContestBoard(gameId: String) => {
        val data = contestApi.fullContestBoard(gameId).awaitSeconds(3)
        sender ! data
      }
    }
  }), name = ActorName)

  system.lilaBus.subscribeFun('finishGame) {
    case lila.game.actorApi.FinishGame(game, _, _) => contestApi finishGame game
  }

}

object Env {

  lazy val current: Env = "teamContest" boot new Env(
    config = lila.common.PlayApp loadConfig "teamContest",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system,
    roundMap = lila.round.Env.current.roundMap,
    hub = lila.hub.Env.current,
    lightUserApi = lila.user.Env.current.lightUserApi,
    historyApi = lila.history.Env.current.api,
    notifyApi = lila.notify.Env.current.api,
    clazzApi = lila.clazz.Env.current.api,
    scheduler = lila.common.PlayApp.scheduler,
    asyncCache = lila.memo.Env.current.asyncCache,
    isOnline = lila.user.Env.current.isOnline
  )
}
