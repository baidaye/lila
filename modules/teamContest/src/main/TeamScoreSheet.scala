package lila.teamContest

case class TeamScoreSheet(
    _id: TeamScoreSheet.ID,
    contestId: TeamContest.ID,
    roundNo: TeamRound.No,
    teamerId: Teamer.ID,
    teamerNo: Teamer.No,
    score: Double,
    rank: Int,
    btssScores: List[TeamBtss.TeamBtssScore],
    cancelled: Boolean = false
) {

  def id = _id

}

object TeamScoreSheet {

  type ID = String

  private[teamContest] def makeId(teamerId: Teamer.ID, no: TeamRound.No) = teamerId + "@" + no

}
