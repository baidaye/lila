package lila.teamContest

import lila.user.User

case class TeamPlayerScoreSheet(
    _id: TeamPlayerScoreSheet.ID,
    contestId: TeamContest.ID,
    roundNo: TeamRound.No,
    playerUid: User.ID,
    playerNo: TeamPlayer.No,
    teamerId: Teamer.ID,
    score: Double,
    rank: Int,
    btssScores: List[TeamPlayerBtss.TeamPlayerBtssScore],
    cancelled: Boolean = false
) {

  def id = _id

}

object TeamPlayerScoreSheet {

  type ID = String

  private[teamContest] def makeId(playerId: String, no: TeamRound.No) = playerId + "@" + no

}
