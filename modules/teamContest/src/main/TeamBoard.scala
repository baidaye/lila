package lila.teamContest

import chess.Color
import lila.game.Game
import lila.user.User
import org.joda.time.DateTime

case class TeamBoard(
    _id: Game.ID,
    no: TeamBoard.No,
    contestId: TeamContest.ID,
    roundId: TeamRound.ID,
    roundNo: TeamRound.No,
    pairedId: TeamPaired.ID,
    pairedNo: TeamPaired.No,
    status: chess.Status,
    whitePlayer: TeamBoard.MiniPlayer,
    blackPlayer: TeamBoard.MiniPlayer,
    startsAt: DateTime,
    reminded: Boolean = false,
    turns: Option[Int] = None, // playedTurns
    finishAt: Option[DateTime] = None,
    result: Option[TeamBoard.Result] = None
) {

  def id = _id
  def gameId = _id
  def isCreated = status.id == chess.Status.Created.id
  def isStarted = status.id == chess.Status.Started.id
  def isFinished = status.id >= chess.Status.Aborted.id
  def shouldStart = startsAt.getMillis <= DateTime.now.getMillis
  def is(b: TeamBoard) = b.id == id
  def secondsToStart = (startsAt.getSeconds - nowSeconds).toInt atLeast 0
  def startStatus = secondsToStart |> { s => "%02d:%02d".format(s / 60, s % 60) }
  def canCancel = isCreated && secondsToStart > 180
  def canReplace = isCreated && secondsToStart > 60

  def player(color: Color): TeamBoard.MiniPlayer =
    color.fold(whitePlayer, blackPlayer)

  def opponentOf(id: TeamPlayer.ID): Option[TeamPlayer.ID] =
    if (id == whitePlayer.id) blackPlayer.id.some
    else if (id == blackPlayer.id) whitePlayer.id.some
    else none

  def colorOf(id: TeamPlayer.ID): Option[Color] =
    if (id == whitePlayer.id) Color.White.some
    else if (id == blackPlayer.id) Color.Black.some
    else none

  def colorOfById(playerId: TeamPlayer.ID): Color =
    if (playerId == whitePlayer.id) Color.White
    else if (playerId == blackPlayer.id) Color.Black
    else Color.White

  def colorOfByUserId(userId: User.ID): Color =
    if (userId == whitePlayer.userId) Color.White
    else if (userId == blackPlayer.userId) Color.Black
    else Color.White

  def contains(id: TeamPlayer.ID): Boolean =
    whitePlayer.id == id || blackPlayer.id == id

  def containsByUser(userId: User.ID): Boolean =
    whitePlayer.userId == userId || blackPlayer.userId == userId

  def containsByTeamer(teamerId: Teamer.ID): Boolean =
    whitePlayer.teamerId == teamerId || blackPlayer.teamerId == teamerId

  def isSigned(id: TeamPlayer.ID): Boolean =
    (whitePlayer.id == id && whitePlayer.signed) || (blackPlayer.id == id && blackPlayer.signed)

  def players = List(whitePlayer, blackPlayer)

  def winner: Option[TeamBoard.MiniPlayer] = players find (_.wins)

  def blackIsWinner = blackPlayer.wins

  def isDraw = winner.isEmpty

  def isWin(id: TeamPlayer.ID) = winner.??(_.id == id)

  def accountable = turns.exists(_ >= 2)

  def resultShow(reverse: Boolean) = status match {
    case chess.Status.Created => "等待开赛"
    case chess.Status.Started => "比赛中"
    case _ => resultFormat(reverse)
  }

  def resultFormat(reverse: Boolean) =
    (if (reverse) result.map(_.reverse) else result.map(_.id)) | {
      if (whitePlayer.wins) if (reverse) "0-1" else "1-0"
      else if (blackPlayer.wins) if (reverse) "1-0" else "0-1"
      else "1/2-1/2"
    }

  def playerResult(id: TeamPlayer.ID) = {
    colorOf(id).?? { color =>
      result.map {
        _.resultOf(color)
      }
    }
  }

  def isOnlyOpponentAbsent(playerNo: TeamPlayer.No) = {
    (playerNo == whitePlayer.no && result.??(_ == TeamBoard.Result.BlackAbsent)) || (playerNo == blackPlayer.no && result.??(_ == TeamBoard.Result.WhiteAbsent))
  }

}

object TeamBoard {

  type No = Int

  case class FullInfo(board: TeamBoard, round: TeamRound, contest: TeamContest) {
    def fullName = s"${contest.fullName} - 第${round.no}轮"
    def boardName = s"第${round.no}轮 #${board.no}"

    def canStarted = (contest.isStarted || contest.isEnterStopped) && (round.isStarted || round.isPublished) && board.shouldStart
  }

  case class WithPov(board: TeamBoard, pov: lila.game.Pov)

  private[teamContest] case class Sign(isAgent: Boolean, signedAt: DateTime)

  private[teamContest] case class MiniPlayer(id: TeamPlayer.ID, no: TeamPlayer.No, userId: User.ID, teamerId: Teamer.ID, teamerNo: Teamer.No, isWinner: Option[Boolean] = None, sign: Option[Sign] = None, absent: Option[Boolean] = None) {
    // absent -> player.quitOrKickOrManualAbsent = leave || quit || kick || manualAbsent
    def wins = isWinner getOrElse false
    def signed = sign.isDefined
  }

  private[teamContest] case class BoardWithPlayer(board: TeamBoard, player: TeamPlayer)

  private[teamContest] sealed abstract class Outcome(val id: String, val name: String) {

    def isWin = this == Outcome.Win
  }
  private[teamContest] object Outcome {
    case object Win extends Outcome("win", "胜")
    case object Loss extends Outcome("loss", "负")
    case object Draw extends Outcome("draw", "和")
    case object Bye extends Outcome("bey", "轮空")
    case object NoStart extends Outcome("no-start", "没有移动")
    case object Quit extends Outcome("quit", "退赛")
    case object Kick extends Outcome("kick", "踢出")
    case object ManualAbsent extends Outcome("manual-absent", "弃权")
    case object Substitute extends Outcome("substitute", "替补") // 替补选手本轮不参加比赛

    val all = List(Win, Loss, Draw, Bye, NoStart, Quit, Kick, ManualAbsent, Substitute)

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Outcome = byId get id err s"Bad Outcome $id"

  }

  private[teamContest] sealed abstract class Result(val id: String, val name: String) {
    def whiteResult: String
    def blackResult: String
    def reverse: String
    def resultOf(color: Color): String
  }
  object Result {
    case object WhiteWin extends Result("1-0", "白方胜") {
      override def whiteResult = "1"
      override def blackResult = "0"
      override def reverse = "0-1"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object BlackWin extends Result("0-1", "黑方胜") {
      override def whiteResult = "0"
      override def blackResult = "1"
      override def reverse = "1-0"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object Draw extends Result("1/2-1/2", "平局") {
      override def whiteResult = "1/2"
      override def blackResult = "1/2"
      override def reverse = "1/2-1/2"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object BlackAbsent extends Result("+-", "黑弃权") {
      override def whiteResult = "+"
      override def blackResult = "-"
      override def reverse = "-+"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object WhiteAbsent extends Result("-+", "白弃权") {
      override def whiteResult = "-"
      override def blackResult = "+"
      override def reverse = "+-"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object AllAbsent extends Result("--", "双弃权") {
      override def whiteResult = "-"
      override def blackResult = "-"
      override def reverse = "--"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }

    val all = List(WhiteWin, BlackWin, Draw, BlackAbsent, WhiteAbsent, AllAbsent)

    def choice = all.map { r => r.id -> s"${r.id}（${r.name}）" }

    def keys = all.map(_.id).toSet

    def byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Result = byId get id err s"Bad Result $id"

  }

}
