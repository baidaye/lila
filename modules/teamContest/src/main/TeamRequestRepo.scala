package lila.teamContest

import lila.db.dsl._
import lila.user.User

object TeamRequestRepo {

  private[teamContest] lazy val coll = Env.current.requestColl

  import BSONHandlers.RequestHandler

  def insert(request: TeamRequest): Funit = coll.insert(request).void

  def byId(id: TeamRequest.ID): Fu[Option[TeamRequest]] = coll.byId[TeamRequest](id)

  def exists(contestId: TeamContest.ID, userId: User.ID): Fu[Boolean] =
    coll.exists(userQuery(contestId, userId))

  def find(contestId: TeamContest.ID, userId: User.ID): Fu[List[TeamRequest]] =
    coll.find(userQuery(contestId, userId)).sort($doc("status" -> 1, "date" -> -1)).list[TeamRequest]()

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamRequest]] =
    coll.find(contestQuery(contestId)).sort($doc("status" -> 1, "date" -> -1)).list[TeamRequest]()

  def countByContest(contestId: TeamContest.ID): Fu[Int] =
    coll.countSel(contestQuery(contestId))

  def getByUserId(userId: User.ID): Fu[List[TeamRequest]] =
    coll.find($doc("userId" -> userId)).list[TeamRequest]()

  def remove(id: TeamRequest.ID) =
    coll.remove($id(id)).void

  def setStatus(id: TeamRequest.ID, status: TeamRequest.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def userQuery(contestId: TeamContest.ID, userId: User.ID) = $doc("contestId" -> contestId, "userId" -> userId)
  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

}
