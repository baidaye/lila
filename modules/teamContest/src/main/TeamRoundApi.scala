package lila.teamContest

import akka.actor.ActorSystem
import org.joda.time.DateTime
import chess.{ Black, Color, White }
import scala.concurrent.duration._
import lila.game.{ Game, GameRepo }
import lila.hub.{ Duct, DuctMap }
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import lila.round.actorApi.round.{ AbortForce, ContestBoardClear }
import lila.hub.actorApi.contest.ContestBoardSigned
import lila.hub.actorApi.calendar.{ CalendarCreate, CalendarRemove, CalendarsCreate, CalendarsRemove }
import lila.user.User

class TeamRoundApi(
    system: ActorSystem,
    sequencers: DuctMap[_],
    roundMap: DuctMap[_],
    notifyApi: NotifyApi,
    pairingDirector: PairingDirector
) {

  private val bus = system.lilaBus

  def pureTeamers(contest: TeamContest, round: TeamRound): Fu[(List[Teamer], List[TeamPlayer])] = {
    val no = round.no
    for {
      teamers <- TeamerRepo.getByContest(contest.id)
      players <- TeamPlayerRepo.getFormalByContest(contest.id)
      //  之前匹配过，所以生成了对应轮次的Outcome，再次匹配时这些Outcome先清除（因为缺席状态可能已经改变）
      historyOutcomeTeamers = teamers.filter(p => p.isAbsentIgnoreManual(round.no))
      historyOutcomePlayers = historyOutcomeTeamers.flatMap { teamer =>
        players.filter(_.teamerId == teamer.id)
      }

      //  上轮 离开、退赛、踢出，本轮继续
      absentTeamers = teamers.filter(p => p.absent /* && p.roundOutcome(no).isEmpty*/ )
      quitTeamers = absentTeamers.filter(_.quit)
      kickTeamers = absentTeamers.filter(_.kick)
      _ <- historyOutcomeTeamers.nonEmpty.?? { TeamerRepo.removeTeamersLastOutcome(contest, historyOutcomeTeamers, no) }
      _ <- historyOutcomeTeamers.nonEmpty.?? { TeamPlayerRepo.removePlayersLastOutcome(contest, players, no) }
      _ <- quitTeamers.nonEmpty.?? { TeamerRepo.setZeroScoreOutcomes(contest.id, no, quitTeamers.map(_.no), TeamBoard.Outcome.Quit) }
      _ <- quitTeamers.nonEmpty.?? { TeamPlayerRepo.setZeroScoreOutcomes(contest.id, no, quitTeamers, TeamBoard.Outcome.Quit) }
      _ <- kickTeamers.nonEmpty.?? { TeamerRepo.setZeroScoreOutcomes(contest.id, no, kickTeamers.map(_.no), TeamBoard.Outcome.Kick) }
      _ <- kickTeamers.nonEmpty.?? { TeamPlayerRepo.setZeroScoreOutcomes(contest.id, no, kickTeamers, TeamBoard.Outcome.Kick) }

      // 做完上面操作再查询
      newTeamers <- TeamerRepo.getByContest(contest.id)
      newPlayers <- TeamPlayerRepo.getFormalByContest(contest.id)
    } yield (newTeamers, newPlayers)
  }

  def pairing(contest: TeamContest, round: TeamRound, publishScoreAndFinish: TeamContest => Funit): Fu[Boolean] = {
    lg(contest, round.some, "比赛编排", None)
    pureTeamers(contest, round) flatMap {
      case (teamers, players) => {
        pairingDirector.roundPairing(contest, round, teamers, players) flatMap {
          case None => {
            lg(contest, round.some, "比赛编排失败", "Pairing impossible under the current rules -> force finished!".some, true)
            pairingFailed(contest, round, publishScoreAndFinish).inject(false)
          }
          case Some(paireds) => {
            if (paireds.isEmpty) {
              lg(contest, round.some, "比赛编排失败", "paireds.isEmpty -> force finished!".some, true)
              pairingFailed(contest, round, publishScoreAndFinish).inject(false)
            } else {
              val bs = paireds.map(_.teamerNos)
              lg(contest, round.some, "比赛编排完成", s"$bs".some)
              contest.autoPairing.?? { autoPublish(contest, round) }.inject(true)
            }
          }
        }
      }
    }
  }

  def pairingFailed(contest: TeamContest, round: TeamRound, publishScoreAndFinish: TeamContest => Funit): Funit = {
    TeamContestRepo.setCurrentRound(contest.id, Math.max(round.no - 1, 1)) >>
      TeamContestRepo.setAllRoundFinished(contest.id) >>
      contest.autoPairing.?? {
        publishScoreAndFinish(contest)
      }
  }

  def autoPublish(contest: TeamContest, round: TeamRound): Funit = {
    lg(contest, round.some, "比赛编排发布（自动）", none)
    toPublish(contest, round)
  }

  def publish(contest: TeamContest, round: TeamRound): Funit = {
    lg(contest, round.some, "比赛编排发布（手动）", none)
    toPublish(contest, round)
  }

  private def toPublish(contest: TeamContest, round: TeamRound): Funit = {
    for {
      _ <- contest.isEnterStopped.?? {
        lg(contest, none, "round publish - 比赛开始", none)
        TeamContestRepo.setStatus(contest.id, TeamContest.Status.Started)
      }
      boards <- TeamBoardRepo.getByRound(round.id)
      teamers <- TeamerRepo.getByContest(contest.id)
      players <- TeamPlayerRepo.getByContest(contest.id)
      _ <- addGames(contest, round, boards, players)
      _ <- TeamRoundRepo.setStatus(round.id, TeamRound.Status.Published)
      _ <- contest.isRoundRobin.?? { TeamPlayerRepo.setSubstituteOutcomes(contest.id, round.no) }
      _ <- setPlayerByeByByeRound(contest, round.no, teamers, players)
      _ <- setGameAutoFinish(contest, round, boards, teamers, players)
      _ <- TeamerRepo.unAbsentByContest(contest.id)
    } yield {
      publishCalendar(contest, boards)
      publishCoachCalendar(contest, round, teamers, boards.flatMap(_.players.map(_.userId)))
    }
  }

  private def addGames(contest: TeamContest, round: TeamRound, boards: List[TeamBoard], players: List[TeamPlayer]): Funit = {
    //logger.info(s"addGames start contest：${contest}, round：${round}, boards：${boards.size}, players：${players.size}")
    val playerMap = TeamPlayer.toMap(players)
    val games = boards.map(makeGame(contest, round, playerMap))
    //logger.info(s"addGames over contest：${contest}, round：${round}, boards：${boards.size}, players：${players.size}, games：${games.size}")
    lila.common.Future.applySequentially(games) { game =>
      GameRepo.insertDenormalized(game)
    }
  }

  private def makeGame(contest: TeamContest, round: TeamRound, players: Map[TeamPlayer.ID, TeamPlayer])(board: TeamBoard): Game = {
    //logger.info(s"makeGame start contest：${contest}, round：${round}, board：${board.id}")
    val game = Game.make(
      chess = chess.Game(
        variantOption = Some {
          if (contest.position.initial) chess.variant.Standard
          else chess.variant.FromPosition
        },
        fen = contest.position.fen.some
      ) |> { g =>
          val turns = g.player.fold(0, 1)
          g.copy(
            clock = contest.clock.toClock.some,
            turns = turns,
            startedAtTurn = turns
          )
        },
      whitePlayer = makePlayer(White, players get board.whitePlayer.id err s"Missing board white $board"),
      blackPlayer = makePlayer(Black, players get board.blackPlayer.id err s"Missing board black $board"),
      mode = chess.Mode(contest.mode.rated),
      source = lila.game.Source.Contest,
      pgnImport = None,
      movedAt = round.actualStartsAt.some
    )
      .withId(board.gameId)
      .withContestIsTeam(true)
      .withContestId(contest.id)
      .withContestCanLateMinutes(contest.canLateMinute)
    //logger.info(s"makeGame over contest：${contest}, round：${round}, board：${board.id}, game：${game}")
    game
  }

  private def makePlayer(color: Color, player: TeamPlayer): lila.game.Player =
    lila.game.Player.make(color, player.userId, player.rating, player.provisional)

  // 根据 ByeRound 属性更新Bey Outcome（循环赛）
  def setPlayerByeByByeRound(contest: TeamContest, roundNo: TeamRound.No, teamers: List[Teamer], players: List[TeamPlayer]): Funit =
    contest.isRoundRobin.?? {
      val newByeTeamers = teamers.filter(_.byeRound.contains(roundNo))
      val newByeNos = newByeTeamers.map(_.no)
      val oldByeTeamers = teamers.filter(_.roundOutcome(roundNo).contains(TeamBoard.Outcome.Bye))
      val oldByeNos = oldByeTeamers.map(_.no)

      oldByeTeamers.filter(p => !newByeNos.contains(p.no)).map { teamer =>
        TeamerRepo.update(
          teamer.copy(
            outcomes = teamer.removeOutcomeByRound(roundNo),
            roundScores = teamer.removeScoreByRound(roundNo)
          ) |> { p =>
              p.copy(
                score = p.allScore(contest.isRoundRobin)
              )
            }
        ) >> {
            players.filter(_.teamerId == teamer.id).map { player =>
              TeamPlayerRepo.update(
                player.copy(
                  outcomes = player.removeOutcomeByRound(roundNo),
                  byeRound = player.removeByeRoundByRound(roundNo)
                ) |> { p =>
                    p.copy(score = p.allScore(contest.isRoundRobin))
                  }
              )
            }.sequenceFu.void
          }
      }.sequenceFu.void >> {
        newByeTeamers.filter(p => !oldByeNos.contains(p.no)).map { teamer =>
          TeamerRepo.update(
            teamer.copy(
              outcomes = teamer.setOutcomeByRound(roundNo, TeamBoard.Outcome.Bye),
              roundScores = teamer.removeScoreByRound(roundNo)
            ) |> { p =>
                p.copy(
                  score = p.allScore(contest.isRoundRobin)
                )
              }
          ) >> {
              players.filter(p => p.teamerId == teamer.id && p.formal).map { player =>
                TeamPlayerRepo.update(
                  player.copy(
                    outcomes = player.setOutcomeByRound(roundNo, TeamBoard.Outcome.Bye)
                  ) |> { p =>
                      p.copy(score = p.allScore(contest.isRoundRobin))
                    }
                )
              }.sequenceFu.void
            }
        }.sequenceFu.void
      }
    }

  def setGameAutoFinish(contest: TeamContest, round: TeamRound, boards: List[TeamBoard], teamers: List[Teamer], players: List[TeamPlayer]): Funit =
    contest.isRoundRobin.?? {
      val absentIds = teamers.filter(_.quitOrKickOrManualAbsent).map(_.id)
      val playerUserIds = players.filter(p => absentIds.contains(p.teamerId)).map(_.userId)
      boards.filter(b => b.roundNo == round.no && playerUserIds.exists(b.containsByUser)).distinct.map { board =>
        playerUserIds.contains(board.whitePlayer.userId).?? {
          TeamBoardRepo.setAbsent(board.id, chess.Color.White)
        } >> playerUserIds.contains(board.blackPlayer.userId).?? {
          TeamBoardRepo.setAbsent(board.id, chess.Color.Black)
        } >>- roundMap.tell(board.id, AbortForce)
      }.sequenceFu.void
    }

  def cancelPublish(contest: TeamContest, round: TeamRound, boards: List[TeamBoard]): Funit = {
    lg(contest, round.some, "取消比赛编排发布（手动）", none)
    contest.rule.flow.cancelPairingPublish ?? {
      for {
        teamers <- TeamerRepo.getByContest(contest.id)
        manualAbsentTeamers = teamers.filter(_.isManualAbsent(round.no))
        _ <- removeGames(boards)
        _ <- TeamRoundRepo.setStatus(round.id, TeamRound.Status.Pairing)
        - <- TeamerRepo.setManualAbsent(manualAbsentTeamers.map(_.id))
      } yield {
        removeCalendar(contest, boards)
        removeCoachCalendar(contest, round, teamers)
        boards.foreach { board =>
          roundMap.tell(board.id, ContestBoardClear)
        }
      }
    }
  }

  private def removeGames(boards: List[TeamBoard]): Funit =
    GameRepo.removeByIds(boards.map(_.id))

  def manualAbsent(contest: TeamContest, round: TeamRound, joins: List[Teamer.ID], absents: List[Teamer.ID]): Funit = {
    lg(contest, round.some, "手动弃权", s"Joins: $joins, Absents: $absents".some)
    val roundNo = round.no
    TeamPlayerRepo.getFormalByContest(contest.id) flatMap { players =>
      TeamerRepo.byIds(joins).flatMap { teamers =>
        teamers.filter(t => t.absent && t.manualAbsent && !t.absentOr && t.roundOutcome(roundNo).??(_ == TeamBoard.Outcome.ManualAbsent)).map { teamer =>
          TeamerRepo.update(
            teamer.copy(
              absent = false,
              manualAbsent = false,
              outcomes = teamer.removeOutcomeByRound(roundNo),
              roundScores = teamer.removeScoreByRound(roundNo)
            ) |> { teamer =>
                teamer.copy(score = teamer.allScore(contest.isRoundRobin))
              }
          ) >> {
              players.filter(_.teamerId == teamer.id).map { player =>
                TeamPlayerRepo.update(
                  player.copy(
                    outcomes = player.removeOutcomeByRound(roundNo)
                  ) |> { p =>
                      p.copy(score = p.allScore(contest.isRoundRobin))
                    }
                )
              }.sequenceFu.void
            }
        }.sequenceFu.void
      } >> TeamerRepo.byIds(absents).flatMap { teamer =>
        teamer.filter(t => !t.manualAbsent && t.roundOutcome(roundNo).isEmpty).map { teamer =>
          TeamerRepo.update(
            teamer.copy(
              absent = true,
              manualAbsent = true,
              outcomes = teamer.setOutcomeByRound(roundNo, TeamBoard.Outcome.ManualAbsent),
              roundScores = teamer.setScoreByRound(roundNo, teamer.scoreByOutcome(TeamBoard.Outcome.ManualAbsent, contest.isRoundRobin))
            ) |> { teamer =>
                teamer.copy(score = teamer.allScore(contest.isRoundRobin))
              }
          ) >> {
              players.filter(_.teamerId == teamer.id).map { player =>
                TeamPlayerRepo.update(
                  player.copy(
                    outcomes = player.setOutcomeByRound(roundNo, TeamBoard.Outcome.ManualAbsent)
                  ) |> { p =>
                      p.copy(score = p.allScore(contest.isRoundRobin))
                    }
                )
              }.sequenceFu.void
            }
        }.sequenceFu.void
      }
    }
  }

  def manualResult(contest: TeamContest, round: TeamRound, board: TeamBoard, r: String): Funit = {
    lg(contest, round.some, "手动设置成绩", s"第 ${board.no} 台 $r".some)
    val result = TeamBoard.Result(r)
    for {
      whiteOption <- TeamPlayerRepo.byId(board.whitePlayer.id)
      blackOption <- TeamPlayerRepo.byId(board.blackPlayer.id)
    } yield (whiteOption |@| blackOption).tupled ?? {
      case (white, black) => {
        val whiteOutcome = whitePlayerOutcome(result)
        val blackOutcome = blackPlayerOutcome(result)
        val winner = boardWinner(result)
        val newWhite = white.manualResult(board.roundNo, whiteOutcome, contest.isRoundRobin)
        val newBlack = black.manualResult(board.roundNo, blackOutcome, contest.isRoundRobin)
        (for {
          _ <- TeamPlayerRepo.update(newWhite)
          _ <- TeamPlayerRepo.update(newBlack)
          _ <- TeamBoardRepo.setResult(board.id, winner, result)
        } yield ()) >> pairedFinish(contest, board.pairedId) >> resetAllScore(contest, round)
      }
    }
    Thread.sleep(500)
    funit
  }

  def pairedFinish(contest: TeamContest, pairedId: TeamPaired.ID): Funit = {
    TeamPairedRepo.byId(pairedId).flatMap {
      case None => {
        logger.error(s"can not find paired $pairedId")
        fufail(s"can not find paired $pairedId")
      }
      case Some(paired) => for {
        whiteTeamerOption <- TeamerRepo.byId(paired.whiteTeamer.id)
        blackTeamerOption <- TeamerRepo.byId(paired.blackTeamer.id)
        whitePlayers <- TeamPlayerRepo.getByTeamer(paired.whiteTeamer.id)
        blackPlayers <- TeamPlayerRepo.getByTeamer(paired.blackTeamer.id)
        whiteTeamer = whiteTeamerOption err s"can not find teamer ${paired.whiteTeamer.id}"
        blackTeamer = blackTeamerOption err s"can not find teamer ${paired.blackTeamer.id}"
        finishedPaired = paired.finish(contest.isRoundRobin, whitePlayers, blackPlayers)
        newWhiteTeamer = whiteTeamer.finish(paired.roundNo, finishedPaired.whiteOutcome, finishedPaired.whiteTeamer.score, contest.isRoundRobin)
        newBlackTeamer = blackTeamer.finish(paired.roundNo, finishedPaired.blackOutcome, finishedPaired.blackTeamer.score, contest.isRoundRobin)
        _ <- TeamerRepo.update(newWhiteTeamer)
        _ <- TeamerRepo.update(newBlackTeamer)
        res <- TeamPairedRepo.update(finishedPaired)
      } yield res
    }
  }

  // 重置所有已发布的轮次成绩册
  def resetAllScore(contest: TeamContest, round: TeamRound): Funit =
    TeamRoundRepo.getAllPublishedRounds(contest.id, round.no).flatMap {
      _.map { round =>
        lg(contest, round.some, "计算成绩册", none)
        computePlayerScore(contest, round.no) >> computeTeamerScore(contest, round.no)
      }.sequenceFu.void
    }

  def setStartsTime(contest: TeamContest, round: TeamRound, st: DateTime): Funit = {
    lg(contest, round.some, "设置轮次开始时间", s"${st.toString("yyyy-MM-dd HH:mm")}".some)
    TeamRoundRepo.setStartsTime(round.id, st)
    TeamBoardRepo.setStartsTimeByRound(round.id, st) >> round.isPublished.?? {
      resetCalendarByRound(contest, round, st) >>- {
        TeamerRepo.getByContest(contest.id).foreach { teamers =>
          resetCoachCalendarByRound(contest, round, teamers, st)
        }
      }
    }
  }

  def setBoardTime(contest: TeamContest, round: TeamRound, board: TeamBoard, st: DateTime): Funit = {
    lg(contest, round.some, "设置对局开始时间", s"第 ${board.no} 台，${st.toString("yyyy-MM-dd HH:mm")}".some)
    TeamBoardRepo.setStartsTime(board.id, st) >>
      GameRepo.setMoveAt(board.id, st) >>-
      resetCalendar(contest, board.copy(startsAt = st))
  }

  def setBoardSign(board: TeamBoard, userId: User.ID, isAgent: Boolean): Funit = {
    TeamBoardRepo.setSign(board.id, board.colorOfByUserId(userId), isAgent) >>-
      roundMap.tell(board.id, ContestBoardSigned(board.id, userId))
  }

  def replaceFormal(contest: TeamContest, round: TeamRound, board: TeamBoard, teamer: Teamer, from: TeamPlayer, target: TeamPlayer): Funit = {
    lg(contest, round.some, "替换棋手", s"${teamer.name} fr：${from.no} to：${target.no}".some)
    val newFrom = from.copy(
      outcomes = from.setOutcomeByRound(round.no, TeamBoard.Outcome.Substitute)
    ) |> { p =>
        p.copy(score = p.allScore(contest.isRoundRobin))
      }

    val newTarget = target.copy(
      outcomes = target.removeOutcomeByRound(round.no)
    ) |> { p =>
        p.copy(score = p.allScore(contest.isRoundRobin))
      }

    val newBoard = board.copy(
      whitePlayer = if (board.whitePlayer.id == from.id) TeamBoard.MiniPlayer(target.id, target.no, target.userId, target.teamerId, teamer.no) else board.whitePlayer,
      blackPlayer = if (board.blackPlayer.id == from.id) TeamBoard.MiniPlayer(target.id, target.no, target.userId, target.teamerId, teamer.no) else board.blackPlayer
    )
    for {
      boardPlayers <- TeamPlayerRepo.byIds(board.players.map(_.id))
      _ <- TeamPlayerRepo.update(newFrom)
      _ <- TeamPlayerRepo.update(newTarget)
      _ <- TeamBoardRepo.update(newBoard)
      _ <- GameRepo.remove(board.id)
      _ <- addGames(contest, round, List(newBoard), boardPlayers :+ target)
      //_ <- contest.isRoundRobin.?? { replaceRoundRobinFormal(contest, round, board, teamer, from, target) }
    } yield {
      bus.publish(CalendarRemove(s"${board.id}@${from.userId}"), 'calendarRemoveBus)
      if (contest.isCreator(target.userId) || teamer.isLeader(target.userId)) {
        bus.publish(CalendarRemove(s"${round.id}@${target.userId}"), 'calendarRemoveBus)
      }

      system.scheduler.scheduleOnce(3 seconds) {
        bus.publish(makeCalendar(contest, newBoard, newTarget.userId), 'calendarCreateBus)
        if (contest.isCreator(from.userId) || teamer.isLeader(from.userId)) {
          bus.publish(makeCoachCalendar(contest, round, from.userId), 'calendarCreateBus)
        }
      }
    }
  }

  /*  def replaceRoundRobinFormal(contest: TeamContest, round: TeamRound, board: TeamBoard, teamer: Teamer, from: TeamPlayer, target: TeamPlayer): Funit = {
    TeamBoardRepo.getByContestGtRound(contest.id, round.no) flatMap { borads =>
      borads.nonEmpty.?? {
        val newBoards = borads.map { board =>
          board.copy(
            whitePlayer = if (board.whitePlayer.id == from.id) TeamBoard.MiniPlayer(target.id, target.no, target.userId, target.teamerId, teamer.no) else board.whitePlayer,
            blackPlayer = if (board.blackPlayer.id == from.id) TeamBoard.MiniPlayer(target.id, target.no, target.userId, target.teamerId, teamer.no) else board.blackPlayer
          )
        }

        for {
          boardPlayers <- TeamPlayerRepo.byIds(newBoards.flatMap(_.players.map(_.id)).distinct)
          _ <- TeamBoardRepo.bulkUpdate(newBoards)
        } yield ()
      }
    }
  }*/

  def start: Funit =
    TeamRoundRepo.published map { rounds =>
      rounds foreach { round =>
        if (round.shouldStart) {
          TeamContestRepo.byId(round.contestId) foreach {
            _.foreach(contest =>
              if (contest.isStarted) {
                setStart(contest, round.id)
              })
          }
        }
      }
    }

  def setStart(contest: TeamContest, id: TeamRound.ID): Unit =
    Sequencing(id)(TeamRoundRepo.publishedById) { round =>
      lg(contest, round.some, "开始", none)
      TeamRoundRepo.setStatus(round.id, TeamRound.Status.Started) >>
        TeamPairedRepo.setStatusByRound(round.id, TeamPaired.Status.Started)
    }

  def publishResult(contest: TeamContest, id: TeamRound.ID, no: TeamRound.No): Fu[TeamContest] = {
    lg(contest, TeamRound.make(no, contest.id, DateTime.now).some, "比赛成绩发布", none)
    for {
      _ <- computePlayerScore(contest, no)
      _ <- computeTeamerScore(contest, no)
      _ <- TeamRoundRepo.setStatus(id, TeamRound.Status.PublishResult)
      contest <- if (contest.isAllRoundFinished) {
        TeamContestRepo.setAllRoundFinished(contest.id) inject contest.copy(allRoundFinished = true)
      } else fuccess(contest)
      contest <- if (!contest.isAllRoundFinished) {
        TeamContestRepo.setCurrentRound(contest.id, no + 1) inject contest.copy(currentRound = no + 1)
      } else fuccess(contest.copy(currentRound = no + 1))
    } yield contest
  }

  def computeTeamerScore(contest: TeamContest, no: TeamRound.No): Funit = {
    for {
      teamers <- TeamerRepo.getByContest(contest.id)
      paireds <- TeamPairedRepo.getByContestLteRound(contest.id, no)
      boards <- TeamBoardRepo.getByContestLteRound(contest.id, no)
    } yield {
      val teamerDeadlineRound = teamers.map { teamer =>
        teamer.copy(
          score = teamer.roundScoreWithCurr(no, contest.isRoundRobin),
          outcomes = teamer.outcomes.take(no)
        )
      }
      val teamerBtssScores = TeamBtss.TeamerBtssScores(teamerDeadlineRound.map(TeamBtss.TeamerBtssScore(_)))
      val newTeamerBtssScores = TeamBtss.default.foldLeft(teamerBtssScores) {
        case (old, btss) => btss.score(paireds, boards, old)
      }

      val scoreSheets = newTeamerBtssScores.sort.zipWithIndex.map {
        case (teamerBtssScore, i) => TeamScoreSheet(
          _id = TeamScoreSheet.makeId(teamerBtssScore.teamer.id, no),
          contestId = teamerBtssScore.teamer.contestId,
          roundNo = no,
          teamerId = teamerBtssScore.teamer.id,
          teamerNo = teamerBtssScore.teamer.no,
          score = teamerBtssScore.teamer.score,
          rank = i + 1,
          btssScores = teamerBtssScore.btsss,
          cancelled = teamerBtssScore.teamer.cancelled
        )
      }

      TeamScoreSheetRepo.removeByRound(contest.id, no) >> TeamScoreSheetRepo.insertMany(scoreSheets)
    }
  }

  def computePlayerScore(contest: TeamContest, no: TeamRound.No): Funit = {
    for {
      players <- TeamPlayerRepo.getByContest(contest.id)
      boards <- TeamBoardRepo.getByContestLteRound(contest.id, no)
    } yield {
      val playerDeadlineRound = players.map { player =>
        player.copy(
          score = player.roundScoreWithCurr(no, contest.isRoundRobin),
          outcomes = player.outcomes.take(no)
        )
      }
      val playerBtssScores = TeamPlayerBtss.PlayerBtssScores(playerDeadlineRound.map(TeamPlayerBtss.PlayerBtssScore(_)))
      val newPlayerBtssScores = TeamPlayerBtss.default.foldLeft(playerBtssScores) {
        case (old, btss) => btss.score(boards, old)
      }

      val scoreSheets = newPlayerBtssScores.sort.zipWithIndex.map {
        case (playerBtssScore, i) => TeamPlayerScoreSheet(
          _id = TeamPlayerScoreSheet.makeId(playerBtssScore.player.id, no),
          contestId = playerBtssScore.player.contestId,
          roundNo = no,
          playerUid = playerBtssScore.player.userId,
          playerNo = playerBtssScore.player.no,
          teamerId = playerBtssScore.player.teamerId,
          score = playerBtssScore.player.score,
          rank = i + 1,
          btssScores = playerBtssScore.btsss,
          cancelled = playerBtssScore.player.cancelled
        )
      }

      TeamPlayerScoreSheetRepo.removeByRound(contest.id, no) >> TeamPlayerScoreSheetRepo.insertMany(scoreSheets)
    }
  }

  def boardWinner(result: TeamBoard.Result) =
    result match {
      case TeamBoard.Result.WhiteWin | TeamBoard.Result.BlackAbsent => chess.Color.White.some
      case TeamBoard.Result.BlackWin | TeamBoard.Result.WhiteAbsent => chess.Color.Black.some
      case TeamBoard.Result.Draw | TeamBoard.Result.AllAbsent => None
    }

  def whitePlayerOutcome(result: TeamBoard.Result) =
    result match {
      case TeamBoard.Result.WhiteWin => TeamBoard.Outcome.Win
      case TeamBoard.Result.BlackWin => TeamBoard.Outcome.Loss
      case TeamBoard.Result.Draw => TeamBoard.Outcome.Draw
      case TeamBoard.Result.BlackAbsent => TeamBoard.Outcome.Win
      case TeamBoard.Result.WhiteAbsent => TeamBoard.Outcome.NoStart
      case TeamBoard.Result.AllAbsent => TeamBoard.Outcome.NoStart
    }

  def blackPlayerOutcome(result: TeamBoard.Result) =
    result match {
      case TeamBoard.Result.WhiteWin => TeamBoard.Outcome.Loss
      case TeamBoard.Result.BlackWin => TeamBoard.Outcome.Win
      case TeamBoard.Result.Draw => TeamBoard.Outcome.Draw
      case TeamBoard.Result.BlackAbsent => TeamBoard.Outcome.NoStart
      case TeamBoard.Result.WhiteAbsent => TeamBoard.Outcome.Win
      case TeamBoard.Result.AllAbsent => TeamBoard.Outcome.NoStart
    }

  private def Sequencing(id: TeamRound.ID)(fetch: TeamRound.ID => Fu[Option[TeamRound]])(run: TeamRound => Funit): Unit =
    doSequence(id) {
      fetch(id) flatMap {
        case Some(t) => run(t)
        case None => fufail(s"Can't run sequenced operation on missing contest round $id")
      }
    }

  private def doSequence(id: TeamRound.ID)(fu: => Funit): Unit =
    sequencers.tell(id, Duct.extra.LazyFu(() => fu))

  private def publishCalendar(contest: TeamContest, boards: Boards): Unit = {
    val calendars = boards.foldLeft(List.empty[CalendarCreate]) {
      case (lst, b) => lst ++ makeBoardCalendar(contest, b)
    }
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  private def removeCalendar(contest: TeamContest, boards: Boards, userId: Option[User.ID] = None): Unit = {
    val ids = boards.foldLeft(List.empty[String]) {
      case (list, board) => list ++ userId.fold(board.players.map { player => s"${board.id}@${player.userId}" }) { u => List(s"${board.id}@$u") }
    }
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
  }

  private def publishCoachCalendar(contest: TeamContest, round: TeamRound, teamers: List[Teamer], excludes: List[User.ID] = Nil): Unit = {
    val calendars = (teamers.flatMap(_.leaders) :+ contest.createdBy)
      .distinct
      .filterNot(excludes.contains(_))
      .map(makeCoachCalendar(contest, round, _))
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  private def removeCoachCalendar(contest: TeamContest, round: TeamRound, teamers: List[Teamer]): Unit = {
    val ids = (teamers.flatMap(_.leaders) :+ contest.createdBy).distinct
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
  }

  private def resetCalendar(contest: TeamContest, board: TeamBoard) = {
    val ids = board.players.map { player => s"${board.id}@${player.userId}" }
    val calendars = makeBoardCalendar(contest, board)
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
    system.scheduler.scheduleOnce(3 seconds) {
      bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def resetCalendarByRound(contest: TeamContest, round: TeamRound, st: DateTime): Funit = {
    TeamBoardRepo.getByRound(round.id) flatMap { boards =>
      val calendars = boards.foldLeft(List.empty[CalendarCreate]) {
        case (list, board) => list ++ makeBoardCalendar(contest, board.copy(startsAt = st))
      }

      val ids = boards.foldLeft(List.empty[String]) {
        case (list, board) => list ++ board.players.map { player => s"${board.id}@${player.userId}" }
      }

      bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
      system.scheduler.scheduleOnce(3 seconds) {
        bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
      }
      funit
    }
  }

  private def resetCoachCalendarByRound(contest: TeamContest, round: TeamRound, teamers: List[Teamer], st: DateTime): Unit = {
    val ids = (teamers.flatMap(_.leaders) :+ contest.createdBy).distinct
    val calendars = ids.map(makeCoachCalendar(contest, round, _))

    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
    system.scheduler.scheduleOnce(3 seconds) {
      bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def makeBoardCalendar(contest: TeamContest, board: TeamBoard): List[CalendarCreate] = {
    board.players.map { player =>
      makeCalendar(contest, board, player.userId)
    }
  }

  private def makeCalendar(contest: TeamContest, board: TeamBoard, userId: User.ID) = {
    CalendarCreate(
      id = s"${board.id}@$userId".some,
      typ = "contest",
      user = userId,
      sdt = board.startsAt,
      edt = board.startsAt.plusMinutes(contest.roundSpace),
      content = s"${contest.name} 第${board.roundNo}轮 #${board.no}",
      onlySdt = false,
      link = s"/${board.id}".some,
      icon = "奖".some,
      bg = "#ae8300".some
    )
  }

  private def makeCoachCalendar(contest: TeamContest, round: TeamRound, userId: User.ID) = CalendarCreate(
    id = s"${round.id}@$userId".some,
    typ = "contest",
    user = userId,
    sdt = round.actualStartsAt,
    edt = round.actualStartsAt.plusMinutes(contest.roundSpace),
    content = s"${contest.name} 第${round.no}轮",
    onlySdt = false,
    link = s"/contest/team/${contest.id}#round${round.no}".some,
    icon = "奖".some,
    bg = "#ae8300".some
  )

  private def lg(contest: TeamContest, round: Option[TeamRound], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
