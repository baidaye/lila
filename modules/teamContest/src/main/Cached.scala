package lila.teamContest

import scala.concurrent.duration._
import lila.memo._

private[teamContest] final class Cached(implicit system: akka.actor.ActorSystem) {

  val nameCache = new Syncache[String, Option[String]](
    name = "teamContest.name",
    compute = id => TeamContestRepo byId id map2 { (contest: TeamContest) => contest.fullName },
    default = _ => none,
    strategy = Syncache.WaitAfterUptime(20 millis),
    expireAfter = Syncache.ExpireAfterAccess(1 hour),
    logger = logger
  )

  def name(id: String): Option[String] = nameCache sync id

}
