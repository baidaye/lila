package lila.teamContest

import lila.common.LightUser
import lila.rating.Perf
import lila.user.{ Perfs, User }
import play.api.libs.json.Json
import org.joda.time.DateTime

import scala.math.BigDecimal.RoundingMode

case class TeamPlayer(
    _id: TeamPlayer.ID,
    no: TeamPlayer.No,
    contestId: TeamContest.ID,
    userId: User.ID,
    teamerId: Teamer.ID,
    rating: Int,
    provisional: Boolean,
    teamRating: Option[Int],
    formal: Boolean,
    score: Double,
    cancelled: Boolean = false, // 取消成绩
    outcomes: List[TeamBoard.Outcome],
    byeRound: List[TeamRound.No] = Nil,
    status: Teamer.Status,
    createdAt: DateTime
) {

  def id = _id

  def is(uid: User.ID): Boolean = uid == userId
  def is(user: User): Boolean = is(user.id)
  def is(other: TeamPlayer): Boolean = is(other.userId)

  def scoreEqual(sc: Double) = {
    val score1 = BigDecimal(score).setScale(1, RoundingMode.DOWN)
    val score2 = BigDecimal(sc).setScale(1, RoundingMode.DOWN)
    score1.equals(score2)
  }

  // 本轮得分
  def roundScoreCurr(rn: TeamRound.No, isRoundRobin: Boolean): Double = {
    roundOutcome(rn).map { o =>
      scoreByOutcome(o, isRoundRobin)
    } | 0.0
  }

  // 不包含本轮的累计得分
  def roundScore(rn: TeamRound.No, isRoundRobin: Boolean): Double =
    outcomes.zipWithIndex.foldLeft(0.0) {
      case (s, (o, i)) => if (i < rn - 1) s + scoreByOutcome(o, isRoundRobin) else s
    }

  // 包含本轮的累计得分
  def roundScoreWithCurr(rn: TeamRound.No, isRoundRobin: Boolean): Double =
    outcomes.zipWithIndex.foldLeft(0.0) {
      case (s, (o, i)) => if (i < rn) s + scoreByOutcome(o, isRoundRobin) else s
    }

  def allScore(isRoundRobin: Boolean): Double =
    outcomes.foldLeft(0.0) {
      case (s, o) => s + scoreByOutcome(o, isRoundRobin)
    }

  def roundIsSubstitute(rn: TeamRound.No): Boolean =
    roundOutcome(rn).has(TeamBoard.Outcome.Substitute) || (roundOutcome(rn).isEmpty && !formal)

  // 获得本轮结果
  def roundOutcome(rn: TeamRound.No): Option[TeamBoard.Outcome] =
    outcomes.zipWithIndex.find(_._2 == rn - 1).map(_._1)

  def roundOutcomeFormat(rn: TeamRound.No): String = {
    import TeamBoard.Outcome._
    roundOutcome(rn).map {
      case Win => "1-0"
      case Loss => "0-1"
      case Draw => "1/2-1/2"
      case Bye => "轮空"
      case NoStart => "未移动"
      case Quit => "退赛"
      case Kick => "退赛"
      case ManualAbsent => "弃权"
      case Substitute => "替补"
    }.orElse(if (isBye(rn)) "轮空".some else None) | "-"
  }

  def roundOutcomeSort(rn: TeamRound.No): Int = {
    import TeamBoard.Outcome._
    roundOutcome(rn).map {
      case Win => 100
      case Loss => 90
      case Draw => 80
      case NoStart => 70
      case Bye => 60
      case ManualAbsent => 40
      case Quit => 20
      case Kick => 10
      case Substitute => 0
    }.orElse(if (isBye(rn)) 60.some else None) | 0
  }

  def scoreByOutcome(o: TeamBoard.Outcome, isRoundRobin: Boolean): Double = {
    import TeamBoard.Outcome._
    o match {
      case Win => 1.0
      case Loss => 0.0
      case Draw => 0.5
      case Bye => if (isRoundRobin) 0.0 else 1.0
      case NoStart => 0.0
      case Quit => 0.0
      case Kick => 0.0
      case ManualAbsent => 0.0
      case Substitute => 0.0
    }
  }

  def isSubstitute(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && o == TeamBoard.Outcome.Substitute
    }

  def isBye(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && o == TeamBoard.Outcome.Bye
    } || byeRound.contains(rn)

  def isManualAbsent(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && o == TeamBoard.Outcome.ManualAbsent
    }

  def isAbsent(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && (o == TeamBoard.Outcome.Quit || o == TeamBoard.Outcome.Kick || o == TeamBoard.Outcome.ManualAbsent || o == TeamBoard.Outcome.Substitute)
    }

  def isAbsentIgnoreManual(rn: TeamRound.No): Boolean =
    outcomes.zipWithIndex.exists {
      case (o, i) => rn == i + 1 && (o == TeamBoard.Outcome.Quit || o == TeamBoard.Outcome.Kick)
    }

  def hasBoard = outcomes.exists(_ != TeamBoard.Outcome.Substitute)

  def noBoard(rn: TeamRound.No): Boolean = isAbsent(rn) || isBye(rn) || isSubstitute(rn)

  def finish(rn: TeamRound.No, outcome: TeamBoard.Outcome, isRoundRobin: Boolean): TeamPlayer = {
    copy(
      outcomes = setOutcomeByRound(rn, outcome)
    ) |> { player =>
        player.copy(
          score = player.allScore(isRoundRobin)
        )
      }
  }

  def setOutcomeByRound(rn: TeamRound.No, outcome: TeamBoard.Outcome) =
    if (outcomes.length >= rn) {
      outcomes.zipWithIndex.map {
        case (o, i) => if (rn == i + 1) outcome else o
      }
    } else {
      if (rn - 1 == outcomes.length) outcomes :+ outcome
      else {
        logger.warn(s"设置棋手结果异常：$id，$rn，$outcome")
        outcomes
      }
    }

  def removeOutcomeByRound(rn: TeamRound.No) =
    if (outcomes.length == rn) {
      outcomes.dropRight(1)
    } else {
      logger.warn(s"删除棋手结果异常：$id，$rn")
      outcomes
    }

  def removeByeRoundByRound(rn: TeamRound.No) =
    byeRound.filter(_ == rn)

  def manualResult(rn: TeamRound.No, newOutcome: TeamBoard.Outcome, isRoundRobin: Boolean): TeamPlayer = {
    copy(
      outcomes = setOutcomeByRound(rn, newOutcome)
    ) |> { player =>
        player.copy(
          score = player.allScore(isRoundRobin)
        )
      }
  }

}

object TeamPlayer {

  type ID = String
  type No = Int

  private[teamContest] def make(
    contestId: TeamContest.ID,
    no: TeamPlayer.No,
    user: User,
    teamerId: Teamer.ID,
    perfLens: Perfs => Perf,
    teamRating: Option[Int],
    formal: Boolean
  ): TeamPlayer = new TeamPlayer(
    _id = makeId(contestId, user.id),
    no = no,
    contestId = contestId,
    userId = user.id,
    teamerId = teamerId,
    rating = perfLens(user.perfs).intRating,
    provisional = perfLens(user.perfs).provisional,
    teamRating = teamRating,
    score = 0,
    formal = formal,
    outcomes = List.empty,
    status = Teamer.Status.Sent,
    createdAt = DateTime.now
  )

  private[teamContest] def makeId(contestId: String, userId: String) = userId + "@" + contestId

  def toMap(players: List[TeamPlayer]): Map[TeamPlayer.ID, TeamPlayer] =
    players.map(p => p.id -> p).toMap

}

case class PlayerWithUser(player: TeamPlayer, user: User, member: Option[lila.team.Member], mark: Option[String]) {
  def id = player.id
  def no = player.no
  def playerId = player.id
  def userId = user.id
  def username = user.username
  def profile = user.profileOrDefault
  def realName = mark.fold(user.realNameOrUsername) { m => m }
  def markOrUsername = mark | user.username
  def is(userId: User.ID) = player.is(userId)

  def json(perfLens: Perfs => Perf) = {
    var attr = Json.obj(
      "id" -> userId,
      "name" -> markOrUsername,
      "username" -> user.username,
      "rating" -> perfLens(user.perfs).intRating,
      "teamRating" -> member.??(_.rating.map(_.intValue)),
      "campus" -> member.??(_.campus),
      "clazz" -> member.??(_.clazzIds | List.empty[String]),
      "level" -> user.profileOrDefault.currentLevel.level,
      "sex" -> user.profileOrDefault.sex
    )
    member.foreach { member =>
      member.tags.foreach { tags =>
        tags.tagMap.foreach {
          case (f, t) => attr = attr.add(f, t.value)
        }
      }
    }
    attr
  }
}

case class AllPlayerWithUser(user: User, member: Option[lila.team.Member], mark: Option[String]) {
  def userId = user.id
  def profile = user.profileOrDefault
  def realName = mark.fold(user.realNameOrUsername) { m => m }
  def markOrUsername = mark | user.username
  def json(perfLens: Perfs => Perf) = {
    var attr = Json.obj(
      "id" -> userId,
      "name" -> markOrUsername,
      "username" -> user.username,
      "rating" -> perfLens(user.perfs).intRating,
      "teamRating" -> member.??(_.rating.map(_.intValue)),
      "campus" -> member.??(_.campus),
      "clazz" -> member.??(_.clazzIds | List.empty[String]),
      "level" -> user.profileOrDefault.currentLevel.level,
      "sex" -> user.profileOrDefault.sex
    )
    member.foreach { member =>
      member.tags.foreach { tags =>
        tags.tagMap.foreach {
          case (f, t) => attr = attr.add(f, t.value)
        }
      }
    }
    attr
  }
}

case class PlayerResult(player: TeamPlayer, lightUser: LightUser, rank: Int)
