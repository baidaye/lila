package lila.teamContest

import lila.db.dsl._
import lila.user.User
import reactivemongo.bson._
import org.joda.time.DateTime
import scala.collection.breakOut

object TeamPlayerRepo {

  private[teamContest] lazy val coll = Env.current.playerColl

  import BSONHandlers.PlayerHandler
  import BSONHandlers.BoardHandler

  type ID = String

  def byId(id: TeamPlayer.ID): Fu[Option[TeamPlayer]] = coll.byId[TeamPlayer](id)

  def byIds(ids: List[TeamPlayer.ID]): Fu[List[TeamPlayer]] = coll.byIds[TeamPlayer](ids)

  def byNos(contestId: TeamContest.ID, nos: List[TeamPlayer.No]): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId) ++ $doc("no" $in nos)).list[TeamPlayer]()

  def byOrderedIds(ids: List[TeamPlayer.ID]): Fu[List[TeamPlayer]] =
    coll.byOrderedIds[TeamPlayer, TeamPlayer.ID](ids)(_.id)

  def findByByeRound(contestId: TeamContest.ID, byeRound: TeamRound.No): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId) ++ $doc("byeRound" -> byeRound)).list[TeamPlayer]()

  def find(contestId: TeamContest.ID, userId: User.ID): Fu[Option[TeamPlayer]] =
    byId(TeamPlayer.makeId(contestId, userId))

  def find(contestId: TeamContest.ID, userIds: List[User.ID]): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId) ++ $doc("userId" $in userIds)).list[TeamPlayer]()

  def insert(player: TeamPlayer): Funit = coll.insert(player).void

  def countByContest(contestId: TeamContest.ID): Fu[Int] =
    coll.countSel(contestQuery(contestId))

  def getByUserId(userId: User.ID): Fu[List[TeamPlayer]] =
    coll.find($doc("userId" -> userId)).list[TeamPlayer]()

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId)).sort($sort asc "no").list[TeamPlayer]()

  def getFormalByTeamer(teamerId: Teamer.ID): Fu[List[TeamPlayer]] =
    coll.find(teamerQuery(teamerId) ++ formalQuery()).sort($sort asc "no").list[TeamPlayer]()

  def getFormalByContest(contestId: TeamContest.ID): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId) ++ formalQuery()).sort($sort asc "no").list[TeamPlayer]()

  def getSubstituteByContest(contestId: TeamContest.ID): Fu[List[TeamPlayer]] =
    coll.find(contestQuery(contestId) ++ substituteQuery()).sort($sort asc "no").list[TeamPlayer]()

  def getByTeamer(teamerId: Teamer.ID): Fu[List[TeamPlayer]] =
    coll.find(teamerQuery(teamerId)).sort($sort asc "no").list[TeamPlayer]()

  def getByTeamers(teamerIds: List[Teamer.ID]): Fu[List[TeamPlayer]] =
    coll.find($doc("teamerId" $in teamerIds)).sort($sort asc "no").list[TeamPlayer]()

  def remove(id: TeamPlayer.ID): Funit = coll.remove($id(id)).void

  def removeByTeamer(teamerId: Teamer.ID): Funit =
    coll.remove(teamerQuery(teamerId)).void

  def removeByTeamers(teamerIds: List[Teamer.ID]): Funit =
    coll.remove($doc("teamerId" $in teamerIds)).void

  def bulkUpdate(contestId: Teamer.ID, players: List[TeamPlayer]): Funit =
    removeByTeamer(contestId) >> bulkInsert(players).void

  def bulkInsert(players: List[TeamPlayer]): Funit = coll.bulkInsert(
    documents = players.map(PlayerHandler.write).toStream,
    ordered = true
  ).void

  def removePlayersLastOutcome(contest: TeamContest, players: List[TeamPlayer], roundNo: TeamRound.No): Funit = {
    players.map { player =>
      removePlayerLastOutcome(contest, player, roundNo)
    }.sequenceFu.void
  }

  def removePlayerLastOutcome(contest: TeamContest, player: TeamPlayer, roundNo: TeamRound.No): Funit = {
    update(
      player.copy(
        outcomes = player.removeOutcomeByRound(roundNo)
      ) |> { p =>
          p.copy(score = p.allScore(contest.isRoundRobin))
        }
    )
  }

  def setStatus(teamerId: Teamer.ID, status: Teamer.Status): Funit =
    coll.update(
      teamerQuery(teamerId),
      $set("status" -> status.id),
      multi = true
    ).void

  def setZeroScoreOutcomes(contestId: TeamContest.ID, roundNo: Int, teamers: List[Teamer], outcome: TeamBoard.Outcome): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("teamerId" $in teamers.map(_.id)),
      $set(s"outcomes.${roundNo - 1}" -> outcome.id),
      multi = true
    ).void

  def setSubstituteOutcomes(contestId: TeamContest.ID, roundNo: Int): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("formal" -> false),
      $set(s"outcomes.${roundNo - 1}" -> TeamBoard.Outcome.Substitute.id),
      multi = true
    ).void

  def setByeRound(teamerId: Teamer.ID, byeRound: List[TeamRound.No]): Funit =
    coll.update(
      teamerQuery(teamerId),
      $set("byeRound" -> byeRound),
      multi = true
    ).void

  def setScore(id: TeamPlayer.ID, score: Double): Funit =
    coll.update(
      $id(id),
      $set("score" -> score)
    ).void

  def setNo(id: TeamPlayer.ID, no: TeamPlayer.No): Funit =
    coll.updateField($id(id), "no", no).void

  def setFormal(id: TeamPlayer.ID, formal: Boolean): Funit =
    coll.updateField($id(id), "formal", formal).void

  def finishGame(contest: TeamContest, rn: TeamRound.No, playerUserId: User.ID, outcome: TeamBoard.Outcome): Funit = {
    val id = TeamPlayer.makeId(contest.id, playerUserId)
    byId(id) flatMap {
      case None => fufail(s"can not find player $id")
      case Some(p) => {
        coll.update(
          $id(id),
          p.finish(rn, outcome, contest.isRoundRobin)
        ).void
      }
    }
  }

  def update(player: TeamPlayer): Funit = {
    coll.update(
      $id(player.id),
      player
    ).void
  }

  def setCancelScore(teamerId: Teamer.ID): Funit =
    coll.update(
      teamerQuery(teamerId),
      $set("cancelled" -> true),
      multi = true
    ).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)
  def teamerQuery(teamerId: Teamer.ID) = $doc("teamerId" -> teamerId)
  def formalQuery() = $doc("formal" -> true)
  def substituteQuery() = $doc("substitute" -> true)

}
