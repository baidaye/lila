package lila.teamContest

import PairingSystem.ByeOrPending

trait Pairing {

  def pairing(contest: TeamContest): Fu[List[ByeOrPending]]

  def pairingAll(contest: TeamContest, teamers: List[Teamer]): Fu[Map[Int, List[ByeOrPending]]]

}
