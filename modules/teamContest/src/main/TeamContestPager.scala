package lila.teamContest

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import reactivemongo.bson.BSONDocument
import lila.user.User

class TeamContestPager {

  import BSONHandlers.ContestHandler

  def enter(page: Int, status: Option[TeamContest.Status], me: User, federationIds: List[String], text: String): Fu[Paginator[TeamContest]] =
    paginator(
      page,
      text,
      TeamContestRepo.enterSelect ++ status ?? TeamContestRepo.statusSelect ++ organizerSelector(me, federationIds),
      TeamContestRepo.startAsc
    )

  def owner(page: Int, status: Option[TeamContest.Status], me: User, text: String): Fu[Paginator[TeamContest]] =
    paginator(
      page,
      text,
      TeamContestRepo.createBySelect(me.id) ++ TeamContestRepo.ownerSelect ++ status ?? TeamContestRepo.statusSelect,
      TeamContestRepo.startDesc
    )

  def belong(page: Int, status: Option[TeamContest.Status], me: User, text: String): Fu[Paginator[TeamContest]] =
    for {
      players <- TeamPlayerRepo.getByUserId(me.id)
      teamers <- TeamerRepo.getByLeaderOrCreatorId(me.id)
      contestIds = (players.map(_.contestId) ++ teamers.map(_.contestId)).distinct
      p <- paginator(
        page,
        text,
        TeamContestRepo.idsSelect(contestIds) ++ TeamContestRepo.belongSelect ++ status ?? TeamContestRepo.statusSelect,
        TeamContestRepo.startDesc
      )
    } yield p

  def finish(page: Int, status: Option[TeamContest.Status], me: User, clazzs: List[String], text: String): Fu[Paginator[TeamContest]] =
    for {
      players <- TeamPlayerRepo.getByUserId(me.id)
      teamers <- TeamerRepo.getByLeaderOrCreatorId(me.id)
      contestIds = (players.map(_.contestId) ++ teamers.map(_.contestId)).distinct
      p <- paginator(
        page,
        text,
        $or(TeamContestRepo.createBySelect(me.id), TeamContestRepo.idsSelect(contestIds)) ++ TeamContestRepo.finishedOrCancelSelect ++ status ?? TeamContestRepo.statusSelect,
        TeamContestRepo.startDesc
      )
    } yield p

  private def organizerSelector(me: User, federationIds: List[String]) =
    me.teamId match {
      case Some(teamId) => {
        $or(
          $doc("typ" -> "team-inner", "organizer" -> teamId),
          $doc("typ" -> "federation-inner", "organizer" $in federationIds)
        )
      }
      case None => {
        $doc("typ" -> "federation-inner", "organizer" $in federationIds)
      }
    }

  private def paginator(page: Int, text: String, $selector: BSONDocument, $order: BSONDocument): Fu[Paginator[TeamContest]] = {
    val textOption = if (text.trim.isEmpty) none else text.trim.some
    //println(BSONDocument.pretty($selector))
    Paginator[TeamContest](adapter = new Adapter[TeamContest](
      collection = TeamContestRepo.coll,
      selector = $selector ++ textOption ?? (t => $doc("name" $regex (t, "i"))),
      projection = $empty,
      sort = $order
    ), currentPage = page, maxPerPage = MaxPerPage(16))
  }

}
