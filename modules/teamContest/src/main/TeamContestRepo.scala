package lila.teamContest

import lila.db.dsl._
import lila.user.User
import lila.db.BSON.BSONJodaDateTimeHandler
import org.joda.time.DateTime
import reactivemongo.api.ReadPreference

object TeamContestRepo {

  private[teamContest] lazy val coll = Env.current.contestColl

  import BSONHandlers.ContestHandler

  private[teamContest] def statusSelect(status: TeamContest.Status) = $doc("status" -> status.id)
  private[teamContest] def createBySelect(userId: User.ID) = $doc("createdBy" -> userId)
  private[teamContest] def idsSelect(ids: List[String]) = $doc("_id" $in ids)
  private[teamContest] def allPublishedSelect(aheadMinutes: Int) = publishedSelect ++
    $doc("startsAt" $lt (DateTime.now plusMinutes aheadMinutes))

  private[teamContest] val createdSelect = $doc("status" -> TeamContest.Status.Created.id)
  private[teamContest] val publishedSelect = $doc("status" -> TeamContest.Status.Published.id)
  private[teamContest] val enterStoppedSelect = $doc("status" -> TeamContest.Status.EnterStopped.id)
  private[teamContest] val startedSelect = $doc("status" -> TeamContest.Status.Started.id)
  private[teamContest] val finishedSelect = $doc("status" -> TeamContest.Status.Finished.id)

  private[teamContest] val enterSelect = $doc("status" $in TeamContest.Status.enter.map(_.id))
  private[teamContest] val belongSelect = $doc("status" $in TeamContest.Status.belong.map(_.id))
  private[teamContest] val ownerSelect = $doc("status" $in TeamContest.Status.owner.map(_.id))
  private[teamContest] val finishedOrCancelSelect = $doc("status" $in TeamContest.Status.finish.map(_.id))
  private[teamContest] val createDesc = $doc("createdAt" -> -1)
  private[teamContest] val startAsc = $doc("startsAt" -> 1)
  private[teamContest] val startDesc = $doc("startsAt" -> -1)

  def insert(tour: TeamContest) = coll.insert(tour)

  def update(tour: TeamContest) = coll.update($id(tour.id), tour)

  def remove(id: TeamContest.ID) = coll.remove($id(id))

  def exists(id: TeamContest.ID) = coll exists $id(id)

  def nameExists(name: String, groupName: Option[String], id: Option[String]) =
    coll.exists($doc("name" -> name) ++ groupName.??(g => $doc("groupName" -> g)) ++ id.??(i => $doc("_id" $ne i)))

  def byId(id: TeamContest.ID): Fu[Option[TeamContest]] = coll.find($id(id)).uno[TeamContest]

  def byIds(ids: Iterable[String]): Fu[List[TeamContest]] =
    coll.find($inIds(ids)).list[TeamContest](none)

  def byOrderedIds(ids: Seq[String]): Fu[List[TeamContest]] =
    coll.byOrderedIds[TeamContest, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def createdById(id: TeamContest.ID): Fu[Option[TeamContest]] =
    coll.find($id(id) ++ createdSelect).uno[TeamContest]

  def publishedById(id: TeamContest.ID): Fu[Option[TeamContest]] =
    coll.find($id(id) ++ publishedSelect).uno[TeamContest]

  def enterStoppedById(id: TeamContest.ID): Fu[Option[TeamContest]] =
    coll.find($id(id) ++ enterStoppedSelect).uno[TeamContest]

  def startedById(id: TeamContest.ID): Fu[Option[TeamContest]] =
    coll.find($id(id) ++ startedSelect).uno[TeamContest]

  def finishedById(id: TeamContest.ID): Fu[Option[TeamContest]] =
    coll.find($id(id) ++ finishedSelect).uno[TeamContest]

  def published: Fu[List[TeamContest]] =
    coll.find($doc("deadlineAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 20))) ++ publishedSelect).list[TeamContest]()

  def enterStopped: Fu[List[TeamContest]] =
    coll.find($doc("startsAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 20))) ++ enterStoppedSelect).list[TeamContest]()

  def setStatus(id: TeamContest.ID, status: TeamContest.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setRounds(id: TeamContest.ID, rounds: Int): Funit =
    coll.updateField($id(id), "rounds", rounds).void

  def setCurrentRound(id: TeamContest.ID, no: TeamRound.No): Funit =
    coll.updateField($id(id), "currentRound", no).void

  def setAllRoundFinished(id: TeamContest.ID): Funit =
    coll.updateField($id(id), "allRoundFinished", true).void

  def incTeamers(id: TeamContest.ID, by: Int): Funit =
    coll.update($id(id), $inc("nbTeamers" -> by)).void

  def setTeamers(id: TeamContest.ID, count: Int): Funit =
    coll.update($id(id), $set("nbTeamers" -> count)).void

  def setAutoPairing(id: TeamContest.ID, autoPairing: Boolean): Funit =
    coll.update(
      $id(id),
      $set("autoPairing" -> autoPairing)
    ).void

  def setRoundRobinPairing(id: TeamContest.ID): Funit =
    coll.update(
      $id(id),
      $set("roundRobinPairing" -> true)
    ).void

  def finish(contest: TeamContest): Funit =
    coll.update(
      $id(contest.id),
      $set(
        "status" -> TeamContest.Status.Finished.id,
        "realFinishAt" -> DateTime.now
      )
    ).void

  def visibleInTeam(teamId: String, nb: Int): Fu[List[TeamContest]] =
    coll.find($doc("typ" -> "team-inner", "organizer" -> teamId)).sort(startDesc).list[TeamContest](nb)

  /*  def findRecently(nb: Int, teamIdOption: Option[String] = None): Fu[List[TeamContest]] = {
    val selector = teamIdOption match {
      case Some(teamId) => $doc(
        "status" -> TeamContest.Status.Finished.id,
        "organizer" -> teamId
      )
      case None => $doc(
        "status" -> TeamContest.Status.Finished.id,
        "typ" -> TeamContest.Type.Public.id
      )
    }
    coll.find(selector)
      .sort(startDesc)
      .list[TeamContest](nb)
  }*/

}
