package lila.teamContest

// https://www.fide.com/FIDE/handbook/C04Annex2_TRF16.pdf
// http://www.rrweb.org/javafo/aum/JaVaFo2_AUM.htm#_Toc524760822
final class SwissTrf(baseUrl: String) {

  private type Bits = List[(Int, String)]

  val sep = System.getProperty("line.separator")

  def apply(contest: TeamContest): Fu[String] = {
    for {
      rounds <- TeamerRepo.rounds(contest)
      teamers <- TeamerRepo.getJoinedByContest(contest.id)
      forbiddens <- ForbiddenRepo.getByContest(contest.id)
    } yield {
      headLines(contest, forbiddenPairs(teamers, forbiddens)) concat sep concat
        rounds.map {
          case (p, m) => teamerLine(contest)(p, m)
        }.map(formatLine).mkString(sep)
    }
  }

  private def forbiddenPairs(teamers: List[Teamer], forbiddens: List[Forbidden]): Set[(Teamer.No, Teamer.No)] = {
    val teamerMap = teamers.map(t => t.id -> t.no).toMap
    forbiddens.flatMap { forbidden =>
      forbidden.pairs(teamers).map {
        case (t1, t2) => (teamerMap.get(t1) err s"can not find teamer $t1") -> (teamerMap.get(t2) err s"can not find teamer $t2")
      }
    }.toSet
  }

  private def headLines(contest: TeamContest, forbiddenPairs: Set[(Teamer.No, Teamer.No)]) = {
    forbiddenPairs.map {
      case (p1, p2) => s"XXP $p1 $p2"
    } ++ Set(
      s"XXR ${contest.actualRound}"
    )
  }.mkString(sep)

  private def teamerLine(contest: TeamContest)(t: Teamer, historyRoundPairedMap: Map[TeamRound.No, TeamPaired]): Bits = {
    val currentRoundNo = contest.currentRound
    List(
      3 -> "001",
      8 -> t.no.toString,
      47 -> t.name,
      84 -> f"${t.roundScore(currentRoundNo, contest.isRoundRobin)}%1.1f"
    ) ::: contest.historyRoundList.zip(t.outcomes).flatMap {
        case (rn, outcome) =>
          val pariedOption = historyRoundPairedMap get rn
          List(
            95 -> pariedOption.fold("0000") { paried =>
              paried.opponentOf(t.no).fold("0000")(_.toString)
            },
            97 -> pariedOption.fold("-") { paried =>
              paried.colorOf(t.no).fold("-")(_.fold("w", "b"))
            },
            99 -> {
              import TeamBoard.Outcome._
              outcome match {
                case Win => "1"
                case Loss => "0"
                case NoStart => "-"
                case Draw => "="
                case Bye => "U"
                case Quit | Kick | ManualAbsent | Substitute => "Z"
              }
            }
          ).map { case (l, s) => (l + (rn - 1) * 10, s) }
      } ::: t.absent.?? {
        List( // http://www.rrweb.org/javafo/aum/JaVaFo2_AUM.htm#_Unusual_info_extensions
          95 -> "0000",
          97 -> "-",
          99 -> "Z"
        ).map { case (l, s) => (l + (currentRoundNo - 1) * 10, s) }
      }
  }

  private def formatLine(bits: Bits): String =
    bits.foldLeft("") {
      case (acc, (pos, txt)) => s"""$acc${" " * (pos - txt.size - acc.size)}$txt"""
    }

  private val dateFormatter = org.joda.time.format.DateTimeFormat forStyle "M-"

}
