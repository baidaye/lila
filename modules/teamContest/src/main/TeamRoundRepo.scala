package lila.teamContest

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import lila.teamContest.TeamRound.ID
import reactivemongo.api.ReadPreference
import reactivemongo.bson._

object TeamRoundRepo {

  private[teamContest] lazy val coll = Env.current.roundColl

  import BSONHandlers.RoundHandler
  private[teamContest] val createdSelect = $doc("status" -> TeamRound.Status.Created.id)
  private[teamContest] val pairingSelect = $doc("status" -> TeamRound.Status.Pairing.id)
  private[teamContest] val publishedSelect = $doc("status" -> TeamRound.Status.Published.id)
  private[teamContest] val startedSelect = $doc("status" -> TeamRound.Status.Started.id)
  private[teamContest] val finishedSelect = $doc("status" -> TeamRound.Status.Finished.id)

  def insert(round: TeamRound): Funit = coll.insert(round).void

  def update(id: TeamRound.ID, round: TeamRound): Funit = coll.update($id(id), round).void

  def bulkUpdate(contestId: TeamContest.ID, roundList: List[TeamRound]): Funit =
    removeByContest(contestId) >> bulkInsert(roundList).void

  def bulkInsert(roundList: List[TeamRound]): Funit = coll.bulkInsert(
    documents = roundList.map(RoundHandler.write).toStream,
    ordered = true
  ).void

  def find(contestId: TeamContest.ID, no: TeamRound.No) = byId(TeamRound.makeId(contestId, no))

  def byId(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.byId[TeamRound](id)

  def getByContest(contestId: TeamContest.ID): Fu[List[TeamRound]] =
    coll.find(contestQuery(contestId)).sort($doc("no" -> 1)).list[TeamRound]()

  def getAllPublishedRounds(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamRound]] =
    coll.find(contestQuery(contestId) ++ $doc("no" $gte no, "status" -> TeamRound.Status.PublishResult.id)).list[TeamRound]()

  def getNextRounds(contestId: TeamContest.ID, no: TeamRound.No): Fu[List[TeamRound]] =
    coll.find(contestQuery(contestId) ++ $doc("no" $gt no)).list[TeamRound]()

  def byOrderedIds(ids: Seq[String]): Fu[List[TeamRound]] =
    coll.byOrderedIds[TeamRound, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def exists(id: TeamRound.ID): Fu[Boolean] =
    coll.exists($id(id))

  def find(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.uno[TeamRound]($id(id))

  def createdById(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.find($id(id) ++ createdSelect).uno[TeamRound]

  def pairingById(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.find($id(id) ++ pairingSelect).uno[TeamRound]

  def publishedById(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.find($id(id) ++ publishedSelect).uno[TeamRound]

  def startedById(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.find($id(id) ++ startedSelect).uno[TeamRound]

  def finishedById(id: TeamRound.ID): Fu[Option[TeamRound]] =
    coll.find($id(id) ++ finishedSelect).uno[TeamRound]

  def countByContest(contestId: TeamContest.ID): Fu[Int] =
    coll.countSel(contestQuery(contestId))

  def getByUser(userId: User.ID): Fu[List[TeamRound]] =
    coll.find($doc("userId" -> userId)).list[TeamRound]()

  def remove(id: TeamRound.ID): Funit =
    coll.remove($id(id)).void

  def removeByContest(contestId: TeamContest.ID): Funit =
    coll.remove(contestQuery(contestId)).void

  def setBoards(id: TeamRound.ID, boards: Int): Funit =
    coll.updateField($id(id), "boards", boards).void

  def setStatus(id: TeamRound.ID, status: TeamRound.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setPairedAndStatus(id: TeamRound.ID, paireds: Int, boards: Int, status: TeamRound.Status): Funit =
    coll.update(
      $id(id),
      $set(
        "paireds" -> paireds,
        "boards" -> boards,
        "status" -> status.id
      ),
      multi = true
    ).void

  def setPairedsAndStatus(ids: List[TeamRound.ID], paireds: Int, boards: Int, status: TeamRound.Status): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "paireds" -> paireds,
        "boards" -> boards,
        "status" -> status.id
      ),
      multi = true
    ).void

  def allFinished(contestId: TeamContest.ID, round: TeamRound.No): Fu[Boolean] =
    coll.exists(contestQuery(contestId) ++ $doc("status" $gte TeamRound.Status.Finished.id, "no" -> round))

  // 找到过去20分钟内published的Round
  def published: Fu[List[TeamRound]] =
    coll.find($doc("actualStartsAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 20))) ++ publishedSelect).list[TeamRound]()

  def finish(id: TeamRound.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "status" -> TeamRound.Status.Finished.id,
        "finishAt" -> DateTime.now
      )
    ).void

  def setStartsTime(id: ID, st: DateTime): Funit =
    coll.updateField($id(id), "actualStartsAt", st).void

  def contestQuery(contestId: TeamContest.ID) = $doc("contestId" -> contestId)

}
