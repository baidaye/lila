package lila.opening

import chess.format.pgn.Glyphs
import chess.format.{ FEN, Forsyth, Uci }
import org.joda.time.DateTime
import lila.user.User
import lila.tree.Node.{ Comments, Shapes }
import OpeningDBNode._

case class OpeningDBNode(
    _id: ID,
    oid: OpeningDB.ID,
    name: Option[String],
    shortName: Option[String],
    extName: List[String],
    move: Uci.WithSan,
    piece: chess.Piece,
    fen: FEN,
    prevFen: FEN,
    check: Boolean,
    shapes: Shapes = Shapes(Nil),
    comments: Comments = Comments(Nil),
    glyphs: Glyphs = Glyphs.empty,
    order: Int = OpeningDBNode.maxOrder,
    pgn: String,
    tmp: Boolean,
    createdBy: User.ID,
    createdAt: DateTime,
    updatedAt: DateTime
) {

  def id = _id

  def uciId = chess.format.UciCharPair(move.uci).toString()

  def first = order == OpeningDBNode.minOrder

  def castling = move.san.contains("O-O")

  def anotherCastlingMove = castlingMap.get(move.uci.uci).flatMap(u => Uci.Move(u))

  def cp(oid: OpeningDB.ID, userId: User.ID) = {
    val now = DateTime.now
    copy(
      _id = makeId(oid, prevFen, move),
      oid = oid,
      createdBy = userId,
      createdAt = now,
      updatedAt = now
    )
  }

}

object OpeningDBNode {

  type ID = String

  val maxTurns = 20
  val minOrder = 1
  val maxOrder = 100

  val castlingMap = Map(
    "e1h1" -> "e1g1", "e1g1" -> "e1h1", "e1a1" -> "e1c1", "e1c1" -> "e1a1",
    "e8h8" -> "e8g8", "e8g8" -> "e8h8", "e8a8" -> "e8c8", "e8c8" -> "e8a8"
  )

  def makeId(oid: OpeningDB.ID, prevFen: FEN, move: Uci.WithSan) = s"$oid:${prevFen.value}:${move.uci.uci}"

  def makeById(id: OpeningDB.ID) = {
    val arr = id.split(":")
    val uci = Uci.Move.apply(arr(2)) err s"Bad initial move"
    val prevFen = arr(1)
    val fen = (
      for {
        sit1 <- Forsyth << prevFen
        sit2 <- sit1.move(uci).toOption.map(_.situationAfter)
      } yield Forsyth >> sit2
    ) err s"can not parser fen $prevFen"

    make(
      oid = arr(0),
      name = None,
      shortName = None,
      move = Uci.WithSan(uci, ""),
      piece = chess.Piece(chess.Color.white, chess.Pawn),
      fen = FEN(Forsyth.toEPD(fen)),
      prevFen = FEN(prevFen),
      check = false,
      pgn = "",
      createdBy = ""
    )
  }

  def make(
    oid: OpeningDB.ID,
    name: Option[String],
    shortName: Option[String],
    move: Uci.WithSan,
    piece: chess.Piece,
    fen: FEN,
    prevFen: FEN,
    check: Boolean,
    pgn: String,
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    OpeningDBNode(
      _id = makeId(oid, prevFen, move),
      oid = oid,
      name = name,
      shortName = shortName,
      extName = List(name, shortName).filter(_.isDefined).map(_.get),
      move = move,
      piece = piece,
      fen = fen,
      prevFen = prevFen,
      check = check,
      comments = Comments(Nil),
      shapes = Shapes(Nil),
      glyphs = Glyphs.empty,
      pgn = pgn,
      tmp = true,
      createdBy = createdBy,
      createdAt = now,
      updatedAt = now
    )
  }

  case class Turn(number: Int, white: Option[OpeningDBNode], black: Option[OpeningDBNode]) {
    override def toString = {
      val text = (white, black) match {
        case (Some(w), Some(b)) => s" ${w.move.san} ${b.move.san}"
        case (Some(w), None) => s" ${w.move.san}"
        case (None, Some(b)) => s".. ${b.move.san}"
        case _ => ""
      }
      s"$number.${text.trim}"
    }
  }

  def toPathMap(nodes: List[OpeningDBNode]) = {
    nodes.foldLeft(("", Map.empty[String, OpeningDBNode])) {
      case ((parentPath, map), node) => (parentPath + node.uciId, map + (parentPath -> node))
    }._2
  }

}
