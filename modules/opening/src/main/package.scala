package lila

package object opening extends PackageObject {

  private[opening] val logger = lila.log("opening")

  case class CleanOpeningdbNotify(openingdb: OpeningDB, n: Int)

}
