package lila.opening

import reactivemongo.api.ReadPreference
import org.joda.time.DateTime
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import OpeningDBNode._
import chess.format.Forsyth

object OpeningDBNodeRepo {

  private lazy val coll = Env.current.CollOpeningDBNode

  import BSONHandlers._

  def byId(id: OpeningDBNode.ID): Fu[Option[OpeningDBNode]] =
    coll.byId[OpeningDBNode](id)

  def byIds(ids: List[OpeningDBNode.ID]): Fu[List[OpeningDBNode]] =
    coll.byIds[OpeningDBNode](ids)

  def byOrderedIds(ids: List[OpeningDBNode.ID]): Fu[List[OpeningDBNode]] =
    coll.byOrderedIds[OpeningDBNode, String](ids, readPreference = ReadPreference.secondaryPreferred)(_.id)

  def optionsByOrderedIds(ids: List[OpeningDBNode.ID]): Fu[List[Option[OpeningDBNode]]] =
    coll.optionsByOrderedIds[OpeningDBNode, String](ids, readPreference = ReadPreference.secondaryPreferred)(_.id)

  def byOpeningId(oid: OpeningDB.ID): Fu[List[OpeningDBNode]] =
    coll.find($doc("oid" -> oid))
      .sort($doc("order" -> 1))
      .list[OpeningDBNode]()

  def countByOpeningId(openingdbId: OpeningDB.ID): Fu[Int] =
    coll.countSel($doc("oid" -> openingdbId))

  def findNextNodes(oid: OpeningDB.ID, prevFen: String): Fu[List[OpeningDBNode]] =
    coll.find($doc("oid" -> oid, "prevFen" -> prevFen))
      .sort($doc("order" -> 1))
      .list[OpeningDBNode]()

  def findNextNodeIds(oid: OpeningDB.ID, prevFen: String): Fu[List[String]] =
    coll.primitive[String]($doc("oid" -> oid, "prevFen" -> prevFen), "_id")

  def transferNodes(oid: OpeningDB.ID, fen: String): Fu[List[OpeningDBNode]] =
    coll.find($doc("oid" -> oid, "fen" -> fen))
      .sort($doc("order" -> 1))
      .list[OpeningDBNode](20)

  def searchNodes(oid: OpeningDB.ID, search: DataForm.NodeSearchData): Fu[List[OpeningDBNode]] = {
    val castlingMap = Map(
      "e1g1" -> "e1h1", "e1c1" -> "e1a1",
      "e8g8" -> "e8h8", "e8c8" -> "e8a8"
    )

    val $selector =
      $doc("oid" -> oid) ++
        search.name.??(name => $or($doc("shortName" $regex (name, "i")), $doc("name" $regex (name, "i")))) ++
        search.glyph.??(glyph => $doc("glyphs" -> glyph)) ++
        search.color.??(color => $doc("color" -> (chess.Color.White.name == color))) ++
        search.role.??(role => $doc("role" -> role)) ++
        search.uci.??(uci => $doc("uci" -> (castlingMap.get(uci) | uci))) ++
        search.fen.??(fen => $doc("fen" -> Forsyth.toEPD(fen)))

    coll.find($selector)
      .sort($doc("order" -> 1))
      .list[OpeningDBNode](20)
  }

  def findPage(page: Int, oid: OpeningDB.ID): Fu[Paginator[OpeningDBNode]] = {
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = byOpeningdbId(oid),
        projection = $empty,
        sort = $sort desc "_id"
      ),
      currentPage = page,
      maxPerPage = MaxPerPage(2000)
    )
  }

  def insert(opening: OpeningDBNode): Funit =
    coll.insert(opening).void

  def batchInsert(nodes: List[OpeningDBNode]): Funit =
    coll.bulkInsert(
      documents = nodes.map(OpeningDBNodeHandler.write).toStream,
      ordered = true
    ).void

  def updateExtName(ids: List[OpeningDBNode.ID], names: List[Option[String]]): Funit =
    names.map { name =>
      name.?? { n => coll.update($inIds(ids), $addToSet("extName" -> n), multi = true).void }
    }.sequenceFu.void

  def update(opening: OpeningDBNode): Funit =
    coll.update($id(opening.id), opening).void

  def updateName(id: OpeningDBNode.ID, name: String): Funit =
    coll.update(
      $id(id),
      $set(
        "name" -> name,
        "updatedAt" -> DateTime.now
      )
    ).void

  def updateShortName(id: OpeningDBNode.ID, shortName: String): Funit =
    coll.update(
      $id(id),
      $set(
        "shortName" -> shortName,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setShapes(id: OpeningDBNode.ID, shapes: lila.tree.Node.Shapes): Funit =
    coll.update(
      $id(id),
      $set(
        "shapes" -> shapes,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setComments(id: OpeningDBNode.ID, comments: lila.tree.Node.Comments): Funit =
    coll.update(
      $id(id),
      $set(
        "comments" -> comments,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setGlyphs(id: OpeningDBNode.ID, glyphs: chess.format.pgn.Glyphs): Funit =
    coll.update(
      $id(id),
      $set(
        "glyphs" -> glyphs,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setUnDelete(oid: OpeningDB.ID, ids: List[OpeningDBNode.ID]): Funit =
    coll.update(
      byOpeningdbId(oid) ++ $inIds(ids),
      $set("delete" -> false),
      multi = true
    ).void

  def setDelete(oid: OpeningDB.ID): Funit =
    coll.update(
      byOpeningdbId(oid) ++ $doc("delete" $exists false),
      $set("delete" -> true),
      multi = true
    ).void

  def unsetDelete(oid: OpeningDB.ID): Funit =
    coll.update(
      byOpeningdbId(oid),
      $unset("delete"),
      multi = true
    ).void

  def removeByDeleteFlag(oid: OpeningDB.ID): Fu[Int] =
    coll.remove(byOpeningdbId(oid) ++ $doc("delete" -> true)).map(_.n)

  def remove(id: OpeningDBNode.ID): Funit =
    coll.remove($id(id)).void

  def removeByIds(ids: List[OpeningDBNode.ID]): Funit =
    coll.remove($inIds(ids)).void

  def removeOpeningdbId(oid: OpeningDB.ID): Funit =
    coll.remove(byOpeningdbId(oid)).void

  private def byOpeningdbId(oid: OpeningDB.ID) = $doc("oid" -> oid)

}
