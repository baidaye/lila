package lila.opening

import lila.user.User
import lila.db.dsl._

object OpeningDBOrderRepo {

  private lazy val coll = Env.current.CollOpeningDBOrder

  import BSONHandlers.OpeningDBOrderHandler

  def byId(id: OpeningDBOrder.ID): Fu[Option[OpeningDBOrder]] = coll.byId(id)

  def insert(order: OpeningDBOrder): Funit =
    coll.insert(order).void

  def mine(userId: User.ID): Fu[List[OpeningDBOrder]] =
    coll.find($doc("userId" -> userId))
      .sort($doc("createdAt" -> -1))
      .list[OpeningDBOrder]()

  def mineOfOpeningdb(userId: User.ID, oid: OpeningDB.ID): Fu[List[OpeningDBOrder]] =
    coll.find($doc("userId" -> userId, "oid" -> oid))
      .sort($doc("createdAt" -> -1))
      .list[OpeningDBOrder]()

}
