package lila.opening

import lila.user.User
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import lila.opening.DataForm.RecordSearchData

object OpeningDBRecordRepo {

  private lazy val coll = Env.current.CollOpeningDBRecord

  import BSONHandlers.OpeningDBRecordHandler

  def byId(id: OpeningDBRecord.ID): Fu[Option[OpeningDBRecord]] = coll.byId(id)

  def insert(record: OpeningDBRecord): Funit =
    coll.insert(record).void

  def batchInsert(records: List[OpeningDBRecord]): Funit =
    coll.bulkInsert(
      documents = records.map(OpeningDBRecordHandler.write).toStream,
      ordered = true
    ).void

  def update(id: OpeningDBRecord.ID, name: String, desc: String): Funit =
    coll.update(
      $id(id),
      $set("name" -> name, "desc" -> desc)
    ).void

  def remove(id: OpeningDBRecord.ID): Funit =
    coll.remove($id(id)).void

  def removeByOpeningdbId(oid: OpeningDB.ID): Funit =
    coll.remove($doc("oid" -> oid)).void

  def findByOpenigndb(oid: OpeningDB.ID): Fu[List[OpeningDBRecord]] =
    coll.find($doc("oid" -> oid))
      .list()

  def page(page: Int, openingdb: OpeningDB, search: RecordSearchData): Fu[Paginator[OpeningDBRecord]] = {
    var selector = $doc("oid" -> openingdb.id) ++
      search.name.??(name => $or($doc("shortName" $regex (name, "i")), $doc("name" $regex (name, "i")))) ++
      search.source.??(source => $doc("source" -> source)) ++
      search.pgn.??(pgn => $doc("pgn" $regex (pgn, "i")))

    if (search.dateMin.isDefined || search.dateMax.isDefined) {
      var dateRange = $doc()
      search.dateMin foreach { dateMin =>
        dateRange = dateRange ++ $gte(dateMin.withTimeAtStartOfDay())
      }
      search.dateMax foreach { dateMax =>
        dateRange = dateRange ++ $lte(dateMax.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999))
      }
      selector = selector ++ $doc("createdAt" -> dateRange)
    }

    val adapter = new Adapter[OpeningDBRecord](
      collection = coll,
      selector = selector,
      projection = $empty,
      sort = $sort desc "createdAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

}
