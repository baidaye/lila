package lila.opening

import lila.user.User
import lila.common.paginator.Paginator
import lila.tree.Node.{ Comment, Comments, Shapes }
import chess.format.{ FEN, Forsyth, Uci }
import chess.format.pgn.{ Glyph, Glyphs }
import lila.common.MaxPerPage
import lila.hub.actorApi.member.{ OpeningdbCreateBuyPayed, OpeningdbSysBuyPayed, OpeningdbUpdatePrice }
import lila.opening.DataForm.SystemSettingData
import lila.opening.OpeningDB.FreeRole
import lila.opening.OpeningDBRecord.Source
import org.joda.time.DateTime
import play.api.libs.json.Json

final class OpeningDBApi(bus: lila.common.Bus) {

  def byId(id: OpeningDB.ID): Fu[Option[OpeningDB]] =
    OpeningDBRepo.byId(id)

  def nodeById(nodeId: OpeningDBNode.ID): Fu[Option[OpeningDBNode]] =
    OpeningDBNodeRepo.byId(nodeId)

  def pgnNodes(node: OpeningDBNode): Fu[List[OpeningDBNode]] = {
    val virtualNodes = PgnParser.processToNodes(node.pgn, node.oid, None, None, User.virtualId)._1
    OpeningDBNodeRepo.optionsByOrderedIds(virtualNodes.map(_.id)) map { dbNodes =>
      virtualNodes.map { vNode =>
        (dbNodes.find(_.exists(_.id == vNode.id)) | vNode.some) | vNode
      }
    }
  }

  def systemTags(): Fu[Set[String]] = OpeningDBRepo.systemTags()

  def minePage(user: User, page: Int, data: DataForm.PagerSearch): Fu[Paginator[OpeningDB]] =
    OpeningDBRepo.minePage(user.id, page, data)

  def systemPage(user: User, page: Int, data: DataForm.PagerSearch): Fu[Paginator[OpeningDB]] =
    OpeningDBRepo.systemPage(user.id, page, data)

  def memberPage(user: User, page: Int, data: DataForm.PagerSearch, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Paginator[OpeningDB]] =
    OpeningDBRepo.memberPage(user.id, page, data, mineCoaches, mineTeamOwners)

  def visiblePage(user: User, page: Int, data: DataForm.PagerSearch, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Paginator[OpeningDB]] =
    OpeningDBRepo.visiblePage(user.id, page, data, mineCoaches, mineTeamOwners)

  def systemBuyPage(user: User, page: Int, data: DataForm.PagerSearch, openingdbIds: List[OpeningDB.ID]): Fu[Paginator[OpeningDB]] =
    for {
      freeRoleList <- OpeningDBRepo.freeRoleList(data, FreeRole.toRoleList(user).map(_.id))
      freeRoleIds = freeRoleList.map(_.id)
      buyList <- OpeningDBRepo.systemBuyList(data, openingdbIds.filterNot(freeRoleIds.contains(_)))
    } yield {
      val ids2 = buyList.map(_.id)
      val orderedOpeningDBs = openingdbIds.filter(ids2.contains(_)).map { id => buyList.find(o => o.id == id) err s"can not find openingdb $id" }
      Paginator.fromList(orderedOpeningDBs ++ freeRoleList, page, MaxPerPage(30))
    }

  def systemBuyList(user: User, forever: Boolean = false): Fu[List[OpeningDB]] = {
    val emptySearch = DataForm.PagerSearch()
    for {
      orders <- lila.opening.OpeningDBOrderRepo.mine(user.id)
      openingdbIds = orders.filter(o => !forever || (forever && o.isForever)).filterNot(_.expired).map(_.oid).distinct
      freeRoleList <- OpeningDBRepo.freeRoleList(emptySearch, FreeRole.toRoleList(user).map(_.id))
      freeRoleIds = freeRoleList.map(_.id)
      buyList <- OpeningDBRepo.systemBuyList(emptySearch, openingdbIds.filterNot(freeRoleIds.contains(_)))
    } yield {
      val ids2 = buyList.map(_.id)
      val orderedOpeningDBs = openingdbIds.filter(ids2.contains(_)).map { id => buyList.find(o => o.id == id) err s"can not find openingdb $id" }
      orderedOpeningDBs ++ freeRoleList
    }
  }

  def canWrittenList(user: User): Fu[List[OpeningDB]] =
    OpeningDBRepo.canWrittenList(user.id)

  def create(user: User, data: DataForm.CreateData): Funit = {
    //val fen = (Forsyth <<<@ (FromPosition, data.initialFen)).map(Forsyth >> _) | Forsyth.initial
    val o = OpeningDB.make(data.name, data.desc, Forsyth.initial, user.id)
    OpeningDBRepo.insert(o)
  }

  def update(user: User, o: OpeningDB, data: DataForm.UpdateData): Funit = {
    OpeningDBRepo.update(o.id, data.name, data.desc, data.color)
  }

  def setClean(openingdb: OpeningDB, clean: Int, cleanBy: User.ID): Funit = {
    OpeningDBRepo.setClean(openingdb.id, clean, cleanBy.some)
  }

  def copyFrom(frOpeningdb: OpeningDB, toOpeningdb: OpeningDB) = {
    val newToOpeningdb = toOpeningdb.cpFrom(frOpeningdb)
    OpeningDBRepo.update(newToOpeningdb) >>
      OpeningDBNodeRepo.removeOpeningdbId(toOpeningdb.id) >> {
        logger.info(s"开始复制，fr：${frOpeningdb.id} ${frOpeningdb.name}，to：${toOpeningdb.id} ${toOpeningdb.name}")
        copyOpeningdbs(1, frOpeningdb, toOpeningdb)
      } >> OpeningDBRecordRepo.removeByOpeningdbId(toOpeningdb.id) >>
      OpeningDBRecordRepo.findByOpenigndb(frOpeningdb.id) flatMap { list =>
        val newRecodes = list.map(_.cp(toOpeningdb.id))
        OpeningDBRecordRepo.batchInsert(newRecodes)
      }
  }

  private def copyOpeningdbs(page: Int, frOpeningdb: OpeningDB, toOpeningdb: OpeningDB): Funit = {
    OpeningDBNodeRepo.findPage(page, frOpeningdb.id) flatMap { pager =>
      val newNodes = pager.currentPageResults.map(_.cp(toOpeningdb.id, toOpeningdb.ownerId))
      OpeningDBNodeRepo.batchInsert(newNodes.toList) >> {
        logger.info(s"复制节点，fr：${frOpeningdb.id} ${frOpeningdb.name}，to：${toOpeningdb.id} ${toOpeningdb.name}，page：$page，nodes：${newNodes.length}")
        pager.hasNextPage.?? {
          copyOpeningdbs(page + 1, frOpeningdb, toOpeningdb)
        }
      }
    }
  }

  def addNodes(openingdb: OpeningDB, data: DataForm.AddNodesData, user: User): Fu[List[OpeningDBNode]] = {
    val nodes = PgnParser.processToNodes(data.pgn, openingdb.id, data.name, data.shortName, user.id)._1
    OpeningDBNodeRepo.byIds(nodes.map(_.id)) flatMap { existsNodes =>
      val existsIds = existsNodes.map(_.id)
      val storeNodes = nodes.filterNot(n => existsIds.contains(n.id))
      val record = OpeningDBRecord.make(
        pgn = data.pgn,
        name = data.name,
        shortName = data.shortName,
        source = data.realSource,
        metadata = data.realSource match {
          case Source.Analysis => OpeningDBRecord.Metadata()
          case Source.Study => OpeningDBRecord.Metadata(chapterId = data.rel.some)
          case Source.Game => OpeningDBRecord.Metadata(gameId = data.rel.some)
          case Source.Homework => OpeningDBRecord.Metadata(taskId = data.rel.some)
          case Source.Task => OpeningDBRecord.Metadata(taskId = data.rel.some)
          case Source.OlClass => OpeningDBRecord.Metadata(olclassChapterId = data.rel.some)
          case Source.Openingdb => OpeningDBRecord.Metadata(openingdbId = data.rel.some)
        },
        oid = openingdb.id,
        userId = user.id
      )

      storeNodes.nonEmpty.?? { OpeningDBNodeRepo.batchInsert(storeNodes) } >>
        storeNodes.nonEmpty.?? { OpeningDBNodeRepo.updateExtName(existsIds, List(data.name, data.shortName)) } >>
        storeNodes.nonEmpty.?? { OpeningDBRepo.incNodes(openingdb.id, storeNodes.size) } >>
        { OpeningDBRecordRepo.insert(record) } inject storeNodes
    }
  }

  def addNode(openingdb: OpeningDB, data: DataForm.AddNodeData, user: User): Fu[OpeningDBNode] = {
    val uci = Uci(data.uci) err s"Bad UCI: ${data.uci}"
    val color = chess.Color(data.color) err s"Bad Color: ${data.color}"
    val role = data.role match {
      case "knight" => chess.Knight
      case "bishop" => chess.Bishop
      case "rook" => chess.Rook
      case "queen" => chess.Queen
      case "king" => chess.King
      case "pawn" => chess.Pawn
    }

    val node = OpeningDBNode.make(
      oid = openingdb.id,
      name = None,
      shortName = None,
      move = Uci.WithSan(uci, data.san),
      piece = chess.Piece(color, role),
      fen = FEN(Forsyth.toEPD(data.fen)),
      prevFen = FEN(Forsyth.toEPD(data.prevFen)),
      check = data.check,
      pgn = data.pgn,
      createdBy = user.id
    )

    OpeningDBNodeRepo.byId(node.id) flatMap {
      case None => OpeningDBNodeRepo.insert(node) >> OpeningDBRepo.incNodes(openingdb.id, 1) inject node.copy(tmp = false)
      case Some(n) => fuccess(n)
    }
  }

  def deleteNode(openingdb: OpeningDB, node: OpeningDBNode): Funit =
    OpeningDBNodeRepo.remove(node.id)

  def startClean() = {
    OpeningDBRepo.getClean().foreach { openingdbs =>
      openingdbs.foreach { openingdb =>
        cleanOneOpeningdb(openingdb)
      }
    }
  }

  def cleanOneOpeningdb(openingdb: OpeningDB): Funit = {
    logger.info(s"开始清理【openingdb】：${openingdb.name} ${openingdb.id}")
    OpeningDBRepo.setClean(openingdb.id, 2) >>
      flagOpeningdbNode(openingdb, Forsyth.toEPD(openingdb.initialFen)) >> {
        logger.info(s"开始清理【openingdbnode】：${openingdb.name} ${openingdb.id}")
        OpeningDBNodeRepo.setDelete(openingdb.id) >>
          OpeningDBNodeRepo.removeByDeleteFlag(openingdb.id) flatMap { n =>
            logger.info(s"清理结束【openingdbnode】：${openingdb.name} ${openingdb.id}，共清理：$n")
            OpeningDBNodeRepo.unsetDelete(openingdb.id) >>- bus.publish(CleanOpeningdbNotify(openingdb, n), 'cleanOpeningdbNotify)
          }
      } >> OpeningDBRepo.setClean(openingdb.id, 0) >>
      OpeningDBNodeRepo.countByOpeningId(openingdb.id).flatMap { count =>
        OpeningDBRepo.setNodes(openingdb.id, count)
      }
  }

  def flagOpeningdbNode(openingdb: OpeningDB, prevFen: String): Funit = {
    logger.info(s"开始染色【openingdbnode】：${openingdb.name} ${openingdb.id}，$prevFen")
    OpeningDBNodeRepo.findNextNodes(openingdb.id, prevFen) flatMap { nodes =>
      nodes.nonEmpty.?? {
        OpeningDBNodeRepo.setUnDelete(openingdb.id, nodes.map(_.id)) >> {
          nodes.map { childNode =>
            flagOpeningdbNode(openingdb, childNode.fen.value)
          }.sequenceFu.void
        }
      }
    }
  }

  def setNodeName(openingdb: OpeningDB, node: OpeningDBNode, name: String): Funit =
    OpeningDBNodeRepo.updateName(node.id, name)

  def setNodeShortName(openingdb: OpeningDB, node: OpeningDBNode, shortName: String): Funit =
    OpeningDBNodeRepo.updateShortName(node.id, shortName)

  def setNodeComment(openingdb: OpeningDB, node: OpeningDBNode, comment: Option[String], me: User): Fu[Comments] = {
    val comments = comment.map { c =>
      Comments(List(Comment(
        id = Comment.Id.make,
        text = Comment.Text(c),
        by = Comment.Author.User(me.id, me.username)
      )))
    } | Comments(Nil)
    OpeningDBNodeRepo.setComments(node.id, comments) inject comments
  }

  def setNodeShapes(openingdb: OpeningDB, node: OpeningDBNode, shapesJson: String): Fu[Shapes] = {
    import JsonView.shapeReader
    val shapeList = Json.parse(shapesJson).asOpt[List[lila.tree.Node.Shape]] | Nil
    val shapes = Shapes(shapeList take 32)
    OpeningDBNodeRepo.setShapes(node.id, shapes) inject shapes
  }

  def toggleNodeGlyph(openingdb: OpeningDB, node: OpeningDBNode, glyphId: Int): Fu[Glyphs] = {
    Glyph.find(glyphId) match {
      case Some(glyph) => {
        val newGlyphs = node.glyphs.toggle(glyph)
        OpeningDBNodeRepo.setGlyphs(node.id, newGlyphs) inject newGlyphs
      }
      case None => fuccess(node.glyphs)
    }
  }

  def systemSetting(openingdb: OpeningDB, data: SystemSettingData): Funit = {
    val newOpeningdb = openingdb.copy(
      price = Some(data.price),
      freeRole = data.realFreeRoles,
      tags = data.realTags,
      sells = Some(data.sells)
    )
    OpeningDBRepo.update(newOpeningdb) >>- (openingdb.system && openingdb.price.exists(p => p != data.price)).??(bus.publish(OpeningdbUpdatePrice(openingdb.id, data.price), 'openingdbUpdatePrice))
  }

  def addMembers(openingdb: OpeningDB, members: List[User.ID]): Funit = {
    val newMembers = openingdb.members ++ members.map(m => OpeningDBMember make m)
    OpeningDBRepo.setMembers(openingdb.id, newMembers)
  }

  def addMember(openingdb: OpeningDB, member: User): Funit =
    (!openingdb.members.contains(member)).?? {
      OpeningDBRepo.addMember(openingdb.id, OpeningDBMember make member)
    }

  def removeMemberById(id: OpeningDB.ID, member: User): Funit =
    byId(id).flatMap {
      _.?? { openingdb =>
        removeMember(openingdb, member)
      }
    }

  def removeMembers(openingdbs: List[OpeningDB], member: User): Funit = {
    val filteredCapsules = openingdbs.filter(_.isReaderOrWriter(member.id))
    OpeningDBRepo.removeMembers(filteredCapsules.map(_.id), member.id)
  }

  def removeMember(openingdb: OpeningDB, member: User): Funit =
    (openingdb.members.contains(member) && !openingdb.members.isOwner(member.id)).?? {
      OpeningDBRepo.removeMember(openingdb.id, member.id)
    }

  def quitMember(openingdb: OpeningDB, member: User): Funit =
    (openingdb.members.contains(member) && !openingdb.members.isOwner(member.id)).?? {
      OpeningDBRepo.quitMember(openingdb.id, member.id)
    }

  def setMemberRoleById(id: OpeningDB.ID, member: User, role: String): Funit =
    byId(id).flatMap {
      _.?? { openingdb =>
        setMemberRole(openingdb, member, role)
      }
    }

  def setMemberRole(openingdb: OpeningDB, member: User, role: String): Funit =
    (openingdb.members.contains(member) && !openingdb.members.isOwner(member.id)).?? {
      val r = OpeningDBMember.Role.byId.getOrElse(role, OpeningDBMember.Role.Read)
      (member.isCoach || r != OpeningDBMember.Role.Write).?? {
        OpeningDBRepo.setMemberRole(openingdb.id, member.id, r)
      }
    }

  def setVisibility(openingdb: OpeningDB, visibility: String, user: User): Funit =
    OpeningDBRepo.setVisibility(openingdb.id, lila.opening.OpeningDB.Visibility.apply(visibility))

  def openingdbSysBuyPayed(data: OpeningdbSysBuyPayed): Funit = {
    OpeningDBOrderRepo.mineOfOpeningdb(data.userId, data.openingdbId).flatMap { orders =>
      val startDate = if (orders.nonEmpty) orders.maxBy(_.expireAt).expireAt else DateTime.now()
      val order = OpeningDBOrder(
        _id = data.orderId,
        userId = data.userId,
        oid = data.openingdbId,
        expireAt = startDate.plusDays(data.days),
        createdAt = DateTime.now
      )
      OpeningDBOrderRepo.insert(order) >> OpeningDBRepo.incSells(data.openingdbId, 1)
    }
  }

  def openingdbCreateBuyPayed(data: OpeningdbCreateBuyPayed): Funit = {
    (1 to data.count).map { _ =>
      OpeningDBRepo.insert({
        OpeningDB.make("新购开局库", "新购开局库", Forsyth.initial, data.userId)
      })
    }.sequenceFu.void
  }

}
