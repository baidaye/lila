package lila.opening

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.hub.actorApi.member.{ OpeningdbCreateBuyPayed, OpeningdbSysBuyPayed }
import org.joda.time.DateTime

import scala.concurrent.duration._

final class Env(
    config: Config,
    db: lila.db.Env,
    system: ActorSystem,
    lightUserApi: lila.user.LightUserApi
) {

  private val AnimationDuration = config duration "animation.duration"
  private val CollectionOpeningDB = config getString "collection.openingdb"
  private val CollectionOpeningDBNode = config getString "collection.openingdb_node"
  private val CollectionOpeningDBRecord = config getString "collection.openingdb_record"
  private val CollectionOpeningDBOrder = config getString "collection.openingdb_order"

  lazy val CollOpeningDB = db(CollectionOpeningDB)
  lazy val CollOpeningDBNode = db(CollectionOpeningDBNode)
  lazy val CollOpeningDBRecord = db(CollectionOpeningDBRecord)
  lazy val CollOpeningDBOrder = db(CollectionOpeningDBOrder)

  lazy val api = new OpeningDBApi(system.lilaBus)

  lazy val form = new DataForm()

  lazy val jsonView = new JsonView(animationDuration = AnimationDuration, lightUserApi.sync)

  // 每天2点定时处理
  system.scheduler.schedule((initialDelay) minute, 1 day) {
    api.startClean()
  }

  //  system.scheduler.schedule(10 seconds, 1 minute) {
  //    api.startClean()
  //  }

  private def initialDelay = {
    val now = DateTime.now
    val nextTick = now.plusDays(1).withTime(2, 0, 0, 0)
    val delay = nextTick.getMillis - now.getMillis
    val delayMinute = delay / 1000 / 60
    delayMinute
  }

  system.lilaBus.subscribeFun('openingdbSysBuyPayed, 'openingdbCreateBuyPayed) {
    case data: OpeningdbSysBuyPayed => api.openingdbSysBuyPayed(data)
    case data: OpeningdbCreateBuyPayed => api.openingdbCreateBuyPayed(data)
  }

}

object Env {

  lazy val current: Env = "opening" boot new Env(
    config = lila.common.PlayApp loadConfig "opening",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system,
    lightUserApi = lila.user.Env.current.lightUserApi
  )
}
