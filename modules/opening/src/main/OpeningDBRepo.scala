package lila.opening

import reactivemongo.api.ReadPreference
import reactivemongo.bson.BSONDocument
import lila.user.User
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.opening.DataForm.PagerSearch
import org.joda.time.DateTime
import OpeningDB._

object OpeningDBRepo {

  private lazy val coll = Env.current.CollOpeningDB

  import BSONHandlers.MemberRoleBSONHandler
  import BSONHandlers.MemberBSONWriter
  import BSONHandlers.MembersBSONHandler
  import BSONHandlers.VisibilityHandler
  import BSONHandlers.OpeningDBHandler

  def byId(id: ID): Fu[Option[OpeningDB]] = coll.byId(id)

  def byIds(ids: List[OpeningDB.ID]): Fu[List[OpeningDB]] =
    coll.byIds[OpeningDB](ids)

  def byOrderedIds(ids: List[OpeningDB.ID]): Fu[List[OpeningDB]] =
    coll.byOrderedIds[OpeningDB, String](ids, readPreference = ReadPreference.secondaryPreferred)(_.id)

  def getClean(): Fu[List[OpeningDB]] =
    coll.find($doc("clean" -> 1)).list()

  def insert(opening: OpeningDB): Funit =
    coll.insert(opening).void

  def update(id: OpeningDB.ID, name: String, desc: String, color: chess.Color): Funit =
    coll.update(
      $id(id),
      $set("name" -> name, "desc" -> desc, "orientation" -> color.white)
    ).void

  def update(openingdb: OpeningDB): Funit =
    coll.update($id(openingdb.id), openingdb).void

  def setClean(id: ID, clean: Int, cleanBy: Option[User.ID] = None): Funit =
    coll.update($id(id), $set("clean" -> clean) ++ cleanBy.??(u => $set("cleanBy" -> cleanBy))).void

  def remove(id: OpeningDB.ID): Funit =
    coll.remove($id(id)).void

  def systemTags(): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("system" -> true).some)

  def memberFor(ownerId: User.ID, memberId: User.ID): Fu[List[OpeningDB]] = {
    coll.find($doc(s"members.$memberId.role" $in List("r", "w")) ++ byOwner(ownerId))
      .sort(descByUpdateAt)
      .list(1000)
  }

  def mineList(userId: User.ID): Fu[List[OpeningDB]] =
    coll.find(byOwner(userId))
      .sort(descByUpdateAt)
      .list()

  def memberList(userId: User.ID, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[List[OpeningDB]] =
    coll.find(
      $doc(s"members.$userId.role" $in List("w", "r")) ++ $doc("ownerId" $in mineCoaches ++ mineTeamOwners)
    ).sort(descByUpdateAt).list()

  def visibleList(userId: User.ID, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[List[OpeningDB]] = {
    coll.find(
      $or(
        $doc("ownerId" $in mineCoaches.filterNot(_ == userId)) ++ selectStudent,
        $doc("ownerId" $in mineTeamOwners.filterNot(_ == userId)) ++ selectTeam
      )
    ).sort(descByUpdateAt).list()
  }

  def freeRoleList(search: PagerSearch, roleIds: List[String]): Fu[List[OpeningDB]] =
    coll.find($doc("system" -> true, "freeRole" $in roleIds) ++ byText(search.text) ++ tagSelector(search)).sort($sort desc "sells").list()

  def systemBuyList(search: PagerSearch, ids: List[OpeningDB.ID]): Fu[List[OpeningDB]] =
    coll.find($doc("system" -> true, "_id" $in ids) ++ byText(search.text) ++ tagSelector(search)).list()

  def minePage(userId: User.ID, page: Int, search: PagerSearch): Fu[Paginator[OpeningDB]] =
    findPage(page, byOwner(userId) ++ byText(search.text), descByUpdateAt)

  def systemPage(userId: User.ID, page: Int, search: PagerSearch): Fu[Paginator[OpeningDB]] =
    findPage(page, $doc("system" -> true) ++ byText(search.text) ++ tagSelector(search), $sort desc "sells")

  def memberPage(userId: User.ID, page: Int, search: PagerSearch, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Paginator[OpeningDB]] = {
    val $d = byText(search.text) ++
      $doc(s"members.$userId.role" $in List("w", "r")) ++
      $doc("ownerId" $in mineCoaches ++ mineTeamOwners)
    findPage(page, $d, descByUpdateAt)
  }

  def visiblePage(userId: User.ID, page: Int, search: PagerSearch, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Paginator[OpeningDB]] = {
    val $d = byText(search.text) ++
      $or(
        $doc("ownerId" $in mineCoaches.filterNot(_ == userId)) ++ selectStudent,
        $doc("ownerId" $in mineTeamOwners.filterNot(_ == userId)) ++ selectTeam
      )
    findPage(page, $d, descByUpdateAt)
  }

  def findPage(page: Int, $selector: BSONDocument, $sort: BSONDocument): Fu[Paginator[OpeningDB]] = {
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = $selector,
        projection = $empty,
        sort = $sort
      ),
      currentPage = page,
      maxPerPage = MaxPerPage(30)
    )
  }

  def canWrittenList(userId: User.ID): Fu[List[OpeningDB]] =
    coll.find(byWritten(userId))
      .sort(descByUpdateAt)
      .list[OpeningDB]()

  def incNodes(id: ID, inc: Int): Funit =
    coll.update($id(id), $set("updatedAt" -> DateTime.now) ++ $inc("nodes" -> inc)).void

  def setNodes(id: ID, nodes: Int): Funit =
    coll.update($id(id), $set("nodes" -> nodes)).void

  def incSells(id: ID, inc: Int): Funit =
    coll.update($id(id), $inc("sells" -> inc, "realSells" -> inc)).void

  def setMembers(id: OpeningDB.ID, members: OpeningDBMembers): Funit =
    coll.update(
      $id(id),
      $set("members" -> members)
    ).void

  def addMember(id: OpeningDB.ID, member: OpeningDBMember): Funit =
    coll.update(
      $id(id),
      $set(s"members.${member.id}" -> member)
    ).void

  def removeMember(id: OpeningDB.ID, memberId: User.ID): Funit =
    coll.update(
      $id(id),
      $unset(s"members.$memberId")
    ).void

  def removeMembers(ids: List[OpeningDB.ID], memberId: User.ID): Funit =
    coll.update(
      $inIds(ids),
      $unset(s"members.$memberId"),
      multi = true
    ).void

  def quitMember(id: OpeningDB.ID, memberId: User.ID): Funit =
    coll.update(
      $id(id),
      $unset(s"members.$memberId")
    ).void

  def setMemberRole(id: OpeningDB.ID, memberId: User.ID, role: OpeningDBMember.Role): Funit =
    coll.update(
      $id(id),
      $set(s"members.$memberId.role" -> role)
    ).void

  def setVisibility(id: OpeningDB.ID, visibility: Visibility): Funit =
    coll.update(
      $id(id),
      $set("visibility" -> visibility.key)
    ).void

  private def descByUpdateAt = $sort desc "updatedAt"

  private def byOwner(userId: User.ID) = $doc("ownerId" -> userId)

  private def byWritten(userId: User.ID) = $doc(s"members.$userId.role" $in List("o", "w"))

  private def byText(text: Option[String]) = text.??(t => $or($doc("name" $regex (t, "i")), $doc("desc" $regex (t, "i"))))

  private val selectStudent = $doc("visibility" -> VisibilityHandler.write(OpeningDB.Visibility.Student))
  private val selectTeam = $doc("visibility" -> VisibilityHandler.write(OpeningDB.Visibility.Team))
  private val selectPrivate = "visibility" -> VisibilityHandler.write(OpeningDB.Visibility.Private)
  private val selectAuth = "visibility" $in List(OpeningDB.Visibility.Student.key, OpeningDB.Visibility.Team.key)

  private def tagSelector(search: PagerSearch) = {
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    (!$andConditions.empty).?? { $and($andConditions: _*) }
  }

}
