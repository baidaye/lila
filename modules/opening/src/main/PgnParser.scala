package lila.opening

import chess.{ Game, Replay }
import chess.format.pgn._
import chess.format.{ FEN, Forsyth, Uci }
import scalaz.{ Failure, Success }
import lila.user.User

object PgnParser {

  def valid(pgn: String, mustPlayable: Boolean = true): Boolean = {
    preprocess(pgn) match {
      case Success((replay, initialFen)) => {
        initialFen.some.exists(f => normalizeFen(Forsyth.initial) == normalizeFen(f)) &&
          (!mustPlayable || (mustPlayable && (replay.setup.situation.playable(true) && replay.state.situation.playable(true))))
      }
      case Failure(e) => false
    }
  }

  def validMaxTurns(pgn: String) = {
    Reader.full(pgn).map {
      case Reader.Result.Complete(replay) => replay.moves.length
      case Reader.Result.Incomplete(replay, _) => replay.moves.length
    }
  }.exists { ms =>
    ms <= OpeningDBNode.maxTurns * 2
  }

  def preprocess(pgn: String): Valid[(Replay, String)] = {
    val variant = chess.variant.FromPgn
    Reader.fullWithSans(pgn, sans => sans.copy(value = sans.value take OpeningDBNode.maxTurns * 2), Tags(List(Tag(_.Variant, variant.name)))) map evenIncomplete map {
      case replay @ Replay(setup, _, _) => {
        val initialFen = Forsyth >> setup
        (replay, initialFen)
      }
    }
  }

  def evenIncomplete(result: Reader.Result): Replay = result match {
    case Reader.Result.Complete(replay) => replay
    case Reader.Result.Incomplete(replay, _) => replay
  }

  def toOpeningDB(pgn: String, user: User) = {
    val opening = OpeningDB.make(
      name = s"来自 ${user.username} 的对局",
      desc = s"来自 ${user.username} 的对局",
      initialFen = Forsyth.initial,
      userId = user.id
    )
    val d: (List[OpeningDBNode], String) = processToNodes(pgn, opening.id, None, None, user.id)
    OpeningDB.WithLine(opening.copy(initialFen = d._2), d._1)
  }

  def processToNodes(
    pgn: String,
    oid: OpeningDB.ID,
    name: Option[String],
    shortName: Option[String],
    createdBy: User.ID
  ): (List[OpeningDBNode], String) = {
    preprocess(pgn) match {
      case Success((replay, initialFen)) => {
        val setupGame = replay.setup
        val lastGame = replay.state
        var prevGame = setupGame
        val nodes = (replay.moves.reverse zip lastGame.pgnMoves) map {
          case (moveOrDrop, san) => {
            val currGame = moveOrDrop.fold(prevGame.apply, prevGame.applyDrop)
            val piece = moveOrDrop.fold(_.piece, _.piece)
            val uci = moveOrDrop.fold(_.toUci, _.toUci)
            val move = Uci.WithSan(uci, san)
            val node = makeNode(prevGame, currGame, move, piece, oid, name, shortName, createdBy)
            prevGame = currGame
            node
          }
        }
        (nodes, initialFen)
      }
      case Failure(_) => (Nil, Forsyth.initial)
    }
  }

  private def makeNode(
    prevGame: Game,
    currGame: Game,
    move: Uci.WithSan,
    piece: chess.Piece,
    oid: OpeningDB.ID,
    name: Option[String],
    shortName: Option[String],
    createdBy: User.ID
  ) = {
    val fen = Forsyth >> currGame
    val prevFen = Forsyth >> prevGame
    OpeningDBNode.make(
      oid = oid,
      name = name,
      shortName = shortName,
      move = move,
      piece = piece,
      fen = FEN(Forsyth.toEPD(fen)),
      prevFen = FEN(Forsyth.toEPD(prevFen)),
      check = currGame.situation.check,
      pgn = toPgn(currGame.startedAtTurn, currGame.pgnMoves.toList),
      createdBy = createdBy
    )
  }

  private def toPgn(startedAtTurn: Int, pgnMoves: List[String]) = {
    val pgnMovesWithPly = pgnMoves.zipWithIndex.map {
      case (san, index) => san -> (startedAtTurn + index + 1)
    }

    pgnMovesWithPly match {
      case head :: tail => {
        if (startedAtTurn % 2 == 0) toPgnOf(startedAtTurn, pgnMovesWithPly)
        else s"${plyToNumber(head._2)}... ${head._1} " + toPgnOf(startedAtTurn, tail)
      }
      case Nil => ""
    }
  }

  private def toPgnOf(startedAtTurn: Int, pgnMoves: List[(String, Int)]) = {
    pgnMoves.grouped(2) map {
      case List((wSan, wPly), (bSan, _)) => s"${plyToNumber(wPly)}. $wSan $bSan"
      case List((wSan, wPly)) => s"${plyToNumber(wPly)}. $wSan"
      case _ => ""
    } mkString " "
  }

  private def plyToNumber(ply: Int) = (ply - 1) / 2 + 1

  private def normalizeFen(fen: String) = fen.split(" ")(0)

}

