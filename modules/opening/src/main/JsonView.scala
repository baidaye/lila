package lila.opening

import chess.Pos
import lila.pref.Pref
import lila.common.LightUser
import lila.tree.Node.Shape
import play.api.libs.json._

final class JsonView(animationDuration: scala.concurrent.duration.Duration, lightUser: LightUser.GetterSync) {

  import JsonView._
  import lila.tree.Node.{ commentsWriter, glyphsWriter, shapesWrites }

  def pref(pref: lila.pref.Pref) =
    Json.obj(
      "animationDuration" -> pref.animationFactor * animationDuration.toMillis,
      "coords" -> pref.coords,
      "moveEvent" -> pref.moveEvent,
      "resizeHandle" -> pref.resizeHandle
    ).add("rookCastle" -> (pref.rookCastle == Pref.RookCastle.YES))
      .add("is3d" -> pref.is3d)
      .add("highlight" -> (pref.highlight || pref.isBlindfold))
      .add("destination" -> (pref.destination && !pref.isBlindfold))

  def openingdb(o: OpeningDB) =
    Json.obj(
      "id" -> o.id,
      "name" -> o.name,
      "desc" -> o.desc,
      "members" -> o.members,
      "nodes" -> o.nodes,
      "orientation" -> o.orientation.name,
      "initialFen" -> o.initialFen,
      "enable" -> o.enable,
      "system" -> o.system,
      "clean" -> o.clean,
      "ownerId" -> o.ownerId
    )

  def mineApi(mineList: List[OpeningDB], memberList: List[OpeningDB], visibleList: List[OpeningDB], systemList: List[OpeningDB]) =
    Json.obj(
      "mine" -> JsArray(mineList.map(openingdb)),
      "member" -> JsArray(memberList.map(openingdb)),
      "visible" -> JsArray(memberList.map(openingdb)),
      "system" -> JsArray(systemList.map(openingdb))
    )

  def mineApi2(mineList: List[OpeningDB], systemList: List[OpeningDB]) =
    Json.obj(
      "mine" -> JsArray(mineList.map(openingdb)),
      "system" -> JsArray(systemList.map(openingdb))
    )

  def nodesWithPath(nodes: List[OpeningDBNode]) = {
    var json = Json.obj()
    OpeningDBNode.toPathMap(nodes).foreach {
      case (path, node) => {
        json = json ++ json.add(path, JsArray(List(nodeJson(node))).some)
      }
    }
    json
  }

  def nodesWithDests(nodes: List[OpeningDBNode]) = {
    Json.obj(
      "nodes" -> nodesJson(nodes),
      "dests" -> {
        val dests = OpeningDBHelper.toDestinations(nodes)
        dests.map {
          case (orig, dests) => s"${orig.piotr}${dests.map(_.piotr).mkString}"
        }.mkString(" ")
      }
    )
  }

  def nodesJson(nodes: List[OpeningDBNode]) =
    JsArray(nodes.map { nodeJson })

  def nodeJson(node: OpeningDBNode) =
    Json.obj(
      "id" -> node.id,
      "uciId" -> node.uciId,
      "name" -> node.name,
      "shortName" -> node.shortName,
      "fen" -> node.fen.value,
      "prevFen" -> node.prevFen.value,
      "uci" -> node.move.uci.uci,
      "san" -> node.move.san,
      "color" -> node.piece.color.name,
      "check" -> node.check,
      "comments" -> node.comments,
      "glyphs" -> node.glyphs,
      "shapes" -> node.shapes,
      "tmp" -> node.tmp
    )

  private implicit val memberRoleWrites = Writes[OpeningDBMember.Role] { r =>
    JsString(r.id)
  }

  private implicit val memberWrites: Writes[OpeningDBMember] = Writes[OpeningDBMember] { m =>
    Json.obj("user" -> lightUser(m.id), "role" -> m.role.id, "canWrite" -> m.role.canWrite)
  }

  private implicit val membersWrites: Writes[OpeningDBMembers] = Writes[OpeningDBMembers] { m =>
    Json toJson m.members
  }

}

object JsonView {

  implicit val posReader: Reads[Pos] = Reads[Pos] { v =>
    (v.asOpt[String] flatMap Pos.posAt).fold[JsResult[Pos]](JsError(Nil))(JsSuccess(_))
  }
  implicit val shapeReader: Reads[Shape] = Reads[Shape] { js =>
    js.asOpt[JsObject].flatMap { o =>
      for {
        brush <- o str "brush"
        orig <- o.get[Pos]("orig")
      } yield o.get[Pos]("dest") match {
        case Some(dest) => Shape.Arrow(brush, orig, dest)
        case _ => Shape.Circle(brush, orig)
      }
    }.fold[JsResult[Shape]](JsError(Nil))(JsSuccess(_))
  }

  def openingWithLine(own: OpeningDB.WithLine) =
    Json.obj(
      "id" -> own.opening.id,
      "name" -> own.opening.name,
      "initialFen" -> own.initialFen,
      "mainline" -> own.pgn,
      "mainlineMoves" -> JsArray(
        own.toTurns.map { turn =>
          Json.obj(
            "index" -> turn.number,
            "white" -> turn.white.map { w =>
              Json.obj(
                "san" -> w.move.san,
                "uci" -> w.move.san,
                "fen" -> w.fen.value
              )
            },
            "black" -> turn.black.map { b =>
              Json.obj(
                "san" -> b.move.san,
                "uci" -> b.move.san,
                "fen" -> b.fen.value
              )
            }
          )
        }
      )
    )

  def miniNodesJson(nodes: List[OpeningDBNode]) =
    JsArray(
      nodes.map { miniNodeJson }
    )

  def miniNodeJson(node: OpeningDBNode) =
    Json.obj(
      "uci" -> node.move.uci.uci,
      "san" -> node.move.san
    )

}
