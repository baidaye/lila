package lila.opening

import org.joda.time.DateTime
import ornicar.scalalib.Random
import chess.format.Forsyth
import chess.variant.FromPosition
import lila.user.User
import lila.opening.OpeningDBOrder.OpeningDBOrders
import scala.math.BigDecimal.RoundingMode
import OpeningDB._

case class OpeningDB(
    _id: ID,
    name: String,
    desc: String,
    initialFen: String,
    orientation: chess.Color,
    members: OpeningDBMembers,
    tags: List[String],
    nodes: Int,
    enable: Boolean,
    system: Boolean,
    visibility: Visibility,
    freeRole: Option[List[FreeRole]],
    price: Option[BigDecimal],
    sells: Option[Int], // 显示到页面上的
    realSells: Option[Int], // 实际销售的
    clean: Int, // 0: 默认, 1: 标记清理, 2: 正在清理
    cleanBy: Option[User.ID], // 清除人
    copyFrom: Option[ID], // 复制来源
    ownerId: User.ID,
    createdBy: User.ID,
    createdAt: DateTime,
    updatedAt: DateTime
) {

  def id = _id

  def startedAtTurn = Forsyth.<<<@(FromPosition, initialFen).map(_.turns) | 0

  def startedAtTurnNext = startedAtTurn + 1

  def notEmpty = nodes > 0

  def isFull = nodes >= maxNode

  def isReader(userId: String) = members.isReader(userId)

  def isWriter(userId: String) = members.isWriter(userId)

  def isReaderOrWriter(userId: String) = isReader(userId) || isWriter(userId)

  def canBeCopy(u: User, mineOrders: OpeningDBOrders) =
    if (isOwner(u.id)) true
    else {
      if (u.isCoachOrTeam) false
      else mineOrders.isForever(this, u.id) || systemFree(u)
    }

  def canWrite(userId: User.ID) = members.isContributor(userId)

  def canRead(u: User, mineOrders: OpeningDBOrders, mineCoaches: Set[User.ID], mineTeamOwners: Set[User.ID]) =
    readOrWrite(u, mineOrders, mineCoaches, mineTeamOwners).isDefined

  def readOrWrite(u: User, mineOrders: OpeningDBOrders, mineCoaches: Set[User.ID], mineTeamOwners: Set[User.ID]) =
    {
      if (isOwner(u.id) || u.isMemberOrCoachOrTeam) {
        members.get(u.id).map(_.role)
      } else None
    }
      .orElse {
        if (isOwner(u.id) || u.isMemberOrCoachOrTeam) {
          visibility match {
            case Visibility.Private => if (isOwner(u.id)) Some(OpeningDBMember.Role.Write) else None
            case Visibility.Student => if (mineCoaches.contains(ownerId)) Some(OpeningDBMember.Role.Read) else None
            case Visibility.Team => if (mineTeamOwners.contains(ownerId)) Some(OpeningDBMember.Role.Read) else None
          }
        } else None
      }
      .orElse {
        if (systemFree(u)) Some(OpeningDBMember.Role.Read) else None
      }
      .orElse {
        if (mineOrders.canRead(this, u.id)) Some(OpeningDBMember.Role.Read) else None
      }

  def systemFree(u: User) =
    (freeRole.??(_.contains(FreeRole.Team)) && u.isTeam) ||
      (freeRole.??(_.contains(FreeRole.Coach)) && u.isCoach) ||
      (freeRole.??(_.contains(FreeRole.Gold)) && u.isGold) ||
      (freeRole.??(_.contains(FreeRole.Silver)) && u.isSilver)

  def isBuyForever(u: User, mineOrders: OpeningDBOrders) = systemFree(u) || mineOrders.isForever(this, u.id)

  def roundPrice = price.map(_.setScale(2, RoundingMode.DOWN).toString()) | "0.0"

  def isOwner(userId: User.ID) = ownerId == userId

  def cpFrom(fr: OpeningDB) = {
    copy(
      initialFen = fr.initialFen,
      orientation = fr.orientation,
      nodes = fr.nodes,
      copyFrom = Some(fr.id),
      updatedAt = DateTime.now
    )
  }

}

object OpeningDB {

  type ID = String

  val maxNode = 10000

  def makeId = Random nextString 8

  def make(name: String, desc: String, initialFen: String, userId: User.ID) = {
    val now = DateTime.now
    val owner = OpeningDBMember(id = userId, role = OpeningDBMember.Role.Owner)
    OpeningDB(
      _id = makeId,
      name = name,
      desc = desc,
      initialFen = initialFen,
      orientation = chess.White,
      members = OpeningDBMembers(Map(userId -> owner)),
      tags = Nil,
      nodes = 0,
      enable = true,
      system = false,
      visibility = Visibility.Private,
      freeRole = None,
      price = None,
      sells = None,
      realSells = None,
      clean = 0,
      cleanBy = None,
      copyFrom = None,
      ownerId = userId,
      createdBy = userId,
      createdAt = now,
      updatedAt = now
    )
  }

  //sopz@7day
  def toProductItemCode(id: OpeningDB.ID, expire: String) = s"$id@$expire"

  sealed abstract class FreeRole(val id: String, val name: String)

  object FreeRole {

    case object Silver extends FreeRole(id = "silver", name = "银牌会员")

    case object Gold extends FreeRole(id = "gold", name = "金牌会员")

    case object Coach extends FreeRole(id = "coach", name = "教练")

    case object Team extends FreeRole(id = "team", name = "俱乐部")

    val all = List(Silver, Gold, Coach, Team)

    def choices = all.map(s => s.id -> s.name)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): FreeRole = byId.get(id) err s"Bad FreeRole $this"

    def toRoleList(user: User) =
      List.empty[FreeRole] ++
        user.isSilver.??(List(Silver)) ++
        user.isGold.??(List(Gold)) ++
        user.isCoach.??(List(Coach)) ++
        user.isTeam.??(List(Team))
  }

  sealed trait Visibility {
    lazy val key = toString.toLowerCase
    def name: String
  }

  object Visibility {
    case object Private extends Visibility {
      override def name: String = "私有"
    }
    case object Student extends Visibility {
      override def name: String = "我的学员"
    }
    case object Team extends Visibility {
      override def name: String = "我的俱乐部"
    }
    val byKey = List(Private, Student, Team).map { v => v.key -> v }.toMap

    def apply(key: String) = byKey.get(key) err s"can not find opening visibility of $key"

    def teamSelects = List(Private, Student, Team).map { v => (v.key, v.name) }

    def coachSelects = List(Private, Student).map { v => (v.key, v.name) }

    def privateSelects = List(Private).map { v => (v.key, v.name) }

  }

  case class WithLine(opening: OpeningDB, line: List[OpeningDBNode]) {

    def nonEmpty = line.nonEmpty

    def initialFen = opening.initialFen

    def lastNode = line.lastOption err s"can not find lastNode of ${opening.id}"

    def lastFen = lastNode.fen

    def lastPly = opening.startedAtTurn + line.size

    def startPly = opening.startedAtTurn

    def nextNodes(fen: String): List[OpeningDBNode] = {
      val epd = Forsyth.toEPD(fen)
      line.filter(n => n.prevFen.value == epd)
    }

    def destinations(fen: String) = OpeningDBHelper.toDestinations(nextNodes(fen))

    def toTurns() = {
      val lineWithPly = line.zipWithIndex.map {
        case (node, index) => node -> (startPly + index + 1)
      }
      lineWithPly match {
        case head :: tail => {
          if (startPly % 2 == 0) toTurnsOf(lineWithPly)
          else
            OpeningDBNode.Turn(
              number = plyToNumber(head._2),
              white = None,
              black = head._1.some
            ) +: toTurnsOf(tail)
        }
        case Nil => Nil
      }
    }

    private def toTurnsOf(lineWithPly: List[(OpeningDBNode, Int)]) =
      (lineWithPly grouped 2) map { pair =>
        val first = pair.head
        val second = if (pair.size == 2) pair.lastOption else None
        OpeningDBNode.Turn(
          number = plyToNumber(first._2),
          white = first._1.some,
          black = second.map(_._1)
        )
      } toList

    private def plyToNumber(ply: Int) = (ply - 1) / 2 + 1

    def pgn = toTurns mkString " "
  }

  case class WithNextNodes(opening: OpeningDB, nodes: List[OpeningDBNode]) {

    def destinations = OpeningDBHelper.toDestinations(nodes)

  }

}
