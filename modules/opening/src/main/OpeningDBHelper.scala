package lila.opening

import lila.tree
import chess.Pos
import chess.format.{ Forsyth, Uci, UciCharPair }
import scala.concurrent.duration._
import com.github.blemale.scaffeine.{ Cache, Scaffeine }

object OpeningDBHelper {

  private val openingCache: Cache[OpeningDB.ID, OpeningDB] =
    Scaffeine()
      .expireAfterWrite(20 minutes)
      .build[OpeningDB.ID, OpeningDB]

  private def openingWithNextNodesCacheKey(oid: OpeningDB.ID, epd: String) = s"$oid:$epd"

  private val openingWithNextNodesCache: Cache[String, OpeningDB.WithNextNodes] =
    Scaffeine()
      .expireAfterWrite(20 minutes)
      .build[String, OpeningDB.WithNextNodes]

  def openingFromCache(oid: OpeningDB.ID): OpeningDB = {
    openingCache.getIfPresent(oid) match {
      case Some(opening) => opening
      case None => OpeningDBRepo.byId(oid) awaitSeconds 3 match {
        case Some(opening) => {
          openingCache.put(oid, opening)
          opening
        }
        case None => sys.error(s"can not find opening $oid")
      }
    }
  }

  def openingWithNextNodesFromCache(oid: OpeningDB.ID, fen: String): OpeningDB.WithNextNodes = {
    val epd = Forsyth.toEPD(fen)
    openingWithNextNodesCache.getIfPresent(openingWithNextNodesCacheKey(oid, epd)) match {
      case Some(own) => own
      case None => openingWithNextNodes(oid, epd) awaitSeconds 3 match {
        case Some(own) => {
          openingWithNextNodesCache.put(openingWithNextNodesCacheKey(oid, epd), own)
          own
        }
        case None => sys.error(s"can not find opening $oid")
      }
    }
  }

  def openingWithNextNodes(oid: OpeningDB.ID, epd: String): Fu[Option[OpeningDB.WithNextNodes]] = {
    for {
      opening <- OpeningDBRepo.byId(oid)
      nextNodes <- OpeningDBNodeRepo.findNextNodes(oid, epd)
    } yield opening.map { o => OpeningDB.WithNextNodes(o, nextNodes) }
  }

  def toDestinations(nodes: List[OpeningDBNode]): Map[Pos, List[Pos]] = {
    val castleMoves = nodes.filter(_.castling).map(_.anotherCastlingMove).filter(_.isDefined).map(_.get)
    (nodes.map(n => uciMove(n.move.uci.uci)) ++ castleMoves)
      .groupBy(_.orig)
      .mapValues(_.map(_.dest))
  }

  def toTreeNodes(nodes: List[OpeningDBNode], ply: Int): List[tree.Branch] =
    nodes.map(toTreeNode(_, ply))

  def toTreeNode(node: OpeningDBNode, ply: Int): tree.Branch =
    tree.Branch(
      id = UciCharPair(node.move.uci),
      ply = ply,
      move = node.move,
      fen = node.fen.value,
      check = node.check,
      shapes = node.shapes,
      comments = node.comments,
      glyphs = node.glyphs,
      crazyData = None
    )

  private def uciMove(uci: String) = Uci.Move(uci) err s"Invalid UCI $uci"

}
