package lila.opening

import lila.user.User
import org.joda.time.DateTime
import OpeningDBOrder._

case class OpeningDBOrder(
    _id: ID, // member->orderId
    userId: User.ID,
    oid: OpeningDB.ID,
    expireAt: DateTime,
    createdAt: DateTime
) {

  def id = _id

  def expired = expireAt.isBeforeNow

  def isForever = expireAt.isAfter(new DateTime(2025 + MaxYear, 1, 1, 0, 0))

  def isTrial = !isForever

}

object OpeningDBOrder {

  val MaxYear = 100

  type ID = String

  case class OpeningDBOrders(orders: List[OpeningDBOrder]) {

    def canRead(openingdb: OpeningDB, userId: User.ID) = orders.exists(order => order.oid == openingdb.id && !order.expired)

    def isForever(openingdb: OpeningDB, userId: User.ID) = orders.exists(order => order.oid == openingdb.id && order.isForever)

    def isTrial(openingdb: OpeningDB, userId: User.ID) = orders.exists(order => order.oid == openingdb.id && order.isTrial)

    def lastOrder(openingdb: OpeningDB) = {
      val os = orders.filter(order => order.oid == openingdb.id)
      if (os.isEmpty) None
      else os.maxBy(_.expireAt).some
    }

  }

}
