package lila.opening

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import OpeningDBRecord._

case class OpeningDBRecord(
    _id: ID,
    pgn: String,
    name: Option[String],
    shortName: Option[String],
    source: Source,
    metadata: Metadata,
    oid: OpeningDB.ID,
    copyFrom: Option[ID],
    createdBy: User.ID,
    createdAt: DateTime
) {

  def id = _id

  def creator(user: User) = createdBy == user.id

  val virtualOpeningDB = PgnParser.toOpeningDB(pgn, lila.user.User.virtual(createdBy, createdBy)) |> { o =>
    o.copy(opening = o.opening.copy(_id = oid))
  }

  val lastNodeId = virtualOpeningDB.lastNode.id

  def cp(oid: OpeningDB.ID) = copy(
    _id = OpeningDBRecord.makeId,
    oid = oid,
    copyFrom = Some(id)
  )

}

object OpeningDBRecord {

  type ID = String

  def makeId = Random nextString 8

  def make(
    pgn: String,
    name: Option[String],
    shortName: Option[String],
    source: Source,
    metadata: Metadata,
    oid: OpeningDB.ID,
    userId: User.ID
  ) = {
    val now = DateTime.now
    OpeningDBRecord(
      _id = makeId,
      pgn = pgn,
      name = name,
      shortName = shortName,
      source = source,
      metadata = metadata,
      oid = oid,
      copyFrom = None,
      createdBy = userId,
      createdAt = now
    )
  }

  sealed abstract class Source(val id: String, val name: String)

  object Source {

    case object Analysis extends Source(id = "analysis", name = "开局浏览器（分析）")

    case object Study extends Source(id = "study", name = "研习")

    case object Game extends Source(id = "game", name = "对局")

    case object Homework extends Source(id = "homework", name = "课后练")

    case object Task extends Source(id = "task", name = "任务")

    case object OlClass extends Source(id = "olclass", name = "课件")

    case object Openingdb extends Source(id = "openingdb", name = "开局库")

    val all = List(Analysis, Study, Game, Task, Homework, OlClass, Openingdb)

    def choices = all.map(s => s.id -> s.name)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Source = byId.get(id) err s"Bad Source $this"
  }

  case class Metadata(gameId: Option[String] = None, chapterId: Option[String] = None, olclassChapterId: Option[String] = None, taskId: Option[String] = None, openingdbId: Option[String] = None)

}
