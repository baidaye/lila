package lila.opening

import chess.Pos
import lila.db.BSON
import lila.db.BSON.{ Reader, Writer }
import lila.db.dsl._
import lila.tree.Node.{ Comment, Comments, Shape, Shapes }
import chess.format.pgn.{ Glyph, Glyphs }
import chess.format.{ FEN, Uci }
import reactivemongo.bson._
import org.joda.time.DateTime

import scala.math.BigDecimal.RoundingMode

object BSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler
  implicit val FENBSONHandler = stringAnyValHandler[FEN](_.value, FEN.apply)
  implicit val stringArrayHandler = bsonArrayToListHandler[String]

  implicit val UciHandler = new BSONHandler[BSONString, Uci] {
    def read(bs: BSONString): Uci = Uci(bs.value) err s"Bad UCI: ${bs.value}"
    def write(x: Uci) = BSONString(x.uci)
  }

  import Uci.WithSan
  implicit val UciWithSanBSONHandler = Macros.handler[WithSan]

  implicit val PosBSONHandler = new BSONHandler[BSONString, Pos] {
    def read(bsonStr: BSONString): Pos = Pos.posAt(bsonStr.value) err s"No such pos: ${bsonStr.value}"
    def write(x: Pos) = BSONString(x.key)
  }

  implicit val ShapeBSONHandler = new BSON[Shape] {
    def reads(r: Reader) = {
      val brush = r str "brush"
      r.getO[Pos]("pos") map { pos =>
        Shape.Circle(brush, pos)
      } getOrElse Shape.Arrow(brush, r.get[Pos]("orig"), r.get[Pos]("dest"))
    }
    def writes(w: Writer, t: Shape) = t match {
      case Shape.Circle(brush, pos) => $doc("brush" -> brush, "pos" -> pos.key)
      case Shape.Arrow(brush, orig, dest) => $doc("brush" -> brush, "orig" -> orig.key, "dest" -> dest.key)
    }
  }

  implicit val ShapesBSONHandler: BSONHandler[BSONArray, Shapes] =
    isoHandler[Shapes, List[Shape], BSONArray](
      (s: Shapes) => s.value,
      Shapes(_)
    )

  implicit val CommentIdBSONHandler = stringAnyValHandler[Comment.Id](_.value, Comment.Id.apply)
  implicit val CommentTextBSONHandler = stringAnyValHandler[Comment.Text](_.value, Comment.Text.apply)
  implicit val CommentAuthorBSONHandler = new BSONHandler[BSONValue, Comment.Author] {
    def read(bsonValue: BSONValue): Comment.Author = bsonValue match {
      case BSONString(lila.user.User.lichessId) => Comment.Author.Lichess
      case BSONString(name) => Comment.Author.External(name)
      case doc: Bdoc => {
        for {
          id <- doc.getAs[String]("id")
          name <- doc.getAs[String]("name")
        } yield Comment.Author.User(id, name)
      } err s"Invalid comment author $doc"
      case _ => Comment.Author.Unknown
    }
    def write(x: Comment.Author): BSONValue = x match {
      case Comment.Author.User(id, name) => $doc("id" -> id, "name" -> name)
      case Comment.Author.External(name) => BSONString(s"${name.trim}")
      case Comment.Author.Lichess => BSONString("l")
      case Comment.Author.Unknown => BSONString("")
    }
  }
  implicit val CommentBSONHandler = Macros.handler[Comment]

  implicit val CommentsBSONHandler: BSONHandler[BSONArray, Comments] =
    isoHandler[Comments, List[Comment], BSONArray](
      (s: Comments) => s.value,
      Comments(_)
    )

  implicit val GlyphsBSONHandler = new BSONHandler[Barr, Glyphs] {
    val idsHandler = bsonArrayToListHandler[Int]
    def read(b: Barr) = Glyphs.fromList(idsHandler read b flatMap Glyph.find)
    // must be BSONArray and not $arr!
    def write(x: Glyphs) = BSONArray(x.toList.map(_.id).map(BSONInteger.apply))
  }

  private implicit val RoleBSONHandler = new BSONHandler[BSONString, chess.Role] {
    def read(b: BSONString) = chess.Role.allByForsyth get b.value.head err s"Invalid role ${b.value}"
    def write(e: chess.Role) = BSONString(e.forsyth.toString)
  }

  private implicit val ColorBSONHandler = new BSONHandler[BSONBoolean, chess.Color] {
    def read(b: BSONBoolean) = chess.Color(b.value)
    def write(c: chess.Color) = BSONBoolean(c.white)
  }

  implicit def OpeningDBNodeHandler: BSON[OpeningDBNode] = new BSON[OpeningDBNode] {
    def reads(r: Reader) = OpeningDBNode(
      _id = r str "_id",
      oid = r str "oid",
      name = r strO "name",
      shortName = r strO "shortName",
      extName = r strsD "extName",
      move = WithSan(r.get[Uci]("uci"), r.str("san")),
      piece = chess.Piece(r.get[chess.Color]("color"), r.get[chess.Role]("role")),
      fen = r.get[FEN]("fen"),
      prevFen = r.get[FEN]("prevFen"),
      order = r int "order",
      check = r boolD "check",
      shapes = r.getO[Shapes]("shapes") | Shapes.empty,
      comments = r.getO[Comments]("comments") | Comments.empty,
      glyphs = r.getO[Glyphs]("glyphs") | Glyphs.empty,
      pgn = r.str("pgn"),
      tmp = false,
      createdBy = r str "createdBy",
      createdAt = r.get[DateTime]("createdAt"),
      updatedAt = r.get[DateTime]("updatedAt")
    )
    def writes(w: Writer, o: OpeningDBNode) = $doc(
      "_id" -> o.id,
      "oid" -> o.oid,
      "name" -> o.name,
      "shortName" -> o.shortName,
      "extName" -> o.extName,
      "uci" -> o.move.uci,
      "san" -> o.move.san,
      "fen" -> o.fen,
      "color" -> o.piece.color.white,
      "role" -> o.piece.role.forsyth.toString,
      "prevFen" -> o.prevFen,
      "order" -> o.order,
      "check" -> w.boolO(o.check),
      "shapes" -> o.shapes.value.nonEmpty.option(o.shapes),
      "comments" -> o.comments.value.nonEmpty.option(o.comments),
      "glyphs" -> o.glyphs.nonEmpty,
      "pgn" -> o.pgn,
      "createdBy" -> o.createdBy,
      "createdAt" -> o.createdAt,
      "updatedAt" -> o.updatedAt
    )
  }

  implicit val MemberRoleBSONHandler = new BSONHandler[BSONString, OpeningDBMember.Role] {
    def read(b: BSONString) = OpeningDBMember.Role.byId get b.value err s"Invalid role ${b.value}"
    def write(x: OpeningDBMember.Role) = BSONString(x.id)
  }

  case class DbMember(role: OpeningDBMember.Role)
  implicit val DbMemberBSONHandler = Macros.handler[DbMember]
  implicit val MemberBSONWriter = new BSONWriter[OpeningDBMember, Bdoc] {
    def write(x: OpeningDBMember) = DbMemberBSONHandler write DbMember(x.role)
  }
  implicit val MembersBSONHandler = new BSONHandler[Bdoc, OpeningDBMembers] {
    val mapHandler = BSON.MapDocument.MapHandler[String, DbMember]
    def read(b: Bdoc) = OpeningDBMembers(mapHandler read b map {
      case (id, dbMember) => id -> OpeningDBMember(id, dbMember.role)
    })
    def write(x: OpeningDBMembers) = BSONDocument(x.members.mapValues(MemberBSONWriter.write))
  }

  implicit val OpeningDBFreeRoleBSONHandler = new BSONHandler[BSONString, OpeningDB.FreeRole] {
    def read(b: BSONString) = OpeningDB.FreeRole(b.value)
    def write(x: OpeningDB.FreeRole) = BSONString(x.id)
  }

  implicit val OpeningDBFreeRoleArrayHandler = bsonArrayToListHandler[OpeningDB.FreeRole]

  import OpeningDB.Visibility
  implicit val VisibilityHandler: BSONHandler[BSONString, Visibility] = new BSONHandler[BSONString, Visibility] {
    def read(bs: BSONString) = Visibility.byKey get bs.value err s"Invalid visibility ${bs.value}"
    def write(x: Visibility) = BSONString(x.key)
  }

  implicit object BigDecimalHandler extends BSONHandler[BSONString, BigDecimal] {
    def read(b: BSONString) = BigDecimal(b.value)
    def write(d: BigDecimal) = BSONString(d.setScale(2, RoundingMode.DOWN).toString)
  }

  implicit val OpeningDBHandler = Macros.handler[OpeningDB]

  implicit val OpeningDBRecordSourceBSONHandler = new BSONHandler[BSONString, OpeningDBRecord.Source] {
    def read(b: BSONString) = OpeningDBRecord.Source(b.value)
    def write(s: OpeningDBRecord.Source) = BSONString(s.id)
  }

  import OpeningDBRecord.Metadata
  implicit val OpeningDBRecordMetadataHandler = Macros.handler[OpeningDBRecord.Metadata]

  implicit val OpeningDBRecordHandler = Macros.handler[OpeningDBRecord]

  implicit val OpeningDBOrderHandler = Macros.handler[OpeningDBOrder]

}
