package lila.opening

import play.api.data._
import play.api.data.Forms._
import lila.common.Form.{ ISODate, stringIn }
import lila.opening.OpeningDB.FreeRole
import org.joda.time.DateTime

case class DataForm() {

  import lila.opening.DataForm._

  def pagerSearch = Form(mapping(
    "text" -> optional(nonEmptyText),
    "emptyTag" -> optional(nonEmptyText),
    "tags" -> optional(list(nonEmptyText))
  )(PagerSearch.apply)(PagerSearch.unapply))

  def pagerSearchOf = pagerSearch fill PagerSearch()

  def createForm() = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 32),
    "desc" -> nonEmptyText(minLength = 2, maxLength = 512),
    "initialFen" -> nonEmptyText
  )(CreateData.apply)(CreateData.unapply))

  def createFormOf = createForm fill CreateData("我的开局库", "我的开局库", chess.format.Forsyth.initial)

  def updateForm() = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 32),
    "desc" -> nonEmptyText(minLength = 2, maxLength = 512),
    "orientation" -> stringIn(orientationChoices)
  )(UpdateData.apply)(UpdateData.unapply))

  def updateFormOf(o: OpeningDB) = updateForm fill UpdateData(o.name, o.desc, o.orientation.name)

  def systemSettingForm() = Form(mapping(
    "price" -> bigDecimal(precision = 5, scale = 2),
    "freeRoles" -> optional(list(stringIn(FreeRole.choices))),
    "tags" -> optional(nonEmptyText(minLength = 1, maxLength = 200)),
    "sells" -> number(min = 0, max = 10000)
  )(SystemSettingData.apply)(SystemSettingData.unapply))

  def systemSettingFormOf(openingdb: OpeningDB) = systemSettingForm fill SystemSettingData(
    price = openingdb.price | BigDecimal(0.00),
    freeRoles = openingdb.freeRole.map(_.map(_.id)),
    tags = openingdb.tags.mkString(",").some,
    sells = openingdb.sells | 0
  )

  def membersForm = Form(single("members" -> text))

  def addMemberForm = Form(single("username" -> lila.user.DataForm.historicalUsernameField))

  def setMemberRole = Form(single("role" -> stringIn(OpeningDBMember.Role.selects)))

  def visibilityForm = Form(single("visibility" -> stringIn(OpeningDB.Visibility.teamSelects)))

  val recordSearchForm = Form(mapping(
    "dateMin" -> optional(ISODate.isoDate),
    "dateMax" -> optional(ISODate.isoDate),
    "source" -> optional(stringIn(OpeningDBRecord.Source.choices)),
    "name" -> optional(nonEmptyText),
    "pgn" -> optional(nonEmptyText)
  )(RecordSearchData.apply)(RecordSearchData.unapply))

  val addNodesForm = Form(mapping(
    "tab" -> nonEmptyText,
    "name" -> optional(nonEmptyText(minLength = 2, maxLength = 120)),
    "shortName" -> optional(nonEmptyText(minLength = 2, maxLength = 50)),
    "pgn" -> nonEmptyText.verifying("PGN格式错误 / 开局不是标准局面", PgnParser.valid(_, mustPlayable = false)),
    "source" -> nonEmptyText.verifying("can not apply source", s => OpeningDBRecord.Source.all.map(_.id).contains(s)),
    "rel" -> nonEmptyText
  )(AddNodesData.apply)(AddNodesData.unapply))

  val addNodeForm = Form(mapping(
    "uci" -> nonEmptyText,
    "san" -> nonEmptyText,
    "fen" -> nonEmptyText,
    "prevFen" -> nonEmptyText,
    "color" -> nonEmptyText,
    "role" -> nonEmptyText,
    "check" -> boolean,
    "pgn" -> nonEmptyText
  )(AddNodeData.apply)(AddNodeData.unapply))

  val nodeName = Form(single(
    "name" -> nonEmptyText(minLength = 2, maxLength = 120)
  ))

  val nodeShortName = Form(single(
    "shortName" -> nonEmptyText(minLength = 2, maxLength = 50)
  ))

  val nodeComment = Form(single(
    "comment" -> optional(nonEmptyText(minLength = 2, maxLength = 256))
  ))

  val nodeShape = Form(single(
    "shapes" -> nonEmptyText(minLength = 0, maxLength = 2048)
  ))

  val nodeGlyph = Form(single(
    "glyphId" -> number(min = 1, max = 500)
  ))

  val nodeSearch = Form(mapping(
    "name" -> optional(nonEmptyText),
    "glyph" -> optional(number(min = 1, max = 500)),
    "color" -> optional(nonEmptyText),
    "role" -> optional(nonEmptyText),
    "uci" -> optional(nonEmptyText),
    "fen" -> optional(nonEmptyText)
  )(NodeSearchData.apply)(NodeSearchData.unapply))

}

object DataForm {

  def orientationChoices = List("white" -> "白方", "black" -> "黑方")

  case class PagerSearch(text: Option[String] = None, emptyTag: Option[String] = None, tags: Option[List[String]] = None)

  case class AddNodesData(tab: String, name: Option[String], shortName: Option[String], pgn: String, source: String, rel: String) {

    def realSource = OpeningDBRecord.Source(source)

  }

  case class CreateData(name: String, desc: String, initialFen: String)

  case class UpdateData(name: String, desc: String, orientation: String) {

    def color = chess.Color(orientation) err s"can not apply color ${orientation}"

  }

  case class SystemSettingData(price: BigDecimal, freeRoles: Option[List[String]], tags: Option[String], sells: Int) {

    def realFreeRoles = freeRoles.map(_.map(r => FreeRole(r)))

    def realTags = tags.map(_.split(",").toList) | Nil

  }

  case class RecordSearchData(dateMin: Option[DateTime], dateMax: Option[DateTime], source: Option[String], name: Option[String], pgn: Option[String])

  case class NodeSearchData(name: Option[String], glyph: Option[Int], color: Option[String], role: Option[String], uci: Option[String], fen: Option[String])

  case class AddNodeData(uci: String, san: String, fen: String, prevFen: String, color: String, role: String, check: Boolean, pgn: String)

}

