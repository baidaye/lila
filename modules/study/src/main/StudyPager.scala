package lila.study

import lila.db.dsl._
import lila.common.paginator.Paginator
import lila.db.paginator.{ Adapter, CachedAdapter }
import lila.study.DataForm.PagerSearch
import reactivemongo.bson.BSONDocument
import lila.user.User

final class StudyPager(
    studyRepo: StudyRepo,
    chapterRepo: ChapterRepo,
    favoritesRepo: StudyFavoritesRepo,
    maxPerPage: lila.common.MaxPerPage
) {

  private val defaultNbChaptersPerStudy = 4

  import BSONHandlers._
  import studyRepo.{ selectPublic, selectStudent, selectTeam, selectPrivate, selectAuth, selectMemberId, selectOwnerId, selectFavoriter }

  private def tagSelector(search: PagerSearch) = {
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    (!$andConditions.empty).?? { $and($andConditions: _*) }
  }

  private def allSelector(me: Option[User], mineCoaches: Set[String], mineTeamOwners: Set[String]) = {
    val selector = me.fold(selectPublic) { u =>
      $or(
        selectPublic,
        selectMemberId(u.id),
        $doc("ownerId" $in mineCoaches.filterNot(_ == u.id)) ++ selectStudent,
        $doc("ownerId" $in mineTeamOwners.filterNot(_ == u.id)) ++ selectTeam
      )
    }
    selector
  }

  def allTags(me: Option[User], mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", allSelector(me, mineCoaches, mineTeamOwners).some)

  def all(
    me: Option[User],
    mineCoaches: Set[String],
    mineTeamOwners: Set[String],
    search: PagerSearch,
    order: Order,
    page: Int
  ): Fu[Paginator[Study.WithChaptersAndLiked]] = {
    val newSelector = allSelector(me, mineCoaches, mineTeamOwners) ++ tagSelector(search)
    paginator(newSelector, me, order, page, fuccess(9999).some)
  }

  def byOwner(owner: User, me: Option[User], order: Order, page: Int) = paginator(
    selectOwnerId(owner.id), me, order, page
  )

  def mineTags(me: User): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", selectOwnerId(me.id).some)

  def mine(me: User, search: PagerSearch, order: Order, page: Int) = paginator(
    selectOwnerId(me.id) ++ tagSelector(search), me.some, order, page
  )

  def minePublicTags(me: User): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", (selectOwnerId(me.id) ++ selectPublic).some)

  def minePublic(me: User, search: PagerSearch, order: Order, page: Int) = paginator(
    selectOwnerId(me.id) ++ selectPublic ++ tagSelector(search), me.some, order, page
  )

  def minePrivateTags(me: User): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", (selectOwnerId(me.id) ++ selectPrivate).some)

  def minePrivate(me: User, search: PagerSearch, order: Order, page: Int) = paginator(
    selectOwnerId(me.id) ++ selectPrivate ++ tagSelector(search), me.some, order, page
  )

  def mineAuthToTags(me: User): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", (selectOwnerId(me.id) ++ selectAuth).some)

  def mineAuthTo(me: User, search: PagerSearch, order: Order, page: Int) = paginator(
    selectOwnerId(me.id) ++ selectAuth ++ tagSelector(search), me.some, order, page
  )

  def mineMemberTags(me: User): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", (selectMemberId(me.id) ++ $doc("ownerId" $ne me.id)).some)

  def mineMember(me: User, search: PagerSearch, order: Order, page: Int) = paginator(
    selectMemberId(me.id) ++ $doc("ownerId" $ne me.id) ++ tagSelector(search), me.some, order, page
  )

  private def mineAuthSelector(me: User, mineCoaches: Set[String], mineTeamOwners: Set[String]) = {
    $or(
      $doc("ownerId" $in mineCoaches.filterNot(_ == me.id)) ++ selectStudent,
      $doc("ownerId" $in mineTeamOwners.filterNot(_ == me.id)) ++ selectTeam
    )
  }

  def mineAuthTags(me: User, mineCoaches: Set[String], mineTeamOwners: Set[String]): Fu[Set[String]] =
    studyRepo.coll.distinct[String, Set]("tags", (mineAuthSelector(me, mineCoaches, mineTeamOwners)).some)

  def mineAuth(
    me: User,
    mineCoaches: Set[String],
    mineTeamOwners: Set[String],
    search: PagerSearch,
    order: Order,
    page: Int
  ) = {
    paginator(mineAuthSelector(me, mineCoaches, mineTeamOwners) ++ tagSelector(search), me.some, order, page)
  }

  private def mineFavoritesSelector(me: User, mineCoaches: Set[String], mineTeamOwners: Set[String]) = {
    selectFavoriter(me.id) ++ accessSelect(me.some, mineCoaches, mineTeamOwners) ++ $doc("ownerId" $ne me.id)
  }

  private def accessSelect(me: Option[User], mineCoaches: Set[String], mineTeamOwners: Set[String]) =
    me.fold(selectPublic) { u =>
      $or(
        selectPublic,
        selectMemberId(u.id),
        $doc("ownerId" $in mineCoaches.filterNot(_ == u.id)) ++ selectStudent,
        $doc("ownerId" $in mineTeamOwners.filterNot(_ == u.id)) ++ selectTeam
      )
    }

  def mineFavoritesTags(me: User): Fu[Set[String]] = favoritesRepo.mineTags(me)

  def mineFavorites(
    me: User,
    mineCoaches: Set[String],
    mineTeamOwners: Set[String],
    search: PagerSearch,
    order: Order,
    page: Int
  ) =
    favoritesRepo.studyIdsByTags(me, search) flatMap { ids =>
      paginator(mineFavoritesSelector(me, mineCoaches, mineTeamOwners) ++ $inIds(ids), me.some, order, page)
    }

  private def paginator(
    selector: Bdoc,
    me: Option[User],
    order: Order,
    page: Int,
    nbResults: Option[Fu[Int]] = none
  ): Fu[Paginator[Study.WithChaptersAndLiked]] = {
    val adapter = new Adapter[Study](
      collection = studyRepo.coll,
      selector = selector,
      projection = studyRepo.projection,
      sort = order match {
        case Order.Hot => $sort desc "rank"
        case Order.Newest => $sort desc "createdAt"
        case Order.Oldest => $sort asc "createdAt"
        case Order.Updated => $sort desc "updatedAt"
        case Order.Popular => $sort desc "likes"
      }
    ) mapFutureList withChaptersAndLiking(me, defaultNbChaptersPerStudy)
    Paginator(
      adapter = nbResults.fold(adapter) { nb =>
        new CachedAdapter(adapter, nb)
      },
      currentPage = page,
      maxPerPage = maxPerPage
    )
  }

  def withChapters(studies: Seq[Study], nbChaptersPerStudy: Int): Fu[Seq[Study.WithChapters]] =
    chapterRepo.idNamesByStudyIds(studies.map(_.id), nbChaptersPerStudy) map { chapters =>
      studies.map { study =>
        Study.WithChapters(study, ~(chapters get study.id map {
          _ map (_.name)
        }))
      }
    }

  def withLiking(me: Option[User])(studies: Seq[Study.WithChapters]): Fu[Seq[Study.WithChaptersAndLiked]] =
    fuccess {
      me.?? { u =>
        studies.map {
          case Study.WithChapters(study, chapters) =>
            Study.WithChaptersAndLiked(study, chapters, study.liked(u.id))
        }
      }
    }
  //    me.?? { u => studyRepo.filterLiked(u, studies.map(_.study.id)) } map { liked =>
  //      studies.map {
  //        case Study.WithChapters(study, chapters) =>
  //          Study.WithChaptersAndLiked(study, chapters, liked(study.id))
  //      }
  //    }

  def withChaptersAndLiking(me: Option[User], nbChaptersPerStudy: Int)(studies: Seq[Study]): Fu[Seq[Study.WithChaptersAndLiked]] =
    withChapters(studies, nbChaptersPerStudy) flatMap withLiking(me)
}

sealed abstract class Order(val key: String, val name: String)

object Order {
  case object Hot extends Order("hot", "热度")
  case object Newest extends Order("newest", "添加日期(最新)")
  case object Oldest extends Order("oldest", "添加日期(最老)")
  case object Updated extends Order("updated", "最近更新")
  case object Popular extends Order("popular", "最受欢迎")

  val default = Popular
  val all = List(Hot, Newest, Oldest, Updated, Popular)
  val allButOldest = all filter (Oldest !=)
  private val byKey: Map[String, Order] = all.map { o => o.key -> o }(scala.collection.breakOut)
  def apply(key: String): Order = byKey.getOrElse(key, default)
}
