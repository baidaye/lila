package lila.study

import lila.db.dsl._
import lila.user.User
import lila.study.DataForm.PagerSearch
import reactivemongo.bson.BSONDocument

final class StudyFavoritesRepo(private[study] val coll: Coll) {

  import BSONHandlers.StudyFavoritesIdBSONHandler
  import BSONHandlers.StudyFavoritesBSONHandler

  def tags(studyId: Study.Id, userId: User.ID): Fu[List[String]] =
    byId(studyId, userId).map {
      _.?? { favorite =>
        favorite.tags
      }
    }

  def mineTags(me: User): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("userId" -> me.id).some)

  private def tagSelector(search: PagerSearch) = {
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    (!$andConditions.empty).?? { $and($andConditions: _*) }
  }

  def studyIdsByTags(user: User, search: PagerSearch): Fu[List[String]] =
    coll.find($doc("userId" -> user.id) ++ tagSelector(search))
      .cursor[Bdoc]()
      .gather[List]()
      .map { _ flatMap { _.getAs[String]("studyId") } }

  def byId(studyId: Study.Id, userId: User.ID): Fu[Option[StudyFavorites]] =
    coll.byId(StudyFavorites.makeId(studyId, userId))

  def favorited(studyId: Study.Id, userId: User.ID): Fu[Boolean] =
    coll.exists($id(StudyFavorites.makeId(studyId, userId)))

  def favorite(studyId: Study.Id, userId: User.ID, v: Boolean): Funit =
    if (v) add(studyId, userId)
    else remove(studyId, userId)

  def add(studyId: Study.Id, userId: User.ID): Funit =
    coll.insert(StudyFavorites.make(studyId, userId)).void

  def remove(studyId: Study.Id, userId: User.ID): Funit =
    coll.remove($id(StudyFavorites.makeId(studyId, userId))).void

  def setTags(studyId: Study.Id, userId: User.ID, tags: List[String]): Funit = {
    val tagId = StudyFavorites.makeId(studyId, userId)
    coll.update(
      $id(tagId),
      $set("tags" -> tags)
    ).void
  }

}
