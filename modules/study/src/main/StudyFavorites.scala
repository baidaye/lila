package lila.study

import lila.user.User
import org.joda.time.DateTime

case class StudyFavorites(
    _id: StudyFavorites.Id,
    studyId: Study.Id,
    userId: User.ID,
    tags: List[String],
    createdAt: DateTime
) {

  def id = _id

}

object StudyFavorites {

  def makeId(studyId: Study.Id, userId: User.ID): StudyFavorites.Id = Id(studyId.value + "@" + userId)

  def make(studyId: Study.Id, userId: User.ID, tags: List[String] = Nil) =
    StudyFavorites(
      _id = StudyFavorites.makeId(studyId, userId),
      studyId = studyId,
      userId = userId,
      tags = tags,
      createdAt = DateTime.now
    )

  case class Id(value: String) extends AnyVal with StringValue
  implicit val idIso = lila.common.Iso.string[Id](Id.apply, _.value)

}

