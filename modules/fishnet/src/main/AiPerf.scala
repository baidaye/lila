package lila.fishnet

final class AiPerfApi {

  def intRatings: Map[Int, Int] = Map(
    1 -> 600,
    2 -> 800,
    3 -> 1000,
    4 -> 1200,
    5 -> 1400,
    6 -> 1600,
    7 -> 1800,
    8 -> 2000,
    9 -> 2500,
    10 -> 3000
  )

  def puzzleAiLevel(rapidRating: Int) =
    intRatings.toList.sortBy(_._1).find(_._2 >= rapidRating).map(_._1) | 5
}

object AiPerfApi {

  def ratings = new AiPerfApi().intRatings

}
