package lila.setup

import chess.format.FEN
import chess.variant.Variant
import lila.common.Form.{ futureDateTime, numberIn }
import lila.lobby.Color
import lila.user.UserContext
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._

private[setup] final class FormFactory(
    anonConfigRepo: AnonConfigRepo,
    userConfigRepo: UserConfigRepo
) {

  import Mappings._

  def filterFilled(implicit ctx: UserContext): Fu[(Form[FilterConfig], FilterConfig)] =
    filterConfig map { f => filter(ctx).fill(f) -> f }

  def filter(ctx: UserContext) = Form(
    mapping(
      "variant" -> list(variantWithVariants),
      "mode" -> list(rawMode(withRated = true)),
      "speed" -> list(speed),
      "ratingRange" -> ratingRange
    )(FilterConfig.<<)(_.>>)
  )

  def filterConfig(implicit ctx: UserContext): Fu[FilterConfig] = savedConfig map (_.filter)

  def aiFilled(fen: Option[FEN], pgn: Option[String], limit: Option[Int] = None, increment: Option[Int] = None, level: Option[Int] = None,
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean])(implicit ctx: UserContext): Fu[Form[AiConfig]] =
    aiConfig map { config =>
      var newConfig =
        fen match {
          case Some(f) => {
            config.copy(
              fen = f.some,
              variant = chess.variant.FromPosition
            )
          }
          case None => config
        }

      newConfig = pgn match {
        case Some(p) => {
          newConfig.copy(
            fen = None,
            pgn = p.some,
            openingdbId = openingdbId,
            openingdbExcludeWhiteBlunder = openingdbExcludeWhiteBlunder,
            openingdbExcludeBlackBlunder = openingdbExcludeBlackBlunder,
            openingdbExcludeWhiteJscx = openingdbExcludeWhiteJscx,
            openingdbExcludeBlackJscx = openingdbExcludeBlackJscx,
            openingdbCanOff = openingdbCanOff,
            variant = chess.variant.FromPgn
          )
        }
        case None => newConfig
      }

      newConfig = newConfig.copy(
        time = limit.map(_ / 60d) | newConfig.time,
        increment = increment | newConfig.increment,
        level = level | newConfig.level
      )

      ai(ctx) fill newConfig
    }

  def ai(ctx: UserContext) = Form(
    mapping(
      "variant" -> aiVariants,
      "timeMode" -> timeMode,
      "time" -> time,
      "increment" -> increment,
      "days" -> days,
      "level" -> level,
      "color" -> color,
      "fen" -> fen,
      "pgn" -> pgn,
      "openingdbId" -> openingdbId,
      "openingdbExcludeWhiteBlunder" -> optional(boolean),
      "openingdbExcludeBlackBlunder" -> optional(boolean),
      "openingdbExcludeWhiteJscx" -> optional(boolean),
      "openingdbExcludeBlackJscx" -> optional(boolean),
      "openingdbCanOff" -> optional(boolean)
    )(AiConfig.<<)(_.>>)
      .verifying("invalidFen", _.validFen)
      .verifying("invalidPgn", _.validPgn)
      .verifying("invalidOpening", _.validOpeningDB)
      .verifying("Can't play that time control from a position", _.timeControlFromPosition)
  )

  def aiConfig(implicit ctx: UserContext): Fu[AiConfig] = fuccess(AiConfig.default) // savedConfig map (_.ai)

  //  def aiPuzzle(puzzleId: String, color: String, fen: String)(implicit ctx: UserContext) =
  //    ai(ctx) fill AiConfig.puzzleAi(puzzleId, color, fen)

  def friendFilled(fen: Option[FEN], pgn: Option[String], limit: Option[Int], increment: Option[Int], color: Option[String], rated: Option[Boolean])(implicit ctx: UserContext): Fu[Form[FriendConfig]] =
    friendConfig map { config =>

      var newConfig = config.copy(
        time = limit.map(_ / 60d) | config.time,
        increment = increment | config.increment,
        mode = rated.map { r => if (r) chess.Mode.Rated else chess.Mode.Casual }.getOrElse(config.mode),
        color = color.map { c => Color(c).getOrElse(Color.White) }.getOrElse(config.color)
      )

      newConfig = fen match {
        case Some(f) => {
          config.copy(
            fen = f.some,
            variant = chess.variant.FromPosition
          )
        }
        case None => newConfig
      }

      newConfig = pgn match {
        case Some(p) => {
          newConfig.copy(
            fen = None,
            pgn = p.some,
            variant = chess.variant.FromPgn
          )
        }
        case None => newConfig
      }

      friend(ctx) fill newConfig
    }

  def friend(ctx: UserContext) = Form(
    mapping(
      "variant" -> variantWithFenAndVariants,
      "timeMode" -> timeMode,
      "time" -> time,
      "increment" -> increment,
      "days" -> days,
      "mode" -> mode(withRated = ctx.isAuth),
      "color" -> color,
      "fen" -> fen,
      "pgn" -> pgn,
      "appt" -> numberIn(booleanChoices),
      "apptStartsAt" -> optional(futureDateTime.verifying("开始时间在两周内", (DateTime.now plusWeeks 2).isAfter(_))),
      "apptMessage" -> optional(nonEmptyText(minLength = 2, maxLength = 100))
    )(FriendConfig.<<)(_.>>)
      .verifying("Invalid clock", _.validClock)
      .verifying("invalidFen", _.validFen)
  )

  val booleanChoices = Seq((0 -> "否"), (1 -> "是"))

  def friendConfig(implicit ctx: UserContext): Fu[FriendConfig] = fuccess(FriendConfig.default) //savedConfig map (_.friend)

  def hookFilled(timeModeString: Option[String])(implicit ctx: UserContext): Fu[Form[HookConfig]] =
    hookConfig map (_ withTimeModeString timeModeString) map hook(ctx).fill

  def hook(ctx: UserContext) = Form(
    mapping(
      "variant" -> variantWithVariants,
      "timeMode" -> timeMode,
      "time" -> time,
      "increment" -> increment,
      "days" -> days,
      "mode" -> mode(ctx.isAuth),
      "ratingRange" -> optional(ratingRange),
      "color" -> color
    )(HookConfig.<<)(_.>>)
      .verifying("Invalid clock", _.validClock)
      .verifying("Can't create rated unlimited in lobby", _.noRatedUnlimited)
  )

  def hookConfig(implicit ctx: UserContext): Fu[HookConfig] = savedConfig map (_.hook)

  lazy val api = Form(
    mapping(
      "variant" -> optional(text.verifying(Variant.byKey.contains _)),
      "clock" -> optional(mapping(
        "limit" -> number.verifying(ApiConfig.clockLimitSeconds.contains _),
        "increment" -> increment
      )(chess.Clock.Config.apply)(chess.Clock.Config.unapply)),
      "days" -> optional(days),
      "rated" -> boolean,
      "color" -> optional(color),
      "fen" -> fen
    )(ApiConfig.<<)(_.>>).verifying("invalidFen", _.validFen)
  )

  def savedConfig(implicit ctx: UserContext): Fu[UserConfig] =
    ctx.me.fold(anonConfigRepo config ctx.req)(userConfigRepo.config)
}
