package lila.setup

import chess.format.FEN
import chess.variant.FromOpening
import lila.fishnet.AiPerfApi
import lila.game.{ Game, PgnSetup, Player, Pov, Source }
import lila.lobby.Color
import lila.opening.OpeningDBHelper
import lila.user.User

case class AiConfig(
    variant: chess.variant.Variant,
    timeMode: TimeMode,
    time: Double,
    increment: Int,
    days: Int,
    level: Int,
    color: Color,
    fen: Option[FEN] = None,
    pgn: Option[String] = None,
    openingdbId: Option[String] = None,
    openingdbExcludeWhiteBlunder: Option[Boolean] = None,
    openingdbExcludeBlackBlunder: Option[Boolean] = None,
    openingdbExcludeWhiteJscx: Option[Boolean] = None,
    openingdbExcludeBlackJscx: Option[Boolean] = None,
    openingdbCanOff: Option[Boolean] = None,
    puzzleId: Option[String] = None
) extends Config with Positional {

  val strictFen = true

  def >> = (variant.id, timeMode.id, time, increment, days, level, color.name, fen.map(_.value), pgn,
    openingdbId, openingdbExcludeWhiteBlunder, openingdbExcludeBlackBlunder, openingdbExcludeWhiteJscx, openingdbExcludeBlackJscx, openingdbCanOff).some

  def game(user: Option[User], canTakeback: Option[Boolean]) = {
    val initialPgn =
      if (variant.fromPgn) {
        pgn map { p =>
          ValidPgn.toPgnSetup(p) err s"can not parse pgn $p"
        }
      } else if (variant.fromOpening) {
        openingdbId.map { oid =>
          val openingdb = OpeningDBHelper.openingFromCache(oid)
          new PgnSetup(
            openingdb.startedAtTurnNext,
            openingdb.initialFen,
            Right(oid),
            openingdbExcludeWhiteBlunder.orElse(Some(false)),
            openingdbExcludeBlackBlunder.orElse(Some(false)),
            openingdbExcludeWhiteJscx.orElse(Some(false)),
            openingdbExcludeBlackJscx.orElse(Some(false)),
            openingdbCanOff.orElse(Some(true))
          )
        }
      } else None

    fenGame(initialPgn) { chessGame =>
      val perfPicker = lila.game.PerfPicker.mainOrDefault(
        chess.Speed(chessGame.clock.map(_.config)),
        chessGame.situation.board.variant,
        makeDaysPerTurn
      )

      Game.make(
        chess = chessGame,
        whitePlayer = creatorColor.fold(
          Player.make(chess.White, user, perfPicker),
          Player.make(chess.White, level.some)
        ),
        blackPlayer = creatorColor.fold(
          Player.make(chess.Black, level.some),
          Player.make(chess.Black, user, perfPicker)
        ),
        mode = chess.Mode.Casual,
        source = if (chessGame.board.variant.fromPosition) Source.Position else Source.Ai,
        initialPgn = initialPgn,
        daysPerTurn = makeDaysPerTurn,
        canTakeback = canTakeback,
        pgnImport = None
      ).sloppy
    } |> { g => puzzleId.fold(g) { puzzle => g.withPuzzleId(puzzle) } } start
  }

  def pov(user: Option[User], canTakeback: Option[Boolean]) = Pov(game(user, canTakeback), creatorColor)

  def timeControlFromPosition = variant != chess.variant.FromPosition || time >= 1

  def validOpeningDB = variant != FromOpening || openingdbId.isDefined
}

object AiConfig extends BaseConfig {

  def <<(v: Int, tm: Int, t: Double, i: Int, d: Int, level: Int, c: String,
    fen: Option[String], pgn: Option[String],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean]) = new AiConfig(
    variant = chess.variant.Variant(v) err "Invalid game variant " + v,
    timeMode = TimeMode(tm) err s"Invalid time mode $tm",
    time = t,
    increment = i,
    days = d,
    level = level,
    color = Color(c) err "Invalid color " + c,
    fen = fen map FEN,
    pgn = pgn,
    openingdbId = openingdbId,
    openingdbExcludeWhiteBlunder = openingdbExcludeWhiteBlunder,
    openingdbExcludeBlackBlunder = openingdbExcludeBlackBlunder,
    openingdbExcludeWhiteJscx = openingdbExcludeWhiteJscx,
    openingdbExcludeBlackJscx = openingdbExcludeBlackJscx,
    openingdbCanOff = openingdbCanOff
  )

  val default = AiConfig(
    variant = variantDefault,
    timeMode = TimeMode.RealTime,
    time = 10d,
    increment = 0,
    days = 2,
    level = 1,
    color = Color.default
  )

  def puzzleAi(puzzleId: String, color: String, fen: String, me: User) = {
    val rapidRating = me.perfs.rapid.intRating
    val level = new AiPerfApi().puzzleAiLevel(rapidRating)
    new AiConfig(
      variant = chess.variant.FromPosition,
      timeMode = TimeMode.RealTime,
      time = 60d,
      increment = 0,
      days = 2,
      level = level,
      color = Color(color) err "Invalid color " + color,
      fen = FEN(fen).some,
      puzzleId = puzzleId.some
    )
  }

  def customAi(limit: Int, increment: Int, aiLevel: Int, fen: Option[String], pgn: Option[String], color: Option[String],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean]) = {
    new AiConfig(
      variant = if (fen.isDefined) chess.variant.FromPosition else if (pgn.isDefined) chess.variant.FromPgn else if (openingdbId.isDefined) chess.variant.FromOpening else chess.variant.Standard,
      timeMode = TimeMode.RealTime,
      time = limit / 60d,
      increment = increment,
      days = 2,
      level = aiLevel,
      color = color.map(c => Color(c) err "Invalid color " + c) | (Color(randomColor.name) | Color.White),
      fen = fen.map(FEN),
      pgn = pgn,
      openingdbId = openingdbId,
      openingdbExcludeWhiteBlunder = openingdbExcludeWhiteBlunder,
      openingdbExcludeBlackBlunder = openingdbExcludeBlackBlunder,
      openingdbExcludeWhiteJscx = openingdbExcludeWhiteJscx,
      openingdbExcludeBlackJscx = openingdbExcludeBlackJscx,
      openingdbCanOff = openingdbCanOff
    )
  }

  def randomColor = List(chess.White, chess.Black)(scala.util.Random.nextInt(2))

  val levels = (1 to 10).toList

  val levelChoices = levels map { l => (l.toString, l.toString, none) }

  import lila.db.BSON
  import lila.db.dsl._
  import lila.game.BSONHandlers.FENBSONHandler

  private[setup] implicit val aiConfigBSONHandler = new BSON[AiConfig] {

    override val logMalformed = false

    def reads(r: BSON.Reader): AiConfig = AiConfig(
      variant = chess.variant.Variant orDefault (r int "v"),
      timeMode = TimeMode orDefault (r int "tm"),
      time = r double "t",
      increment = r int "i",
      days = r int "d",
      level = r int "l",
      color = Color.White,
      fen = r.getO[FEN]("f") filter (_.value.nonEmpty),
      pgn = r strO "p",
      openingdbId = r strO "oid",
      openingdbExcludeWhiteBlunder = r boolO "oewb",
      openingdbExcludeBlackBlunder = r boolO "oebb",
      openingdbExcludeWhiteJscx = r boolO "oewj",
      openingdbExcludeBlackJscx = r boolO "oebj",
      openingdbCanOff = r boolO "oof",
      puzzleId = r strO "pid"
    )

    def writes(w: BSON.Writer, o: AiConfig) = $doc(
      "v" -> o.variant.id,
      "tm" -> o.timeMode.id,
      "t" -> o.time,
      "i" -> o.increment,
      "d" -> o.days,
      "l" -> o.level,
      "f" -> o.fen,
      "p" -> o.pgn,
      "oid" -> o.openingdbId,
      "oewb" -> o.openingdbExcludeWhiteBlunder,
      "oebb" -> o.openingdbExcludeBlackBlunder,
      "oewj" -> o.openingdbExcludeWhiteJscx,
      "oebj" -> o.openingdbExcludeBlackJscx,
      "oof" -> o.openingdbCanOff,
      "pid" -> o.puzzleId
    )
  }
}
