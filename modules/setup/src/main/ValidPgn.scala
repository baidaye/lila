package lila.setup

import lila.opening.PgnParser
import lila.game.PgnSetup
import scalaz.{ Failure, Success }

case class ValidPgn(pgn: String, success: Boolean, message: Option[String]) {

  def orNone = if (success) Some(this) else None

}

object ValidPgn {

  def of(pgn: String, mustPlayable: Boolean = true): ValidPgn =
    PgnParser.preprocess(pgn) match {
      case Success((replay, _)) => {
        if (!mustPlayable || (mustPlayable && (replay.setup.situation.playable(true) && replay.state.situation.playable(true))))
          ValidPgn(pgn, true, None)
        else ValidPgn(pgn, false, "棋局已结束".some)
      }
      case Failure(e) => ValidPgn(pgn, false, s"${e.toString()}".some)
    }

  def toPgnSetup(pgn: String, mustPlayable: Boolean = true) = {
    PgnParser.preprocess(pgn) match {
      case Success((replay, initialFen)) => {
        if (!mustPlayable || (mustPlayable && (replay.setup.situation.playable(true) && replay.state.situation.playable(true))))
          new PgnSetup(replay.state.turns, initialFen, Left(pgn)).some
        else None
      }
      case Failure(_) => None
    }
  }

}

