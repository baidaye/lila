package lila.relation

import lila.db.dsl._
import lila.memo.Syncache
import lila.user.User
import reactivemongo.bson._
import scala.concurrent.duration._

final class MarkedApi(
    coll: Coll,
    bus: lila.common.Bus,
    asyncCache: lila.memo.AsyncCache.Builder
)(implicit system: akka.actor.ActorSystem) {

  implicit val MarkedHandler = Macros.handler[Marked]

  def getMarks(userId: User.ID): Fu[MarkedMap] =
    cache.async(userId)

  def getMark(userId: User.ID, relUserId: User.ID): Fu[Option[String]] =
    cache.async(userId).map { map =>
      map.get(relUserId) match {
        case None => None
        case Some(x) => x
      }
    }

  def save(userId: User.ID, relUserId: User.ID, mark: Option[String]): Funit = {
    val d = Marked.make(userId, relUserId, mark)
    coll.update($id(d.id), d, upsert = true).void >>- cache.invalidate(userId)
  }

  private val cache = new Syncache[User.ID, MarkedMap](
    name = "relation.marks",
    compute = id => loadMarks(id),
    default = _ => Map[String, Option[String]](),
    strategy = Syncache.WaitAfterUptime(10 millis),
    expireAfter = Syncache.ExpireAfterAccess(30 minutes),
    logger = logger branch "MarkedApi"
  )

  def loadMarks(userId: User.ID): Fu[MarkedMap] =
    coll.find($doc("userId" -> userId)).list[Marked]().map { list =>
      list.map { m =>
        m.relUserId -> m.mark
      }.toMap
    }

}
