package lila

package object relation extends PackageObject {

  private[relation] def logger = lila.log("relation")

  type Relation = Boolean
  val Follow: Relation = true
  val Block: Relation = false

  private[relation] type ID = String

  private[relation] type MarkedMap = Map[String, Option[String]]

  private[relation] type OnlineStudyingCache = com.github.blemale.scaffeine.Cache[ID, String]
}
