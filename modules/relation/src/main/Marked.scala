package lila.relation

import lila.user.User
import org.joda.time.DateTime

case class Marked(
    _id: User.ID,
    userId: User.ID,
    relUserId: User.ID,
    mark: Option[String],
    updateAt: DateTime
) {

  def id = _id

}

object Marked {

  def make(userId: User.ID, relUserId: User.ID, mark: Option[String]) =
    Marked(makeId(userId, relUserId), userId, relUserId, mark, DateTime.now)

  def makeId(u1: String, u2: String) = s"$u1/$u2"

}
