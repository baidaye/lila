package lila.train

import lila.user.User
import lila.common.LightUser
import play.api.libs.json._

final class JsonView(
    isOnline: lila.user.User.ID => Boolean,
    isPlaying: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi
) {

  def trainCourse(trainCourse: TrainCourse) =
    Json.obj(
      "id" -> trainCourse.id,
      "name" -> trainCourse.name,
      "studentIds" -> trainCourse.studentIds,
      "max" -> trainCourse.max,
      "date" -> trainCourse.date.toString("yyyy-MM-dd"),
      "startAt" -> trainCourse.startAt.toString("yyyy-MM-dd HH:mm"),
      "stopAt" -> trainCourse.stopAt.toString("yyyy-MM-dd HH:mm"),
      "taskDeadlineAt" -> trainCourse.stopAt.plusDays(1).toString("yyyy-MM-dd HH:mm"),
      "timeBegin" -> trainCourse.timeBegin,
      "timeEnd" -> trainCourse.timeEnd,
      "status" -> Json.obj(
        "id" -> trainCourse.status.id,
        "name" -> trainCourse.status.name
      )
    )

  def unAddStudentJson(users: List[User], markMap: Map[String, Option[String]]) =
    JsArray(
      users.map { user =>
        renderLightUser(user.light, markMap)
      }
    )

  def students(signs: List[TrainCourseStudent], markMap: Map[String, Option[String]]) =
    lightUserApi.asyncMany(signs.map(_.studentId)) map { lightUsers =>
      JsArray(
        signs.zip(lightUsers).map {
          case (sign, userOption) => {
            renderLightUser(userOption | LightUser.fallback(sign.studentId), markMap) ++ Json.obj(
              "status" -> Json.obj(
                "id" -> sign.status.id,
                "name" -> sign.status.name
              )
            )
          }
        }
      )
    }

  def players(coach: lila.user.User.ID, signs: List[TrainCourseStudent], markMap: Map[String, Option[String]]) = {
    val ids = signs.map(_.studentId).filter(isOnline(_)) :+ coach
    lightUserApi.asyncMany(ids) map { lightUsers =>
      JsArray(
        ids.zip(lightUsers).map {
          case (id, userOption) => {
            val lightUser = userOption | LightUser.fallback(id)
            val markOption = markMap.get(id) match {
              case None => none[String]
              case Some(m) => m
            }

            LightUser
              .lightUserWrites.writes(lightUser)
              .add("mark" -> markOption)
              .add("isPlaying" -> isPlaying(lightUser.id).some)
          }
        }
      )
    }
  }

  private def renderLightUser(lightUser: LightUser, markMap: Map[String, Option[String]]): JsObject = {
    val markOption = markMap.get(lightUser.id) match {
      case None => none[String]
      case Some(m) => m
    }

    LightUser
      .lightUserWrites.writes(lightUser)
      .add("online" -> isOnline(lightUser.id))
      .add("isPlaying" -> isPlaying(lightUser.id))
      .add("mark" -> markOption)
  }

}

