package lila.train

import lila.user.User
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ornicar.scalalib.Random

case class TrainCourse(
    _id: TrainCourse.ID,
    name: String,
    studentIds: List[User.ID],
    max: Int,
    startAt: DateTime,
    stopAt: DateTime,
    duration: Int,
    status: TrainCourse.Status,
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def date = startAt.withTimeAtStartOfDay()

  def timeBegin = startAt.toString("HH:mm")

  def timeEnd = stopAt.toString("HH:mm")

  def period = {
    if (timeBegin < "12:00") "上午"
    else if (timeBegin >= "12:00" && timeBegin < "18:00") "下午"
    else if (timeBegin >= "18:00") "晚上"
    else "-"
  }

  def canUpdate = !expired

  def expired = startAt.isBeforeNow

  def isCreator(user: User) = user.id == createdBy

  def belongTo(user: User) = studentIds.contains(user.id)

}

object TrainCourse {

  type ID = String

  val datePattern = "yyyy-MM-dd"
  val dateTimeFormatter = DateTimeFormat forPattern s"$datePattern HH:mm"

  def make(
    name: String,
    studentIds: List[User.ID],
    max: Int,
    startAt: DateTime,
    duration: Int,
    userId: User.ID
  ) = {
    val now = DateTime.now
    TrainCourse(
      _id = Random nextString 8,
      name = name,
      studentIds = studentIds,
      max = max,
      startAt = startAt,
      stopAt = startAt.plusMinutes(duration),
      duration = duration,
      status = Status.Created,
      createdAt = now,
      updatedAt = now,
      createdBy = userId,
      updatedBy = userId
    )
  }

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "未开始")
    case object Started extends Status("started", "上课中")
    case object Stopped extends Status("stopped", "已下课")

    def all = List(Created, Started, Stopped)

    def apply(id: String): Status = all.find(_.id == id) getOrElse Created
  }

  def updateOf(
    old: TrainCourse,
    name: String,
    studentIds: List[User.ID],
    max: Int,
    startAt: DateTime,
    duration: Int,
    userId: User.ID
  ) = old.copy(
    name = name,
    studentIds = studentIds,
    max = max,
    startAt = startAt,
    stopAt = startAt.plusMinutes(duration),
    duration = duration,
    updatedAt = DateTime.now,
    updatedBy = userId
  )

}
