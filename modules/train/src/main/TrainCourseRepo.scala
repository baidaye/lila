package lila.train

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson.Macros

object TrainCourseRepo {

  import BSONHandlers.TrainCourseBSONHandler

  private lazy val coll = Env.current.TrainCourseColl

  def byId(id: TrainCourse.ID): Fu[Option[TrainCourse]] =
    coll.byId[TrainCourse](id)

  def weekCourse(firstDay: DateTime, lastDay: DateTime, userId: User.ID): Fu[List[TrainCourse]] = {
    coll.find($doc(
      "createdBy" -> userId,
      "startAt" -> ($gte(firstDay.withTimeAtStartOfDay()) ++ $lte(lastDay.withTime(23, 59, 59, 999)))
    )).sort($sort asc "startAt").list[TrainCourse]()
  }

  def canStopList: Fu[List[TrainCourse]] =
    coll.find($doc("status" $in List(TrainCourse.Status.Created.id, TrainCourse.Status.Started.id), "stopAt" $lte DateTime.now())).list[TrainCourse]()

  def insert(trainCourse: TrainCourse): Funit =
    coll.insert(trainCourse).void

  def update(trainCourse: TrainCourse): Funit =
    coll.update($id(trainCourse.id), trainCourse).void

  def remove(id: TrainCourse.ID): Funit =
    coll.remove($id(id)).void

  def setStudents(id: TrainCourse.ID, studentIds: List[User.ID]): Funit =
    coll.update(
      $id(id),
      $set("studentIds" -> studentIds)
    ).void

  def setStatus(id: TrainCourse.ID, status: TrainCourse.Status): Funit =
    coll.update(
      $id(id),
      $set(
        "status" -> status.id,
        "updateAt" -> DateTime.now
      )
    ).void

}
