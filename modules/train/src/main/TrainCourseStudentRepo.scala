package lila.train

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson.{ BSONHandler, BSONString, Macros }

object TrainCourseStudentRepo {

  import lila.db.dsl.BSONJodaDateTimeHandler
  implicit val StatusBSONHandler = new BSONHandler[BSONString, TrainCourseStudent.Status] {
    def read(b: BSONString): TrainCourseStudent.Status = TrainCourseStudent.Status(b.value)
    def write(c: TrainCourseStudent.Status) = BSONString(c.id)
  }
  implicit val TrainCourseSignBSONHandler = Macros.handler[TrainCourseStudent]

  private lazy val coll = Env.current.TrainCourseSignColl

  def byId(id: TrainCourseStudent.ID): Fu[Option[TrainCourseStudent]] =
    coll.byId[TrainCourseStudent](id)

  def signed(tid: TrainCourse.ID, userId: User.ID): Fu[Boolean] =
    coll.byId(TrainCourseStudent.makeId(tid, userId)).map {
      _.??(_.signed)
    }

  def list(tid: TrainCourse.ID, statusList: List[TrainCourseStudent.Status] = Nil): Fu[List[TrainCourseStudent]] =
    coll.find(
      $doc(
        "tid" -> tid
      ) ++ statusList.nonEmpty.?? {
          $doc(
            "status" $in statusList.map(_.id)
          )
        }
    )
      .sort($doc("studentId" -> 1))
      .list[TrainCourseStudent]()
      .map { list =>
        list.sortBy(_.status.sort)
      }

  def setStatus(id: TrainCourseStudent.ID, status: TrainCourseStudent.Status): Funit =
    coll.update($id(id), $set("status" -> status.id, "updateAt" -> DateTime.now)).void

  def remove(tid: TrainCourseStudent.ID): Funit =
    coll.remove($doc("tid" -> tid)).void

  def bulkInsert(students: List[TrainCourseStudent]): Funit =
    coll.bulkInsert(
      documents = students.map(TrainCourseSignBSONHandler.write).toStream,
      ordered = true
    ).void

}
