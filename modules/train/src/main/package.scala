package lila

import lila.hub.TrouperMap
import lila.socket.WithSocket

package object train extends PackageObject with WithSocket {

  type SocketMap = TrouperMap[TrainCourseSocket]

  private[train] val logger = lila.log("train")

}
