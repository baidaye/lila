package lila.train

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.common.{ AtMost, Every, LightUser, ResilientScheduler }
import lila.hub.{ Duct, DuctMap }
import lila.notify.NotifyApi

import scala.concurrent.duration._

final class Env(
    config: Config,
    hub: lila.hub.Env,
    db: lila.db.Env,
    notifyApi: NotifyApi,
    lightUser: LightUser.Getter,
    lightUserSync: LightUser.GetterSync,
    asyncCache: lila.memo.AsyncCache.Builder,
    system: ActorSystem,
    scheduler: lila.common.Scheduler,
    isOnline: lila.user.User.ID => Boolean,
    isPlaying: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi
) {

  private val SocketTimeout = config duration "socket.timeout"
  private val SocketSriTimeout = config duration "socket.sri.timeout"
  private val CollectionTrainCourse = config getString "collection.train_course"
  private val CollectionTrainCourseSign = config getString "collection.train_course_sign"

  private[train] val TrainCourseColl = db(CollectionTrainCourse)
  private[train] val TrainCourseSignColl = db(CollectionTrainCourseSign)

  lazy val form = new DataForm()

  lazy val jsonView = new JsonView(isOnline, isPlaying, lightUserApi)

  lazy val api = new TrainCourseApi(
    jsonView = jsonView,
    socketMap = socketMap,
    bus = hub.bus,
    notifyApi = notifyApi,
    scheduler = scheduler,
    asyncCache = asyncCache,
    isOnline = isOnline,
    chatActor = hub.chat
  )(system)

  lazy val socketMap: SocketMap = lila.socket.SocketMap[TrainCourseSocket](
    system = system,
    mkTrouper = (id: String) => new TrainCourseSocket(
      id = id,
      system = system,
      sriTtl = SocketSriTimeout,
      keepMeAlive = () => socketMap touch id
    ),
    accessTimeout = SocketTimeout,
    monitoringName = "trainCourse.socketMap",
    broomFrequency = 3691 millis
  )

  lazy val socketHandler = new TrainCourseSocketHandler(
    api = api,
    socketMap = socketMap,
    hub = hub,
    chat = hub.chat
  )

  private val sequencerMap = new DuctMap(
    mkDuct = _ => Duct.extra.lazyPromise(5.seconds.some)(system),
    accessTimeout = 10.minute
  )

  ResilientScheduler(
    every = Every(1 minute),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 1 minute
  ) { api.scheduleStop() }(system)

}

object Env {

  lazy val current: Env = "train" boot new Env(
    config = lila.common.PlayApp loadConfig "train",
    hub = lila.hub.Env.current,
    db = lila.db.Env.current,
    notifyApi = lila.notify.Env.current.api,
    lightUser = lila.user.Env.current.lightUser,
    lightUserSync = lila.user.Env.current.lightUserSync,
    asyncCache = lila.memo.Env.current.asyncCache,
    system = lila.common.PlayApp.system,
    scheduler = lila.common.PlayApp.scheduler,
    isOnline = lila.user.Env.current.isOnline,
    isPlaying = lila.relation.Env.current.online.isPlaying,
    lightUserApi = lila.user.Env.current.lightUserApi
  )

}
