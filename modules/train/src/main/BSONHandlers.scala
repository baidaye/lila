package lila.train

import reactivemongo.bson.{ BSONHandler, BSONString, Macros }

private object BSONHandlers {

  import lila.db.dsl.BSONJodaDateTimeHandler

  private implicit val TTaskStatusBSONHandler = new BSONHandler[BSONString, TrainCourse.Status] {
    def read(b: BSONString) = TrainCourse.Status(b.value)
    def write(x: TrainCourse.Status) = BSONString(x.id)
  }

  implicit val TrainCourseBSONHandler = Macros.handler[TrainCourse]

}
