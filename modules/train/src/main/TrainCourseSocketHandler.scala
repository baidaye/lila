package lila.train

import akka.actor.ActorSelection
import lila.common.ApiVersion
import lila.socket._
import lila.user.User
import lila.chat.Chat
import lila.train.TrainCourseSocket._

final class TrainCourseSocketHandler(
    api: TrainCourseApi,
    socketMap: SocketMap,
    hub: lila.hub.Env,
    chat: ActorSelection
) {

  private def makeController(
    id: TrainCourse.ID,
    sri: Socket.Sri,
    socket: TrainCourseSocket,
    member: TrainCourseSocket.Member,
    user: Option[User]
  ): Handler.Controller = {
    case ("x", d) => Unit
  }

  def join(
    id: TrainCourse.ID,
    sri: Socket.Sri,
    user: Option[User],
    apiVersion: ApiVersion
  ): Fu[Option[JsSocketHandler]] = {
    val socket = socketMap.getOrMake(id)
    socket.ask[Connected](Join(sri, user.map(_.id), _)) map {
      case Connected(enum, member) => {
        Handler.iteratee(
          hub,
          makeController(id, sri, socket, member, user) orElse lila.chat.Socket.in(
            chatId = Chat.Id(id),
            member = member,
            chat = chat,
            publicSource = lila.hub.actorApi.shutup.PublicSource.TrainCourse(id).some
          ),
          member,
          socket,
          sri,
          apiVersion
        ) -> enum
      }
    } map (_.some)
  }

}
