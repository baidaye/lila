package lila.train

import play.api.data._
import play.api.data.Forms._
import lila.common.Form.{ ISODate, numberIn }
import org.joda.time.DateTime

case class DataForm() {

  val createOrUpdate = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "max" -> number(min = 1, max = 100),
    "startAt" -> nonEmptyText,
    "duration" -> numberIn(DataForm.duration),
    "studentIds" -> nonEmptyText
  )(CreateOrUpdateData.apply)(CreateOrUpdateData.unapply))

  def createDefault = createOrUpdate fill DataForm.default

  def updateOf(tc: TrainCourse) =
    createOrUpdate fill CreateOrUpdateData(
      name = tc.name,
      max = tc.max,
      startAt = tc.startAt.toString("yyyy-MM-dd HH:mm"),
      duration = tc.duration,
      studentIds = tc.studentIds.mkString(",")
    )

}

object DataForm {

  val duration = List(30 -> "30分钟", 60 -> "1小时", 60 * 2 -> "2小时", 60 * 3 -> "3小时", 60 * 4 -> "4小时")

  def default = {
    val startAt = DateTime.now().withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).plusHours(1).toString("yyyy-MM-dd HH:mm")
    CreateOrUpdateData(
      name = "新的训练课",
      max = 20,
      startAt = startAt,
      duration = 60,
      studentIds = ""
    )
  }

}

case class CreateOrUpdateData(name: String, max: Int, startAt: String, duration: Int, studentIds: String) {

  def stus = studentIds.split(",").toList

}
