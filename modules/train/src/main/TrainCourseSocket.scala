package lila.train

import lila.chat.Chat
import lila.hub.Trouper
import play.api.libs.iteratee._
import play.api.libs.json._
import scala.concurrent.duration._
import scala.concurrent.Promise
import lila.user.User
import lila.socket._

final class TrainCourseSocket(
    id: TrainCourse.ID,
    system: akka.actor.ActorSystem,
    sriTtl: FiniteDuration,
    keepMeAlive: () => Unit
) extends SocketTrouper[TrainCourseSocket.Member](system, sriTtl) with LoneSocket {

  def monitoringName = "trainCourse"
  def broomFrequency = 4027 millis

  import TrainCourseSocket._

  private def chatClassifier = Chat classify Chat.Id(id)

  lilaBus.subscribe(this, chatClassifier)

  override def stop(): Unit = {
    super.stop()
    lilaBus.unsubscribe(this, chatClassifier)
  }

  def receiveSpecific = ({

    case Join(sri, userId, promise) => {
      val (enumerator, channel) = Concurrent.broadcast[JsValue]
      val member = Member(channel, userId)
      addMember(sri, member)
      promise success Connected(enumerator, member)
    }

    case TrainCourseStart(trainCourse) => {
      notifyAll("start", trainCourse.id)
    }

    case TrainCourseStop(trainCourse) => {
      notifyAll("stop", trainCourse.id)
    }

    case StudentSignIn(trainCourse, userId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("signIn", trainCourse.id)(member)
      }
    }

    case StudentSignOut(trainCourse, userId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("signOut", trainCourse.id)(member)
      }
    }

    case StudentKick(trainCourse, userId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("kick", trainCourse.id)(member)
      }
    }

    case TaskCreate(trainCourse, _, userId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("taskCreate", trainCourse.id)(member)
      }
    }

    case TaskChangeStatus(trainCourse, _, userId, _) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("taskChangeStatus", trainCourse.id)(member)
      }
    }

    case TaskChangeProgress(trainCourse, _, userId, _) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("taskChangeProgress", trainCourse.id)(member)
      }
    }

    case GameTaskCreate(trainCourse, userId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("gameTaskCreate", trainCourse.id)(member)
      }
    }

    case GameTaskStart(_, userId, gameId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("gameTaskStart", gameId)(member)
      }
    }

    case GameTaskFinish(_, userId, gameId) => {
      firstMemberByUserId(userId) foreach { member =>
        notifyMember("gameTaskFinish", gameId)(member)
      }
    }

  }: Trouper.Receive) orElse lila.chat.Socket.out(
    send = (t, d, _) => notifyAll(t, d)
  )

  override protected def broom: Unit = {
    super.broom
    if (members.nonEmpty) keepMeAlive()
  }

  override protected def afterQuit(sri: Socket.Sri, member: Member): Unit = {
    //    member.userId.foreach { userId =>
    //      lilaBus.publish(StudentQuit(id, userId), 'studentQuit)
    //    }
  }

}

object TrainCourseSocket {

  case class Member(
      channel: JsChannel,
      userId: Option[User.ID]
  ) extends SocketMember

  case class Join(sri: Socket.Sri, userId: Option[String], promise: Promise[Connected])
  case class Connected(enumerator: JsEnumerator, member: TrainCourseSocket.Member)
  case class TrainCourseStart(trainCourse: TrainCourse)
  case class TrainCourseStop(trainCourse: TrainCourse)
  case class StudentSignIn(trainCourse: TrainCourse, userId: String)
  case class StudentSignOut(trainCourse: TrainCourse, userId: String)
  case class StudentKick(trainCourse: TrainCourse, userId: String)
  case class StudentQuit(trainCourseId: TrainCourse.ID, userId: String)
  case class TaskCreate(trainCourse: TrainCourse, taskId: String, userId: String)
  case class TaskChangeStatus(trainCourse: TrainCourse, taskId: String, userId: String, status: String)
  case class TaskChangeProgress(trainCourse: TrainCourse, taskId: String, userId: String, progress: Double)
  case class GameTaskCreate(trainCourse: TrainCourse, userId: String)
  case class GameTaskStart(trainCourse: TrainCourse, userId: String, gameId: String)
  case class GameTaskFinish(trainCourse: TrainCourse, userId: String, gameId: String)
}
