package lila.train

import lila.user.User
import org.joda.time.DateTime

case class TrainCourseStudent(
    _id: TrainCourseStudent.ID,
    tid: TrainCourse.ID,
    studentId: User.ID,
    status: TrainCourseStudent.Status,
    updateAt: DateTime
) {

  def id = _id

  def signed = status != TrainCourseStudent.Status.Quit && status != TrainCourseStudent.Status.Create

  def isKicked = status == TrainCourseStudent.Status.Kick

}

object TrainCourseStudent {

  type ID = String

  sealed abstract class Status(val id: String, val name: String, val sort: Int)
  object Status {
    case object Create extends Status("create", "未签到", 4)
    case object Free extends Status("free", "空闲中", 1)
    case object Train extends Status("train", "训练中", 2)
    case object Quit extends Status("quit", "已退出", 3)
    case object Kick extends Status("kick", "已移除", 5)

    def all = List(Create, Free, Train, Quit, Kick)

    def apply(id: String): Status = all.find(_.id == id) getOrElse Quit
  }

  def make(tid: TrainCourse.ID, studentId: User.ID) = {
    TrainCourseStudent(
      _id = makeId(tid, studentId),
      tid = tid,
      studentId = studentId,
      status = Status.Create,
      updateAt = DateTime.now
    )
  }

  def makeId(tid: TrainCourse.ID, studentId: User.ID) = s"$tid@$studentId"

}
