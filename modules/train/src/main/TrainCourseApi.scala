package lila.train

import lila.user.User
import akka.actor.{ ActorSelection, ActorSystem }
import lila.hub.actorApi.calendar.{ CalendarCreate, CalendarsCreate, CalendarsRemove }
import lila.hub.actorApi.task._
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import org.joda.time.DateTime
import scala.concurrent.duration._
import TrainCourseSocket._
import lila.chat.Chat

final class TrainCourseApi(
    jsonView: JsonView,
    socketMap: SocketMap,
    bus: lila.common.Bus,
    notifyApi: NotifyApi,
    scheduler: lila.common.Scheduler,
    asyncCache: lila.memo.AsyncCache.Builder,
    isOnline: lila.user.User.ID => Boolean,
    chatActor: ActorSelection
)(implicit system: ActorSystem) {

  bus.subscribeFun('ttask) {
    case ct: CreateTask => (ct.sourceRel.source == "trainCourse").?? {
      byId(ct.sourceRel.id) foreach {
        _.?? { trainCourse =>
          sendTaskCreate(trainCourse, ct)
        }
      }
    }
    case cs: ChangeStatus => (cs.sourceRel.source == "trainCourse").?? {
      byId(cs.sourceRel.id) foreach {
        _.?? { trainCourse =>
          changeStatus(trainCourse, cs)
        }
      }
    }
    case cp: ChangeProgress => (cp.sourceRel.source == "trainCourse").?? {
      byId(cp.sourceRel.id) foreach {
        _.?? { trainCourse =>
          sendChangeProgress(trainCourse, cp)
        }
      }
    }
    case ct: CreateTrainGameTask => (ct.sourceRel.source == "trainCourse").?? {
      byId(ct.sourceRel.id) foreach {
        _.?? { trainCourse =>
          sendGameTaskCreate(trainCourse, ct)
        }
      }
    }
    case ct: StartTrainGameTask => (ct.sourceRel.source == "trainCourse").?? {
      byId(ct.sourceRel.id) foreach {
        _.?? { trainCourse =>
          sendGameTaskStart(trainCourse, ct)
        }
      }
    }
    case ct: FinishTrainGameTask => (ct.sourceRel.source == "trainCourse").?? {
      byId(ct.sourceRel.id) foreach {
        _.?? { trainCourse =>
          sendGameTaskFinish(trainCourse, ct)
        }
      }
    }
  }

  bus.subscribeFun('studentQuit) {
    case StudentQuit(trainCourseId, userId) => {
      byId(trainCourseId) foreach {
        _.?? { trainCourse =>
          signOut(trainCourse, userId)
        }
      }
    }
  }

  def byId(id: TrainCourse.ID): Fu[Option[TrainCourse]] =
    TrainCourseRepo.byId(id)

  def weekCourse(firstDay: DateTime, lastDay: DateTime, userId: User.ID): Fu[List[TrainCourse]] = {
    TrainCourseRepo.weekCourse(firstDay, lastDay, userId)
  }

  def create(data: CreateOrUpdateData, userId: User.ID): Funit = {
    val d = TrainCourse.make(
      name = data.name,
      studentIds = data.stus,
      max = data.max,
      startAt = TrainCourse.dateTimeFormatter.parseDateTime(data.startAt),
      duration = data.duration,
      userId = userId
    )

    val students = d.studentIds.map {
      TrainCourseStudent.make(d.id, _)
    }
    TrainCourseRepo.insert(d) >>
      TrainCourseStudentRepo.bulkInsert(students) >>
      addNotify(d) >>-
      publishCalendar(d)
  }

  def addStudents(trainCourse: TrainCourse, studentIds: List[String]): Funit =
    studentIds.nonEmpty.?? {
      val students = studentIds.map {
        TrainCourseStudent.make(trainCourse.id, _)
      }
      TrainCourseRepo.setStudents(trainCourse.id, trainCourse.studentIds ++ studentIds) >>
        TrainCourseStudentRepo.bulkInsert(students) >>
        addNotify(trainCourse.copy(studentIds = studentIds)) >>-
        publishCalendar(trainCourse.copy(studentIds = studentIds))
    }

  def update(old: TrainCourse, data: CreateOrUpdateData, userId: User.ID): Funit = {
    val d = TrainCourse.updateOf(
      old = old,
      name = data.name,
      studentIds = data.stus,
      max = data.max,
      startAt = TrainCourse.dateTimeFormatter.parseDateTime(data.startAt),
      duration = data.duration,
      userId = userId
    )
    val students = d.studentIds.map {
      TrainCourseStudent.make(d.id, _)
    }
    TrainCourseRepo.update(d) >>
      TrainCourseStudentRepo.remove(d.id) >>
      TrainCourseStudentRepo.bulkInsert(students) >>
      addNotify(d) >>-
      resetCalendar(d)
  }

  def changeStatus(trainCourse: TrainCourse, cs: ChangeStatus): Funit = {
    {
      cs.status match {
        case "train" => TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, cs.userId), TrainCourseStudent.Status.Train)
        case "finished" => TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, cs.userId), TrainCourseStudent.Status.Free)
        case _ => funit
      }
    } >>- sendChangeStatus(trainCourse, cs)
  }

  def remove(trainCourse: TrainCourse): Funit = {
    TrainCourseRepo.remove(trainCourse.id) >>- removeCalendar(trainCourse)
  }

  def start(trainCourse: TrainCourse): Funit = {
    TrainCourseRepo.setStatus(trainCourse.id, TrainCourse.Status.Started) >>-
      sendStart(trainCourse)
  }

  def stop(trainCourse: TrainCourse): Funit = {
    TrainCourseRepo.setStatus(trainCourse.id, TrainCourse.Status.Stopped) >>-
      sendStop(trainCourse)
  }

  def scheduleStop(): Funit = {
    TrainCourseRepo.canStopList.flatMap { list =>
      list.map { trainCourse =>
        logger.info(s"停止训练课：${trainCourse.id}-${trainCourse.name}")
        stop(trainCourse)
      }.sequenceFu.void
    }
  }

  def signIn(trainCourse: TrainCourse, userId: User.ID): Funit = {
    TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, userId), TrainCourseStudent.Status.Free) >>-
      sendSignIn(trainCourse, userId)
  }

  def signOut(trainCourse: TrainCourse, userId: User.ID): Funit = {
    TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, userId), TrainCourseStudent.Status.Quit) >>-
      sendSignOut(trainCourse, userId)
  }

  def kick(trainCourse: TrainCourse, userId: User.ID): Funit = {
    TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, userId), TrainCourseStudent.Status.Kick) >>-
      sendKick(trainCourse, userId)
  }

  private def addNotify(trainCourse: TrainCourse): Funit = {
    trainCourse.studentIds.map { studentId =>
      notifyApi.addNotification(Notification.make(
        Sender(trainCourse.createdBy).some,
        Notifies(studentId),
        lila.notify.GenericLink(
          url = s"/trainCourse/${trainCourse.id}/student",
          title = "加入训练课".some,
          text = s"您已加入训练课【${trainCourse.name}】".some,
          icon = "训"
        )
      ))
    }.sequenceFu.void
  }

  private def resetCalendar(trainCourse: TrainCourse) = {
    removeCalendar(trainCourse)
    system.scheduler.scheduleOnce(3 seconds) {
      publishCalendar(trainCourse)
    }
  }

  private def removeCalendar(trainCourse: TrainCourse): Unit = {
    val ids = trainCourse.studentIds.map { studentId => s"${trainCourse.id}@$studentId" } :+ s"${trainCourse.id}@${trainCourse.createdBy}"
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
  }

  private def publishCalendar(trainCourse: TrainCourse): Unit = {
    val calendars = makeCalendar(trainCourse) :+ CalendarCreate(
      id = s"${trainCourse.id}@${trainCourse.createdBy}".some,
      typ = "trainCourse",
      user = trainCourse.createdBy,
      sdt = trainCourse.startAt,
      edt = trainCourse.stopAt,
      content = trainCourse.name,
      onlySdt = false,
      link = s"/trainCourse/${trainCourse.id}/coach".some,
      icon = "训".some,
      bg = "#507803".some
    )
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  private def makeCalendar(trainCourse: TrainCourse): List[CalendarCreate] = {
    trainCourse.studentIds.map { studentId =>
      CalendarCreate(
        id = s"${trainCourse.id}@$studentId".some,
        typ = "trainCourse",
        user = studentId,
        sdt = trainCourse.startAt,
        edt = trainCourse.stopAt,
        content = trainCourse.name,
        onlySdt = false,
        link = s"/trainCourse/${trainCourse.id}/student".some,
        icon = "训".some,
        bg = "#507803".some
      )
    }
  }

  private def sendStop(trainCourse: TrainCourse) = {
    socketMap.tell(trainCourse.id, TrainCourseStart(trainCourse))
  }

  private def sendStart(trainCourse: TrainCourse) = {
    socketMap.tell(trainCourse.id, TrainCourseStop(trainCourse))
  }

  private def sendSignIn(trainCourse: TrainCourse, userId: String) = {
    socketMap.tell(trainCourse.id, StudentSignIn(trainCourse, trainCourse.createdBy))
  }

  private def sendSignOut(trainCourse: TrainCourse, userId: String) = {
    socketMap.tell(trainCourse.id, StudentSignOut(trainCourse, trainCourse.createdBy))
  }

  private def sendKick(trainCourse: TrainCourse, userId: String) = {
    socketMap.tell(trainCourse.id, StudentKick(trainCourse, userId))
  }

  private def sendTaskCreate(trainCourse: TrainCourse, ct: CreateTask) = {
    socketMap.tell(trainCourse.id, TaskCreate(trainCourse, ct.taskId, ct.userId))
  }

  private def sendChangeStatus(trainCourse: TrainCourse, cs: ChangeStatus) = {
    if (cs.status == "finished") {
      chatActor ! lila.chat.actorApi.UserTalk(Chat.Id(trainCourse.id), cs.userId, s"${cs.taskName} - 已完成", publicSource = lila.hub.actorApi.shutup.PublicSource.TrainCourse(trainCourse.id).some)
    }
    socketMap.tell(trainCourse.id, TaskChangeStatus(trainCourse, cs.taskId, cs.userId, cs.status))
    socketMap.tell(trainCourse.id, TaskChangeStatus(trainCourse, cs.taskId, trainCourse.createdBy, cs.status))
  }

  private def sendChangeProgress(trainCourse: TrainCourse, cp: ChangeProgress) = {
    socketMap.tell(trainCourse.id, TaskChangeProgress(trainCourse, cp.taskId, cp.userId, cp.progress))
    socketMap.tell(trainCourse.id, TaskChangeProgress(trainCourse, cp.taskId, trainCourse.createdBy, cp.progress))
  }

  private def sendGameTaskCreate(trainCourse: TrainCourse, ct: CreateTrainGameTask) = {
    socketMap.tell(trainCourse.id, GameTaskCreate(trainCourse, ct.whiteUser))
    socketMap.tell(trainCourse.id, GameTaskCreate(trainCourse, ct.blackUser))
    socketMap.tell(trainCourse.id, GameTaskCreate(trainCourse, trainCourse.createdBy))
  }

  private def sendGameTaskStart(trainCourse: TrainCourse, ct: StartTrainGameTask) = {
    socketMap.tell(trainCourse.id, GameTaskStart(trainCourse, ct.whiteUser, ct.gameId))
    socketMap.tell(trainCourse.id, GameTaskStart(trainCourse, ct.blackUser, ct.gameId))
    socketMap.tell(trainCourse.id, GameTaskStart(trainCourse, trainCourse.createdBy, ct.gameId))
  }

  private def sendGameTaskFinish(trainCourse: TrainCourse, ct: FinishTrainGameTask) = {

    TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, ct.whiteUser), TrainCourseStudent.Status.Free) >>-
      socketMap.tell(trainCourse.id, GameTaskFinish(trainCourse, ct.whiteUser, ct.gameId)) >>-
      { chatActor ! lila.chat.actorApi.UserTalk(Chat.Id(trainCourse.id), ct.whiteUser, s"${ct.taskName} - 已完成", publicSource = lila.hub.actorApi.shutup.PublicSource.TrainCourse(trainCourse.id).some) }

    TrainCourseStudentRepo.setStatus(TrainCourseStudent.makeId(trainCourse.id, ct.blackUser), TrainCourseStudent.Status.Free) >>-
      socketMap.tell(trainCourse.id, GameTaskFinish(trainCourse, ct.blackUser, ct.gameId)) >>-
      { chatActor ! lila.chat.actorApi.UserTalk(Chat.Id(trainCourse.id), ct.blackUser, s"${ct.taskName} - 已完成", publicSource = lila.hub.actorApi.shutup.PublicSource.TrainCourse(trainCourse.id).some) }

    socketMap.tell(trainCourse.id, GameTaskFinish(trainCourse, trainCourse.createdBy, ct.gameId))
  }

}
