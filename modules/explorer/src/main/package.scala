package lila

package object explorer extends PackageObject {

  private[explorer] val logger = lila.log("explorer")

  val maxPlies = 50
}
