package lila.explorer

import lila.user.User
import lila.game.{ Game, GameRepo, Player }
import lila.importer.{ ImportData, Importer }
import org.joda.time.DateTime
import scala.concurrent.blocking
import scala.sys.process.ProcessLogger
import scala.sys.process._

final class ExplorerImporter(
    endpoint: String,
    gameImporter: Importer
) {

  private val masterGameEncodingFixedAt = new DateTime(2016, 3, 9, 0, 0)

  def apply(id: Game.ID, url: Option[String] = None): Fu[Option[Game]] =
    GameRepo game id flatMap {
      case Some(game) if !game.isPgnImport || game.createdAt.isAfter(masterGameEncodingFixedAt) => fuccess(game.some)
      case _ => (GameRepo remove id) >> fetchAndImport(id, url, "haichess".some)
    }

  def fetchAndImport(
    id: Game.ID,
    url: Option[String] = None,
    userId: Option[User.ID],
    white: Option[Player] = None,
    black: Option[Player] = None,
    lichessUnion: Option[Boolean] = None
  ): Fu[Option[Game]] = {
    fetchPgn(id, url) flatMap {
      case None => fuccess(none)
      case Some(pgn) => pgnImport(pgn, id, userId, white, black, lichessUnion)
    }
  }

  def pgnImport(
    pgn: String,
    id: String,
    userId: Option[User.ID],
    white: Option[Player] = None,
    black: Option[Player] = None,
    lichessUnion: Option[Boolean] = None
  ): Fu[Option[Game]] = {
    gameImporter(
      ImportData(pgn, none),
      user = userId,
      forceId = id.some,
      white = white,
      black = black,
      lichessUnion = lichessUnion
    ) map some
  }

  def fetchPgn(id: String, u: Option[String] = None): Fu[Option[String]] = {
    import play.api.libs.ws.WS
    import play.api.Play.current
    val url = u | s"$endpoint/master/pgn/$id"
    WS.url(url).get() map {
      case res if res.status == 200 => res.body.some
      case _ => None
    }
  }

}
