package lila.contest

import chess.Color
import reactivemongo.bson._
import lila.db.dsl._
import lila.game.Game
import org.joda.time.DateTime

object BoardRepo {

  private[contest] lazy val coll = Env.current.boardColl
  import BSONHandlers._

  def insertMany(roundId: Round.ID, boardList: List[Board]): Funit =
    coll.remove(roundQuery(roundId)) >>
      coll.bulkInsert(
        documents = boardList.map(boardHandler.write).toStream,
        ordered = true
      ).void

  def insertManyByRounds(roundIds: List[Round.ID], boardList: List[Board]): Funit =
    coll.remove($doc("roundId" $in roundIds)) >>
      coll.bulkInsert(
        documents = boardList.map(boardHandler.write).toStream,
        ordered = true
      ).void

  def byId(id: Game.ID): Fu[Option[Board]] = coll.byId[Board](id)

  def getByContest(contestId: Contest.ID): Fu[List[Board]] =
    coll.find(contestQuery(contestId)).list[Board]()

  def getByContestLteRound(contestId: Contest.ID, no: Round.No): Fu[List[Board]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $lte no)).list[Board]()

  def getByContestGteRound(contestId: Contest.ID, no: Round.No): Fu[List[Board]] =
    coll.find(contestQuery(contestId) ++ $doc("roundNo" $gte no)).list[Board]()

  def getByUser(contestId: Contest.ID, no: Round.No, userId: String): Fu[List[Board]] =
    coll.find(
      contestQuery(contestId) ++ $or(
        $doc("whitePlayer.userId" -> userId),
        $doc("blackPlayer.userId" -> userId)
      ) ++ $doc("roundNo" $lte no)
    ).sort($doc("startsAt" -> 1)).list[Board]()

  def getByRound(roundId: Round.ID): Fu[List[Board]] =
    coll.find(roundQuery(roundId)).list[Board]()

  def setStatus(id: Game.ID, status: chess.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setSign(id: Game.ID, color: chess.Color, isAgent: Boolean): Funit =
    coll.update(
      $id(id),
      $set(
        s"${color.name}Player.sign.isAgent" -> isAgent,
        s"${color.name}Player.sign.signedAt" -> DateTime.now
      )
    ).void

  def setAbsent(id: Game.ID, color: chess.Color): Funit =
    coll.update(
      $id(id),
      $set(
        s"${color.name}Player.absent" -> true
      )
    ).void

  def finishGame(game: Game, winner: Option[chess.Color], result: Board.Result): Funit = {
    coll.update(
      $id(game.id),
      $set(
        "status" -> game.status.id,
        "turns" -> game.playedTurns.some,
        "result" -> result.id,
        "finishAt" -> DateTime.now
      ) ++ winner.?? { wn =>
          wn.fold(
            $set("whitePlayer.isWinner" -> true),
            $set("blackPlayer.isWinner" -> true)
          )
        }
    ).void
  }

  def allFinished(id: Round.ID, boards: Option[Int]): Fu[Boolean] =
    coll.countSel(roundQuery(id) ++ $doc("status" $gte chess.Status.Aborted.id)) map { count =>
      boards.?? { _ == count }
    }

  def pending: Fu[List[Board]] =
    coll.find($doc("status" -> chess.Status.Created.id, "startsAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 20))) /* ++ apptQuery*/ ).list[Board]()

  // 找到未来1分钟内将要开始的对局
  def remindAtSoon: Fu[List[Board]] = {
    val doc = $doc("status" -> chess.Status.Created.id, "startsAt" -> ($lte(DateTime.now plusMinutes 1) ++ $gte(DateTime.now)), "reminded" -> false) /* ++ apptQuery*/
    coll.find(doc).list[Board]()
  }

  def setReminded(id: Game.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "reminded" -> true
      )
    ).void

  def setResult(id: Game.ID, winner: Option[chess.Color], result: Board.Result): Funit =
    winner.fold {
      coll.update(
        $id(id),
        $unset("whitePlayer.isWinner", "blackPlayer.isWinner") ++ $set("result" -> result.id)
      ).void
    } { color =>
      coll.update(
        $id(id),
        $set(
          "whitePlayer.isWinner" -> (color == chess.Color.White),
          "blackPlayer.isWinner" -> (color == chess.Color.Black),
          "result" -> result.id
        )
      ).void
    }

  def update(board: Board): Funit =
    coll.update($id(board.id), board).void

  def remove(id: Game.ID): Funit = coll.remove($id(id)).void

  def removeByRound(roundId: Round.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def removeByRounds(roundId: Round.ID): Funit =
    coll.remove($doc("roundId" -> roundId)).void

  def setStartsTimeByRound(roundId: Round.ID, st: DateTime): Funit =
    coll.update(
      roundQuery(roundId),
      $set(
        "startsAt" -> st
      ),
      multi = true
    ).void

  def gameStartBatch(ids: List[Game.ID], startsAt: DateTime): Funit =
    coll.update(
      $doc("_id" -> $in(ids: _*)),
      $set(
        "status" -> chess.Status.Started.id,
        "startsAt" -> startsAt
      ),
      multi = true
    ).void

  def apptComplete(id: Game.ID, st: DateTime): Funit =
    coll.update(
      $id(id),
      $set("startsAt" -> st, "apptComplete" -> true)
    ).void

  def setStartsTime(id: Game.ID, st: DateTime): Funit =
    coll.update(
      $id(id),
      $set("startsAt" -> st)
    ).void

  def setStartsTimes(roundId: Round.ID, st: DateTime): Funit =
    coll.update(
      roundQuery(roundId),
      $set("startsAt" -> st),
      multi = true
    ).void

  def roundSwap(ids: List[Game.ID], targetRound: Round): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "roundId" -> targetRound.id,
        "roundNo" -> targetRound.no,
        "startsAt" -> targetRound.actualStartsAt
      ),
      multi = true
    ).void

  def contestQuery(contestId: Contest.ID) = $doc("contestId" -> contestId)

  def roundQuery(roundId: Round.ID) = $doc("roundId" -> roundId)

  val apptQuery = $or(
    $doc("appt" -> false),
    $doc("appt" -> true, "apptComplete" -> true)
  )
}
