package lila.contest

import akka.actor.ActorSystem
import chess.{ Black, White, Color }
import lila.game.{ Game, GameRepo }
import lila.hub.{ Duct, DuctMap }
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import org.joda.time.DateTime
import scala.concurrent.duration._
import lila.round.actorApi.round.{ AbortForce, ContestBoardClear }
import lila.contest.actorApi.{ ContestBoardSetTime, ContestRoundPublish, ContestRoundPublishCancel }
import lila.hub.actorApi.contest.ContestBoardSigned
import lila.hub.actorApi.calendar.{ CalendarCreate, CalendarRemove, CalendarsCreate, CalendarsRemove }
import lila.user.User

class RoundApi(
    system: ActorSystem,
    sequencers: DuctMap[_],
    roundMap: DuctMap[_],
    notifyApi: NotifyApi,
    pairingDirector: PairingDirector
) {

  private val bus = system.lilaBus

  def purePlayers(contest: Contest, round: Round): Fu[List[Player]] = {
    val no = round.no
    PlayerRepo.getByContest(contest.id).flatMap { players =>
      //  之前匹配过，所以生成了对应轮次的Outcome，再次匹配时这些Outcome先清除（因为缺席状态可能已经改变）
      val historyOutcomePlayers = players.filter(p => p.isAbsentIgnoreManual(round.no) || p.isHalf(round.no))

      //  上轮 离开、退赛、踢出，本轮继续
      val absents = players.filter(p => p.absent /* && p.roundOutcome(no).isEmpty*/ )
      val quits = absents.filter(_.quit).map(_.no)
      val leaves = absents.filter(_.leave).map(_.no)
      val kicks = absents.filter(_.kick).map(_.no)

      historyOutcomePlayers.nonEmpty.?? { PlayerRepo.removePlayersLastOutcome(contest, historyOutcomePlayers, no) } >>
        quits.nonEmpty.?? { PlayerRepo.setOutcomes(contest.id, no, quits, Board.Outcome.Quit) } >>
        leaves.nonEmpty.?? { PlayerRepo.setOutcomes(contest.id, no, leaves, Board.Outcome.Leave) } >>
        kicks.nonEmpty.?? { PlayerRepo.setOutcomes(contest.id, no, kicks, Board.Outcome.Kick) } >>
        PlayerRepo.getByContest(contest.id)
    }
  }

  //  上轮 离开、退赛、踢出，本轮继续
  //  private def setPlayerAbsent(contest: Contest, no: Round.No): Funit = {
  //    PlayerRepo.getByContest(contest.id).flatMap { players =>
  //      val absents = players.filter(p => p.absent && p.roundOutcome(no).isEmpty)
  //      val leaves = absents.filter(_.leave).map(_.no)
  //      val quits = absents.filter(_.quit).map(_.no)
  //      val kicks = absents.filter(_.kick).map(_.no)
  //      PlayerRepo.setOutcomes(contest.id, no, leaves, Board.Outcome.Leave) >>
  //        PlayerRepo.setOutcomes(contest.id, no, quits, Board.Outcome.Quit) >>
  //        PlayerRepo.setOutcomes(contest.id, no, kicks, Board.Outcome.Kick)
  //    }
  //  }

  def pairing(contest: Contest, round: Round, publishScoreAndFinish: Contest => Funit, random: Boolean = false): Fu[Boolean] = {
    lg(contest, round.some, "比赛编排", s"Random：$random".some)
    purePlayers(contest, round) flatMap { players =>
      pairingDirector.roundPairing(contest, round, players, random) flatMap {
        case None => {
          lg(contest, round.some, "比赛编排失败", "Pairing impossible under the current rules -> force finished!".some, true)
          pairingFailed(contest, round, publishScoreAndFinish).inject(false)
        }
        case Some(boards) => {
          if (boards.isEmpty) {
            lg(contest, round.some, "比赛编排失败", "boards.isEmpty -> force finished!".some, true)
            pairingFailed(contest, round, publishScoreAndFinish).inject(false)
          } else {
            val bs = boards.map(_.playerNos)
            lg(contest, round.some, "比赛编排完成", s"$bs".some)
            contest.autoPairing.?? { autoPublish(contest, round, boards, players) }.inject(true)
          }
        }
      }
    }
  }

  def pairingFailed(contest: Contest, round: Round, publishScoreAndFinish: Contest => Funit): Funit = {
    ContestRepo.setCurrentRound(contest.id, Math.max(round.no - 1, 1)) >>
      ContestRepo.setAllRoundFinished(contest.id) >>
      contest.autoPairing.?? {
        publishScoreAndFinish(contest)
      }
  }

  def publish(contest: Contest, round: Round): Funit = {
    lg(contest, round.some, "比赛编排发布（手动）", none)
    for {
      boards <- BoardRepo.getByRound(round.id)
      players <- PlayerRepo.getByContest(contest.id)
      _ <- addGames(contest, round, boards, players)
      _ <- RoundRepo.setStatus(round.id, Round.Status.Published)
      _ <- setPlayerByeByByeRound(contest, round.no, players)
      _ <- setGameAutoFinish(contest, round, boards, players)
      _ <- PlayerRepo.unAbsentByContest(contest.id)
    } yield {
      bus.publish(ContestRoundPublish(contest, round), 'contestRoundPublish)
      publishCalendar(contest, boards)
      publishCoachCalendar(contest, round)
    }
  }

  def cancelPublish(contest: Contest, round: Round, boards: List[Board]): Funit = {
    lg(contest, round.some, "取消比赛编排发布（手动）", none)
    contest.rule.flow.cancelPairingPublish ?? {
      for {
        players <- PlayerRepo.getByContest(contest.id)
        byePlayers = players.filter(_.isManualAbsent(round.no))
        _ <- removeGames(boards)
        _ <- RoundRepo.setStatus(round.id, Round.Status.Pairing)
        - <- PlayerRepo.setManualAbsent(byePlayers.map(_.id))
      } yield {
        bus.publish(ContestRoundPublishCancel(contest, round), 'contestRoundPublishCancel)
        removeCalendarByBoards(contest, boards)
        removeCoachCalendar(contest, round)
        boards.foreach { board =>
          roundMap.tell(board.id, ContestBoardClear)
        }
      }
    }
  }

  private def removeGames(boards: List[Board]): Funit =
    GameRepo.removeByIds(boards.map(_.id))

  def autoPublish(contest: Contest, round: Round, boards: List[Board], players: List[Player]): Funit = {
    lg(contest, round.some, "比赛编排发布（自动）", none)
    for {
      _ <- addGames(contest, round, boards, players)
      _ <- RoundRepo.setStatus(round.id, Round.Status.Published)
      _ <- setPlayerByeByByeRound(contest, round.no, players)
      _ <- setGameAutoFinish(contest, round, boards, players)
      _ <- PlayerRepo.unAbsentByContest(contest.id)
    } yield {
      bus.publish(ContestRoundPublish(contest, round), 'contestRoundPublish)
      publishCalendar(contest, boards)
      publishCoachCalendar(contest, round)
    }
  }

  private def addGames(contest: Contest, round: Round, boards: List[Board], players: List[Player]): Funit = {
    //logger.info(s"addGames start contest：${contest}, round：${round}, boards：${boards.size}, players：${players.size}")
    val playerMap = Player.toMap(players)
    val games = boards.map(makeGame(contest, round, playerMap))
    //logger.info(s"addGames over contest：${contest}, round：${round}, boards：${boards.size}, players：${players.size}, games：${games.size}")
    lila.common.Future.applySequentially(games) { game =>
      GameRepo.insertDenormalized(game)
    }
  }

  private def makeGame(contest: Contest, round: Round, players: Map[Player.No, Player])(board: Board): Game = {
    //logger.info(s"makeGame start contest：${contest}, round：${round}, board：${board.id}")
    val game = Game.make(
      chess = chess.Game(
        variantOption = Some {
          if (contest.position.initial) chess.variant.Standard
          else chess.variant.FromPosition
        },
        fen = contest.position.fen.some
      ) |> { g =>
          val turns = g.player.fold(0, 1)
          g.copy(
            clock = contest.clock.toClock.some,
            turns = turns,
            startedAtTurn = turns
          )
        },
      whitePlayer = makePlayer(White, players get board.whitePlayer.no err s"Missing board white $board"),
      blackPlayer = makePlayer(Black, players get board.blackPlayer.no err s"Missing board black $board"),
      mode = chess.Mode(contest.mode.rated),
      source = lila.game.Source.Contest,
      pgnImport = None,
      movedAt = Some {
        if (contest.appt) { round.actualStartsAt.plusMinutes(contest.roundSpace).minusMinutes(contest.apptDeadline | 0) } else round.actualStartsAt
      }
    )
      .withId(board.gameId)
      .withContestId(contest.id)
      .withContestCanLateMinutes(contest.canLateMinute)
      .withAppt(contest.appt)
    //logger.info(s"makeGame over contest：${contest}, round：${round}, board：${board.id}, game：${game}")
    game
  }

  private def makePlayer(color: Color, player: Player): lila.game.Player =
    lila.game.Player.make(color, player.userId, player.rating, player.provisional)

  // 根据 ByeRound 属性更新Bey Outcome（循环赛）
  def setPlayerByeByByeRound(contest: Contest, roundNo: Round.No, players: List[Player]): Funit =
    contest.isRoundRobin.?? {
      val newByePlayers = players.filter(_.byeRound.contains(roundNo))
      val newByeNos = newByePlayers.map(_.no)
      val oldByePlayers = players.filter(_.roundOutcome(roundNo).contains(Board.Outcome.Bye))
      val oldByeNos = oldByePlayers.map(_.no)

      oldByePlayers.filter(p => !newByeNos.contains(p.no)).map { player =>
        PlayerRepo.update(
          player.copy(
            outcomes = player.removeOutcomeByRound(roundNo)
          ) |> { p =>
              p.copy(
                score = p.allScore(contest.isRoundRobin),
                points = p.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void >> {
        newByePlayers.filter(p => !oldByeNos.contains(p.no)).map { player =>
          PlayerRepo.update(
            player.copy(
              outcomes = player.setOutcomeByRound(roundNo, Board.Outcome.Bye)
            ) |> { p =>
                p.copy(
                  score = p.allScore(contest.isRoundRobin),
                  points = p.allScore(contest.isRoundRobin)
                )
              }
          )
        }.sequenceFu.void
      }
    }

  def setGameAutoFinish(contest: Contest, round: Round, boards: List[Board], players: List[Player]): Funit =
    contest.isRoundRobin.?? {
      val playerUserIds = players.filter(_.quitOrKickOrManualAbsent).map(_.userId)
      boards.filter(b => b.roundNo == round.no && playerUserIds.exists(b.contains)).distinct.map { board =>
        playerUserIds.contains(board.whitePlayer.userId).?? {
          BoardRepo.setAbsent(board.id, chess.Color.White)
        } >> playerUserIds.contains(board.blackPlayer.userId).?? {
          BoardRepo.setAbsent(board.id, chess.Color.Black)
        } >>- roundMap.tell(board.id, AbortForce)
      }.sequenceFu.void
    }

  def publishResult(contest: Contest, id: Round.ID, no: Round.No): Fu[Contest] = {
    lg(contest, Round.make(no, contest.id, DateTime.now).some, "比赛成绩发布", none)
    for {
      _ <- computeScore(contest, no)
      _ <- RoundRepo.setStatus(id, Round.Status.PublishResult)
      contest <- if (contest.isAllRoundFinished) {
        ContestRepo.setAllRoundFinished(contest.id) inject contest.copy(allRoundFinished = true)
      } else fuccess(contest)
      contest <- if (!contest.isAllRoundFinished) {
        ContestRepo.setCurrentRound(contest.id, no + 1) inject contest.copy(currentRound = no + 1)
      } else fuccess(contest.copy(currentRound = no + 1))
    } yield contest
  }

  def roundPairingTest(contest: Contest, random: Boolean = false): Fu[Boolean] =
    pairingDirector.roundPairingTest(contest, random).map(!_)

  def testScoreSheet(contest: Contest): Fu[Boolean] =
    for {
      _ <- insertMany(contest)
      scoreSheets <- ScoreSheetRepo.getByContest(contest.id)
    } yield {
      println("------------------------------------------------")
      scoreSheets.groupBy(_.roundNo).map {
        case (no, list) => {
          val lst = list.map(d => d.playerUid -> d.rank)
          println(lst)
        }
      }
      true
    }

  def insertMany(contest: Contest): Funit = {
    val no = 6
    val scoreSheets = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15).map { i =>
      ScoreSheet(
        id = ScoreSheet.makeId(i.toString, no),
        contestId = contest.id,
        roundNo = no,
        playerUid = i.toString,
        playerNo = 1,
        score = 10,
        rank = i + 1,
        btssScores = Nil
      )
    }
    for {
      res <- ScoreSheetRepo.removeByRound(contest.id, no) >> ScoreSheetRepo.insertMany(scoreSheets)
    } yield res
  }

  def computeScore(contest: Contest, no: Round.No): Funit = {
    for {
      players <- PlayerRepo.getByContest(contest.id)
      boards <- BoardRepo.getByContestLteRound(contest.id, no)
    } yield {
      val playerDeadlineRound = players.map { player =>
        player.copy(
          score = player.roundScoreWithCurr(no, contest.isRoundRobin),
          points = player.roundScoreWithCurr(no, contest.isRoundRobin),
          outcomes = player.outcomes.take(no)
        )
      }
      val playerBtssScores = Btss.PlayerBtssScores(playerDeadlineRound.map(Btss.PlayerBtssScore(_)))
      val newPlayerBtssScores = contest.btsss.foldLeft(playerBtssScores) {
        case (old, btss) => btss.score(boards, old)
      }

      val scoreSheets = newPlayerBtssScores.sort.zipWithIndex.map {
        case (playerBtssScore, i) => ScoreSheet(
          id = ScoreSheet.makeId(playerBtssScore.player.id, no),
          contestId = playerBtssScore.player.contestId,
          roundNo = no,
          playerUid = playerBtssScore.player.userId,
          playerNo = playerBtssScore.player.no,
          score = playerBtssScore.player.score,
          rank = i + 1,
          btssScores = playerBtssScore.btsss,
          cancelled = playerBtssScore.player.cancelled
        )
      }

      ScoreSheetRepo.removeByRound(contest.id, no) >> ScoreSheetRepo.insertMany(scoreSheets)
    }
  }

  private def finishNotify(c: Contest): Funit = {
    PlayerRepo.getByContest(c.id) map { players =>
      players.foreach { player =>
        notifyApi.addNotification(Notification.make(
          Sender(User.lichessId).some,
          Notifies(player.userId),
          lila.notify.GenericLink(
            url = s"/contest/${c.id}",
            title = "比赛结束".some,
            text = s"比赛【${c.fullName}】已经结束".some,
            icon = "赛"
          )
        ))
      }
    }
  }

  def start: Funit =
    RoundRepo.published map { rounds =>
      rounds foreach { round =>
        if (round.shouldStart) {
          ContestRepo.byId(round.contestId) foreach {
            _.foreach(contest =>
              if (contest.isStarted) {
                setStart(contest, round.id)
              })
          }
        }
      }
    }

  def setStart(contest: Contest, id: Round.ID): Unit =
    Sequencing(id)(RoundRepo.publishedById) { round =>
      lg(contest, round.some, "开始", none)
      RoundRepo.setStatus(round.id, Round.Status.Started)
    }

  def manualAbsent(contest: Contest, round: Round, joins: List[Player.ID], absents: List[Player.ID]): Funit = {
    lg(contest, round.some, "手动弃权", s"Joins: $joins, Absents: $absents".some)
    PlayerRepo.byIds(joins).flatMap { players =>
      players.filter(p => p.absent && p.manualAbsent && !p.absentOr && p.roundOutcome(round.no).??(_ == Board.Outcome.ManualAbsent)).map { player =>
        PlayerRepo.update(
          player.copy(
            absent = false,
            manualAbsent = false,
            outcomes = player.removeOutcomeByRound(round.no)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    } >> PlayerRepo.byIds(absents).flatMap { players =>
      players.filter(p => !p.manualAbsent && p.roundOutcome(round.no).isEmpty).map { player =>
        PlayerRepo.update(
          player.copy(
            absent = true,
            manualAbsent = true,
            outcomes = player.setOutcomeByRound(round.no, Board.Outcome.ManualAbsent)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    }
  }

  def manualPairing(contest: Contest, round: Round, data: ManualPairing): Funit = {
    lg(contest, round.some, "手动匹配", s"source: ${data.source}, target: ${data.target}".some)
    if (!data.source.isBye_ && !data.target.isBye_) {
      for {
        sourceBoardOption <- data.source.board.??(BoardRepo.byId(_))
        targetBoardOption <- data.target.board.??(BoardRepo.byId(_))
      } yield {
        val sourceBoard = sourceBoardOption.err(s"can find board ${data.source.board_}")
        val targetBoard = targetBoardOption.err(s"can find board ${data.target.board_}")
        val sourceWhite = data.source.color_ == 1
        val targetWhite = data.target.color_ == 1
        val sourcePlayer = sourceBoard.player(chess.Color(sourceWhite))
        val targetPlayer = targetBoard.player(chess.Color(targetWhite))
        if (sourceBoard.is(targetBoard)) {
          BoardRepo.update(
            sourceBoard.copy(
              whitePlayer = sourceBoard.blackPlayer,
              blackPlayer = sourceBoard.whitePlayer
            )
          )
        } else {
          val b1 = if (sourceWhite) sourceBoard.copy(whitePlayer = targetPlayer) else sourceBoard.copy(blackPlayer = targetPlayer)
          val b2 = if (targetWhite) targetBoard.copy(whitePlayer = sourcePlayer) else targetBoard.copy(blackPlayer = sourcePlayer)
          BoardRepo.update(b1) >> BoardRepo.update(b2)
        }
      }
    } else if (data.source.isBye_ && !data.target.isBye_) {
      for {
        sourcePlayerOption <- data.source.player.??(PlayerRepo.byId(_))
        targetBoardOption <- data.target.board.??(BoardRepo.byId(_))
        targetPlayerOption <- targetBoardOption.??(b => PlayerRepo.byId(b.player(chess.Color(data.target.color_ == 1)).id))
      } yield {
        val sourcePlayer = sourcePlayerOption.err(s"can find board ${data.source.board_}")
        val targetPlayer = targetPlayerOption.err(s"can find board ${data.source.board_}")
        val targetBoard = targetBoardOption.err(s"can find board ${data.target.board_}")
        val targetWhite = data.target.color_ == 1
        val newTargetBoard =
          if (targetWhite) {
            targetBoard.copy(whitePlayer = Board.MiniPlayer(sourcePlayer.id, sourcePlayer.userId, sourcePlayer.no))
          } else targetBoard.copy(blackPlayer = Board.MiniPlayer(sourcePlayer.id, sourcePlayer.userId, sourcePlayer.no))

        PlayerRepo.update(
          sourcePlayer.copy(
            outcomes = sourcePlayer.removeOutcomeByRound(round.no)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        ) >> PlayerRepo.update(
            targetPlayer.copy(
              outcomes = targetPlayer.setOutcomeByRound(round.no, Board.Outcome.Bye)
            ) |> { player =>
                player.copy(
                  score = player.allScore(contest.isRoundRobin),
                  points = player.allScore(contest.isRoundRobin)
                )
              }
          ) >> BoardRepo.update(newTargetBoard)
      }
    } else if (!data.source.isBye_ && data.target.isBye_) {
      for {
        sourceBoardOption <- data.source.board.??(BoardRepo.byId(_))
        sourcePlayerOption <- sourceBoardOption.??(b => PlayerRepo.byId(b.player(chess.Color(data.source.color_ == 1)).id))
        targetPlayerOption <- data.target.player.??(PlayerRepo.byId(_))
      } yield {
        val sourcePlayer = sourcePlayerOption.err(s"can find board ${data.source.board_}")
        val targetPlayer = targetPlayerOption.err(s"can find board ${data.source.board_}")
        val sourceBoard = sourceBoardOption.err(s"can find board ${data.target.board_}")
        val sourceWhite = data.source.color_ == 1
        val newSourceBoard =
          if (sourceWhite) {
            sourceBoard.copy(whitePlayer = Board.MiniPlayer(targetPlayer.id, targetPlayer.userId, targetPlayer.no))
          } else sourceBoard.copy(blackPlayer = Board.MiniPlayer(targetPlayer.id, targetPlayer.userId, targetPlayer.no))

        PlayerRepo.update(
          sourcePlayer.copy(
            outcomes = sourcePlayer.setOutcomeByRound(round.no, Board.Outcome.Bye)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        ) >> PlayerRepo.update(
            targetPlayer.copy(
              outcomes = targetPlayer.removeOutcomeByRound(round.no)
            ) |> { player =>
                player.copy(
                  score = player.allScore(contest.isRoundRobin),
                  points = player.allScore(contest.isRoundRobin)
                )
              }
          ) >> BoardRepo.update(newSourceBoard)
      }
    } else funit
  }

  def manualResult(contest: Contest, round: Round, board: Board, r: String): Funit = {
    lg(contest, round.some, "手动设置成绩", s"第 ${board.no} 台 $r".some)
    val result = Board.Result(r)
    for {
      whiteOption <- PlayerRepo.byId(board.whitePlayer.id)
      blackOption <- PlayerRepo.byId(board.blackPlayer.id)
    } yield (whiteOption |@| blackOption).tupled ?? {
      case (white, black) => {
        val whiteOutcome = whitePlayerOutcome(result)
        val blackOutcome = blackPlayerOutcome(result)
        val winner = boardWinner(result)
        val newWhite = white.manualResult(board.roundNo, whiteOutcome, contest.isRoundRobin)
        val newBlack = black.manualResult(board.roundNo, blackOutcome, contest.isRoundRobin)
        PlayerRepo.update(newWhite) >>
          PlayerRepo.update(newBlack) >>
          BoardRepo.setResult(board.id, winner, result) >> resetAllScore(contest, round)
      }
    }
  }

  def boardWinner(result: Board.Result) =
    result match {
      case Board.Result.WhiteWin | Board.Result.BlackAbsent => chess.Color.White.some
      case Board.Result.BlackWin | Board.Result.WhiteAbsent => chess.Color.Black.some
      case Board.Result.Draw | Board.Result.AllAbsent => None
    }

  def whitePlayerOutcome(result: Board.Result) =
    result match {
      case Board.Result.WhiteWin => Board.Outcome.Win
      case Board.Result.BlackWin => Board.Outcome.Loss
      case Board.Result.Draw => Board.Outcome.Draw
      case Board.Result.BlackAbsent => Board.Outcome.Win
      case Board.Result.WhiteAbsent => Board.Outcome.NoStart
      case Board.Result.AllAbsent => Board.Outcome.NoStart
    }

  def blackPlayerOutcome(result: Board.Result) =
    result match {
      case Board.Result.WhiteWin => Board.Outcome.Loss
      case Board.Result.BlackWin => Board.Outcome.Win
      case Board.Result.Draw => Board.Outcome.Draw
      case Board.Result.BlackAbsent => Board.Outcome.NoStart
      case Board.Result.WhiteAbsent => Board.Outcome.Win
      case Board.Result.AllAbsent => Board.Outcome.NoStart
    }

  // 重置所有已发布的轮次成绩册
  def resetAllScore(contest: Contest, round: Round): Funit =
    RoundRepo.getAllPublishedRounds(contest.id, round.no).flatMap {
      _.map { round =>
        lg(contest, round.some, "计算成绩册", none)
        computeScore(contest, round.no)
      }.sequenceFu.void
    }

  def roundSwap(contest: Contest, round: Round, roundId: Round.ID): Funit = {
    RoundRepo.byId(roundId) flatMap {
      _.?? { targetRound =>
        lg(contest, round.some, "循环赛交换轮次", s"与第 ${targetRound.no} 轮交换".some)
        for {
          sourcePlayers <- PlayerRepo.findByByeRound(contest.id, round.no)
          targetPlayers <- PlayerRepo.findByByeRound(contest.id, targetRound.no)
          _ <- setPlayersByeRound(sourcePlayers, round, targetRound)
          _ <- setPlayersByeRound(targetPlayers, targetRound, round)
          sourceBoards <- BoardRepo.getByRound(round.id)
          targetBoards <- BoardRepo.getByRound(targetRound.id)
          _ <- BoardRepo.roundSwap(sourceBoards.map(_.id), targetRound)
          res <- BoardRepo.roundSwap(targetBoards.map(_.id), round)
        } yield res
      }
    }
  }

  private def setPlayersByeRound(players: List[Player], round: Round, targetRound: Round): Funit = {
    players.map { player =>
      val newByeRound = player.byeRound.map { no =>
        if (no == round.no) targetRound.no else no
      }
      PlayerRepo.setByeRound(player.id, newByeRound)
    }.sequenceFu.void
  }

  def setStartsTime(contest: Contest, round: Round, st: DateTime): Funit = {
    lg(contest, round.some, "设置轮次开始时间", s"${st.toString("yyyy-MM-dd HH:mm")}".some)
    RoundRepo.setStartsTime(round.id, st) >>-
      resetCoachCalendarByRound(contest, round, st) >>
      (!contest.appt).?? {
        BoardRepo.setStartsTimeByRound(round.id, st) >> resetCalendarByRound(contest, round, st)
      }
  }

  def setBoardTime(contest: Contest, round: Round, board: Board, st: DateTime): Funit = {
    lg(contest, round.some, "设置对局开始时间", s"第 ${board.no} 台，${st.toString("yyyy-MM-dd HH:mm")}".some)
    BoardRepo.apptComplete(board.id, st) >>
      GameRepo.apptComplete(board.id, st) >>- {
        bus.publish(ContestBoardSetTime(contest, board, st), 'contestBoardSetTime)
        resetCalendar(contest, board.copy(startsAt = st))
      }
  }

  def setBoardSign(board: Board, userId: User.ID, isAgent: Boolean): Funit = {
    BoardRepo.setSign(board.id, board.colorOfByUserId(userId), isAgent) >>-
      roundMap.tell(board.id, ContestBoardSigned(board.id, userId))
  }

  def apptComplete(gameId: String, time: DateTime): Funit =
    BoardRepo.apptComplete(gameId, time) >> GameRepo.apptComplete(gameId, time)

  private def Sequencing(id: Round.ID)(fetch: Round.ID => Fu[Option[Round]])(run: Round => Funit): Unit =
    doSequence(id) {
      fetch(id) flatMap {
        case Some(t) => run(t)
        case None => fufail(s"Can't run sequenced operation on missing contest round $id")
      }
    }

  private def doSequence(id: Round.ID)(fu: => Funit): Unit =
    sequencers.tell(id, Duct.extra.LazyFu(() => fu))

  private def removeCalendarByBoards(contest: Contest, boards: List[Board]): Funit = {
    (!contest.appt).?? {
      val ids = boards.foldLeft(List.empty[String]) {
        case (list, board) => list ++ board.players.map { player => s"${board.id}@${player.userId}" }
      }
      bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
      funit
    }
  }

  private def removeCoachCalendar(contest: Contest, round: Round): Funit = {
    (!contest.appt).?? {
      bus.publish(CalendarRemove(s"${round.id}@${contest.createdBy}"), 'calendarRemoveBus)
      funit
    }
  }

  private def resetCalendarByRound(contest: Contest, round: Round, st: DateTime): Funit = {
    (!contest.appt).?? {
      BoardRepo.getByRound(round.id) flatMap { boards =>
        val calendars = boards.foldLeft(List.empty[CalendarCreate]) {
          case (list, board) => list ++ makeCalendar(contest, board.copy(startsAt = st))
        }

        val ids = boards.foldLeft(List.empty[String]) {
          case (list, board) => list ++ board.players.map { player => s"${board.id}@${player.userId}" }
        }

        bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
        system.scheduler.scheduleOnce(3 seconds) {
          bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
        }
        funit
      }
    }
  }

  private def resetCalendar(contest: Contest, board: Board) = {
    if (!contest.appt) {
      val ids = board.players.map { player => s"${board.id}@${player.userId}" }
      val calendars = makeCalendar(contest, board)
      bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
      system.scheduler.scheduleOnce(3 seconds) {
        bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
      }
    }
  }

  private def publishCalendar(contest: Contest, boards: Boards): Unit = {
    if (!contest.appt) {
      val calendars = boards.foldLeft(List.empty[CalendarCreate]) {
        case (lst, b) => lst ++ makeCalendar(contest, b)
      }
      bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def makeCalendar(contest: Contest, board: Board): List[CalendarCreate] = {
    board.players.map { player =>
      CalendarCreate(
        id = s"${board.id}@${player.userId}".some,
        typ = "contest",
        user = player.userId,
        sdt = board.startsAt,
        edt = board.startsAt.plusMinutes(contest.roundSpace),
        content = s"${contest.name} 第${board.roundNo}轮 #${board.no}",
        onlySdt = false,
        link = s"/${board.id}".some,
        icon = "奖".some,
        bg = "#ae8300".some
      )
    }
  }

  private def publishCoachCalendar(contest: Contest, round: Round): Unit = {
    val calendars = List(
      CalendarCreate(
        id = s"${round.id}@${contest.createdBy}".some,
        typ = "contest",
        user = contest.createdBy,
        sdt = round.actualStartsAt,
        edt = round.actualStartsAt.plusMinutes(contest.roundSpace),
        content = s"${contest.name} 第${round.no}轮",
        onlySdt = false,
        link = s"/contest/${contest.id}#round${round.no}".some,
        icon = "奖".some,
        bg = "#ae8300".some
      )
    )
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  private def resetCoachCalendarByRound(contest: Contest, round: Round, st: DateTime): Unit = {
    val calendars = List(
      CalendarCreate(
        id = s"${round.id}@${contest.createdBy}".some,
        typ = "contest",
        user = contest.createdBy,
        sdt = st,
        edt = st.plusMinutes(contest.roundSpace),
        content = s"${contest.name} 第${round.no}轮",
        onlySdt = false,
        link = s"/contest/${contest.id}#round${round.no}".some,
        icon = "奖".some,
        bg = "#ae8300".some
      )
    )

    bus.publish(CalendarRemove(s"${round.id}@${contest.createdBy}"), 'calendarRemoveBus)
    system.scheduler.scheduleOnce(3 seconds) {
      bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def lg(contest: Contest, round: Option[Round], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
