package lila.contest

import lila.user.User
import lila.game.Game
import lila.db.dsl._
import reactivemongo.bson._
import org.joda.time.DateTime

import scala.collection.breakOut

object PlayerRepo {

  private[contest] lazy val coll = Env.current.playerColl
  import BSONHandlers._

  type ID = String

  def byId(id: Player.ID): Fu[Option[Player]] = coll.byId[Player](id)

  def byIds(ids: List[Player.ID]): Fu[List[Player]] = coll.byIds[Player](ids)

  def byNos(contestId: Contest.ID, nos: List[Player.No]): Fu[List[Player]] =
    coll.find(contestQuery(contestId) ++ $doc("no" $in nos)).list[Player]()

  def byOrderedIds(ids: List[Player.ID]): Fu[List[Player]] =
    coll.byOrderedIds[Player, Player.ID](ids)(_.id)

  def findByByeRound(contestId: Contest.ID, byeRound: Round.No): Fu[List[Player]] =
    coll.find(contestQuery(contestId) ++ $doc("byeRound" -> byeRound)).list[Player]()

  def find(contestId: Contest.ID, userId: User.ID): Fu[Option[Player]] =
    byId(Player.makeId(contestId, userId))

  def insert(player: Player): Funit = coll.insert(player).void

  def countByContest(contestId: Contest.ID): Fu[Int] =
    coll.countSel(contestQuery(contestId))

  def getByUserId(userId: User.ID): Fu[List[Player]] =
    coll.find($doc("userId" -> userId)).list[Player]()

  def getByUserIdNear1Year(userId: User.ID): Fu[List[Player]] =
    coll.find($doc("userId" -> userId, "entryTime" $gt DateTime.now.minusYears(1))).list[Player]()

  def getByContest(contestId: Contest.ID): Fu[List[Player]] =
    coll.find(contestQuery(contestId)).sort($sort asc "no").list[Player]()

  def remove(id: Player.ID): Funit = coll.remove($id(id)).void

  def kick(id: Player.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "absent" -> true,
        "kick" -> true
      )
    ).void

  def unkick(id: Player.ID, isAbsent: Boolean): Funit =
    coll.update(
      $id(id),
      $set(
        "absent" -> isAbsent,
        "leave" -> false,
        "quit" -> false,
        "kick" -> false
      )
    ).void

  def quit(contestId: Contest.ID, userId: User.ID): Funit =
    coll.update(
      $id(Player.makeId(contestId, userId)),
      $set(
        "absent" -> true,
        "quit" -> true
      )
    ).void

  // 上轮弃权，本轮恢复->影响所有棋手
  def unAbsentByContest(contestId: Contest.ID): Funit =
    // 之前轮次不是禁赛、退赛、离开的状态，那么取消弃权后可以继续匹配
    coll.update(
      $doc("contestId" -> contestId, "manualAbsent" -> true, "leave" -> false, "quit" -> false, "kick" -> false),
      $set(
        "absent" -> false,
        "manualAbsent" -> false
      ),
      multi = true
    ).void >>
      // 之前轮次存在（禁赛、退赛、离开）其中一个状态，那么取消弃权后可以不能匹配
      coll.update(
        $doc("contestId" -> contestId, "manualAbsent" -> true, $or("leave" $eq true, "quit" $eq true, "kick" $eq true)),
        $set(
          "manualAbsent" -> false
        ),
        multi = true
      ).void

  // 设置手动弃权状态，unAbsentByContest函数的反向操作，取消编排发布时使用
  def setManualAbsent(ids: List[Player.ID]): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "absent" -> true,
        "manualAbsent" -> true
      ),
      multi = true
    ).void

  def findNextNo(contestId: Contest.ID): Fu[Int] = {
    coll.find(contestQuery(contestId), $doc("_id" -> false, "no" -> true))
      .sort($sort desc "no")
      .uno[Bdoc] map {
        _ flatMap { doc => doc.getAs[Int]("no") map (1 + _) } getOrElse 1
      }
  }

  def bulkUpdate(contestId: Contest.ID, players: List[Player]): Funit =
    removeByContest(contestId) >> bulkInsert(players).void

  def removeByContest(contestId: Contest.ID): Funit =
    coll.remove(contestQuery(contestId)).void

  def bulkInsert(players: List[Player]): Funit = coll.bulkInsert(
    documents = players.map(playerHandler.write).toStream,
    ordered = true
  ).void

  def rounds(contest: Contest): Fu[List[(Player, Map[Round.No, Board])]] = {
    import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework._
    coll.aggregateList(
      Match($doc("contestId" -> contest.id)),
      List(
        PipelineOperator(
          $doc(
            "$lookup" -> $doc(
              "from" -> Env.current.settings.CollectionBoard,
              "let" -> $doc("pno" -> "$no"),
              "pipeline" -> $arr(
                $doc(
                  "$match" -> $doc(
                    "$expr" -> $doc(
                      "$and" -> $arr(
                        $doc("$eq" -> $arr("$contestId", contest.id)),
                        $doc("$or" -> $arr(
                          $doc("$eq" -> $arr("$$pno", "$whitePlayer.no")),
                          $doc("$eq" -> $arr("$$pno", "$blackPlayer.no"))
                        ))
                      )
                    )
                  )
                )
              ),
              "as" -> "boards"
            )
          )
        )
      ),
      maxDocs = 1000
    ).map {
        _.flatMap { doc =>
          val result = for {
            player <- playerHandler.readOpt(doc)
            boards <- doc.getAs[List[Board]]("boards")
            boardMap = boards.map { b =>
              b.roundNo -> b
            }.toMap
          } yield (player, boardMap)
          result
        }(breakOut).toList
      }
  }

  def removePlayersLastOutcome(contest: Contest, players: List[Player], roundNo: Round.No): Funit =
    players.map { player =>
      removePlayerLastOutcome(contest, player, roundNo)
    }.sequenceFu.void

  def removePlayerLastOutcome(contest: Contest, player: Player, roundNo: Round.No): Funit = {
    update(
      player.copy(
        outcomes = player.removeOutcomeByRound(roundNo)
      ) |> { p =>
          p.copy(
            score = p.allScore(contest.isRoundRobin),
            points = p.allScore(contest.isRoundRobin)
          )
        }
    )
  }

  def setOutcomes(contestId: Contest.ID, roundNo: Int, players: List[Player.No], outcome: Board.Outcome): Funit =
    coll.update(
      contestQuery(contestId) ++ $doc("no" -> $in(players: _*)),
      $set(s"outcomes.${roundNo - 1}" -> outcome.id),
      multi = true
    ).void

  def setScore(id: Player.ID, score: Double): Funit =
    coll.update(
      $id(id),
      $set(
        "score" -> score,
        "points" -> score
      )
    ).void

  def setNo(id: Player.ID, no: Player.No): Funit = coll.updateField($id(id), "no", no).void

  def finishGame(contest: Contest, rn: Round.No, playerUserId: User.ID, outcome: Board.Outcome): Funit = {
    val id = Player.makeId(contest.id, playerUserId)
    byId(id) flatMap {
      case None => fufail(s"can not find player $id")
      case Some(p) => {
        coll.update(
          $id(id),
          p.finish(rn, outcome, contest.isRoundRobin, contest.canQuitNumber)
        ).void
      }
    }
  }

  def update(player: Player): Funit = {
    coll.update(
      $id(player.id),
      player
    ).void
  }

  def setCancelScore(id: Player.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "cancelled" -> true
      )
    ).void

  def setByeRound(id: Player.ID, byeRound: List[Round.No]): Funit =
    coll.update(
      $id(id),
      $set(
        "byeRound" -> byeRound
      )
    ).void

  def contestQuery(contestId: Contest.ID) = $doc("contestId" -> contestId)

}
