package lila.contest

import BSONHandlers._
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import reactivemongo.bson.BSONDocument
import lila.user.User

class ContestPager {

  def all(page: Int, status: Option[Contest.Status], me: User, clazzs: List[String], text: String) = paginator(
    page,
    text,
    ContestRepo.allSelect ++ status ?? ContestRepo.statusSelect ++ organizerSelector(me, clazzs),
    ContestRepo.startDesc
  )

  def belong(page: Int, status: Option[Contest.Status], me: User, text: String) =
    PlayerRepo.getByUserId(me.id) flatMap { list =>
      paginator(
        page,
        text,
        ContestRepo.idsSelect(list.map(_.contestId)) ++ ContestRepo.belongSelect ++ status ?? ContestRepo.statusSelect,
        ContestRepo.startDesc
      )
    }

  def owner(page: Int, status: Option[Contest.Status], me: User, text: String) = paginator(
    page,
    text,
    ContestRepo.createBySelect(me.id) ++ ContestRepo.ownerSelect ++ status ?? ContestRepo.statusSelect,
    ContestRepo.startDesc
  )

  def finish(page: Int, status: Option[Contest.Status], me: User, clazzs: List[String], text: String) = paginator(
    page,
    text,
    ContestRepo.finishedOrCancelSelect ++ status ?? ContestRepo.statusSelect ++ organizerSelector(me, clazzs),
    ContestRepo.startDesc
  )

  def api(page: Int, me: User, clazzs: List[String], text: String) = paginator(
    page,
    text,
    $doc("status" $in Contest.Status.overStarted.map(_.id)) ++ organizerSelector(me, clazzs),
    ContestRepo.startDesc
  )

  private def organizerSelector(me: User, clazzs: List[String]) = {
    me.belongTeamId.fold {
      $or(
        $doc("typ" $in List("public")),
        $doc("typ" -> "clazz-inner", "organizer" $in clazzs)
      )
    } { teamId =>
      $or(
        $doc("typ" $in List("public", "team-inner"), "organizer" -> teamId),
        $doc("typ" -> "clazz-inner", "organizer" $in clazzs)
      )
    }
  }

  private def paginator(page: Int, text: String, $selector: BSONDocument, $order: BSONDocument) = {
    val textOption = if (text.trim.isEmpty) none else text.trim.some
    //println(BSONDocument.pretty($selector))
    Paginator[Contest](adapter = new Adapter[Contest](
      collection = ContestRepo.coll,
      selector = $selector ++ textOption ?? (t => $doc("name" $regex (t, "i"))),
      projection = $empty,
      sort = $order
    ), currentPage = page, maxPerPage = MaxPerPage(16))
  }

}
