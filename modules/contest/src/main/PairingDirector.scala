package lila.contest

import lila.game.IdGenerator

final class PairingDirector(pairingSystem: PairingSystem) {

  private[contest] def roundPairingTest(contest: Contest, random: Boolean = false): Fu[Boolean] =
    pairingSystem(contest, random)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(false)
        else fuccess(true)
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          false
      }

  private[contest] def roundPairing(contest: Contest, round: Round, players: List[Player], random: Boolean = false): Fu[Option[List[Board]]] =
    pairingSystem(contest, random)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          for {
            boards <- boards(contest, round, pendings, players)
            _ <- addBoards(round, boards)
            _ <- setPlayerBye(contest, round.no, pendings, players)
            _ <- RoundRepo.setBoards(round.id, boards.size)
            _ <- RoundRepo.setStatus(round.id, Round.Status.Pairing)
          } yield Some(boards)
        }
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[Board])
        case RoundRobinPairing.PairingException(msg) =>
          logger.warn(s"RoundRobinPairing failed, contest: $contest, msg: " + msg)
          Some(List.empty[Board])
      }

  private[contest] def pairingAll(contest: Contest, rounds: List[Round], players: List[Player]): Fu[Option[List[Board]]] = {
    logger.info(s"循环赛匹配开始：$contest")
    pairingSystem.pairingAll(contest, players)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          for {
            boards <- allRoundBoards(contest, rounds, players, pendings)
            _ <- addAllBoards(rounds, boards)
            _ <- setAllPlayerByeRound(contest, rounds, players, pendings)
            _ <- RoundRepo.setBoardsAndStatus(rounds.map(_.id), players.size / 2, Round.Status.Pairing)
            firstRound = rounds.minBy(_.no)
          } yield Some(boards.filter(_.roundNo == firstRound.no))
        }
      }
      .recover {
        case SwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[Board])
        case RoundRobinPairing.PairingException(msg) =>
          logger.warn(s"RoundRobinPairing failed, contest: $contest, msg: " + msg)
          Some(List.empty[Board])
      }
  }

  private def addBoards(round: Round, boards: List[Board]): Funit = {
    BoardRepo.removeByRound(round.id) >> BoardRepo.insertMany(round.id, boards)
  }

  private def addAllBoards(rounds: List[Round], boards: List[Board]): Funit = {
    BoardRepo.insertManyByRounds(rounds.map(_.id), boards)
  }

  private def boards(contest: Contest, round: Round, byeOrPendings: List[PairingSystem.ByeOrPending], players: List[Player]): Fu[List[Board]] = {
    byeOrPendings.zipWithIndex.collect {
      case (Right(PairingSystem.Pending(w, b)), i) =>
        IdGenerator.game dmap { id =>
          val white = players.find(_.no == w) err s"cannot find player $w"
          val black = players.find(_.no == b) err s"cannot find player $b"
          Board(
            id = id,
            no = i + 1,
            contestId = contest.id,
            roundId = round.id,
            roundNo = round.no,
            status = chess.Status.Created,
            whitePlayer = Board.MiniPlayer(white.id, white.userId, white.no, None),
            blackPlayer = Board.MiniPlayer(black.id, black.userId, black.no, None),
            startsAt = if (contest.appt) { round.actualStartsAt.plusMinutes(contest.roundSpace).minusMinutes(contest.apptDeadline | 0) } else round.actualStartsAt,
            appt = contest.appt
          )
        }
    }.sequenceFu
  }

  private def allRoundBoards(contest: Contest, rounds: List[Round], players: List[Player], byeOrPendingMap: Map[Int, List[PairingSystem.ByeOrPending]]): Fu[List[Board]] = {
    IdGenerator.games(rounds.size * (players.size / 2)) dmap { gameIds =>
      val gameIdArray = gameIds.toArray
      var idIndex = 0
      rounds.flatMap { round =>
        byeOrPendingMap.get(round.no) match {
          case Some(byeOrPendings) => {
            byeOrPendings.collect { case Right(pending) => pending }.zipWithIndex.map {
              case (pending, i) => {
                val white = players.find(_.no == pending.white) err s"cannot find player ${pending.white}"
                val black = players.find(_.no == pending.black) err s"cannot find player ${pending.black}"
                val board = Board(
                  id = gameIdArray(idIndex),
                  no = i + 1,
                  contestId = contest.id,
                  roundId = round.id,
                  roundNo = round.no,
                  status = chess.Status.Created,
                  whitePlayer = Board.MiniPlayer(white.id, white.userId, white.no, None),
                  blackPlayer = Board.MiniPlayer(black.id, black.userId, black.no, None),
                  startsAt = if (contest.appt) { round.actualStartsAt.plusMinutes(contest.roundSpace).minusMinutes(contest.apptDeadline | 0) } else round.actualStartsAt,
                  appt = contest.appt
                )
                idIndex = idIndex + 1
                board
              }
            }
          }
          case None => throw RoundRobinPairing.PairingException(s"allRoundBoards RoundRobinPairing PairError $contest 第${round.no}轮")
        }
      }
    }
  }

  private def setPlayerBye(contest: Contest, roundNo: Round.No, byeOrPendings: List[PairingSystem.ByeOrPending], players: List[Player]): Funit = {
    val newByeNos = byeOrPendings.collect { case Left(bye) => bye.player }
    val newByePlayers = players.filter(p => newByeNos.contains(p.no))
    val oldByePlayers = players.filter(_.roundOutcome(roundNo).contains(Board.Outcome.Bye))
    val oldByeNos = oldByePlayers.map(_.no)

    oldByePlayers.filter(p => !newByeNos.contains(p.no)).map { player =>
      PlayerRepo.update(
        player.copy(
          outcomes = player.removeOutcomeByRound(roundNo)
        ) |> { p =>
            p.copy(
              score = p.allScore(contest.isRoundRobin),
              points = p.allScore(contest.isRoundRobin)
            )
          }
      )
    }.sequenceFu.void >> {
      newByePlayers.filter(p => !oldByeNos.contains(p.no)).map { player =>
        PlayerRepo.update(
          player.copy(
            outcomes = player.setOutcomeByRound(roundNo, Board.Outcome.Bye)
          ) |> { p =>
              p.copy(
                score = p.allScore(contest.isRoundRobin),
                points = p.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    }
  }

  private def setAllPlayerByeRound(contest: Contest, rounds: List[Round], players: List[Player], byeOrPendingMap: Map[Int, List[PairingSystem.ByeOrPending]]): Funit = {
    byeOrPendingMap.foldLeft(Map.empty[Player.No, List[Round.No]]) {
      case (playerByeRound, (roundNo, byeOrPendings)) => {
        val byeNos = byeOrPendings.collect { case Left(bye) => bye.player }
        var newMap = playerByeRound
        byeNos.foreach { playerNo =>
          newMap = newMap.get(playerNo) match {
            case None => newMap + (playerNo -> List(roundNo))
            case Some(roundNos) => newMap + (playerNo -> (roundNos :+ roundNo))
          }
        }
        newMap
      }
    }.map {
      case (playerNo, byeRound) => {
        val player = players.find(_.no == playerNo) err s"cannot find player $playerNo"
        PlayerRepo.setByeRound(player.id, byeRound)
      }
    }.sequenceFu.void
  }

}

