package lila.contest

object RoundRobinPairingGenerate {

  val list = (2 to 22).toList

  list.foreach { v =>
    if (v % 2 == 0) {
      val players = (1 to v).toList
      val pairs = playersPair(players)

      println(players)
      println(pairs)
    }
  }

  def playersPair(players: List[Int]) = {
    val playerCount = players.size
    players.flatMap { player1 =>
      players.filterNot(_ == player1).map { player2 =>
        playAtRound(player1, player2, playerCount) -> firstMove(player1, player2, playerCount)
      }
    }
      .groupBy(_._1)
      .mapValues(_.map(_._2))
      .mapValues(_.distinct)
  }

  def playAtRound(player1: Int, player2: Int, playerCount: Int) = {
    val hasLastPlayer = player1 == playerCount || player2 == playerCount
    if (hasLastPlayer) lastPlayAtRound(player1, player2, playerCount)
    else generalPlayAtRound(player1, player2, playerCount)
  }

  // 除最后一名棋手外的轮次计算
  def generalPlayAtRound(player1: Int, player2: Int, playerCount: Int) = {
    // 总数 n >= a(编号) + b(编号) => 轮次R = a + b - 1
    if (playerCount >= player1 + player2) {
      player1 + player2 - 1
      // 总数 n < a(编号) + b(编号) => 轮次R = a + b - n
    } else {
      player1 + player2 - playerCount
    }
  }

  // 最后一名棋手的轮次计算
  def lastPlayAtRound(player1: Int, player2: Int, playerCount: Int) = {
    var last, other = 0
    if (player1 == playerCount) {
      last = player1
      other = player2
    } else {
      last = player2
      other = player1
    }

    // 总数 n >= 2x(编号) => 轮次R = 2x - 1
    if (playerCount >= 2 * other) {
      2 * other - 1
      // 总数 n < 2x(编号) => 轮次R = 2x - n
    } else {
      2 * other - playerCount
    }
  }

  def firstMove(player1: Int, player2: Int, playerCount: Int) = {
    val hasLastPlayer = player1 == playerCount || player2 == playerCount
    if (hasLastPlayer) lastFirstMove(player1, player2, playerCount)
    else generalFirstMove(player1, player2, playerCount)
  }

  // 除最后一名棋手外的先后手计算
  // a + b = 单数时，小号先走，即 a 先走:
  // a + b = 双数时，大号先走，即 b 先走
  def generalFirstMove(player1: Int, player2: Int, playerCount: Int) = {
    if ((player1 + player2) % 2 == 1) {
      if (player1 < player2) (player1, player2)
      else (player2, player1)
    } else {
      if (player1 < player2) (player2, player1)
      else (player1, player2)
    }
  }

  // 最后一名棋手的先后手计算
  // 所有 n/2 以前编号的棋手执白先走，n/2 以后编号的棋执黑后走。
  def lastFirstMove(player1: Int, player2: Int, playerCount: Int) = {
    val mid = playerCount / 2
    if (player1 == playerCount) {
      if (player2 <= mid) (player2, player1)
      else (player1, player2)
    } else {
      if (player1 <= mid) (player1, player2)
      else (player2, player1)
    }
  }

}
