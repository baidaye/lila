package lila.contest

final class PairingSystem(
    swissPairing: SwissPairing,
    roundRobinPairing: RoundRobinPairing,
    randomPairing: RandomPairing
) {

  import PairingSystem._

  def apply(contest: Contest, random: Boolean = false): Fu[List[ByeOrPending]] =
    if (random) {
      randomPairing.pairing(contest)
    } else {
      contest.rule match {
        case Contest.Rule.Swiss => swissPairing.pairing(contest)
        case Contest.Rule.RoundRobin => roundRobinPairing.pairing(contest)
        case Contest.Rule.Random => randomPairing.pairing(contest)
        case _ => fuccess(Nil)
      }
    }

  def pairingAll(contest: Contest, players: List[Player]): Fu[Map[Int, List[ByeOrPending]]] =
    contest.rule match {
      case Contest.Rule.Swiss => swissPairing.pairingAll(contest, players)
      case Contest.Rule.RoundRobin => roundRobinPairing.pairingAll(contest, players)
      case Contest.Rule.DBRoundRobin => roundRobinPairing.pairingAll(contest, players)
      case Contest.Rule.Random => randomPairing.pairingAll(contest, players)
      case _ => fuccess(Map.empty[Int, List[ByeOrPending]])
    }

}

object PairingSystem {

  type ByeOrPending = Either[Bye, Pending]

  case class Pending(
      white: Player.No,
      black: Player.No
  )
  case class Bye(player: Player.No)

}
