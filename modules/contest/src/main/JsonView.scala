package lila.contest

import lila.common.LightUser
import play.api.libs.json._
import lila.common.paginator.{ Paginator, PaginatorJson }

final class JsonView(lightUserApi: lila.user.LightUserApi) {

  def rule(r: Contest.Rule) = {
    Json.obj(
      "id" -> r.id,
      "name" -> r.name,
      "setup" -> Json.obj(
        "formField" -> r.setup.formField,
        "maxRound" -> r.setup.maxRound,
        "defaultRound" -> r.setup.defaultRound,
        "minPlayer" -> r.setup.minPlayer,
        "maxPlayer" -> r.setup.maxPlayer,
        "defaultPlayer" -> r.setup.defaultPlayer,
        "btsss" -> JsArray(
          r.setup.btsss.list.map { btss =>
            Json.obj(
              "id" -> btss.id,
              "name" -> btss.name
            )
          }
        )
      )
    )
  }

  def contestPage(pager: Paginator[Contest]) = {
    implicit val pagerWriter = Writes[Contest] { c => contest(c) }
    Json.obj(
      "paginator" -> PaginatorJson(pager)
    )
  }

  def contest(c: Contest) = {
    Json.obj(
      "id" -> c.id,
      "name" -> c.fullName,
      "startsAt" -> c.startsAt.toString("yyyy-MM-dd HH:mm")
    )
  }

  def rounds(list: List[Round]) = {
    JsArray(list.map(r => round(r)))
  }

  def round(r: Round) = {
    Json.obj(
      "id" -> r.id,
      "no" -> r.no,
      "boards" -> r.boards,
      "startsAt" -> r.actualStartsAt.toString("yyyy-MM-dd HH:mm")
    )
  }

  def boards(list: List[Board], markMap: Map[String, Option[String]]) = {
    val players = list.flatMap(_.players.map(_.userId))
    lightUserApi.asyncMany(players.distinct) map { users =>
      JsArray(
        list.map(b =>
          board(b, users, markMap))
      )
    }
  }

  def board(b: Board, lightUsers: List[Option[LightUser]], markMap: Map[String, Option[String]]) = {
    val white = lightUsers.find(_.exists(_.id == b.whitePlayer.userId)) match {
      case None => LightUser.fallback(b.whitePlayer.userId)
      case Some(p) => p | LightUser.fallback(b.whitePlayer.userId)
    }
    val black = lightUsers.find(_.exists(_.id == b.blackPlayer.userId)) match {
      case None => LightUser.fallback(b.whitePlayer.userId)
      case Some(p) => p | LightUser.fallback(b.blackPlayer.userId)
    }

    Json.obj(
      "id" -> b.id,
      "no" -> b.no,
      "white" -> markOrUsername(markMap, white),
      "black" -> markOrUsername(markMap, black),
      "result" -> b.resultFormat
    )
  }

  private def markOrUsername(markMap: Map[String, Option[String]], lightUser: LightUser): String = {
    val mark = markMap.get(lightUser.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | lightUser.name
  }

}

