package lila.contest

import chess.Color
import lila.game.Game
import lila.user.User
import org.joda.time.DateTime

case class Board(
    id: Game.ID,
    no: Board.No,
    contestId: Contest.ID,
    roundId: Round.ID,
    roundNo: Round.No,
    status: chess.Status,
    whitePlayer: Board.MiniPlayer,
    blackPlayer: Board.MiniPlayer,
    startsAt: DateTime,
    appt: Boolean = false,
    apptComplete: Boolean = false,
    reminded: Boolean = false,
    turns: Option[Int] = None, // playedTurns
    finishAt: Option[DateTime] = None,
    result: Option[Board.Result] = None
) {

  def gameId = id
  def isCreated = status.id == chess.Status.Created.id
  def isStarted = status.id == chess.Status.Started.id
  def isFinished = status.id >= chess.Status.Aborted.id
  def shouldStart = startsAt.getMillis <= DateTime.now.getMillis
  def is(b: Board) = b.id == id
  def secondsToStart = (startsAt.getSeconds - nowSeconds).toInt atLeast 0
  def startStatus = secondsToStart |> { s => "%02d:%02d".format(s / 60, s % 60) }
  def canCancel = isCreated && secondsToStart > 180

  def player(color: Color): Board.MiniPlayer =
    color.fold(whitePlayer, blackPlayer)

  def opponentOf(no: Player.No): Option[Player.No] =
    if (no == whitePlayer.no) blackPlayer.no.some
    else if (no == blackPlayer.no) whitePlayer.no.some
    else none

  def colorOf(no: Player.No): Option[Color] =
    if (no == whitePlayer.no) Color.White.some
    else if (no == blackPlayer.no) Color.Black.some
    else none

  def colorOfById(playerId: Player.ID): Color =
    if (playerId == whitePlayer.id) Color.White
    else if (playerId == blackPlayer.id) Color.Black
    else Color.White

  def colorOfByUserId(userId: User.ID): Color =
    if (userId == whitePlayer.userId) Color.White
    else if (userId == blackPlayer.userId) Color.Black
    else Color.White

  def contains(no: Player.No): Boolean =
    whitePlayer.no == no || blackPlayer.no == no

  def contains(userId: User.ID): Boolean =
    whitePlayer.userId == userId || blackPlayer.userId == userId

  def players = List(whitePlayer, blackPlayer)

  def playerNos = (whitePlayer.no, blackPlayer.no)

  def exists(no: Player.No) = players.exists(_.no == no)

  def winner: Option[Board.MiniPlayer] = players find (_.wins)

  def isDraw = winner.isEmpty

  def isWin(no: Player.No) = winner.??(_.no == no)

  def accountable = turns.exists(_ >= 2)

  def resultShow = status match {
    case chess.Status.Created => "等待开赛"
    case chess.Status.Started => "比赛中"
    case _ => resultFormat
  }

  def resultFormat = result.map(_.id) | {
    if (whitePlayer.wins) "1-0"
    else if (blackPlayer.wins) "0-1"
    else "1/2-1/2"
  }

  def playerResult(playerNo: Player.No) = {
    colorOf(playerNo).?? { color =>
      result.map {
        _.resultOf(color)
      }
    }
  }

  def isOnlyOpponentAbsent(playerNo: Player.No) = {
    (playerNo == whitePlayer.no && result.??(_ == Board.Result.BlackAbsent)) || (playerNo == blackPlayer.no && result.??(_ == Board.Result.WhiteAbsent))
  }

}

object Board {

  type No = Int

  case class FullInfo(board: Board, round: Round, contest: Contest) {
    def fullName = s"${contest.fullName} - 第${round.no}轮"
    def boardName = s"第${round.no}轮 #${board.no}"

    def canStarted = (contest.isStarted || contest.isEnterStopped) && (round.isStarted || round.isPublished) && board.shouldStart
  }

  case class WithPov(board: Board, pov: lila.game.Pov)

  private[contest] case class Sign(isAgent: Boolean, signedAt: DateTime)

  private[contest] case class MiniPlayer(id: Player.ID, userId: User.ID, no: Player.No, isWinner: Option[Boolean] = None, sign: Option[Sign] = None, absent: Option[Boolean] = None) {
    // absent -> player.quitOrKickOrManualAbsent = leave || quit || kick || manualAbsent
    def wins = isWinner getOrElse false
    def signed = sign.isDefined
  }

  private[contest] case class BoardWithPlayer(board: Board, player: Player)

  private[contest] sealed abstract class Outcome(val id: String, val name: String) {

    def isWin = this == Outcome.Win
  }
  private[contest] object Outcome {
    case object Win extends Outcome("win", "胜")
    case object Loss extends Outcome("loss", "负")
    case object Draw extends Outcome("draw", "和")
    case object Bye extends Outcome("bey", "轮空")
    case object NoStart extends Outcome("no-start", "没有移动")
    case object Leave extends Outcome("leave", "离开") // 超过最大NoStart次数-> Leave
    case object Quit extends Outcome("quit", "退赛")
    case object Kick extends Outcome("kick", "踢出")
    case object ManualAbsent extends Outcome("manual-absent", "弃权")
    case object Half extends Outcome("half", "半分轮空") // 棋手中途加入，之前的成绩就是Half

    val all = List(Win, Loss, Draw, Bye, NoStart, Leave, Quit, Kick, ManualAbsent, Half)

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Outcome = byId get id err s"Bad Outcome $id"

  }

  private[contest] sealed abstract class Result(val id: String, val name: String) {
    def whiteResult: String
    def blackResult: String
    def resultOf(color: Color): String
  }
  object Result {
    case object WhiteWin extends Result("1-0", "白方胜") {
      override def whiteResult = "1"
      override def blackResult = "0"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object BlackWin extends Result("0-1", "黑方胜") {
      override def whiteResult = "0"
      override def blackResult = "1"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object Draw extends Result("1/2-1/2", "平局") {
      override def whiteResult = "1/2"
      override def blackResult = "1/2"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object BlackAbsent extends Result("+-", "黑弃权") {
      override def whiteResult = "+"
      override def blackResult = "-"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object WhiteAbsent extends Result("-+", "白弃权") {
      override def whiteResult = "-"
      override def blackResult = "+"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }
    case object AllAbsent extends Result("--", "双弃权") {
      override def whiteResult = "-"
      override def blackResult = "-"
      override def resultOf(color: Color) = color.fold(whiteResult, blackResult)
    }

    val all = List(WhiteWin, BlackWin, Draw, BlackAbsent, WhiteAbsent, AllAbsent)

    def choice = all.map { r => r.id -> s"${r.id}（${r.name}）" }

    def keys = all.map(_.id).toSet

    def byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Result = byId get id err s"Bad Result $id"

  }

}
