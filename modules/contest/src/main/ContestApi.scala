package lila.contest

import akka.actor.{ ActorSelection, ActorSystem }
import lila.contest.Invite.InviteStatus
import lila.contest.Request.RequestStatus
import lila.db.{ DbFile, DbImage, FileUploader, Photographer }
import lila.game.{ Game, GameRepo }
import lila.hub.{ Duct, DuctMap }
import lila.notify.Notification.{ Notifies, Sender }
import lila.notify.{ Notification, NotifyApi }
import lila.user.{ User, UserRepo }
import lila.hub.lightTeam._
import lila.hub.lightClazz._
import lila.round.actorApi.round.ContestRoundStart
import org.joda.time.DateTime
import lila.clazz.ClazzApi
import lila.team.{ Campus, CampusRepo, MemberRepo, TeamRepo }
import scala.concurrent.duration._
import akka.pattern.ask
import lila.contest.Contest.Rule
import lila.game.actorApi.BoardWithPov
import lila.hub.actorApi.calendar.{ CalendarRemove, CalendarsRemove }
import lila.hub.actorApi.contest.{ ContestBoard, MiniBoard, MiniBoardPlayer }
import lila.hub.actorApi.relation.{ GetMark, GetMarks }
import makeTimeout.large

class ContestApi(
    system: ActorSystem,
    sequencers: DuctMap[_],
    renderer: ActorSelection,
    timeline: ActorSelection,
    verify: Condition.Verify,
    notifyApi: NotifyApi,
    clazzApi: ClazzApi,
    roundApi: RoundApi,
    roundMap: DuctMap[_],
    photographer: Photographer,
    fileUploader: FileUploader,
    reminder: ContestReminder,
    markActor: ActorSelection,
    asyncCache: lila.memo.AsyncCache.Builder,
    isOnline: lila.user.User.ID => Boolean,
    pairingDirector: PairingDirector
) {

  def verdicts(c: Contest, user: User, getUserTeamIds: User => Fu[TeamIdList], getUserClazzIds: User => Fu[ClazzIdList]): Fu[Condition.All.WithVerdicts] =
    verify(c, user, getUserTeamIds, getUserClazzIds)

  def byId(id: Contest.ID): Fu[Option[Contest]] = ContestRepo.byId(id)

  def fullContestBoard(gameId: Game.ID): Fu[Option[ContestBoard]] =
    fullBoardInfo(gameId).map {
      _.map { c =>
        ContestBoard(
          contestId = c.contest.id,
          contestFullName = c.contest.fullName,
          setup = c.contest.setup,
          isTeam = false,
          teamRated = c.contest.teamRated,
          board = MiniBoard(
            id = c.board.id,
            no = c.board.no,
            roundId = c.round.id,
            roundNo = c.round.no,
            pairedNo = None,
            status = c.board.status.id,
            startsAt = c.board.startsAt,
            whitePlayer = MiniBoardPlayer(
              no = c.board.whitePlayer.no,
              userId = c.board.whitePlayer.userId,
              teamerNo = None,
              signed = c.board.whitePlayer.signed
            ),
            blackPlayer = MiniBoardPlayer(
              no = c.board.blackPlayer.no,
              userId = c.board.blackPlayer.userId,
              teamerNo = None,
              signed = c.board.blackPlayer.signed
            )
          )
        )
      }
    }

  def fullBoardInfo(gameId: Game.ID): Fu[Option[Board.FullInfo]] =
    for {
      boardOption <- BoardRepo.byId(gameId)
      roundOption <- boardOption.?? { b => RoundRepo.byId(b.roundId) }
      contestOption <- boardOption.?? { b => byId(b.contestId) }
    } yield (boardOption |@| roundOption |@| contestOption).tupled map {
      case (board, round, contest) => Board.FullInfo(board, round, contest)
    }

  def roundGames(boardId: Game.ID, me: Option[User]): Fu[List[BoardWithPov]] =
    me.?? { u =>
      for {
        boardOption <- BoardRepo.byId(boardId)
        contestOption <- boardOption.?? { b => byId(b.contestId) }
        canView <- canViewGame(contestOption, u.id)
        boards <- boardOption.?? { b => canView.??(BoardRepo.getByRound(b.roundId)) }
        games <- canView.??(GameRepo.gamesFromSecondary(boards.map(_.id)))
      } yield boards zip games map {
        case (board, game) => BoardWithPov(MiniBoard(
          id = board.id,
          no = board.no,
          roundId = board.roundId,
          roundNo = board.roundNo,
          pairedNo = None,
          status = board.status.id,
          startsAt = board.startsAt,
          whitePlayer = MiniBoardPlayer(
            no = board.whitePlayer.no,
            userId = board.whitePlayer.userId,
            teamerNo = None,
            signed = board.whitePlayer.signed
          ),
          blackPlayer = MiniBoardPlayer(
            no = board.blackPlayer.no,
            userId = board.blackPlayer.userId,
            teamerNo = None,
            signed = board.blackPlayer.signed
          )
        ), lila.game.Pov.white(game))
      }
    }

  private def canViewGame(contest: Option[Contest], userId: User.ID) = {
    contest.?? { c =>
      c.typ match {
        case Contest.Type.Public | Contest.Type.TeamInner => MemberRepo.exists(c.organizer, userId)
        case Contest.Type.ClazzInner => clazzApi.belongTo(c.organizer, userId)
      }
    }
  }

  def contestBoard(game: Game): Fu[Option[Board]] = BoardRepo.byId(game.id)

  def create(contest: Contest, rounds: List[Round]): Fu[Contest] = {
    ContestRepo.insert(contest) >> RoundRepo.bulkInsert(rounds) inject contest
  }

  def createByClazz(clazzId: String, courseId: String, user: User, students: List[User.ID], data: ClazzContest): Fu[Contest] = {
    val contest = data.toContest(clazzId, courseId, user)
    val round = Round.make(
      no = 1,
      contestId = contest.id,
      startsAt = contest.startsAt
    )

    //val shuffleStudents = Random.shuffle(students)
    ContestRepo.insert(contest) >>
      RoundRepo.insert(round) >>
      publish(contest) >> PlayerRepo.getByContest(contest.id).flatMap { oldMembers =>
        setPlayers(contest.copy(status = Contest.Status.Published), oldMembers, students)
      } >>- setEnterStop(contest.id) inject contest
  }

  def update(old: Contest, c: Contest, rounds: List[Round]): Funit =
    ContestRepo.update(
      old.copy(
        name = c.name,
        groupName = c.groupName,
        logo = c.logo,
        typ = c.typ,
        organizer = c.organizer,
        variant = c.variant,
        position = c.position,
        mode = c.mode,
        clock = c.clock,
        rule = c.rule,
        teamRated = c.teamRated,
        startsAt = c.startsAt,
        finishAt = c.finishAt,
        deadline = c.deadline,
        deadlineAt = c.deadlineAt,
        maxPlayers = c.maxPlayers,
        minPlayers = c.minPlayers,
        conditions = c.conditions,
        roundSpace = c.roundSpace,
        rounds = c.rounds,
        swissBtss = c.swissBtss,
        roundRobinBtss = c.roundRobinBtss,
        canLateMinute = c.canLateMinute,
        canQuitNumber = c.canQuitNumber,
        enterApprove = c.enterApprove,
        autoPairing = c.autoPairing,
        selfQuit = c.selfQuit,
        enterCost = c.enterCost,
        hasPrizes = c.hasPrizes,
        description = c.description,
        attachments = c.attachments,
        enterShow = c.enterShow,
        meta = c.meta
      )
    ) >> RoundRepo.bulkUpdate(old.id, rounds).void

  def remove(contest: Contest): Funit = {
    lg(contest, none, "删除比赛", none)
    ContestRepo.remove(contest.id) >> RoundRepo.removeByContest(contest.id)
  }

  def publish(contest: Contest): Funit = {
    lg(contest, none, "发布比赛", none)
    ContestRepo.setStatus(contest.id, Contest.Status.Published)
  }

  def cancel(contest: Contest): Funit = {
    lg(contest, none, "取消比赛", none)
    ContestRepo.setStatus(contest.id, Contest.Status.Canceled) >>
      finishNotify(contest) >>
      removeCoachCalendar(contest) >>
      removeCalendar(contest)
  }

  def joinRequest(contest: Contest, request: Request, user: User): Funit = {
    lg(contest, none, "加入比赛申请", s"棋手：${user.id}".some)
    RequestRepo.insert(request) >> !contest.enterApprove ?? {
      processRequest(contest, request, true, user)
    }
  }

  def processRequest(c: Contest, request: Request, accept: Boolean, user: User): Funit = {
    lg(c, none, "加入比赛处理", s"棋手：${user.id}，Accept: $accept".some)
    RequestRepo.setStatus(request.id, RequestStatus.applyByAccept(accept)) >> {
      if (accept) {
        teamRating(c, user) flatMap { teamRating =>
          RoundRepo.byId(Round.makeId(c.id, c.currentRound)) flatMap { roundOption =>
            roundOption.fold(funit) { round =>
              PlayerRepo.findNextNo(c.id) flatMap { no =>
                PlayerRepo.insert(Player.make(
                  contestId = request.contestId,
                  no = no,
                  user = user,
                  perfLens = c.perfLens,
                  teamRating = teamRating,
                  currentRound = c.currentRound,
                  currentRoundOverPairing = round.isOverPairing
                )) >> ContestRepo.incPlayers(c.id, +1) >>- acceptNotify(c, request)
              }
            }
          }
        }
      } else funit
    }
  }

  def requestsWithUsers(c: Contest, markMap: Map[String, Option[String]]): Fu[List[RequestWithUser]] = for {
    requests ← RequestRepo.getByContest(c.id)
    users ← UserRepo usersFromSecondary requests.map(_.userId)
  } yield requests zip users map {
    case (request, user) => {
      val mark = markOption(user.id, markMap)
      RequestWithUser(request, user, mark)
    }
  }

  def invite(c: Contest, invite: Invite): Funit = {
    lg(c, none, "邀请加入比赛", s"棋手：${invite.userId}".some)
    InviteRepo.insert(invite) >>- inviteNotify(c, invite)
  }

  def processInvite(c: Contest, invite: Invite, accept: Boolean, user: User): Funit = {
    lg(c, none, "邀请加入比赛处理", s"棋手：${invite.userId}，Accept: $accept".some)
    InviteRepo.setStatus(invite.id, InviteStatus.applyByAccept(accept)) >> {
      if (accept) {
        teamRating(c, user) flatMap { teamRating =>
          RoundRepo.byId(Round.makeId(c.id, c.currentRound)) flatMap { roundOption =>
            roundOption.fold(funit) { round =>
              PlayerRepo.findNextNo(c.id) flatMap { no =>
                PlayerRepo.insert(
                  Player.make(
                    contestId = invite.contestId,
                    no = no,
                    user = user,
                    perfLens = c.perfLens,
                    teamRating = teamRating,
                    currentRound = c.currentRound,
                    currentRoundOverPairing = round.isOverPairing
                  )
                ) >> ContestRepo.incPlayers(c.id, +1)
              }
            }
          }
        }
      } else funit
    }
  }

  private def teamRating(c: Contest, user: User): Fu[Option[Int]] = {
    {
      c.typ match {
        case Contest.Type.Public | Contest.Type.TeamInner => MemberRepo.byId(c.organizer, user.id) map (_.??(_.rating.map(_.intValue)))
        case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
          _.?? { clazz =>
            clazz.team.fold(fuccess(none[Int])) { campusId =>
              MemberRepo.byId(Campus.toTeamId(campusId), user.id) map (_.??(_.rating.map(_.intValue)))
            }
          }
        }
      }
    }
  }

  def inviteWithUsers(c: Contest, markMap: Map[String, Option[String]]): Fu[List[InviteWithUser]] = for {
    invites ← InviteRepo.getByContest(c.id)
    users ← UserRepo usersFromSecondary invites.map(_.userId)
  } yield invites zip users map {
    case (invite, user) => {
      val mark = markOption(user.id, markMap)
      InviteWithUser(invite, user, mark)
    }
  }

  def teamClazzs(c: Contest): Fu[List[(String, String)]] = {
    c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => TeamRepo.byId(c.organizer) flatMap { team =>
        team.?? {
          _.clazzIds.?? { clazzIds =>
            clazzApi.byIds(clazzIds).map { clazzs =>
              clazzs.filterNot(_.deleted | false) map (c => c.id -> c.name)
            }
          }
        }
      }
      case Contest.Type.ClazzInner => fuccess(List.empty[(String, String)])
    }
  }

  def teamTags(c: Contest): Fu[List[lila.team.Tag]] = {
    c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => lila.team.TagRepo.findByTeam(c.organizer)
      case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            lila.team.TagRepo.findByTeam(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def team(c: Contest): Fu[Option[lila.team.Team]] = {
    c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => TeamRepo.byId(c.organizer)
      case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            TeamRepo.byId(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def teamCampus(c: Contest): Fu[List[lila.team.Campus]] = {
    c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => CampusRepo.byTeam(c.organizer)
      case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            CampusRepo.byTeam(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def playersWithUsers(me: User, c: Contest): Fu[List[PlayerWithUser]] = for {
    players ← PlayerRepo.getByContest(c.id)
    users ← UserRepo usersFromSecondary players.map(_.userId)
    markMap ← userMarks(me.id)
    members ← c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(c.organizer, players.map(_.userId))
      case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.fold(fuccess(players.map(_ => none[lila.team.Member]))) { campusId =>
            MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), players.map(_.userId))
          }
        }
      }
    }
  } yield players zip users zip members map {
    case ((player, user), member) => {
      val mark = markOption(user.id, markMap)
      PlayerWithUser(player, user, member, mark)
    }
  }

  def allPlayersWithUsers(me: User, c: Contest): Fu[List[AllPlayerWithUser]] =
    c.typ match {
      case Contest.Type.Public | Contest.Type.TeamInner => for {
        members <- MemberRepo.memberByTeam(c.organizer)
        users <- UserRepo usersFromSecondary members.map(_.user)
        markMap ← userMarks(me.id)
      } yield users zip members map {
        case (user, member) => {
          val mark = markOption(user.id, markMap)
          AllPlayerWithUser(user, member.some, mark)
        }
      }
      case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          for {
            userIds <- fuccess(clazz.studentIds)
            users <- UserRepo usersFromSecondary userIds
            markMap ← userMarks(me.id)
            members <- clazz.team.fold(fuccess(userIds.map(_ => none[lila.team.Member]))) { campusId =>
              MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), userIds)
            }
          } yield users zip members map {
            case (user, member) => {
              val mark = markOption(user.id, markMap)
              AllPlayerWithUser(user, member, mark)
            }
          }
        }
      }
    }

  private def markOption(userId: User.ID, markMap: Map[String, Option[String]]): Option[String] = {
    markMap.get(userId).fold(none[String]) { m => m }
  }

  def setPlayers(c: Contest, oldMembers: List[Player], userIds: List[String]): Funit = {
    lg(c, none, "添加棋手", s"棋手：$userIds".some)
    (for {
      roundOption ← RoundRepo.byId(Round.makeId(c.id, c.currentRound))
      nextNo ← PlayerRepo.findNextNo(c.id)
      oldUserIds = oldMembers.map(_.userId)
      addUserIds = userIds.filterNot(u => oldUserIds.contains(u))
      users ← UserRepo usersFromSecondary userIds
      members ← c.typ match {
        case Contest.Type.Public | Contest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(c.organizer, userIds)
        case Contest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
          _.?? { clazz =>
            clazz.team.fold(fuccess(userIds.map(_ => none[lila.team.Member]))) { campusId =>
              MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), userIds)
            }
          }
        }
      }
    } yield ((((((roundOption, nextNo), oldMembers), oldUserIds), addUserIds), users), members)).flatMap {
      case ((((((roundOption, nextNo), oldMembers), oldUserIds), addUserIds), users), members) => {
        roundOption.?? { round =>
          val players = (members zip users).zipWithIndex.map {
            case ((memberOption, user), index) => {
              Player.make(
                contestId = c.id,
                no = if (c.isPublished) { index + 1 } else { nextNo + index },
                user = user,
                perfLens = c.perfLens,
                teamRating = memberOption.??(_.rating.map(_.intValue)),
                currentRound = c.currentRound,
                currentRoundOverPairing = round.isOverPairing
              )
            }
          }

          if (c.isPublished) {
            PlayerRepo.bulkUpdate(c.id, players) >> ContestRepo.setPlayers(c.id, userIds.size) >> joinNotify(addUserIds, c)
          } else if (c.isStarted || c.isEnterStopped) {
            PlayerRepo.bulkInsert(players) >> ContestRepo.incPlayers(c.id, players.size) >> joinNotify(userIds, c)
          } else funit
        }
      }
    }
  }

  private def joinNotify(userIds: List[User.ID], contest: Contest): Funit = {
    userIds.map { userId =>
      notifyApi.addNotification(Notification.make(
        Sender(User.lichessId).some,
        Notifies(userId),
        lila.notify.GenericLink(
          url = s"/contest/${contest.id}",
          title = "加入比赛".some,
          text = s"您已加入比赛【${contest.fullName}】".some,
          icon = "赛"
        )
      ))
    }.sequenceFu
    funit
  }

  def removePlayer(contest: Contest, playerId: Player.ID): Funit = {
    lg(contest, none, "删除棋手", s"棋手：$playerId".some)
    for {
      _ <- InviteRepo.remove(playerId)
      _ <- RequestRepo.remove(playerId)
      _ <- PlayerRepo.remove(playerId)
      _ <- reorderPlayer(contest.id)
      res <- ContestRepo.incPlayers(contest.id, -1)
    } yield res
  }

  private def reorderPlayer(id: Contest.ID): Funit =
    PlayerRepo.getByContest(id) flatMap { players =>
      players.zipWithIndex.map {
        case (p, i) => PlayerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  def reorderPlayerByPlayerIds(playerIds: List[String]): Funit =
    PlayerRepo.byOrderedIds(playerIds) flatMap { players =>
      players.zipWithIndex.map {
        case (p, i) => PlayerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  def toggleKickPlayer(contest: Contest, round: Round, player: Player): Funit =
    player.absentOr match {
      case true => {
        lg(contest, round.some, "棋手恢复比赛", s"棋手：${player.userId}，state：[absent-${player.absent},leave-${player.leave},quit-${player.quit},kick-${player.kick},manualAbsent-${player.manualAbsent}]".some)
        PlayerRepo.unkick(player.id, player.manualAbsent)
      }
      case false => {
        lg(contest, round.some, "棋手禁止比赛", s"棋手：${player.userId}，state：[absent-${player.absent},leave-${player.leave},quit-${player.quit},kick-${player.kick},manualAbsent-${player.manualAbsent}]".some)
        PlayerRepo.kick(player.id)
      }
    }

  def quit(contest: Contest, round: Round, userId: User.ID): Funit = {
    lg(contest, round.some, "棋手退赛", s"棋手：$userId".some)
    PlayerRepo.quit(contest.id, userId)
  }

  def quitByUser(userId: User.ID): Funit =
    PlayerRepo.getByUserIdNear1Year(userId) flatMap { players =>
      ContestRepo.byIds(players.map(_.contestId)) flatMap { contests =>
        contests.filter(_.quitable).map { contest =>
          PlayerRepo.quit(contest.id, userId)
        }.sequenceFu.void
      }
    }

  private def inviteNotify(c: Contest, invite: Invite): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(c.createdBy).some,
      Notifies(invite.userId),
      lila.notify.GenericLink(
        url = s"/contest/${c.id}",
        title = "参赛邀请".some,
        text = s"您被邀请参加【${c.fullName}】比赛".some,
        icon = "赛"
      )
    ))
  }

  private def acceptNotify(c: Contest, request: Request): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(User.lichessId).some,
      Notifies(request.userId),
      lila.notify.GenericLink(
        url = s"/contest/${c.id}",
        title = "参赛申请通过".some,
        text = s"您已经通过【${c.fullName}】的参赛申请，请提前 ${c.canLateMinute} 分钟进入比赛".some,
        icon = "赛"
      )
    ))
  }

  private def finishNotify(c: Contest): Funit = {
    PlayerRepo.getByContest(c.id) map { players =>
      players.foreach { player =>
        notifyApi.addNotification(Notification.make(
          Sender(User.lichessId).some,
          Notifies(player.userId),
          lila.notify.GenericLink(
            url = s"/contest/${c.id}",
            title = "比赛结束".some,
            text = s"比赛【${c.fullName}】已经结束".some,
            icon = "赛"
          )
        ))
      }
    }
  }

  def enterStop: Funit =
    ContestRepo.published map { contests =>
      contests foreach { contest =>
        if (contest.shouldEnterStop) {
          setEnterStop(contest.id)
        }
      }
    }

  def start: Funit =
    ContestRepo.enterStopped map { contests =>
      contests foreach { contest =>
        if (contest.shouldStart) {
          setStart(contest.id)
        }
      }
    }

  def finishGame(game: Game) =
    if (!game.contestIsTeamOrFalse) {
      game.contestId.?? { id =>
        Sequencing(id)(byId) { contest =>
          val wp = game.whitePlayer.userId err s"contest game miss player userId $game"
          val bp = game.blackPlayer.userId err s"contest game miss player userId $game"
          BoardRepo.byId(game.id) flatMap {
            _.?? { board =>
              lg(contest, None, "对局结束", s"第${board.roundNo}轮 - #${board.no} ${board.whitePlayer.userId} Vs. ${board.blackPlayer.userId}".some)
              val currentRoundNo = contest.currentRound
              val currentRoundId = Round.makeId(contest.id, currentRoundNo)

              val result = boardResult(contest, game, board)
              val whiteOutcome = roundApi.whitePlayerOutcome(result)
              val blackOutcome = roundApi.blackPlayerOutcome(result)
              val winner = roundApi.boardWinner(result)

              for {
                _ <- BoardRepo.finishGame(game, winner, result)
                _ <- PlayerRepo.finishGame(contest, board.roundNo, wp, whiteOutcome)
                _ <- PlayerRepo.finishGame(contest, board.roundNo, bp, blackOutcome)
                roundOption <- RoundRepo.byId(currentRoundId)
                isAllBoardFinished <- roundOption.?? { round => BoardRepo.allFinished(round.id, round.boards) }
                _ <- isAllBoardFinished.?? { RoundRepo.finish(currentRoundId) }
                contest <- if (isAllBoardFinished && contest.autoPairing) {
                  roundApi.publishResult(contest, currentRoundId, currentRoundNo)
                } else fuccess(contest)
                _ <- contest.allRoundFinished.?? {
                  contest.autoPairing.?? {
                    system.scheduler.scheduleOnce(3 seconds) {
                      publishScoreAndFinish(contest)
                    }
                    funit
                  }
                }
                res <- isAllBoardFinished.?? {
                  toNextRound(contest, currentRoundNo + 1)
                }
              } yield res
            }
          }
        }
      }
    }

  private def boardResult(contest: Contest, game: Game, board: Board): Board.Result = {
    val whiteSigned = board.whitePlayer.signed
    val blackSigned = board.blackPlayer.signed
    val whiteAbsent = board.whitePlayer.absent | false
    val blackAbsent = board.blackPlayer.absent | false

    if (whiteSigned && blackSigned) {
      game.winnerColor.map {
        case chess.Color.White => Board.Result.WhiteWin
        case chess.Color.Black => Board.Result.BlackWin
      } | Board.Result.Draw
    } else if ((whiteSigned && !blackSigned) || (contest.isRoundRobin && blackAbsent && !whiteAbsent)) {
      Board.Result.BlackAbsent
    } else if ((!whiteSigned && blackSigned) || (contest.isRoundRobin && whiteAbsent && !blackAbsent)) {
      Board.Result.WhiteAbsent
    } else if ((!whiteSigned && !blackSigned) || (contest.isRoundRobin && whiteAbsent && blackAbsent)) {
      Board.Result.AllAbsent
    } else {
      logger.error(s"can not apply board result $contest ${board.id}")
      Board.Result.AllAbsent
    }
  }

  private def toNextRound(contest: Contest, no: Round.No) =
    (!contest.isFinishedOrCanceled && !contest.allRoundFinished).?? {
      nextRound(contest, no, publishScoreAndFinish)
    }

  private def nextRound(contest: Contest, no: Round.No, publishScoreAndFinish: Contest => Funit): Funit = {
    contest.autoPairing.?? {
      RoundRepo.byId(Round.makeId(contest.id, no)).flatMap { roundOption =>
        roundOption.?? { r =>
          lg(contest, r.some, "进入下一轮", none)
          delayNextRound(contest, r) map { nr =>
            pairingByRule(contest, nr, publishScoreAndFinish)
          }
        }
      }
    }
  }

  private def pairingByRule(contest: Contest, round: Round, publishScoreAndFinish: Contest => Funit): Funit =
    contest.rule match {
      case Rule.RoundRobin | Rule.DBRoundRobin => {
        if (round.isFirstRound()) {
          pairingRoundRobin(contest).void
        } else {
          contest.autoPairing.?? {
            roundApi.publish(contest, round)
          }
        }
      }
      case Rule.Swiss | Rule.Random => {
        roundApi.pairing(contest, round, publishScoreAndFinish).void
      }
    }

  private def delayNextRound(contest: Contest, round: Round): Fu[Round] = {
    val now = DateTime.now.withSecondOfMinute(0).withMillisOfSecond(0)
    if (round.actualStartsAt.isBefore(now.plusMinutes(Round.beforeStartMinutes))) {
      lg(contest, round.some, "轮次推迟执行", none)
      val st = now.plusMinutes(Round.beforeStartMinutes)
      RoundRepo.setStartsTime(round.id, st) >> {
        BoardRepo.setStartsTimes(round.id, st)
      } inject round.copy(actualStartsAt = st)
    } else fuccess(round)
  }

  def publishScoreAndFinish(contest: Contest): Funit = {
    lg(contest, none, "比赛结束", none)
    computeAllScoreWithoutCancelled(contest) >>
      ContestRepo.finish(contest) >>
      finishNotify(contest)
  }

  private def computeAllScoreWithoutCancelled(contest: Contest): Funit = {
    ScoreSheetRepo.getByContest(contest.id) flatMap { scoreSheets =>
      scoreSheets.exists(_.cancelled).?? {
        scoreSheets.groupBy(_.roundNo).map {
          case (no, list) => {
            val lst = list.map(d => d.playerUid -> d.rank)
            lg(contest, none, "计算成绩", s"第${no}轮：$lst".some)
            list.filterNot(_.cancelled).zipWithIndex.map {
              case (s, i) => ScoreSheetRepo.setRank(s.id, i + 1)
            }.sequenceFu.void
          }
        }.sequenceFu.void >> ScoreSheetRepo.setCancelledRank(contest.id)
      }
    }
  }

  def cancelScore(contest: Contest, scoreSheet: ScoreSheet): Funit = {
    lg(contest, none, "取消比赛成绩", s"棋手：${scoreSheet.playerUid}".some)
    ScoreSheetRepo.setCancelScore(scoreSheet) >>
      PlayerRepo.setCancelScore(Player.makeId(scoreSheet.contestId, scoreSheet.playerUid))
  }

  def setEnterStop(id: Contest.ID): Unit =
    Sequencing(id)(ContestRepo.publishedById) { contest =>
      if (contest.autoPairing && contest.nbPlayers < contest.minPlayers) {
        lg(contest, none, "比赛取消", none)
        ContestRepo.setStatus(id, Contest.Status.Canceled)
      } else {
        lg(contest, none, "比赛报名截止", none)
        ContestRepo.setStatus(id, Contest.Status.EnterStopped) >>- contest.autoPairing.?? {
          RoundRepo.byId(Round.makeId(id, 1)) flatMap {
            case None => {
              ContestRepo.setStatus(id, Contest.Status.Canceled) >> fufail(s"can not find first round of $contest")
            }
            case Some(r) => {
              pairingByRule(contest.copy(status = Contest.Status.EnterStopped), r, publishScoreAndFinish)
            }
          }
        }
      }
    }

  def setStart(id: Contest.ID): Unit =
    Sequencing(id)(ContestRepo.enterStoppedById) { contest =>
      lg(contest, none, "比赛开始", none)
      ContestRepo.setStatus(id, Contest.Status.Started)
    }

  def launch: Funit = {
    launchBoards.flatMap { list =>
      val gameIds = list.filter(_.canStarted).map(_.board.id)
      gameIds.nonEmpty.?? {
        val now = DateTime.now
        for {
          _ <- GameRepo.gameStartBatch(gameIds, now)
          _ <- BoardRepo.gameStartBatch(gameIds, now)
        } yield {
          gameIds.foreach { gameId =>
            roundMap.tell(gameId, ContestRoundStart(now))
          }
        }
      }
    }
  }

  def launchBoards: Fu[List[Board.FullInfo]] = for {
    boards ← BoardRepo.pending
    rounds ← boards.nonEmpty ?? { RoundRepo.byOrderedIds(boards.map(_.roundId)) }
    contests <- boards.nonEmpty ?? { ContestRepo.byOrderedIds(boards.map(_.contestId)) }
  } yield boards zip rounds zip contests map {
    case ((board, round), contest) => Board.FullInfo(board, round, contest)
  }

  def remind: Funit =
    remindBoards map { list =>
      list.foreach { info =>
        info.board.players.foreach { player =>
          if (!info.contest.isFinishedOrCanceled && (info.round.isPublished || info.round.isStarted)) {
            if (!info.board.appt || (info.board.appt && info.board.apptComplete)) {
              BoardRepo.setReminded(info.board.id) >>-
                reminder(
                  info,
                  player.userId
                )
            }
          }
        }
      }
    }

  def remindBoards: Fu[List[Board.FullInfo]] = for {
    boards ← BoardRepo.remindAtSoon
    rounds ← boards.nonEmpty ?? { RoundRepo.byOrderedIds(boards.map(_.roundId)) }
    contests <- boards.nonEmpty ?? { ContestRepo.byOrderedIds(boards.map(_.contestId)) }
  } yield boards zip rounds zip contests map {
    case ((board, round), contest) => Board.FullInfo(board, round, contest)
  }

  def setAutoPairing(contest: Contest, auto: Boolean): Funit = {
    lg(contest, none, "比赛自动设置", s"Auto：$auto".some)
    ContestRepo.setAutoPairing(contest.id, auto) flatMap { _ =>
      if (auto) {
        RoundRepo.byId(Round.makeId(contest.id, contest.currentRound)).flatMap { roundOption =>
          roundOption.?? { round =>
            val c = contest.copy(autoPairing = true)
            round.status match {
              case Round.Status.Created => (c.isEnterStopped || c.isStarted).?? { delayNextRound(c, round).map { nr => pairingByRule(c, nr, publishScoreAndFinish) } }
              case Round.Status.Pairing => (c.isEnterStopped || c.isStarted).?? { delayNextRound(c, round).map { nr => roundApi.publish(c, nr) } }
              case Round.Status.Published | Round.Status.Started => funit
              case Round.Status.Finished => c.isStarted.?? {
                roundApi.publishResult(c, round.id, round.no).flatMap { nc =>
                  if (nc.allRoundFinished) {
                    publishScoreAndFinish(contest)
                  } else toNextRound(nc, c.currentRound + 1)
                }
              }
              case Round.Status.PublishResult => c.isStarted.?? {
                if (c.allRoundFinished) {
                  publishScoreAndFinish(c)
                } else toNextRound(c, c.currentRound + 1)
              }
            }
          }
        }
      } else funit
    }
  }

  def setRoundRobin(contest: Contest, list: List[DateTime]): Funit = {
    lg(contest, none, "循环赛设置", s"Rounds：${list.size}".some)
    val rounds = list.zipWithIndex map {
      case (startsAt, i) => Round.make(
        no = i + 1,
        contestId = contest.id,
        startsAt = startsAt
      )
    }

    ContestRepo.setRounds(contest.id, rounds.size) >>
      RoundRepo.bulkUpdate(contest.id, rounds)
  }

  def pairingRoundRobin(contest: Contest): Fu[Boolean] = {
    lg(contest, none, "循环赛编排", none)
    (for {
      rounds <- RoundRepo.getByContest(contest.id)
      players <- PlayerRepo.getByContest(contest.id)
      firstRound = rounds.minBy(_.no)
    } yield (rounds, players, firstRound)) flatMap {
      case (rounds, players, firstRound) => {
        pairingDirector.pairingAll(contest, rounds.take(contest.actualRound), players).flatMap {
          case None => {
            lg(contest, none, "循环赛编排失败", none, true)
            roundApi.pairingFailed(contest, firstRound, publishScoreAndFinish).inject(false)
          }
          case Some(boards) => {
            if (boards.isEmpty) {
              lg(contest, none, "循环赛编排失败", "boards.isEmpty -> force finished!".some, true)
              roundApi.pairingFailed(contest, firstRound, publishScoreAndFinish).inject(false)
            } else {
              val bs = boards.map(_.playerNos)
              lg(contest, none, "循环赛编排完成", s"$bs".some)
              ContestRepo.setRoundRobinPairing(contest.id) >> contest.autoPairing.?? {
                PlayerRepo.getByContest(contest.id) flatMap { players =>
                  roundApi.autoPublish(contest, firstRound, boards, players)
                }
              }.inject(true)
            }
          }
        }
      }
    }
  }

  //  private val championTop5Cache = asyncCache.single[List[(Contest, User.ID)]](
  //    name = "contest.championTop5",
  //    f = {
  //      for {
  //        contests <- ContestRepo.findRecently(5)
  //        rounds <- ScoreSheetRepo.findChampion(contests.map(_.id))
  //      } yield {
  //        contests.map { contest =>
  //          contest -> rounds.find(_._1 == contest.id).map(_._2)
  //        }.map(d => d._1 -> (d._2 | "-"))
  //      }
  //    },
  //    expireAfter = _.ExpireAfterWrite(30 minute)
  //  )

  def championTop5(user: Option[User]): Fu[List[(Contest, User.ID)]] = {
    for {
      contests <- {
        user match {
          case Some(u) => ContestRepo.findRecently(5, u.belongTeamId)
          case None => fuccess(Nil)
        }
      }
      rounds <- ScoreSheetRepo.findChampion(contests.map(_.id))
    } yield {
      contests.map { contest =>
        contest -> rounds.find(_._1 == contest.id).map(_._2)
      }.map(d => d._1 -> (d._2 | "-"))
    }
  }

  private def Sequencing(contestId: Contest.ID)(fetch: Contest.ID => Fu[Option[Contest]])(run: Contest => Funit): Unit =
    doSequence(contestId) {
      fetch(contestId) flatMap {
        case Some(t) => run(t)
        case None => fufail(s"Can't run sequenced operation on missing contest $contestId")
      }
    }

  private def doSequence(contestId: Contest.ID)(fu: => Funit): Unit =
    sequencers.tell(contestId, Duct.extra.LazyFu(() => fu))

  def uploadPicture(id: String, picture: Photographer.Uploaded, processFile: Boolean = false): Fu[DbImage] =
    photographer(id, picture, processFile)

  def uploadFile(id: String, file: FileUploader.Uploaded): Fu[DbFile] =
    fileUploader(id, file)

  private def removeCalendar(contest: Contest): Funit = {
    (!contest.appt).?? {
      RoundRepo.getByContest(contest.id) flatMap { rounds =>
        val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round ${contest.id}-${contest.currentRound}"
        round.isPublished.?? {
          BoardRepo.getByRound(round.id) map { boards =>
            val ids = boards.flatMap { board => board.players.map { player => s"${board.id}@${player.userId}" } }
            system.lilaBus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
          }
        }
      }
    }
  }

  private def removeCoachCalendar(contest: Contest): Funit = {
    RoundRepo.getByContest(contest.id) map { rounds =>
      val round = rounds.find { r => r.no == contest.currentRound } err s"can not find round ${contest.id}-${contest.currentRound}"
      system.lilaBus.publish(CalendarRemove(s"${round.id}@${contest.createdBy}"), 'calendarRemoveBus)
    }
  }

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  def userMark(userId: User.ID, relUserId: String): Fu[Option[String]] =
    (markActor ? GetMark(userId, relUserId)).mapTo[Option[String]]

  private def lg(contest: Contest, round: Option[Round], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
