package lila.contest

import PairingSystem.{ Bye, ByeOrPending, Pending }
import scala.util.Random

final class RandomPairing extends Pairing {

  override def pairing(contest: Contest): Fu[List[ByeOrPending]] = {
    for {
      players <- PlayerRepo.getByContest(contest.id)
      forbiddens <- ForbiddenRepo.getByContest(contest.id)
    } yield {
      val absentPlayers = players.filter(_.absent).map(_.no)
      val unAbsentPlayers = players.filterNot(_.absent).map(_.no)
      println(s"==============RandomPairing Players $contest 第${contest.currentRound}轮 ================")
      println(unAbsentPlayers, absentPlayers)
      val result = Random.shuffle(unAbsentPlayers)
        .grouped(2)
        .map { g =>
          g.length match {
            case 2 => Right(Pending(g.head, g.reverse.head))
            case 1 => Left(Bye(g.head))
          }
        } toList

      println(s"==============RandomPairing PairResult $contest 第${contest.currentRound}轮 ================")
      result.foreach { p => println(p) }
      result
    }
  }

  override def pairingAll(contest: Contest, players: List[Player]): Fu[Map[Int, List[ByeOrPending]]] =
    fuccess(Map.empty[Int, List[ByeOrPending]])

  private def forbiddenPairs(players: List[Player], forbiddens: List[Forbidden]): Set[(Player.No, Player.No)] = {
    val playerMap = players.map(p => p.id -> p.no).toMap
    forbiddens.flatMap { forbidden =>
      forbidden.pairs(players).map {
        case (p1, p2) => (playerMap.get(p1) err s"can not find player $p1") -> (playerMap.get(p2) err s"can not find player $p2")
      }
    }.toSet
  }

}
