package lila.contest

import PairingSystem.ByeOrPending

trait Pairing {

  def pairing(contest: Contest): Fu[List[ByeOrPending]]

  def pairingAll(contest: Contest, players: List[Player]): Fu[Map[Int, List[ByeOrPending]]]

}
