package lila.member

import lila.user.User
import lila.user.MemberLevel
import org.joda.time.DateTime
import ExchangeCard._

case class ExchangeCard(
    _id: ID,
    code: String,
    batch: ExchangeBatch.ID,
    level: MemberLevel,
    days: Days,
    expireAt: DateTime,
    remark: Option[String],
    status: CardStatus,
    createAt: DateTime,
    createBy: User.ID,
    usedBy: Option[User.ID],
    usedAt: Option[DateTime],
    activityName: Option[DateTime]
) {

  def id = _id

  def expired = expireAt.isBeforeNow

  def isAvailable = isCreate && !expired

  def isCreate = status == CardStatus.Create

  def isUsed = status == CardStatus.Used

  def useIn1Year = usedAt.?? { _.plusMonths(11).isAfterNow }

  def desc = s"${level.name}（${days.name}）"

}

object ExchangeCard {

  type ID = Int

  sealed abstract class CardStatus(val id: String, val name: String)
  object CardStatus {
    case object Create extends CardStatus("create", "待使用")
    case object Used extends CardStatus("used", "已使用")
    case object Expired extends CardStatus("expired", "已过期")
    val all = List(Create, Used, Expired)
    def apply(id: String): CardStatus = all.find(_.id == id) err s"can not find CardStatus $id"
    def choices = all.map(d => d.id -> d.name)
  }

}

