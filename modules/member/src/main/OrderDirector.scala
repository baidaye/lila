package lila.member

import lila.user.User
import lila.user.MemberLevel
import org.joda.time.DateTime

object OrderDirector {

  val zero = BigDecimal(0.00)
  val yearDays = 365
  val monthDays = 30

  def calcPrice(data: CalcPriceData, existsInviteOrder: Boolean, user: User) = {
    val p = lockProductItem(data)
    val product = p._1
    val item = p._2
    val price = item.price

    val totalPrice = price * data.count

    // 1.白银会员抵扣金额（购买会员卡）
    var silverDaysAmount = zero
    var priceAfterSilver = totalPrice
    var silverDays = 0
    if (item.isSilverMember && Product.toMemberLevel(data.productId) == lila.user.MemberLevel.Gold) {
      silverDays = calcSilverDays(item, user)
      silverDaysAmount = silverDays * MemberLevel.Silver.prices.day
      priceAfterSilver = (totalPrice - silverDaysAmount).max(zero)
    }

    // 2.优惠金额
    val promotions = OrderPromotions.of(item.promotions, priceAfterSilver, data.count, user)
    val priceAfterPromotions = promotions.outPrice(priceAfterSilver)

    // 3.邀请码金额(一年:50/30, 6个月:25/15, 3个月:12.5/7.5)
    var inviteUserAmount = zero
    var priceAfterInviteUserAmount = priceAfterPromotions
    if (data.inviteUser.isDefined && item.isInviteUser && !existsInviteOrder) {
      inviteUserAmount = {
        val level = item.attrs.get("level") err "cant get level"
        val max = if (level == MemberLevel.Gold.code) {
          BigDecimal(50.00)
        } else if (level == MemberLevel.Silver.code) {
          BigDecimal(30.00)
        } else zero

        //--------------------------------------------
        val attr = item.attrs.get("days") | ""
        if (attr.contains("year")) max
        else if (attr.contains("month")) {
          val month = attr.replace("month", "").toInt
          max / 12 * month
        } else zero
      }
      priceAfterInviteUserAmount = priceAfterPromotions - inviteUserAmount
    }

    // 4.积分金额
    val maxPoints = priceAfterInviteUserAmount.min(BigDecimal(user.memberOrDefault.points)).intValue()
    var points = 0
    var priceAfterPoints = priceAfterInviteUserAmount
    if (item.isPoint) {
      val fillPoints = data.points | 0
      if (data.isPointsChange | false) {
        points = math.min(maxPoints, fillPoints)
      } else {
        points = maxPoints
      }
      priceAfterPoints = (priceAfterInviteUserAmount - points).max(zero)
    }

    // 5.抹零 集采会员卡 使用积分后 还需要再支付几毛钱 直接抹掉
    var priceAfterSmallChange = priceAfterPoints
    var smallChangeAmount = zero
    if (item.isSmallChange) {
      if (priceAfterPoints > zero && priceAfterPoints < BigDecimal(1.0)) {
        priceAfterSmallChange = zero
        smallChangeAmount = priceAfterPoints
      }
    }

    // 计算折后单价
    val singlePromotions = OrderPromotions.of(item.promotions, price, data.count, user)
    val discountSinglePrice = singlePromotions.outPrice(price)

    OrderAmountInfo(
      amounts = OrderAmounts(
        discountPrice = discountSinglePrice.some,
        silverDays = if (silverDays == 0) None else silverDays.some,
        silverDaysAmount = if (silverDays == 0) None else silverDaysAmount.some,
        promotions = promotions,
        points = if (points == 0) None else points.some,
        pointsAmount = if (points == 0) None else BigDecimal(points).some,
        coupon = None,
        couponAmount = None,
        inviteUser = data.inviteUser,
        inviteUserAmount = data.inviteUser.map(_ => inviteUserAmount),
        smallChangeAmount = smallChangeAmount.some
      ),
      count = data.count,
      price = price,
      totalPrice = totalPrice,
      payPrice = priceAfterSmallChange,
      priceAfterSilver = priceAfterSilver,
      priceAfterPromotions = priceAfterPromotions,
      priceAfterPoints = priceAfterPoints,
      maxPoints = maxPoints,
      productId = product.id,
      productName = product.name,
      productItemCode = item.code,
      productItemName = item.name,
      attrs = item.attrs
    )
  }

  private def calcSilverDays(item: Item, user: User) = {
    val member = user.memberOrDefault
    val goldLevel = member.goldLevel
    val silverLevel = member.silverLevel

    val buyGoldDays = toDays(user, item.attrs)
    val remainderGoldDays = goldLevel.map(_.remainderChargeDays) | 0
    val remainderSilverDays = silverLevel.map(_.remainderChargeDays) | 0
    val availableSilverDays = math.max(remainderSilverDays - remainderGoldDays, 0)
    if (buyGoldDays > availableSilverDays) {
      availableSilverDays
    } else buyGoldDays
  }

  def toDays(user: User, attrs: Map[String, String]) = {
    val attrOption = attrs.get("days")
    val level = attrs.get("level") err "cant get level"
    attrOption.map { attr =>
      val oldExpireAt = user.memberOrDefault.levels.get(level).map(_.expireAt) | DateTime.now
      Days(attr).toDays(oldExpireAt)
    } getOrElse 0
  }

  def toDays(attrs: Map[String, String]) = {
    val attrOption = attrs.get("days")
    attrOption.map { attr =>
      Days(attr).toDays(DateTime.now)
    } getOrElse 0
  }

  def lockProductItem(data: CalcPriceData) = {
    val typ = ProductType(data.productTyp)
    val product = if (typ.indb) {
      ProductRepo.byId(data.productId) awaitSeconds (3) err (s"can not find product ${data.productId}")
    } else {
      typ.all.find(_.id == data.productId).err(s"can not find product ${data.productId}")
    }
    val item = product.item(data.itemCode).err(s"can not find item ${data.itemCode}")
    product -> item
  }

}

case class OrderAmountInfo(
    amounts: OrderAmounts,
    count: Int,
    price: BigDecimal, //单价
    totalPrice: BigDecimal, //原始总价
    payPrice: BigDecimal, //最终支付金额
    priceAfterSilver: BigDecimal, //银牌会员抵扣后的总价
    priceAfterPromotions: BigDecimal, //各种折扣或优惠抵扣后的总价
    priceAfterPoints: BigDecimal, //积分抵扣后的总价
    maxPoints: Int, // 可以使用的最大积分数量
    productId: String, // 商品ID
    productName: String, // 商品名称
    productItemCode: String, // 规格编码
    productItemName: String, // 规格名称
    attrs: Map[String, String]
)
