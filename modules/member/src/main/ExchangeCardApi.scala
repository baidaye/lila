package lila.member

import lila.db.dsl._
import lila.common.Bus
import lila.member.ExchangeCard.CardStatus
import lila.hub.actorApi.member.MemberExchangeCardUse
import org.joda.time.DateTime
import lila.user.User

case class ExchangeCardApi(coll: Coll, bus: Bus) {

  import BSONHandlers.MemberExchangeCardBSONHandler

  def byId(id: String): Fu[Option[ExchangeCard]] = coll.byId(id)

  def byCode(code: String): Fu[Option[ExchangeCard]] = coll.uno[ExchangeCard]($doc("code" -> code))

  def mine(userId: User.ID): Fu[List[ExchangeCard]] = {
    coll.find(
      $doc("usedBy" -> userId)
    ).sort($sort desc "usedAt").list()
  }

  def use(user: User, card: ExchangeCard): Funit = {
    val oldExpireAt = user.memberOrDefault.levels.get(card.level.code).map(_.expireAt) | DateTime.now
    coll.update(
      $id(card.id),
      $set(
        "status" -> CardStatus.Used.id,
        "usedBy" -> user.id,
        "usedAt" -> DateTime.now
      )
    ).void >>- bus.publish(
        MemberExchangeCardUse(
          userId = user.id,
          level = card.level.code,
          days = card.days.toDays(oldExpireAt),
          desc = card.desc,
          cardId = card.id
        ), 'memberExchangeCardUse
      )
  }

  def expiredTesting: Funit =
    coll.update(
      $doc(
        "status" -> CardStatus.Create.id,
        "expireAt" $lt DateTime.now
      ),
      $set("status" -> CardStatus.Expired.id),
      multi = true
    ).void

}
