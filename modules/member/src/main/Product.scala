package lila.member

import lila.user.MemberLevel
import org.joda.time.{ DateTime, Period, PeriodType }

case class Product(
    _id: String, // ID
    name: String, // 商品名称
    typ: ProductType, // 商品类型
    unit: String, // 单位
    desc: Option[String], // 商品描述
    publishStatus: PublishStatus, // 上架状态
    fixOne: Boolean, // 强制只销售一个
    defaultItem: String, // 展示的默认的规格
    itemName: String, // 展示的规格名称
    items: Items, // 规格
    attrs: Map[String, String] = Map.empty
) {

  def id = _id

  def itemList = items.map.toList.map(_._2).sortBy(_.sort)

  def item(itemCode: String) = items.get(itemCode)

}

object Product {

  def toMemberLevel(product: Product): MemberLevel = toMemberLevel(product.id)

  def toMemberLevel(productId: String): MemberLevel = {
    if (productId.toLowerCase.contains(MemberLevel.Gold.code)) MemberLevel.Gold
    else if (productId.toLowerCase.contains(MemberLevel.Silver.code)) MemberLevel.Silver
    else MemberLevel.General
  }

}

// code -> Item
case class Items(map: Map[String, Item]) {

  def get(itemCode: String) = map.get(itemCode)

}

case class Item(
    code: String, // 编码
    name: String, // 名称
    price: BigDecimal, // 价格
    isPoint: Boolean, // 是否可以使用积分
    isCoupon: Boolean, // 是否可以使用优惠券
    isInviteUser: Boolean, // 是否可以使用邀请码
    isSilverMember: Boolean, // 是否可以使用银牌会员抵扣
    isSmallChange: Boolean = false, // 是否可以抹零
    discountDesc: Option[String] = None,
    promotions: Promotions, // 折扣规则
    sort: Int,
    attrs: Map[String, String] = Map.empty
)

sealed abstract class PublishStatus(val id: String, val name: String)
object PublishStatus {
  case object ON extends PublishStatus("on", "已上架")
  case object OFF extends PublishStatus("off", "已下架")
  val all = List(ON, OFF)
  def apply(id: String): PublishStatus = all.find(_.id == id) err s"can not find PublishStatus $id"
}

object VmcGold extends Product(
  _id = "vmcGold",
  name = "金牌会员",
  typ = ProductType.VirtualMember,
  unit = "个",
  desc = none,
  publishStatus = PublishStatus.ON,
  fixOne = true,
  defaultItem = "vmcGold@" + Days.Year1.id,
  itemName = "使用期限",
  items = Items(
    Map(
      "vmcGold@" + Days.Month1.id -> Item(
        code = "vmcGold@" + Days.Month1.id,
        name = Days.Month1.name,
        price = MemberLevel.Gold.prices.month,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 0,
        attrs = Map(
          "days" -> Days.Month1.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Month3.id -> Item(
        code = "vmcGold@" + Days.Month3.id,
        name = Days.Month3.name,
        price = MemberLevel.Gold.prices.month * 3,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 1,
        attrs = Map(
          "days" -> Days.Month3.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Month6.id -> Item(
        code = "vmcGold@" + Days.Month6.id,
        name = Days.Month6.name,
        price = MemberLevel.Gold.prices.month * 6,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 2,
        attrs = Map(
          "days" -> Days.Month6.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Year1.id -> Item(
        code = "vmcGold@" + Days.Year1.id,
        name = Days.Year1.name,
        price = MemberLevel.Gold.prices.year,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 3,
        attrs = Map(
          "days" -> Days.Year1.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Year2.id -> Item(
        code = "vmcGold@" + Days.Year2.id,
        name = Days.Year2.name,
        price = MemberLevel.Gold.prices.year * 2,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.9
            )
          )
        ),
        sort = 4,
        attrs = Map(
          "days" -> Days.Year2.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Year3.id -> Item(
        code = "vmcGold@" + Days.Year3.id,
        name = Days.Year3.name,
        price = MemberLevel.Gold.prices.year * 3,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 5,
        attrs = Map(
          "days" -> Days.Year3.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Year4.id -> Item(
        code = "vmcGold@" + Days.Year4.id,
        name = Days.Year4.name,
        price = MemberLevel.Gold.prices.year * 4,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 6,
        attrs = Map(
          "days" -> Days.Year4.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmcGold@" + Days.Forever.id -> Item(
        code = "vmcGold@" + Days.Forever.id,
        name = Days.Forever.name,
        price = MemberLevel.Gold.prices.year * 5,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 7,
        attrs = Map(
          "days" -> Days.Forever.id,
          "level" -> MemberLevel.Gold.code
        )
      )
    )
  )
)

object VmcSilver extends Product(
  _id = "vmcSilver",
  name = "银牌会员",
  typ = ProductType.VirtualMember,
  unit = "个",
  desc = none,
  publishStatus = PublishStatus.ON,
  fixOne = true,
  defaultItem = "vmcSilver@" + Days.Year1.id,
  itemName = "使用期限",
  items = Items(
    Map(
      "vmcSilver@" + Days.Month1.id -> Item(
        code = "vmcSilver@" + Days.Month1.id,
        name = Days.Month1.name,
        price = MemberLevel.Silver.prices.month,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 0,
        attrs = Map(
          "days" -> Days.Month1.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Month3.id -> Item(
        code = "vmcSilver@" + Days.Month3.id,
        name = Days.Month3.name,
        price = MemberLevel.Silver.prices.month * 3,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 1,
        attrs = Map(
          "days" -> Days.Month3.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Month6.id -> Item(
        code = "vmcSilver@" + Days.Month6.id,
        name = Days.Month6.name,
        price = MemberLevel.Silver.prices.month * 6,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 2,
        attrs = Map(
          "days" -> Days.Month6.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Year1.id -> Item(
        code = "vmcSilver@" + Days.Year1.id,
        name = Days.Year1.name,
        price = MemberLevel.Silver.prices.year,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(),
        sort = 3,
        attrs = Map(
          "days" -> Days.Year1.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Year2.id -> Item(
        code = "vmcSilver@" + Days.Year2.id,
        name = Days.Year2.name,
        price = MemberLevel.Silver.prices.year * 2,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.9
            )
          )
        ),
        sort = 4,
        attrs = Map(
          "days" -> Days.Year2.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Year3.id -> Item(
        code = "vmcSilver@" + Days.Year3.id,
        name = Days.Year3.name,
        price = MemberLevel.Silver.prices.year * 3,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 5,
        attrs = Map(
          "days" -> Days.Year3.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Year4.id -> Item(
        code = "vmcSilver@" + Days.Year4.id,
        name = Days.Year4.name,
        price = MemberLevel.Silver.prices.year * 4,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 6,
        attrs = Map(
          "days" -> Days.Year4.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmcSilver@" + Days.Forever.id -> Item(
        code = "vmcSilver@" + Days.Forever.id,
        name = Days.Forever.name,
        price = MemberLevel.Silver.prices.year * 5,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 1,
              discount = 0.8
            )
          )
        ),
        sort = 7,
        attrs = Map(
          "days" -> Days.Forever.id,
          "level" -> MemberLevel.Silver.code
        )
      )
    )
  )
)

object VmCardGold extends Product(
  _id = "vmCardGold",
  name = "金牌会员",
  typ = ProductType.VirtualMemberCard,
  unit = "个",
  desc = none,
  publishStatus = PublishStatus.ON,
  fixOne = false,
  defaultItem = "vmCardGold@" + Days.Year1.id,
  itemName = "使用期限",
  items = Items(
    Map(
      "vmCardGold@" + Days.Year1.id -> Item(
        code = "vmCardGold@" + Days.Year1.id,
        name = Days.Year1.name,
        price = MemberLevel.Gold.prices.year,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = false,
        isSmallChange = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 20,
              discount = 0.4
            ),
            LadderPromotion(
              fullCount = 1,
              discount = 0.5
            )
          )
        ),
        sort = 1,
        attrs = Map(
          "days" -> Days.Year1.id,
          "level" -> MemberLevel.Gold.code
        )
      ),
      "vmCardGold@" + Days.Month6.id -> Item(
        code = "vmCardGold@" + Days.Month6.id,
        name = Days.Month6.name,
        price = MemberLevel.Gold.prices.month * 6,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = false,
        isSmallChange = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 20,
              discount = 0.4
            ),
            LadderPromotion(
              fullCount = 1,
              discount = 0.5
            )
          )
        ),
        sort = 2,
        attrs = Map(
          "days" -> Days.Month6.id,
          "level" -> MemberLevel.Gold.code
        )
      )
    )
  )
)

object VmCardSilver extends Product(
  _id = "vmCardSilver",
  name = "银牌会员",
  typ = ProductType.VirtualMemberCard,
  unit = "个",
  desc = none,
  publishStatus = PublishStatus.ON,
  fixOne = false,
  defaultItem = "vmCardSilver@" + Days.Year1.id,
  itemName = "使用期限",
  items = Items(
    Map(
      "vmCardSilver@" + Days.Year1.id -> Item(
        code = "vmCardSilver@" + Days.Year1.id,
        name = Days.Year1.name,
        price = MemberLevel.Silver.prices.year,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = false,
        isSmallChange = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 20,
              discount = 0.4
            ),
            LadderPromotion(
              fullCount = 1,
              discount = 0.5
            )
          )
        ),
        sort = 1,
        attrs = Map(
          "days" -> Days.Year1.id,
          "level" -> MemberLevel.Silver.code
        )
      ),
      "vmCardSilver@" + Days.Month6.id -> Item(
        code = "vmCardSilver@" + Days.Month6.id,
        name = Days.Month6.name,
        price = MemberLevel.Silver.prices.month * 6,
        isPoint = true,
        isCoupon = true,
        isInviteUser = false,
        isSilverMember = false,
        isSmallChange = true,
        promotions = Promotions(
          ladderPromotions = List(
            LadderPromotion(
              fullCount = 20,
              discount = 0.4
            ),
            LadderPromotion(
              fullCount = 1,
              discount = 0.5
            )
          )
        ),
        sort = 2,
        attrs = Map(
          "days" -> Days.Month6.id,
          "level" -> MemberLevel.Silver.code
        )
      )
    )
  )
)

sealed abstract class Days(val id: String, val name: String, val period: String, val n: Int) {

  def toDays(date: DateTime) = {
    if (period == "year") {
      val days = new Period(date.getMillis, date.plusYears(n).getMillis, PeriodType.days).getDays
      days
    } else if (period == "month") {
      val days = new Period(date.getMillis, date.plusMonths(n).getMillis, PeriodType.days).getDays
      days
    } else if (period == "day") {
      n
    } else 0
  }

}

object Days {
  case object Day7 extends Days("7day", "7天", "day", 7)
  case object Month1 extends Days("1month", "1个月", "month", 1)
  case object Month3 extends Days("3month", "3个月", "month", 3)
  case object Month6 extends Days("6month", "6个月", "month", 6)
  case object Year1 extends Days("1year", "1年", "year", 1)
  case object Year2 extends Days("2year", "2年", "year", 2)
  case object Year3 extends Days("3year", "3年", "year", 3)
  case object Year4 extends Days("4year", "4年", "year", 4)
  case object Forever extends Days(lila.user.Member.MaxYear + "year", "5年（永久）", "year", lila.user.Member.MaxYear)

  val all = List(Day7, Month1, Month3, Month6, Year1, Year2, Year3, Year4, Forever)
  def apply(id: String): Days = all.find(_.id == id) err s"can not find Days $id"
  def choices: List[(String, String)] = all.map(d => d.id -> d.name)
  def choices2: List[(String, String)] = List(Year1, Month6, Month3, Month1).map(d => d.id -> d.name)
}
