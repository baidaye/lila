package lila.member

import lila.db.dsl._
import lila.user.User

object ProductRepo {

  private lazy val coll = Env.current.ProductColl

  import BSONHandlers.BigDecimalHandler
  import BSONHandlers.ProductBSONHandler

  def byId(id: String): Fu[Option[Product]] = coll.byId(id)

  def byAttr(field: String, v: String): Fu[Option[Product]] =
    coll.find($doc(field -> v)).uno[Product]

  def updateOpeningdbPrice(id: String, price: BigDecimal): Funit =
    coll.update($doc("_id" -> id), $set(s"items.$id@100year.price" -> price)).void

}
