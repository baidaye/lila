package lila.member

import akka.actor.{ ActorSystem, Props }
import com.typesafe.config.Config
import lila.game.actorApi.StartGame
import lila.hub.actorApi.analyse.AnalysisStart
import lila.notify.NotifyApi

import scala.concurrent.duration._
import lila.hub.actorApi.puzzle._
import lila.hub.actorApi.member._
import lila.puzzle.PuzzleResult

final class Env(
    config: Config,
    rootConfig: Config,
    db: lila.db.Env,
    hub: lila.hub.Env,
    notifyApi: NotifyApi,
    system: ActorSystem
) {

  val bus = hub.bus

  private val payTest = rootConfig getBoolean "alipay.test"
  private val appId = rootConfig getString "alipay.appId"
  private val sellerId = rootConfig getString "alipay.sellerId"
  private val ActorName = config getString "actor.name"
  private val CollectionOrder = config getString "collection.member_order"
  private val CollectionCard = config getString "collection.member_card"
  private val CollectionOrderStatusLog = config getString "collection.member_order_statuslog"
  private val CollectionOrderPay = config getString "collection.member_order_pay"
  private val CollectionMemberPointsLog = config getString "collection.member_points_log"
  private val CollectionMemberLevelLog = config getString "collection.member_level_log"
  private val CollectionMemberCardLog = config getString "collection.member_card_log"
  private val CollectionMemberCardStatusLog = config getString "collection.member_card_statuslog"
  private val CollectionMemberActive = config getString "collection.member_active"
  private val CollectionExchangeBatch = config getString "collection.member_exchange_batch"
  private val CollectionExchangeCard = config getString "collection.member_exchange_card"
  private val CollectionProduct = config getString "collection.member_product"

  lazy val form = new DataForm

  lazy val ProductColl = db(CollectionProduct)

  lazy val orderStatusLogApi = OrderStatusLogApi(
    db(CollectionOrderStatusLog)
  )

  lazy val memberPointsLogApi = MemberPointsLogApi(
    db(CollectionMemberPointsLog)
  )

  lazy val memberLevelLogApi = MemberLevelLogApi(
    db(CollectionMemberLevelLog)
  )

  lazy val memberCardLogApi = MemberCardLogApi(
    db(CollectionMemberCardLog)
  )

  lazy val memberCardStatusLogApi = MemberCardStatusLogApi(
    db(CollectionMemberCardStatusLog)
  )

  lazy val memberCardApi = MemberCardApi(
    db(CollectionCard),
    memberCardLogApi,
    memberCardStatusLogApi,
    bus,
    notifyApi
  )

  lazy val orderPayApi = OrderPayApi(
    db(CollectionOrderPay)
  )

  lazy val orderApi = OrderApi(
    db(CollectionOrder),
    orderStatusLogApi,
    orderPayApi,
    memberCardApi,
    payTest,
    appId,
    sellerId,
    bus,
    notifyApi
  )

  lazy val memberActiveRecordApi = MemberActiveRecordApi(db(CollectionMemberActive))

  lazy val exchangeBatchApi = ExchangeBatchApi(db(CollectionExchangeBatch))

  lazy val exchangeCardApi = ExchangeCardApi(db(CollectionExchangeCard), bus)

  system.actorOf(Props(new MemberActor(
    memberActiveRecordApi = memberActiveRecordApi
  )), name = ActorName)

  bus.subscribeFun('finishPuzzle, /*'nextPuzzle, 'nextThemePuzzle, 'nextRushPuzzle, 'nextRacePuzzle, 'nextCapsulePuzzle, 'nextHomeworkPuzzle, 'nextTaskPuzzle, 'nextErrorPuzzle,*/ 'startPuzzleRush, 'startGame, 'analysisStart) {
    //    case NextPuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzle = true, puzzleId = puzzleId.some)
    //    case NextThemePuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, themePuzzle = true, puzzleId = puzzleId.some)
    //    case NextRushPuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    //    case NextRacePuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    //    case NextCapsulePuzzle(_, puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    //    case NextHomeworkPuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    //    case NextTaskPuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    //    case NextErrorPuzzle(puzzleId, userId) => memberActiveRecordApi.updateRecord(userId, puzzleId = puzzleId.some)
    case res: PuzzleResult => res.source match {
      case PuzzleResult.Source.Puzzle => memberActiveRecordApi.updateRecord(res.userId, puzzle = true, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Theme => memberActiveRecordApi.updateRecord(res.userId, themePuzzle = true, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Rush => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Race => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Capsule => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Homework => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Task => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
      case PuzzleResult.Source.Error => memberActiveRecordApi.updateRecord(res.userId, puzzleId = res.puzzleId.some)
    }
    case StartPuzzleRush(_, userId) => memberActiveRecordApi.updateRecord(userId, puzzleRush = true)
    case StartGame(game) => if (game.hasAi) memberActiveRecordApi.updateAiGameRecord(game)
    case AnalysisStart(_, userId) => memberActiveRecordApi.updateRecord(userId, analyse = true)
  }

  bus.subscribeFun('memberLevelChange, 'memberPointsChange, 'memberBuyComplete, 'memberCardBuyComplete, 'openingdbSysBuyComplete, 'openingdbCreateBuyComplete, 'openingdbUpdatePrice) {
    case MemberLevelChange(userId, typ, level, oldExpireAt, newExpireAt, desc, orderId, cardId) =>
      memberLevelLogApi.setLog(userId, typ, level, oldExpireAt, newExpireAt, desc, orderId, cardId)
    case MemberPointsChange(userId, typ, pointsDiff, orderId, _) =>
      memberPointsLogApi.setLog(userId, typ, pointsDiff, orderId)
    case MemberBuyComplete(orderId, _) =>
      orderApi.member.completed(orderId)
    case MemberCardBuyComplete(orderId, _) =>
      orderApi.card.completed(orderId)
    case OpeningdbSysBuyComplete(orderId, _) =>
      orderApi.openingdbSys.completed(orderId)
    case OpeningdbCreateBuyComplete(orderId, _) =>
      orderApi.openingdbCreate.completed(orderId)
    case OpeningdbUpdatePrice(id, price) =>
      ProductRepo.updateOpeningdbPrice(id, price)
  }

  //  bus.subscribeFun('signupConfirmed) {
  //    case lila.security.SignupConfirmed(userId) => memberCardApi.addCardBySignup(userId)
  //  }

  system.scheduler.schedule(1 minute, 1 minute) {
    memberCardApi.expiredTesting
    orderApi.expiredTesting
    exchangeCardApi.expiredTesting
  }

}

object Env {

  lazy val current: Env = "member" boot new Env(
    config = lila.common.PlayApp loadConfig "member",
    rootConfig = lila.common.PlayApp.loadConfig,
    db = lila.db.Env.current,
    hub = lila.hub.Env.current,
    notifyApi = lila.notify.Env.current.api,
    system = lila.common.PlayApp.system
  )
}
