package lila.member

import lila.db.dsl._

case class ExchangeBatchApi(coll: Coll) {

  import BSONHandlers.MemberExchangeBatchBSONHandler

}
