package lila.member

import lila.user.User
import org.joda.time.DateTime

case class MemberActiveRecord(
    _id: String,
    userId: User.ID,
    date: Int,
    puzzle: Option[Int],
    themePuzzle: Option[Int],
    puzzleRush: Option[Int],
    playWithAi: Option[Int],
    analyse: Option[Int],
    puzzleIds: Option[Set[Int]],
    updateAt: DateTime
) {

  def id: String = _id

  def totalPuzzle = puzzleIds.map(_.size)

}

object MemberActiveRecord {

  def makeId(userId: User.ID, date: DateTime) = date.toString("yyyyMMdd") + "@" + userId

}
