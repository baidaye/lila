package lila.member

import lila.db.dsl._
import lila.game.Game
import lila.user.{ User, UserRepo }
import org.joda.time.DateTime

case class MemberActiveRecordApi(coll: Coll) {

  import BSONHandlers.MemberRecRecordBSONHandler

  def isPuzzleAccept(user: User, puzzleId: Option[Int] = None): Fu[Boolean] = {
    isAccept(user, r => {
      val maxPuzzleCount = user.memberLevel.permissions.puzzle
      val maxTotalPuzzleCount = user.memberLevel.permissions.puzzleTotal
      puzzleId.exists(p => r.puzzleIds.exists(_.contains(p))) || ((r.puzzle | 0) < maxPuzzleCount) && ((r.totalPuzzle | 0) < maxTotalPuzzleCount)
    })
  }

  def isThemePuzzleAccept(user: User, puzzleId: Option[Int] = None): Fu[Boolean] = {
    isAccept(user, r => {
      val maxThemePuzzleCount = user.memberLevel.permissions.themePuzzle
      val maxTotalPuzzleCount = user.memberLevel.permissions.puzzleTotal
      puzzleId.exists(p => r.puzzleIds.exists(_.contains(p))) || ((r.themePuzzle | 0) < maxThemePuzzleCount) && ((r.totalPuzzle | 0) < maxTotalPuzzleCount)
    })
  }

  def isRushAccept(user: User, puzzleId: Option[Int] = None): Fu[Boolean] = {
    isAccept(user, r => {
      val maxPuzzleRushCount = user.memberLevel.permissions.puzzleRush
      val maxTotalPuzzleCount = user.memberLevel.permissions.puzzleTotal
      puzzleId.exists(p => r.puzzleIds.exists(_.contains(p))) || ((r.puzzleRush | 0) < maxPuzzleRushCount) && ((r.totalPuzzle | 0) < maxTotalPuzzleCount)
    })
  }

  def isTotalPuzzleAccept(user: User, puzzleId: Option[Int] = None): Fu[Boolean] = {
    isAccept(user, r => {
      val maxTotalPuzzleCount = user.memberLevel.permissions.puzzleTotal
      puzzleId.exists(p => r.puzzleIds.exists(_.contains(p))) || (r.totalPuzzle | 0) < maxTotalPuzzleCount
    })
  }

  def isAiPlayAccept(user: User): Fu[Boolean] = {
    isAccept(user, r => {
      val maxPlayWithAiCount = user.memberLevel.permissions.playWithAi
      (r.playWithAi | 0) < maxPlayWithAiCount
    })
  }

  def isAnalyseAccept(user: User): Fu[Boolean] = {
    isAccept(user, r => {
      val maxAnalyseCount = user.memberLevel.permissions.analyse
      (r.analyse | 0) < maxAnalyseCount
    })
  }

  def isAccept(userId: String, typ: String): Fu[Boolean] = UserRepo.byId(userId).flatMap {
    _.?? { user =>
      typ match {
        case "Puzzle" => isPuzzleAccept(user)
        case "ThemePuzzle" => isThemePuzzleAccept(user)
        case "TotalPuzzle" => isTotalPuzzleAccept(user)
        case "Rush" => isRushAccept(user)
        case "AiPlay" => isAiPlayAccept(user)
        case "Analyse" => isAnalyseAccept(user)
      }
    }
  }

  private def isAccept(user: User, f: MemberActiveRecord => Boolean): Fu[Boolean] = {
    if (user.isCoachOrTeam) fuccess(true)
    else {
      byUserId(user.id) map {
        case None => true
        case Some(r) => f(r)
      }
    }
  }

  def byUserId(userId: User.ID): Fu[Option[MemberActiveRecord]] =
    coll.byId[MemberActiveRecord](MemberActiveRecord.makeId(userId, DateTime.now))

  def updateAiGameRecord(game: Game) = {
    game.players.filter(_.isHuman).foreach { player =>
      player.userId.foreach { userId =>
        updateRecord(userId, playWithAi = true)
      }
    }
  }

  def updateRecord(
    userId: User.ID,
    puzzle: Boolean = false,
    themePuzzle: Boolean = false,
    puzzleRush: Boolean = false,
    puzzleId: Option[Int] = None,
    playWithAi: Boolean = false,
    analyse: Boolean = false
  ): Funit = {
    val now = DateTime.now
    val id = MemberActiveRecord.makeId(userId, now)
    coll.update(
      $id(id),
      $set(
        "userId" -> userId,
        "date" -> now.toString("yyyyMMdd").toInt,
        "updateAt" -> now
      )
        ++ puzzle.?? {
          $inc("puzzle" -> 1)
        } ++ themePuzzle.?? {
          $inc("themePuzzle" -> 1)
        } ++ puzzleRush.?? {
          $inc("puzzleRush" -> 1)
        } ++ puzzleId.?? { pid =>
          $addToSet("puzzleIds" -> pid)
        } ++ playWithAi.?? {
          $inc("playWithAi" -> 1)
        } ++ analyse.?? {
          $inc("analyse" -> 1)
        },
      upsert = true
    ).void
  }

}
