package lila.member

import akka.actor.Actor

private[member] final class MemberActor(
    memberActiveRecordApi: MemberActiveRecordApi
) extends Actor {

  def receive = {
    case lila.hub.actorApi.member.IsAccept(userId, typ) =>
      sender ! memberActiveRecordApi.isAccept(userId, typ).awaitSeconds(3)
  }

}
