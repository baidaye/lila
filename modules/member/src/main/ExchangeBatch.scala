package lila.member

import lila.user.{ MemberLevel, User }
import org.joda.time.DateTime
import ExchangeBatch._

case class ExchangeBatch(
    _id: ID,
    startNo: Int,
    nb: Int,
    level: MemberLevel,
    days: Days,
    expireAt: DateTime,
    remark: Option[String],
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

  def expired = expireAt.isBeforeNow

  def desc = s"${level.name}（${days.name}）"

}

object ExchangeBatch {

  type ID = Int

}

