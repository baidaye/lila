package lila.member

sealed abstract class ProductType(val id: String, val name: String, val prefix: String, val indb: Boolean) {
  def all: List[Product]
}

object ProductType {

  case object VirtualMember extends ProductType("virtualMember", "虚拟会员", "UB", indb = false) {

    def all = List(VmcGold, VmcSilver)

    def defaultDays = Days.Year1

    def defaultProduct = VmcGold

    def productOfLevel(level: String) = all.find(_.id.toLowerCase.contains(level.toLowerCase)).map(_.id)

  }

  case object VirtualMemberCard extends ProductType("virtualMemberCard", "虚拟会员卡", "OB", indb = false) {

    def all = List(VmCardGold, VmCardSilver)

    def defaultDays = Days.Year1

    def defaultProduct = VmCardGold

    def productOfLevel(level: String) = all.find(_.id.toLowerCase.contains(level.toLowerCase)).map(_.id)

  }

  case object SystemOpeningdb extends ProductType("systemOpeningdb", "系统开局库", "SO", indb = true) {

    def all = Nil

  }

  case object CreateOpeningdb extends ProductType("createOpeningdb", "新建开局库", "CO", indb = true) {

    def all = Nil

  }

  val all = List(VirtualMember, VirtualMemberCard, SystemOpeningdb, CreateOpeningdb)
  def apply(id: String): ProductType = all.find(_.id == id) err s"can not find ProductType $id"
  def choices = all.map(d => d.id -> d.name)
}

