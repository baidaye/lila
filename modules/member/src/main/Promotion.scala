package lila.member

import lila.user.User
import org.joda.time.DateTime

abstract class Promotion {

  def fit(sourcePrice: BigDecimal, count: Int, user: User): Boolean

  def calc(sourcePrice: BigDecimal, count: Int, user: User): BigDecimal
}

case class Promotions(
    pricePromotions: List[PricePromotion] = Nil,
    memberPromotions: List[MemberPromotion] = Nil,
    memberDiscountPromotions: List[MemberDiscountPromotion] = Nil,
    ladderPromotions: List[LadderPromotion] = Nil,
    fullReducePromotions: List[FullReducePromotion] = Nil
) {

  // 限定条件：达成条件必须从难道易（按顺序放入List中）
  // 每种折扣只能选择一种，多种折扣可以叠加
  /*  def every(sourcePrice: BigDecimal, count: Int, user: User): List[Promotion] =
    (
      pricePromotions.find(_.fit(sourcePrice, count, user)),
      memberPromotions.find(_.fit(sourcePrice, count, user)),
      ladderPromotions.find(_.fit(sourcePrice, count, user)),
      fullReducePromotions.find(_.fit(sourcePrice, count, user))
    ).filter(_.nonEmpty).map(_.get)*/

  /*  def calc(sourcePrice: BigDecimal, count: Int, user: User): BigDecimal = {
    every(sourcePrice, count, user).foldLeft(sourcePrice) {
      case (price, promotion) => promotion.calc(price, count, user)
    }
  }*/

}

// 特惠促销
case class PricePromotion(
    sTime: DateTime, // 开始时间
    eTime: DateTime, // 结束时间
    price: BigDecimal // 促销价格
) extends Promotion {

  def fit(sourcePrice: BigDecimal, count: Int, user: User) = {
    val now = DateTime.now
    now.isBefore(eTime) && now.isAfter(eTime)
  }

  def calc(sourcePrice: BigDecimal, count: Int, user: User) = {
    if (fit(sourcePrice, count, user)) price
    else sourcePrice
  }.max(BigDecimal(0.00))
}

// 会员促销
case class MemberPromotion(
    level: String, // 会员等级（code）
    price: BigDecimal // 促销价格
) extends Promotion {

  def fit(sourcePrice: BigDecimal, count: Int, user: User) =
    user.memberLevel.code == level

  def calc(sourcePrice: BigDecimal, count: Int, user: User) = {
    if (fit(sourcePrice, count, user)) price
    else sourcePrice
  }.max(BigDecimal(0.00))
}

// 会员折扣
case class MemberDiscountPromotion(
    level: String, // 会员等级（code）
    discount: BigDecimal // 折扣（0.8）
) extends Promotion {

  def name = s"${discount * 10}折"

  def fit(sourcePrice: BigDecimal, count: Int, user: User) =
    user.memberLevel.code == level

  def calc(sourcePrice: BigDecimal, count: Int, user: User) = {
    if (fit(sourcePrice, count, user)) sourcePrice * discount
    else sourcePrice
  }.max(BigDecimal(0.00))
}

// 阶梯价格
case class LadderPromotion(
    fullCount: Int, // 满足的商品数量
    discount: BigDecimal // 折扣（0.8）
) extends Promotion {

  def name = s"${discount * 10}折"

  def fit(sourcePrice: BigDecimal, count: Int, user: User) =
    count >= fullCount

  def calc(sourcePrice: BigDecimal, count: Int, user: User) = {
    if (fit(sourcePrice, count, user)) sourcePrice * discount
    else sourcePrice
  }.max(BigDecimal(0.00))
}

// 满减价格
case class FullReducePromotion(
    fullPrice: BigDecimal, // 商品满足金额
    reducePrice: BigDecimal // 商品减少金额
) extends Promotion {

  def fit(sourcePrice: BigDecimal, count: Int, user: User) =
    sourcePrice >= fullPrice

  def calc(sourcePrice: BigDecimal, count: Int, user: User) = {
    if (fit(sourcePrice, count, user)) sourcePrice - reducePrice
    else sourcePrice
  }.max(BigDecimal(0.00))
}
