package lila.member

import lila.common.{ Bus, MaxPerPage }
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.hub.actorApi.member.MemberCardUse
import lila.notify.{ Notification, NotifyApi }
import lila.user.User
import lila.member.MemberCard.CardStatus
import lila.notify.Notification.{ Notifies, Sender }
import org.joda.time.DateTime

case class MemberCardApi(coll: Coll, logApi: MemberCardLogApi, statusLogApi: MemberCardStatusLogApi, bus: Bus, notifyApi: NotifyApi) {

  import BSONHandlers.MemberCardBSONHandler

  def byId(id: String): Fu[Option[MemberCard]] = coll.byId(id)

  def goldCardNumber(userId: User.ID): Fu[Int] =
    coll.countSel(
      $doc(
        "userId" -> userId,
        "level" -> lila.user.MemberLevel.Gold.code,
        "status" -> MemberCard.CardStatus.Create.id
      )
    )

  def silverCardNumber(userId: User.ID): Fu[Int] =
    coll.countSel(
      $doc(
        "userId" -> userId,
        "level" -> lila.user.MemberLevel.Silver.code,
        "status" -> MemberCard.CardStatus.Create.id
      )
    )

  def mine(userId: User.ID): Fu[List[MemberCard]] = {
    for {
      create <- coll.find(
        $doc(
          "userId" -> userId,
          "status" -> MemberCard.CardStatus.Create.id
        )
      ).sort($sort desc "expireAt").list()
      used <- coll.find(
        $doc(
          "userId" -> userId,
          "status" -> MemberCard.CardStatus.Used.id
        )
      ).sort($sort desc "expireAt").list()
      expired <- coll.find(
        $doc(
          "userId" -> userId,
          "status" -> MemberCard.CardStatus.Expired.id
        )
      ).sort($sort desc "expireAt").list()
    } yield create ++ used ++ expired
  }

  def minePage(userId: User.ID, page: Int, status: Option[MemberCard.CardStatus], level: Option[lila.user.MemberLevel]): Fu[Paginator[MemberCard]] = {
    val adapter = new Adapter[MemberCard](
      collection = coll,
      selector = $doc(
        "userId" -> userId
      ) ++ status.??(s => $doc("status" -> s.id)) ++ level.??(l => $doc("level" -> l.code)),
      projection = $empty,
      sort = $sort desc "expireAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def setStatus(card: MemberCard, status: CardStatus): Funit =
    coll.update(
      $id(card.id),
      $set("status" -> status.id)
    ).void >> logApi.setStatus(card.id, status) >> statusLogApi.setLog(
        card = card,
        status = status,
        userId = status match {
          case CardStatus.Used => card.userId
          case CardStatus.Expired => "system"
          case _ => "-"
        },
        note = none
      )

  def findLastBatch(): Fu[Int] = {
    coll.find($empty, $doc("batch" -> true))
      .sort($sort desc "batch")
      .uno[Bdoc] map {
        _ flatMap { doc => doc.getAs[Int]("batch") map (1+) } getOrElse 1
      }
  }

  def addCardByOrder(order: Order): Funit = findLastBatch flatMap { batch =>
    val cards = MemberCard.makeByOrder(order, batch)
    coll.bulkInsert(
      documents = cards.map(MemberCardBSONHandler.write).toStream,
      ordered = true
    ).void
  }

  def addCardBySignup(userId: User.ID): Funit =
    logApi.hasSignupGiven(userId) flatMap { exists =>
      (!exists).?? {
        findLastBatch flatMap { batch =>
          val card = MemberCard.makeBySignup(userId, batch)
          coll.insert(card) >> logApi.setLog(
            userId = userId,
            card = card.copy(userId = "admin"),
            typ = MemberCardLog.Type.SignupGiven,
            note = none
          ) >> applyNotify("admin", "haichess.com", userId, card)
        }
      }
    }

  def batchGive(me: User, members: List[User], data: BatchGive): Fu[List[MemberCardLog]] =
    getMineCards(me.id, data, members.size).flatMap { cards =>
      (members zip cards).map {
        case (member, card) => give(me, member, card, true)
      }.sequenceFu
    }

  def batchGive2(me: User, members: List[User], data: BatchGive): Fu[List[(User, MemberCard)]] =
    getMineCards(me.id, data, members.size).flatMap { cards =>
      val memberWithCards = members zip cards
      memberWithCards.map {
        case (member, card) => give(me, member, card, false)
      }.sequenceFu inject memberWithCards.map {
        case (member, card) => (member, card.copy(userId = member.id))
      }
    }

  def give(me: User, member: User, card: MemberCard, isNotify: Boolean = true): Fu[MemberCardLog] =
    coll.update(
      $id(card.id),
      $set("userId" -> member.id)
    ).void >> logApi.setLog(
        userId = member.id,
        card = card,
        typ = MemberCardLog.Type.CoachOrTeam,
        note = none
      ) flatMap { log =>
        isNotify.?? { applyNotify(me.id, me.username, member.id, card) } inject log
      }

  def give2(me: User, member: User, data: BatchGive): Fu[Option[MemberCard]] = {
    getMineCards(me.id, data, 1).flatMap { cards =>
      cards.headOption match {
        case None => fuccess(none[MemberCard])
        case Some(card) => {
          give(me, member, card, false) inject card.copy(userId = member.id).some
        }
      }
    }
  }

  def batchUse(memberWithCards: List[(User, MemberCard)]): Funit =
    memberWithCards.map {
      case (user, card) => use(user, card)
    }.sequenceFu.void

  def use(user: User, card: MemberCard): Funit = {
    val oldExpireAt = user.memberOrDefault.levels.get(card.level.code).map(_.expireAt) | DateTime.now
    setStatus(card, CardStatus.Used) >>- bus.publish(
      MemberCardUse(
        userId = card.userId,
        level = card.level.code,
        days = card.days.toDays(oldExpireAt),
        desc = card.desc,
        cardId = card.id
      ), 'memberCardUse
    )
  }

  def expiredTesting =
    coll.find(
      $doc(
        "status" -> CardStatus.Create.id,
        "expireAt" $lt DateTime.now
      )
    ).list(100) flatMap { cards =>
        cards.map { card =>
          setStatus(card, CardStatus.Expired)
        }.sequenceFu.void
      }

  def cardCount(userId: String, cardLevel: String, days: String): Fu[Int] =
    coll.countSel(
      $doc(
        "userId" -> userId,
        "level" -> cardLevel,
        "days" -> days,
        "status" -> CardStatus.Create.id,
        "expireAt" $gt DateTime.now
      )
    )

  def getMineCards(userId: String, data: BatchGive, size: Int): Fu[List[MemberCard]] =
    coll.find(
      $doc(
        "userId" -> userId,
        "level" -> data.cardLevel,
        "days" -> data.days,
        "status" -> CardStatus.Create.id,
        "expireAt" $gt DateTime.now
      )
    ).sort($doc("expireAt" -> 1)).list[MemberCard](size)

  private def applyNotify(senderId: User.ID, senderName: String, member: User.ID, card: MemberCard): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(senderId).some,
      Notifies(member),
      lila.notify.GenericLink(
        url = "/member/info#card",
        title = "赠送会员卡".some,
        text = s"${senderName}赠送您一张${card.level.name}卡，使用期限${card.days.name}".some,
        icon = "兵"
      )
    ))
  }

}
