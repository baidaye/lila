package lila.member

import play.api.data._
import play.api.data.Forms._
import lila.common.Form.{ ISODate, ISODateTime2, stringIn }
import org.joda.time.DateTime
import lila.user.User

final class DataForm {

  def order(user: User, discounts: List[(String, String)]) = Form(mapping(
    "productTyp" -> nonEmptyText,
    "productId" -> nonEmptyText,
    "itemCode" -> nonEmptyText,
    "count" -> number(1, 999),
    "points" -> optional(number(0, DataForm.MaxPoints)),
    "isPointsChange" -> optional(boolean),
    "coupon" -> optional(nonEmptyText),
    "inviteUser" -> optional(nonEmptyText
      .verifying("不可以填写自己哦~", user.id != _)
      .verifying("邀请人必须是认证教练或俱乐部", discounts.map(_._1).contains(_))),
    "payWay" -> stringIn(PayWay.choices)
  )(OrderData.apply)(OrderData.unapply).verifying("请选择正确的产品", _ validLevel (user)))

  def calcPrice(user: User, discounts: List[(String, String)]) = Form(mapping(
    "productTyp" -> nonEmptyText,
    "productId" -> nonEmptyText,
    "itemCode" -> nonEmptyText,
    "count" -> number(1, 999),
    "points" -> optional(number(0, DataForm.MaxPoints)),
    "isPointsChange" -> optional(boolean),
    "coupon" -> optional(nonEmptyText),
    "inviteUser" -> optional(nonEmptyText
      .verifying("不可以填写自己哦~", user.id != _)
      .verifying("邀请人必须是认证教练或俱乐部", discounts.map(_._1).contains(_)))
  )(CalcPriceData.apply)(CalcPriceData.unapply).verifying("请选择正确的产品", _ validLevel (user)))

  val orderSearch = Form(mapping(
    "typ" -> optional(stringIn(ProductType.choices)),
    "level" -> optional(stringIn(lila.user.MemberLevel.choices)),
    "dateMin" -> optional(ISODate.isoDate),
    "dateMax" -> optional(ISODate.isoDate)
  )(OrderSearch.apply)(OrderSearch.unapply))

  val cardLogSearch = Form(mapping(
    "username" -> optional(lila.user.DataForm.historicalUsernameField),
    "level" -> optional(stringIn(lila.user.MemberLevel.choices)),
    "status" -> optional(stringIn(MemberCard.CardStatus.choices)),
    "dateMin" -> optional(ISODate.isoDate),
    "dateMax" -> optional(ISODate.isoDate)
  )(MemberCardLogSearch.apply)(MemberCardLogSearch.unapply))

  val orderAlipayNofify = Form(mapping(
    "trade_no" -> nonEmptyText,
    "app_id" -> nonEmptyText,
    "out_trade_no" -> nonEmptyText,
    "out_biz_no" -> optional(nonEmptyText),
    "buyer_id" -> optional(nonEmptyText),
    "seller_id" -> optional(nonEmptyText),
    "trade_status" -> optional(nonEmptyText),
    "total_amount" -> optional(bigDecimal),
    "receipt_amount" -> optional(bigDecimal),
    "invoice_amount" -> optional(bigDecimal),
    "buyer_pay_amount" -> optional(bigDecimal),
    "point_amount" -> optional(bigDecimal),
    "refund_fee" -> optional(bigDecimal),
    "subject" -> optional(nonEmptyText),
    "body" -> optional(nonEmptyText),
    "gmt_create" -> optional(ISODateTime2.isoDate),
    "gmt_payment" -> optional(ISODateTime2.isoDate),
    "gmt_refund" -> optional(ISODateTime2.isoDate),
    "gmt_close" -> optional(ISODateTime2.isoDate),
    "fund_bill_list" -> optional(nonEmptyText),
    "voucher_detail_list" -> optional(nonEmptyText),
    "passback_params" -> optional(nonEmptyText)
  )(AlipayData.apply)(AlipayData.unapply))

  val batchGive = Form(mapping(
    "mems" -> list(lila.user.DataForm.historicalUsernameField),
    "cardLevel" -> stringIn(lila.user.MemberLevel.choices),
    "days" -> stringIn(lila.member.Days.choices)
  )(BatchGive.apply)(BatchGive.unapply))

  val batchGiveSuccess = Form(mapping(
    "logIds" -> nonEmptyText,
    "cardLevel" -> stringIn(lila.user.MemberLevel.choices),
    "days" -> stringIn(lila.member.Days.choices)
  )(BatchGiveSuccess.apply)(BatchGiveSuccess.unapply))

  val cardCount = Form(mapping(
    "cardLevel" -> stringIn(lila.user.MemberLevel.choices),
    "days" -> stringIn(lila.member.Days.choices)
  )(CardCount.apply)(CardCount.unapply))

  def memberSearch = Form(mapping(
    "username" -> optional(nonEmptyText(minLength = 1, maxLength = 20)),
    "clazzId" -> optional(nonEmptyText),
    "memberLevel" -> optional(nonEmptyText),
    "hideGivingInMonth" -> optional(boolean)
  )(MemberSearch.apply)(MemberSearch.unapply))

  val exchangeUse = Form(mapping(
    "code1" -> nonEmptyText(minLength = 4, maxLength = 4),
    "code2" -> nonEmptyText(minLength = 4, maxLength = 4),
    "code3" -> nonEmptyText(minLength = 4, maxLength = 4)
  )(ExchangeUse.apply)(ExchangeUse.unapply))

}

case class CalcPriceData(
    productTyp: String,
    productId: String,
    itemCode: String,
    count: Int,
    points: Option[Int],
    isPointsChange: Option[Boolean],
    coupon: Option[String],
    inviteUser: Option[String]
) {

  def validLevel(me: User) = {
    val typ = ProductType(productTyp)
    val ml = Product.toMemberLevel(productId)
    (typ == ProductType.VirtualMemberCard || typ == ProductType.SystemOpeningdb || typ == ProductType.CreateOpeningdb) || (ml.id > me.memberLevel.id || (ml == me.memberLevel && !me.memberOrDefault.lvWithExpire.isForever))
  }
}

object CalcPriceData {

  def of(data: OrderData) = CalcPriceData(
    productTyp = data.productTyp,
    productId = data.productId,
    itemCode = data.itemCode,
    count = data.count,
    points = data.points,
    isPointsChange = data.isPointsChange,
    coupon = data.coupon,
    inviteUser = data.inviteUser
  )
}

case class OrderData(
    productTyp: String,
    productId: String,
    itemCode: String,
    count: Int,
    points: Option[Int],
    isPointsChange: Option[Boolean],
    coupon: Option[String],
    inviteUser: Option[String],
    payWay: String
) {

  def validLevel(me: User) = {
    val typ = ProductType(productTyp)
    val ml = Product.toMemberLevel(productId)
    (typ == ProductType.VirtualMemberCard || typ == ProductType.SystemOpeningdb || typ == ProductType.CreateOpeningdb) || (ml.id > me.memberLevel.id || (ml == me.memberLevel && !me.memberOrDefault.lvWithExpire.isForever))
  }

}

case class OrderSearch(
    typ: Option[String],
    level: Option[String],
    dateMin: Option[DateTime],
    dateMax: Option[DateTime]
)

case class MemberCardLogSearch(
    username: Option[String],
    level: Option[String],
    status: Option[String],
    dateMin: Option[DateTime],
    dateMax: Option[DateTime]
)

case class BatchGive(
    mems: List[String],
    cardLevel: String,
    days: String
)

case class BatchGiveSuccess(
    logIds: String,
    cardLevel: String,
    days: String
) {
  def ids = logIds.split(",").toList
  def cardLevelName = lila.user.MemberLevel.apply(cardLevel).name
  def daysName = Days.apply(days).name
}

case class CardCount(
    cardLevel: String,
    days: String
)

case class ExchangeUse(code1: String, code2: String, code3: String) {

  def toCode = s"$code1-$code2-$code3"

}

case class MemberSearch(
    clazzId: Option[String] = None,
    username: Option[String] = None,
    memberLevel: Option[String] = None,
    hideGivingInMonth: Option[Boolean] = None
)

object DataForm {

  val MaxPoints = 10000

}
