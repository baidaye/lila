package lila.hub

object PuzzleHub {

  val minRating = 500

  val maxRating = 2600

  // 主题战术 100000 - 1000000
  val minThemeId = 100000 // 不包含

  // 主题战术 只包含lichess-mark
  val maxThemeId = 200000 // 不包含

}
