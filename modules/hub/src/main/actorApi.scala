package lila.hub
package actorApi
import chess.format.Uci
import org.joda.time.DateTime
import play.api.libs.json._
import scala.concurrent.Promise

sealed abstract class Deploy(val key: String)
case object DeployPre extends Deploy("deployPre")
case object DeployPost extends Deploy("deployPost")

case object Shutdown // on actor system termination

// announce something to all clients
case class Announce(msg: String)

package streamer {
  case class StreamsOnAir(html: String)
  case class StreamStart(userId: String)
}

package map {
  case class Tell(id: String, msg: Any)
  case class TellIfExists(id: String, msg: Any)
  case class Exists(id: String, promise: Promise[Boolean])
}

package socket {
  case class WithUserIds(f: Iterable[String] => Unit)
  case class HasUserId(userId: String, promise: Promise[Boolean])
  case class SendTo(userId: String, message: JsObject)
  object SendTo {
    def apply[A: Writes](userId: String, typ: String, data: A): SendTo =
      SendTo(userId, Json.obj("t" -> typ, "d" -> data))
  }
  case class SendTos(userIds: Set[String], message: JsObject)
  object SendTos {
    def apply[A: Writes](userIds: Set[String], typ: String, data: A): SendTos =
      SendTos(userIds, Json.obj("t" -> typ, "d" -> data))
  }
  case class RemoteSocketTellSriIn(sri: String, user: Option[String], data: JsObject)
  case class RemoteSocketTellSriOut(sri: String, payload: JsValue)
}

package report {
  case class Cheater(userId: String, text: String)
  case class Shutup(userId: String, text: String)
  case class Booster(winnerId: String, loserId: String)
}

package security {
  case class GarbageCollect(userId: String, ipBan: Boolean)
  case class GCImmediateSb(userId: String)
  case class CloseAccount(userId: String)
}

package shutup {
  case class RecordPublicForumMessage(userId: String, text: String)
  case class RecordTeamForumMessage(userId: String, text: String)
  case class RecordPrivateMessage(userId: String, toUserId: String, text: String)
  case class RecordPrivateChat(chatId: String, userId: String, text: String)
  case class RecordPublicChat(userId: String, text: String, source: PublicSource)

  sealed trait PublicSource
  object PublicSource {
    case class Tournament(id: String) extends PublicSource
    case class Simul(id: String) extends PublicSource
    case class Study(id: String) extends PublicSource
    case class Watcher(gameId: String) extends PublicSource
    case class OlClass(id: String) extends PublicSource
    case class TrainCourse(id: String) extends PublicSource
  }
}

package mod {
  case class MarkCheater(userId: String, value: Boolean)
  case class MarkBooster(userId: String)
  case class ChatTimeout(mod: String, user: String, reason: String)
  case class Shadowban(user: String, value: Boolean)
  case class KickFromRankings(userId: String)
  case class SetPermissions(userId: String, oldPermissions: List[String], newPermissions: List[String])
}

package playban {
  case class Playban(userId: String, mins: Int)
}

package captcha {
  case object AnyCaptcha
  case class GetCaptcha(id: String)
  case class ValidCaptcha(id: String, solution: String)
}

package smsCaptcha {
  import lila.common.SmsKey
  case class SendGeneral(key: SmsKey)
  case class SendSmsCaptcha(key: SmsKey)
  case class ValidSmsCaptcha(key: SmsKey, code: String)
}

package lobby {
  case class ReloadTournaments(html: String)
  case class ReloadSimuls(html: String)
}

package home {
  case class ReloadTask(userIds: List[String])
  case class ReloadAppt(userIds: List[String])
  case class ReloadCalendar(userIds: List[String])
}

package simul {
  case class GetHostIds(promise: Promise[Set[String]])
  case class PlayerMove(gameId: String)
}

package slack {
  sealed trait Event
  case class Error(msg: String) extends Event
  case class Warning(msg: String) extends Event
  case class Info(msg: String) extends Event
  case class Victory(msg: String) extends Event
  case class TournamentName(userName: String, tourId: String, tourName: String) extends Event
}

package timeline {
  case class ReloadTimelines(userIds: List[String])

  sealed abstract class Atom(val channel: String, val okForKid: Boolean) {
    def userIds: List[String]
  }
  case class Follow(u1: String, u2: String) extends Atom("follow", true) {
    def userIds = List(u1, u2)
  }
  case class TeamJoin(userId: String, teamId: String) extends Atom("teamJoin", false) {
    def userIds = List(userId)
  }
  case class TeamCreate(userId: String, teamId: String) extends Atom("teamCreate", false) {
    def userIds = List(userId)
  }
  case class ForumPost(userId: String, topicId: Option[String], topicName: String, postId: String) extends Atom(s"forum:${~topicId}", false) {
    def userIds = List(userId)
  }
  case class NoteCreate(from: String, to: String) extends Atom("note", false) {
    def userIds = List(from, to)
  }
  case class TourJoin(userId: String, tourId: String, tourName: String) extends Atom("tournament", true) {
    def userIds = List(userId)
  }
  case class GameEnd(playerId: String, opponent: Option[String], win: Option[Boolean], perf: String) extends Atom("gameEnd", true) {
    def userIds = opponent.toList
  }
  case class SimulCreate(userId: String, simulId: String, simulName: String) extends Atom("simulCreate", true) {
    def userIds = List(userId)
  }
  case class SimulJoin(userId: String, simulId: String, simulName: String) extends Atom("simulJoin", true) {
    def userIds = List(userId)
  }
  case class StudyCreate(userId: String, studyId: String, studyName: String) extends Atom("studyCreate", true) {
    def userIds = List(userId)
  }
  case class StudyLike(userId: String, studyId: String, studyName: String) extends Atom("studyLike", true) {
    def userIds = List(userId)
  }
  case class PlanStart(userId: String) extends Atom("planStart", true) {
    def userIds = List(userId)
  }
  case class BlogPost(id: String, slug: String, title: String) extends Atom("blogPost", true) {
    def userIds = Nil
  }
  case class StreamStart(id: String, name: String) extends Atom("streamStart", true) {
    def userIds = List(id)
  }

  object propagation {
    sealed trait Propagation
    case class Users(users: List[String]) extends Propagation
    case class Followers(user: String) extends Propagation
    case class Friends(user: String) extends Propagation
    case class ExceptUser(user: String) extends Propagation
    case class ModsOnly(value: Boolean) extends Propagation
  }

  import propagation._

  case class Propagate(data: Atom, propagations: List[Propagation] = Nil) {
    def toUsers(ids: List[String]) = add(Users(ids))
    def toUser(id: String) = add(Users(List(id)))
    def toFollowersOf(id: String) = add(Followers(id))
    def toFriendsOf(id: String) = add(Friends(id))
    def exceptUser(id: String) = add(ExceptUser(id))
    def modsOnly(value: Boolean) = add(ModsOnly(value))
    private def add(p: Propagation) = copy(propagations = p :: propagations)
  }
}

package game {
  case class ApptProcessBus(gameId: String, user: String, source: String)
  case class ApptCompleteBus(gameId: String, user: Option[String], time: DateTime, source: String)
  case class ChangeFeatured(id: String, msg: JsObject)
  case object Count
}

package tv {
  case class Select(msg: JsObject)
}

package notify {
  case class Notified(userId: String)
}

package coach {
  case class Certify(userId: String, bool: Boolean)
  case class IsCoachOfUser(coachId: String, studentId: String, promise: Promise[Boolean])
}

package team {
  case class CreateTeam(id: String, name: String, userId: String)
  case class DisableTeam(id: String, name: String, userId: String)
  case class EnableTeam(id: String, name: String, userId: String)
  case class JoinTeam(id: String, userId: String)
  case class SetOwner(id: String, name: String, certified: Boolean, userId: String)
  case class UnsetOwner(id: String, name: String, certified: Boolean, userId: String)
  case class SetCampusAdmin(id: String, campusId: String, userId: String)
  case class UnsetCampusAdmin(id: String, campusId: String, userId: String)
  case class Certify(id: String, userId: String, bool: Boolean)

  case class IsManagerOfUser(managerId: String, memberId: String, promise: Promise[Boolean])
  case class IsManagerOfCampus(managerId: String, campusId: String, promise: Promise[Boolean])

  case class TeamCooperator(teamId: String, promise: Promise[Option[String]])

  case class TeamCoinChangeFromHomework(teamId: String, userId: String, homeworkId: String, coinDiff: Int, coinName: String, note: String)
  case class TeamCoinChangeFromTTask(teamId: String, userId: String, taskId: String, coinDiff: Int, coinName: String, note: String)
  case class TeamCoinChangeFromSetting(teamId: String, userId: String, coinDiff: Int, coinName: String, note: String)
}

package fishnet {
  case class AutoAnalyse(gameId: String)
  case class NewKey(userId: String, key: String)
  case class StudyChapterRequest(
      studyId: String,
      chapterId: String,
      initialFen: Option[chess.format.FEN],
      variant: chess.variant.Variant,
      moves: List[Uci],
      userId: String
  )
}

package user {
  case class Note(from: String, to: String, text: String, mod: Boolean)
  case class UserSignup(userId: String)
}

package round {
  case class MoveEvent(
      gameId: String,
      fen: String,
      move: String,
      turnColor: Option[String] = None,
      whiteRemainingSeconds: Option[Int] = None,
      blackRemainingSeconds: Option[Int] = None
  )
  case class CorresMoveEvent(
      move: MoveEvent,
      playerUserId: Option[String],
      mobilePushable: Boolean,
      alarmable: Boolean,
      unlimited: Boolean
  )
  case class CorresTakebackOfferEvent(gameId: String)
  case class CorresDrawOfferEvent(gameId: String)
  case class SimulMoveEvent(
      move: MoveEvent,
      simulId: String,
      opponentUserId: String
  )
  case class NbRounds(nb: Int)
  case class Berserk(gameId: String, userId: String)
  case class IsOnGame(color: chess.Color, promise: Promise[Boolean])
  sealed trait SocketEvent
  case class TourStanding(json: JsArray)
  case class FishnetPlay(uci: Uci, currentFen: chess.format.FEN)
  case class BotPlay(playerId: String, uci: Uci, promise: Option[scala.concurrent.Promise[Unit]] = None)
  case class RematchOffer(gameId: String)
  case class RematchYes(playerId: String)
  case class RematchNo(playerId: String)
  case class Abort(playerId: String)
  case class Resign(playerId: String)
  case class Mlat(ms: Int)
  case class FinishGameHub(gameId: String, win: Option[String])

}

package evaluation {
  case class AutoCheck(userId: String)
  case class Refresh(userId: String)
}

package bookmark {
  case class Toggle(gameId: String, userId: String)
  case class Remove(gameId: String)
}

package relation {
  case class ReloadOnlineFriends(userId: String)
  case class Block(u1: String, u2: String)
  case class UnBlock(u1: String, u2: String)
  case class Follow(u1: String, u2: String)
  case class SetMark(u1: String, u2: String, mark: Option[String])
  case class GetMark(u1: String, u2: String)
  case class GetMarks(u1: String)
}

package study {
  case class StudyDoor(userId: String, studyId: String, contributor: Boolean, public: Boolean, enters: Boolean)
  case class StudyBecamePrivate(studyId: String, contributors: Set[String])
  case class StudyBecamePublic(studyId: String, contributors: Set[String])
  case class StudyMemberGotWriteAccess(userId: String, studyId: String)
  case class StudyMemberLostWriteAccess(userId: String, studyId: String)
  case class RemoveStudy(studyId: String, contributors: Set[String])
}

package plan {
  case class ChargeEvent(username: String, amount: Int, percent: Int, date: DateTime)
  case class MonthInc(userId: String, months: Int)
}

package clazz {
  case class ClazzJoinAccept(clazzId: String, clazzName: String, coachId: String, studentId: String)
  case class ChangeCoach(clazzId: String, oldCoachId: String, newCoachId: String, clazzStudentIds: List[String], excludeStudentIds: List[String])

  case class IsCoachOfUser(coachId: String, studentId: String, promise: Promise[Boolean])

  case class HomeworkFinish(userId: String, creator: String, homeworkId: String, homeworkName: String, coinRule: Option[Int], coinDiff: Option[Int], oldCoinDiff: Option[Int])
  case class HomeworkDeadline(userId: String, creator: String, homeworkId: String, homeworkName: String, coinRule: Option[Int], coinDiff: Option[Int], oldCoinDiff: Option[Int])
  case class HomeworkUpdateCoin(userId: String, creator: String, homeworkId: String, homeworkName: String, coinRule: Option[Int], coinDiff: Option[Int], oldCoinDiff: Option[Int])
}

package contest {
  case class MiniBoardPlayer(no: Int, userId: String, teamerNo: Option[Int], signed: Boolean)
  case class MiniBoard(id: String, no: Int, roundId: String, roundNo: Int, pairedNo: Option[Int], status: Int, startsAt: DateTime, whitePlayer: MiniBoardPlayer, blackPlayer: MiniBoardPlayer) {
    def secondsToStart = (startsAt.getSeconds - nowSeconds).toInt atLeast 0
    def startStatus = secondsToStart |> { s => "%02d:%02d".format(s / 60, s % 60) }
    def isCreated = status == 10
    def boardName = s"第${roundNo}轮 #${pairedNo.fold(s"$no") { pno => s"$pno.$no" }}"
  }
  case class ContestBoard(contestId: String, contestFullName: String, setup: String, isTeam: Boolean, teamRated: Boolean, board: MiniBoard)
  case class GetContestBoard(gameId: String)
  case class ContestBoardSigned(gameId: String, userId: String)
}

package offContest {

  case class OffContestUser(
      userId: String,
      realName: String,
      external: Boolean,
      teamRating: Option[Int],
      isWinner: Option[Boolean] = None
  )

  case class OffContestBoard(
      id: String,
      white: OffContestUser,
      black: OffContestUser,
      isAbsent: Boolean // 有选手弃权的情况
  )

  case class OffContestRoundResult(
      contestId: String,
      contestFullName: String,
      teamId: Option[String],
      teamRated: Boolean,
      roundNo: Int,
      boards: List[OffContestBoard]
  )
}

package calendar {

  case class CalendarCreate(
      id: Option[String],
      typ: String,
      user: String,
      sdt: DateTime,
      edt: DateTime,
      content: String,
      onlySdt: Boolean,
      link: Option[String],
      icon: Option[String],
      bg: Option[String]
  )

  case class CalendarsCreate(calendars: List[CalendarCreate])

  case class CalendarRemove(id: String)

  case class CalendarsRemove(ids: List[String])
}

package resource {

  case class PuzzleResourceRemove(ids: List[Int])

}

case class Recall(id: Option[String], hashId: Option[String], win: Boolean, turns: Int, userId: String)

case class Distinguish(id: Option[String], hashId: Option[String], win: Boolean, turns: Int, userId: String)

package member {

  case class MemberBuyPayed(
      orderId: String,
      userId: String,
      level: String,
      totalAmount: BigDecimal,
      payAmount: BigDecimal,
      days: Int,
      points: Option[Int],
      coupon: Option[String],
      inviteUser: Option[String],
      desc: String
  )

  case class MemberCardBuyPayed(
      orderId: String,
      userId: String,
      level: String,
      totalAmount: BigDecimal,
      payAmount: BigDecimal,
      points: Option[Int],
      coupon: Option[String],
      desc: String
  )

  case class OpeningdbSysBuyPayed(
      orderId: String,
      userId: String,
      openingdbId: String,
      totalAmount: BigDecimal,
      payAmount: BigDecimal,
      days: Int,
      points: Option[Int],
      coupon: Option[String],
      inviteUser: Option[String],
      desc: String
  )

  case class OpeningdbCreateBuyPayed(
      orderId: String,
      userId: String,
      count: Int,
      totalAmount: BigDecimal,
      payAmount: BigDecimal,
      points: Option[Int],
      coupon: Option[String],
      inviteUser: Option[String],
      desc: String
  )

  case class MemberBuyComplete(orderId: String, userId: String)

  case class MemberCardBuyComplete(orderId: String, userId: String)

  case class OpeningdbSysBuyComplete(orderId: String, userId: String)

  case class OpeningdbCreateBuyComplete(orderId: String, userId: String)

  case class OpeningdbUpdatePrice(id: String, price: BigDecimal)

  case class MemberLevelChange(
      userId: String,
      typ: String,
      level: String,
      oldExpireAt: Option[DateTime],
      newExpireAt: Option[DateTime],
      desc: String,
      orderId: Option[String],
      cardId: Option[String]
  )

  case class MemberPointsSet(userId: String, diff: Int, orderId: Option[String])

  case class TeamInviteRel(teamId: String, userId: String)

  case class MemberPointsChange(userId: String, typ: String, diff: Int, orderId: Option[String], teamRel: Option[TeamInviteRel] = None)

  case class MemberCoinChange(userId: String, typ: String, diff: Int, orderId: Option[String])

  case class MemberCardUse(userId: String, level: String, days: Int, desc: String, cardId: String)

  case class MemberExchangeCardUse(userId: String, level: String, days: Int, desc: String, cardId: Int)

  //  sealed trait Typ
  //  case object Puzzle extends Typ
  //  case object ThemePuzzle extends Typ
  //  case object TotalPuzzle extends Typ
  //  case object Rush extends Typ
  //  case object AiPlay extends Typ
  //  case object Analyse extends Typ
  case class IsAccept(userId: String, typ: String)

}

package puzzle {

  case class NextPuzzle(puzzleId: Int, userId: String)

  case class NextThemePuzzle(puzzleId: Int, userId: String, query: ThemeQuery, saveQuery: Boolean)

  case class NextRushPuzzle(puzzleId: Int, userId: String)

  case class NextRacePuzzle(puzzleId: Int, userId: String)

  case class NextHomeworkPuzzle(puzzleId: Int, userId: String)

  case class NextTaskPuzzle(puzzleId: Int, userId: String)

  case class NextErrorPuzzle(puzzleId: Int, userId: String)

  case class NextCapsulePuzzle(capsuleId: String, puzzleId: Int, userId: String)

  case class StartPuzzleRush(rushId: String, userId: String)

  case class ThemeQuery(
      ratingMin: Option[Int] = None,
      ratingMax: Option[Int] = None,
      stepsMin: Option[Int] = None,
      stepsMax: Option[Int] = None,
      tags: Option[List[String]] = None,
      phase: Option[List[String]] = None,
      moveFor: Option[List[String]] = None,
      pieceColor: Option[List[String]] = None,
      subject: Option[List[String]] = None,
      strength: Option[List[String]] = None,
      chessGame: Option[List[String]] = None,
      comprehensive: Option[List[String]] = None
  ) {

    def rawString =
      ratingMin.?? { rm => s"ratingMin=$rm" } +
        ratingMax.?? { rm => s"&ratingMax=$rm" } +
        stepsMin.?? { sm => s"&stepsMin=$sm" } +
        stepsMax.?? { sm => s"&stepsMax=$sm" } +
        tags.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&tags[$index]=$tag"
          }
        } + phase.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&phase[$index]=$tag"
          }
        } + moveFor.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&moveFor[$index]=$tag"
          }
        } + pieceColor.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&pieceColor[$index]=$tag"
          }
        } + subject.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&subject[$index]=$tag"
          }
        } + strength.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&strength[$index]=$tag"
          }
        } + chessGame.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&chessGame[$index]=$tag"
          }
        } + comprehensive.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&comprehensive[$index]=$tag"
          }
        }
  }

}

package analyse {

  case class AnalysisStart(gameId: String, userId: String)

}

package coordTrain {

  //mode=basic&orient=1&coordShow=1&limitTime=1
  case class TrainData(userId: String, mode: String, color: Int, coord: Int, limit: Int, score: Int) {
    def search = s"mode=$mode&orient=$color&coordShow=$coord&limitTime=$limit"
  }

}

package task {

  case class CreateTask(taskId: String, sourceRel: SourceRelData, userId: String)

  case class ChangeStatus(taskId: String, taskName: String, sourceRel: SourceRelData, userId: String, creator: String, status: String, num: Int, coinRule: Option[Int])

  case class ChangeProgress(taskId: String, sourceRel: SourceRelData, userId: String, num: Int, progress: Double)

  case class CreateTrainGameTask(taskId: String, taskName: String, whiteUser: String, blackUser: String, sourceRel: SourceRelData)

  case class StartTrainGameTask(taskId: String, taskName: String, whiteUser: String, blackUser: String, gameId: String, sourceRel: SourceRelData)

  case class FinishTrainGameTask(taskId: String, taskName: String, whiteUser: String, blackUser: String, gameId: String, sourceRel: SourceRelData)

  case class SourceRelData(id: String, name: String, source: String)

}

