package lila.resource

import lila.user.User
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.resource.DataForm.gameShare.LogSearchData
import reactivemongo.bson.{ BSONDocument, Macros }

final class GameShareApi(coll: Coll, gamedbRelApi: GameDBRelApi, gameShareRelApi: GameShareRelApi, bus: lila.common.Bus) {

  implicit val studentArrayHandler = bsonArrayToListHandler[String]
  implicit val GameShareRelHandler = Macros.handler[GameDBRelMini]
  implicit val GameShareRelArrayHandler = bsonArrayToListHandler[GameDBRelMini]
  implicit val GameShareHandler = Macros.handler[GameShare]

  def byId(id: GameShare.ID): Fu[Option[GameShare]] = coll.byId[GameShare](id)

  def relPage(userId: User.ID, search: LogSearchData, page: Int): Fu[Paginator[GameShare]] = {
    var $selector = $doc("students" -> userId)
    search.shareBy foreach { shareBy =>
      $selector = $selector ++ $doc("createBy" -> shareBy)
    }
    if (search.dateMin.isDefined || search.dateMax.isDefined) {
      var dateRange = $doc()
      search.dateMin foreach { dateMin =>
        dateRange = dateRange ++ $gte(dateMin.withTimeAtStartOfDay())
      }
      search.dateMax foreach { dateMax =>
        dateRange = dateRange ++ $lte(dateMax.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999))
      }
      $selector = $selector ++ $doc("createAt" -> dateRange)
    }

    val adapter = new Adapter[GameShare](
      collection = coll,
      selector = $selector,
      projection = $empty,
      sort = $sort desc "createAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def page(userId: User.ID, search: LogSearchData, page: Int): Fu[Paginator[GameShare]] = {
    var $selector = $doc("createBy" -> userId)
    if (search.dateMin.isDefined || search.dateMax.isDefined) {
      var dateRange = $doc()
      search.dateMin foreach { dateMin =>
        dateRange = dateRange ++ $gte(dateMin.withTimeAtStartOfDay())
      }
      search.dateMax foreach { dateMax =>
        dateRange = dateRange ++ $lte(dateMax.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999))
      }
      $selector = $selector ++ $doc("createAt" -> dateRange)
    }

    val adapter = new Adapter[GameShare](
      collection = coll,
      selector = $selector,
      projection = $empty,
      sort = $sort desc "createAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def create(data: DataForm.gameShare.ShareData, userId: User.ID): Funit = {
    gamedbRelApi.byIds(data.relIds) flatMap { rels =>
      val share = GameShare.make(
        rels = rels.map(rel => GameDBRelMini(rel.id, rel.name, rel.gameId)),
        clazzId = data.clazzId,
        students = data.students,
        remark = data.remark,
        userId = userId
      )

      val shareRels = rels.flatMap { rel =>
        data.students.map { student =>
          GameShareRel.make(
            shareId = share.id,
            student = student,
            relId = rel.id,
            gameId = rel.gameId,
            name = rel.name,
            tags = if (data.isShareTag) rel.tags else none,
            remark = data.remark,
            userId = userId
          )
        }
      }

      coll.insert(share) >> gameShareRelApi.batchInsert(shareRels)
    }
  }

  def rename(id: GameShare.ID, name: String): Funit =
    coll.update(
      $id(id),
      $set("name" -> name)
    ).void

  def remove(id: GameShare.ID): Funit =
    coll.remove($id(id)).void

}
