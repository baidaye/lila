package lila.resource

import lila.game.Game
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class GameShareRel(
    _id: GameShareRel.ID,
    shareId: GameShare.ID,
    student: User.ID,
    relId: GameDBRel.ID,
    gameId: String,
    name: String,
    tags: Option[List[String]],
    remark: Option[String],
    createAt: DateTime,
    createBy: User.ID // share by
) {

  def id = _id

}

object GameShareRel {

  type ID = String

  def make(
    shareId: GameShare.ID,
    student: User.ID,
    relId: GameDBRel.ID,
    gameId: String,
    name: String,
    tags: Option[List[String]],
    remark: Option[String],
    userId: User.ID
  ) = GameShareRel(
    _id = Random nextString 8,
    shareId = shareId,
    student = student,
    relId = relId,
    gameId = gameId,
    name = name,
    tags = tags,
    remark = remark,
    createAt = DateTime.now,
    createBy = userId
  )

  case class WithGame(rel: GameShareRel, game: Game)
}

