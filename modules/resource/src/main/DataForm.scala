package lila.resource

import chess.{ Color, format }
import play.api.data.{ Form, _ }
import play.api.data.Forms._
import lila.common.Form._
import lila.study.Study
import lila.user.User
import org.joda.time.DateTime
import play.api.libs.ws.WS

object DataForm {

  object puzzle {

    val liked = Form(mapping(
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText)),
      "order" -> optional(numberIn(Sorting.orders))
    )(LikedData.apply)(LikedData.unapply)) fill LikedData()

    val imported = Form(mapping(
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText)),
      "order" -> optional(numberIn(Sorting.orders))
    )(ImportedData.apply)(ImportedData.unapply)) fill ImportedData()

    val themeOr = Form(mapping(
      "idMin" -> optional(number(min = lila.hub.PuzzleHub.minThemeId, max = lila.hub.PuzzleHub.maxThemeId)),
      "idMax" -> optional(number(min = lila.hub.PuzzleHub.minThemeId, max = lila.hub.PuzzleHub.maxThemeId)),
      "ratingMin" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
      "ratingMax" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
      "stepsMin" -> optional(number(min = 1, max = 10)),
      "stepsMax" -> optional(number(min = 1, max = 10)),
      "tags" -> optional(list(nonEmptyText)),
      "phase" -> optional(list(stringIn(ThemeQuery.phase))),
      "moveFor" -> optional(list(stringIn(ThemeQuery.moveFor))),
      "pieceColor" -> optional(list(stringIn(ThemeQuery.pieceColor))),
      "subject" -> optional(list(stringIn(ThemeQuery.subject))),
      "strength" -> optional(list(stringIn(ThemeQuery.strength))),
      "chessGame" -> optional(list(stringIn(ThemeQuery.chessGame))),
      "comprehensive" -> optional(list(stringIn(ThemeQuery.comprehensive))),
      "order" -> optional(numberIn(Sorting.orders))
    )(ThemeDataOr.apply)(ThemeDataOr.unapply)) fill ThemeDataOr.default

    val themeAnd = Form(mapping(
      "idMin" -> optional(number(min = lila.hub.PuzzleHub.minThemeId, max = lila.hub.PuzzleHub.maxThemeId)),
      "idMax" -> optional(number(min = lila.hub.PuzzleHub.minThemeId, max = lila.hub.PuzzleHub.maxThemeId)),
      "ratingMin" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
      "ratingMax" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
      "stepsMin" -> optional(number(min = 1, max = 10)),
      "stepsMax" -> optional(number(min = 1, max = 10)),
      "tags" -> optional(list(nonEmptyText)),
      "phase" -> optional(list(stringIn(ThemeQuery.phase))),
      "moveFor" -> optional(list(stringIn(ThemeQuery.moveFor))),
      "pieceColor" -> optional(list(stringIn(ThemeQuery.pieceColor))),
      "subject" -> optional(list(stringIn(ThemeQuery.subject))),
      "strength" -> optional(list(stringIn(ThemeQuery.strength))),
      "chessGame" -> optional(list(stringIn(ThemeQuery.chessGame))),
      "comprehensive" -> optional(list(stringIn(ThemeQuery.comprehensive))),
      "order" -> optional(numberIn(Sorting.orders))
    )(ThemeDataAnd.apply)(ThemeDataAnd.unapply)) fill ThemeDataAnd()

    val machine = {
      Form(mapping(
        "idRange" -> number(min = 0, max = 15),
        "idMin" -> optional(number(min = 1, max = 100000)),
        "idMax" -> optional(number(min = 1, max = 100000)),
        "ratingMin" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
        "ratingMax" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
        "moves" -> optional(list(stringIn(ThemeQuery.moves))),
        "tags" -> optional(list(nonEmptyText)),
        "phase" -> optional(list(stringIn(ThemeQuery.phase))),
        "moveFor" -> optional(list(stringIn(ThemeQuery.moveForMachine))),
        "pieceColor" -> optional(list(stringIn(ThemeQuery.pieceColor))),
        "subject" -> optional(list(stringIn(ThemeQuery.subjectMachine))),
        "strength" -> optional(list(stringIn(ThemeQuery.strength))),
        "comprehensive" -> optional(list(stringIn(ThemeQuery.comprehensiveMachine))),
        "order" -> optional(nonEmptyText)
      )(MachineData.apply)(MachineData.unapply)) fill MachineData()
    }

    case class LikedData(
        emptyTag: Option[String] = None,
        tags: Option[List[String]] = None,
        order: Option[Int] = Sorting.default.order.some
    ) {

      val sortOrder = order getOrElse (Sorting.default.order)
    }

    case class ImportedData(
        emptyTag: Option[String] = None,
        tags: Option[List[String]] = None,
        order: Option[Int] = Sorting.default.order.some
    ) {

      val sortOrder = order getOrElse (Sorting.default.order)
    }

    case class ThemeDataOr(
        idMin: Option[Int] = None,
        idMax: Option[Int] = None,
        ratingMin: Option[Int] = None,
        ratingMax: Option[Int] = None,
        stepsMin: Option[Int] = None,
        stepsMax: Option[Int] = None,
        tags: Option[List[String]] = None,
        phase: Option[List[String]] = None,
        moveFor: Option[List[String]] = None,
        pieceColor: Option[List[String]] = None,
        subject: Option[List[String]] = None,
        strength: Option[List[String]] = None,
        chessGame: Option[List[String]] = None,
        comprehensive: Option[List[String]] = None,
        order: Option[Int] = Sorting.default.order.some
    ) {

      val sortOrder = order getOrElse (Sorting.default.order)

    }

    object ThemeDataOr {
      def default = ThemeDataOr(
        ratingMin = 800.some,
        ratingMax = 1300.some,
        stepsMin = 1.some,
        stepsMax = 5.some
      )
    }

    case class ThemeDataAnd(
        idMin: Option[Int] = None,
        idMax: Option[Int] = None,
        ratingMin: Option[Int] = None,
        ratingMax: Option[Int] = None,
        stepsMin: Option[Int] = None,
        stepsMax: Option[Int] = None,
        tags: Option[List[String]] = None,
        phase: Option[List[String]] = None,
        moveFor: Option[List[String]] = None,
        pieceColor: Option[List[String]] = None,
        subject: Option[List[String]] = None,
        strength: Option[List[String]] = None,
        chessGame: Option[List[String]] = None,
        comprehensive: Option[List[String]] = None,
        order: Option[Int] = Sorting.default.order.some
    ) {

      val sortOrder = order getOrElse (Sorting.default.order)
    }

    case class MachineData(
        idRange: Int = 0,
        idMin: Option[Int] = None,
        idMax: Option[Int] = None,
        ratingMin: Option[Int] = None,
        ratingMax: Option[Int] = None,
        moves: Option[List[String]] = None,
        tags: Option[List[String]] = None,
        phase: Option[List[String]] = None,
        moveFor: Option[List[String]] = None,
        pieceColor: Option[List[String]] = None,
        subject: Option[List[String]] = None,
        strength: Option[List[String]] = None,
        comprehensive: Option[List[String]] = None,
        order: Option[String] = None
    ) {

      val sortField = order.map(_.split(":")(0)) getOrElse Sorting.default.field
      val sortOrder = order.map(_.split(":")(1).toInt) getOrElse Sorting.default.order
    }

  }

  object game {
    val liked = Form(tuple(
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    ))

    val imported = Form(tuple(
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    ))
  }

  object capsule {

    def createForm(u: User) =
      Form(mapping(
        "name" -> nonEmptyText(minLength = 2, maxLength = 20).verifying("名称已经存在", n => !CapsuleRepo.nameExists(n, None, u.id).awaitSeconds(3)),
        "tags" -> optional(nonEmptyText),
        "desc" -> optional(nonEmptyText)
      )(CapsuleCreateData.apply)(CapsuleCreateData.unapply))

    def updateFormOf(u: User, capsule: Capsule) =
      updateForm(u, capsule.id.some) fill CapsuleUpdateData(name = capsule.name, status = capsule.status.id, tags = capsule.tags.mkString(",").some, desc = capsule.desc)

    def updateForm(u: User, id: Option[Capsule.ID]) =
      Form(mapping(
        "name" -> nonEmptyText(minLength = 2, maxLength = 20).verifying("名称已经存在", n => !CapsuleRepo.nameExists(n, id, u.id).awaitSeconds(3)),
        "status" -> boolean,
        "tags" -> optional(nonEmptyText),
        "desc" -> optional(nonEmptyText)
      )(CapsuleUpdateData.apply)(CapsuleUpdateData.unapply))

    def searchForm = Form(mapping(
      "name" -> optional(nonEmptyText),
      "status" -> optional(boolean),
      "visibility" -> optional(stringIn(CapsuleVisibility.selects)),
      "role" -> optional(stringIn(CapsuleMember.Role.memberSelects)),
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    )(CapsuleSearchData.apply)(CapsuleSearchData.unapply))

    def sortForm = Form(single("sort" -> numberIn(Seq(-1, 1))))

    def changeOwnerForm = Form(single("username" -> lila.user.DataForm.historicalUsernameField))

    def visibilityForm = Form(single("visibility" -> stringIn(CapsuleVisibility.selects)))

    def addMemberForm = Form(single("username" -> lila.user.DataForm.historicalUsernameField))

    def setMemberRole = Form(single("role" -> stringIn(CapsuleMember.Role.selects)))

    case class CapsuleSearchData(
        name: Option[String] = None,
        status: Option[Boolean] = None,
        visibility: Option[String] = None,
        role: Option[String] = None,
        emptyTag: Option[String] = None,
        tags: Option[List[String]] = None
    )

    case class CapsuleCreateData(
        name: String,
        tags: Option[String],
        desc: Option[String]
    ) {

      def makeTags =
        tags.map { tg =>
          val t = tg.trim()
          if (t.isEmpty) List.empty else t.split(",").toList
        } | List.empty
    }

    case class CapsuleUpdateData(
        name: String,
        status: Boolean,
        tags: Option[String],
        desc: Option[String]
    ) {

      def makeTags =
        tags.map { tg =>
          val t = tg.trim()
          if (t.isEmpty) List.empty else t.split(",").toList
        } | List.empty
    }

    val puzzleOrder = Form(single("order" -> optional(numberIn(Sorting.orders))))
  }

  object gamedb {

    val search = Form(mapping(
      "name" -> optional(nonEmptyText),
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    )(SearchData.apply)(SearchData.unapply))

    val create = Form(mapping(
      "parent" -> nonEmptyText,
      "name" -> nonEmptyText(minLength = 2, maxLength = 20),
      "sort" -> number(min = 1, max = 1000)
    )(CreateData.apply)(CreateData.unapply))

    val rename = Form(single(
      "name" -> nonEmptyText(minLength = 2, maxLength = 20)
    ))

    val move = Form(tuple(
      "parent" -> nonEmptyText,
      "children" -> list(nonEmptyText)
    ))

    case class SearchData(
        name: Option[String],
        emptyTag: Option[String],
        tags: Option[List[String]]
    )

    case class CreateData(
        parent: String,
        name: String,
        sort: Int
    )
  }

  object gameDBRel {

    import play.api.data.validation.Constraints
    import scala.util.matching.Regex
    import play.api.Play.current

    private val ProdGameRegex = """https://haichess\.com/(\w{8})(\w{4})?/?(white|black)?""".r
    private val DevGameRegex = """http://localhost/(\w{8})(\w{4})?/?(white|black)?""".r
    private val ProdChapterRegex = """https://haichess.com/study/(\w{8})/(\w{8})""".r
    private val DevChapterRegex = """http://localhost/study/(\w{8})/(\w{8})""".r

    def isProd = lila.common.PlayApp.isProd

    def gameRegex = if (isProd) ProdGameRegex else DevGameRegex

    def chapterRegex = if (isProd) ProdChapterRegex else DevChapterRegex

    def gameId(url: String) = group(gameRegex, url, 1)

    def studyId(url: String) = group(chapterRegex, url, 1)

    def chapterId(url: String) = group(chapterRegex, url, 2)

    def group(r: Regex, s: String, g: Int): String = {
      val m = r.pattern.matcher(s)
      if (m.find) {
        m.group(g).some
      } else None
    } err s"can not find regex group of $s"

    def urlTest(url: String): Fu[Boolean] = WS.url(url).get().map(r => r.status == 200)

    def chapterTest(url: String, user: User, findStudy: Study.Id => Fu[Option[Study]]): Fu[Boolean] = {
      findStudy(Study.Id(studyId(url))).map {
        _.?? { study =>
          study.isPublic || study.members.contains(user.id)
        }
      }
    }

    def create(user: User, findStudy: Study.Id => Fu[Option[Study]]) = Form(mapping(
      "name" -> nonEmptyText,
      "tags" -> optional(nonEmptyText),
      "tab" -> nonEmptyText,
      "chapter" -> optional(
        nonEmptyText.verifying(Constraints.pattern(regex = chapterRegex, error = "章节URL格式错误"))
          .verifying("您无权使用这个研习.", url => chapterTest(url, user, findStudy).awaitSeconds(3))
      ),
      "game" -> optional(
        nonEmptyText.verifying(Constraints.pattern(regex = gameRegex, error = "对局URL格式错误"))
          .verifying("对局URL无法识别", url => urlTest(url).awaitSeconds(3))
      ),
      "pgn" -> optional(nonEmptyText)
    )(CreateData.apply)(CreateData.unapply))

    case class CreateData(
        name: String,
        tags: Option[String],
        tab: String,
        chapter: Option[String],
        game: Option[String],
        pgn: Option[String]
    ) {

      def allEmpty = chapter.isEmpty && game.isEmpty && pgn.isEmpty

      def gameId(url: String) = gameDBRel.gameId(url)

      def studyId(url: String) = gameDBRel.studyId(url)

      def chapterId(url: String) = gameDBRel.chapterId(url)

    }

    def editOf(rel: GameDBRel) =
      edit fill EditData(rel.name, rel.tags.map(_.mkString(",")))

    val edit = Form(mapping(
      "name" -> nonEmptyText,
      "tags" -> optional(nonEmptyText)
    )(EditData.apply)(EditData.unapply))

    case class EditData(
        name: String,
        tags: Option[String]
    ) {

      def tagsSet = tags.map(_.split(",").toSet)

    }

    def copyOrMove = Form(mapping(
      "action" -> nonEmptyText,
      "source" -> nonEmptyText,
      "frGamedb" -> optional(nonEmptyText),
      "toGamedb" -> nonEmptyText,
      "rels" -> nonEmptyText
    )(CopyOrMoveData.apply)(CopyOrMoveData.unapply))

    case class CopyOrMoveData(
        action: String, // copy, move
        source: String, // import, like, share, gamedb
        frGamedb: Option[String], // exists when source is gamedb
        toGamedb: String,
        rels: String // rel or game ids
    ) {
      def relIds = rels.split(",").toList
    }

  }

  object gameShare {

    def share = Form(mapping(
      "rels" -> nonEmptyText,
      "clazzId" -> optional(nonEmptyText),
      "students" -> list(nonEmptyText),
      "isShareTag" -> boolean,
      "remark" -> optional(nonEmptyText)
    )(ShareData.apply)(ShareData.unapply))

    case class ShareData(
        rels: String,
        clazzId: Option[String],
        students: List[String],
        isShareTag: Boolean,
        remark: Option[String]
    ) {
      def relIds = rels.split(",").toList
    }

    val search = Form(mapping(
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    )(SearchData.apply)(SearchData.unapply))

    case class SearchData(
        emptyTag: Option[String],
        tags: Option[List[String]]
    )

    val logSearch = Form(mapping(
      "shareBy" -> optional(nonEmptyText),
      "dateMin" -> optional(ISODate.isoDate),
      "dateMax" -> optional(ISODate.isoDate)
    )(LogSearchData.apply)(LogSearchData.unapply))

    case class LogSearchData(
        shareBy: Option[String],
        dateMin: Option[DateTime],
        dateMax: Option[DateTime]
    )

    def editOf(rel: GameShareRel) =
      edit fill EditData(rel.name, rel.tags.map(_.mkString(",")))

    val edit = Form(mapping(
      "name" -> nonEmptyText,
      "tags" -> optional(nonEmptyText)
    )(EditData.apply)(EditData.unapply))

    case class EditData(
        name: String,
        tags: Option[String]
    ) {
      def tagsSet = tags.map(_.split(",").toSet)
    }

  }

  object situationdb {

    val search = Form(mapping(
      "standard" -> optional(boolean),
      "name" -> optional(nonEmptyText),
      "emptyTag" -> optional(nonEmptyText),
      "tags" -> optional(list(nonEmptyText))
    )(SearchData.apply)(SearchData.unapply))

    val create = Form(mapping(
      "parent" -> nonEmptyText,
      "name" -> nonEmptyText(minLength = 2, maxLength = 20),
      "sort" -> number(min = 1, max = 1000)
    )(CreateData.apply)(CreateData.unapply))

    val rename = Form(single(
      "name" -> nonEmptyText(minLength = 2, maxLength = 20)
    ))

    val move = Form(tuple(
      "parent" -> nonEmptyText,
      "children" -> list(nonEmptyText)
    ))

    case class SearchData(
        standard: Option[Boolean],
        name: Option[String],
        emptyTag: Option[String],
        tags: Option[List[String]]
    )

    case class CreateData(
        parent: String,
        name: String,
        sort: Int
    )
  }

  object situationdbRel {

    def createOf(fen: Option[String], looksLegit: Option[Boolean] = None) =
      create fill CreateData(fen | format.Forsyth.initial, looksLegit | true, none, none, none, none)

    val create = Form(mapping(
      "fen" -> nonEmptyText,
      "standard" -> boolean,
      "name" -> optional(nonEmptyText),
      "score" -> optional(nonEmptyText),
      "tags" -> optional(nonEmptyText),
      "instruction" -> optional(nonEmptyText)
    )(CreateData.apply)(CreateData.unapply))

    case class CreateData(
        fen: String,
        standard: Boolean,
        name: Option[String],
        score: Option[String],
        tags: Option[String],
        instruction: Option[String]
    ) {

      def tagList = tags.map(_.split(",").toList.distinct)

      def color = (chess.format.Forsyth << fen).map { situation =>
        situation.color.name
      } | Color.white.name

    }

    def editOf(rel: SituationDBRel) =
      edit fill EditData(rel.fen, rel.standard, rel.name, rel.score, rel.tags.map(_.mkString(",")), rel.instruction)

    val edit = Form(mapping(
      "fen" -> nonEmptyText,
      "standard" -> boolean,
      "name" -> optional(nonEmptyText),
      "score" -> optional(nonEmptyText),
      "tags" -> optional(nonEmptyText),
      "instruction" -> optional(nonEmptyText)
    )(EditData.apply)(EditData.unapply))

    case class EditData(
        fen: String,
        standard: Boolean,
        name: Option[String],
        score: Option[String],
        tags: Option[String],
        instruction: Option[String]
    ) {

      def tagList = tags.map(_.split(",").toList.distinct)

      def color = (chess.format.Forsyth << fen).map { situation =>
        situation.color.name
      } | Color.white.name

    }

    def move = Form(mapping(
      "action" -> nonEmptyText,
      "frSituationdb" -> optional(nonEmptyText),
      "toSituationdb" -> nonEmptyText,
      "rels" -> nonEmptyText
    )(MoveData.apply)(MoveData.unapply))

    case class MoveData(
        action: String, // copy, move
        frSituationdb: Option[String],
        toSituationdb: String,
        rels: String
    ) {
      def relIds = rels.split(",").toList
    }
  }

}

