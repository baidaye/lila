package lila.resource

import lila.user.User
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import lila.resource.DataForm.capsule.{ CapsuleCreateData, CapsuleSearchData, CapsuleUpdateData }

final class CapsuleApi(bus: lila.common.Bus, notifyApi: NotifyApi) {

  def byId(id: Capsule.ID): Fu[Option[Capsule]] =
    CapsuleRepo.byId(id)

  def byIds(ids: List[Capsule.ID]): Fu[List[Capsule]] =
    CapsuleRepo.byIds(ids)

  def create(data: CapsuleCreateData, user: User): Fu[Capsule] = {
    val capsule = Capsule.make(data.name, data.desc, data.makeTags, user.id)
    CapsuleRepo.insert(capsule) inject capsule
  }

  def update(capsule: Capsule, data: CapsuleUpdateData, user: User): Funit =
    CapsuleRepo.update(capsule, data, user.id)

  def remove(capsule: Capsule): Funit =
    CapsuleRepo.remove(capsule.id)

  def clone(capsule: Capsule, user: User): Fu[Capsule] = {
    val newCapsule = capsule.clone(user.id)
    CapsuleRepo.insert(newCapsule) inject newCapsule
  }

  def setVisibility(capsule: Capsule, visibility: String, user: User): Funit =
    CapsuleRepo.setVisibility(capsule.id, CapsuleVisibility.apply(visibility), user.id)

  def changeOwner(capsule: Capsule, member: User, user: User): Funit =
    (!capsule.members.isOwner(member.id)).?? {
      CapsuleRepo.changeOwner(capsule.id, member.id, user.id) >> changeOwnerNotify(capsule, member)
    }

  def changeOwnerNotify(capsule: Capsule, member: User): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(capsule.ownerId).some,
      Notifies(member.id),
      lila.notify.GenericLink(
        url = s"/resource/capsule/${capsule.id}/update",
        title = "列表转移".some,
        text = s"${capsule.ownerId}用户将 ${capsule.name} 列表所有者转交给您，点击查看。".some,
        icon = "?"
      )
    ))
  }

  def addMember(capsule: Capsule, member: User): Funit =
    (!capsule.members.contains(member)).?? {
      CapsuleRepo.addMember(capsule.id, CapsuleMember make member)
    }

  def removeMemberById(id: Capsule.ID, member: User): Funit =
    byId(id).flatMap {
      _.?? { capsule =>
        removeMember(capsule, member)
      }
    }

  def removeMembers(capsules: List[Capsule], member: User): Funit = {
    val filteredCapsules = capsules.filter(_.isReaderOrWriter(member.id))
    CapsuleRepo.removeMembers(filteredCapsules.map(_.id), member.id)
  }

  def removeMember(capsule: Capsule, member: User): Funit =
    (capsule.members.contains(member) && !capsule.members.isOwner(member.id)).?? {
      CapsuleRepo.removeMember(capsule.id, member.id)
    }

  def quitMember(capsule: Capsule, member: User): Funit =
    (capsule.members.contains(member) && !capsule.members.isOwner(member.id)).?? {
      CapsuleRepo.quitMember(capsule.id, member.id)
    }

  def setMemberRoleById(id: Capsule.ID, member: User, role: String): Funit =
    byId(id).flatMap {
      _.?? { capsule =>
        setMemberRole(capsule, member, role)
      }
    }

  def setMemberRole(capsule: Capsule, member: User, role: String): Funit =
    (capsule.members.contains(member) && !capsule.members.isOwner(member.id)).?? {
      val r = CapsuleMember.Role.byId.getOrElse(role, CapsuleMember.Role.Read)
      CapsuleRepo.setMemberRole(capsule.id, member.id, r)
    }

  def addPuzzle(capsule: Capsule, puzzleIds: List[Int], user: User): Funit =
    CapsuleRepo.addPuzzle(capsule, puzzleIds, user.id)

  def delPuzzle(capsule: Capsule, puzzleIds: List[Int], user: User): Funit =
    CapsuleRepo.delPuzzle(capsule, puzzleIds, user.id)

  def reorderPuzzles(capsule: Capsule, puzzleIds: List[Int], user: User): Funit =
    CapsuleRepo.reorderPuzzles(capsule, puzzleIds, user.id)

  def sortPuzzles(capsule: Capsule, sortedPuzzleIds: List[Int], user: User): Funit = {
    sortedPuzzleIds.nonEmpty.?? {
      val idIndexMap = sortedPuzzleIds.zipWithIndex.toMap

      val indexIdMap = sortedPuzzleIds.zipWithIndex.map {
        case (id, index) => index -> id
      }.toMap

      var i = -1
      val newSortedPuzzleIds = capsule.ids.map { id =>
        id -> {
          idIndexMap.get(id).map { _ =>
            i = i + 1
            i
          }
        }
      }.map {
        case (id, indexOption) => {
          indexOption.fold(id) { index =>
            indexIdMap.get(index) err s"can not find puzzle of index $index"
          }
        }
      }

      CapsuleRepo.reorderPuzzles(capsule, newSortedPuzzleIds, user.id)
    }
  }

}
