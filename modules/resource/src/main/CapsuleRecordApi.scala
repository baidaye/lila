package lila.resource

import lila.user.User
import lila.db.dsl._
import reactivemongo.bson.Macros
import lila.hub.actorApi.puzzle.NextCapsulePuzzle

class CapsuleRecordApi(coll: Coll, bus: lila.common.Bus) {

  private implicit val CapsuleRecordBSONHandler = Macros.handler[CapsuleRecord]

  bus.subscribeFun('nextCapsulePuzzle) {
    case NextCapsulePuzzle(capsuleId, puzzleId, userId) => upsert(userId, capsuleId, puzzleId)
  }

  def lastId(capsuleId: String, userId: String): Fu[Option[Int]] =
    coll.byId(CapsuleRecord.makeId(capsuleId, userId)).map(_.map(_.puzzleId))

  def upsert(
    userId: User.ID,
    capsuleId: Capsule.ID,
    puzzleId: Int
  ): Funit = {
    val record = CapsuleRecord.make(userId, capsuleId, puzzleId)
    coll.update($id(record.id), record, upsert = true).void
  }

}
