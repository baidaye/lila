package lila.resource

import lila.base.PimpedFuture
import org.joda.time.DateTime
import lila.common.{ MaxPerPage, TreeNode }
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.user.User
import lila.game.{ Game, GameRepo }
import reactivemongo.bson.Macros
import lila.importer.ImportData
import lila.resource.DataForm.gamedb.SearchData
import lila.resource.GameDB.ID
import ornicar.scalalib.Random
import play.api.libs.json.JsArray
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.BSONDocument

final class GameDBRelApi(
    coll: Coll,
    bus: lila.common.Bus,
    studyApi: lila.study.StudyApi,
    importer: lila.importer.Importer,
    pgnDump: lila.game.PgnDump
) {

  implicit val TagsHandler = bsonArrayToListHandler[String]
  implicit val GameDBRelHandler = Macros.handler[GameDBRel]

  private def byGameDBId(gamedbId: GameDB.ID) = $doc("gamedbId" -> gamedbId)
  private def desc = $sort desc "createAt"

  def page(rels: List[GameDBRel]): Fu[Paginator[GameDBRel.WithGame]] = {
    GameRepo.gamesFromSecondary(rels.map(_.gameId)).map { games =>
      val list = rels zip games collect {
        case (rel, game) => GameDBRel.WithGame(rel, game)
      }
      Paginator.fromList(list, 1, MaxPerPage(list.size))
    }
  }

  def byId(id: GameDBRel.ID): Fu[Option[GameDBRel]] = coll.byId[GameDBRel](id)

  def byIds(ids: List[GameDBRel.ID]): Fu[List[GameDBRel]] = coll.byIds(ids)

  def countByDb(id: GameDB.ID): Fu[Int] = coll.countSel($doc("gamedbId" -> id))

  def findByIds(selected: GameDB.ID, data: SearchData, userId: User.ID, size: Int): Fu[List[GameDBRel]] = {
    var condition = $doc("gamedbId" -> selected)
    data.name.foreach { n =>
      condition = condition ++ $doc("name" $regex (n, "i"))
    }
    var $andConditions = List.empty[BSONDocument]
    data.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    data.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    condition = condition ++ (!$andConditions.empty).?? {
      $and($andConditions: _*)
    }
    //println(BSONDocument.pretty(condition))
    coll.find(condition)
      .sort(desc)
      .list[GameDBRel](size)
  }

  def tagsByUser(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("createBy" -> userId).some)

  def findByUser(userId: User.ID): Fu[List[GameDBRel]] =
    coll.find($doc("createBy" -> userId))
      .sort(desc)
      .list[GameDBRel]()

  def queryByName(userId: User.ID, q: String) =
    if (q.trim.nonEmpty) {
      coll.find($doc("createBy" -> userId, "name" $regex (q, "i"))).list[GameDBRel]().map { rels =>
        val nodes = rels.map { rel =>
          TreeNode(
            id = s"${rel.id}:${rel.gameId}",
            text = rel.name,
            parent = Some(rel.gamedbId),
            typ = Some("file"),
            icon = Some("jstree-file")
          )
        }
        TreeNode.toJsArray(nodes)
      }
    } else fuccess(JsArray())

  def create(gamedbId: String, data: DataForm.gameDBRel.CreateData, userId: User.ID): Fu[GameDBRel] = {
    data.tab match {
      case "pgn" => data.pgn match {
        case Some(pgn) => addGame(pgn, userId).map { game =>
          makeGameDBRel(gamedbId, data, game.id, userId)
        }
        case None => fufail("no available pgn")
      }
      case "chapter" => data.chapter match {
        case Some(chapter) => fetchChapterPgn(data.studyId(chapter), data.chapterId(chapter)) flatMap {
          case None => fufail("no available chapter")
          case Some(p) => addGame(p, userId).map { game =>
            makeGameDBRel(gamedbId, data, game.id, userId)
          }
        }
        case None => fufail("no available chapter")
      }
      case "game" => data.game match {
        case Some(g) => fuccess(makeGameDBRel(gamedbId, data, data.gameId(g), userId))
        case None => fufail("no available game")
      }
      case _ => fufail("can not apply tab")
    }
  } flatMap { rel =>
    coll.insert(rel) inject rel
  }

  private def fetchChapterPgn(studyId: String, chapterId: String): Fu[Option[String]] =
    studyApi.chapterPgn(studyId, chapterId)

  private def addGame(pgn: String, userId: User.ID): Fu[Game] =
    importer(
      ImportData(pgn, none),
      user = userId.some
    )

  private def makeGameDBRel(gamedbId: String, data: DataForm.gameDBRel.CreateData, gameId: String, userId: User.ID): GameDBRel =
    GameDBRel.make(
      name = data.name,
      tags = data.tags.map(_.split(",").toList),
      gamedbId = gamedbId,
      gameId = gameId,
      userId = userId
    )

  def update(id: GameDBRel.ID, name: String, tags: Option[Set[String]]): Funit =
    coll.update(
      $id(id),
      $set("name" -> name, "tags" -> tags)
    ).void

  def remove(ids: List[GameDBRel.ID], userId: String): Funit =
    coll.remove($inIds(ids)).void

  def removeByGameDB(gamedbIds: List[String]): Funit =
    coll.remove($doc("gamedbId" $in gamedbIds)).void

  def toCopyRels(data: DataForm.gameDBRel.CopyOrMoveData): Fu[List[GameDBRel]] =
    coll.find($inIds(data.relIds)).sort(desc).list[GameDBRel]().map { list =>
      list.map { rel =>
        rel.copy(
          _id = if (data.action == "move") rel.id else Random nextString 8,
          gamedbId = data.toGamedb,
          updateAt = DateTime.now
        )
      }
    }

  def copyOrMove(
    userId: String,
    data: DataForm.gameDBRel.CopyOrMoveData,
    gamedbRels: List[GameDBRel],
    remove: (List[String], String) => Funit
  ): Funit = (data.action == "move").?? { remove(data.relIds, userId) } >> bulkInsert(gamedbRels)

  def bulkInsert(rels: List[GameDBRel]): Funit =
    coll.bulkInsert(
      documents = rels.map(GameDBRelHandler.write).toStream,
      ordered = true
    ).void

}
