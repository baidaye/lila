package lila.resource

import play.api.libs.json._

final class CapsuleJsonView(
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi
) {

  def info(capsule: Capsule) =
    Json.obj(
      "id" -> capsule.id,
      "name" -> capsule.name,
      "total" -> capsule.total,
      "puzzles" -> capsule.ids,
      "tags" -> capsule.tags
    )

  def list(capsules: List[Capsule]) =
    JsArray(
      capsules.map(info)
    )

  def listWithTags(capsules: List[Capsule], tags: List[String]) =
    Json.obj(
      "list" -> list(capsules),
      "tags" -> JsArray(tags.map(JsString))
    )

  def capsulesWithPuzzles(capsules: List[Capsule], puzzles: List[CapsulePuzzle]) =
    JsArray(
      capsules.map { capsule =>
        val orderedPuzzles = capsule.ids.map(puzzleId => puzzles.find(_.id == puzzleId)).filter(_.isDefined).map(_.get)
        capsuleWithPuzzles(capsule, orderedPuzzles)
      }
    )

  def capsuleWithPuzzles(capsule: Capsule, puzzles: List[CapsulePuzzle]) = {
    info(capsule) ++ Json.obj("puzzles" -> puzzleList(puzzles))
  }

  def puzzleList(puzzles: List[CapsulePuzzle]) =
    JsArray(
      puzzles.map { puzzle =>
        Json.obj(
          "id" -> puzzle.id,
          "fen" -> puzzle.fen,
          "color" -> puzzle.color,
          "lastMove" -> puzzle.lastMove,
          "lines" -> puzzle.lines
        )
      }
    )

}

