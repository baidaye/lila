package lila.resource

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import reactivemongo.bson.BSONDocument

case class SituationDB(
    _id: SituationDB.ID,
    parent: SituationDB.ID,
    name: String,
    sort: Int,
    relSortBy: Option[SituationDB.SortBy],
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

}

object SituationDB {

  type ID = String

  sealed abstract class SortBy(val id: String, val name: String, val doc: BSONDocument)
  object SortBy {
    case object CreateAtDesc extends SortBy("CreateAtDesc", "时间逆序", $doc("createAt" -> -1))
    case object CreateAtAsc extends SortBy("CreateAtAsc", "时间正序", $doc("createAt" -> 1))
    case object NameDesc extends SortBy("NameDesc", "名称逆序", $doc("name" -> -1))
    case object NameAsc extends SortBy("NameAsc", "名称正序", $doc("name" -> 1))

    val default = CreateAtDesc

    val all = List(CreateAtDesc, CreateAtAsc, NameDesc, NameAsc)
    def byId = all map { v => (v.id, v) } toMap
    def choices = all.map(d => d.id -> d.name)
    def apply(id: String): SortBy = byId get id err s"Bad SortBy $id"
  }

  case class WithChildren(situationdb: SituationDB, children: List[SituationDB]) {
    def id = situationdb.id
    def parent = situationdb.parent
  }

  def make(parent: String, name: String, sort: Int, userId: User.ID) = SituationDB(
    _id = Random nextString 8,
    parent = parent,
    name = name,
    sort = sort,
    relSortBy = None,
    createAt = DateTime.now,
    createBy = userId
  )

}

