package lila.resource

import lila.db.BSON
import lila.db.dsl._
import reactivemongo.bson._

object CapsuleBSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler

  implicit val stringArrayHandler = bsonArrayToListHandler[String]

  implicit val StatusBSONHandler = new BSONHandler[BSONBoolean, CapsuleStatus] {
    def read(b: BSONBoolean) = CapsuleStatus(b.value)
    def write(x: CapsuleStatus) = BSONBoolean(x.id)
  }

  implicit val ItemTypeBSONHandler = new BSONHandler[BSONString, CapsuleItemType] {
    def read(b: BSONString) = CapsuleItemType(b.value)
    def write(x: CapsuleItemType) = BSONString(x.id)
  }

  implicit val VisibilityBSONHandler = new BSONHandler[BSONString, CapsuleVisibility] {
    def read(b: BSONString) = CapsuleVisibility(b.value)
    def write(x: CapsuleVisibility) = BSONString(x.id)
  }

  implicit val MemberRoleBSONHandler = new BSONHandler[BSONString, CapsuleMember.Role] {
    def read(b: BSONString) = CapsuleMember.Role.byId get b.value err s"Invalid role ${b.value}"
    def write(x: CapsuleMember.Role) = BSONString(x.id)
  }

  case class DbMember(role: CapsuleMember.Role)
  implicit val DbMemberBSONHandler = Macros.handler[DbMember]
  implicit val MemberBSONWriter = new BSONWriter[CapsuleMember, Bdoc] {
    def write(x: CapsuleMember) = DbMemberBSONHandler write DbMember(x.role)
  }
  implicit val MembersBSONHandler = new BSONHandler[Bdoc, CapsuleMembers] {
    val mapHandler = BSON.MapDocument.MapHandler[String, DbMember]
    def read(b: Bdoc) = CapsuleMembers(mapHandler read b map {
      case (id, dbMember) => id -> CapsuleMember(id, dbMember.role)
    })
    def write(x: CapsuleMembers) = BSONDocument(x.members.mapValues(MemberBSONWriter.write))
  }

  implicit val CapsuleItemHandler = Macros.handler[CapsuleItem]

  implicit val CapsuleHandler = Macros.handler[Capsule]

}
