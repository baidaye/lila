package lila.resource

import lila.user.User
import ornicar.scalalib.Random
import org.joda.time.DateTime

case class SituationDBRel(
    _id: GameDBRel.ID,
    situationdbId: SituationDB.ID,
    fen: String,
    standard: Boolean,
    color: String,
    name: Option[String],
    score: Option[String],
    tags: Option[List[String]],
    instruction: Option[String],
    createAt: DateTime,
    updateAt: DateTime,
    createBy: User.ID
) {

  def id = _id

  def tagsOrEmpty = tags | List.empty[String]

}

object SituationDBRel {

  type ID = String

  def make(
    situationdbId: SituationDB.ID,
    fen: String,
    standard: Boolean,
    color: String,
    name: Option[String],
    score: Option[String],
    tags: Option[List[String]],
    instruction: Option[String],
    userId: User.ID
  ) = SituationDBRel(
    _id = Random nextString 8,
    situationdbId = situationdbId,
    fen = fen,
    standard = standard,
    color = color,
    name = name,
    score = score,
    tags = tags,
    instruction = instruction,
    createAt = DateTime.now,
    updateAt = DateTime.now,
    createBy = userId
  )

}
