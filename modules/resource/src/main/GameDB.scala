package lila.resource

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class GameDB(
    _id: GameDB.ID,
    parent: GameDB.ID,
    name: String,
    sort: Int,
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

}

object GameDB {

  type ID = String

  case class WithChildren(gamedb: GameDB, children: List[GameDB]) {
    def id = gamedb.id
    def parent = gamedb.parent
  }

  def make(parent: String, name: String, sort: Int, userId: User.ID) = GameDB(
    _id = Random nextString 8,
    parent = parent,
    name = name,
    sort = sort,
    createAt = DateTime.now,
    createBy = userId
  )

}