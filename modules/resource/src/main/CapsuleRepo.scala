package lila.resource

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson.BSONDocument
import DataForm.capsule.{ CapsuleSearchData, CapsuleCreateData, CapsuleUpdateData }

object CapsuleRepo {

  import CapsuleBSONHandlers.MemberRoleBSONHandler
  import CapsuleBSONHandlers.MemberBSONWriter
  import CapsuleBSONHandlers.CapsuleHandler

  private lazy val coll = Env.current.CapsuleColl

  def byId(id: Capsule.ID): Fu[Option[Capsule]] =
    coll.byId[Capsule](id)

  def byIds(ids: List[Capsule.ID]): Fu[List[Capsule]] =
    coll.byIds(ids)

  def nameExists(name: String, id: Option[Capsule.ID], userId: User.ID): Fu[Boolean] =
    coll exists $doc("name" -> name, "createdBy" -> userId) ++ id.fold($empty) { _id =>
      $doc("_id" $ne _id)
    }

  def mineTags(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc(s"members.$userId.role" -> "o").some)

  def mine(search: CapsuleSearchData, userId: User.ID): Fu[List[Capsule]] = {
    var doc = $doc(s"members.$userId.role" -> "o")
    search.status.foreach { s =>
      doc = doc ++ $doc("status" -> s)
    }
    search.visibility.foreach { v =>
      doc = doc ++ $doc("visibility" -> v)
    }
    search.name.foreach { n =>
      doc = doc ++ $doc("name" $regex (n, "i"))
    }
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    doc = doc ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
    coll.find(doc).sort($sort desc "updatedAt").list(1000)
  }

  def memberTags(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc(s"members.$userId.role" $in List("r", "w")).some)

  def memberFor(ownerId: User.ID, memberId: User.ID): Fu[List[Capsule]] = {
    coll.find($doc(s"members.$memberId.role" $in List("r", "w"), s"members.$ownerId.role" -> "o"))
      .sort($sort desc "updatedAt")
      .list(1000)
  }

  def member(search: CapsuleSearchData, userId: User.ID): Fu[List[Capsule]] = {
    var doc = $doc(s"members.$userId.role" $in search.role.fold(List("r", "w")) { r => List(r) })
    search.status.foreach { s =>
      doc = doc ++ $doc("status" -> s)
    }
    search.visibility.foreach { v =>
      doc = doc ++ $doc("visibility" -> v)
    }
    search.name.foreach { n =>
      doc = doc ++ $doc("name" $regex (n, "i"))
    }
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    doc = doc ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
    coll.find(doc).sort($sort desc "updatedAt").list(1000)
  }

  def teamManagerTags(belongTeamOwners: List[User.ID]): Fu[Set[String]] = {
    belongTeamOwners.nonEmpty match {
      case true => {
        val $owners = belongTeamOwners.map(o => $doc(s"members.$o.role" -> "o"))
        val doc = $doc("visibility" -> CapsuleVisibility.TeamManager.id) ++ $or($owners: _*)
        coll.distinct[String, Set]("tags", doc.some)
      }
      case false => fuccess(Set.empty[String])
    }
  }

  def teamManager(search: CapsuleSearchData, belongTeamOwners: List[User.ID]): Fu[List[Capsule]] = {
    belongTeamOwners.nonEmpty match {
      case true => {
        val $owners = belongTeamOwners.map(o => $doc(s"members.$o.role" -> "o"))
        var doc = $doc("visibility" -> CapsuleVisibility.TeamManager.id) ++ $or($owners: _*)
        search.status.foreach { s =>
          doc = doc ++ $doc("status" -> s)
        }
        search.visibility.foreach { v =>
          doc = doc ++ $doc("visibility" -> v)
        }
        search.name.foreach { n =>
          doc = doc ++ $doc("name" $regex (n, "i"))
        }
        var $andConditions = List.empty[BSONDocument]
        search.tags foreach { tg =>
          $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
        }
        search.emptyTag foreach { tg =>
          if (tg == "on") {
            $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
          }
        }
        doc = doc ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
        coll.find(doc).sort($sort desc "updatedAt").list(1000)
      }
      case false => fuccess(List.empty[Capsule])
    }
  }

  def insert(capsule: Capsule): Funit =
    coll.insert(capsule).void

  def remove(id: Capsule.ID): Funit = coll.remove($id(id)).void

  def update(capsule: Capsule, data: CapsuleUpdateData, userId: User.ID): Funit =
    coll.update(
      $id(capsule.id),
      $set(
        "name" -> data.name,
        "status" -> data.status,
        "desc" -> data.desc,
        "tags" -> data.makeTags,
        "updatedBy" -> userId,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setStatus(id: Capsule.ID, status: Boolean, userId: User.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "status" -> status,
        "updatedBy" -> userId,
        "updatedAt" -> DateTime.now
      )
    ).void

  def setVisibility(id: Capsule.ID, visibility: CapsuleVisibility, userId: User.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "visibility" -> visibility.id,
        "updatedBy" -> userId,
        "updatedAt" -> DateTime.now
      )
    ).void

  def addMember(id: Capsule.ID, member: CapsuleMember): Funit =
    coll.update(
      $id(id),
      $set(s"members.${member.id}" -> member)
    ).void

  def removeMember(id: Capsule.ID, memberId: User.ID): Funit =
    coll.update(
      $id(id),
      $unset(s"members.$memberId")
    ).void

  def removeMembers(ids: List[Capsule.ID], memberId: User.ID): Funit =
    coll.update(
      $inIds(ids),
      $unset(s"members.$memberId"),
      multi = true
    ).void

  def quitMember(id: Capsule.ID, memberId: User.ID): Funit =
    coll.update(
      $id(id),
      $unset(s"members.$memberId")
    ).void

  def setMemberRole(id: Capsule.ID, memberId: User.ID, role: CapsuleMember.Role): Funit =
    coll.update(
      $id(id),
      $set(s"members.$memberId.role" -> role)
    ).void

  def changeOwner(id: Capsule.ID, memberId: User.ID, userId: User.ID): Funit =
    coll.update(
      $id(id),
      $set(
        "visibility" -> CapsuleVisibility.Private.id,
        s"members.$userId.role" -> CapsuleMember.Role.Read.id,
        s"members.$memberId" -> CapsuleMember(memberId, CapsuleMember.Role.Owner)
      )
    ).void

  def addPuzzle(capsule: Capsule, puzzleIds: List[Int], userId: User.ID): Funit = {
    val ids = puzzleIds.filterNot(capsule.item.puzzlesOrEmpty.contains(_))
    puzzleIds.nonEmpty.?? {
      coll.update(
        $id(capsule.id),
        $set(
          "item.puzzles" -> (capsule.item.puzzlesOrEmpty ++ ids),
          "updatedBy" -> userId,
          "updatedAt" -> DateTime.now
        )
      ).void
    }
  }

  def delPuzzle(capsule: Capsule, puzzleIds: List[Int], userId: User.ID): Funit =
    puzzleIds.nonEmpty.?? {
      coll.update(
        $id(capsule.id),
        $set(
          "item.puzzles" -> capsule.item.puzzlesOrEmpty.filter(p => !puzzleIds.contains(p)),
          "updatedBy" -> userId,
          "updatedAt" -> DateTime.now
        )
      ).void
    }

  def reorderPuzzles(capsule: Capsule, puzzleIds: List[Int], userId: User.ID): Funit =
    (puzzleIds.nonEmpty && capsule.item.puzzlesOrEmpty.length == puzzleIds.length).?? {
      coll.update(
        $id(capsule.id),
        $set(
          "item.puzzles" -> puzzleIds,
          "updatedBy" -> userId,
          "updatedAt" -> DateTime.now
        )
      ).void
    }

}
