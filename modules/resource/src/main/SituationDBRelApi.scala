package lila.resource

import lila.common.paginator.Paginator
import lila.common.MaxPerPage
import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson.{ BSONDocument, Macros }
import lila.resource.DataForm.situationdb.SearchData
import lila.resource.DataForm.situationdbRel.EditData
import ornicar.scalalib.Random

final class SituationDBRelApi(
    coll: Coll,
    bus: lila.common.Bus
) {

  implicit val TagsHandler = bsonArrayToListHandler[String]
  implicit val SituationDBRelHandler = Macros.handler[SituationDBRel]

  private def bySituationDBId(situationdbId: SituationDB.ID) = $doc("situationdbId" -> situationdbId)
  private def desc = $sort desc "createAt"

  def page(rels: List[SituationDBRel]): Fu[Paginator[SituationDBRel]] =
    fuccess {
      Paginator.fromList(rels, 1, MaxPerPage(rels.size))
    }

  def byId(id: SituationDBRel.ID): Fu[Option[SituationDBRel]] = coll.byId[SituationDBRel](id)

  def countByDb(id: SituationDB.ID): Fu[Int] = coll.countSel($doc("situationdbId" -> id))

  def byIds(ids: List[SituationDBRel.ID]): Fu[List[SituationDBRel]] = coll.byIds(ids)

  def findByDB(selected: SituationDB.ID, data: SearchData, userId: User.ID, size: Int, sortBy: Option[SituationDB.SortBy]): Fu[List[SituationDBRel]] = {
    var condition = $doc("situationdbId" -> selected)
    data.standard.foreach { standard =>
      condition = condition ++ $doc("standard" -> standard)
    }
    data.name.foreach { n =>
      condition = condition ++ $doc("name" $regex (n, "i"))
    }
    var $andConditions = List.empty[BSONDocument]
    data.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    data.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    condition = condition ++ (!$andConditions.empty).?? {
      $and($andConditions: _*)
    }
    //println(BSONDocument.pretty(condition))
    coll.find(condition)
      .sort((sortBy | SituationDB.SortBy.default).doc)
      .list[SituationDBRel](size)
  }

  def tagsByUser(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("createBy" -> userId).some)

  def findByUser(userId: User.ID, standard: Option[Boolean] = None): Fu[List[SituationDBRel]] =
    coll.find($doc("createBy" -> userId) ++ standard.?? { standard => $doc("standard" -> standard) })
      .sort(desc)
      .list[SituationDBRel]()

  def create(situationdbId: String, data: DataForm.situationdbRel.CreateData, userId: User.ID): Fu[SituationDBRel] = {
    val rel =
      SituationDBRel.make(
        fen = data.fen,
        standard = data.standard,
        color = data.color,
        name = data.name,
        score = data.score,
        tags = data.tagList,
        instruction = data.instruction,
        situationdbId = situationdbId,
        userId = userId
      )
    coll.insert(rel) inject rel
  }

  def update(id: SituationDBRel.ID, data: EditData): Funit =
    coll.update(
      $id(id),
      $set(
        "name" -> data.name,
        "fen" -> data.fen,
        "standard" -> data.standard,
        "color" -> data.color,
        "score" -> data.score,
        "tags" -> data.tagList,
        "instruction" -> data.instruction
      )
    ).void

  def remove(ids: List[SituationDBRel.ID], userId: String): Funit =
    coll.remove($inIds(ids)).void

  def removeBySituationDB(situationdbIds: List[String]): Funit =
    coll.remove($doc("situationdbId" $in situationdbIds)).void

  def toCopyRels(data: DataForm.situationdbRel.MoveData): Fu[List[SituationDBRel]] =
    coll.find($inIds(data.relIds)).sort(desc).list[SituationDBRel]().map { list =>
      list.map { rel =>
        rel.copy(
          _id = if (data.action == "move") rel.id else Random nextString 8,
          situationdbId = data.toSituationdb,
          updateAt = DateTime.now
        )
      }
    }

  def copyOrMove(
    userId: String,
    data: DataForm.situationdbRel.MoveData,
    situationdbRels: List[SituationDBRel],
    remove: (List[String], String) => Funit
  ): Funit = (data.action == "move").?? { remove(data.relIds, userId) } >> bulkInsert(situationdbRels)

  def bulkInsert(rels: List[SituationDBRel]): Funit =
    coll.bulkInsert(
      documents = rels.map(SituationDBRelHandler.write).toStream,
      ordered = true
    ).void

}
