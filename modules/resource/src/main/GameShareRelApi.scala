package lila.resource

import lila.user.User
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.game.GameRepo
import reactivemongo.bson.{ BSONDocument, Macros }
import lila.resource.DataForm.gameShare.SearchData

final class GameShareRelApi(coll: Coll, gamedbRelApi: GameDBRelApi, bus: lila.common.Bus) {

  implicit val TagsHandler = bsonArrayToListHandler[String]
  implicit val GameShareRelHandler = Macros.handler[GameShareRel]

  def byId(id: GameShareRel.ID): Fu[Option[GameShareRel]] = coll.byId[GameShareRel](id)

  def byIds(ids: List[GameShareRel.ID]): Fu[List[GameShareRel]] = coll.byIds(ids)

  def shareBys(userId: User.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("createBy", $doc("student" -> userId).some)

  def tagsShare(shareId: GameShare.ID): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("shareId" -> shareId).some)

  def list(shareId: GameShare.ID, search: SearchData): Fu[List[GameShareRel.WithGame]] = {
    var $selector = $doc("shareId" -> shareId)
    var $andConditions = List.empty[BSONDocument]
    search.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    search.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    $selector = $selector ++ (!$andConditions.empty).?? {
      $and($andConditions: _*)
    }
    coll.find($selector).list[GameShareRel]().flatMap { rels =>
      GameRepo.gamesFromSecondary(rels.map(_.gameId)).map { games =>
        rels zip games collect {
          case (rel, game) => GameShareRel.WithGame(rel, game)
        }
      }
    }
  }

  def batchInsert(rels: List[GameShareRel]): Funit =
    coll.bulkInsert(
      documents = rels.map(GameShareRelHandler.write).toStream,
      ordered = true
    ).void

  def update(id: GameShareRel.ID, name: String, tags: Option[Set[String]]): Funit =
    coll.update(
      $id(id),
      $set("name" -> name, "tags" -> tags)
    ).void

  def remove(ids: List[GameShareRel.ID], userId: String): Funit =
    coll.remove($inIds(ids)).void

  def toCopyRels(data: DataForm.gameDBRel.CopyOrMoveData, userId: User.ID): Fu[List[GameDBRel]] =
    coll.find($inIds(data.relIds)).sort($sort desc "createAt").list[GameShareRel]().map { list =>
      list.map { rel =>
        GameDBRel.make(
          name = rel.name,
          tags = rel.tags,
          gamedbId = data.toGamedb,
          gameId = rel.gameId,
          userId = userId
        )
      }
    }

}
