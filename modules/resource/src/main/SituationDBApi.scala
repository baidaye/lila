package lila.resource

import lila.common.TreeNode
import lila.db.dsl._
import lila.user.User
import play.api.libs.json.JsArray
import reactivemongo.bson.{ BSONHandler, BSONString, Macros }

final class SituationDBApi(coll: Coll, situationdbRelApi: SituationDBRelApi, bus: lila.common.Bus) {

  implicit val SortByBSONHandler = new BSONHandler[BSONString, SituationDB.SortBy] {
    def read(b: BSONString): SituationDB.SortBy = SituationDB.SortBy(b.value)
    def write(x: SituationDB.SortBy) = BSONString(x.id)
  }
  implicit val SituationDBHandler = Macros.handler[SituationDB]

  private def createUser(userId: User.ID) = $doc("createBy" -> userId)
  private def asc = $sort asc "sort"
  private def desc = $sort desc "sort"

  def byId(id: SituationDB.ID): Fu[Option[SituationDB]] = coll.byId[SituationDB](id)

  def loadFolderNode(userId: User.ID, selected: String, withRoot: Boolean = true): Fu[JsArray] =
    findList(userId).map { list =>
      val nodes =
        TreeNode(
          id = userId,
          text = "局面数据库",
          parent = Some("#"),
          typ = Some("folder"),
          icon = Some("jstree-folder"),
          state = TreeNode.State(
            opened = true,
            disabled = false,
            selected = selected == userId
          ).some
        ) +: list.map { situationdb =>
            TreeNode(
              id = situationdb.id,
              text = situationdb.name,
              parent = Some(situationdb.parent),
              typ = Some("folder"),
              icon = Some("jstree-folder"),
              state = TreeNode.State(
                opened = true,
                disabled = false,
                selected = selected == situationdb.id
              ).some
            )
          }
      TreeNode.toJsArray(nodes.filter(n => withRoot || n.id != userId))
    }

  def findList(userId: User.ID, parent: Option[String] = None): Fu[List[SituationDB]] =
    coll.find(createUser(userId) ++ parent.?? { p => $doc("parent" -> p) })
      .sort(asc)
      .list[SituationDB]()

  def findWithChildren(userId: User.ID): Fu[List[SituationDB.WithChildren]] = {
    import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework._
    coll.aggregateList(
      Match(createUser(userId)),
      List(
        Sort(Ascending("sort")),
        PipelineOperator(
          $doc(
            "$lookup" -> $doc(
              "from" -> Env.current.CollectionSituationDB,
              "localField" -> "_id",
              "foreignField" -> "parent",
              "as" -> "children"
            )
          )
        )
      ),
      maxDocs = 1000
    ).map {
        _.flatMap { doc =>
          for {
            situationdb <- SituationDBHandler.readOpt(doc)
            children <- doc.getAs[List[SituationDB]]("children")
          } yield SituationDB.WithChildren(situationdb, children)
        }(scala.collection.breakOut).toList
      }
  }

  def findAllChildren(userId: User.ID, parent: SituationDB.ID): Fu[List[SituationDB]] =
    findWithChildren(userId).map { situationdbWithChildren =>
      findChildren(situationdbWithChildren, parent)
    }

  def findChildren(situationdbWithChildrens: List[SituationDB.WithChildren], parent: SituationDB.ID): List[SituationDB] =
    situationdbWithChildrens.filter(_.parent == parent).foldLeft(List.empty[SituationDB]) {
      case (all, swc) => (all :+ swc.situationdb) ++ findChildren(situationdbWithChildrens, swc.id)
    }

  def create(parent: SituationDB.ID, name: String, sort: Int, userId: User.ID): Fu[SituationDB] = {
    val gdb = SituationDB.make(parent, name, sort, userId)
    coll.insert(gdb).inject(gdb)
  }

  def rename(id: SituationDB.ID, name: String): Funit =
    coll.update(
      $id(id),
      $set("name" -> name)
    ).void

  def remove(userId: User.ID, parent: SituationDB.ID): Funit =
    findAllChildren(userId, parent).flatMap { list =>
      val ids = list.map(_.id) :+ parent
      coll.remove($inIds(ids)) >> situationdbRelApi.removeBySituationDB(ids)
    }

  def move(id: SituationDB.ID, parent: SituationDB.ID, children: List[SituationDB.ID], userId: User.ID): Funit =
    coll.update(
      $id(id),
      $set("parent" -> parent)
    ) >> children.zipWithIndex.map {
        case (id, sort) => coll.update(
          $id(id),
          $set("sort" -> (sort + 1))
        )
      }.sequenceFu.void

  def relSortBy(id: SituationDB.ID, sort: SituationDB.SortBy): Funit = {
    coll.update(
      $id(id),
      $set("relSortBy" -> sort)
    ).void
  }

}
