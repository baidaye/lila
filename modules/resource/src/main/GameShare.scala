package lila.resource

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class GameShare(
    _id: GameShare.ID,
    rels: List[GameDBRelMini],
    clazzId: Option[String],
    students: List[User.ID],
    remark: Option[String],
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

}

object GameShare {

  type ID = String

  def make(
    rels: List[GameDBRelMini],
    clazzId: Option[String],
    students: List[User.ID],
    remark: Option[String],
    userId: User.ID
  ) = GameShare(
    _id = Random nextString 8,
    rels = rels,
    clazzId = clazzId,
    students = students,
    remark = remark,
    createAt = DateTime.now,
    createBy = userId
  )

}

case class GameDBRelMini(id: GameDB.ID, name: String, gameId: String)
