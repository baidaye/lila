package lila.resource

import lila.user.User
import org.joda.time.DateTime

case class CapsuleRecord(
    _id: String,
    userId: User.ID,
    capsuleId: Capsule.ID,
    puzzleId: Int,
    updateAt: DateTime
) {

  def id = _id

}

object CapsuleRecord {

  def make(
    userId: User.ID,
    capsuleId: Capsule.ID,
    puzzleId: Int
  ): CapsuleRecord = {
    CapsuleRecord(
      _id = makeId(capsuleId, userId),
      userId: User.ID,
      capsuleId: Capsule.ID,
      puzzleId = puzzleId,
      updateAt = DateTime.now
    )
  }

  def makeId(capsuleId: Capsule.ID, userId: User.ID) = capsuleId + "@" + userId

}
