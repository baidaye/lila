package lila.resource

import lila.common.TreeNode
import lila.db.dsl._
import lila.user.User
import play.api.libs.json.JsArray
import reactivemongo.bson.Macros

final class GameDBApi(coll: Coll, gameDBRelApi: GameDBRelApi, bus: lila.common.Bus) {

  implicit val GameDBHandler = Macros.handler[GameDB]

  private def createUser(userId: User.ID) = $doc("createBy" -> userId)
  private def asc = $sort asc "sort"
  private def desc = $sort desc "sort"

  def byId(id: GameDB.ID): Fu[Option[GameDB]] = coll.byId[GameDB](id)

  def loadFolderNode(userId: User.ID, selected: String, withRoot: Boolean = true): Fu[JsArray] =
    findList(userId).map { list =>
      val nodes =
        TreeNode(
          id = userId,
          text = "对局数据库",
          parent = Some("#"),
          typ = Some("folder"),
          icon = Some("jstree-folder"),
          state = TreeNode.State(
            opened = true,
            disabled = false,
            selected = selected == userId
          ).some
        ) +: list.map { gamedb =>
            TreeNode(
              id = gamedb.id,
              text = gamedb.name,
              parent = Some(gamedb.parent),
              typ = Some("folder"),
              icon = Some("jstree-folder"),
              state = TreeNode.State(
                opened = true,
                disabled = false,
                selected = selected == gamedb.id
              ).some
            )
          }
      TreeNode.toJsArray(nodes.filter(n => withRoot || n.id != userId))
    }

  def loadAllNode(userId: User.ID, selected: Option[String] = None): Fu[JsArray] =
    for {
      gamedbs <- findList(userId)
      rels <- gameDBRelApi.findByUser(userId)
    } yield {
      val nodes =
        List(
          TreeNode(
            id = userId,
            text = "对局数据库",
            parent = Some("#"),
            typ = Some("folder"),
            icon = Some("jstree-folder"),
            state = TreeNode.State(
              opened = true,
              disabled = false,
              selected = false
            ).some
          )
        ) ++ {
            gamedbs.map { gamedb =>
              TreeNode(
                id = gamedb.id,
                text = gamedb.name,
                parent = Some(gamedb.parent),
                typ = Some("folder"),
                icon = Some("jstree-folder")
              )
            }
          } ++ {
            rels.map { rel =>
              TreeNode(
                id = s"${rel.id}:${rel.gameId}",
                text = rel.name,
                parent = Some(rel.gamedbId),
                typ = Some("file"),
                icon = Some("jstree-file"),
                state = TreeNode.State(
                  opened = false,
                  disabled = false,
                  selected = selected.contains(s"${rel.id}:${rel.gameId}")
                ).some
              )
            }
          }
      TreeNode.toJsArray(nodes)
    }

  def findList(userId: User.ID, parent: Option[String] = None): Fu[List[GameDB]] =
    coll.find(createUser(userId) ++ parent.?? { p => $doc("parent" -> p) })
      .sort(asc)
      .list[GameDB]()

  def findWithChildren(userId: User.ID): Fu[List[GameDB.WithChildren]] = {
    import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework._
    coll.aggregateList(
      Match(createUser(userId)),
      List(
        Sort(Ascending("sort")),
        PipelineOperator(
          $doc(
            "$lookup" -> $doc(
              "from" -> Env.current.CollectionGameDB,
              "localField" -> "_id",
              "foreignField" -> "parent",
              "as" -> "children"
            )
          )
        )
      ),
      maxDocs = 1000
    ).map {
        _.flatMap { doc =>
          for {
            gamedb <- GameDBHandler.readOpt(doc)
            children <- doc.getAs[List[GameDB]]("children")
          } yield GameDB.WithChildren(gamedb, children)
        }(scala.collection.breakOut).toList
      }
  }

  def findAllChildren(userId: User.ID, parent: GameDB.ID): Fu[List[GameDB]] =
    findWithChildren(userId).map { gamedbWithChildren =>
      findChildren(gamedbWithChildren, parent)
    }

  def findChildren(gamedbWithChildrens: List[GameDB.WithChildren], parent: GameDB.ID): List[GameDB] =
    gamedbWithChildrens.filter(_.parent == parent).foldLeft(List.empty[GameDB]) {
      case (all, gwc) => (all :+ gwc.gamedb) ++ findChildren(gamedbWithChildrens, gwc.id)
    }

  def create(parent: GameDB.ID, name: String, sort: Int, userId: User.ID): Fu[GameDB] = {
    val gdb = GameDB.make(parent, name, sort, userId)
    coll.insert(gdb).inject(gdb)
  }

  def rename(id: GameDB.ID, name: String): Funit =
    coll.update(
      $id(id),
      $set("name" -> name)
    ).void

  def remove(userId: User.ID, parent: GameDB.ID): Funit =
    findAllChildren(userId, parent).flatMap { list =>
      val ids = list.map(_.id) :+ parent
      coll.remove($inIds(ids)) >> gameDBRelApi.removeByGameDB(ids)
    }

  def move(id: GameDB.ID, parent: GameDB.ID, children: List[GameDB.ID], userId: User.ID): Funit =
    coll.update(
      $id(id),
      $set("parent" -> parent)
    ) >> children.zipWithIndex.map {
        case (id, sort) => coll.update(
          $id(id),
          $set("sort" -> (sort + 1))
        )
      }.sequenceFu.void

}
