package lila.resource

import lila.game.Game
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class GameDBRel(
    _id: GameDBRel.ID,
    gamedbId: GameDB.ID,
    gameId: String,
    name: String,
    tags: Option[List[String]],
    createAt: DateTime,
    updateAt: DateTime,
    createBy: User.ID
) {

  def id = _id

  def tagsOrEmpty = tags | List.empty[String]

}

object GameDBRel {

  type ID = String

  case class WithGame(rel: GameDBRel, game: Game)

  def make(
    name: String,
    tags: Option[List[String]],
    gamedbId: GameDB.ID,
    gameId: Game.ID,
    userId: User.ID
  ) = GameDBRel(
    _id = Random nextString 8,
    gamedbId = gamedbId,
    gameId = gameId,
    name = name,
    tags = tags,
    createAt = DateTime.now,
    updateAt = DateTime.now,
    createBy = userId
  )

}

