package lila.resource

import lila.user.User

case class CapsuleMember(id: User.ID, role: CapsuleMember.Role) {

  def canContribute = role.canWrite

  def isReader = role == CapsuleMember.Role.Read

  def isWriter = role == CapsuleMember.Role.Write

  def isOwner = role == CapsuleMember.Role.Owner

}

object CapsuleMember {

  type MemberMap = Map[User.ID, CapsuleMember]

  def make(user: User) = CapsuleMember(id = user.id, role = Role.Read)

  sealed abstract class Role(val id: String, val name: String, val canWrite: Boolean, val order: Int)
  object Role {
    case object Read extends Role("r", "参与者", false, 3)
    case object Write extends Role("w", "贡献者", true, 2)
    case object Owner extends Role("o", "所有者", true, 1)

    def all = List(Read, Write, Owner)

    def byId = all.map { x => x.id -> x }.toMap

    def selects = all.map { v => (v.id, v.name) }

    def memberSelects = List(Read, Write).map { v => (v.id, v.name) }

  }
}

case class CapsuleMembers(members: CapsuleMember.MemberMap) {

  def +(member: CapsuleMember) = copy(members = members + (member.id -> member))

  def ownerId = members.find(_._2.isOwner).map(_._1) err s"can not find owner"
  def contains(userId: User.ID): Boolean = members contains userId
  def contains(user: User): Boolean = contains(user.id)
  def isOwner(userId: User.ID): Boolean = members.get(userId).??(_.isOwner)
  def isContributor(userId: User.ID): Boolean = members.get(userId).??(_.canContribute)
  def isWriter(userId: User.ID): Boolean = members.get(userId).??(_.isWriter)
  def isReader(userId: User.ID): Boolean = members.get(userId).??(_.isReader)
  def roleOf(userId: User.ID): Option[CapsuleMember.Role] = members.get(userId).map(_.role)

  def get = members.get _

  def ids = members.keys

  def sorted = members.toList.sortBy(_._2.role.order)

  def contributorIds: Set[User.ID] = members.collect {
    case (id, member) if member.canContribute => id
  }(scala.collection.breakOut)
}

object CapsuleMembers {
  val empty = CapsuleMembers(Map.empty)
}
