package lila.resource

case class Sorting(field: String, order: Int)

object Sorting {

  val fields = List(
    "perf.gl.r" -> "难度",
    "_id" -> "随机"
  )

  val orders = List(
    1 -> "按难度正序",
    -1 -> "按难度倒序"
  )

  val machineOrders = List(
    "perf.gl.r:1" -> "按难度正序",
    "perf.gl.r:-1" -> "按难度倒序",
    "_id:1" -> "随机排序"
  )

  val default = Sorting("perf.gl.r", 1)
}
