package lila.resource

object ThemeQuery {

  val phase = List("Opening" -> "开局", "MiddleGame" -> "中局", "EndingGame" -> "残局")

  val moveFor = List("DeZi" -> "得子", "ShaWang" -> "杀王", "QiuHe" -> "求和", "BingShengBian" -> "兵升变", "FangYu" -> "防守")

  val moveForMachine = List("equality" -> "均势", "advantage" -> "优势", "crushing" -> "胜势", "mate" -> "将杀")

  val moves = List("oneMove" -> "超短(1步)", "short" -> "短的(2步)", "long" -> "长的(3步)", "veryLong" -> "超长(4+步)")

  val pieceColor = List("White" -> "白方", "Black" -> "黑方")

  val subject = List("QianZi" -> "牵制", "ZhuoShuang" -> "捉双", "ShuangChongGongJi" -> "双重攻击", "ShanJi" -> "闪击", "ChuanJi" -> "串击", "ShuangJiang" -> "双将", "YinRu" -> "引入", "YinLi" -> "引离", "TouShi" -> "透视", "LanJie" -> "拦截", "TengNuo" -> "腾挪", "DuSe" -> "堵塞", "QuGan" -> "驱赶", "GuoMen" -> "过门", "XiaoChuBaoHu" -> "消除保护", /*"GuoZai" -> "过载",*/ "BiZouLieZhao" -> "逼走劣着", "GuoDuZhao" -> "过渡着", /*"DengZhao" -> "等着",*/ "WeiKun" -> "围困", "FangYu" -> "防御", "MenSha" -> "闷杀", "KaiXian" -> "开线(清理)", "YiWei" -> "易位", "AnJingYiZhao" -> "安静一招")

  val subjectMachine = List("pin" -> "牵制", "fork" -> "捉双", "discoveredAttack" -> "闪击", "skewer" -> "串击", "doubleCheck" -> "双将", "attraction" -> "引入", "deflection" -> "引离", "xRayAttack" -> "透视", "interference" -> "拦截", "intermezzo" -> "过门(过渡招)", "capturingDefender" -> "消除保护", "zugzwang" -> "楚茨文克", "defensiveMove" -> "防御", "sacrifice" -> "弃子", "smotheredMate" -> "闷杀(窒息杀)", "backRankMate" -> "底线闷杀", "quietMove" -> "安静一招", "clearance" -> "开线(清理)", "attackingF2F7" -> "攻击f2/f7", "kingsideAttack" -> "王翼攻击", "queensideAttack" -> "后翼攻击", "castling" -> "易位", "enPassant" -> "吃过路兵", "promotion" -> "兵升变", "underPromotion" -> "低升变" /*, "healthyMix" -> "战术组合"*/ )

  val chessGame = List( /*"YiSeGeXiang" -> "异色格象", "TongSeGeXiang" -> "同色格象",*/ "ShuangXiang" -> "双象", "CeYiXiang" -> "侧翼象", "ShuangChe" -> " 双车", /*"TiChe" -> "提车",*/ "CiDiXianChe" -> "次底线车", "DiXian" -> "底线", "GuoZai" -> "过载", /*"MenSha" -> "闷杀",*/ "BeiKunDeZi" -> "被困的子", /* "QianShao" -> "前哨", "XuanZi" -> "悬子", "FengChe" -> "风车",*/ "XieXianChongDie" -> "斜线重叠", "ChuiZhiChongDie" -> "垂直重叠", "YiShouGongJiDeWang" -> "易受攻击的王", "NiXiangYiWei" -> "逆向易位", "ZhongXinWang" -> "中心王", /* "DuiWang" -> "对王", "TianChuang" -> "天窗",*/ "TongLuBing" -> "通路兵" /*, "LianBing" -> "连兵", "DieBing" -> "叠兵", "GuBing" -> "孤兵"*/ )

  val comprehensive = List("JiBenChiZi" -> "基本吃子", "YiBuSha" -> "一步杀", "LiangBuSha" -> "两步杀", "SanBuSha" -> "三步杀", "SiBuYiShangSha" -> "四步以上杀", "JianHua" -> "简化", "BingShengBian" -> "兵升变", "TeSuShengBian" -> "特殊升变", "ChiGuoLuBing" -> "吃过路兵", "LiangBuBing" -> "两步兵", "ChangJiang" -> "长将/长捉", /*"ChangZhuo" -> "长捉",*/ "QiZiMouHe" -> "弃子谋和", "WeiXieShaWang" -> "威胁杀王", "QiMeng" -> "启蒙")

  val comprehensiveMachine = List("mateIn1" -> "一步杀", "mateIn2" -> "两步杀", "mateIn3" -> "三步杀", "mateIn4" -> "四步杀", "mateIn5" -> "五步及以上将杀", "anastasiaMate" -> "安娜斯塔西亚将杀", "arabianMate" -> "阿拉伯将杀", "bodenMate" -> "伯顿将杀", "doubleBishopMate" -> "双象将杀", "dovetailMate" -> "鸽尾将杀", "hookMate" -> "勾子将杀", "pawnEndgame" -> "兵残局", "bishopEndgame" -> "象兵残局", "knightEndgame" -> "马兵残局", "rookEndgame" -> "车兵残局", "queenEndgame" -> "后兵残局", "queenRookEndgame" -> "后车残局", "advancedPawn" -> "通路兵", "trappedPiece" -> "被困的子", "exposedKing" -> "暴露的王", "hangingPiece" -> "悬子", "master" -> "大师", "masterVsMaster" -> "大师对大师", "superGM" -> "特级大师")

  val strength = List("Queen" -> "后", "Rook" -> "车", "Bishop" -> "象", "Knight" -> "马", "Pawn" -> "兵", "King" -> "王")

  //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  val ResComprehensiveThemeMateIn = List("YiBuSha" -> "一步杀", "LiangBuSha" -> "两步杀", "SanBuSha" -> "三步杀", "SiBuYiShangSha" -> "四步以上杀")

  val ResComprehensiveTheme = List("JiBenChiZi" -> "基本吃子", "JianHua" -> "简化", "BingShengBian" -> "兵升变", "TeSuShengBian" -> "特殊升变", "ChiGuoLuBing" -> "吃过路兵", "LiangBuBing" -> "两步兵", "ChangJiang" -> "长将/长捉", /*"ChangZhuo" -> "长捉",*/ "QiZiMouHe" -> "弃子谋和", "WeiXieShaWang" -> "威胁杀王", "QiMeng" -> "启蒙")

  //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  val ResComprehensiveMachineMateIn = List("mateIn1" -> "一步杀", "mateIn2" -> "两步杀", "mateIn3" -> "三步杀", "mateIn4" -> "四步杀", "mateIn5" -> "五步及以上将杀")
  val ResComprehensiveMachineMate = List("anastasiaMate" -> "安娜斯塔西亚将杀", "arabianMate" -> "阿拉伯将杀", "bodenMate" -> "伯顿将杀", "doubleBishopMate" -> "双象将杀", "dovetailMate" -> "鸽尾将杀", "hookMate" -> "勾子将杀")
  val ResComprehensiveMachineEndgame = List("pawnEndgame" -> "兵残局", "bishopEndgame" -> "象兵残局", "knightEndgame" -> "马兵残局", "rookEndgame" -> "车兵残局", "queenEndgame" -> "后兵残局", "queenRookEndgame" -> "后车残局")
  val ResComprehensiveMachineRemaind = List("advancedPawn" -> "通路兵", "trappedPiece" -> "被困的子", "exposedKing" -> "暴露的王", "hangingPiece" -> "悬子", "master" -> "大师", "masterVsMaster" -> "大师对大师", "superGM" -> "特级大师")

  def parseLabel(key: Option[String], theme: List[(String, String)]) = key map { k =>
    theme.filter(k == _._1).map(_._2)
  }

  def parseArrayLabel(array: Option[List[String]], theme: List[(String, String)]) = array map { arr =>
    theme.filter(t => arr.contains(t._1)).map(_._2)
  }

  def parseAllLabel(array: List[String]) = {
    val all = phase ++ moveFor ++ pieceColor ++ subject ++ strength ++ chessGame ++ comprehensive
    all.filter(t => array.contains(t._1)).map(_._2)
  }
}
