package lila.resource

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import Capsule._

case class Capsule(
    _id: ID,
    name: String,
    tags: List[String],
    desc: Option[String],
    itemType: CapsuleItemType,
    item: CapsuleItem,
    status: CapsuleStatus,
    members: CapsuleMembers,
    visibility: CapsuleVisibility,
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID,
    cloneFrom: Option[Capsule.ID] = None
) {

  def id = _id

  def ids = itemType match {
    case CapsuleItemType.PuzzleItem => item.puzzlesOrEmpty
  }

  def total = ids.size

  def active = status == CapsuleStatus.Active

  def nonEmpty = itemType match {
    case CapsuleItemType.PuzzleItem => item.puzzles.??(_.nonEmpty)
  }

  def ownerId = members.ownerId

  def isPrivate = visibility == CapsuleVisibility.Private

  def isOwner(userId: String) = members.isOwner(userId)

  def isReader(userId: String) = members.isReader(userId)

  def isWriter(userId: String) = members.isWriter(userId)

  def isReaderOrWriter(userId: String) = isReader(userId) || isWriter(userId)

  def isContributor(userId: String) = members.isContributor(userId)

  def roleOf(userId: String) = members.roleOf(userId)

  def isCreator(userId: String) = userId == createdBy

  def clone(creator: User.ID) = {
    val owner = CapsuleMember(id = creator, role = CapsuleMember.Role.Owner)
    copy(
      _id = Random nextString 8,
      name = s"$name （复制）",
      status = CapsuleStatus.Active,
      members = CapsuleMembers(Map(creator -> owner)),
      visibility = CapsuleVisibility.Private,
      createdAt = DateTime.now,
      updatedAt = DateTime.now,
      createdBy = creator,
      updatedBy = creator,
      cloneFrom = id.some
    )
  }

}

object Capsule {

  type ID = String

  def make(
    name: String,
    desc: Option[String],
    tags: List[String],
    creator: User.ID
  ) = {
    val owner = CapsuleMember(id = creator, role = CapsuleMember.Role.Owner)
    Capsule(
      _id = Random nextString 8,
      name = name,
      tags = tags,
      desc = desc,
      itemType = CapsuleItemType.PuzzleItem,
      item = CapsuleItem.empty,
      status = CapsuleStatus.Active,
      members = CapsuleMembers(Map(creator -> owner)),
      visibility = CapsuleVisibility.Private,
      createdAt = DateTime.now,
      updatedAt = DateTime.now,
      createdBy = creator,
      updatedBy = creator
    )
  }

}

case class CapsuleItem(puzzles: Option[List[Int]] = None) {

  def puzzlesOrEmpty = puzzles | Nil

}

object CapsuleItem {
  def empty = CapsuleItem()
}

sealed abstract class CapsuleItemType(val id: String, val name: String)
object CapsuleItemType {
  case object PuzzleItem extends CapsuleItemType("puzzle", "战术训练")

  def all = List(PuzzleItem)

  def selects = all.map { v => (v.id, v.name) }

  def apply(id: String): CapsuleItemType = all.find(_.id == id) err s"can not apply CapsuleItemType $id"
}

sealed abstract class CapsuleStatus(val id: Boolean, val name: String)
object CapsuleStatus {
  case object Locked extends CapsuleStatus(false, "锁定")
  case object Active extends CapsuleStatus(true, "活动")
  def all = List(Locked, Active)

  def selects = all.map { v => (v.id, v.name) }

  def apply(id: Boolean): CapsuleStatus = all.find(_.id == id) err s"can not apply CapsuleStatus $id"
}

sealed abstract class CapsuleVisibility(val id: String, val name: String)
object CapsuleVisibility {
  case object Private extends CapsuleVisibility("private", "私有")
  case object TeamManager extends CapsuleVisibility("teamManager", "俱乐部教练")

  def all = List(Private, TeamManager)

  def selects = all.map { v => (v.id, v.name) }

  def privateSelects = List(Private).map { v => (v.id, v.name) }

  def apply(id: String): CapsuleVisibility = all.find(_.id == id) err s"can not apply Visibility $id"
}

case class PuzzleStatistics(total: Int, avgRating: Int, minRating: Int, maxRating: Int)
case class CapsuleWithPuzzleStatistics(capsule: Capsule, statistics: PuzzleStatistics, puzzles: List[Int]) {
  def id = capsule.id
  def name = capsule.name
  def puzzleCount = puzzles.length
  def puzzlesJson = s"""[${puzzles.mkString(",")}]"""
}
