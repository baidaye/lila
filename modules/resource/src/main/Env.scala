package lila.resource

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.notify.NotifyApi

final class Env(
    config: Config,
    db: lila.db.Env,
    system: ActorSystem,
    studyApi: lila.study.StudyApi,
    importer: lila.importer.Importer,
    gamePgnDump: lila.game.PgnDump,
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi,
    notifyApi: NotifyApi
) {

  private[resource] val CollectionGameDB = config getString "collection.gamedb"
  private[resource] val CollectionGameDBRel = config getString "collection.gamedb_rel"
  private[resource] val CollectionSituationDB = config getString "collection.situationdb"
  private[resource] val CollectionSituationDBRel = config getString "collection.situationdb_rel"
  private[resource] val CollectionGameShare = config getString "collection.gameshare"
  private[resource] val CollectionGameShareRel = config getString "collection.gameshare_rel"
  private[resource] val CollectionCapsule = config getString "collection.capsule2"
  private[resource] val CollectionCapsuleRecord = config getString "collection.capsule_record"

  lazy val CapsuleColl = db(CollectionCapsule)

  lazy val capsuleApi = new CapsuleApi(bus = system.lilaBus, notifyApi)

  lazy val capsuleRecordApi = new CapsuleRecordApi(
    coll = db(CollectionCapsuleRecord),
    bus = system.lilaBus
  )

  lazy val CapsuleJsonView = new CapsuleJsonView(isOnline, lightUserApi)

  lazy val gamedbApi = new GameDBApi(
    coll = db(CollectionGameDB),
    gameDBRelApi = gamedbRelApi,
    bus = system.lilaBus
  )

  lazy val gamedbRelApi = new GameDBRelApi(
    coll = db(CollectionGameDBRel),
    bus = system.lilaBus,
    studyApi = studyApi,
    importer = importer,
    pgnDump = gamePgnDump
  )

  lazy val gameShareRelApi = new GameShareRelApi(
    coll = db(CollectionGameShareRel),
    gamedbRelApi = gamedbRelApi,
    bus = system.lilaBus
  )

  lazy val gameShareApi = new GameShareApi(
    coll = db(CollectionGameShare),
    gamedbRelApi = gamedbRelApi,
    gameShareRelApi = gameShareRelApi,
    bus = system.lilaBus
  )

  lazy val situationdbApi = new SituationDBApi(
    coll = db(CollectionSituationDB),
    situationdbRelApi = situationdbRelApi,
    bus = system.lilaBus
  )

  lazy val situationdbRelApi = new SituationDBRelApi(
    coll = db(CollectionSituationDBRel),
    bus = system.lilaBus
  )

  lazy val forms = DataForm

  //  system.lilaBus.subscribeFun('puzzleResourceRemove) {
  //    case lila.hub.actorApi.resource.PuzzleResourceRemove(puzzleIds) => capsuleApi.removePuzzle(puzzleIds)
  //  }

}

object Env {

  lazy val current: Env = "resource" boot new Env(
    config = lila.common.PlayApp loadConfig "resource",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system,
    studyApi = lila.study.Env.current.api,
    importer = lila.importer.Env.current.importer,
    gamePgnDump = lila.game.Env.current.pgnDump,
    isOnline = lila.user.Env.current.isOnline,
    lightUserApi = lila.user.Env.current.lightUserApi,
    notifyApi = lila.notify.Env.current.api
  )
}
