package lila.simul

case class SimulApplicant(
    player: SimulPlayer,
    accepted: Boolean,
    rejected: Boolean // 比赛开始后申请被主持人拒绝时 = true
) {

  def is(userId: String): Boolean = player is userId
  def is(other: SimulPlayer): Boolean = player is other
}

private[simul] object SimulApplicant {

  def make(player: SimulPlayer): SimulApplicant = new SimulApplicant(
    player = player,
    accepted = false,
    rejected = false
  )
}
