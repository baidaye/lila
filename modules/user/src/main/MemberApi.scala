package lila.user

import lila.db.dsl._
import User.{ BSONFields => F }
import lila.hub.actorApi.member._

final class MemberApi(coll: Coll, lightUserApi: LightUserApi, bus: lila.common.Bus) {

  import Member.MemberBSONHandler

  def buyPayed(data: MemberBuyPayed): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find member buy order user ${data.userId} of ${data.orderId}")
      case Some(u1) => {
        val level = MemberLevel(data.level)
        val oldMember = u1.memberOrDefault
        val pointsDiff = data.points.map(_ * -1) | 0
        val newMember = oldMember.merge(level, data.days, pointsDiff, 0)

        val oldExpireAt = oldMember.levels.get(data.level).map(_.expireAt)
        val newExpireAt = newMember.levels.get(data.level).map(_.expireAt)
        coll.update(
          $id(data.userId),
          $set(
            F.member -> newMember
          )
        ) >>- (pointsDiff != 0).?? {
            bus.publish(MemberPointsChange(u1.id, "orderPay", pointsDiff, data.orderId.some), 'memberPointsChange)
          } >>- lightUserApi.invalidate(data.userId) >>
          data.inviteUser.?? { userId =>
            UserRepo.byId(userId) flatMap {
              case None => fufail(s"can not find member buy order inviteUser $userId of ${data.orderId}")
              case Some(u2) => {
                val pointsDiff = (data.payAmount * 0.5).toInt
                (pointsDiff != 0).?? {
                  coll.update(
                    $id(userId),
                    $set(
                      F.member -> u2.memberOrDefault.mergePoints(pointsDiff)
                    )
                  ).void >>- bus.publish(MemberPointsChange(u2.id, "orderPayRebate", pointsDiff, data.orderId.some), 'memberPointsChange)
                }
              }
            }
          } >> u1.belongTeamId.??(t => teamCooperator(t)).flatMap {
            _.?? { cooperator =>
              UserRepo.byId(cooperator) flatMap {
                case None => fufail(s"can not find member buy order cooperatorUser $cooperator of ${data.orderId}")
                case Some(u3) => {
                  val pointsDiff = (data.payAmount * 0.2).toInt
                  (pointsDiff != 0).?? {
                    coll.update(
                      $id(u3.id),
                      $set(
                        F.member -> u3.memberOrDefault.mergePoints(pointsDiff)
                      )
                    ).void >>- bus.publish(MemberPointsChange(u3.id, "orderInviteRebate", pointsDiff, data.orderId.some, TeamInviteRel(u1.belongTeamIdValue, u1.id).some), 'memberPointsChange)
                  }
                }
              }
            }
          } >>- bus.publish(MemberLevelChange(data.userId, "buy", data.level, oldExpireAt, newExpireAt, data.desc, data.orderId.some, none), 'memberLevelChange) >>-
          bus.publish(MemberBuyComplete(data.orderId, data.userId), 'memberBuyComplete)
      }
    }
  }

  def cardBuyPayed(data: MemberCardBuyPayed): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find user ${data.userId} of ${data.orderId}")
      case Some(u1) => {
        val pointsDiff = data.points.map(_ * -1) | 0
        (pointsDiff != 0).?? {
          val oldMember = u1.memberOrDefault
          val newMember = oldMember.mergePoints(pointsDiff)
          coll.update(
            $id(u1.id),
            $set(
              F.member -> newMember
            )
          ).void >>- bus.publish(MemberPointsChange(u1.id, "orderPay", pointsDiff, data.orderId.some), 'memberPointsChange)
        }
      } >> u1.belongTeamId.??(t => teamCooperator(t)).flatMap {
        _.?? { cooperator =>
          UserRepo.byId(cooperator) flatMap {
            case None => fufail(s"can not find member buy order cooperatorUser $cooperator of ${data.orderId}")
            case Some(u2) => {
              val pointsDiff = (data.payAmount * 0.4).toInt
              (pointsDiff != 0).?? {
                coll.update(
                  $id(u2.id),
                  $set(
                    F.member -> u2.memberOrDefault.mergePoints(pointsDiff)
                  )
                ).void >>- bus.publish(MemberPointsChange(u2.id, "orderInviteRebate", pointsDiff, data.orderId.some, TeamInviteRel(u1.belongTeamIdValue, u1.id).some), 'memberPointsChange)
              }
            }
          }
        }
      } >>- bus.publish(MemberCardBuyComplete(data.orderId, data.userId), 'memberCardBuyComplete)
    }
  }

  def openingdbSysBuyPayed(data: OpeningdbSysBuyPayed): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find openingdbSysBuy order user ${data.userId} of ${data.orderId}")
      case Some(u1) => {
        // 更新购买者的积分
        val pointsDiff = data.points.map(_ * -1) | 0
        (pointsDiff != 0).?? {
          coll.update(
            $id(u1.id),
            $set(
              F.member -> u1.memberOrDefault.mergePoints(pointsDiff)
            )
          ).void >>- bus.publish(MemberPointsChange(u1.id, "orderPay", pointsDiff, data.orderId.some), 'memberPointsChange)
        } >>
          // 俱乐部/教练 积分返点
          data.inviteUser.?? { userId =>
            UserRepo.byId(userId) flatMap {
              case None => fufail(s"can not find openingdbSysBuy order inviteUser $userId of ${data.orderId}")
              case Some(u2) => {
                val pointsDiff = (data.payAmount * 0.5).toInt
                (pointsDiff != 0).?? {
                  coll.update(
                    $id(userId),
                    $set(
                      F.member -> u2.memberOrDefault.mergePoints(pointsDiff)
                    )
                  ).void >>- bus.publish(MemberPointsChange(u2.id, "orderPayRebate", pointsDiff, data.orderId.some), 'memberPointsChange)
                }
              }
            }
          } >>- bus.publish(OpeningdbSysBuyComplete(data.orderId, data.userId), 'openingdbSysBuyComplete)
      }
    }
  }

  def openingdbCreateBuyPayed(data: OpeningdbCreateBuyPayed): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find openingdbCreateBuy order user ${data.userId} of ${data.orderId}")
      case Some(u1) => {
        // 更新购买者的积分
        val pointsDiff = data.points.map(_ * -1) | 0
        (pointsDiff != 0).?? {
          coll.update(
            $id(u1.id),
            $set(
              F.member -> u1.memberOrDefault.mergePoints(pointsDiff)
            )
          ).void >>- bus.publish(MemberPointsChange(u1.id, "orderPay", pointsDiff, data.orderId.some), 'memberPointsChange)
        } >>
          // 俱乐部/教练 积分返点
          data.inviteUser.?? { userId =>
            UserRepo.byId(userId) flatMap {
              case None => fufail(s"can not find openingdbCreateBuy order inviteUser $userId of ${data.orderId}")
              case Some(u2) => {
                val pointsDiff = (data.payAmount * 0.5).toInt
                (pointsDiff != 0).?? {
                  coll.update(
                    $id(userId),
                    $set(
                      F.member -> u2.memberOrDefault.mergePoints(pointsDiff)
                    )
                  ).void >>- bus.publish(MemberPointsChange(u2.id, "orderPayRebate", pointsDiff, data.orderId.some), 'memberPointsChange)
                }
              }
            }
          } >>- bus.publish(OpeningdbSysBuyComplete(data.orderId, data.userId), 'openingdbCreateBuyComplete)
      }
    }
  }

  private def teamCooperator(teamId: String): Fu[Option[String]] =
    bus.ask[Option[String]]('teamCooperator) { lila.hub.actorApi.team.TeamCooperator(teamId, _) }

  def cardUse(data: MemberCardUse): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find user ${data.userId}")
      case Some(u1) => {
        val level = MemberLevel(data.level)
        val oldMember = u1.memberOrDefault
        val newMember = oldMember.mergeLevel(level, data.days)

        val oldExpireAt = oldMember.levels.get(data.level).map(_.expireAt)
        val newExpireAt = newMember.levels.get(data.level).map(_.expireAt)
        coll.update(
          $id(data.userId),
          $set(
            F.member -> newMember
          )
        ).void >>- bus.publish(MemberLevelChange(data.userId, "card", data.level, oldExpireAt, newExpireAt, data.desc, none, data.cardId.some), 'memberLevelChange) >>- {
            // 如果银牌会员没有过期 使用金牌会员卡时 也要将银牌会员延期
            if (oldMember.isSilverAvailable && !oldMember.silverLevel.??(_.isForever) && level == MemberLevel.Gold) {
              val newSilverMember = newMember.mergeLevel(MemberLevel.Silver, data.days)

              val oldSilverExpireAt = oldMember.silverLevel.map(_.expireAt)
              val newSilverExpireAt = newSilverMember.silverLevel.map(_.expireAt)
              coll.update(
                $id(data.userId),
                $set(
                  F.member -> newSilverMember
                )
              ).void >>- bus.publish(MemberLevelChange(data.userId, "card", MemberLevel.Silver.code, oldSilverExpireAt, newSilverExpireAt, s"${data.desc}，顺延银牌会员到期时间", none, data.cardId.some), 'memberLevelChange)
            }
          } >>- lightUserApi.invalidate(data.userId)
      }
    }
  }

  def exchangeCardUse(data: MemberExchangeCardUse): Funit = {
    UserRepo.byId(data.userId) flatMap {
      case None => fufail(s"can not find user ${data.userId}")
      case Some(u1) => {
        val level = MemberLevel(data.level)
        val oldMember = u1.memberOrDefault
        val newMember = oldMember.mergeLevel(level, data.days)

        val oldExpireAt = oldMember.levels.get(data.level).map(_.expireAt)
        val newExpireAt = newMember.levels.get(data.level).map(_.expireAt)
        coll.update(
          $id(data.userId),
          $set(
            F.member -> newMember
          )
        ).void >>- bus.publish(MemberLevelChange(data.userId, "exchange", data.level, oldExpireAt, newExpireAt, data.desc, none, data.cardId.toString.some), 'memberLevelChange) >>- {
            // 如果银牌会员没有过期 使用金牌会员卡时 也要将银牌会员延期
            if (oldMember.isSilverAvailable && !oldMember.silverLevel.??(_.isForever) && level == MemberLevel.Gold) {
              val newSilverMember = newMember.mergeLevel(MemberLevel.Silver, data.days)

              val oldSilverExpireAt = oldMember.silverLevel.map(_.expireAt)
              val newSilverExpireAt = newSilverMember.silverLevel.map(_.expireAt)
              coll.update(
                $id(data.userId),
                $set(
                  F.member -> newSilverMember
                )
              ).void >>- bus.publish(MemberLevelChange(data.userId, "exchange", MemberLevel.Silver.code, oldSilverExpireAt, newSilverExpireAt, s"${data.desc}，顺延银牌会员到期时间", none, data.cardId.toString.some), 'memberLevelChange)
            }
          } >>- lightUserApi.invalidate(data.userId)
      }
    }
  }

  def signup(u: User): Funit = {
    coll.update(
      $id(u.id),
      $set(
        F.member -> u.memberOrDefault
      )
    ).void
  }

  def expiredTesting(): Funit = {
    // 拥有黄金会员（已到期） && code=黄金会员 && 拥有白银会员（未到期） => 更新为白银会员
    UserRepo.goldExpiredAndSilverNotExpired().flatMap { ids =>
      coll.update(
        $inIds(ids),
        $set(F.mcode -> MemberLevel.Silver.code),
        multi = true
      ).void >>- ids.foreach(lightUserApi.invalidate)
    } >> {
      // 拥有黄金会员（已到期） && code=黄金会员 && （不拥有白银会员 或 已到期） => 更新为注册会员
      UserRepo.goldExpiredAndSilverExpired().flatMap { ids =>
        coll.update(
          $inIds(ids),
          $set(F.mcode -> MemberLevel.General.code),
          multi = true
        ).void >>- ids.foreach(lightUserApi.invalidate)
      }
    } >> {
      // 拥有白银会员（已到期）&& code=白银会员 => 更新为注册会员
      UserRepo.silverExpired() flatMap { ids =>
        coll.update(
          $inIds(ids),
          $set(F.mcode -> MemberLevel.General.code),
          multi = true
        ).void >>- ids.foreach(lightUserApi.invalidate)
      }
    }
  }

}
