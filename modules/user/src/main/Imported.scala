package lila.user

import reactivemongo.bson.Macros

case class Imported(userId: User.ID, teamId: String)

object Imported {
  implicit val ImportedBSONHandler = Macros.handler[Imported]
}
