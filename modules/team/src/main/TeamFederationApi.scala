package lila.team

import lila.db.{ DbImage, Photographer }
import lila.team.DataForm.federationData._
import lila.user.User

final class TeamFederationApi(
    notifier: Notifier,
    photographer: Photographer
) {

  def byId(id: TeamFederation.ID): Fu[Option[TeamFederation]] = TeamFederationRepo.byId(id)

  def create(data: TeamFederationCreate, me: User): Fu[TeamFederation] = {
    val teamId = me.teamIdValue
    TeamFederationRepo.findNextId flatMap { id =>
      val federation = TeamFederation.make(
        id = id.toString,
        teamId = teamId,
        teamOwnerId = me.id,
        name = data.name,
        description = data.description,
        open = data.open,
        createdBy = me.id
      )
      TeamFederationRepo.insert(federation) >>
        TeamFederationMemberRepo.insert(
          TeamFederationMember.make(
            id.toString,
            teamId,
            teamId,
            me.id,
            TeamFederationMember.Role.Owner,
            me.id
          )
        ) inject federation
    }
  }

  def update(old: TeamFederation, data: TeamFederationUpdate): Funit = {
    val federation = old.copy(
      logo = data.logo,
      description = data.description
    )
    TeamFederationRepo.update(federation)
  }

  def setting(old: TeamFederation, data: TeamFederationSetting): Funit = {
    val federation = old.copy(
      open = data.open,
      visibility = TeamFederation.Visibility(data.visibility)
    )
    TeamFederationRepo.update(federation)
  }

  def disable(federation: TeamFederation): Funit = {
    TeamFederationRepo.setEnabled(federation.id, false) >> {
      TeamFederationMemberRepo.findByFederation(federation.id).flatMap { members =>
        members.map { member =>
          notifier.federationClosed(federation, member.teamOwnerId, federation.teamOwnerId)
        }.sequenceFu.void
      }
    }
  }

  def requestable(federation: TeamFederation, user: User): Fu[Boolean] = {
    val memberTeamId = user.teamIdValue
    for {
      memberExists <- TeamFederationMemberRepo.exists(federation.id, memberTeamId)
      requestExists <- TeamFederationRequestRepo.exists(federation.id, memberTeamId)
    } yield !memberExists && !requestExists && federation.enabled
  }

  def joinRequest(federation: TeamFederation, message: String, team: Team, me: User): Funit =
    requestable(federation, me) flatMap {
      _ ?? {
        val request = TeamFederationRequest.make(federation.id, federation.teamId, team.id, me.id, message, me.id)
        if (federation.isOpen) {
          doJoin(federation, request, me.id)
        } else {
          TeamFederationRequestRepo.insert(request) >> notifier.federationJoinRequest(federation, team)
        }
      }
    }

  def joinProcess(federation: TeamFederation, request: TeamFederationRequest, me: User, accept: Boolean): Funit = {
    for {
      _ <- TeamFederationRequestRepo.remove(request.id)
      _ <- accept.??(doJoin(federation, request, me.id))
    } yield ()
  }

  def doJoin(federation: TeamFederation, request: TeamFederationRequest, userId: User.ID): Funit = {
    TeamRepo.byId(request.teamId).flatMap {
      _.?? { team =>
        val member = TeamFederationMember.make(federation.id, federation.teamId, request.teamId, team.createdBy, TeamFederationMember.Role.Member, userId)
        for {
          _ <- TeamFederationMemberRepo.insert(member)
          _ <- TeamFederationRepo.incMembers(federation.id, 1)
          _ <- notifier.federationJoinAccept(federation, team.createdBy, federation.teamOwnerId)
        } yield ()
      }
    }
  }

  def quit(federation: TeamFederation, member: TeamFederationMember): Funit =
    (!member.isOwner).?? {
      TeamFederationMemberRepo.remove(TeamFederationMember.makeId(federation.id, member.teamId)) >>
        TeamFederationRepo.incMembers(federation.id, -1)
    }

  def kick(federation: TeamFederation, member: TeamFederationMember): Funit =
    (!member.isOwner).?? {
      TeamFederationMemberRepo.remove(TeamFederationMember.makeId(federation.id, member.teamId)) >>
        TeamFederationRepo.incMembers(federation.id, -1)
    }

  def changeTeamOwner(teamId: Team.ID, ownerId: User.ID): Funit =
    TeamFederationRepo.changeTeamOwner(teamId, ownerId) >>
      TeamFederationMemberRepo.changeTeamOwner(teamId, ownerId)

  def uploadPicture(id: String, picture: Photographer.Uploaded, processFile: Boolean = false): Fu[DbImage] =
    photographer(id, picture, processFile)
}
