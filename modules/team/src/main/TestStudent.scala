package lila.team

import chess.Color
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import play.api.libs.json.{ JsObject, Json }

case class TestStudent(
    _id: TestStudent.ID,
    studentId: User.ID,
    teamId: Team.ID,
    tplId: TestTemplate.ID,
    inviteId: TestInvite.ID,
    remainsTime: Int, // seconds
    tpl: LightTestTemplate,
    items: TestStudentItems,
    status: TestStudent.Status,
    deadline: DateTime,
    startAt: Option[DateTime],
    finishAt: Option[DateTime],
    isPassed: Option[Boolean],
    remark: Option[String],
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

  def spendTime = Math.ceil((tpl.limitTimeSeconds - remainsTime) / 60.0)

  def newRemainsTime = Math.max(tpl.limitTimeSeconds - (startAt.map(st => (DateTime.now.getMillis - st.getMillis) / 1000) | 0), 0).toInt

  def allPuzzles = items.puzzles.length
  def finishPuzzles = items.puzzles.count(_.isTry)
  def rightPuzzles = items.puzzles.count(_.isRight)
  def isInItems(puzzleId: String, index: Int) = items.puzzles.zipWithIndex.exists {
    case (pz, idx) => idx == index && pz.puzzle.mixId == puzzleId
  }

  def canRedo(puzzleId: String, index: Int) = items.puzzles.zipWithIndex.exists {
    case (pz, idx) => {
      idx == index && pz.puzzle.mixId == puzzleId && pz.retry < tpl.maxRetry
    }
  }

  def isCreated = status == TestStudent.Status.Created
  def isStarted = status == TestStudent.Status.Started
  def isFinished = status == TestStudent.Status.Finished
  def isExpired = status == TestStudent.Status.Expired

  def isStudent(userId: User.ID) = userId == studentId
  def isCreator(userId: User.ID) = userId == createdBy

}

object TestStudent {

  type ID = String

  case class WithUser(testStu: TestStudent, user: User)

  def make(
    tpl: TestTemplate,
    invite: TestInvite,
    items: TestStudentItems,
    studentId: User.ID,
    userId: User.ID
  ) = TestStudent(
    _id = Random nextString 8,
    studentId = studentId,
    teamId = tpl.teamId,
    tplId = tpl.id,
    inviteId = invite.id,
    remainsTime = tpl.limitTime * 60,
    tpl = tpl.light,
    items = items,
    status = Status.Created,
    deadline = invite.deadline,
    startAt = None,
    finishAt = None,
    isPassed = None,
    remark = None,
    createdAt = DateTime.now,
    createdBy = userId
  )

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "未完成")
    case object Started extends Status("started", "考核中")
    case object Finished extends Status("finished", "已完成")
    case object Expired extends Status("expired", "已过期")

    val all = List(Created, Started, Finished, Expired)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): Status = byId get id err s"Bad Status $id"
  }

}

case class MiniPuzzle(id: Int, fen: String, color: Color, rating: Int, depth: Int, lines: String, lastMove: Option[String]) {

  override def toString: String = id.toString

  val plusNum = 20221118
  val numCharMap = Map("0" -> "T", "1" -> "x", "2" -> "e", "3" -> "L", "4" -> "i", "5" -> "o", "6" -> "U", "7" -> "d", "8" -> "j", "9" -> "R")
  def reverseNumCharMap = numCharMap.toList.map {
    case (num, ch) => ch -> num
  }.toMap

  def mixId = (id + plusNum).toString.toCharArray.map(num => numCharMap.getOrElse(num.toString, "*")).mkString

  def deMixId(id: String) = id.toString.toCharArray.map(ch => reverseNumCharMap.getOrElse(ch.toString, "0")).mkString.toInt - plusNum

  def linesJson = {
    Json.parse(lines).asOpt[JsObject] err s"can not parse puzzle lines: $lines"
  }

}

case class PuzzleNode(san: String, uci: String, fen: String, lastMove: Option[String])
case class PuzzleMove(index: Int, white: Option[PuzzleNode], black: Option[PuzzleNode]) {
  override def toString(): String = {
    val text = (white, black) match {
      case (Some(w), Some(b)) => s" ${w.san} ${b.san}"
      case (Some(w), None) => s" ${w.san}"
      case (None, Some(b)) => s".. ${b.san}"
      case _ => ""
    }
    s"$index.$text"
  }
}
case class PuzzleResult(win: Boolean, moves: List[PuzzleMove])
case class MiniPuzzleWithResult(puzzle: MiniPuzzle, retry: Int, result: Option[PuzzleResult]) {

  def isTry = result.isDefined

  def isRight = result.exists(_.win)

  def apply(puzzle: MiniPuzzle): MiniPuzzleWithResult = MiniPuzzleWithResult(puzzle, 0, None)

}
case class TestStudentItems(puzzles: List[MiniPuzzleWithResult]) {

  def finish(index: Int, win: Boolean, moves: List[PuzzleMove]) = puzzles.zipWithIndex.map {
    case (pwr, idx) => if (index == idx) pwr.copy(pwr.puzzle, 1, PuzzleResult(win, moves).some) else pwr
  }

}
