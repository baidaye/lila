package lila.team

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import ClockInSetting._
import lila.task.TTaskTemplate

case class ClockInSetting(
    _id: ID,
    teamId: String,
    templateId: String,
    name: String,
    startDate: DateTime,
    period: Period,
    status: Status,
    coinTeam: Option[String],
    coinRule: Option[Int],
    studentIds: List[User.ID], // 报名学员
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def isCreator(userId: User.ID) = userId == createdBy

  def editable = status == Status.Created

  def canPublish = status == Status.Created

  def canClose = status == Status.Created || status == Status.Published

  def canJoin = status == Status.Published

  def nb = studentIds.size

  def isJoined(userId: User.ID) = studentIds.contains(userId)

  def isJoined(userId: Option[User.ID]) = userId.??(studentIds.contains)

  def hasCoin = coinRule.??(_ > 0)

}

object ClockInSetting {

  type ID = String

  case class WithTask(setting: ClockInSetting, tpl: TTaskTemplate)

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "未发布")
    case object Published extends Status("published", "已发布")
    case object Closed extends Status("closed", "已关闭")

    def all = List(Created, Published, Closed)

    def current = List(Created, Published)

    def history = List(Closed)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Status = all.find(_.id == id) getOrElse Created
  }

  sealed abstract class Period(val id: String, val name: String)
  object Period {
    case object Week extends Period("week", "周")
    case object Month extends Period("month", "月")

    def all = List(Week, Month)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Period = all.find(_.id == id) err s"can not apply period $id"
  }

  def make(
    teamId: String,
    templateId: String,
    name: String,
    startDate: DateTime,
    period: String,
    coinTeam: Option[String],
    coinRule: Option[Int],
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    ClockInSetting(
      _id = Random nextString 8,
      teamId = teamId,
      templateId = templateId,
      name = name,
      startDate = startDate,
      period = Period(period),
      status = Status.Created,
      coinTeam = coinTeam,
      coinRule = coinRule,
      studentIds = Nil,
      createdAt = now,
      updatedAt = now,
      createdBy = createdBy,
      updatedBy = createdBy
    )
  }

}
