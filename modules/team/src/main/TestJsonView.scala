package lila.team

import lila.user.User
import play.api.libs.json._

final class TestJsonView(animationDuration: scala.concurrent.duration.Duration) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "coords" -> p.coords,
    "rookCastle" -> p.rookCastle,
    "animationDuration" -> p.animationFactor * animationDuration.toMillis,
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d
  )

  def testStuData(testStu: TestStudent) =
    Json.obj(
      "id" -> testStu.id,
      "studentId" -> testStu.studentId,
      "teamId" -> testStu.teamId,
      "tplId" -> testStu.tplId,
      "inviteId" -> testStu.inviteId,
      "remainsTime" -> testStu.newRemainsTime,
      "spendTime" -> testStu.spendTime,
      "tpl" -> Json.obj(
        "id" -> testStu.tpl.id,
        "name" -> testStu.tpl.name,
        "limitTime" -> testStu.tpl.limitTime,
        "nbQ" -> testStu.tpl.nbQ,
        "passedQ" -> testStu.tpl.passedQ,
        "maxRetry" -> testStu.tpl.maxRetry,
        "desc" -> testStu.tpl.desc
      ),
      "items" -> Json.obj(
        "puzzles" -> JsArray(
          testStu.items.puzzles.map { pz =>
            val puzzle = pz.puzzle
            Json.obj(
              "puzzle" -> Json.obj(
                "id" -> puzzle.mixId,
                "fen" -> puzzle.fen,
                "color" -> puzzle.color.name,
                "depth" -> puzzle.depth,
                "lines" -> puzzle.linesJson,
                "lastMove" -> puzzle.lastMove
              ),
              "result" -> pz.result.map { result =>
                Json.obj(
                  "win" -> result.win,
                  "completed" -> true,
                  "moves" -> movesJson(result.moves)
                )
              },
              "retry" -> pz.retry
            )
          }
        )
      ),
      "rightQ" -> testStu.rightPuzzles,
      "status" -> Json.obj(
        "id" -> testStu.status.id,
        "name" -> testStu.status.name
      ),
      "deadline" -> testStu.deadline.toString("yyyy-MM-dd HH:mm:ss"),
      "startAt" -> testStu.startAt.map(_.toString("yyyy-MM-dd HH:mm:ss")),
      "finishAt" -> testStu.finishAt.map(_.toString("yyyy-MM-dd HH:mm:ss")),
      "isPassed" -> testStu.isPassed
    )

  private def movesJson(moves: List[PuzzleMove]) =
    JsArray(
      moves.map { move =>
        Json.obj(
          "index" -> move.index,
          "white" -> move.white.map { w =>
            Json.obj(
              "san" -> w.san,
              "uci" -> w.uci,
              "fen" -> w.fen,
              "lastMove" -> w.lastMove
            )
          },
          "black" -> move.black.map { b =>
            Json.obj(
              "san" -> b.san,
              "uci" -> b.uci,
              "fen" -> b.fen,
              "lastMove" -> b.lastMove
            )
          }
        )
      }
    )

  def lightResult(testStu: TestStudent) =
    Json.obj(
      "id" -> testStu.id,
      "isPassed" -> testStu.isPassed,
      "passedQ" -> testStu.tpl.passedQ,
      "rightQ" -> testStu.rightPuzzles,
      "spendTime" -> testStu.spendTime
    )

}

