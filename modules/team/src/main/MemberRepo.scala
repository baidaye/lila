package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import reactivemongo.bson._
import lila.user.{ User, UserRepo }
import lila.common.paginator.Paginator
import reactivemongo.api.ReadPreference
import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework.{ GroupField, Match, Project, SumValue }

object MemberRepo {

  // dirty
  private val coll = Env.current.colls.member

  import BSONHandlers._

  type ID = String

  private type NbMembers = Int

  def byId(teamId: ID, userId: ID) = {
    coll.byId[Member](Member.makeId(teamId, userId))
  }

  def byOrderedIds(ids: List[String]): Fu[List[Member]] =
    coll.byOrderedIds[Member, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def memberOptionByOrderedIds(ids: List[String]): Fu[List[Option[Member]]] =
    coll.optionsByOrderedIds[Member, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def memberFromSecondary(teamId: Seq[String], userId: String): Fu[List[Member]] =
    coll.byOrderedIds[Member, String](
      teamId.map(Member.makeId(_, userId)),
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def memberFromSecondary(teamId: String, userIds: Seq[String]): Fu[List[Member]] =
    coll.byOrderedIds[Member, String](
      userIds.map(Member.makeId(teamId, _)),
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def memberOptionFromSecondary(teamId: String, userIds: Seq[String]): Fu[List[Option[Member]]] =
    coll.optionsByOrderedIds[Member, String](
      userIds.map(Member.makeId(teamId, _)),
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def notCoach(teamId: String, userIds: Seq[String]): Fu[Seq[MemberWithUser]] = {
    coll.list[Member](teamQuery(teamId) ++ $doc("user" $in userIds, "role" $ne "coach")).flatMap { withUsers _ }
  }

  def memberByTeam(teamId: String): Fu[List[Member]] =
    coll.list[Member](teamQuery(teamId))

  def coachByTeam(teamId: String): Fu[List[MemberWithUser]] =
    coll.list[Member](teamQuery(teamId) ++ $doc("role" $in List("owner", "coach"))).flatMap { withUsers _ }.map { list =>
      list.filter(_.user.isCoach).toList
    }

  def membersWithUser(teamId: String): Fu[List[MemberWithUser]] =
    coll.list[Member](teamQuery(teamId) ++ $doc("role" -> "trainee")).flatMap(withUsers).map(_.toList)

  def membersWithUserByCampus(campusId: String): Fu[List[MemberWithUser]] =
    coll.list[Member]($doc("campus" -> campusId, "role" -> "trainee")).flatMap(withUsers).map(_.toList)

  def membersWithUser(teamId: String, userIds: List[String]): Fu[List[MemberWithUser]] =
    coll.list[Member](teamQuery(teamId) ++ $doc("user" $in userIds, "role" -> "trainee")).flatMap(withUsers).map(_.toList)

  def allMembersWithUser(teamId: String, userIds: List[String]): Fu[List[MemberWithUser]] =
    coll.list[Member](teamQuery(teamId) ++ $doc("user" $in userIds)).flatMap(withUsers).map(_.toList)

  def allMembersWithUser(teamId: String): Fu[List[MemberWithUser]] =
    coll.list[Member](teamQuery(teamId)).flatMap(withUsers).map(_.toList)

  def membersByUser(userId: ID): Fu[List[Member]] =
    coll.list[Member]($doc("user" -> userId))

  def findByText(teamId: String, userIds: List[String], q: String): Fu[List[MemberWithUser]] =
    coll.find(teamQuery(teamId) ++ $or($doc("user" $regex (q, "i")), $doc("user" $in userIds)))
      .list[Member](10)
      .flatMap(withUsers)
      .map(_.toList)

  def userIdsByUserIds(teamId: ID, userIds: List[String]): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", (teamQuery(teamId) ++ $doc("user" $in userIds, "role" -> "trainee")).some)

  def userIdsByTeam(teamId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", $doc("team" -> teamId).some)

  def teamIdsByUser(userId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("team", $doc("user" -> userId).some)

  def ownerTeamIdsByUser(userId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("team", $doc("role" -> "owner", "user" -> userId).some)

  def coachTeamIdsByUser(userId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("team", $doc("role" -> "coach", "user" -> userId).some)

  def managersByTeam(teamId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", (teamQuery(teamId) ++ $doc("role" $in List("owner", "coach"))).some)

  def coachAllTeamUserIds(userId: ID): Fu[Set[ID]] =
    coachTeamIdsByUser(userId) flatMap { teamIds =>
      coll.distinct[String, Set]("user", $doc("team" $in teamIds, "role" -> "trainee").some)
    }

  def ownerOrCoach(teamId: ID, userId: ID): Fu[Option[Member]] =
    coll.uno[Member](selectId(teamId, userId) ++ $doc("role" $in List("owner", "coach")))

  def teamOwner(teamId: ID): Fu[Option[Member]] =
    coll.uno[Member]($doc("role" -> "owner", "team" -> teamId))

  def teamOwners(teamIds: Set[ID]): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", $doc("role" -> "owner", "team" $in teamIds).some)

  def teamMembers(teamIds: Set[ID]): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", $doc("role" -> "trainee", "team" $in teamIds).some)

  def teamIdsByMember(userId: ID): Fu[Set[ID]] =
    coll.distinct[String, Set]("team", $doc("user" -> userId, "role" -> "trainee").some)

  def membersByUsers(userIds: Seq[String]): Fu[List[Member]] =
    coll.find($doc("user" $in userIds)).list[Member]()

  def removeByteam(teamId: ID): Funit =
    coll.remove(teamQuery(teamId)).void

  def removeByUser(userId: ID): Funit =
    coll.remove(userQuery(userId)).void

  def exists(teamId: ID, userId: ID): Fu[Boolean] =
    coll.exists(selectId(teamId, userId))

  def existsByCampus(campusIds: List[String], userId: ID): Fu[Boolean] =
    coll.exists($doc("campus" $in campusIds, "user" -> userId))

  def add(
    teamId: String,
    userId: String,
    campusId: String,
    role: Member.Role = Member.Role.Trainee,
    tags: Option[MemberTags] = None,
    rating: Option[Int] = None,
    clazzIds: Option[List[String]] = None
  ): Funit =
    coll.insert(Member.make(team = teamId, user = userId, campus = campusId, role = role, tags = tags, rating = rating, clazzIds = clazzIds)).void

  def batchAdd(members: List[Member]): Funit =
    coll.bulkInsert(
      documents = members.map(BSONHandlers.MemberBSONHandler.write).toStream,
      ordered = true
    ).void

  def remove(teamId: String, userId: String): Funit =
    coll.remove(selectId(teamId, userId)).void

  def countByTeam(teamId: String): Fu[Int] =
    coll.countSel(teamQuery(teamId))

  def byRole(teamId: ID, role: Member.Role): Fu[Set[ID]] =
    coll.distinct[String, Set]("user", (teamQuery(teamId) ++ roleQuery(role)).some)

  def addRole(teamId: ID, userId: ID, role: Member.Role): Funit =
    byId(teamId, userId).flatMap {
      case None => fufail(s"can not find member $userId, of $teamId")
      case Some(member) => {
        var roles = member.role
        if (roles.contains(Member.Role.Trainee)) {
          roles = roles - Member.Role.Trainee
        }
        roles = roles + role
        coll.update(
          selectId(teamId, userId),
          $set("role" -> roles)
        ).void
      }
    }

  def removeRole(teamId: ID, userId: ID, role: Member.Role): Funit =
    byId(teamId, userId).flatMap {
      case None => fufail(s"can not find member $userId, of $teamId")
      case Some(member) => {
        var roles = member.role - role
        if (roles.isEmpty) {
          roles = roles + Member.Role.Trainee
        }
        coll.update(
          selectId(teamId, userId),
          $set("role" -> roles)
        ).void
      }
    }

  def updateMember(member: Member): Funit =
    coll.update($id(member.id), member).void

  def initMembersRating(teamId: ID, defaultRating: Int): Funit =
    coll.update(
      teamQuery(teamId) ++ $doc("rating" $exists false),
      $set(
        "rating" -> EloRating(defaultRating, 0)
      ),
      multi = true
    ).void

  def initMembersCoin(teamId: ID, defaultCoin: Int): Funit =
    coll.update(
      teamQuery(teamId) ++ $doc("coin" $exists false),
      $set("coin" -> defaultCoin),
      multi = true
    ).void

  def removeTag(teamId: ID, field: String) = coll.update(
    teamQuery(teamId),
    $unset(s"tags.$field")
  )

  def memberWithUser(id: ID): Fu[Option[MemberWithUser]] = for {
    memberOption <- coll.byId[Member](id)
    userOption <- memberOption.??(m => UserRepo.byId(m.user))
  } yield {
    memberOption |@| userOption apply {
      case (member, user) => MemberWithUser(member, user)
    }
  }

  def addClazz(teamId: String, userId: ID, clazzId: String): Funit =
    coll.update(
      selectId(teamId, userId),
      $addToSet("clazzIds" -> clazzId)
    ).void

  def addClazzs(memberIds: List[String], clazzId: String): Funit =
    coll.update(
      $inIds(memberIds),
      $addToSet("clazzIds" -> clazzId),
      multi = true
    ).void

  def removeClazz(teamId: String, userId: ID, clazzId: String): Funit =
    coll.update(
      selectId(teamId, userId),
      $pull("clazzIds" -> clazzId)
    ).void

  def ratingPage(teamId: String, markMap: Map[String, Option[String]], page: Int, q: Option[String], clazzId: Option[String]): Fu[Paginator[MemberWithUser]] = {
    val filterMarkUserIds = q.?? { txt =>
      markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(txt.toLowerCase)
        }
      }.keySet
    }

    var doc = $doc("team" -> teamId)
    clazzId.foreach(c =>
      doc = doc ++ $doc("clazzIds" -> c))
    q.foreach(q =>
      doc = doc ++ $or($doc("user" $regex (q, "i")), $doc("user" $in filterMarkUserIds)))

    val adapter = new Adapter[Member](
      collection = coll,
      selector = doc,
      projection = $empty,
      sort = $doc("rating.r" -> -1, "user" -> 1)
    ) mapFutureList withUsers
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  def coinPage(teamId: String, markMap: Map[String, Option[String]], page: Int, q: Option[String], campus: Option[String]): Fu[Paginator[MemberWithUser]] = {
    val filterMarkUserIds = q.?? { txt =>
      markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(txt.toLowerCase)
        }
      }.keySet
    }

    var doc = $doc("team" -> teamId)
    campus.foreach(c =>
      doc = doc ++ $doc("campus" -> c))
    q.foreach(q =>
      doc = doc ++ $or($doc("user" $regex (q, "i")), $doc("user" $in filterMarkUserIds)))

    val adapter = new Adapter[Member](
      collection = coll,
      selector = doc,
      projection = $empty,
      sort = $doc("coin" -> -1, "user" -> 1)
    ) mapFutureList withUsers
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  private def withUsers(members: Seq[Member]): Fu[Seq[MemberWithUser]] =
    UserRepo.withColl {
      _.byOrderedIds[User, User.ID](members.map(_.user))(_.id)
    } map { users =>
      members zip users collect {
        case (coach, user) => MemberWithUser(coach, user)
      }
    }

  def incMemberCoin(memberId: String, diffCoin: Int): Funit =
    coll.update(
      $id(memberId),
      $inc("coin" -> diffCoin),
      multi = true
    ).void

  def ratingDistribution(team: Team, clazzId: Option[String]) = {
    coll.aggregateList(
      Match($doc("team" -> team.id) ++ clazzId.??(c => $doc("clazzIds" -> c))),
      List(
        Project($doc(
          "_id" -> false,
          "r" -> $doc(
            "$subtract" -> $arr("$rating.r", $doc("$mod" -> $arr("$rating.r", EloRating.group)))
          )
        )),
        GroupField("r")("nb" -> SumValue(1))
      ),
      maxDocs = Int.MaxValue,
      ReadPreference.secondaryPreferred
    ).map { res =>
        val hash: Map[Int, NbMembers] = res.flatMap { obj =>
          for {
            rating <- obj.getAs[Double]("_id").map(_.toInt)
            nb <- obj.getAs[NbMembers]("nb")
          } yield rating -> nb
        }(scala.collection.breakOut)
        (team.ratingSettingOrDefault.minRating to EloRating.max by EloRating.group).map { r =>
          hash.getOrElse(r, 0)
        }.toList
      }
  }

  def ratingRank(m: Member) =
    m.rating match {
      case None => fuccess(0)
      case Some(r) => coll.countSel(teamQuery(m.team) ++ $doc("rating.r" $gt r.rating)).map(_ + 1)
    }

  def selectId(teamId: ID, userId: ID) = $id(Member.makeId(teamId, userId))
  def teamQuery(teamId: ID) = $doc("team" -> teamId)
  def userQuery(userId: ID) = $doc("user" -> userId)
  def roleQuery(role: Member.Role) = $doc("role" -> role.id)
}
