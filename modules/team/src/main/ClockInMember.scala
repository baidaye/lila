package lila.team

import lila.user.User
import org.joda.time.DateTime
import ClockInMember._

case class ClockInMember(
    _id: ID,
    teamId: String,
    templateId: String,
    settingId: String,
    clockInTaskId: String,
    taskId: String,
    userId: User.ID,
    date: DateTime,
    nb: Int, // 当前完成的数量
    isComplete: Boolean, // 是否已经完成
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def dateString = date.toString("MM月dd日")

}

object ClockInMember {

  type ID = String

  case class WithUser(member: ClockInMember, user: User)

  def makeId(clockInTaskId: String, userId: User.ID) = s"$clockInTaskId@$userId"

  def make(
    teamId: String,
    templateId: String,
    settingId: String,
    clockInTaskId: String,
    taskId: String,
    userId: User.ID,
    date: DateTime,
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    ClockInMember(
      _id = makeId(clockInTaskId, userId),
      teamId = teamId,
      templateId = templateId,
      settingId = settingId,
      clockInTaskId = clockInTaskId,
      taskId = taskId,
      userId = userId,
      date = date,
      nb = 0,
      isComplete = false,
      createdAt = now,
      updatedAt = now,
      createdBy = createdBy,
      updatedBy = createdBy
    )
  }

}
