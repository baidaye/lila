package lila.team

import actorApi._
import akka.actor.ActorSelection
import lila.db.dsl._
import makeTimeout.large
import lila.mod.ModlogApi
import lila.user.{ User, UserRepo }
import org.joda.time.Period
import reactivemongo.api.{ Cursor, ReadPreference }
import lila.db.{ DbImage, Photographer }
import lila.game.Game
import lila.user.User.ID
import akka.pattern.ask
import com.github.stuxuhai.jpinyin.{ PinyinFormat, PinyinHelper }
import lila.hub.actorApi.contest.{ ContestBoard, GetContestBoard }
import lila.hub.actorApi.offContest.{ OffContestBoard, OffContestRoundResult, OffContestUser }
import lila.hub.actorApi.relation.{ GetMark, GetMarks, SetMark }
import lila.hub.actorApi.task.ChangeStatus
import lila.hub.actorApi.team._
import lila.hub.actorApi.timeline.{ Propagate, TeamCreate, TeamJoin }
import scala.math.BigDecimal.RoundingMode
import lila.team.DataForm.teamData._
import lila.team.DataForm.memberData._
import lila.team.DataForm.campusData._
import lila.team.DataForm.tagData._
import lila.team.Team.{ TeamWithMemberRank, TeamWithMember }

final class TeamApi(
    coll: Colls,
    cached: Cached,
    notifier: Notifier,
    bus: lila.common.Bus,
    indexer: ActorSelection,
    timeline: ActorSelection,
    modLog: ModlogApi,
    photographer: Photographer,
    contestActor: akka.actor.ActorSelection,
    teamContestActor: akka.actor.ActorSelection,
    markActor: ActorSelection,
    adminUid: String,
    isOnline: User.ID => Boolean,
    isPlaying: User.ID => Boolean
) {

  import BSONHandlers._

  val creationPeriod = Period weeks 1

  def team(id: Team.ID) = coll.team.byId[Team](id)

  def ownerOrCoach(id: Team.ID, userId: String) =
    for {
      member <- MemberRepo.ownerOrCoach(id, userId)
      team <- member.??(_ => coll.team.byId[Team](id))
    } yield team

  import play.api.libs.json.Json
  def teamJson(team: Option[Team]) =
    team.map { t =>
      Json.obj(
        "id" -> t.id,
        "name" -> t.name,
        "coinSetting" -> Json.obj(
          "open" -> t.coinSettingOrDefault.open,
          "name" -> t.coinSettingOrDefault.name,
          "singleVal" -> t.coinSettingOrDefault.singleVal
        )
      )
    }

  def teamOptionFromSecondary(ids: Seq[String]): Fu[List[Option[Team]]] =
    coll.team.optionsByOrderedIds[Team, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def teamFromSecondary(ids: Seq[String]): Fu[List[Team]] =
    coll.team.byOrderedIds[Team, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def request(id: Team.ID) = coll.request.byId[Request](id)

  def invite(id: Team.ID) = coll.invite.byId[Invite](id)

  def create(setup: TeamSetup, me: User): Option[Fu[Team]] = me.canTeam option {
    val s = setup.trim
    findNextId flatMap { id =>
      val team = Team.make(
        id = id.toString,
        name = s.name,
        province = s.province,
        city = s.city,
        description = s.description,
        open = s.isOpen,
        createdBy = me
      )
      coll.team.insert(team) >>
        MemberRepo.add(team.id, me.id, Campus.defaultId(team.id), Member.Role.Owner) >>
        CampusRepo.add(team.id, team.createdBy, CampusData(Campus.defaultName, 1, none, none, none)) >>- {
          cached invalidateTeamIds me.id
          indexer ! InsertTeam(team)
          timeline ! Propagate(
            TeamCreate(me.id, team.id)
          ).toFollowersOf(me.id)
          bus.publish(CreateTeam(id = team.id, name = team.name, userId = me.id), 'team)
        } inject team
    }
  }

  def findNextId = {
    coll.team.find($empty, $id(true))
      .sort($sort desc "_id")
      .uno[Bdoc] map {
        _ flatMap { doc => doc.getAs[String]("_id") map (1 + _.toInt) } getOrElse 100000
      }
  }

  def basicSetting(team: Team, data: BasicTeamSetting, me: User): Funit =
    TeamRepo.basicSetting(team.id, data) >> {
      !team.isCreator(me.id) ?? {
        modLog.teamEdit(me.id, team.createdBy, team.name)
      }
    }

  def ratingSetting(team: Team, data: RatingTeamSetting, me: User): Funit =
    TeamRepo.ratingSetting(team.id, data) >> data.ratingSetting.open.?? {
      MemberRepo.initMembersRating(team.id, data.ratingSetting.defaultRating)
    } >> {
      !team.isCreator(me.id) ?? {
        modLog.teamEdit(me.id, team.createdBy, team.name)
      }
    }

  def coinSetting(team: Team, data: CoinTeamSetting, me: User): Funit =
    TeamRepo.coinSetting(team.id, data) >> data.coinSetting.open.?? {
      MemberRepo.initMembersCoin(team.id, CoinSetting.defaultCoin)
    } >> {
      !team.isCreator(me.id) ?? {
        modLog.teamEdit(me.id, team.createdBy, team.name)
      }
    }

  def update(team: Team, edit: TeamEdit, me: User): Funit = edit.trim |> { e =>
    team.copy(
      name = if (team.certified) team.name else edit.name,
      province = e.province,
      city = e.city,
      description = e.description,
      logo = e.logo,
      envPicture = e.envPicture
    ) |> { team =>
      coll.team.update($id(team.id), team).void >>
        !team.isCreator(me.id) ?? {
          modLog.teamEdit(me.id, team.createdBy, team.name)
        } >>-
        (indexer ! InsertTeam(team))
    }
  }

  def mine(me: User): Fu[List[TeamWithMember]] = {
    for {
      teamIds <- MemberRepo.teamIdsByUser(me.id)
      teams <- coll.team.byIds[Team](teamIds)
      members <- MemberRepo.memberFromSecondary(teams.map(_.id), me.id)
    } yield (teams zip members) map {
      case (team, member) => TeamWithMember(team, member)
    }
  }

  def teamWithMemberRank(me: User): Fu[Option[TeamWithMemberRank]] = {
    for {
      teamOption <- me.belongTeamId.??(TeamRepo.byId)
      memberOption <- teamOption.??(t => MemberRepo.byId(t.id, me.id))
      rank <- memberOption.??(m => MemberRepo.ratingRank(m))
    } yield (teamOption |@| memberOption).tupled map {
      case (team, member) => {
        TeamWithMemberRank(team, member, rank)
      }
    }
  }

  def mineTeamOwner(user: Option[User]): Fu[Set[String]] = user.?? { u =>
    for {
      teamIds <- MemberRepo.teamIdsByUser(u.id)
      teamsOwners <- MemberRepo.teamOwners(teamIds)
    } yield teamsOwners
  }

  def mineCertifyTeam(user: Option[User]): Fu[List[Team]] = user.?? { u =>
    for {
      teamIds <- MemberRepo.teamIdsByMember(u.id)
      teams <- TeamRepo.byOrderedIds(teamIds.toSeq)
    } yield teams.filter(_.certified)
  }

  def mineMembers(user: Option[User]): Fu[Set[String]] = user.?? { u =>
    for {
      teamIds <- MemberRepo.ownerTeamIdsByUser(u.id)
      memberIds <- MemberRepo.teamMembers(teamIds)
    } yield memberIds
  }

  def hasTeams(me: User): Fu[Boolean] = cached.teamIds(me.id).map(_.value.nonEmpty)

  def hasCreatedRecently(me: User): Fu[Boolean] =
    TeamRepo.userHasCreatedSince(me.id, creationPeriod)

  def requestsWithUsers(team: Team): Fu[List[RequestWithUser]] = for {
    requests ← RequestRepo findByTeam team.id
    campuses <- CampusRepo.byOrderedIds(requests.map(_.campus).filter(_.nonEmpty).map(_.get))
    users ← UserRepo usersFromSecondary requests.map(_.user)
    tags <- TagRepo.findByTeam(team.id)
  } yield requests zip users map {
    case (request, user) => {
      val campus = request.campus.fold(none[Campus]) { c => campuses.find(_.id == c) }
      RequestWithUser(request, user, team, campus, tags)
    }
  }

  def requestsWithUsers(user: User): Fu[List[RequestWithUser]] = for {
    ownerRequests <- user.teamId.?? { t => RequestRepo.findByTeam(t) }
    adminRequests <- user.headCampusAdmin.?? { t => RequestRepo.findByCampus(t) }
    campuses <- user.belongTeamId.?? { teamId => CampusRepo.byTeam(teamId) }
    requests = (ownerRequests.filter(r => r.campus.isEmpty || !r.campus.exists(cid => campuses.exists(c => c.id == cid && c.hasAdmin))) ++ adminRequests).distinct.sortBy(_.date)
    users <- UserRepo usersFromSecondary requests.map(_.user)
    teams <- teamFromSecondary(requests.map(_.team).distinct)
    tagMap <- TagRepo.findByTeams(requests.map(_.team).distinct)
  } yield requests zip users map {
    case (request, user) => {
      val team = teams.find(t => t.id == request.team) err s"can not find team ${request.team}"
      val campus = request.campus.fold(none[Campus]) { c => campuses.find(_.id == c) }
      val tags = tagMap.get(request.team) | List.empty
      RequestWithUser(request, user, team, campus, tags)
    }
  }

  def requestOrBelong(userId: String): Fu[Boolean] = requestOrBelongTeams(userId) map {
    case (joinedTeam, requestTeam) => joinedTeam.isDefined || requestTeam.isDefined
  }

  def requestOrBelongTeams(userId: String) =
    for {
      joinedIds <- MemberRepo.teamIdsByUser(userId)
      requestIds <- RequestRepo.teamIdsByUser(userId)
      joinedTeams <- joinedIds.nonEmpty.?? { TeamRepo.byOrderedIds(joinedIds.toSeq) }
      requestTeams <- requestIds.nonEmpty.?? { TeamRepo.byOrderedIds(requestIds.toSeq) }
    } yield joinedTeams.find(!_.disabled) -> requestTeams.find(!_.disabled)

  def batchJoin(team: Team, members: List[User]): Funit = {
    val rt = if (team.ratingSettingOrDefault.open) team.ratingSettingOrDefault.defaultRating.some else none
    val mems = members.map { user =>
      Member.make(team = team.id, user = user.id, campus = Campus.defaultId(team.id), role = Member.Role.Trainee, tags = None, rating = rt, clazzIds = None)
    }
    for {
      _ <- MemberRepo.batchAdd(mems)
      _ <- TeamRepo.incMembers(team.id, mems.size)
      _ <- CampusRepo.incMembers(Campus.defaultId(team.id), mems.size)
    } yield ()
  }

  def doJoin(
    team: Team,
    user: User,
    campus: String,
    role: Member.Role = Member.Role.Trainee,
    tags: Option[MemberTags] = None,
    mark: Option[String] = None,
    rating: Option[Int] = None,
    clazzIds: List[String],
    me: User
  ): Funit =
    !belongsTo(team.id, user.id) flatMap {
      _ ?? {
        val mk = if (team.tagTip) mark else mark.fold(user.profileOrDefault.realName) { m => m.some }
        val rt = if (team.tagTip) rating else if (team.ratingSettingOrDefault.open) team.ratingSettingOrDefault.defaultRating.some else none
        MemberRepo.add(team.id, user.id, campus, role, tags, rt, clazzIds.some) >>
          TeamRepo.incMembers(team.id, +1) >> CampusRepo.incMembers(campus, +1) >>- {
            cached invalidateTeamIds user.id
            timeline ! Propagate(TeamJoin(user.id, team.id)).toFollowersOf(user.id)
            markActor ! SetMark(me.id, user.id, mk)
            bus.publish(JoinTeam(id = team.id, userId = user.id), 'team)
          }
      } recover lila.db.recoverDuplicateKey(_ => ())
    }

  def join(teamId: Team.ID, me: User, clazzIds: List[String]): Fu[Option[Requesting]] =
    coll.team.byId[Team](teamId) flatMap {
      _ ?? { team =>
        if (team.open) {
          requestOrBelong(me.id) flatMap { rob =>
            if (rob) fuccess(none)
            else doJoin(team, me, Campus.defaultId(team.id), clazzIds = clazzIds, me = me) inject Joined(team).some
          }
        } else fuccess(Motivate(team).some)
      }
    }

  def joinApi(teamId: Team.ID, me: User, oAuthAppOwner: User.ID, clazzIds: List[String]): Fu[Option[Requesting]] =
    coll.team.byId[Team](teamId) flatMap {
      _ ?? { team =>
        if (team.open || team.createdBy == oAuthAppOwner) doJoin(team, me, Campus.defaultId(team.id), clazzIds = clazzIds, me = me) inject Joined(team).some
        else fuccess(Motivate(team).some)
      }
    }

  def requestable(teamId: Team.ID, user: User): Fu[Option[Team]] = for {
    teamOption ← coll.team.byId[Team](teamId)
    able ← teamOption.??(requestable(_, user))
  } yield teamOption filter (_ => able)

  def requestable(team: Team, user: User): Fu[Boolean] = for {
    belongs <- belongsTo(team.id, user.id)
    requested <- RequestRepo.exists(team.id, user.id)
    invited <- InviteRepo.exists(team.id, user.id)
  } yield !belongs && !requested && !invited && team.enabled

  def createRequest(team: Team, setup: RequestSetup, user: User): Funit =
    requestable(team, user) flatMap {
      _ ?? {
        val request = Request.make(
          team = team.id,
          user = user.id,
          message = setup.message,
          campus = setup.campus,
          tags = setup.fields.map { fields =>
            lila.team.MemberTags.byTagList(fields)
          }
        )
        coll.request.insert(request).void >>- (cached.nbRequests invalidate team.createdBy) >>- setup.campus.?? { campusId =>
          CampusRepo.byId(campusId) map {
            case None =>
            case Some(campus) => campus.admin.?? { admin => cached.nbRequests invalidate admin }
          }
        }
      }
    }

  def acceptRequest(
    team: Team,
    request: Request,
    tags: MemberTags,
    mark: Option[String],
    campus: String,
    rating: Option[Int] = None,
    clazzIds: List[String],
    me: User
  ): Funit = for {
    _ ← coll.request.remove(request)
    _ = cached.nbRequests invalidate team.createdBy
    _ <- request.campus.?? { campusId =>
      CampusRepo.byId(campusId) map {
        case None =>
        case Some(campus2) => campus2.admin.?? { admin => cached.nbRequests invalidate admin }
      }
    }
    userOption ← UserRepo byId request.user
    _ ← userOption.??(user =>
      doJoin(
        team,
        user,
        campus,
        tags = tags.some,
        mark = mark,
        rating = rating,
        clazzIds = clazzIds,
        me = me
      ) >>- notifier.acceptRequest(team, request))
  } yield ()

  def processRequest(team: Team, request: Request, accept: Boolean, clazzIds: List[String], me: User): Funit = for {
    _ ← coll.request.remove(request)
    _ = cached.nbRequests invalidate team.createdBy
    _ <- request.campus.?? { campusId =>
      CampusRepo.byId(campusId) map {
        case None =>
        case Some(campus) => campus.admin.?? { admin => cached.nbRequests invalidate admin }
      }
    }
    userOption ← UserRepo byId request.user
    _ ← userOption.filter(_ => accept).??(user =>
      doJoin(team, user, Campus.defaultId(team.id), clazzIds = clazzIds, me = me) >>- notifier.acceptRequest(team, request))
  } yield ()

  def deleteRequestsByUserId(userId: lila.user.User.ID) =
    RequestRepo.getByUserId(userId) flatMap {
      _.map { request =>
        RequestRepo.remove(request.id) >>
          TeamRepo.creatorOf(request.team).map { _ ?? cached.nbRequests.invalidate }
      }.sequenceFu
    }

  def invitesWithUsers(team: Team): Fu[List[InviteWithUser]] = for {
    invites ← InviteRepo findByTeam team.id
    users ← UserRepo usersFromSecondary invites.map(_.user)
  } yield invites zip users map {
    case (invite, user) => InviteWithUser(invite, user)
  }

  def createInvite(team: Team, user: User): Funit =
    coll.invite.insert(
      Invite.make(
        team = team.id,
        user = user.id,
        message = s"管理员邀请您加入 « ${team.name} » 俱乐部"
      )
    ).void >>- notifier.inviteSend(team, user.id)

  def processInvite(team: Team, invite: Invite, accept: Boolean, clazzIds: List[String], me: User): Funit = for {
    _ ← coll.invite.remove(invite)
    userOption ← UserRepo byId invite.user
    _ ← userOption.filter(_ => accept).??(user => doJoin(team, user, Campus.defaultId(team.id), Member.Role.Trainee, clazzIds = clazzIds, me = me))
  } yield ()

  def quit(teamId: Team.ID, me: User): Fu[Option[Team]] =
    coll.team.byId[Team](teamId) flatMap {
      _ ?? { team =>
        doQuit(team, me.id) inject team.some
      }
    }

  def doQuit(team: Team, userId: User.ID): Funit =
    MemberRepo.byId(team.id, userId).flatMap {
      case None => funit
      case Some(m) => {
        MemberRepo.remove(team.id, userId) >>
          TeamRepo.incMembers(team.id, -1) >>
          CampusRepo.incMembers(m.campus, -1) >>
          UserRepo.unsetBelongTeam(userId) >>-
          (cached invalidateTeamIds userId)
      }
    }

  def quitAll(user: User): Funit =
    user.belongTeamId match {
      case None => funit
      case Some(teamId) => {
        team(teamId).flatMap {
          case None => funit
          case Some(team) => {
            doQuit(team, user.id)
          }
        }
      }
    }

  def kick(team: Team, userId: User.ID, me: User): Funit =
    doQuit(team, userId) >>
      !team.isCreator(me.id) ?? {
        modLog.teamKick(me.id, userId, team.name)
      }

  def changeOwner(team: Team, userId: User.ID, me: User): Funit =
    MemberRepo.exists(team.id, userId) flatMap { e =>
      e ?? {
        for {
          _ <- TeamRepo.changeOwner(team.id, userId)
          _ <- MemberRepo.addRole(team.id, userId, Member.Role.Owner)
          _ <- MemberRepo.removeRole(team.id, me.id, Member.Role.Owner)
          _ <- modLog.teamMadeOwner(me.id, userId, team.name)
        } yield {
          bus.publish(SetOwner(id = team.id, name = team.name, certified = team.certified, userId = userId), 'teamSetOwner)
          bus.publish(UnsetOwner(id = team.id, name = team.name, certified = team.certified, userId = me.id), 'teamUnsetOwner)
          notifier.madeOwner(team, userId)
          cached.invalidateManagers(team.id)
        }
      }
    }

  def addCoach(id: Team.ID, coach: User.ID): Funit =
    MemberRepo.addRole(id, coach, Member.Role.Coach) >> CampusRepo.addCoach(Campus.defaultId(id), coach)

  def enable(team: Team): Funit =
    UserRepo.byId(team.createdBy).flatMap { uo =>
      uo.?? { u =>
        if (u.teamId.isEmpty) {
          TeamRepo.enable(team).void >>-
            bus.publish(EnableTeam(id = team.id, name = team.name, userId = team.createdBy), 'teamEnable) >>-
            (indexer ! InsertTeam(team))
        } else fufail("成员已经拥有俱乐部")
      }
    }

  def disable(team: Team): Funit =
    TeamRepo.disable(team).void >> MemberRepo.remove(team.id, team.createdBy) >>-
      bus.publish(DisableTeam(id = team.id, name = team.name, userId = team.createdBy), 'teamDisable) >>-
      (indexer ! RemoveTeam(team.id))

  // delete for ever, with members but not forums
  def delete(team: Team): Funit =
    coll.team.remove($id(team.id)) >>
      MemberRepo.removeByteam(team.id) >>-
      (indexer ! RemoveTeam(team.id))

  def syncBelongsTo(teamId: Team.ID, userId: User.ID): Boolean =
    cached.syncTeamIds(userId) contains teamId

  def belongsTo(teamId: Team.ID, userId: User.ID): Fu[Boolean] =
    cached.teamIds(userId) map (_ contains teamId)

  def owns(teamId: Team.ID, userId: User.ID): Fu[Boolean] =
    TeamRepo ownerOf teamId map (Some(userId) ==)

  def teamName(teamId: Team.ID): Option[String] = cached.name(teamId)

  def nbRequests(teamId: Team.ID) = cached.nbRequests get teamId

  def recomputeNbMembers =
    coll.team.find($empty).cursor[Team](ReadPreference.secondaryPreferred).foldWhileM({}) { (_, team) =>
      for {
        nb <- MemberRepo.countByTeam(team.id)
        _ <- coll.team.updateField($id(team.id), "nbMembers", nb)
      } yield Cursor.Cont({})
    }

  def uploadPicture(id: String, picture: Photographer.Uploaded, processFile: Boolean = false): Fu[DbImage] =
    photographer(id, picture, processFile)

  def addTag(team: Team, user: User, tag: TagAdd): Funit =
    TagRepo.create(
      Tag.make(
        team = team.id,
        label = tag.label,
        value = tag.value,
        required = tag.required,
        request = tag.request,
        visitable = tag.visitable,
        sort = tag.sort,
        typ = tag.realType,
        userId = user.id
      )
    ).void

  def updateTag(tagId: String, tag: TagEdit): Funit =
    TagRepo.update(tagId, tag)

  def removeTag(id: String, tag: Tag): Funit =
    TagRepo.remove(tag._id) >> MemberRepo.removeTag(id, tag.field).void

  def addCampus(team: Team, user: User, data: CampusData): Funit =
    CampusRepo.add(team.id, user.id, data) map { campus =>
      data.admin.?? { userId =>
        MemberRepo.addRole(team.id, userId, Member.Role.Admin) >>-
          bus.publish(SetCampusAdmin(id = team.id, campusId = campus.id, userId), 'teamSetCampusAdmin) >>-
          cached.invalidateManagers(team.id)
      }
    }

  def updateCampus(c: Campus, data: CampusData, isChangeAdmin: Boolean): Funit =
    CampusRepo.update(
      c.copy(
        name = data.name,
        sort = data.sort,
        admin = data.admin,
        addr = data.addr,
        intro = data.intro
      )
    ) >> isChangeAdmin.?? {
        c.admin.?? { userId => MemberRepo.removeRole(c.team, userId, Member.Role.Admin) >>- bus.publish(UnsetCampusAdmin(id = c.team, campusId = c.id, userId), 'teamUnsetCampusAdmin) } >>
          data.admin.?? { userId => MemberRepo.addRole(c.team, userId, Member.Role.Admin) >>- bus.publish(SetCampusAdmin(id = c.team, campusId = c.id, userId), 'teamSetCampusAdmin) } >>-
          cached.invalidateManagers(c.team)
      }

  def removeCampus(c: Campus): Funit =
    CampusRepo.remove(c.id) >> {
      c.admin.?? { userId =>
        MemberRepo.removeRole(c.team, userId, Member.Role.Admin) >>-
          bus.publish(UnsetCampusAdmin(id = c.team, campusId = c.id, userId), 'teamUnsetCampusAdmin) >>-
          cached.invalidateManagers(c.team)
      }
    }

  def campusWithTeams(campusIds: List[String]): Fu[List[CampusWithTeam]] = for {
    campuses ← CampusRepo.byIds(campusIds)
    teams ← TeamRepo.byOrderedIds(campuses.map(_.team))
  } yield campuses zip teams map {
    case (campus, team) => CampusWithTeam(campus, team)
  }

  def campusWithTeam(campusId: String): Fu[Option[CampusWithTeam]] = for {
    campusOption ← CampusRepo.byId(campusId)
    teamOption ← campusOption.?? { c => TeamRepo.byId(c.team) }
  } yield (campusOption |@| teamOption).tupled map {
    case (campus, team) => CampusWithTeam(campus, team)
  }

  def addClazz(campusId: String, clazzId: String): Funit =
    TeamRepo.addClazz(Campus.toTeamId(campusId), clazzId) >> CampusRepo.addClazz(campusId, clazzId)

  def removeClazz(campusId: String, clazzId: String): Funit =
    TeamRepo.removeClazz(Campus.toTeamId(campusId), clazzId) >> CampusRepo.removeClazz(campusId, clazzId)

  def addMemberClazz(userId: ID, campusId: String, clazzId: String): Funit =
    MemberRepo.addClazz(Campus.toTeamId(campusId), userId, clazzId)

  def addMemberClazzs(userIds: List[String], campusId: String, clazzId: String): Funit = {
    val memberIds = userIds.map(Member.makeId(Campus.toTeamId(campusId), _))
    MemberRepo.addClazzs(memberIds, clazzId)
  }

  def removeMemberClazz(userId: ID, campusId: String, clazzId: String): Funit =
    MemberRepo.removeClazz(Campus.toTeamId(campusId), userId, clazzId)

  def updateMember(team: Team, member: Member, edit: MemberEdit, me: User): Funit = {
    MemberRepo.updateMember(
      member.copy(
        campus = edit.campus,
        tags = lila.team.MemberTags.byTagList(edit.fields).some
      )
    ).void >> CampusRepo.incMembers(member.campus, -1) >>
      CampusRepo.incMembers(edit.campus, 1) >>- {
        markActor ! SetMark(me.id, member.user, edit.mark)
      }
  }

  def setMemberRating(team: Team, member: Member, k: Int, rating: Double, note: Option[String]): Funit = {
    MemberRepo.updateMember(
      member.copy(
        rating = {
          member.rating.fold(EloRating(rating, 0)) { r =>
            r.copy(
              rating = rating,
              k = if (team.ratingSettingOrDefault.k == k) none else k.some
            )
          }
        }.some
      )
    ) >> (!member.rating.??(_.rating == rating)).?? {
        TeamRatingRepo.insert(
          TeamRating.make(
            userId = member.user,
            rating = member.rating.map(_.rating) | 0,
            diff = diffRating(rating, member.rating.map(_.rating) | 0),
            note = note | "",
            typ = TeamRating.Typ.Setting,
            metaData = TeamRatingMetaData()
          )
        )
      }
  }

  def updateOnlineRating(game: Game, whiteOption: Option[User], blackOption: Option[User]): Funit = {
    (whiteOption |@| blackOption).tupled ?? {
      case (white, black) => {
        (for {
          whiteTeamIds <- MemberRepo.teamIdsByUser(white.id)
          blackTeamIds <- MemberRepo.teamIdsByUser(black.id)
        } yield (whiteTeamIds, blackTeamIds)).flatMap {
          case (wtis, btis) => {
            teamFromSecondary((wtis & btis).toSeq) flatMap { teams =>
              teams.map { team =>
                team.ratingSetting.?? { setting =>
                  (for {
                    whiteMemberOption <- MemberRepo.byId(team.id, white.id)
                    blackMemberOption <- MemberRepo.byId(team.id, black.id)
                  } yield (whiteMemberOption |@| blackMemberOption).tupled) flatMap {
                    _.?? {
                      case (whiteMember, blackMember) => {
                        val whiteRating = whiteMember.rating | EloRating(setting.defaultRating, 0)
                        val blackRating = blackMember.rating | EloRating(setting.defaultRating, 0)
                        val newWhiteRating = whiteRating.calc(blackRating.rating, game.whitePlayer.isWinner, setting.k, setting.minRating)
                        val newBlackRating = blackRating.calc(whiteRating.rating, game.blackPlayer.isWinner, setting.k, setting.minRating)

                        userMarks(team.createdBy) flatMap { markMap =>
                          if (game.isContest) {
                            contestBoard(game.id, game.contestIsTeamOrFalse) flatMap {
                              _.?? { board =>
                                board.teamRated.?? {
                                  setContestRating(game, whiteMember, board, whiteRating, newWhiteRating, realName(markMap, white, whiteMember), realName(markMap, black, blackMember)) >>
                                    setContestRating(game, blackMember, board, blackRating, newBlackRating, realName(markMap, white, whiteMember), realName(markMap, black, blackMember))
                                }
                              }
                            }
                          } else {
                            val clockReach = game.clock.?? { c => (c.limitSeconds + c.incrementSeconds * 40) / 60 >= math.max(setting.minutes, 2) }
                            (setting.open && game.turns > setting.turns && clockReach).?? {
                              game.rated.?? {
                                setGameRating(game, whiteMember, whiteRating, newWhiteRating, realName(markMap, white, whiteMember), realName(markMap, black, blackMember)) >>
                                  setGameRating(game, blackMember, blackRating, newBlackRating, realName(markMap, white, whiteMember), realName(markMap, black, blackMember))
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }.sequenceFu.void
            }
          }
        }
      }
    }
  }

  private def contestBoard(gameId: String, isTeam: Boolean) = {
    if (isTeam) {
      (teamContestActor ? GetContestBoard(gameId)).mapTo[Option[ContestBoard]]
    } else {
      (contestActor ? GetContestBoard(gameId)).mapTo[Option[ContestBoard]]
    }
  }

  def updateOfflineRating(result: OffContestRoundResult): Funit = {
    result.teamId.?? { teamId =>
      TeamRepo.byId(teamId) flatMap {
        _.?? { team =>
          team.ratingSetting.?? { setting =>
            setting.open.?? {
              val userIds = result.boards.flatten { board =>
                Map(board.white.userId -> board.white.external, board.black.userId -> board.black.external).filterNot(_._2).keySet
              }

              MemberRepo.memberFromSecondary(teamId, userIds) flatMap { members =>
                result.boards.map { board =>
                  (!board.isAbsent).?? {
                    def whiteMemberAndExternalOption = findMember(team, board.white, members)
                    def blackMemberAndExternalOption = findMember(team, board.black, members)
                    (whiteMemberAndExternalOption |@| blackMemberAndExternalOption).tupled ?? {
                      case (whiteMemberAndExternal, blackMemberAndExternal) => {
                        // 外部棋手等级分为0时，他的对手不计算等级分
                        (!((whiteMemberAndExternal._2 && whiteMemberAndExternal._1.intRating.contains(0)) || (blackMemberAndExternal._2 && blackMemberAndExternal._1.intRating.contains(0)))) ?? {
                          val whiteMember = whiteMemberAndExternal._1
                          val blackMember = blackMemberAndExternal._1
                          val whiteRating = whiteMember.rating | EloRating(setting.defaultRating, 0)
                          val blackRating = blackMember.rating | EloRating(setting.defaultRating, 0)
                          val newWhiteRating = whiteRating.calc(blackRating.rating, board.white.isWinner, setting.k, setting.minRating)
                          val newBlackRating = blackRating.calc(whiteRating.rating, board.black.isWinner, setting.k, setting.minRating)

                          val r = {
                            if (board.white.isWinner | false) {
                              "1-0"
                            } else if (board.black.isWinner | false) {
                              "0-1"
                            } else "1/2-1/2 "
                          }

                          val note = s"${result.contestFullName} 第${result.roundNo}轮  ${board.white.realName} $r ${board.black.realName}"
                          setOffContestRating(result, board, board.white, whiteMember, whiteRating, newWhiteRating, note) >>
                            setOffContestRating(result, board, board.black, blackMember, blackRating, newBlackRating, note)
                        }
                      }
                    }
                  }
                }.sequenceFu.void
              }
            }
          }
        }
      }
    }
  }

  private def findMember(team: Team, u: OffContestUser, members: List[Member]): Option[(Member, Boolean)] = {
    if (u.external) {
      (Member.make(team.id, u.userId, Campus.defaultId(team.id), rating = u.teamRating) -> true).some
    } else {
      members.find(_.user == u.userId).map(_ -> false)
    }
  }

  private def setOffContestRating(
    result: OffContestRoundResult,
    board: OffContestBoard,
    u: OffContestUser,
    member: Member,
    oldRating: EloRating,
    newRating: EloRating,
    note: String
  ): Funit = if (u.external) funit else {
    setRating(member, oldRating, newRating, note, TeamRating.Typ.OffContest,
      TeamRatingMetaData(
        contestId = result.contestId.some,
        roundNo = result.roundNo.some,
        boardId = board.id.some
      ))
  }

  private def setGameRating(
    game: Game,
    member: Member,
    oldRating: EloRating,
    newRating: EloRating,
    whiteUserRealName: String,
    blackUserRealName: String
  ): Funit = {
    setRating(
      member,
      oldRating,
      newRating,
      gameResult(game, whiteUserRealName, blackUserRealName),
      TeamRating.Typ.Game,
      TeamRatingMetaData(gameId = game.id.some)
    )
  }

  private def setContestRating(
    game: Game,
    member: Member,
    board: ContestBoard,
    oldRating: EloRating,
    newRating: EloRating,
    whiteUserRealName: String,
    blackUserRealName: String
  ): Funit = {
    val note = s"${board.contestFullName} 第${board.board.roundNo}轮  ${gameResult(game, whiteUserRealName, blackUserRealName)}"
    setRating(
      member,
      oldRating,
      newRating,
      note,
      if (game.contestIsTeamOrFalse) TeamRating.Typ.TeamContest else TeamRating.Typ.Contest,
      TeamRatingMetaData(
        contestId = board.contestId.some,
        roundNo = board.board.roundNo.some,
        boardId = game.id.some,
        isTeam = game.metadata.contestIsTeam
      )
    )
  }

  private def setRating(
    member: Member,
    oldRating: EloRating,
    newRating: EloRating,
    note: String,
    typ: TeamRating.Typ,
    metaData: TeamRatingMetaData
  ): Funit = {
    MemberRepo.updateMember(member.copy(rating = newRating.some)) >> TeamRatingRepo.insert(
      TeamRating.make(
        userId = member.user,
        rating = oldRating.rating,
        diff = diffRating(newRating.rating, oldRating.rating),
        note = note,
        typ = typ,
        metaData = metaData
      )
    )
  }

  private def diffRating(newRating: Double, oldRating: Double) = (BigDecimal(newRating) - BigDecimal(oldRating)).setScale(1, RoundingMode.DOWN).doubleValue()

  private def gameResult(game: Game, whiteUserRealName: String, blackUserRealName: String): String = {
    val result = {
      game.winnerColor match {
        case None => "1/2-1/2"
        case Some(c) => c.fold("1-0", "0-1")
      }
    }
    s"$whiteUserRealName $result $blackUserRealName"
  }

  private def realName(markMap: Map[String, Option[String]], user: User, member: Member): String = {
    val mark = markMap.get(user.id) match {
      case None => none[String]
      case Some(m) => m
    }
    mark | user.realNameOrUsername
  }

  def setMemberCoin(team: Team, member: Member, operate: TeamCoin.Operate, diff: Int, note: Option[String], operator: User): Funit = {
    val diffCoin = diff * operate.cst
    val oldCoin = member.coinOrZero
    val newCoin = oldCoin + diffCoin
    MemberRepo.incMemberCoin(member.id, diffCoin) >> {
      TeamCoinRepo.insert(
        TeamCoin.make(
          userId = member.user,
          teamId = member.team,
          campusId = member.campus,
          oldCoin = oldCoin,
          newCoin = newCoin,
          diffCoin = diffCoin,
          note = note | "",
          typ = TeamCoin.Typ.Setting,
          metaData = TeamCoinMetaData(),
          createBy = operator.id
        )
      ) >>- bus.publish(TeamCoinChangeFromSetting(member.team, member.user, diffCoin, team.coinSettingOrDefault.name, note | ""), 'teamCoinChange)
    }
  }

  def updateTTaskCoin(ttaskStatus: ChangeStatus): Funit = {
    ttaskStatus.sourceRel.source match {
      case "trainCourse" | "throughTrain" | "teamClockIn" => {
        val userId = ttaskStatus.userId
        MemberRepo.membersByUser(userId).flatMap { members =>
          teamFromSecondary(members.map(_.team)) flatMap { teams =>
            teams.map { team =>
              team.coinSettingOrDefault.open.?? {
                val member = members.find(_.team == team.id) err s"can not find member ${team.id}@$userId"
                ttaskStatus.coinRule.?? { coinRule =>
                  MemberRepo.incMemberCoin(member.id, coinRule) >> {
                    TeamCoinRepo.insert(
                      TeamCoin.make(
                        userId = member.user,
                        teamId = member.team,
                        campusId = member.campus,
                        oldCoin = member.coinOrZero,
                        newCoin = member.coinOrZero + coinRule,
                        diffCoin = coinRule,
                        note = s"${ttaskStatus.sourceRel.name} ${ttaskStatus.taskName}",
                        typ = TeamCoin.Typ.TTask,
                        metaData = TeamCoinMetaData(taskId = ttaskStatus.taskId.some),
                        createBy = ttaskStatus.creator
                      )
                    ) >>- bus.publish(TeamCoinChangeFromTTask(member.team, member.user, ttaskStatus.taskId, coinRule, team.coinSettingOrDefault.name, s"${ttaskStatus.sourceRel.name} ${ttaskStatus.taskName}"), 'teamCoinChange)
                  }
                }
              }
            }.sequenceFu.void
          }
        }
      }
      case _ => funit
    }
  }

  def updateHomeworkCoin(userId: String, creator: String, homeworkId: String, homeworkName: String, coinDiff: Option[Int], oldCoinDiff: Option[Int]) = {

    def setNewHomeworkCoin(cd: Int) =
      (cd > 0).?? {
        MemberRepo.membersByUser(userId).flatMap { members =>
          teamFromSecondary(members.map(_.team)) flatMap { teams =>
            teams.map { team =>
              team.coinSettingOrDefault.open.?? {
                val member = members.find(_.team == team.id) err s"can not find member ${team.id}@$userId"
                MemberRepo.incMemberCoin(member.id, cd) >> {
                  TeamCoinRepo.insert(
                    TeamCoin.make(
                      userId = member.user,
                      teamId = member.team,
                      campusId = member.campus,
                      oldCoin = member.coinOrZero,
                      newCoin = member.coinOrZero + cd,
                      diffCoin = cd,
                      note = homeworkName,
                      typ = TeamCoin.Typ.Homework,
                      metaData = TeamCoinMetaData(homeworkId = homeworkId.some),
                      createBy = creator
                    )
                  ) >>- bus.publish(TeamCoinChangeFromHomework(member.team, member.user, homeworkId, cd, team.coinSettingOrDefault.name, homeworkName), 'teamCoinChange)
                }
              }
            }.sequenceFu.void
          }
        }
      }

    def setOldHomeworkCoin(ocd: Int, cd: Int) =
      (cd != ocd).?? {
        MemberRepo.membersByUser(userId).flatMap { members =>
          teamFromSecondary(members.map(_.team)) flatMap { teams =>
            teams.map { team =>
              team.coinSettingOrDefault.open.?? {
                val member = members.find(_.team == team.id) err s"can not find member ${team.id}@$userId"
                val diff = ocd * -1 + cd
                val newCoin = member.coinOrZero + diff
                (newCoin > 0).?? {
                  MemberRepo.incMemberCoin(member.id, diff) >> {
                    TeamCoinRepo.insert(
                      TeamCoin.make(
                        userId = member.user,
                        teamId = member.team,
                        campusId = member.campus,
                        oldCoin = member.coinOrZero,
                        newCoin = newCoin,
                        diffCoin = diff,
                        note = homeworkName + s"积分变动(+$ocd -> +$cd)",
                        typ = TeamCoin.Typ.Homework,
                        metaData = TeamCoinMetaData(homeworkId = homeworkId.some),
                        createBy = creator
                      )
                    ) >>- bus.publish(TeamCoinChangeFromHomework(member.team, member.user, homeworkId, cd, team.coinSettingOrDefault.name, homeworkName), 'teamCoinChange)
                  }
                }
              }
            }.sequenceFu.void
          }
        }
      }

    oldCoinDiff match {
      case Some(ocd) => {
        coinDiff.??(setOldHomeworkCoin(ocd, _))
      }
      case None => coinDiff.??(setNewHomeworkCoin)
    }
  }

  def findMemberByText(teamId: String, userId: String, q: Option[String]) = {
    if (q.isEmpty || q.get.trim.length <= 0) fuccess(List.empty[MemberWithMark])
    else {
      userMarks(userId).flatMap { markMap =>
        val userIds = markMap.filterValues { markOption =>
          markOption.?? { mark =>
            mark.toLowerCase.contains(q.get.toLowerCase)
          }
        }.keySet

        MemberRepo.findByText(teamId, userIds.toList, q.get) map { members =>
          members.map { mwu =>
            val mark = markMap.get(mwu.member.user) | none
            MemberWithMark(mwu.member, mwu.user, mark)
          }
        }
      }
    }
  }

  def isManagerOfUser(manager: User.ID, member: User.ID) = {
    cached.teamIdsList(member) flatMap { teamIds =>
      teamIds.headOption ?? { teamId =>
        MemberRepo.membersByUser(member).flatMap { members =>
          val campusIds = members.map(_.campus)
          cached.managers.get(teamId) map { managers =>
            managers.exists {
              case (managerId, teamOrCampusId) =>
                (manager == managerId) && (teamId == teamOrCampusId || campusIds.contains(teamOrCampusId))
            }
          }
        }
      }
    }
  }

  def isManagerOfCampus(manager: User.ID, campusId: Campus.ID) =
    cached.managers.get(Campus.toTeamId(campusId)) map { managers =>
      managers.exists {
        case (managerId, teamOrCampusId) =>
          manager == managerId && (campusId == teamOrCampusId || Campus.toTeamId(campusId) == teamOrCampusId)
      }
    }

  def isMyTeamMember(me: Option[User], userId: User.ID): Fu[Boolean] = me match {
    case None => fuFalse
    case Some(m) => {
      m.teamId match {
        case Some(teamId) => MemberRepo.exists(teamId, userId)
        case None => m.teamCampusIds match {
          case Some(campusIds) => MemberRepo.existsByCampus(campusIds.toList, userId)
          case None => fuFalse
        }
      }
    }
  }

  def loadTeamRating(me: User, user: User): Fu[Option[Int]] =
    me.belongTeamId == user.belongTeamId match {
      case false => fuccess(none[Int])
      case true => me.belongTeamId match {
        case None => fuccess(none[Int])
        case Some(teamId) => {
          TeamRepo.byId(teamId) flatMap {
            case None => fuccess(none[Int])
            case Some(t) => t.ratingSettingOrDefault.open match {
              case false => fuccess(none[Int])
              case true => {
                MemberRepo.byId(teamId, user.id).map {
                  _.?? {
                    _.intRating
                  }
                }
              }
            }
          }
        }
      }
    }

  def onLineMembers(teamId: String): Fu[List[MemberWithUser]] = MemberRepo.teamMembers(Set(teamId)) flatMap { userIds =>
    val top20 = userIds.filter(isOnline).map { userId =>
      PinyinHelper.convertToPinyinString(userId.toLowerCase, "", PinyinFormat.WITHOUT_TONE)
    }.toList.sorted.take(20)
    MemberRepo.allMembersWithUser(teamId, top20).map {
      _.map { mwu =>
        mwu.copy(isPlaying = isPlaying(mwu.userId))
      }
    }
  }

  def findTeamCooperator(teamId: String): Fu[Option[User.ID]] = {
    TeamCooperateRepo.findByTeam(teamId).flatMap {
      case None => fuccess(none)
      case Some(c) => {
        TeamCooperateRepo.findByUserWithTeam(c.userId).map { cooperates =>
          val enabledTeamIds = TeamCooperates(cooperates).enabledTeams()
          if (enabledTeamIds.contains(teamId)) {
            c.userId.some
          } else none
        }
      }
    }
  }

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  def userMark(userId: User.ID, relUserId: String): Fu[Option[String]] =
    (markActor ? GetMark(userId, relUserId)).mapTo[Option[String]]

}
