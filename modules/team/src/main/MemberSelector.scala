package lila.team

import lila.db.dsl._
import org.joda.time.DateTime
import lila.user.{ User, UserRepo }
import lila.common.MaxPerPage
import lila.common.paginator.{ AdapterLike, Paginator }
import lila.team.DataForm.memberData.MemberSearch
import lila.team.DataForm.memberAccountData.MemberAccountSearch
import com.github.stuxuhai.jpinyin.{ PinyinFormat, PinyinHelper }
import lila.db.paginator.Adapter
import scala.concurrent.duration._

class MemberSelector(colls: Colls) {

  import BSONHandlers._

  def teamMemberAccount(
    team: Team,
    page: Int,
    search: MemberAccountSearch,
    markMap: Map[String, Option[String]]
  ): Fu[Paginator[MemberOptionWithUser]] = {

    var $selector = $doc("imported.teamId" -> team.id)

    search.enabled.foreach { e =>
      $selector = $selector ++ $doc("enabled" -> e)
    }

    search.level.foreach { l =>
      $selector = $selector ++ $doc("member.code" -> l)
    }

    search.username.foreach { u =>
      val markUserIds = markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(u.toLowerCase)
        }
      }.keySet
      $selector = $selector ++ $or($doc("username" $regex (u, "i")), $doc("_id" $in markUserIds))
    }

    //println(bsonDocumentToPretty($selector))
    Paginator(
      adapter = new Adapter[User](
        collection = lila.user.Env.current.userColl,
        selector = $selector,
        projection = $empty,
        sort = $sort desc "createdAt"
      ).mapFutureList { users =>
        MemberRepo.memberOptionFromSecondary(team.id, users.map(_.id)) map { members =>
          users zip members collect {
            case (user, memberOption) => {
              val markOrName = markMap.get(user.id).fold(none[String]) { m => m } | user.realNameOrUsername
              MemberOptionWithUser(memberOption, user, markOrName)
            }
          }
        }
      },
      page,
      MaxPerPage(15)
    )
  }

  def teamMembers(
    team: Team,
    page: Int,
    search: MemberSearch,
    tags: List[Tag],
    markMap: Map[String, Option[String]],
    excludes: Option[List[User.ID]] = None
  ): Fu[Paginator[MemberWithUser]] =
    Paginator(
      adapter = new TeamAdapter(team, search, tags, markMap, excludes),
      page,
      MaxPerPage(15)
    )

  class TeamAdapter(
      team: Team,
      search: MemberSearch,
      tags: List[Tag],
      markMap: Map[String, Option[String]],
      excludes: Option[List[User.ID]] = None
  ) extends AdapterLike[MemberWithUser] {

    val nbResults = fuccess(team.nbMembers)

    def slice(offset: Int, length: Int): Fu[Seq[MemberWithUser]] = {
      val memberSelector = buildSelector(team.id, search, tags, markMap, excludes)
      for {
        members <- colls.member.find(memberSelector).list[Member]()
        userSelector = buildUserSelector(search, members.map(_.user))
        users <- UserRepo.find(userSelector).sort($sort asc "_id").list[lila.user.User]()
      } yield {
        users.map { u =>
          val markOrName = markMap.get(u.id).fold(none[String]) { m => m } | u.realNameOrUsername
          MemberWithUser(members.find(m => m.user == u.id) err s"can not find member ${u.id}", u, markOrName)
        }.sortWith {
          case (m1, m2) => memberSort(m1, m2)
        }.slice(offset, offset + length)
      }
    }

    def memberSort(m1: MemberWithUser, m2: MemberWithUser): Boolean = {
      search.sortField match {
        case Some(sortField) => {
          sortField match {
            case "username" => {
              val v1 = PinyinHelper.convertToPinyinString(m1.user.username.toLowerCase, "_", PinyinFormat.WITHOUT_TONE)
              val v2 = PinyinHelper.convertToPinyinString(m2.user.username.toLowerCase, "_", PinyinFormat.WITHOUT_TONE)
              search.sortType match {
                case Some(sortType) => {
                  sortType match {
                    case "up" => v1 < v2
                    case "down" => v1 > v2
                    case _ => v1 < v2
                  }
                }
                case None => v1 < v2
              }
            }
            case "markname" => {
              val v1 = PinyinHelper.convertToPinyinString(m1.markOrName.toLowerCase, "_", PinyinFormat.WITHOUT_TONE)
              val v2 = PinyinHelper.convertToPinyinString(m2.markOrName.toLowerCase, "_", PinyinFormat.WITHOUT_TONE)
              search.sortType match {
                case Some(sortType) => {
                  sortType match {
                    case "up" => v1 < v2
                    case "down" => v1 > v2
                    case _ => v1 < v2
                  }
                }
                case None => v1 < v2
              }
            }
            case "rating" => {
              search.sortType match {
                case Some(sortType) => {
                  sortType match {
                    case "up" => m1.member.intRatingOrZero < m2.member.intRatingOrZero
                    case "down" => m1.member.intRatingOrZero > m2.member.intRatingOrZero
                    case _ => m1.member.intRatingOrZero < m2.member.intRatingOrZero
                  }
                }
                case None => m1.user.username < m2.user.username
              }
            }
            case _ => m1.member.role.minBy(_.sort).sort < m2.member.role.minBy(_.sort).sort
          }
        }
        case None => m1.member.role.minBy(_.sort).sort < m2.member.role.minBy(_.sort).sort
      }
    }

  }

  def findMember(
    teamId: String,
    search: MemberSearch,
    tags: List[Tag],
    markMap: Map[String, Option[String]],
    excludes: Option[List[User.ID]] = None
  ): Fu[List[MemberWithUser]] = {

    val memberSelector = buildSelector(teamId, search, tags, markMap, excludes)
    for {
      members <- colls.member.list[Member](memberSelector)
      userSelector = buildUserSelector(search, members.map(_.user))
      users <- UserRepo.find(userSelector).sort($sort asc "_id").list[User]()
    } yield {
      users.map { u =>
        MemberWithUser(members.find(m => m.user == u.id) err s"can not find member ${u.id}", u)
      }
    }
  }

  def buildSelector(
    teamId: String,
    search: MemberSearch,
    tags: List[Tag],
    markMap: Map[String, Option[String]],
    excludes: Option[List[User.ID]] = None
  ) = {
    var $selector = $doc("team" -> teamId)

    search.clazzId.foreach { c =>
      $selector = $selector ++ $doc("clazzIds" -> c)
    }

    search.role.foreach { r =>
      $selector = $selector ++ $doc("role" -> r)
    }

    search.campus.foreach { c =>
      $selector = $selector ++ $doc("campus" -> c)
    }

    if (search.teamRatingMin.isDefined || search.teamRatingMax.isDefined) {
      var teamRatingRange = $empty
      search.teamRatingMin foreach { teamRatingMin =>
        teamRatingRange = teamRatingRange ++ $gte(teamRatingMin)
      }
      search.teamRatingMax foreach { teamRatingMax =>
        teamRatingRange = teamRatingRange ++ $lte(teamRatingMax)
      }
      $selector = $selector ++ $doc("rating.r" -> teamRatingRange)
    }

    search.fields.foreach { f =>
      f.value.foreach(v =>
        if (tags.exists(t => t.field == f.field && t.typ == Tag.Type.Text)) {
          $selector = $selector ++ $doc(s"tags.${f.field}.value" $regex (v, "i"))
        } else {
          $selector = $selector ++ $doc(s"tags.${f.field}.value" -> v)
        })
    }

    search.rangeFields.foreach { rf =>
      {
        var range = $doc()
        rf.min.foreach(v =>
          range = range ++ $gte(v))
        rf.max.foreach(v =>
          range = range ++ $lte(v))
        if (!range.isEmpty) {
          $selector = $selector ++ $doc(s"tags.${rf.field}.value" -> range)
        }
      }
    }

    search.username.foreach { u =>
      val markUserIds = markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(u.toLowerCase)
        }
      }.keySet

      val realNameIds = UserRepo.findIds($doc(s"${lila.user.User.BSONFields.profile}.realName" $regex (u, "i"))) awaitSeconds (2)

      $selector = $selector ++ $or($doc("user" $regex (u, "i")), $doc("user" $in markUserIds ++ realNameIds))
    }

    excludes.foreach { excludes =>
      $selector = $selector ++ $doc("user" $nin excludes)
    }
    //println(reactivemongo.bson.BSONDocument.pretty($selector))
    $selector
  }

  def buildUserSelector(search: MemberSearch, userIds: List[User.ID]) = {
    var $selector = $doc("_id" $in userIds)

    search.age.foreach { a =>
      $selector = $selector ++ $doc("profile.birthyear" -> (DateTime.now.getYear - a))
    }

    search.sex.foreach { s =>
      $selector = $selector ++ $doc("profile.sex" -> s)
    }

    search.level.foreach { l =>
      $selector = $selector ++ $doc("profile.levels" -> $doc("level" -> l, "current" -> 1))
    }

    search.memberLevel.foreach { l =>
      $selector = $selector ++ $doc("member.code" -> l)
    }

    $selector
  }

}
