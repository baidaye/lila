package lila.team

import org.joda.time.{ DateTime, Period }
import lila.team.Certification.Status
import reactivemongo.api._
import reactivemongo.bson._
import lila.db.dsl._
import lila.user.User
import lila.team.DataForm.teamData._

object TeamRepo {

  // dirty
  private val coll = Env.current.colls.team

  import BSONHandlers._

  def byId(id: Team.ID) = coll.byId[Team](id)

  def byOrderedIds(ids: Seq[Team.ID]) = coll.byOrderedIds[Team, Team.ID](ids)(_.id)

  def byCampusId(campusId: String) = coll.byId[Team](Campus.toTeamId(campusId))

  def byCampusIds(campusIds: List[String]) = coll.byOrderedIds[Team, Team.ID](campusIds.map(Campus.toTeamId))(_.id)

  def cursor(
    selector: Bdoc,
    readPreference: ReadPreference = ReadPreference.secondaryPreferred
  )(
    implicit
    cp: CursorProducer[Team]
  ) =
    coll.find(selector).cursor[Team](readPreference)

  def managered(id: Team.ID, campusId: Option[String], me: User): Fu[Option[Team]] =
    coll.uno[Team]($id(id)).filter {
      _.?? { t =>
        t.isCreator(me.id) || campusId.?? { c => Campus.toTeamId(c) == id && me.isCampusAdmin(c) }
      }
    }

  def creatorOf(teamId: Team.ID): Fu[Option[User.ID]] =
    coll.primitiveOne[User.ID]($id(teamId), "_id")

  def name(id: String): Fu[Option[String]] =
    coll.primitiveOne[String]($id(id), "name")

  def userHasCreatedSince(userId: String, duration: Period): Fu[Boolean] =
    coll.exists($doc(
      "createdAt" $gt DateTime.now.minus(duration),
      "createdBy" -> userId
    ))

  def ownerOf(teamId: String): Fu[Option[String]] =
    coll.primitiveOne[String]($id(teamId), "createdBy")

  def ownersOf(teamIds: List[Team.ID]): Fu[List[String]] =
    coll.primitive[String]($inIds(teamIds), "createdBy")

  def incMembers(teamId: String, by: Int): Funit =
    coll.update($id(teamId), $inc("nbMembers" -> by)).void

  def enable(team: Team) =
    coll.updateField($id(team.id), "enabled", true)

  def disable(team: Team) =
    coll.updateField($id(team.id), "enabled", false)

  def changeOwner(teamId: String, newOwner: User.ID) =
    coll.updateField($id(teamId), "createdBy", newOwner)

  def teamExists(setup: TeamSetup) =
    coll.exists($doc("name" -> setup.trim.name))

  def addCertification(teamId: String, certification: Certification): Funit =
    coll.update(
      $id(teamId),
      $set("certification" -> certification)
    ).void

  def addClazz(teamId: String, clazzId: String): Funit =
    coll.update(
      $id(teamId),
      $addToSet("clazzIds" -> clazzId)
    ).void

  def removeClazz(teamId: String, clazzId: String): Funit =
    coll.update(
      $id(teamId),
      $pull("clazzIds" -> clazzId)
    ).void

  def basicSetting(teamId: String, data: BasicTeamSetting): Funit =
    coll.update(
      $id(teamId),
      $set(
        "open" -> (data.open == 1),
        "tagTip" -> (data.tagTip == 1),
        "requestTagTip" -> (data.requestTagTip == 1)
      )
    ).void

  def ratingSetting(teamId: String, data: RatingTeamSetting): Funit =
    coll.update(
      $id(teamId),
      $set("ratingSetting" -> data.ratingSetting)
    ).void

  def coinSetting(teamId: String, data: CoinTeamSetting): Funit =
    coll.update(
      $id(teamId),
      $set("coinSetting" -> data.coinSetting)
    ).void

  def toggleStatus(teamId: String, status: Status) =
    coll.updateField($id(teamId), "certification.status", status.id)

  def setCertComments(teamId: String, comments: Option[String]) =
    coll.updateField($id(teamId), "certification.processComments", comments)

  def setCertCooperator(teamId: String, cooperator: User.ID) =
    coll.updateField($id(teamId), "certification.cooperator", cooperator)

  val enabledQuery = $doc("enabled" -> true)

  val sortPopular = $sort desc "nbMembers"
}
