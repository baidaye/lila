package lila.team

import play.api.libs.json._

final class ClockInJsonView() {

  def clockInSettingJson(ci: ClockInSetting) =
    Json.obj(
      "id" -> ci.id,
      "teamId" -> ci.teamId,
      "templateId" -> ci.templateId,
      "name" -> ci.name,
      "startDate" -> ci.startDate.toString("yyyy-MM-dd"),
      "period" -> Json.obj(
        "id" -> ci.period.id,
        "name" -> ci.period.name
      ),
      "status" -> Json.obj(
        "id" -> ci.status.id,
        "name" -> ci.status.name
      ),
      "coinRule" -> (ci.coinRule | 0)
    )

  def tasksJson(tasks: List[ClockInTask]) =
    JsArray(
      tasks.map { task =>
        Json.obj(
          "id" -> task.id,
          "teamId" -> task.teamId,
          "templateId" -> task.templateId,
          "settingId" -> task.settingId,
          "name" -> task.name,
          "index" -> task.index,
          "date" -> task.date.toString("yyyy-MM-dd"),
          "status" -> Json.obj(
            "id" -> task.status.id,
            "name" -> task.status.name
          )
        )
      }
    )

}

