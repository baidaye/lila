package lila.team

import lila.db.dsl._
import lila.user.User

object TeamFederationMemberRepo {

  private val coll = Env.current.colls.federationMember
  import BSONHandlers.TeamFederationMemberBSONHandler

  def byId(id: String): Fu[Option[TeamFederationMember]] =
    coll.byId[TeamFederationMember](id)

  def find(federationId: TeamFederation.ID, teamId: Team.ID): Fu[Option[TeamFederationMember]] =
    coll.byId[TeamFederationMember](TeamFederationMember.makeId(federationId, teamId))

  def exists(federationId: TeamFederation.ID, teamId: Team.ID): Fu[Boolean] =
    coll.exists(selectId(federationId, teamId))

  def findByFederation(federationId: TeamFederation.ID): Fu[List[TeamFederationMember]] =
    coll.list($doc("federationId" -> federationId))

  def findByTeam(teamId: String): Fu[List[TeamFederationMember]] =
    coll.list($doc("teamId" -> teamId))

  def findByFederationTeam(teamId: Team.ID): Fu[List[TeamFederationMember]] =
    coll.find($doc("federationTeamId" -> teamId)).list()

  def insert(member: TeamFederationMember): Funit =
    coll.insert(member).void

  def remove(id: TeamFederationMember.ID): Funit =
    coll.remove($id(id)).void

  def changeTeamOwner(teamId: Team.ID, ownerId: User.ID): Funit =
    coll.update(
      $doc("teamId" -> teamId),
      $set("teamOwnerId" -> ownerId),
      multi = true
    ).void

  def selectId(federationId: TeamFederation.ID, teamId: Team.ID) =
    $id(TeamFederationMember.makeId(federationId, teamId))

}
