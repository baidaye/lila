package lila.team

import akka.actor.ActorSystem
import lila.hub.{ Duct, DuctMap }
import lila.hub.actorApi.socket.SendTo
import lila.notify.Notification.{ Notifies, Sender }
import lila.notify.{ Notification, NotifyApi }
import lila.socket.Socket.makeMessage
import lila.task.{ TTask, TTaskApi, TTaskItem, TeamTestItem }
import lila.user.User
import org.joda.time.DateTime
import lila.team.DataForm.testData._

final class TestApi(taskApi: TTaskApi, system: ActorSystem, sequencers: DuctMap[_], notifyApi: NotifyApi) {

  private val bus = system.lilaBus

  def byId(id: TestTemplate.ID): Fu[Option[TestTemplate]] = TestTemplateRepo.byId(id)

  def addTemplate(data: CreateOrUpdateData, me: User): Fu[TestTemplate] = {
    val tpl = TestTemplate.make(
      name = data.name,
      teamId = me.teamIdValue,
      limitTime = data.limitTime,
      nbQ = data.nbQ,
      passedQ = data.passedQ,
      desc = data.desc,
      items = data.items,
      rule = TestTemplate.Rule(data.rule),
      maxRetry = data.maxRetry,
      userId = me.id
    ).publish
    TestTemplateRepo.insert(tpl) inject tpl
  }

  def updateTemplate(old: TestTemplate, data: CreateOrUpdateData): Funit = {
    val tpl = old.copy(
      name = data.name,
      limitTime = data.limitTime,
      nbQ = data.nbQ,
      passedQ = data.passedQ,
      desc = data.desc,
      items = data.items,
      rule = TestTemplate.Rule(data.rule),
      maxRetry = data.maxRetry,
      updatedAt = DateTime.now
    )
    TestTemplateRepo.update(tpl)
  }

  def close(id: TestTemplate.ID): Funit = {
    TestTemplateRepo.setStatus(id, TestTemplate.Status.Canceled)
  }

  def recover(id: TestTemplate.ID): Funit = {
    TestTemplateRepo.setStatus(id, TestTemplate.Status.Published)
  }

  def sendTest(tpl: TestTemplate, data: TestInviteData, fetchPuzzle: TestTemplate => Fu[List[MiniPuzzle]], me: User): Fu[TestInvite] = {
    val invite = TestInvite.make(tpl, data.deadline, data.members, me.id)
    for {
      _ <- TestInviteRepo.insert(invite)
      puzzles <- fetchPuzzle(tpl)
      testStus = data.members.map { studentId =>
        val outPuzzles = tpl.rule.out(puzzles, tpl.nbQ)
        val items = TestStudentItems(
          puzzles = outPuzzles.map(MiniPuzzleWithResult(_, 0, None))
        )
        TestStudent.make(tpl, invite, items, studentId, me.id)
      }
      _ <- TestStudentRepo.batchInsert(testStus)
      _ <- TestTemplateRepo.incMembers(tpl.id, data.members.size)
      _ <- sendTask(tpl, testStus)
      _ <- applyNotifys(testStus)
    } yield invite
  }

  def sendTask(tpl: TestTemplate, testStus: List[TestStudent]): Funit = {
    val tasks = testStus.map { testStu =>
      TTask.make(
        userIds = List(testStu.studentId),
        name = tpl.name,
        remark = None,
        itemType = TTask.TTaskItemType.TeamTest,
        item = TTaskItem(
          teamTest = TeamTestItem(
            stuTestId = testStu.id,
            num = 0,
            total = testStu.allPuzzles
          ).some
        ),
        sourceRel = TTask.SourceRel(
          id = tpl.id,
          name = tpl.name,
          source = TTask.Source.TeamTest
        ),
        coinTeam = None,
        coinRule = None,
        coinDiff = None,
        tplId = None,
        planAt = None,
        startAt = None,
        deadlineAt = testStu.deadline.some,
        createdBy = tpl.createdBy
      )
    }
    taskApi.batchInsert(tasks)
  }

  def setStart(testStu: TestStudent): Funit =
    Sequencing(testStu.id) {
      testStu.isCreated.?? {
        TestStudentRepo.setStart(testStu)
      }
    }

  def round(testStu: TestStudent, data: TestRoundData): Funit =
    Sequencing(testStu.id) {
      testStu.isStarted.?? {
        testStu.isInItems(data.id, data.index) ?? {
          TestStudentRepo.setRound(testStu.id, data.index, PuzzleResult(data.win, data.moves)) >> {
            taskApi.byTeamTestStuId(testStu.id).flatMap { taskOption =>
              taskOption.?? { task =>
                val taskItem = task.item.copy(
                  teamTest = task.item.teamTest.map { teamTestItem =>
                    val newTestStudent = testStu.copy(items = TestStudentItems(testStu.items.finish(data.index, data.win, data.moves)))
                    val num = newTestStudent.finishPuzzles
                    teamTestItem.copy(num = num)
                  }
                )
                taskApi.setProgress(task, taskItem)
              }
            }
          }
        }
      }
    }

  def redo(testStu: TestStudent, data: TestRoundPuzzle): Funit =
    Sequencing(testStu.id) {
      testStu.isStarted.?? {
        testStu.canRedo(data.id, data.index) ?? {
          TestStudentRepo.redo(testStu.id, data.index)
        }
      }
    }

  def setFinish(testStu: TestStudent): Funit =
    Sequencing(testStu.id) {
      doSetFinish(testStu)
    }

  def doSetFinish(testStu: TestStudent): Funit =
    testStu.isStarted.?? {
      TestStudentRepo.setRemainsTime(testStu) >> TestStudentRepo.setFinish(testStu)
    }

  def scheduledRemainsTime(): Funit = {
    TestStudentRepo.fetchStarted() map { list =>
      list.foreach { testStu =>
        Sequencing(testStu.id) {
          testStu.isStarted.?? {
            TestStudentRepo.setRemainsTime(testStu)
          }
        }
      }
    }
  }

  // 可能会产生5-8秒的延迟
  def scheduledFinish(): Funit = {
    TestStudentRepo.fetchFinished() map { list =>
      list.foreach { testStu =>
        doSetFinish(testStu) >>-
          bus.publish(SendTo(testStu.studentId, makeMessage("stuTestAborted", testStu.id)), 'socketUsers)
      }
    }
  }

  def scheduledExpire(): Funit = {
    TestStudentRepo.fetchExpired() flatMap { ids =>
      ids.nonEmpty.?? {
        TestStudentRepo.setExpire(ids.toList)
      }
    }
  }

  def setRemark(testStu: TestStudent, data: String): Funit = {
    TestStudentRepo.setRemark(testStu.id, data)
  }

  def Sequencing(testStuId: TestStudent.ID)(f: => Funit): Funit = {
    val promise = scala.concurrent.Promise[Unit]
    sequencers.tell(testStuId, Duct.extra.LazyPromise(Duct.extra.LazyFu(() => f), promise))
    promise.future
  }

  private def applyNotifys(testStus: List[TestStudent]): Funit =
    testStus.map(applyNotify).sequenceFu.void

  private def applyNotify(testStu: TestStudent): Funit = {
    notifyApi.addNotification(Notification.make(
      Sender(testStu.createdBy).some,
      Notifies(testStu.studentId),
      lila.notify.GenericLink(
        url = s"/team/test/stu/${testStu.id}/show",
        title = "战术测评".some,
        text = s"收到一份测评《${testStu.tpl.name}》".some,
        icon = "测"
      )
    ))
  }

}
