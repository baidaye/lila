package lila.team

import lila.db.dsl._

object ClockInTaskRepo {

  // dirty
  private val coll = Env.current.colls.clockInTask

  import BSONHandlers.ClockInTaskBSONHandler

  def byId(id: ClockInTask.ID): Fu[Option[ClockInTask]] = coll.byId[ClockInTask](id)

  def bySourceId(sourceId: String): Fu[Option[ClockInTask]] = {
    val d = lila.team.ClockInTask.parseSourseId(sourceId)
    coll.byId[ClockInTask](d._1)
  }

  def bySetting(settingId: ClockInSetting.ID): Fu[List[ClockInTask]] =
    coll.find($doc("settingId" -> settingId))
      .sort($sort asc "index")
      .list()

  def bulkInsert(tasks: List[ClockInTask]): Funit =
    coll.bulkInsert(
      documents = tasks.map(ClockInTaskBSONHandler.write).toStream,
      ordered = true
    ).void

  def update(task: ClockInTask): Funit =
    coll.update($id(task.id), task).void

  def removeBySetting(settingId: ClockInSetting.ID): Funit =
    coll.remove($doc("settingId" -> settingId)).void

}
