package lila.team

import org.joda.time.DateTime
import lila.user.User

case class Member(
    _id: String,
    team: String,
    campus: String,
    user: String,
    role: Set[Member.Role],
    rating: Option[EloRating],
    coin: Option[Int],
    clazzIds: Option[List[String]],
    tags: Option[MemberTags],
    date: DateTime
) {

  def tagsIfEmpty: MemberTags = tags | MemberTags.empty
  def tagValue(field: String) = tagsIfEmpty.tagMap.get(field) ?? (_.value)
  def is(userId: String): Boolean = user == userId
  def is(user: User): Boolean = is(user.id)
  def id = _id
  def isOwner = role.contains(Member.Role.Owner)
  def isAdmin = role.contains(Member.Role.Admin)
  def isCoach = role.contains(Member.Role.Coach)
  def isTrainee = role.contains(Member.Role.Trainee)
  def isManager = isOwner || isAdmin || isCoach
  def isOwnerOrAdmin = isOwner || isAdmin
  def intRating = rating.map(_.intValue)
  def intRatingOrZero = intRating | 0

  def coinOrZero = coin | 0

  def clazzIdOrDefault = clazzIds | Nil

  def roles = role.toList.sortBy(_.sort).map(_.name).mkString(",")

}

object Member {

  def makeId(team: String, user: String) = user + "@" + team

  def make(
    team: String,
    user: String,
    campus: String,
    role: Role = Role.Trainee,
    tags: Option[MemberTags] = None,
    rating: Option[Int] = None,
    coin: Option[Int] = None,
    clazzIds: Option[List[String]] = None
  ): Member = new Member(
    _id = makeId(team, user),
    user = user,
    team = team,
    campus = campus,
    role = Set(role),
    rating = rating.map(EloRating(_, 0)),
    coin = coin,
    tags = tags,
    clazzIds = clazzIds,
    date = DateTime.now
  )

  sealed abstract class Role(val id: String, val name: String, val canWrite: Boolean, val sort: Int)
  object Role {
    case object Owner extends Role("owner", "所有者", true, 1)
    case object Admin extends Role("admin", "校区管理员", false, 2)
    case object Coach extends Role("coach", "教练", false, 3)
    case object Trainee extends Role("trainee", "成员", false, 4)

    val all = List(Owner, Admin, Coach, Trainee)
    def list = all.map { r => r.id -> r.name }
    def byId = all.map { x => x.id -> x }.toMap
    def apply(id: String): Role = all.find(_.id == id) err s"Count find Role: $id"
  }

}

case class MemberWithUser(member: Member, user: User, markOrName: String = "", isPlaying: Boolean = false) {
  def team = member.team
  def date = member.date
  def profile = user.profileOrDefault
  def userId = user.id
}

case class MemberOptionWithUser(member: Option[Member], user: User, markOrName: String = "") {
  def profile = user.profileOrDefault
  def userId = user.id
}

case class MemberWithMark(member: Member, user: User, mark: Option[String])

case class RangeMemberTag(field: String, min: Option[String], max: Option[String])
case class MemberTag(field: String, value: Option[String])
object MemberTag {
  type TagField = String
  type MemberTagMap = Map[TagField, MemberTag]
}

case class MemberTags(tagMap: MemberTag.MemberTagMap) {
  def toList = tagMap.toList
  def fieldValue(field: String): Option[String] = tagMap.get(field).??(_.value)
}

object MemberTags {

  val empty = MemberTags(Map.empty)
  def byTagList(tagList: List[MemberTag]) = MemberTags(tagList.map(t => t.field -> t).toMap)
}
