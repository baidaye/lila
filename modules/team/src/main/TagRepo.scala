package lila.team

import lila.db.dsl._
import lila.team.DataForm.tagData.TagEdit
import reactivemongo.api.ReadPreference

object TagRepo {

  // dirty
  private val coll = Env.current.colls.tag

  type ID = String
  import BSONHandlers.TagBSONHandler

  def byId(id: ID): Fu[Option[Tag]] = coll.byId[Tag](id)

  def findByTeam(teamId: ID): Fu[List[Tag]] =
    coll.find(teamQuery(teamId) ++ $doc("field" $nin Tag.oldDefault))
      .sort($doc("sort" -> 1, "createAt" -> 1))
      .list[Tag]()

  def findByTeams(teamIds: List[ID]): Fu[Map[String, List[Tag]]] =
    coll.find($doc("team" $in teamIds, "field" $nin Tag.oldDefault))
      .sort($doc("team" -> 1, "sort" -> 1, "createAt" -> 1))
      .list[Tag]() map { list =>
        list.groupBy(_.team)
      }

  def create(tag: Tag) = coll.insert(tag)

  def update(id: ID, tag: TagEdit): Funit =
    coll.update(
      $id(id),
      $set(
        "label" -> tag.label,
        "value" -> tag.value,
        "required" -> tag.required,
        "request" -> tag.request,
        "visitable" -> tag.visitable,
        "sort" -> tag.sort
      )
    ).void

  def remove(id: ID) = coll.remove($id(id))

  def teamQuery(teamId: ID) = $doc("team" -> teamId)

  def editableQuery = $doc("editable" -> true)

}
