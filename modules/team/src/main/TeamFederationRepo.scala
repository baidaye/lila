package lila.team

import lila.db.dsl._
import lila.user.User

object TeamFederationRepo {

  private val coll = Env.current.colls.federation
  import BSONHandlers.TeamFederationBSONHandler

  def byId(id: TeamFederation.ID): Fu[Option[TeamFederation]] = coll.byId[TeamFederation](id)

  def byIds(ids: List[TeamFederation.ID]): Fu[List[TeamFederation]] = coll.byIds[TeamFederation](ids)

  def findByTeam(teamId: String): Fu[List[TeamFederation]] =
    coll.list($doc("teamId" -> teamId))

  def byIdOrName(q: String): Fu[List[TeamFederation]] =
    coll.list($or($doc("_id" -> q), $doc("name" -> q)))

  def findNextId: Fu[Int] = {
    coll.find($empty, $id(true))
      .sort($sort desc "_id")
      .uno[Bdoc] map {
        _ flatMap { doc => doc.getAs[String]("_id") map (1 + _.toInt) } getOrElse 100000
      }
  }

  def insert(federation: TeamFederation): Funit =
    coll.insert(federation).void

  def update(federation: TeamFederation): Funit =
    coll.update($id(federation.id), federation).void

  def setEnabled(id: TeamFederation.ID, enabled: Boolean): Funit =
    coll.update($id(id), $set("enabled" -> enabled)).void

  def incMembers(id: TeamFederation.ID, by: Int): Funit =
    coll.update($id(id), $inc("nbMembers" -> by)).void

  def open(id: TeamFederation.ID, open: Boolean): Funit =
    coll.updateField($id(id), "open", open).void

  def changeTeamOwner(teamId: Team.ID, ownerId: User.ID): Funit =
    coll.update(
      $doc("teamId" -> teamId),
      $set("teamOwnerId" -> ownerId),
      multi = true
    ).void

}
