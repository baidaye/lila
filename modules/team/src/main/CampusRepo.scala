package lila.team

import lila.db.dsl._
import lila.team.DataForm.campusData._

object CampusRepo {

  private val coll = Env.current.colls.campus

  import BSONHandlers.TeamCampusBSONHandler

  type ID = String

  def exists(teamId: ID, name: String): Fu[Boolean] =
    coll.exists($doc("team" -> teamId, "name" -> name))

  def byId(id: ID): Fu[Option[Campus]] = coll.byId(id)

  def byIds(ids: List[String]): Fu[List[Campus]] =
    coll.byIds(ids)

  def byTeamIds(teamIds: List[String]): Fu[List[Campus]] =
    coll.find($doc("team" $in teamIds)).list()

  def byTeam(teamId: String): Fu[List[Campus]] =
    coll.find(teamQuery(teamId)).sort($doc("sort" -> 1)).list()

  def byAdmin(teamId: String, userId: String): Fu[List[Campus]] =
    coll.find(teamQuery(teamId) ++ $doc("admin" -> userId)).sort($doc("sort" -> 1)).list()

  def byCoach(teamId: String, userId: String): Fu[List[Campus]] =
    coll.find(teamQuery(teamId) ++ $doc("coach" -> userId)).sort($doc("sort" -> 1)).list()

  def byOrderedIds(campuses: Seq[ID]) =
    coll.byOrderedIds[Campus, ID](campuses)(_.id)

  def add(
    teamId: ID,
    createBy: lila.user.User.ID,
    data: CampusData
  ): Fu[Campus] =
    coll.find(teamQuery(teamId), $doc("no" -> true))
      .sort($sort desc "no")
      .uno[Bdoc] map {
        _ flatMap { doc =>
          doc.getAs[Int]("no") map (1 + _)
        } getOrElse 1
      } flatMap { no =>
        val campus = Campus.make(no, teamId, data.name, data.sort, data.admin, none, data.addr, data.intro, createBy)
        coll.insert(campus).inject(campus)
      }

  def findNextSort(teamId: ID) =
    coll.find(teamQuery(teamId), $doc("sort" -> true))
      .sort($sort desc "sort")
      .uno[Bdoc] map {
        _ flatMap { doc =>
          doc.getAs[Int]("sort") map (1 + _)
        } getOrElse 1
      }

  def update(c: Campus): Funit = coll.update($id(c.id), c).void

  def remove(id: ID): Funit = coll.remove($id(id)).void

  def addClazz(id: ID, clazzId: String): Funit =
    coll.update(
      $id(id),
      $addToSet("clazzIds" -> clazzId)
    ).void

  def removeClazz(id: ID, clazzId: String): Funit =
    coll.update(
      $id(id),
      $pull("clazzIds" -> clazzId)
    ).void

  def addCoach(id: String, coach: String): Funit =
    coll.update(
      $id(id),
      $addToSet("coach" -> coach)
    ).void

  def removeCoach(id: String, coach: String): Funit =
    coll.update(
      $id(id),
      $pull("coach" -> coach)
    ).void

  def incMembers(id: ID, by: Int): Funit =
    coll.update($id(id), $inc("nbMembers" -> by)).void

  def teamQuery(teamId: ID) = $doc("team" -> teamId)
  def selectId(teamId: ID, no: Int) = $id(Campus.makeId(teamId, no))

}
