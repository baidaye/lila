package lila.team

import lila.user.User
import org.joda.time.DateTime
import akka.actor.ActorSystem
import lila.hub.actorApi.task.{ ChangeProgress, ChangeStatus }
import lila.task.{ TTask, TTaskApi, TTaskTemplate, TTaskTemplateApi, TeamClockInTaskSettingData }

final class ClockInApi(taskApi: TTaskApi, taskTemplateApi: TTaskTemplateApi, system: ActorSystem) {

  private val bus = system.lilaBus

  bus.subscribeFun('ttask) {
    case cs: ChangeStatus => (cs.sourceRel.source == "teamClockIn" && cs.status == "finished").?? {
      ClockInTaskRepo.bySourceId(cs.sourceRel.id) foreach {
        _.?? { clockInTask =>
          ClockInMemberRepo.setFinished(clockInTask.id, cs.userId, cs.num)
        }
      }
    }
    case cp: ChangeProgress => (cp.sourceRel.source == "teamClockIn").?? {
      ClockInTaskRepo.bySourceId(cp.sourceRel.id) foreach {
        _.?? { clockInTask =>
          ClockInMemberRepo.setNum(clockInTask.id, cp.userId, cp.num)
        }
      }
    }
  }

  def byId(id: ClockInSetting.ID): Fu[Option[ClockInSetting]] = ClockInSettingRepo.byId(id)

  def create(team: Team, data: TeamClockInTaskSettingData, creator: User): Funit = {
    val tpl = data.toTaskTemplate(creator)
    val setting = ClockInSetting.make(
      teamId = team.id,
      templateId = tpl.id,
      name = data.name,
      startDate = data.startDate,
      period = data.period,
      coinTeam = data.coinRule.map(_ => creator.belongTeamIdValue),
      coinRule = data.coinRule,
      createdBy = creator.id
    )
    taskTemplateApi.create(tpl) >> {
      val tasks = data.tasks.map { t =>
        ClockInTask.make(
          name = t.name,
          teamId = team.id,
          templateId = tpl.id,
          settingId = setting.id,
          index = t.index,
          date = t.date,
          status = t.status,
          coinTeam = setting.coinTeam,
          coinRule = setting.coinRule,
          createdBy = creator.id
        )
      }
      ClockInTaskRepo.bulkInsert(tasks)
    } >> ClockInSettingRepo.insert(setting)
  }

  def update(ci: ClockInSetting, team: Team, data: TeamClockInTaskSettingData, creator: User): Funit = {
    taskTemplateApi.byId(ci.templateId) flatMap {
      _.?? { oldTpl =>
        val tpl = data.toTaskTemplate(creator).copy(_id = oldTpl.id)
        val setting = ClockInSetting.make(
          teamId = team.id,
          templateId = tpl.id,
          name = data.name,
          startDate = data.startDate,
          period = data.period,
          coinTeam = data.coinRule.map(_ => creator.belongTeamIdValue),
          coinRule = data.coinRule,
          createdBy = creator.id
        ).copy(_id = ci.id)
        taskTemplateApi.update(tpl) >> {
          val tasks = data.tasks.map { t =>
            ClockInTask.make(
              name = t.name,
              teamId = team.id,
              templateId = tpl.id,
              settingId = setting.id,
              index = t.index,
              date = t.date,
              status = t.status,
              coinTeam = setting.coinTeam,
              coinRule = setting.coinRule,
              createdBy = creator.id
            )
          }
          ClockInTaskRepo.removeBySetting(ci.id) >> ClockInTaskRepo.bulkInsert(tasks)
        } >> ClockInSettingRepo.update(setting)
      }
    }
  }

  def publish(clockInSetting: ClockInSetting): Funit =
    ClockInSettingRepo.setStatus(clockInSetting, ClockInSetting.Status.Published)

  def close(clockInSetting: ClockInSetting): Funit =
    ClockInSettingRepo.setStatus(clockInSetting, ClockInSetting.Status.Closed) >> {
      ClockInMemberRepo.bySettingNoStart(clockInSetting.id) map { members =>
        val ids = members.map { m => s"${m.taskId}@${m.userId}" }
        taskApi.removeCalendarByIds(ids)
      }
    }

  def join(clockInSetting: ClockInSetting, taskTemplate: TTaskTemplate, studentId: User.ID) =
    ClockInSettingRepo.addStudent(clockInSetting.id, studentId) >> {
      ClockInMemberRepo.exists(clockInSetting.id, studentId) flatMap { wasJoined =>
        (!wasJoined).?? {
          ClockInTaskRepo.bySetting(clockInSetting.id) flatMap { clockInTasks =>
            val tasks = clockInTasks.filter(_.isCreated).map { clockInTask =>
              TTask.makeByTemplate(
                taskTemplate = taskTemplate,
                userId = studentId,
                sourceId = clockInTask.sourceId,
                sourceName = clockInTask.sourceName(clockInSetting.name),
                coinTeam = taskTemplate.coinTeam,
                coinRule = taskTemplate.coinRule,
                planAt = clockInTask.date.withTimeAtStartOfDay().some,
                startAt = clockInTask.date.withTimeAtStartOfDay().some,
                deadlineAt = clockInTask.date.withTime(23, 59, 59, 999).some,
                createdBy = clockInSetting.createdBy
              )
            }
            taskApi.batchInsert(tasks) >>- taskApi.publishCalendars(tasks)

            val members = tasks.map { task =>
              val d = ClockInTask.parseSourseId(task.sourceRel.id)
              ClockInMember.make(
                teamId = clockInSetting.teamId,
                templateId = clockInSetting.templateId,
                settingId = clockInSetting.id,
                clockInTaskId = d._1,
                taskId = task.id,
                userId = studentId,
                date = d._2,
                createdBy = clockInSetting.createdBy
              )
            }
            ClockInMemberRepo.bulkInsert(members)
          }
        }
      }
    }

  def leave(clockInSetting: ClockInSetting, studentId: User.ID): Funit =
    ClockInSettingRepo.removeStudent(clockInSetting.id, studentId) >> {
      val today = DateTime.now.withTime(23, 59, 59, 999)
      ClockInMemberRepo.bySetting(clockInSetting.id, studentId) flatMap { members =>
        val taskIds = members.filter(_.date.isAfter(today)).map(_.taskId)
        taskApi.byIds(taskIds) map { tasks =>
          taskApi.removeCalendars(tasks)
        }
      }
    }

}
