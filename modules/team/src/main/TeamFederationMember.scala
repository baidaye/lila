package lila.team

import org.joda.time.DateTime
import lila.user.User

case class TeamFederationMember(
    _id: TeamFederationMember.ID,
    federationId: TeamFederation.ID,
    federationTeamId: Team.ID,
    teamId: Team.ID,
    teamOwnerId: User.ID,
    role: TeamFederationMember.Role,
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

  def isOwner = role == TeamFederationMember.Role.Owner
  def isMember = role == TeamFederationMember.Role.Member
}

object TeamFederationMember {

  type ID = String

  case class WithTeam(member: TeamFederationMember, team: Team)

  def makeId(federationId: String, teamId: String) = federationId + "@" + teamId

  def make(
    federationId: TeamFederation.ID,
    federationTeamId: Team.ID,
    teamId: Team.ID,
    teamOwnerId: User.ID,
    role: TeamFederationMember.Role = TeamFederationMember.Role.Member,
    createdBy: User.ID
  ): TeamFederationMember =
    new TeamFederationMember(
      _id = makeId(federationId, teamId),
      federationId = federationId,
      federationTeamId = federationTeamId,
      teamId = teamId,
      teamOwnerId = teamOwnerId,
      role = role,
      createdAt = DateTime.now,
      createdBy = createdBy
    )

  sealed abstract class Role(val id: String, val name: String)
  object Role {
    case object Owner extends Role("owner", "所有者")
    case object Member extends Role("member", "成员")

    val all = List(Owner, Member)
    def list = all.map { r => r.id -> r.name }
    def byId = all.map { x => x.id -> x }.toMap
    def apply(id: String): Role = all.find(_.id == id) err s"Count find Role: $id"
  }

  case class WithUser(member: TeamFederationMember, user: User)

}

