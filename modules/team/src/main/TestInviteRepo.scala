package lila.team

import lila.db.dsl._

object TestInviteRepo {

  private val coll = Env.current.colls.testInvite

  import BSONHandlers.TestInviteBSONHandler

  def byId(id: TestInvite.ID): Fu[Option[TestInvite]] =
    coll.byId(id)

  def byTeam(teamId: Team.ID): Fu[List[TestInvite]] =
    coll.find(teamQuery(teamId)).list[TestInvite]()

  def insert(invite: TestInvite): Funit =
    coll.insert(invite).void

  def teamQuery(teamId: Team.ID) = $doc("teamId" -> teamId)

}
