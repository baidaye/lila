package lila.team

import lila.user.User
import org.joda.time.DateTime

case class TeamCooperate(
    _id: TeamCooperate.ID,
    userId: User.ID,
    teamId: String,
    certifyTime: DateTime, // 同俱乐部认证时间（applyAt）
    orders: List[TeamCooperateOrder]
) {

  def deadlineAt = certifyTime.plusYears(TeamCooperate.maxYear)

  def isDeadlined = deadlineAt.isBeforeNow

  def points = orders.foldLeft(0) {
    case (all, order) => all + order.points
  }

}
case class TeamCooperateOrder(
    orderId: String,
    userId: String,
    points: Int,
    createdAt: DateTime
)

object TeamCooperate {

  type ID = String

  val maxYear = 5
  val maxCount = 3

  def makeId(userId: String, teamId: String) = s"$userId@$teamId"

  def make(userId: String, teamId: String, applyAt: DateTime) =
    TeamCooperate(
      _id = makeId(userId, teamId),
      userId = userId,
      teamId = teamId,
      certifyTime = applyAt,
      orders = Nil
    )

  case class WithTeam(team: Team, cooperate: TeamCooperate, enabled: EnabledTyp = EnabledTyp.Disabled) {

    def manzhuyaoqiu = team.enabled && team.certified && !cooperate.isDeadlined

  }

  sealed class EnabledTyp(val id: Boolean, val name: String)
  object EnabledTyp {
    case object Enabled extends EnabledTyp(true, "满足要求")
    case object Disabled extends EnabledTyp(false, "不满足要求")

    val all = List(Enabled, Disabled)
    val byId = all map { v => (v.id, v) } toMap
    def selects = all.map { v => (v.id, v.name) }
    def apply(id: Boolean): EnabledTyp = byId.get(id) err s"Bad EnabledTyp $id"
  }
}

case class TeamCooperates(cooperates: List[TeamCooperate.WithTeam]) {

  def nonEmpty = cooperates.nonEmpty

  def points = cooperates.foldLeft(0) {
    case (all, iwt) => all + iwt.cooperate.points
  }

  def processCooperates(typ: Option[TeamCooperate.EnabledTyp]) = {
    val enabledTeamIds = enabledTeams()
    val cps = cooperates.map { iwt =>
      if (enabledTeamIds.contains(iwt.team.id)) iwt.copy(enabled = TeamCooperate.EnabledTyp.Enabled) else iwt
    }.sortBy(-_.cooperate.certifyTime.getMillis)

    typ match {
      case None => cps
      case Some(t) => cps.filter(_.enabled == t)
    }
  }

  /*
    开始积分说明：
    1、邀请俱乐部认证，5年内、未关闭，满足积分要求
    2、满足积分要求俱乐部 >=3
  */
  def enabledTeams() = {
    val filteredCooperates = cooperates.filter(itw => itw.manzhuyaoqiu)
    if (filteredCooperates.size >= TeamCooperate.maxCount) {
      filteredCooperates.map(_.team.id)
    } else Nil
  }

}
