package lila.team

import TestTemplate._
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class TestTemplate(
    _id: ID,
    teamId: String,
    name: String,
    limitTime: Int, //minutes
    nbQ: Int,
    passedQ: Int,
    desc: Option[String],
    items: TestItems,
    nbMembers: Int,
    rule: Rule,
    maxRetry: Int,
    status: Status,
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

  def isCreator(userId: User.ID) = createdBy == userId

  def capsuleIds = items.capsuleIds

  def publish = copy(status = Status.Published)

  def isPublished = status == Status.Published
  def isCanceled = status == Status.Canceled

  def light = LightTestTemplate(
    id = id,
    name = name,
    limitTime = limitTime,
    nbQ = nbQ,
    passedQ = passedQ,
    desc = desc,
    items = items,
    rule = rule,
    maxRetry = maxRetry
  )

}

object TestTemplate {

  type ID = String

  def make(
    name: String,
    teamId: String,
    limitTime: Int,
    nbQ: Int,
    passedQ: Int,
    desc: Option[String],
    items: TestItems,
    rule: Rule,
    maxRetry: Int,
    userId: User.ID
  ) = TestTemplate(
    _id = Random nextString 8,
    teamId = teamId,
    name = name,
    limitTime = limitTime,
    nbQ = nbQ,
    passedQ = passedQ,
    desc = desc,
    items = items,
    nbMembers = 0,
    rule = rule,
    maxRetry = maxRetry,
    status = Status.Created,
    createdAt = DateTime.now,
    updatedAt = DateTime.now,
    createdBy = userId
  )

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "未发布")
    case object Published extends Status("published", "已发布")
    case object Canceled extends Status("canceled", "已作废")

    val all = List(Created, Published, Canceled)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): Status = byId get id err s"Bad Status $id"
  }

  sealed abstract class Rule(val id: String, val name: String) {
    def out(in: List[MiniPuzzle], limit: Int): List[MiniPuzzle]
  }
  object Rule {
    case object Increase extends Rule("increase", "难度递增") {

      override def out(in: List[MiniPuzzle], limit: Int): List[MiniPuzzle] = {
        // 第一步：将题库分为三份
        // 简单:30%, 中等:50%, 困难:20%
        val grpRatingPuzzle = in.sortBy(_.rating).zipWithIndex.groupBy {
          case (_, index) => {
            val rate = (index + 1) * 1.0 / in.length
            if (rate < 0.3) "simple"
            else if (rate >= 0.3 && rate <= 0.8) "medium"
            else "difficulty"
          }
        }

        // 第二步：得到每份数据中要截取的题目数量
        val grpLimit = (1 to limit).groupBy { nb =>
          val rate = nb * 1.0 / limit
          if (rate < 0.3) "simple"
          else if (rate >= 0.3 && rate <= 0.8) "medium"
          else "difficulty"
        }

        // 第三步：每份数据中取得对应的数量 -> 合并 -> 排序
        val result = grpRatingPuzzle.map {
          case (key, list) => scala.util.Random.shuffle(list).take(grpLimit.get(key).map(_.size) | 0)
        }.flatMap(_.map(_._1)).toList.sortBy(_.rating)

        result
      }
    }
    case object Random extends Rule("random", "难度随机") {
      override def out(in: List[MiniPuzzle], limit: Int): List[MiniPuzzle] = {
        scala.util.Random.shuffle(in).take(limit)
      }
    }

    val all = List(Increase, Random)
    val byId = all map { v => (v.id, v) } toMap
    def choices = all.map(r => r.id -> r.name)
    def apply(id: String): Rule = byId get id err s"Bad Rule $id"
  }

}

case class LightTestTemplate(
    id: ID,
    name: String,
    limitTime: Int,
    nbQ: Int,
    passedQ: Int,
    desc: Option[String],
    items: TestItems,
    rule: Rule,
    maxRetry: Int
) {

  def limitTimeSeconds = limitTime * 60
}

case class TestItems(capsules: Option[List[String]]) {

  def capsuleIds = capsules | Nil

}
