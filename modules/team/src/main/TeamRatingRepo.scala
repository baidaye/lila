package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import play.api.libs.json.JsArray
import play.api.libs.json.Json
import lila.user.User

import scala.math.BigDecimal.RoundingMode

object TeamRatingRepo {

  private val coll = Env.current.colls.rating
  import BSONHandlers._

  def byId(id: String): Fu[Option[TeamRating]] = coll.byId[TeamRating](id)

  def findByUser(userId: String): Fu[List[TeamRating]] =
    coll.find(userQuery(userId)).sort($doc("createAt" -> -1)).list[TeamRating]()

  def findByContest(contestId: String, isTeam: Option[Boolean] = None): Fu[Map[User.ID, Double]] =
    coll.find($doc("metaData.contestId" -> contestId) ++ isTeam.?? { it => $doc("metaData.isTeam" -> it) })
      .sort($doc("createAt" -> -1))
      .list[TeamRating]().map {
        _.groupBy(_.userId).map {
          case (userId, list) => {
            userId -> {
              val diff = list.foldLeft(0.0d) {
                case (num, tr) => num + tr.diff
              }
              BigDecimal(diff).setScale(1, RoundingMode.DOWN).doubleValue()
            }
          }
        }
      }

  def historyData(userId: String): Fu[JsArray] = findByUser(userId).map { list =>
    JsArray(
      list.groupBy { _.createAt.toString("yyyy/MM/dd") }.map {
        case (date, list) => date -> {
          val last = list.maxBy(_.createAt)
          (last.rating + last.diff).toInt
        }
      }.toSeq.sortBy(_._1).map { d =>
        val format = d._1.split("/").map { x =>
          if (x.startsWith("0")) {
            x.drop(1)
          } else x
        }.mkString("/")
        Json.obj("date" -> format, "rating" -> d._2)
      }
    )
  }

  def insert(rating: TeamRating): Funit = coll.insert(rating).void

  def page(page: Int, userId: String): Fu[Paginator[TeamRating]] = {
    val adapter = new Adapter[TeamRating](
      collection = coll,
      selector = userQuery(userId),
      projection = $empty,
      sort = $doc("createAt" -> -1)
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  def userQuery(userId: String) = $doc("userId" -> userId)

}
