package lila.team

import org.joda.time.DateTime

import lila.user.User

case class TeamFederationRequest(
    _id: TeamFederationRequest.ID,
    federationId: TeamFederation.ID,
    federationTeamId: Team.ID,
    teamId: Team.ID,
    teamOwnerId: User.ID,
    message: String,
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

}

object TeamFederationRequest {

  type ID = String

  case class WithTeam(request: TeamFederationRequest, team: Team)

  def makeId(federationId: TeamFederation.ID, teamId: Team.ID) = federationId + "@" + teamId

  def make(
    federationId: TeamFederation.ID,
    federationTeamId: Team.ID,
    teamId: Team.ID,
    teamOwnerId: User.ID,
    message: String,
    createdBy: User.ID
  ): TeamFederationRequest =
    new TeamFederationRequest(
      _id = makeId(federationId, teamId),
      federationId = federationId,
      federationTeamId = federationTeamId,
      teamId = teamId,
      teamOwnerId = teamOwnerId,
      message = message,
      createdAt = DateTime.now,
      createdBy = createdBy
    )
}

