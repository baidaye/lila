package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import lila.team.DataForm.testData.TestStudentSearch
import lila.user.{ User, UserRepo }
import org.joda.time.DateTime

object TestStudentRepo {

  private val coll = Env.current.colls.testStudent

  import BSONHandlers.TestStudentBSONHandler

  def byId(id: TestStudent.ID): Fu[Option[TestStudent]] =
    coll.byId(id)

  def page(page: Int, tplId: TestTemplate.ID, search: TestStudentSearch, markMap: Map[String, Option[String]]): Fu[Paginator[TestStudent.WithUser]] = {
    var $selector = tplQuery(tplId)
    search.username.foreach { username =>
      val filterMarkUserIds = markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(username.toLowerCase)
        }
      }.keySet

      $selector = $selector ++ $or($doc("studentId" $regex (username.trim, "i")), $doc("studentId" $in filterMarkUserIds))
    }

    search.status.foreach { status =>
      $selector = $selector ++ $doc("status" -> status)
    }

    search.passed.foreach { passed =>
      $selector = $selector ++ $doc("isPassed" -> passed)
    }

    val adapter = new Adapter[TestStudent](
      collection = coll,
      selector = $selector,
      projection = $empty,
      sort = $doc("createdAt" -> -1)
    ) mapFutureList withUsers

    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  private def withUsers(testStus: Seq[TestStudent]): Fu[List[TestStudent.WithUser]] =
    UserRepo.withColl {
      _.byOrderedIds[User, User.ID](testStus.map(_.studentId))(_.id)
    } map { users =>
      testStus zip users collect {
        case (testStu, user) => TestStudent.WithUser(testStu, user)
      } toList
    }

  def createdById(id: TestStudent.ID): Fu[Option[TestStudent]] =
    coll.find($id(id) ++ createdSelect).uno[TestStudent]

  def startedById(id: TestStudent.ID): Fu[Option[TestStudent]] =
    coll.find($id(id) ++ startedSelect).uno[TestStudent]

  def finishedById(id: TestStudent.ID): Fu[Option[TestStudent]] =
    coll.find($id(id) ++ finishedSelect).uno[TestStudent]

  def fetchStarted(): Fu[List[TestStudent]] =
    coll.list[TestStudent](startedSelect ++ $doc("remainsTime" $gte 0))

  def fetchFinished(): Fu[List[TestStudent]] =
    coll.list[TestStudent](startedSelect ++ $doc("remainsTime" $lte 0))

  def fetchExpired(): Fu[Set[TestStudent.ID]] =
    coll.distinct[String, Set]("_id", (createdSelect ++ $doc("deadline" $lt DateTime.now())).some)

  def setStart(testStu: TestStudent): Funit = {
    val now = DateTime.now
    coll.update(
      $id(testStu.id),
      $set(
        "status" -> TestStudent.Status.Started.id,
        "startAt" -> DateTime.now,
        "updatedAt" -> now
      )
    ).void
  }

  def insert(testStu: TestStudent): Funit =
    coll.insert(testStu).void

  def update(testStu: TestStudent): Funit =
    coll.update($id(testStu.id), testStu).void

  def batchInsert(testStus: List[TestStudent]): Funit =
    coll.bulkInsert(
      documents = testStus.map(TestStudentBSONHandler.write).toStream,
      ordered = true
    ).void

  def setRound(id: TestStudent.ID, index: Int, result: PuzzleResult): Funit =
    coll.update(
      $id(id),
      $set(s"items.puzzles.$index.result" -> BSONHandlers.MiniPuzzleResultHandler.write(result))
    ).void

  def redo(id: TestStudent.ID, index: Int): Funit =
    coll.update(
      $id(id),
      $unset(s"items.puzzles.$index.result") ++ $inc(s"items.puzzles.$index.retry" -> 1)
    ).void

  def setRemainsTime(testStu: TestStudent): Funit = {
    coll.update(
      $id(testStu.id),
      $set("remainsTime" -> testStu.newRemainsTime)
    ).void
  }

  def setFinish(testStu: TestStudent): Funit = {
    val now = DateTime.now
    coll.update(
      $id(testStu.id),
      $set(
        "status" -> TestStudent.Status.Finished.id,
        "isPassed" -> (testStu.rightPuzzles >= testStu.tpl.passedQ),
        "finishAt" -> now,
        "updatedAt" -> now
      )
    ).void
  }

  def setExpire(ids: List[TestStudent.ID]): Funit =
    coll.update(
      $inIds(ids),
      $set(
        "status" -> TestStudent.Status.Expired.id,
        "updatedAt" -> DateTime.now
      ),
      multi = true
    ).void

  def setRemark(id: TestStudent.ID, remark: String): Funit =
    coll.update(
      $id(id),
      $set(
        "remark" -> remark,
        "updatedAt" -> DateTime.now
      ),
      multi = true
    ).void

  private def tplQuery(tplId: TestTemplate.ID) = $doc("tplId" -> tplId)
  private val createdSelect = $doc("status" -> TestStudent.Status.Created.id)
  private val startedSelect = $doc("status" -> TestStudent.Status.Started.id)
  private val finishedSelect = $doc("status" -> TestStudent.Status.Finished.id)

}
