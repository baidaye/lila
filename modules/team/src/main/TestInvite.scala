package lila.team

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class TestInvite(
    _id: String,
    tplId: TestTemplate.ID,
    teamId: Team.ID,
    deadline: DateTime,
    studentIds: List[User.ID],
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

}

object TestInvite {

  type ID = String

  def make(
    tpl: TestTemplate,
    deadline: DateTime,
    studentIds: List[User.ID],
    userId: User.ID
  ) =
    TestInvite(
      _id = Random nextString 8,
      tplId = tpl.id,
      teamId = tpl.teamId,
      deadline = deadline,
      studentIds = studentIds,
      createdAt = DateTime.now,
      createdBy = userId
    )

}
