package lila.team

import lila.db.dsl._
import lila.db.BSON
import reactivemongo.bson.{ BSONDocument, BSONHandler, BSONInteger, BSONString, BSONWriter, Macros }

private object BSONHandlers {

  import lila.db.dsl.BSONJodaDateTimeHandler

  private[team] implicit val RoleBSONHandler = new BSONHandler[BSONString, Member.Role] {
    def read(b: BSONString) = Member.Role.byId get b.value err s"Invalid role ${b.value}"
    def write(x: Member.Role) = BSONString(x.id)
  }
  private[team] implicit val RoleArrayHandler = bsonArrayToListHandler[Member.Role]
  private[team] implicit val MemberTagBSONHandler = Macros.handler[MemberTag]
  private[team] implicit val MemberTagBSONWriter = new BSONWriter[MemberTag, Bdoc] {
    def write(x: MemberTag) = MemberTagBSONHandler write x
  }
  private[team] implicit val MemberTagsBSONHandler = new BSONHandler[Bdoc, MemberTags] {
    private val mapHandler = BSON.MapDocument.MapHandler[String, MemberTag]
    def read(b: Bdoc) = MemberTags(mapHandler read b map {
      case (field, tag) => field -> tag
    })
    def write(x: MemberTags) = BSONDocument(x.tagMap.mapValues(MemberTagBSONWriter.write))
  }
  private[team] implicit val EloRatingBSONHandler = new BSON[EloRating] {

    def reads(r: BSON.Reader): EloRating = EloRating(
      rating = r double "r",
      games = r int "g",
      k = r intO "k"
    )

    def writes(w: BSON.Writer, o: EloRating) = BSONDocument(
      "r" -> w.double(o.rating),
      "g" -> w.int(o.games),
      "k" -> o.k.map { k => BSONInteger(k) }
    )
  }
  private[team] implicit val MemberBSONHandler = Macros.handler[Member]

  private[team] implicit val RequestBSONHandler = Macros.handler[Request]
  private[team] implicit val InviteBSONHandler = Macros.handler[Invite]

  private[team] implicit val TagTypeBSONHandler = new BSONHandler[BSONString, Tag.Type] {
    def read(b: BSONString) = Tag.Type.byId get b.value err s"Invalid TagType. ${b.value}"
    def write(x: Tag.Type) = BSONString(x.id)
  }
  private[team] implicit val TagBSONHandler = Macros.handler[Tag]

  private[team] implicit val TeamCampusBSONHandler = Macros.handler[Campus]

  private[team] implicit val EnvPictureHandler = lila.db.dsl.bsonArrayToListHandler[String]
  private[team] implicit val CertificationStatusBSONHandler = new BSONHandler[BSONString, Certification.Status] {
    def read(b: BSONString): Certification.Status = Certification.Status(b.value)
    def write(c: Certification.Status) = BSONString(c.id)
  }
  private[team] implicit val CertificationBSONHandler = Macros.handler[Certification]
  private[team] implicit val RatingSettingBSONHandler = Macros.handler[RatingSetting]
  private[team] implicit val CoinSettingBSONHandler = Macros.handler[CoinSetting]
  private[team] implicit val TeamBSONHandler = Macros.handler[Team]

  private[team] implicit val TeamRatingTypBSONHandler = new BSONHandler[BSONString, TeamRating.Typ] {
    def read(b: BSONString) = TeamRating.Typ(b.value)
    def write(x: TeamRating.Typ) = BSONString(x.id)
  }
  private[team] implicit val TeamRatingMetaDataBSONHandler = Macros.handler[lila.team.TeamRatingMetaData]
  private[team] implicit val TeamRatingBSONHandler = Macros.handler[TeamRating]

  private[team] implicit val TeamCoinTypBSONHandler = new BSONHandler[BSONString, TeamCoin.Typ] {
    def read(b: BSONString) = TeamCoin.Typ(b.value)
    def write(x: TeamCoin.Typ) = BSONString(x.id)
  }
  private[team] implicit val TeamCoinMetaDataBSONHandler = Macros.handler[lila.team.TeamCoinMetaData]
  private[team] implicit val TeamCoinBSONHandler = Macros.handler[TeamCoin]

  private[team] implicit val ColorBSONHandler = new BSONHandler[BSONString, chess.Color] {
    def read(b: BSONString) = chess.Color(b.value) err s"invalid color ${b.value}"
    def write(c: chess.Color) = BSONString(c.name)
  }
  private[team] implicit val TestStatusBSONHandler = new BSONHandler[BSONString, TestTemplate.Status] {
    def read(b: BSONString) = TestTemplate.Status(b.value)
    def write(x: TestTemplate.Status) = BSONString(x.id)
  }
  private[team] implicit val TestRuleBSONHandler = new BSONHandler[BSONString, TestTemplate.Rule] {
    def read(b: BSONString) = TestTemplate.Rule(b.value)
    def write(x: TestTemplate.Rule) = BSONString(x.id)
  }
  private[team] implicit val TestStuStatusBSONHandler = new BSONHandler[BSONString, TestStudent.Status] {
    def read(b: BSONString) = TestStudent.Status(b.value)
    def write(x: TestStudent.Status) = BSONString(x.id)
  }
  private[team] implicit val MiniPuzzleHandler = Macros.handler[MiniPuzzle]
  private[team] implicit val MiniPuzzleNodeHandler = Macros.handler[PuzzleNode]
  private[team] implicit val MiniPuzzleMoveHandler = Macros.handler[PuzzleMove]
  private[team] implicit val MiniPuzzleMoveArrayHandler = bsonArrayToListHandler[PuzzleMove]
  private[team] implicit val MiniPuzzleResultHandler = Macros.handler[PuzzleResult]
  private[team] implicit val MiniPuzzleWithResultHandler = Macros.handler[MiniPuzzleWithResult]
  private[team] implicit val MiniPuzzleWithResultArrayHandler = bsonArrayToListHandler[MiniPuzzleWithResult]
  private[team] implicit val TestItemsHandler = Macros.handler[TestItems]
  private[team] implicit val TestStudentItemsHandler = Macros.handler[TestStudentItems]
  private[team] implicit val LightTestTemplateHandler = Macros.handler[LightTestTemplate]
  private[team] implicit val TestTemplateBSONHandler = Macros.handler[TestTemplate]
  private[team] implicit val TestInviteBSONHandler = Macros.handler[TestInvite]
  private[team] implicit val TestStudentBSONHandler = Macros.handler[TestStudent]

  private[team] implicit val ClockInSettingStatusBSONHandler = new BSONHandler[BSONString, ClockInSetting.Status] {
    def read(b: BSONString) = ClockInSetting.Status(b.value)
    def write(x: ClockInSetting.Status) = BSONString(x.id)
  }
  private[team] implicit val ClockInSettingPeriodBSONHandler = new BSONHandler[BSONString, ClockInSetting.Period] {
    def read(b: BSONString) = ClockInSetting.Period(b.value)
    def write(x: ClockInSetting.Period) = BSONString(x.id)
  }
  private[team] implicit val ClockInTaskStatusBSONHandler = new BSONHandler[BSONString, ClockInTask.Status] {
    def read(b: BSONString) = ClockInTask.Status(b.value)
    def write(x: ClockInTask.Status) = BSONString(x.id)
  }
  private[team] implicit val ClockInSettingBSONHandler = Macros.handler[ClockInSetting]
  private[team] implicit val ClockInTaskBSONHandler = Macros.handler[ClockInTask]
  private[team] implicit val ClockInMemberBSONHandler = Macros.handler[ClockInMember]

  private[team] implicit val TeamCooperateOrderBSONHandler = Macros.handler[TeamCooperateOrder]
  private[team] implicit val TeamCooperateOrderArrayHandler = bsonArrayToListHandler[TeamCooperateOrder]
  private[team] implicit val TeamCooperateBSONHandler = Macros.handler[TeamCooperate]

  private[team] implicit val TeamFederationVisibilityBSONHandler = new BSONHandler[BSONString, TeamFederation.Visibility] {
    def read(b: BSONString) = TeamFederation.Visibility(b.value)
    def write(x: TeamFederation.Visibility) = BSONString(x.id)
  }
  private[team] implicit val TeamFederationMemberRoleBSONHandler = new BSONHandler[BSONString, TeamFederationMember.Role] {
    def read(b: BSONString) = TeamFederationMember.Role(b.value)
    def write(x: TeamFederationMember.Role) = BSONString(x.id)
  }
  private[team] implicit val TeamFederationBSONHandler = Macros.handler[TeamFederation]
  private[team] implicit val TeamFederationMemberBSONHandler = Macros.handler[TeamFederationMember]
  private[team] implicit val TeamFederationRequestBSONHandler = Macros.handler[TeamFederationRequest]

}
