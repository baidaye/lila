package lila.team

import scala.math.BigDecimal.RoundingMode

/**
 * Approach:
 * P1: Probability of winning of player with rating2
 * P2: Probability of winning of player with rating1.
 *
 * P1 = (1.0 / (1.0 + pow(10, ((rating1 – rating2) / 400))));
 * P2 = (1.0 / (1.0 + pow(10, ((rating2 – rating1) / 400))));
 * Obviously, P1 + P2 = 1.
 *
 * The rating of player is updated using the formula given below :-
 *
 * rating1 = rating1 + K*(Actual Score – Expected score);
 *
 * https://blog.csdn.net/destruction666/article/details/7597348
 * https://www.geeksforgeeks.org/elo-rating-algorithm
 */
case class EloRating(rating: Double, games: Int, k: Option[Int] = None) {

  def intValue = rating.intValue

  /*
  // reference FIDE rule
  def k: Int = {
    if (games <= 30) 25
    else {
      if (rating < 2400) 15
      else 10
    }
  }
*/

  // Probability
  def p(opponentRating: Double): Double = {
    1.0 / (1.0 + math.pow(10, (opponentRating - rating) / 400))
  }
  // Actual Score
  def ac(win: Option[Boolean]): Double = win match {
    case None => 0.5D
    case Some(true) => 1.0D
    case Some(false) => 0.0D
  }

  def calc(opponentRating: Double, win: Option[Boolean], teamK: Int, min: Int): EloRating = {
    val oldRating = BigDecimal(rating).setScale(1, RoundingMode.DOWN).doubleValue()
    var newRating = oldRating
    // 和棋并且两个人同分就不计算了
    if (win.isDefined || oldRating != opponentRating) {
      val r = oldRating + (k | teamK) * (ac(win) - p(opponentRating))
      newRating = r
      newRating = BigDecimal(newRating).setScale(1, RoundingMode.DOWN).doubleValue()
      newRating = {
        val df = diff(newRating, oldRating)
        if (math.abs(df) < EloRating.minDiff) {
          if (df == 0) {
            win match {
              case None => oldRating
              case Some(true) => oldRating + EloRating.minDiff
              case Some(false) => oldRating - EloRating.minDiff
            }
          } else if (df < 0) {
            oldRating - EloRating.minDiff
          } else {
            oldRating + EloRating.minDiff
          }
        } else newRating
      }
      newRating = math.min(math.max(newRating, min), EloRating.max)
      newRating = BigDecimal(newRating).setScale(1, RoundingMode.DOWN).doubleValue()
    }
    copy(rating = newRating, games = games + 1)
  }

  private def diff(newRating: Double, oldRating: Double) =
    BigDecimal(newRating - oldRating).setScale(1, RoundingMode.DOWN).doubleValue()

}

object EloRating {

  val max = 3200
  val minDiff = 0.8
  val defaultRating = 1500
  val group = 25

  def default = EloRating(defaultRating, 0)

}
