package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import org.joda.time.DateTime

object TestTemplateRepo {

  private val coll = Env.current.colls.testTemplate

  import BSONHandlers.TestTemplateBSONHandler
  import BSONHandlers.TestStatusBSONHandler

  def byId(id: TestTemplate.ID): Fu[Option[TestTemplate]] =
    coll.byId(id)

  def byTeam(teamId: Team.ID): Fu[List[TestTemplate]] =
    coll.find(teamQuery(teamId)).list[TestTemplate]()

  def page(page: Int, teamId: Team.ID, status: List[TestTemplate.Status]): Fu[Paginator[TestTemplate]] = {
    val adapter = new Adapter[TestTemplate](
      collection = coll,
      selector = teamQuery(teamId) ++ $doc("status" $in status.map(_.id)),
      projection = $empty,
      sort = $doc("createdAt" -> -1)
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  def insert(testTlt: TestTemplate): Funit =
    coll.insert(testTlt).void

  def update(testTlt: TestTemplate): Funit =
    coll.update($id(testTlt.id), testTlt).void

  def setStatus(id: TestTemplate.ID, status: TestTemplate.Status): Funit =
    coll.update(
      $id(id),
      $set(
        "status" -> status,
        "updatedAt" -> DateTime.now
      )
    ).void

  def incMembers(id: TestTemplate.ID, inc: Int): Funit =
    coll.update(
      $id(id),
      $set("updatedAt" -> DateTime.now) ++ $inc("nbMembers" -> inc)
    ).void

  def teamQuery(teamId: Team.ID) = $doc("teamId" -> teamId)

}
