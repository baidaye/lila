package lila.team

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import ClockInTask._
import org.joda.time.format.DateTimeFormat

case class ClockInTask(
    _id: ID,
    teamId: String,
    templateId: String,
    settingId: String,
    name: String,
    index: Int,
    date: DateTime,
    status: Status,
    coinTeam: Option[String],
    coinRule: Option[Int],
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def isCreated = status == Status.Created

  def isCanceled = status == Status.Canceled

  def isCreator(userId: User.ID) = userId == createdBy

  def dateString = date.toString("MM月dd日")

  def sourceId = s"$id@${date.toString("yyyyMMdd")}"

  def sourceName(n: String) = s"$n，$name"

}

object ClockInTask {

  type ID = String

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "新建")
    case object Canceled extends Status("canceled", "取消")

    def all = List(Created, Canceled)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Status = all.find(_.id == id) err s"can not apply Status ${id}"
  }

  def make(
    name: String,
    teamId: String,
    templateId: String,
    settingId: String,
    index: Int,
    date: DateTime,
    status: String,
    coinTeam: Option[String],
    coinRule: Option[Int],
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    ClockInTask(
      _id = Random nextString 8,
      teamId = teamId,
      templateId = templateId,
      settingId = settingId,
      name = name,
      index = index,
      date = date,
      status = Status(status),
      coinTeam = coinTeam,
      coinRule = coinRule,
      createdAt = now,
      updatedAt = now,
      createdBy = createdBy,
      updatedBy = createdBy
    )
  }

  def parseSourseId(id: String) = {
    val arr = id.split("@")
    arr(0) -> DateTime.parse(arr(1), DateTimeFormat.forPattern("yyyyMMdd"))
  }

}
