package lila.team

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class TeamCoin(
    _id: String,
    userId: String,
    teamId: String,
    campusId: String,
    oldCoin: Int,
    newCoin: Int,
    diffCoin: Int,
    note: String,
    typ: TeamCoin.Typ,
    metaData: TeamCoinMetaData,
    createBy: User.ID,
    createAt: DateTime
) {

  def id = _id

  def noteOrDefault = if (note.isEmpty) "-" else note

}

object TeamCoin {

  def make(
    userId: String,
    teamId: String,
    campusId: String,
    oldCoin: Int,
    newCoin: Int,
    diffCoin: Int,
    note: String,
    typ: TeamCoin.Typ,
    metaData: TeamCoinMetaData,
    createBy: User.ID
  ) = TeamCoin(
    _id = Random nextString 8,
    userId = userId,
    teamId = teamId,
    campusId = campusId,
    oldCoin = oldCoin,
    newCoin = newCoin,
    diffCoin = diffCoin,
    note = note,
    typ = typ,
    metaData = metaData,
    createAt = DateTime.now,
    createBy = createBy
  )

  sealed class Typ(val id: String, val name: String)
  object Typ {
    case object TTask extends Typ("ttask", "任务")
    case object Homework extends Typ("homework", "课后练")
    case object Setting extends Typ("setting", "管理员设置")

    val all = List(TTask, Homework, Setting)
    val byId = all map { v => (v.id, v) } toMap
    def selects = all.map { v => (v.id, v.name) }
    def apply(id: String): Typ = byId.get(id) err s"Bad Typ $id"
  }

  sealed abstract class Operate(val id: String, val name: String, val cst: Int)
  object Operate {
    case object Plus extends Operate("plus", "加", 1)
    case object Minus extends Operate("minus", "减", -1)

    def all = List(Plus, Minus)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Operate = all.find(_.id == id) err s"can not apply Operate $id"
  }
}

case class TeamCoinMetaData(taskId: Option[String] = None, homeworkId: Option[String] = None)
