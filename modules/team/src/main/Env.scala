package lila.team

import akka.actor._
import com.typesafe.config.Config
import lila.common.{ AtMost, Every, MaxPerPage, ResilientScheduler }
import lila.game.actorApi.FinishGame
import lila.hub.{ Duct, DuctMap }
import lila.hub.actorApi.clazz.{ HomeworkDeadline, HomeworkFinish, HomeworkUpdateCoin }
import lila.hub.actorApi.member.MemberPointsChange
import lila.hub.actorApi.offContest.OffContestRoundResult
import lila.hub.actorApi.task.ChangeStatus
import lila.hub.actorApi.team.SetOwner
import lila.mod.ModlogApi
import lila.notify.NotifyApi
import lila.task.TTaskApi
import lila.task.TTaskTemplateApi
import scala.concurrent.duration._

final class Env(
    config: Config,
    rootConfig: Config,
    hub: lila.hub.Env,
    modLog: ModlogApi,
    notifyApi: NotifyApi,
    system: ActorSystem,
    asyncCache: lila.memo.AsyncCache.Builder,
    db: lila.db.Env,
    isOnline: lila.user.User.ID => Boolean,
    isPlaying: lila.user.User.ID => Boolean,
    taskApi: TTaskApi,
    taskTemplateApi: TTaskTemplateApi
) {

  private val settings = new {
    val CollectionTeam = config getString "collection.team"
    val CollectionCampus = config getString "collection.campus"
    val CollectionMember = config getString "collection.member"
    val CollectionRequest = config getString "collection.request"
    val CollectionInvite = config getString "collection.invite"
    val CollectionTag = config getString "collection.tag"
    val CollectionRating = config getString "collection.rating"
    val CollectionCoin = config getString "collection.coin"
    val CollectionTestTemplate = config getString "collection.test_template"
    val CollectionTestInvite = config getString "collection.test_invite"
    val CollectionTestStudent = config getString "collection.test_student"
    val CollectionClockInSetting = config getString "collection.clockin_setting"
    val CollectionClockInTask = config getString "collection.clockin_task"
    val CollectionClockInMember = config getString "collection.clockin_member"
    val CollectionCooperate = config getString "collection.cooperate"
    val CollectionFederation = config getString "collection.federation"
    val CollectionFederationMember = config getString "collection.federation_member"
    val CollectionFederationRequest = config getString "collection.federation_request"
    val CollectionImage = config getString "collection.image"
    val AdminUid = rootConfig getString "net.admin_uid"
    val PaginatorMaxPerPage = config getInt "paginator.max_per_page"
    val PaginatorMaxUserPerPage = config getInt "paginator.max_user_per_page"
    val AnimationDuration = config duration "animation.duration"
  }
  import settings._

  private[team] lazy val colls = new Colls(
    team = db(CollectionTeam),
    campus = db(CollectionCampus),
    request = db(CollectionRequest),
    invite = db(CollectionInvite),
    member = db(CollectionMember),
    tag = db(CollectionTag),
    rating = db(CollectionRating),
    coin = db(CollectionCoin),
    testTemplate = db(CollectionTestTemplate),
    testInvite = db(CollectionTestInvite),
    testStudent = db(CollectionTestStudent),
    clockInSetting = db(CollectionClockInSetting),
    clockInTask = db(CollectionClockInTask),
    clockInMember = db(CollectionClockInMember),
    cooperate = db(CollectionCooperate),
    federation = db(CollectionFederation),
    federationMember = db(CollectionFederationMember),
    federationRequest = db(CollectionFederationRequest)
  )

  lazy val forms = new DataForm(
    colls.team,
    captcherActor = hub.captcher,
    smsCaptcherActor = hub.smsCaptcher
  )

  lazy val memberStream = new TeamMemberStream(colls.member)(system)

  private lazy val imageColl = db(CollectionImage)

  private lazy val photographer = new lila.db.Photographer(imageColl, "team")

  lazy val api = new TeamApi(
    coll = colls,
    cached = cached,
    notifier = notifier,
    bus = system.lilaBus,
    indexer = hub.teamSearch,
    timeline = hub.timeline,
    modLog = modLog,
    photographer = photographer,
    contestActor = hub.contest,
    teamContestActor = hub.teamContest,
    markActor = hub.relation,
    adminUid = AdminUid,
    isOnline = isOnline,
    isPlaying = isPlaying
  )

  lazy val certificationApi = new CertificationApi(
    coll = colls,
    notifier = notifier,
    indexer = hub.teamSearch,
    adminUid = AdminUid,
    hub = hub
  )

  lazy val paginator = new PaginatorBuilder(
    coll = colls,
    teamApi = api,
    maxPerPage = MaxPerPage(100),
    maxUserPerPage = MaxPerPage(100)
  )

  lazy val memberSelector = new MemberSelector(colls)

  lazy val testJsonView = new TestJsonView(AnimationDuration)

  lazy val testApi = new TestApi(taskApi, system, sequencerMap, notifyApi)

  lazy val clockInApi = new ClockInApi(taskApi, taskTemplateApi, system)

  lazy val clockInJsonView = new ClockInJsonView()

  lazy val cli = new Cli(api, colls)

  lazy val cached = new Cached(asyncCache)(system)

  lazy val notifier = new Notifier(notifyApi = notifyApi)

  lazy val federationApi = new TeamFederationApi(notifier = notifier, photographer = photographer)

  lazy val sequencerMap = new DuctMap(
    mkDuct = _ => Duct.extra.lazyPromise(5.seconds.some)(system),
    accessTimeout = 10.minute
  )

  ResilientScheduler(
    every = Every(3 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 10 seconds
  ) { testApi.scheduledRemainsTime() }(system)

  ResilientScheduler(
    every = Every(5 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 15 seconds
  ) { testApi.scheduledFinish() }(system)

  ResilientScheduler(
    every = Every(1 minute),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 20 seconds
  ) { testApi.scheduledExpire() }(system)

  system.lilaBus.subscribeFun('shadowban, 'memberPointsChange, 'finishGame, 'offContestRoundResult, 'ttask, 'teamCoinChange, 'homeworkFinish, 'homeworkDeadline, 'homeworkUpdateCoin, 'teamSetOwner) {
    case lila.hub.actorApi.mod.Shadowban(userId, true) =>
      api deleteRequestsByUserId userId
    case MemberPointsChange(userId, typ, pointsDiff, orderId, teamId) => {
      TeamCooperateRepo.setOrder(userId, typ, pointsDiff, orderId, teamId)
    }
    case FinishGame(game, white, black) =>
      if (game.nonAi && game.accountable && game.hasClock) {
        api.updateOnlineRating(game, white, black)
      }
    case result: OffContestRoundResult => if (result.teamRated) {
      api.updateOfflineRating(result)
    }
    case ttaskStatus: ChangeStatus => {
      if (ttaskStatus.status == "finished" && ttaskStatus.coinRule.??(_ > 0)) {
        api.updateTTaskCoin(ttaskStatus)
      }
    }
    case homeworkFinish: HomeworkFinish => {
      if (homeworkFinish.coinRule.??(_ > 0)) {
        api.updateHomeworkCoin(homeworkFinish.userId, homeworkFinish.creator, homeworkFinish.homeworkId, homeworkFinish.homeworkName, homeworkFinish.coinDiff, homeworkFinish.oldCoinDiff)
      }
    }
    case homeworkDeadline: HomeworkDeadline => {
      if (homeworkDeadline.coinRule.??(_ > 0)) {
        api.updateHomeworkCoin(homeworkDeadline.userId, homeworkDeadline.creator, homeworkDeadline.homeworkId, homeworkDeadline.homeworkName, homeworkDeadline.coinDiff, homeworkDeadline.oldCoinDiff)
      }
    }
    case homeworkUpdateCoin: HomeworkUpdateCoin => {
      if (homeworkUpdateCoin.coinRule.??(_ >= 0)) {
        api.updateHomeworkCoin(homeworkUpdateCoin.userId, homeworkUpdateCoin.creator, homeworkUpdateCoin.homeworkId, homeworkUpdateCoin.homeworkName, homeworkUpdateCoin.coinDiff, homeworkUpdateCoin.oldCoinDiff)
      }
    }
    case lila.hub.actorApi.team.TeamCoinChangeFromHomework(teamId, userId, homeworkId, coinDiff, coinName, note) => {
      notifier.changeCoin(TeamCoin.Typ.Homework, teamId, userId, homeworkId.some, coinDiff, coinName, note)
    }
    case lila.hub.actorApi.team.TeamCoinChangeFromTTask(teamId, userId, taskId, coinDiff, coinName, note) => {
      notifier.changeCoin(TeamCoin.Typ.TTask, teamId, userId, taskId.some, coinDiff, coinName, note)
    }
    case lila.hub.actorApi.team.TeamCoinChangeFromSetting(teamId, userId, coinDiff, coinName, note) => {
      notifier.changeCoin(TeamCoin.Typ.Setting, teamId, userId, None, coinDiff, coinName, note)
    }
    case SetOwner(teamId, _, _, userId) => {
      federationApi.changeTeamOwner(teamId, userId)
    }
  }

  system.lilaBus.subscribeFuns(
    'isTeamManagerOf -> {
      case lila.hub.actorApi.team.IsManagerOfUser(managerId, userId, promise) =>
        promise completeWith api.isManagerOfUser(managerId, userId)
    },
    'isTeamManagerOfCampus -> {
      case lila.hub.actorApi.team.IsManagerOfCampus(managerId, campusId, promise) =>
        promise completeWith api.isManagerOfCampus(managerId, campusId)
    },
    'teamCooperator -> {
      case lila.hub.actorApi.team.TeamCooperator(teamId, promise) =>
        promise completeWith api.findTeamCooperator(teamId)
    }
  )
}

object Env {

  lazy val current = "team" boot new Env(
    config = lila.common.PlayApp loadConfig "team",
    rootConfig = lila.common.PlayApp.loadConfig,
    hub = lila.hub.Env.current,
    modLog = lila.mod.Env.current.logApi,
    notifyApi = lila.notify.Env.current.api,
    system = lila.common.PlayApp.system,
    asyncCache = lila.memo.Env.current.asyncCache,
    db = lila.db.Env.current,
    isOnline = lila.user.Env.current.isOnline,
    isPlaying = lila.relation.Env.current.online.isPlaying,
    taskApi = lila.task.Env.current.api,
    taskTemplateApi = lila.task.Env.current.tplApi
  )

}
