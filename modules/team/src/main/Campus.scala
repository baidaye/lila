package lila.team

import lila.user.User
import org.joda.time.DateTime

case class Campus(
    _id: Campus.ID,
    no: Int,
    team: String,
    name: String,
    sort: Int,
    admin: Option[User.ID],
    coach: Option[Set[User.ID]],
    addr: Option[String],
    intro: Option[String],
    nbMembers: Int,
    clazzIds: List[String],
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

  def deletable = no > 1

  def hasAdmin = admin.isDefined

  def isAdmin(userId: String) = admin.contains(userId)

  def coachIgnoreEmpty = coach | Set.empty[User.ID]

  def containsCoach(userId: String) = coach.??(_.contains(userId))

}

object Campus {

  type ID = String

  val defaultName = "主校区"

  def make(
    no: Int,
    team: String,
    name: String,
    sort: Int,
    admin: Option[User.ID],
    coach: Option[Set[User.ID]],
    addr: Option[String],
    intro: Option[String],
    createBy: String
  ): Campus = new Campus(
    _id = makeId(team, no),
    no = no,
    team = team,
    name = name,
    sort = sort,
    admin = admin,
    coach = coach,
    addr = addr,
    intro = intro,
    nbMembers = 1,
    clazzIds = Nil,
    createAt = DateTime.now,
    createBy = createBy
  )

  def defaultId(team: String) = makeId(team, 1)

  def makeId(team: String, no: Int) = team + "@" + no

  def toTeamId(campusId: String) = campusId.split("@")(0)

}

case class CampusWithTeam(campus: Campus, team: Team) {

  def teamId = team.id
  def campusId = campus.id
  def fullName = team.name + " - " + campus.name
}
