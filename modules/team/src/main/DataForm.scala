package lila.team

import akka.actor.ActorSelection
import lila.common.{ Region, SmsTemplate }
import play.api.data._
import play.api.data.Forms._
import lila.common.Form.{ ISODate, stringIn }
import lila.db.dsl._
import lila.security.{ Granter, SmsCaptcher }
import lila.user.{ FormSelect, User, UserRepo }
import play.api.data.validation.Constraints
import scala.math.BigDecimal.RoundingMode
import org.joda.time.DateTime
import scala.concurrent.duration._

case class DataForm(
    teamColl: Coll,
    val captcherActor: akka.actor.ActorSelection,
    val smsCaptcherActor: akka.actor.ActorSelection
) extends lila.hub.CaptchedForm with lila.hub.SmsCaptchedForm {

  override def captcher: ActorSelection = captcherActor
  override def smsCaptcha: ActorSelection = smsCaptcherActor

  import DataForm._

  object Fields {
    val name = "name" -> text(minLength = 3, maxLength = 20)
    val province = "province" -> text.verifying(Region.Province.keySet contains _)
    val city = "city" -> text.verifying(Region.City.keySet contains _)
    val description = "description" -> text(minLength = 10, maxLength = 1000)
    val logo = "logo" -> optional(text(minLength = 5, maxLength = 150))
    val envPicture = "envPicture" -> optional(list(text(minLength = 5, maxLength = 150)))
    val open = "open" -> number
    val tagTip = "tagTip" -> number
    val requestTagTip = "requestTagTip" -> number
    val gameId = "gameId" -> text
    val move = "move" -> text
  }

  object team {

    import DataForm.teamData._

    def createWithCaptcha = withCaptcha(create)

    val create = Form(mapping(
      Fields.name,
      Fields.province,
      Fields.city,
      Fields.description,
      Fields.open,
      Fields.gameId,
      Fields.move
    )(TeamSetup.apply)(TeamSetup.unapply)
      .verifying("当前俱乐部已经存在", d => !lila.team.TeamRepo.teamExists(d).awaitSeconds(2))
      .verifying(captchaFailMessage, validateCaptcha _))

    def basicSetting(team: Team) = Form(mapping(
      Fields.open,
      Fields.tagTip,
      Fields.requestTagTip
    )(BasicTeamSetting.apply)(BasicTeamSetting.unapply)) fill BasicTeamSetting(
      open = if (team.open) 1 else 0,
      tagTip = if (team.tagTip) 1 else 0,
      requestTagTip = if (team.requestTagTip) 1 else 0
    )

    def ratingSetting(team: Team) = Form(mapping(
      "ratingSetting" -> mapping(
        "open" -> boolean,
        "defaultRating" -> number(min = team.ratingSettingOrDefault.minRating, max = RatingSetting.max),
        "minRating" -> number(min = 0, max = 600),
        "k" -> number(min = 10, max = 40),
        "coachSupport" -> boolean,
        "turns" -> number(min = 0, max = 500),
        "minutes" -> number(min = 0, max = 240)
      )(RatingSetting.apply)(RatingSetting.unapply)
    )(RatingTeamSetting.apply)(RatingTeamSetting.unapply)) fill RatingTeamSetting(
      ratingSetting = team.ratingSettingOrDefault
    )

    def coinSetting(team: Team) = Form(mapping(
      "coinSetting" -> mapping(
        "open" -> boolean,
        "name" -> nonEmptyText(minLength = 1, maxLength = 4),
        "singleVal" -> number(min = 1, max = CoinSetting.maxSingleVal)
      )(CoinSetting.apply)(CoinSetting.unapply)
    )(CoinTeamSetting.apply)(CoinTeamSetting.unapply)) fill CoinTeamSetting(
      coinSetting = team.coinSettingOrDefault
    )

    def edit(team: Team) = Form(mapping(
      Fields.name,
      Fields.province,
      Fields.city,
      Fields.description,
      Fields.logo,
      Fields.envPicture
    )(TeamEdit.apply)(TeamEdit.unapply)) fill TeamEdit(
      name = team.name,
      province = team.province,
      city = team.city,
      description = team.description,
      logo = team.logo,
      envPicture = team.envPicture
    )

    val clazzTable = Form(tuple(
      "campus" -> optional(nonEmptyText),
      "coach" -> optional(lila.user.DataForm.historicalUsernameField)
    ))

  }

  object campus {

    import DataForm.campusData._

    val campus = Form(mapping(
      "name" -> nonEmptyText(minLength = 2, maxLength = 10),
      "sort" -> number(min = 1, max = 10000),
      "admin" -> optional(lila.user.DataForm.historicalUsernameField),
      "addr" -> optional(nonEmptyText(minLength = 2, maxLength = 50)),
      "intro" -> optional(nonEmptyText(minLength = 2, maxLength = 50))
    )(CampusData.apply)(CampusData.unapply))

    def campusOf(c: Campus) = campus.fill(CampusData(c.name, c.sort, c.admin, c.addr, c.intro))

  }

  object tag {

    import DataForm.tagData._

    val tagAdd = Form(mapping(
      "typ" -> nonEmptyText.verifying("非法类型", Tag.Type.keySet.contains _),
      "label" -> nonEmptyText(minLength = 1, maxLength = 10),
      "value" -> optional(nonEmptyText),
      "required" -> optional(boolean),
      "request" -> optional(boolean),
      "visitable" -> optional(boolean),
      "sort" -> optional(number(min = 1, max = 100))
    )(TagAdd.apply)(TagAdd.unapply)
      .verifying("当前类型可选值必填", _.mustHaveValue))

    def tagEditOf(tag: Tag) =
      tagEdit fill TagEdit(tag.typ.id, tag.label, tag.value, tag.required, tag.request, tag.visitable, tag.sort)

    def tagEdit = Form(mapping(
      "typ" -> nonEmptyText.verifying("非法类型", Tag.Type.keySet.contains _),
      "label" -> nonEmptyText(minLength = 1, maxLength = 10),
      "value" -> optional(nonEmptyText),
      "required" -> optional(boolean),
      "request" -> optional(boolean),
      "visitable" -> optional(boolean),
      "sort" -> optional(number(min = 1, max = 100))
    )(TagEdit.apply)(TagEdit.unapply)
      .verifying("当前类型可选值必填", _.mustHaveValue))

  }

  object cert {

    import DataForm.certData._

    def isCertifiedTeamOrCoach(userId: User.ID): Fu[Boolean] =
      UserRepo.byId(userId).map {
        case None => false
        case Some(user) => user.enabled && (Granter(_.Team)(user) || Granter(_.Coach)(user))
      }

    def certificationForm(user: lila.user.User) = Form(mapping(
      "leader" -> nonEmptyText(minLength = 2, maxLength = 20),
      "businessLicense" -> nonEmptyText(minLength = 5, maxLength = 150),
      "members" -> nonEmptyText.verifying(Constraints.pattern(regex = """^(?!0)(?:[0-9]{1,4}|10000)$""".r, error = "请填写1-10000之间的数字")),
      "org" -> nonEmptyText(minLength = 2, maxLength = 20),
      "addr" -> nonEmptyText(minLength = 2, maxLength = 20),
      "cooperator" -> optional(lila.user.DataForm.historicalUsernameField.verifying("必须是“认证教练”或“认证俱乐部”", d => isCertifiedTeamOrCoach(d).awaitSeconds(2))),
      "message" -> optional(nonEmptyText(minLength = 2, maxLength = 1000)),
      SmsCaptcher.form.cellphone2,
      SmsCaptcher.form.template,
      SmsCaptcher.form.code
    )(CertificationData.apply)(CertificationData.unapply)
      .verifying(smsCaptchaFailMessage, validSmsCaptcha(_) awaitSeconds 3))

    def certificationOf(user: lila.user.User, team: Team) = team.certification.fold {
      certificationForm(user).fill(CertificationData(
        leader = "",
        businessLicense = "",
        members = "",
        org = "",
        addr = "",
        cooperator = None,
        message = None,
        cellphone = "",
        template = SmsTemplate.Confirm.code,
        code = ""
      ))
    } { c =>
      certificationForm(user).fill(CertificationData(
        leader = c.leader,
        businessLicense = c.businessLicense,
        members = c.members.toString,
        org = c.org,
        addr = c.addr,
        cooperator = c.cooperator,
        message = c.message,
        cellphone = c.leaderContact,
        template = SmsTemplate.Confirm.code,
        code = ""
      ))
    }

    val certificationProcessForm = Form(tuple(
      "process" -> nonEmptyText,
      "comments" -> optional(nonEmptyText)
    ))

  }

  object member {

    import DataForm.memberData._

    def memberSearch = Form(mapping(
      "username" -> optional(nonEmptyText(minLength = 1, maxLength = 20)),
      "campus" -> optional(nonEmptyText),
      "role" -> optional(text.verifying(Member.Role.list.map(_._1) contains _)),
      "name" -> optional(text(minLength = 2, maxLength = 20)),
      "sex" -> optional(text.verifying(FormSelect.Sex.keySet contains _)),
      "age" -> optional(number(min = 0, max = 100)),
      "level" -> optional(text.verifying(FormSelect.Level.keySet contains _)),
      "clazzId" -> optional(nonEmptyText),
      "memberLevel" -> optional(nonEmptyText),
      "teamRatingMin" -> optional(number(min = 0, max = 3200)),
      "teamRatingMax" -> optional(number(min = 0, max = 3200)),
      "fields" -> list(mapping(
        "fieldName" -> nonEmptyText,
        "fieldValue" -> optional(nonEmptyText)
      )(MemberTag.apply)(MemberTag.unapply)),
      "rangeFields" -> list(mapping(
        "fieldName" -> nonEmptyText,
        "min" -> optional(nonEmptyText),
        "max" -> optional(nonEmptyText)
      )(RangeMemberTag.apply)(RangeMemberTag.unapply)),
      "sortField" -> optional(nonEmptyText),
      "sortType" -> optional(nonEmptyText)
    )(MemberSearch.apply)(MemberSearch.unapply))

    def memberAdd(team: Team) = Form(mapping(
      "mark" -> optional(text(minLength = 1, maxLength = 20)),
      "campus" -> nonEmptyText,
      "rating" -> optional(number(min = team.ratingSettingOrDefault.minRating, EloRating.max)),
      "fields" -> list(mapping(
        "fieldName" -> nonEmptyText,
        "fieldValue" -> optional(nonEmptyText)
      )(MemberTag.apply)(MemberTag.unapply))
    )(MemberAdd.apply)(MemberAdd.unapply))

    def memberEditOf(team: Team, mwu: MemberWithUser, markOption: Option[String]) =
      memberEdit(mwu.user) fill MemberEdit(
        mark = markOption,
        campus = mwu.member.campus,
        fields = mwu.member.tagsIfEmpty.toList.map {
          case (_, t) => t
        }
      )

    def memberEdit(u: lila.user.User) = Form(mapping(
      "mark" -> optional(text(minLength = 1, maxLength = 10)),
      "campus" -> nonEmptyText,
      "fields" -> list(mapping(
        "fieldName" -> nonEmptyText,
        "fieldValue" -> optional(nonEmptyText)
      )(MemberTag.apply)(MemberTag.unapply))
    )(MemberEdit.apply)(MemberEdit.unapply))

    def request(joined: Option[String], request: Option[String]) = Form(mapping(
      "message" -> text(minLength = 10, maxLength = 1000),
      "campus" -> optional(nonEmptyText),
      "fields" -> optional(list(mapping(
        "fieldName" -> nonEmptyText,
        "fieldValue" -> optional(nonEmptyText)
      )(MemberTag.apply)(MemberTag.unapply))),
      Fields.gameId,
      Fields.move
    )(RequestSetup.apply)(RequestSetup.unapply)
      .verifying(s"您已经是【${joined | ""}】俱乐部成员，退出原俱乐部，才能加入其他认证俱乐部", _ => joined.isEmpty)
      .verifying(s"您已经申请加入【${request | ""}】，正在等待管理员确认，此期间不能加入其他俱乐部", _ => request.isEmpty)
      .verifying(captchaFailMessage, validateCaptcha _))

    def requestOf =
      request(none, none) fill RequestSetup(
        message = "您好，我想加入俱乐部！",
        campus = none,
        fields = none,
        gameId = "",
        move = ""
      )

    val processRequest = Form(tuple(
      "process" -> nonEmptyText,
      "url" -> nonEmptyText
    ))

    val selectMember = Form(single(
      "userId" -> lila.user.DataForm.historicalUsernameField
    ))

    val kickForm = Form(single(
      "userId" -> lila.user.DataForm.historicalUsernameField
    ))

    val inviteForm = Form(single(
      "username" -> lila.user.DataForm.historicalUsernameField
    ))

    val inviteProcessForm = Form(single(
      "process" -> nonEmptyText
    ))

  }

  object rating {

    import DataForm.ratingData._

    def ratingEditOf(team: Team, mwu: MemberWithUser) = ratingEdit fill RatingEdit(
      k = mwu.member.rating.fold(team.ratingSettingOrDefault.k) { _.k | team.ratingSettingOrDefault.k },
      rating = BigDecimal(mwu.member.rating.map(_.rating) | team.ratingSettingOrDefault.defaultRating.toDouble).setScale(1, RoundingMode.DOWN),
      note = none
    )

    def ratingEdit = Form(mapping(
      "k" -> number(min = 10, max = 40),
      "rating" -> bigDecimal(precision = 5, scale = 1),
      "note" -> optional(nonEmptyText(minLength = 2, maxLength = 50))
    )(RatingEdit.apply)(RatingEdit.unapply))

    val ratingDistribution = Form(tuple(
      "q" -> optional(lila.user.DataForm.historicalUsernameField),
      "clazzId" -> optional(nonEmptyText)
    ))

  }

  object coin {
    import DataForm.coinData._

    def coinEditOf(team: Team, mwu: MemberWithUser) =
      coinEdit(team) fill CoinEdit(
        operate = "plus",
        coin = 1,
        note = none
      )

    def coinEdit(team: Team) = Form(mapping(
      "operate" -> nonEmptyText,
      "coin" -> number(min = 1, max = team.coinSettingOrDefault.singleVal),
      "note" -> optional(nonEmptyText(minLength = 2, maxLength = 50))
    )(CoinEdit.apply)(CoinEdit.unapply))

    val coinLogSearch = Form(mapping(
      "dateMin" -> optional(ISODate.isoDate),
      "dateMax" -> optional(ISODate.isoDate),
      "campus" -> optional(nonEmptyText),
      "member" -> optional(lila.user.DataForm.historicalUsernameField),
      "operator" -> optional(lila.user.DataForm.historicalUsernameField),
      "typ" -> optional(stringIn(TeamCoin.Typ.selects))
    )(CoinLogSearch.apply)(CoinLogSearch.unapply))

    val coinMemberSearch = Form(tuple(
      "q" -> optional(lila.user.DataForm.historicalUsernameField),
      "campus" -> optional(nonEmptyText)
    ))

  }

  object coach {

    import DataForm.coachData._

    val addCoach = Form(single(
      "coach" -> lila.user.DataForm.historicalUsernameField
    ))

    val addCoachCampus = Form(single(
      "campusId" -> nonEmptyText
    ))

    val removeCoachCampus = Form(single(
      "campusId" -> nonEmptyText
    ))

    val addCoachStu = Form(single(
      "members" -> list(lila.user.DataForm.historicalUsernameField)
    ))

    val removeCoachStu = Form(single(
      "userId" -> lila.user.DataForm.historicalUsernameField
    ))

    val removeCoachStudy = Form(single(
      "studyId" -> nonEmptyText(minLength = 8, maxLength = 8)
    ))

    val coachStudyRole = Form(mapping(
      "studyId" -> nonEmptyText(minLength = 8, maxLength = 8),
      "role" -> nonEmptyText(minLength = 1, maxLength = 1)
    )(StudyRole.apply)(StudyRole.unapply))

    val removeCoachCapsule = Form(single(
      "capsuleId" -> nonEmptyText(minLength = 8, maxLength = 8)
    ))

    val coachCapsuleRole = Form(mapping(
      "capsuleId" -> nonEmptyText(minLength = 8, maxLength = 8),
      "role" -> nonEmptyText(minLength = 1, maxLength = 1)
    )(CapsuleRole.apply)(CapsuleRole.unapply))

    val removeCoachOpeningdb = Form(single(
      "openingdbId" -> nonEmptyText(minLength = 8, maxLength = 8)
    ))

    val coachOpeningdbRole = Form(mapping(
      "openingdbId" -> nonEmptyText(minLength = 8, maxLength = 8),
      "role" -> nonEmptyText(minLength = 1, maxLength = 1)
    )(OpeningdbRole.apply)(OpeningdbRole.unapply))

  }

  object memberAccount {
    import DataForm.memberAccountData._
    def memberAccountSearch = Form(mapping(
      "username" -> optional(nonEmptyText(minLength = 1, maxLength = 20)),
      "level" -> optional(stringIn(lila.user.MemberLevel.allChoices)),
      "enabled" -> optional(boolean)
    )(MemberAccountSearch.apply)(MemberAccountSearch.unapply))

    def memberAccountAdd(team: Team) = Form(mapping(
      "usernames" -> nonEmptyText(minLength = 2, maxLength = 20 * MemberAccountMaxSize),
      "password" -> lila.user.DataForm.password,
      "cardLevel" -> stringIn(lila.user.MemberLevel.allChoices),
      "days" -> stringIn(accountDaysChoices)
    )(MemberAccountAdd.apply)(MemberAccountAdd.unapply))

    def memberAccountAddOf(team: Team) =
      memberAccountAdd(team) fill MemberAccountAdd(
        usernames = "",
        password = "123456",
        cardLevel = lila.user.MemberLevel.Gold.code,
        days = "1year"
      )

    val memberAccountResetPassword = Form(single(
      "password" -> lila.user.DataForm.password
    ))

    val memberAccountUseCard = Form(mapping(
      "cardLevel" -> stringIn(lila.user.MemberLevel.choices),
      "days" -> stringIn(accountDaysChoices)
    )(MemberAccountUseCard.apply)(MemberAccountUseCard.unapply))

    def memberAccountUseCardOf =
      memberAccountUseCard fill MemberAccountUseCard(lila.user.MemberLevel.Gold.code, days = "1year")
  }

  object test {

    import DataForm.testData._

    val MaxQ = 200

    def createOrUpdateOf(tpl: TestTemplate, fetchPuzzleCount: List[String] => Fu[Int]) =
      createOrUpdate(fetchPuzzleCount) fill
        CreateOrUpdateData(
          name = tpl.name,
          limitTime = tpl.limitTime,
          nbQ = tpl.nbQ,
          passedQ = tpl.passedQ,
          rule = tpl.rule.id,
          maxRetry = tpl.maxRetry,
          desc = tpl.desc,
          items = tpl.items
        )

    def createOrUpdate(fetchPuzzleCount: List[String] => Fu[Int]) = Form(mapping(
      "name" -> nonEmptyText(minLength = 2, maxLength = 30),
      "limitTime" -> number(min = 1, max = 150),
      "nbQ" -> number(min = 0, max = MaxQ),
      "passedQ" -> number(min = 0, max = MaxQ),
      "rule" -> stringIn(TestTemplate.Rule.choices),
      "maxRetry" -> number(min = 0, max = 10),
      "desc" -> optional(nonEmptyText(minLength = 0, maxLength = 500)),
      "items" -> mapping(
        "capsules" -> optional(list(nonEmptyText))
      )(TestItems.apply)(TestItems.unapply)
    )(CreateOrUpdateData.apply)(CreateOrUpdateData.unapply)
      .verifying("战术题列表数量必须大于0并且小于50", _.validItemNb())
      .verifying("达标数量不能大于题目数量", _.validPassedQ())
      .verifying("测试题目数量不能大于题库内题目数量", _.validNbQ(fetchPuzzleCount)))

    val testStudentSearch = Form(mapping(
      "username" -> optional(nonEmptyText),
      "status" -> optional(nonEmptyText),
      "passed" -> optional(boolean)
    )(TestStudentSearch.apply)(TestStudentSearch.unapply))

    val round = Form(mapping(
      "id" -> nonEmptyText,
      "index" -> number(min = 0, max = MaxQ),
      "win" -> boolean,
      "moves" -> list(mapping(
        "index" -> number(min = 1, max = 500),
        "white" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText,
          "lastMove" -> optional(nonEmptyText)
        )(PuzzleNode.apply)(PuzzleNode.unapply)),
        "black" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText,
          "lastMove" -> optional(nonEmptyText)
        )(PuzzleNode.apply)(PuzzleNode.unapply))
      )(PuzzleMove.apply)(PuzzleMove.unapply))
    )(TestRoundData.apply)(TestRoundData.unapply))

    val redo = Form(mapping(
      "id" -> nonEmptyText,
      "index" -> number(min = 0, max = 500)
    )(TestRoundPuzzle.apply)(TestRoundPuzzle.unapply))

    val testInvite = Form(mapping(
      "deadline" -> lila.common.Form.futureDateTime,
      "members" -> list(lila.user.DataForm.historicalUsernameField)
    )(TestInviteData.apply)(TestInviteData.unapply))

    val remark = Form(single(
      "data" -> text(minLength = 0, maxLength = 100)
    ))
  }

  object cooperate {

    val search = Form(single(
      "typ" -> optional(boolean)
    ))

    def setCooperatorForm = Form(single(
      "cooperator" -> lila.user.DataForm.historicalUsernameField.verifying("必须是“认证教练”或“认证俱乐部”", d => cert.isCertifiedTeamOrCoach(d).awaitSeconds(2))
    ))
  }

  object federation {

    import DataForm.federationData._

    val create = Form(mapping(
      "name" -> nonEmptyText(minLength = 2, maxLength = 20),
      "description" -> nonEmptyText(minLength = 10, maxLength = 1000),
      "open" -> boolean,
      "visibility" -> stringIn(TeamFederation.Visibility.selects)
    )(TeamFederationCreate.apply)(TeamFederationCreate.unapply))

    def createOf = create fill TeamFederationCreate("", "", false, TeamFederation.Visibility.All.id)

    val setting = Form(mapping(
      "open" -> boolean,
      "visibility" -> stringIn(TeamFederation.Visibility.selects)
    )(TeamFederationSetting.apply)(TeamFederationSetting.unapply))

    def settingOf(federation: TeamFederation) =
      setting fill TeamFederationSetting(federation.open, federation.visibility.id)

    val update = Form(mapping(
      "description" -> nonEmptyText(minLength = 10, maxLength = 1000),
      "logo" -> optional(text(minLength = 5, maxLength = 150))
    )(TeamFederationUpdate.apply)(TeamFederationUpdate.unapply))

    def updateOf(federation: TeamFederation) =
      update fill TeamFederationUpdate(federation.description, federation.logo)

    def join = Form(single(
      "message" -> nonEmptyText(minLength = 2, maxLength = 100)
    ))

    def joinProcess = Form(single(
      "process" -> text.verifying(p => Set("accept", "decline").contains(p))
    ))

  }
}

object DataForm {

  object teamData {

    case class TeamSetup(
        name: String,
        province: String,
        city: String,
        description: String,
        open: Int,
        gameId: String,
        move: String
    ) {

      def isOpen = open == 1

      def trim = copy(
        name = name.trim,
        province = province,
        city = city,
        description = description.trim
      )
    }

    case class BasicTeamSetting(open: Int, tagTip: Int, requestTagTip: Int) {
      def isOpen = open == 1
      def showTagTip = tagTip == 1
      def showRequestTagTip = requestTagTip == 1
    }

    case class RatingTeamSetting(ratingSetting: RatingSetting)
    object RatingTeamSetting {
      val kList = List(10, 15, 20, 30, 40).map(v => v -> v.toString)
    }

    case class CoinTeamSetting(coinSetting: CoinSetting)

    case class TeamEdit(
        name: String,
        province: String,
        city: String,
        description: String,
        logo: Option[String],
        envPicture: Option[List[String]]
    ) {

      def trim = copy(
        province = province,
        city = city,
        description = description.trim
      )
    }

  }

  object campusData {

    case class CampusData(name: String, sort: Int, admin: Option[User.ID], addr: Option[String], intro: Option[String])

  }

  object tagData {

    case class TagAdd(
        typ: String,
        label: String,
        value: Option[String],
        required: Option[Boolean],
        request: Option[Boolean],
        visitable: Option[Boolean],
        sort: Option[Int]
    ) {
      def realType = Tag.Type(typ)
      def mustHaveValue = value.isDefined == realType.hasValue
    }

    case class TagEdit(
        typ: String,
        label: String,
        value: Option[String],
        required: Option[Boolean],
        request: Option[Boolean],
        visitable: Option[Boolean],
        sort: Option[Int]
    ) {
      def realType = Tag.Type(typ)
      def mustHaveValue = value.isDefined == realType.hasValue
    }

  }

  object certData {

    case class CertificationData(
        leader: String,
        businessLicense: String,
        members: String,
        org: String,
        addr: String,
        cooperator: Option[String],
        message: Option[String],
        cellphone: String,
        template: String,
        code: String
    )

  }

  object memberData {

    case class MemberSearch(
        username: Option[String] = None,
        campus: Option[String] = None,
        role: Option[String] = None,
        name: Option[String] = None,
        sex: Option[String] = None,
        age: Option[Int] = None,
        level: Option[String] = None,
        clazzId: Option[String] = None,
        memberLevel: Option[String] = None,
        teamRatingMin: Option[Int] = None,
        teamRatingMax: Option[Int] = None,
        fields: List[MemberTag] = List.empty[MemberTag],
        rangeFields: List[RangeMemberTag] = List.empty[RangeMemberTag],
        sortField: Option[String] = None,
        sortType: Option[String] = None
    )

    object MemberSearch {
      def empty = MemberSearch()
    }

    case class MemberAdd(
        mark: Option[String],
        campus: String,
        rating: Option[Int],
        fields: List[MemberTag]
    )

    case class MemberEdit(
        mark: Option[String],
        campus: String,
        fields: List[MemberTag]
    )

    case class RequestSetup(
        message: String,
        campus: Option[String],
        fields: Option[List[MemberTag]],
        gameId: String,
        move: String
    )

  }

  object ratingData {

    case class RatingEdit(k: Int, rating: BigDecimal, note: Option[String])

  }

  object coinData {
    case class CoinEdit(operate: String, coin: Int, note: Option[String]) {
      def realOperate = TeamCoin.Operate(operate)
    }

    case class CoinLogSearch(dateMin: Option[DateTime], dateMax: Option[DateTime], campus: Option[String], member: Option[String], operator: Option[String], typ: Option[String])
  }

  object memberAccountData {

    case class MemberAccountSearch(username: Option[String] = None, level: Option[String] = None, enabled: Option[Boolean])

    case class MemberAccountAdd(usernames: String, password: String, cardLevel: String, days: String) {

      def realUsernames =
        usernames
          .split("""\r\n""")
          .toList
          .map(_.trim)
          .filter(_.nonEmpty)
          .distinct

      def nofreeLevel = lila.user.MemberLevel.nofree.contains(lila.user.MemberLevel(cardLevel))
    }

    case class MemberAccountUseCard(cardLevel: String, days: String)

    val accountDaysChoices = List("1month" -> "1个月", "3month" -> "3个月", "6month" -> "6个月", "1year" -> "1年")

    val MemberAccountMaxSize = 20
  }

  object coachData {

    case class StudyRole(studyId: String, role: String)

    case class CapsuleRole(capsuleId: String, role: String)

    case class OpeningdbRole(openingdbId: String, role: String)
  }

  object testData {

    def booleanChoices = Seq("" -> "全部", true -> "是", false -> "否")
    def testStuStatus = Seq("" -> "全部") ++ TestStudent.Status.all.map(d => d.id -> d.name)

    case class CreateOrUpdateData(
        name: String,
        limitTime: Int,
        nbQ: Int,
        passedQ: Int,
        rule: String,
        maxRetry: Int,
        desc: Option[String],
        items: TestItems
    ) {

      def validNbQ(fetchPuzzleCount: List[String] => Fu[Int]): Boolean =
        fetchPuzzleCount(items.capsuleIds) map { nbQ <= _ } awaitSeconds (3)

      def validPassedQ(): Boolean = passedQ <= nbQ

      def validItemNb(): Boolean = items.capsuleIds.nonEmpty && items.capsuleIds.length <= 50

    }

    case class TestStudentSearch(
        username: Option[String] = None,
        status: Option[String] = None,
        passed: Option[Boolean] = None
    )

    case class TestInviteData(deadline: DateTime, members: List[User.ID])

    case class TestRoundPuzzle(id: String, index: Int)
    case class TestRoundData(id: String, index: Int, win: Boolean, moves: List[PuzzleMove])

  }

  object federationData {
    case class TeamFederationCreate(
        name: String,
        description: String,
        open: Boolean,
        visibility: String
    )

    case class TeamFederationSetting(
        open: Boolean,
        visibility: String
    )

    case class TeamFederationUpdate(
        description: String,
        logo: Option[String]
    )

  }

}

