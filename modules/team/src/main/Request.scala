package lila.team

import org.joda.time.DateTime

import lila.user.User

case class Request(
    _id: String,
    team: String,
    user: String,
    message: String,
    campus: Option[String],
    tags: Option[MemberTags],
    date: DateTime
) {

  def id = _id

  def requiredFieldValues(tags: List[Tag]) = tags.filter(_.isRequired).sortBy(_.sort | 1).map { tag =>
    tagFieldValue(tag.field)
  }.filter(_.isDefined).map(_.get).mkString(", ")

  def unRequiredFieldValues(tags: List[Tag]) = tags.filterNot(_.isRequired).sortBy(_.sort | 1).map { tag =>
    tagFieldValue(tag.field)
  }.filter(_.isDefined).map(_.get).mkString(", ")

  def tagFieldValue(field: String) = tags.??(_.fieldValue(field))
}

object Request {

  def makeId(team: String, user: String) = user + "@" + team

  def make(
    team: String,
    user: String,
    message: String,
    campus: Option[String],
    tags: Option[MemberTags]
  ): Request = new Request(
    _id = makeId(team, user),
    user = user,
    team = team,
    message = message.trim,
    campus = campus,
    tags = tags,
    date = DateTime.now
  )
}

case class RequestWithUser(request: Request, user: User, team: Team, campus: Option[Campus], tags: List[Tag]) {
  def id = request.id
  def message = request.message
  def date = request.date
  def teamId = team.id
}

sealed trait Requesting
case class Joined(team: Team) extends Requesting
case class Motivate(team: Team) extends Requesting
