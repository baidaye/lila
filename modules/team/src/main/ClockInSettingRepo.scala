package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import lila.task.TTaskTemplateRepo
import lila.user.User

object ClockInSettingRepo {

  // dirty
  private val coll = Env.current.colls.clockInSetting

  import BSONHandlers.ClockInSettingBSONHandler

  def byId(id: ClockInSetting.ID): Fu[Option[ClockInSetting]] = coll.byId[ClockInSetting](id)

  def findByPage(page: Int, teamId: String, statusArray: List[ClockInSetting.Status], status: Option[ClockInSetting.Status]): Fu[Paginator[ClockInSetting]] =
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = $doc(
          "teamId" -> teamId,
          "status" $in statusArray.map(_.id)
        ) ++ status.?? { s => $doc("status" -> s.id) },
        projection = $empty,
        sort = $sort desc "createdAt"
      ),
      page,
      lila.common.MaxPerPage(10)
    )

  def findWithTask(teamId: String): Fu[List[ClockInSetting.WithTask]] =
    coll.find($doc(
      "teamId" -> teamId,
      "status" -> ClockInSetting.Status.Published.id
    ))
      .sort($sort desc "createdAt")
      .list(100)
      .flatMap { settings =>
        TTaskTemplateRepo.byOrderedIds(settings.map(_.templateId)) map { tpls =>
          settings zip tpls collect {
            case (setting, tpl) => ClockInSetting.WithTask(setting, tpl)
          }
        }
      }

  def insert(clockInSetting: ClockInSetting): Funit =
    coll.insert(clockInSetting).void

  def update(clockInSetting: ClockInSetting): Funit =
    coll.update($id(clockInSetting.id), clockInSetting).void

  def setStatus(clockInSetting: ClockInSetting, status: ClockInSetting.Status): Funit =
    coll.update(
      $id(clockInSetting.id),
      $set("status" -> status.id)
    ).void

  def addStudent(id: ClockInSetting.ID, studentId: User.ID): Funit =
    coll.update(
      $id(id),
      $addToSet("studentIds" -> studentId)
    ).void

  def removeStudent(id: ClockInSetting.ID, studentId: User.ID): Funit =
    coll.update(
      $id(id),
      $pull("studentIds" -> studentId)
    ).void

}
