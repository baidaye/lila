package lila.team

case class CoinSetting(
    open: Boolean,
    name: String,
    singleVal: Int
)

object CoinSetting {

  // 成员默认积分
  val defaultCoin = 0
  // 一次操作默认积分
  val defaultSingleVal = 1000
  // 一次操作最大积分
  val maxSingleVal = defaultSingleVal * 100

  def default = new CoinSetting(
    open = false,
    name = "",
    singleVal = defaultSingleVal
  )

}
