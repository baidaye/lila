package lila.team

import lila.db.dsl.Coll

private final class Colls(
    val team: Coll,
    val campus: Coll,
    val request: Coll,
    val invite: Coll,
    val member: Coll,
    val tag: Coll,
    val rating: Coll,
    val coin: Coll,
    val testTemplate: Coll,
    val testInvite: Coll,
    val testStudent: Coll,
    val clockInSetting: Coll,
    val clockInTask: Coll,
    val clockInMember: Coll,
    val cooperate: Coll,
    val federation: Coll,
    val federationRequest: Coll,
    val federationMember: Coll
)
