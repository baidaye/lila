package lila.team

import lila.notify.Notification.{ Notifies, Sender }
import lila.notify.TeamJoined.{ Id => TJId, Name => TJName }
import lila.notify.TeamMadeOwner.{ Id => TMOId, Name => TMOName }
import lila.notify.{ InvitedToTeam, Notification, NotifyApi, TeamApply, TeamApproved, TeamJoined, TeamMadeOwner }
import lila.team.TeamCoin.Typ
import lila.user.User

private[team] final class Notifier(notifyApi: NotifyApi) {

  def acceptRequest(team: Team, request: Request) = {
    val notificationContent = TeamJoined(TJId(team.id), TJName(team.name))
    val notification = Notification.make(Sender(User.lichessId).some, Notifies(request.user), notificationContent)
    notifyApi.addNotification(notification)
  }

  def madeOwner(team: Team, newOwner: String) = {
    val notificationContent = TeamMadeOwner(TMOId(team.id), TMOName(team.name))
    val notification = Notification.make(Sender(User.lichessId).some, Notifies(newOwner), notificationContent)
    notifyApi.addNotification(notification)
  }

  def certificationSend(team: Team, adminUid: User.ID) = {
    notifyApi.addNotification(
      Notification.make(Sender(User.lichessId).some, Notification.Notifies(adminUid), TeamApply(team.createdBy, team.id))
    )
  }

  def approveSend(team: Team, status: Certification.Status) = {
    notifyApi.addNotification(
      Notification.make(Sender(User.lichessId).some, Notification.Notifies(team.createdBy), TeamApproved(team.id, status.id))
    )
  }

  def approveCooperatorSend(team: Team, cooperator: User.ID) = {
    notifyApi.addNotification(Notification.make(
      Sender(User.lichessId).some,
      Notifies(cooperator),
      lila.notify.GenericLink(
        url = "/team/cooperates",
        title = "邀请俱乐部认证通过".some,
        text = s"${team.name} 认证通过".some,
        icon = "f"
      )
    ))
  }

  def inviteSend(team: Team, userId: User.ID): Funit = {
    val notificationContent = InvitedToTeam(
      InvitedToTeam.InvitedBy(team.createdBy),
      InvitedToTeam.TeamName(team.name),
      InvitedToTeam.TeamId(team.id)
    )
    notifyApi.addNotification(
      Notification.make(Sender(team.createdBy).some, Notification.Notifies(userId), notificationContent)
    )
  }

  def changeCoin(typ: TeamCoin.Typ, teamId: String, userId: User.ID, id: Option[String], coinDiff: Int, coinName: String, note: String) = {
    val defaultLink = s"/team/$teamId/coin/member?memberId=${Member.makeId(teamId, userId)}"
    val link = typ match {
      case Typ.TTask => id.fold(defaultLink) { idd => s"/ttask/$idd" }
      case Typ.Homework => id.fold(defaultLink) { idd => s"/clazz/homework/show?id=${idd + "@" + userId}" }
      case Typ.Setting => defaultLink
      case _ => defaultLink
    }

    notifyApi.addNotification(Notification.make(
      Sender(User.lichessId).some,
      Notifies(userId),
      lila.notify.GenericLink(
        url = link,
        title = s"$coinName ${if (coinDiff >= 0) s"+$coinDiff" else s"$coinDiff"}".some,
        text = s"${if (note.isEmpty) s"$coinName 发生变动" else note}".some,
        icon = "币"
      )
    ))
  }

  def federationClosed(federation: TeamFederation, memberId: User.ID, ownerId: User.ID) = {
    notifyApi.addNotification(Notification.make(
      Sender(ownerId).some,
      Notifies(memberId),
      lila.notify.GenericLink(
        url = s"/team/federation/${federation.id}/show",
        title = "联盟关闭".some,
        text = s"“${federation.name}”联盟 已关闭".some,
        icon = "f"
      )
    ))
  }

  def federationJoinAccept(federation: TeamFederation, memberId: User.ID, ownerId: User.ID) = {
    notifyApi.addNotification(Notification.make(
      Sender(ownerId).some,
      Notifies(memberId),
      lila.notify.GenericLink(
        url = s"/team/federation/${federation.id}/show",
        title = "加入同意".some,
        text = s"您的俱乐部已加入“${federation.name}”联盟".some,
        icon = "f"
      )
    ))
  }

  def federationJoinRequest(federation: TeamFederation, team: Team) = {
    notifyApi.addNotification(Notification.make(
      Sender(team.createdBy).some,
      Notifies(federation.teamOwnerId),
      lila.notify.GenericLink(
        url = s"/team/federation/requestList",
        title = "加入申请".some,
        text = s"俱乐部“${team.name}”申请加入“${federation.name}”联盟".some,
        icon = "f"
      )
    ))
  }

}
