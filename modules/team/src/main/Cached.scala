package lila.team

import lila.memo.Syncache
import lila.user.UserRepo
import scala.concurrent.duration._

private[team] final class Cached(
    asyncCache: lila.memo.AsyncCache.Builder
)(implicit system: akka.actor.ActorSystem) {

  val nameCache = new Syncache[String, Option[String]](
    name = "team.name",
    compute = TeamRepo.name,
    default = _ => none,
    strategy = Syncache.WaitAfterUptime(20 millis),
    expireAfter = Syncache.ExpireAfterAccess(1 hour),
    logger = logger
  )

  def name(id: String) = nameCache sync id

  // ~ 30k entries as of 04/02/17
  private val teamIdsCache = new Syncache[lila.user.User.ID, Team.IdsStr](
    name = "team.ids",
    compute = u => MemberRepo.teamIdsByUser(u).dmap(Team.IdsStr.apply),
    default = _ => Team.IdsStr.empty,
    strategy = Syncache.WaitAfterUptime(20 millis),
    expireAfter = Syncache.ExpireAfterAccess(1 hour),
    logger = logger
  )

  def syncTeamIds = teamIdsCache sync _
  def teamIds = teamIdsCache async _
  def teamIdsList(userId: lila.user.User.ID) = teamIds(userId).dmap(_.toList)

  def invalidateTeamIds = teamIdsCache invalidate _

  val nbRequests = asyncCache.clearable[lila.user.User.ID, Int](
    name = "team.nbRequests",
    f = getNbRequests,
    expireAfter = _.ExpireAfterAccess(12 minutes)
  )

  def getNbRequests(userId: String) = {
    for {
      userOption <- UserRepo.byId(userId)
      ownerRequests <- userOption.?? { user => user.teamId.?? { t => RequestRepo.findByTeam(t) } }
      adminRequests <- userOption.?? { user => user.headCampusAdmin.?? { t => RequestRepo.findByCampus(t) } }
      campuses <- userOption.?? { user => user.belongTeamId.?? { teamId => CampusRepo.byTeam(teamId) } }
    } yield (ownerRequests.filter(r => r.campus.isEmpty || !r.campus.exists(cid => campuses.exists(c => c.id == cid && c.hasAdmin))) ++ adminRequests).distinct.distinct.size
  }

  val managers = asyncCache.clearable[Team.ID, List[(lila.user.User.ID, Campus.ID)]](
    name = "team.managers",
    f = teamManagers,
    expireAfter = _.ExpireAfterWrite(1 hour)
  )

  def invalidateManagers(teamId: Team.ID) = managers.invalidate(teamId)

  def teamManagers(teamId: Team.ID) =
    for {
      teamOption <- TeamRepo.byId(teamId)
      campuses <- CampusRepo.byTeam(teamId)
    } yield { teamOption.map { team => List(team.createdBy -> team.id) } | Nil } ++ campuses.filter(_.hasAdmin).map(c => c.admin.get -> c.id)

}
