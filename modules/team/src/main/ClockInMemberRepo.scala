package lila.team

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime

object ClockInMemberRepo {

  // dirty
  private val coll = Env.current.colls.clockInMember

  import BSONHandlers.ClockInMemberBSONHandler

  def byId(id: ClockInMember.ID): Fu[Option[ClockInMember]] = coll.byId[ClockInMember](id)

  def bySettingAll(settingId: ClockInSetting.ID): Fu[List[ClockInMember]] =
    coll.find($doc("settingId" -> settingId))
      .sort($doc("date" -> 1))
      .list()

  def bySettingNoStart(settingId: ClockInSetting.ID): Fu[List[ClockInMember]] =
    coll.find($doc("settingId" -> settingId, "date" $gt DateTime.now))
      .list()

  def bySetting(settingId: ClockInSetting.ID, userId: User.ID): Fu[List[ClockInMember]] =
    coll.find($doc("settingId" -> settingId, "userId" -> userId))
      .sort($doc("date" -> 1))
      .list()

  def byClockInTask(clockInTaskId: ClockInTask.ID): Fu[List[ClockInMember]] =
    coll.find($doc("clockInTaskId" -> clockInTaskId))
      .sort($doc("date" -> 1))
      .list()

  def exists(settingId: ClockInSetting.ID, userId: User.ID): Fu[Boolean] =
    coll.exists($doc("settingId" -> settingId, "userId" -> userId))

  def bulkInsert(members: List[ClockInMember]): Funit =
    coll.bulkInsert(
      documents = members.map(ClockInMemberBSONHandler.write).toStream,
      ordered = true
    ).void

  def setFinished(clockInTaskId: ClockInTask.ID, userId: User.ID, num: Int): Funit =
    coll.update(
      $id(ClockInMember.makeId(clockInTaskId, userId)),
      $set("isComplete" -> true, "num" -> num)
    ).void

  def setNum(clockInTaskId: ClockInTask.ID, userId: User.ID, num: Int): Funit =
    coll.update(
      $id(ClockInMember.makeId(clockInTaskId, userId)),
      $set("nb" -> num)
    ).void

}
