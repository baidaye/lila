package lila.team

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import lila.hub.actorApi.member.TeamInviteRel

object TeamCooperateRepo {

  private val coll = Env.current.colls.cooperate
  import BSONHandlers._

  def byId(id: String): Fu[Option[TeamCooperate]] = coll.byId[TeamCooperate](id)

  def findByTeam(teamId: String): Fu[Option[TeamCooperate]] =
    coll.uno[TeamCooperate]($doc("teamId" -> teamId))

  def findByUserWithTeam(userId: String): Fu[List[TeamCooperate.WithTeam]] =
    findByUser(userId) flatMap { cooperates =>
      TeamRepo.byOrderedIds(cooperates.map(_.teamId)) map { teams =>
        cooperates.map { cooperate =>
          TeamCooperate.WithTeam(teams.find(_.id == cooperate.teamId).err(s"can not find team ${cooperate.teamId}"), cooperate)
        }
      }
    }

  def findByUser(userId: String): Fu[List[TeamCooperate]] =
    coll.find(userQuery(userId))
      .sort($doc("certifyTime" -> 1))
      .list[TeamCooperate]()

  def insert(cooperate: TeamCooperate): Funit =
    coll.insert(cooperate).void

  def setOrder(userId: User.ID, typ: String, diff: Int, orderId: Option[String], teamRel: Option[TeamInviteRel]): Funit =
    (typ == "orderInviteRebate" && diff != 0).?? {
      teamRel.?? { t =>
        orderId.?? { o =>
          coll.update(
            $id(TeamCooperate.makeId(userId, t.teamId)),
            $addToSet("orders" -> TeamCooperateOrder(o, t.userId, diff, DateTime.now))
          ).void
        }
      }
    }

  def userQuery(userId: String) = $doc("userId" -> userId)

}
