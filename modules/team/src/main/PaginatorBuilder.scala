package lila.team

import lila.db.dsl._
import lila.db.paginator._
import lila.common.paginator._
import lila.common.MaxPerPage

private[team] final class PaginatorBuilder(
    coll: Colls,
    teamApi: TeamApi,
    maxPerPage: MaxPerPage,
    maxUserPerPage: MaxPerPage
) {

  import BSONHandlers._

  def popularTeams(page: Int, text: Option[String] = None): Fu[Paginator[Team]] = Paginator(
    adapter = new Adapter(
      collection = coll.team,
      selector = TeamRepo.enabledQuery ++ text.?? { t =>
        $or(
          $doc("_id" -> t.trim),
          $doc("name" -> t.trim)
        )
      },
      projection = $empty,
      sort = TeamRepo.sortPopular
    ),
    page,
    maxPerPage
  )

}
