package lila.team

import lila.user.User
import org.joda.time.DateTime

case class TeamFederation(
    _id: TeamFederation.ID,
    teamId: Team.ID,
    teamOwnerId: User.ID,
    name: String,
    description: String,
    logo: Option[String],
    nbMembers: Int,
    open: Boolean,
    enabled: Boolean,
    visibility: TeamFederation.Visibility,
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

  def disabled = !enabled

  def isOpen = open

  def isOwner(userId: String) = userId == teamOwnerId

  def isAllVisible = visibility == TeamFederation.Visibility.All

}

object TeamFederation {

  type ID = String

  val MAX_CREATE = 5
  val MAX_JOIN = 10

  sealed abstract class Visibility(val id: String, val name: String)
  object Visibility {
    case object All extends Visibility("all", "所有人")
    case object Member extends Visibility("member", "仅联盟成员")

    def all = List(All, Member)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Visibility = all.find(_.id == id) err s"can not apply Visibility $id"
  }

  case class WithMember(federation: TeamFederation, member: TeamFederationMember)

  case class WithRequestAndMember(federation: TeamFederation, request: Option[TeamFederationRequest], member: Option[TeamFederationMember])

  def make(
    id: String,
    teamId: String,
    teamOwnerId: User.ID,
    name: String,
    description: String,
    open: Boolean,
    createdBy: User.ID
  ): TeamFederation = new TeamFederation(
    _id = id,
    teamId = teamId,
    teamOwnerId = teamOwnerId,
    name = name,
    description = description,
    logo = None,
    nbMembers = 1,
    open = open,
    enabled = true,
    visibility = Visibility.All,
    createdAt = DateTime.now,
    createdBy = createdBy
  )

}
