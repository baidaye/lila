package lila.team

import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.common.paginator.Paginator
import lila.team.DataForm.coinData.CoinLogSearch

object TeamCoinRepo {

  private val coll = Env.current.colls.coin
  import BSONHandlers._

  def byId(id: String): Fu[Option[TeamCoin]] = coll.byId[TeamCoin](id)

  def findByUser(userId: String): Fu[List[TeamCoin]] =
    coll.find(userQuery(userId)).sort($doc("createAt" -> -1)).list[TeamCoin]()

  def page(page: Int, userId: String): Fu[Paginator[TeamCoin]] = {
    val adapter = new Adapter[TeamCoin](
      collection = coll,
      selector = userQuery(userId),
      projection = $empty,
      sort = $doc("createAt" -> -1)
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  def logPage(page: Int, teamId: String, search: CoinLogSearch): Fu[Paginator[TeamCoin]] = {
    var selector = $doc("teamId" -> teamId) ++
      search.campus.??(campus => $doc("campusId" -> campus)) ++
      search.member.??(member => $doc("userId" -> member.toLowerCase)) ++
      search.operator.??(operator => $doc("createBy" -> operator.toLowerCase)) ++
      search.typ.??(typ => $doc("typ" -> typ))

    if (search.dateMin.isDefined || search.dateMax.isDefined) {
      var dateRange = $doc()
      search.dateMin foreach { dateMin =>
        dateRange = dateRange ++ $gte(dateMin.withTimeAtStartOfDay())
      }
      search.dateMax foreach { dateMax =>
        dateRange = dateRange ++ $lte(dateMax.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999))
      }
      selector = selector ++ $doc("createAt" -> dateRange)
    }

    val adapter = new Adapter[TeamCoin](
      collection = coll,
      selector = selector,
      projection = $empty,
      sort = $sort desc "createAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(30)
    )
  }

  def insert(rating: TeamCoin): Funit =
    coll.insert(rating).void

  def userQuery(userId: String) = $doc("userId" -> userId)

}
