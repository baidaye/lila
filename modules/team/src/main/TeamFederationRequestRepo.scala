package lila.team

import lila.db.dsl._

object TeamFederationRequestRepo {

  // dirty
  private val coll = Env.current.colls.federationRequest

  import BSONHandlers.TeamFederationRequestBSONHandler

  def byId(id: TeamFederationRequest.ID): Fu[Option[TeamFederationRequest]] =
    coll.byId[TeamFederationRequest](id)

  def find(federationId: TeamFederation.ID, teamId: Team.ID): Fu[Option[TeamFederationRequest]] =
    coll.byId[TeamFederationRequest](TeamFederationMember.makeId(federationId, teamId))

  def exists(federationId: TeamFederation.ID, teamId: Team.ID): Fu[Boolean] =
    coll.exists(selectId(federationId, teamId))

  def findByTeam(teamId: Team.ID): Fu[List[TeamFederationRequest]] =
    coll.find($doc("teamId" -> teamId)).list()

  def findByFederationTeam(teamId: Team.ID): Fu[List[TeamFederationRequest]] =
    coll.find($doc("federationTeamId" -> teamId)).list()

  def countByTeam(teamId: Team.ID): Fu[Int] =
    coll.countSel($doc("federationTeamId" -> teamId))

  def insert(request: TeamFederationRequest): Funit =
    coll.insert(request).void

  def remove(id: TeamFederationRequest.ID) =
    coll.remove($id(id))

  def selectId(federationId: TeamFederation.ID, teamId: Team.ID) =
    $id(TeamFederationRequest.makeId(federationId, teamId))

}
