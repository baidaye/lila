package lila.interest

import lila.user.User
import org.joda.time.DateTime

case class SearchRecord(
    _id: String,
    userId: User.ID,
    source: Source,
    interestId: ID,
    updateAt: DateTime
) {

  def id = _id

}

object SearchRecord {

  def make(
    userId: User.ID,
    source: Source,
    interestId: ID
  ) = {
    SearchRecord(
      _id = makeId(userId, source),
      userId = userId,
      source = source,
      interestId = interestId,
      updateAt = DateTime.now
    )
  }

  def makeId(userId: User.ID, source: Source) = userId + "@" + source.id

}
