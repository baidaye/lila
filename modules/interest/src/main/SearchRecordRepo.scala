package lila.interest

import lila.db.dsl._
import lila.user.User

object SearchRecordRepo {

  private lazy val coll = Env.current.CollSearchRecord

  import BSONHandlers.SearchRecordHandler

  def byId(source: Source, userId: User.ID): Fu[Option[SearchRecord]] =
    coll.byId(SearchRecord.makeId(userId, source))

  def lastId(source: Source, userId: Option[User.ID]): Fu[Int] =
    userId match {
      case None => fuccess(source.min)
      case Some(uid) => byId(source, uid).map { tr =>
        tr.fold(source.min)(_.interestId)
      }
    }

  def upsert(
    userId: User.ID,
    source: Source,
    interestId: ID
  ): Funit = {
    val record = SearchRecord.make(userId, source, interestId)
    coll.update(
      $id(SearchRecord.makeId(userId, source)),
      record,
      upsert = true
    ).void
  }

}
