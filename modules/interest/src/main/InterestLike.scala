package lila.interest

import lila.user.User
import org.joda.time.DateTime

case class InterestLike(
    _id: String,
    interestId: ID,
    userId: User.ID,
    source: Source,
    tags: List[String],
    createdAt: DateTime
) {

  def id = _id

}

object InterestLike {

  def makeId(interestId: ID, userId: User.ID, source: Source): String =
    interestId + "@" + userId + "@" + source.id

  def make(interestId: ID, userId: User.ID, source: Source, tags: List[String]) =
    InterestLike(
      _id = makeId(interestId, userId, source),
      interestId = interestId,
      userId = userId,
      source = source,
      tags = tags,
      createdAt = DateTime.now
    )

}
