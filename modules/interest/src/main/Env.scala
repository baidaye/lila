package lila.interest

import akka.actor.ActorSystem
import com.typesafe.config.Config

final class Env(
    config: Config,
    db: lila.db.Env,
    system: ActorSystem
) {

  private val AnimationDuration = config duration "animation.duration"
  private val CollectionInterest = config getString "collection.interest"
  private val CollectionLike = config getString "collection.like"
  private val CollectionRound = config getString "collection.round"
  private val CollectionSearch = config getString "collection.search"
  private val CollectionSearchRecord = config getString "collection.search_record"

  lazy val CollInterest = db(CollectionInterest)
  lazy val CollLike = db(CollectionLike)
  lazy val CollRound = db(CollectionRound)
  lazy val CollSearch = db(CollectionSearch)
  lazy val CollSearchRecord = db(CollectionSearchRecord)

  lazy val jsonView = new JsonView(
    animationDuration = AnimationDuration
  )

  lazy val api = new InterestApi()

}

object Env {

  lazy val current: Env = "interest" boot new Env(
    config = lila.common.PlayApp loadConfig "interest",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system
  )
}
