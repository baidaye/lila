package lila.interest

import lila.user.User
import play.api.libs.json._

final class JsonView(animationDuration: scala.concurrent.duration.Duration) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "coords" -> p.coords,
    "rookCastle" -> p.rookCastle,
    "animationDuration" -> p.animationFactor * animationDuration.toMillis,
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d
  )

  def data(data: InterestData, user: Option[User]) =
    user.fold(fuccess((false, List.empty[String]))) { u =>
      InterestLikeRepo.byId(data.id, u.id, data.source) map {
        case None => (false, List.empty[String])
        case Some(lk) => (true, lk.tags)
      }
    } map {
      case (liked, tags) => {
        Json.obj(
          "id" -> data.id,
          "fen" -> data.fen,
          "role" -> data.role.toString.toLowerCase,
          "color" -> data.color.name,
          "phase" -> data.phase,
          "square" -> data.square,
          "kingSquare" -> data.kingSquare,
          "colorKingSquare" -> data.colorKingSquare(),
          "path" -> data.path,
          "uciPath" -> data.uciPath,
          "rightPiece" -> data.rightPiece.map(_.map(_.toString.toLowerCase)),
          "rightSquare" -> data.rightSquare,
          "wrongSquare" -> data.wrongSquare,
          "steps" -> data.steps,
          "source" -> data.source.id,
          "attempts" -> data.attempts,
          "liked" -> liked,
          "tags" -> tags
        )
      }
    }

}

