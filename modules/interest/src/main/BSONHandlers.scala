package lila.interest

import reactivemongo.bson._
import lila.db.dsl._

object BSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler

  private implicit val ColorBSONHandler = new BSONHandler[BSONString, chess.Color] {
    def read(b: BSONString) = chess.Color(b.value) err s"invalid color ${b.value}"
    def write(c: chess.Color) = BSONString(c.name)
  }

  private implicit val RoleBSONHandler = new BSONHandler[BSONString, chess.Role] {
    def read(b: BSONString) = chess.Role.forsyth(b.value.charAt(0)) err s"invalid role ${b.value}"
    def write(r: chess.Role) = BSONString(r.forsyth.toString)
  }

  private implicit val RoleArrayHandler = bsonArrayToListHandler[chess.Role]

  private implicit val SourceBSONHandler = new BSONHandler[BSONString, Source] {
    def read(b: BSONString) = Source(b.value)
    def write(c: Source) = BSONString(c.id)
  }

  private implicit val stringArrayHandler = bsonArrayToListHandler[String]

  implicit val InterestHandler = Macros.handler[InterestData]
  implicit val LikeHandler = Macros.handler[InterestLike]
  implicit val RoundHandler = Macros.handler[InterestRound]
  implicit val SearchHandler = Macros.handler[InterestSearch]
  implicit val SearchRecordHandler = Macros.handler[SearchRecord]
}
