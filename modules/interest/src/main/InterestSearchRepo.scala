package lila.interest

import lila.db.dsl._
import lila.user.User

object InterestSearchRepo {

  private lazy val coll = Env.current.CollSearch
  import BSONHandlers.SearchHandler

  def byId(source: Source, userId: User.ID): Fu[Option[InterestSearch]] =
    coll.byId(InterestSearch.makeId(userId, source))

  def mine(source: Source, userId: Option[User.ID]): Fu[InterestSearchData] =
    userId match {
      case None => fuccess(InterestSearchData.default(source))
      case Some(uid) => byId(source, uid).map {
        case None => InterestSearchData.default(source)
        case Some(s) => s.toData
      }
    }

  def upsert(
    userId: User.ID,
    source: Source,
    search: InterestSearchData
  ): Funit = {
    val record = InterestSearch.make(userId, source, search)
    coll.update(
      $id(InterestSearch.makeId(userId, source)),
      record,
      upsert = true
    ).void
  }

}
