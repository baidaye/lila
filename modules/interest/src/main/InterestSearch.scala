package lila.interest

import lila.user.User
import org.joda.time.DateTime

case class InterestSearch(
    _id: String,
    userId: User.ID,
    source: Source,
    role: Option[String],
    color: Option[String],
    phase: Option[String],
    step: Option[String],
    orientation: Option[String],
    autoNext: Option[Boolean],
    updateAt: DateTime
) {

  def toData = {
    InterestSearchData(
      role = role,
      color = color,
      phase = phase,
      step = step,
      orientation = orientation,
      prevResult = none,
      prevStep = none,
      autoNext = autoNext
    )
  }

}

object InterestSearch {

  def make(
    userId: User.ID,
    source: Source,
    search: InterestSearchData
  ) = {
    InterestSearch(
      _id = makeId(userId, source),
      userId = userId,
      source = source,
      role = search.role,
      color = search.color,
      phase = search.phase,
      step = search.step,
      orientation = search.orientation,
      autoNext = search.autoNext,
      updateAt = DateTime.now
    )
  }

  def makeId(userId: User.ID, source: Source) = userId + "@" + source.id
}
