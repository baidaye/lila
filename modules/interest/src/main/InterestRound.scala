package lila.interest

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class InterestRound(
    _id: String,
    interestId: ID,
    userId: User.ID,
    nbMoves: Int,
    score: Int,
    stars: Int,
    success: Boolean,
    lines: List[String],
    squares: List[String],
    source: Source,
    createdAt: DateTime
) {

  def id = _id

}

object InterestRound {

  def make(
    interestId: ID,
    userId: User.ID,
    nbMoves: Int,
    score: Int,
    stars: Int,
    success: Boolean,
    lines: List[String],
    squares: List[String],
    source: Source
  ) =
    InterestRound(
      _id = Random nextString 12,
      interestId = interestId,
      userId = userId,
      nbMoves = nbMoves,
      score = score,
      stars = stars,
      success = success,
      lines = lines,
      squares = squares,
      source = source,
      createdAt = DateTime.now
    )

}
