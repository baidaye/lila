package lila.interest

import lila.user.User

final class InterestApi() {

  def finish(id: ID, source: Source, userId: Option[User.ID], data: FinishData): Funit =
    userId.?? { u => InterestRoundRepo.insert(id, source, u, data) } >>- InterestRepo.incAttempts(id)

}
