package lila.interest

import lila.user.User
import lila.db.dsl._

object InterestLikeRepo {

  private lazy val coll = Env.current.CollLike

  import BSONHandlers.LikeHandler

  def byId(interestId: ID, userId: User.ID, source: Source): Fu[Option[InterestLike]] =
    coll.byId(InterestLike.makeId(interestId, userId, source))

  def liked(interestId: ID, userId: User.ID, source: Source): Fu[Boolean] =
    coll.exists($id(InterestLike.makeId(interestId, userId, source)))

  def insert(interestId: ID, userId: User.ID, source: Source, tags: List[String] = Nil): Funit =
    coll.insert(
      InterestLike.make(
        interestId = interestId,
        userId = userId,
        source = source,
        tags = tags
      )
    ).void

  def remove(interestId: ID, userId: User.ID, source: Source): Funit =
    coll.remove($id(InterestLike.makeId(interestId, userId, source))).void

  def toggle(interestId: ID, userId: User.ID, source: Source): Funit =
    liked(interestId, userId, source) flatMap { e =>
      (if (e) remove(interestId, userId, source) else insert(interestId, userId, source)) inject !e
    } flatMap { liked =>
      InterestRepo.incLikes(interestId, if (liked) 1 else -1)
    }

  def setTag(interestId: ID, userId: User.ID, source: Source, tags: List[String]): Funit =
    liked(interestId, userId, source) flatMap { e =>
      {
        if (e) doSetTag(interestId, userId, source, tags)
        else insert(interestId, userId, source, tags)
      } inject !e
    } flatMap { liked =>
      liked.?? { InterestRepo.incLikes(interestId, 1) }
    }

  def doSetTag(interestId: ID, userId: User.ID, source: Source, tags: List[String]): Funit =
    coll.update(
      $id(InterestLike.makeId(interestId, userId, source)),
      $set(
        "tags" -> tags
      )
    ).void

  def tags(userId: String, source: Source): Fu[Set[String]] =
    coll.distinct[String, Set]("tags", $doc("userId" -> userId, "source" -> source.id).some)

  def getIds(condition: Bdoc): Fu[List[ID]] =
    coll.find(condition)
      .sort($doc("createdAt" -> -1))
      .cursor[Bdoc]()
      .gather[List]()
      .map { _ flatMap { _.getAs[Int]("interestId") } }

  def likeByIds(interestIds: List[ID], userId: User.ID, source: Source): Funit = {
    val ids = interestIds.map { interestId => InterestLike.makeId(interestId, userId, source) }
    coll.byIds[InterestLike](ids) flatMap { likeds =>
      val existsInterestIds = likeds.map(_.interestId)
      val list =
        interestIds.filter(!existsInterestIds.contains(_)).map { interestId =>
          InterestLike.make(
            interestId = interestId,
            userId = userId,
            source = source,
            tags = Nil
          )
        }

      list.nonEmpty.?? {
        coll.bulkInsert(
          documents = list.map(LikeHandler.write).toStream,
          ordered = true
        ).void
      }
    }
  }

  def removeByIds(ids: List[ID], userId: User.ID, source: Source): Funit =
    coll.remove($doc("interestId" $in ids, "userId" -> userId, "source" -> source.id)) >>
      InterestRepo.incLikesByIds(ids, -1).void

}
