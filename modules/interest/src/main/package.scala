package lila

package object interest extends PackageObject {

  private[interest] val logger = lila.log("interest")

  type ID = Int

  sealed abstract class Source(val id: String, val name: String, val desc: String, val min: Int, val max: Int, val maxStep: Int)
  object Source {
    case object AnnihilationStar extends Source(id = "g1", name = "吃星星", desc = "吃光所有的星星，保证每步棋都吃到星星，才能得到满分。", 1000000, 1999999, 15)
    case object AnnihilationPiece extends Source(id = "g2", name = "消灭对手", desc = "吃光对手的棋子，中间自己的棋子不能被攻击，保证每步棋都吃子，才能得到满分。", 1000000, 1999999, 15)
    case object CaptureKing extends Source(id = "g3", name = "单子攻王", desc = "使用指定的棋子，攻击对方的王，中间不能被攻击，也不能吃对方子。", 2000000, 2999999, 15)
    case object UnprotectedPieces extends Source(id = "g4", name = "未保护棋子", desc = "找出指定棋色，全部未被保护的棋子，不包含王。", 3000000, 3999999, 6)
    case object UnprotectedSquares extends Source(id = "g5", name = "未保护格子", desc = "找出指定棋色，在自己的半个棋盘内，全部未被保护的格子。", 4000000, 4999999, 5)

    case object PawnsOnly extends Source(id = "g6", name = "小兵大战", desc = "双方都只有小兵，白方先行，先升变为后一方获胜。", 0, 0, 0)
    case object DualRooksAndBishops extends Source(id = "g7", name = "双车捉双象", desc = "一方双车，另一方双象，白方先行，吃掉对手任意棋子获胜。", 0, 0, 0)

    val all = List(AnnihilationStar, AnnihilationPiece, CaptureKing, UnprotectedPieces, UnprotectedSquares, PawnsOnly, DualRooksAndBishops)

    def choices = all.map(s => s.id -> s.name)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Source = byId.get(id) err s"Bad Source $this"
  }

}
