package lila.interest

import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.user.User
import lila.db.dsl._
import lila.db.paginator.Adapter
import reactivemongo.bson.BSONDocument

object InterestRepo {

  private lazy val coll = Env.current.CollInterest

  import BSONHandlers.InterestHandler

  val enabled = $doc("enabled" -> true)

  def byId(id: ID): Fu[Option[InterestData]] = coll.byId(id)

  def next(me: Option[User], source: Source, lastId: ID, search: InterestSearchData): Fu[Option[InterestData]] = {
    var $filter = enabled ++ $doc("_id" -> ($gte(source.min) ++ $lte(source.max))) ++ $doc("_id" -> $gt(lastId))
    search.role.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("role" -> tg)
      }
    }

    search.color.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("color" -> tg)
      }
    }

    search.phase.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("phase" -> tg)
      }
    }

    $filter = $filter ++ (adaptStep(search, source) | $empty)

    //println(BSONDocument.pretty($filter))
    coll.find($filter)
      .sort($sort asc "_id")
      .uno[InterestData]
      .flatMap {
        case None => {
          (lastId != source.min).?? {
            next(me, source, source.min, search)
          }
        }
        case Some(d) => {
          me.?? { u =>
            SearchRecordRepo.upsert(u.id, source, d.id)
          } inject d.some
        }
      }
  }

  private def adaptStep(search: InterestSearchData, source: Source) =
    search.step map { tg =>
      if (tg == "adapt") {
        (search.prevResult |@| search.prevStep).apply {
          case (prevResult, prevStep) => {
            if (prevResult == 1) {
              $doc("steps" -> Math.min(prevStep + 1, source.maxStep))
            } else if (prevResult == -1) {
              $doc("steps" -> Math.max(prevStep - 1, 1))
            } else {
              $doc("steps" -> prevStep)
            }
          }
        } | $doc("steps" -> 1)
      } else {
        val steps = tg.split(",").toList.map(_.toInt)
        $doc("steps" -> $in(steps: _*))
      }
    }

  def incLikes(id: ID, inc: Int): Funit =
    coll.update($id(id), $inc("likes" -> inc)).void

  def incLikesByIds(ids: List[ID], inc: Int): Funit =
    coll.update($inIds(ids), $inc("likes" -> inc)).void

  def incAttempts(id: ID) =
    coll.incFieldUnchecked($id(id), "attempts")

  def resourcePage(source: Source, page: Int, search: ResourceSearch): Fu[Paginator[InterestData]] = {
    var $filter = enabled ++ $doc("_id" -> ($gte(source.min) ++ $lte(source.max)))

    search.role.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("role" -> tg)
      }
    }

    search.color.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("color" -> tg)
      }
    }

    search.phase.foreach { tg =>
      if (!tg.isEmpty) {
        $filter = $filter ++ $doc("phase" -> tg)
      }
    }

    if (search.stepMin.isDefined || search.stepMax.isDefined) {
      var stepRange = $doc()
      search.stepMin foreach { stepMin =>
        stepRange = stepRange ++ $gte(stepMin)
      }
      search.stepMax foreach { stepMax =>
        stepRange = stepRange ++ $lte(stepMax)
      }
      $filter = $filter ++ $doc("steps" -> stepRange)
    }

    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = $filter,
        projection = $empty,
        sort = $doc("_id" -> 1)
      ),
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def likePage(source: Source, userId: String, page: Int, query: LikedData): Fu[Paginator[InterestData]] = {
    var condition = $doc("userId" -> userId, "source" -> source.id)
    var $andConditions = List.empty[BSONDocument]
    query.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    query.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }

    InterestLikeRepo.getIds(condition) flatMap { ids =>
      Paginator(
        adapter = new Adapter(
          collection = coll,
          selector = $inIds(ids),
          projection = $empty,
          sort = $empty
        ),
        currentPage = page,
        maxPerPage = MaxPerPage(15)
      ).map { page =>
          val sortedList = ids.map { id =>
            page.currentPageResults.find(p => p.id == id)
          }.filter(_.isDefined).map(_.get)
          page.withCurrentPageResults(sortedList)
        }
    }
  }

}
