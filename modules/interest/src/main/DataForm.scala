package lila.interest

import play.api.data._
import play.api.data.Forms._
import lila.common.Form.stringIn
import scala.util.Random

object DataForm {

  def interestSearch(source: Source) = {
    val sc = source match {
      case Source.AnnihilationStar | Source.AnnihilationPiece => stepChoices
      case Source.CaptureKing => kingStepChoices
      case Source.UnprotectedPieces => pieceNumChoices
      case Source.UnprotectedSquares => squareNumChoices
      case Source.PawnsOnly => Nil
      case Source.DualRooksAndBishops => Nil
    }

    Form(mapping(
      "role" -> optional(stringIn(roleChoices)),
      "color" -> optional(stringIn(colorChoices)),
      "phase" -> optional(stringIn(phaseChoices)),
      "step" -> optional(stringIn(sc)),
      "orientation" -> optional(stringIn(orientationChoices)),
      "prevResult" -> optional(number.verifying(d => Set(-1, 0, 1).contains(d))),
      "prevStep" -> optional(number(min = 1, max = source.maxStep)),
      "autoNext" -> optional(boolean)
    )(InterestSearchData.apply)(InterestSearchData.unapply))
  }

  def colorChoices = List("white" -> "白棋", "black" -> "黑棋")
  def roleChoices = List("" -> "随机", "q" -> "后", "r" -> "车", "b" -> "象", "n" -> "马")
  def phaseChoices = List("" -> "随机", "Opening" -> "开局", "MiddleGame" -> "中局", "EndingGame" -> "残局")
  def pieceNumChoices = List("adapt" -> "自适应", "1" -> "1子", "2" -> "2子", "3,4" -> "3-4子", "5,6" -> "5-6子")
  def squareNumChoices = List("adapt" -> "自适应", "1" -> "1格", "2" -> "2格", "3" -> "3格", "4" -> "4格")
  def kingStepChoices = List("adapt" -> "自适应", "1,2" -> "1-2步", "3,4" -> "3-4步", "5,6,7" -> "5-7步", "8,9,10,11,12,13,14,15" -> "8步以上")
  def stepChoices = List("adapt" -> "自适应", "1,2" -> "1-2步", "3,4" -> "3-4步", "5,6,7" -> "5-7步", "8,9,10" -> "8-10步", "11,12,13,14,15" -> "10步以上")
  def orientationChoices = List("attacker" -> "进攻", "defend" -> "防守", "white" -> "白方", "black" -> "黑方")

  def interestVariantSearch(source: Source) = {
    Form(mapping(
      "rookColor" -> optional(text),
      "color" -> optional(text),
      "level" -> optional(text),
      "situation" -> optional(text),
      "fen" -> optional(text)
    )(InterestVariantSearchData.apply)(InterestVariantSearchData.unapply))
  }

  def finish(source: Source) = Form(mapping(
    "id" -> number(min = source.min, max = source.max),
    "nbMoves" -> number(min = 0),
    "score" -> number(min = 0),
    "stars" -> number(min = 0),
    "success" -> boolean,
    "lines" -> list(nonEmptyText),
    "squares" -> list(nonEmptyText)
  )(FinishData.apply)(FinishData.unapply))

  val likedSearch = Form(mapping(
    "emptyTag" -> optional(nonEmptyText),
    "tags" -> optional(list(nonEmptyText))
  )(LikedData.apply)(LikedData.unapply)) fill LikedData()

  val like = Form(single(
    "tags" -> text(minLength = 0, maxLength = 200)
  ))

  def resourceSearch(source: Source) = Form(mapping(
    "role" -> optional(stringIn(roleChoices)),
    "color" -> optional(stringIn(colorChoices)),
    "phase" -> optional(stringIn(phaseChoices)),
    "stepMin" -> optional(number(min = 1, max = source.maxStep)),
    "stepMax" -> optional(number(min = 1, max = source.maxStep))
  )(ResourceSearch.apply)(ResourceSearch.unapply)
    .verifying("最小步数必须最大步数", _.verifyStep))

}

case class InterestSearchData(
    role: Option[String],
    color: Option[String],
    phase: Option[String],
    step: Option[String],
    orientation: Option[String],
    prevResult: Option[Int],
    prevStep: Option[Int],
    autoNext: Option[Boolean]
) {

  def orDefault(source: Source) = {
    val default = InterestSearchData.default(source)
    copy(
      role = role.orElse(default.role),
      color = color.orElse(default.color),
      phase = phase.orElse(default.phase),
      step = step.orElse(default.step),
      orientation = orientation.orElse(default.orientation),
      prevResult = prevResult.orElse(default.prevResult),
      prevStep = prevStep.orElse(default.prevStep),
      autoNext = autoNext.orElse(default.autoNext)
    )
  }

  def toUrlParam =
    role.??(r => s"&role=$r") +
      color.??(c => s"&color=$c") +
      phase.??(p => s"&phase=$p") +
      step.??(s => s"&step=$s") +
      orientation.??(o => s"&orientation=$o") +
      autoNext.??(a => s"&autoNext=$a")
}

object InterestSearchData {

  def default(source: Source) =
    InterestSearchData(
      role = "".some,
      color = defaultColor(source).name.some,
      phase = "".some,
      step = "adapt".some,
      orientation = "attacker".some,
      prevResult = none,
      prevStep = none,
      autoNext = none
    )

  def defaultColor(source: Source) =
    source match {
      case Source.AnnihilationStar | Source.AnnihilationPiece | Source.CaptureKing => chess.White
      case Source.UnprotectedPieces | Source.UnprotectedSquares => chess.Black
      case Source.PawnsOnly | Source.DualRooksAndBishops => chess.White
    }
}

case class InterestVariantSearchData(
    rookColor: Option[String],
    color: Option[String],
    level: Option[String],
    situation: Option[String],
    fen: Option[String]
) {

  def orDefault(source: Source) = {
    val default = InterestVariantSearchData.default(source)
    copy(
      rookColor = rookColor.orElse(default.rookColor),
      color = color.orElse(default.color),
      level = level.orElse(default.level),
      situation = situation.orElse(default.situation),
      fen = fen.orElse(default.fen)
    )
  }

  def toUrlParam =
    rookColor.??(c => s"&rookColor=$c") +
      color.??(c => s"&color=$c") +
      level.??(l => s"&level=$l") +
      situation.??(s => s"&situation=$s") +
      fen.??(f => s"&fen=$f")

}

object InterestVariantSearchData {

  val rookAndBishopFens = Array(
    "5R2/7R/8/8/4b3/8/8/6b1 w - - 0 1", "4b3/8/1R6/8/8/8/5b2/R7 w - - 0 1", "8/8/8/8/5RR1/b7/4b3/8 w - - 0 1", "8/8/8/7R/2b5/7R/8/6b1 w - - 0 1", "8/8/4b3/7R/5b2/3R4/8/8 w - - 0 1", "7R/2b5/6b1/8/5R2/8/8/8 w - - 0 1", "7b/5b2/8/8/8/6R1/8/2R5 w - - 0 1", "8/1R3R2/4b3/8/8/b7/8/8 w - - 0 1", "8/3b4/5R2/2b5/8/8/8/4R3 w - - 0 1", "8/8/6b1/8/8/1R6/5b2/2R5 w - - 0 1", "b7/8/8/8/7R/7R/3b4/8 w - - 0 1", "4R3/7b/8/6b1/8/4R3/8/8 w - - 0 1", "1R6/6b1/8/3b4/8/8/8/5R2 w - - 0 1", "8/8/5R2/R7/8/8/7b/1b6 w - - 0 1", "7b/1b6/8/8/R7/8/R7/8 w - - 0 1", "8/8/3b4/2R5/4b3/8/8/2R5 w - - 0 1", "6R1/8/8/8/1R6/8/b7/2b5 w - - 0 1", "3b4/8/6b1/4R3/8/7R/8/8 w - - 0 1", "8/5b2/3R4/8/8/3R4/8/2b5 w - - 0 1", "3b4/5R2/b7/8/8/8/6R1/8 w - - 0 1", "8/8/8/8/3b4/8/4b3/R1R5 w - - 0 1", "b7/1R6/7b/8/8/8/8/3R4 w - - 0 1", "8/6R1/8/5R2/1b6/8/8/3b4 w - - 0 1", "8/8/R7/6b1/R7/5b2/8/8 w - - 0 1", "2R5/8/8/8/8/6b1/R7/3b4 w - - 0 1", "8/8/3R4/8/8/7b/6R1/2b5 w - - 0 1", "1b6/8/4R3/8/8/8/5R2/3b4 w - - 0 1", "8/6b1/8/8/3R4/5b2/2R5/8 w - - 0 1", "6RR/8/8/8/8/5b2/8/2b5 w - - 0 1", "8/4R3/6R1/b7/8/8/8/3b4 w - - 0 1", "8/8/8/5R2/3b4/7b/8/4R3 w - - 0 1", "4R3/8/8/3R4/2b5/8/8/b7 w - - 0 1", "3R4/8/6R1/8/8/7b/8/2b5 w - - 0 1", "8/8/8/8/3R4/6R1/1b6/5b2 w - - 0 1", "8/8/b7/6R1/6R1/4b3/8/8 w - - 0 1", "4b3/6R1/7b/2R5/8/8/8/8 w - - 0 1", "8/8/3R4/2b5/8/1b6/4R3/8 w - - 0 1", "5b2/8/8/7R/8/1b6/7R/8 w - - 0 1", "6b1/8/1R3R2/8/8/8/3b4/8 w - - 0 1", "2R5/6b1/8/1R6/8/8/4b3/8 w - - 0 1", "8/4b3/3R4/3R4/8/8/8/5b2 w - - 0 1", "8/8/8/b7/6R1/8/7R/3b4 w - - 0 1", "7R/8/6b1/8/8/8/5R2/2b5 w - - 0 1", "3b4/8/2R5/8/8/2R5/b7/8 w - - 0 1", "8/8/7b/8/1RR5/8/8/5b2 w - - 0 1", "4b3/8/3R4/3R4/1b6/8/8/8 w - - 0 1", "8/1R6/8/8/2b5/8/5b2/6R1 w - - 0 1", "3R4/8/8/7R/8/6b1/2b5/8 w - - 0 1", "8/8/6b1/8/8/8/7b/2RR4 w - - 0 1", "8/6R1/8/8/1b6/3b4/8/2R5 w - - 0 1"
  )

  def randomPiece = rookAndBishopFens(Random.nextInt(rookAndBishopFens.length))

  def default(source: Source) = InterestVariantSearchData(
    rookColor = "white".some,
    color = "black".some,
    level = "6-5000".some,
    situation = "8pawn".some,
    fen = if (source == Source.PawnsOnly) "8/pppppppp/8/8/8/8/PPPPPPPP/8 w - - 0 1".some else s"${randomPiece}".some
  )

}

case class FinishData(
    id: Int,
    nbMoves: Int,
    score: Int,
    stars: Int,
    success: Boolean,
    lines: List[String],
    squares: List[String]
)

case class LikedData(
    emptyTag: Option[String] = None,
    tags: Option[List[String]] = None
)

case class ResourceSearch(
    role: Option[String],
    color: Option[String],
    phase: Option[String],
    stepMin: Option[Int],
    stepMax: Option[Int]
) {

  def verifyStep =
    (stepMin |@| stepMax).tupled map {
      case (min, max) => min <= max
    } getOrElse (true)

  def toUrlParam =
    role.??(r => s"&role=$r") +
      color.??(c => s"&color=$c") +
      phase.??(p => s"&phase=$p")
}

object ResourceSearch {

  def default(source: Source) =
    ResourceSearch(
      role = if (source == Source.UnprotectedPieces || source == Source.UnprotectedSquares) none else "n".some,
      color = "white".some,
      phase = "".some,
      stepMin = 1.some,
      stepMax = source.max.some
    )
}
