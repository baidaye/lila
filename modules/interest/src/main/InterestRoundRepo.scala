package lila.interest

import lila.user.User
import lila.db.dsl._

object InterestRoundRepo {

  private lazy val coll = Env.current.CollRound

  import BSONHandlers.RoundHandler

  def insert(id: ID, source: Source, userId: User.ID, data: FinishData): Funit =
    coll.insert(
      InterestRound.make(
        interestId = id,
        userId = userId,
        nbMoves = data.nbMoves,
        score = data.score,
        stars = data.stars,
        success = data.success,
        lines = data.lines,
        squares = data.squares,
        source = source
      )
    ).void

}
