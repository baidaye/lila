package lila.interest

import chess.Situation
import chess.format.Forsyth

case class InterestData(
    _id: ID,
    fen: String,
    role: chess.Role,
    color: chess.Color,
    square: String,
    kingSquare: Option[String], // g3 has
    path: List[String],
    uciPath: List[String],
    rightPiece: Option[List[chess.Role]], // g4 has
    rightSquare: Option[List[String]], // g4,g5 has
    wrongSquare: Option[List[String]], // g4,g5 has
    phase: Option[String], // g4,g5 has
    steps: Int,
    source: Source,
    attempts: Int,
    likes: Int,
    enabled: Boolean
) {

  def id = _id

  def colorKingSquare(): Option[String] = (Forsyth << fen) map (_.copy(color = color).kingPos.map(_.toString) | "")

}

object InterestData {

  def default(sc: Source) =
    InterestData(
      _id = 0,
      fen = "8/8/8/8/8/8/8/8 w - -",
      role = chess.King,
      color = chess.Color.White,
      square = "",
      kingSquare = "".some,
      path = Nil,
      uciPath = Nil,
      rightPiece = None,
      rightSquare = None,
      wrongSquare = None,
      phase = None,
      steps = 0,
      source = sc,
      attempts = 0,
      likes = 0,
      enabled = true
    )

}
