package lila.bookmark

import org.joda.time.DateTime

case class BookmarkMode(
    _id: String,
    g: String,
    u: String,
    t: Option[List[String]],
    d: DateTime
)
