package lila.bookmark

import lila.common.paginator._
import lila.db.dsl._
import lila.game.GameRepo
import lila.user.User
import reactivemongo.bson.BSONDocument

private[bookmark] final class PaginatorBuilder(
    coll: Coll,
    maxPerPage: lila.common.MaxPerPage
) {

  def byUser(user: User, page: Int, emptyTag: Option[String], tags: Option[List[String]] = None): Fu[Paginator[Bookmark]] =
    paginator(new UserAdapter(user, emptyTag, tags), page)

  private def paginator(adapter: AdapterLike[Bookmark], page: Int): Fu[Paginator[Bookmark]] =
    Paginator(
      adapter,
      currentPage = page,
      maxPerPage = maxPerPage
    )

  final class UserAdapter(user: User, emptyTag: Option[String], tags: Option[List[String]] = None) extends AdapterLike[Bookmark] {

    def nbResults: Fu[Int] = coll countSel selector

    def slice(offset: Int, length: Int): Fu[Seq[Bookmark]] = for {
      gameIds ← coll.find(selector, $doc("g" -> true))
        .sort(sorting)
        .skip(offset)
        .cursor[Bdoc]()
        .gather[List](length) map { _ flatMap { _.getAs[String]("g") } }
      games ← GameRepo gamesFromSecondary gameIds
    } yield games map { g => Bookmark(g, user) }

    private def selector = {
      var condition = $doc("u" -> user.id)
      var $andConditions = List.empty[BSONDocument]
      tags foreach { tg =>
        $andConditions = $andConditions ++ tg.map(t => $doc("t" -> t))
      }
      emptyTag foreach { tg =>
        if (tg == "on") {
          $andConditions = $andConditions :+ $or($doc("t" $exists false), $doc("t.0" $exists false))
        }
      }
      condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
      condition
    }
    private def sorting = $sort desc "d"
  }
}
