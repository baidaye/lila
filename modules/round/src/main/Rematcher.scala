package lila.round

import chess.format.Forsyth
import chess.variant._
import chess.{ Board, Castles, Clock, Situation, Color => ChessColor, Game => ChessGame }
import ChessColor.{ Black, White }
import akka.actor.ActorSelection
import com.github.blemale.scaffeine.{ Cache, Scaffeine }

import scala.concurrent.duration._
import lila.game.{ AnonCookie, Event, Game, GameRepo, PerfPicker, PgnSetup, Pov, Progress, Source }
import lila.hub.actorApi.member.IsAccept
import lila.memo.ExpireSetMemo
import lila.user.{ User, UserRepo }
import makeTimeout.large
import akka.pattern.ask

private[round] final class Rematcher(
    messenger: Messenger,
    onStart: String => Unit,
    rematch960Cache: ExpireSetMemo,
    memberActor: ActorSelection,
    bus: lila.common.Bus
) {

  private val rematchCreated: Cache[Game.ID, Game.ID] = Scaffeine()
    .expireAfterWrite(1 minute)
    .build[Game.ID, Game.ID]

  def yes(pov: Pov)(implicit proxy: GameProxy): Fu[Events] = pov match {
    case Pov(game, color) if game playerCanRematch color =>
      if (game.opponent(color).isOfferingRematch || game.opponent(color).isAi)
        if (game.opponent(color).isHuman) {
          game.next.fold(rematchJoin(pov))(rematchExists(pov))
        } else {
          pov.player.userId match {
            case None => fuccess(List(Event.NotAccept))
            case Some(userId) => {
              isMemberAccept(userId) flatMap { accept =>
                if (accept) {
                  game.next.fold(rematchJoin(pov))(rematchExists(pov))
                } else {
                  fuccess(List(Event.NotAccept))
                }
              }
            }
          }
        }
      else rematchCreate(pov)
    case _ => fuccess(List(Event.ReloadOwner))
  }

  def no(pov: Pov)(implicit proxy: GameProxy): Fu[Events] = pov match {
    case Pov(game, color) if pov.player.isOfferingRematch => proxy.save {
      messenger.system(game, _.rematchOfferCanceled)
      Progress(game) map { g => g.updatePlayer(color, _.removeRematchOffer) }
    } inject List(Event.RematchOffer(by = none))
    case Pov(game, color) if pov.opponent.isOfferingRematch => proxy.save {
      messenger.system(game, _.rematchOfferDeclined)
      Progress(game) map { g => g.updatePlayer(!color, _.removeRematchOffer) }
    } inject List(Event.RematchOffer(by = none))
    case _ => fuccess(List(Event.ReloadOwner))
  }

  private def rematchExists(pov: Pov)(nextId: String): Fu[Events] =
    GameRepo game nextId flatMap {
      _.fold(rematchJoin(pov))(g => fuccess(redirectEvents(g)))
    }

  private def rematchJoin(pov: Pov): Fu[Events] =
    rematchCreated.getIfPresent(pov.gameId) match {
      case None => for {
        nextGame ← returnGame(pov) map (_.start)
        _ = rematchCreated.put(pov.gameId, nextGame.id)
        _ ← (GameRepo insertDenormalized nextGame) >>
          GameRepo.saveNext(pov.game, nextGame.id) >>-
          messenger.system(pov.game, _.rematchOfferAccepted) >>- {
            if (pov.game.variant == Chess960 && !rematch960Cache.get(pov.gameId))
              rematch960Cache.put(nextGame.id)
          }
      } yield {
        onStart(nextGame.id)
        redirectEvents(nextGame)
      }
      case Some(rematchId) => GameRepo game rematchId map { _ ?? redirectEvents }
    }

  private def rematchCreate(pov: Pov)(implicit proxy: GameProxy): Fu[Events] = proxy.save {
    messenger.system(pov.game, _.rematchOfferSent)
    pov.opponent.userId foreach { forId =>
      bus.publish(lila.hub.actorApi.round.RematchOffer(pov.gameId), Symbol(s"rematchFor:$forId"))
    }
    Progress(pov.game) map { g => g.updatePlayer(pov.color, _ offerRematch) }
  } inject List(Event.RematchOffer(by = pov.color.some))

  private def returnGame(pov: Pov): Fu[Game] = for {
    initialFen <- GameRepo initialFen pov.game
    situation = initialFen flatMap { fen => Forsyth <<< fen.value }
    pieces = pov.game.variant match {
      case Chess960 =>
        if (rematch960Cache get pov.gameId) Chess960.pieces
        else situation.fold(Chess960.pieces)(_.situation.board.pieces)
      case FromPosition | FromPgn | FromOpening => situation.fold(Standard.pieces)(_.situation.board.pieces)
      case variant => variant.pieces
    }
    users <- UserRepo byIds pov.game.userIds
    game <- Game.make(
      chess = ChessGame(
        situation = Situation(
          board = Board(pieces, variant = pov.game.variant).withCastles {
            situation.fold(Castles.init)(_.situation.board.history.castles)
          },
          color = situation.fold[chess.Color](White)(_.situation.color)
        ),
        clock = pov.game.clock map { c => Clock(c.config) },
        turns = situation ?? (_.turns),
        startedAtTurn =
          if (pov.game.fromPgn) pov.game.chess.startedAtTurn
          else if (pov.game.fromOpening) (situation.map(_.turns) | 0) + 1
          else situation ?? (_.turns)
      ),
      whitePlayer = returnPlayer(pov.game, White, users),
      blackPlayer = returnPlayer(pov.game, Black, users),
      mode = if (users.exists(_.lame)) chess.Mode.Casual else pov.game.mode,
      source = pov.game.source | Source.Lobby,
      daysPerTurn = pov.game.daysPerTurn,
      canTakeback = pov.game.canTakeback,
      initialPgn = if (pov.game.fromOpening) pov.game.initialPgn.map(_.copy(lastPly = (situation.map(_.turns) | 0) + 1)) else pov.game.initialPgn,
      pgnImport = None
    ).withUniqueId map { g => if (pov.game.isPuzzle) { g.withPuzzleId(pov.game.metadata.puzzleId.get) } else g }
  } yield game

  private def returnPlayer(game: Game, color: ChessColor, users: List[User]): lila.game.Player =
    if (game.isPuzzle) {
      game.player(color).aiLevel match {
        case Some(ai) => lila.game.Player.make(color, ai.some)
        case None => lila.game.Player.make(
          color,
          game.player(color).userId.flatMap { id => users.find(_.id == id) },
          PerfPicker.mainOrDefault(game)
        )
      }
    } else {
      game.opponent(color).aiLevel match {
        case Some(ai) => lila.game.Player.make(color, ai.some)
        case None => lila.game.Player.make(
          color,
          game.opponent(color).userId.flatMap { id => users.find(_.id == id) },
          PerfPicker.mainOrDefault(game)
        )
      }
    }

  private def redirectEvents(game: Game): Events = {
    val whiteId = game fullIdOf White
    val blackId = game fullIdOf Black
    List(
      Event.RedirectOwner(White, blackId, AnonCookie.json(game, Black)),
      Event.RedirectOwner(Black, whiteId, AnonCookie.json(game, White)),
      // tell spectators about the rematch
      Event.RematchTaken(game.id)
    )
  }

  def isMemberAccept(userId: User.ID): Fu[Boolean] =
    (memberActor ? IsAccept(userId, "AiPlay")).mapTo[Boolean]
}
