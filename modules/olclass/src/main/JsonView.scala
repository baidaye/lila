package lila.olclass

import chess.Pos
import chess.format.FEN
import lila.game.Pov
import lila.pref.Pref
import lila.socket.Socket.Sri
import lila.study.{ Node, Path, PgnDump }
import play.api.libs.json._
import lila.tree.Node.{ Shape, partitionTreeJsonWriter }

final class JsonView(animationDuration: scala.concurrent.duration.Duration, roundJsonView: lila.round.JsonView) {

  import JsonView._

  def pref(pref: lila.pref.Pref) =
    Json.obj(
      "animationDuration" -> pref.animationFactor * animationDuration.toMillis,
      "coords" -> pref.coords,
      "moveEvent" -> pref.moveEvent,
      "resizeHandle" -> pref.resizeHandle
    ).add("rookCastle" -> (pref.rookCastle == Pref.RookCastle.YES))
      .add("is3d" -> pref.is3d)
      .add("highlight" -> (pref.highlight || pref.isBlindfold))
      .add("destination" -> (pref.destination && !pref.isBlindfold))

  def olClassJs(olClass: OlClass) =
    Json.obj(
      "id" -> olClass.id,
      "status" -> olClass.status.id,
      "position" -> olClass.positionOrEmpty,
      "seconds" -> olClass.durationSeconds,
      "muteAll" -> olClass.muteAll,
      "syncs" -> olClass.syncs,
      "handsUp" -> olClass.handsUp,
      "speakers" -> olClass.speakers
    )

  def clazz(cls: lila.clazz.Clazz) =
    Json.obj(
      "id" -> cls.id,
      "name" -> cls.name,
      "coach" -> cls.coach,
      "students" -> cls.studentIds
    )

  def course(course: lila.clazz.Course) =
    Json.obj(
      "id" -> course.id,
      "index" -> course.index,
      "dateStartTime" -> course.dateTimeStr,
      "dateEndTime" -> course.dateEndTime
    )

  def courseWares(list: List[CourseWareMetadata]) =
    JsArray(
      list.map { cw =>
        Json.obj(
          "id" -> cw.id,
          "olClassId" -> cw.olClassId,
          "name" -> cw.name,
          "order" -> cw.order,
          "source" -> cw.source.id,
          "showTree" -> (cw.showTree | true),
          "orientation" -> (cw.orientation.map(o => chess.Color.apply(o) | chess.Color.White) | chess.Color.White),
          "note" -> cw.note
        )
      }
    )

  implicit val pgnTagWrites: Writes[chess.format.pgn.Tag] = Writes[chess.format.pgn.Tag] { t =>
    Json.arr(t.name.toString, t.value)
  }
  implicit val pgnTagsWrites = Writes[chess.format.pgn.Tags] { tags =>
    JsArray(tags.value map pgnTagWrites.writes)
  }
  def courseWare(cw: CourseWare, pov: Pov, pref: lila.pref.Pref) = {
    Json.obj(
      "id" -> cw.id,
      "name" -> cw.name,
      "order" -> cw.order,
      "source" -> cw.source.id,
      "tags" -> cw.tags,
      "showTree" -> (cw.showTree | true),
      "note" -> cw.note
    ) ++ Json.obj(
        "round" -> {
          roundJsonView.userAnalysisJson(
            pov = pov,
            pref = pref,
            initialFen = cw.root.fen.some,
            orientation = cw.orientation.map(o => chess.Color.apply(o) | chess.Color.White) | chess.Color.White,
            owner = false,
            me = none
          ) ++ Json.obj(
            "initPly" -> cw.initPly,
            "treeParts" -> partitionTreeJsonWriter.writes {
              lila.study.TreeBuilder(cw.root, pov.game.variant)
            }
          )
        }
      )
  }

  def courseWareInfo(cw: CourseWare) =
    Json.obj(
      "name" -> s"${cw.name}",
      "color" -> (cw.orientation | "white"),
      "pgn" -> CourseWarePgnDump.toPgn(cw).toString()
    ) ++ turns(cw.root)

  def turns(root: Node.Root) =
    Json.obj(
      "root" -> root.fen.value,
      "moves" -> JsArray(
        PgnDump.toTurnsWithNode(root) map { twn =>
          Json.obj(
            "index" -> twn.number,
            "white" -> twn.white.map { w =>
              Json.obj(
                "san" -> w.node.move.san,
                "uci" -> w.node.move.uci.uci,
                "fen" -> w.node.fen.value
              )
            },
            "black" -> twn.black.map { b =>
              Json.obj(
                "san" -> b.node.move.san,
                "uci" -> b.node.move.uci.uci,
                "fen" -> b.node.fen.value
              )
            }
          )
        }
      )
    )
}

object JsonView {

  implicit val posReader: Reads[Pos] = Reads[Pos] { v =>
    (v.asOpt[String] flatMap Pos.posAt).fold[JsResult[Pos]](JsError(Nil))(JsSuccess(_))
  }
  implicit val pathWrites: Writes[Path] = Writes[Path] { p =>
    JsString(p.toString)
  }
  implicit val colorWriter: Writes[chess.Color] = Writes[chess.Color] { c =>
    JsString(c.name)
  }
  implicit val fenWriter: Writes[FEN] = Writes[FEN] { f =>
    JsString(f.value)
  }
  implicit val sriWriter: Writes[Sri] = Writes[Sri] { sri =>
    JsString(sri.value)
  }

  implicit val shapeReader: Reads[Shape] = Reads[Shape] { js =>
    js.asOpt[JsObject].flatMap { o =>
      for {
        brush <- o str "brush"
        orig <- o.get[Pos]("orig")
      } yield o.get[Pos]("dest") match {
        case Some(dest) => Shape.Arrow(brush, orig, dest)
        case _ => Shape.Circle(brush, orig)
      }
    }.fold[JsResult[Shape]](JsError(Nil))(JsSuccess(_))
  }

  implicit val variantWrites = OWrites[chess.variant.Variant] { v =>
    Json.obj("key" -> v.key, "name" -> v.name)
  }

  implicit val positionRefWrites: Writes[Position.Ref] = Json.writes[Position.Ref]

  implicit val pgnTagWrites: Writes[chess.format.pgn.Tag] = Writes[chess.format.pgn.Tag] { t =>
    Json.arr(t.name.toString, t.value)
  }
  implicit val pgnTagsWrites = Writes[chess.format.pgn.Tags] { tags =>
    JsArray(tags.value map pgnTagWrites.writes)
  }
}

