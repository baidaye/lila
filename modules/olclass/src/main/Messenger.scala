package lila.olclass

import lila.chat.Chat
import lila.chat.actorApi._
import akka.actor.ActorSelection

final class Messenger(val chat: ActorSelection) {

  def system(chatId: OlClass.ID, message: String): Unit = {
    chat ! SystemTalk(Chat.Id(chatId), message)
  }
}
