package lila.olclass

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class OlClassJoinLog(
    _id: OlClassJoinLog.ID,
    olClassId: OlClass.ID,
    userId: User.ID,
    joinAt: DateTime,
    leaveAt: Option[DateTime]
) {

  def id = _id

}

object OlClassJoinLog {

  type ID = String

  def make(olClassId: OlClass.ID, userId: User.ID) =
    OlClassJoinLog(
      _id = Random nextString 8,
      olClassId = olClassId,
      userId = userId,
      joinAt = DateTime.now,
      leaveAt = None
    )
}
