package lila.olclass

import chess.Centis
import chess.format.pgn.{ Glyph, Tags }
import chess.opening.{ FullOpening, FullOpeningDB }
import chess.variant.{ Standard, Variant }
import lila.study.{ Node, Path }
import lila.tree.Node.{ Comment, Gamebook, Shapes }
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class CourseWare(
    _id: CourseWare.ID,
    olClassId: OlClass.ID,
    name: String,
    order: Int,
    source: CourseWare.Source,
    root: Node.Root,
    tags: Tags,
    showTree: Option[Boolean],
    orientation: Option[String],
    initPly: Option[Int],
    setup: Option[CourseWareSetup],
    note: Option[String],
    createBy: User.ID,
    createAt: DateTime
) {

  def id = _id

  def isCreator(userId: User.ID) = userId == createBy

  def updateRoot(f: Node.Root => Option[Node.Root]) =
    f(root) map { newRoot =>
      copy(root = newRoot)
    }

  def addNode(node: Node, path: Path): Option[CourseWare] =
    updateRoot {
      _.withChildren(_.addNodeAt(node, path))
    }

  def updateNode(node: Node, path: Path): Option[CourseWare] =
    updateRoot {
      _.withChildren(_.updateAt(path, _ => node))
    }

  def setShapes(shapes: Shapes, path: Path): Option[CourseWare] =
    updateRoot(_.setShapesAt(shapes, path))

  def setComment(comment: Comment, path: Path): Option[CourseWare] =
    updateRoot(_.setCommentAt(comment, path))

  def setGamebook(gamebook: Gamebook, path: Path): Option[CourseWare] =
    updateRoot(_.setGamebookAt(gamebook, path))

  def deleteComment(commentId: Comment.Id, path: Path): Option[CourseWare] =
    updateRoot(_.deleteCommentAt(commentId, path))

  def toggleGlyph(glyph: Glyph, path: Path): Option[CourseWare] =
    updateRoot(_.toggleGlyphAt(glyph, path))

  def setClock(clock: Option[Centis], path: Path): Option[CourseWare] =
    updateRoot(_.setClockAt(clock, path))

  def forceVariation(force: Boolean, path: Path): Option[CourseWare] =
    updateRoot(_.forceVariationAt(force, path))

  def opening: Option[FullOpening] =
    if (!Variant.openingSensibleVariants(Standard)) none
    else FullOpeningDB searchInFens root.mainline.map(_.fen)

  def isEmptyInitial = order == 1 && root.children.nodes.isEmpty

  def withoutChildren = copy(root = root.withoutChildren)

  def isOverweight = root.children.countRecursive >= CourseWare.maxNodes

}

object CourseWare {

  type ID = String

  val maxNodes = 3000
  val MaxSize = 100

  def make(
    olClassId: OlClass.ID,
    name: String,
    order: Int,
    source: CourseWare.Source,
    root: Node.Root,
    tags: Tags,
    orientation: Option[String] = None,
    showTree: Option[Boolean] = None,
    initPly: Option[Int] = None,
    setup: Option[CourseWareSetup],
    note: Option[String] = None,
    createBy: User.ID
  ) = CourseWare(
    _id = Random nextString 8,
    olClassId = olClassId,
    name = name,
    order = order,
    source = source,
    root: Node.Root,
    tags: Tags,
    orientation = orientation,
    showTree = showTree,
    initPly = initPly,
    setup = setup,
    note = note,
    createBy = createBy,
    createAt = DateTime.now
  )

  sealed abstract class Source(val id: String, val name: String)
  object Source {
    case object Study extends Source("study", "研习")
    case object Contest extends Source("contest", "比赛")
    case object Gamedb extends Source("gamedb", "对局库")
    case object Situationdb extends Source("situationdb", "局面库")
    case object Capsule extends Source("capsule", "战术题列表")
    case object Puzzle extends Source("puzzle", "战术题")
    case object HomeworkPuzzle extends Source("homeworkPuzzle", "课后练·战术题")
    case object HomeworkReplayGame extends Source("homeworkReplayGame", "课后练·打谱")
    case object HomeworkRecallGame extends Source("homeworkRecallGame", "课后练·记谱")
    case object HomeworkDistinguishGame extends Source("homeworkDistinguishGame", "课后练·棋谱记录")
    case object Editor extends Source("editor", "编辑器")
    case object Game extends Source("game", "对局")
    case object Fen extends Source("fen", "FEN")
    case object Pgn extends Source("pgn", "PGN")

    val all = List(Study, Contest, Gamedb, Situationdb, Capsule, Puzzle, HomeworkPuzzle, HomeworkReplayGame, HomeworkRecallGame, HomeworkDistinguishGame, Editor, Game, Fen, Pgn)

    def apply(id: String) = all.find(_.id == id) err s"Bad Source $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => r.id -> r.name }

    def byId = all.map { x => x.id -> x }.toMap

  }

}

case class CourseWareSetup(
    chapterId: Option[String] = None,
    boardId: Option[String] = None,
    gamedbRelId: Option[String] = None,
    situationdbRelId: Option[String] = None,
    capsuleId: Option[String] = None,
    homeworkId: Option[String] = None,
    puzzleId: Option[String] = None,
    gameUrl: Option[String] = None
)

case class CourseWareMetadata(
    _id: CourseWare.ID,
    olClassId: OlClass.ID,
    name: String,
    order: Int,
    source: CourseWare.Source,
    orientation: Option[String],
    showTree: Option[Boolean],
    initPly: Option[Int],
    note: Option[String]
) {

  def id = _id

  def likePgn =
    source == CourseWare.Source.Study ||
      source == CourseWare.Source.HomeworkReplayGame ||
      source == CourseWare.Source.HomeworkRecallGame ||
      source == CourseWare.Source.HomeworkDistinguishGame ||
      source == CourseWare.Source.Contest ||
      source == CourseWare.Source.Gamedb ||
      source == CourseWare.Source.Game ||
      source == CourseWare.Source.Pgn

}
