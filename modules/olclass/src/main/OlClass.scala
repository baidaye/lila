package lila.olclass

import lila.clazz.Clazz
import lila.user.User
import org.joda.time.DateTime

case class OlClass(
    _id: OlClass.ID, // Course.ID
    clazzId: Clazz.ID,
    position: Option[Position.Ref],
    live: Option[LiveRecord],
    status: OlClass.Status,
    opened: Boolean,
    coach: User.ID,
    createdAt: DateTime,
    updatedAt: DateTime
) {

  def id = _id

  def isCreated = status == OlClass.Status.Created
  def isStarted = status == OlClass.Status.Started
  def isStopped = status == OlClass.Status.Stopped

  def positionOrEmpty = position | Position.Ref("", lila.study.Path.root)

  def muteAll = live.map(_.muteAll) | true
  def syncs = live.??(_.syncs)
  def handsUp = live.??(_.handsUp)
  def speakers = live.??(_.speakers)
  def durationSeconds = live.??(_.durationSeconds)

  def isHost(userId: User.ID) = userId == coach

  def open(o: Boolean) =
    copy(opened = o)

  def start() =
    if (isCreated) {
      copy(
        live = LiveRecord(
          startAt = DateTime.now,
          stopAt = None,
          muteAll = true,
          joins = Nil,
          hisJoins = Nil,
          syncs = Nil,
          handsUp = Nil,
          speakers = Nil
        ).some,
        status = OlClass.Status.Started,
        updatedAt = DateTime.now
      )
    } else this

  def stop() =
    if (isStarted) {
      copy(
        live = live.map(_.stop()),
        status = OlClass.Status.Stopped,
        updatedAt = DateTime.now
      )
    } else this

  def leave(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.leave(userId))
      )
    } else this

  def isJoined(userId: User.ID) =
    live.??(_.joins.contains(userId))

  def join(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.join(userId))
      )
    } else this

  def muteAll(m: Boolean) =
    if (isStarted) {
      copy(
        live = live.map(_.muteAll(m))
      )
    } else this

  def isSync(userId: User.ID) =
    live.??(_.syncs.contains(userId))

  def addSync(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.addSync(userId))
      )
    } else this

  def removeSync(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.removeSync(userId))
      )
    } else this

  def isHandUp(userId: User.ID) =
    live.??(_.handsUp.contains(userId))

  def addHandUp(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.addHandUp(userId))
      )
    } else this

  def removeHandUp(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.removeHandUp(userId))
      )
    } else this

  def isSpeaking(userId: User.ID) =
    live.??(_.speakers.contains(userId))

  def addSpeaker(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.addSpeaker(userId))
      )
    } else this

  def removeSpeaker(userId: User.ID) =
    if (isStarted) {
      copy(
        live = live.map(_.removeSpeaker(userId))
      )
    } else this

}

object OlClass {

  type ID = String

  case class WithCourseWare(olClass: OlClass, courseWare: CourseWare)

  def make(
    clazzId: String,
    courseId: String,
    coach: User.ID
  ) = {
    OlClass(
      _id = courseId,
      clazzId = clazzId,
      status = OlClass.Status.Created,
      opened = false,
      position = None,
      live = None,
      coach = coach,
      createdAt = DateTime.now,
      updatedAt = DateTime.now
    )
  }

  sealed abstract class Status(val id: String, val name: String)
  object Status {
    case object Created extends Status("created", "未开课")
    case object Started extends Status("started", "上课中")
    case object Stopped extends Status("stopped", "已结束")

    val all = List(Created, Started, Stopped)

    def apply(id: String) = all.find(_.id == id) err s"Bad Status $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => r.id -> r.name }

    def byId = all.map { x => x.id -> x }.toMap
  }

}

case class LiveRecord(
    startAt: DateTime,
    stopAt: Option[DateTime],
    muteAll: Boolean,
    joins: List[User.ID],
    hisJoins: List[User.ID],
    syncs: List[User.ID],
    handsUp: List[User.ID],
    speakers: List[User.ID]
) {
  def durationSeconds = durationMillis / 1000
  def durationMillis = stopAt.map { stopAt =>
    stopAt.getMillis - startAt.getMillis
  } | DateTime.now.getMillis - startAt.getMillis

  def stop(): LiveRecord = copy(stopAt = DateTime.now.some, joins = Nil, muteAll = false, syncs = Nil, handsUp = Nil, speakers = Nil)

  def join(userId: User.ID): LiveRecord = copy(
    joins = joins :+ userId,
    hisJoins = if (hisJoins.contains(userId)) hisJoins else hisJoins :+ userId
  )

  def leave(userId: User.ID): LiveRecord = {
    val newLive = copy(
      joins = joins.filterNot(_ == userId),
      syncs = syncs.filterNot(_ == userId),
      handsUp = handsUp.filterNot(_ == userId),
      speakers = speakers.filterNot(_ == userId)
    )
    newLive
  }

  def muteAll(mute: Boolean): LiveRecord = copy(muteAll = mute)

  def addSync(userId: User.ID): LiveRecord = copy(syncs = syncs :+ userId)
  def removeSync(userId: User.ID): LiveRecord = copy(syncs = syncs.filterNot(_ == userId))

  def addHandUp(userId: User.ID): LiveRecord = copy(handsUp = handsUp :+ userId)
  def removeHandUp(userId: User.ID): LiveRecord = copy(handsUp = handsUp.filterNot(_ == userId))

  def addSpeaker(userId: User.ID): LiveRecord = copy(speakers = speakers :+ userId)
  def removeSpeaker(userId: User.ID): LiveRecord = copy(speakers = speakers.filterNot(_ == userId))
}
