package lila.olclass

import lila.study.{ Path, Node, RootOrNode }

case class Position(cw: CourseWare, path: Path) {

  def ref = Position.Ref(cw.id, path)

  def node: Option[RootOrNode] = cw.root nodeAt path

  override def toString = ref.toString
}

case object Position {

  case class Ref(courseWareId: CourseWare.ID, path: Path) {

    def encode = s"$courseWareId $path"

    def +(node: Node) = copy(path = path + node)

    def withPath(p: Path) = copy(path = p)
  }

  object Ref {

    def decode(str: String): Option[Ref] = str.split(' ') match {
      case Array(courseWareId, path) => Ref(courseWareId, Path(path)).some
      case Array(courseWareId) => Ref(courseWareId, Path.root).some
      case _ => none
    }
  }
}
