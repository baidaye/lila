package lila.olclass

import chess.format.pgn._
import chess.variant.Standard
import lila.socket.Socket.Sri
import lila.study.{ MoveOpts, Node, Path }
import lila.tree.Node.{ Comment, Shapes }
import lila.user.User

class CourseWareApi(
    socketMap: SocketMap,
    maker: CourseWareMaker,
    sequencer: OlClassSequencer,
    fetchOlClass: OlClass.ID => Fu[Option[OlClass]],
    lightUser: lila.common.LightUser.GetterSync
) {

  def fetchPgn(olClassId: OlClass.ID, id: CourseWare.ID): Fu[Option[(String, String)]] =
    CourseWareRepo.byIdAndOlClass(olClassId, id) map {
      case None => none[(String, String)]
      case Some(courseWare) => (courseWare.name, CourseWarePgnDump.toPgn(courseWare).toString).some
    }

  def orderedMetadataByOlClass(olClassId: OlClass.ID): Fu[List[CourseWareMetadata]] =
    CourseWareRepo.orderedMetadataByOlClass(olClassId)

  def updateCourseWare(id: CourseWare.ID, data: CourseWareEdit): Funit =
    CourseWareRepo.updateFields(id, data)

  def deleteCourseWare(olClass: OlClass, id: CourseWare.ID): Funit =
    CourseWareRepo.byIdAndOlClass(olClass.id, id) map {
      _.?? { courseWare =>
        CourseWareRepo.remove(id) >> CourseWareRepo.updateOrder(olClass.id, courseWare.order, -1)
      }
    }

  def sortCourseWare(olClassId: OlClass.ID, ids: List[String]): Funit =
    CourseWareRepo.sort(olClassId, ids)

  def setNote(id: CourseWare.ID, note: Option[String]): Funit =
    CourseWareRepo.setNote(id, note)

  def nextOrder(olClassId: OlClass.ID, data: CourseWareCreate) = {
    data.prevOrder match {
      case None => CourseWareRepo.nextOrderByOlClass(olClassId)
      case Some(prevOrder) => fuccess(prevOrder + 1)
    }
  }

  def addCourseWare(olClass: OlClass, data: CourseWareCreate): Funit =
    isOverMax(olClass.id, data) flatMap {
      case true => {
        nextOrder(olClass.id, data) flatMap { order =>
          maker(olClass, data, order) flatMap { list =>
            updateOrder(olClass.id, data, list.size) >> CourseWareRepo.bulkInsert(list)
          }
        }
      }
      case false => funit
    }

  def updateOrder(olClassId: OlClass.ID, data: CourseWareCreate, inc: Int): Funit = {
    data.prevOrder match {
      case None => funit
      case Some(prevOrder) => CourseWareRepo.updateOrder(olClassId, prevOrder, inc)
    }
  }

  private def isOverMax(olClassId: OlClass.ID, data: CourseWareCreate): Fu[Boolean] = {
    CourseWareRepo.countByOlClass(olClassId) flatMap { count =>
      data.realSource() match {
        case CourseWare.Source.Study => fuccess((data.study.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.Contest => fuccess((data.contest.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.Gamedb => fuccess((data.gamedb.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.Situationdb => fuccess((data.situationdb.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.Capsule => fuccess((data.capsule.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.HomeworkPuzzle => fuccess((data.homeworkPuzzle.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.HomeworkReplayGame => fuccess((data.homeworkReplayGame.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.HomeworkRecallGame => fuccess((data.homeworkRecallGame.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.HomeworkDistinguishGame => fuccess((data.homeworkDistinguishGame.map(_.length) | 0) + count <= CourseWare.MaxSize)
        case CourseWare.Source.Puzzle => fuccess(data.puzzleIds.size + count <= CourseWare.MaxSize)
        case CourseWare.Source.Editor | CourseWare.Source.Game | CourseWare.Source.Fen | CourseWare.Source.Pgn => fuccess(count + 1 <= CourseWare.MaxSize)
        case _ => fuFalse
      }
    }
  }

  def setCourseWare(byUserId: User.ID, olClassId: OlClass.ID, courseWareId: CourseWare.ID, sri: Sri): Funit =
    sequencer.sequenceOlClass(olClassId)(fetchOlClass) { olClass =>
      Contribute(byUserId, olClass) {
        doSetCourseWare(olClass, courseWareId, sri)
      }
    }

  private def doSetCourseWare(olClass: OlClass, courseWareId: CourseWare.ID, sri: Sri): Funit =
    (olClass.positionOrEmpty.courseWareId != courseWareId) ?? {
      CourseWareRepo.byIdAndOlClass(olClass.id, courseWareId) flatMap {
        _ ?? { courseWare =>
          val position = Position.Ref(courseWareId = courseWare.id, path = Path.root)
          OlClassRepo.setPosition(olClass.id, position) >>-
            sendTo(olClass, OlClassSocket.ChangeCourseWare(sri, position))
        }
      }
    }

  def setPath(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, sri: Sri): Funit =
    sequencer.sequenceOlClass(olClassId)(fetchOlClass) { olClass =>
      Contribute(userId, olClass) {
        CourseWareRepo.byId(position.courseWareId).map {
          _ filter { c =>
            c.root.pathExists(position.path) && (olClass.positionOrEmpty.courseWareId.isEmpty || olClass.positionOrEmpty.courseWareId == c.id)
          }
        } flatMap {
          case None => funit >>- reloadSri(olClass, sri)
          case Some(_) =>
            (olClass.positionOrEmpty.path != position.path).?? {
              OlClassRepo.setPosition(olClass.id, position)
            } >>- sendTo(olClass, OlClassSocket.SetPath(position, sri))
          case _ => funit
        }
      }
    }

  def addNode(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, node: Node, sri: Sri, opts: MoveOpts): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        doAddNode(userId, olClass, Position(courseWare, position.path), node, sri, opts).void
      }
    }

  private def doAddNode(userId: User.ID, olClass: OlClass, position: Position, rawNode: Node, sri: Sri, opts: MoveOpts): Funit = {
    val node = rawNode.withoutChildren
    def failReload = reloadSriBecauseOf(olClass, sri, position.cw.id)
    if (position.cw.isOverweight) {
      logger.info(s"Overweight courseWare ${olClass.id}/${position.cw.id}")
      fuccess(failReload)
    } else position.cw.addNode(node, position.path) match {
      case None =>
        failReload
        fufail(s"Invalid addNode ${olClass.id} ${position.ref} $node")
      case Some(courseWare) =>
        courseWare.root.nodeAt(position.path) ?? { parent =>
          val newPosition = position.ref + node
          CourseWareRepo.setChildren(courseWare, position.path, parent.children) >>
            (opts.sticky ?? OlClassRepo.setPosition(olClass.id, newPosition)) >>- {
              sendTo(olClass, OlClassSocket.AddNode(
                position.ref,
                node,
                Standard,
                sri,
                sticky = opts.sticky
              ))
              if (opts.promoteToMainline && !Path.isMainline(courseWare.root, newPosition.path))
                promote(userId, olClass.id, position.ref + node, toMainline = true, sri)
            }
        }
    }
  }

  def deleteNodeAt(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, sri: Sri) =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        courseWare.updateRoot { root =>
          root.withChildren(_.deleteNodeAt(position.path))
        } match {
          case Some(newChapter) =>
            CourseWareRepo.update(newChapter) >>-
              sendTo(olClass, OlClassSocket.DeleteNode(position, sri))
          case None =>
            fufail(s"Invalid delNode $olClassId $position") >>-
              reloadSriBecauseOf(olClass, sri, courseWare.id)
        }
      }
    }

  def promote(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, toMainline: Boolean, sri: Sri) =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        courseWare.updateRoot { root =>
          root.withChildren { children =>
            if (toMainline) children.promoteToMainlineAt(position.path)
            else children.promoteUpAt(position.path).map(_._1)
          }
        } match {
          case Some(newChapter) =>
            CourseWareRepo.update(newChapter) >>-
              sendTo(olClass, OlClassSocket.Promote(position, toMainline, sri)) >>
              newChapter.root.children.nodesOn {
                newChapter.root.mainlinePath.intersect(position.path)
              }.collect {
                case (node, path) if node.forceVariation =>
                  doForceVariation(OlClass.WithCourseWare(olClass, newChapter), path, false, Sri(""))
              }.sequenceFu.void
          case None =>
            fufail(s"Invalid promoteToMainline $olClassId $position") >>-
              reloadSriBecauseOf(olClass, sri, courseWare.id)
        }
      }
    }

  def forceVariation(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, force: Boolean, sri: Sri): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) { oc =>
      Contribute(userId, oc.olClass) {
        doForceVariation(oc, position.path, force, sri)
      }
    }

  private def doForceVariation(sc: OlClass.WithCourseWare, path: Path, force: Boolean, sri: Sri): Funit =
    sc.courseWare.forceVariation(force, path) match {
      case Some(newChapter) =>
        CourseWareRepo.forceVariation(newChapter, path, force) >>-
          sendTo(sc.olClass, OlClassSocket.ForceVariation(Position(newChapter, path).ref, force, sri))
      case None =>
        fufail(s"Invalid forceVariation ${Position(sc.courseWare, path)} $force") >>-
          reloadSriBecauseOf(sc.olClass, sri, sc.courseWare.id)
    }

  def setShapes(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, shapes: Shapes, sri: Sri): Funit =
    sequencer.sequenceOlClass(olClassId)(fetchOlClass) { olClass =>
      Contribute(userId, olClass) {
        CourseWareRepo.byId(position.courseWareId) flatMap {
          _ ?? { courseWare =>
            courseWare.setShapes(shapes, position.path) match {
              case Some(newChapter) =>
                OlClassRepo.updateNow(olClass)
                CourseWareRepo.setShapes(newChapter, position.path, shapes) >>-
                  sendTo(olClass, OlClassSocket.SetShapes(position, shapes, sri))
              case None =>
                fufail(s"Invalid setShapes $position $shapes") >>-
                  reloadSriBecauseOf(olClass, sri, courseWare.id)
            }
          }
        }
      }
    }

  def toggleGlyph(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, glyph: Glyph, sri: Sri): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        courseWare.toggleGlyph(glyph, position.path) match {
          case Some(newChapter) =>
            OlClassRepo.updateNow(olClass)
            newChapter.root.nodeAt(position.path) ?? { node =>
              CourseWareRepo.setGlyphs(newChapter, position.path, node.glyphs) >>-
                newChapter.root.nodeAt(position.path).foreach { node =>
                  sendTo(olClass, OlClassSocket.SetGlyphs(position, node.glyphs, sri))
                }
            }
          case None =>
            fufail(s"Invalid toggleGlyph $olClassId $position $glyph") >>-
              reloadSriBecauseOf(olClass, sri, courseWare.id)
        }
      }
    }

  def setComment(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, text: Comment.Text, sri: Sri): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        lightUser(userId) ?? { author =>
          val comment = Comment(
            id = Comment.Id.make,
            text = text,
            by = Comment.Author.User(author.id, author.titleName)
          )
          doSetComment(userId, olClass, Position(courseWare, position.path), comment, sri)
        }
      }
    }

  private def doSetComment(userId: User.ID, olClass: OlClass, position: Position, comment: Comment, sri: Sri): Funit =
    position.cw.setComment(comment, position.path) match {
      case Some(newChapter) =>
        OlClassRepo.updateNow(olClass)
        newChapter.root.nodeAt(position.path) ?? { node =>
          node.comments.findBy(comment.by) ?? { c =>
            CourseWareRepo.setComments(newChapter, position.path, node.comments.filterEmpty) >>- {
              sendTo(olClass, OlClassSocket.SetComment(position.ref, c, sri))
            }
          }
        }
      case None =>
        fufail(s"Invalid setComment ${olClass.id} $position") >>-
          reloadSriBecauseOf(olClass, sri, position.cw.id)
    }

  def deleteComment(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, id: Comment.Id, sri: Sri): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        courseWare.deleteComment(id, position.path) match {
          case Some(newChapter) =>
            CourseWareRepo.update(newChapter) >>-
              sendTo(olClass, OlClassSocket.DeleteComment(position, id, sri))
          case None =>
            fufail(s"Invalid deleteComment $olClassId $position $id") >>-
              reloadSriBecauseOf(olClass, sri, courseWare.id)
        }
      }
    }

  def setTag(userId: User.ID, olClassId: OlClass.ID, courseWareId: CourseWare.ID, tag: chess.format.pgn.Tag, sri: Sri) =
    sequencer.sequenceOlClassWithCourseWare(olClassId, courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, courseWare) => Contribute(userId, olClass) {
        val exists = courseWare.tags.value.exists(_.name == tag.name)
        def newTags = {
          if (exists) {
            Tags(
              courseWare.tags.value.map { t =>
                if (t.name == tag.name) tag else t
              }
            )
          } else {
            courseWare.tags + tag
          }
        }
        doSetTags(olClass, courseWare, newTags, sri)
      }
    }

  private def doSetTags(olClass: OlClass, oldCourseWare: CourseWare, tags: Tags, sri: Sri): Funit = {
    val newCourseWare = oldCourseWare.copy(tags = tags)
    (newCourseWare.tags != oldCourseWare.tags) ?? {
      CourseWareRepo.setTagsFor(newCourseWare) >>-
        sendTo(olClass, OlClassSocket.SetTags(newCourseWare.id, newCourseWare.tags, sri))
    }
  }

  def selectNode(userId: User.ID, olClassId: OlClass.ID, position: Position.Ref, sri: Sri): Funit =
    sequencer.sequenceOlClassWithCourseWare(olClassId, position.courseWareId)(fetchOlClass) {
      case OlClass.WithCourseWare(olClass, _) => Contribute(userId, olClass) {
        fuccess {
          sendTo(olClass, OlClassSocket.SelectNode(position, sri))
        }
      }
    }

  import ornicar.scalalib.Zero
  private def Contribute[A](userId: User.ID, olClass: OlClass)(f: => A)(implicit default: Zero[A]): A =
    if (olClass.isSync(userId) || olClass.isHost(userId)) f else default.zero

  private def reloadSri(olClass: OlClass, sri: Sri, becauseOf: Option[CourseWare.ID] = None) =
    sendTo(olClass, OlClassSocket.ReloadSri(sri))

  private def reloadSriBecauseOf(olClass: OlClass, sri: Sri, courseWareId: CourseWare.ID) =
    sendTo(olClass, OlClassSocket.ReloadSriBecauseOf(sri, courseWareId))

  private def sendTo(olClass: OlClass, msg: Any): Unit = sendTo(olClass.id, msg)

  private def sendTo(olClassId: OlClass.ID, msg: Any): Unit =
    socketMap.tell(olClassId, msg)

}
