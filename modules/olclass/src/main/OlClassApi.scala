package lila.olclass

import akka.actor.ActorSystem
import lila.user.User
import lila.clazz.{ Clazz, Course }
import scala.concurrent.duration._

final class OlClassApi(
    jsonView: JsonView,
    socketMap: SocketMap,
    sequencer: OlClassSequencer,
    bus: lila.common.Bus,
    scheduler: lila.common.Scheduler,
    asyncCache: lila.memo.AsyncCache.Builder,
    isOnline: lila.user.User.ID => Boolean
)(implicit system: ActorSystem) {

  bus.subscribeFuns(
    'olcMemberQuit -> {
      case OlClassSocket.AfterQuit(courseId, userId) => memberQuit(courseId, userId)
    }
  )

  private[olclass] val cache = asyncCache.multi[OlClass.ID, Option[OlClass]](
    name = "olclass",
    f = OlClassRepo.byId,
    expireAfter = _.ExpireAfterWrite(2 hour)
  )

  def findByClass(clazzId: Clazz.ID): Fu[List[OlClass]] =
    OlClassRepo.findByClazz(clazzId)

  def find(id: OlClass.ID): Fu[Option[OlClass]] =
    //cache.get(id)
    OlClassRepo.byId(id)

  def findOrCreate(clazzId: Clazz.ID, courseId: Course.ID, coach: User.ID): Fu[OlClass] =
    find(courseId).flatMap {
      case None => OlClass.make(clazzId, courseId, coach) |> { olClass =>
        OlClassRepo.insert(olClass) >>- cache.refresh(olClass.id) inject olClass
      }
      case Some(olClass) => fuccess(olClass)
    }

  def create(clazzId: Clazz.ID, courseId: Course.ID, coach: User.ID): Fu[OlClass] =
    find(courseId).flatMap {
      case None => OlClass.make(clazzId, courseId, coach) |> { olClass =>
        OlClassRepo.insert(olClass) >>- cache.refresh(olClass.id) inject olClass
      }
      case Some(olClass) => fuccess(olClass)
    }

  def open(id: OlClass.ID, o: Boolean): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      val newOlClass = olClass.open(o)
      OlClassRepo.update(newOlClass) >>-
        cache.refresh(id)
    }

  def start(id: OlClass.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      val newOlClass = olClass.start()
      OlClassRepo.update(newOlClass) >>-
        cache.refresh(id) >>-
        socketMap.tell(id, OlClassSocket.Started(jsonView.olClassJs(newOlClass)))
    }

  def stop(id: OlClass.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      val newOlClass = olClass.stop()
      OlClassRepo.update(newOlClass) >>-
        OlClassJoinLogRepo.setAllLeaveAt(id) >>-
        socketMap.tell(id, OlClassSocket.Stopped(jsonView.olClassJs(newOlClass)))
    }

  def join(id: OlClass.ID, userId: User.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (!olClass.isJoined(userId)) {
        val newOlClass = olClass.join(userId)
        OlClassRepo.update(newOlClass) >>
          OlClassJoinLogRepo.insert(OlClassJoinLog.make(id, userId)) >>-
          cache.refresh(id)
      } else funit
    }

  def leave(id: OlClass.ID, userId: User.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      val isHandUp = olClass.isHandUp(userId)
      val isSpeaking = olClass.isSpeaking(userId)
      val isSync = olClass.isSync(userId)
      val newOlClass = olClass.leave(userId)
      OlClassRepo.update(newOlClass) >>
        setLeaveAt(id, userId) >>-
        cache.refresh(id) >>-
        isSync.?? {
          socketMap.tell(id, OlClassSocket.RemoveSync(userId))
        } >>-
        isHandUp.?? {
          socketMap.tell(id, OlClassSocket.RemoveHandUp(userId))
        } >>-
        isSpeaking.?? {
          socketMap.tell(id, OlClassSocket.RemoveSpeaker(userId))
        }
    }

  private def setLeaveAt(id: OlClass.ID, userId: User.ID): Funit =
    OlClassJoinLogRepo.lastLog(id, userId) flatMap {
      case None => {
        logger.error(s"can not find olClass join log of $id - $userId")
        funit
      }
      case Some(last) => OlClassJoinLogRepo.setLeaveAt(last.id)
    }

  def muteAll(id: OlClass.ID, m: Boolean): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      val newOlClass = olClass.muteAll(m)
      OlClassRepo.update(newOlClass) >>-
        cache.refresh(id) >>-
        socketMap.tell(id, OlClassSocket.MuteAll(jsonView.olClassJs(newOlClass)))
    }

  def addSync(id: OlClass.ID, userId: User.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (!olClass.isSync(userId)) {
        val newOlClass = olClass.addSync(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>-
          socketMap.tell(id, OlClassSocket.AddSync(userId))
      } else funit
    }

  def removeSync(id: OlClass.ID, userId: User.ID): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (olClass.isSync(userId)) {
        val newOlClass = olClass.removeSync(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>- {
            socketMap.tell(id, OlClassSocket.RemoveSync(userId))
          }
      } else funit
    }

  def addHandUp(id: OlClass.ID, userId: String): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (!olClass.isHandUp(userId)) {
        val newOlClass = olClass.addHandUp(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>-
          socketMap.tell(id, OlClassSocket.AddHandUp(userId)) >>-
          scheduler.once(10 seconds) {
            removeHandUp(id, userId)
          }
      } else funit
    }

  def removeHandUp(id: OlClass.ID, userId: String): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (olClass.isHandUp(userId)) {
        val newOlClass = olClass.removeHandUp(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>-
          socketMap.tell(id, OlClassSocket.RemoveHandUp(userId))
      } else funit
    }

  def addSpeaker(id: OlClass.ID, userId: String): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (!olClass.isSpeaking(userId)) {
        val newOlClass = olClass.addSpeaker(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>-
          socketMap.tell(id, OlClassSocket.AddSpeaker(userId))
      } else funit
    }

  def removeSpeaker(id: OlClass.ID, userId: String): Funit =
    sequencer.sequenceOlClass(id)(find) { olClass =>
      if (olClass.isSpeaking(userId)) {
        val newOlClass = olClass.removeSpeaker(userId)
        OlClassRepo.update(newOlClass) >>-
          cache.refresh(id) >>-
          socketMap.tell(id, OlClassSocket.RemoveSpeaker(userId))
      } else funit
    }

  def memberQuit(id: OlClass.ID, userId: User.ID): Funit =
    lila.common.Future.delay(3 minute) {
      find(id) flatMap {
        _.?? { olClass =>
          if (olClass.isStarted && !isOnline(userId)) {
            if (olClass.isHost(userId)) {
              stop(id)
            } else {
              leave(id, userId)
            }
          } else funit
        }
      }
    }

}
