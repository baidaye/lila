package lila.olclass

import lila.user.User
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.Constraints
import play.api.libs.ws.WS
import scala.util.matching.Regex
import lila.common.Form.stringIn
import play.api.Play.current
import lila.study.{ Study, StudyApi }

case class DataForm(studyApi: StudyApi) {

  import DataForm._

  def create(user: User) = Form(mapping(
    "source" -> stringIn(CourseWare.Source.list),
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "orientation" -> optional(nonEmptyText),
    "showTree" -> optional(boolean),
    "prevOrder" -> optional(number(min = 1, max = 100)),
    "study" -> optional(list(createSubMapping)),
    "contest" -> optional(list(createSubMapping)),
    "gamedb" -> optional(list(createSubMapping)),
    "situationdb" -> optional(list(createSubMapping)),
    "capsule" -> optional(list(createSubMapping)),
    "puzzle" -> optional(nonEmptyText.verifying("确保内容不为空并且格式正确", d => puzzleIds(d).nonEmpty)),
    "homeworkPuzzle" -> optional(list(createSubMapping)),
    "homeworkReplayGame" -> optional(list(createSubMapping)),
    "homeworkRecallGame" -> optional(list(createSubMapping)),
    "homeworkDistinguishGame" -> optional(list(createSubMapping)),
    "editor" -> optional(nonEmptyText),
    "game" -> optional(
      nonEmptyText.verifying(Constraints.pattern(regex = gameRegex, error = "对局URL格式错误"))
        .verifying("对局URL无法识别", url => urlTest(url).awaitSeconds(3))
    ),
    "fen" -> optional(nonEmptyText),
    "pgn" -> optional(nonEmptyText)
  )(CourseWareCreate.apply)(CourseWareCreate.unapply)
    .verifying("请正确输入章节内容，检查输入或选择的内容不为空且格式正确", _.checkSource()))

  val createSubMapping = mapping(
    "content" -> nonEmptyText,
    "name" -> optional(nonEmptyText),
    "setup" -> optional(nonEmptyText)
  )(CourseWareSub.apply)(CourseWareSub.unapply)

  def edit = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "orientation" -> nonEmptyText,
    "showTree" -> boolean
  )(CourseWareEdit.apply)(CourseWareEdit.unapply))

  def sort = Form(mapping(
    "ids" -> list(nonEmptyText)
  )(CourseWareSort.apply)(CourseWareSort.unapply))

  def note = Form(mapping(
    "text" -> optional(nonEmptyText)
  )(CourseWareNote.apply)(CourseWareNote.unapply))

  def editOf(courseWare: CourseWare) = edit.fill(CourseWareEdit(
    name = courseWare.name,
    orientation = (courseWare.orientation.map(o => chess.Color.apply(o) | chess.Color.White) | chess.Color.White).name,
    showTree = (courseWare.showTree | true)
  ))

  def urlTest(url: String): Fu[Boolean] = WS.url(url).get().map(r => r.status == 200)

  def chapterTest(url: String, user: User): Fu[Boolean] = {
    studyApi.byId(Study.Id(studyId(url))).map {
      _.?? { study =>
        study.isPublic || study.members.contains(user.id)
      }
    }
  }

}

object DataForm {

  private val ProdGameRegex = """https://haichess\.com/(\w{8})(\w{4})?/?(white|black)?""".r
  private val DevGameRegex = """http://localhost/(\w{8})(\w{4})?/?(white|black)?""".r
  private val ProdChapterRegex = """https://haichess.com/study/(\w{8})/(\w{8})""".r
  private val DevChapterRegex = """http://localhost/study/(\w{8})/(\w{8})""".r

  def finish = Form(tuple("win" -> boolean, "turns" -> number(0, 500)))

  def colorChoices = List("all" -> "全部", "white" -> "白棋", "black" -> "黑棋")

  def orientChoices = List("white" -> "白方", "black" -> "黑方")

  def isProd = lila.common.PlayApp.isProd

  def gameRegex = if (isProd) ProdGameRegex else DevGameRegex

  def chapterRegex = if (isProd) ProdChapterRegex else DevChapterRegex

  def gameId(url: String) = group(gameRegex, url, 1)

  def studyId(url: String) = group(chapterRegex, url, 1)

  def chapterId(url: String) = group(chapterRegex, url, 2)

  def puzzleIds(puzzle: String) = try {
    puzzle.split("""\n""").toList.map(_.trim.toInt)
  } catch {
    case _: Exception => Nil
  }

  def group(r: Regex, s: String, g: Int): String = {
    val m = r.pattern.matcher(s)
    if (m.find) {
      m.group(g).some
    } else None
  } err s"can not find regex group of $s"

}

case class CourseWareCreate(
    source: String,
    name: String,
    orientation: Option[String],
    showTree: Option[Boolean],
    prevOrder: Option[Int],
    study: Option[List[CourseWareSub]],
    contest: Option[List[CourseWareSub]],
    gamedb: Option[List[CourseWareSub]],
    situationdb: Option[List[CourseWareSub]],
    capsule: Option[List[CourseWareSub]],
    puzzle: Option[String],
    homeworkPuzzle: Option[List[CourseWareSub]],
    homeworkReplayGame: Option[List[CourseWareSub]],
    homeworkRecallGame: Option[List[CourseWareSub]],
    homeworkDistinguishGame: Option[List[CourseWareSub]],
    editor: Option[String],
    game: Option[String],
    fen: Option[String],
    pgn: Option[String]
) {

  def gameId = game.map(DataForm.gameId) err "game is empty"
  def gameId(url: String) = DataForm.gameId(url)

  def studyId = game.map(DataForm.studyId) err "study is empty"
  def studyId(url: String) = DataForm.studyId(url)

  def chapterId = game.map(DataForm.chapterId) err "study is empty"
  def chapterId(url: String) = DataForm.chapterId(url)

  def puzzleIds = puzzle.map(DataForm.puzzleIds) | Nil

  def realSource(): CourseWare.Source = CourseWare.Source.apply(source)

  def checkSource() = realSource() match {
    case CourseWare.Source.Study => study.nonEmpty
    case CourseWare.Source.Contest => contest.nonEmpty
    case CourseWare.Source.Gamedb => gamedb.nonEmpty
    case CourseWare.Source.Situationdb => situationdb.nonEmpty
    case CourseWare.Source.Capsule => capsule.nonEmpty
    case CourseWare.Source.HomeworkPuzzle => homeworkPuzzle.nonEmpty
    case CourseWare.Source.HomeworkReplayGame => homeworkReplayGame.nonEmpty
    case CourseWare.Source.HomeworkRecallGame => homeworkRecallGame.nonEmpty
    case CourseWare.Source.HomeworkDistinguishGame => homeworkDistinguishGame.nonEmpty
    case CourseWare.Source.Puzzle => puzzle.nonEmpty
    case CourseWare.Source.Editor => editor.nonEmpty
    case CourseWare.Source.Game => game.nonEmpty
    case CourseWare.Source.Fen => fen.nonEmpty
    case CourseWare.Source.Pgn => pgn.nonEmpty
    case _ => false
  }

}

case class CourseWareSub(content: String, name: Option[String], setup: Option[String])

case class CourseWareEdit(name: String, orientation: String, showTree: Boolean)

case class CourseWareSort(ids: List[String])

case class CourseWareNote(text: Option[String])
