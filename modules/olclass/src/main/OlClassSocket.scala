package lila.olclass

import chess.Centis
import chess.format.pgn.Glyphs
import lila.chat.Chat
import lila.hub.Trouper
import lila.socket.Socket.Sri
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.json.{ JsObject, JsValue }
import scala.concurrent.duration._
import scala.concurrent.Promise
import lila.user.User
import lila.study.{ Node, TreeBuilder }
import lila.tree.Node.{ Comment, Shapes }
import lila.socket._

final class OlClassSocket(
    id: OlClass.ID,
    system: akka.actor.ActorSystem,
    sriTtl: FiniteDuration,
    keepMeAlive: () => Unit
) extends SocketTrouper[OlClassSocket.Member](system, sriTtl) with LoneSocket {

  def monitoringName = "olclass"
  def broomFrequency = 4027 millis

  import OlClassSocket._
  import JsonView._
  import lila.tree.Node.{ openingWriter, commentWriter, glyphsWriter, shapesWrites, clockWrites }

  private def chatClassifier = Chat classify Chat.Id(id)

  lilaBus.subscribe(this, chatClassifier)

  override def stop(): Unit = {
    super.stop()
    lilaBus.unsubscribe(this, chatClassifier)
  }

  private def who(sri: Sri) = sriToUserId(sri) map { Who(_, sri) }

  def receiveSpecific = ({

    case Join(sri, userId, promise) => {
      val (enumerator, channel) = Concurrent.broadcast[JsValue]
      val member = Member(channel, userId)
      addMember(sri, member)
      promise success Connected(enumerator, member)
    }

    case Started(data) =>
      notifyAll("olcStarted", data)

    case Stopped(data) =>
      notifyAll("olcStopped", data)

    case MuteAll(data) =>
      notifyAll("olcMuteAll", data)

    case AddSync(userId) =>
      notifyAll("olcAddSync", userId)

    case RemoveSync(userId) =>
      notifyAll("olcRemoveSync", userId)

    case AddHandUp(userId) =>
      notifyAll("olcAddHandUp", userId)

    case RemoveHandUp(userId) =>
      notifyAll("olcRemoveHandUp", userId)

    case AddSpeaker(userId) =>
      notifyAll("olcAddSpeaker", userId)

    case RemoveSpeaker(userId) =>
      notifyAll("olcRemoveSpeaker", userId)

    case ChangeCourseWare(sri, pos) =>
      notifyAll("changeCourseWare", Json.obj(
        "p" -> pos,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SetPath(pos, sri) =>
      notifyAll("path", Json.obj(
        "p" -> pos,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case AddNode(pos, node, variant, sri, sticky) =>
      val dests = AnaDests(
        variant,
        node.fen,
        pos.path.toString,
        pos.courseWareId.some
      )
      notifyAll("addNode", Json.obj(
        "n" -> TreeBuilder.toBranch(node, variant),
        "p" -> pos,
        "w" -> who(sri).map(whoWriter.writes),
        "d" -> dests.dests,
        "o" -> dests.opening,
        "s" -> sticky
      ))

    case DeleteNode(pos, sri) =>
      notifyAll("deleteNode", Json.obj(
        "p" -> pos,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case Promote(pos, toMainline, sri) =>
      notifyAll("promote", Json.obj(
        "p" -> pos,
        "toMainline" -> toMainline,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case ForceVariation(pos, force, sri) =>
      notifyAll("forceVariation", Json.obj(
        "p" -> pos,
        "force" -> force,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SetShapes(pos, shapes, sri) =>
      notifyAll("shapes", Json.obj(
        "p" -> pos,
        "s" -> shapes,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SelectNode(pos, sri) =>
      notifyAll("selectNode", Json.obj(
        "p" -> pos,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SetGlyphs(pos, glyphs, sri) =>
      notifyAll("glyphs", Json.obj(
        "p" -> pos,
        "g" -> glyphs,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SetComment(pos, comment, sri) =>
      notifyAll("setComment", Json.obj(
        "p" -> pos,
        "c" -> comment,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case DeleteComment(pos, commentId, sri) =>
      notifyAll("deleteComment", Json.obj(
        "p" -> pos,
        "id" -> commentId,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case SetTags(courseWareId, tags, sri) =>
      notifyAll("setTags", Json.obj(
        "courseWareId" -> courseWareId,
        "tags" -> tags,
        "w" -> who(sri).map(whoWriter.writes)
      ))

    case ReloadAll => notifyAll("reload", JsNull)

    case ReloadSri(sri) => notifySri("reload", JsNull)(sri)

    case ReloadSriBecauseOf(sri, chapterId) =>
      notifySri("reload", Json.obj(
        "chapterId" -> chapterId
      ))(sri)

  }: Trouper.Receive) orElse lila.chat.Socket.out(
    send = (t, d, _) => notifyAll(t, d)
  )

  override protected def broom: Unit = {
    super.broom
    if (members.nonEmpty) keepMeAlive()
  }

  override protected def afterQuit(sri: Socket.Sri, member: Member): Unit = {
    member.userId.foreach { userId =>
      notifyAll("olcMemberQuit", userId)
      lilaBus.publish(AfterQuit(id, userId), 'olcMemberQuit)
    }
  }

}

object OlClassSocket {

  case class Who(u: String, s: Sri)
  import lila.study.JsonView.sriWriter
  implicit private val whoWriter = Json.writes[Who]

  case class Member(
      channel: JsChannel,
      userId: Option[User.ID]
  ) extends SocketMember

  case class Join(sri: Socket.Sri, userId: Option[String], promise: Promise[Connected])
  case class Connected(enumerator: JsEnumerator, member: OlClassSocket.Member)
  case class AfterQuit(id: OlClass.ID, userId: User.ID)

  case class Started(data: JsObject)
  case class Stopped(data: JsObject)

  case class MuteAll(data: JsObject)

  case class AddSync(userId: String)
  case class RemoveSync(userId: String)

  case class AddHandUp(userId: String)
  case class RemoveHandUp(userId: String)

  case class AddSpeaker(userId: String)
  case class RemoveSpeaker(userId: String)

  case object ReloadAll
  case class ReloadSri(sri: Sri)
  case class ReloadSriBecauseOf(sri: Sri, courseWareId: CourseWare.ID)

  case class AddNode(
      position: Position.Ref,
      node: Node,
      variant: chess.variant.Variant,
      sri: Sri,
      sticky: Boolean
  )
  case class DeleteNode(position: Position.Ref, sri: Sri)
  case class Promote(position: Position.Ref, toMainline: Boolean, sri: Sri)
  case class ChangeCourseWare(sri: Sri, position: Position.Ref)
  case class SetPath(position: Position.Ref, sri: Sri)
  case class SetShapes(position: Position.Ref, shapes: Shapes, sri: Sri)
  case class SetComment(position: Position.Ref, comment: Comment, sri: Sri)
  case class DeleteComment(position: Position.Ref, commentId: Comment.Id, sri: Sri)
  case class SetGlyphs(position: Position.Ref, glyphs: Glyphs, sri: Sri)
  case class SetClock(position: Position.Ref, clock: Option[Centis], sri: Sri)
  case class ForceVariation(position: Position.Ref, force: Boolean, sri: Sri)
  case class SetTags(courseWareId: CourseWare.ID, tags: chess.format.pgn.Tags, sri: Sri)
  case class SelectNode(position: Position.Ref, sri: Sri)
}
