package lila.olclass

import akka.actor._
import com.typesafe.config.Config
import lila.hub.{ Duct, DuctMap }

import scala.concurrent.duration._

final class Env(
    config: Config,
    system: ActorSystem,
    hub: lila.hub.Env,
    scheduler: lila.common.Scheduler,
    db: lila.db.Env,
    noteApi: lila.round.NoteApi,
    studyApi: lila.study.StudyApi,
    puzzleApi: lila.puzzle.PuzzleApi,
    lightUserApi: lila.user.LightUserApi,
    roundJsonView: lila.round.JsonView,
    importer: lila.importer.Importer,
    asyncCache: lila.memo.AsyncCache.Builder,
    gamePgnDump: lila.game.PgnDump,
    isOnline: lila.user.User.ID => Boolean
) {

  private val AnimationDuration = config duration "animation.duration"
  private val SdkAppId = config getLong "tencent.sdkAppId"
  private val SecretKey = config getString "tencent.secretKey"
  private val SocketTimeout = config duration "socket.timeout"
  private val SocketSriTimeout = config duration "socket.sri.timeout"
  private val CollectionOlClass = config getString "collection.olclass"
  private val CollectionCourseWare = config getString "collection.courseware"
  private val CollectionJoinLog = config getString "collection.joinlog"

  private[olclass] val OlClassColl = db(CollectionOlClass)
  private[olclass] val CourseWareColl = db(CollectionCourseWare)
  private[olclass] val JoinLogColl = db(CollectionJoinLog)

  lazy val form = new DataForm(studyApi)

  lazy val jsonView = new JsonView(
    animationDuration = AnimationDuration,
    roundJsonView = roundJsonView
  )

  lazy val sequencer = new OlClassSequencer(sequencerMap)

  lazy val api = new OlClassApi(
    jsonView = jsonView,
    socketMap = socketMap,
    sequencer = sequencer,
    bus = hub.bus,
    scheduler = scheduler,
    asyncCache = asyncCache,
    isOnline = isOnline
  )(system)

  lazy val courseWareMaker = new CourseWareMaker(
    noteApi = noteApi,
    studyApi = studyApi,
    puzzleApi = puzzleApi,
    lightUser = lightUserApi,
    gamePgnDump = gamePgnDump,
    importer = importer
  )

  lazy val courseWareApi = new CourseWareApi(
    socketMap,
    courseWareMaker,
    sequencer,
    api.find,
    lightUserApi.sync
  )

  lazy val tlsSig = new TLSSigAPIv2(SdkAppId, SecretKey)

  lazy val socketMap: SocketMap = lila.socket.SocketMap[OlClassSocket](
    system = system,
    mkTrouper = (id: String) => new OlClassSocket(
      id = id,
      system = system,
      sriTtl = SocketSriTimeout,
      keepMeAlive = () => socketMap touch id
    ),
    accessTimeout = SocketTimeout,
    monitoringName = "olclass.socketMap",
    broomFrequency = 3691 millis
  )

  lazy val socketHandler = new OlClassSocketHandler(
    api = api,
    courseWareApi = courseWareApi,
    socketMap = socketMap,
    hub = hub,
    chat = hub.chat
  )

  lazy val messenger = new Messenger(
    chat = hub.chat
  )

  private val sequencerMap = new DuctMap(
    mkDuct = _ => Duct.extra.lazyPromise(5.seconds.some)(system),
    accessTimeout = 10.minute
  )

}

object Env {

  lazy val current: Env = "olclass" boot new Env(
    config = lila.common.PlayApp loadConfig "olclass",
    system = lila.common.PlayApp.system,
    hub = lila.hub.Env.current,
    scheduler = lila.common.PlayApp.scheduler,
    noteApi = lila.round.Env.current.noteApi,
    db = lila.db.Env.current,
    studyApi = lila.study.Env.current.api,
    puzzleApi = lila.puzzle.Env.current.api,
    lightUserApi = lila.user.Env.current.lightUserApi,
    roundJsonView = lila.round.Env.current.jsonView,
    importer = lila.importer.Env.current.importer,
    asyncCache = lila.memo.Env.current.asyncCache,
    gamePgnDump = lila.game.Env.current.pgnDump,
    isOnline = lila.user.Env.current.isOnline
  )
}
