package lila.olclass

import chess.format.Forsyth
import chess.format.pgn.{ Initial, Pgn, Tag, Tags }
import lila.study.PgnDump.{ shapeComment, toTurns }

object CourseWarePgnDump {

  def toPgn(courseWare: CourseWare) = Pgn(
    tags = makeTags(courseWare),
    turns = toTurns(courseWare.root),
    initial = Initial(
      courseWare.root.comments.list.map(_.text.value) ::: shapeComment(courseWare.root.shapes).toList
    )
  )

  private def makeTags(courseWare: CourseWare): Tags = Tags {
    val genTags = (courseWare.root.fen.value != Forsyth.initial).??(List(
      Tag(_.FEN, courseWare.root.fen.value),
      Tag("SetUp", "1")
    ))
    genTags.foldLeft(courseWare.tags.value.reverse) {
      case (tags, tag) =>
        if (tags.exists(t => tag.name == t.name)) tags
        else tag :: tags
    }.reverse
  }

}
