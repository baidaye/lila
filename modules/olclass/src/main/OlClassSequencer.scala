package lila.olclass

import lila.hub.{ Duct, DuctMap }

final class OlClassSequencer(sequencers: DuctMap[_]) {

  def sequenceOlClass(olClassId: OlClass.ID)(fetch: OlClass.ID => Fu[Option[OlClass]])(run: OlClass => Funit): Funit =
    doSequence(olClassId) {
      fetch(olClassId) flatMap {
        _ ?? { run(_) }
      }
    }

  def sequenceOlClassWithCourseWare(olClassId: OlClass.ID, courseWareId: CourseWare.ID)(fetch: OlClass.ID => Fu[Option[OlClass]])(f: OlClass.WithCourseWare => Funit): Funit =
    sequenceOlClass(olClassId)(fetch) { olClass =>
      CourseWareRepo.byId(courseWareId) flatMap {
        _ ?? { courseWare =>
          f(OlClass.WithCourseWare(olClass, courseWare))
        }
      }
    }

  private def doSequence(id: OlClass.ID)(f: => Funit): Funit = {
    val promise = scala.concurrent.Promise[Unit]
    sequencers.tell(id, Duct.extra.LazyPromise(Duct.extra.LazyFu(() => f), promise))
    promise.future
  }

}
