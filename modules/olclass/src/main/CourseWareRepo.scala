package lila.olclass

import lila.db.dsl._
import lila.study.{ Node, Path }
import reactivemongo.api.ReadPreference
import reactivemongo.bson.{ BSONElement, BSONHandler, BSONString, Macros }

object CourseWareRepo {

  import lila.study.BSONHandlers._

  val noRootProjection = $doc("root" -> false)

  implicit val BoardResultBSONHandler = new BSONHandler[BSONString, CourseWare.Source] {
    def read(b: BSONString): CourseWare.Source = CourseWare.Source(b.value)
    def write(x: CourseWare.Source) = BSONString(x.id)
  }
  private implicit val CourseWareSetupHandler = Macros.handler[CourseWareSetup]
  private implicit val CourseWareHandler = Macros.handler[CourseWare]
  private implicit val CourseWareMetadataHandler = Macros.handler[CourseWareMetadata]

  private lazy val coll = Env.current.CourseWareColl

  private def byOlClass(olClassId: OlClass.ID) = $doc("olClassId" -> olClassId)

  def byId(id: CourseWare.ID): Fu[Option[CourseWare]] = coll.byId[CourseWare](id)

  def byIdAndOlClass(olClassId: OlClass.ID, id: CourseWare.ID): Fu[Option[CourseWare]] =
    coll.byId[CourseWare](id).map { _.filter(_.olClassId == olClassId) }

  def getByOlClass(olClassId: OlClass.ID): Fu[List[CourseWare]] =
    coll.find(byOlClass(olClassId))
      .sort($sort.asc("order"))
      .list[CourseWare]()

  def orderedMetadataByOlClass(olClassId: OlClass.ID): Fu[List[CourseWareMetadata]] =
    coll.find(
      byOlClass(olClassId),
      noRootProjection
    ).sort($sort asc "order").list[CourseWareMetadata]()

  def nextOrderByOlClass(olClassId: OlClass.ID): Fu[Int] =
    coll.primitiveOne[Int](
      byOlClass(olClassId),
      $sort desc "order",
      "order"
    ) map { order => ~order + 1 }

  def countByOlClass(olClassId: OlClass.ID): Fu[Int] =
    coll.countSel(byOlClass(olClassId))

  def insert(olClassId: CourseWare): Funit =
    coll.insert(olClassId).void

  def bulkInsert(courseWares: List[CourseWare]): Funit =
    coll.bulkInsert(
      documents = courseWares.map(CourseWareHandler.write).toStream,
      ordered = true
    ).void

  def update(c: CourseWare): Funit = coll.update($id(c.id), c).void

  def updateFields(id: CourseWare.ID, data: CourseWareEdit): Funit =
    coll.update(
      $id(id), $set(
        "name" -> data.name,
        "orientation" -> data.orientation,
        "showTree" -> data.showTree
      )
    ).void

  def setName(id: CourseWare.ID, name: String): Funit =
    coll.updateField($id(id), "name", name).void

  def remove(id: CourseWare.ID): Funit =
    coll.remove($id(id)).void

  def sort(olClassId: OlClass.ID, ids: List[CourseWare.ID]): Funit =
    ids.zipWithIndex.map {
      case (id, index) =>
        coll.updateField(byOlClass(olClassId) ++ $id(id), "order", index + 1)
    }.sequenceFu.void

  def updateOrder(olClassId: OlClass.ID, prevOrder: Int, inc: Int): Funit =
    coll.update(
      byOlClass(olClassId) ++ $doc("order" $gt prevOrder),
      $inc("order" -> inc),
      multi = true
    ).void

  def setNote(id: CourseWare.ID, note: Option[String]): Funit =
    coll.updateField($id(id), "note", note).void

  def setTagsFor(courseWare: CourseWare): Funit =
    coll.updateField($id(courseWare.id), "tags", courseWare.tags).void

  def setShapes(courseWare: CourseWare, path: Path, shapes: lila.tree.Node.Shapes): Funit =
    setNodeValue(courseWare, path, "h", shapes.value.nonEmpty option shapes)

  def setComments(courseWare: CourseWare, path: Path, comments: lila.tree.Node.Comments): Funit =
    setNodeValue(courseWare, path, "co", comments.value.nonEmpty option comments)

  def setGamebook(courseWare: CourseWare, path: Path, gamebook: lila.tree.Node.Gamebook): Funit =
    setNodeValue(courseWare, path, "ga", gamebook.nonEmpty option gamebook)

  def setGlyphs(courseWare: CourseWare, path: Path, glyphs: chess.format.pgn.Glyphs): Funit =
    setNodeValue(courseWare, path, "g", glyphs.nonEmpty)

  def setClock(courseWare: CourseWare, path: Path, clock: Option[chess.Centis]): Funit =
    setNodeValue(courseWare, path, "l", clock)

  def forceVariation(courseWare: CourseWare, path: Path, force: Boolean): Funit =
    setNodeValue(courseWare, path, "fv", force option true)

  def setScore(courseWare: CourseWare, path: Path, score: Option[lila.tree.Eval.Score]): Funit =
    setNodeValue(courseWare, path, "e", score)

  private def setNodeValue[A: BSONValueWriter](courseWare: CourseWare, path: Path, field: String, value: Option[A]): Funit =
    pathToField(courseWare, path, field) match {
      case None =>
        logger.warn(s"Can't setNodeValue ${courseWare.id} $path $field")
        funit
      case Some(field) => (value match {
        case None => coll.unsetField($id(courseWare.id), field)
        case Some(v) => coll.updateField($id(courseWare.id), field, v)
      }) void
    }

  // 废弃：root.n.0.n.0.n.1.n.0.n.2.subField
  // root.n.0:0:0:0:0:0.subField
  private def pathToField(courseWare: CourseWare, path: Path, subField: String): Option[String] =
    if (path.isEmpty) s"root.$subField".some
    else courseWare.root.children.pathToIndexes(path) map { indexes =>
      s"root.n.${indexes.mkString(":")}.$subField"
    }

  private def setChild(courseWare: CourseWare, path: Path, child: Node): Funit = coll.byId[CourseWare, CourseWare.ID](courseWare.id) flatMap {
    case None => fufail(s"Invalid Chapter ${courseWare.id} Not exist")
    case Some(courseWare) => courseWare.addNode(child, path) match {
      case None => fufail(s"Invalid setChild ${courseWare.id} $path $child")
      case Some(c) => update(c)
    }
  }

  private def pathToFieldForSetChildren(courseWare: CourseWare, path: Path): Option[String] =
    if (path.isEmpty) s"root.n".some
    else courseWare.root.children.pathToIndexes(path) map { indexes =>
      s"root.n.${indexes.mkString(":")}"
    }

  def setChildren(courseWare: CourseWare, path: Path, children: Node.Children): Funit =
    pathToFieldForSetChildren(courseWare, path) match {
      case None =>
        logger.warn(s"Can't setChildren ${courseWare.id} $path")
        funit
      case Some(parent) =>
        coll.update(
          $id(courseWare.id),
          $doc("$set" -> $doc(
            children.nodes.zipWithIndex.foldLeft(scala.collection.mutable.Map.empty[String, Node]) {
              case (m, (n, i)) => {
                val path = s"$i"
                n.depthFix.toMap(m, path.some)
                m.put(path, n)
                m
              }
            }.map {
              case (k, n) => BSONElement(s"$parent${if (path.isEmpty) "." else ":"}$k", NodeBSONHandler.write(n))
            }
          )),
          multi = true
        ) void
    }

}
