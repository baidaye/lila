package lila.olclass

import chess.format.pgn.{ Parser, Tags }
import chess.format.{ FEN, Forsyth }
import chess.variant.{ Crazyhouse, Variant }
import lila.analyse.AnalysisRepo
import lila.game.{ Game, GameRepo, Player, Pov }
import lila.study.{ Chapter, GameToRoot, Node, PgnImport, StudyApi }
import lila.importer.Importer
import lila.puzzle.{ Line, Puzzle, PuzzleApi }
import lila.round.NoteApi

final class CourseWareMaker(
    noteApi: NoteApi,
    studyApi: StudyApi,
    puzzleApi: PuzzleApi,
    lightUser: lila.user.LightUserApi,
    gamePgnDump: lila.game.PgnDump,
    importer: Importer
) {

  def apply(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.realSource() match {
      case CourseWare.Source.Study => fromStudy(olClass, data, order)
      case CourseWare.Source.Contest => fromContest(olClass, data, order)
      case CourseWare.Source.Gamedb => fromGamedb(olClass, data, order)
      case CourseWare.Source.Situationdb => fromSituationdb(olClass, data, order)
      case CourseWare.Source.Capsule => fromCapsule(olClass, data, order)
      case CourseWare.Source.Puzzle => fromPuzzle(olClass, data, order)
      case CourseWare.Source.HomeworkPuzzle => fromHomeworkPuzzle(olClass, data, order)
      case CourseWare.Source.HomeworkReplayGame => fromHomeworkReplayGame(olClass, data, order)
      case CourseWare.Source.HomeworkRecallGame => fromHomeworkRecallGame(olClass, data, order)
      case CourseWare.Source.HomeworkDistinguishGame => fromHomeworkDistinguishGame(olClass, data, order)
      case CourseWare.Source.Editor => fromEditor(olClass, data, order)
      case CourseWare.Source.Game => fromGame(olClass, data, order)
      case CourseWare.Source.Fen => fuccess { List(fromFen(olClass, order, data.fen, data.name, CourseWare.Source.Fen)) }
      case CourseWare.Source.Pgn => fromPgn(olClass, data, order)
      case _ => fufail("invalid source")
    }

  private def makeName(name: String, subName: Option[String], len: Int): String =
    if (len > 1) {
      subName.map { n => s"$name $n" } | name
    } else name

  private def fromStudy(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.study.map { studies =>
      val ids = studies.map(s => Chapter.Id(s.content))
      studyApi.chapters(ids) map { chapters =>
        chapters.zipWithIndex.map {
          case (chapter, index) => {
            val sub = studies.find(sub => sub.content == chapter.id.value) err s"can not find chapter id ${chapter.id.value}"
            CourseWare.make(
              olClassId = olClass.id,
              name = makeName(data.name, sub.name, chapters.length),
              order = order + index,
              source = data.realSource(),
              root = chapter.root,
              tags = chapter.tags,
              setup = CourseWareSetup(
                chapterId = sub.setup
              ).some,
              note = chapter.description,
              createBy = olClass.coach
            )
          }
        }
      }
    } err "study is empty"

  private def fromContest(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.contest.map { contests =>
      val ids = contests.map(_.content)
      GameRepo.povsByGameIds(ids) flatMap { povs =>
        povs.zipWithIndex.map {
          case (pov, index) => {
            val sub = contests.find(sub => sub.content == pov.gameId) err s"can not find contest board id ${pov.gameId}"
            fromPov(
              olClass,
              pov,
              order + index,
              makeName(data.name, sub.name, povs.length),
              data.realSource(),
              CourseWareSetup(
                boardId = sub.setup
              )
            )
          }
        }.sequenceFu
      }
    } err "contest is empty"

  private def fromGamedb(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.gamedb.map { gamedbs =>
      val ids = gamedbs.map(_.content)
      GameRepo.povsByGameIds(ids) flatMap { povs =>
        povs.zipWithIndex.map {
          case (pov, index) => {
            val sub = gamedbs.find(sub => sub.content == pov.gameId) err s"can not find gamedb rel id ${pov.gameId}"
            fromPov(
              olClass,
              pov,
              order + index,
              makeName(data.name, sub.name, povs.length),
              data.realSource(),
              CourseWareSetup(
                gamedbRelId = sub.setup
              )
            )
          }
        }.sequenceFu
      }
    } err "gamedb is empty"

  private def fromSituationdb(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    fuccess {
      data.situationdb.map { situationdbs =>
        situationdbs.zipWithIndex.map {
          case (situationdb, index) => {
            fromFen(
              olClass,
              order + index,
              situationdb.content.some,
              makeName(data.name, situationdb.name, situationdbs.length),
              data.realSource(),
              CourseWareSetup(
                situationdbRelId = situationdb.setup
              ).some
            )
          }
        }
      } err "situationdb is empty"
    }

  private def fromCapsule(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.capsule.map { formPuzzles =>
      fromPuzzleIds(olClass, order, formPuzzles, data.name, data.realSource())
    } err "capsule is empty"

  private def fromHomeworkPuzzle(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.homeworkPuzzle.map { formPuzzles =>
      fromPuzzleIds(olClass, order, formPuzzles, data.name, data.realSource())
    } err "homework is empty"

  private def fromHomeworkReplayGame(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.homeworkReplayGame.map { homeworkReplayGame =>
      fromPgns(olClass, homeworkReplayGame, data, order)
    } err "homeworkReplayGame is empty"

  private def fromHomeworkRecallGame(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.homeworkRecallGame.map { homeworkRecallGame =>
      fromPgns(olClass, homeworkRecallGame, data, order)
    } err "homeworkRecallGame is empty"

  private def fromHomeworkDistinguishGame(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.homeworkDistinguishGame.map { homeworkDistinguishGame =>
      fromPgns(olClass, homeworkDistinguishGame, data, order)
    } err "homeworkDistinguishGame is empty"

  private def fromPuzzleIds(
    olClass: OlClass,
    order: Int,
    subs: List[CourseWareSub],
    name: String,
    source: CourseWare.Source
  ): Fu[List[CourseWare]] = {
    val puzzleIds = subs.map(_.content.toInt)
    puzzleApi.puzzle.findMany2(puzzleIds) flatMap { puzzles =>
      puzzles.zipWithIndex map {
        case (puzzle, index) => {
          val sub = subs.find(_.content.toInt == puzzle.id) err s"can not find puzzle id ${puzzle.id}"
          fromSinglePuzzle(
            olClass,
            puzzle,
            order + index,
            makeName(name, sub.name, puzzles.length),
            source,
            setup = CourseWareSetup(
              capsuleId = if (source == CourseWare.Source.Capsule) sub.setup else none,
              homeworkId = if (source == CourseWare.Source.HomeworkPuzzle) sub.setup else none
            )
          )
        }
      } sequenceFu
    }
  }

  private def fromPuzzle(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.puzzle.map { _ =>
      val puzzleIds = data.puzzleIds
      puzzleApi.puzzle.findMany2(puzzleIds) flatMap { puzzles =>
        puzzles.zipWithIndex map {
          case (puzzle, index) => {
            fromSinglePuzzle(
              olClass,
              puzzle,
              order + index,
              makeName(data.name, s"#${puzzle.id}".some, puzzleIds.length),
              data.realSource(),
              setup = CourseWareSetup(
                puzzleId = puzzle.id.toString.some
              )
            )
          }
        } sequenceFu
      }
    } err "puzzle is empty"

  def fromEditor(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    fuccess {
      List(
        fromFen(
          olClass,
          order,
          data.editor,
          data.name,
          data.realSource()
        )
      )
    }

  def fromGame(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.game.map { gameUrl =>
      GameRepo.pov(data.gameId, chess.White) flatMap {
        case None => fuccess(None)
        case Some(pov) => {
          fromPov(
            olClass,
            pov,
            order,
            data.name,
            data.realSource(),
            CourseWareSetup(
              gameUrl = gameUrl.some
            )
          ).map(_.some)
        }
      } map {
        case None => Nil
        case Some(cw) => List(cw)
      }
    } err "game is empty"

  private def fromPgns(olClass: OlClass, subs: List[CourseWareSub], data: CourseWareCreate, order: Int): Fu[List[CourseWare]] = fuccess {
    subs.zipWithIndex.map {
      case (sub, index) => {
        PgnImport(sub.content, Nil).toOption match {
          case None => throw sys.error("pgn is invalid")
          case Some(res) => {
            CourseWare.make(
              olClassId = olClass.id,
              name = makeName(data.name, sub.name, subs.length),
              order = order + index,
              source = data.realSource(),
              root = res.root,
              tags = res.tags,
              setup = CourseWareSetup(
                homeworkId = sub.setup
              ).some,
              createBy = olClass.coach
            )
          }
        }
      }
    }
  }

  private def fromPgn(olClass: OlClass, data: CourseWareCreate, order: Int): Fu[List[CourseWare]] =
    data.pgn match {
      case None => fufail("pgn is empty")
      case Some(pgn) => {
        lightUser.asyncMany(olClass.syncs) map { contributors =>
          PgnImport(pgn, contributors.flatten).toOption match {
            case None => throw sys.error("pgn is invalid")
            case Some(res) => {
              List(
                CourseWare.make(
                  olClassId = olClass.id,
                  name = data.name,
                  order = order,
                  source = data.realSource(),
                  root = res.root,
                  tags = res.tags,
                  setup = none,
                  createBy = olClass.coach
                )
              )
            }
          }
        }
      }
    }

  private def fromFen(
    olClass: OlClass,
    order: Int,
    fen: Option[String],
    name: String,
    source: CourseWare.Source,
    setup: Option[CourseWareSetup] = None
  ): CourseWare = {
    val variant = Variant.default
    (fen.map(_.trim).filter(_.nonEmpty).flatMap { fenStr =>
      Forsyth.<<<@(variant, fenStr)
    } match {
      case Some(sit) => Node.Root(
        ply = sit.turns,
        fen = FEN(Forsyth.>>(sit)),
        check = sit.situation.check,
        clock = none,
        crazyData = sit.situation.board.crazyData,
        children = Node.emptyChildren
      ) -> true
      case None => Node.Root(
        ply = 0,
        fen = FEN(variant.initialFen),
        check = false,
        clock = none,
        crazyData = variant.crazyhouse option Crazyhouse.Data.init,
        children = Node.emptyChildren
      ) -> false
    }) match {
      case (root, isFromFen) => {
        CourseWare.make(
          olClassId = olClass.id,
          name = name,
          order = order,
          source = source,
          root = root,
          tags = Tags.empty,
          setup = setup,
          createBy = olClass.coach
        )
      }
    }
  }

  private def fromSinglePuzzle(olClass: OlClass, puzzle: Puzzle, order: Int, name: String, source: CourseWare.Source, setup: CourseWareSetup): Fu[CourseWare] = {

    def makeGame(puzzle: Puzzle): chess.Game = {
      import chess.format._
      val fullSolution: List[Uci.Move] = (Line solution puzzle.lines).map { uci =>
        Uci.Move(uci) err s"Invalid puzzle solution UCI $uci"
      }
      val solution = {
        if (fullSolution.isEmpty) {
          logger.warn(s"Puzzle ${puzzle.id} has an empty solution from ${puzzle.lines}")
          fullSolution
        } else if (fullSolution.size % 2 == 0) {
          fullSolution.init
        } else fullSolution
      }

      val solutionWithInitMove = {
        if (puzzle.hasLastMove) {
          puzzle.initialMove +: solution
        } else solution
      }

      def init = {
        val g = chess.Game(chess.variant.FromPosition.some, puzzle.fen.some)
        if (puzzle.hasLastMove) {
          g.withTurns(puzzle.initialPlyOld)
        } else g.withTurns(0)
      }

      solutionWithInitMove.foldLeft[chess.Game](init) {
        case (prev, uci) => {
          val (game, _) = prev(uci.orig, uci.dest, uci.promotion).prefixFailuresWith(s"puzzle ${puzzle.id}").err
          game
        }
      }
    }

    val game = makeGame(puzzle)

    val dbGame = Game.make(
      chess = game,
      whitePlayer = Player.make(chess.White, None),
      blackPlayer = Player.make(chess.Black, None),
      mode = chess.Mode.Casual,
      source = lila.game.Source.Import,
      pgnImport = None
    ).withId("synthetic")

    game2root(dbGame, FEN(puzzle.fen).some) map { root =>
      CourseWare.make(
        olClassId = olClass.id,
        name = name,
        order = order,
        source = source,
        root = root,
        tags = Tags.empty,
        orientation = puzzle.color.name.some,
        initPly = puzzle.initialPly.some,
        setup = setup.some,
        createBy = olClass.coach
      )
    }
  }

  private def fromPov(olClass: OlClass, pov: Pov, order: Int, name: String, source: CourseWare.Source, setup: CourseWareSetup): Fu[CourseWare] =
    for {
      root <- game2root(pov.game)
      note <- noteApi.get(pov.game.id, olClass.coach)
      initialFen = (root.fen.value != Forsyth.initial).option(root.fen)
      imported = pov.game.pgnImport.flatMap { pgni =>
        Parser.full(pgni.pgn).toOption
      }
      tags <- gamePgnDump.tags(pov.game, initialFen, imported, true)
    } yield {
      CourseWare.make(
        olClassId = olClass.id,
        name = name,
        order = order,
        source = source,
        root = root,
        tags = tags,
        setup = setup.some,
        note = note.some,
        createBy = olClass.coach
      )
    }

  def game2root(game: Game, initialFen: Option[FEN] = None): Fu[Node.Root] = {
    for {
      initialFen <- initialFen.fold {
        (!game.synthetic).?? {
          GameRepo initialFen game
        }
      } { fen => fuccess(fen.some) }
      analysis <- AnalysisRepo byGame game
    } yield GameToRoot(game, initialFen, withClocks = true, analysis = analysis)
  }

}

