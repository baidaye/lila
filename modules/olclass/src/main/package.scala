package lila

import lila.hub.TrouperMap

package object olclass extends PackageObject {

  type SocketMap = TrouperMap[OlClassSocket]

  private[olclass] def logger = lila.log("olclass")

}
