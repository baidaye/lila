package lila.olclass

import lila.clazz.Clazz
import lila.db.dsl._
import org.joda.time.DateTime
import reactivemongo.bson.{ BSONHandler, BSONString, Macros }

object OlClassRepo {

  private implicit val LiveRecordHandler = Macros.handler[LiveRecord]
  private implicit val StatusBSONHandler = new BSONHandler[BSONString, OlClass.Status] {
    def read(b: BSONString): OlClass.Status = OlClass.Status(b.value)
    def write(x: OlClass.Status) = BSONString(x.id)
  }
  implicit val PositionRefBSONHandler = new BSONHandler[BSONString, Position.Ref] {
    def read(b: BSONString) = Position.Ref.decode(b.value) err s"Invalid position ${b.value}"
    def write(x: Position.Ref) = BSONString(x.encode)
  }
  private implicit val OlClazzHandler = Macros.handler[OlClass]

  private lazy val coll = Env.current.OlClassColl

  def byId(id: OlClass.ID): Fu[Option[OlClass]] =
    coll.byId[OlClass](id)

  def findByClazz(clazzId: Clazz.ID): Fu[List[OlClass]] =
    coll.find($doc("clazzId" -> clazzId)).list[OlClass]()

  def insert(olClass: OlClass): Funit =
    coll.insert(olClass).void

  def update(olClass: OlClass): Funit =
    coll.update($id(olClass.id), olClass).void

  def setPosition(studyId: OlClass.ID, position: Position.Ref): Funit =
    coll.update(
      $id(studyId),
      $set(
        "position" -> position,
        "updatedAt" -> DateTime.now
      )
    ).void

  def remove(olClass: OlClass): Funit =
    coll.remove($id(olClass.id)).void

  def updateNow(olClass: OlClass) =
    coll.updateFieldUnchecked($id(olClass.id), "updatedAt", DateTime.now)
}
