package lila.olclass

import lila.user.User
import lila.db.dsl._
import org.joda.time.DateTime
import reactivemongo.bson.Macros

object OlClassJoinLogRepo {

  import lila.db.BSON.BSONJodaDateTimeHandler
  private implicit val OlClassLogHandler = Macros.handler[OlClassJoinLog]

  private lazy val coll = Env.current.JoinLogColl

  private def byOlClass(olClassId: OlClass.ID) = $doc("olClassId" -> olClassId)

  def byId(id: OlClassJoinLog.ID): Fu[Option[OlClassJoinLog]] = coll.byId[OlClassJoinLog](id)

  def lastLog(olClassId: OlClass.ID, userId: User.ID): Fu[Option[OlClassJoinLog]] =
    coll.find(byOlClass(olClassId) ++ $doc("userId" -> userId))
      .sort($doc("joinAt" -> -1))
      .uno[OlClassJoinLog]

  def insert(olClassLog: OlClassJoinLog): Funit =
    coll.insert(olClassLog).void

  def setLeaveAt(id: OlClassJoinLog.ID): Funit =
    coll.updateField($id(id), "leaveAt", DateTime.now).void

  def setAllLeaveAt(olClassId: OlClass.ID): Funit =
    coll.update(
      byOlClass(olClassId) ++ $doc("leaveAt" $exists false),
      $set("leaveAt" -> DateTime.now),
      multi = true
    ).void
}
