package lila.olclass

import akka.actor.ActorSelection
import chess.format.pgn.Glyph
import lila.chat.Chat
import lila.common.ApiVersion
import lila.olclass.OlClassSocket._
import lila.socket._
import lila.socket.Handler.AnaRateLimit
import lila.socket.Socket.{ Sri, makeMessage }
import lila.study.{ MoveOpts, Node, Path }
import lila.tree.Node.{ Shape, Shapes, Comment }
import play.api.libs.json.JsValue
import lila.user.User

final class OlClassSocketHandler(
    api: OlClassApi,
    courseWareApi: CourseWareApi,
    socketMap: SocketMap,
    hub: lila.hub.Env,
    chat: ActorSelection
) {

  import JsonView.shapeReader

  private def moveOrDrop(id: OlClass.ID, m: AnaAny, opts: MoveOpts, sri: Sri, member: OlClassSocket.Member) =
    AnaRateLimit(sri, member) {
      m.branch match {
        case scalaz.Success(branch) if branch.ply < Node.MAX_PLIES =>
          member push makeMessage("node", m json branch)
          for {
            userId <- member.userId
            courseWareId <- m.chapterId
            if opts.write
          } courseWareApi.addNode(
            userId,
            id,
            Position.Ref(courseWareId, Path(m.path)),
            Node.fromBranch(branch) withClock opts.clock,
            sri,
            opts
          )
        case scalaz.Success(branch) =>
          member push makeMessage("stepFailure", s"ply ${branch.ply}/${Node.MAX_PLIES}")
        case scalaz.Failure(err) =>
          member push makeMessage("stepFailure", err.toString)
      }
    }

  def reading(o: JsValue)(f: AtPosition => Unit): Unit =
    o obj "d" flatMap { d =>
      for {
        path ← d str "path"
        courseWareId ← d str "ch"
      } yield AtPosition(path, courseWareId)
    } foreach (a => f(a))

  case class AtPosition(path: String, courseWareId: CourseWare.ID) {
    def ref = Position.Ref(courseWareId, Path(path))
  }

  def readingSetTag(o: JsValue)(f: SetTag => Unit): Unit =
    o obj "d" flatMap { d =>
      for {
        courseWareId ← d str "ch"
        name ← d str "name"
        value ← d str "value"
      } yield SetTag(courseWareId, name, value)
    } foreach (a => f(a))

  case class SetTag(courseWareId: CourseWare.ID, name: String, value: String) {
    def tag = chess.format.pgn.Tag(name, value take 140)
  }

  private def makeController(
    id: OlClass.ID,
    sri: Socket.Sri,
    socket: OlClassSocket,
    member: OlClassSocket.Member,
    user: Option[User]
  ): Handler.Controller = {
    case ("anaMove", o) => AnaMove parse o foreach {
      moveOrDrop(id, _, MoveOpts parse o, sri, member)
    }
    case ("anaDrop", o) => AnaDrop parse o foreach {
      moveOrDrop(id, _, MoveOpts parse o, sri, member)
    }
    case ("setCourseWare", o) => for {
      byUserId <- member.userId
      chapterId <- o.get[String]("d")
    } courseWareApi.setCourseWare(byUserId, id, chapterId, sri)
    case ("setPath", o) => AnaRateLimit(sri, member) {
      reading(o) { position =>
        member.userId foreach { userId =>
          courseWareApi.setPath(userId, id, position.ref, sri)
        }
      }
    }
    case ("deleteNode", o) => AnaRateLimit(sri, member) {
      reading(o) { position =>
        for {
          jumpTo <- (o \ "d" \ "jumpTo").asOpt[String] map Path.apply
          userId <- member.userId
        } courseWareApi.setPath(userId, id, position.ref.withPath(jumpTo), sri) >>
          courseWareApi.deleteNodeAt(userId, id, position.ref, sri)
      }
    }
    case ("promote", o) => AnaRateLimit(sri, member) {
      reading(o) { position =>
        for {
          toMainline <- (o \ "d" \ "toMainline").asOpt[Boolean]
          userId <- member.userId
        } courseWareApi.promote(userId, id, position.ref, toMainline, sri)
      }
    }
    case ("forceVariation", o) => AnaRateLimit(sri, member) {
      reading(o) { position =>
        for {
          force <- (o \ "d" \ "force").asOpt[Boolean]
          userId <- member.userId
        } courseWareApi.forceVariation(userId, id, position.ref, force, sri)
      }
    }
    case ("shapes", o) => {
      reading(o) { position =>
        for {
          shapes <- (o \ "d" \ "shapes").asOpt[List[Shape]]
          userId <- member.userId
        } courseWareApi.setShapes(userId, id, position.ref, Shapes(shapes take 32), sri)
      }
    }
    case ("selectNode", o) => {
      reading(o) { position =>
        member.userId.foreach { userId =>
          courseWareApi.selectNode(userId, id, position.ref, sri)
        }
      }
    }
    case ("toggleGlyph", o) => {
      reading(o) { position =>
        for {
          userId <- member.userId
          glyph <- (o \ "d" \ "id").asOpt[Int] flatMap Glyph.find
        } courseWareApi.toggleGlyph(userId, id, position.ref, glyph, sri)
      }
    }
    case ("setComment", o) => {
      reading(o) { position =>
        for {
          userId <- member.userId
          text <- (o \ "d" \ "text").asOpt[String]
        } courseWareApi.setComment(userId, id, position.ref, Comment sanitize text, sri)
      }
    }
    case ("deleteComment", o) => {
      reading(o) { position =>
        for {
          userId <- member.userId
          commentId <- (o \ "d" \ "id").asOpt[String]
        } courseWareApi.deleteComment(userId, id, position.ref, Comment.Id(commentId), sri)
      }
    }
    case ("setTag", o) => {
      readingSetTag(o) { setTag =>
        for {
          userId <- member.userId
        } courseWareApi.setTag(userId, id, setTag.courseWareId, setTag.tag, sri)
      }
    }
  }

  def join(
    id: OlClass.ID,
    sri: Socket.Sri,
    user: Option[User],
    apiVersion: ApiVersion
  ): Fu[Option[JsSocketHandler]] = {
    val socket = socketMap.getOrMake(id)
    socket.ask[Connected](Join(sri, user.map(_.id), _)) map {
      case Connected(enum, member) => {
        Handler.iteratee(
          hub,
          makeController(id, sri, socket, member, user) orElse lila.chat.Socket.in(
            chatId = Chat.Id(id),
            member = member,
            chat = chat,
            publicSource = lila.hub.actorApi.shutup.PublicSource.OlClass(id).some
          ),
          member,
          socket,
          sri,
          apiVersion
        ) -> enum
      }
    } map (_.some)
  }

}
