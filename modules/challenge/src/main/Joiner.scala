package lila.challenge

import chess.format.Forsyth
import chess.format.Forsyth.SituationPlus
import chess.format.FEN
import chess.{ Situation, Mode }
import lila.game.{ GameRepo, Game, Pov, Source, Player }
import lila.user.{ User, UserRepo }

private[challenge] final class Joiner(onStart: String => Unit) {

  def apply(c: Challenge, destUser: Option[User]): Fu[Option[Pov]] =
    GameRepo exists c.id flatMap {
      case true => fuccess(None)
      case false =>
        c.challengerUserId.??(UserRepo.byId) flatMap { challengerUser =>

          def makeChess(variant: chess.variant.Variant): chess.Game =
            chess.Game(situation = Situation(variant), clock = c.clock.map(_.config.toClock), startedAtTurn = c.initialPgn.map(_.lastPly) | 0)

          val f =
            if (c.variant.fromPgn)
              c.initialPgn.flatMap(p => p.notInitialFen.map(FEN))
            else if (c.variant.fromPosition)
              c.initialFen
            else None

          val baseState = f ifTrue (c.variant.fromPosition || c.variant.fromPgn) flatMap { f =>
            Forsyth.<<<@(chess.variant.FromPosition, f.value)
          }

          val (chessGame, state) = baseState.fold(makeChess(c.variant) -> none[SituationPlus]) {
            case sit @ SituationPlus(s, _) => {
              val startedAtTurn = if (c.variant.fromPgn) c.initialPgn.map(_.lastPly) | 0 else sit.turns
              val game = chess.Game(
                situation = s,
                turns = sit.turns,
                startedAtTurn = startedAtTurn,
                clock = c.clock.map(_.config.toClock)
              )
              game -> baseState
              //            //2021.11.26 如果FromPosition的初始局面是默认局面，那么这盘棋也是FromPosition，不再改为Standard
              //              if (Forsyth.>>(game) == Forsyth.initial) makeChess(chess.variant.Standard) -> none
              //              else game -> baseState
            }
          }
          val perfPicker = (perfs: lila.user.Perfs) => perfs(c.perfType)
          val game = Game.make(
            chess = chessGame,
            whitePlayer = Player.make(chess.White, c.finalColor.fold(challengerUser, destUser), perfPicker),
            blackPlayer = Player.make(chess.Black, c.finalColor.fold(destUser, challengerUser), perfPicker),
            mode = if (chessGame.board.variant.fromPosition) Mode.Casual else c.mode,
            source = if (chessGame.board.variant.fromPosition) Source.Position else Source.Friend,
            daysPerTurn = c.daysPerTurn,
            initialPgn = c.initialPgn,
            pgnImport = None,
            movedAt = {
              if (c.appt) c.apptStartsAt
              else None
            },
            canTakeback = c.canTakeback
          ).withId(c.id).|> { g =>
              state.fold(g) {
                case sit @ SituationPlus(Situation(board, _), _) => g.copy(
                  chess = g.chess.copy(
                    situation = g.situation.copy(
                      board = g.board.copy(
                        history = board.history,
                        variant = if (c.variant.fromPgn) c.variant else chess.variant.FromPosition
                      )
                    ),
                    turns = sit.turns
                  )
                )
              }
            }.withApptWhenCreate(c.appt) |> { g =>
              if (g.isAppt) g else g.start
            }
          GameRepo.insertDenormalized(game) >>- (!c.appt).?? { onStart(game.id) } inject Pov(game, !c.finalColor).some
        }
    }
}
