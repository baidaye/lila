package lila.recall

import chess.format.FEN
import lila.db.dsl._
import lila.user.User
import lila.game.{ Game, GameRepo, PgnDump }
import lila.importer.ImportData
import lila.common.paginator.Paginator
import lila.db.paginator.Adapter
import lila.common.MaxPerPage
import chess.format.Forsyth
import scalaz.{ Failure, Success }

case class GamePgnResult(name: String, fen: String, pgn: String)
final class RecallApi(
    coll: Coll,
    bus: lila.common.Bus,
    studyApi: lila.study.StudyApi,
    importer: lila.importer.Importer,
    pgnDump: lila.game.PgnDump
) {

  import BSONHandlers._

  def byId(id: Recall.ID): Fu[Option[Recall]] = coll.byId[Recall](id)

  def page(userId: User.ID, page: Int): Fu[Paginator[Recall]] = {
    val adapter = new Adapter[Recall](
      collection = coll,
      selector = $doc(
        "deleted" -> false,
        "createBy" -> userId
      ),
      projection = $empty,
      sort = $sort desc "createAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def history(userId: User.ID): Fu[List[Recall]] =
    coll.find(
      $doc(
        "deleted" -> false,
        "createBy" -> userId
      )
    ).sort($sort desc "createAt").list(20)

  def createBy(recall: Recall): Fu[Recall] =
    coll.insert(recall) inject recall

  def create(data: RecallData, userId: User.ID): Fu[Recall] = {
    data.tab match {
      case "gamedb" => data.gamedb match {
        case Some(gameId) => fuccess(makeRecall(data, userId, gameId))
        case None => fufail("no available gameId")
      }
      case "pgn" => data.pgn match {
        case Some(pgn) => addGame(pgn, userId).map { game =>
          makeRecall(data, userId, game.id)
        }
        case None => fufail("no available pgn")
      }
      case "chapter" => data.chapter match {
        case Some(chapter) => fetchChapterPgn(data.studyId(chapter), data.chapterId(chapter)) flatMap {
          case None => fufail("no available chapter")
          case Some(p) => addGame(p, userId).map { game =>
            makeRecall(data, userId, game.id)
          }
        }
        case None => fufail("no available chapter")
      }
      case "game" => data.game match {
        case Some(g) => fuccess(makeRecall(data, userId, data.gameId(g)))
        case None => fufail("no available game")
      }
      case _ => fufail("can not apply tab")
    }
  } flatMap { recall =>
    coll.insert(recall) inject recall
  }

  private def fetchChapterPgn(studyId: String, chapterId: String): Fu[Option[String]] =
    studyApi.chapterPgn(studyId, chapterId)

  private def addGame(pgn: String, userId: User.ID): Fu[Game] =
    importer(
      ImportData(pgn, none),
      user = userId.some
    )

  def gamePgn(data: RecallData, fetchCourseWarePgn: (String, String) => Fu[Option[(String, String)]]): Fu[GamePgnResult] = {
    data.tab match {
      case "gamedb" => data.gamedb match {
        case Some(gameId) => GameRepo.gameWithInitialFen(gameId) flatMap {
          case None => fufail("no available game")
          case Some((g, initialFen)) => toPgn(g, initialFen)
        }
        case None => fufail("no available gamedb")
      }
      case "pgn" => data.pgn match {
        case Some(pgn) => validToPgn(pgn)
        case None => fufail("no available pgn")
      }
      case "chapter" => data.chapter match {
        case Some(chapter) => fetchChapterPgn(data.studyId(chapter), data.chapterId(chapter)) flatMap {
          case None => fufail("no available chapter")
          case Some(p) => validToPgn(p, true)
        }
        case None => fufail("no available chapter")
      }
      case "game" => data.game match {
        case Some(url) => GameRepo.gameWithInitialFen(data.gameId(url)) flatMap {
          case None => fufail("no available game")
          case Some((g, initialFen)) => toPgn(g, initialFen)
        }
        case None => fufail("no available game")
      }
      case "courseWare" => data.courseWare match {
        case Some(courseWareData) => {
          val arr = courseWareData.split(":")
          fetchCourseWarePgn(arr(0), arr(1)) flatMap {
            case None => fufail("no available courseWare")
            case Some(p) => validToPgn(p._2, true).map { r => r.copy(name = p._1) }
          }
        }
        case None => fufail("no available courseWare")
      }
      case _ => fufail("can not apply tab")
    }
  }

  private def validToPgn(pgn: String, study: Boolean = false): Fu[GamePgnResult] =
    ImportData(pgn, none).preprocess(user = none) match {
      case Success(p) => toPgn(p.game.withId("-"), p.initialFen, study)
      case Failure(e) => fufail(e.toString())
    }

  private def toPgn(game: Game, initialFen: Option[FEN], study: Boolean = false): Fu[GamePgnResult] =
    pgnDump(
      game,
      initialFen,
      PgnDump.WithFlags(clocks = false, evals = false, opening = false)
    ).map { dump =>
        GamePgnResult(
          if (study) dump.tags(_.Event) | "" else s"${dump.tags(_.White) | ""} vs ${dump.tags(_.Black) | ""}",
          initialFen.map(_.value) | Forsyth.initial,
          dump.toString
        )
      }

  def update(recall: Recall, data: RecallEdit): Funit =
    coll.update(
      $id(recall.id),
      recall.copy(
        name = data.name,
        turns = data.turns,
        color = if (data.color == "all") None else chess.Color(data.color),
        orient = data.orient
      )
    ).void

  def delete(recall: Recall): Funit =
    coll.update(
      $id(recall.id),
      $set("deleted" -> true)
    ).void

  def finish(id: Option[Recall.ID], hashId: Option[String], win: Boolean, turns: Int, userId: User.ID): Funit = {
    bus.publish(lila.hub.actorApi.Recall(id, hashId, win, turns, userId), 'recallFinished)
    funit
  }

  private def makeRecall(data: RecallData, userId: User.ID, gameId: String): Recall =
    Recall.make(
      name = data.name,
      gameId = gameId,
      turns = data.turns,
      color = if (data.color == "all") None else chess.Color(data.color),
      orient = data.orient,
      userId = userId
    )

}
