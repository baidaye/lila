package lila

import lila.hub.TrouperMap
import lila.socket.WithSocket

package object racer extends PackageObject with WithSocket {

  type SocketMap = TrouperMap[Socket]

  private[racer] val logger = lila.log("racer")
}
