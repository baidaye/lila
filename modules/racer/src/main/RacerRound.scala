package lila.racer

import org.joda.time.DateTime
import scala.util.Random

case class RacerRound(
    _id: RacerRound.ID,
    no: RacerRound.No,
    raceId: RacerRace.ID,
    duration: Int,
    color: chess.Color,
    puzzles: List[RacerPuzzle],
    puzzleIds: List[Int],
    players: RacerRound.PlayerScore,
    status: Status,
    startedAt: DateTime
) {

  def id = _id

  def isFirst = no == 1

  def playerSize = players.size

  def playerIds = players.playerIds

  def hasPuzzle = puzzleIds.nonEmpty

  def durationMinute = duration / 60d

  def startsInMillis = startedAt.getMillis - nowMillis

  def isCountdown = startsInMillis < RacerRace.countdown * 1000

  def isCreated = status == Status.Created

  def isReadying = status == Status.Readying

  def isStarted = status == Status.Started

  def isFinished = status == Status.Finished

  def hasStarted = isStarted || startsInMillis <= 0

  def finishedAt = startedAt.plusSeconds(duration)

  def finished = isFinished || finishedAt.isBeforeNow

  def rating(puzzleId: Int) = puzzles.find(_.id == puzzleId).map(_.rating)

  def joinable(race: RacerRace) =
    race.typ match {
      case RacerRace.Type.Public => (isCreated || isReadying) && !isCountdown && playerSize < race.maxPlayers
      case _ => !finished && playerSize < race.maxPlayers
    }

  def join(playerId: RacerPlayer.ID) = this.synchronized {
    if (players.exists(playerId)) this
    else copy(
      players = players.join(playerId)
    )
  }

  def getScore(playerId: RacerPlayer.ID) = players.getScore(playerId)

  def setScore(playerId: RacerPlayer.ID, score: Int) = this.synchronized {
    copy(
      players = players.setScore(playerId, score)
    )
  }

}

object RacerRound {

  type ID = String
  type No = Int

  val random = new Random()

  def makeId(raceId: RacerRace.ID, no: No) = raceId + "@" + no

  def make(race: RacerRace, no: No, setting: RoundSetting) = RacerRound(
    _id = makeId(race.id, no),
    no = no,
    raceId = race.id,
    duration = race.roundTime,
    color = colors(random.nextInt(2)),
    puzzles = Nil,
    puzzleIds = setting.rule match {
      case RoundSettingChoiceRule.Range => Nil
      case RoundSettingChoiceRule.Capsule => setting.capsule.map(_.puzzles) | Nil
    },
    players = PlayerScore.empty,
    status = Status.Created,
    startedAt = race.roundStartedAt(no)
  )

  val colors = chess.Color.all.toArray

  case class PlayerScore(psMap: Map[RacerPlayer.ID, Int]) {

    def size = psMap.size

    def exists(playerId: RacerPlayer.ID) = psMap.exists(_._1 == playerId)

    def playerIds = psMap.keySet.toList

    def join(playerId: RacerPlayer.ID) = copy(
      psMap = psMap.+(playerId -> 0)
    )

    def getScore(playerId: RacerPlayer.ID) = psMap.get(playerId) | 0

    def setScore(playerId: RacerPlayer.ID, score: Int) = copy(
      psMap = psMap.updated(playerId, score)
    )
  }

  object PlayerScore {
    def empty = PlayerScore(Map.empty[RacerPlayer.ID, Int])
  }

}