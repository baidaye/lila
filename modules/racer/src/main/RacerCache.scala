package lila.racer

import lila.user.User
import scala.concurrent.duration._

final class RacerCache(asyncCache: lila.memo.AsyncCache.Builder) {

  private val raceCache = asyncCache.multi[RacerRace.ID, Option[RacerRace]](
    name = "racer.race",
    f = RacerRaceRepo.byId,
    expireAfter = _.ExpireAfterAccess(10 minutes),
    resultTimeout = 10 seconds
  )

  private val roundCache = asyncCache.multi[RacerRound.ID, Option[RacerRound]](
    name = "racer.round",
    f = RacerRoundRepo.byId,
    expireAfter = _.ExpireAfterAccess(10 minutes),
    resultTimeout = 10 seconds
  )

  private val playerCache = asyncCache.multi[RacerPlayer.ID, Option[RacerPlayer]](
    name = "racer.player",
    f = RacerPlayerRepo.byId,
    expireAfter = _.ExpireAfterAccess(10 minutes),
    resultTimeout = 10 seconds
  )

  def getRaceO(id: RacerRace.ID): Fu[Option[RacerRace]] = raceCache.get(id)
  def getRace(id: RacerRace.ID): Fu[RacerRace] = getRaceO(id).map { r => r err s"can not find race $id" }

  def getRoundO(roundId: RacerRound.ID): Fu[Option[RacerRound]] = roundCache.get(roundId)
  def getRoundO(id: RacerRace.ID, no: RacerRound.No): Fu[Option[RacerRound]] = getRoundO(RacerRound.makeId(id, no))
  def getRoundO(race: RacerRace): Fu[Option[RacerRound]] = getRoundO(RacerRound.makeId(race.id, race.currentRoundNo))
  def getRound(id: RacerRace.ID, no: RacerRound.No): Fu[RacerRound] = getRoundO(id, no).map { r => r err s"can not find round $no of $id" }
  def getRound(race: RacerRace): Fu[RacerRound] = getRoundO(race).map { r => r err s"can not find round ${race.currentRoundNo} of ${race.id}" }
  def getRoundList(roundIds: List[RacerRound.ID]): Fu[List[RacerRound]] = roundCache.getAll(roundIds).map { m => m.values.map(p => p.err("can not find round")).toList }

  def getPlayerO(playerId: RacerPlayer.ID): Fu[Option[RacerPlayer]] = playerCache.get(playerId)
  def getPlayerO(id: RacerRace.ID, userId: User.ID): Fu[Option[RacerPlayer]] = getPlayerO(RacerPlayer.makeId(id, userId))
  def getPlayer(playerId: RacerPlayer.ID): Fu[RacerPlayer] = getPlayerO(playerId).map { p => p err s"can not find player $playerId" }
  def getPlayer(id: RacerRace.ID, userId: User.ID): Fu[RacerPlayer] = getPlayerO(id, userId).map { p => p err s"can not find player $userId of $id" }
  def getPlayerList(playerIds: List[RacerPlayer.ID]): Fu[List[RacerPlayer]] = playerCache.getAll(playerIds).map { m => m.values.map(p => p.err("can not find player")).toList }

  def setRace(race: RacerRace): Unit = raceCache.put(race.id, race.some)
  def setRound(round: RacerRound) = roundCache.put(round.id, round.some)
  def setRounds(rounds: List[RacerRound]) = roundCache.putAll(rounds.map(r => r.id -> r.some).toMap)
  def setPlayer(player: RacerPlayer) = playerCache.put(player.id, player.some)
  def setPlayers(players: List[RacerPlayer]) = playerCache.putAll(players.map(r => r.id -> r.some).toMap)

  def invalidateRace(race: RacerRace) = {
    val roundIds = race.roundNoList.map { RacerRound.makeId(race.id, _) }
    getRoundList(roundIds).map { rounds =>
      val playerIds: Set[RacerPlayer.ID] = rounds.flatMap(_.playerIds).toSet
      playerIds foreach { playerId =>
        playerCache.put(playerId, None)
      }
    }
    roundIds.foreach { roundId =>
      roundCache.put(roundId, None)
    }
    raceCache.put(race.id, None)
  }

}
