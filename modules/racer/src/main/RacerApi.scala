package lila.racer

import lila.user.User
import org.joda.time.DateTime
import akka.actor.{ ActorSelection, ActorSystem }
import lila.hub.actorApi.relation.GetMarks
import lila.puzzle.PuzzleId
import makeTimeout.large
import akka.pattern.ask
import lila.hub.actorApi.member.IsAccept
import lila.hub.actorApi.puzzle.NextRacePuzzle
import lila.puzzle.PuzzleResult
import scala.concurrent.duration._

final class RacerApi(
    selector: RacerPuzzleApi,
    socketMap: SocketMap,
    system: ActorSystem,
    cache: RacerCache,
    memberActor: ActorSelection,
    markActor: ActorSelection
) {

  val supplement = 5.millis

  def lobbyList(me: User, clazzIds: List[String]): Fu[List[RacerRace.WithRound]] = for {
    races <- RacerRaceRepo.list(me, clazzIds)
    rounds <- RacerRoundRepo.byOrderedIds(races.map(r => RacerRound.makeId(r.id, r.currentRoundNo)))
  } yield (races zip rounds) map {
    case (race, round) => RacerRace.WithRound(race, round)
  }

  def setReady: Funit =
    RacerRaceRepo.innerCreated.map { races =>
      races foreach { race =>
        cache.setRace(race)
        cache.getRound(race).map { round =>
          setReadyToStart(race, round)
        }
      }
    }

  def createPublic: Fu[RacerRace] = create(RacerRace.makePublic)

  def createInner(user: User, data: DataForm.CreateData): Fu[RacerRace] = {
    create(RacerRace.make(
      name = data.name,
      typ = data.realType,
      organizer = data.organizer,
      maxPlayers = data.maxPlayers,
      duration = data.duration,
      roundTime = data.roundTime,
      restTime = data.restTime,
      roundSetting = data.toSetting(),
      startedAt = data.startedAt,
      createdAt = DateTime.now,
      createdBy = user.id
    ))
  }

  def create(race: RacerRace): Fu[RacerRace] = {
    val rounds = race.roundNoList.map { no => RacerRound.make(race, no, race.settingOf(no)) }
    RacerRaceRepo.insert(race) >>- cache.setRace(race) >>
      RacerRoundRepo.bulkInsert(rounds) >>- cache.setRounds(rounds) >> {
        if (race.isPublic) setStart(race)
        else injectPuzzle(race)
      } inject race
  }

  private def injectPuzzle(race: RacerRace, no: RacerRound.No = 1): Fu[RacerRound] = for {
    round <- cache.getRound(race.id, no)
    newRound <- {
      if (round.hasPuzzle) {
        if (round.puzzles.isEmpty) {
          selector.byIds(round.puzzleIds).flatMap { puzzles =>
            val newRound = round.copy(puzzles = puzzles)
            RacerRoundRepo.setPuzzles(newRound.id, puzzles) >>- cache.setRound(newRound) inject newRound
          }
        } else fuccess(round)
      } else {
        selector.puzzles(round.color, race.settingOf(no)).flatMap { puzzles =>
          val newRound = round.copy(puzzles = puzzles, puzzleIds = puzzles.map(_.id))
          RacerRoundRepo.setPuzzles(newRound.id, puzzles) >>- cache.setRound(newRound) inject newRound
        }
      }
    }
  } yield newRound

  def join(id: RacerRace.ID, no: RacerRound.No, userId: User.ID): Fu[RacerPlayer] = {
    isMemberAccept(userId) flatMap { accept =>
      if (accept) {
        cache.getRace(id).flatMap { race =>
          setPlayer(id, no, userId).flatMap { player =>
            setRoundPlayer(race, no, player.id) >> joinPublish(race, no) inject player
          }
        }
      } else {
        cache.getRound(id, no) map { round =>
          publishNotAccept(round, userId)
        } inject RacerPlayer.make(id, no, userId)
      }
    }
  }

  def setPlayer(id: RacerRace.ID, no: RacerRound.No, userId: User.ID): Fu[RacerPlayer] = {
    cache.getPlayerO(id, userId).flatMap {
      case Some(player) => {
        val newPlayer = player.getScoreO(no).fold { player.setScore(no, 0) } { _ => player }
        RacerPlayerRepo.update(newPlayer) >>- cache.setPlayer(newPlayer) inject newPlayer
      }
      case None => {
        val player = RacerPlayer.make(id, no, userId)
        RacerPlayerRepo.insert(player) >>- cache.setPlayer(player) inject player
      }
    }
  }

  def setRoundPlayer(race: RacerRace, no: RacerRound.No, playerId: RacerPlayer.ID, score: Option[Int] = None): Fu[RacerRound] = {
    cache.getRound(race.id, no).flatMap { round =>
      score match {
        case None => {
          val rd = round.join(playerId)
          RacerRoundRepo.update(rd) >>- cache.setRound(rd) inject rd
        }
        case Some(s) => {
          val rd = round.setScore(playerId, s)
          cache.setRound(rd)
          fuccess(rd)
        }
      }
    }
  }

  def setPlayerScore(race: RacerRace, no: RacerRound.No, playerId: RacerPlayer.ID, score: Int): Funit = {
    setRoundPlayer(race, no, playerId, score.some).flatMap { round =>
      cache.getPlayer(playerId).map { player =>
        cache.setPlayer(player.setScore(round.no, score))
      }
    }
  }

  private def setStart(race: RacerRace, no: RacerRound.No = 1): Funit =
    injectPuzzle(race, no).map { round =>
      setReadyToStart(race, round)
    }

  private def setReadyToStart(race: RacerRace, round: RacerRound): Funit = for {
    _ <- RacerRoundRepo.setStatus(round.id, Status.Readying) >>- cache.setRound(round.copy(status = Status.Readying))
    _ <- round.isFirst.?? { RacerRaceRepo.setStatus(race.id, Status.Readying) >>- cache.setRace(race.copy(status = Status.Readying)) }
  } yield {
    system.scheduler.scheduleOnce(round.startsInMillis.millis + supplement) {
      doStart(race.id, round.no)
    }
  }

  private def doStart(id: RacerRace.ID, no: RacerRound.No) =
    raceAndRound(id, no).flatMap {
      case (race, round) => {
        round.isFirst.?? { RacerRaceRepo.setStatus(id, Status.Started) >>- cache.setRace(race.copy(status = Status.Started)) } >>
          RacerRoundRepo.setStatus(round.id, Status.Started) >>- cache.setRound(round.copy(status = Status.Started)) >> {
            system.scheduler.scheduleOnce(round.duration.second + supplement) { doFinish(race.id, round.no) }
            funit
          }
      }
    }

  private def doFinish(id: RacerRace.ID, no: RacerRound.No): Funit =
    raceAndRound(id, no).flatMap {
      case (race, round) => {
        val newRound = round.copy(status = Status.Finished)
        RacerRoundRepo.update(newRound) >>- cache.setRound(newRound) >>
          cache.getPlayerList(round.playerIds).flatMap { players => RacerPlayerRepo.updateScore(players) } >>
          (!race.isRoundAllFinished).?? {
            val newRace = race.copy(currentRoundNo = race.currentRoundNo + 1)
            RacerRaceRepo.update(newRace) >>- cache.setRace(newRace) >> setStart(newRace, newRace.currentRoundNo)
          } >> race.isRoundAllFinished.?? {
            val newRace = race.copy(status = Status.Finished)
            RacerRaceRepo.update(newRace) >>- cache.setRace(newRace)
          } >>- publishFinish(newRound)
      }
    }

  def raceAndRoundO(id: RacerRace.ID, no: RacerRound.No): Fu[Option[(RacerRace, RacerRound)]] = {
    for {
      raceOption <- cache.getRaceO(id)
      roundOption <- cache.getRoundO(id, no)
    } yield (raceOption |@| roundOption).tupled
  }

  private def raceAndRound(id: RacerRace.ID, no: RacerRound.No): Fu[(RacerRace, RacerRound)] = {
    raceAndRoundO(id, no).map { _.err(s"can find race $id or round $no") }
  }

  def registerPlayerScore(id: RacerRace.ID, no: RacerRound.No, playerId: RacerPlayer.ID, score: Int): Funit =
    cache.getRace(id).flatMap { race =>
      for {
        _ <- setPlayerScore(race, no, playerId, score)
        round <- cache.getRound(id, no)
        players <- cache.getPlayerList(round.playerIds)
      } yield publishState(round, players)
    }

  def registerPlayerJoin(id: RacerRace.ID, no: RacerRound.No, userId: User.ID) =
    join(id, no, userId)

  private def joinPublish(race: RacerRace, no: RacerRound.No): Funit = for {
    round <- cache.getRound(race.id, no)
    players <- cache.getPlayerList(round.playerIds)
  } yield publishJoin(round, players)

  private def publishState(round: RacerRound, players: List[RacerPlayer]) = {
    socketMap.tell(round.id, State(round, players))
  }

  private def publishJoin(round: RacerRound, players: List[RacerPlayer]) = {
    socketMap.tell(round.id, Join(round, players))
  }

  private def publishFinish(round: RacerRound) = {
    socketMap.tell(round.id, Finish(round))
  }

  private def publishNotAccept(round: RacerRound, userId: User.ID) = {
    socketMap.tell(round.id, NotAccept(round, userId))
  }

  def feedback(race: RacerRace, round: RacerRound, puzzleId: PuzzleId, me: User, data: DataForm.Feedback) = {
    val userRating = me.perfs.puzzle.intRating
    val puzzleRating = round.rating(puzzleId) | -1
    system.lilaBus.publish(NextRacePuzzle(puzzleId, me.id), 'nextRacePuzzle)
    system.lilaBus.publish(
      PuzzleResult(
        puzzleId = puzzleId,
        userId = me.id,
        result = lila.puzzle.Result(data.win == 1),
        rating = userRating -> userRating,
        puzzleRating = puzzleRating -> puzzleRating,
        seconds = data.seconds,
        lines = Nil,
        timeout = None,
        source = PuzzleResult.Source.Race,
        metaData = PuzzleResult.MetaData(
          raceId = race.id.some
        ).some
      ), 'finishPuzzle
    )
  }

  def remove(race: RacerRace): Funit = {
    val round = cache.getRound(race.id, 1)
    RacerRaceRepo.remove(race.id) >>
      RacerRoundRepo.removeByRace(race.id) >>
      RacerPlayerRepo.removeByRace(race.id) >>-
      cache.invalidateRace(race) >>
      round.map(publishRemove)
  }

  private def publishRemove(round: RacerRound) = {
    socketMap.tell(round.id, Remove(round))
  }

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  def isMemberAccept(userId: User.ID): Fu[Boolean] =
    (memberActor ? IsAccept(userId, "TotalPuzzle")).mapTo[Boolean]

}
