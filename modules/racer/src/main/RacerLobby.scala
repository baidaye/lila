package lila.racer

import lila.user.User

final class RacerLobby(api: RacerApi, cache: RacerCache) {

  def join(userId: User.ID): Fu[RacerRace.ID] = {
    currentRace.flatMap { race =>
      cache.getRoundO(race) flatMap {
        case None => makeNewRace
        case Some(round) => {
          if (round.joinable(race)) fuccess(race.id)
          else makeNewRace
        }
      }
    }.flatMap { raceId =>
      api.join(raceId, 1, userId) inject raceId
    }
  }

  private var currentId: Fu[RacerRace.ID] = makeNewRace

  private def currentRace: Fu[RacerRace] = currentId.flatMap(cache.getRaceO).map {
    case None => RacerRace.makePublic //一个残缺比赛
    case Some(race) => race
  }

  private def makeNewRace: Fu[RacerRace.ID] = {
    currentId = api.createPublic.map(_.id)
    currentId
  }

}
