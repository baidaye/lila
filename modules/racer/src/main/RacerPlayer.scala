package lila.racer

import lila.user.User
import lila.racer.RacerPlayer.RoundScore

case class RacerPlayer(
    _id: RacerPlayer.ID,
    userId: User.ID,
    raceId: RacerRace.ID,
    scores: RoundScore
) {

  def id = _id

  def getScoreO(no: RacerRound.No) = scores.getScoreO(no)

  def getScore(no: RacerRound.No) = scores.getScore(no)

  def setScore(no: RacerRound.No, score: Int) = copy(scores = scores.setScore(no, score))

}

object RacerPlayer {

  type ID = User.ID

  type Score = Int

  def makeId(raceId: RacerRace.ID, userId: User.ID) = raceId + "@" + userId

  def make(raceId: RacerRace.ID, no: RacerRound.No, userId: User.ID) = RacerPlayer(
    _id = makeId(raceId, userId),
    userId = userId,
    raceId = raceId,
    scores = RoundScore.empty
  ) |> { p => p.setScore(no, 0) }

  case class RoundScore(rsMap: Map[RacerRound.No, Score]) {

    def getScoreO(no: RacerRound.No) = rsMap.get(no)

    def getScore(no: RacerRound.No) = getScoreO(no) | 0

    def setScore(no: RacerRound.No, score: Int) = copy(
      rsMap = rsMap.get(no).fold(rsMap.+(no -> score)) { _ =>
        rsMap.updated(no, score)
      }
    )
  }

  object RoundScore {

    def empty = RoundScore(Map.empty[RacerRound.No, Int])
  }

}
