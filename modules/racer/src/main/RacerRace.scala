package lila.racer

import org.joda.time.DateTime
import ornicar.scalalib.Random
import lila.user.User
import RacerRace._

case class RacerRace(
    _id: ID,
    name: String,
    typ: Type,
    organizer: String, //  User.lichessId or teamId or classId
    maxPlayers: Int,
    duration: Int, // second
    roundTime: Int, // second
    restTime: Int, // second
    roundSetting: List[RoundSetting],
    currentRoundNo: Int,
    status: Status,
    startedAt: DateTime,
    finishedAt: DateTime,
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id = _id

  def startsInMillis = startedAt.getMillis - nowMillis

  def isCountdown = startsInMillis < countdown * 1000

  def isCreated = status == Status.Created

  def isReadying = status == Status.Readying

  def hasStarted = status == Status.Started || startsInMillis <= 0

  def finished = status == Status.Finished || finishedAt.isBeforeNow

  def canDelete = isCreated || isReadying

  def isPublic = typ == RacerRace.Type.Public

  def isCreator(userId: User.ID) = createdBy == userId

  def settingOf(no: RacerRound.No) = roundSetting.find(_.no == no) err s"can not find round ${no}"

  def roundStartedAt(no: RacerRound.No) = startedAt.plusSeconds((no - 1) * (roundTime + restTime))

  def roundCount = (duration + restTime) / (roundTime + restTime)

  def roundNoList = (1 to roundCount) toList

  def isRoundAllFinished = currentRoundNo >= roundCount

  def isTeamInner = typ == Type.TeamInner
  def isClazzInner = typ == Type.ClazzInner
  def timeBetween = startedAt.toString("M月dd日 HH:mm") + "~" + finishedAt.toString("M月dd日 HH:mm")
  def durationText = DataForm.durationChoices.find(_._1 == duration).??(_._2)
}

object RacerRace {

  type ID = String

  def countdown = 5 // second

  def defaultStartIn = 30 // second

  def defaultMaxPlayers = 10

  def defaultDuration = (1 + 0.5) * 60 toInt // second

  def makePublic = {
    val now = DateTime.now()
    make(
      name = "公开赛",
      typ = Type.Public,
      organizer = User.lichessId,
      maxPlayers = defaultMaxPlayers,
      duration = defaultDuration,
      roundTime = defaultDuration,
      restTime = 0,
      roundSetting = RacerRace.defaultSetting,
      startedAt = now.plusSeconds(defaultStartIn),
      createdAt = now,
      createdBy = User.lichessId
    )
  }

  def make(
    name: String,
    typ: Type,
    organizer: String,
    maxPlayers: Int,
    duration: Int,
    roundTime: Int,
    restTime: Int,
    roundSetting: List[RoundSetting],
    startedAt: DateTime,
    createdAt: DateTime,
    createdBy: User.ID
  ) = RacerRace(
    _id = Random.nextString(8),
    name = name,
    typ = typ,
    organizer = organizer,
    maxPlayers = maxPlayers,
    duration = duration,
    roundTime = roundTime,
    restTime = restTime,
    roundSetting = roundSetting,
    currentRoundNo = 1,
    status = Status.Created,
    startedAt = startedAt,
    finishedAt = startedAt.plusSeconds(duration),
    createdAt = createdAt,
    createdBy = createdBy
  )

  def defaultSetting = List(
    RoundSetting(
      no = 1,
      rule = RoundSettingChoiceRule.Range,
      ranges = RoundSettingRangeNum(20, 20, 20).some,
      capsule = None
    )
  )

  sealed abstract class Type(val id: String, val name: String)
  object Type {
    case object Public extends Type("public", "公开赛")
    case object TeamInner extends Type("team-inner", "俱乐部内部赛")
    case object ClazzInner extends Type("clazz-inner", "班级内部赛")

    val all = List(Public, TeamInner, ClazzInner)

    def apply(id: String) = all.find(_.id == id) err s"Bad Type $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => r.id -> r.name }

    def byId = all.map { x => x.id -> x }.toMap
  }

  case class WithRound(race: RacerRace, round: RacerRound)

  case class WithRounds(race: RacerRace, rounds: List[RacerRound]) {
    def maxPlayer = rounds.maxBy(_.playerSize).playerSize
  }

}

case class RoundSetting(
    no: Int,
    rule: RoundSettingChoiceRule,
    ranges: Option[RoundSettingRangeNum],
    capsule: Option[RoundSettingCapsule]
) {

  def size = rule match {
    case RoundSettingChoiceRule.Range => ranges.map(r => r.num1 + r.num2 + r.num3) | 0
    case RoundSettingChoiceRule.Capsule => capsule.map(_.puzzles.length) | 0
  }

}

case class RoundSettingRangeNum(num1: Int, num2: Int, num3: Int)
case class RoundSettingCapsule(id: String, name: String, puzzles: List[Int])

sealed abstract class RoundSettingChoiceRule(val id: String, val name: String)
object RoundSettingChoiceRule {
  case object Range extends RoundSettingChoiceRule("range", "默认")
  case object Capsule extends RoundSettingChoiceRule("capsule", "指定列表")

  val all = List(Range, Capsule)

  def apply(id: String) = all.find(_.id == id) err s"Bad Type $id"

  def keySet = all.map(_.id).toSet

  def list = all.map { r => r.id -> r.name }

  def byId = all.map { x => x.id -> x }.toMap
}