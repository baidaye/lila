package lila.racer

import chess.Color
import lila.db.BSON
import lila.db.dsl.Bdoc
import lila.racer.RacerPlayer.RoundScore
import lila.racer.RacerPuzzle.BSONFields
import reactivemongo.bson.{ BSONDocument, BSONHandler, BSONInteger, BSONString, Macros }
import reactivemongo.bson._
import scala.collection.breakOut

object BSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler

  implicit val ColorBSONHandler = new BSONHandler[BSONBoolean, chess.Color] {
    def read(b: BSONBoolean) = chess.Color(b.value)
    def write(c: chess.Color) = BSONBoolean(c.white)
  }

  implicit val StatusBSONHandler = new BSONHandler[BSONInteger, Status] {
    def read(b: BSONInteger): Status = Status(b.value)
    def write(x: Status) = BSONInteger(x.id)
  }

  private[racer] implicit val PlayerScoreBSONHandler = new BSONHandler[Bdoc, RacerRound.PlayerScore] {
    private val mapHandler = BSON.MapValue.MapHandler[String, Int]
    def read(b: Bdoc) = RacerRound.PlayerScore(mapHandler read b map {
      case (playerId, score) => playerId -> score
    })
    def write(x: RacerRound.PlayerScore) = BSONDocument(x.psMap.mapValues(BSONInteger))
  }

  private[racer] implicit val RoundScoreBSONHandler = new BSONHandler[Bdoc, RoundScore] {
    private val mapHandler = BSON.MapValue.MapHandler[String, Int]
    def read(b: Bdoc) = RoundScore(mapHandler read b map {
      case (no, score) => no.toInt -> score
    })
    def write(x: RoundScore) = BSONDocument(x.rsMap.mapKeys(_.toString).mapValues(BSONInteger))
  }

  implicit val TypeBSONHandler = new BSONHandler[BSONString, RacerRace.Type] {
    def read(b: BSONString): RacerRace.Type = RacerRace.Type(b.value)
    def write(x: RacerRace.Type) = BSONString(x.id)
  }

  private[racer] implicit val lineBSONHandler = new BSONHandler[BSONDocument, List[Line]] {
    private def readMove(move: String) = chess.Pos.doublePiotrToKey(move take 2) match {
      case Some(m) => s"$m${move drop 2}"
      case _ => sys error s"Invalid piotr move notation: $move"
    }
    def read(doc: BSONDocument): List[Line] = doc.elements.map {
      case BSONElement(move, BSONBoolean(true)) => Win(readMove(move))

      case BSONElement(move, BSONBoolean(false)) => Retry(readMove(move))

      case BSONElement(move, more: BSONDocument) =>
        Node(readMove(move), read(more))

      case BSONElement(move, value) =>
        throw new Exception(s"Can't read value of $move: $value")
    }(breakOut)
    private def writeMove(move: String) = chess.Pos.doubleKeyToPiotr(move take 4) match {
      case Some(m) => s"$m${move drop 4}"
      case _ => sys error s"Invalid move notation: $move"
    }
    def write(lines: List[Line]): BSONDocument = BSONDocument(lines map {
      case Win(move) => writeMove(move) -> BSONBoolean(true)
      case Retry(move) => writeMove(move) -> BSONBoolean(false)
      case Node(move, lines) => writeMove(move) -> write(lines)
    })
  }

  import lila.db.BSON.BSONJodaDateTimeHandler
  private implicit val tagsHandler = lila.db.dsl.bsonArrayToListHandler[String]
  implicit val importBSONHandler = Macros.handler[ImportMeta]

  private[racer] implicit val PuzzleBSONHandler = new BSON[RacerPuzzle] {

    import BSONFields._

    def reads(r: BSON.Reader): RacerPuzzle =
      RacerPuzzle(
        id = r int id,
        gameId = r str gameId,
        history = r str history split ' ' toList,
        fen = r str fen,
        lines = r.get[List[Line]](lines),
        rating = r.get[BSONDocument](perf).getAs[BSONDocument]("gl").?? { b => b.getAs[Double]("r").??(_.toInt) },
        depth = r int depth,
        color = Color(r bool white),
        retry = r boolD retry,
        ipt = r.getO[ImportMeta](ipt)
      )

    def writes(w: BSON.Writer, o: RacerPuzzle) = BSONDocument()
  }

  private[racer] implicit val ChoiceRuleBSONHandler = new BSONHandler[BSONString, RoundSettingChoiceRule] {
    def read(b: BSONString): RoundSettingChoiceRule = RoundSettingChoiceRule(b.value)
    def write(x: RoundSettingChoiceRule) = BSONString(x.id)
  }

  private[racer] implicit val RoundSettingRangeNumBSONHandler = Macros.handler[RoundSettingRangeNum]

  private[racer] implicit val PuzzleIdsArrayHandler = lila.db.dsl.bsonArrayToListHandler[Int]

  private[racer] implicit val RoundSettingCapsuleSONHandler = Macros.handler[RoundSettingCapsule]

  private[racer] implicit val RoundSettingBSONHandler = Macros.handler[RoundSetting]

  private[racer] implicit val RoundSettingArrayHandler = lila.db.dsl.bsonArrayToListHandler[RoundSetting]

  private[racer] implicit val RacerPuzzleArrayHandler = lila.db.dsl.bsonArrayToListHandler[RacerPuzzle]

  private[racer] implicit val RacerPlayerBSONHandler = Macros.handler[RacerPlayer]

  private[racer] implicit val RacerRoundBSONMacrosHandler = Macros.handler[RacerRound]

  private[racer] implicit val RacerRoundBSONHandler = new BSON[RacerRound] {
    def reads(r: BSON.Reader): RacerRound = RacerRoundBSONMacrosHandler.read(r.doc)
    def writes(w: BSON.Writer, o: RacerRound) = RacerRoundBSONMacrosHandler.write(o.copy(puzzles = Nil))
  }

  private[racer] implicit val RacerRaceBSONHandler = Macros.handler[RacerRace]

}
