package lila.racer

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.common.{ AtMost, Every, LightUser, ResilientScheduler }
import lila.socket.History
import scala.concurrent.duration._

final class Env(
    config: Config,
    hub: lila.hub.Env,
    db: lila.db.Env,
    lightUser: LightUser.Getter,
    lightUserSync: LightUser.GetterSync,
    asyncCache: lila.memo.AsyncCache.Builder,
    system: ActorSystem
) {

  private val settings = new {
    val CollectionRacer = config getString "collection.racer"
    val CollectionPlayer = config getString "collection.player"
    val CollectionRound = config getString "collection.round"
    val CollectionPuzzle = config getString "collection.puzzle"
    val SriTimeout = config duration "sri.timeout"
    val SocketTimeout = config duration "socket.timeout"
    val HistoryMessageTtl = config duration "history.message.ttl"
    val AnimationDuration = config duration "animation.duration"
  }
  import settings._

  lazy val racerColl = db(CollectionRacer)
  lazy val playerColl = db(CollectionPlayer)
  lazy val roundColl = db(CollectionRound)

  lazy val forms = new DataForm()

  lazy val jsonView = new JsonView(
    animationDuration = AnimationDuration,
    lightUserSync = lightUserSync
  )

  lazy val selector = new RacerPuzzleApi(
    coll = db(CollectionPuzzle)
  )

  private val socketMap: SocketMap = lila.socket.SocketMap[Socket](
    system = system,
    mkTrouper = (roundId: String) => new Socket(
      system = system,
      roundId = roundId,
      history = new History(ttl = HistoryMessageTtl),
      lightUser = lightUser,
      jsonView = jsonView,
      sriTtl = SriTimeout,
      keepMeAlive = () => socketMap touch roundId
    ),
    accessTimeout = SocketTimeout,
    monitoringName = "racer.socketMap",
    broomFrequency = 3691 millis
  )

  lazy val api = new RacerApi(
    selector = selector,
    socketMap = socketMap,
    system = system,
    cache = cache,
    memberActor = hub.member,
    markActor = hub.relation
  )

  lazy val cache = new RacerCache(
    asyncCache = asyncCache
  )

  lazy val lobby = new RacerLobby(api, cache)

  lazy val socketHandler = new SocketHandler(
    hub = hub,
    socketMap = socketMap,
    api = api
  )

  // 比赛开始
  ResilientScheduler(
    every = Every(5 seconds),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 30 seconds
  ) { api.setReady }(system)

}

object Env {

  lazy val current: Env = "racer" boot new Env(
    config = lila.common.PlayApp loadConfig "racer",
    hub = lila.hub.Env.current,
    db = lila.db.Env.current,
    lightUser = lila.user.Env.current.lightUser,
    lightUserSync = lila.user.Env.current.lightUserSync,
    asyncCache = lila.memo.Env.current.asyncCache,
    system = lila.common.PlayApp.system
  )

}
