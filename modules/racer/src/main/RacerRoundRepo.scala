package lila.racer

import lila.db.dsl._
import reactivemongo.api.ReadPreference

object RacerRoundRepo {

  private[racer] lazy val coll = Env.current.roundColl

  import BSONHandlers.RacerRoundBSONHandler

  def insert(round: RacerRound): Funit = coll.insert(round).void

  def update(round: RacerRound): Funit = coll.update($id(round.id), round).void

  def byId(id: RacerRound.ID): Fu[Option[RacerRound]] = coll.byId(id).flatMap {
    case None => fuccess(none[RacerRound])
    case Some(round) => Env.current.selector.byIds(round.puzzleIds).map { puzzles =>
      round.copy(puzzles = puzzles).some
    }
  }

  def removeByRace(raceId: RacerRace.ID): Funit =
    coll.remove($doc("raceId" -> raceId)).void

  def setPuzzles(id: RacerRound.ID, puzzles: List[RacerPuzzle]): Funit =
    coll.update($id(id), $set("puzzleIds" -> puzzles.map(_.id))).void

  def bulkInsert(roundList: List[RacerRound]): Funit = coll.bulkInsert(
    documents = roundList.map(RacerRoundBSONHandler.write).toStream,
    ordered = true
  ).void

  def getByRace(raceId: RacerRace.ID): Fu[List[RacerRound]] =
    coll.find($doc("raceId" -> raceId))
      .sort($doc("no" -> 1))
      .list[RacerRound]()

  def getByRaces(raceIds: List[RacerRace.ID]): Fu[List[RacerRound]] =
    coll.find($doc("raceId" $in raceIds))
      .list[RacerRound]()

  def setStatus(id: RacerRound.ID, status: Status): Funit =
    coll.update($id(id), $set("status" -> status.id)).void

  def byOrderedIds(ids: Seq[RacerRound.ID]): Fu[List[RacerRound]] =
    coll.byOrderedIds[RacerRound, RacerRound.ID](ids, readPreference = ReadPreference.secondaryPreferred)(_.id)

}
