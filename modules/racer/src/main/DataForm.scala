package lila.racer

import play.api.data._
import play.api.data.Forms._
import lila.common.Form._
import lila.user.User
import org.joda.time.DateTime

final class DataForm {

  import DataForm._

  def createOf(user: User) = create(user) fill CreateData.default

  def create(user: User) = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 30),
    "typ" -> stringIn(RacerRace.Type.list),
    "organizer" -> nonEmptyText(minLength = 6, maxLength = 8),
    "maxPlayers" -> number(min = 2, max = 50),
    "duration" -> numberIn(durationChoices),
    "roundTime" -> numberIn(roundTimeChoices),
    "restTime" -> numberIn(restTimeChoices),
    "roundSetting" -> list(mapping(
      "no" -> number(min = 1, max = 100),
      "capsule" -> optional(mapping(
        "id" -> nonEmptyText,
        "name" -> nonEmptyText,
        "puzzles" -> nonEmptyText
      )(RoundSettingCapsuleData.apply)(RoundSettingCapsuleData.unapply)),
      "ranges" -> optional(mapping(
        "num1" -> number(min = 0, max = 60),
        "num2" -> number(min = 0, max = 60),
        "num3" -> number(min = 0, max = 60)
      )(RoundSettingRangeNum.apply)(RoundSettingRangeNum.unapply))
    )(RoundSettingData.apply)(RoundSettingData.unapply)),
    "startedAt" -> futureDateTime
  )(CreateData.apply)(CreateData.unapply)
    .verifying("每个账号最多可以同时建立10个房间，请等待之前的房间关闭后再建立新房间！", d => d.limit(user)))

  val feedback = Form(mapping(
    "win" -> number,
    "seconds" -> number
  )(Feedback.apply)(Feedback.unapply))

}

object DataForm {

  val durationChoices = Seq(10 * 60 -> "10分钟", 15 * 60 -> "15分钟", 30 * 60 -> "30分钟", 60 * 60 -> "60分钟")
  val roundTimeChoices = Seq(1 * 60 -> "1分钟", (1.5 * 60).toInt -> "1分半", 2 * 60 -> "2分钟", 3 * 60 -> "3分钟", 5 * 60 -> "5分钟")
  val restTimeChoices = Seq(10 -> "10秒", 15 -> "15秒", 30 -> "30秒", 60 -> "1分钟")

  case class CreateData(
      name: String,
      typ: String,
      organizer: String,
      maxPlayers: Int,
      duration: Int,
      roundTime: Int,
      restTime: Int,
      roundSetting: List[RoundSettingData],
      startedAt: DateTime
  ) {

    def realType = RacerRace.Type(typ)

    def limit(user: User) = RacerRaceRepo.countUnFinished(user).map(_ < 10).awaitSeconds(3)

    def toSetting() = roundSetting.map { setting =>
      RoundSetting(
        no = setting.no,
        rule = setting.rule,
        ranges = setting.ranges,
        capsule = setting.capsule.map { capsule =>
          RoundSettingCapsule(
            id = capsule.id,
            name = capsule.name,
            puzzles = capsule.puzzles.split(",").toList.map(_.toInt)
          )
        }
      )
    }
  }

  object CreateData {

    def default = {
      val now = DateTime.now()
      CreateData(
        name = "",
        typ = RacerRace.Type.TeamInner.id,
        organizer = "",
        maxPlayers = 20,
        duration = 30 * 60,
        roundTime = 3 * 60,
        restTime = 30,
        roundSetting = Nil,
        startedAt = now.withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).plusHours(1)
      )
    }
  }

  case class RoundSettingData(
      no: Int,
      capsule: Option[RoundSettingCapsuleData],
      ranges: Option[RoundSettingRangeNum]
  ) {

    def rule = capsule match {
      case None => RoundSettingChoiceRule.Range
      case Some(_) => RoundSettingChoiceRule.Capsule
    }
  }

  case class RoundSettingCapsuleData(id: String, name: String, puzzles: String)

  case class Feedback(win: Int, seconds: Int)

}
