package lila.racer

import lila.user.User
import akka.actor._
import lila.socket.actorApi.{ Connected => _ }
import lila.socket.{ Historical, History, Socket => SocketMode, SocketTrouper }
import play.api.libs.iteratee._
import play.api.libs.json._
import scala.concurrent.duration._

private[racer] final class Socket(
    system: ActorSystem,
    roundId: RacerRound.ID,
    protected val history: History[Messadata],
    lightUser: lila.common.LightUser.Getter,
    jsonView: JsonView,
    sriTtl: Duration,
    keepMeAlive: () => Unit
) extends SocketTrouper[Member](system, sriTtl) with Historical[Member, Messadata] {

  protected def receiveSpecific = {

    case PlayerJoin(sri, user, version, promise) =>
      val (enumerator, channel) = Concurrent.broadcast[JsValue]
      val member = Member(channel, user)
      addMember(sri, member)
      promise success Connected(
        prependEventsSince(version, enumerator, member),
        member
      )

    case State(round, players) => publishState(round, players)

    case Join(round, players) => publishJoin(round, players)

    case Finish(round) => publishFinish(round)

    case Remove(round) => publishRemove(round)

    case NotAccept(round, userId) =>
      publishNotAccept(round, userId)
  }

  override protected def broom: Unit = {
    super.broom
    if (members.nonEmpty) keepMeAlive()
  }

  override protected def afterQuit(sri: SocketMode.Sri, member: Member) = members -= sri.value

  override protected def shouldSkipMessageFor(message: Message, member: Member): Boolean = false

  def publishState(round: RacerRound, players: List[RacerPlayer]) =
    notifyAll("racerState", jsonView.state(round, players))

  def publishJoin(round: RacerRound, players: List[RacerPlayer]) =
    notifyAll("racerJoin", jsonView.state(round, players))

  def publishFinish(round: RacerRound) =
    notifyAll("racerFinish", jsonView.finish(round))

  def publishRemove(round: RacerRound) =
    notifyAll("racerRemove", jsonView.remove(round))

  def publishNotAccept(round: RacerRound, userId: User.ID) =
    membersByUserId(userId) foreach { member =>
      notifyMember("notAccept", jsonView.notAccept(round))(member)
    }

}
