package lila.racer

import lila.socket.Socket.{ Sri, SocketVersion }
import lila.socket.SocketMember
import scala.concurrent.Promise
import lila.user.User

private[racer] case class Member(
    channel: JsChannel,
    userId: Option[String],
    troll: Boolean
) extends SocketMember

private[racer] object Member {
  def apply(channel: JsChannel, user: Option[User]): Member = Member(
    channel = channel,
    userId = user map (_.id),
    troll = user.??(_.troll)
  )
}

private[racer] case class Messadata(trollish: Boolean = false)
private[racer] case class Connected(enumerator: JsEnumerator, member: Member)
private[racer] case class PlayerJoin(sri: Sri, user: Option[User], version: Option[SocketVersion], promise: Promise[Connected])

private[racer] case class State(round: RacerRound, players: List[RacerPlayer])
private[racer] case class Join(round: RacerRound, players: List[RacerPlayer])
private[racer] case class Finish(round: RacerRound)
private[racer] case class Remove(round: RacerRound)
private[racer] case class NotAccept(round: RacerRound, userId: User.ID)

