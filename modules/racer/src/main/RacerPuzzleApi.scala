package lila.racer

import lila.db.dsl._
import reactivemongo.bson._
import RacerPuzzle.{ BSONFields => F }
import chess.Color
import reactivemongo.api.ReadPreference
import reactivemongo.api.collections.bson.BSONBatchCommands.AggregationFramework._

final class RacerPuzzleApi(coll: Coll) {

  import BSONHandlers.PuzzleBSONHandler

  val notMachine = $doc(F.id $lt 1000000)

  val enabled = $doc(F.enabled -> true)

  val projection = $doc(F.id -> 1, F.gameId -> 1, F.history -> 1, F.fen -> 1, F.lines -> 1, F.depth -> 1, F.retry -> 1, F.white -> 1, F.rating -> 1, F.ipt -> 1)

  def byIds(ids: List[Int]): Fu[List[RacerPuzzle]] =
    coll.byOrderedIds[RacerPuzzle, Int](ids, projection.some, readPreference = ReadPreference.secondaryPreferred)(_.id)

  def filter(puzzles: List[RacerPuzzle], color: Color): List[RacerPuzzle] =
    puzzles.filter(p => p.color == color && p.depth <= 3 && !p.retry)

  def puzzles(color: Color, setting: RoundSetting): Fu[List[RacerPuzzle]] = {
    val size = 60
    for {
      lst1 <- range(500, 1000, setting.ranges.map(_.num1 * 3) | size)
      lst2 <- range(1000, 1500, setting.ranges.map(_.num2 * 3) | size)
      lst3 <- range(1500, 2000, (setting.ranges.map(_.num3 * 3) | size) * 2)
    } yield {
      filter(lst1, color).take(setting.ranges.map(_.num1) | (size / 3)) ++
        filter(lst2, color).take(setting.ranges.map(_.num2) | (size / 3)) ++
        filter(lst3, color).take(setting.ranges.map(_.num3) | (size / 3))
    }
  }

  def range(minRating: Int, maxRating: Int, size: Int): Fu[List[RacerPuzzle]] = {
    val selector = notMachine ++ $doc(F.rating -> ($gte(minRating) ++ $lte(maxRating)))
    coll.aggregateList(
      Match(selector),
      List(
        Sample(size),
        Project(projection)
      ),
      maxDocs = size
    ).map { _.flatMap(_.asOpt[RacerPuzzle]) }
  }

}
