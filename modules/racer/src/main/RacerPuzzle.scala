package lila.racer

import chess.Color
import chess.format.{ Forsyth, Uci }

case class RacerPuzzle(
    id: Int,
    gameId: String,
    history: List[String],
    fen: String,
    lines: List[Line],
    rating: Int,
    depth: Int,
    color: Color,
    retry: Boolean,
    ipt: Option[ImportMeta] = None
) {

  def fenFormat = Forsyth << fen map (Forsyth >> _) err s"Bad fen $fen"

  def fenAfterInitialMove: Option[String] = {
    ipt.fold(fenAfterInitialMove2) { i =>
      if (i.hasLastMove) fenAfterInitialMove2 else fenFormat.some
    }
  }

  def fenAfterInitialMove2: Option[String] = {
    for {
      sit1 <- Forsyth << fen
      sit2 <- sit1.move(initialMove).toOption.map(_.situationAfter)
    } yield Forsyth >> sit2
  }

  def initialMove: Uci.Move = history.lastOption flatMap Uci.Move.apply err s"Bad initial move $this"

  def initialUci: String =
    if (ipt.isEmpty) {
      initialMove.uci
    } else ""

}

object RacerPuzzle {

  object BSONFields {
    val id = "_id"
    val gameId = "gameId"
    val history = "history"
    val fen = "fen"
    val lines = "lines"
    val depth = "depth"
    val retry = "retry"
    val white = "white"
    val perf = "perf"
    val rating = s"$perf.gl.r"
    val enabled = "enabled"
    val likers = "likers"
    val taggers = "taggers"
    val ipt = "ipt"
  }

}

case class ImportMeta(
    pgn: String,
    tags: Option[List[String]],
    hasLastMove: Boolean,
    fenAfterMove: Option[String],
    userId: Option[String],
    date: Option[String]
)