package lila.racer

import play.api.libs.json._
import lila.common.LightUser

final class JsonView(
    animationDuration: scala.concurrent.duration.Duration,
    lightUserSync: LightUser.GetterSync
) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "coords" -> p.coords,
    "is3d" -> p.is3d,
    "destination" -> p.destination,
    "rookCastle" -> p.rookCastle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "animation" -> p.animationFactor * animationDuration.toMillis
  )

  def data(
    race: RacerRace,
    round: RacerRound,
    me: RacerPlayer,
    players: List[RacerPlayer],
    markMap: Map[String, Option[String]],
    canJoin: Boolean,
    accept: Boolean
  ) =
    Json.obj(
      "notAccept" -> !accept,
      "race" -> Json.obj(
        "id" -> race.id,
        "name" -> race.name,
        "lobby" -> race.isPublic,
        "owner" -> race.isCreator(me.userId),
        "countdown" -> RacerRace.countdown,
        "initial" -> race.roundTime,
        "maxRounds" -> race.roundCount,
        "maxPlayers" -> race.maxPlayers
      ),
      "round" -> Json.obj(
        "id" -> round.id,
        "no" -> round.no,
        "rule" -> race.settingOf(round.no).rule.id,
        "color" -> round.color.name,
        "puzzles" -> round.puzzles.map(puzzle)
      ),
      "marks" -> markJson(markMap),
      "player" -> player(me, round.no).add("canJoin", canJoin)
    ) ++ state(round, players)

  // socket updates
  def state(round: RacerRound, players: List[RacerPlayer]) =
    Json.obj(
      "startsIn" -> round.startsInMillis,
      "players" -> players.map(p => player(p, round.no))
    )

  def puzzle(p: RacerPuzzle) = {
    val sol = Line.solution(p.lines).take(p.depth * 2 - 1)
    Json.obj(
      "id" -> p.id,
      "fen" -> p.fenAfterInitialMove,
      "lastMove" -> p.initialUci,
      "color" -> p.color.name,
      "depth" -> p.depth,
      "rating" -> p.rating,
      "lines" -> sol.mkString(" ")
    )
  }

  def player(p: RacerPlayer, no: RacerRound.No) = {
    val lightUser = lightUserSync(p.userId)
    Json.obj(
      "id" -> p.id,
      "userId" -> p.userId,
      "score" -> p.getScore(no)
    )
      .add("username", lightUser.map(_.name))
      .add("title", lightUser.map(_.title))
  }

  def finish(round: RacerRound) = Json.obj("no" -> round.no)

  def remove(round: RacerRound) = Json.obj("raceId" -> round.raceId, "no" -> round.no)

  def notAccept(round: RacerRound) = Json.obj("raceId" -> round.raceId, "no" -> round.no)

  def markJson(markMap: Map[String, Option[String]]) =
    Json.toJson(markMap.mapValues(v => v | ""))

  private def userMark(lightUser: LightUser, markMap: Map[String, Option[String]]): String = {
    markMap.get(lightUser.id).fold(none[String]) { m => m } | lightUser.name
  }

}

