package lila.racer

import play.api.libs.json._

sealed trait Line
case class Node(move: String, lines: List[Line]) extends Line
case class Win(move: String) extends Line
case class Retry(move: String) extends Line

object Line {

  def toJson(lines: List[Line]): JsObject = JsObject(lines map {
    case Win(move) => move -> JsBoolean(true)
    case Retry(move) => move -> JsBoolean(false)
    case Node(move, more) => move -> toJson(more)
  })

  def solution(lines: List[Line]): List[String] = {

    def getIn(lines: List[Line], path: List[String]): List[Line] = path match {
      case Nil => lines
      case head :: rest => lines collectFirst {
        case Node(move, lines) if move == head => getIn(lines, rest)
        case w @ Win(move) if move == head => List(w)
        case r @ Retry(move) if move == head => List(r)
      } getOrElse Nil
    }

    def loop(paths: List[List[String]]): List[String] = paths match {
      case Nil => Nil
      case path :: siblings => getIn(lines, path) match {
        case List(Win(m)) => path :+ m
        case List(Retry(_)) => loop(siblings)
        case ahead => ahead.collectFirst {
          case Win(m) => path :+ m
        } | {
          val children = ahead collect { case Node(m, ls) => path :+ m }
          loop(siblings ::: children)
        }
      }
    }

    lines.collectFirst {
      case Win(move) => List(move)
    } | loop(lines collect {
      case Node(move, _) => List(move)
    })

  }

}
