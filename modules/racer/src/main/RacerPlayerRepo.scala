package lila.racer

import lila.db.dsl._

object RacerPlayerRepo {

  private[racer] lazy val coll = Env.current.playerColl

  import BSONHandlers.RacerPlayerBSONHandler

  def insert(player: RacerPlayer): Funit = coll.insert(player).void

  def update(player: RacerPlayer): Funit = coll.update($id(player.id), player).void

  def byId(id: RacerPlayer.ID): Fu[Option[RacerPlayer]] = coll.byId(id)

  def removeByRace(raceId: RacerRace.ID): Funit =
    coll.remove($doc("raceId" -> raceId)).void

  def bulkInsert(roundList: List[RacerPlayer]): Funit = coll.bulkInsert(
    documents = roundList.map(RacerPlayerBSONHandler.write).toStream,
    ordered = true
  ).void

  def getByRound(roundId: RacerRound.ID): Fu[List[RacerPlayer]] =
    RacerRoundRepo.byId(roundId).flatMap {
      case None => fuccess(nil)
      case Some(round) => coll.byIds[RacerPlayer](round.playerIds)
    }

  def updateScore(players: List[RacerPlayer]): Funit =
    players.map(update).sequenceFu.void

}
