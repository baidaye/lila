package lila.racer

sealed abstract class Status(val id: Int, val name: String) extends Ordered[Status] {
  def compare(other: Status) = Integer.compare(id, other.id)
  def is(s: Status): Boolean = this == s
  def is(f: Status.type => Status): Boolean = is(f(Status))
}

object Status {

  case object Created extends Status(10, "新建")
  case object Readying extends Status(15, "准备中")
  case object Started extends Status(20, "比赛中")
  case object Finished extends Status(50, "比赛结束")

  val all = List(Created, Readying, Started, Finished)
  val byId = all map { v => (v.id, v) } toMap
  def apply(id: Int): Status = byId get id err s"Bad Status $id"
}