package lila.racer

import lila.common.ApiVersion
import lila.socket.Socket.{ SocketVersion, Sri }
import lila.socket.actorApi.{ Connected => _ }
import lila.socket.Handler
import lila.user.User

final class SocketHandler(
    hub: lila.hub.Env,
    socketMap: SocketMap,
    api: RacerApi
) {

  def makeController(
    socket: Socket,
    raceId: RacerRace.ID,
    no: RacerRound.No,
    sri: Sri,
    member: Member,
    user: Option[User]
  ): Handler.Controller = {
    case ("racerScore", o) => member.userId foreach { userId =>
      api.registerPlayerScore(raceId, no, RacerPlayer.makeId(raceId, userId), (o int "d") | 0)
    }

    case ("racerJoin", _) => member.userId foreach { userId =>
      api.registerPlayerJoin(raceId, no, userId)
    }
  }

  def socketId(raceId: RacerRace.ID, no: RacerRound.No) = RacerRound.makeId(raceId, no)
  def getSocket(raceId: RacerRace.ID, no: RacerRound.No) = socketMap.getOrMake(socketId(raceId, no))

  def join(
    raceId: RacerRace.ID,
    no: RacerRound.No,
    sri: Sri,
    user: Option[User],
    version: Option[SocketVersion],
    apiVersion: ApiVersion
  ): Fu[Option[JsSocketHandler]] = {
    val socket = getSocket(raceId, no)
    join(raceId, no, sri, user, socket, member => makeController(socket, raceId, no, sri, member, user), version, apiVersion) map some
  }

  def join(
    raceId: RacerRace.ID,
    no: RacerRound.No,
    sri: Sri,
    user: Option[User],
    socket: Socket,
    controller: Member => Handler.Controller,
    version: Option[SocketVersion],
    apiVersion: ApiVersion
  ): Fu[JsSocketHandler] =
    socket.ask[Connected](PlayerJoin(sri, user, version, _)) map {
      case Connected(enum, member) => {
        Handler.iteratee(
          hub,
          controller(member),
          member,
          socket,
          sri,
          apiVersion
        ) -> enum
      }
    }
}
