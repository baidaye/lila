package lila.racer

import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.user.User
import lila.db.dsl._
import lila.db.paginator.Adapter
import org.joda.time.DateTime

object RacerRaceRepo {

  import BSONHandlers.RacerRaceBSONHandler

  private[racer] lazy val coll = Env.current.racerColl

  def insert(racer: RacerRace): Funit = coll.insert(racer).void

  def update(racer: RacerRace): Funit = coll.update($id(racer.id), racer).void

  def remove(raceId: RacerRace.ID): Funit = coll.remove($id(raceId)).void

  def byId(id: RacerRace.ID): Fu[Option[RacerRace]] = coll.byId(id)

  def countUnFinished(me: User): Fu[Int] = coll.count(
    $doc(
      "finishedAt" $gt DateTime.now()
    ).some
  )

  def innerCreated: Fu[List[RacerRace]] = {
    val now = DateTime.now
    coll.find(
      $doc(
        "status" -> Status.Created.id,
        "typ" $in List(RacerRace.Type.TeamInner.id, RacerRace.Type.ClazzInner.id),
        "startedAt" -> ($gte(now) ++ $lte(now.plusMinutes(3)))
      )
    ).list[RacerRace]()
  }

  def setStatus(id: RacerRace.ID, status: Status): Funit =
    coll.update($id(id), $set("status" -> status.id)).void

  def list(me: User, clazzs: List[String]): Fu[List[RacerRace]] = {
    coll.find(
      $doc("status" $in List(Status.Created.id, Status.Readying.id, Status.Started.id), "finishedAt" $gt DateTime.now) ++ organizerSelector(me, clazzs)
    )
      .sort($doc("startedAt" -> 1))
      .list()
  }

  private def organizerSelector(me: User, clazzs: List[String]) = {
    me.belongTeamId.fold {
      $doc("typ" -> RacerRace.Type.ClazzInner.id, "organizer" $in clazzs)
    } { teamId =>
      $or(
        $doc("typ" -> RacerRace.Type.TeamInner.id, "organizer" -> teamId),
        $doc("typ" -> RacerRace.Type.ClazzInner.id, "organizer" $in clazzs)
      )
    }
  }

  def historyPage(me: User, page: Int): Fu[Paginator[RacerRace.WithRounds]] = {
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = $doc("createdBy" -> me.id, $or($doc("status" -> Status.Finished.id), $doc("finishedAt" $lt DateTime.now))),
        projection = $empty,
        sort = $doc("finishedAt" -> -1)
      ) mapFutureList withRounds,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def withRounds(races: Seq[RacerRace]): Fu[Seq[RacerRace.WithRounds]] = {
    RacerRoundRepo.getByRaces(races.map(_.id).toList).map { rounds =>
      races.map { race =>
        RacerRace.WithRounds(race, rounds.filter(_.raceId == race.id))
      }
    }
  }

}
