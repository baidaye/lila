package lila.memo

import scala.concurrent.duration._
import play.api.mvc.{ RequestHeader, Result, Results }
import ornicar.scalalib.Zero
import lila.common.{ CellphoneAddress, HTTPRequest, IpAddress }

object SmsRateLimit {

  private lazy val rateLimitGlobal = new lila.memo.RateLimit[String](
    credits = 1000,
    duration = 1 day,
    name = "Confirm cellphone global",
    key = "cellphone.confirm.global"
  )

  private lazy val rateLimitPerIP = new RateLimit[IpAddress](
    credits = 5,
    duration = 1 hour,
    name = "Confirm cellphone per IP",
    key = "cellphone.confirm.ip"
  )

  private lazy val rateLimitPerCellphone = new RateLimit[String](
    credits = 5,
    duration = 1 hour,
    name = "Confirm cellphone per mobile",
    key = "cellphone.confirm.cellphone"
  )

  def rateLimit(cellphone: CellphoneAddress, req: RequestHeader)(run: => Fu[Result]): Fu[Result] = {
    implicit val limitedDefault = Zero.instance[Fu[Result]](fuccess(Results.TooManyRequest("请求过于频繁，请稍后再试。")))
    rateLimitGlobal("-", cost = 1) {
      rateLimitPerCellphone(cellphone.value, cost = 1) {
        rateLimitPerIP(HTTPRequest lastRemoteAddress req, cost = 1) {
          run
        }
      }
    }
  }

}
