package lila.clazz

import lila.db.dsl._
import org.joda.time.DateTime

object InviteRepo {

  private val coll = Env.current.inviteColl

  import BSONHandlers.InviteBSONHandler

  def byId(id: String): Fu[Option[Invite]] =
    coll.byId(id)

  def find(clazzId: String, userId: String): Fu[Option[Invite]] =
    coll.uno[Invite](selectId(clazzId, userId))

  def exists(clazzId: String, userId: String): Fu[Boolean] =
    coll.exists(selectId(clazzId, userId))

  def exists(userId: String): Fu[Boolean] =
    coll.exists($doc("userId" -> userId))

  def findByUserId(userId: String): Fu[List[Invite]] =
    coll.find($doc("userId" -> userId)).list[Invite]()

  def findByClazz(clazzId: String): Fu[List[Invite]] =
    coll.find($doc("clazzId" -> clazzId)).sort($doc("createAt" -> -1)).list[Invite]()

  def add(clazzId: String, userId: String): Funit =
    coll.insert(Invite.make(clazzId = clazzId, userId = userId)).void

  def setStatus(clazzId: String, userId: String, status: Invite.Status): Funit =
    coll.update(
      selectId(clazzId, userId),
      $set("status" -> status.id)
    ).void

  def updateInviteTime(clazzId: String, userId: String): Funit =
    coll.update(
      selectId(clazzId, userId),
      $set("createAt" -> DateTime.now)
    ).void

  def remove(id: String): Funit =
    coll.remove($id(id)).void

  def selectId(clazzId: String, userId: String) =
    $id(Invite.makeId(clazzId, userId))

}
