package lila.clazz

import lila.db.dsl._
import reactivemongo.bson._
import org.joda.time.DateTime
import lila.user.User

object HomeworkV2Repo {

  import HomeworkV2BSONHandlers._

  private[clazz] lazy val coll = Env.current.homeworkV2Coll

  def byId(id: String): Fu[Option[HomeworkV2]] = coll.byId[HomeworkV2](id)

  def find(clazzId: Clazz.ID, courseId: Course.ID, index: Int): Fu[Option[HomeworkV2]] =
    byId(HomeworkV2.makeId(clazzId, courseId, index))

  def findByClazz(clazzId: Clazz.ID): Fu[List[HomeworkV2]] =
    coll.find($doc("clazzId" -> clazzId)).list[HomeworkV2]()

  def insert(homework: HomeworkV2): Funit =
    coll.insert(homework).void

  def update(homework: HomeworkV2): Funit =
    coll.update($id(homework.id), homework).void

  def setStatus(homework: HomeworkV2, status: HomeworkV2.Status): Funit =
    coll.update(
      $id(homework.id),
      $set("status" -> status.id)
    ).void

  def setDeadline(homework: HomeworkV2, deadline: DateTime): Funit =
    coll.update(
      $id(homework.id),
      $set("deadlineAt" -> deadline)
    ).void

  def setStartAt(homework: HomeworkV2, startAt: DateTime): Funit =
    coll.update(
      $id(homework.id),
      $set("startAt" -> startAt)
    ).void

  def deadlines: Fu[List[HomeworkV2]] =
    coll.find($doc("deadlineAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusMinutes 10)))).list[HomeworkV2]()

  def overDeadlines: Fu[List[HomeworkV2]] =
    coll.find($doc("overDeadline" -> false, "deadlineAt" -> ($lte(DateTime.now) ++ $gte(DateTime.now minusHours 12)))).list[HomeworkV2]()

  def setOverDeadline(homeworkIds: List[HomeworkV2.ID]): Funit =
    coll.update(
      $inIds(homeworkIds),
      $set("overDeadline" -> true),
      multi = true
    ).void

}
