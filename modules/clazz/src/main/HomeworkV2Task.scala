package lila.clazz

import lila.task.TTaskTemplate
import lila.task.TTask
import lila.task.TTask._

// 任务类型->任务模板ID
case class HomeworkV2TaskTpl(taskItem: TTaskItemType, taskTplId: TTaskTemplate.ID)
case class HomeworkV2TaskTpls(taskTpls: List[HomeworkV2TaskTpl])

// 任务类型->任务模板
case class HomeworkV2RealTaskTpl(taskItem: TTaskItemType, taskTpl: TTaskTemplate)
case class HomeworkV2RealTaskTpls(taskTpls: List[HomeworkV2RealTaskTpl])

object HomeworkV2RealTaskTpls {

  def toHomeworkV2TaskTpls(homeworkV2TaskTpls: HomeworkV2TaskTpls, realTaskTpls: List[TTaskTemplate]) =
    HomeworkV2RealTaskTpls(
      homeworkV2TaskTpls.taskTpls.map { taskTpl =>
        val realTaskTpl = realTaskTpls.find(t => t.id == taskTpl.taskTplId) err s"can not find real taskTpl ${taskTpl.taskTplId}"
        HomeworkV2RealTaskTpl(taskTpl.taskItem, realTaskTpl)
      }
    )

}

// 任务类型->任务ID
case class HomeworkV2Task(taskItem: TTaskItemType, taskId: TTask.ID)
case class HomeworkV2Tasks(tasks: List[HomeworkV2Task])

// 任务类型->任务
case class HomeworkV2RealTask(taskItem: TTaskItemType, task: TTask)
case class HomeworkV2RealTasks(tasks: List[HomeworkV2RealTask])
object HomeworkV2RealTasks {

  def toHomeworkV2Tasks(homeworkV2Tasks: HomeworkV2Tasks, realTasks: List[TTask]) =
    HomeworkV2RealTasks(
      homeworkV2Tasks.tasks.map { task =>
        val realTask = realTasks.find(t => t.id == task.taskId) err s"can not find real task ${task.taskId}"
        HomeworkV2RealTask(task.taskItem, realTask)
      }
    )

}
