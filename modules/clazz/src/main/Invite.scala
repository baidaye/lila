package lila.clazz

import org.joda.time.DateTime
import lila.user.User

case class Invite(
    _id: String,
    clazzId: String,
    userId: String,
    status: Invite.Status,
    createAt: DateTime
) {

  def id = _id

  val expireDay: Int = 3

  def expired = createAt.plusDays(expireDay).isBeforeNow

  def isInvited = status == Invite.Status.Invited

  def isJoined = status == Invite.Status.Joined

  def statusLabel = if (isInvited && expired) "已过期" else status.name

}

object Invite {

  def makeId(clazzId: String, userId: String) = userId + "@" + clazzId

  def make(clazzId: String, userId: String): Invite = new Invite(
    _id = makeId(clazzId, userId),
    clazzId = clazzId,
    userId = userId,
    status = Status.Invited,
    createAt = DateTime.now
  )

  sealed abstract class Status(val id: String, val name: String, val sort: Int)
  object Status {
    case object Invited extends Status("invited", "待同意", 1)
    case object Joined extends Status("joined", "已加入", 2)
    case object Refused extends Status("refused", "已拒绝", 4)

    val all = List(Invited, Joined, Refused)

    def byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Status = byId get id err s"can not apply Status $id"
  }
}

case class InviteWithUser(invite: Invite, user: User, mark: Option[String]) {
  def id = invite.id
  def userId = user.id
  def createAt = invite.createAt
  def clazzId = invite.clazzId
  def realName = mark | user.username
  def levelLabel = user.profileOrDefault.currentLevel.label
}

