package lila.clazz

import lila.user.User
import lila.common.LightUser
import play.api.libs.json._

final class JsonView(taskJsonView: lila.task.JsonView, lightUserApi: lila.user.LightUserApi, isOnline: User.ID => Boolean) {

  def renderLightUser(lightUser: LightUser): JsObject = {
    LightUser
      .lightUserWrites.writes(lightUser)
      .add("online" -> isOnline(lightUser.id))
  }

  def clazzJson(clazz: Clazz) =
    lightUserApi.async(clazz.coach) map { userOption =>
      val u = userOption | lila.common.LightUser.fallback(clazz.coach)
      Json.obj(
        "id" -> clazz.id,
        "name" -> clazz.name,
        "coach" -> renderLightUser(u),
        "stopped" -> clazz.stopped,
        "deleted" -> clazz.deleted
      )
    }

  def courseJson(course: Course) =
    Json.obj(
      "id" -> course.id,
      "index" -> course.index,
      "clazz" -> course.clazz,
      "date" -> course.date.toString("yyyy-MM-dd"),
      "timeBegin" -> course.timeBegin,
      "timeEnd" -> course.timeEnd,
      "dateTimeStart" -> course.dateTimeStr,
      "week" -> course.weekFormat
    )

  def homeworkJson(homework: HomeworkV2) =
    Json.obj(
      "id" -> homework.id,
      "name" -> homework.name,
      "clazzId" -> homework.clazzId,
      "courseId" -> homework.courseId,
      "index" -> homework.index,
      "startAt" -> homework.startAt.map(_.toString("yyyy-MM-dd HH:mm")),
      "deadlineAt" -> homework.deadlineAt.map(_.toString("yyyy-MM-dd HH:mm")),
      "isDeadline" -> homework.isDeadline,
      "summary" -> homework.summary,
      "prepare" -> homework.prepare,
      "status" -> Json.obj(
        "id" -> homework.status.id,
        "name" -> homework.status.name
      ),
      "coinRule" -> (homework.coinRule | 0)
    )

  def homeworkWithTaskJson(h: HomeworkV2.WithTask) =
    homeworkJson(h.homework) ++ Json.obj(
      "common" -> h.common.fold(JsArray()) { homeworkV2RealTaskTpls =>
        JsArray(
          homeworkV2RealTaskTpls.taskTpls.map { realTaskTpl =>
            taskJsonView.taskTemplateInfo(realTaskTpl.taskTpl)
          }
        )
      },
      "practice" -> h.practice.fold(JsArray()) { homeworkV2RealTaskTpls =>
        JsArray(
          homeworkV2RealTaskTpls.taskTpls.map { realTaskTpl =>
            taskJsonView.taskTemplateInfo(realTaskTpl.taskTpl)
          }
        )
      }
    )

}

