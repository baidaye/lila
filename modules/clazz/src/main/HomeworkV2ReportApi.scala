package lila.clazz

import lila.db.dsl._
import lila.task.TTask.TTaskItemType
import lila.task.{ MiniPuzzle, PuzzleCapsuleWithResult, TTaskTemplate }
import org.joda.time.DateTime
import lila.user.User

final class HomeworkV2ReportApi(api: HomeworkV2Api, coll: Coll) {

  import HomeworkV2BSONHandlers.HomeworkReportHandler

  def byId(id: HomeworkV2.ID): Fu[Option[HomeworkV2Report]] = coll.byId[HomeworkV2Report](id)

  def byHomeworkView(hwt: HomeworkV2.WithTask): Fu[HomeworkV2ReportView] =
    byId(hwt.id).flatMap {
      case None => refreshReport(hwt).map { homeworkReport =>
        toView(hwt, homeworkReport)
      }
      case Some(homeworkReport) => fuccess(toView(hwt, homeworkReport))
    }

  def byIdView(id: HomeworkV2.ID): Fu[Option[HomeworkV2ReportView]] = {
    for {
      homeworkReportOption <- byId(id)
      hwtOption <- homeworkReportOption.??(hr => api.byId(id))
    } yield (homeworkReportOption |@| hwtOption).tupled map {
      case (homeworkReport, hwt) => {
        toView(hwt, homeworkReport)
      }
    }
  }

  def toView(hwt: HomeworkV2.WithTask, homeworkReport: HomeworkV2Report): HomeworkV2ReportView = {
    val taskTplMap = hwt.allTaskTpls.map(tpl => tpl.taskTpl.id -> tpl.taskTpl).toMap
    def findTaskTpl(taskTplId: TTaskTemplate.ID) = taskTplMap.get(taskTplId) err s"can not find TaskTemplate $taskTplId"

    HomeworkV2ReportView(
      _id = homeworkReport.id,
      students = homeworkReport.students,
      completeRate = homeworkReport.completeRate,
      coinDiff = homeworkReport.coinDiff,
      common = homeworkReport.common.mapValues { commonWithReports =>
        commonWithReports.map { commonWithReport =>
          CommonWithReportView(findTaskTpl(commonWithReport.taskTplId), commonWithReport.report)
        }
      },
      practice = HomeworkPracticeReportView(
        capsulePuzzles = homeworkReport.practice.capsulePuzzles.map { capsulePuzzleWithReport =>
          val taskTpl = findTaskTpl(capsulePuzzleWithReport.taskTplId)
          val puzzles = taskTpl.item.capsulePuzzle.?? { capsulePuzzle =>
            capsulePuzzle.puzzles
          }
          def findPuzzle(puzzleId: Int) = puzzles.find(_.puzzle.id == puzzleId) err s"can not find puzzle of $puzzleId"

          CapsulePuzzleWithReportView(
            taskTpl = findTaskTpl(capsulePuzzleWithReport.taskTplId),
            report = capsulePuzzleWithReport.report.map { pwr =>
              PuzzleWithReportView(findPuzzle(pwr.puzzleId).puzzle, pwr.report)
            },
            firstRightRate = capsulePuzzleWithReport.firstRightRate
          )
        },
        replayGames = homeworkReport.practice.replayGames.map { replayGameWithReport =>
          val taskTpl = findTaskTpl(replayGameWithReport.taskTplId)
          val replayGames = taskTpl.item.replayGame.??(_.replayGames.map(_.replayGame))
          def findReplayGame(unique: String) = replayGames.find(_.unique == unique) err s"can not find replayGame of $unique"

          ReplayGameWithReportView(
            taskTpl = taskTpl,
            report = replayGameWithReport.report.map { report =>
              ReplayGameReportView(
                replayGame = findReplayGame(report.unique),
                complete = report.complete,
                students = report.students
              )
            }
          )
        },
        recallGames = homeworkReport.practice.recallGames.map { recallGameWithReport =>
          val taskTpl = findTaskTpl(recallGameWithReport.taskTplId)
          val recallGames = taskTpl.item.recallGame.??(_.recallGames.map(_.recallGame))
          def findRecallGame(unique: String) = recallGames.find(_.unique == unique) err s"can not find recallGame of $unique"

          RecallGameWithReportView(
            taskTpl = taskTpl,
            report = recallGameWithReport.report.map { report =>
              RecallGameReportView(
                recallGame = findRecallGame(report.unique),
                turnsDistribute = report.turnsDistribute
              )
            }
          )
        },
        distinguishGames = homeworkReport.practice.distinguishGames.map { distinguishGameWithReport =>
          val taskTpl = findTaskTpl(distinguishGameWithReport.taskTplId)
          val distinguishGames = taskTpl.item.distinguishGame.??(_.distinguishGames.map(_.distinguishGame))
          def findDistinguishGame(unique: String) = distinguishGames.find(_.unique == unique) err s"can not find distinguishGame of $unique"

          DistinguishGameWithReportView(
            taskTpl = taskTpl,
            report = distinguishGameWithReport.report.map { report =>
              DistinguishGameReportView(
                distinguishGame = findDistinguishGame(report.unique),
                turnsDistribute = report.turnsDistribute
              )
            }
          )
        },
        fromPositions = homeworkReport.practice.fromPositions.map { fromPositionWithReport =>
          val taskTpl = findTaskTpl(fromPositionWithReport.taskTplId)
          val fromPositions = taskTpl.item.fromPosition.??(_.fromPositions.map(_.fromPosition))
          def findFromPosition(unique: String) = fromPositions.find(_.unique == unique) err s"can not find fromPosition of $unique"

          FromPositionWithReportView(
            taskTpl = taskTpl,
            report = fromPositionWithReport.report.map { report =>
              FromPositionReportView(
                fromPosition = findFromPosition(report.unique),
                roundDistribute = report.roundDistribute
              )
            }
          )
        },
        fromPgns = homeworkReport.practice.fromPgns.map { fromPgnWithReport =>
          val taskTpl = findTaskTpl(fromPgnWithReport.taskTplId)
          val fromPgns = taskTpl.item.fromPgn.??(_.fromPgns.map(_.fromPgn))
          def findFromPgn(unique: String) = fromPgns.find(_.unique == unique) err s"can not find fromPgn of $unique"

          FromPgnWithReportView(
            taskTpl = taskTpl,
            report = fromPgnWithReport.report.map { report =>
              FromPgnReportView(
                fromPgn = findFromPgn(report.unique),
                roundDistribute = report.roundDistribute
              )
            }
          )
        },
        fromOpeningdbs = homeworkReport.practice.fromOpeningdbs.map { fromOpeningdbWithReport =>
          val taskTpl = findTaskTpl(fromOpeningdbWithReport.taskTplId)
          val fromOpeningdbs = taskTpl.item.fromOpeningdb.??(_.fromOpeningdbs.map(_.fromOpeningdb))
          def findFromOpeningdb(unique: String) = fromOpeningdbs.find(_.unique == unique) err s"can not find fromOpeningdb of $unique"

          FromOpeningdbWithReportView(
            taskTpl = taskTpl,
            report = fromOpeningdbWithReport.report.map { report =>
              FromOpeningdbReportView(
                fromOpeningdb = findFromOpeningdb(report.unique),
                roundDistribute = report.roundDistribute
              )
            }
          )
        }
      ),
      updateAt = homeworkReport.updateAt,
      createAt = homeworkReport.createAt,
      createBy = homeworkReport.createBy
    )
  }

  def schedulerRefresh: Funit =
    HomeworkV2Repo.deadlines flatMap { homeworks =>
      homeworks.nonEmpty.?? {
        api.findWithTask(homeworks) map { hwts =>
          hwts.foreach { hwt =>
            refreshReport(hwt).void
          }
        }
      }
    }

  def refreshReport(hwt: HomeworkV2.WithTask): Fu[HomeworkV2Report] = {
    val homework = hwt.homework
    api.findStudentsWithTaskForCourse(homework.clazzId, homework.courseId) flatMap { shwts =>
      val report = HomeworkV2Report(
        _id = hwt.id,
        students = shwts.map(_.studentId),
        completeRate = calcCompleteRate(shwts).some,
        coinDiff = calcCoinDiff(shwts).some,
        common = calcCommon(shwts),
        practice = HomeworkPracticeReport(
          capsulePuzzles = capsulePuzzles(hwt, shwts),
          replayGames = replayGames(hwt, shwts),
          recallGames = recallGames(hwt, shwts),
          distinguishGames = distinguishGames(hwt, shwts),
          fromPositions = fromPositions(hwt, shwts),
          fromPgns = fromPgns(hwt, shwts),
          fromOpeningdbs = fromOpeningdbs(hwt, shwts)
        ),
        updateAt = DateTime.now,
        createAt = DateTime.now,
        createBy = homework.createdBy
      )

      coll.update($id(hwt.id), report, upsert = true).inject(report)
    }
  }

  private def calcCompleteRate(shwts: List[HomeworkV2Student.WithTask]): Map[User.ID, Double] = {
    shwts.map { shwt =>
      shwt.studentId -> shwt.progress
    }.toMap
  }

  private def calcCoinDiff(shwts: List[HomeworkV2Student.WithTask]): Map[User.ID, Int] = {
    shwts.map { shwt =>
      shwt.studentId -> shwt.homework.coinDiffOrZero
    }.toMap
  }

  private def calcCommon(shwts: List[HomeworkV2Student.WithTask]): Map[User.ID, List[CommonWithReport]] = {
    shwts.map { shwt =>
      shwt.studentId -> shwt.common.?? { common =>
        common.tasks.map { homeworkV2RealTask =>
          val task = homeworkV2RealTask.task
          CommonWithReport(task.tplId.get, CommonReport(task.total, task.num, task.isComplete))
        }
      }
    }.toMap
  }

  // 指定战术题
  private def capsulePuzzles(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[CapsulePuzzleWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.CapsulePuzzleItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.capsulePuzzle.map { capsulePuzzle =>
        val shwtsForCapsulePuzzles = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.capsulePuzzle
              case None => None
            }
          )
        }

        // 一个任务可能对应多个战术题列表
        CapsulePuzzleWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = capsulePuzzle.puzzles.map { puzzleWithReport =>
            val puzzle = puzzleWithReport.puzzle
            val completeRate = calcPuzzleTryRate(shwtsForCapsulePuzzles, puzzle)
            val rightRate = calcPuzzleRightRate(shwtsForCapsulePuzzles, puzzle)
            val firstMoveRightRate = calcPuzzleFirstMoveRightRate(shwtsForCapsulePuzzles, puzzle)
            val rightMoveDistribute = calcRightMoveDistribute(shwtsForCapsulePuzzles, puzzle)
            val wrongMoveDistribute = calcWrongMoveDistribute(shwtsForCapsulePuzzles, puzzle)
            PuzzleWithReport(
              puzzleId = puzzle.id,
              report = PuzzleReport(
                completeRate = completeRate,
                rightRate = rightRate,
                firstMoveRightRate = firstMoveRightRate,
                rightMoveDistribute = rightMoveDistribute,
                wrongMoveDistribute = wrongMoveDistribute
              )
            )
          },
          firstRightRate = Some(
            calcPuzzleFirstRightRate(shwtsForCapsulePuzzles)
          )
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 所有题目首次正确率
  private def calcPuzzleFirstRightRate(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])]) = {
    shwtsForCapsulePuzzles.filter(_._2.??(_.puzzles.exists(_.isTry))).map {
      case (studentId, capsulePuzzleWithResultOption) => {
        capsulePuzzleWithResultOption.fold(UserRate(studentId, 0.0)) { capsulePuzzleWithResult =>
          val firstRightCount = capsulePuzzleWithResult.puzzles.count(_.isFirstComplete)
          val tryCount = capsulePuzzleWithResult.puzzles.count(_.isTry)
          val rate = round2(firstRightCount, tryCount)
          UserRate(studentId, rate)
        }
      }
    }
  }

  // 完成率
  private def calcPuzzleTryRate(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])], puzzle: MiniPuzzle): RateNum = {
    val tryStudents = shwtsForCapsulePuzzles.filter(_._2.??(_.findPuzzle(puzzle).isTry)).map(_._1)
    val tryCount = tryStudents.size
    val rate = round2(tryCount, shwtsForCapsulePuzzles.size)
    RateNum(rate, tryStudents)
  }

  // 正确率
  private def calcPuzzleRightRate(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])], puzzle: MiniPuzzle): RateNum = {
    val tryCount = shwtsForCapsulePuzzles.count(_._2.??(_.findPuzzle(puzzle).isTry))
    val rightStudents = shwtsForCapsulePuzzles.filter(_._2.??(_.findPuzzle(puzzle).isComplete)).map(_._1)
    val rightCount = rightStudents.size
    if (tryCount == 0) {
      RateNum(0.0D, Nil)
    } else {
      val rate = round2(rightCount, tryCount)
      RateNum(rate, rightStudents)
    }
  }

  // 单题首次正确率
  private def calcPuzzleFirstMoveRightRate(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])], puzzle: MiniPuzzle): RateNum = {
    val tryCount = shwtsForCapsulePuzzles.count(_._2.??(_.findPuzzle(puzzle).isTry))
    val firstRightStudents = shwtsForCapsulePuzzles.filter(_._2.??(_.findPuzzle(puzzle).isFirstComplete)).map(_._1)
    val firstRightCount = firstRightStudents.size
    if (tryCount == 0) {
      RateNum(0.0D, Nil)
    } else {
      val rate = round2(firstRightCount, tryCount)
      RateNum(rate, firstRightStudents)
    }
  }

  // 第一步走法 正确UCI分布
  private def calcRightMoveDistribute(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])], puzzle: MiniPuzzle): List[MoveNum] = {
    shwtsForCapsulePuzzles.map {
      case (studentId, puzzleCapsuleOption) => puzzleCapsuleOption match {
        case Some(puzzleCapsule) => puzzleCapsule.findPuzzle(puzzle).firstRightMove -> studentId
        case None => None -> studentId
      }
    }.filter(_._1.isDefined)
      .groupBy(_._1)
      .map {
        case (m, list) => MoveNum(m.get, list.size, list.map(_._2).some)
      }.toList
  }

  // 第一步走法 错误UCI分布
  private def calcWrongMoveDistribute(shwtsForCapsulePuzzles: List[(User.ID, Option[PuzzleCapsuleWithResult])], puzzle: MiniPuzzle): List[MoveNum] = {
    shwtsForCapsulePuzzles.map {
      case (studentId, puzzleCapsuleOption) => puzzleCapsuleOption match {
        case Some(puzzleCapsule) => puzzleCapsule.findPuzzle(puzzle).firstWrongMove -> studentId
        case None => None -> studentId
      }
    }.filter(_._1.isDefined)
      .groupBy(_._1)
      .map {
        case (m, list) => MoveNum(m.get, list.size, list.map(_._2).some)
      }.toList
  }

  // 打谱
  private def replayGames(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[ReplayGameWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.ReplayGameItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.replayGame.map { replayGameItem =>
        val shwtsForReplayGame = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.replayGame
              case None => None
            }
          )
        }

        // 一个任务可能对应多个打谱
        ReplayGameWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = replayGameItem.replayGames.map { replayGameWithResult =>
            val replayGame = replayGameWithResult.replayGame
            val completeStudents = shwtsForReplayGame.filter(_._2.exists(_.findReplayGame(replayGame).isComplete()))
            ReplayGameReport(replayGame.unique, completeStudents.size, completeStudents.map(_._1).some)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 记谱
  private def recallGames(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[RecallGameWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.RecallGameItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.recallGame.map { recallGameItem =>
        val shwtsForRecallGame = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.recallGame
              case None => None
            }
          )
        }

        // 一个任务可能对应多个记谱
        RecallGameWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = recallGameItem.recallGames.map { recallGameWithResult =>
            val recallGame = recallGameWithResult.recallGame
            val studentTurnsMap = shwtsForRecallGame.map {
              case (studentId, recallGameWithReportOption) => recallGameWithReportOption.??(_.findRecallGame(recallGame).turns) -> studentId
            }

            val turnsDistribute =
              studentTurnsMap
                .groupBy(_._1)
                .map {
                  case (m, list) => RecallTurnsDistribute(m, list.size, list.map(_._2).some)
                }.toList.sortBy(_.turns)

            RecallGameReport(recallGame.unique, turnsDistribute)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 棋谱记录
  private def distinguishGames(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[DistinguishGameWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.DistinguishGameItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.distinguishGame.map { distinguishGameItem =>
        val shwtsForDistinguishGame = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.distinguishGame
              case None => None
            }
          )
        }

        // 一个任务可能对应多个棋谱记录
        DistinguishGameWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = distinguishGameItem.distinguishGames.map { distinguishGameWithResult =>
            val distinguishGame = distinguishGameWithResult.distinguishGame
            val studentTurnsMap = shwtsForDistinguishGame.map {
              case (studentId, distinguishGameWithReportOption) => distinguishGameWithReportOption.??(_.findDistinguishGame(distinguishGame).turns) -> studentId
            }

            val turnsDistribute =
              studentTurnsMap
                .groupBy(_._1)
                .map {
                  case (m, list) => DistinguishTurnsDistribute(m, list.size, list.map(_._2).some)
                }.toList.sortBy(_.turns)

            DistinguishGameReport(distinguishGame.unique, turnsDistribute)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 指定起始位置
  private def fromPositions(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[FromPositionWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.FromPositionItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.fromPosition.map { fromPositionItem =>
        val shwtsForFromPosition = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.fromPosition
              case None => None
            }
          )
        }

        // 一个任务可能对应多个
        FromPositionWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = fromPositionItem.fromPositions.map { fromPositionWithResult =>
            val fromPosition = fromPositionWithResult.fromPosition
            val studentRoundMap = shwtsForFromPosition.map {
              case (studentId, fromPositionWithReportOption) => fromPositionWithReportOption.??(_.findFromPosition(fromPosition).completeSize()) -> studentId
            }

            val roundsDistribute =
              studentRoundMap
                .groupBy(_._1)
                .map {
                  case (m, list) => FromPositionRoundDistribute(m, list.size, list.map(_._2).some)
                }.toList.sortBy(_.rounds)

            FromPositionReport(fromPosition.unique, roundsDistribute)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 指定起始PGN
  private def fromPgns(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[FromPgnWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.FromPgnItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.fromPgn.map { fromPgnItem =>
        val shwtsForFromPgn = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.fromPgn
              case None => None
            }
          )
        }

        // 一个任务可能对应多个
        FromPgnWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = fromPgnItem.fromPgns.map { fromPgnWithResult =>
            val fromPgn = fromPgnWithResult.fromPgn
            val studentRoundMap = shwtsForFromPgn.map {
              case (studentId, fromPgnWithReportOption) => fromPgnWithReportOption.??(_.findFromPgn(fromPgn).completeSize()) -> studentId
            }

            val roundsDistribute =
              studentRoundMap
                .groupBy(_._1)
                .map {
                  case (m, list) => FromPgnRoundDistribute(m, list.size, list.map(_._2).some)
                }.toList.sortBy(_.rounds)

            FromPgnReport(fromPgn.unique, roundsDistribute)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  // 指定起始开局库
  private def fromOpeningdbs(hwt: HomeworkV2.WithTask, shwts: List[HomeworkV2Student.WithTask]): List[FromOpeningdbWithReport] = {
    hwt.practiceOrEmpty.filter(_.taskItem == TTaskItemType.FromOpeningdbItem) map { homeworkV2RealTaskTpl =>
      homeworkV2RealTaskTpl.taskTpl.item.fromOpeningdb.map { fromOpeningdbItem =>
        val shwtsForFromOpeningdb = shwts.map { shwt =>
          shwt.studentId -> (
            shwt.practiceOrEmpty.find(_.task.tplId.has(homeworkV2RealTaskTpl.taskTpl.id)) match {
              case Some(homeworkV2RealTask) => homeworkV2RealTask.task.item.fromOpeningdb
              case None => None
            }
          )
        }

        // 一个任务可能对应多个
        FromOpeningdbWithReport(
          taskTplId = homeworkV2RealTaskTpl.taskTpl.id,
          report = fromOpeningdbItem.fromOpeningdbs.map { fromOpeningdbWithResult =>
            val fromOpeningdb = fromOpeningdbWithResult.fromOpeningdb
            val studentRoundMap = shwtsForFromOpeningdb.map {
              case (studentId, fromOpeningdbWithReportOption) => fromOpeningdbWithReportOption.??(_.findFromOpeningdb(fromOpeningdb).completeSize()) -> studentId
            }

            val roundsDistribute =
              studentRoundMap
                .groupBy(_._1)
                .map {
                  case (m, list) => FromOpeningdbRoundDistribute(m, list.size, list.map(_._2).some)
                }.toList.sortBy(_.rounds)

            FromOpeningdbReport(fromOpeningdb.unique, roundsDistribute)
          }
        )
      }
    }
  }.filter(_.isDefined).map(_.get)

  private def round2(divided: Int, divisor: Int) =
    if (divisor == 0) 0.0
    else Math.round(divided * 1000 / divisor) / 10.0

}
