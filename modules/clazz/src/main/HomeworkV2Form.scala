package lila.clazz

import play.api.data._
import play.api.data.Forms._
import lila.common.Form._
import lila.task.TTask.TTaskItemType
import org.joda.time.DateTime
import lila.user.User

class HomeworkFormV2 {

  def create = Form(mapping(
    "deadlineAt" -> optional(futureDateTime),
    "summary" -> optional(nonEmptyText),
    "prepare" -> optional(nonEmptyText),
    "common" -> optional(list(mapping(
      "taskItem" -> stringIn(TTaskItemType.selects),
      "taskTplId" -> nonEmptyText
    )(HomeworkV2TaskTplData.apply)(HomeworkV2TaskTplData.unapply))),
    "practice" -> optional(list(mapping(
      "taskItem" -> stringIn(TTaskItemType.selects),
      "taskTplId" -> nonEmptyText
    )(HomeworkV2TaskTplData.apply)(HomeworkV2TaskTplData.unapply))),
    "coinRule" -> optional(number(min = 0, max = 10000))
  )(HomeworkV2Data.apply)(HomeworkV2Data.unapply).verifying("课后练不能为空", _.hasContent))

  def deadlineForm = Form(single("deadlineAt" -> lila.common.Form.futureDateTime))

  def coinDiffForm(max: Int) = Form(single("coinDiff" -> number(0, max)))

}

case class HomeworkV2Data(
    deadlineAt: Option[DateTime],
    summary: Option[String],
    prepare: Option[String],
    common: Option[List[HomeworkV2TaskTplData]],
    practice: Option[List[HomeworkV2TaskTplData]],
    coinRule: Option[Int]
) {

  def hasContent = summary.nonEmpty || prepare.nonEmpty || common.??(_.nonEmpty) || practice.??(_.nonEmpty)

  def toHomework(clazz: Clazz, course: Course, creator: User) =
    HomeworkV2.make(
      clazzId = course.clazz,
      courseId = course.id,
      index = course.index,
      startAt = course.dateEndTime.some,
      deadlineAt = deadlineAt,
      summary = summary,
      prepare = prepare,
      common = toTaskTpls(common),
      practice = toTaskTpls(practice),
      name = s"${clazz.name} 第${course.index}次课 课后练".some,
      coinTeam = coinRule.map(_ => creator.belongTeamIdValue),
      coinRule = coinRule,
      userId = creator.id
    )

  def toTaskTpls(taskTplsOpt: Option[List[HomeworkV2TaskTplData]]) =
    taskTplsOpt.map { taskTpls =>
      HomeworkV2TaskTpls(
        taskTpls.map { taskTpl =>
          HomeworkV2TaskTpl(taskTpl.realTaskItem, taskTpl.taskTplId)
        }
      )
    }

}

case class HomeworkV2TaskTplData(taskItem: String, taskTplId: String) {

  def realTaskItem = TTaskItemType(taskItem)
}
