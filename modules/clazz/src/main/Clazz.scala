package lila.clazz

import lila.user.User
import lila.team.{ CampusWithTeam, Team }
import org.joda.time.DateTime
import ornicar.scalalib.Random
import Clazz._

case class Clazz(
    _id: ID,
    name: String,
    color: String,
    coach: CoachID,
    team: Option[String],
    clazzType: ClazzType,
    weekClazz: Option[WeekClazz],
    trainClazz: Option[TrainClazz],
    studentIds: List[User.ID],
    showAttend: Option[Boolean],
    contestIds: Option[List[String]],
    stopped: Boolean,
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID,
    deleted: Option[Boolean] = None
) {

  def id = _id
  def belongTeam = team.nonEmpty

  def belongTo(userId: String) = isCoach(userId) || isStudent(userId)
  def isStudent(userId: String) = studentIds.contains(userId)
  def isStudent(user: Option[User]) = user.??(u => studentIds.contains(u.id))
  def isCoach(userId: String) = userId == coach
  def isCoach(user: Option[User]) = user.??(_.id == coach)

  def studentCount = studentIds.size

  def studentEmpty = studentIds.isEmpty

  def deletable = studentEmpty && !isDelete

  def isDelete = deleted | false

  def isTimeout = endDate.withTime(23, 59, 59, 999).isBefore(DateTime.now)

  def endDate = clazzType match {
    case ClazzType.Week => weekClazz.map(_.dateEnd) err "empty weekClazz"
    case ClazzType.Train => trainClazz.map(_.dateEnd) err "empty trainClazz"
  }

  def editable = clazzType match {
    case ClazzType.Week => weekClazz.??(_.dateTimeBegin.isAfterNow && !stopped && studentEmpty)
    case ClazzType.Train => trainClazz.??(_.dateTimeBegin.isAfterNow && !stopped && studentEmpty)
  }

  def isShowAttend = showAttend | false

  def teamOrDefault = team | "-"

  def contests = contestIds | Nil

  def toCourse(id: ID, coach: CoachID): List[Course] = {
    clazzType match {
      case ClazzType.Week => weekClazz.map(_.toCourseFromWeek(id, coach))
      case ClazzType.Train => trainClazz.map(_.toCourseFromTrain(id, coach))
    }
  } err s"can not convert to course of clazz ${name}"

  def times: Int = {
    clazzType match {
      case ClazzType.Week => weekClazz.map(_.times)
      case ClazzType.Train => trainClazz.map(_.times)
    }
  } err s"can not find times of clazz ${name}"

  def isWeek = clazzType match {
    case ClazzType.Week => true
    case ClazzType.Train => false
  }

}

object Clazz {

  type ID = String
  type CoachID = User.ID

  val MAX_COURSE = 300

  sealed abstract class ClazzType(val id: String, val name: String)
  object ClazzType {
    case object Week extends ClazzType("week", "周定时")
    case object Train extends ClazzType("train", "集训")

    val all = List(Week, Train)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): ClazzType = byId get id err s"Bad ClazzType $id"
  }

  def make(
    user: User,
    name: String,
    color: String,
    team: Option[String],
    coach: String,
    clazzType: ClazzType,
    weekClazz: Option[WeekClazz],
    trainClazz: Option[TrainClazz]
  ): Clazz = new Clazz(
    _id = genId,
    name = name,
    color = color,
    coach = coach,
    team = team,
    clazzType = clazzType,
    weekClazz = weekClazz,
    trainClazz = trainClazz,
    studentIds = List.empty[User.ID],
    showAttend = None,
    contestIds = None,
    stopped = false,
    createdAt = DateTime.now,
    updatedAt = DateTime.now,
    createdBy = user.id,
    updatedBy = user.id
  )

  def genId = Random nextString 8

  case class ClazzWithCoach(clazz: Clazz, coach: User, mark: Option[String] = None, courses: List[Course] = Nil) {

    def id = clazz.id
    def coachId = coach.id

    def isStudent(user: Option[User]) = clazz.isStudent(user)
    def isCoach(user: Option[User]) = clazz.isCoach(user)

    def exists(week: Int, period: String) = findCourse(week, period).nonEmpty
    def findCourse(week: Int, period: String): List[Course] = clazz.clazzType match {
      case ClazzType.Week => courses.filter(c => c.week == week && c.period == period)
      case ClazzType.Train => {
        val course = courses.minBy(_.dateTime)
        if (course.week == week && course.period == period) List(course)
        else Nil
      }
    }

    def distinctCourse(week: Int, period: String): List[Course] = findCourse(week, period).groupBy(_.timeBegin).map {
      case (_, courseList) => recentlyCourse(courseList)
    }.toList.sortBy(_.dateTimeStr)

    def recentlyCourse(courseList: List[Course]): Course = {
      val now = DateTime.now.toString("yyyy-MM-dd HH:mm")
      val sortedList = courseList.sortBy(_.dateTimeStr)
      sortedList.findRight(c => now > c.dateTimeStr)
    } | courseList.head.copy(index = 0)
  }

  case class ClazzWithTeam(clazz: Clazz, team: Option[CampusWithTeam]) {
    val teamName = team.map(_.fullName) | "-"
  }

}
