package lila.clazz

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.api.ReadPreference
import scala.concurrent.duration._

class CourseApi(coll: Coll, clazzColl: Coll, bus: lila.common.Bus)(implicit system: akka.actor.ActorSystem) {

  import BSONHandlers.clazzHandler
  import BSONHandlers.courseHandler

  def bulkInsert(courseList: List[Course]): Funit = coll.bulkInsert(
    documents = courseList.map(courseHandler.write).toStream,
    ordered = false
  ).void

  def bulkUpdate(id: String, courseList: List[Course]): Funit =
    coll.remove($doc("clazz" -> id)) >> bulkInsert(courseList)

  def byId(id: String): Fu[Option[Course]] = coll.byId[Course](id)

  def findByCourseIndex(clazzId: String, index: Int): Fu[Option[Course]] =
    coll.find(
      $doc(
        "clazz" -> clazzId,
        "index" -> index,
        "stopped" -> false,
        "enabled" -> true
      )
    ).uno[Course]

  def clazzCourse(clazzId: Clazz.ID, withStopped: Boolean = false): Fu[List[Course]] =
    coll.find(
      $doc(
        "clazz" -> clazzId,
        "enabled" -> true
      ) ++ (!withStopped).??($doc("stopped" -> false))
    ).sort($sort asc "index").list[Course]()

  def weekCourse(firstDay: DateTime, lastDay: DateTime, clazzIds: List[String]): Fu[List[Course]] = {
    coll.find($doc(
      "clazz" $in clazzIds,
      "enabled" -> true,
      "date" -> ($gte(firstDay.withTimeAtStartOfDay()) ++ $lte(lastDay.withTime(23, 59, 59, 999)))
    )).sort($sort asc "date").list[Course]()
  }

  def courseFromSecondary(ids: Seq[String]): Fu[List[Course]] =
    coll.byOrderedIds[Course, String](
      ids,
      readPreference = ReadPreference.secondaryPreferred
    )(_.id)

  def update(c: Course, data: UpdateData): Funit =
    coll.update(
      $id(c._id),
      $set(
        "date" -> data.date,
        "timeBegin" -> data.timeBegin,
        "timeEnd" -> data.timeEnd,
        "week" -> data.date.getDayOfWeek,
        "updatedAt" -> DateTime.now()
      )
    ).void >> updatePublish(c.copy(
        date = data.date,
        timeBegin = data.timeBegin,
        timeEnd = data.timeEnd,
        week = data.date.getDayOfWeek
      )) >> byId(c.id).map {
        _.?? { newCourse =>
          bus.publish(CourseUpdate(newCourse), 'courseUpdated)
        }
      }

  def stop(course: Course): Funit =
    coll.update($id(course.id), $set("stopped" -> true)).void /* >> stopPublish(course)*/

  def stopByClazz(clazzId: String): Funit =
    coll.update(
      $doc("clazz" -> clazzId),
      $set("stopped" -> true),
      multi = true
    ).void >> removeCalendars(clazzId)

  def deleteByClazz(clazzId: String): Funit =
    coll.remove(
      $doc("clazz" -> clazzId)
    ).void

  // 只有周定时的能推迟
  def postpone(course: Course, clazz: Clazz): Fu[DateTime] = {
    beforeList(course) flatMap { courseList =>
      val sortedCourseList = courseList.sortWith { (thisCourse, thatCourse) =>
        thisCourse.dateTime.isBefore(thatCourse.dateTime)
      }

      sortedCourseList.map { course =>
        val virtualCourse = courseList.find(_.dateTime.isAfter(course.dateTime))
          .getOrElse(clazz.weekClazz.map(_.nextWeekCourse(course)) err s"cannot find next week course of ${course.toString}")
        coll.update(
          $id(course.id),
          $set(
            "date" -> virtualCourse.date,
            "timeBegin" -> virtualCourse.timeBegin,
            "timeEnd" -> virtualCourse.timeEnd,
            "updatedAt" -> DateTime.now()
          )
        ) inject virtualCourse.copy(index = course.index)
      }.sequenceFu.flatMap { virtualCourseList =>
        fuccess(postponePublish(clazz, sortedCourseList, virtualCourseList)) inject sortedCourseList.last.date
      }
    }
  }

  // 只有周定时的能追加
  def appendCourse(clazz: Clazz, data: Append, updateWeekClazz: (Clazz.ID, WeekClazz) => Funit): Funit = {
    clazz.weekClazz.?? { weekClazz =>
      val courses = weekClazz.copy(
        times = data.times,
        appends = weekClazz.appends.map(_ :+ data)
      ).toCourseFromWeek(clazz.id, clazz.coach, clazz.times + 1, data.date)

      courses.nonEmpty.?? {
        val newWeekClazz = weekClazz.copy(
          dateEnd = courses.last.date,
          times = weekClazz.times + data.times,
          appends = weekClazz.appends.fold(List(data).some) { list => (list :+ data).some }
        )

        bulkInsert(courses) >> updateWeekClazz(clazz.id, newWeekClazz) >>- {
          val coachCalendars = courses.map { course =>
            makeCalendar(clazz, course, clazz.coach)
          }

          val calendars = courses.foldLeft(List.empty[lila.hub.actorApi.calendar.CalendarCreate]) {
            case (all, course) => {
              all ++ clazz.studentIds.map { userId =>
                makeCalendar(clazz, course, userId)
              }
            }
          }

          bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(coachCalendars), 'calendarCreateBus)
          bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(calendars), 'calendarCreateBus)
        }
      }
    }
  }

  private def beforeList(course: Course): Fu[List[Course]] =
    coll.find($doc(
      "clazz" -> course.clazz,
      "index" -> $gte(course.index),
      "enabled" -> true
    )).sort($sort asc "index").list[Course]()

  def setHomework(id: String, hwk: Boolean): Funit =
    coll.update(
      $id(id),
      $set("homework" -> hwk)
    ).void

  def lessTdyCourse(clazzIds: List[String]): Fu[List[Course]] =
    coll.find($doc(
      "clazz" $in clazzIds,
      "date" $lte DateTime.now.withTimeAtStartOfDay,
      "enabled" -> true
    )).list[Course]()

  /*  def recentlyCourse(courseList: List[Course]): Option[Course] = {
    if (courseList.isEmpty) none[Course]
    else {
      val now = DateTime.now.toString("yyyy-MM-dd HH:mm")
      val sortedList = courseList.sortBy(_.dateTimeStr)
      sortedList.findRight(c => now > c.dateTimeStr)
    }
  }*/

  def recentlyCourseWithDefault(courseList: List[Course]): Option[Course] = {
    if (courseList.isEmpty) none[Course]
    else {
      val now = DateTime.now.toString("yyyy-MM-dd HH:mm")
      val sortedList = courseList.sortBy(_.dateTimeStr)
      sortedList.findRight(c => now > c.dateTimeStr).fold(sortedList.headOption)(_.some)
    }
  }

  def courseRelations(clazz: Clazz, olClassMetaDatas: List[OlClassMetaData], user: User.ID): Fu[List[CourseRelation]] = for {
    courses <- Env.current.courseApi.clazzCourse(clazz.id, withStopped = true)
    stuAttends <- Env.current.courseAttendApi.listByUser(clazz.id, user)
    homeworks <- HomeworkV2Repo.findByClazz(clazz.id)
    shwts <- Env.current.homeworkV2Api.findStudentsWithTaskForUser(clazz.id, user)
  } yield {
    courses.map { course =>
      CourseRelation(
        course,
        olClassMetaDatas.find(_.courseId == course.id),
        homeworks.find(_.courseId == course.id),
        shwts.find(_.homework.courseId == course.id),
        stuAttends.find(_.course == course.id)
      )
    }
  }

  def removeCalendars(clazzId: Clazz.ID): Funit =
    clazzColl.byId[Clazz](clazzId) flatMap {
      case None => funit
      case Some(c) => removeCalendarsByClazz(c)
    }

  def removeCalendarsByClazz(c: Clazz): Funit = {
    val clazzId = c.id
    clazzCourse(clazzId, true).map { list =>
      val afterCourses = list.filter(_.dateTime.isAfterNow)
      val coachIds = list.map { course => s"$clazzId@${course.coach}@${course.index}" }
      val ids = afterCourses.foldLeft(List.empty[String]) { (all, course) =>
        {
          all ++ c.studentIds.map { userId => s"$clazzId@$userId@${course.index}" }
        }
      }

      bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(coachIds), 'calendarRemoveBus)
      ids.grouped(1000).foreach { splitIds =>
        bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(splitIds), 'calendarRemoveBus)
      }
    }
  }

  private def updatePublish(course: Course): Funit =
    clazzColl.byId[Clazz](course.clazz) map {
      case None =>
      case Some(clazz) => {
        val coachCalendars = List(makeCalendar(clazz, course, clazz.coach))
        bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(coachCalendars.map(_.id.get)), 'calendarRemoveBus)
        system.scheduler.scheduleOnce(3 second) {
          bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(coachCalendars), 'calendarCreateBus)
        }

        val ids = clazz.studentIds.map { userId => s"${course.clazz}@$userId@${course.index}" }
        bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(ids), 'calendarRemoveBus)
        system.scheduler.scheduleOnce(3 second) {
          val calendars = clazz.studentIds.map { userId => makeCalendar(clazz, course, userId) }
          bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(calendars), 'calendarCreateBus)
        }
      }
    }

  private def stopPublish(course: Course): Funit =
    clazzColl.byId[Clazz](course.clazz) map {
      case None =>
      case Some(c) => {
        val ids = c.studentIds.map { userId => s"${course.clazz}@$userId@${course.index}" }
        bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(ids), 'calendarRemoveBus)
      }
    }

  private def postponePublish(clazz: Clazz, oldCourseList: List[Course], newCourseList: List[Course]) = {
    val coachIds = oldCourseList.map { course =>
      s"${clazz.id}@${clazz.coach}@${course.index}"
    }

    val ids = oldCourseList.foldLeft(List.empty[String]) { (all, course) =>
      {
        all ++ clazz.studentIds.map { userId => s"${clazz.id}@$userId@${course.index}" }
      }
    }
    bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(coachIds), 'calendarRemoveBus)
    ids.grouped(1000).foreach { splitIds =>
      bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(splitIds), 'calendarRemoveBus)
    }

    // -------------------------------------------------------------------------------
    val coachCalendars = newCourseList.map { course =>
      makeCalendar(clazz, course, clazz.coach)
    }
    system.scheduler.scheduleOnce(3 second) {
      bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(coachCalendars), 'calendarCreateBus)
    }

    val calendars = newCourseList.foldLeft(List.empty[lila.hub.actorApi.calendar.CalendarCreate]) { (all, course) =>
      {
        all ++ clazz.studentIds.map { userId =>
          makeCalendar(clazz, course, userId)
        }
      }
    }
    system.scheduler.scheduleOnce(3 second) {
      calendars.grouped(1000).foreach { splitCalendars =>
        bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(splitCalendars), 'calendarCreateBus)
      }
    }
  }

  def resetCoachCalendarPublish(clazz: Clazz) = {
    Env.current.courseApi.clazzCourse(clazz.id).foreach { list =>
      val calendars = list.map { course =>
        makeCalendar(clazz, course, clazz.coach)
      }

      bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(calendars.map(_.id.get)), 'calendarRemoveBus)
      system.scheduler.scheduleOnce(3 second) {
        bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(calendars), 'calendarCreateBus)
      }
    }
  }

  def removeCoachCalendarPublish(clazz: Clazz) = {
    Env.current.courseApi.clazzCourse(clazz.id).foreach { list =>
      val calendars = list.map { course =>
        makeCalendar(clazz, course, clazz.coach)
      }

      bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(calendars.map(_.id.get)), 'calendarRemoveBus)
    }
  }

  def coachCalendarPublish(clazz: Clazz) = {
    Env.current.courseApi.clazzCourse(clazz.id).foreach { list =>
      val calendars = list.map { course =>
        makeCalendar(clazz, course, clazz.coach)
      }
      bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def makeCalendar(clazz: Clazz, course: Course, userId: User.ID) =
    lila.hub.actorApi.calendar.CalendarCreate(
      id = s"${clazz.id}@$userId@${course.index}".some,
      typ = "course",
      user = userId,
      sdt = course.dateTime,
      edt = course.dateEndTime,
      content = s"${clazz.name} 第${course.index}节",
      onlySdt = false,
      link = s"/clazz/homework/show2?clazzId=${clazz.id}&courseId=${course.id}".some,
      icon = "写".some,
      bg = "#507803".some
    )

}
