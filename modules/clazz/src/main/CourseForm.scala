package lila.clazz

import lila.common.Form.ISODate
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import org.joda.time.format.DateTimeFormat

class CourseForm(val api: CourseApi) {

  def updateForm = Form(mapping(
    "date" -> ISODate.isoDate,
    "timeBegin" -> nonEmptyText,
    "timeEnd" -> nonEmptyText
  )(UpdateData.apply)(UpdateData.unapply).verifying("开始时间必须小于结束时间，并且大于当前时间", _.validTime))

  def appendForm(clazz: Clazz) = Form(mapping(
    "date" -> ISODate.isoDate.verifying("确保开始日期在最后的课节之后", _.isAfter(clazz.endDate)),
    "times" -> number(min = 1, max = Clazz.MAX_COURSE - clazz.times).verifying(s"总课节数小于${Clazz.MAX_COURSE}", t => t + clazz.times <= Clazz.MAX_COURSE)
  )(Append.apply)(Append.unapply))

}

case class UpdateData(date: DateTime, timeBegin: String, timeEnd: String) {

  private val datePattern = "yyyy-MM-dd"
  private val dateTimeFormatter = DateTimeFormat forPattern s"$datePattern HH:mm"

  def validTime =
    timeEnd.compareTo(timeBegin) > 0 && dateTimeFormatter.parseDateTime(date.toString(datePattern) + " " + timeBegin).isAfterNow

}

