package lila.clazz

import reactivemongo.bson.{ BSONHandler, BSONString, Macros }
import lila.db.BSON.BSONJodaDateTimeHandler
import lila.db.dsl.bsonArrayToListHandler

object BSONHandlers {

  private implicit val appendHandler = Macros.handler[Append]
  private implicit val appendArrayHandler = bsonArrayToListHandler[Append]
  private implicit val weekCourseHandler = Macros.handler[WeekCourse]
  private implicit val trainCourseHandler = Macros.handler[TrainCourse]
  private implicit val weekCourseArrayHandler = bsonArrayToListHandler[WeekCourse]
  private implicit val trainCourseArrayHandler = bsonArrayToListHandler[TrainCourse]
  implicit val weekClassHandler = Macros.handler[WeekClazz]
  private implicit val trainClassHandler = Macros.handler[TrainClazz]

  implicit val RequestStatusBSONHandler = new BSONHandler[BSONString, Invite.Status] {
    def read(b: BSONString) = Invite.Status.byId get b.value err s"Invalid Invite.Status. ${b.value}"
    def write(x: Invite.Status) = BSONString(x.id)
  }
  implicit val InviteBSONHandler = Macros.handler[Invite]
  implicit val StudentBSONHandler = Macros.handler[Student]

  implicit val ClazzTypeBSONHandler = new BSONHandler[BSONString, Clazz.ClazzType] {
    def read(b: BSONString) = Clazz.ClazzType.byId get b.value err s"Invalid ClazzType. ${b.value}"
    def write(x: Clazz.ClazzType) = BSONString(x.id)
  }

  private implicit val stringArrayHandler = bsonArrayToListHandler[String]
  private implicit val intCourseArrayHandler = bsonArrayToListHandler[Int]

  implicit val clazzHandler = Macros.handler[Clazz]
  implicit val courseHandler = Macros.handler[Course]
  implicit val CourseAttendHandler = Macros.handler[CourseAttend]

}
