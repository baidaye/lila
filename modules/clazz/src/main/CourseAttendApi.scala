package lila.clazz

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime

final class CourseAttendApi(coll: Coll) {

  import BSONHandlers.CourseAttendHandler

  def bulkInsert(courseAttendList: List[CourseAttend]): Funit = coll.bulkInsert(
    documents = courseAttendList.map(CourseAttendHandler.write).toStream,
    ordered = false
  ).void

  def byId(id: CourseAttend.ID): Fu[Option[CourseAttend]] = coll.byId[CourseAttend](id)

  def list(clazz: Clazz, courseId: String): Fu[Map[User.ID, (Option[Boolean], Int)]] = coll.find($doc("clazz" -> clazz.id)).list[CourseAttend]() map { list =>
    list.groupBy(_.user).mapValues { list =>
      list.find(_.course == courseId).map(_.absent) -> list.count(_.absent)
    }
  }

  def listByCourse(clazzId: Clazz.ID, courseId: String): Fu[List[CourseAttend]] =
    coll.find(
      $doc(
        "clazz" -> clazzId,
        "course" -> courseId
      )
    ).list[CourseAttend]()

  def listByUser(clazzId: Clazz.ID, user: User.ID): Fu[List[CourseAttend]] =
    coll.find(
      $doc(
        "clazz" -> clazzId,
        "user" -> user
      )
    ).list[CourseAttend]()

  def setAbsents(clazz: Clazz, course: Course, absents: List[String]) = {
    val attends = absents.map { userId =>
      CourseAttend.make(course, userId, true)
    } ++ clazz.studentIds.filterNot(u => absents.contains(u)).map { userId =>
      CourseAttend.make(course, userId, false)
    }

    coll.remove(
      $doc(
        "clazz" -> clazz.id,
        "course" -> course.id
      )
    ) >> bulkInsert(attends)
  }

  def setStudentAbsents(clazz: Clazz, userId: String, courses: List[Course], courseAttends: List[String]) = {
    val map = courseAttends.map { s =>
      val ca = s.split(":")
      ca(0) -> ca(1).toInt
    }.toMap

    val attends =
      courses.map { course =>
        course -> map.get(course.id)
      }.filter(_._2.isDefined)
        .map(x => x._1 -> x._2.get)
        .filter(_._2 >= 0)
        .map { courseWithAbsent =>
          CourseAttend.make(courseWithAbsent._1, userId, courseWithAbsent._2 == 0)
        }

    coll.remove(
      $doc(
        "clazz" -> clazz.id,
        "user" -> userId
      )
    ) >> bulkInsert(attends)
  }

}
