package lila.clazz

import reactivemongo.bson.{ BSONArray, BSONDocument, BSONDouble, BSONElement, BSONHandler, BSONInteger, Macros }
import lila.db.dsl.{ Bdoc, bsonArrayToListHandler }
import lila.db.BSON.BSONJodaDateTimeHandler
import lila.user.User

object HomeworkV2BSONHandlers {

  import lila.task.BSONHandlers.TTaskItemTypeBSONHandler
  implicit val stringArrayHandler = bsonArrayToListHandler[String]

  implicit val HomeworkV2StatusBSONHandler = new BSONHandler[BSONInteger, HomeworkV2.Status] {
    def read(b: BSONInteger) = HomeworkV2.Status.byId get b.value err s"Invalid Status ${b.value}"
    def write(x: HomeworkV2.Status) = BSONInteger(x.id)
  }

  private implicit val HomeworkV2TaskTplHandler = Macros.handler[HomeworkV2TaskTpl]
  private implicit val HomeworkV2TaskTplArrayHandler = bsonArrayToListHandler[HomeworkV2TaskTpl]
  private implicit val HomeworkV2TaskTplsHandler = Macros.handler[HomeworkV2TaskTpls]

  private implicit val HomeworkV2TaskHandler = Macros.handler[HomeworkV2Task]
  private implicit val HomeworkV2TaskArrayHandler = bsonArrayToListHandler[HomeworkV2Task]
  private implicit val HomeworkV2TasksHandler = Macros.handler[HomeworkV2Tasks]

  private implicit val CompleteRateHandler = new BSONHandler[Bdoc, Map[User.ID, Double]] {
    def read(doc: Bdoc) =
      doc.elements.map {
        case BSONElement(u, rate: BSONDouble) => u -> rate.value
      }.toMap

    def write(rate: Map[User.ID, Double]) = BSONDocument(rate.mapValues(r => BSONDouble(r)))
  }

  private implicit val CommonReportHandler = Macros.handler[CommonReport]
  private implicit val CommonWithReportHandler = Macros.handler[CommonWithReport]
  private implicit val CommonWithReportArrayHandler = bsonArrayToListHandler[CommonWithReport]
  private implicit val CommonWithReportMapHandler = new BSONHandler[Bdoc, Map[User.ID, List[CommonWithReport]]] {
    def read(doc: Bdoc) =
      doc.elements.map {
        case BSONElement(u, array: BSONArray) => u -> CommonWithReportArrayHandler.read(array)
      }.toMap

    def write(common: Map[User.ID, List[CommonWithReport]]) = BSONDocument(common.mapValues(CommonWithReportArrayHandler.write))
  }

  private implicit val MoveNumHandler = Macros.handler[MoveNum]
  private implicit val RateNumHandler = Macros.handler[RateNum]
  private implicit val UserRateHandler = Macros.handler[UserRate]
  private implicit val PuzzleReportHandler = Macros.handler[PuzzleReport]
  private implicit val PuzzleWithReportHandler = Macros.handler[PuzzleWithReport]
  private implicit val PuzzleWithReportArrayHandler = bsonArrayToListHandler[PuzzleWithReport]
  private implicit val CapsulePuzzleWithReportHandler = Macros.handler[CapsulePuzzleWithReport]

  private implicit val ReplayGameReportHandler = Macros.handler[ReplayGameReport]
  private implicit val ReplayGameReportArrayHandler = bsonArrayToListHandler[ReplayGameReport]
  private implicit val ReplayGameWithReportHandler = Macros.handler[ReplayGameWithReport]

  private implicit val RecallTurnsDistributeHandler = Macros.handler[RecallTurnsDistribute]
  private implicit val RecallGameReportHandler = Macros.handler[RecallGameReport]
  private implicit val RecallGameReportArrayHandler = bsonArrayToListHandler[RecallGameReport]
  private implicit val RecallGameWithReportHandler = Macros.handler[RecallGameWithReport]

  private implicit val DistinguishTurnsDistributeHandler = Macros.handler[DistinguishTurnsDistribute]
  private implicit val DistinguishGameReportHandler = Macros.handler[DistinguishGameReport]
  private implicit val DistinguishGameReportArrayHandler = bsonArrayToListHandler[DistinguishGameReport]
  private implicit val DistinguishGameWithReportHandler = Macros.handler[DistinguishGameWithReport]

  private implicit val FromPositionRoundDistributeHandler = Macros.handler[FromPositionRoundDistribute]
  private implicit val FromPositionReportHandler = Macros.handler[FromPositionReport]
  private implicit val FromPositionReportArrayHandler = bsonArrayToListHandler[FromPositionReport]
  private implicit val FromPositionWithReportHandler = Macros.handler[FromPositionWithReport]

  private implicit val FromPgnRoundDistributeHandler = Macros.handler[FromPgnRoundDistribute]
  private implicit val FromPgnReportHandler = Macros.handler[FromPgnReport]
  private implicit val FromPgnReportArrayHandler = bsonArrayToListHandler[FromPgnReport]
  private implicit val FromPgnWithReportHandler = Macros.handler[FromPgnWithReport]

  private implicit val FromOpeningdbRoundDistributeHandler = Macros.handler[FromOpeningdbRoundDistribute]
  private implicit val FromOpeningdbReportHandler = Macros.handler[FromOpeningdbReport]
  private implicit val FromOpeningdbReportArrayHandler = bsonArrayToListHandler[FromOpeningdbReport]
  private implicit val FromOpeningdbWithReportHandler = Macros.handler[FromOpeningdbWithReport]

  private implicit val PracticeReportHandler = Macros.handler[HomeworkPracticeReport]

  implicit val HomeworkV2Handler = Macros.handler[HomeworkV2]
  implicit val HomeworkV2StudentHandler = Macros.handler[HomeworkV2Student]
  implicit val HomeworkReportHandler = Macros.handler[HomeworkV2Report]

}
