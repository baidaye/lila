package lila.clazz

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.notify.NotifyApi
import lila.team.TeamApi
import lila.user.User
import scala.concurrent.duration._
import lila.common.{ AtMost, Every, ResilientScheduler }

final class Env(
    config: Config,
    db: lila.db.Env,
    notifyApi: NotifyApi,
    teamApi: TeamApi,
    system: ActorSystem,
    asyncCache: lila.memo.AsyncCache.Builder,
    hub: lila.hub.Env,
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi,
    taskJsonView: lila.task.JsonView
) {

  val bus = system.lilaBus

  private[clazz] val CollectionClazz = config getString "collection.clazz"
  private[clazz] val CollectionClazzStudent = config getString "collection.student"
  private[clazz] val CollectionClazzInvite = config getString "collection.invite"
  private[clazz] val CollectionCourse = config getString "collection.course"
  private[clazz] val CollectionCourseAttend = config getString "collection.attend"
  private[clazz] val CollectionHomeworkV2 = config getString "collection.homework2"
  private[clazz] val CollectionHomeworkV2Student = config getString "collection.homework2_student"
  private[clazz] val CollectionHomeworkV2Report = config getString "collection.homework2_report"

  val mineClazzCache = asyncCache.clearable[User.ID, List[Clazz]](
    name = "clazz.mine",
    f = api.coachClazzs,
    expireAfter = _.ExpireAfterWrite(1 hour)
  )

  val clazzColl = db(CollectionClazz)

  val studentColl = db(CollectionClazzStudent)

  val inviteColl = db(CollectionClazzInvite)

  val homeworkV2Coll = db(CollectionHomeworkV2)

  val homeworkV2StudentColl = db(CollectionHomeworkV2Student)

  val homeworkV2ReportColl = db(CollectionHomeworkV2Report)

  lazy val courseApi = new CourseApi(
    coll = db(CollectionCourse),
    clazzColl = db(CollectionClazz),
    bus = bus
  )(system)

  lazy val api = new ClazzApi(
    coll = clazzColl,
    teamApi = teamApi,
    markActor = hub.relation,
    bus = bus,
    asyncCache = asyncCache
  )

  lazy val studentApi = new StudentApi(
    coll = studentColl,
    clazzColl = clazzColl,
    teamApi = teamApi,
    notifyApi = notifyApi,
    bus = bus,
    markActor = hub.relation,
    mineClazzCache = mineClazzCache
  )

  lazy val courseAttendApi = new CourseAttendApi(
    coll = db(CollectionCourseAttend)
  )

  lazy val jsonView = new JsonView(taskJsonView, lightUserApi, isOnline)

  lazy val form = new ClazzForm(api)

  lazy val courseForm = new CourseForm(courseApi)

  lazy val homeworkV2Form = new HomeworkFormV2

  lazy val homeworkV2Api = new HomeworkV2Api(
    notifyApi = notifyApi,
    bus = bus
  )

  lazy val homeworkV2Report = new HomeworkV2ReportApi(
    api = homeworkV2Api,
    coll = db(CollectionHomeworkV2Report)
  )

  def removeByUserId(userId: User.ID): Funit = {
    api.mine(userId) flatMap { clazzes =>
      clazzes.map { clazz =>
        studentApi.removeStudent(clazz, userId)
      }.sequenceFu.void
    }
  }

  ResilientScheduler(
    every = Every(10 minute),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 30 seconds
  ) { homeworkV2Api.deadlinePublish }(system)

  //  ResilientScheduler(
  //    every = Every(10 minute),
  //    atMost = AtMost(30 seconds),
  //    logger = logger,
  //    initialDelay = 30 seconds
  //  ) { homeworkV2Report.schedulerRefresh }(system)

  system.lilaBus.subscribeFuns(
    'isClazzCoachOf -> {
      case lila.hub.actorApi.clazz.IsCoachOfUser(coachId, studentId, promise) =>
        promise completeWith {
          mineClazzCache.get(coachId).map { clazzs =>
            val studentIds = clazzs.flatMap(_.studentIds)
            studentIds.contains(studentId)
          }
        }
    }
  )

}

object Env {

  lazy val current: Env = "clazz" boot new Env(
    config = lila.common.PlayApp loadConfig "clazz",
    db = lila.db.Env.current,
    notifyApi = lila.notify.Env.current.api,
    teamApi = lila.team.Env.current.api,
    system = lila.common.PlayApp.system,
    asyncCache = lila.memo.Env.current.asyncCache,
    hub = lila.hub.Env.current,
    isOnline = lila.user.Env.current.isOnline,
    lightUserApi = lila.user.Env.current.lightUserApi,
    taskJsonView = lila.task.Env.current.jsonView
  )
}
