package lila.clazz

import lila.task.TTask.TTaskItemType
import lila.task.{ DistinguishGameItem, FromPositionItem, FromPgnItem, FromOpeningdbItem, MiniPuzzle, RecallGameItem, ReplayGameItem, TTaskTemplate }
import org.joda.time.DateTime
import lila.user.User

case class HomeworkV2Report(
    _id: HomeworkV2.ID,
    students: List[User.ID],
    completeRate: Option[Map[User.ID, Double]],
    coinDiff: Option[Map[User.ID, Int]],
    common: Map[User.ID, List[CommonWithReport]],
    practice: HomeworkPracticeReport,
    updateAt: DateTime,
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

}

object HomeworkV2Report {

  sealed class PuzzleSort(val id: String, val name: String)
  object PuzzleSort {
    case object No extends PuzzleSort("no", "题目编号")
    case object FirstRightRate extends PuzzleSort("firstRightRate", "首次正确率")
    case object RightRate extends PuzzleSort("rightRate", "正确率")
    case object CompleteRate extends PuzzleSort("completeRate", "完成率")

    val all = List(No, FirstRightRate, RightRate)
    val selector = all.map(x => x.id -> x.name)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: String): PuzzleSort = byId.get(id) err s"Bad PuzzleSort $id"

    def apiSort(id: String): PuzzleSort = List(No, CompleteRate, RightRate, FirstRightRate).find(_.id == id) err s"Bad PuzzleSort $id"
  }

}

case class HomeworkV2ReportView(
    _id: HomeworkV2.ID,
    students: List[User.ID],
    completeRate: Option[Map[User.ID, Double]],
    coinDiff: Option[Map[User.ID, Int]],
    common: Map[User.ID, List[CommonWithReportView]],
    practice: HomeworkPracticeReportView,
    updateAt: DateTime,
    createAt: DateTime,
    createBy: User.ID
) {

  def size = students.size

  def completeRateOrDefault = completeRate | Map.empty[User.ID, Double]

  def completeRateDistribute =
    (0 to 10).map { i =>
      val r = i * 10
      (r, completeRateOrDefault.filter(ur => ur._2 >= r && ur._2 < r + 10).toSeq.sortBy(-_._2))
    }.sortBy(-_._1)

  def sortedCommon(studentIds: List[User.ID], sortType: Option[String]): List[(User.ID, List[CommonWithReportView])] =
    sortType match {
      case Some(sortType) => sortType match {
        case "up" => sortedCommonByCompleteRateAsc(studentIds)
        case "down" => sortedCommonByCompleteRateDesc(studentIds)
        case _ => sortedCommonByDefault(studentIds)
      }
      case None => sortedCommonByDefault(studentIds)
    }

  def sortedCommonByDefault(studentIds: List[User.ID]): List[(User.ID, List[CommonWithReportView])] =
    studentIds.map { studentId =>
      studentId -> common.get(studentId)
    }.filter(_._2.isDefined).map(c => c._1 -> c._2.get)

  def sortedCommonByCompleteRateAsc(defaultStudentIds: List[User.ID]): List[(User.ID, List[CommonWithReportView])] = {
    val studentIndexMap = defaultStudentIds.zipWithIndex.map {
      case (stu, index) => stu -> index
    }.toMap

    val sortedStudentIds = completeRateOrDefault.toList.sortWith {
      case (s1, s2) => {
        val s1Rate = (s1._2 * 100).toInt
        val s2Rate = (s2._2 * 100).toInt
        if (s1Rate == s2Rate) {
          val s1Index = studentIndexMap.get(s1._1) | 0
          val s2Index = studentIndexMap.get(s2._1) | 0
          s1Index < s2Index
        } else {
          s1Rate < s2Rate
        }
      }
    }.map(_._1)

    sortedCommonByDefault(sortedStudentIds)
  }

  def sortedCommonByCompleteRateDesc(defaultStudentIds: List[User.ID]): List[(User.ID, List[CommonWithReportView])] = {
    val studentIndexMap = defaultStudentIds.zipWithIndex.map {
      case (stu, index) => stu -> index
    }.toMap

    val sortedStudentIds = completeRateOrDefault.toList.sortWith {
      case (s1, s2) => {
        val s1Rate = (s1._2 * 100).toInt
        val s2Rate = (s2._2 * 100).toInt
        if (s1Rate == s2Rate) {
          val s1Index = studentIndexMap.get(s1._1) | 0
          val s2Index = studentIndexMap.get(s2._1) | 0
          s1Index < s2Index
        } else {
          s1Rate > s2Rate
        }
      }
    }.map(_._1)

    sortedCommonByDefault(sortedStudentIds)
  }

}

case class HomeworkPracticeReport(
    capsulePuzzles: List[CapsulePuzzleWithReport], // 指定战术题
    replayGames: List[ReplayGameWithReport], // 打谱
    recallGames: List[RecallGameWithReport], // 记谱
    distinguishGames: List[DistinguishGameWithReport], // 棋谱记录
    fromPositions: List[FromPositionWithReport], // 指定位置对局
    fromPgns: List[FromPgnWithReport], // 指定PGN对局
    fromOpeningdbs: List[FromOpeningdbWithReport] // 指定开局库对局
)

case class HomeworkPracticeReportView(
    capsulePuzzles: List[CapsulePuzzleWithReportView], // 指定战术题
    replayGames: List[ReplayGameWithReportView], // 打谱
    recallGames: List[RecallGameWithReportView], // 记谱
    distinguishGames: List[DistinguishGameWithReportView], // 棋谱记录
    fromPositions: List[FromPositionWithReportView], // 指定位置对局
    fromPgns: List[FromPgnWithReportView],
    fromOpeningdbs: List[FromOpeningdbWithReportView]
)

case class CommonReport(total: Int, num: Int, completed: Boolean)
case class CommonWithReport(taskTplId: TTaskTemplate.ID, report: CommonReport)
//  显示时使用
case class CommonWithReportView(taskTpl: TTaskTemplate, report: CommonReport)

case class MoveNum(move: String, num: Int, students: Option[List[User.ID]])
case class RateNum(rate: Double, students: List[User.ID])
case class UserRate(student: User.ID, rate: Double)
case class PuzzleReport(completeRate: RateNum, rightRate: RateNum, firstMoveRightRate: RateNum, rightMoveDistribute: List[MoveNum], wrongMoveDistribute: List[MoveNum])
case class PuzzleWithReport(puzzleId: Int, report: PuzzleReport)
case class CapsulePuzzleWithReport(taskTplId: TTaskTemplate.ID, report: List[PuzzleWithReport], firstRightRate: Option[List[UserRate]]) {
  def taskItemType = TTaskItemType.CapsulePuzzleItem
}
//  显示时使用
case class PuzzleWithReportView(puzzle: MiniPuzzle, report: PuzzleReport)
case class CapsulePuzzleWithReportView(taskTpl: TTaskTemplate, report: List[PuzzleWithReportView], firstRightRate: Option[List[UserRate]]) {

  def firstRightRateOrEmpty = firstRightRate | List.empty[UserRate]

  def firstRightRateDistribute =
    (0 to 10).map { i =>
      val r = i * 10
      (r, firstRightRateOrEmpty.filter(ur => ur.rate >= r && ur.rate < r + 10).sortBy(-_.rate))
    }.sortBy(-_._1)

}

case class ReplayGameReport(unique: String, complete: Int, students: Option[List[User.ID]])
case class ReplayGameWithReport(taskTplId: TTaskTemplate.ID, report: List[ReplayGameReport]) {
  def taskItemType = TTaskItemType.ReplayGameItem
}
//  显示时使用
case class ReplayGameReportView(replayGame: ReplayGameItem, complete: Int, students: Option[List[User.ID]])
case class ReplayGameWithReportView(taskTpl: TTaskTemplate, report: List[ReplayGameReportView])

case class RecallTurnsDistribute(turns: Int, num: Int, students: Option[List[User.ID]])
case class RecallGameReport(unique: String, turnsDistribute: List[RecallTurnsDistribute])
case class RecallGameWithReport(taskTplId: TTaskTemplate.ID, report: List[RecallGameReport]) {
  def taskItemType = TTaskItemType.RecallGameItem
}
//  显示时使用
case class RecallGameReportView(recallGame: RecallGameItem, turnsDistribute: List[RecallTurnsDistribute])
case class RecallGameWithReportView(taskTpl: TTaskTemplate, report: List[RecallGameReportView])

case class DistinguishTurnsDistribute(turns: Int, num: Int, students: Option[List[User.ID]])
case class DistinguishGameReport(unique: String, turnsDistribute: List[DistinguishTurnsDistribute])
case class DistinguishGameWithReport(taskTplId: TTaskTemplate.ID, report: List[DistinguishGameReport]) {
  def taskItemType = TTaskItemType.DistinguishGameItem
}
//  显示时使用
case class DistinguishGameReportView(distinguishGame: DistinguishGameItem, turnsDistribute: List[DistinguishTurnsDistribute])
case class DistinguishGameWithReportView(taskTpl: TTaskTemplate, report: List[DistinguishGameReportView])

case class FromPositionRoundDistribute(rounds: Int, num: Int, students: Option[List[User.ID]])
case class FromPositionReport(unique: String, roundDistribute: List[FromPositionRoundDistribute])
case class FromPositionWithReport(taskTplId: TTaskTemplate.ID, report: List[FromPositionReport]) {
  def taskItemType = TTaskItemType.FromPositionItem
}

//  显示时使用
case class FromPositionReportView(fromPosition: FromPositionItem, roundDistribute: List[FromPositionRoundDistribute])
case class FromPositionWithReportView(taskTpl: TTaskTemplate, report: List[FromPositionReportView])

case class FromPgnRoundDistribute(rounds: Int, num: Int, students: Option[List[User.ID]])
case class FromPgnReport(unique: String, roundDistribute: List[FromPgnRoundDistribute])
case class FromPgnWithReport(taskTplId: TTaskTemplate.ID, report: List[FromPgnReport]) {
  def taskItemType = TTaskItemType.FromPgnItem
}

//  显示时使用
case class FromPgnReportView(fromPgn: FromPgnItem, roundDistribute: List[FromPgnRoundDistribute])
case class FromPgnWithReportView(taskTpl: TTaskTemplate, report: List[FromPgnReportView])

case class FromOpeningdbRoundDistribute(rounds: Int, num: Int, students: Option[List[User.ID]])
case class FromOpeningdbReport(unique: String, roundDistribute: List[FromOpeningdbRoundDistribute])
case class FromOpeningdbWithReport(taskTplId: TTaskTemplate.ID, report: List[FromOpeningdbReport]) {
  def taskItemType = TTaskItemType.FromOpeningdbItem
}

//  显示时使用
case class FromOpeningdbReportView(fromOpeningdb: FromOpeningdbItem, roundDistribute: List[FromOpeningdbRoundDistribute])
case class FromOpeningdbWithReportView(taskTpl: TTaskTemplate, report: List[FromOpeningdbReportView])
