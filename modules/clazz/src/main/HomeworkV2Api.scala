package lila.clazz

import lila.hub.actorApi.clazz.{ HomeworkDeadline, HomeworkFinish, HomeworkUpdateCoin }
import lila.notify.Notification.{ Notifies, Sender }
import lila.notify.{ Notification, NotifyApi }
import lila.task.{ TTask, TTaskRepo, TTaskTemplate, TTaskTemplateRepo }
import org.joda.time.DateTime
import lila.user.User

final class HomeworkV2Api(notifyApi: NotifyApi, bus: lila.common.Bus) {

  bus.subscribeFun('clazzJoinAccept, 'homeworkCreate, 'ttask, 'teamCoinChange, 'courseUpdated) {
    case lila.hub.actorApi.clazz.ClazzJoinAccept(clazzId, clazzName, _, studentId) => {
      HomeworkV2Repo.findByClazz(clazzId) flatMap { homeworks =>
        createOldHomeworks(clazzId, clazzName, studentId, homeworks.filter(_.isPublished))
      }
    }
    case StudentHomeworkCreate(clazzName, homeworks) => homeworkCreateNotify(clazzName, homeworks)
    case lila.hub.actorApi.task.ChangeStatus(taskId, taskName, sourceRel, userId, creator, status, num, _) => {
      if (sourceRel.source == "homework" && status == "finished") {
        val shwkId = sourceRel.id + "@" + userId
        finishTaskProcess(shwkId)
      }
    }
    case lila.hub.actorApi.team.TeamCoinChangeFromHomework(_, userId, homeworkId, coinDiff, _, _) => {
      val shwkId = homeworkId + "@" + userId
      HomeworkV2StudentRepo.setCoinDiff(shwkId, coinDiff)
    }
    case CourseUpdate(newCourse) => updateStartAt(newCourse)
  }

  def byId(id: HomeworkV2.ID): Fu[Option[HomeworkV2.WithTask]] =
    HomeworkV2Repo.byId(id) flatMap {
      case Some(homework) => findWithTask(homework).map(_.some)
      case None => fuccess(none[HomeworkV2.WithTask])
    }

  def byCourse(course: Course): Fu[Option[HomeworkV2.WithTask]] =
    byId(HomeworkV2.makeId(course.clazz, course.id, course.index))

  def findWithTask(homework: HomeworkV2): Fu[HomeworkV2.WithTask] =
    TTaskTemplateRepo.byIds(homework.taskTplIds).map { taskTpls =>
      val common = homework.common.map(comm => HomeworkV2RealTaskTpls.toHomeworkV2TaskTpls(comm, taskTpls))
      val practice = homework.practice.map(prac => HomeworkV2RealTaskTpls.toHomeworkV2TaskTpls(prac, taskTpls))
      HomeworkV2.WithTask(homework, common, practice)
    }

  def findWithTask(homeworks: List[HomeworkV2]): Fu[List[HomeworkV2.WithTask]] =
    TTaskTemplateRepo.byIds(homeworks.flatMap(_.taskTplIds)).map { taskTpls =>
      homeworks.map { homework =>
        val common = homework.common.map(comm => HomeworkV2RealTaskTpls.toHomeworkV2TaskTpls(comm, taskTpls))
        val practice = homework.practice.map(prac => HomeworkV2RealTaskTpls.toHomeworkV2TaskTpls(prac, taskTpls))
        HomeworkV2.WithTask(homework, common, practice)
      }
    }

  def findStudentByCourse(course: Course, studentId: User.ID): Fu[Option[HomeworkV2Student.WithTask]] = {
    val homeworkId = HomeworkV2.makeId(course.clazz, course.id, course.index)
    findStudentById(HomeworkV2Student.makeId(homeworkId, studentId))
  }

  def findStudentById(id: HomeworkV2Student.ID): Fu[Option[HomeworkV2Student.WithTask]] =
    HomeworkV2StudentRepo.byId(id) flatMap {
      case Some(homework) => findStudentWithTask(homework).map(_.some)
      case None => fuccess(none[HomeworkV2Student.WithTask])
    }

  def findStudentWithTask(homework: HomeworkV2Student): Fu[HomeworkV2Student.WithTask] =
    TTaskRepo.byIds(homework.taskIds).map { tasks =>
      val common = homework.common.map(comm => HomeworkV2RealTasks.toHomeworkV2Tasks(comm, tasks))
      val practice = homework.practice.map(prac => HomeworkV2RealTasks.toHomeworkV2Tasks(prac, tasks))
      HomeworkV2Student.WithTask(homework, common, practice)
    }

  def findStudentsWithTaskForUser(clazzId: Clazz.ID, userId: User.ID): Fu[List[HomeworkV2Student.WithTask]] = {
    HomeworkV2StudentRepo.mineOfClazz(clazzId, userId) flatMap { homeworks =>
      findStudentWithTask(homeworks)
    }
  }

  def findStudentsWithTaskForCourse(clazzId: Clazz.ID, courseId: Course.ID): Fu[List[HomeworkV2Student.WithTask]] = {
    HomeworkV2StudentRepo.findByCourse(clazzId, courseId) flatMap { homeworks =>
      findStudentWithTask(homeworks)
    }
  }

  def findStudentWithTask(homeworks: List[HomeworkV2Student]): Fu[List[HomeworkV2Student.WithTask]] =
    TTaskRepo.byIds(homeworks.flatMap(_.taskIds)).map { tasks =>
      val userTaskMap = tasks.groupBy(_.userIds.head)
      homeworks.map { homework =>
        val common = userTaskMap.get(homework.studentId) match {
          case None => None
          case Some(ts) => homework.common.map(comm => HomeworkV2RealTasks.toHomeworkV2Tasks(comm, ts))
        }
        val practice = userTaskMap.get(homework.studentId) match {
          case None => None
          case Some(ts) => homework.practice.map(prac => HomeworkV2RealTasks.toHomeworkV2Tasks(prac, ts))
        }
        HomeworkV2Student.WithTask(homework, common, practice)
      }
    }

  def findOrCreate(clazz: Clazz, course: Course, userId: User.ID): Fu[HomeworkV2.WithTask] =
    byCourse(course) flatMap {
      case None => create(clazz, course, userId) flatMap { homework =>
        findWithTask(homework)
      }
      case Some(h) => fuccess(h)
    }

  def create(clazz: Clazz, course: Course, userId: User.ID): Fu[HomeworkV2] = {
    val homework = HomeworkV2.empty(clazz.id, clazz.name, course.id, course.index, course.dateEndTime.some, userId)
    HomeworkV2Repo.insert(homework).inject(homework)
  }

  def cloneBy(sourceHomework: HomeworkV2.WithTask, clazz: Clazz, course: Course, userId: User.ID): Fu[HomeworkV2.WithTask] =
    byCourse(course) flatMap {
      case None => {
        val newHomeworkWithTaskTpl = cloneHomework(sourceHomework, clazz, course, userId)
        for {
          _ <- TTaskTemplateRepo.batchInsert(newHomeworkWithTaskTpl.allTaskTpls.map(_.taskTpl))
          _ <- HomeworkV2Repo.insert(newHomeworkWithTaskTpl.homework)
        } yield newHomeworkWithTaskTpl
      }
      case Some(h) => {
        val homework = h.homework
        if (homework.isCreated) {
          val newHomeworkWithTaskTpl = cloneHomework(sourceHomework, clazz, course, userId)
          TTaskTemplateRepo.removeByHomework(newHomeworkWithTaskTpl.id, Nil) flatMap { _ =>
            for {
              _ <- TTaskTemplateRepo.batchInsert(newHomeworkWithTaskTpl.allTaskTpls.map(_.taskTpl))
              _ <- HomeworkV2Repo.update(newHomeworkWithTaskTpl.homework)
            } yield newHomeworkWithTaskTpl
          }
        } else fufail(s"当前课后练不可被覆盖：${homework.name}")
      }
    }

  private def cloneHomework(sourceHomework: HomeworkV2.WithTask, clazz: Clazz, course: Course, userId: User.ID) = {
    val homework = HomeworkV2.cloneBy(sourceHomework.homework, clazz, course, userId)
    val common = cloneTaskTpl(homework, sourceHomework.commonOrEmpty, userId)
    val practice = cloneTaskTpl(homework, sourceHomework.practiceOrEmpty, userId)
    val homeworkWithTaskTpl = homework.copy(common = HomeworkV2TaskTpls(
      common.map { tpl =>
        HomeworkV2TaskTpl(tpl.taskItem, tpl.taskTpl.id)
      }
    ).some, practice = HomeworkV2TaskTpls(
      practice.map { tpl =>
        HomeworkV2TaskTpl(tpl.taskItem, tpl.taskTpl.id)
      }
    ).some)
    HomeworkV2.WithTask(homeworkWithTaskTpl, HomeworkV2RealTaskTpls(common).some, HomeworkV2RealTaskTpls(practice).some)
  }

  private def cloneTaskTpl(homework: HomeworkV2, taskTpls: List[HomeworkV2RealTaskTpl], userId: User.ID) = {
    taskTpls.map { tpl =>
      tpl.copy(
        taskItem = tpl.taskItem,
        taskTpl = TTaskTemplate.make(
          name = tpl.taskTpl.name,
          remark = tpl.taskTpl.remark,
          item = tpl.taskTpl.item,
          itemType = tpl.taskTpl.itemType,
          sourceRel = TTask.SourceRel(
            id = homework.id,
            name = homework.name | s"Clone From ${tpl.taskTpl.sourceRel.name}",
            source = tpl.taskTpl.sourceRel.source
          ),
          coinTeam = tpl.taskTpl.coinTeam,
          coinRule = tpl.taskTpl.coinRule,
          deadlineAt = homework.deadlineAt,
          createdBy = userId
        )
      )
    }
  }

  def update(homework: HomeworkV2, clazz: Clazz, course: Course, data: HomeworkV2Data, creator: User): Fu[HomeworkV2] = {
    val hw = data.toHomework(clazz, course, creator)
    val now = DateTime.now
    val newHomework = homework.copy(
      deadlineAt = hw.deadlineAt,
      overDeadline = Some(false),
      summary = hw.summary,
      prepare = hw.prepare,
      common = hw.common,
      practice = hw.practice,
      coinTeam = hw.coinTeam,
      coinRule = hw.coinRule,
      updatedAt = now
    )
    HomeworkV2Repo.update(newHomework) >> setDeadline(newHomework, (newHomework.deadlineAt | now.plusWeeks(1))) inject newHomework
  }

  def updateAndPublish(homework: HomeworkV2, clazz: Clazz, course: Course, data: HomeworkV2Data, creator: User): Funit = {
    update(homework, clazz, course, data, creator) flatMap { publish(_, clazz) }
  }

  def publish(homework: HomeworkV2, clazz: Clazz): Funit =
    TTaskTemplateRepo.removeByHomework(homework.id, homework.taskTplIds) >>
      HomeworkV2Repo.setStatus(homework, HomeworkV2.Status.Published) >>
      createStudentHomeworks(homework, clazz) >>
      Env.current.courseApi.setHomework(homework.courseId, true)

  def createStudentHomeworks(homework: HomeworkV2, clazz: Clazz): Funit = {
    findWithTask(homework) flatMap { homeworkWithTask =>
      var realTasks: List[TTask] = Nil
      val studentHomeworks = clazz.studentIds.map { studentId =>
        val h = toStudentHomework(homeworkWithTask, studentId)
        realTasks = realTasks ++ h._2
        h._1
      }

      realTasks.grouped(200).map { tasks =>
        TTaskRepo.batchInsert(tasks)
      }.sequenceFu.void >>
        HomeworkV2StudentRepo.bulkInsert(studentHomeworks) >>-
        bus.publish(StudentHomeworkCreate(clazz.name, studentHomeworks), 'homeworkCreate)
    }
  }

  def createOldHomeworks(clazzId: String, clazzName: String, studentId: String, homeworks: List[HomeworkV2]): Funit = {
    HomeworkV2StudentRepo.mineOfClazz(clazzId, studentId) flatMap { myHomeworks =>
      var realTasks: List[TTask] = Nil
      val ids = myHomeworks.map(_.homeworkId)
      val studentHomeworks = homeworks.filterNot(h => ids.contains(h.id))
      findWithTask(studentHomeworks).flatMap { homeworkWithTasks =>
        val studentHomeworks = homeworkWithTasks.map { homeworkWithTask =>
          val h = toStudentHomework(homeworkWithTask, studentId)
          realTasks = realTasks ++ h._2
          h._1
        }

        realTasks.grouped(200).map { tasks =>
          TTaskRepo.batchInsert(tasks)
        }.sequenceFu.void >>
          HomeworkV2StudentRepo.bulkInsert(studentHomeworks) >>-
          bus.publish(StudentHomeworkCreate(clazzName, studentHomeworks), 'homeworkCreate)
      }
    }
  }

  private def toStudentHomework(homeworkWithTask: HomeworkV2.WithTask, studentId: User.ID): (HomeworkV2Student, List[TTask]) = {
    val commonRealTasks = homeworkWithTask.toStudentRealTasks(homeworkWithTask.homework.startAt, homeworkWithTask.common, studentId)
    val commonTask = homeworkWithTask.common.map { _ =>
      HomeworkV2Tasks(
        commonRealTasks.map { task =>
          HomeworkV2Task(task.itemType, task.id)
        }
      )
    }

    val practiceRealTasks = homeworkWithTask.toStudentRealTasks(homeworkWithTask.homework.startAt, homeworkWithTask.practice, studentId)
    val practiceTask = homeworkWithTask.practice.map { _ =>
      HomeworkV2Tasks(
        practiceRealTasks.map { task =>
          HomeworkV2Task(task.itemType, task.id)
        }
      )
    }
    HomeworkV2Student.byHomework(homeworkWithTask.homework, commonTask, practiceTask, studentId) -> (commonRealTasks ++ practiceRealTasks)
  }

  def revoke(homework: HomeworkV2): Funit = {
    TTaskRepo.removeByHomework(homework.id) >>
      HomeworkV2StudentRepo.removeByCourse(homework.clazzId, homework.courseId) >>
      HomeworkV2Repo.setStatus(homework, HomeworkV2.Status.Created) >>
      Env.current.courseApi.setHomework(homework.courseId, hwk = false)
  }

  def setDeadline(homework: HomeworkV2, deadlineAt: DateTime): Funit =
    HomeworkV2Repo.setDeadline(homework, deadlineAt) >>
      TTaskTemplateRepo.setDeadline(homework.taskTplIds, deadlineAt) >> homework.isPublished.?? {
        HomeworkV2StudentRepo.setDeadline(homework.clazzId, homework.courseId, deadlineAt) >>
          TTaskRepo.setDeadline(homework.taskTplIds, deadlineAt)
      }

  def deadlinePublish: Funit =
    HomeworkV2Repo.overDeadlines map { homeworks =>
      homeworks.nonEmpty.?? {
        HomeworkV2Repo.setOverDeadline(homeworks.map(_.id)) >>
          findWithTask(homeworks) foreach { hwts =>
            hwts.foreach { hwt =>
              HomeworkV2StudentRepo.setOverDeadline(hwt.homework.clazzId, hwt.homework.courseId) >>
                findStudentsWithTaskForCourse(hwt.homework.clazzId, hwt.homework.courseId) foreach { shwts =>
                  shwts.foreach { shwt =>
                    bus.publish(HomeworkDeadline(shwt.homework.studentId, shwt.homework.createdBy, shwt.homework.homeworkId, shwt.homework.nameOrDefault, shwt.homework.coinRule, shwt.coinDiff, shwt.homework.coinDiff), 'homeworkDeadline)
                  }
                }
            }
          }
      }
    }

  def finishTaskProcess(shwkId: HomeworkV2Student.ID): Funit =
    HomeworkV2StudentRepo.byId(shwkId) flatMap {
      _.?? { stuHomework =>
        findStudentWithTask(stuHomework) flatMap { shwt =>
          (!shwt.homework.isDeadlined).?? {
            HomeworkV2StudentRepo.setProgress(shwkId, shwt.progress) >>- {
              if (shwt.isFinished) {
                bus.publish(HomeworkFinish(stuHomework.studentId, stuHomework.createdBy, stuHomework.homeworkId, stuHomework.nameOrDefault, stuHomework.coinRule, shwt.coinDiff, shwt.homework.coinDiff), 'homeworkFinish)
              }
            }
          }
        }
      }
    }

  def updateCoin(stuHomework: HomeworkV2Student, coinDiff: Int) =
    bus.publish(HomeworkUpdateCoin(stuHomework.studentId, stuHomework.createdBy, stuHomework.homeworkId, stuHomework.nameOrDefault, stuHomework.coinRule, coinDiff.some, stuHomework.coinDiff), 'homeworkUpdateCoin)

  def updateStartAt(course: Course) = {
    val homeworkId = HomeworkV2.makeId(course.clazz, course.id, course.index)
    HomeworkV2Repo.byId(homeworkId).flatMap {
      _.?? { homework =>
        HomeworkV2Repo.setStartAt(homework, course.dateEndTime) >>
          HomeworkV2StudentRepo.setStartAt(course.clazz, course.id, course.dateEndTime) >>
          TTaskRepo.updateStartAtByHomework(homework.id, course.dateEndTime)
      }
    }
  }

  private def homeworkCreateNotify(clazzName: String, studentHomeworks: List[HomeworkV2Student]) =
    studentHomeworks.foreach { homework =>
      notifyApi.addNotification(Notification.make(
        Sender(homework.createdBy).some,
        Notifies(homework.studentId),
        lila.notify.GenericLink(
          url = s"/clazz/homework/show?id=${homework.id}",
          title = "新的课后练".some,
          text = s"【$clazzName】第${homework.index}次课后练已发布，请在${homework.deadline}之前完成".some,
          icon = "写"
        )
      ))
    }

}
