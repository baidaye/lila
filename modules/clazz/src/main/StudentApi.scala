package lila.clazz

import lila.db.dsl._
import lila.team.TeamApi
import akka.pattern.ask
import akka.actor.ActorSelection
import lila.hub.actorApi.relation.{ GetMark, GetMarks }
import lila.user.{ User, UserRepo }
import lila.notify.{ InvitedToClazz, Notification, NotifyApi }
import lila.common.paginator.Paginator
import lila.db.paginator.Adapter
import lila.hub.actorApi.calendar.CalendarCreate
import lila.memo.AsyncCacheClearable
import lila.notify.Notification.Sender
import makeTimeout.large

final class StudentApi(
    coll: Coll,
    clazzColl: Coll,
    teamApi: TeamApi,
    notifyApi: NotifyApi,
    bus: lila.common.Bus,
    markActor: ActorSelection,
    mineClazzCache: AsyncCacheClearable[String, List[Clazz]]
) {

  import BSONHandlers.StudentBSONHandler

  def selectId(clazzId: String, userId: String) = $id(Student.makeId(clazzId, userId))
  def clazzQuery(clazzId: String) = $doc("clazzId" -> clazzId)
  def userQuery(userId: String) = $doc("userId" -> userId)

  def byId(clazzId: String, userId: String): Fu[Option[Student]] = {
    coll.byId[Student](Student.makeId(clazzId, userId))
  }

  def inviteStudent(clazz: Clazz, userId: User.ID): Funit =
    InviteRepo.add(clazz.id, userId = userId).void >> sendInviteNotify(clazz, userId)

  def reInviteStudent(clazz: Clazz, userId: User.ID): Funit =
    InviteRepo.updateInviteTime(clazz.id, userId = userId).void >> sendInviteNotify(clazz, userId)

  def invitedAgain(clazz: Clazz, userId: User.ID): Funit =
    InviteRepo.setStatus(clazz.id, userId, Invite.Status.Invited) >> sendInviteNotify(clazz, userId)

  def accept(clazz: Clazz, user: User): Funit =
    InviteRepo.setStatus(clazz.id, user.id, Invite.Status.Joined) >>
      coll.insert(Student.make(clazzId = clazz.id, userId = user.id)).void >>
      clazzColl.update($id(clazz.id), $addToSet("studentIds" -> user.id)) >>
      clazz.team.?? { teamId => teamApi.addMemberClazz(user.id, teamId, clazz.id) } >>
      acceptPublish(clazz, user) >>-
      mineClazzCache.invalidate(user.id)

  def refused(clazzId: String, user: User): Funit =
    InviteRepo.setStatus(clazzId, user.id, Invite.Status.Refused)

  def removeStudent(clazz: Clazz, userId: User.ID): Funit =
    coll.remove(selectId(clazz.id, userId)) >>
      InviteRepo.remove(Invite.makeId(clazz.id, userId)) >>
      clazzColl.update($id(clazz.id), $pull("studentIds" -> userId)) >>
      clazz.team.?? { teamId => teamApi.removeMemberClazz(userId, teamId, clazz.id) } >>
      removePublish(clazz.id, userId) >>-
      mineClazzCache.invalidate(userId)

  def studentsWithUser(clazz: Clazz, me: User): Fu[List[StudentWithUser]] =
    studentsWithUserById(clazz.id, me.id)

  def studentsById(clazzId: Clazz.ID): Fu[List[Student]] =
    coll.find(clazzQuery(clazzId))
      .sort($doc("createAt" -> 1))
      .list[Student]()

  def studentsWithUserById(clazzId: Clazz.ID, userId: User.ID): Fu[List[StudentWithUser]] =
    studentsById(clazzId).flatMap { students =>
      userMarks(userId) flatMap { markMap =>
        UserRepo.withColl {
          _.byOrderedIds[User, User.ID](students.map(_.userId))(_.id)
        } map { users =>
          students zip users collect {
            case (student, user) => {
              val mark = markMap.get(user.id) match {
                case None => none[String]
                case Some(m) => m
              }
              StudentWithUser(student, user, mark)
            }
          }
        }
      }
    }

  def clazzStudents(ids: List[User.ID], username: Option[String], markMap: Map[String, Option[String]], memberLevel: Option[String]): Fu[List[User]] = {
    val markUserIds =
      username.?? { txt =>
        markMap.filterValues { markOption =>
          markOption.?? { mark =>
            mark.toLowerCase.contains(txt.toLowerCase)
          }
        }.keySet
      }

    var $selector = $inIds(ids)
    username.foreach { u =>
      $selector = $selector ++ $or($doc("username" $regex (u, "i")), $doc("_id" $in markUserIds))
    }

    memberLevel.foreach { l =>
      $selector = $selector ++ $doc("member.code" -> l)
    }
    UserRepo.find($selector).list[User]().map(_.sortWith {
      case (u1, u2) => {
        if (u1.memberLevel.id > u2.memberLevel.id) true
        else if (u1.memberLevel.id < u2.memberLevel.id) false
        else u1.memberOrDefault.lvWithExpire.expireAt.isBefore(u2.memberOrDefault.lvWithExpire.expireAt)
      }
    })
  }

  def invitesWithUser(clazz: Clazz, me: User): Fu[List[InviteWithUser]] =
    InviteRepo.findByClazz(clazz.id) flatMap { invites =>
      userMarks(me.id) flatMap { markMap =>
        UserRepo.withColl {
          _.byOrderedIds[User, User.ID](invites.filterNot(_.isJoined).map(_.userId))(_.id)
        }.map { users =>
          invites.filterNot(_.isJoined) zip users collect {
            case (invite, user) => {
              val mark = markMap.get(user.id) match {
                case None => none[String]
                case Some(m) => m
              }
              InviteWithUser(invite, user, mark)
            }
          }
        }
      }
    }

  def addStudents(clazz: Clazz, userIds: List[String]): Funit = {
    val students = userIds.map(Student.make(clazz.id, _))
    coll.bulkInsert(
      documents = students.map(StudentBSONHandler.write).toStream,
      ordered = false
    ) >> clazzColl.update($id(clazz.id), $set("studentIds" -> (clazz.studentIds ++ userIds).toSet)) >>
      clazz.team.?? { teamId => teamApi.addMemberClazzs(userIds, teamId, clazz.id) } >>
      joinPublish(clazz, userIds)
  }

  def bulkInsert(students: List[Student]): Funit = coll.bulkInsert(
    documents = students.map(StudentBSONHandler.write).toStream,
    ordered = false
  ).void

  def page(clazzId: String, markMap: Map[String, Option[String]], page: Int, q: Option[String]): Fu[Paginator[StudentWithUser]] = {
    val filterMarkUserIds = q.?? { txt =>
      markMap.filterValues { markOption =>
        markOption.?? { mark =>
          mark.toLowerCase.contains(txt.toLowerCase)
        }
      }.keySet
    }

    var doc = clazzQuery(clazzId)
    q.foreach(q =>
      doc = doc ++ $or($doc("userId" $regex (q, "i")), $doc("userId" $in filterMarkUserIds)))

    val adapter = new Adapter[Student](
      collection = coll,
      selector = doc,
      projection = $empty,
      sort = $doc("createAt" -> 1)
    ) mapFutureList withUsers
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = lila.common.MaxPerPage(100)
    )
  }

  private def withUsers(students: Seq[Student]): Fu[Seq[StudentWithUser]] =
    UserRepo.withColl {
      _.byOrderedIds[User, User.ID](students.map(_.userId))(_.id)
    } map { users =>
      students zip users collect {
        case (coach, userId) => StudentWithUser(coach, userId, none)
      }
    }

  private def sendInviteNotify(clazz: Clazz, userId: User.ID): Funit = {
    val notificationContent = InvitedToClazz(
      InvitedToClazz.InvitedBy(clazz.coach),
      InvitedToClazz.ClazzName(clazz.name),
      InvitedToClazz.ClazzId(clazz._id)
    )
    notifyApi.addNotification(
      Notification.make(Sender(clazz.coach).some, Notification.Notifies(userId), notificationContent)
    )
  }

  private def acceptPublish(clazz: Clazz, user: User): Funit = {
    bus.publish(lila.hub.actorApi.clazz.ClazzJoinAccept(clazz.id, clazz.name, clazz.coach, user.id), 'clazzJoinAccept)
    Env.current.courseApi.clazzCourse(clazz.id).map { list =>
      val calendars = list.map { course =>
        makeCalendar(clazz, course, user)
      }
      bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(calendars), 'calendarCreateBus)
    }
  }

  private def joinPublish(clazz: Clazz, userIds: List[String]): Funit = {
    UserRepo.byIds(userIds) flatMap { users =>
      userIds.foreach { userId =>
        bus.publish(lila.hub.actorApi.clazz.ClazzJoinAccept(clazz.id, clazz.name, clazz.coach, userId), 'clazzJoinAccept)
      }
      Env.current.courseApi.clazzCourse(clazz.id).map { list =>
        val calendars = users.foldLeft(List.empty[CalendarCreate]) {
          case (cList, user) => {
            cList ++ list.map { course =>
              makeCalendar(clazz, course, user)
            }
          }
        }

        calendars.grouped(1000).foreach { splitCalendars =>
          bus.publish(lila.hub.actorApi.calendar.CalendarsCreate(splitCalendars), 'calendarCreateBus)
        }
      }
    }
  }

  private def removePublish(clazzId: Clazz.ID, userId: User.ID): Funit =
    Env.current.courseApi.clazzCourse(clazzId, true).map { list =>
      val ids = list.map { course => s"$clazzId@$userId@${course.index}" }
      bus.publish(lila.hub.actorApi.calendar.CalendarsRemove(ids), 'calendarRemoveBus)
    }

  private def makeCalendar(clazz: Clazz, course: Course, user: User) =
    lila.hub.actorApi.calendar.CalendarCreate(
      id = s"${clazz.id}@${user.id}@${course.index}".some,
      typ = "course",
      user = user.id,
      sdt = course.dateTime,
      edt = course.dateEndTime,
      content = s"${clazz.name} 第${course.index}节",
      onlySdt = false,
      link = s"/clazz/homework/show2?clazzId=${clazz.id}&courseId=${course.id}".some,
      icon = "写".some,
      bg = "#507803".some
    )

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  def userMark(userId: User.ID, relUserId: String): Fu[Option[String]] =
    (markActor ? GetMark(userId, relUserId)).mapTo[Option[String]]

}
