package lila.clazz

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class CourseAttend(
    _id: CourseAttend.ID,
    clazz: Clazz.ID,
    course: Course.ID,
    index: Int,
    user: User.ID,
    absent: Boolean,
    date: DateTime,
    createAt: DateTime
) {

  def id = _id

}

object CourseAttend {

  type ID = String

  def make(course: Course, user: User.ID, absent: Boolean) =
    CourseAttend(
      _id = Random nextString 8,
      clazz = course.clazz,
      course = course.id,
      index = course.index,
      user = user,
      absent = absent,
      date = course.dateTime,
      createAt = DateTime.now
    )

}
