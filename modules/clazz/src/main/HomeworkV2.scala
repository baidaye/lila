package lila.clazz

import lila.user.User
import org.joda.time.DateTime
import HomeworkV2._
import lila.task.TTask

case class HomeworkV2(
    _id: ID,
    clazzId: String,
    courseId: String,
    index: Int,
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    overDeadline: Option[Boolean],
    summary: Option[String],
    prepare: Option[String],
    common: Option[HomeworkV2TaskTpls],
    practice: Option[HomeworkV2TaskTpls],
    status: Status,
    name: Option[String],
    coinTeam: Option[String],
    coinRule: Option[Int],
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID,
    cloneFrom: Option[HomeworkV2.ID]
) {

  def id = _id

  def isCreated = status == Status.Created
  def isPublished = status == Status.Published

  def available = deadlineAt.??(_.isAfter(DateTime.now))

  def isDeadline = !available

  def deadline = deadlineAt.map(_.toString("yyyy-MM-dd HH:mm"))

  def taskTplIds = common.??(_.taskTpls.map(_.taskTplId)) ++ practice.??(_.taskTpls.map(_.taskTplId))

  def hasCoin = coinRule.??(_ > 0)

  def hasCoinZero = coinRule.??(_ == 0)

  def hasCommon = common.??(_.taskTpls.nonEmpty)

  def hasPractice = practice.??(_.taskTpls.nonEmpty)

  def hasContent = summary.isDefined || prepare.isDefined || hasCommon || hasPractice

}

object HomeworkV2 {

  type ID = String

  def cloneBy(source: HomeworkV2, clazz: Clazz, course: Course, userId: User.ID) = HomeworkV2(
    _id = makeId(clazz.id, course.id, course.index),
    clazzId = clazz.id,
    courseId = course.id,
    index = course.index,
    startAt = course.dateEndTime.some,
    deadlineAt = course.dateEndTime.plusWeeks(1).some,
    overDeadline = Some(false),
    summary = source.summary,
    prepare = source.prepare,
    common = None,
    practice = None,
    status = Status.Created,
    name = makeName(clazz.name, course.index).some,
    coinTeam = source.coinTeam,
    coinRule = source.coinRule,
    createdAt = DateTime.now,
    updatedAt = DateTime.now,
    createdBy = userId,
    updatedBy = userId,
    cloneFrom = source.id.some
  )

  def empty(
    clazzId: String,
    clazzName: String,
    courseId: String,
    index: Int,
    startAt: Option[DateTime],
    userId: User.ID,
    cloneFrom: Option[HomeworkV2.ID] = None
  ) = make(
    clazzId = clazzId,
    courseId = courseId,
    index = index,
    startAt = startAt,
    deadlineAt = None,
    summary = None,
    prepare = None,
    common = None,
    practice = None,
    name = makeName(clazzName, index).some,
    coinTeam = None,
    coinRule = None,
    userId = userId,
    cloneFrom = cloneFrom
  )

  def make(
    clazzId: String,
    courseId: String,
    index: Int,
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    summary: Option[String],
    prepare: Option[String],
    common: Option[HomeworkV2TaskTpls],
    practice: Option[HomeworkV2TaskTpls],
    name: Option[String],
    coinTeam: Option[String],
    coinRule: Option[Int],
    userId: User.ID,
    cloneFrom: Option[HomeworkV2.ID] = None
  ) = HomeworkV2(
    _id = makeId(clazzId, courseId, index),
    clazzId = clazzId,
    courseId = courseId,
    index = index,
    startAt = startAt,
    deadlineAt = deadlineAt,
    overDeadline = Some(false),
    summary = summary,
    prepare = prepare,
    common = common,
    practice = practice,
    status = Status.Created,
    name = name,
    coinTeam = coinTeam,
    coinRule = coinRule,
    createdAt = DateTime.now,
    updatedAt = DateTime.now,
    createdBy = userId,
    updatedBy = userId,
    cloneFrom = cloneFrom
  )

  def makeId(clazzId: String, courseId: String, index: Int) = s"$clazzId@$courseId@$index"

  def makeName(clazzName: String, index: Int) = s"$clazzName 第${index}次课 课后练"

  sealed abstract class Status(val id: Int, val name: String) extends Ordered[Status] {
    def compare(other: Status) = Integer.compare(id, other.id)
    def is(s: Status): Boolean = this == s
    def is(f: Status.type => Status): Boolean = is(f(Status))
  }
  object Status {
    case object Created extends Status(10, "准备中")
    case object Published extends Status(20, "已发布")

    val all = List(Created, Published)
    val byId = all map { v => (v.id, v) } toMap
    def apply(id: Int): Status = byId get id err s"Bad Status $id"
  }

  case class WithTask(homework: HomeworkV2, common: Option[HomeworkV2RealTaskTpls], practice: Option[HomeworkV2RealTaskTpls]) {

    def id = homework.id

    def hasCommon = homework.hasCommon

    def hasPractice = homework.hasPractice

    def commonOrEmpty = common.??(_.taskTpls)

    def practiceOrEmpty = practice.??(_.taskTpls)

    def allTaskTpls = commonOrEmpty ++ practiceOrEmpty

    def toStudentRealTasks(startAt: Option[DateTime], studentId: User.ID): List[TTask] =
      toStudentRealTasks(startAt, common, studentId) ++ toStudentRealTasks(startAt, practice, studentId)

    def toStudentRealTasks(startAt: Option[DateTime], homeworkV2RealTaskTplsOpt: Option[HomeworkV2RealTaskTpls], studentId: User.ID): List[TTask] =
      homeworkV2RealTaskTplsOpt.?? { homeworkV2RealTaskTpls =>
        homeworkV2RealTaskTpls.taskTpls.map { homeworkV2RealTaskTpl =>
          val taskTpl = homeworkV2RealTaskTpl.taskTpl
          val now = DateTime.now
          TTask.makeByTemplate(
            taskTemplate = taskTpl,
            userId = studentId,
            sourceId = taskTpl.sourceRel.id,
            sourceName = taskTpl.sourceRel.name,
            coinTeam = taskTpl.coinTeam,
            coinRule = taskTpl.coinRule,
            planAt = None,
            startAt = startAt,
            deadlineAt = taskTpl.deadlineAt,
            createdBy = taskTpl.createdBy
          )
        }
      }
  }

}

