package lila.clazz

import lila.db.dsl._
import lila.team.TeamApi
import lila.user.{ User, UserRepo }
import org.joda.time.DateTime
import reactivemongo.bson.BSONDocument
import lila.hub.actorApi.relation.{ GetMark, GetMarks }
import akka.actor.ActorSelection
import makeTimeout.large
import akka.pattern.ask
import reactivemongo.api.ReadPreference
import scala.concurrent.duration._

final class ClazzApi(
    coll: Coll,
    teamApi: TeamApi,
    markActor: ActorSelection,
    bus: lila.common.Bus,
    asyncCache: lila.memo.AsyncCache.Builder
) {

  import BSONHandlers.clazzHandler

  val unDeleted = $or("deleted" $exists false, $doc("deleted" -> false))

  def $mine(userId: User.ID) = $or(
    $doc("coach" -> userId),
    $doc("studentIds" -> userId)
  )

  val $current = $doc("stopped" -> false)

  val $history = $doc("stopped" -> true)

  def current(user: User): Fu[List[Clazz.ClazzWithTeam]] =
    list(user.id, $current)

  def history(user: User): Fu[List[Clazz.ClazzWithTeam]] =
    list(user.id, $history)

  def mine(userId: User.ID): Fu[List[Clazz]] =
    coll.find(
      $mine(userId) ++ $current ++ unDeleted
    ).sort($sort desc "createdAt")
      .list[Clazz]()

  def coachClazzs(userId: User.ID): Fu[List[Clazz]] =
    coll.list[Clazz](
      $doc("coach" -> userId) ++ unDeleted
    )

  def list(userId: User.ID, dateCondition: BSONDocument): Fu[List[Clazz.ClazzWithTeam]] =
    coll.find(
      $mine(userId) ++ dateCondition ++ unDeleted
    ).sort($sort desc "createdAt").list[Clazz]() flatMap { clazzs =>
        val teamIds = clazzs.map(_.team).filter(_.isDefined).map(_.get)
        teamApi.campusWithTeams(teamIds).map { teams =>
          clazzs.map { clazz =>
            val teamOption = teams.find(t => t.campusId == clazz.teamOrDefault)
            Clazz.ClazzWithTeam(clazz, teamOption)
          }
        }
      }

  def byCourseList(userId: User.ID): Fu[List[Clazz]] =
    coll.find($doc("coach" -> userId) ++ unDeleted).list()

  def byIdWithCoach(id: Clazz.ID, me: User): Fu[Option[Clazz.ClazzWithCoach]] =
    for {
      clazzOption <- coll.byId[Clazz](id)
      coachOption <- clazzOption.?? { c => UserRepo.byId(c.coach) }
      markOption <- clazzOption.?? { c => userMark(me.id, c.coach) }
    } yield {
      (clazzOption |@| coachOption).tupled.map {
        case (clazz, coach) => Clazz.ClazzWithCoach(clazz, coach, markOption)
      }
    }

  def byId(id: Clazz.ID): Fu[Option[Clazz]] = coll.byId[Clazz](id)

  def byIds(ids: List[Clazz.ID]): Fu[List[Clazz]] = coll.byIds[Clazz](ids)

  def teamClazz(ids: List[String], campusId: Option[String], coach: Option[String], me: User.ID): Fu[List[Clazz.ClazzWithCoach]] = {
    for {
      clazzes <- byIds(ids).map(_.filter(!_.stopped))
      filteredClazzes = campusId.fold(clazzes.filter(c => coach.isEmpty || coach.has(c.coach))) { cid => clazzes.filter(c => c.team.has(cid) && (coach.isEmpty || coach.has(c.coach))) }
      users <- UserRepo.byOrderedIds(filteredClazzes.map(_.coach), ReadPreference.secondaryPreferred)
      markMap <- userMarks(me)
    } yield {
      filteredClazzes zip users collect {
        case (clazz, user) => {
          val mark = markMap.get(clazz.coach) match {
            case None => none[String]
            case Some(m) => m
          }
          Clazz.ClazzWithCoach(clazz, user, mark, clazz.toCourse(clazz.id, clazz.coach))
        }
      }
    }
  }

  def byTeam(campusId: String, coach: Option[String]): Fu[List[Clazz]] =
    coll.find(
      $doc("team" -> campusId) ++ coach.??(c => $doc("coach" -> c)) ++ $current ++ unDeleted
    ).sort($sort asc "createdAt").list()

  def byTeams(campusIds: List[String]): Fu[List[Clazz]] =
    coll.find(
      $doc("team" $in campusIds) ++ $current ++ unDeleted
    ).sort($sort asc "createdAt").list()

  def byTeams(campusIds: List[String], coach: String): Fu[List[Clazz]] =
    coll.find(
      $doc("team" $in campusIds, "coach" -> coach) ++ $current ++ unDeleted
    ).sort($sort asc "createdAt").list()

  def create(clazz: Clazz): Funit =
    coll.insert(clazz).void >> {
      val courseList = clazz.toCourse(clazz.id, clazz.coach)
      Env.current.courseApi.bulkInsert(courseList)
    } >> clazz.team.?? { campusId =>
      teamApi.addClazz(campusId, clazz.id)
    } >>- Env.current.courseApi.coachCalendarPublish(clazz)

  def nameExists(user: User, name: String, id: Option[Clazz.ID]): Fu[Boolean] =
    coll exists $doc("name" -> name, "coach" -> user.id) ++ id.fold($doc()) { _id =>
      $doc("_id" $ne _id)
    }

  def update(old: Clazz, clazz: Clazz): Funit = {
    val courseList = clazz.toCourse(clazz.id, clazz.coach)
    coll.update($id(clazz.id), clazz).void >>
      Env.current.courseApi.bulkUpdate(clazz.id, courseList) >>
      old.team.?? { campusId => teamApi.removeClazz(campusId, clazz.id) } >>
      clazz.team.?? { campusId => teamApi.addClazz(campusId, clazz.id) } >>-
      Env.current.courseApi.resetCoachCalendarPublish(clazz)
  }

  def updateSetting(clazz: Clazz): Funit = coll.update($id(clazz.id), clazz).void

  def updateCoach(clazz: Clazz, newCoach: String): Funit = {
    coll.update($id(clazz.id), $set("coach" -> newCoach)).void >>
      clazz.team.?? { campusId =>
        val teamId = lila.team.Campus.toTeamId(campusId)
        lila.team.CampusRepo.byTeam(teamId) flatMap { campus =>
          val campusIds = campus.map(_.id)
          byTeams(campusIds, clazz.coach) map { clazzs =>
            val otherClazzStudentIds = clazzs.filter(_.id != clazz.id).flatMap(_.studentIds)
            // 当前班级学员不在任教的其它班级内
            val excludeIds = clazz.studentIds.filter(!otherClazzStudentIds.contains(_))
            bus.publish(lila.hub.actorApi.clazz.ChangeCoach(clazz.id, clazz.coach, newCoach, clazz.studentIds, excludeIds), 'clazzChangeCoach)
            Env.current.courseApi.removeCoachCalendarPublish(clazz)
            Env.current.courseApi.coachCalendarPublish(clazz.copy(coach = newCoach))
          }
        }
      }
  }

  def updateWeekCourseLastDate(id: Clazz.ID, lastCourseTime: DateTime): Funit =
    coll.update(
      $id(id),
      $set("weekClazz.dateEnd" -> lastCourseTime, "updatedAt" -> DateTime.now)
    ).void

  import BSONHandlers.weekClassHandler
  def updateWeekClazz(id: Clazz.ID, weekClazz: WeekClazz): Funit =
    coll.update(
      $id(id),
      $set(
        "weekClazz" -> weekClassHandler.write(weekClazz),
        "updatedAt" -> DateTime.now
      )
    ).void

  def stop(id: String): Funit = coll.update(
    $id(id),
    $set("stopped" -> true)
  ).void

  def delete(clazz: Clazz): Funit = coll.update(
    $id(clazz.id),
    $set("deleted" -> true)
  ) >>-
    Env.current.courseApi.removeCalendarsByClazz(clazz) >>
    Env.current.courseApi.deleteByClazz(clazz.id) >>
    clazz.team.?? { campusId =>
      teamApi.removeClazz(campusId, clazz.id)
    }

  def mineClazzIds(userId: User.ID): Fu[List[String]] =
    coll.distinct[String, List](
      "_id",
      ($mine(userId) ++ unDeleted).some
    ).map(_.toList)

  def belongTo(clazzId: String, userId: String): Fu[Boolean] =
    coll.exists($id(clazzId) ++ $mine(userId))

  def myTeamClazzIds(userId: String, teamId: String): Fu[List[String]] =
    coll.distinct[String, List](
      "_id",
      ($doc("studentIds" -> userId) ++ unDeleted).some
    ).map(_.toList)

  def setContest(clazz: Clazz, contestId: String): Funit =
    coll.update(
      $id(clazz.id),
      $addToSet("contestIds" -> contestId)
    ).void

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  def userMark(userId: User.ID, relUserId: String): Fu[Option[String]] =
    (markActor ? GetMark(userId, relUserId)).mapTo[Option[String]]

}
