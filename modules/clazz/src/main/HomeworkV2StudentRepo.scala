package lila.clazz

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime

object HomeworkV2StudentRepo {

  import HomeworkV2BSONHandlers._

  private[clazz] lazy val coll = Env.current.homeworkV2StudentColl

  def byId(id: String): Fu[Option[HomeworkV2Student]] = coll.byId[HomeworkV2Student](id)

  def mineOfClazz(clazzId: Clazz.ID, userId: User.ID): Fu[List[HomeworkV2Student]] =
    coll.find(
      $doc(
        "clazzId" -> clazzId,
        "studentId" -> userId
      )
    ).list[HomeworkV2Student]()

  def findByCourse(clazzId: Clazz.ID, courseId: Course.ID): Fu[List[HomeworkV2Student]] =
    coll.find(
      $doc(
        "clazzId" -> clazzId,
        "courseId" -> courseId
      )
    ).list[HomeworkV2Student]()

  def fullInfo(id: HomeworkV2Student.ID): Fu[Option[HomeworkV2StudentFullInfo]] =
    for {
      homeworkOption <- byId(id)
      clazzOption <- homeworkOption.??(h => Env.current.api.byId(h.clazzId))
      courseOption <- homeworkOption.??(h => Env.current.courseApi.byId(h.courseId))
    } yield (homeworkOption |@| clazzOption |@| courseOption).apply {
      case (homework, clazz, course) => HomeworkV2StudentFullInfo(homework, clazz, course)
    }

  def insert(homework: HomeworkV2Student): Funit =
    coll.insert(homework).void

  def bulkInsert(homeworks: List[HomeworkV2Student]) =
    coll.bulkInsert(
      documents = homeworks.map(HomeworkV2StudentHandler.write).toStream,
      ordered = false
    ).void

  def update(homework: HomeworkV2Student): Funit =
    coll.update($id(homework.id), homework).void

  def setDeadline(clazzId: Clazz.ID, courseId: Course.ID, deadline: DateTime): Funit =
    coll.update(
      $doc("clazzId" -> clazzId, "courseId" -> courseId),
      $set("deadlineAt" -> deadline),
      multi = true
    ).void

  def setStartAt(clazzId: Clazz.ID, courseId: Course.ID, startAt: DateTime): Funit =
    coll.update(
      $doc("clazzId" -> clazzId, "courseId" -> courseId),
      $set("startAt" -> startAt),
      multi = true
    ).void

  def setProgress(id: HomeworkV2Student.ID, progress: Double): Funit =
    coll.update(
      $id(id),
      $set("progress" -> progress)
    ).void

  def setCoinDiff(id: HomeworkV2Student.ID, coinDiff: Int): Funit =
    coll.update(
      $id(id),
      $set("coinDiff" -> coinDiff)
    ).void

  def setOverDeadline(clazzId: Clazz.ID, courseId: Course.ID): Funit =
    coll.update(
      $doc("clazzId" -> clazzId, "courseId" -> courseId),
      $set("overDeadline" -> true),
      multi = true
    ).void

  def removeByCourse(clazzId: Clazz.ID, courseId: Course.ID): Funit =
    coll.remove($doc("clazzId" -> clazzId, "courseId" -> courseId)).void

}
