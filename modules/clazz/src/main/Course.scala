package lila.clazz

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import org.joda.time.format.DateTimeFormat

case class Course(
    _id: Course.ID,
    date: DateTime,
    timeBegin: String,
    timeEnd: String,
    week: Int,
    clazz: String,
    coach: String, // 这个属性标识的是课节的创建教练，改版后请忽略这个属性
    index: Int,
    stopped: Boolean,
    enabled: Boolean,
    homework: Boolean,
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  val dateTimeStr = date.toString("yyyy-MM-dd") + " " + timeBegin

  val dateTime = Course.dateTimeFormatter.parseDateTime(dateTimeStr)

  val dateEndTime = Course.dateTimeFormatter.parseDateTime(date.toString("yyyy-MM-dd") + " " + timeEnd)

  def editable = dateTime.isAfterNow

  def isCreator(user: String) = user == createdBy

  def weekFormat =
    "周".concat(
      week match {
        case 1 => "一"
        case 2 => "二"
        case 3 => "三"
        case 4 => "四"
        case 5 => "五"
        case 6 => "六"
        case 7 => "日"
      }
    )

  def period = {
    if (timeBegin < "12:00") "上午"
    else if (timeBegin >= "12:00" && timeBegin < "18:00") "下午"
    else if (timeBegin >= "18:00") "晚上"
    else "-"
  }

  def courseFormatTime = s"${date.toString("yyyy年MM月dd")}（$weekFormat）$timeBegin"

  override def toString: String = s"clazz: $clazz, course: $id"
}

object Course {

  type ID = String

  def genId = Random nextString 8

  val dateTimeFormatter = DateTimeFormat forPattern "yyyy-MM-dd HH:mm"

  case class WithClazz(course: Course, clazz: Clazz)

}

case class OlClassMetaData(courseId: Course.ID, status: OlClassStatus, opened: Boolean)
sealed abstract class OlClassStatus(val id: String, val name: String)
object OlClassStatus {
  case object Created extends OlClassStatus("created", "未开课")
  case object Started extends OlClassStatus("started", "上课中")
  case object Stopped extends OlClassStatus("stopped", "已结束")

  val all = List(Created, Started, Stopped)

  def apply(id: String) = all.find(_.id == id) err s"Bad Status $id"

  def keySet = all.map(_.id).toSet

  def list = all.map { r => r.id -> r.name }

  def byId = all.map { x => x.id -> x }.toMap
}
case class CourseRelation(course: Course, olClass: Option[OlClassMetaData], homework: Option[HomeworkV2], shwt: Option[HomeworkV2Student.WithTask], attend: Option[CourseAttend])
