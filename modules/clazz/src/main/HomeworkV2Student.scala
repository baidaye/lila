package lila.clazz

import lila.user.User
import org.joda.time.DateTime
import HomeworkV2Student._

case class HomeworkV2Student(
    _id: ID,
    clazzId: String,
    courseId: String,
    index: Int,
    studentId: User.ID,
    startAt: Option[DateTime],
    deadlineAt: DateTime,
    overDeadline: Option[Boolean],
    summary: Option[String],
    prepare: Option[String],
    comment: Option[String],
    common: Option[HomeworkV2Tasks],
    practice: Option[HomeworkV2Tasks],
    name: Option[String],
    coinTeam: Option[String],
    coinRule: Option[Int],
    coinDiff: Option[Int],
    progress: Option[Double],
    createdAt: DateTime,
    createdBy: User.ID
) {

  def id: ID = _id

  def homeworkId = s"$clazzId@$courseId@$index"

  def isStarted = startAt.isEmpty || startAt.??(_.isBeforeNow)

  def isDeadlined = deadlineAt.isBeforeNow

  def available = isStarted && !isDeadlined

  def hasCoin = coinRule.??(_ > 0)

  def hasCoinZero = coinRule.??(_ == 0)

  def coinDiffOrZero = coinDiff | 0

  def deadline = deadlineAt.toString("yyyy-MM-dd HH:mm")

  def isCreator(userId: User.ID) = userId == createdBy

  def belongTo(userId: User.ID) = userId == studentId

  def taskIds = common.??(_.tasks.map(_.taskId)) ++ practice.??(_.tasks.map(_.taskId))

  def nameOrDefault = name | "课后练"

}

object HomeworkV2Student {

  type ID = String

  def makeId(homeworkId: HomeworkV2.ID, userId: User.ID) = s"$homeworkId@$userId"

  def byHomework(homework: HomeworkV2, common: Option[HomeworkV2Tasks], practice: Option[HomeworkV2Tasks], userId: User.ID) =
    HomeworkV2Student(
      _id = makeId(homework.id, userId),
      clazzId = homework.clazzId,
      courseId = homework.courseId,
      index = homework.index,
      studentId = userId,
      startAt = homework.startAt,
      deadlineAt = homework.deadlineAt err s"cant find deadline of ${homework.id}",
      overDeadline = None,
      summary = homework.summary,
      prepare = homework.prepare,
      comment = None,
      common = common,
      practice = practice,
      name = homework.name,
      coinTeam = homework.coinTeam,
      coinRule = homework.coinRule,
      coinDiff = None,
      progress = None,
      createdAt = DateTime.now,
      createdBy = homework.createdBy
    )

  case class WithClazz(homework: HomeworkV2Student, clazz: Clazz)

  case class WithTask(homework: HomeworkV2Student, common: Option[HomeworkV2RealTasks], practice: Option[HomeworkV2RealTasks]) {

    def id = homework.id

    def studentId = homework.studentId

    def commonOrEmpty = common.??(_.tasks)

    def practiceOrEmpty = practice.??(_.tasks)

    def allTask = common.??(_.tasks.map(_.task)) ++ practice.??(_.tasks.map(_.task))

    def oneTaskRate = 1.0D / allTask.size.toDouble

    def progress = Math.round(
      allTask.foldLeft(0.0D) {
        case (pg, task) => pg + task.progress * oneTaskRate
      } * 100
    ) / 100.0D

    def progressFormat = s"$progress%"

    def isFinished = progress > 99.0D

    def coinDiff = homework.coinRule match {
      case Some(cr) => Math.floor(cr * (progress / 100D)).toInt.some
      case None => none[Int]
    }

    def isDeadlined = homework.isDeadlined

  }

}

case class HomeworkV2StudentFullInfo(homework: HomeworkV2Student, clazz: Clazz, course: Course)
