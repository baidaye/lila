package lila.clazz

import lila.user.User
import org.joda.time.DateTime

case class Student(
    _id: String,
    clazzId: String,
    userId: String,
    createAt: DateTime
) {

  def id = _id

}

object Student {

  def makeId(clazzId: String, userId: String) = userId + "@" + clazzId

  def make(clazzId: String, userId: String) = Student(
    _id = makeId(clazzId, userId),
    clazzId = clazzId,
    userId = userId,
    createAt = DateTime.now()
  )

}

case class StudentWithUser(student: Student, user: User, mark: Option[String]) {
  def clazzId = student.clazzId
  def userId = user.id
  def realName = mark | user.realNameOrUsername
  def levelLabel = user.profileOrDefault.currentLevel.label
  def json = s"""{"id":"${user.id}","name": "$realName"}"""

}
