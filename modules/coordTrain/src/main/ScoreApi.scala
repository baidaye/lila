package lila.coordTrain

import lila.coordTrain.DataForm.ScoreData
import lila.db.dsl._
import reactivemongo.bson._

final class ScoreApi(coll: Coll, bus: lila.common.Bus) {

  private implicit val ModeBSONHandler = new BSONHandler[BSONString, Score.Mode] {
    def read(b: BSONString) = Score.Mode.byId get b.value err s"Invalid Mode ${b.value}"
    def write(m: Score.Mode) = BSONString(m.id)
  }
  private implicit val scoreBSONHandler = Macros.handler[Score]

  def getScore(userId: String, mode: Score.Mode): Fu[Score] =
    coll.byId[Score](Score.makeId(userId, mode)) map (_ | Score.empty(userId, mode))

  def addScore(userId: String, mode: Score.Mode, white: Boolean, hits: Int): Funit =
    coll.update(
      $doc("_id" -> Score.makeId(userId, mode)),
      $set("userId" -> userId, "mode" -> mode.id) ++ $doc(
        "$push" -> $doc(
          "white" -> $doc(
            "$each" -> (white ?? List(BSONInteger(hits))),
            "$slice" -> -20
          ),
          "black" -> $doc(
            "$each" -> (!white ?? List(BSONInteger(hits))),
            "$slice" -> -20
          )
        )
      ),
      upsert = true
    ).void

  def publish(userId: String, data: ScoreData) =
    bus.publish(lila.hub.actorApi.coordTrain.TrainData(userId, data.mode, data.colorV, data.coord, data.limit, data.score), 'coordTrainFinish)
}
