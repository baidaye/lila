package lila.coordTrain

import play.api.data._
import play.api.data.Forms._

object DataForm {

  val start = Form(mapping(
    "mode" -> text.verifying(Score.Mode.keys.contains _),
    "color" -> number(min = 1, max = 3),
    "coord" -> number(min = 0, max = 2),
    "limit" -> number(min = 0, max = 1)
  )(StartData.apply)(StartData.unapply))

  val score = Form(mapping(
    "mode" -> text.verifying(Score.Mode.keys.contains _),
    "colorV" -> number(min = 1, max = 3),
    "colorC" -> text.verifying(Set("white", "black") contains _),
    "coord" -> number(min = 0, max = 2),
    "limit" -> number(min = 0, max = 1),
    "score" -> number(min = 0, max = 10000)
  )(ScoreData.apply)(ScoreData.unapply))

  case class ScoreData(mode: String, colorV: Int, colorC: String, coord: Int, limit: Int, score: Int) {
    def isWhite = colorC == "white"
    def realMode = Score.Mode(mode)
    def isLimit = limit == 1
  }

  case class StartData(mode: String, color: Int, coord: Int, limit: Int) {
    def realMode = Score.Mode(mode)
  }

}
