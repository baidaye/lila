package lila.coordTrain

case class Score(
    _id: String,
    userId: String,
    mode: Score.Mode,
    white: List[Int] = Nil,
    black: List[Int] = Nil
)

object Score {

  def makeId(userId: String, mode: Mode) = userId + "@" + mode.id

  def empty(userId: String, mode: Mode): Score = new Score(makeId(userId, mode), userId, mode)

  sealed abstract class Mode(val id: String, val name: String)
  object Mode {

    case object Basic extends Mode(id = "basic", name = "坐标训练")
    case object Move extends Mode(id = "move", name = "着法训练")
    case object Combat extends Mode(id = "combat", name = "实战着发")

    val default = Basic

    val all = List(Basic, Move, Combat)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Mode = byId.get(id) err s"Bad Mode $this"

  }
}