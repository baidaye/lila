package lila.coordTrain

import com.typesafe.config.Config

final class Env(
    config: Config,
    db: lila.db.Env,
    system: akka.actor.ActorSystem
) {

  private val CollectionScore = config getString "collection.score"
  private val CollectionMove = config getString "collection.move"
  private val CollectionCombat = config getString "collection.combat"
  private val AnimationDuration = config duration "animation.duration"

  lazy val scoreApi = new ScoreApi(db(CollectionScore), system.lilaBus)
  lazy val moveApi = new MoveApi(db(CollectionMove))
  lazy val combatApi = new CombatApi(db(CollectionCombat))

  lazy val jsonView = new JsonView(
    animationDuration = AnimationDuration
  )

  lazy val forms = DataForm
}

object Env {

  lazy val current: Env = "coordTrain" boot new Env(
    config = lila.common.PlayApp loadConfig "coordTrain",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system
  )
}
