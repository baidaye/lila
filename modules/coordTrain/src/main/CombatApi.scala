package lila.coordTrain

import lila.db.dsl._
import reactivemongo.bson._
import scala.util.Random
import Combat.{ BSONFields => F }

final class CombatApi(coll: Coll) {

  import Combat.CombatBSONHandler

  val enabled = $doc(F.enabled -> true)

  val projection = $doc(F.taggers -> 0, F.likers -> 0)

  val notMachine = $doc(F.id $lt 1000000)

  // 兵升变：101846
  // 最后一步是对方：125146
  def fetchNext(color: String, prevId: Option[Int]): Fu[Combat] =
    //coll.byId[Combat](125146) flattenWith NoCombatAvailableException
    coll
      .find(notMachine ++ enabled ++ $doc(F.rating $lte 1500, F.depth $lte 3, F.white -> (color == "white"), F.retry -> false) ++ prevId.?? { id => $doc(F.id $gt id) }, projection)
      .hint($doc(F.id -> 1))
      .sort($sort asc F.id)
      .skip(Random nextInt prevId.fold(1000)(_ => 200))
      .uno[Combat] flattenWith NoCombatAvailableException
}

case object NoCombatAvailableException extends lila.base.LilaException {
  val message = "No Combat available"
}