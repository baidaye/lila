package lila.coordTrain

import chess.Color
import scala.collection.breakOut
import chess.format.{ Forsyth, Uci }

// from puzzle
case class Combat(
    id: Int,
    gameId: String,
    history: List[String],
    fen: String,
    lines: List[Line],
    depth: Int,
    color: Color
) {

  def fenAfterInitialMove: Option[String] = {
    for {
      sit1 <- Forsyth << fen
      sit2 <- sit1.move(initialMove).toOption.map(_.situationAfter)
    } yield Forsyth >> sit2
  }

  def initialMove: Uci.Move = history.lastOption flatMap Uci.Move.apply err s"Bad initial move $this"

}

object Combat {

  object BSONFields {
    val id = "_id"
    val gameId = "gameId"
    val history = "history"
    val fen = "fen"
    val lines = "lines"
    val depth = "depth"
    val retry = "retry"
    val white = "white"
    val perf = "perf"
    val rating = s"$perf.gl.r"
    val enabled = "enabled"
    val likers = "likers"
    val taggers = "taggers"
  }

  import reactivemongo.bson._
  import lila.db.BSON
  import BSON.BSONJodaDateTimeHandler
  private implicit val lineBSONHandler = new BSONHandler[BSONDocument, List[Line]] {
    private def readMove(move: String) = chess.Pos.doublePiotrToKey(move take 2) match {
      case Some(m) => s"$m${move drop 2}"
      case _ => sys error s"Invalid piotr move notation: $move"
    }
    def read(doc: BSONDocument): List[Line] = doc.elements.map {
      case BSONElement(move, BSONBoolean(true)) => Win(readMove(move))

      case BSONElement(move, BSONBoolean(false)) => Retry(readMove(move))

      case BSONElement(move, more: BSONDocument) =>
        Node(readMove(move), read(more))

      case BSONElement(move, value) =>
        throw new Exception(s"Can't read value of $move: $value")
    }(breakOut)
    private def writeMove(move: String) = chess.Pos.doubleKeyToPiotr(move take 4) match {
      case Some(m) => s"$m${move drop 4}"
      case _ => sys error s"Invalid move notation: $move"
    }
    def write(lines: List[Line]): BSONDocument = BSONDocument(lines map {
      case Win(move) => writeMove(move) -> BSONBoolean(true)
      case Retry(move) => writeMove(move) -> BSONBoolean(false)
      case Node(move, lines) => writeMove(move) -> write(lines)
    })
  }

  implicit val CombatBSONHandler = new BSON[Combat] {

    import BSONFields._

    def reads(r: BSON.Reader): Combat = Combat(
      id = r int id,
      gameId = r str gameId,
      history = r str history split ' ' toList,
      fen = r str fen,
      lines = r.get[List[Line]](lines),
      depth = r int depth,
      color = Color(r bool white)
    )

    def writes(w: BSON.Writer, o: Combat) = BSONDocument()
  }
}
