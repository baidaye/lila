package lila.coordTrain

import play.api.libs.json._

final class JsonView(animationDuration: scala.concurrent.duration.Duration) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "rookCastle" -> p.rookCastle,
    "animation" -> Json.obj(
      "duration" -> p.animationFactor * animationDuration.toMillis
    ),
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d,
    "coordTrain" -> Json.obj(
      "mode" -> p.coordTrain.mode,
      "coord" -> p.coordTrain.coord,
      "color" -> p.coordTrain.color,
      "limit" -> p.coordTrain.limit
    )
  )

  def scores(s: Score) = Json.obj(
    "white" -> s.white,
    "black" -> s.black
  )

  def move(m: Move) = Json.obj(
    "piece" -> m.piece,
    "color" -> m.color,
    "fr" -> m.fr,
    "to" -> m.to,
    "uci" -> m.uci,
    "san" -> m.san,
    "fen" -> m.fen
  )

  def combat(c: Combat) = {
    Json.obj(
      "id" -> c.id,
      "fen" -> c.fen,
      "depth" -> c.depth,
      "uci" -> c.initialMove.uci,
      "color" -> c.color.name,
      "lines" -> Line.toJson(c.lines)
    )
  }

}

