package lila.coordTrain

import scala.util.Random

case class Move(
    _id: String,
    piece: String,
    color: String,
    fr: String,
    to: String,
    uci: String,
    san: String,
    fen: String,
    len: Int
) {

  def id = _id

}

object Move {

  // 后2，车2，马2 ，象2，王1，兵1
  val pieces = Array("q", "q", "r", "r", "n", "n", "b", "b", "k", "p")

  def randomPiece = pieces(Random.nextInt(pieces.length))

}
