package lila.coordTrain

import play.api.libs.json._

sealed trait Line
case class Node(move: String, lines: List[Line]) extends Line
case class Win(move: String) extends Line
case class Retry(move: String) extends Line

object Line {

  def toJson(lines: List[Line]): JsObject = JsObject(lines map {
    case Win(move) => move -> JsBoolean(true)
    case Retry(move) => move -> JsBoolean(false)
    case Node(move, more) => move -> toJson(more)
  })

}
