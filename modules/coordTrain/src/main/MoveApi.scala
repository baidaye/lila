package lila.coordTrain

import lila.db.dsl._
import reactivemongo.bson._
import scala.util.Random

final class MoveApi(coll: Coll) {

  private implicit val moveBSONHandler = Macros.handler[Move]

  def fetchNext(color: String): Fu[Move] = {
    val $selector = $doc("piece" -> Move.randomPiece, "color" -> color)
    coll.countSel($selector).flatMap { count =>
      coll.find($selector)
        .sort($sort asc "_id")
        .skip(Random.nextInt(count))
        .uno[Move] flattenWith NoMovesAvailableException
    }
  }

}

case object NoMovesAvailableException extends lila.base.LilaException {
  val message = "No moves available"
}
