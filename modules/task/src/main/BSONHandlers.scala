package lila.task

import lila.db.BSON
import reactivemongo.bson._
import lila.db.dsl._
import chess.Clock.{ Config => ClockConfig }
import chess.variant.Variant

object BSONHandlers {

  implicit val TTaskSourceBSONHandler = new BSONHandler[BSONString, TTask.Source] {
    def read(b: BSONString) = TTask.Source(b.value)
    def write(x: TTask.Source) = BSONString(x.id)
  }

  implicit val TTaskSourceRelHandler = new BSON[TTask.SourceRel] {
    def reads(r: BSON.Reader) = TTask.SourceRel(
      id = r str "id",
      name = r str "name",
      source = r.get[TTask.Source]("source")
    )

    def writes(w: BSON.Writer, c: TTask.SourceRel) = $doc(
      "id" -> c.id,
      "name" -> c.name,
      "source" -> c.source
    )
  }

  implicit val TTaskStatusBSONHandler = new BSONHandler[BSONString, TTask.Status] {
    def read(b: BSONString) = TTask.Status(b.value)
    def write(x: TTask.Status) = BSONString(x.id)
  }

  implicit val TTaskItemTypeBSONHandler = new BSONHandler[BSONString, TTask.TTaskItemType] {
    def read(b: BSONString) = TTask.TTaskItemType(b.value)
    def write(x: TTask.TTaskItemType) = BSONString(x.id)
  }

  implicit val ColorBSONHandler = new BSONHandler[BSONBoolean, chess.Color] {
    def read(b: BSONBoolean) = chess.Color(b.value)
    def write(c: chess.Color) = BSONBoolean(c.white)
  }

  implicit val ClockConfigBSONHandler = new BSONHandler[BSONDocument, ClockConfig] {
    def read(doc: BSONDocument) = ClockConfig(
      doc.getAs[Int]("limit").get,
      doc.getAs[Int]("increment").get
    )

    def write(config: ClockConfig) = BSONDocument(
      "limit" -> config.limitSeconds,
      "increment" -> config.incrementSeconds
    )
  }

  implicit val VariantBSONHandler = new BSONHandler[BSONInteger, Variant] {
    def read(b: BSONInteger) = Variant.orDefault(b.value)
    def write(c: Variant) = BSONInteger(c.id)
  }

  implicit val RateModeBSONHandler = new BSONHandler[BSONBoolean, chess.Mode] {
    def read(b: BSONBoolean) = chess.Mode.orDefault(if (b.value) 1 else 0)
    def write(c: chess.Mode) = BSONBoolean(c.rated)
  }

  implicit val SpeedBSONHandler = new BSONHandler[BSONInteger, chess.Speed] {
    def read(b: BSONInteger) = chess.Speed(b.value) err s"apply chess.speed ${b.value}"
    def write(c: chess.Speed) = BSONInteger(c.id)
  }

  implicit val stringArrayHandler = bsonArrayToListHandler[String]

  implicit val NodeHandler = Macros.handler[Node]
  implicit val MoveHandler = Macros.handler[Move]

  implicit val CommonExtraHandler = Macros.handler[CommonItemExtra]
  implicit val CommonDiffHandler = Macros.handler[CommonDiff]
  implicit val CommonDiffArrayHandler = bsonArrayToListHandler[CommonDiff]
  implicit val CommonResultHandler = Macros.handler[CommonResult]

  implicit val PuzzleItemHandler = Macros.handler[PuzzleItem]
  implicit val PuzzleWithResultHandler = Macros.handler[PuzzleWithResult]

  implicit val ThemePuzzleItemHandler = Macros.handler[ThemePuzzleItem]
  implicit val ThemePuzzleWithResultHandler = Macros.handler[ThemePuzzleWithResult]

  implicit val CoordTrainItemHandler = Macros.handler[CoordTrainItem]
  implicit val CoordTrainWithResultHandler = Macros.handler[CoordTrainWithResult]

  implicit val PuzzleRushItemHandler = Macros.handler[PuzzleRushItem]
  implicit val PuzzleRushWithResultHandler = Macros.handler[PuzzleRushWithResult]

  implicit val GameItemHandler = Macros.handler[GameItem]
  implicit val GameWithResultHandler = Macros.handler[GameWithResult]

  implicit val MiniPuzzleHandler = Macros.handler[MiniPuzzle]
  implicit val MiniPuzzleArrayHandler = bsonArrayToListHandler[MiniPuzzle]
  implicit val MiniPuzzleResultHandler = Macros.handler[MiniPuzzleResult]
  implicit val MiniPuzzleWithResultHandler = Macros.handler[MiniPuzzleWithResult]
  implicit val MiniPuzzleResultArrayHandler = bsonArrayToListHandler[MiniPuzzleWithResult]
  implicit val PuzzleCapsuleHandler = Macros.handler[PuzzleCapsule]
  implicit val PuzzleCapsuleArrayHandler = bsonArrayToListHandler[PuzzleCapsule]
  implicit val PuzzleCapsuleResultHandler = Macros.handler[PuzzleCapsuleWithResult]

  implicit val ReplayGameItemHandler = Macros.handler[ReplayGameItem]
  implicit val ReplayGameResultHandler = Macros.handler[ReplayGameResult]
  implicit val ReplayGameWithResultHandler = Macros.handler[ReplayGameWithResult]
  implicit val ReplayGameWithResultArrayHandler = bsonArrayToListHandler[ReplayGameWithResult]
  implicit val ReplayGamesWithResultHandler = Macros.handler[ReplayGamesWithResult]

  implicit val RecallGameItemHandler = Macros.handler[RecallGameItem]
  implicit val RecallGameResultHandler = Macros.handler[RecallGameResult]
  implicit val RecallGameWithResultHandler = Macros.handler[RecallGameWithResult]
  implicit val RecallGameWithResultArrayHandler = bsonArrayToListHandler[RecallGameWithResult]
  implicit val RecallGamesWithResultHandler = Macros.handler[RecallGamesWithResult]

  implicit val DistinguishGameItemHandler = Macros.handler[DistinguishGameItem]
  implicit val DistinguishGameResultHandler = Macros.handler[DistinguishGameResult]
  implicit val DistinguishGameWithResultHandler = Macros.handler[DistinguishGameWithResult]
  implicit val DistinguishGameWithResultArrayHandler = bsonArrayToListHandler[DistinguishGameWithResult]
  implicit val DistinguishGamesWithResultHandler = Macros.handler[DistinguishGamesWithResult]

  implicit val FromPositionHandler = Macros.handler[FromPositionItem]
  implicit val FromPositionResultHandler = Macros.handler[FromPositionResult]
  implicit val FromPositionResultArrayHandler = bsonArrayToListHandler[FromPositionResult]
  implicit val FromPositionWithResultHandler = Macros.handler[FromPositionWithResult]
  implicit val FromPositionWithResultArrayHandler = bsonArrayToListHandler[FromPositionWithResult]
  implicit val FromPositionsWithResultHandler = Macros.handler[FromPositionsWithResult]

  import lila.game.BSONHandlers.EitherOpeningBSONHandler
  implicit val FromPgnHandler = Macros.handler[FromPgnItem]
  implicit val FromPgnResultHandler = Macros.handler[FromPgnResult]
  implicit val FromPgnResultArrayHandler = bsonArrayToListHandler[FromPgnResult]
  implicit val FromPgnWithResultHandler = Macros.handler[FromPgnWithResult]
  implicit val FromPgnWithResultArrayHandler = bsonArrayToListHandler[FromPgnWithResult]
  implicit val FromPgnsWithResultHandler = Macros.handler[FromPgnsWithResult]

  implicit val FromOpeningdbHandler = Macros.handler[FromOpeningdbItem]
  implicit val FromOpeningdbResultHandler = Macros.handler[FromOpeningdbResult]
  implicit val FromOpeningdbResultArrayHandler = bsonArrayToListHandler[FromOpeningdbResult]
  implicit val FromOpeningdbWithResultHandler = Macros.handler[FromOpeningdbWithResult]
  implicit val FromOpeningdbWithResultArrayHandler = bsonArrayToListHandler[FromOpeningdbWithResult]
  implicit val FromOpeningdbsWithResultHandler = Macros.handler[FromOpeningdbsWithResult]

  implicit val TrainGameHandler = Macros.handler[TrainGameItem]
  implicit val TrainGameResultHandler = Macros.handler[TrainGameResult]
  implicit val TrainGameWithResultHandler = Macros.handler[TrainGameWithResult]

  implicit val TeamTestItemHandler = Macros.handler[TeamTestItem]

  implicit val TTaskItemHandler = Macros.handler[TTaskItem]

  implicit val TTaskHandler = Macros.handler[TTask]

  implicit val TTaskTemplateHandler = Macros.handler[TTaskTemplate]

}
