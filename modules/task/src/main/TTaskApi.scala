package lila.task

import lila.user.{ User, UserRepo }
import akka.actor.ActorSystem
import chess.format.Forsyth
import chess.Situation
import chess.format.Forsyth.SituationPlus
import lila.common.paginator.Paginator
import lila.game.{ Game, GameRepo, IdGenerator, Player, Source }
import lila.hub.DuctMap
import lila.hub.actorApi.task.{ ChangeStatus, CreateTask, CreateTrainGameTask, SourceRelData }
import lila.hub.actorApi.calendar.{ CalendarCreate, CalendarsCreate, CalendarsRemove }
import lila.round.actorApi.round.AbortForce
import lila.notify.{ Notification, NotifyApi }
import lila.notify.Notification.{ Notifies, Sender }
import lila.task.TTask.TTaskItemType
import org.joda.time.DateTime
import scala.concurrent.duration._
import lila.hub.actorApi.socket.SendTo
import lila.socket.Socket.makeMessage
import play.api.libs.json.Json

final class TTaskApi(
    taskSolve: TTaskSolve,
    notifyApi: NotifyApi,
    bus: lila.common.Bus,
    onStart: Game.ID => Unit,
    roundMap: DuctMap[_],
    jsonView: JsonView
)(implicit system: ActorSystem) {

  def byId(id: TTask.ID): Fu[Option[TTask]] = TTaskRepo.byId(id)

  def byTeamTestStuId(teamTestStuId: String): Fu[Option[TTask]] =
    TTaskRepo.byTeamTestStuId(teamTestStuId)

  def byIds(ids: List[TTask.ID]): Fu[List[TTask]] = TTaskRepo.byIds(ids)

  def findByPage(
    page: Int,
    source: TTask.Source,
    metaId: String,
    userId: User.ID,
    status: Option[TTask.Status]
  ): Fu[Paginator[TTask]] = TTaskRepo.findByPage(page, source, metaId, userId, status)

  def findBySource(
    source: TTask.Source,
    metaId: String,
    userId: User.ID
  ): Fu[List[TTask]] = TTaskRepo.findBySource(source, metaId, userId)

  def create(data: TTaskData, creator: User): Funit = {
    val tasks = data.toTasks(creator)
    batchInsert(tasks) >>-
      tasks.foreach { addNotify } >>-
      tasks.foreach { task =>
        task.userIds.foreach { userId =>
          bus.publish(CreateTask(task.id, SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id), userId), 'ttask)
          if (task.isNumber) bus.publish(SendTo(userId, makeMessage("ttaskCreate", jsonView.sampleInfo(task))), 'socketUsers)
        }
      }
  }

  def batchInsert(tasks: List[TTask]): Funit =
    TTaskRepo.batchInsert(tasks) >>- {
      val userIds = tasks.flatMap(_.userIds)
      bus.publish(lila.hub.actorApi.home.ReloadTask(userIds), 'reloadTask)
    }

  def createTrainGame(data: TrainGameData, creator: User): Funit = {
    val gameId = IdGenerator.game.awaitSeconds(3)
    val task = data.toTask(gameId, creator)
    val trainGame = task.item.nonTrainGame.trainGame
    for {
      game <- makeGame(task)
      _ <- GameRepo.insertDenormalized(game) >>- onStart(gameId)
      _ <- TTaskRepo.insert(task)
      _ <- addNotify(task)
    } yield {
      bus.publish(CreateTrainGameTask(
        task.id,
        task.name,
        trainGame.white,
        trainGame.black,
        SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id)
      ), 'ttask)
    }
  }

  def cancel(task: TTask): Funit =
    task.isCreated ?? { setStatus(task, TTask.Status.Canceled) } >>- {
      task.itemType match {
        case TTaskItemType.TrainGameItem => task.item.trainGame.?? { tg => roundMap.tell(tg.trainGame.gameId, AbortForce) }
        case _ =>
      }
    }

  def doTask(task: TTask): Funit =
    task.isCreated ?? { setStatus(task, TTask.Status.Train) }

  def setStatus(task: TTask, status: TTask.Status): Funit = {
    val newTask = task.copy(status = status)
    TTaskRepo.setStatus(task.id, status) >>-
      task.userIds.foreach { userId =>
        bus.publish(ChangeStatus(task.id, task.name, SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id), userId, task.createdBy, status.id, task.num, task.coinRule), 'ttask)
        if (task.isNumber) bus.publish(SendTo(userId, makeMessage("ttaskStatus", jsonView.sampleInfo(newTask))), 'socketUsers)
      }
  }

  def setProgress(task: TTask, item: TTaskItem): Funit =
    taskSolve.setProgress(task, item)

  private def makeGame(task: TTask): Fu[Game] = {

    val trainGame = task.item.nonTrainGame.trainGame

    def makeChess(variant: chess.variant.Variant): chess.Game =
      chess.Game(situation = Situation(variant), clock = trainGame.clock.toClock.some)

    val baseState = trainGame.fen.ifTrue(trainGame.variant.fromPosition) flatMap { fen =>
      Forsyth.<<<@(chess.variant.FromPosition, fen)
    }

    val (chessGame, state) = baseState.fold(makeChess(trainGame.variant) -> none[SituationPlus]) {
      case sit @ SituationPlus(s, _) =>
        val game = chess.Game(
          situation = s,
          turns = sit.turns,
          startedAtTurn = sit.turns,
          clock = trainGame.clock.toClock.some
        )
        game -> baseState
    }

    val perfPicker = (perfs: lila.user.Perfs) => perfs(trainGame.perfType)
    for {
      whiteUser <- UserRepo.byId(trainGame.white)
      blackUser <- UserRepo.byId(trainGame.black)
    } yield {
      val g = Game.make(
        chess = chessGame,
        whitePlayer = Player.make(chess.White, whiteUser, perfPicker),
        blackPlayer = Player.make(chess.Black, blackUser, perfPicker),
        mode = trainGame.mode,
        source = Source.TTask,
        none
      ).withId(trainGame.gameId)

      state.fold(g) {
        case sit @ SituationPlus(Situation(board, _), _) => g.copy(
          chess = g.chess.copy(
            situation = g.situation.copy(
              board = g.board.copy(
                history = board.history,
                variant = chess.variant.FromPosition
              )
            ),
            turns = sit.turns
          )
        )
      }
        .withTaskId(task.id)
        .withTrainCourseId(task.sourceRel.id) |> (g => g.start)
    }
  }

  def setExpire(): Funit = TTaskRepo.setExpire()

  def updatePlanAt(task: TTask, planAt: DateTime): Funit = {
    TTaskRepo.updatePlanAt(task.id, planAt) >>- resetCalendar(task.copy(planAt = planAt.some))
  }

  private def addNotify(task: TTask): Funit = {
    task.userIds.map { userId =>
      notifyApi.addNotification(
        Notification.make(
          Sender(task.createdBy).some,
          Notifies(userId),
          lila.notify.GenericLink(
            url = s"/ttask/${task.id}",
            title = "新的任务".some,
            text = task.name.some,
            icon = task.sourceRel.source.icon
          )
        )
      )
    }.sequenceFu.void
  }

  private def resetCalendar(task: TTask) = {
    removeCalendar(task)
    system.scheduler.scheduleOnce(3 seconds) {
      publishCalendar(task)
    }
  }

  private def removeCalendar(task: TTask): Unit = {
    val ids = task.userIds.map { userId => s"${task.id}@$userId" }
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
  }

  def removeCalendars(tasks: List[TTask]): Unit = {
    val ids = tasks.flatMap { task => task.userIds.map { userId => s"${task.id}@$userId" } }
    bus.publish(CalendarsRemove(ids), 'calendarRemoveBus)
  }

  def removeCalendarByIds(calendarIds: List[String]): Unit = {
    bus.publish(CalendarsRemove(calendarIds), 'calendarRemoveBus)
  }

  private def publishCalendar(task: TTask): Unit = {
    val calendars = makeCalendar(task)
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  def publishCalendars(tasks: List[TTask]): Unit = {
    val calendars = tasks.flatMap { task => makeCalendar(task) }
    bus.publish(CalendarsCreate(calendars), 'calendarCreateBus)
  }

  private def makeCalendar(task: TTask): List[CalendarCreate] = {
    val now = DateTime.now
    task.userIds.map { userId =>
      CalendarCreate(
        id = s"${task.id}@$userId".some,
        typ = "task",
        user = userId,
        sdt = task.planAt | now,
        edt = task.deadlineAt | now.plusWeeks(1),
        content = task.name,
        onlySdt = false,
        link = s"/ttask/${task.id}".some,
        icon = "任".some,
        bg = "#507803".some
      )
    }
  }

}
