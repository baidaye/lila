package lila.task

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import TTask._

case class TTask(
    _id: TTask.ID,
    userIds: List[User.ID],
    name: String,
    remark: Option[String],
    item: TTaskItem,
    itemType: TTaskItemType,
    sourceRel: SourceRel,
    status: Status,
    coinTeam: Option[String],
    coinRule: Option[Int],
    coinDiff: Option[Int],
    tplId: Option[TTaskTemplate.ID],
    planAt: Option[DateTime],
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    finishAt: Option[DateTime],
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def isCreator(user: User) = user.id == createdBy

  def belongTo(user: User) = userIds.contains(user.id)

  def isFree = itemType.free

  def isStarted = startAt.isEmpty || startAt.??(_.isBeforeNow)

  def isDeadlined = deadlineAt.isDefined && deadlineAt.??(_.isBeforeNow)

  def isTrain = status == Status.Train

  def isCreated = status == Status.Created

  def isFinished = status == Status.Finished

  def isCanceled = status == Status.Canceled

  def isExpired = status == Status.Expired

  def canSolve = (isTrain || isCreated || (isFinished && (itemType == TTaskItemType.CapsulePuzzleItem || itemType == TTaskItemType.FromPositionItem || itemType == TTaskItemType.FromPgnItem || itemType == TTaskItemType.FromOpeningdbItem))) && isStarted && !isDeadlined

  def canPlay = (isTrain || isCreated || isFinished) && isStarted

  def hasTag =
    itemType match {
      case TTaskItemType.ThemePuzzleItem | TTaskItemType.PuzzleRushItem | TTaskItemType.CoordTrainItem | TTaskItemType.GameItem => true
      case _ => false
    }

  def hasCoin = coinRule.??(_ > 0)

  def link(u: User, extraId: Option[String]) = {
    if (itemType.free)
      withId(item.link(itemType, id, extraId))
    else {
      if (u.isMemberOrCoachOrTeam)
        withId(item.link(itemType, id, extraId))
      else
        s"/ttask/$id#notAccept"
    }
  }

  def withId(link: String) = if (link.contains("?")) s"$link&fr=$id" else s"$link?fr=$id"

  def num = item.num(itemType)

  def total = item.total(itemType)

  def formatTotal = item.formatTotal(itemType)

  def isComplete = item.isComplete(itemType)

  def progress = item.progress(itemType)

  def isNumber = item.isNumber(itemType)

}

object TTask {

  type ID = String

  def make(
    userIds: List[User.ID],
    name: String,
    remark: Option[String],
    itemType: TTaskItemType,
    item: TTaskItem,
    sourceRel: SourceRel,
    coinTeam: Option[String],
    coinRule: Option[Int],
    coinDiff: Option[Int],
    tplId: Option[TTaskTemplate.ID],
    planAt: Option[DateTime],
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    TTask(
      _id = Random nextString 12,
      userIds = userIds,
      name = name,
      remark = remark,
      item = item,
      itemType = itemType,
      sourceRel = sourceRel,
      status = Status.Created,
      coinTeam = coinTeam,
      coinRule = coinRule,
      coinDiff = None,
      planAt = planAt,
      tplId = tplId,
      startAt = startAt.orElse(now.some),
      deadlineAt = deadlineAt,
      finishAt = None,
      createdAt = now,
      updatedAt = now,
      createdBy = createdBy,
      updatedBy = createdBy
    )
  }

  def makeByTemplate(
    taskTemplate: TTaskTemplate,
    userId: User.ID,
    sourceId: String,
    sourceName: String,
    coinTeam: Option[String],
    coinRule: Option[Int],
    planAt: Option[DateTime],
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    createdBy: User.ID
  ) = make(
    userIds = List(userId),
    name = taskTemplate.name,
    remark = taskTemplate.remark,
    itemType = taskTemplate.itemType,
    item = taskTemplate.item,
    sourceRel = SourceRel(
      id = sourceId,
      name = sourceName,
      source = taskTemplate.sourceRel.source
    ),
    coinTeam = coinTeam,
    coinRule = coinRule,
    coinDiff = None,
    tplId = taskTemplate.id.some,
    planAt = planAt,
    startAt = startAt.orElse(DateTime.now.some),
    deadlineAt = deadlineAt orElse taskTemplate.deadlineAt,
    createdBy = createdBy
  )

  case class SourceRel(id: String, name: String, source: Source)

  sealed abstract class Source(val id: String, val name: String, val icon: String)
  object Source {
    case object TrainCourse extends Source(id = "trainCourse", name = "训练课", icon = "训")
    case object ThroughTrain extends Source(id = "throughTrain", name = "直通车", icon = "车")
    case object TeamClockIn extends Source(id = "teamClockIn", name = "俱乐部打卡", icon = "俱")
    case object TeamTest extends Source(id = "teamTest", name = "战术测评", icon = "测")
    case object Homework extends Source(id = "homework", name = "课后练", icon = "写")

    val all = List(TrainCourse, ThroughTrain, TeamClockIn, TeamTest, Homework)

    val mine = List(ThroughTrain, TeamClockIn, TeamTest)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Source = byId.get(id) err s"Bad Source $id"
  }

  sealed abstract class Status(val id: String, val name: String, sort: Int)
  object Status {
    case object Train extends Status("train", "训练中", 1)
    case object Created extends Status("created", "未完成", 2)
    case object Finished extends Status("finished", "已完成", 3)
    case object Canceled extends Status("canceled", "已取消", 4)
    case object Expired extends Status("expired", "已过期", 5)

    def all = List(Train, Created, Finished, Canceled, Expired)

    def current = List(Train, Created)

    def history = List(Finished, Canceled, Expired)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Status = all.find(_.id == id) err s"Bad Status $id"
  }

  sealed abstract class TTaskItemType(val id: String, val name: String, val source: TTaskItemSource, val free: Boolean)
  object TTaskItemType {
    case object PuzzleItem extends TTaskItemType("puzzle", "战术训练", TTaskItemSource.PuzzleItem, true)
    case object ThemePuzzleItem extends TTaskItemType("themePuzzle", "主题战术", TTaskItemSource.PuzzleItem, true)
    case object PuzzleRushItem extends TTaskItemType("puzzleRush", "战术冲刺", TTaskItemSource.RushItem, true)
    case object CoordTrainItem extends TTaskItemType("coordTrain", "坐标训练", TTaskItemSource.CoordTrainItem, true)
    case object CapsulePuzzleItem extends TTaskItemType("capsulePuzzle", "指定战术题", TTaskItemSource.PuzzleItem, true)
    case object ReplayGameItem extends TTaskItemType("replayGame", "打谱", TTaskItemSource.ReplayGameItem, true)
    case object RecallGameItem extends TTaskItemType("recallGame", "记谱", TTaskItemSource.RecallGameItem, true)
    case object DistinguishGameItem extends TTaskItemType("distinguishGame", "棋谱记录", TTaskItemSource.DistinguishGameItem, true)
    case object GameItem extends TTaskItemType("game", "对局", TTaskItemSource.GameItem, true)
    case object FromPositionItem extends TTaskItemType("fromPosition", "指定起始位置对局", TTaskItemSource.GameItem, true)
    case object FromPgnItem extends TTaskItemType("fromPgn", "指定起始PGN对局", TTaskItemSource.GameItem, false)
    case object FromOpeningdbItem extends TTaskItemType("fromOpeningdb", "指定开局库对局", TTaskItemSource.GameItem, false)
    case object TrainGameItem extends TTaskItemType("trainGame", "战术训练对局", TTaskItemSource.GameItem, true)
    case object TeamTest extends TTaskItemType("teamTest", "战术测评", TTaskItemSource.TeamTestItem, true)

    def all = List(PuzzleItem, ThemePuzzleItem, PuzzleRushItem, CoordTrainItem, CapsulePuzzleItem, ReplayGameItem, RecallGameItem, DistinguishGameItem, GameItem, FromPositionItem, FromPgnItem, FromOpeningdbItem, TrainGameItem, TeamTest)

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): TTaskItemType = all.find(_.id == id) err s"can not apply TTaskItemType $id"
  }

  sealed abstract class TTaskItemSource(val id: String)
  object TTaskItemSource {
    case object GameItem extends TTaskItemSource("gameItem")
    case object PuzzleItem extends TTaskItemSource("puzzleItem")
    case object RushItem extends TTaskItemSource("rushItem")
    case object CoordTrainItem extends TTaskItemSource("coordTrainItem")
    case object ReplayGameItem extends TTaskItemSource("replayGameItem")
    case object RecallGameItem extends TTaskItemSource("recallGameItem")
    case object DistinguishGameItem extends TTaskItemSource("distinguishGameItem")
    case object TeamTestItem extends TTaskItemSource("teamTestItem")

    def all = List(GameItem, PuzzleItem, RushItem, CoordTrainItem, ReplayGameItem, RecallGameItem, DistinguishGameItem, TeamTestItem)

    def apply(id: String): Option[TTaskItemSource] = all.find(_.id == id)
  }

  case class Light(_id: TTask.ID, userIds: List[User.ID], name: String, item: TTaskItem, itemType: TTaskItemType)

}

