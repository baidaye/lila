package lila.task

import lila.user.User
import lila.game.{ Game, GameRepo }
import lila.puzzle.PuzzleRush
import lila.task.TTask.TTaskItemType
import lila.hub.actorApi.task._
import akka.actor.ActorSystem
import chess.format.Forsyth
import lila.hub.actorApi.socket.SendTo
import lila.socket.Socket.makeMessage

final class TTaskSolve(jsonView: JsonView, bus: lila.common.Bus, system: ActorSystem) {

  def setProgress(task: TTask, item: TTaskItem): Funit = {
    val newTask = task.copy(item = item)
    TTaskRepo.updateItem(task.id, item) >>
      item.isComplete(task.itemType).?? {
        TTaskRepo.setFinish(task.id) >>- {
          task.userIds.foreach { userId =>
            bus.publish(ChangeStatus(
              task.id,
              task.name,
              SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id),
              userId,
              task.createdBy,
              TTask.Status.Finished.id,
              item.num(task.itemType),
              task.coinRule
            ), 'ttask)
            if (task.isNumber) bus.publish(SendTo(userId, makeMessage("ttaskFinish", jsonView.sampleInfo(newTask))), 'socketUsers)
          }
        }
      } >>- (task.item.progress(task.itemType) != item.progress(task.itemType)).?? {
        task.userIds.foreach { userId =>
          bus.publish(ChangeProgress(
            task.id,
            SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id),
            userId,
            item.num(task.itemType),
            item.progress(task.itemType)
          ), 'ttask)
          if (task.isNumber) bus.publish(SendTo(userId, makeMessage("ttaskProgress", jsonView.sampleInfo(newTask))), 'socketUsers)
        }
      }
  }

  def handlePuzzle(res: lila.puzzle.PuzzleResult): Funit = {
    TTaskRepo.mineAvailable(res.userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.PuzzleItem => {
            task.item.puzzle.?? { puzzleWithResult =>
              if (res.source == lila.puzzle.PuzzleResult.Source.Puzzle && !puzzleWithResult.contains(res.puzzleId.toString)) {
                val item = task.item.copy(puzzle = puzzleWithResult.finishPuzzle(res).some)
                setProgress(task, item)
              } else funit
            }
          }
          case TTaskItemType.ThemePuzzleItem => {
            task.item.themePuzzle.?? { themePuzzleWithResult =>
              if (res.source == lila.puzzle.PuzzleResult.Source.Theme && !themePuzzleWithResult.contains(res.puzzleId.toString) && res.search.exists(_.contains(themePuzzleWithResult.extraVal))) {
                val item = task.item.copy(themePuzzle = themePuzzleWithResult.finishThemePuzzle(res).some)
                setProgress(task, item)
              } else funit
            }
          }
          case TTaskItemType.CapsulePuzzleItem => {
            task.item.capsulePuzzle.?? { capsulePuzzleWithResult =>
              if (res.source == lila.puzzle.PuzzleResult.Source.Task) {
                val item = task.item.copy(capsulePuzzle = capsulePuzzleWithResult.finishPuzzle(res).some)
                setProgress(task, item)
              } else funit
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

  def handleRush(rush: PuzzleRush): Funit = {
    TTaskRepo.mineAvailable(rush.userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.PuzzleRushItem => {
            task.item.puzzleRush.?? { puzzleRushWithResult =>
              val item = task.item.copy(puzzleRush = puzzleRushWithResult.finishRush(rush).some)
              setProgress(task, item)
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

  def handleCoordTrain(coordTrain: lila.hub.actorApi.coordTrain.TrainData): Funit = {
    TTaskRepo.mineAvailable(coordTrain.userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.CoordTrainItem => {
            task.item.coordTrain.?? { coordTrainWithResult =>
              if (coordTrain.search.contains(coordTrainWithResult.extraVal)) {
                val item = task.item.copy(coordTrain = coordTrainWithResult.finishCoordTrain(coordTrain.score).some)
                setProgress(task, item)
              } else funit
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

  def handleStartGame(game: Game) = {
    if (game.metadata.taskId.isDefined) {
      TTaskRepo.trainGameTask(game.id).foreach {
        _.?? { task =>
          bus.publish(StartTrainGameTask(
            task.id,
            task.name,
            task.userIds.head,
            task.userIds.last,
            game.id,
            SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id)
          ), 'ttask)
        }
      }
    }
  }

  def handleFinishTrainGame(game: Game) = {
    if (game.metadata.taskId.isDefined && game.status != chess.Status.Aborted) {
      TTaskRepo.trainGameTask(game.id).map {
        _.?? { task =>
          task.item.trainGame.?? { trainGameWithResult =>
            val item = task.item.copy(trainGame = trainGameWithResult.finishTrainGame(game).some)
            TTaskRepo.updateItem(task.id, item) >> TTaskRepo.setStatus(task.id, TTask.Status.Finished) >>- {
              bus.publish(FinishTrainGameTask(
                task.id,
                task.name,
                task.userIds.head,
                task.userIds.last,
                game.id,
                SourceRelData(task.sourceRel.id, task.sourceRel.name, task.sourceRel.source.id)
              ), 'ttask)
            }
          }
        }
      }
    }
  }

  def handleFinishGame(game: Game) = {
    if (game.bothPlayersHaveMoved) {
      game.userIds.foreach { userId =>
        TTaskRepo.mineAvailable(userId).flatMap { tasks =>
          tasks.filter(_.canSolve).map { task =>
            task.itemType match {
              case TTaskItemType.FromPositionItem => {
                task.item.fromPosition.?? { fromPosition =>
                  //(!fromPosition.isComplete()).?? {
                  val fen = GameRepo.initialFen(game.id).awaitSeconds(3) | chess.format.FEN(Forsyth.initial)
                  val item = task.item.copy(fromPosition = fromPosition.finishFromPosition(game, fen, userId).some)
                  setProgress(task, item)
                  //}
                }
              }
              case TTaskItemType.FromPgnItem => {
                task.item.fromPgn.?? { fromPgn =>
                  //(!fromPosition.isComplete()).?? {
                  val item = task.item.copy(fromPgn = fromPgn.finishFromPgn(game, userId).some)
                  setProgress(task, item)
                  //}
                }
              }
              case TTaskItemType.FromOpeningdbItem => {
                task.item.fromOpeningdb.?? { fromOpeningdb =>
                  val item = task.item.copy(fromOpeningdb = fromOpeningdb.finishFromOpeningdb(game, userId).some)
                  setProgress(task, item)
                }
              }
              case TTaskItemType.GameItem => {
                task.item.game.?? { gm =>
                  (!gm.isComplete()).?? {
                    val item = task.item.copy(game = gm.finishGame(game, userId).some)
                    setProgress(task, item)
                  }
                }
              }
              case _ => funit
            }
          }.sequenceFu.void
        }
      }
    }
  }

  def handleReplayGame(userId: User.ID, homeworkId: String, red: String): Funit = {
    TTaskRepo.mineAvailable(userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.ReplayGameItem => {
            task.item.replayGame.?? { replayGame =>
              (!replayGame.isComplete()).?? {
                val item = task.item.copy(replayGame = replayGame.finishReplayGame(red).some)
                setProgress(task, item)
              }
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

  def handleRecall(recall: lila.hub.actorApi.Recall): Funit = {
    TTaskRepo.mineAvailable(recall.userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.RecallGameItem => {
            task.item.recallGame.?? { recallGame =>
              (!recallGame.isComplete()).?? {
                recall.hashId.?? { hashId =>
                  val item = task.item.copy(recallGame = recallGame.finishRecallGame(hashId, recall.win, recall.turns).some)
                  setProgress(task, item)
                }
              }
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

  def handleDistinguish(distinguish: lila.hub.actorApi.Distinguish): Funit = {
    TTaskRepo.mineAvailable(distinguish.userId).flatMap { tasks =>
      tasks.filter(_.canSolve).map { task =>
        task.itemType match {
          case TTaskItemType.DistinguishGameItem => {
            task.item.distinguishGame.?? { distinguishGame =>
              (!distinguishGame.isComplete()).?? {
                distinguish.hashId.?? { hashId =>
                  val item = task.item.copy(distinguishGame = distinguishGame.finishDistinguishGame(hashId, distinguish.win, distinguish.turns).some)
                  setProgress(task, item)
                }
              }
            }
          }
          case _ => funit
        }
      }.sequenceFu.void
    }
  }

}
