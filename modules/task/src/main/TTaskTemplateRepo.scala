package lila.task

import lila.db.dsl._
import org.joda.time.DateTime
import reactivemongo.api.ReadPreference

object TTaskTemplateRepo {

  import BSONHandlers.TTaskTemplateHandler

  private lazy val coll = Env.current.TTaskTemplateColl

  def byId(id: TTaskTemplate.ID): Fu[Option[TTaskTemplate]] =
    coll.byId[TTaskTemplate](id)

  def byIds(ids: List[TTaskTemplate.ID]): Fu[List[TTaskTemplate]] =
    coll.byIds[TTaskTemplate](ids)

  def byOrderedIds(ids: List[TTaskTemplate.ID]): Fu[List[TTaskTemplate]] =
    coll.byOrderedIds[TTaskTemplate, String](ids, readPreference = ReadPreference.secondaryPreferred)(_.id)

  def insert(task: TTaskTemplate): Funit =
    coll.insert(task).void

  def batchInsert(tasks: List[TTaskTemplate]): Funit =
    coll.bulkInsert(
      documents = tasks.map(TTaskTemplateHandler.write).toStream,
      ordered = true
    ).void

  def setDeadline(ids: List[TTaskTemplate.ID], deadlineAt: DateTime): Funit =
    coll.update($inIds(ids), $set("deadlineAt" -> deadlineAt), multi = true).void

  def update(task: TTaskTemplate): Funit =
    coll.update($id(task.id), task).void

  def remove(id: TTaskTemplate.ID): Funit =
    coll.remove($id(id)).void

  def removeByHomework(homeworkId: String, ninIds: List[TTaskTemplate.ID]): Funit =
    coll.remove($doc("sourceRel.id" -> homeworkId, "_id" $nin ninIds)).void

}
