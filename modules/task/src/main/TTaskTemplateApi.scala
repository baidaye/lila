package lila.task

import lila.user.User

final class TTaskTemplateApi() {

  def byId(id: TTaskTemplate.ID): Fu[Option[TTaskTemplate]] =
    TTaskTemplateRepo.byId(id)

  def byIds(ids: List[TTaskTemplate.ID]): Fu[List[TTaskTemplate]] =
    TTaskTemplateRepo.byIds(ids)

  def create(data: TTaskTemplateData, creator: User): Fu[TTaskTemplate] = {
    val tpl = data.toTaskTemplate(creator)
    create(tpl) inject tpl
  }

  def create(tpl: TTaskTemplate): Funit =
    TTaskTemplateRepo.insert(tpl)

  def update(oldTpl: TTaskTemplate, data: TTaskTemplateData, creator: User): Fu[TTaskTemplate] = {
    val tpl = data.toTaskTemplate(creator)
    val newTpl = oldTpl.copy(
      name = tpl.name,
      remark = tpl.remark,
      item = tpl.item,
      itemType = tpl.itemType,
      sourceRel = tpl.sourceRel,
      deadlineAt = tpl.deadlineAt
    )
    TTaskTemplateRepo.update(newTpl) inject newTpl
  }

  def update(tpl: TTaskTemplate): Funit =
    TTaskTemplateRepo.update(tpl)

  def remove(tpl: TTaskTemplate): Funit =
    TTaskTemplateRepo.remove(tpl.id)

}
