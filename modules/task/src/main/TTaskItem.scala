package lila.task

import java.net.URLDecoder
import java.net.URLEncoder
import chess.variant.Variant
import chess.{ Clock, Color, Mode }
import chess.format.Forsyth
import lila.game.PgnSetup.EitherOpening
import lila.game.{ Game, PerfPicker, PgnSetup, Player }
import lila.opening.{ OpeningDBHelper, PgnParser }
import lila.rating.PerfType
import lila.resource.ThemeQuery
import lila.task.TTask.TTaskItemType
import org.joda.time.DateTime
import play.api.libs.json._
import lila.user.User
import scalaz.{ Failure, Success }

case class TTaskItem(
    puzzle: Option[PuzzleWithResult] = None, // 战术训练
    themePuzzle: Option[ThemePuzzleWithResult] = None, // 主题战术
    coordTrain: Option[CoordTrainWithResult] = None, // 坐标训练
    puzzleRush: Option[PuzzleRushWithResult] = None, // 战术冲刺
    game: Option[GameWithResult] = None, // 对局
    capsulePuzzle: Option[PuzzleCapsuleWithResult] = None, // 指定战术题
    replayGame: Option[ReplayGamesWithResult] = None, // 打谱
    recallGame: Option[RecallGamesWithResult] = None, // 记谱
    distinguishGame: Option[DistinguishGamesWithResult] = None, // 棋谱记录
    fromPosition: Option[FromPositionsWithResult] = None, // 起始位置对局
    fromPgn: Option[FromPgnsWithResult] = None, // 起始PGN对局
    fromOpeningdb: Option[FromOpeningdbsWithResult] = None, // 起始开局库对局
    trainGame: Option[TrainGameWithResult] = None, // 训练课对局
    teamTest: Option[TeamTestItem] = None
) {

  def linkOfCommon(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => "/training"
    case TTaskItemType.ThemePuzzleItem => themePuzzle.?? { tp => s"/training/themeTask?${tp.extraVal}" }
    case TTaskItemType.PuzzleRushItem => puzzleRush.?? { pr => s"/training/rush?mode=${pr.puzzleRush.mode}&${pr.extraVal}&auto=true" }
    case TTaskItemType.CoordTrainItem => coordTrain.?? { ct => s"/training/coord?${ct.extraVal}&auto=true" }
    case _ => "/"
  }

  def link(itemType: TTaskItemType, taskId: String, extraId: Option[String]) = itemType match {
    case TTaskItemType.PuzzleItem => "/training"
    case TTaskItemType.ThemePuzzleItem => themePuzzle.?? { tp => s"/training/themeTask?${tp.extraVal}" }
    case TTaskItemType.PuzzleRushItem => puzzleRush.?? { pr => s"/training/rush?mode=${pr.puzzleRush.mode}&${pr.extraVal}&auto=true" }
    case TTaskItemType.CoordTrainItem => coordTrain.?? { ct => s"/training/coord?${ct.extraVal}&auto=true" }
    case TTaskItemType.GameItem => game.?? { gm => gm.gameItem.link(taskId) }
    case TTaskItemType.CapsulePuzzleItem => capsulePuzzle.?? { ct => extraId.fold(ct.firstUnWin.?? { p => s"/training/task/$taskId?startsAt=${p.puzzle.id}" }) { id => s"/training/task/$taskId?startsAt=$id" } }
    case TTaskItemType.ReplayGameItem => replayGame.?? { rg => extraId.fold(rg.firstUnComplete.?? { d => s"/ttask/$taskId/solveReplayGame?red=${URLEncoder.encode(d.replayGame.chapterLink, "UTF-8")}" }) { id => s"/ttask/$taskId/solveReplayGame?red=${URLEncoder.encode(id, "UTF-8")}" } }
    case TTaskItemType.RecallGameItem => recallGame.?? { rg => extraId.fold(rg.firstUnComplete.?? { d => s"/recall/showOfMate?${d.extraId}" }) { id => rg.findOrHead(id).?? { d => s"/recall/showOfMate?${d.extraId}" } } }
    case TTaskItemType.DistinguishGameItem => distinguishGame.?? { dg => extraId.fold(dg.firstUnComplete.?? { d => s"/distinguish/showOfMate?${d.extraId}" }) { id => dg.findOrHead(id).?? { d => s"/distinguish/showOfMate?${d.extraId}" } } }
    case TTaskItemType.FromPositionItem => fromPosition.?? { fp => extraId.fold(fp.firstUnComplete.?? { d => d.link(taskId) }) { id => fp.findOrHead(id).?? { d => d.link(taskId) } } }
    case TTaskItemType.FromPgnItem => fromPgn.?? { fp => extraId.fold(fp.firstUnComplete.?? { d => d.link(taskId) }) { id => fp.findOrHead(id).?? { d => d.link(taskId) } } }
    case TTaskItemType.FromOpeningdbItem => fromOpeningdb.?? { fp => extraId.fold(fp.firstUnComplete.?? { d => d.link(taskId) }) { id => fp.findOrHead(id).?? { d => d.link(taskId) } } }
    case TTaskItemType.TrainGameItem => trainGame.?? { tg => s"/${tg.trainGame.gameId}" }
    case TTaskItemType.TeamTest => teamTest.?? { tt => s"/team/test/stu/${tt.stuTestId}/show" }
  }

  def progress(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => puzzle.??(_.progress)
    case TTaskItemType.ThemePuzzleItem => themePuzzle.??(_.progress)
    case TTaskItemType.PuzzleRushItem => puzzleRush.??(_.progress)
    case TTaskItemType.CoordTrainItem => coordTrain.??(_.progress)
    case TTaskItemType.GameItem => game.??(_.progress)
    case TTaskItemType.CapsulePuzzleItem => capsulePuzzle.??(_.progress)
    case TTaskItemType.ReplayGameItem => replayGame.??(_.progress)
    case TTaskItemType.RecallGameItem => recallGame.??(_.progress)
    case TTaskItemType.DistinguishGameItem => distinguishGame.??(_.progress)
    case TTaskItemType.FromPositionItem => fromPosition.??(_.progress)
    case TTaskItemType.FromPgnItem => fromPgn.??(_.progress)
    case TTaskItemType.FromOpeningdbItem => fromOpeningdb.??(_.progress)
    case TTaskItemType.TrainGameItem => trainGame.??(_.progress)
    case TTaskItemType.TeamTest => teamTest.??(_.progress)
  }

  def isComplete(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => puzzle.??(_.isComplete)
    case TTaskItemType.ThemePuzzleItem => themePuzzle.??(_.isComplete)
    case TTaskItemType.PuzzleRushItem => puzzleRush.??(_.isComplete)
    case TTaskItemType.CoordTrainItem => coordTrain.??(_.isComplete)
    case TTaskItemType.GameItem => game.??(_.isComplete())
    case TTaskItemType.CapsulePuzzleItem => capsulePuzzle.??(_.isComplete)
    case TTaskItemType.ReplayGameItem => replayGame.??(_.isComplete())
    case TTaskItemType.RecallGameItem => recallGame.??(_.isComplete())
    case TTaskItemType.DistinguishGameItem => distinguishGame.??(_.isComplete())
    case TTaskItemType.FromPositionItem => fromPosition.??(_.isComplete())
    case TTaskItemType.FromPgnItem => fromPgn.??(_.isComplete())
    case TTaskItemType.FromOpeningdbItem => fromOpeningdb.??(_.isComplete())
    case TTaskItemType.TrainGameItem => trainGame.??(_.isComplete)
    case TTaskItemType.TeamTest => teamTest.??(_.isComplete())
  }

  def num(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => puzzle.??(_.num)
    case TTaskItemType.ThemePuzzleItem => themePuzzle.??(_.num)
    case TTaskItemType.PuzzleRushItem => puzzleRush.??(_.num)
    case TTaskItemType.GameItem => game.??(_.num)
    case TTaskItemType.CoordTrainItem => coordTrain.??(_.num)
    case TTaskItemType.CapsulePuzzleItem => capsulePuzzle.??(_.num)
    case TTaskItemType.ReplayGameItem => replayGame.??(_.num)
    case TTaskItemType.RecallGameItem => recallGame.??(_.num)
    case TTaskItemType.DistinguishGameItem => distinguishGame.??(_.num)
    case TTaskItemType.FromPositionItem => fromPosition.??(_.num)
    case TTaskItemType.FromPgnItem => fromPgn.??(_.num)
    case TTaskItemType.FromOpeningdbItem => fromOpeningdb.??(_.num)
    case TTaskItemType.TrainGameItem => trainGame.??(_.num)
    case TTaskItemType.TeamTest => teamTest.??(_.num)
  }

  def total(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => puzzle.??(_.total)
    case TTaskItemType.ThemePuzzleItem => themePuzzle.??(_.total)
    case TTaskItemType.PuzzleRushItem => puzzleRush.??(_.total)
    case TTaskItemType.CoordTrainItem => coordTrain.??(_.total)
    case TTaskItemType.GameItem => game.??(_.total)
    case TTaskItemType.CapsulePuzzleItem => capsulePuzzle.??(_.total)
    case TTaskItemType.ReplayGameItem => replayGame.??(_.total)
    case TTaskItemType.RecallGameItem => recallGame.??(_.total)
    case TTaskItemType.DistinguishGameItem => distinguishGame.??(_.total)
    case TTaskItemType.FromPositionItem => fromPosition.??(_.total)
    case TTaskItemType.FromPgnItem => fromPgn.??(_.total)
    case TTaskItemType.FromOpeningdbItem => fromOpeningdb.??(_.total)
    case TTaskItemType.TrainGameItem => trainGame.??(_.total)
    case TTaskItemType.TeamTest => teamTest.??(_.total)
  }

  def formatTotal(itemType: TTaskItemType) =
    if (isNumber(itemType)) total(itemType).toString
    else s"+${total(itemType)}"

  def isNumber(itemType: TTaskItemType) = itemType match {
    case TTaskItemType.PuzzleItem => puzzle.??(_.puzzle.isNumber)
    case TTaskItemType.ThemePuzzleItem => themePuzzle.??(_.themePuzzle.isNumber)
    case TTaskItemType.PuzzleRushItem => puzzleRush.??(_.puzzleRush.isNumber)
    case TTaskItemType.CoordTrainItem => coordTrain.??(_.coordTrain.isNumber)
    case TTaskItemType.GameItem => game.??(_.gameItem.isNumber)
    case TTaskItemType.CapsulePuzzleItem => true
    case TTaskItemType.ReplayGameItem => true
    case TTaskItemType.RecallGameItem => true
    case TTaskItemType.DistinguishGameItem => true
    case TTaskItemType.FromPositionItem => true
    case TTaskItemType.FromPgnItem => true
    case TTaskItemType.FromOpeningdbItem => true
    case TTaskItemType.TrainGameItem => true
    case TTaskItemType.TeamTest => true
  }

  def nonTrainGame = trainGame err s"can not find trainGame"

}

sealed trait TaskItemResult {
  def num: Int

  def total: Int

  def isComplete: Boolean

  def progress: Double
}

case class CommonItemExtra(cond: Option[String])

case class CommonDiff(id: String, diff: Int, win: Option[Boolean] = None)

case class CommonResult(num: Int, rating: Int, nowRating: Int, diffs: List[CommonDiff])

case class PuzzleItem(num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None)

case class PuzzleWithResult(puzzle: PuzzleItem, result: Option[CommonResult] = None) extends TaskItemResult {

  def finishPuzzle(res: lila.puzzle.PuzzleResult): PuzzleWithResult = copy(
    result = result.fold(
      CommonResult(
        num = if (res.result.win) 1 else 0,
        rating = res.rating._1,
        nowRating = res.rating._2,
        diffs = List(
          CommonDiff(
            id = res.puzzleId.toString,
            diff = res.rating._2 - res.rating._1,
            win = res.result.win.some
          )
        )
      )
    ) { old =>
        old.copy(
          num = if (res.result.win) old.num + 1 else old.num,
          nowRating = res.rating._2,
          diffs = old.diffs :+ CommonDiff(
            id = res.puzzleId.toString,
            diff = res.rating._2 - res.rating._1,
            win = res.result.win.some
          )
        )
      }.some
  )

  def contains(id: String) = result.exists { result =>
    result.diffs.filter(comm => comm.win | false).map(_.id).contains(id)
  }

  def currentResult(): Option[Int] =
    if (puzzle.isNumber) result.map(_.num)
    else result.map(r => r.nowRating - r.rating)

  def num = currentResult() | 0

  def total = puzzle.num

  def isComplete = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class ThemePuzzleItem(num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None) {

  def buildTags() = {
    val queryString = extra.??(_.cond | "")
    val condMap = DataForm.urlToMap(queryString)
    val condArray = condMap.map(_._2).toList
    List(
      condMap.find(_._1 == "ratingMin").map { x => s">=${x._2}分" },
      condMap.find(_._1 == "ratingMax").map { x => s"<=${x._2}分" },
      condMap.find(_._1 == "stepsMin").map { x => s">=${x._2}步" },
      condMap.find(_._1 == "stepsMax").map { x => s"<=${x._2}步" }
    ).filter(_.isDefined).map(_.get) ++ ThemeQuery.parseAllLabel(condArray)
  }

}

case class ThemePuzzleWithResult(themePuzzle: ThemePuzzleItem, result: Option[CommonResult] = None) extends TaskItemResult {

  def finishThemePuzzle(res: lila.puzzle.PuzzleResult): ThemePuzzleWithResult = copy(
    result = result.fold(
      CommonResult(
        num = if (res.result.win) 1 else 0,
        rating = res.rating._1,
        nowRating = res.rating._2,
        diffs = List(
          CommonDiff(
            id = res.puzzleId.toString,
            diff = res.rating._2 - res.rating._1,
            win = res.result.win.some
          )
        )
      )
    ) { old =>
        old.copy(
          num = if (res.result.win) old.num + 1 else old.num,
          nowRating = res.rating._2,
          diffs = old.diffs :+ CommonDiff(
            id = res.puzzleId.toString,
            diff = res.rating._2 - res.rating._1,
            win = res.result.win.some
          )
        )
      }.some
  )

  def contains(id: String) = result.exists { result =>
    result.diffs.filter(comm => comm.win | false).map(_.id).contains(id)
  }

  def extraVal = themePuzzle.extra.?? { extra =>
    extra.cond.?? {
      s => s
    }
  }

  def currentResult(): Option[Int] =
    if (themePuzzle.isNumber) result.map(_.num)
    else result.map(r => r.nowRating - r.rating)

  def num = currentResult() | 0

  def total = themePuzzle.num

  def isComplete = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class CoordTrainItem(num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None) {

  def buildTags() = {
    val queryString = extra.??(_.cond | "")
    val condMap = DataForm.urlToMap(queryString)
    List(
      condMap.find(_._1 == "mode").map {
        case (_, c) => DataForm.coordTrainMode.find(_._1 == c).?? { x => s"${x._2}" }
      },
      condMap.find(_._1 == "orient").map {
        case (_, c) => DataForm.coordTrainOrient.find(_._1 == c.toInt).?? { x => s"${x._2}" }
      },
      condMap.find(_._1 == "coordShow").map {
        case (_, c) => if (c == "1") {
          "显示棋盘坐标"
        } else {
          "不显示棋盘坐标"
        }
      },
      condMap.find(_._1 == "limitTime").map {
        case (_, c) => if (c == "1") {
          "限制时间"
        } else {
          "不限时间"
        }
      }
    ).filter(_.isDefined).map(_.get)
  }

}

case class CoordTrainWithResult(coordTrain: CoordTrainItem, result: Option[CommonResult] = None) extends TaskItemResult {

  def finishCoordTrain(score: Int): CoordTrainWithResult =
    copy(
      result = result.fold(
        CommonResult(
          num = 1,
          rating = 0,
          nowRating = 0,
          diffs = List(
            CommonDiff(
              id = "-",
              diff = 0
            )
          )
        )
      ) { old =>
          old.copy(
            num = old.num + 1,
            nowRating = 0,
            diffs = old.diffs :+ CommonDiff(
              id = "-",
              diff = 0
            )
          )
        }.some
    )

  def extraVal = coordTrain.extra.?? { extra =>
    extra.cond.?? {
      s => s
    }
  }

  def num = result.map(_.num) | 0

  def total = coordTrain.num

  def isComplete = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class PuzzleRushItem(mode: String, num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None) {

  def isCustom = mode == "custom"

  def buildTags() = {
    val queryString = extra.??(_.cond | "")
    val condMap = DataForm.urlToMap(queryString)
    List(
      DataForm.puzzleRushMode.find(_._1 == mode).map { x => s"${x._2}" },
      condMap.find(_._1 == "ratingMin").map { x => s">=${x._2}分" },
      condMap.find(_._1 == "ratingMax").map { x => s"<=${x._2}分" },
      condMap.find(_._1 == "stepsMin").map { x => s">=${x._2}步" },
      condMap.find(_._1 == "stepsMax").map { x => s"<=${x._2}步" },
      condMap.find(_._1 == "color").map {
        case (_, c) => DataForm.color.find(_._1 == c).?? { x => s"${x._2}" }
      },
      condMap.find(_._1 == "phase").map {
        case (_, c) => DataForm.phase.find(_._1 == c).?? { x => s"${x._2}" }
      },
      condMap.find(_._1 == "selector").map {
        case (_, c) => DataForm.selector.find(_._1 == c).?? { x => s"${x._2}" }
      },
      condMap.find(_._1 == "minutes").map { x => s"${x._2}分钟" },
      condMap.find(_._1 == "limit").map { x => s"错题<${x._2}个" }
    ).filter(_.isDefined).map(_.get)
  }

}

case class PuzzleRushWithResult(puzzleRush: PuzzleRushItem, result: Option[CommonResult] = None) {

  def finishRush(rush: lila.puzzle.PuzzleRush): PuzzleRushWithResult =
    if ((puzzleRush.mode == rush.mode.id && puzzleRush.mode != "custom") ||
      (puzzleRush.mode == rush.mode.id && puzzleRush.mode == "custom" && rush.search.exists(_.contains(extraVal)))) {
      copy(
        result = result.fold(
          CommonResult(
            num = 1,
            rating = 0,
            nowRating = 0,
            diffs = List(
              CommonDiff(
                id = rush.id,
                diff = 0
              )
            )
          )
        ) { old =>
            old.copy(
              num = old.num + 1,
              nowRating = 0,
              diffs = old.diffs :+ CommonDiff(
                id = rush.id,
                diff = 0
              )
            )
          }.some
      )
    } else this

  def extraVal = puzzleRush.extra.?? { extra =>
    extra.cond.?? {
      s => s
    }
  }

  def num = result.map(_.num) | 0

  def total = puzzleRush.num

  def isComplete = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class GameItem(speed: chess.Speed, num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None) {

  def buildTags() =
    List(speed.name) ++ clockTag

  def clockTag = speed match {
    case chess.Speed.UltraBullet => Nil
    case chess.Speed.Bullet => Nil
    case chess.Speed.Blitz => List("3+2/5+0/5+3")
    case chess.Speed.Rapid => List("10+0/10+5/15+10")
    case chess.Speed.Classical => List("30+0/30+20")
    case _ => Nil
  }

  def limitAndInc = speed match {
    case chess.Speed.UltraBullet => (29, 0)
    case chess.Speed.Bullet => (2 * 60, 1)
    case chess.Speed.Blitz => (5 * 60, 3)
    case chess.Speed.Rapid => (10 * 60, 5)
    case chess.Speed.Classical => (30 * 60, 0)
    case _ => (30 * 60, 0)
  }

  def clock = {
    val lai = limitAndInc
    chess.Clock.Config(lai._1, lai._2)
  }

  def link(taskId: String) = "/lobby"

  /*  def link(taskId: String) = {
    val clock = limitAndInc
    s"/setup/friend/custom?limit=${clock._1}&increment=${clock._2}&rated=true"
  }*/
}

case class GameWithResult(gameItem: GameItem, result: Option[CommonResult] = None) extends TaskItemResult {

  def finishGame(game: Game, userId: User.ID): GameWithResult = {
    if (game.nonAi && game.speed == gameItem.speed) {
      val diff = game.playerByUserId(userId).map(p => p.ratingDiff | 0) | 0
      val rating = game.playerByUserId(userId).map(p => p.rating | 0) | 0
      val nowRating = rating + diff
      copy(
        result = result.fold(
          CommonResult(
            num = 1,
            rating = rating,
            nowRating = nowRating,
            diffs = List(
              CommonDiff(
                id = game.id,
                diff = diff,
                win = game.winnerUserId.contains(userId).some
              )
            )
          )
        ) { old =>
            old.copy(
              num = old.num + 1,
              nowRating = nowRating,
              diffs = old.diffs :+ CommonDiff(
                id = game.id,
                diff = diff,
                win = game.winnerUserId.contains(userId).some
              )
            )
          }.some
      )
    } else this
  }

  def currentResult(): Option[Int] =
    if (gameItem.isNumber) result.map(_.num)
    else result.map(r => r.nowRating - r.rating)

  def num = currentResult() | 0

  def total = gameItem.num

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class MiniPuzzle(id: Int, fen: String, color: Color, lastMove: Option[String], lines: String) {

  def firstRightMoveSet = {
    val ln = URLDecoder.decode(lines, "UTF-8")
    Json.parse(ln).asOpt[JsObject].map(_.keys)
  } err s"can not find puzzle right move of $id"

  override def toString: String = id.toString

}

case class MiniPuzzleResult(win: Boolean, lines: List[Node])

case class MiniPuzzleWithResult(puzzle: MiniPuzzle, result: Option[List[MiniPuzzleResult]]) {

  def isTry = result.isDefined

  def isComplete = result.?? { r =>
    r.exists(_.win)
  }

  def isFirstComplete = result.?? { r =>
    r.headOption.exists(_.win)
  }

  def notComplete = !isComplete

  val nonEmptyResultList = result.?? { r =>
    r.filterNot(_.lines.isEmpty)
  }

  def headResult = nonEmptyResultList.headOption.fold(List.empty[MiniPuzzleResult]) { r => List(r) }

  def isFirstMoveRight = firstRightMove.isDefined

  def firstRightMove = headResult.filter { pr =>
    puzzle.firstRightMoveSet.contains(pr.lines.head.uci) || pr.win
  } map { pr =>
    formatSan(pr.lines.head.san)
  } find (_.nonEmpty)

  def firstRightMoveUci = headResult.filter { pr =>
    puzzle.firstRightMoveSet.contains(pr.lines.head.uci) || pr.win
  } map { pr => pr.lines.head.uci } find (_.nonEmpty) map (move => List(move.take(2), move.drop(2)))

  def firstWrongMove = headResult.filter { pr =>
    !puzzle.firstRightMoveSet.contains(pr.lines.head.uci) && !pr.win
  } map { pr =>
    formatSan(pr.lines.head.san)
  } find (_.nonEmpty)

  def firstWrongMoveUci = headResult.filter { pr =>
    !puzzle.firstRightMoveSet.contains(pr.lines.head.uci) && !pr.win
  } map { pr => pr.lines.head.uci } find (_.nonEmpty) map (move => List(move.take(2), move.drop(2)))

  def formatSan(san: String) = san.replace("+", "").replace("#", "")

  def finishPuzzle(res: lila.puzzle.PuzzleResult): MiniPuzzleWithResult = copy(
    result = result.fold(
      List(
        MiniPuzzleResult(
          win = res.result.win,
          lines = res.lines.map { n =>
            Node(n.san, n.uci, n.fen)
          }
        )
      )
    ) { old =>
        old :+ MiniPuzzleResult(
          win = res.result.win,
          lines = res.lines.map { n =>
            Node(n.san, n.uci, n.fen)
          }
        )
      }.some
  )

}

case class PuzzleCapsule(id: String, name: String, puzzles: List[Int])

case class PuzzleCapsuleWithResult(capsules: List[PuzzleCapsule], puzzles: List[MiniPuzzleWithResult]) extends TaskItemResult {

  def finishPuzzle(res: lila.puzzle.PuzzleResult) =
    copy(
      puzzles = puzzles.map { p =>
        if (p.puzzle.id == res.puzzleId) {
          p.finishPuzzle(res)
        } else p
      }
    )

  def findPuzzle(puzzle: MiniPuzzle) =
    puzzles.find(_.puzzle == puzzle) err s"can not find puzzle of $puzzle"

  def puzzleRightRate = Math.round(puzzles.count(_.isComplete).toDouble / puzzles.count(_.isTry).toDouble * 10000).toDouble / 100

  def puzzleFirstRightRate = Math.round(puzzles.count(_.isFirstComplete).toDouble / puzzles.count(_.isTry).toDouble * 10000).toDouble / 100

  def firstUnWin = puzzles.find(!_.isComplete) orElse puzzles.headOption

  def firstUnTry = puzzles.find(!_.isTry) orElse puzzles.headOption

  def num = puzzles.count(_.isComplete)

  def total = puzzles.size

  def isComplete = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class Node(san: String, uci: String, fen: String)

case class Move(index: Int, white: Option[Node], black: Option[Node]) {

  override def toString(): String = {
    val text = (white, black) match {
      case (Some(w), Some(b)) => s" ${w.san} ${b.san}"
      case (Some(w), None) => s" ${w.san}"
      case (None, Some(b)) => s".. ${b.san}"
      case _ => ""
    }
    s"$index.${text.trim}"
  }

}

case class ReplayGameItem(chapterLink: String, name: String, root: String, pgn: Option[String], color: Option[Color], moves: List[Move]) {

  def unique = this.chapterIdFromLink

  // OlClass:   /olclass/${olClassId}#${courseWareId}
  // Study:     /study/${studyId}/${chapterId}
  def chapterIdFromLink = {
    if (chapterLink.startsWith("/study")) {
      val arr = chapterLink.split("/")
      arr(arr.length - 1)
    } else if (chapterLink.startsWith("/olclass")) {
      val arr = chapterLink.split("#")
      arr(1)
    } else s"${System.currentTimeMillis()}"
  }

  def isContains(red: String) =
    chapterLink.contains(red)

  def lastFen = {
    moves.lastOption.map { move => move.black.map(_.fen).orElse(move.white.map(_.fen)) } | root.some
  } | Forsyth.initial

  def toPGN = pgn | {
    val turns = if (moves.nonEmpty) {
      moves.mkString(" ")
    } else "*"
    s"""[FEN "$root"]\n\n$turns"""
  }

  def simplePgn = {
    val turns = if (moves.nonEmpty) {
      moves.mkString(" ")
    } else "*"
    s"""[FEN "$root"]\n\n$turns"""
  }

  override def toString: String = chapterLink

}

case class ReplayGameResult(win: Boolean)

case class ReplayGameWithResult(replayGame: ReplayGameItem, result: Option[ReplayGameResult]) {

  def unique = replayGame.unique

  def extraId = replayGame.chapterLink

  def isTry = result.isDefined

  def isComplete() = result.?? {
    _.win
  }

  def isContains(red: String) = replayGame.isContains(red)

  def finishReplayGame: ReplayGameWithResult = copy(
    result = {
      result | ReplayGameResult(true)
    }.some
  )
}

case class ReplayGamesWithResult(replayGames: List[ReplayGameWithResult]) extends TaskItemResult {

  def firstUnComplete = replayGames.find(!_.isComplete) orElse replayGames.headOption

  def finishReplayGame(red: String): ReplayGamesWithResult = copy(
    replayGames = replayGames.map { r =>
      if (r.isContains(red)) r.finishReplayGame else r
    }
  )

  def findReplayGame(replayGame: ReplayGameItem) =
    replayGames.find(_.replayGame == replayGame) err s"can not find replayGame of $replayGame"

  def num = replayGames.count(_.isComplete())

  def total = replayGames.size

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class RecallGameItem(name: Option[String], root: String, pgn: String, moves: Option[List[Move]], turns: Option[Int], color: Option[Color], orient: Option[String], title: Option[String]) {

  def unique = this.toString()

  def extraId = s"hashId=${hashMD5}&pgn=${URLEncoder.encode(pgn, "UTF-8")}&color=${color.??(_.name)}&orient=${orient | ""}&turns=${turns.??(_.toString)}&title=${URLEncoder.encode(title | "", "UTF-8")}"

  def hashMD5: String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest((pgn + turns.??(_.toString) + color.??(_.name)).getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def lastFen = {
    moves.map { moves =>
      moves.lastOption.map { move => move.black.map(_.fen).orElse(move.white.map(_.fen)) } | root.some
    } | root.some
  } | Forsyth.initial

  def simplePgn = moves.map { moves =>
    val turns = if (moves.nonEmpty) {
      moves.mkString(" ")
    } else "*"
    s"""[FEN "$root"]\n\n$turns"""
  }

  override def toString: String = s"${if (pgn.length < 50) pgn else pgn.substring(pgn.length - 51, pgn.length - 1)} ${turns | 0} ${color.map(_.name) | ""} ${orient | ""} ${name | ""} ${title | ""}"
}

case class RecallGameResult(win: Boolean, turns: Int)

case class RecallGameWithResult(recallGame: RecallGameItem, result: Option[RecallGameResult]) {

  def unique = recallGame.unique

  def extraId = recallGame.extraId

  def isComplete() = result.?? {
    _.win
  }

  def turns = result.??(_.turns)

  def finishRecallGame(win: Boolean, turns: Int): RecallGameWithResult = copy(
    result = {
      result.map(r =>
        if (turns > r.turns) {
          r.copy(
            win, turns
          )
        } else r) | RecallGameResult(win, turns)
    }.some
  )

}

case class RecallGamesWithResult(recallGames: List[RecallGameWithResult]) extends TaskItemResult {

  def firstUnComplete = recallGames.find(!_.isComplete) orElse recallGames.headOption

  def findOrHead(hashId: String) = recallGames.find(_.recallGame.hashMD5 == hashId) orElse recallGames.headOption

  def finishRecallGame(hashId: String, win: Boolean, turns: Int): RecallGamesWithResult = copy(
    recallGames = recallGames.map { r =>
      if (r.recallGame.hashMD5 == hashId) r.finishRecallGame(win, turns) else r
    }
  )

  def findRecallGame(recallGame: RecallGameItem) =
    recallGames.find(_.recallGame.toString == recallGame.toString) err s"can not find recallGame of $recallGame"

  def num = recallGames.count(_.isComplete())

  def total = recallGames.size

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class DistinguishGameItem(name: Option[String], root: String, pgn: String, moves: Option[List[Move]], rightTurns: Int, turns: Option[Int], orient: Option[String], title: Option[String]) {

  def unique = this.toString

  def extraId = s"hashId=${hashMD5}&pgn=${URLEncoder.encode(pgn, "UTF-8")}&orientation=${orient | ""}&rightTurns=${rightTurns}&turns=${turns.??(_.toString)}&title=${URLEncoder.encode(title | "", "UTF-8")}"

  def hashMD5: String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest((pgn + rightTurns + turns.??(_.toString) + (orient | "")).getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def lastFen = {
    moves.map { moves =>
      moves.lastOption.map { move => move.black.map(_.fen).orElse(move.white.map(_.fen)) } | root.some
    } | root.some
  } | Forsyth.initial

  def simplePgn = moves.map { moves =>
    val turns = if (moves.nonEmpty) {
      moves.mkString(" ")
    } else "*"
    s"""[FEN "$root"]\n\n$turns"""
  }

  override def toString: String = s"${if (pgn.length < 50) pgn else pgn.substring(pgn.length - 51, pgn.length - 1)} $rightTurns ${turns | 0} ${orient | ""} ${name | ""} ${title | ""}"

}

case class DistinguishGameResult(win: Boolean, turns: Int)

case class DistinguishGameWithResult(distinguishGame: DistinguishGameItem, result: Option[DistinguishGameResult]) {

  def unique = distinguishGame.unique

  def extraId = distinguishGame.extraId

  def isComplete() = result.?? {
    _.win
  }

  def turns = result.??(_.turns)

  def finishDistinguishGame(win: Boolean, turns: Int): DistinguishGameWithResult = copy(
    result = {
      result.map(r =>
        if (turns > r.turns) {
          r.copy(
            win, turns
          )
        } else r) | DistinguishGameResult(win, turns)
    }.some
  )

}

case class DistinguishGamesWithResult(distinguishGames: List[DistinguishGameWithResult]) extends TaskItemResult {

  def finishDistinguishGame(hashId: String, win: Boolean, turns: Int): DistinguishGamesWithResult = copy(
    distinguishGames = distinguishGames.map { r =>
      if (r.distinguishGame.hashMD5 == hashId) r.finishDistinguishGame(win, turns) else r
    }
  )

  def firstUnComplete = distinguishGames.find(!_.isComplete) orElse distinguishGames.headOption

  def findOrHead(hashId: String) = distinguishGames.find(_.distinguishGame.hashMD5 == hashId) orElse distinguishGames.headOption

  def findDistinguishGame(distinguishGame: DistinguishGameItem) =
    distinguishGames.find(_.distinguishGame.toString == distinguishGame.toString) err s"can not find distinguishGame of $distinguishGame"

  def num = distinguishGames.count(_.isComplete())

  def total = distinguishGames.size

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class FromPositionItem(fen: String, clock: Clock.Config, num: Int, isAi: Option[Boolean], aiLevel: Option[Int], color: Option[Color], chessStatus: Option[String], canTakeback: Option[Boolean]) {

  def unique = this.toString

  def hashMD5: String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest(toString().getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def ai: Boolean = isAi.??(x => x)

  def link(taskId: String) = if (ai) {
    s"/ttask/$taskId/formPositionModal?fen=$fen&limit=${clock.limitSeconds}&increment=${clock.incrementSeconds}&aiLevel=${aiLevel | 1}${color.??(c => s"&color=${c.name}")}${canTakeback.??(t => s"&canTakeback=$t")}"
  } else {
    s"/setup/friend/custom?fen=$fen&limit=${clock.limitSeconds}&increment=${clock.incrementSeconds}${color.??(c => s"&color=${c.name}")}${canTakeback.??(t => s"&canTakeback=$t")}"
  }

  override def toString: String = s"$fen ${clock.show} ${isAi | false} ${aiLevel | 1}${color.??(c => s" ${c.name}")}${chessStatus.??(c => s" $c")}${canTakeback.??(t => s" canTakeback:$t")}"
}

case class FromPositionResult(gameId: Game.ID, white: User.ID, black: User.ID, winnerColor: Option[chess.Color], userId: Option[User.ID]) {

  def storeUser = userId.isDefined

  def isWin = userId match {
    case None => none[Boolean]
    case Some(uid) => {
      winnerColor match {
        case None => none[Boolean]
        case Some(c) => c match {
          case chess.Color.White => Some(uid == white)
          case chess.Color.Black => Some(uid == black)
        }
      }
    }
  }

  def isWinOrFalse = storeUser && (isWin | false)

  def isDraw = userId match {
    case None => none[Boolean]
    case Some(_) => Some(winnerColor.isEmpty)
  }

  def isDrawOrFalse = storeUser && (isDraw | false)

  def isWinOrDraw = isWinOrFalse || isDrawOrFalse

  def resultText =
    userId.map { _ =>
      winnerColor match {
        case None => "1/2-1/2"
        case Some(c) => c match {
          case chess.Color.White => "1-0"
          case chess.Color.Black => "0-1"
        }
      }
    }

}

case class FromPositionWithResult(fromPosition: FromPositionItem, result: Option[List[FromPositionResult]]) {

  def unique = fromPosition.unique

  def ai = fromPosition.ai

  def sameGame(game: Game, fen: chess.format.FEN, userId: String) = {
    sameFen(fen.value) && sameClock(game.clock.??(_.config.show)) && sameAi(game) && sameColor(game, userId) && sameCanTakeback(game)
  }

  def sameFen(fen: String) = fen.split("-")(0) == fromPosition.fen.split("-")(0)

  def sameClock(clock: String) = clock == fromPosition.clock.show

  def sameAi(game: Game) = game.nonAi || (game.hasAi && ai && sameAiLevel(game.aiLevel))

  def sameAiLevel(level: Option[Int]) = fromPosition.aiLevel.isEmpty || level.??(fromPosition.aiLevel.has(_))

  def sameColor(game: Game, userId: String) = fromPosition.color.isEmpty || fromPosition.color.?? { c => game.playerByUserId(userId).??(_.color == c) }

  def sameCanTakeback(game: Game) = fromPosition.canTakeback.isEmpty || fromPosition.canTakeback.??(game.canTakeback.has(_))

  def sameChessStatus(result: FromPositionResult) = !result.storeUser || fromPosition.chessStatus.isEmpty || fromPosition.chessStatus.?? {
    case "all" => true
    case "win" => result.isWinOrFalse
    case "winOrDraw" => result.isWinOrDraw
  }

  def completeSize() = result.??(_.count(r => sameChessStatus(r)))

  def isComplete() = completeSize >= fromPosition.num

  def progress = {
    val v = completeSize.toDouble / fromPosition.num.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

  def link(taskId: String) = fromPosition.link(taskId)

  def finishFromPosition(game: Game, userId: String): FromPositionWithResult = copy(
    result = {
      result.fold(
        List(
          FromPositionResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        )
      ) { r =>
          r :+ FromPositionResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        }
    }.some
  )

  def playerUserId(p: Player) = {
    p.aiLevel.fold(
      p.userId | User.anonymous
    ) { level => s"A.I. level $level" }
  }

}

case class FromPositionsWithResult(fromPositions: List[FromPositionWithResult]) extends TaskItemResult {

  def finishFromPosition(game: Game, fen: chess.format.FEN, userId: String): FromPositionsWithResult = copy(
    fromPositions = fromPositions.map { fp =>
      if (fp.sameGame(game, fen, userId)) fp.finishFromPosition(game, userId) else fp
    }
  )

  def firstUnComplete = fromPositions.find(!_.isComplete) orElse fromPositions.headOption

  def findOrHead(hashId: String) = fromPositions.find(_.fromPosition.hashMD5 == hashId) orElse fromPositions.headOption

  def findFromPosition(fromPosition: FromPositionItem) =
    fromPositions.find(_.fromPosition == fromPosition) err s"can not find fromPosition of $fromPosition"

  def num = fromPositions.map(_.completeSize()).sum

  def total = fromPositions.map(_.fromPosition.num).sum

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class FromPgnItem(opening: EitherOpening, clock: Clock.Config, num: Int, isAi: Option[Boolean], aiLevel: Option[Int], color: Option[Color], chessStatus: Option[String], canTakeback: Option[Boolean]) {

  val pgnSetup = opening match {
    case Left(pgn) => PgnParser.preprocess(pgn) match {
      case Success((replay, initialFen)) => {
        if (replay.setup.situation.playable(true) && replay.state.situation.playable(true))
          new PgnSetup(replay.state.turns, initialFen, opening)
        else sys.error(s"can not parse pgn $pgn")
      }
      case Failure(_) => sys.error(s"can not parse pgn $pgn")
    }
    case Right(openingdbId) => {
      val o = OpeningDBHelper.openingFromCache(openingdbId)
      PgnSetup(o.startedAtTurnNext, o.initialFen, opening)
    }
  }

  def openingParam = opening match {
    case Left(pgn) => s"pgn=${URLEncoder.encode(pgn, "UTF-8")}"
    case Right(openingdbId) => s"openingdbId=$openingdbId"
  }

  def link(taskId: String) = if (ai) {
    s"/ttask/$taskId/formPgnModal?${openingParam}&limit=${clock.limitSeconds}&increment=${clock.incrementSeconds}&aiLevel=${aiLevel | 1}${color.??(c => s"&color=${c.name}")}${canTakeback.??(t => s"&canTakeback=$t")}"
  } else {
    s"/setup/friend/custom?${openingParam}&limit=${clock.limitSeconds}&increment=${clock.incrementSeconds}${color.??(c => s"&color=${c.name}")}${canTakeback.??(t => s"&canTakeback=$t")}"
  }

  def hashMD5: String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest(toString().getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def ai: Boolean = isAi.??(x => x)

  def unique = this.toString

  override def toString: String = s"${openingParam} ${clock.show} ${isAi | false} ${aiLevel | 1}${color.??(c => s" ${c.name}")}${chessStatus.??(c => s" $c")}${canTakeback.??(t => s" canTakeback:$t")}"

}

case class FromPgnResult(gameId: Game.ID, white: User.ID, black: User.ID, winnerColor: Option[chess.Color], userId: Option[User.ID]) {

  def storeUser = userId.isDefined

  def isWin = userId match {
    case None => none[Boolean]
    case Some(uid) => {
      winnerColor match {
        case None => none[Boolean]
        case Some(c) => c match {
          case chess.Color.White => Some(uid == white)
          case chess.Color.Black => Some(uid == black)
        }
      }
    }
  }

  def isWinOrFalse = storeUser && (isWin | false)

  def isDraw = userId match {
    case None => none[Boolean]
    case Some(_) => Some(winnerColor.isEmpty)
  }

  def isDrawOrFalse = storeUser && (isDraw | false)

  def isWinOrDraw = isWinOrFalse || isDrawOrFalse

  def resultText =
    userId.map { _ =>
      winnerColor match {
        case None => "1/2-1/2"
        case Some(c) => c match {
          case chess.Color.White => "1-0"
          case chess.Color.Black => "0-1"
        }
      }
    }

}

case class FromPgnWithResult(fromPgn: FromPgnItem, result: Option[List[FromPgnResult]]) {

  def unique = fromPgn.unique

  def ai = fromPgn.ai

  def sameGame(game: Game, userId: String) = {
    game.initialPgn.??(ip => samePgn(ip.opening)) && sameClock(game.clock.??(_.config.show)) && sameAi(game) && sameColor(game, userId) && sameCanTakeback(game)
  }

  def samePgn(opening: EitherOpening) = fromPgn.opening == opening

  def sameClock(clock: String) = clock == fromPgn.clock.show

  def sameAi(game: Game) = game.nonAi || (game.hasAi && ai && sameAiLevel(game.aiLevel))

  def sameAiLevel(level: Option[Int]) = fromPgn.aiLevel.isEmpty || level.??(fromPgn.aiLevel.has(_))

  def sameColor(game: Game, userId: String) = fromPgn.color.isEmpty || fromPgn.color.?? { c => game.playerByUserId(userId).??(_.color == c) }

  def sameCanTakeback(game: Game) = fromPgn.canTakeback.isEmpty || fromPgn.canTakeback.??(game.canTakeback.has(_))

  def sameChessStatus(result: FromPgnResult) = !result.storeUser || fromPgn.chessStatus.isEmpty || fromPgn.chessStatus.?? {
    case "all" => true
    case "win" => result.isWinOrFalse
    case "winOrDraw" => result.isWinOrDraw
  }

  def completeSize() = result.??(_.count(r => sameChessStatus(r)))

  def isComplete() = completeSize >= fromPgn.num

  def progress = {
    val v = completeSize.toDouble / fromPgn.num.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

  def link(taskId: String) = fromPgn.link(taskId)

  def finishFromPgn(game: Game, userId: String): FromPgnWithResult = copy(
    result = {
      result.fold(
        List(
          FromPgnResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        )
      ) { r =>
          r :+ FromPgnResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        }
    }.some
  )

  def playerUserId(p: Player) = {
    p.aiLevel.fold(
      p.userId | User.anonymous
    ) { level => s"A.I. level $level" }
  }

}

case class FromPgnsWithResult(fromPgns: List[FromPgnWithResult]) extends TaskItemResult {

  def finishFromPgn(game: Game, userId: String): FromPgnsWithResult = copy(
    fromPgns = fromPgns.map { fp =>
      if (fp.sameGame(game, userId)) fp.finishFromPgn(game, userId) else fp
    }
  )

  def firstUnComplete = fromPgns.find(!_.isComplete) orElse fromPgns.headOption

  def findOrHead(hashId: String) = fromPgns.find(_.fromPgn.hashMD5 == hashId) orElse fromPgns.headOption

  def findFromPgn(fromPgn: FromPgnItem) =
    fromPgns.find(_.fromPgn == fromPgn) err s"can not find fromPgn of $fromPgn"

  def num = fromPgns.map(_.completeSize()).sum

  def total = fromPgns.map(_.fromPgn.num).sum

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class FromOpeningdbItem(
    opening: EitherOpening,
    openingdbExcludeWhiteBlunder: Option[Boolean] = None,
    openingdbExcludeBlackBlunder: Option[Boolean] = None,
    openingdbExcludeWhiteJscx: Option[Boolean] = None,
    openingdbExcludeBlackJscx: Option[Boolean] = None,
    openingdbCanOff: Option[Boolean] = None,
    clock: Clock.Config,
    num: Int,
    isAi: Option[Boolean],
    aiLevel: Option[Int],
    color: Option[Color],
    chessStatus: Option[String],
    canTakeback: Option[Boolean]
) {

  val pgnSetup = opening match {
    case Left(pgn) => PgnParser.preprocess(pgn) match {
      case Success((replay, initialFen)) => {
        if (replay.setup.situation.playable(true) && replay.state.situation.playable(true))
          new PgnSetup(replay.state.turns, initialFen, opening)
        else sys.error(s"can not parse pgn $pgn")
      }
      case Failure(_) => sys.error(s"can not parse pgn $pgn")
    }
    case Right(openingdbId) => {
      val o = OpeningDBHelper.openingFromCache(openingdbId)
      PgnSetup(
        o.startedAtTurnNext,
        o.initialFen,
        opening,
        openingdbExcludeWhiteBlunder,
        openingdbExcludeBlackBlunder,
        openingdbExcludeWhiteJscx,
        openingdbExcludeBlackJscx,
        openingdbCanOff
      )
    }
  }

  def openingParam = opening match {
    case Left(pgn) => s"pgn=${URLEncoder.encode(pgn, "UTF-8")}"
    case Right(openingdbId) => s"openingdbId=$openingdbId&openingdbExcludeWhiteBlunder=${openingdbExcludeWhiteBlunder | true}&openingdbExcludeBlackBlunder=${openingdbExcludeBlackBlunder | true}&openingdbExcludeWhiteJscx=${openingdbExcludeWhiteJscx | true}&openingdbExcludeBlackJscx=${openingdbExcludeBlackJscx | true}&openingdbCanOff=${openingdbCanOff | false}"
  }

  def link(taskId: String) =
    s"/setup/ai/custom?${openingParam}&limit=${clock.limitSeconds}&increment=${clock.incrementSeconds}&aiLevel=${aiLevel | 1}${color.??(c => s"&color=${c.name}")}${canTakeback.??(t => s"&canTakeback=$t")}"

  def hashMD5: String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest(toString().getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def ai: Boolean = isAi.??(x => x)

  def unique = this.toString

  override def toString: String = s"${openingParam} ${clock.show} ${isAi | false} ${aiLevel | 1}${color.??(c => s" ${c.name}")}${chessStatus.??(c => s" $c")}${canTakeback.??(t => s" canTakeback:$t")}"

}

case class FromOpeningdbResult(gameId: Game.ID, white: User.ID, black: User.ID, winnerColor: Option[chess.Color], userId: Option[User.ID]) {

  def storeUser = userId.isDefined

  def isWin = userId match {
    case None => none[Boolean]
    case Some(uid) => {
      winnerColor match {
        case None => none[Boolean]
        case Some(c) => c match {
          case chess.Color.White => Some(uid == white)
          case chess.Color.Black => Some(uid == black)
        }
      }
    }
  }

  def isWinOrFalse = storeUser && (isWin | false)

  def isDraw = userId match {
    case None => none[Boolean]
    case Some(_) => Some(winnerColor.isEmpty)
  }

  def isDrawOrFalse = storeUser && (isDraw | false)

  def isWinOrDraw = isWinOrFalse || isDrawOrFalse

  def resultText =
    userId.map { _ =>
      winnerColor match {
        case None => "1/2-1/2"
        case Some(c) => c match {
          case chess.Color.White => "1-0"
          case chess.Color.Black => "0-1"
        }
      }
    }

}

case class FromOpeningdbWithResult(fromOpeningdb: FromOpeningdbItem, result: Option[List[FromOpeningdbResult]]) {

  def unique = fromOpeningdb.unique

  def ai = fromOpeningdb.ai

  def sameGame(game: Game, userId: String) = {
    game.initialPgn.??(ip => sameOpeningdb(ip.opening)) && sameClock(game.clock.??(_.config.show)) && sameAi(game) && sameColor(game, userId) && sameOpeningdbConfig(game) && sameCanTakeback(game)
  }

  def sameOpeningdb(opening: EitherOpening) = fromOpeningdb.opening == opening

  def sameClock(clock: String) = clock == fromOpeningdb.clock.show

  def sameAi(game: Game) = game.nonAi || (game.hasAi && ai && sameAiLevel(game.aiLevel))

  def sameAiLevel(level: Option[Int]) = fromOpeningdb.aiLevel.isEmpty || level.??(fromOpeningdb.aiLevel.has(_))

  def sameColor(game: Game, userId: String) = fromOpeningdb.color.isEmpty || fromOpeningdb.color.?? { c => game.playerByUserId(userId).??(_.color == c) }

  def sameCanTakeback(game: Game) = fromOpeningdb.canTakeback.isEmpty || fromOpeningdb.canTakeback.??(game.canTakeback.has(_))

  def sameOpeningdbConfig(game: Game) = sameOpeningdbExcludeWhiteBlunder(game) && sameOpeningdbExcludeBlackBlunder(game) && sameOpeningdbExcludeWhiteJscx(game) && sameOpeningdbExcludeBlackJscx(game) && sameOpeningdbCanOff(game)

  def sameOpeningdbExcludeWhiteBlunder(game: Game) = fromOpeningdb.openingdbExcludeWhiteBlunder.isEmpty || fromOpeningdb.openingdbExcludeWhiteBlunder.??(v => game.initialPgn.exists(_.openingdbExcludeWhiteBlunder.has(v)))
  def sameOpeningdbExcludeBlackBlunder(game: Game) = fromOpeningdb.openingdbExcludeBlackBlunder.isEmpty || fromOpeningdb.openingdbExcludeBlackBlunder.??(v => game.initialPgn.exists(_.openingdbExcludeBlackBlunder.has(v)))
  def sameOpeningdbExcludeWhiteJscx(game: Game) = fromOpeningdb.openingdbExcludeWhiteJscx.isEmpty || fromOpeningdb.openingdbExcludeWhiteJscx.??(v => game.initialPgn.exists(_.openingdbExcludeWhiteJscx.has(v)))
  def sameOpeningdbExcludeBlackJscx(game: Game) = fromOpeningdb.openingdbExcludeBlackJscx.isEmpty || fromOpeningdb.openingdbExcludeBlackJscx.??(v => game.initialPgn.exists(_.openingdbExcludeBlackJscx.has(v)))
  def sameOpeningdbCanOff(game: Game) = fromOpeningdb.openingdbCanOff.isEmpty || fromOpeningdb.openingdbCanOff.??(v => game.initialPgn.exists(_.openingdbCanOff.has(v)))

  def sameChessStatus(result: FromOpeningdbResult) = !result.storeUser || fromOpeningdb.chessStatus.isEmpty || fromOpeningdb.chessStatus.?? {
    case "all" => true
    case "win" => result.isWinOrFalse
    case "winOrDraw" => result.isWinOrDraw
  }

  def completeSize() = result.??(_.count(r => sameChessStatus(r)))

  def isComplete() = completeSize >= fromOpeningdb.num

  def progress = {
    val v = completeSize.toDouble / fromOpeningdb.num.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

  def link(taskId: String) = fromOpeningdb.link(taskId)

  def finishFromOpeningdb(game: Game, userId: String): FromOpeningdbWithResult = copy(
    result = {
      result.fold(
        List(
          FromOpeningdbResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        )
      ) { r =>
          r :+ FromOpeningdbResult(
            game.id,
            playerUserId(game.whitePlayer),
            playerUserId(game.blackPlayer),
            game.winnerColor,
            userId.some
          )
        }
    }.some
  )

  def playerUserId(p: Player) = {
    p.aiLevel.fold(
      p.userId | User.anonymous
    ) { level => s"A.I. level $level" }
  }

}

case class FromOpeningdbsWithResult(fromOpeningdbs: List[FromOpeningdbWithResult]) extends TaskItemResult {

  def finishFromOpeningdb(game: Game, userId: String): FromOpeningdbsWithResult = copy(
    fromOpeningdbs = fromOpeningdbs.map { fp =>
      if (fp.sameGame(game, userId)) fp.finishFromOpeningdb(game, userId) else fp
    }
  )

  def firstUnComplete = fromOpeningdbs.find(!_.isComplete) orElse fromOpeningdbs.headOption

  def findOrHead(hashId: String) = fromOpeningdbs.find(_.fromOpeningdb.hashMD5 == hashId) orElse fromOpeningdbs.headOption

  def findFromOpeningdb(fromOpeningdb: FromOpeningdbItem) =
    fromOpeningdbs.find(_.fromOpeningdb == fromOpeningdb) err s"can not find fromOpeningdb of $fromOpeningdb"

  def num = fromOpeningdbs.map(_.completeSize()).sum

  def total = fromOpeningdbs.map(_.fromOpeningdb.num).sum

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}

case class TrainGameItem(gameId: Game.ID, white: User.ID, black: User.ID, variant: Variant, mode: Mode, clock: Clock.Config, fen: Option[String]) {
  def speed = chess.Speed(clock)

  def perfType = PerfPicker.perfType(speed, variant, none) | PerfType.Standard

  def sameGame(game: Game) = game.id == gameId
}

case class TrainGameResult(winner: Option[Color], finishAt: DateTime)

case class TrainGameWithResult(trainGame: TrainGameItem, result: Option[TrainGameResult]) extends TaskItemResult {

  def colorOf(userId: Option[String]) =
    if (userId.has(trainGame.white))
      chess.Color.White.some
    else if (userId.has(trainGame.black))
      chess.Color.Black.some
    else None

  def opponentOf(userId: User.ID) = {
    colorOf(userId.some) map { color =>
      color.fold(trainGame.black, trainGame.white)
    }
  }

  def sameGame(game: Game) = trainGame.sameGame(game)

  def num = 1

  def total = 1

  def isComplete = result.isDefined

  def progress = if (isComplete) 100d else 0d

  def finishTrainGame(game: Game): TrainGameWithResult =
    copy(
      result = result.getOrElse(TrainGameResult(game.winnerColor, DateTime.now)).some
    )

}

case class TeamTestItem(stuTestId: String, num: Int, total: Int) {

  def isComplete() = num >= total

  def progress = {
    val v = num.toDouble / total.toDouble
    Math.min(100.0, Math.round(v * 10000).toDouble / 100).max(0.0)
  }

}
