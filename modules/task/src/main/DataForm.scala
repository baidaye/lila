package lila.task

import java.net.URLDecoder
import chess.{ Clock, Mode }
import chess.format.Forsyth
import chess.variant.Variant
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import lila.common.Form._
import lila.task.TTask.TTaskItemType
import lila.user.User
import org.joda.time.DateTime

case class DataForm() {

  import DataForm._

  val commonItemMapping: Mapping[TTaskCommonData] =
    mapping(
      "num" -> number(min = 1, max = 1000),
      "isNumber" -> boolean,
      "extra" -> optional(mapping(
        "cond" -> optional(nonEmptyText)
      )(CommonItemExtra.apply)(CommonItemExtra.unapply))
    )(TTaskCommonData.apply)(TTaskCommonData.unapply)

  val puzzleRushMapping: Mapping[PuzzleRushData] =
    mapping(
      "mode" -> nonEmptyText,
      "num" -> number(min = 1, max = 1000),
      "isNumber" -> boolean,
      "extra" -> optional(mapping(
        "cond" -> optional(nonEmptyText)
      )(CommonItemExtra.apply)(CommonItemExtra.unapply))
    )(PuzzleRushData.apply)(PuzzleRushData.unapply)

  val capsulePuzzleMapping: Mapping[PuzzleCapsuleData] =
    mapping(
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,
      "puzzles" -> list(mapping(
        "id" -> number(min = 10000, max = 99999999),
        "fen" -> nonEmptyText,
        "color" -> nonEmptyText,
        "lastMove" -> optional(nonEmptyText),
        "lines" -> nonEmptyText
      )(MiniPuzzleData.apply)(MiniPuzzleData.unapply))
    )(PuzzleCapsuleData.apply)(PuzzleCapsuleData.unapply)

  val recallGameMapping: Mapping[RecallGameData] =
    mapping(
      "name" -> optional(nonEmptyText),
      "root" -> nonEmptyText,
      "pgn" -> nonEmptyText,
      "moves" -> list(mapping(
        "index" -> number(min = 1, max = 500),
        "white" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply)),
        "black" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply))
      )(Move.apply)(Move.unapply)),
      "turns" -> optional(number(min = 1, max = 500)),
      "color" -> optional(nonEmptyText),
      "orient" -> optional(nonEmptyText),
      "title" -> optional(nonEmptyText)
    )(RecallGameData.apply)(RecallGameData.unapply)

  val replayGameMapping: Mapping[ReplayGameData] =
    mapping(
      "chapterLink" -> nonEmptyText,
      "name" -> nonEmptyText,
      "root" -> nonEmptyText,
      "pgn" -> optional(nonEmptyText),
      "color" -> optional(nonEmptyText),
      "moves" -> list(mapping(
        "index" -> number(min = 1, max = 500),
        "white" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply)),
        "black" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply))
      )(Move.apply)(Move.unapply))
    )(ReplayGameData.apply)(ReplayGameData.unapply)

  val distinguishGameMapping: Mapping[DistinguishGameData] =
    mapping(
      "name" -> optional(nonEmptyText),
      "root" -> nonEmptyText,
      "pgn" -> nonEmptyText,
      "moves" -> list(mapping(
        "index" -> number(min = 1, max = 500),
        "white" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply)),
        "black" -> optional(mapping(
          "san" -> nonEmptyText,
          "uci" -> nonEmptyText,
          "fen" -> nonEmptyText
        )(Node.apply)(Node.unapply))
      )(Move.apply)(Move.unapply)),
      "rightTurns" -> number(min = 1, max = 500),
      "turns" -> optional(number(min = 1, max = 500)),
      "orientation" -> optional(nonEmptyText),
      "title" -> optional(nonEmptyText)
    )(DistinguishGameData.apply)(DistinguishGameData.unapply)

  val gameMapping: Mapping[GameData] =
    mapping(
      "speed" -> nonEmptyText,
      "num" -> number(min = 1, max = 1000),
      "isNumber" -> boolean
    )(GameData.apply)(GameData.unapply)

  val fromPositionMapping =
    mapping(
      "fen" -> nonEmptyText,
      "clockTime" -> numberInDouble(clockTimeChoices),
      "clockIncrement" -> numberIn(clockIncrementChoices),
      "num" -> number(min = 1, max = 20),
      "isAi" -> number(min = 0, max = 1),
      "aiLevel" -> number(min = 1, max = 10),
      "color" -> nonEmptyText,
      "chessStatus" -> nonEmptyText,
      "canTakeback" -> number(min = 0, max = 1)
    )(FromPositionData.apply)(FromPositionData.unapply)

  val fromPgnMapping =
    mapping(
      "pgn" -> optional(nonEmptyText),
      "openingdbId" -> optional(nonEmptyText),
      "clockTime" -> numberInDouble(clockTimeChoices),
      "clockIncrement" -> numberIn(clockIncrementChoices),
      "num" -> number(min = 1, max = 20),
      "isAi" -> number(min = 0, max = 1),
      "aiLevel" -> number(min = 1, max = 10),
      "color" -> nonEmptyText,
      "chessStatus" -> nonEmptyText,
      "canTakeback" -> number(min = 0, max = 1)
    )(FromPgnData.apply)(FromPgnData.unapply)

  val fromOpeningdbMapping =
    mapping(
      "pgn" -> optional(nonEmptyText),
      "openingdbId" -> optional(nonEmptyText),
      "openingdbExcludeWhiteBlunder" -> optional(boolean),
      "openingdbExcludeBlackBlunder" -> optional(boolean),
      "openingdbExcludeWhiteJscx" -> optional(boolean),
      "openingdbExcludeBlackJscx" -> optional(boolean),
      "openingdbCanOff" -> optional(boolean),
      "clockTime" -> numberInDouble(clockTimeChoices),
      "clockIncrement" -> numberIn(clockIncrementChoices),
      "num" -> number(min = 1, max = 20),
      "isAi" -> number(min = 0, max = 1),
      "aiLevel" -> number(min = 1, max = 10),
      "color" -> nonEmptyText,
      "chessStatus" -> nonEmptyText,
      "canTakeback" -> number(min = 0, max = 1)
    )(FromOpeningdbData.apply)(FromOpeningdbData.unapply)

  val taskItemMapping =
    mapping(
      "puzzle" -> optional(commonItemMapping),
      "themePuzzle" -> optional(commonItemMapping),
      "coordTrain" -> optional(commonItemMapping),
      "puzzleRush" -> optional(puzzleRushMapping),
      "capsulePuzzle" -> optional(list(capsulePuzzleMapping)),
      "replayGame" -> optional(list(replayGameMapping)),
      "recallGame" -> optional(list(recallGameMapping)),
      "distinguishGame" -> optional(list(distinguishGameMapping)),
      "game" -> optional(gameMapping),
      "fromPosition" -> optional(list(fromPositionMapping)),
      "fromPgn" -> optional(list(fromPgnMapping)),
      "fromOpeningdb" -> optional(list(fromOpeningdbMapping))
    )(TTaskItemData.apply)(TTaskItemData.unapply)

  val search = Form(
    mapping(
      "source" -> optional(stringIn(TTask.Source.selects)),
      "status" -> optional(stringIn(TTask.Status.selects))
    )(SearchData.apply)(SearchData.unapply)
  )

  val create = Form(mapping(
    "userIds" -> list(lila.user.DataForm.historicalUsernameField),
    "planAt" -> optional(lila.common.Form.futureDateTime),
    "startAt" -> optional(lila.common.Form.futureDateTime),
    "deadlineAt" -> optional(lila.common.Form.futureDateTime),
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "remark" -> optional(text(minLength = 0, maxLength = 100)),
    "itemType" -> stringIn(TTaskItemType.selects),
    "item" -> taskItemMapping,
    "sourceRel" -> mapping(
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,
      "source" -> stringIn(TTask.Source.selects)
    )(TTaskSourceRelData.apply)(TTaskSourceRelData.unapply),
    "coinRule" -> optional(number(min = 0, max = 10000))
  )(TTaskData.apply)(TTaskData.unapply))

  val tplCreate = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "remark" -> optional(text(minLength = 0, maxLength = 100)),
    "item" -> taskItemMapping,
    "itemType" -> stringIn(TTaskItemType.selects),
    "sourceRel" -> mapping(
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,
      "source" -> stringIn(TTask.Source.selects)
    )(TTaskSourceRelData.apply)(TTaskSourceRelData.unapply),
    "coinRule" -> optional(number(min = 0, max = 10000)),
    "deadlineAt" -> optional(lila.common.Form.futureDateTime)
  )(TTaskTemplateData.apply)(TTaskTemplateData.unapply))

  val teamClockInCreate = Form(mapping(
    "name" -> nonEmptyText(minLength = 2, maxLength = 50),
    "remark" -> optional(text(minLength = 0, maxLength = 100)),
    "item" -> taskItemMapping,
    "itemType" -> stringIn(TTaskItemType.selects),
    "sourceRel" -> mapping(
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,
      "source" -> stringIn(TTask.Source.selects)
    )(TTaskSourceRelData.apply)(TTaskSourceRelData.unapply),
    "coinRule" -> optional(number(min = 0, max = 10000)),
    "startDate" -> ISODate.isoDate,
    "period" -> nonEmptyText,
    "tasks" -> list(mapping(
      "name" -> nonEmptyText,
      "index" -> number(min = 1, max = 100),
      "date" -> ISODate.isoDate,
      "status" -> nonEmptyText
    )(TeamClockInTaskData.apply)(TeamClockInTaskData.unapply))
  )(TeamClockInTaskSettingData.apply)(TeamClockInTaskSettingData.unapply))

  val trainGame = Form(
    mapping(
      "white" -> lila.user.DataForm.historicalUsernameField,
      "black" -> lila.user.DataForm.historicalUsernameField,
      "planAt" -> optional(lila.common.Form.futureDateTime),
      "startAt" -> optional(lila.common.Form.futureDateTime),
      "deadlineAt" -> optional(lila.common.Form.futureDateTime),
      "variant" -> number.verifying(List(chess.variant.Standard.id, chess.variant.FromPosition.id) contains _),
      "time" -> of[Double].verifying(TrainGameData.validateTime _),
      "increment" -> number.verifying(TrainGameData.validateIncrement _),
      "rated" -> boolean,
      "fen" -> optional(nonEmptyText),
      "sourceRel" -> mapping(
        "id" -> nonEmptyText,
        "name" -> nonEmptyText,
        "source" -> stringIn(TTask.Source.selects)
      )(TTaskSourceRelData.apply)(TTaskSourceRelData.unapply)
    )(TrainGameData.apply)(TrainGameData.unapply)
      .verifying("Invalid clock", _.validClock)
      .verifying("invalidFen", _.validFen)
  )

  val planAtForm = Form(single("planAt" -> lila.common.Form.futureDateTime))

}

object DataForm {

  val phase = List("Opening" -> "开局", "MiddleGame" -> "中局", "EndingGame" -> "残局")
  val color = List("White" -> "白方", "Black" -> "黑方")
  val selector = List("round" -> "循环", "random" -> "随机")

  val coordTrainOrient = List(1 -> "白方", 2 -> "随机", 3 -> "黑方")
  val booleanChoices = List(0 -> "否", 1 -> "是")
  val coordTrainMode = List("basic" -> "坐标训练", "move" -> "着法训练", "combat" -> "实战着发")

  val clockTimes: Seq[Double] = Seq(3d, 5d) ++ (10d to 30d by 5d) ++ (40d to 60d by 10d)
  //val clockTimes: Seq[Double] = Seq(0d, 1 / 4d, 1 / 2d, 3 / 4d, 1d, 3 / 2d) ++ (2d to 7d by 1d) ++ (10d to 30d by 5d) ++ (40d to 60d by 10d)

  val puzzleRushMode = List("threeMinutes" -> "3分钟", "fiveMinutes" -> "5分钟", "survival" -> "生存", "custom" -> "自定义")

  private def formatLimit(l: Double) =
    chess.Clock.Config(l * 60 toInt, 0).limitString + {
      if (l <= 1) " 分钟" else " 分钟"
    }
  val clockTimeChoices = optionsDouble(clockTimes, formatLimit)
  val clockIncrements = (0 to 2 by 1) ++ Seq(3, 5) ++ (10 to 30 by 5) ++ (40 to 60 by 10)
  //val clockIncrements = (0 to 2 by 1) ++ (3 to 7) ++ (10 to 30 by 5) ++ (40 to 60 by 10)
  val clockIncrementChoices = options(clockIncrements, "%d 秒")

  def urlToMap(queryString: String) = {
    val qs = URLDecoder.decode(queryString, "UTF-8")
    qs.split("&").map { kvString =>
      val kv = kvString.split("=")
      if (kv.length > 1) {
        kv(0) -> kv(1)
      } else {
        kv(0) -> ""
      }
    }.filterNot(_._2 == "")
  }

  def toTaskItem(item: TTaskItemData) = {
    TTaskItem(
      puzzle = item.puzzle.map { puzzle =>
        PuzzleWithResult(
          PuzzleItem(puzzle.num, puzzle.isNumber, puzzle.extra)
        )
      },
      themePuzzle = item.themePuzzle.map { themePuzzle =>
        ThemePuzzleWithResult(
          ThemePuzzleItem(themePuzzle.num, themePuzzle.isNumber, themePuzzle.extra)
        )
      },
      coordTrain = item.coordTrain.map { coordTrain =>
        CoordTrainWithResult(
          CoordTrainItem(coordTrain.num, coordTrain.isNumber, coordTrain.extra)
        )
      },
      puzzleRush = item.puzzleRush.map { puzzleRush =>
        PuzzleRushWithResult(
          PuzzleRushItem(puzzleRush.mode, puzzleRush.num, puzzleRush.isNumber, puzzleRush.extra)
        )
      },
      capsulePuzzle = item.capsulePuzzle.map { capsulePuzzles =>
        PuzzleCapsuleWithResult(
          capsules = capsulePuzzles.map { cp => PuzzleCapsule(cp.id, cp.name, cp.puzzles.map(_.id)) },
          puzzles = {
            capsulePuzzles.flatMap(_.puzzles).map { puzzle =>
              MiniPuzzle(
                id = puzzle.id,
                fen = puzzle.fen,
                color = chess.Color(puzzle.color) err "error color",
                lastMove = puzzle.lastMove,
                lines = puzzle.lines
              )
            }.distinct.map(MiniPuzzleWithResult(_, None))
          }
        )
      },
      replayGame = item.replayGames.map { replayGames =>
        ReplayGamesWithResult(
          replayGames.map { r =>
            ReplayGameWithResult(
              ReplayGameItem(
                chapterLink = r.chapterLink,
                name = r.name,
                root = r.root,
                pgn = r.pgn,
                color = r.color.map(chess.Color(_) err "error color"),
                moves = r.moves
              ),
              None
            )
          }
        )
      },
      recallGame = item.recallGames.map { recallGames =>
        RecallGamesWithResult(
          recallGames.map { r =>
            RecallGameWithResult(
              RecallGameItem(
                name = r.name,
                root = r.root,
                pgn = r.pgn,
                moves = r.moves.some,
                turns = r.turns,
                color = r.color.map(chess.Color(_) err "error color"),
                orient = r.orient,
                title = r.title
              ),
              None
            )
          }
        )
      },
      distinguishGame = item.distinguishGames.map { distinguishGames =>
        DistinguishGamesWithResult(
          distinguishGames.map { r =>
            DistinguishGameWithResult(
              DistinguishGameItem(
                name = r.name,
                root = r.root,
                pgn = r.pgn,
                moves = r.moves.some,
                rightTurns = r.rightTurns,
                turns = r.turns,
                orient = r.orientation,
                title = r.title
              ),
              None
            )
          }
        )
      },
      game = item.game.map { gm =>
        GameWithResult(
          GameItem(chess.Speed.all.find(_.key == gm.speed) err s"can not apply speed ${gm.speed}", gm.num, gm.isNumber)
        )
      },
      fromPosition = item.fromPositions.map { fromPositions =>
        FromPositionsWithResult(
          fromPositions.map { f =>
            FromPositionWithResult(
              FromPositionItem(
                fen = Forsyth >> chess.Game(chess.variant.FromPosition.some, f.fen.some),
                clock = chess.Clock.Config((f.clockTime * 60).toInt, f.clockIncrement),
                num = f.num,
                isAi = (f.isAi == 1).some,
                aiLevel = f.aiLevel.some,
                color = if (f.color == "random") None else chess.Color(f.color),
                chessStatus = f.chessStatus.some,
                canTakeback = (f.canTakeback == 1).some
              ),
              None
            )
          }
        )
      },
      fromPgn = item.fromPgns.map { fromPgns =>
        FromPgnsWithResult(
          fromPgns.map { f =>
            FromPgnWithResult(
              FromPgnItem(
                opening = f.pgn match {
                  case Some(p) => Left(p)
                  case None => f.openingdbId match {
                    case Some(oid) => Right(oid)
                    case None => sys.error("both pgn openingdbId are empty")
                  }
                },
                clock = chess.Clock.Config((f.clockTime * 60).toInt, f.clockIncrement),
                num = f.num,
                isAi = (f.isAi == 1).some,
                aiLevel = f.aiLevel.some,
                color = if (f.color == "random") None else chess.Color(f.color),
                chessStatus = f.chessStatus.some,
                canTakeback = (f.canTakeback == 1).some
              ),
              None
            )
          }
        )
      },
      fromOpeningdb = item.fromOpeningdbs.map { fromOpeningdbs =>
        FromOpeningdbsWithResult(
          fromOpeningdbs.map { f =>
            FromOpeningdbWithResult(
              FromOpeningdbItem(
                opening = f.pgn match {
                  case Some(p) => Left(p)
                  case None => f.openingdbId match {
                    case Some(oid) => Right(oid)
                    case None => sys.error("both pgn openingdbId are empty")
                  }
                },
                openingdbExcludeWhiteBlunder = f.openingdbExcludeWhiteBlunder,
                openingdbExcludeBlackBlunder = f.openingdbExcludeBlackBlunder,
                openingdbExcludeWhiteJscx = f.openingdbExcludeWhiteJscx,
                openingdbExcludeBlackJscx = f.openingdbExcludeBlackJscx,
                openingdbCanOff = f.openingdbCanOff,
                clock = chess.Clock.Config((f.clockTime * 60).toInt, f.clockIncrement),
                num = f.num,
                isAi = (f.isAi == 1).some,
                aiLevel = f.aiLevel.some,
                color = if (f.color == "random") None else chess.Color(f.color),
                chessStatus = f.chessStatus.some,
                canTakeback = (f.canTakeback == 1).some
              ),
              None
            )
          }
        )
      }
    )
  }

}

case class TeamClockInTaskSettingData(
    name: String,
    remark: Option[String],
    item: TTaskItemData,
    itemType: String,
    sourceRel: TTaskSourceRelData,
    coinRule: Option[Int],
    startDate: DateTime,
    period: String,
    tasks: List[TeamClockInTaskData]
) {

  def toTaskTemplate(creator: User): TTaskTemplate =
    TTaskTemplate.make(
      name = name,
      remark = remark,
      item = DataForm.toTaskItem(item),
      itemType = TTaskItemType(itemType),
      sourceRel = TTask.SourceRel(
        id = sourceRel.id,
        name = sourceRel.name,
        source = TTask.Source(sourceRel.source)
      ),
      coinTeam = coinRule.map(_ => creator.belongTeamIdValue),
      coinRule = coinRule,
      deadlineAt = None,
      createdBy = creator.id
    )

}

case class TeamClockInTaskData(
    name: String,
    index: Int,
    date: DateTime,
    status: String
)

case class TTaskTemplateData(
    name: String,
    remark: Option[String],
    item: TTaskItemData,
    itemType: String,
    sourceRel: TTaskSourceRelData,
    coinRule: Option[Int],
    deadlineAt: Option[DateTime]
) {

  def toTaskTemplate(creator: User): TTaskTemplate =
    TTaskTemplate.make(
      name = name,
      remark = remark,
      item = DataForm.toTaskItem(item),
      itemType = TTaskItemType(itemType),
      sourceRel = TTask.SourceRel(
        id = sourceRel.id,
        name = sourceRel.name,
        source = TTask.Source(sourceRel.source)
      ),
      coinTeam = coinRule.map(_ => creator.belongTeamIdValue),
      coinRule = coinRule,
      deadlineAt = deadlineAt,
      createdBy = creator.id
    )

}

case class TTaskData(
    userIds: List[String],
    planAt: Option[DateTime],
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    name: String,
    remark: Option[String],
    itemType: String,
    item: TTaskItemData,
    sourceRel: TTaskSourceRelData,
    coinRule: Option[Int]
) {

  def toTasks(creator: User): List[TTask] =
    userIds.map { userId =>
      TTask.make(
        userIds = List(userId),
        name = name,
        remark = remark,
        item = DataForm.toTaskItem(item),
        itemType = TTaskItemType(itemType),
        sourceRel = TTask.SourceRel(
          id = sourceRel.id,
          name = sourceRel.name,
          source = TTask.Source(sourceRel.source)
        ),
        coinTeam = coinRule.map(_ => creator.belongTeamIdValue),
        coinRule = coinRule,
        coinDiff = None,
        tplId = None,
        planAt = planAt,
        startAt = startAt,
        deadlineAt = deadlineAt,
        createdBy = creator.id
      )
    }

}

case class TTaskSourceRelData(id: String, name: String, source: String)
case class TTaskCommonData(num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None)
case class PuzzleRushData(mode: String, num: Int, isNumber: Boolean, extra: Option[CommonItemExtra] = None)
case class GameData(speed: String, num: Int, isNumber: Boolean)
case class PuzzleCapsuleData(id: String, name: String, puzzles: List[MiniPuzzleData])
case class ReplayGameData(chapterLink: String, name: String, root: String, pgn: Option[String], color: Option[String], moves: List[Move])
case class RecallGameData(name: Option[String], root: String, pgn: String, moves: List[Move], turns: Option[Int], color: Option[String], orient: Option[String], title: Option[String])
case class DistinguishGameData(name: Option[String], root: String, pgn: String, moves: List[Move], rightTurns: Int, turns: Option[Int], orientation: Option[String], title: Option[String])
case class FromPositionData(fen: String, clockTime: Double, clockIncrement: Int, num: Int, isAi: Int, aiLevel: Int, color: String, chessStatus: String, canTakeback: Int)
case class FromPgnData(pgn: Option[String], openingdbId: Option[String], clockTime: Double, clockIncrement: Int, num: Int, isAi: Int, aiLevel: Int, color: String, chessStatus: String, canTakeback: Int)
case class FromOpeningdbData(
    pgn: Option[String],
    openingdbId: Option[String],
    openingdbExcludeWhiteBlunder: Option[Boolean],
    openingdbExcludeBlackBlunder: Option[Boolean],
    openingdbExcludeWhiteJscx: Option[Boolean],
    openingdbExcludeBlackJscx: Option[Boolean],
    openingdbCanOff: Option[Boolean],
    clockTime: Double,
    clockIncrement: Int,
    num: Int,
    isAi: Int,
    aiLevel: Int,
    color: String,
    chessStatus: String,
    canTakeback: Int
)

case class MiniPuzzleData(id: Int, fen: String, color: String, lastMove: Option[String], lines: String)
case class TTaskItemData(
    puzzle: Option[TTaskCommonData],
    themePuzzle: Option[TTaskCommonData],
    coordTrain: Option[TTaskCommonData],
    puzzleRush: Option[PuzzleRushData],
    capsulePuzzle: Option[List[PuzzleCapsuleData]],
    replayGames: Option[List[ReplayGameData]],
    recallGames: Option[List[RecallGameData]],
    distinguishGames: Option[List[DistinguishGameData]],
    game: Option[GameData],
    fromPositions: Option[List[FromPositionData]],
    fromPgns: Option[List[FromPgnData]],
    fromOpeningdbs: Option[List[FromOpeningdbData]]
)

case class TrainGameData(
    white: User.ID,
    black: User.ID,
    planAt: Option[DateTime],
    startAt: Option[DateTime],
    deadlineAt: Option[DateTime],
    variant: Int,
    time: Double,
    increment: Int,
    rated: Boolean,
    fen: Option[String],
    sourceRel: TTaskSourceRelData
) {

  def clockConfig = Clock.Config((time * 60).toInt, increment)

  def validClock = time + increment > 0

  def validFen = variant != chess.variant.FromPosition.id || {
    fen ?? { f => ~(Forsyth <<< f).map(_.situation.playable(false)) }
  }

  def toTask(gameId: String, creator: User): TTask = {
    TTask.make(
      userIds = List(white, black),
      name = s"${clockConfig.show} • ${if (rated) "计算等级分" else "不计算等级分"}",
      remark = None,
      item = TTaskItem(
        trainGame = TrainGameWithResult(
          trainGame = TrainGameItem(
            gameId,
            white,
            black,
            Variant.orDefault(variant),
            if (rated) Mode.Rated else Mode.Casual,
            clockConfig,
            if (variant == chess.variant.FromPosition.id) fen else None
          ),
          result = None
        ).some
      ),
      itemType = TTaskItemType.TrainGameItem,
      sourceRel = TTask.SourceRel(
        id = sourceRel.id,
        name = sourceRel.name,
        source = TTask.Source(sourceRel.source)
      ),
      coinTeam = None,
      coinRule = None,
      coinDiff = None,
      tplId = None,
      planAt = planAt,
      startAt = startAt,
      deadlineAt = deadlineAt,
      createdBy = creator.id
    )
  }

}

case class SearchData(source: Option[String], status: Option[String]) {

  def sc = source.filter(_.nonEmpty).map { s => TTask.Source(s) }

  def st = status.filter(_.nonEmpty).map { s => TTask.Status(s) }

}

object TrainGameData {
  private val timeMin = 0
  private val timeMax = 180
  private val acceptableFractions = Set(1 / 4d, 1 / 2d, 3 / 4d, 3 / 2d)
  def validateTime(t: Double) =
    t >= timeMin && t <= timeMax && (t.isWhole || acceptableFractions(t))

  private val incrementMin = 0
  private val incrementMax = 180
  def validateIncrement(i: Int) = i >= incrementMin && i <= incrementMax

}
