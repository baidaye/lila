package lila.task

import chess.Color
import chess.format.Forsyth
import lila.game.{ Game, GameRepo }
import lila.common.LightUser
import lila.common.paginator.{ Paginator, PaginatorJson }
import lila.task.TTask.TTaskItemType
import play.api.libs.json._

final class JsonView(
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi,
    roundProxyGame: Game.ID => Fu[Option[Game]]
) {

  implicit val pagerWriter = Writes[TTask] { d => sampleInfo(d) }

  def page(pager: Paginator[TTask]) =
    PaginatorJson(pager)

  def list(tasks: List[TTask]) =
    JsArray(
      tasks.map { task =>
        sampleInfo(task)
      }
    )

  def info(task: TTask) = {
    sampleInfo(task) ++ Json.obj(
      "item" -> itemJson(task.itemType, task.item)
    )
  }

  def trainGameInfo(task: TTask) = {
    sampleInfo(task) ++ Json.obj(
      "item" -> itemJson(task.itemType, task.item)
    )
  }

  def trainGameTasks(tasks: List[TTask], markMap: Map[String, Option[String]]) = {
    val playerIds = tasks.flatMap(_.userIds)
    val gameIds = tasks.map(t => t.item.nonTrainGame.trainGame.gameId)
    for {
      games <- gameIds.map { id =>
        roundProxyGame(id) orElse GameRepo.game(id)
      }.sequenceFu.map(_.flatten)
      lightUsers <- lightUserApi.asyncMany(playerIds)
    } yield {
      JsArray(
        tasks.map { task =>
          val tg = task.item.nonTrainGame
          val game = games.find(g => g.id == tg.trainGame.gameId) err s"can not find game ${tg.trainGame.gameId}"
          val whiteLightUser = (lightUsers.find(_.exists(_.id == tg.trainGame.white)) | LightUser.fallback(tg.trainGame.white).some) | LightUser.fallback(tg.trainGame.white)
          val blackLightUser = (lightUsers.find(_.exists(_.id == tg.trainGame.black)) | LightUser.fallback(tg.trainGame.black).some) | LightUser.fallback(tg.trainGame.black)
          sampleInfo(task) ++ Json.obj(
            "trainGameWithResult" -> trainGameJson(tg, game, whiteLightUser, blackLightUser, markMap)
          )
        }
      )
    }
  }

  def sampleInfo(task: TTask): JsObject = {
    Json.obj(
      "id" -> task.id,
      "name" -> task.name,
      "remark" -> task.remark,
      "itemType" -> Json.obj(
        "id" -> task.itemType.id,
        "name" -> task.itemType.name
      ),
      "sourceRel" -> Json.obj(
        "id" -> task.sourceRel.id,
        "name" -> task.sourceRel.name,
        "source" -> Json.obj(
          "id" -> task.sourceRel.source.id,
          "name" -> task.sourceRel.source.name,
          "icon" -> task.sourceRel.source.icon
        )
      ),
      "status" -> Json.obj(
        "id" -> task.status.id,
        "name" -> task.status.name
      ),
      "num" -> task.num,
      "total" -> task.total,
      "link" -> s"/ttask/${task.id}/doTask",
      "progress" -> task.progress,
      "completed" -> task.isFinished,
      "createdAt" -> task.createdAt.toString("yyyy-MM-dd HH:mm"),
      "deadlineAt" -> task.deadlineAt.map(_.toString("MM月dd日 HH:mm"))
    )
  }

  def taskTemplateInfo(taskTemplate: TTaskTemplate): JsObject = {
    Json.obj(
      "id" -> taskTemplate.id,
      "name" -> taskTemplate.name,
      "remark" -> taskTemplate.remark,
      "itemType" -> Json.obj(
        "id" -> taskTemplate.itemType.id,
        "name" -> taskTemplate.itemType.name
      ),
      "sourceRel" -> Json.obj(
        "id" -> taskTemplate.sourceRel.id,
        "name" -> taskTemplate.sourceRel.name,
        "source" -> Json.obj(
          "id" -> taskTemplate.sourceRel.source.id,
          "name" -> taskTemplate.sourceRel.source.name
        )
      ),
      "createdAt" -> taskTemplate.createdAt.toString("yyyy-MM-dd HH:mm"),
      "item" -> itemJson(taskTemplate.itemType, taskTemplate.item)
    )
  }

  def itemJson(itemType: TTaskItemType, it: TTaskItem) =
    Json.obj(
      "puzzleWithResult" -> {
        it.puzzle.map { pz =>
          Json.obj(
            "puzzle" -> commonItemJson(pz.puzzle.isNumber, pz.puzzle.num),
            "result" -> commonResultJson(pz.result)
          )
        }
      },
      "themePuzzleWithResult" -> {
        it.themePuzzle.map { tp =>
          Json.obj(
            "themePuzzle" -> commonItemJson(tp.themePuzzle.isNumber, tp.themePuzzle.num, tp.themePuzzle.extra, tp.themePuzzle.buildTags().some),
            "result" -> commonResultJson(tp.result)
          )
        }
      },
      "coordTrainWithResult" -> {
        it.coordTrain.map { ct =>
          Json.obj(
            "coordTrain" -> commonItemJson(ct.coordTrain.isNumber, ct.coordTrain.num, ct.coordTrain.extra, ct.coordTrain.buildTags().some),
            "result" -> commonResultJson(ct.result)
          )
        }
      },
      "puzzleRushWithResult" -> {
        it.puzzleRush.map { pr =>
          Json.obj(
            "puzzleRush" -> (commonItemJson(pr.puzzleRush.isNumber, pr.puzzleRush.num, pr.puzzleRush.extra, pr.puzzleRush.buildTags().some) ++ Json.obj("mode" -> pr.puzzleRush.mode)),
            "result" -> commonResultJson(pr.result)
          )
        }
      },
      "gameWithResult" -> {
        it.game.map { gm =>
          Json.obj(
            "game" -> (commonItemJson(gm.gameItem.isNumber, gm.gameItem.num, None, gm.gameItem.buildTags().some) ++ Json.obj("speed" -> gm.gameItem.speed.key)),
            "result" -> commonResultJson(gm.result)
          )
        }
      },
      "capsulePuzzleWithResult" -> {
        it.capsulePuzzle.map { capsulePuzzleJson }
      },
      "replayGameWithResult" -> {
        it.replayGame.map { replayGameJson }
      },
      "recallGameWithResult" -> {
        it.recallGame.map { recallGameJson }
      },
      "distinguishGameWithResult" -> {
        it.distinguishGame.map { distinguishGameJson }
      },
      "fromPositionWithResult" -> {
        it.fromPosition.map { fromPositionJson }
      },
      "fromPgnWithResult" -> {
        it.fromPgn.map { fromPgnJson }
      },
      "fromOpeningdbWithResult" -> {
        it.fromOpeningdb.map { fromOpeningdbJson }
      }
    )

  private def commonItemJson(isNumber: Boolean, num: Int, extra: Option[CommonItemExtra] = None, tags: Option[List[String]] = None) =
    Json.obj(
      "isNumber" -> isNumber,
      "num" -> num,
      "extra" -> extra.map { extra =>
        Json.obj(
          "cond" -> extra.cond
        )
      },
      "tags" -> tags
    )

  private def commonResultJson(result: Option[CommonResult] = None) =
    result.map { result =>
      Json.obj(
        "num" -> result.num,
        "rating" -> result.rating,
        "nowRating" -> result.nowRating,
        "diffs" -> JsArray(
          result.diffs.map { diff =>
            Json.obj(
              "id" -> diff.id,
              "diff" -> diff.diff,
              "win" -> diff.win
            )
          }
        )
      )
    }

  private def capsulePuzzleJson(cp: PuzzleCapsuleWithResult) = {
    val capsules = cp.capsules
    val puzzleMap = cp.puzzles.map(p => p.puzzle.id -> p).toMap
    Json.obj(
      "num" -> cp.total,
      "capsules" -> {
        JsArray(
          capsules.map { capsule =>
            {
              val puzzleIds = capsule.puzzles
              Json.obj(
                "id" -> capsule.id,
                "name" -> capsule.name,
                "puzzles" -> JsArray(
                  capsule.puzzles.map { puzzleId =>
                    val pwr = puzzleMap(puzzleId)
                    val puzzle = pwr.puzzle
                    Json.obj(
                      "puzzle" -> {
                        Json.obj(
                          "id" -> puzzle.id,
                          "fen" -> puzzle.fen,
                          "color" -> puzzle.color.name,
                          "lastMove" -> puzzle.lastMove,
                          "lines" -> puzzle.lines
                        )
                      },
                      "result" -> pwr.result.map { results =>
                        JsArray(
                          results.map { result =>
                            Json.obj(
                              "win" -> result.win,
                              "lines" -> {
                                JsArray(
                                  result.lines.map {
                                    nodeJson
                                  }
                                )
                              }
                            )
                          }
                        )
                      }
                    )
                  }
                )
              )
            }
          }
        )
      }
    )
  }

  private def replayGameJson(rg: ReplayGamesWithResult) = {
    JsArray(
      rg.replayGames.map { replayGameWithResult =>
        val replayGame = replayGameWithResult.replayGame
        Json.obj(
          "replayGame" -> {
            Json.obj(
              "chapterId" -> replayGame.chapterIdFromLink,
              "chapterLink" -> replayGame.chapterLink,
              "name" -> replayGame.name,
              "root" -> replayGame.lastFen,
              "pgn" -> replayGame.toPGN,
              "color" -> replayGame.color.map(_.name),
              "moves" -> movesJson(replayGame.moves)
            )
          },
          "result" -> replayGameWithResult.result.map { result =>
            Json.obj(
              "win" -> result.win
            )
          }
        )
      }
    )
  }

  private def recallGameJson(rg: RecallGamesWithResult) = {
    JsArray(
      rg.recallGames.map { recallGameWithResult =>
        val recallGame = recallGameWithResult.recallGame
        Json.obj(
          "recallGame" -> {
            Json.obj(
              "name" -> (recallGame.name | "-"),
              "fen" -> recallGame.lastFen,
              "pgn" -> recallGame.pgn,
              "turns" -> recallGame.turns,
              "color" -> recallGame.color.map(_.name),
              "orient" -> recallGame.orient,
              "title" -> recallGame.title,
              "moves" -> recallGame.moves.map { movesJson }
            )
          },
          "result" -> recallGameWithResult.result.map { result =>
            Json.obj(
              "win" -> result.win,
              "turns" -> result.turns
            )
          }
        )
      }
    )
  }

  private def distinguishGameJson(dg: DistinguishGamesWithResult) = {
    JsArray(
      dg.distinguishGames.map { distinguishGameWithResult =>
        val distinguishGame = distinguishGameWithResult.distinguishGame
        Json.obj(
          "distinguishGame" -> {
            Json.obj(
              "name" -> (distinguishGame.name | "-"),
              "fen" -> distinguishGame.lastFen,
              "pgn" -> distinguishGame.pgn,
              "turns" -> distinguishGame.turns,
              "rightTurns" -> distinguishGame.rightTurns,
              "orientation" -> distinguishGame.orient,
              "title" -> distinguishGame.title,
              "moves" -> distinguishGame.moves.map { movesJson }
            )
          },
          "result" -> distinguishGameWithResult.result.map { result =>
            Json.obj(
              "win" -> result.win,
              "turns" -> result.turns
            )
          }
        )
      }
    )
  }

  private def fromPositionJson(fr: FromPositionsWithResult) = {
    JsArray(
      fr.fromPositions.map { fromPositionWithResult =>
        val fromPosition = fromPositionWithResult.fromPosition
        Json.obj(
          "fromPosition" -> {
            Json.obj(
              "fen" -> fromPosition.fen,
              "clock" -> {
                Json.obj(
                  "initial" -> fromPosition.clock.limitSeconds,
                  "increment" -> fromPosition.clock.incrementSeconds
                )
              },
              "num" -> fromPosition.num,
              "isAi" -> fromPosition.isAi,
              "aiLevel" -> fromPosition.aiLevel,
              "color" -> (fromPosition.color.map(_.name) | "random"),
              "chessStatus" -> fromPosition.chessStatus,
              "canTakeback" -> fromPosition.canTakeback
            )
          },
          "result" -> fromPositionWithResult.result.map { results =>
            JsArray(
              results.map { result =>
                Json.obj(
                  "gameId" -> result.gameId,
                  "white" -> result.white,
                  "black" -> result.black
                )
              }
            )
          }
        )
      }
    )
  }

  private def fromPgnJson(fr: FromPgnsWithResult) = {
    JsArray(
      fr.fromPgns.map { fromPgnWithResult =>
        val fromPgn = fromPgnWithResult.fromPgn
        val pgnSetup = fromPgn.pgnSetup
        Json.obj(
          "fromPgn" -> {
            Json.obj(
              "initialPgn" -> lila.game.JsonView.initialPgnJson(pgnSetup),
              "clock" -> {
                Json.obj(
                  "initial" -> fromPgn.clock.limitSeconds,
                  "increment" -> fromPgn.clock.incrementSeconds
                )
              },
              "num" -> fromPgn.num,
              "isAi" -> fromPgn.isAi,
              "aiLevel" -> fromPgn.aiLevel,
              "color" -> (fromPgn.color.map(_.name) | "random"),
              "chessStatus" -> fromPgn.chessStatus,
              "canTakeback" -> fromPgn.canTakeback
            )
          },
          "result" -> fromPgnWithResult.result.map { results =>
            JsArray(
              results.map { result =>
                Json.obj(
                  "gameId" -> result.gameId,
                  "white" -> result.white,
                  "black" -> result.black
                )
              }
            )
          }
        )
      }
    )
  }

  private def fromOpeningdbJson(fr: FromOpeningdbsWithResult) = {
    JsArray(
      fr.fromOpeningdbs.map { fromOpeningdbWithResult =>
        val fromOpeningdb = fromOpeningdbWithResult.fromOpeningdb
        val pgnSetup = fromOpeningdb.pgnSetup
        Json.obj(
          "fromOpeningdb" -> {
            Json.obj(
              "initialPgn" -> lila.game.JsonView.initialPgnJson(pgnSetup),
              "clock" -> {
                Json.obj(
                  "initial" -> fromOpeningdb.clock.limitSeconds,
                  "increment" -> fromOpeningdb.clock.incrementSeconds
                )
              },
              "num" -> fromOpeningdb.num,
              "isAi" -> fromOpeningdb.isAi,
              "aiLevel" -> fromOpeningdb.aiLevel,
              "color" -> (fromOpeningdb.color.map(_.name) | "random"),
              "chessStatus" -> fromOpeningdb.chessStatus,
              "canTakeback" -> fromOpeningdb.canTakeback
            )
          },
          "result" -> fromOpeningdbWithResult.result.map { results =>
            JsArray(
              results.map { result =>
                Json.obj(
                  "gameId" -> result.gameId,
                  "white" -> result.white,
                  "black" -> result.black
                )
              }
            )
          }
        )
      }
    )
  }

  private def trainGameJson(tg: TrainGameWithResult, game: Game, whitePlayer: LightUser, blackPlayer: LightUser, markMap: Map[String, Option[String]]) = {
    val trainGame = tg.trainGame
    Json.obj(
      "trainGame" -> {
        Json.obj(
          "gameId" -> trainGame.gameId,
          "name" -> s"${trainGame.clock.show} • ${if (trainGame.mode.rated) "计算等级分" else "不计算等级分"}",
          "white" -> playerJson(whitePlayer, markMap),
          "black" -> playerJson(blackPlayer, markMap),
          "clock" -> {
            Json.obj(
              "initial" -> trainGame.clock.limitSeconds,
              "increment" -> trainGame.clock.incrementSeconds
            )
          },
          "speed" -> Json.obj(
            "key" -> trainGame.speed.key,
            "name" -> trainGame.speed.name
          ),
          "variant" -> Json.obj(
            "key" -> trainGame.variant.key,
            "name" -> trainGame.variant.name
          ),
          "rated" -> trainGame.mode.rated,
          "fen" -> Forsyth.exportBoard(game.board),
          "color" -> Color.White.name,
          "turnColor" -> game.turnColor.name,
          "lastMove" -> game.lastMoveKeys,
          "remainClock" -> game.clock.map { c =>
            Json.obj(
              "white" -> c.remainingTime(Color.White).roundSeconds,
              "black" -> c.remainingTime(Color.Black).roundSeconds
            )
          },
          "status" -> game.status.id,
          "isLive" -> game.started,
          "pause" -> (game.created || (game.started && game.playedTurns < 2) || game.finished)
        )
      },
      "result" -> tg.result.map { result =>
        Json.obj(
          "winner" -> result.winner.map(_.name),
          "finishAt" -> result.finishAt.getMillis
        )
      }
    )
  }

  private def movesJson(moves: List[Move]) =
    JsArray(
      moves.map { move =>
        Json.obj(
          "index" -> move.index,
          "white" -> move.white.map { nodeJson },
          "black" -> move.black.map { nodeJson }
        )
      }
    )

  private def nodeJson(node: Node) =
    Json.obj(
      "san" -> node.san,
      "uci" -> node.uci,
      "fen" -> node.fen
    )

  def playerJson(lightUser: LightUser, markMap: Map[String, Option[String]]) = {
    val markOption = markMap.get(lightUser.id) match {
      case None => none[String]
      case Some(m) => m
    }

    LightUser
      .lightUserWrites.writes(lightUser)
      .add("mark" -> markOption)
  }

  private def renderLightUser(lightUser: LightUser, markMap: Map[String, Option[String]]): JsObject = {
    val markOption = markMap.get(lightUser.id) match {
      case None => none[String]
      case Some(m) => m
    }

    LightUser
      .lightUserWrites.writes(lightUser)
      .add("online" -> isOnline(lightUser.id))
      .add("mark" -> markOption)
  }

}

