package lila.task

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.common.{ AtMost, Every, LightUser, ResilientScheduler }
import lila.game.Game
import lila.hub.DuctMap
import lila.notify.NotifyApi
import scala.concurrent.duration._

final class Env(
    config: Config,
    hub: lila.hub.Env,
    db: lila.db.Env,
    lightUser: LightUser.Getter,
    lightUserSync: LightUser.GetterSync,
    asyncCache: lila.memo.AsyncCache.Builder,
    system: ActorSystem,
    isOnline: lila.user.User.ID => Boolean,
    lightUserApi: lila.user.LightUserApi,
    onStart: Game.ID => Unit,
    proxyGame: Game.ID => Fu[Option[Game]],
    roundMap: DuctMap[_],
    notifyApi: NotifyApi
) {

  private val CollectionTTask = config getString "collection.ttask"
  private val CollectionTTaskTemplate = config getString "collection.ttask_template"
  private[task] val TTaskColl = db(CollectionTTask)
  private[task] val TTaskTemplateColl = db(CollectionTTaskTemplate)

  lazy val jsonView = new JsonView(isOnline, lightUserApi, proxyGame)

  lazy val forms = new DataForm()

  lazy val api = new TTaskApi(taskSolve, notifyApi, hub.bus, onStart, roundMap, jsonView)(system)

  lazy val tplApi = new TTaskTemplateApi()

  lazy val taskSolve = new TTaskSolve(jsonView, hub.bus, system)

  ResilientScheduler(
    every = Every(5 minute),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 2 minute
  ) { api.setExpire() }(system)

  system.lilaBus.subscribeFun('finishPuzzle, 'finishRush, 'recallFinished, 'distinguishFinished, 'coordTrainFinish, 'startGame, 'finishGame, 'allPlayerMoved, 'teamCoinChange) {
    case res: lila.puzzle.PuzzleResult =>
      if (res.source == lila.puzzle.PuzzleResult.Source.Puzzle ||
        res.source == lila.puzzle.PuzzleResult.Source.Theme ||
        res.source == lila.puzzle.PuzzleResult.Source.Task) {
        taskSolve.handlePuzzle(res)
      }
    case rush: lila.puzzle.PuzzleRush => taskSolve.handleRush(rush)
    case lila.game.actorApi.AllPlayerMoved(game) => taskSolve.handleStartGame(game)
    case lila.game.actorApi.FinishGame(game, _, _) => {
      if (game.hasClock) taskSolve.handleFinishTrainGame(game)
      taskSolve.handleFinishGame(game)
    }
    case coordTrain: lila.hub.actorApi.coordTrain.TrainData => taskSolve.handleCoordTrain(coordTrain)
    case recall: lila.hub.actorApi.Recall => taskSolve.handleRecall(recall)
    case distinguish: lila.hub.actorApi.Distinguish => taskSolve.handleDistinguish(distinguish)
    case lila.hub.actorApi.team.TeamCoinChangeFromTTask(_, _, taskId, coinDiff, _, _) => {
      TTaskRepo.setCoinDiff(taskId, coinDiff)
    }
  }

}

object Env {

  lazy val current: Env = "task" boot new Env(
    config = lila.common.PlayApp loadConfig "task",
    hub = lila.hub.Env.current,
    db = lila.db.Env.current,
    lightUser = lila.user.Env.current.lightUser,
    lightUserSync = lila.user.Env.current.lightUserSync,
    asyncCache = lila.memo.Env.current.asyncCache,
    system = lila.common.PlayApp.system,
    isOnline = lila.user.Env.current.isOnline,
    lightUserApi = lila.user.Env.current.lightUserApi,
    onStart = lila.round.Env.current.onStart,
    proxyGame = lila.round.Env.current.proxy.gameIfPresent _,
    roundMap = lila.round.Env.current.roundMap,
    notifyApi = lila.notify.Env.current.api
  )

}
