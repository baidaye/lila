package lila.task

import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random
import TTask._

case class TTaskTemplate(
    _id: TTaskTemplate.ID,
    name: String,
    remark: Option[String],
    item: TTaskItem,
    itemType: TTaskItemType,
    sourceRel: SourceRel,
    coinTeam: Option[String],
    coinRule: Option[Int],
    deadlineAt: Option[DateTime],
    createdAt: DateTime,
    updatedAt: DateTime,
    createdBy: User.ID,
    updatedBy: User.ID
) {

  def id = _id

  def isCreator(user: User) = user.id == createdBy

  def total = item.total(itemType)

  def isNumber = item.isNumber(itemType)

  def hasTag =
    itemType match {
      case TTaskItemType.ThemePuzzleItem | TTaskItemType.PuzzleRushItem | TTaskItemType.CoordTrainItem | TTaskItemType.GameItem => true
      case _ => false
    }
}

object TTaskTemplate {

  type ID = String

  def make(
    name: String,
    remark: Option[String],
    item: TTaskItem,
    itemType: TTaskItemType,
    sourceRel: SourceRel,
    coinTeam: Option[String],
    coinRule: Option[Int],
    deadlineAt: Option[DateTime],
    createdBy: User.ID
  ) = {
    val now = DateTime.now
    TTaskTemplate(
      _id = Random nextString 8,
      name = name,
      remark = remark,
      item = item,
      itemType = itemType,
      sourceRel = sourceRel,
      coinTeam = coinTeam,
      coinRule = coinRule,
      deadlineAt = deadlineAt,
      createdAt = now,
      updatedAt = now,
      createdBy = createdBy,
      updatedBy = createdBy
    )
  }

  case class Light(_id: TTaskTemplate.ID, name: String, item: TTaskItem, itemType: TTaskItemType)

}

