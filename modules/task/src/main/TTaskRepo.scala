package lila.task

import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import lila.user.User
import org.joda.time.DateTime

object TTaskRepo {

  import BSONHandlers.TTaskItemHandler
  import BSONHandlers.TTaskHandler

  private lazy val coll = Env.current.TTaskColl

  private def started = $or("startAt" $exists false, $doc("startAt" $lte DateTime.now))

  def byId(id: TTask.ID): Fu[Option[TTask]] =
    coll.byId[TTask](id)

  def byTeamTestStuId(teamTestStuId: String): Fu[Option[TTask]] =
    coll.uno[TTask]($doc("item.teamTest.stuTestId" -> teamTestStuId))

  def byIds(ids: List[TTask.ID]): Fu[List[TTask]] =
    coll.byIds[TTask](ids)

  def mineAvailable(userId: User.ID): Fu[List[TTask]] =
    coll.list[TTask](
      $doc("userIds" -> userId /*, "deadlineAt" $gt DateTime.now()*/ ) ++ $or(
        $doc("status" $in List(TTask.Status.Train.id, TTask.Status.Created.id)),
        $doc(
          "status" -> TTask.Status.Finished.id,
          "itemType" $in List(TTask.TTaskItemType.CapsulePuzzleItem.id, TTask.TTaskItemType.FromPositionItem.id, TTask.TTaskItemType.FromPgnItem.id, TTask.TTaskItemType.FromOpeningdbItem.id)
        )
      )
    )

  def findByPage(page: Int, source: TTask.Source, metaId: String, userId: User.ID, status: Option[TTask.Status]): Fu[Paginator[TTask]] =
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = {
          $doc(
            "userIds" -> userId,
            "sourceRel.source" -> source.id,
            "sourceRel.id" -> metaId
          ) ++ status.?? { s => $doc("status" -> s.id) }
        },
        projection = $empty,
        sort = $doc("createdAt" -> -1)
      ),
      page,
      lila.common.MaxPerPage(10)
    )

  def findBySource(source: TTask.Source, metaId: String, userId: User.ID): Fu[List[TTask]] =
    coll.find(
      $doc(
        "userIds" -> userId,
        "sourceRel.source" -> source.id,
        "sourceRel.id" -> metaId
      )
    )
      .sort($doc("createdAt" -> -1))
      .list[TTask]()

  def findMine(userId: User.ID, items: List[TTask.TTaskItemType]): Fu[List[TTask]] = {
    val $selector = $doc(
      "userIds" -> userId,
      "status" $in TTask.Status.current.map(_.id),
      "itemType" $in items.map(_.id)
    ) ++ started
    coll.find($selector)
      .sort($doc("deadlineAt" -> 1))
      .list[TTask]()
      .map(_.filter(_.isNumber))
  }

  def findMine(userId: User.ID): Fu[List[TTask]] = {
    val $selector = $doc(
      "userIds" -> userId,
      "sourceRel.source" $in TTask.Source.mine.map(_.id),
      "status" $in TTask.Status.current.map(_.id)
    ) ++ started

    coll.find($selector)
      .sort($doc("deadlineAt" -> 1))
      .list[TTask](5)
  }

  def findByUserPage(page: Int, userId: User.ID, statusArray: List[TTask.Status], source: Option[TTask.Source], status: Option[TTask.Status]): Fu[Paginator[TTask]] =
    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = {
          $doc("userIds" -> userId, "status" $in statusArray.map(_.id)) ++
            source.?? { s =>
              $doc("sourceRel.source" -> s.id)
            } ++ status.?? { s =>
              $doc("status" -> s.id)
            } ++ started
        },
        projection = $empty,
        sort = $doc("createdAt" -> -1)
      ),
      page,
      lila.common.MaxPerPage(10)
    )

  def trainGameTask(gameId: String): Fu[Option[TTask]] =
    coll.uno[TTask]($doc("item.trainGame.trainGame.gameId" -> gameId))

  def trainGameTasks(trainCourseId: String): Fu[List[TTask]] =
    coll.list[TTask](
      $doc(
        "sourceRel.source" -> TTask.Source.TrainCourse.id,
        "sourceRel.id" -> trainCourseId,
        "itemType" -> TTask.TTaskItemType.TrainGameItem.id
      )
    )

  def insert(task: TTask): Funit =
    coll.insert(task).void

  def batchInsert(tasks: List[TTask]): Funit =
    coll.bulkInsert(
      documents = tasks.map(TTaskHandler.write).toStream,
      ordered = true
    ).void

  def update(task: TTask): Funit =
    coll.update($id(task.id), task).void

  def updateItem(id: TTask.ID, item: TTaskItem): Funit =
    coll.update($id(id), $set("item" -> item)).void

  def remove(id: TTask.ID): Funit =
    coll.remove($id(id)).void

  def existsByHomework(homeworkId: String): Fu[Set[String]] =
    coll.distinct[String, Set]("userIds", $doc(
      "sourceRel.source" -> "homework",
      "sourceRel.id" -> homeworkId,
      "status" $in List(TTask.Status.Train, TTask.Status.Finished).map(_.id)
    ).some)

  def removeByHomework(homeworkId: String): Funit =
    coll.remove($doc(
      "sourceRel.source" -> "homework",
      "sourceRel.id" -> homeworkId
    )).void

  def setStatus(id: TTask.ID, status: TTask.Status): Funit =
    coll.update(
      $id(id),
      $set("status" -> status.id)
    ).void

  def setFinish(id: TTask.ID): Funit =
    coll.update(
      $id(id),
      $set("status" -> TTask.Status.Finished.id, "finishAt" -> DateTime.now)
    ).void

  def updatePlanAt(id: TTask.ID, planAt: DateTime): Funit =
    coll.update(
      $id(id),
      $set("planAt" -> planAt)
    ).void

  def updateStartAt(id: TTask.ID, startAt: DateTime): Funit =
    coll.update(
      $id(id),
      $set("startAt" -> startAt)
    ).void

  def updateStartAtByHomework(homeworkId: String, startAt: DateTime): Funit =
    coll.update(
      $doc(
        "sourceRel.source" -> "homework",
        "sourceRel.id" -> homeworkId
      ),
      $set("startAt" -> startAt),
      multi = true
    ).void

  def setDeadline(ids: List[TTaskTemplate.ID], deadlineAt: DateTime): Funit =
    coll.update(
      $doc("tplId" $in ids),
      $set("deadlineAt" -> deadlineAt),
      multi = true
    ).void >> coll.update(
        $doc("tplId" $in ids, "status" -> TTask.Status.Expired.id),
        $set("status" -> TTask.Status.Train.id),
        multi = true
      ).void

  def setCoinDiff(id: TTask.ID, coinDiff: Int): Funit =
    coll.update(
      $id(id),
      $set("coinDiff" -> coinDiff)
    ).void

  def setExpire(): Funit =
    coll.update(
      $doc($doc("status" $in List(TTask.Status.Created.id, TTask.Status.Train.id), "deadlineAt" $lte DateTime.now())),
      $set("status" -> TTask.Status.Expired.id),
      multi = true
    ).void

}
