package lila.puzzle

import lila.db.dsl._
import lila.user.User
import Puzzle.{ BSONFields => P }
import scala.util.Random
import com.github.blemale.scaffeine.{ Cache, Scaffeine }
import lila.hub.actorApi.puzzle.NextRushPuzzle
import reactivemongo.bson.BSONDocument
import scala.concurrent.duration._

private[puzzle] final class PuzzleRushSelector(puzzleColl: Coll, puzzleRoundApi: PuzzleRoundApi, bus: lila.common.Bus) {

  private val cache: Cache[PuzzleRush.ID, Puzzle] =
    Scaffeine().expireAfterWrite(10 minutes).build[PuzzleRush.ID, Puzzle]

  import Puzzle.puzzleBSONHandler

  val projection = $doc(P.taggers -> 0, P.likers -> 0)
  val notMachine = $doc(P.id $lt lila.hub.PuzzleHub.maxThemeId)
  val enabled = $doc(P.enabled -> true)
  val depth = $doc("depth" $lt 6)
  val retry = $doc("retry" -> false)
  val defaultFilter = notMachine ++ enabled ++ retry
  val notGenerate = $doc("idHistory.source" $nin List("checkmate1", "checkmate2", "checkmate3"))

  val index = $doc(P.id -> 1, P.rating -> 1)

  val firstSkipMax = 1000
  val minRating = lila.hub.PuzzleHub.minRating
  val maxRating = lila.hub.PuzzleHub.maxRating
  val minRatingStep = 40
  val maxRatingStep = 60
  val maxDepth = 5

  private def ratingRandom = Random.nextInt(5)

  private def mergeMaxRating(query: CustomCondition) = query.ratingMax | maxRating

  private def mergeFirstSkipMax(query: CustomCondition) = if (query.isNone) firstSkipMax else 15

  private def mergeMinRatingStep(query: CustomCondition) = if (query.isNone) minRatingStep else 20

  private def mergeMaxRatingStep(query: CustomCondition) = if (query.isNone) maxRatingStep else 30

  private def mergeRatingInterval(query: CustomCondition) = if (query.isNone) 10 else 5

  case object NoPuzzlesAvailableException extends lila.base.LilaException {
    val message = "No puzzles available"
  }

  def apply(user: User, rushId: PuzzleRush.ID, condition: CustomCondition): Fu[Puzzle] = {
    if (!condition.isNone && condition.isRandom) {
      puzzleRoundApi.rushRounds(rushId) flatMap { list =>
        random(condition, list.map(_.puzzleId))
      }
    } else {
      puzzleRoundApi.rushLastRound(rushId) flatMap {
        case None => selectFirst(condition)
        case Some(r) => {
          if (r.puzzleRating >= mergeMaxRating(condition)) {
            selectFirst(condition)
          } else {
            selectNext(r, condition)
          }
        }
      }
    }
  } flattenWith NoPuzzlesAvailableException addEffect { p =>
    bus.publish(NextRushPuzzle(p.id, user.id), 'nextRushPuzzle)
    cache.put(rushId, p)
  }

  def last(user: User, rushId: PuzzleRush.ID, condition: CustomCondition): Fu[Puzzle] = {
    cache.getIfPresent(rushId) match {
      case Some(p) => fuccess(p)
      case None => apply(user, rushId, condition)
    }
  }

  private def selectFirst(condition: CustomCondition): Fu[Option[Puzzle]] = {
    val $filter = filter(condition)
    //println("----------------------------first--------------------------------")
    //println(BSONDocument.pretty($filter))
    //puzzleColl.countSel($filter, hint = index.some) flatMap { count =>
    //  val skip = math.min(mergeFirstSkipMax(condition), math.max(count, 1))
    // logger.info(BSONDocument.pretty($filter))
    val skip = mergeFirstSkipMax(condition)
    val randomSkip = Random nextInt skip
    puzzleColl.find($filter, projection)
      .hint(index)
      .sort($sort asc P.rating)
      .skip(randomSkip)
      .uno[Puzzle]
    //}
  }

  private def selectNext(prev: PuzzleRound, condition: CustomCondition, retry: Int = 0): Fu[Option[Puzzle]] = {
    val nextMinRatingStep = Random.nextInt(mergeMaxRatingStep(condition) - mergeMinRatingStep(condition)) + mergeMinRatingStep(condition)
    val minRating = math.min(prev.puzzleRating + nextMinRatingStep, mergeMaxRating(condition))
    val maxRating = math.min(minRating + mergeRatingInterval(condition) + 5 * retry, mergeMaxRating(condition))
    val $filter = filter(condition, minRating.some, maxRating.some)
    //println("----------------------------next--------------------------------")
    //println(BSONDocument.pretty($filter))
    //puzzleColl.countSel($filter, hint = index.some) flatMap { count =>
    //  val skp = math.min(100, math.max(count, 1))
    // logger.info(BSONDocument.pretty($filter))
    puzzleColl.find($filter, projection)
      .hint(index)
      .sort($sort asc P.rating)
      .skip(Random nextInt 100)
      .uno[Puzzle]
    //}
  } flatMap {
    // 没有查到分为两种情况
    case None => {
      // 1.下次查询的分数 > 最大分数 => 循环到第一条
      if (prev.puzzleRating + mergeMaxRatingStep(condition) + mergeRatingInterval(condition) + 5 * retry > mergeMaxRating(condition)) {
        selectFirst(condition)
      } else {
        // 2.在当前分数段内查询不到数据 => 扩大分数段
        selectNext(prev, condition, retry + 1)
      }
    }
    case res => fuccess(res)
  }

  private def random(condition: CustomCondition, ids: List[PuzzleId]) = {
    val $filter = filter(condition) ++ $doc(P.id $nin ids)
    //    println("----------------------------next--------------------------------")
    //    println(BSONDocument.pretty($filter))
    //puzzleColl.countSel($filter, hint = index.some) flatMap { count =>
    //  val skp = math.max(count, 1)
    puzzleColl.find($filter, projection)
      .hint(index)
      .skip(Random nextInt 20)
      .uno[Puzzle]
    //}
  }

  private def filter(query: CustomCondition, r1: Option[Int] = None, r2: Option[Int] = None) = {
    var $filter = defaultFilter
    if ((query.ratingMin.isDefined || query.ratingMax.isDefined) || (r1.isDefined || r2.isDefined)) {
      var ratingRange = $empty
      if (query.ratingMin.isDefined || r1.isDefined) {
        val r = math.max(r1 | minRating, (query.ratingMin | minRating) + ratingRandom)
        ratingRange = ratingRange ++ $gte(r)
      }

      if (query.ratingMax.isDefined || r2.isDefined) {
        val r = math.min(r2 | maxRating, query.ratingMax | maxRating)
        ratingRange = ratingRange ++ $lte(r)
      }
      $filter = $filter ++ $doc(P.rating -> ratingRange)
    } else {
      $filter = $filter ++ $doc(P.rating $lte maxRating)
    }

    if (query.stepsMin.isDefined || query.stepsMax.isDefined) {
      var stepsRange = $empty
      query.stepsMin foreach { stepsMin =>
        stepsRange = stepsRange ++ $gte(stepsMin)
      }
      query.stepsMax foreach { stepsMax =>
        stepsRange = stepsRange ++ $lte(stepsMax)
      }
      $filter = $filter ++ $doc(P.depth -> stepsRange)
    } else {
      $filter = $filter ++ $doc(P.depth $lte maxDepth)
    }

    query.color foreach { c =>
      $filter = $filter ++ $doc(P.white -> (c == "White"))
    }

    query.phase foreach { p =>
      $filter = $filter ++ $doc(s"${P.mark}.phase" -> p)
    }
    $filter
  }

}
