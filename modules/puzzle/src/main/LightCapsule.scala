package lila.puzzle

case class LightCapsule(id: String, name: String, no: Int, total: Int)

object LightCapsule {

  def empty = LightCapsule("", "", -1, -1)

}
