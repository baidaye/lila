package lila.puzzle

import lila.db.dsl.bsonArrayToListHandler
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson.Macros

case class ThemeRecord(
    _id: User.ID,
    puzzleId: PuzzleId,
    query: Option[QueryData],
    updateAt: DateTime
) {

  def id = _id

  def queryOrDefault = query | QueryData.default
}

object ThemeRecord {

  import lila.db.BSON.BSONJodaDateTimeHandler
  implicit val StringArrayHandler = bsonArrayToListHandler[String]
  implicit val ThemeQueryBSONHandler = Macros.handler[QueryData]
  implicit val ThemeRecordBSONHandler = Macros.handler[ThemeRecord]

  def make(
    userId: User.ID,
    puzzleId: PuzzleId,
    query: QueryData
  ) = {
    ThemeRecord(
      _id = userId,
      puzzleId = puzzleId,
      query = query.some,
      updateAt = DateTime.now
    )
  }

  def default(userId: User.ID) = ThemeRecord(
    _id = userId,
    puzzleId = 100000,
    query = QueryData.default.some,
    updateAt = DateTime.now
  )

}

case class QueryData(
    ratingMin: Option[Int] = None,
    ratingMax: Option[Int] = None,
    stepsMin: Option[Int] = None,
    stepsMax: Option[Int] = None,
    tags: Option[List[String]] = None,
    phase: Option[List[String]] = None,
    moveFor: Option[List[String]] = None,
    pieceColor: Option[List[String]] = None,
    subject: Option[List[String]] = None,
    strength: Option[List[String]] = None,
    chessGame: Option[List[String]] = None,
    comprehensive: Option[List[String]] = None
) {

  def rangeRawString =
    ratingMin.?? { rm => s"ratingMin=$rm" } +
      ratingMax.?? { rm => s"&ratingMax=$rm" } +
      stepsMin.?? { sm => s"&stepsMin=$sm" } +
      stepsMax.?? { sm => s"&stepsMax=$sm" }

  def allRawString =
    ratingMin.?? { rm => s"ratingMin=$rm" } +
      ratingMax.?? { rm => s"&ratingMax=$rm" } +
      stepsMin.?? { sm => s"&stepsMin=$sm" } +
      stepsMax.?? { sm => s"&stepsMax=$sm" } +
      tags.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&tags[$index]=$tag"
        }
      } + phase.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&phase[$index]=$tag"
        }
      } + moveFor.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&moveFor[$index]=$tag"
        }
      } + pieceColor.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&pieceColor[$index]=$tag"
        }
      } + subject.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&subject[$index]=$tag"
        }
      } + strength.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&strength[$index]=$tag"
        }
      } + chessGame.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&chessGame[$index]=$tag"
        }
      } + comprehensive.?? { tags =>
        tags.zipWithIndex.foldLeft("") {
          case (query, (tag, index)) => query + s"&comprehensive[$index]=$tag"
        }
      }

  def same(query: QueryData) = query.allRawString == allRawString
}

object QueryData {

  def default = QueryData(
    ratingMin = 800.some,
    ratingMax = 1300.some,
    stepsMin = 1.some,
    stepsMax = 3.some,
    subject = List("ZhuoShuang").some
  )

}
