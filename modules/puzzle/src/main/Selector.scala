package lila.puzzle

import scala.util.Random
import lila.db.dsl._
import lila.user.User
import Puzzle.{ BSONFields => F }
import lila.resource.DataForm.puzzle.ThemeDataOr
import reactivemongo.bson.BSONDocument
import lila.hub.actorApi.puzzle.{ NextCapsulePuzzle, NextHomeworkPuzzle, NextPuzzle, NextThemePuzzle, NextTaskPuzzle }

private[puzzle] final class Selector(
    puzzleColl: Coll,
    api: PuzzleApi,
    puzzleRoundApi: PuzzleRoundApi,
    puzzleIdMin: Int,
    bus: lila.common.Bus
) {

  val minRating = lila.hub.PuzzleHub.minRating
  val maxRating = lila.hub.PuzzleHub.maxRating

  val enabled = $doc(F.enabled -> true)

  val projection = $doc(F.taggers -> 0, F.likers -> 0)

  val notMachine = $doc(F.id $lt lila.hub.PuzzleHub.maxThemeId)

  import Selector._

  def byId(id: PuzzleId): Fu[Option[Puzzle]] = {
    puzzleColl.byId[Puzzle](id)
  }

  def nextPuzzle(me: Option[User]): Fu[Puzzle] = {
    lila.mon.puzzle.selector.count()
    me match {
      // anon
      case None => puzzleColl // this query precisely matches a mongodb partial index
        .find(notMachine ++ enabled, projection)
        .hint($doc(F.id -> 1))
        .sort($sort desc F.voteRatio)
        .skip(Random nextInt anonSkipMax)
        .uno[Puzzle]
      // user
      case Some(user) => api.head find user flatMap {
        // new player
        case None => api.puzzle find puzzleIdMin flatMap { puzzleOption =>
          puzzleOption ?? { p =>
            api.head.addNew(user, p.id) >>-
              bus.publish(NextPuzzle(p.id, user.id), 'nextPuzzle)
          } inject puzzleOption
        }
        // current puzzle
        case Some(PuzzleHead(_, Some(current), _)) => api.puzzle find current
        // find new based on last
        case Some(PuzzleHead(_, _, last)) => newPuzzleForUser(user, last) flatMap {
          // user played all puzzles. Reset rounds and start anew.
          case None => api.puzzle.cachedLastId.get flatMap { maxId =>
            (last > maxId - 1000) ?? {
              /*api.round.reset(user) >> */ api.puzzle.find(puzzleIdMin)
            }
          }
          case Some(found) => fuccess(found.some)
        } flatMap { puzzleOption =>
          puzzleOption ?? { p =>
            api.head.addNew(user, p.id) >>-
              bus.publish(NextPuzzle(p.id, user.id), 'nextPuzzle)
          } inject puzzleOption
        }
      }
    }
  }.mon(_.puzzle.selector.time) flattenWith NoPuzzlesAvailableException addEffect { puzzle =>
    if (puzzle.vote.sum < -1000) {
      logger.info(s"Select #${puzzle.id} vote.sum: ${puzzle.vote.sum} for ${me.fold("Anon")(_.username)} (${me.fold("?")(_.perfs.puzzle.intRating.toString)})")
    } else {
      lila.mon.puzzle.selector.vote(puzzle.vote.sum)
    }
  }

  def themePuzzleSearch(themeSearchCondition: BSONDocument): Fu[Option[Puzzle]] = {
    puzzleColl.find(themeSearchCondition, projection)
      .hint($doc(F.id -> 1))
      .sort($sort asc F.id)
      .uno[Puzzle]
  }

  def nextThemePuzzle(user: User, queryData: ThemeDataOr, themeSearchCondition: BSONDocument, id: PuzzleId, saveQuery: Boolean): Fu[Option[Puzzle]] = {
    themePuzzleSearch(themeSearchCondition) flatMap {
      case None => (id != puzzleIdMin).?? {
        nextThemePuzzle(user, queryData, notMachine ++ themeSearchCondition.remove("_id") ++ $doc(F.id -> $gt(puzzleIdMin)), puzzleIdMin, saveQuery)
      }
      case Some(p) => {
        bus.publish(NextThemePuzzle(p.id, user.id,
          lila.hub.actorApi.puzzle.ThemeQuery(
            ratingMin = queryData.ratingMin,
            ratingMax = queryData.ratingMax,
            stepsMin = queryData.stepsMin,
            stepsMax = queryData.stepsMax,
            tags = queryData.tags,
            phase = queryData.phase,
            moveFor = queryData.moveFor,
            pieceColor = queryData.pieceColor,
            subject = queryData.subject,
            strength = queryData.strength,
            chessGame = queryData.chessGame,
            comprehensive = queryData.comprehensive
          ), saveQuery), 'nextThemePuzzle)
        fuccess(p.some)
      }
    }
  }

  def nextCapsulePuzzle(user: User, capsuleId: String, ids: List[PuzzleId], lastPlayed: Option[PuzzleId], init: Boolean = false): Fu[Option[Puzzle]] = {
    if (ids.isEmpty) fuccess(none)
    else {
      {
        if (lastPlayed.isDefined && init) {
          lastPlayed
        } else {
          lastPlayed.fold(ids.headOption) { lp =>
            val lastPlayedIndex = ids.zipWithIndex.find(_._1 == lp).map(_._2 + 1) | 0
            if (lastPlayedIndex == ids.length) {
              ids.headOption
            } else {
              ids.zipWithIndex.find(_._2 == lastPlayedIndex).map(_._1)
            }
          }
        }
      } match {
        case None => fuccess(none)
        case Some(nextPuzzleId) => {
          bus.publish(NextCapsulePuzzle(capsuleId, nextPuzzleId, user.id), 'nextCapsulePuzzle)
          puzzleColl.byId[Puzzle](nextPuzzleId)
        }
      }
    }
  }

  def currentHomeworkPuzzle(id: PuzzleId): Fu[Option[Puzzle]] = puzzleColl.byId[Puzzle](id)

  def nextHomeworkPuzzle(user: User, lastPlayed: PuzzleId, mappingArr: Array[(PuzzleId, Boolean)]): Fu[Option[Puzzle]] = {
    if (mappingArr.forall(_._2)) fuccess(none)
    else {
      val idArr = mappingArr.map(_._1)
      val lastIndex = idArr.indexOf(lastPlayed)
      if (lastIndex < 0) {
        fufail(s"can not find puzzle of $lastPlayed")
      } else {

        def nextPuzzleIndex(lastIndex: Int): Int = {
          var nextIndex = lastIndex + 1
          if (nextIndex > idArr.length - 1) {
            nextIndex = 0
          }

          if (mappingArr(nextIndex)._2) {
            nextPuzzleIndex(nextIndex)
          } else nextIndex
        }

        val npi = nextPuzzleIndex(lastIndex)
        puzzleColl.byId[Puzzle](idArr(npi)).map {
          case None => None
          case Some(p) => {
            publishNextHomeworkPuzzle(p.id, user.id)
            p.some
          }
        }
      }
    }
  }

  def publishNextHomeworkPuzzle(puzzleId: Int, userId: String) =
    bus.publish(NextHomeworkPuzzle(puzzleId, userId), 'nextHomeworkPuzzle)

  // -----------------------
  def currentTaskPuzzle(id: PuzzleId): Fu[Option[Puzzle]] = puzzleColl.byId[Puzzle](id)

  def nextTaskPuzzle(user: User, lastPlayed: PuzzleId, mappingArr: Array[(PuzzleId, Boolean)]): Fu[Option[Puzzle]] = {
    if (mappingArr.forall(_._2)) fuccess(none)
    else {
      val idArr = mappingArr.map(_._1)
      val lastIndex = idArr.indexOf(lastPlayed)
      if (lastIndex < 0) {
        fufail(s"can not find puzzle of $lastPlayed")
      } else {

        def nextPuzzleIndex(lastIndex: Int): Int = {
          var nextIndex = lastIndex + 1
          if (nextIndex > idArr.length - 1) {
            nextIndex = 0
          }

          if (mappingArr(nextIndex)._2) {
            nextPuzzleIndex(nextIndex)
          } else nextIndex
        }

        val npi = nextPuzzleIndex(lastIndex)
        puzzleColl.byId[Puzzle](idArr(npi)).map {
          case None => None
          case Some(p) => {
            publishNextTaskPuzzle(p.id, user.id)
            p.some
          }
        }
      }
    }
  }

  def publishNextTaskPuzzle(puzzleId: Int, userId: String) =
    bus.publish(NextTaskPuzzle(puzzleId, userId), 'nextTaskPuzzle)
  // -----------------------

  private def newPuzzleForUser(user: User, lastPlayed: PuzzleId): Fu[Option[Puzzle]] = {
    val rating = user.perfs.puzzle.intRating atMost maxRating atLeast minRating
    val step = toleranceStepFor(rating, user.perfs.puzzle.nb)
    tryRange(
      rating = rating,
      tolerance = step,
      step = step,
      idRange = Range(lastPlayed, lastPlayed + 200)
    )
  }

  private def tryRange(
    rating: Int,
    tolerance: Int,
    step: Int,
    idRange: Range
  ): Fu[Option[Puzzle]] = {
    val $filter = notMachine ++ rangeSelector(
      rating = rating,
      tolerance = tolerance,
      idRange = idRange
    ) ++ enabled

    //println(BSONDocument.pretty($filter))
    puzzleColl.find($filter).hint($doc(F.id -> 1)).sort($sort asc F.id).uno[Puzzle] flatMap {
      case None if (tolerance + step) <= toleranceMax =>
        tryRange(rating, tolerance + step, step, Range(idRange.min, idRange.max + 100))
      case res => fuccess(res)
    }
  }

}

private final object Selector {

  case object NoPuzzlesAvailableException extends lila.base.LilaException {
    val message = "No puzzles available"
  }

  val toleranceMax = 1000

  val anonSkipMax = 50

  val balancer = 50

  def toleranceStepFor(rating: Int, nbPuzzles: Int) = {
    math.abs(1500 - rating) match {
      case d if d >= 500 => 300
      case d if d >= 300 => 250
      case d => 200
    }
  } * {
    // increase rating tolerance for puzzle blitzers,
    // so they get more puzzles to play
    if (nbPuzzles > 10000) 2
    else if (nbPuzzles > 5000) 3 / 2
    else 1
  }

  def rangeSelector(rating: Int, tolerance: Int, idRange: Range) = $doc(
    F.id $gt idRange.min $lt idRange.max,
    F.rating $gt (rating - tolerance) $lt (rating + tolerance - balancer)
  )
}
