package lila.puzzle

import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.db.dsl._
import lila.db.paginator.Adapter
import Puzzle.{ BSONFields => F }
import lila.user.User
import lila.resource.DataForm.puzzle._
import reactivemongo.api.ReadPreference
import reactivemongo.bson.{ BSONDocument, BSONElement }

private[puzzle] final class PuzzleResource(
    puzzleColl: Coll,
    pager: PuzzlePaginator,
    api: PuzzleApi,
    bus: lila.common.Bus
) {

  val projection = $doc(F.taggers -> 0, F.likers -> 0)

  val enabled = $doc(F.enabled -> true)

  val notMachine = $doc(F.id $lt Puzzle.maxThemeId)

  val imports = $doc(F.id $gte Puzzle.minImportId)

  val theme = $doc(F.theme -> true)

  val markExists = $doc(s"${F.mark}.markStatus" -> "Marked")

  val index = $doc(F.id -> 1)

  val themeIndex = $doc(F.theme -> 1, F.enabled -> 1)

  def likedTags(userId: String): Fu[Set[String]] = api.tagger.tagsByUser(userId)

  def liked(page: Int, userId: String, query: LikedData): Fu[Paginator[Puzzle]] = {
    var condition = $doc("user" -> userId)
    var $andConditions = List.empty[BSONDocument]
    query.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc("tags" -> t))
    }
    query.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc("tags" $exists false), $doc("tags.0" $exists false))
      }
    }
    condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }

    api.tagger.puzzleIds(condition) flatMap { ids =>
      Paginator(
        adapter = new Adapter(
          collection = puzzleColl,
          selector = $inIds(ids) ++ enabled,
          projection = projection,
          sort = $doc(F.rating -> query.sortOrder)
        ),
        currentPage = page,
        maxPerPage = MaxPerPage(15)
      )
    }
  }

  def importedTags(userId: String): Fu[Set[String]] =
    puzzleColl.distinct[String, Set](s"${F.ipt}.tags", (imports ++ $doc(s"${F.ipt}.userId" -> userId) ++ enabled).some)

  def imported(page: Int, userId: String, query: ImportedData): Fu[Paginator[Puzzle]] = {
    var condition = imports ++ $doc(s"${F.ipt}.userId" -> userId) ++ enabled
    var $andConditions = List.empty[BSONDocument]
    query.tags foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.ipt}.tags" -> t))
    }
    query.emptyTag foreach { tg =>
      if (tg == "on") {
        $andConditions = $andConditions :+ $or($doc(s"${F.ipt}.tags" $exists false), $doc(s"${F.ipt}.tags.0" $exists false))
      }
    }
    condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }

    Paginator(
      adapter = new Adapter(
        collection = puzzleColl,
        selector = condition,
        projection = projection,
        sort = $sort desc F.date
      ),
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  /*
    可以按照如下语法优化
    db.puzzle.aggregate(
    { $group: { _id: "$mark.tag", count: { $sum: 1 }}},
    { $sort: { count: -1 } }
  )*/
  def themeTags(): Fu[Set[String]] = fuccess(Set.empty[String])
  //puzzleColl.distinct[String, Set](s"${F.mark}.tag", (notMachine ++ $doc(F.mark $exists true, s"${F.mark}.tag" $exists true) ++ enabled).some)

  def theme(page: Int, userId: String, query: ThemeDataAnd): Fu[Paginator[Puzzle]] = pager.page(
    page = page,
    selector = themeSearchConditionAnd(query),
    projection = projection,
    sort = $doc(F.rating -> query.sortOrder),
    hint = themeIndex,
    count = false
  )

  def themeAndCount(query: ThemeDataAnd): Fu[Int] =
    puzzleColl.countSel(themeSearchConditionAnd(query), ReadPreference.secondaryPreferred, hint = themeIndex.some)

  def themeOrCount(query: ThemeDataOr): Fu[Int] =
    puzzleColl.countSel(themeSearchConditionOr(query), ReadPreference.secondaryPreferred, hint = index.some)

  def themeSearchConditionAnd(query: ThemeDataAnd) = {
    var condition = theme ++ enabled

    if (query.idMin.isDefined || query.idMax.isDefined) {
      var idRange = $doc()
      query.idMin foreach { idMin =>
        idRange = idRange ++ $gte(idMin)
      }
      query.idMax foreach { idMax =>
        idRange = idRange ++ $lte(idMax)
      }
      condition = condition ++ $doc(F.id -> idRange)
    }

    if (query.ratingMin.isDefined || query.ratingMax.isDefined) {
      var ratingRange = $doc()
      query.ratingMin foreach { ratingMin =>
        ratingRange = ratingRange ++ $gte(ratingMin)
      }
      query.ratingMax foreach { ratingMax =>
        ratingRange = ratingRange ++ $lte(ratingMax)
      }
      condition = condition ++ $doc(F.rating -> ratingRange)
    }

    if (query.stepsMin.isDefined || query.stepsMax.isDefined) {
      var stepsRange = $doc()
      query.stepsMin foreach { stepsMin =>
        stepsRange = stepsRange ++ $gte(stepsMin)
      }
      query.stepsMax foreach { stepsMax =>
        stepsRange = stepsRange ++ $lte(stepsMax)
      }
      condition = condition ++ $doc(F.depth -> stepsRange)
    }

    var $andConditions = List.empty[BSONDocument]
    query.pieceColor foreach { tg =>
      val colors = tg.map(_.toLowerCase == "white")
      $andConditions = $andConditions ++ colors.map(t => $doc(F.white -> t))
    }

    query.phase foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.phase" -> t))
    }

    query.moveFor foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.moveFor" -> t))
    }

    query.subject foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.subject" -> t))
    }

    query.strength foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.strength" -> t))
    }

    query.chessGame foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.chessGame" -> t))
    }

    query.comprehensive foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.comprehensive" -> t))
    }

    condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
    //println(BSONDocument.pretty(condition))
    condition
  }

  def themeSearchConditionOr(query: ThemeDataOr, fromPuzzleId: Option[Int] = None) = {
    var condition = notMachine
    condition = condition ++ $doc(F.id -> ($gte(query.idMin | lila.hub.PuzzleHub.minThemeId) ++ $lte(query.idMax | lila.hub.PuzzleHub.maxThemeId)))
    fromPuzzleId foreach { minPuzzleId =>
      condition = condition ++ $doc(F.id -> $gt(minPuzzleId))
    }
    condition = condition ++ markExists ++ enabled

    if (query.ratingMin.isDefined || query.ratingMax.isDefined) {
      var ratingRange = $doc()
      query.ratingMin foreach { ratingMin =>
        ratingRange = ratingRange ++ $gte(ratingMin)
      }
      query.ratingMax foreach { ratingMax =>
        ratingRange = ratingRange ++ $lte(ratingMax)
      }
      condition = condition ++ $doc(F.rating -> ratingRange)
    }

    if (query.stepsMin.isDefined || query.stepsMax.isDefined) {
      var stepsRange = $doc()
      query.stepsMin foreach { stepsMin =>
        stepsRange = stepsRange ++ $gte(stepsMin)
      }
      query.stepsMax foreach { stepsMax =>
        stepsRange = stepsRange ++ $lte(stepsMax)
      }
      condition = condition ++ $doc(F.depth -> stepsRange)
    }

    query.pieceColor foreach { tg =>
      val color = tg map { _.toLowerCase == "white" }
      condition = condition ++ $doc(s"${F.white}" -> $in(color: _*))
    }

    query.tags foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.tag" -> $in(tg: _*))
    }

    query.phase foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.phase" -> $in(tg: _*))
    }

    query.moveFor foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.moveFor" -> $in(tg: _*))
    }

    query.subject foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.subject" -> $in(tg: _*))
    }

    query.strength foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.strength" -> $in(tg: _*))
    }

    query.chessGame foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.chessGame" -> $in(tg: _*))
    }

    query.comprehensive foreach { tg =>
      condition = condition ++ $doc(s"${F.mark}.comprehensive" -> $in(tg: _*))
    }

    //logger.info(BSONDocument.pretty(condition))
    condition
  }

  def machine(page: Int, query: MachineData): Fu[Paginator[Puzzle]] =
    pager.page(
      page = page,
      selector = machineSearchCondition(query),
      projection = projection,
      sort = $doc(query.sortField -> query.sortOrder),
      hint = index,
      count = false
    )

  def machineCount(query: MachineData): Fu[Int] =
    puzzleColl.countSel(machineSearchCondition(query), ReadPreference.secondaryPreferred, hint = index.some)

  def machineSearchCondition(query: MachineData) = {
    var condition = $doc(F.id -> $doc($gte(Puzzle.minMachineId), $lt(Puzzle.maxMachineId))) ++ enabled

    if (query.idMin.isDefined || query.idMax.isDefined) {
      var idRange = $doc()
      query.idMin foreach { idMin =>
        idRange = idRange ++ $gte(Puzzle.minMachineId + (query.idRange * 100000) + idMin)
      }
      query.idMax foreach { idMax =>
        idRange = idRange ++ $lte(Puzzle.minMachineId + (query.idRange * 100000) + idMax)
      }
      condition = condition ++ $doc(F.id -> idRange)
    }

    if (query.ratingMin.isDefined || query.ratingMax.isDefined) {
      var ratingRange = $doc()
      query.ratingMin foreach { ratingMin =>
        ratingRange = ratingRange ++ $gte(ratingMin)
      }
      query.ratingMax foreach { ratingMax =>
        ratingRange = ratingRange ++ $lte(ratingMax)
      }
      condition = condition ++ $doc(F.rating -> ratingRange)
    }

    var $andConditions = List.empty[BSONDocument]
    query.pieceColor foreach { tg =>
      val colors = tg.map(_.toLowerCase == "white")
      $andConditions = $andConditions ++ colors.map(t => $doc(F.white -> t))
    }

    query.phase foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.phase" -> t))
    }

    query.moves foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.moves" -> t))
    }

    query.moveFor foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.moveFor" -> t))
    }

    query.subject foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.subject" -> t))
    }

    query.strength foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.strength" -> t))
    }

    query.comprehensive foreach { tg =>
      $andConditions = $andConditions ++ tg.map(t => $doc(s"${F.mark}.comprehensive" -> t))
    }
    condition = condition ++ (!$andConditions.empty).?? { $and($andConditions: _*) }
    //println(BSONDocument.pretty(condition))
    condition
  }

  def disableByIds(ids: List[PuzzleId], user: User): Funit =
    puzzleColl.update(
      $inIds(ids) ++ $doc(s"${F.ipt}.userId" -> user.id),
      $doc("$set" -> $doc(
        F.vote -> AggregateVote.disable,
        F.enabled -> false
      )),
      multi = true
    ).void >>- bus.publish(lila.hub.actorApi.resource.PuzzleResourceRemove(ids), 'puzzleResourceRemove)

  def capsule(page: Int, ids: List[PuzzleId], offset: Int = 15): Fu[Paginator[Puzzle]] = {
    puzzleColl.byOrderedIds[Puzzle, PuzzleId](ids)(_.id) map { capsules =>
      Paginator.fromList(capsules, page, MaxPerPage(offset))
    }
  }

}
