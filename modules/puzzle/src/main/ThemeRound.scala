package lila.puzzle

import java.net.URLDecoder
import lila.user.User
import org.joda.time.DateTime
import lila.resource.ThemeQuery
import reactivemongo.bson.Macros
import lila.db.dsl.bsonArrayToListHandler

case class ThemeRound(
    _id: String,
    userId: User.ID,
    search: String,
    puzzles: List[ThemeRoundPuzzle],
    lastSelectorId: Option[PuzzleId],
    updatedAt: DateTime
) {

  def id = _id

  def lastTrainId = puzzles.lastOption.map(_.id)

  def toThemeUrl(lastId: PuzzleId) = s"/training/theme/$lastId?next=true&saveQuery=true&showDrawer=false&$search"

  def toRounds(max: Int) =
    puzzles.takeRight(max).map { puzzle =>
      Round(
        id = Round.Id(userId, puzzle.id),
        date = puzzle.date,
        result = Result(puzzle.win),
        rating = 0,
        ratingDiff = if (puzzle.win) 1 else -1,
        url = s"/training/theme/${puzzle.id}?next=false&saveQuery=false&showDrawer=false&$search".some
      )
    }

  def toTags: List[String] = {
    val dataMap = search.split("&").map { kvString =>
      val kv = kvString.split("=")
      if (kv.length > 1) {
        kv(0) -> kv(1)
      } else {
        kv(0) -> ""
      }
    }.filter(_._2.nonEmpty).groupBy {
      case (k, _) => k.replaceAll("\\[\\d+\\]", "")
    }.mapValues(_.map(_._2))

    List(
      dataMap.get("ratingMin").map(_.toList.map(r => s">=${r}分")),
      dataMap.get("ratingMax").map(_.toList.map(r => s"<=${r}分")),
      dataMap.get("stepsMin").map(_.toList.map(r => s">=${r}步")),
      dataMap.get("stepsMax").map(_.toList.map(r => s"<=${r}步")),
      ThemeQuery.parseArrayLabel(dataMap.get("phase").map(_.toList), ThemeQuery.phase),
      ThemeQuery.parseArrayLabel(dataMap.get("moveFor").map(_.toList), ThemeQuery.moveFor),
      ThemeQuery.parseArrayLabel(dataMap.get("pieceColor").map(_.toList), ThemeQuery.pieceColor),
      ThemeQuery.parseArrayLabel(dataMap.get("subject").map(_.toList), ThemeQuery.subject),
      ThemeQuery.parseArrayLabel(dataMap.get("strength").map(_.toList), ThemeQuery.strength),
      ThemeQuery.parseArrayLabel(dataMap.get("chessGame").map(_.toList), ThemeQuery.chessGame),
      ThemeQuery.parseArrayLabel(dataMap.get("comprehensive").map(_.toList), ThemeQuery.comprehensive),
      dataMap.get("tags").map(_.toList)
    ).filter(_.isDefined).flatMap(_.get)
  }

}

object ThemeRound {

  def makeId(user: User.ID, search: String) = s"$user:${searchToId(search)}"

  def normalizedSearch(search: String) = {
    val qs = URLDecoder.decode(search.replace("?", ""), "UTF-8")
    val dataMap = qs.split("&").map { kvString =>
      val kv = kvString.split("=")
      if (kv.length > 1) {
        kv(0) -> kv(1)
      } else {
        kv(0) -> ""
      }
    }.filter(_._2.nonEmpty).groupBy {
      case (k, _) => k.replaceAll("\\[\\d+\\]", "")
    }.mapValues(_.map(_._2))

    val ratingMin = dataMap.get("ratingMin").??(_.headOption)
    val ratingMax = dataMap.get("ratingMax").??(_.headOption)
    val stepsMin = dataMap.get("stepsMin").??(_.headOption)
    val stepsMax = dataMap.get("stepsMax").??(_.headOption)
    val tags = dataMap.get("tags").map(_.toList)
    val phase = dataMap.get("phase").map(_.toList)
    val moveFor = dataMap.get("moveFor").map(_.toList)
    val pieceColor = dataMap.get("pieceColor").map(_.toList)
    val subject = dataMap.get("subject").map(_.toList)
    val strength = dataMap.get("strength").map(_.toList)
    val chessGame = dataMap.get("chessGame").map(_.toList)
    val comprehensive = dataMap.get("comprehensive").map(_.toList)

    val allRawString =
      ratingMin.?? { rm => s"ratingMin=$rm" } +
        ratingMax.?? { rm => s"&ratingMax=$rm" } +
        stepsMin.?? { sm => s"&stepsMin=$sm" } +
        stepsMax.?? { sm => s"&stepsMax=$sm" } +
        tags.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&tags[$index]=$tag"
          }
        } + phase.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&phase[$index]=$tag"
          }
        } + moveFor.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&moveFor[$index]=$tag"
          }
        } + pieceColor.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&pieceColor[$index]=$tag"
          }
        } + subject.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&subject[$index]=$tag"
          }
        } + strength.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&strength[$index]=$tag"
          }
        } + chessGame.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&chessGame[$index]=$tag"
          }
        } + comprehensive.?? { tags =>
          tags.zipWithIndex.foldLeft("") {
            case (query, (tag, index)) => query + s"&comprehensive[$index]=$tag"
          }
        }

    allRawString
  }

  def hashMD5(search: String): String = {
    import java.security.MessageDigest
    val md5 = MessageDigest.getInstance("MD5")
    val encoded = md5.digest(search.getBytes)
    encoded.map("%02x".format(_)).mkString
  }

  def searchToId(search: String) = {
    val ns = normalizedSearch(search)
    hashMD5(ns)
  }

  import lila.db.BSON.BSONJodaDateTimeHandler
  implicit val ThemeRoundPuzzleBSONHandler = Macros.handler[ThemeRoundPuzzle]
  implicit val ThemeRoundPuzzleArrayHandler = bsonArrayToListHandler[ThemeRoundPuzzle]
  implicit val ThemeRoundBSONHandler = Macros.handler[ThemeRound]
}

case class ThemeRoundPuzzle(id: PuzzleId, win: Boolean, date: DateTime)
