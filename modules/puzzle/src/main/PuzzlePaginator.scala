package lila.puzzle

import lila.common.MaxPerPage
import lila.db.dsl._
import reactivemongo.bson._
import reactivemongo.api.ReadPreference
import lila.common.paginator.{ AdapterLike, Paginator }

class PuzzlePaginator(
    collection: Coll,
    maxPerPage: MaxPerPage,
    readPreference: ReadPreference = ReadPreference.secondaryPreferred
) {

  import Puzzle.puzzleBSONHandler

  def page(
    page: Int,
    selector: Bdoc,
    projection: Bdoc,
    sort: Bdoc,
    hint: Bdoc,
    count: Boolean = true
  ): Fu[Paginator[Puzzle]] = Paginator(
    adapter = new PuzzleAdapter(selector, projection, sort, hint, count),
    page,
    maxPerPage
  )

  private class PuzzleAdapter(selector: Bdoc, projection: Bdoc, sort: Bdoc, hint: Bdoc, count: Boolean) extends AdapterLike[Puzzle] {

    def nbResults: Fu[Int] = if (count) collection.countSel(selector, readPreference) else fuccess(Integer.MAX_VALUE)

    def slice(offset: Int, length: Int): Fu[List[Puzzle]] =
      collection.find(selector, projection)
        .hint(hint)
        .sort(sort)
        .skip(offset)
        .list[Puzzle](length, readPreference)
  }

}
