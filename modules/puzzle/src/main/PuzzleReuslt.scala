package lila.puzzle

case class PuzzleResult(
    puzzleId: PuzzleId,
    userId: lila.user.User.ID,
    result: Result,
    rating: (Int, Int),
    puzzleRating: (Int, Int),
    seconds: Int,
    lines: List[ResultNode],
    timeout: Option[Boolean],
    source: PuzzleResult.Source,
    metaData: Option[PuzzleResult.MetaData],
    search: Option[String] = None
)

object PuzzleResult {

  case class MetaData(
      rushId: Option[String] = None,
      homeworkId: Option[String] = None,
      taskId: Option[String] = None,
      raceId: Option[String] = None,
      capsuleId: Option[String] = None
  )

  sealed abstract class Source(val id: String, val name: String)
  object Source {
    case object Puzzle extends Source(id = "puzzle", name = "战术训练")
    case object Theme extends Source(id = "theme", name = "主题战术")
    case object Rush extends Source(id = "rush", name = "战术冲刺")
    case object Race extends Source(id = "race", name = "战术竞速")
    case object Homework extends Source(id = "homework", name = "课后练")
    case object Task extends Source(id = "task", name = "任务")
    case object Error extends Source(id = "error", name = "错题库")
    case object Capsule extends Source(id = "capsule", name = "战术题列表")

    val all = List(Puzzle, Theme, Rush, Race, Homework, Task, Error, Capsule)

    def choices = all.map(s => s.id -> s.name)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Source = byId.get(id) err s"Bad Source $this"
  }

}

