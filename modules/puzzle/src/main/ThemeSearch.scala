package lila.puzzle

import play.api.data.Form

class ThemeShow(
    val id: PuzzleId,
    val queryString: String,
    val searchForm: Form[_],
    val markTags: Set[String],
    val rnf: Boolean,
    val history: Option[ThemeRecord],
    val showDrawer: Boolean
) {

  def search = if (queryString.startsWith("?")) queryString else "?" + queryString

}

object ThemeShow {

  def apply(
    id: PuzzleId,
    queryString: String = "",
    searchForm: Form[_] = null,
    markTags: Set[String] = Set.empty,
    rnf: Boolean = false,
    history: Option[ThemeRecord] = None,
    showDrawer: Boolean = false
  ): Option[ThemeShow] = new ThemeShow(id, queryString, searchForm, markTags, rnf, history, showDrawer).some
}
