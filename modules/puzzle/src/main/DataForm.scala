package lila.puzzle

import chess.format.Forsyth
import chess.format.pgn.{ ParsedPgn, Parser, Reader, Tags }
import play.api.data._
import play.api.data.Forms._
import scalaz.Validation.FlatMap._

object DataForm {

  val round = Form(mapping(
    "win" -> number,
    "seconds" -> number,
    "lines" -> optional(list(mapping(
      "san" -> nonEmptyText,
      "uci" -> nonEmptyText,
      "fen" -> nonEmptyText
    )(ResultNode.apply)(ResultNode.unapply))),
    "timeout" -> optional(boolean),
    "source" -> nonEmptyText.verifying("invalid source", PuzzleResult.Source.keys.contains _),
    "metaData" -> optional(mapping(
      "rushId" -> optional(nonEmptyText),
      "homeworkId" -> optional(nonEmptyText),
      "taskId" -> optional(nonEmptyText),
      "raceId" -> optional(nonEmptyText),
      "capsuleId" -> optional(nonEmptyText)
    )(PuzzleResult.MetaData.apply)(PuzzleResult.MetaData.unapply)),
    "search" -> optional(text)
  )(RoundData.apply)(RoundData.unapply))

  val vote = Form(single(
    "vote" -> number
  ))

  val like = Form(single(
    "like" -> boolean
  ))

  val tag = Form(single(
    "tags" -> text(minLength = 0, maxLength = 200)
  ))

  def fenForm(fromFenTag: Boolean = true) = Form(mapping(
    "pgn" -> nonEmptyText,
    "hasLastMove" -> boolean,
    "puzzleTag" -> optional(text)
  )(PuzzleImportData.apply)(PuzzleImportData.unapply).verifying("invalidPgn", _.checkPuzzle(fromFenTag)))

  case class RoundData(
      win: Int,
      seconds: Int,
      lines: Option[List[ResultNode]],
      timeout: Option[Boolean],
      source: String,
      metaData: Option[PuzzleResult.MetaData],
      search: Option[String]
  ) {
    def linesWithEmpty = lines | List.empty[ResultNode]
  }

  case class PuzzleImportData(
      pgn: String,
      hasLastMove: Boolean,
      puzzleTag: Option[String] = None
  ) {

    private val maxPlies = 20

    def preprocess(userId: Option[String], fromFenTag: Boolean = true) = Parser.full(pgn).flatMap {
      case parsed @ ParsedPgn(_, tags, sans) => Reader.fullWithSans(
        pgn,
        sans => sans.copy(value = sans.value take maxPlies),
        Tags.empty
      ) map {
          /*failureNel(s"Invalid FEN $atFen")*/
          case Reader.Result.Complete(replay) => {
            val moves: List[chess.Move] = replay.moves.map { mod =>
              mod.fold(
                move => move,
                drop => throw sys.error(drop.toString())
              )
            }

            val fen =
              if (fromFenTag) {
                tags.fen.fold(throw sys.error("缺少FEN标签"))(_.value)
                  .replace("w--", "w - -")
                  .replace("b--", "b - -")
              } else {
                Forsyth >> moves.last.situationBefore
              }

            val lastMove = hasLastMove ?? List(moves.last.toUci.uci)
            val solution = if (!hasLastMove) moves else moves.take(moves.length - 1)
            //val color = solution.last.situationBefore.color
            val color = solution.last.situationBefore.color
            val mate = replay.state.situation.check
            val lines = toLine(solution.reverse)
            val date = tags.anyDate

            Puzzle.make(
              gameId = "-",
              history = lastMove,
              fen = fen,
              color = color,
              lines = lines,
              mate = mate,
              ImportMeta.make(
                pgn = pgn,
                tags = makeTags,
                hasLastMove = hasLastMove,
                fenAfterMove = if (hasLastMove) (Forsyth >> solution.last.situationBefore).some else None,
                userId = userId,
                date = date
              )
            )(-1)
          }
          case Reader.Result.Incomplete(replay, err) => throw sys.error(err.toString())
        }
    }

    def toLine(moves: List[chess.Move]): Lines = {
      val uci = moves.head.toUci.uci
      if (moves.length == 1) {
        List(Win(uci))
      } else {
        List(Node(uci, toLine(moves.drop(1))))
      }
    }

    def makeTags = {
      puzzleTag.map { t =>
        val tags = t.trim()
        if (tags.isEmpty) {
          List()
        } else {
          tags.split(",").toList
        }
      }
    }

    def checkPuzzle(fromFenTag: Boolean = true) = MultiPgn.split(pgn, max = 20).value.map { onePgn =>
      checkOne(onePgn, fromFenTag)
    } forall (_ == true)

    def checkOne(pgn: String, fromFenTag: Boolean = true): Boolean = {
      Parser.full(pgn) flatMap {
        case parsed @ ParsedPgn(_, _, _) =>
          if (fromFenTag && parsed.tags.fen.isEmpty) return false
          Reader.fullWithSans(
            pgn,
            sans => sans.copy(value = sans.value take maxPlies),
            Tags.empty
          )
      } fold (_ => false, {
        case Reader.Result.Complete(replay) => {
          val moves: List[chess.Move] = replay.moves.map { mod =>
            mod.fold(
              move => move,
              _ => null
            )
          }
          val moveFilters = moves.filter(_ != null)
          if (replay.moves.length > moveFilters.length) return false
          if (moveFilters.length < 1 || (hasLastMove && moveFilters.length < 2)) return false
        }
        case Reader.Result.Incomplete(_, _) => return false
      })
      return true
    }
  }

}

