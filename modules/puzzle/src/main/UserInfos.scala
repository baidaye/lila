package lila.puzzle

import reactivemongo.bson._

import lila.db.dsl._
import lila.user.User

case class UserInfos(user: User, history: List[Round])

final class UserInfosApi(roundColl: Coll, themeRoundApi: ThemeRoundApi, currentPuzzleId: User => Fu[Option[PuzzleId]]) {

  private val historySize = 15
  private val chartSize = 15

  def apply(user: Option[User], themeShow: Option[ThemeShow]): Fu[Option[UserInfos]] =
    user ?? { apply(_, themeShow) map (_.some) }

  def apply(user: User, themeShow: Option[ThemeShow]): Fu[UserInfos] = for {
    current <- currentPuzzleId(user)
    rounds <- fetchRounds(user.id, current, themeShow)
  } yield new UserInfos(user, rounds)

  private def fetchRounds(userId: User.ID, currentPuzzleId: Option[PuzzleId], themeShow: Option[ThemeShow]): Fu[List[Round]] = {
    if (themeShow.isDefined) {
      themeShow.?? { ts =>
        themeRoundApi.byId(userId, ts.search).map {
          _.?? { d =>
            d.toRounds(historySize)
          }
        }
      }
    } else {
      val idSelector = $doc("$regex" -> BSONRegex(s"^$userId:", "")) /* ++
        currentPuzzleId.?? { id => $doc("$lte" -> s"$userId:${Round encode id}") }*/
      roundColl.find($doc(Round.BSONFields.id -> idSelector))
        .sort($sort desc Round.BSONFields.date)
        .list[Round](historySize atLeast chartSize)
        .map(_.reverse)
    }
  }
}
