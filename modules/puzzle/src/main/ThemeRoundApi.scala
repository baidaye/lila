package lila.puzzle

import lila.user.User
import lila.db.dsl._
import org.joda.time.DateTime

private[puzzle] final class ThemeRoundApi(coll: Coll) {

  import ThemeRound.ThemeRoundPuzzleArrayHandler
  import ThemeRound.ThemeRoundBSONHandler

  def byId(userId: User.ID, search: String): Fu[Option[ThemeRound]] = {
    val id = ThemeRound.makeId(userId, search)
    coll.byId[ThemeRound](id)
  }

  def near10(userId: User.ID): Fu[List[ThemeRound]] =
    coll.find($doc("userId" -> userId))
      .sort($doc("updatedAt" -> -1))
      .list(10)

  def upsert(
    userId: User.ID,
    puzzleId: PuzzleId,
    query: lila.hub.actorApi.puzzle.ThemeQuery
  ): Funit = {
    val now = DateTime.now
    val search = query.rawString
    val id = ThemeRound.makeId(userId, search)
    coll.exists($id(id)) flatMap { exists =>
      // 每次通过检索后都记录lastSelectorId
      val $doc = $set(
        "userId" -> userId,
        "search" -> ThemeRound.normalizedSearch(search),
        "lastSelectorId" -> puzzleId,
        "updatedAt" -> now
      ) ++ (!exists).?? { $set("puzzles" -> List.empty[ThemeRoundPuzzle]) }
      coll.update(
        $id(ThemeRound.makeId(userId, search)),
        $doc,
        upsert = true
      ).void
    }
  }

}
