package lila.puzzle

case class LightTask(
    id: String,
    name: String
)

object LightTask {

  def empty = LightTask("", "")

}
