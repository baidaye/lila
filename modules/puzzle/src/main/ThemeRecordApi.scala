package lila.puzzle

import org.joda.time.DateTime
import lila.user.User
import lila.db.dsl._

private[puzzle] final class ThemeRecordApi(coll: Coll) {

  import ThemeRecord.ThemeRecordBSONHandler

  def byId(id: String): Fu[Option[ThemeRecord]] = coll.byId(id)

  def last(id: String): Fu[ThemeRecord] = byId(id).map { _ | ThemeRecord.default(id) }

  def lastId(id: String): Fu[Int] = last(id).map(_.puzzleId)

  def upsert(
    userId: User.ID,
    puzzleId: PuzzleId,
    query: lila.hub.actorApi.puzzle.ThemeQuery,
    saveQuery: Boolean
  ): Funit = {
    if (saveQuery) {
      val record = ThemeRecord.make(userId, puzzleId, QueryData(
        ratingMin = query.ratingMin,
        ratingMax = query.ratingMax,
        stepsMin = query.stepsMin,
        stepsMax = query.stepsMax,
        tags = query.tags,
        phase = query.phase,
        moveFor = query.moveFor,
        pieceColor = query.pieceColor,
        subject = query.subject,
        strength = query.strength,
        chessGame = query.chessGame,
        comprehensive = query.comprehensive
      ))
      coll.update($id(userId), record, upsert = true).void
    } else {
      coll.update(
        $id(userId),
        $set(
          "puzzleId" -> puzzleId,
          "updateAt" -> DateTime.now
        ),
        upsert = true
      ).void
    }
  }

  def remove(id: String, userId: User.ID): Funit = coll.remove($doc(
    "_id" -> id,
    "userId" -> userId
  )).void

  def list(userId: User.ID): Fu[List[ThemeRecord]] =
    coll.find($doc("userId" -> userId)).sort($doc("updateAt" -> -1)).list(5)

}
