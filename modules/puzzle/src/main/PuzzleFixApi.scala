package lila.puzzle

import lila.db.dsl._
import Puzzle.{ BSONFields => F, Source }

private[puzzle] final class PuzzleFixApi(puzzleColl: Coll) {

  import Puzzle.puzzleBSONHandler

  val notImport = $doc(F.id $lt Puzzle.minImportId)
  val notMachine = $doc(F.id $lt lila.hub.PuzzleHub.maxThemeId)
  val enabled = $doc(F.enabled -> true)

  def find(source: Source, prevId: PuzzleId): Fu[Option[Puzzle]] =
    puzzleColl.find(notMachine ++ $doc(
      F.source -> source.id,
      F.id $gt prevId
    ) ++ notImport ++ enabled).sort($sort asc F.id).uno[Puzzle]

  def delete(id: PuzzleId): Funit =
    puzzleColl.remove($id(id)).void

}
