package lila.patterns

import PatternsRankFeature._

case class PatternsRankFeature(
    _id: ID,
    gameId: String,
    patternsOp: String,
    checkerRole: Option[chess.Role],
    fixCheckerRole: Option[chess.Role],
    controllerRole: Option[List[chess.Role]],
    pinner: Option[Boolean],
    originalInitFen: String,
    originalInitMove: String,
    originalFen: String,
    originalCheckmateFen: String,
    simplifiedFen: String,
    simplifiedCheckmateFen: String,
    checkmateMove: String,
    win: chess.Color,
    order: Int
) {

  def id = _id

  def ply: Int = {
    originalCheckmateFen.split(' ').lastOption flatMap parseIntOption map { move =>
      move * 2 - win.fold(0, 1)
    }
  } | 0

}

object PatternsRankFeature {

  type ID = Int

  def EmptyNeighbor = WithNeighbor(None, None, None)

  case class WithNeighbor(prev: Option[PatternsRankFeature], curr: Option[PatternsRankFeature], next: Option[PatternsRankFeature]) {

    def hasPrev = prev.isDefined

    def hasNext = next.isDefined

    def currOrder = curr.map(_.order)
    def prevOrder = prev.map(_.order)
    def nextOrder = next.map(_.order)

    def currOrderOrEmpty = curr.map(_.order).map(_.toString) | ""
    def prevOrderOrEmpty = prev.map(_.order).map(_.toString) | ""
    def nextOrderOrEmpty = next.map(_.order).map(_.toString) | ""

  }

}

