package lila.patterns

import play.api.data._
import play.api.data.Forms._
import lila.common.Form.stringIn

case class DataForm() {

  import DataForm._

  val patternsRankSearch = Form(
    mapping(
      "color" -> stringIn(colorChoices),
      "sort" -> optional(stringIn(sortChoices)),
      "order" -> optional(number(1, 999999))
    )(PatternsRankSearchData.apply)(PatternsRankSearchData.unapply)
  )

  val patternsRankFeatureSearch = Form(
    mapping(
      "patternsOp" -> text(minLength = 8, maxLength = 8).verifying(PatternsOp.allSet.contains _),
      "color" -> stringIn(colorChoices),
      "order" -> optional(number(1, 999999)),
      "flipped" -> optional(boolean)
    )(PatternsRankFeatureSearchData.apply)(PatternsRankFeatureSearchData.unapply)
  )

  val kingposRankSearch = Form(
    mapping(
      "color" -> stringIn(colorChoices),
      "checkerRole" -> optional(list(stringIn(checkerChoices)))
    )(KingposRankSearchData.apply)(KingposRankSearchData.unapply)
  )

  val kingposRankFeatureSearch = Form(
    mapping(
      "kingSquare" -> text.verifying(squares.contains _),
      "color" -> stringIn(colorChoices),
      "checkerRole" -> optional(list(stringIn(checkerChoices))),
      "order" -> optional(number(1, 999999)),
      "flipped" -> optional(boolean)
    )(KingposRankFeatureSearchData.apply)(KingposRankFeatureSearchData.unapply)
  )

  val patternsPieceRankSearch = Form(
    mapping(
      "color" -> stringIn(colorChoices),
      "checkerRole" -> optional(stringIn(checkerChoices)),
      "fixCheckerRole" -> optional(stringIn(fixCheckerChoices)),
      "controllerRole" -> optional(list(stringIn(controllerChoices))),
      "pinner" -> optional(boolean),
      "order" -> optional(number(1, 999999))
    )(PatternsPieceRankSearchData.apply)(PatternsPieceRankSearchData.unapply)
  )

  val patternsCheckmateRound = Form(
    mapping(
      "win" -> boolean,
      "orient" -> stringIn(colorChoices),
      "typ" -> stringIn(PatternsCheckmate1Typ.choices)
    )(PatternsCheckmateData.apply)(PatternsCheckmateData.unapply)
  )

}

object DataForm {

  case class PatternsRankSearchData(color: String, sort: Option[String], order: Option[Int])
  case class PatternsRankFeatureSearchData(patternsOp: String, color: String, order: Option[Int], flipped: Option[Boolean])
  case class KingposRankSearchData(color: String, checkerRole: Option[List[String]])
  case class KingposRankFeatureSearchData(kingSquare: String, color: String, checkerRole: Option[List[String]], order: Option[Int], flipped: Option[Boolean])
  case class PatternsPieceRankSearchData(color: String, checkerRole: Option[String], fixCheckerRole: Option[String], controllerRole: Option[List[String]], pinner: Option[Boolean], order: Option[Int]) {

    //{"1":"p","2":"n","3":"b","4":"r","5":"q","6":"k"}
    val roleSortMap = Map("p" -> 1, "n" -> 2, "b" -> 3, "r" -> 4, "q" -> 5, "k" -> 6)

    def sortedControllerRole = controllerRole.map { roles =>
      roles.map { role =>
        role -> (roleSortMap.get(role) err "error role sort")
      }.sortBy(_._2).map(_._1)
    }

  }
  case class PatternsCheckmateData(win: Boolean, orient: String, typ: String) {

    def realOrient = chess.Color(orient) | chess.Color.White

    def realTyp = PatternsCheckmate1Typ(typ)

  }

  val sortChoices = List("asc" -> "正序", "desc" -> "倒序")
  val colorChoices = List("white" -> "白棋", "black" -> "黑棋")
  val pinnerChoices = List("true" -> "是", "false" -> "否")
  val checkerChoices = List(chess.Queen -> "后", chess.Rook -> "车", chess.Bishop -> "象", chess.Knight -> "马", chess.Pawn -> "兵").map(d => d._1.forsyth.toString -> d._2)
  val fixCheckerChoices = List(chess.Queen -> "后", chess.Rook -> "车", chess.Bishop -> "象").map(d => d._1.forsyth.toString -> d._2)
  val controllerChoices = List(chess.Queen -> "后", chess.Rook -> "车", chess.Bishop -> "象", chess.Knight -> "马", chess.Pawn -> "兵", chess.King -> "王").map(d => d._1.forsyth.toString -> d._2)
  val squares = "abcdefgh".toCharArray.toList.flatMap { ch =>
    (1 to 8).map(no => s"$ch$no")
  }

}
