package lila.patterns

import lila.db.dsl._
import lila.patterns.DataForm.PatternsRankSearchData
import lila.patterns.PatternsOp.PatternsType

object PatternsRankRepo {

  import BSONHandlers.PatternsRankHandler

  def singleColl = Env.current.PatternsRankColl

  def doubleColl = Env.current.PatternsRankDblColl

  def staleColl = Env.current.PatternsRankStaleColl

  def next(patternsType: PatternsType, data: PatternsRankSearchData): Fu[PatternsRank.WithNeighbor] = {
    patternsType match {
      case PatternsType.Single => next(singleColl, data)
      case PatternsType.Double => next(doubleColl, data)
      case PatternsType.Stalemate => next(staleColl, data)
    }
  }

  private def next(coll: Coll, data: PatternsRankSearchData): Fu[PatternsRank.WithNeighbor] = {
    val $selector = $doc("win" -> data.color)
    for {
      curr <- coll.find($selector ++ data.order.?? { order => $doc("order" -> order) }).sort($doc("order" -> 1)).uno[PatternsRank]
      order = curr.map(_.order) orElse data.order
      prev <- order.?? { od => coll.find($selector ++ $doc("order" $lt od)).sort($doc("order" -> -1)).uno[PatternsRank] }
      next <- order.?? { od => coll.find($selector ++ $doc("order" $gt od)).sort($doc("order" -> 1)).uno[PatternsRank] }
    } yield PatternsRank.WithNeighbor(prev, curr, next)
  }

}
