package lila.patterns

import lila.db.dsl._
import lila.user.User

object PatternsCheckmate1ProgressRepo {

  import BSONHandlers.PatternsCheckmate1ProgressHandler

  private lazy val coll = Env.current.PatternsCheckmate1ProgressColl

  def find(id: User.ID): Fu[Option[PatternsCheckmate1Progress]] =
    coll.byId[PatternsCheckmate1Progress](id)

  def findOrEmpty(id: User.ID): Fu[PatternsCheckmate1Progress] =
    find(id).map(_ | PatternsCheckmate1Progress.empty(id))

  def upsert(progress: PatternsCheckmate1Progress): Funit =
    coll.update($id(progress.id), progress, upsert = true).void

  def lastId(id: User.ID, chapter: PatternsCheckmate1Chapter): Fu[PatternsCheckmate1.ID] =
    find(id) map {
      case None => chapter.minId
      case Some(progress) => progress.lastId(chapter)
    }

  def setProgress(
    id: User.ID,
    chapter: PatternsCheckmate1Chapter,
    checkmate1Id: PatternsCheckmate1.ID,
    win: Boolean,
    orient: chess.Color,
    typ: PatternsCheckmate1Typ
  ): Funit =
    find(id) flatMap {
      case None => {
        val progress =
          PatternsCheckmate1Progress
            .empty(id)
            .withCheckmate1Result(chapter, checkmate1Id, win, orient, typ)
        upsert(progress)
      }
      case Some(progress) => upsert(progress.withCheckmate1Result(chapter, checkmate1Id, win, orient, typ))
    }

  def resetProgress(progress: PatternsCheckmate1Progress): Funit =
    upsert(progress.reset)

}
