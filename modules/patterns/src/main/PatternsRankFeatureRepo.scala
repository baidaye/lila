package lila.patterns

import lila.db.dsl._
import lila.patterns.DataForm.PatternsRankFeatureSearchData
import lila.patterns.PatternsOp.PatternsType

object PatternsRankFeatureRepo {

  import BSONHandlers.PatternsRankFeatureHandler

  def singleColl = Env.current.PatternsRankFeatureColl

  def doubleColl = Env.current.PatternsRankFeatureDblColl

  def staleColl = Env.current.PatternsRankFeatureStaleColl

  def next(patternsType: PatternsType, data: PatternsRankFeatureSearchData): Fu[PatternsRankFeature.WithNeighbor] = {
    patternsType match {
      case PatternsType.Single => next(singleColl, data)
      case PatternsType.Double => next(doubleColl, data)
      case PatternsType.Stalemate => next(staleColl, data)
    }
  }

  private def next(coll: Coll, data: PatternsRankFeatureSearchData): Fu[PatternsRankFeature.WithNeighbor] = {
    val $selector = $doc("patternsOp" -> data.patternsOp, "win" -> data.color)

    for {
      curr <- coll.find($selector ++ data.order.?? { order => $doc("order" -> order) }).sort($doc("order" -> 1)).uno[PatternsRankFeature]
      order = curr.map(_.order) orElse data.order
      prev <- order.?? { od => coll.find($selector ++ $doc("order" $lt od)).sort($doc("order" -> -1)).uno[PatternsRankFeature] }
      next <- order.?? { od => coll.find($selector ++ $doc("order" $gt od)).sort($doc("order" -> 1)).uno[PatternsRankFeature] }
    } yield PatternsRankFeature.WithNeighbor(prev, curr, next)
  }

}
