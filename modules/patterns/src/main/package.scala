package lila

import lila.socket.WithSocket

package object patterns extends PackageObject with WithSocket {

  private[patterns] val logger = lila.log("patterns")

  private[patterns] val minId = 100000

}
