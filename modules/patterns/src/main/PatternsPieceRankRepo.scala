package lila.patterns

import lila.db.dsl._
import lila.patterns.DataForm.PatternsPieceRankSearchData
import lila.patterns.PatternsOp.PatternsType

object PatternsPieceRankRepo {

  import BSONHandlers.PatternsPieceRankHandler

  def singleColl = Env.current.PatternsPieceRankColl

  def doubleColl = Env.current.PatternsPieceRankDblColl

  def staleColl = Env.current.PatternsPieceRankStaleColl

  def next(patternsType: PatternsType, data: PatternsPieceRankSearchData): Fu[PatternsPieceRank.WithNeighbor] = {
    patternsType match {
      case PatternsType.Single => next(patternsType, singleColl, data)
      case PatternsType.Double => next(patternsType, doubleColl, data)
      case PatternsType.Stalemate => next(patternsType, staleColl, data)
    }
  }

  private def next(patternsType: PatternsType, coll: Coll, data: PatternsPieceRankSearchData): Fu[PatternsPieceRank.WithNeighbor] = {
    val $selector =
      $doc("win" -> data.color) ++
        data.checkerRole.?? { checkerRole => $doc("checkerRole" -> checkerRole) } ++
        data.fixCheckerRole.?? { fixCheckerRole => $doc("fixCheckerRole" -> fixCheckerRole) } ++
        data.pinner.?? { pinner => $doc("pinner" -> pinner) } ++
        (patternsType == PatternsType.Single || patternsType == PatternsType.Stalemate).?? {
          data.sortedControllerRole.fold($doc("controllerRole" -> List.empty[String])) { cr => $doc("controllerRole" -> cr) }
        }

    println(reactivemongo.bson.BSONDocument.pretty($selector))
    for {
      curr <- coll.find($selector ++ data.order.?? { order => $doc("order" -> order) }).sort($doc("order" -> 1)).uno[PatternsPieceRank]
      order = curr.map(_.order) orElse data.order
      prev <- order.?? { od => coll.find($selector ++ $doc("order" $lt od)).sort($doc("order" -> -1)).uno[PatternsPieceRank] }
      next <- order.?? { od => coll.find($selector ++ $doc("order" $gt od)).sort($doc("order" -> 1)).uno[PatternsPieceRank] }
    } yield PatternsPieceRank.WithNeighbor(prev, curr, next)
  }

}
