package lila.patterns

import PatternsKingposRankFeature._
import chess.format.Forsyth
import chess.{ Board, Color, Piece, Pos, Situation, White }

case class PatternsKingposRankFeature(
    _id: ID,
    kingSquare: String,
    patternsOp: String,
    checkerRole: Option[chess.Role],
    originalInitFen: String,
    originalInitMove: String,
    originalFen: String,
    originalCheckmateFen: String,
    simplifiedFen: String,
    simplifiedCheckmateFen: String,
    checkmateMove: String,
    win: chess.Color,
    order: Int
) {

  def id = _id

  def kingSquareFen = kingSquareFenOf(kingSquare, win)

}

object PatternsKingposRankFeature {

  type ID = Int

  val fileMap = Map("a" -> 1, "b" -> 2, "c" -> 3, "d" -> 4, "e" -> 5, "f" -> 6, "g" -> 7, "h" -> 8)

  def EmptyNeighbor = WithNeighbor(None, None, None)

  case class WithNeighbor(prev: Option[PatternsKingposRankFeature], curr: Option[PatternsKingposRankFeature], next: Option[PatternsKingposRankFeature]) {

    def hasPrev = prev.isDefined

    def hasNext = next.isDefined

    def currOrder = curr.map(_.order)
    def prevOrder = prev.map(_.order)
    def nextOrder = next.map(_.order)

    def currOrderOrEmpty = curr.map(_.order).map(_.toString) | ""
    def prevOrderOrEmpty = prev.map(_.order).map(_.toString) | ""
    def nextOrderOrEmpty = next.map(_.order).map(_.toString) | ""

  }

  def kingSquareFenOf(kingSquare: String, checkerColor: Color) = {
    val file = fileMap.get(kingSquare.charAt(0).toString) err s"err square $kingSquare"
    val rank = kingSquare.charAt(1).toString.toInt
    Pos.posAt(file, rank) map { pos =>
      val pieces: Map[Pos, Piece] = Map(pos -> (!checkerColor - chess.King))
      val fen = Forsyth >> Situation(Board(pieces, chess.variant.Standard), White)
      fen
    }
  } err s"err square $kingSquare"

}

