package lila.patterns

import PatternsPieceRank._

case class PatternsPieceRank(
    _id: ID,
    patternsOp: String,
    checkerRole: Option[chess.Role],
    fixCheckerRole: Option[chess.Role], // 双子
    controllerRole: Option[List[chess.Role]],
    pinner: Option[Boolean],
    originalInitFen: String,
    originalInitMove: String,
    originalFen: String,
    originalCheckmateFen: String,
    simplifiedFen: String,
    simplifiedCheckmateFen: String,
    checkmateMove: String,
    win: chess.Color,
    order: Int
) {

  def id = _id

}

object PatternsPieceRank {

  type ID = Int

  def EmptyNeighbor = WithNeighbor(None, None, None)

  case class WithNeighbor(prev: Option[PatternsPieceRank], curr: Option[PatternsPieceRank], next: Option[PatternsPieceRank]) {

    def hasPrev = prev.isDefined

    def hasNext = next.isDefined

    def currOrder = curr.map(_.order)
    def prevOrder = prev.map(_.order)
    def nextOrder = next.map(_.order)

    def currOrderOrEmpty = curr.map(_.order).map(_.toString) | ""
    def prevOrderOrEmpty = prev.map(_.order).map(_.toString) | ""
    def nextOrderOrEmpty = next.map(_.order).map(_.toString) | ""

  }

}

