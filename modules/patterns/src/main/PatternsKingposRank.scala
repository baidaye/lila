package lila.patterns

import PatternsKingposRank._

case class PatternsKingposRank(
    _id: ID,
    kingSquare: String,
    checkerRole: Option[chess.Role],
    win: chess.Color,
    percent: Double
) {

  def id = _id

  def percentString = s"${math.round(percent * 10000) / 100D}%"

}

object PatternsKingposRank {

  type ID = Int

}

