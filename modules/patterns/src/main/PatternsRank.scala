package lila.patterns

import PatternsRank._

case class PatternsRank(
    _id: ID,
    patternsOp: String,
    patternsA: String,
    simplifiedFen: String,
    simplifiedFenMost: String,
    win: chess.Color,
    percent: Double,
    order: Int
) {

  def id = _id

  def percentString = s"${math.round(percent * 10000) / 100D}%"

}

object PatternsRank {

  type ID = Int

  def EmptyNeighbor = WithNeighbor(None, None, None)

  case class WithNeighbor(prev: Option[PatternsRank], curr: Option[PatternsRank], next: Option[PatternsRank]) {

    def hasPrev = prev.isDefined

    def hasNext = next.isDefined

    def currOrder = curr.map(_.order)
    def prevOrder = prev.map(_.order)
    def nextOrder = next.map(_.order)

    def currOrderOrEmpty = curr.map(_.order).map(_.toString) | ""
    def prevOrderOrEmpty = prev.map(_.order).map(_.toString) | ""
    def nextOrderOrEmpty = next.map(_.order).map(_.toString) | ""

  }

}

