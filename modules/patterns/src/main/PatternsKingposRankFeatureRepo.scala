package lila.patterns

import lila.db.dsl._
import lila.patterns.DataForm.KingposRankFeatureSearchData
import lila.patterns.PatternsOp.PatternsType

object PatternsKingposRankFeatureRepo {

  import BSONHandlers.PatternsKingposRankFeatureHandler

  def singleColl = Env.current.PatternsKingposRankFeatureColl

  def doubleColl = Env.current.PatternsKingposRankFeatureDblColl

  def staleColl = Env.current.PatternsKingposRankFeatureStaleColl

  def next(patternsType: PatternsType, data: KingposRankFeatureSearchData): Fu[PatternsKingposRankFeature.WithNeighbor] = {
    patternsType match {
      case PatternsType.Single => next(singleColl, data)
      case PatternsType.Double => next(doubleColl, data)
      case PatternsType.Stalemate => next(staleColl, data)
    }
  }

  private def next(coll: Coll, data: KingposRankFeatureSearchData): Fu[PatternsKingposRankFeature.WithNeighbor] = {
    val $selector =
      $doc(
        "kingSquare" -> data.kingSquare,
        "win" -> data.color
      ) ++ data.checkerRole.fold($doc("checkerRole" $exists false)) { cr => $doc("checkerRole" $in cr) }

    for {
      curr <- coll.find($selector ++ data.order.?? { order => $doc("order" -> order) }).sort($doc("order" -> 1)).uno[PatternsKingposRankFeature]
      order = curr.map(_.order) orElse data.order
      prev <- order.?? { od => coll.find($selector ++ $doc("order" $lt od)).sort($doc("order" -> -1)).uno[PatternsKingposRankFeature] }
      next <- order.?? { od => coll.find($selector ++ $doc("order" $gt od)).sort($doc("order" -> 1)).uno[PatternsKingposRankFeature] }
    } yield PatternsKingposRankFeature.WithNeighbor(prev, curr, next)
  }

}
