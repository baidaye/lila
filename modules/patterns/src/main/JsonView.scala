package lila.patterns

import play.api.libs.json._

final class JsonView(animationDuration: scala.concurrent.duration.Duration) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "coords" -> p.coords,
    "rookCastle" -> p.rookCastle,
    "animationDuration" -> p.animationFactor * animationDuration.toMillis,
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d
  )

  def patternsRankNeighbor(rankNeighbor: PatternsRank.WithNeighbor) =
    Json.obj(
      "prev" -> rankNeighbor.prev.map(patternsRank),
      "curr" -> rankNeighbor.curr.map(patternsRank),
      "next" -> rankNeighbor.next.map(patternsRank)
    )

  def patternsRank(pr: PatternsRank) =
    Json.obj(
      "id" -> pr.id,
      "patternsOp" -> pr.patternsOp,
      "patternsA" -> pr.patternsA,
      "simplifiedFen" -> pr.simplifiedFen,
      "simplifiedFenMost" -> pr.simplifiedFenMost,
      "win" -> pr.win.name,
      "percent" -> pr.percentString,
      "order" -> pr.order
    )

  def patternsRankFeatureNeighbor(featureNeighbor: PatternsRankFeature.WithNeighbor) =
    Json.obj(
      "prev" -> featureNeighbor.prev.map(patternsRankFeature),
      "curr" -> featureNeighbor.curr.map(patternsRankFeature),
      "next" -> featureNeighbor.next.map(patternsRankFeature)
    )

  def patternsRankFeature(prf: PatternsRankFeature) =
    Json.obj(
      "id" -> prf.id,
      "gameId" -> prf.gameId,
      "patternsOp" -> prf.patternsOp,
      "checkerRole" -> prf.checkerRole.map(_.forsyth.toString),
      "controllerRole" -> prf.controllerRole.map(_.map(_.forsyth.toString)),
      "pinner" -> prf.pinner,
      "originalInitFen" -> prf.originalInitFen,
      "originalInitMove" -> prf.originalInitMove,
      "originalFen" -> prf.originalFen,
      "originalCheckmateFen" -> prf.originalCheckmateFen,
      "simplifiedFen" -> prf.simplifiedFen,
      "simplifiedCheckmateFen" -> prf.simplifiedCheckmateFen,
      "checkmateMove" -> prf.checkmateMove,
      "win" -> prf.win.name,
      "order" -> prf.order,
      "ply" -> prf.ply
    )

  def kingposRanks(list: List[PatternsKingposRank]) = {
    JsArray(
      list.map(kingposRank)
    )
  }

  def kingposRank(pkr: PatternsKingposRank) =
    Json.obj(
      "id" -> pkr.id,
      "kingSquare" -> pkr.kingSquare,
      "checkerRole" -> pkr.checkerRole.map(_.forsyth.toString),
      "win" -> pkr.win.name,
      "percent" -> pkr.percentString,
      "percentValue" -> pkr.percent * 100
    )

  def kingposRankFeatureNeighbor(featureNeighbor: PatternsKingposRankFeature.WithNeighbor) =
    Json.obj(
      "prev" -> featureNeighbor.prev.map(kingposRankFeature),
      "curr" -> featureNeighbor.curr.map(kingposRankFeature),
      "next" -> featureNeighbor.next.map(kingposRankFeature)
    )

  def kingposRankFeature(pkrf: PatternsKingposRankFeature) =
    Json.obj(
      "id" -> pkrf.id,
      "kingSquare" -> pkrf.kingSquare,
      "kingSquareFen" -> pkrf.kingSquareFen,
      "patternsOp" -> pkrf.patternsOp,
      "checkerRole" -> pkrf.checkerRole.map(_.forsyth.toString),
      "originalInitFen" -> pkrf.originalInitFen,
      "originalInitMove" -> pkrf.originalInitMove,
      "originalFen" -> pkrf.originalFen,
      "originalCheckmateFen" -> pkrf.originalCheckmateFen,
      "simplifiedFen" -> pkrf.simplifiedFen,
      "simplifiedCheckmateFen" -> pkrf.simplifiedCheckmateFen,
      "checkmateMove" -> pkrf.checkmateMove,
      "win" -> pkrf.win.name,
      "order" -> pkrf.order
    )

  def patternsPieceRankNeighbor(pieceRankNeighbor: PatternsPieceRank.WithNeighbor) =
    Json.obj(
      "prev" -> pieceRankNeighbor.prev.map(patternsPieceRank),
      "curr" -> pieceRankNeighbor.curr.map(patternsPieceRank),
      "next" -> pieceRankNeighbor.next.map(patternsPieceRank)
    )

  def patternsPieceRank(ppr: PatternsPieceRank) =
    Json.obj(
      "id" -> ppr.id,
      "patternsOp" -> ppr.patternsOp,
      "checkerRole" -> ppr.checkerRole.map(_.forsyth.toString),
      "controllerRole" -> ppr.controllerRole.map(_.map(_.forsyth.toString)),
      "pinner" -> ppr.pinner,
      "originalInitFen" -> ppr.originalInitFen,
      "originalInitMove" -> ppr.originalInitMove,
      "originalFen" -> ppr.originalFen,
      "originalCheckmateFen" -> ppr.originalCheckmateFen,
      "simplifiedFen" -> ppr.simplifiedFen,
      "simplifiedCheckmateFen" -> ppr.simplifiedCheckmateFen,
      "checkmateMove" -> ppr.checkmateMove,
      "win" -> ppr.win.name,
      "order" -> ppr.order
    )

  def checkmate1Chapter(checkmate1: PatternsCheckmate1) =
    Json.obj(
      "id" -> checkmate1.id,
      "chapter" -> checkmate1.chapter,
      "originalInitFen" -> checkmate1.originalInitFen,
      "originalInitMove" -> checkmate1.originalInitMove,
      "originalFen" -> checkmate1.originalFen,
      "originalCheckmateFen" -> checkmate1.originalCheckmateFen,
      "simplifiedFen" -> checkmate1.simplifiedFen,
      "simplifiedCheckmateFen" -> checkmate1.simplifiedCheckmateFen,
      "checkmateMove" -> checkmate1.checkmateMove,
      "color" -> checkmate1.win.name
    )

  def checkmate1Progress(progress: PatternsCheckmate1Progress) =
    Json.obj(
      "typ" -> progress.typ.id,
      "orient" -> progress.orient.map(_.name),
      "chapters" -> JsArray(
        PatternsCheckmate1Chapter.all.map { ch =>
          Json.obj(
            "id" -> ch.id,
            "name" -> ch.name,
            "color" -> ch.color.name,
            "lastId" -> progress.lastId(ch),
            "progress" -> progress.progressPercentOf(ch)
          )
        }
      )
    )

}

