package lila.patterns

import akka.actor.ActorSystem
import com.typesafe.config.Config

final class Env(
    config: Config,
    hub: lila.hub.Env,
    db: lila.db.Env,
    system: ActorSystem,
    asyncCache: lila.memo.AsyncCache.Builder
) {

  private val CollectionPatternsRank = config getString "collection.patterns_rank"
  private val CollectionPatternsRankFeature = config getString "collection.patterns_rank_feature"
  private val CollectionPatternsKingposRank = config getString "collection.patterns_kingpos_rank"
  private val CollectionPatternsKingposRankFeature = config getString "collection.patterns_kingpos_rank_feature"
  private val CollectionPatternsPieceRank = config getString "collection.patterns_piece_rank"
  private val CollectionPatternsCheckmate1 = config getString "collection.patterns_checkmate1"
  private val CollectionPatternsCheckmate1Progress = config getString "collection.patterns_checkmate1_progress"

  private val CollectionPatternsRankDbl = config getString "collection.patterns_rank_dbl"
  private val CollectionPatternsRankFeatureDbl = config getString "collection.patterns_rank_feature_dbl"
  private val CollectionPatternsKingposRankDbl = config getString "collection.patterns_kingpos_rank_dbl"
  private val CollectionPatternsKingposRankFeatureDbl = config getString "collection.patterns_kingpos_rank_feature_dbl"
  private val CollectionPatternsPieceRankDbl = config getString "collection.patterns_piece_rank_dbl"

  private val CollectionPatternsRankStale = config getString "collection.patterns_rank_stale"
  private val CollectionPatternsRankFeatureStale = config getString "collection.patterns_rank_feature_stale"
  private val CollectionPatternsKingposRankStale = config getString "collection.patterns_kingpos_rank_stale"
  private val CollectionPatternsKingposRankFeatureStale = config getString "collection.patterns_kingpos_rank_feature_stale"
  private val CollectionPatternsPieceRankStale = config getString "collection.patterns_piece_rank_stale"

  private val AnimationDuration = config duration "animation.duration"

  private[patterns] val PatternsRankColl = db(CollectionPatternsRank)
  private[patterns] val PatternsRankFeatureColl = db(CollectionPatternsRankFeature)
  private[patterns] val PatternsKingposRankColl = db(CollectionPatternsKingposRank)
  private[patterns] val PatternsKingposRankFeatureColl = db(CollectionPatternsKingposRankFeature)
  private[patterns] val PatternsPieceRankColl = db(CollectionPatternsPieceRank)
  private[patterns] val PatternsCheckmate1Coll = db(CollectionPatternsCheckmate1)
  private[patterns] val PatternsCheckmate1ProgressColl = db(CollectionPatternsCheckmate1Progress)

  private[patterns] val PatternsRankDblColl = db(CollectionPatternsRankDbl)
  private[patterns] val PatternsRankFeatureDblColl = db(CollectionPatternsRankFeatureDbl)
  private[patterns] val PatternsKingposRankDblColl = db(CollectionPatternsKingposRankDbl)
  private[patterns] val PatternsKingposRankFeatureDblColl = db(CollectionPatternsKingposRankFeatureDbl)
  private[patterns] val PatternsPieceRankDblColl = db(CollectionPatternsPieceRankDbl)

  private[patterns] val PatternsRankStaleColl = db(CollectionPatternsRankStale)
  private[patterns] val PatternsRankFeatureStaleColl = db(CollectionPatternsRankFeatureStale)
  private[patterns] val PatternsKingposRankStaleColl = db(CollectionPatternsKingposRankStale)
  private[patterns] val PatternsKingposRankFeatureStaleColl = db(CollectionPatternsKingposRankFeatureStale)
  private[patterns] val PatternsPieceRankStaleColl = db(CollectionPatternsPieceRankStale)

  val forms = new DataForm

  val jsonView = new JsonView(AnimationDuration)

}

object Env {

  lazy val current: Env = "patterns" boot new Env(
    config = lila.common.PlayApp loadConfig "patterns",
    hub = lila.hub.Env.current,
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system,
    asyncCache = lila.memo.Env.current.asyncCache
  )

}
