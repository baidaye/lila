package lila.patterns

import PatternsCheckmate1._
import chess.format.Forsyth

case class PatternsCheckmate1(
    _id: ID,
    chapter: Int,
    originalInitFen: String,
    originalInitMove: String,
    originalFen: String,
    originalCheckmateFen: String,
    simplifiedFen: String,
    simplifiedCheckmateFen: String,
    checkmateMove: String,
    win: chess.Color
) {

  def id = _id

}

object PatternsCheckmate1 {

  type ID = Int

  def empty(id: Int, chapter: Int) = PatternsCheckmate1(
    _id = id,
    chapter = chapter,
    originalInitFen = Forsyth.initial,
    originalInitMove = "",
    originalFen = Forsyth.initial,
    originalCheckmateFen = Forsyth.initial,
    simplifiedFen = Forsyth.initial,
    simplifiedCheckmateFen = Forsyth.initial,
    checkmateMove = "",
    win = chess.White
  )

}

sealed abstract class PatternsCheckmate1Chapter(val id: Int, val name: String, val color: chess.Color, val minId: Int, val total: Int)
object PatternsCheckmate1Chapter {
  case object H8 extends PatternsCheckmate1Chapter(1, "角h8", chess.White, 100000, 78)
  case object A8 extends PatternsCheckmate1Chapter(2, "角a8", chess.White, 100078, 78)
  case object B8G8 extends PatternsCheckmate1Chapter(3, "边b8-g8", chess.White, 100156, 168)
  case object H2H7 extends PatternsCheckmate1Chapter(4, "边h2-h7", chess.White, 100324, 180)
  case object B2G7White extends PatternsCheckmate1Chapter(5, "中心b2-g7", chess.White, 100504, 222)

  case object H1 extends PatternsCheckmate1Chapter(6, "角h1", chess.Black, 100726, 78)
  case object A1 extends PatternsCheckmate1Chapter(7, "角a1", chess.Black, 100804, 78)
  case object B1G1 extends PatternsCheckmate1Chapter(8, "边b1-g1", chess.Black, 100882, 168)
  case object A2A7 extends PatternsCheckmate1Chapter(9, "边a2-a7", chess.Black, 101050, 180)
  case object B2G7Black extends PatternsCheckmate1Chapter(10, "中心b2-g7", chess.Black, 101230, 162)

  val whites = List(H8, A8, B8G8, H2H7, B2G7White)
  val blacks = List(H1, A1, B1G1, A2A7, B2G7Black)

  val all = whites ++ blacks

  def total = all.map(_.total).sum

  def find(id: Int) = all.find(_.id == id)

  def apply(id: Int) = all.find(_.id == id) err s"Bad Chapter $id"
}

sealed abstract class PatternsCheckmate1Typ(val id: String, val name: String)
object PatternsCheckmate1Typ {
  case object Order extends PatternsCheckmate1Typ("order", "顺序")
  case object Random extends PatternsCheckmate1Typ("random", "随机")

  val all = List(Order, Random)

  def choices = all.map { d => d.id -> d.name }

  def apply(id: String) = all.find(_.id == id) err s"Bad Typ $id"
}

