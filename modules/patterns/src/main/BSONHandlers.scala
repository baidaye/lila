package lila.patterns

import lila.db.dsl._
import lila.db.BSON
import reactivemongo.bson._

object BSONHandlers {

  import lila.db.BSON.BSONJodaDateTimeHandler

  private implicit val RoleBSONHandler = new BSONHandler[BSONString, chess.Role] {
    def read(b: BSONString) = chess.Role.forsyth(b.value.charAt(0)) err s"invalid role ${b.value}"
    def write(r: chess.Role) = BSONString(r.forsyth.toString)
  }

  private implicit val ColorBSONHandler = new BSONHandler[BSONString, chess.Color] {
    def read(b: BSONString) = chess.Color(b.value).err(s"invalid color ${b.value}")
    def write(c: chess.Color) = BSONString(c.name)
  }

  private implicit val stringArrayHandler = bsonArrayToListHandler[String]
  private implicit val roleArrayHandler = bsonArrayToListHandler[chess.Role]

  implicit val PatternsRankHandler = Macros.handler[PatternsRank]
  implicit val PatternsRankFeatureHandler = Macros.handler[PatternsRankFeature]

  implicit val PatternsKingposRankHandler = Macros.handler[PatternsKingposRank]
  implicit val PatternsKingposRankFeatureHandler = Macros.handler[PatternsKingposRankFeature]

  implicit val PatternsPieceRankHandler = Macros.handler[PatternsPieceRank]
  implicit val PatternsCheckmate1Handler = Macros.handler[PatternsCheckmate1]

  private implicit val Checkmate1TypBSONHandler = new BSONHandler[BSONString, PatternsCheckmate1Typ] {
    def read(b: BSONString) = PatternsCheckmate1Typ(b.value)
    def write(c: PatternsCheckmate1Typ) = BSONString(c.id)
  }
  private implicit val Checkmate1ChapterIso = lila.common.Iso.string[PatternsCheckmate1Chapter](c => PatternsCheckmate1Chapter(c.toInt), _.id.toString)
  private implicit val Checkmate1ResultHandler = Macros.handler[PatternsCheckmate1WithResult]
  private implicit val Checkmate1ResultArrayHandler = bsonArrayToListHandler[PatternsCheckmate1WithResult]
  private implicit val Checkmate1ResultsHandler = Macros.handler[PatternsCheckmate1WithResults]
  private implicit val Checkmate1ChapterProgressMapHandler = BSON.MapDocument.MapHandler[PatternsCheckmate1Chapter, PatternsCheckmate1WithResults]

  implicit val PatternsCheckmate1ProgressHandler = Macros.handler[PatternsCheckmate1Progress]

}
