package lila.patterns

import lila.db.dsl._
import scala.util.Random

object PatternsCheckmate1Repo {

  import BSONHandlers.PatternsCheckmate1Handler

  private lazy val coll = Env.current.PatternsCheckmate1Coll

  case object NoCheckmateAvailableException extends lila.base.LilaException {
    val message = "No checkmate available"
  }

  def next(chapter: PatternsCheckmate1Chapter, progress: PatternsCheckmate1Progress, lastId: Int, typ: PatternsCheckmate1Typ, id: Option[PatternsCheckmate1.ID]): Fu[PatternsCheckmate1] = {
    id match {
      case None => {
        val winIds = progress.winOrEmptyIds(chapter)
        val remainderOrTotal = progress.remainderOrTotal(chapter)
        typ match {
          case PatternsCheckmate1Typ.Order => {
            coll.find($doc("chapter" -> chapter.id, "_id" $gt lastId, "_id" $nin winIds))
              .sort($doc("_id" -> 1))
              .uno[PatternsCheckmate1] flatMap {
                case None => {
                  if (lastId == chapter.minId) fufail(NoCheckmateAvailableException)
                  else next(chapter, progress, chapter.minId, typ, id)
                }
                case Some(d) => fuccess(d)
              }
          }
          case PatternsCheckmate1Typ.Random => {
            coll.find($doc("chapter" -> chapter.id, "_id" $nin winIds))
              .sort($doc("_id" -> 1))
              .skip(Random.nextInt(remainderOrTotal))
              .uno[PatternsCheckmate1] flattenWith NoCheckmateAvailableException
          }
          case _ => fufail(NoCheckmateAvailableException)
        }
      }
      case Some(id) => {
        coll.find($doc("chapter" -> chapter.id, "_id" -> id))
          .uno[PatternsCheckmate1] flattenWith NoCheckmateAvailableException
      }
    }

  }

}
