package lila.patterns

import lila.db.dsl._
import lila.patterns.DataForm.KingposRankSearchData
import lila.patterns.PatternsOp.PatternsType

object PatternsKingposRankRepo {

  import BSONHandlers.PatternsKingposRankHandler

  def singleColl = Env.current.PatternsKingposRankColl

  def doubleColl = Env.current.PatternsKingposRankDblColl

  def staleColl = Env.current.PatternsKingposRankStaleColl

  def find(patternsType: PatternsType, data: KingposRankSearchData): Fu[List[PatternsKingposRank]] = {
    patternsType match {
      case PatternsType.Single => find(singleColl, data)
      case PatternsType.Double => find(doubleColl, data)
      case PatternsType.Stalemate => find(staleColl, data)
    }
  }

  private def find(coll: Coll, data: KingposRankSearchData): Fu[List[PatternsKingposRank]] = {
    coll.find(
      $doc("win" -> data.color) ++ data.checkerRole.fold($doc("checkerRole" $exists false)) { cr => $doc("checkerRole" $in cr) }
    ).list[PatternsKingposRank]()
  }

}
