package lila.patterns

import lila.user.User
import org.joda.time.DateTime

case class PatternsCheckmate1Progress(
    _id: User.ID,
    orient: Option[chess.Color],
    typ: PatternsCheckmate1Typ,
    chapters: Map[PatternsCheckmate1Chapter, PatternsCheckmate1WithResults],
    createdAt: DateTime,
    updatedAt: DateTime
) {

  def id = _id

  def lastId(chapter: PatternsCheckmate1Chapter) = chapters.get(chapter).map(_.lastId(chapter)) | chapter.minId

  def winOrEmptyIds(chapter: PatternsCheckmate1Chapter) = chapters.get(chapter).map(_.winOrEmptyIds(chapter)) | Nil

  def remainderOrTotal(chapter: PatternsCheckmate1Chapter) = chapters.get(chapter).map(_.remainderOrTotal(chapter)) | chapter.total

  def progressOf(chapter: PatternsCheckmate1Chapter) = chapters.get(chapter).map(_.winSize) | 0

  def progressPercentOf(chapter: PatternsCheckmate1Chapter) = Math.round(progressOf(chapter) / chapter.total.toDouble * 100)

  def progress = PatternsCheckmate1Chapter.all.map(progressOf).sum

  def progressPercent = Math.round(progress.toDouble / PatternsCheckmate1Chapter.total.toDouble * 100)

  def withCheckmate1Result(chapter: PatternsCheckmate1Chapter, id: PatternsCheckmate1.ID, win: Boolean, orient: chess.Color, typ: PatternsCheckmate1Typ) = {
    val now = DateTime.now
    copy(
      chapters = {
        chapters.get(chapter) match {
          case None => chapters + (chapter -> PatternsCheckmate1WithResults(List(PatternsCheckmate1WithResult(id, win, now))))
          case Some(checkWithResults) => chapters + (chapter -> checkWithResults.appendOrUpdate(id, win))
        }
      },
      orient = orient.some,
      typ = typ,
      updatedAt = now
    )
  }

  def reset = copy(chapters = Map.empty, updatedAt = DateTime.now)

  def isDone(chapter: PatternsCheckmate1Chapter) = progressOf(chapter) >= chapter.total

  def isOngoing(chapter: PatternsCheckmate1Chapter) = progressOf(chapter) >= chapter.total

}

object PatternsCheckmate1Progress {

  def empty(id: User.ID) = PatternsCheckmate1Progress(
    _id = id,
    orient = None,
    typ = PatternsCheckmate1Typ.Order,
    chapters = Map.empty,
    createdAt = DateTime.now,
    updatedAt = DateTime.now
  )

}

case class PatternsCheckmate1WithResults(list: List[PatternsCheckmate1WithResult]) {

  def size = list.size

  def winSize = list.count(_.win)

  def lastId(chapter: PatternsCheckmate1Chapter) = list.sortBy(_.updatedAt).lastOption.map(_.id) | chapter.minId

  def winOrEmptyIds(chapter: PatternsCheckmate1Chapter) = {
    val winIds = list.filter(_.win).map(_.id)
    if (winIds.size == chapter.total) Nil
    else winIds
  }

  def remainderOrTotal(chapter: PatternsCheckmate1Chapter) = {
    val winCount = list.count(_.win)
    val remainderCount = chapter.total - winCount
    if (remainderCount == 0) chapter.total
    else remainderCount
  }

  def exists(id: PatternsCheckmate1.ID) = list.exists(_.exists(id))

  def appendOrUpdate(id: PatternsCheckmate1.ID, win: Boolean) = {
    val now = DateTime.now
    if (exists(id)) {
      copy(list = list.map { check =>
        if (check.exists(id)) {
          if (win && !check.win) {
            check.copy(win = win, updatedAt = now)
          } else {
            check.copy(updatedAt = now)
          }
        } else check
      })
    } else copy(list :+ PatternsCheckmate1WithResult(id, win, now))
  }

}

case class PatternsCheckmate1WithResult(id: PatternsCheckmate1.ID, win: Boolean, updatedAt: DateTime) {

  def fail = !win

  def exists(checkmate1Id: PatternsCheckmate1.ID) = checkmate1Id == id

}
