package lila.security

import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints
import lila.common.{ CellphoneAddress, EmailAddress, LameName, SmsTemplate }
import lila.user.{ FormSelect, TotpSecret, User, UserRepo }
import User.{ ClearPassword, TotpToken }
import akka.actor.ActorSelection

final class DataForm(
    val captcherActor: akka.actor.ActorSelection,
    val smsCaptcherActor: akka.actor.ActorSelection,
    authenticator: lila.user.Authenticator,
    emailValidator: EmailAddressValidator
) extends lila.hub.CaptchedForm with lila.hub.SmsCaptchedForm {

  import DataForm._

  override def captcher: ActorSelection = captcherActor
  override def smsCaptcha: ActorSelection = smsCaptcherActor

  private val anyEmail = trimField(text).verifying(Constraints.emailAddress)
  private val acceptableEmail = anyEmail.verifying(emailValidator.acceptableConstraint)
  private def acceptableUniqueEmail(forUser: Option[User]) =
    acceptableEmail.verifying(emailValidator uniqueConstraint forUser)
  private def trimField(m: Mapping[String]) = m.transform[String](_.trim, identity)

  object signup {

    val maxUsernameLen = 20

    val username = trimField(nonEmptyText).verifying(
      Constraints minLength 2,
      Constraints maxLength maxUsernameLen,
      Constraints.pattern(
        regex = User.newUsernamePrefix,
        error = "usernamePrefixInvalid"
      ),
      Constraints.pattern(
        regex = User.newUsernameSuffix,
        error = "usernameSuffixInvalid"
      ),
      Constraints.pattern(
        regex = User.newUsernameChars,
        error = "usernameCharsInvalid"
      )
    )
      .verifying("用户账号长度不能超过20个字母或10个汉字", u => !validUsernameLength(u))
      .verifying("usernameUnacceptable", u => !LameName.sensitive(u))
      .verifying("该用户账号已保留，如有问题请点击下方“联系我们”", u => !LameName.reserved(u))
      .verifying("usernameAlreadyUsed", u => !UserRepo.nameExists(u).awaitSeconds(4))

    def validUsernameLength(username: String) =
      username.getBytes("GBK").length > maxUsernameLen

    val password = trimField(nonEmptyText).verifying(
      Constraints minLength 6,
      Constraints maxLength 20,
      Constraints.pattern(
        regex = User.passwordRegex,
        error = "密码格式不正确, 6-20位英文数字下划线"
      )
    )

    val website = Form(mapping(
      "username" -> trimField(username),
      "password" -> trimField(password),
      "level" -> text.verifying(FormSelect.Level.keySet.contains _),
      "fp" -> optional(nonEmptyText),
      "gameId" -> text,
      "move" -> text
    )(SignupData.apply)(_ => None)
      .verifying(captchaFailMessage, validateCaptcha _))

    def websiteWithCaptcha = withCaptcha(website)

    def cellphoneConfirm(user: User) = Form(mapping(
      SmsCaptcher.form.cellphone(user.id),
      SmsCaptcher.form.template,
      SmsCaptcher.form.code
    )(CellphoneData.apply)(CellphoneData.unapply)
      .verifying(smsCaptchaFailMessage, validSmsCaptcha(_) awaitSeconds 3)) fill (CellphoneData(
      cellphone = "",
      template = SmsTemplate.Confirm.code,
      code = ""
    ))

    def emailConfirm(user: User) = Form(single("email" -> acceptableUniqueEmail(user.some)))

  }

  object passwordReset {

    val passwordResetByEmail = Form(mapping(
      "email" -> anyEmail, // allow unacceptable emails for BC
      "gameId" -> text,
      "move" -> text
    )(PasswordResetData.apply)(_ => None)
      .verifying(captchaFailMessage, validateCaptcha _))

    def passwordResetByEmailWithCaptcha = withCaptcha(passwordResetByEmail)

    lazy val passwordResetByCellphone = Form(mapping(
      SmsCaptcher.form.cellphone2,
      SmsCaptcher.form.template,
      SmsCaptcher.form.code,
      "gameId" -> text,
      "move" -> text
    )(CellphoneDataWithCaptcha.apply)(CellphoneDataWithCaptcha.unapply)
      .verifying(captchaFailMessage, validateCaptcha _)
      .verifying(smsCaptchaFailMessage, validSmsCaptcha(_) awaitSeconds 3)) fill CellphoneDataWithCaptcha(
      cellphone = "",
      template = SmsTemplate.Login.code,
      code = "",
      gameId = "",
      move = ""
    )

    def passwordResetByCellphoneWithCaptcha = withCaptcha(passwordResetByCellphone)

    val newPassword = Form(single(
      "password" -> trimField(signup.password)
    ))

    val passwdReset = Form(mapping(
      "newPasswd1" -> trimField(signup.password),
      "newPasswd2" -> trimField(signup.password)
    )(PasswordResetConfirm.apply)(PasswordResetConfirm.unapply).verifying(
        "两次密码输入不一致", _.samePasswords
      ))

  }

  object login {

    lazy val usernameOrEmailForm = Form(single(
      "username" -> nonEmptyText
    ))

    lazy val usernameLogin = Form(tuple(
      "username" -> nonEmptyText, // can also be an email
      "password" -> nonEmptyText
    ))

    lazy val cellphoneLogin = Form(mapping(
      SmsCaptcher.form.cellphone2,
      SmsCaptcher.form.template,
      SmsCaptcher.form.code
    )(CellphoneData.apply)(CellphoneData.unapply)
      .verifying(smsCaptchaFailMessage, validSmsCaptcha(_) awaitSeconds 3)) fill (CellphoneData(
      cellphone = "",
      template = SmsTemplate.Login.code,
      code = ""
    ))
  }

  object confirm {

    def changeEmail(u: User) =
      Form(mapping("email" -> acceptableUniqueEmail(u.some).verifying(emailValidator differentConstraint u.email))(ChangeEmail.apply)(ChangeEmail.unapply)).fill(ChangeEmail(email = u.email.??(_.value)))

    def changeCellphone(user: User) = Form(mapping(
      SmsCaptcher.form.cellphone(user.id),
      SmsCaptcher.form.template,
      SmsCaptcher.form.code
    )(CellphoneData.apply)(CellphoneData.unapply)
      .verifying(smsCaptchaFailMessage, validSmsCaptcha(_) awaitSeconds 3)) fill (CellphoneData(
      cellphone = "",
      template = SmsTemplate.Confirm.code,
      code = ""
    ))
  }

  def modEmail(user: User) = Form(single("email" -> acceptableUniqueEmail(user.some)))

  def modPassword = Form(single("password" -> trimField(signup.password)))

  def closeAccount(u: User) = authenticator loginCandidate u map { candidate =>
    Form(single("passwd" -> text.verifying("incorrectPassword", p => candidate.check(ClearPassword(p)))))
  }

}

object DataForm {

  case class SignupData(
      username: String,
      password: String,
      level: String,
      fp: Option[String],
      gameId: String,
      move: String
  ) {

    def fingerPrint = fp.filter(_.nonEmpty) map FingerPrint.apply
  }

  case class MobileSignupData(
      username: String,
      password: String,
      email: String,
      level: String
  ) {
    def realEmail = EmailAddress(email)
  }

  case class PasswordResetData(
      email: String,
      gameId: String,
      move: String
  ) {
    def realEmail = EmailAddress(email)
  }

  case class PasswordResetConfirm(newPasswd1: String, newPasswd2: String) {
    def samePasswords = newPasswd1 == newPasswd2
  }

  case class ChangeEmail(email: String) {
    def realEmail = EmailAddress(email)
  }

  case class CellphoneData(cellphone: String, template: String, code: String) {
    def realCellphone = CellphoneAddress(cellphone)
  }

  case class CellphoneDataWithCaptcha(
      cellphone: String,
      template: String,
      code: String,
      gameId: String,
      move: String
  ) {
    def realCellphone = CellphoneAddress(cellphone)
  }

}

