package lila.security

import play.api.data.validation._
import scala.concurrent.duration._

import lila.common.{ EmailAddress, Domain }
import lila.user.User

/**
 * Validate and normalize emails
 */
final class EmailAddressValidator(
    disposable: DisposableEmailDomain,
    dnsApi: DnsApi,
    checkMail: CheckMail
) {

  private def isAcceptable(email: EmailAddress): Boolean =
    email.domain exists disposable.isOk

  def validate(email: EmailAddress): Option[EmailAddressValidator.Acceptable] =
    (EmailAddress.matches(email.value) && isAcceptable(email)) option EmailAddressValidator.Acceptable(email)

  /**
   * Returns true if an E-mail address is taken by another user.
   * @param email The E-mail address to be checked
   * @param forUser Optionally, the user the E-mail address field is to be assigned to.
   *                If they already have it assigned, returns false.
   * @return
   */
  private def isTakenBySomeoneElse(email: EmailAddress, forUser: Option[User]): Fu[Boolean] =
    lila.user.UserRepo.idByEmail(email.normalize) map (_ -> forUser) map {
      case (None, _) => false
      case (Some(userId), Some(user)) => userId != user.id
      case (_, _) => true
    }

  private def wasUsedTwiceRecently(email: EmailAddress): Fu[Boolean] =
    lila.user.UserRepo.countRecentByPrevEmail(email.normalize).map(1<)

  val acceptableConstraint = Constraint[String]("constraint.email_acceptable") { e =>
    if (isAcceptable(EmailAddress(e))) Valid
    else Invalid(ValidationError("error.email_acceptable"))
  }

  def uniqueConstraint(forUser: Option[User]) = Constraint[String]("constraint.email_unique") { e =>
    val email = EmailAddress(e)
    val (taken, reused) = (isTakenBySomeoneElse(email, forUser) zip wasUsedTwiceRecently(email)) awaitSeconds 2
    if (taken || reused) Invalid(ValidationError("error.email_unique"))
    else Valid
  }

  def differentConstraint(than: Option[EmailAddress]) = Constraint[String]("constraint.email_different") { e =>
    if (than has EmailAddress(e))
      Invalid(ValidationError("error.email_different"))
    else Valid
  }

}

object EmailAddressValidator {
  case class Acceptable(acceptable: EmailAddress)
}
