package lila.importer

import scala.concurrent.duration._
import chess.format.FEN
import lila.game.{ Game, GameRepo, NewGame, Player }

final class Importer(
    delay: FiniteDuration,
    scheduler: akka.actor.Scheduler
) {

  val puzzleApi = Env

  def apply(
    data: ImportData,
    user: Option[String],
    forceId: Option[String] = None,
    white: Option[Player] = None,
    black: Option[Player] = None,
    lichessUnion: Option[Boolean] = None
  ): Fu[Game] = {
    /*
    def gameExists(processing: => Fu[Game]): Fu[Game] =
      GameRepo.findPgnImport(data.pgn) flatMap { _.fold(processing)(fuccess) }

    gameExists {*/
    (data.preprocess(user, white, black, lichessUnion)).future flatMap {
      case Preprocessed(g, replay, initialFen, _) => {
        insertAndFinish(g, initialFen, forceId)
      }
    }
    /*    }*/
  }

  def insertAndFinish(g: NewGame, initialFen: Option[chess.format.FEN] = None, forceId: Option[String] = None): Fu[Game] = {
    val game = forceId.fold(g.sloppy)(g.withId)
    (GameRepo.insertDenormalized(game, initialFen = initialFen)) >> {
      game.pgnImport.flatMap(_.user).isDefined ?? GameRepo.setImportCreatedAt(game)
    } >> {
      GameRepo.finish(
        id = game.id,
        winnerColor = game.winnerColor,
        winnerId = None,
        status = game.status
      )
    } inject game
  }

  def inMemory(data: ImportData): Valid[(Game, Option[FEN])] = data.preprocess(user = none).map {
    case Preprocessed(game, replay, fen, _) => (game withId "synthetic", fen)
  }

}
