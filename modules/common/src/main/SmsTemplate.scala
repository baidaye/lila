package lila.common

case class SmsKey(template: SmsTemplate, to: CellphoneAddress)

sealed abstract class SmsTemplate(val code: String, val name: String, val hasParam: Boolean)
object SmsTemplate {

  case object Login extends SmsTemplate("SMS_184990184", "登录确认验证码", hasParam = true)
  case object Confirm extends SmsTemplate("SMS_198917160", "身份验证验证码", hasParam = true)
  case object SSLExpired extends SmsTemplate("SMS_471360151", "SSL证书过期提醒", hasParam = false)
  val all = List(Login, Confirm, SSLExpired)
  def keys = all.map(_.code).toSet
  def byKey(code: String) = all.find(_.code == code) err s"Bad SmsTemplate $code"
}
