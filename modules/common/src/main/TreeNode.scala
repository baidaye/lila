package lila.common

import play.api.libs.json.{ JsArray, JsObject, Json }

case class TreeNode(
    id: String,
    text: String,
    parent: Option[String] = None,
    children: Option[Boolean] = None, // 是否有子节点
    icon: Option[String] = None,
    typ: Option[String] = None, // folder,file
    state: Option[TreeNode.State] = None,
    liAttr: Option[TreeNode.Attr] = None
) {

}

object TreeNode {

  type Attr = Map[String, String]

  case class State(opened: Boolean, disabled: Boolean, selected: Boolean)

  def toJsArray(nodes: List[TreeNode]): JsArray =
    JsArray(nodes.map(toJson))

  def toJson(node: TreeNode): JsObject =
    Json.obj(
      "id" -> node.id,
      "text" -> node.text
    )
      .add("parent", node.parent)
      .add("children", node.children)
      .add("icon", node.icon)
      .add("type", node.typ)
      .add("state", node.state.map { state =>
        Json.obj(
          "opened" -> state.opened,
          "disabled" -> state.disabled,
          "selected" -> state.selected
        )
      })
      .add("li_attr", node.liAttr.map { attr =>
        var obj = Json.obj()
        attr.foreach {
          case (key, value) => {
            obj = obj.add(key, value.some)
          }
        }
        obj
      })

}
