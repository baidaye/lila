package lila.common

case class FlatTasks(tasks: List[FlatTask]) {

}

case class FlatTask(taskId: String, taskName: String, link: String, num: Int, total: Int, source: FlatTask.Source)

object FlatTask {

  sealed abstract class Source(val id: String, val name: String, val icon: String)
  object Source {
    case object HomeWork extends Source(id = "homework", name = "课后练", icon = "课")
    case object TrainCourse extends Source(id = "trainCourse", name = "训练课", icon = "训")
    case object ThroughTrain extends Source(id = "throughTrain", name = "直通车", icon = "车")
    case object TeamClockIn extends Source(id = "teamClockIn", name = "俱乐部打卡", icon = "俱")
    case object TeamTest extends Source(id = "teamTest", name = "战术测评", icon = "测")

    val all = List(TrainCourse, ThroughTrain, TeamClockIn, TeamTest)

    val mine = List(ThroughTrain, TeamClockIn, TeamTest)

    val keys = all map { v => v.id } toSet

    val byId = all map { v => (v.id, v) } toMap

    def selects = all.map { v => (v.id, v.name) }

    def apply(id: String): Source = byId.get(id) err s"Bad Source $id"
  }
}
