package lila.demonstrate

import play.api.libs.json._

final class JsonView(animationDuration: scala.concurrent.duration.Duration) {

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "coords" -> p.coords,
    "rookCastle" -> p.rookCastle,
    "animationDuration" -> p.animationFactor * animationDuration.toMillis,
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d
  )

}

