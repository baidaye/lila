package lila.demonstrate

import akka.actor._
import com.typesafe.config.Config

final class Env(
    config: Config,
    system: ActorSystem,
    hub: lila.hub.Env,
    db: lila.db.Env
) {

  private val AnimationDuration = config duration "animation.duration"

  lazy val jsonView = new JsonView(
    animationDuration = AnimationDuration
  )

}

object Env {

  lazy val current: Env = "demonstrate" boot new Env(
    config = lila.common.PlayApp loadConfig "demonstrate",
    system = lila.common.PlayApp.system,
    hub = lila.hub.Env.current,
    db = lila.db.Env.current
  )
}
