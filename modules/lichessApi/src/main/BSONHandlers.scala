package lila.lichessApi

import chess.Color
import reactivemongo.bson._
import lila.rating.PerfType
import lila.rating.BSONHandlers.perfTypeKeyHandler
import chess.Clock.{ Config => ClockConfig }

object BSONHandlers {

  import lila.db.dsl.BSONJodaDateTimeHandler
  implicit val OAuthScopeHandler = new BSONHandler[BSONString, OAuthScope] {
    def read(b: BSONString): OAuthScope = OAuthScope.byId(b.value)
    def write(d: OAuthScope) = BSONString(d.key)
  }

  implicit val OAuth2LichessUserBSONHandler = Macros.handler[LichessUser]
  implicit val OAuth2BSONHandler = Macros.handler[OAuth2]

  implicit val ColorBSONHandler = new BSONHandler[BSONBoolean, Color] {
    def read(b: BSONBoolean) = Color(b.value)
    def write(c: Color) = BSONBoolean(c.white)
  }

  implicit val StatusBSONHandler = new BSONHandler[BSONInteger, chess.Status] {
    def read(bsonInt: BSONInteger): chess.Status = chess.Status(bsonInt.value) err s"No such status: ${bsonInt.value}"
    def write(x: chess.Status) = BSONInteger(x.id)
  }

  implicit val ClockBSONHandler = new BSONHandler[BSONDocument, ClockConfig] {
    def read(doc: BSONDocument) = ClockConfig(
      doc.getAs[Int]("limit").get,
      doc.getAs[Int]("increment").get
    )

    def write(config: ClockConfig) = BSONDocument(
      "limit" -> config.limitSeconds,
      "increment" -> config.incrementSeconds
    )
  }

  implicit val RoundMiniUserBSONHandler = Macros.handler[MiniUser]
  implicit val RoundPlayerBSONHandler = Macros.handler[RoundPlayer]
  implicit val RoundBSONHandler = Macros.handler[Round]

}
