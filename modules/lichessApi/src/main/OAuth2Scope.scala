package lila.lichessApi

sealed abstract class OAuthScope(val key: String, val name: String) {
  override def toString = s"Scope($key)"
}

object OAuthScope {

  object Preference {
    case object Read extends OAuthScope("preference:read", "读取首选项")
    case object Write extends OAuthScope("preference:write", "写入首选项")
  }

  object Email {
    case object Read extends OAuthScope("email:read", "阅读电子邮件地址")
  }

  object Challenge {
    case object Read extends OAuthScope("challenge:read", "读取收到的挑战")
    case object Write extends OAuthScope("challenge:write", "发送、接受和拒绝挑战")
    case object Bulk extends OAuthScope("challenge:bulk", "为其他玩家一次创建许多游戏")
  }

  object Study {
    case object Read extends OAuthScope("study:read", "阅读私人研讨和广播")
    case object Write extends OAuthScope("study:write", "创建、更新、删除研讨和广播")
  }

  object Tournament {
    case object Write extends OAuthScope("tournament:write", "创建、更新和加入锦标赛")
  }

  object Puzzle {
    case object Read extends OAuthScope("puzzle:read", "读取谜题活动")
    case object Write extends OAuthScope("puzzle:write", "创建并加入谜题赛事")
  }

  object Team {
    case object Read extends OAuthScope("team:read", "读取私人团队信息")
    case object Write extends OAuthScope("team:write", "加入和离开团队")
    case object Lead extends OAuthScope("team:lead", "管理你领导的团队：发送私信，踢出成员")
  }

  object Msg {
    case object Write extends OAuthScope("msg:write", "向其他棋手发送私信")
  }

  object Board {
    case object Play extends OAuthScope("board:play", "用棋盘 API 下棋")
  }

  object Bot {
    case object Play extends OAuthScope("bot:play", "用机器人 API 下棋")
  }

  object Engine {
    case object Read extends OAuthScope("engine:read", "查看和使用你的外部引擎")
    case object Write extends OAuthScope("engine:write", "创建和更新外部引擎")
  }

  object Web {
    case object Login
      extends OAuthScope("web:login", "Create authenticated website sessions (grants full access!)")
    case object Mod
      extends OAuthScope("web:mod", "Use moderator tools (within the bounds of your permissions)")
  }

  case class Scoped(user: lila.user.User, scopes: List[OAuthScope])

  type Selector = OAuthScope.type => OAuthScope

  val all = List(
    Preference.Read,
    Preference.Write,
    Email.Read,
    Challenge.Read,
    Challenge.Write,
    Challenge.Bulk,
    Study.Read,
    Study.Write,
    Tournament.Write,
    Puzzle.Read,
    Team.Read,
    Team.Write,
    Team.Lead,
    Msg.Write,
    Board.Play,
    Bot.Play,
    Engine.Read,
    Engine.Write,
    Web.Login,
    Web.Mod
  )

  val allMap = all.map { x => x.key -> x }.toMap

  def byId(key: String) = allMap.get(key) err "can not find scope"

}
