package lila.lichessApi

import lila.game.RatingDiffs
import lila.user.User
import lila.pref.Pref
import play.api.libs.json._

final class JsonView(userJsonView: lila.user.JsonView, animationDuration: scala.concurrent.duration.Duration) {

  def pref(pref: lila.pref.Pref) =
    Json.obj(
      "animationDuration" -> pref.animationFactor * animationDuration.toMillis,
      "coords" -> pref.coords,
      "resizeHandle" -> pref.resizeHandle,
      "replay" -> pref.replay,
      "autoQueen" -> pref.autoQueen,
      "clockTenths" -> pref.clockTenths,
      "moveEvent" -> pref.moveEvent
    ).add("is3d" -> pref.is3d)
      .add("clockBar" -> pref.clockBar)
      .add("clockSound" -> pref.clockSound)
      .add("confirmResign" -> (pref.confirmResign == Pref.ConfirmResign.YES))
      .add("keyboardMove" -> (pref.keyboardMove == Pref.KeyboardMove.YES))
      .add("rookCastle" -> (pref.rookCastle == Pref.RookCastle.YES))
      .add("blindfold" -> pref.isBlindfold)
      .add("highlight" -> (pref.highlight || pref.isBlindfold))
      .add("destination" -> (pref.destination && !pref.isBlindfold))
      .add("enablePremove" -> pref.premove)
      .add("showCaptured" -> pref.captured)
      .add("submitMove" -> (pref.submitMove == Pref.SubmitMove.ALWAYS))

  def oAuth2(o: OAuth2) =
    Json.obj(
      "code" -> o.id,
      "scopes" -> o.scopes.map(_.key),
      "accessToken" -> o.accessToken,
      "lichessUser" -> o.lichessUser.map { u =>
        Json.obj(
          "id" -> u.id,
          "username" -> u.username
        )
      },
      "expired" -> o.expired,
      "longTerm" -> o.longTerm,
      "userId" -> o.userId
    )

  def user(r: Round, u: User) = {
    userJsonView.minimal(u, r.perf.some)
  }

  def round(r: Round, white: Option[User], black: Option[User]) = {
    val w = r.white
    val b = r.black
    val perf = r.perf
    Json.obj(
      "id" -> r.id,
      "white" -> Json.obj(
        "local" -> Json.obj(
          "id" -> w.local.id,
          "username" -> w.local.username,
          "rating" -> w.local.rating,
          "ratingDiff" -> w.local.ratingDiff,
          "title" -> w.local.title,
          "provisional" -> w.local.provisional,
          "head" -> white.??(_.head)
        ),
        "lichess" -> Json.obj(
          "id" -> w.lichess.id,
          "username" -> w.lichess.username,
          "rating" -> w.lichess.rating,
          "ratingDiff" -> w.lichess.ratingDiff,
          "title" -> w.lichess.title,
          "provisional" -> w.lichess.provisional
        ),
        "color" -> w.color.name,
        "offeringRematch" -> w.offeringRematch,
        "offeringDraw" -> w.offeringDraw,
        "proposingTakeback" -> w.proposingTakeback,
        "isLichess" -> w.isLichess
      ),
      "black" -> Json.obj(
        "local" -> Json.obj(
          "id" -> b.local.id,
          "username" -> b.local.username,
          "rating" -> b.local.rating,
          "ratingDiff" -> b.local.ratingDiff,
          "title" -> b.local.title,
          "provisional" -> b.local.provisional,
          "head" -> black.??(_.head)
        ),
        "lichess" -> Json.obj(
          "id" -> b.lichess.id,
          "username" -> b.lichess.username,
          "rating" -> b.lichess.rating,
          "ratingDiff" -> b.lichess.ratingDiff,
          "title" -> b.lichess.title,
          "provisional" -> b.lichess.provisional
        ),
        "color" -> b.color.name,
        "offeringRematch" -> b.offeringRematch,
        "offeringDraw" -> b.offeringDraw,
        "proposingTakeback" -> b.proposingTakeback,
        "isLichess" -> b.isLichess
      ),
      "clock" -> Json.obj(
        "initial" -> r.clock.limitSeconds,
        "increment" -> r.clock.incrementSeconds,
        "show" -> r.clock.show
      ),
      "speed" -> r.speed.key,
      "variant" -> Json.obj(
        "key" -> r.variant.key,
        "name" -> r.variant.name
      ),
      "perf" -> Json.obj(
        "key" -> perf.key,
        "name" -> perf.name,
        "icon" -> perf.iconChar.toString
      ),
      "status" -> Json.obj(
        "id" -> r.status.id,
        "name" -> r.status.name
      ),
      "source" -> r.source,
      "rated" -> r.rated,
      "redirected" -> r.redirected,
      "gameStart" -> Json.parse(r.gameStart),
      "createdAt" -> r.createdAt.getMillis
    )
  }

  def diff(d: RatingDiffs) =
    Json.obj(
      "white" -> d.white,
      "black" -> d.black
    )

}
