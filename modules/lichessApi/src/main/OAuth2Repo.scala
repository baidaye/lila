package lila.lichessApi

import lila.db.dsl._
import lila.user.User
import org.joda.time.DateTime
import reactivemongo.bson._

object OAuth2Repo {

  private lazy val coll = Env.current.CollOAuth2
  import BSONHandlers._

  def $active(userId: User.ID) = $doc("userId" -> userId, "active" -> true)

  def findList(userId: User.ID): Fu[List[OAuth2]] =
    coll.find($doc("userId" -> userId))
      .sort($doc("createdAt" -> -1))
      .list[OAuth2]()

  def findActive(userId: User.ID): Fu[Option[OAuth2]] =
    coll.uno[OAuth2]($active(userId)).map(_.filter(!_.expired))

  def byId(state: String): Fu[Option[OAuth2]] = coll.byId[OAuth2](state)

  def insert(o: OAuth2): Funit = coll.insert(o).void

  def update(o: OAuth2): Funit =
    coll.update($id(o.id), o).void

  def keepLongTerm(state: String, longTerm: Boolean): Funit =
    coll.update(
      $id(state),
      $set("longTerm" -> longTerm)
    ).void

  def setAccountInfo(state: String, account: LichessUser): Funit =
    coll.update(
      $id(state),
      $set("lichessUser" -> account)
    ).void

  def setActive(state: String, active: Boolean): Funit =
    coll.update(
      $id(state),
      $set("active" -> active)
    ).void

  def setUnActive(state: String): Funit =
    coll.update(
      $doc($id(state)),
      $set("active" -> false)
    ).void

  def setAllUnActive(userId: User.ID): Funit =
    coll.update(
      $doc($active(userId)),
      $set("active" -> false),
      multi = true
    ).void

}
