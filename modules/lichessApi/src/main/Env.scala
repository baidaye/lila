package lila.lichessApi

import akka.actor.ActorSystem
import com.typesafe.config.Config
import lila.common.{ AtMost, Every, ResilientScheduler }

import scala.concurrent.duration._

final class Env(
    config: Config,
    db: lila.db.Env,
    system: ActorSystem,
    userJsonView: lila.user.JsonView,
    importer: lila.explorer.ExplorerImporter,
    finisher: lila.round.Finisher,
    playban: lila.playban.PlaybanApi
) {

  private[lichessApi] val AnimationDuration = config duration "animation.duration"
  private[lichessApi] val CollectionOAuth2 = config getString "collection.oauth2"
  private[lichessApi] val CollectionRound = config getString "collection.round"
  private[lichessApi] val CollOAuth2 = db(CollectionOAuth2)
  private[lichessApi] val CollRound = db(CollectionRound)

  lazy val oAuth2Api = new OAuth2Api(system.lilaBus)
  lazy val roundApi = new RoundApi(system.lilaBus, importer, finisher, playban)

  lazy val jsonView = new JsonView(userJsonView = userJsonView, animationDuration = AnimationDuration)

  lazy val forms = new DataForm()

  ResilientScheduler(
    every = Every(10 minute),
    atMost = AtMost(30 seconds),
    logger = logger,
    initialDelay = 1 minute
  ) { roundApi.syncRound() }(system)

}

object Env {

  lazy val current: Env = "lichessApi" boot new Env(
    config = lila.common.PlayApp loadConfig "lichessApi",
    db = lila.db.Env.current,
    system = lila.common.PlayApp.system,
    userJsonView = lila.user.Env.current.jsonView,
    importer = lila.explorer.Env.current.importer,
    finisher = lila.round.Env.current.finisher,
    playban = lila.playban.Env.current.api
  )
}
