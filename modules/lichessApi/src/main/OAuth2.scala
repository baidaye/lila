package lila.lichessApi

import lila.user.User
import org.joda.time.DateTime

case class OAuth2(
    _id: String,
    userId: User.ID,
    verifierCode: String,
    scopes: List[OAuthScope],
    accessToken: Option[String],
    expireAt: Option[DateTime],
    lichessUser: Option[LichessUser],
    active: Boolean,
    longTerm: Boolean,
    createdAt: DateTime
) {

  def id = _id

  def expired = expireAt.isEmpty || expireAt.??(_.isBeforeNow)

}

object OAuth2 {

  def make(state: String, verifierCode: String, scopes: List[OAuthScope], userId: User.ID) =
    OAuth2(
      _id = state,
      userId = userId,
      scopes = scopes,
      verifierCode = verifierCode,
      accessToken = None,
      expireAt = None,
      lichessUser = None,
      active = false,
      longTerm = false,
      createdAt = DateTime.now
    )

}

case class LichessUser(id: String, username: String)
