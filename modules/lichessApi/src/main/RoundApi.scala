package lila.lichessApi

import chess.Status
import lila.user.{ User, UserRepo }
import lila.explorer.ExplorerImporter
import lila.game.actorApi.FinishGame
import lila.game.{ Game, GameRepo, Player, RatingDiffs }
import lila.round.Finisher
import play.api.libs.json.Json
import play.api.libs.ws.WS
import play.api.Play.current

final class RoundApi(lilaBus: lila.common.Bus, importer: ExplorerImporter, finisher: Finisher, playban: lila.playban.PlaybanApi) {

  lilaBus.subscribeFun('finishGame) {
    case lila.game.actorApi.FinishGame(game, _, _) => {
      game.lichessUnion.?? {
        game.lichessUnionLocalPlayer.?? { localPlayer =>
          val winner = game.situation.winner
          game.status match {
            case Status.Aborted => winner.??(w => playban.abort(lila.game.Pov(game, !w), Set(chess.White, chess.Black)))
            case Status.Mate => playban.other(game, _ => chess.Status.Mate, winner)
            case Status.Resign => playban.other(game, _ => chess.Status.Resign, winner)
            case Status.Stalemate => playban.other(game, _ => chess.Status.Stalemate, winner)
            case Status.Timeout => winner.??(w => playban.rageQuit(game, !w))
            case Status.Draw => playban.other(game, _ => chess.Status.Draw, winner)
            case Status.Outoftime => winner.??(w => playban.flag(game, !w))
            case Status.Cheat => playban.other(game, _ => chess.Status.Cheat, winner)
            case Status.NoStart => game.playerWhoDidNotMove ?? { culprit =>
              playban.noStart(lila.game.Pov(game, culprit))
            }
            case Status.VariantEnd => playban.other(game, _ => chess.Status.VariantEnd, game.situation.winner)
            case _ => funit
          }
        }
      }
    }
  }

  def byId(id: String): Fu[Option[Round]] = RoundRepo.byId(id)

  def getCurrentRound(userId: User.ID): Fu[List[Round]] =
    RoundRepo.findCurrentRound(userId)

  def create(data: DataForm.RoundData, user: User): Fu[Round] = {
    val w = data.white
    val b = data.black
    val whiteIsLocal = w.userId.isDefined && w.userId.has(user.id)
    val blackIsLocal = b.userId.isDefined && b.userId.has(user.id)
    val d =
      Round.make(
        gameId = data.gameId,
        white = RoundPlayer.make(
          local = {
            if (whiteIsLocal) {
              val perf = user.perfs(data.perf)
              MiniUser(
                id = user.id,
                username = user.username,
                title = user.title.map(_.value),
                rating = perf.intRating,
                provisional = Some(perf.provisional),
                ratingDiff = None
              )
            } else {
              MiniUser(
                id = s"${w.id}_lichess",
                username = s"${w.username}_lichess",
                rating = w.rating,
                provisional = w.provisional,
                title = w.title,
                ratingDiff = None
              )
            }
          },
          lichess = MiniUser(
            id = w.id,
            username = w.username,
            rating = w.rating,
            provisional = w.provisional,
            title = w.title,
            ratingDiff = None
          ),
          isLichess = !whiteIsLocal,
          color = chess.White
        ),
        black = RoundPlayer.make(
          local = {
            if (blackIsLocal) {
              val perf = user.perfs(data.perf)
              MiniUser(
                id = user.id,
                username = user.username,
                title = user.title.map(_.value),
                rating = perf.intRating,
                provisional = Some(perf.provisional),
                ratingDiff = None
              )
            } else {
              MiniUser(
                id = s"${b.id}_lichess",
                username = s"${b.username}_lichess",
                title = b.title,
                rating = b.rating,
                provisional = b.provisional,
                ratingDiff = None
              )
            }
          },
          lichess = MiniUser(
            id = b.id,
            username = b.username,
            title = b.title,
            rating = b.rating,
            provisional = b.provisional,
            ratingDiff = None
          ),
          isLichess = !blackIsLocal,
          color = chess.Black
        ),
        clock = data.clk,
        rated = data.rated,
        status = chess.Status(data.status) | chess.Status.Started,
        source = data.source,
        winner = data.winner.map(w => chess.Color(w) err s"can not apply color $w"),
        gameStart = data.gameStart,
        userId = user.id
      )

    byId(data.gameId) flatMap {
      case None => RoundRepo.upsert(d).inject(d)
      case Some(r) => {
        val newRound = r.copy(
          white = d.white,
          black = d.black,
          clock = d.clock,
          status = d.status,
          gameStart = d.gameStart
        )
        RoundRepo.upsert(newRound).inject(newRound)
      }
    }
  }

  def updatePlayer(round: Round, data: DataForm.RoundPlayerData, userId: User.ID): Funit =
    round.isLocal(userId).?? {
      RoundRepo.updateLichessPlayer(round, data)
    }

  def finish(round: Round, data: DataForm.RoundFinishData, userId: User.ID): Fu[Option[RatingDiffs]] =
    round.isLocal(userId).?? {
      RoundRepo.finish(round, data.status, data.winner) >> {
        val w = round.white
        val b = round.black
        for {
          whiteLocalUser <- w.isLocal.?? { UserRepo.byId(w.local.id) }
          blackLocalUser <- b.isLocal.?? { UserRepo.byId(b.local.id) }
          whiteUser = if (w.isLocal) { whiteLocalUser } else { blackLocalUser.map(bu => mergeRating(round, w.toVirtualUser, bu)) }
          blackUser = if (b.isLocal) { blackLocalUser } else { whiteLocalUser.map(wu => mergeRating(round, b.toVirtualUser, wu)) }
          winnerColor = data.winner.map(w => chess.Color(w == chess.White.name))
          winnerUserId = winnerColor.map(wc => round.winnerUserId(wc))
          whitePlayer = w.toGamePlayer(winnerColor)
          blackPlayer = b.toGamePlayer(winnerColor)
          gameOption <- importGame(round.id, userId.some, whitePlayer, blackPlayer)
          _ <- gameOption.??(g => GameRepo.setLichessGame(g.id, round.rated, winnerUserId))
          diff <- gameOption.?? { game =>
            val finish = FinishGame(game.copy(mode = chess.Mode.Rated), whiteUser, blackUser)
            val diff = round.rated.?? {
              for {
                diff <- finisher.updateCountAndPerfs(finish)
                _ <- diff.??(RoundRepo.setLocalRatingDiff(round, _))
                _ <- data.ratingDiff.??(RoundRepo.setLichessRatingDiff(round, _))
              } yield diff
            }
            lilaBus.publish(finish, 'finishGame)
            diff
          }
        } yield diff
      }
    }

  private def mergeRating(round: Round, virtual: User, other: User) = {
    val virtualPerf = virtual.perfs(round.perf)
    val otherPerf = other.perfs(round.perf)
    virtual.copy(
      perfs = lila.user.Perfs.copyFrom(otherPerf.copy(glicko = otherPerf.glicko.copy(rating = virtualPerf.intRating)))
    )
  }

  private def importGame(
    id: Game.ID,
    userId: Option[User.ID],
    white: Player,
    black: Player
  ): Fu[Option[Game]] =
    GameRepo game id flatMap {
      case Some(game) => fuccess(game.some)
      case None => importer.fetchAndImport(
        id,
        s"https://lichess.org/game/export/$id?literate=1".some,
        userId,
        Some(white),
        Some(black),
        Some(true)
      )
    }

  def setRedirected(round: Round, userId: User.ID): Funit =
    round.isLocal(userId).?? {
      RoundRepo.setRedirected(round)
    }

  def syncRound(): Funit = {
    RoundRepo.findAllCurrentRound().flatMap { rounds =>
      rounds.nonEmpty.?? {
        fetchGames(rounds.map(_.id)).map { ndGames =>
          val rwg = rounds.map { round =>
            (round, ndGames.find(_.id == round.id))
          }

          rwg.foreach {
            case (round, gameOption) => {
              gameOption match {
                case Some(game) => game.isFinished.?? {
                  val data = DataForm.RoundFinishData(game.status.id, game.winner.map(_.name), game.ratingDiffs)
                  finish(round, data, round.defaultLocalId)
                }
                case None => round.isOld.?? {
                  RoundRepo.remove(round)
                }
              }
            }
          }
        }
      }
    }
  }

  def fetchGames(gameIds: List[String]): Fu[List[NDGame]] = {
    WS.url("https://lichess.org/api/games/export/_ids")
      .withHeaders("Content-Type" -> "text/plain", "Accept" -> "application/x-ndjson")
      .post(gameIds.mkString(",")) flatMap {
        case res if res.status == 200 => {
          fuccess(parseNDJson(res.body))
        }
        case res => {
          logger.error(s"fetchRounds fail：${res.status} ${res.body}")
          fuccess(Nil)
        }
      } recover {
        case e: Exception => {
          logger.error(s"fetchGames Exception：${e.getMessage} ${e.printStackTrace()}")
          Nil
        }
      }
  }

  case class NDGame(id: String, status: chess.Status, winner: Option[chess.Color], ratingDiffs: Option[DataForm.RoundRatingDiffData]) {
    def isFinished = status.id >= chess.Status.Aborted.id
  }

  private def parseNDJson(ndjson: String): List[NDGame] = {
    ndjson.trim.nonEmpty.?? {
      ndjson.split("\n").toList.map { one =>
        val jsonOne = Json.parse(one)
        val idOption = jsonOne str "id"
        val statusOption = jsonOne str "status"
        val winnerOption = jsonOne str "winner"
        val rated = (jsonOne boolean "rated") | false
        val playersOption = rated.??(jsonOne obj "players")
        val whiteOption = playersOption.??(_ obj "white")
        val blackOption = playersOption.??(_ obj "black")
        val whiteRatingDiffOption = whiteOption.??(_ int "ratingDiff")
        val blackRatingDiffOption = blackOption.??(_ int "ratingDiff")
        val ratingDiffs = (whiteRatingDiffOption |@| blackRatingDiffOption).tupled map {
          case (whiteRatingDiff, blackRatingDiff) => {
            DataForm.RoundRatingDiffData(whiteRatingDiff, blackRatingDiff)
          }
        }
        (idOption |@| statusOption).tupled ?? {
          case (id, status) => {
            applyGameStatus(status).map { s =>
              NDGame(id, s, winnerOption.??(chess.Color.apply), ratingDiffs)
            }
          }
        }
      }
    }.filter(_.isDefined).map(_.get)
  }

  private def applyGameStatus(status: String): Option[chess.Status] = {
    status match {
      case "created" => chess.Status.Created.some
      case "started" => chess.Status.Started.some
      case "aborted" => chess.Status.Aborted.some
      case "mate" => chess.Status.Mate.some
      case "resign" => chess.Status.Resign.some
      case "stalemate" => chess.Status.Stalemate.some
      case "timeout" => chess.Status.Timeout.some
      case "draw" => chess.Status.Draw.some
      case "outoftime" => chess.Status.Outoftime.some
      case "cheat" => chess.Status.Cheat.some
      case "noStart" => chess.Status.NoStart.some
      case "variantEnd" => chess.Status.VariantEnd.some
      case _ => None
    }
  }

}
