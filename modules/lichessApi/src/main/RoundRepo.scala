package lila.lichessApi

import lila.db.dsl._
import lila.user.User
import lila.rating.PerfType
import DataForm._
import lila.game.RatingDiffs

object RoundRepo {

  private lazy val coll = Env.current.CollRound
  import BSONHandlers.RoundBSONHandler

  def byId(gameId: String): Fu[Option[Round]] =
    coll.byId[Round](gameId)

  def findCurrentRound(userId: User.ID): Fu[List[Round]] =
    coll.find(
      $or(
        $doc("white.local.id" -> userId),
        $doc("black.local.id" -> userId)
      ) ++ $doc("status" -> chess.Status.Started.id)
    ).list()

  def findAllCurrentRound(): Fu[List[Round]] =
    coll.find($doc("status" -> chess.Status.Started.id)).list()

  def upsert(g: Round): Funit =
    coll.update($id(g.id), g, upsert = true).void

  def finish(round: Round, status: Int, winner: Option[String]): Funit =
    coll.update(
      $id(round.id),
      $set("status" -> status) ++ winner.?? { c =>
        chess.Color(c).?? { c =>
          $set("winner" -> c.white)
        }
      }
    ).void

  def setRedirected(round: Round): Funit =
    coll.update(
      $id(round.id),
      $set("redirected" -> true)
    ).void

  def setLichessRatingDiff(round: Round, data: DataForm.RoundRatingDiffData): Funit =
    coll.update(
      $id(round.id),
      $set(
        "white.lichess.ratingDiff" -> data.white,
        "black.lichess.ratingDiff" -> data.black
      )
    ).void

  def setLocalRatingDiff(round: Round, diffs: RatingDiffs): Funit =
    coll.update(
      $id(round.id),
      $set(
        "white.local.ratingDiff" -> diffs.white,
        "black.local.ratingDiff" -> diffs.black
      )
    ).void

  def updateLichessPlayer(round: Round, data: DataForm.RoundPlayerData): Funit =
    coll.update(
      $id(round.id),
      $set(
        "white.lichess.rating" -> data.white.rating,
        "white.lichess.title" -> data.white.title,
        "white.lichess.provisional" -> data.white.provisional,
        "black.lichess.rating" -> data.black.rating,
        "black.lichess.title" -> data.black.title,
        "black.lichess.provisional" -> data.black.provisional
      ) ++ round.white.isLichess.?? {
          $set(
            "white.local.rating" -> data.white.rating,
            "white.local.title" -> data.white.title,
            "white.local.provisional" -> data.white.provisional
          )
        } ++ round.black.isLichess.?? {
          $set(
            "black.local.rating" -> data.black.rating,
            "black.local.title" -> data.black.title,
            "black.local.provisional" -> data.black.provisional
          )
        }
    ).void

  def remove(round: Round): Funit =
    coll.remove($id(round.id)).void

}
