package lila.lichessApi

import lila.rating.PerfType
import play.api.data._
import play.api.data.Forms._

final class DataForm() {

  import DataForm._

  val oAuth2Create = Form(mapping(
    "state" -> nonEmptyText,
    "verifierCode" -> nonEmptyText,
    "scopes" -> list(nonEmptyText)
  )(OAuth2Create.apply)(OAuth2Create.unapply))

  val oAuth2Update = Form(mapping(
    "state" -> nonEmptyText,
    "accessToken" -> nonEmptyText,
    "expiresIn" -> number,
    "userId" -> nonEmptyText,
    "username" -> nonEmptyText
  )(OAuth2Update.apply)(OAuth2Update.unapply))

  val keepLongTerm = Form(mapping(
    "state" -> nonEmptyText,
    "longTerm" -> boolean
  )(KeepLongTerm.apply)(KeepLongTerm.unapply))

  val playerMapping: Mapping[PlayerData] =
    mapping(
      "id" -> nonEmptyText,
      "username" -> nonEmptyText,
      "rating" -> number,
      "title" -> optional(nonEmptyText),
      "provisional" -> optional(boolean),
      "userId" -> optional(nonEmptyText),
      "color" -> nonEmptyText
    )(PlayerData.apply)(PlayerData.unapply)

  val round = Form(mapping(
    "gameId" -> nonEmptyText,
    "white" -> playerMapping,
    "black" -> playerMapping,
    "clock" -> mapping(
      "limit" -> number,
      "increment" -> number
    )(ClockData.apply)(ClockData.unapply),
    "rated" -> boolean,
    "status" -> number,
    "source" -> nonEmptyText,
    "winner" -> optional(nonEmptyText),
    "gameStart" -> nonEmptyText
  )(RoundData.apply)(RoundData.unapply))

  val roundPlayer = Form(mapping(
    "white" -> playerMapping,
    "black" -> playerMapping
  )(RoundPlayerData.apply)(RoundPlayerData.unapply))

  val roundFinish = Form(mapping(
    "status" -> number,
    "winner" -> optional(nonEmptyText),
    "ratingDiff" -> optional(mapping(
      "white" -> number,
      "black" -> number
    )(RoundRatingDiffData.apply)(RoundRatingDiffData.unapply))
  )(RoundFinishData.apply)(RoundFinishData.unapply))

}

object DataForm {

  case class OAuth2Create(state: String, verifierCode: String, scopes: List[String]) {
    def realScopes = scopes.map(OAuthScope.byId)
  }
  case class OAuth2Update(state: String, accessToken: String, expiresIn: Int, userId: String, username: String)
  case class KeepLongTerm(state: String, longTerm: Boolean)

  case class ClockData(limit: Int, increment: Int)
  case class PlayerData(id: String, username: String, rating: Int, title: Option[String], provisional: Option[Boolean], userId: Option[String], color: String)
  case class RoundData(gameId: String, white: PlayerData, black: PlayerData, clock: ClockData, rated: Boolean, status: Int, source: String, winner: Option[String], gameStart: String) {

    def clk = chess.Clock.Config(clock.limit, clock.increment)

    def speed = chess.Speed(clk)

    def perf = PerfType(speed.key) | PerfType.Classical
  }
  case class RoundPlayerData(white: PlayerData, black: PlayerData)
  case class RoundFinishData(status: Int, winner: Option[String], ratingDiff: Option[RoundRatingDiffData])
  case class RoundRatingDiffData(white: Int, black: Int)
}

