package lila.lichessApi

import lila.game.IdGenerator
import lila.user.User
import lila.rating.PerfType
import org.joda.time.DateTime

case class Round(
    _id: String,
    white: RoundPlayer,
    black: RoundPlayer,
    clock: chess.Clock.Config,
    rated: Boolean,
    status: chess.Status,
    source: String,
    winner: Option[chess.Color],
    gameStart: String,
    redirected: Boolean,
    createdBy: User.ID,
    createdAt: DateTime
) {

  def id = _id

  def speed = chess.Speed(clock)

  def variant = chess.variant.Standard

  def perf = PerfType(speed.key) | PerfType.Classical

  def players = List(white, black)

  def isLocal(userId: User.ID): Boolean =
    players.exists(_.local.id == userId)

  def player(userId: User.ID): RoundPlayer =
    players.find(_.local.id == userId) err s"can not find user of $userId"

  def opponent(userId: User.ID): RoundPlayer =
    color(userId).fold(black, white)

  def color(userId: User.ID): chess.Color =
    player(userId).color

  def winnerUserId(winner: chess.Color): String =
    winner.fold(white.local.id, black.local.id)

  def defaultLocalId = players.find(_.isLocal).map(_.local.id) err s"can not find local user"

  def isOld = createdAt.isBefore(DateTime.now.minusDays(2))

}

object Round {

  def make(
    gameId: String,
    white: RoundPlayer,
    black: RoundPlayer,
    clock: chess.Clock.Config,
    rated: Boolean,
    status: chess.Status,
    source: String,
    winner: Option[chess.Color],
    gameStart: String,
    userId: User.ID
  ) = Round(
    _id = gameId,
    white = white,
    black = black,
    clock = clock,
    rated = rated,
    status = status,
    source = source,
    winner = winner,
    gameStart = gameStart,
    redirected = false,
    createdBy = userId,
    createdAt = DateTime.now()
  )

}

case class RoundPlayer(
    local: MiniUser, // 可能使虚拟的
    lichess: MiniUser,
    isLichess: Boolean,
    color: chess.Color,
    offeringRematch: Boolean,
    offeringDraw: Boolean,
    proposingTakeback: Boolean
) {

  def isLocal = !isLichess

  def toVirtualUser = User.virtual(lichess.id, lichess.username, lichess.rating, lichess.provisionalOrDefault)

  def toGamePlayer(winnerColor: Option[chess.Color]) =
    lila.game.Player(
      id = IdGenerator.player(color),
      color = color,
      aiLevel = None,
      isLichess = isLichess.some,
      isWinner = winnerColor.map(_ == color),
      lastDrawOffer = None,
      userId = local.id.some,
      rating = local.rating.some,
      ratingDiff = local.ratingDiff,
      provisional = local.provisionalOrDefault,
      blurs = lila.game.Blurs.blursZero.zero,
      holdAlert = None,
      name = local.username.some
    )

}

object RoundPlayer {

  def make(
    local: MiniUser,
    lichess: MiniUser,
    isLichess: Boolean,
    color: chess.Color
  ) = RoundPlayer(
    local = local,
    lichess = lichess,
    isLichess = isLichess,
    color = color,
    offeringRematch = false,
    offeringDraw = false,
    proposingTakeback = false
  )

}

case class MiniUser(id: String, username: String, rating: Int, title: Option[String], provisional: Option[Boolean], ratingDiff: Option[Int]) {

  def provisionalOrDefault = provisional | false

}
