package lila.lichessApi

import lila.user.User
import org.joda.time.DateTime

final class OAuth2Api(lilaBus: lila.common.Bus) {

  val ClientId = "haichess.com"
  val isProd = lila.common.PlayApp.isProd
  val HaichessHost = if (isProd) "https://haichess.com" else "http://localhost"
  val LichessApiEndpoint = "https://lichess.org/api"

  def byId(state: String): Fu[Option[OAuth2]] = OAuth2Repo.byId(state)

  def findActive(userId: User.ID): Fu[Option[OAuth2]] = OAuth2Repo.findActive(userId)

  def create(data: DataForm.OAuth2Create, userId: User.ID): Funit =
    OAuth2Repo.setAllUnActive(userId) >>
      OAuth2Repo.insert(OAuth2.make(data.state, data.verifierCode, data.realScopes, userId)).void

  def setAuthInfo(o: OAuth2, data: DataForm.OAuth2Update): Funit = {
    val newOAuth2 = o.copy(
      accessToken = data.accessToken.some,
      expireAt = DateTime.now.plusSeconds(data.expiresIn).some,
      lichessUser = LichessUser(
        id = data.userId,
        username = data.username
      ).some,
      active = true
    )

    OAuth2Repo.update(newOAuth2)
  }

  def quit(o: OAuth2): Funit = OAuth2Repo.setActive(o.id, false)

  def keepLongTerm(state: String, longTerm: Boolean): Funit =
    OAuth2Repo.keepLongTerm(state, longTerm)

}
