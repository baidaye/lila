package lila.pool

import play.api.libs.json.{ JsArray, Json }
import scala.concurrent.duration._

object PoolList {

  import PoolConfig._

  val all: List[PoolConfig] = List(
    PoolConfig(3 ++ 2, Wave(20 seconds, 20 players), false),
    PoolConfig(5 ++ 0, Wave(20 seconds, 30 players), false),
    PoolConfig(5 ++ 3, Wave(20 seconds, 30 players), false),
    PoolConfig(10 ++ 0, Wave(20 seconds, 30 players), true),
    PoolConfig(10 ++ 5, Wave(20 seconds, 30 players), true),
    PoolConfig(15 ++ 10, Wave(20 seconds, 20 players), true),
    PoolConfig(30 ++ 0, Wave(60 seconds, 20 players), true),
    PoolConfig(30 ++ 20, Wave(60 seconds, 20 players), true)
  )

  val clockStringSet: Set[String] = all.map(_.clock.show)(scala.collection.breakOut)

  val toJson = JsArray(
    all.map { poolConfig =>
      Json.obj(
        "id" -> poolConfig.clock.show,
        "lim" -> poolConfig.clock.limitInMinutes,
        "inc" -> poolConfig.clock.incrementSeconds,
        "perf" -> poolConfig.perfType.name,
        "key" -> poolConfig.perfType.key,
        "lichessApi" -> poolConfig.lichessApi
      )
    }
  )

  private implicit class PimpedInt(self: Int) {
    def ++(increment: Int) = chess.Clock.Config(self * 60, increment)
    def players = NbPlayers(self)
  }
}
