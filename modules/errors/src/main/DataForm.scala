package lila.errors

import play.api.data._
import play.api.data.Forms._
import lila.common.Form._
import lila.puzzle.PuzzleResult

object DataForm {

  val puzzle = Form(mapping(
    "ratingMin" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
    "ratingMax" -> optional(number(min = lila.hub.PuzzleHub.minRating, max = lila.hub.PuzzleHub.maxRating)),
    "sTime" -> optional(ISODate.isoDate),
    "eTime" -> optional(ISODate.isoDate),
    "time" -> optional(stringIn(timeChoices)),
    "source" -> optional(stringIn(sourceChoices)),
    "color" -> optional(stringIn(colorChoices)),
    "sort" -> optional(stringIn(sortChoices))
  )(PuzzleQuery.apply)(PuzzleQuery.unapply))

  val game = Form(mapping(
    "gameAtMin" -> optional(ISODate.isoDate),
    "gameAtMax" -> optional(ISODate.isoDate),
    "color" -> optional(stringIn(colorChoices)),
    "opponent" -> optional(lila.user.DataForm.historicalUsernameField),
    "phase" -> optional(stringIn(phaseChoices)),
    "judgement" -> optional(stringIn(judgementChoices)),
    "eco" -> optional(text)
  )(GameQuery.apply)(GameQuery.unapply))

  def colorChoices = List("" -> "全部", "white" -> "白棋", "black" -> "黑棋")
  def sourceChoices = ("" -> "全部") +: PuzzleResult.Source.choices.filter(_._1 != "error")
  def timeChoices = List((6 * 30 * -1).toString -> "全部", "0" -> "今天", "-3" -> "近3天", "-7" -> "近1周")
  def phaseChoices: List[(String, String)] = GameErrors.Phase.all.map(p => p.id.toString -> p.name)
  def judgementChoices: List[(String, String)] = GameErrors.Judgement.all.map(p => p.id -> p.name)
  def sortChoices = List("ratingAsc" -> "难度正序", "ratingDesc" -> "难度倒序", "timeAsc" -> "时间正序", "timeDesc" -> "时间倒序")

}

