package lila.errors

import org.joda.time.DateTime

case class PuzzleQuery(
    ratingMin: Option[Int] = None,
    ratingMax: Option[Int] = None,
    sTime: Option[DateTime] = None,
    eTime: Option[DateTime] = None,
    time: Option[String] = None,
    source: Option[String] = None,
    color: Option[String] = None,
    sort: Option[String] = None
)
