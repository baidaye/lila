package lila.errors

import chess.Color
import lila.user.User
import lila.puzzle.Puzzle
import lila.puzzle.PuzzleId
import org.joda.time.DateTime
import lila.puzzle.PuzzleResult

case class PuzzleErrors(
    _id: String,
    puzzleId: PuzzleId,
    rating: Int,
    depth: Int,
    fen: String,
    color: Color,
    lastMove: Option[String],
    source: PuzzleResult.Source,
    metaData: Option[PuzzleResult.MetaData],
    createAt: DateTime,
    createBy: User.ID
) {

  def id = _id

}

object PuzzleErrors {

  def make(
    puzzle: Puzzle,
    userId: User.ID,
    source: PuzzleResult.Source,
    metaData: Option[PuzzleResult.MetaData]
  ) = PuzzleErrors(
    _id = makeId(userId, puzzle.id),
    puzzleId = puzzle.id,
    rating = puzzle.rating.toInt,
    depth = puzzle.depth,
    fen = puzzle.fenAfterInitialMove | puzzle.fen,
    color = puzzle.color,
    lastMove = puzzle.history.lastOption,
    source = source,
    metaData = metaData,
    createAt = DateTime.now,
    createBy = userId
  )

  def makeId(userId: User.ID, puzzleId: PuzzleId) = s"$userId@$puzzleId"

}