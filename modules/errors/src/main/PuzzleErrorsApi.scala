package lila.errors

import lila.db.dsl._
import lila.user.User
import reactivemongo.bson._
import lila.common.MaxPerPage
import lila.common.paginator.Paginator
import lila.db.paginator.Adapter
import lila.hub.actorApi.puzzle.NextErrorPuzzle
import lila.puzzle.{ PuzzleApi, PuzzleId }
import org.joda.time.DateTime

final class PuzzleErrorsApi(coll: Coll, puzzleApi: PuzzleApi, bus: lila.common.Bus) {

  import BSONHandlers._

  def receive(res: lila.puzzle.PuzzleResult): Funit =
    puzzleApi.puzzle.find(res.puzzleId) flatMap {
      case None => funit
      case Some(p) => {
        val error = PuzzleErrors.make(p, res.userId, res.source, res.metaData)
        coll.update(
          $id(error.id),
          error,
          upsert = true
        ).void
      }
    }

  def nextPuzzleErrors(puzzleId: PuzzleId, userId: User.ID, query: PuzzleQuery): Fu[Option[PuzzleErrors]] = {
    coll.byId[PuzzleErrors](PuzzleErrors.makeId(userId, puzzleId)).flatMap {
      case None => fuccess(none)
      case Some(err) => {
        val doc = (query.sort | "ratingAsc") match {
          case "ratingAsc" => $or($doc("rating" -> err.rating, "puzzleId" $gt puzzleId), $doc("rating" $gt err.rating))
          case "ratingDesc" => $or($doc("rating" -> err.rating, "puzzleId" $gt puzzleId), $doc("rating" $lt err.rating))
          case "timeAsc" => $or($doc("createAt" -> err.createAt, "puzzleId" $gt puzzleId), $doc("createAt" $gt err.createAt))
          case "timeDesc" => $or($doc("createAt" -> err.createAt, "puzzleId" $gt puzzleId), $doc("createAt" $lt err.createAt))
        }
        val condition = createQuery(userId, query) ++ doc
        //println(BSONDocument.pretty(condition))
        coll.find(condition).sort(createSort(query)).uno[PuzzleErrors]
      }
    } map {
      case None => None
      case Some(e) => {
        publishNextPuzzle(e.puzzleId, userId)
        e.some
      }
    }
  }

  def publishNextPuzzle(puzzleId: Int, userId: User.ID) =
    bus.publish(NextErrorPuzzle(puzzleId, userId), 'nextErrorPuzzle)

  def removeByIds(ids: List[String]): Funit =
    coll.remove($inIds(ids)).void

  def removeById(id: Int, userId: User.ID): Funit =
    coll.remove($id(PuzzleErrors.makeId(userId, id))).void

  def page(page: Int, userId: User.ID, query: PuzzleQuery): Fu[Paginator[PuzzleErrors]] = {
    val condition = createQuery(userId, query)
    //println(BSONDocument.pretty(condition))

    Paginator(
      adapter = new Adapter(
        collection = coll,
        selector = condition,
        projection = $empty,
        sort = createSort(query)
      ),
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  private def createQuery(userId: User.ID, query: PuzzleQuery) = {
    var condition = $doc("createBy" -> userId)
    if (query.ratingMin.isDefined || query.ratingMax.isDefined) {
      var ratingRange = $doc()
      query.ratingMin foreach { ratingMin =>
        ratingRange = ratingRange ++ $gte(ratingMin)
      }
      query.ratingMax foreach { ratingMax =>
        ratingRange = ratingRange ++ $lte(ratingMax)
      }
      condition = condition ++ $doc("rating" -> ratingRange)
    }

    if (query.sTime.isDefined || query.eTime.isDefined) {
      var timeRange = $doc()
      query.sTime foreach { sTime =>
        timeRange = timeRange ++ $gte(sTime.withTimeAtStartOfDay())
      }
      query.eTime foreach { eTime =>
        timeRange = timeRange ++ $lte(eTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999))
      }
      condition = condition ++ $doc("createAt" -> timeRange)
    }

    query.source foreach { source =>
      condition = condition ++ $doc("source" -> source)
    }

    query.color foreach { tg =>
      condition = condition ++ $doc("color" -> (tg == "white"))
    }
    condition
  }

  private def timeQuery(time: String) = {
    val now = DateTime.now
    time match {
      case "today" => $doc("createAt" $gte now.withTimeAtStartOfDay())
      case "threeDay" => $doc("createAt" $gte now.minusDays(3).withTimeAtStartOfDay())
      case "oneWeek" => $doc("createAt" $gte now.minusWeeks(1).withTimeAtStartOfDay())
      case _ => $empty
    }
  }

  private def createSort(query: PuzzleQuery) = {
    (query.sort | "ratingAsc") match {
      case "ratingAsc" => $doc("rating" -> 1, "_id" -> 1)
      case "ratingDesc" => $doc("rating" -> -1, "_id" -> 1)
      case "timeAsc" => $doc("createAt" -> 1, "_id" -> 1)
      case "timeDesc" => $doc("createAt" -> -1, "_id" -> 1)
    }
  }

}
