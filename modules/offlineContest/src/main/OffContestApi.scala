package lila.offlineContest

import akka.pattern.ask
import akka.actor.{ ActorSelection, ActorSystem }
import lila.clazz.ClazzApi
import lila.db.{ DbImage, Photographer }
import lila.hub.actorApi.relation.GetMarks
import lila.notify.Notification.Notifies
import lila.notify.{ Notification, NotifyApi }
import lila.user.{ User, UserRepo }
import lila.team.{ Campus, CampusRepo, MemberRepo, TagRepo, TeamRepo }
import makeTimeout.large

class OffContestApi(
    system: ActorSystem,
    notifyApi: NotifyApi,
    clazzApi: ClazzApi,
    markActor: ActorSelection,
    photographer: Photographer
) {

  def byId(id: OffContest.ID): Fu[Option[OffContest]] = OffContestRepo.byId(id)

  def create(contest: OffContest, rounds: List[OffRound]): Fu[OffContest] = {
    OffContestRepo.insert(contest) >> OffRoundRepo.bulkInsert(rounds) inject contest
  }

  def update(old: OffContest, c: OffContest, rounds: List[OffRound]): Funit =
    OffContestRepo.update(
      old.copy(
        name = c.name,
        groupName = c.groupName,
        logo = c.logo,
        typ = c.typ,
        teamRated = c.teamRated,
        organizer = c.organizer,
        rule = c.rule,
        rounds = c.rounds,
        swissBtss = c.swissBtss,
        roundRobinBtss = c.roundRobinBtss
      )
    ) >> OffRoundRepo.bulkUpdate(old.id, rounds).void

  def remove(contest: OffContest): Funit = {
    lg(contest, none, "删除比赛", none)
    OffContestRepo.remove(contest.id) >>
      OffRoundRepo.removeByContest(contest.id) >>
      OffPlayerRepo.removeByContest(contest.id)
  }

  def start(contest: OffContest): Funit = {
    lg(contest, none, "开始比赛", none)
    OffContestRepo.setStatus(contest.id, OffContest.Status.Started)
  }

  def cancel(contest: OffContest): Funit = {
    lg(contest, none, "取消比赛", none)
    OffContestRepo.setStatus(contest.id, OffContest.Status.Canceled)
  }

  def finish(contest: OffContest): Funit = {
    lg(contest, none, "结束结束", none)
    OffContestRepo.setStatus(contest.id, OffContest.Status.Finished)
  }

  def teamClazzs(c: OffContest): Fu[List[(String, String)]] = {
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => TeamRepo.byId(c.organizer) flatMap { team =>
        team.?? {
          _.clazzIds.?? { clazzIds =>
            clazzApi.byIds(clazzIds).map { clazzs =>
              clazzs.filterNot(_.deleted | false) map (c => c.id -> c.name)
            }
          }
        }
      }
      case OffContest.Type.ClazzInner => fuccess(List.empty[(String, String)])
    }
  }

  def teamTags(c: OffContest): Fu[List[lila.team.Tag]] = {
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => TagRepo.findByTeam(c.organizer)
      case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            TagRepo.findByTeam(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def team(c: OffContest): Fu[Option[lila.team.Team]] = {
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => TeamRepo.byId(c.organizer)
      case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            TeamRepo.byId(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def teamCampus(c: OffContest): Fu[List[lila.team.Campus]] = {
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => CampusRepo.byTeam(c.organizer)
      case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.?? { campusId =>
            CampusRepo.byTeam(Campus.toTeamId(campusId))
          }
        }
      }
    }
  }

  def allPlayersWithUsers(me: User, c: OffContest): Fu[List[OffPlayer.AllPlayerWithUser]] =
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => for {
        members <- MemberRepo.memberByTeam(c.organizer)
        users <- UserRepo usersFromSecondary members.map(_.user)
        markMap ← userMarks(me.id)
      } yield users zip members map {
        case (user, member) => {
          val mark = markOption(user.id, markMap)
          OffPlayer.AllPlayerWithUser(user, member.some, mark)
        }
      }
      case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          for {
            userIds <- fuccess(clazz.studentIds)
            users <- UserRepo usersFromSecondary userIds
            markMap ← userMarks(me.id)
            members <- clazz.team.fold(fuccess(userIds.map(_ => none[lila.team.Member]))) { campusId =>
              MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), userIds)
            }
          } yield users zip members map {
            case (user, member) => {
              val mark = markOption(user.id, markMap)
              OffPlayer.AllPlayerWithUser(user, member, mark)
            }
          }
        }
      }
    }

  private def markOption(userId: User.ID, markMap: Map[String, Option[String]]): Option[String] = {
    markMap.get(userId).fold(none[String]) { m => m }
  }

  def playersWithUsers(me: User, c: OffContest): Fu[List[OffPlayer.PlayerWithUser]] = for {
    players ← OffPlayerRepo.getByContest(c.id)
    users ← userWithExternal(players)
    markMap ← userMarks(me.id)
    members ← c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(c.organizer, players.map(_.userId))
      case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
        _.?? { clazz =>
          clazz.team.fold(fuccess(players.map(_ => none[lila.team.Member]))) { campusId =>
            MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), players.map(_.userId))
          }
        }
      }
    }
  } yield players zip users zip members map {
    case ((player, user), member) => {
      val mark = markOption(user.id, markMap)
      OffPlayer.PlayerWithUser(player, user, member, mark)
    }
  }

  def userWithExternal(players: List[OffPlayer]): Fu[List[lila.user.User]] = {
    UserRepo.optionsByOrderedIds(players.map(_.userId)) map { users =>
      players zip users map {
        case (player, user) => if (player.external) player.virtualUser else user err s"can not find user ${player.userId}"
      }
    }
  }

  def setPlayers(c: OffContest, userIds: List[String]): Funit = {
    {
      lg(c, none, "添加棋手", s"棋手：$userIds".some)
      c.typ match {
        case OffContest.Type.Public | OffContest.Type.TeamInner => MemberRepo.memberOptionFromSecondary(c.organizer, userIds)
        case OffContest.Type.ClazzInner => clazzApi.byId(c.organizer) flatMap {
          _.?? { clazz =>
            clazz.team.fold(fuccess(userIds.map(_ => none[lila.team.Member]))) { campusId =>
              MemberRepo.memberOptionFromSecondary(Campus.toTeamId(campusId), userIds)
            }
          }
        }
      }
    } flatMap { members =>
      OffRoundRepo.byId(OffRound.makeId(c.id, c.currentRound)) flatMap { roundOption =>
        roundOption.?? { round =>
          OffPlayerRepo.findNextNo(c.id) flatMap { no =>
            val players = members.zip(userIds).zipWithIndex map {
              case ((memberOption, userId), index) => {
                OffPlayer.make(c.id, if (c.isCreated) index + 1 else no + index, userId, memberOption.??(_.rating.map(_.intValue)), OffPlayer.isExternal(userId), c.currentRound, round.isOverPairing)
              }
            }
            if (c.isCreated) {
              OffPlayerRepo.bulkUpdate(c.id, players) >> OffContestRepo.setPlayers(c.id, userIds.size)
            } else {
              OffPlayerRepo.bulkInsert(players) >> OffContestRepo.incPlayers(c.id, players.size)
            }
          }
        }
      }
    }
  }

  def externalPlayer(c: OffContest, srcUsername: String, teamRating: Option[Int]): Funit = {
    OffRoundRepo.byId(OffRound.makeId(c.id, c.currentRound)) flatMap { roundOption =>
      roundOption.?? { round =>
        OffPlayerRepo.findNextNo(c.id) flatMap { no =>
          val player = OffPlayer.make(c.id, no, OffPlayer.withExternal(srcUsername), teamRating, true, c.currentRound, round.isOverPairing)
          OffPlayerRepo.insert(player) >> OffContestRepo.incPlayers(c.id, 1)
        }
      }
    }
  }

  def kickPlayer(c: OffContest, playerId: OffPlayer.ID): Funit = {
    lg(c, none, "棋手禁止比赛", s"棋手：$playerId".some)
    OffPlayerRepo.byId(playerId) flatMap {
      _.?? { player =>
        val outcome = player.roundOutcome(c.currentRound)
        OffPlayerRepo.kick(playerId, outcome.isEmpty)
      }
    }
  }

  def removePlayer(c: OffContest, playerId: OffPlayer.ID): Funit = {
    lg(c, none, "删除棋手", s"棋手：$playerId".some)
    for {
      _ <- OffPlayerRepo.remove(playerId)
      _ <- reorderPlayer2(c.id)
      res <- OffContestRepo.incPlayers(c.id, -1)
    } yield res
  }

  private def reorderPlayer2(id: OffContest.ID): Funit =
    OffPlayerRepo.getByContest(id) flatMap { players =>
      players.zipWithIndex.map {
        case (p, i) => OffPlayerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  def reorderPlayer(playerIds: List[String]): Funit =
    OffPlayerRepo.byOrderedIds(playerIds) flatMap { players =>
      players.zipWithIndex.map {
        case (p, i) => OffPlayerRepo.setNo(p.id, i + 1)
      }.sequenceFu.void
    }

  def quitByUser(userId: User.ID): Funit =
    OffPlayerRepo.getByUserId(userId) flatMap { players =>
      OffContestRepo.byIds(players.map(_.contestId)) flatMap { contests =>
        contests.filter(_.playerKickable).map { contest =>
          players.find(_.id == OffPlayer.makeId(contest.id, userId)).?? { player =>
            removePlayer(contest, player.id)
          }
        }.sequenceFu.void >> {
          contests.filter(_.playerRemoveable).map { contest =>
            players.find(_.id == OffPlayer.makeId(contest.id, userId)).?? { player =>
              removePlayer(contest, player.id)
            }
          }.sequenceFu.void
        }
      }
    }

  def uploadPicture(id: String, picture: Photographer.Uploaded, processFile: Boolean = false): Fu[DbImage] =
    photographer(id, picture, processFile)

  def userMarks(userId: User.ID): Fu[Map[String, Option[String]]] =
    (markActor ? GetMarks(userId)).mapTo[Map[String, Option[String]]]

  private def lg(contest: OffContest, round: Option[OffRound], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
