package lila.offlineContest

import ornicar.scalalib.Random

final class OffPairingDirector(pairingSystem: OffPairingSystem) {

  import OffPairingSystem._

  private[offlineContest] def roundPairingTest(contest: OffContest): Fu[Boolean] =
    pairingSystem(contest)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(false)
        else fuccess(true)
      }
      .recover {
        case OffSwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"BBPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          false
      }

  private[offlineContest] def roundPairing(contest: OffContest, round: OffRound, players: List[OffPlayer]): Fu[Option[List[OffBoard]]] =
    pairingSystem(contest)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          val boards = buildBoards(contest, round, pendings, players)
          for {
            _ <- OffBoardRepo.bulkInsert(round.id, boards)
            _ <- setPlayerBye(contest, round.no, pendings, players)
            _ <- OffRoundRepo.setBoards(round.id, boards.size)
            _ <- OffRoundRepo.setStatus(round.id, OffRound.Status.Pairing)
          } yield Some(boards)
        }
      }
      .recover {
        case OffSwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"BBPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[OffBoard])
      }

  private[offlineContest] def pairingAll(contest: OffContest, rounds: List[OffRound], players: List[OffPlayer]): Fu[Option[List[OffBoard]]] = {
    logger.info(s"循环赛匹配开始：$contest")
    pairingSystem.pairingAll(contest, players)
      .flatMap { pendings =>
        if (pendings.isEmpty) fuccess(none)
        else {
          val boards = allRoundBoards(contest, rounds, players, pendings)
          for {
            _ <- addAllBoards(rounds, boards)
            _ <- setAllPlayerByeRound(contest, rounds, players, pendings)
            _ <- OffRoundRepo.setBoardsAndStatus(rounds.map(_.id), players.size / 2, OffRound.Status.Pairing)
            firstRound = rounds.minBy(_.no)
          } yield Some(boards.filter(_.roundNo == firstRound.no))
        }
      }
      .recover {
        case OffSwissPairing.PairingException(msg, trfContent) =>
          logger.warn(s"SwissPairing failed, contest: $contest, msg: " + msg + ", \n" + trfContent)
          Some(List.empty[OffBoard])
        case OffRoundRobinPairing.PairingException(msg) =>
          logger.warn(s"RoundRobinPairing failed, contest: $contest, msg: " + msg)
          Some(List.empty[OffBoard])
      }
  }

  private def buildBoards(contest: OffContest, round: OffRound, byeOrPendings: List[ByeOrPending], players: List[OffPlayer]): List[OffBoard] = {
    byeOrPendings.zipWithIndex.collect {
      case (Right(Pending(w, b)), i) => {
        val white = players.find(_.no == w) err s"cannot find player $w"
        val black = players.find(_.no == b) err s"cannot find player $b"
        OffBoard(
          id = Random nextString 8,
          no = i + 1,
          contestId = contest.id,
          roundId = round.id,
          roundNo = round.no,
          status = OffBoard.Status.Created,
          whitePlayer = OffBoard.MiniPlayer(white.id, white.userId, white.no, None),
          blackPlayer = OffBoard.MiniPlayer(black.id, black.userId, black.no, None)
        )
      }
    }
  }

  private def setPlayerBye(contest: OffContest, roundNo: OffRound.No, byeOrPendings: List[ByeOrPending], players: List[OffPlayer]): Funit = {
    val newByeNos = byeOrPendings.collect { case Left(bye) => bye.player }
    val newByePlayers = players.filter(p => newByeNos.contains(p.no))
    val oldByePlayers = players.filter(_.roundOutcome(roundNo).contains(OffBoard.Outcome.Bye))
    val oldByeNos = oldByePlayers.map(_.no)

    oldByePlayers.filter(p => !newByeNos.contains(p.no)).map { player =>
      OffPlayerRepo.update(
        player.copy(
          outcomes = player.removeOutcomeByRound(roundNo)
        ) |> { p =>
            p.copy(
              score = p.allScore(contest.isRoundRobin),
              points = p.allScore(contest.isRoundRobin)
            )
          }
      )
    }.sequenceFu.void >> {
      newByePlayers.filter(p => !oldByeNos.contains(p.no)).map { player =>
        OffPlayerRepo.update(
          player.copy(
            outcomes = player.setOutcomeByRound(roundNo, OffBoard.Outcome.Bye)
          ) |> { p =>
              p.copy(
                score = p.allScore(contest.isRoundRobin),
                points = p.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    }
  }

  private def addAllBoards(rounds: List[OffRound], boards: List[OffBoard]): Funit = {
    OffBoardRepo.insertManyByRounds(rounds.map(_.id), boards)
  }

  private def allRoundBoards(contest: OffContest, rounds: List[OffRound], players: List[OffPlayer], byeOrPendingMap: Map[Int, List[ByeOrPending]]): List[OffBoard] = {
    rounds.flatMap { round =>
      byeOrPendingMap.get(round.no) match {
        case Some(byeOrPendings) => {
          byeOrPendings.collect { case Right(pending) => pending }.zipWithIndex.map {
            case (pending, i) => {
              val white = players.find(_.no == pending.white) err s"cannot find player ${pending.white}"
              val black = players.find(_.no == pending.black) err s"cannot find player ${pending.black}"
              OffBoard(
                id = Random nextString 8,
                no = i + 1,
                contestId = contest.id,
                roundId = round.id,
                roundNo = round.no,
                status = OffBoard.Status.Created,
                whitePlayer = OffBoard.MiniPlayer(white.id, white.userId, white.no, None),
                blackPlayer = OffBoard.MiniPlayer(black.id, black.userId, black.no, None)
              )
            }
          }
        }
        case None => throw OffRoundRobinPairing.PairingException(s"allRoundBoards RoundRobinPairing PairError $contest 第${round.no}轮")
      }
    }
  }

  private def setAllPlayerByeRound(contest: OffContest, rounds: List[OffRound], players: List[OffPlayer], byeOrPendingMap: Map[Int, List[ByeOrPending]]): Funit = {
    byeOrPendingMap.foldLeft(Map.empty[OffPlayer.No, List[OffRound.No]]) {
      case (playerByeRound, (roundNo, byeOrPendings)) => {
        val byeNos = byeOrPendings.collect { case Left(bye) => bye.player }
        var newMap = playerByeRound
        byeNos.foreach { playerNo =>
          newMap = newMap.get(playerNo) match {
            case None => newMap + (playerNo -> List(roundNo))
            case Some(roundNos) => newMap + (playerNo -> (roundNos :+ roundNo))
          }
        }
        newMap
      }
    }.map {
      case (playerNo, byeRound) => {
        val player = players.find(_.no == playerNo) err s"cannot find player $playerNo"
        OffPlayerRepo.setByeRound(player.id, byeRound)
      }
    }.sequenceFu.void
  }

}

