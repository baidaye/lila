package lila.offlineContest

import lila.clazz.ClazzApi
import lila.notify.NotifyApi
import lila.team.Campus
import lila.user.User

class OffRoundApi(pairingDirector: OffPairingDirector, notifyApi: NotifyApi, clazzApi: ClazzApi, bus: lila.common.Bus) {

  def purePlayers(contest: OffContest, round: OffRound): Fu[List[OffPlayer]] = {
    val no = round.no
    OffPlayerRepo.getByContest(contest.id).flatMap { players =>
      //  之前匹配过，所以生成了对应轮次的Outcome，再次匹配时这些Outcome先清除（因为缺席状态可能已经改变）
      val historyOutcomePlayers = players.filter(p => p.isAbsentIgnoreManual(round.no) || p.isHalf(round.no))

      //  上轮 离开、退赛、踢出，本轮继续
      val absents = players.filter(p => p.absent)
      val kicks = absents.filter(_.kick).map(_.no)

      historyOutcomePlayers.nonEmpty.?? { OffPlayerRepo.removePlayersLastOutcome(contest, historyOutcomePlayers, no) } >>
        kicks.nonEmpty.?? { OffPlayerRepo.setOutcomes(contest.id, no, kicks, OffBoard.Outcome.Kick) } >>
        OffPlayerRepo.getByContest(contest.id)
    }
  }

  def pairing(contest: OffContest, round: OffRound, finish: OffContest => Funit): Fu[Boolean] = {
    lg(contest, round.some, "比赛编排", none)
    purePlayers(contest, round) flatMap { players =>
      pairingDirector.roundPairing(contest, round, players) flatMap {
        case None => {
          lg(contest, round.some, "比赛编排失败", "Pairing impossible under the current rules -> force finished!".some, true)
          pairingFailed(contest, round, finish).inject(false)
        }
        case Some(boards) => {
          if (boards.isEmpty) {
            lg(contest, round.some, "比赛编排失败", "boards.isEmpty -> force finished!".some, true)
            pairingFailed(contest, round, finish).inject(false)
          } else {
            val bs = boards.map(_.playerNos)
            lg(contest, round.some, "比赛编排完成", s"$bs".some)
            fuTrue
          }
        }
      }
    }
  }

  def pairingRoundRobin(contest: OffContest, finish: OffContest => Funit): Fu[Boolean] = {
    lg(contest, none, "循环赛编排", none)
    (for {
      rounds <- OffRoundRepo.getByContest(contest.id)
      players <- OffPlayerRepo.getByContest(contest.id)
      firstRound = rounds.minBy(_.no)
    } yield (rounds, players, firstRound)) flatMap {
      case (rounds, players, firstRound) => {
        pairingDirector.pairingAll(contest, rounds.take(contest.actualRound), players).flatMap {
          case None => {
            lg(contest, none, "循环赛编排失败", none, true)
            pairingFailed(contest, firstRound, finish).inject(false)
          }
          case Some(boards) => {
            if (boards.isEmpty) {
              lg(contest, none, "循环赛编排失败", "boards.isEmpty -> force finished!".some, true)
              pairingFailed(contest, firstRound, finish).inject(false)
            } else {
              val bs = boards.map(_.playerNos)
              lg(contest, none, "循环赛编排完成", s"$bs".some)
              OffContestRepo.setRoundRobinPairing(contest.id).inject(true)
            }
          }
        }
      }
    }
  }

  def setRoundRobin(contest: OffContest, rounds: Int): Funit = {
    lg(contest, none, "循环赛设置", s"Rounds：${rounds.toString}".some)
    val roundList = (1 to rounds) map { no =>
      OffRound.make(
        no = no,
        contestId = contest.id
      )
    } toList

    OffContestRepo.setRounds(contest.id, rounds) >>
      OffRoundRepo.bulkUpdate(contest.id, roundList)
  }

  def roundSwap(contest: OffContest, round: OffRound, roundId: OffRound.ID): Funit = {
    OffRoundRepo.byId(roundId) flatMap {
      _.?? { targetRound =>
        lg(contest, round.some, "循环赛交换轮次", s"与第 ${targetRound.no} 轮交换".some)
        for {
          sourcePlayers <- OffPlayerRepo.findByByeRound(contest.id, round.no)
          targetPlayers <- OffPlayerRepo.findByByeRound(contest.id, targetRound.no)
          _ <- setPlayersByeRound(sourcePlayers, round, targetRound)
          _ <- setPlayersByeRound(targetPlayers, targetRound, round)
          sourceBoards <- OffBoardRepo.getByRound(round.id)
          targetBoards <- OffBoardRepo.getByRound(targetRound.id)
          _ <- OffBoardRepo.roundSwap(sourceBoards.map(_.id), targetRound)
          res <- OffBoardRepo.roundSwap(targetBoards.map(_.id), round)
        } yield res
      }
    }
  }

  private def setPlayersByeRound(players: List[OffPlayer], round: OffRound, targetRound: OffRound): Funit = {
    players.map { player =>
      val newByeRound = player.byeRound.map { no =>
        if (no == round.no) targetRound.no else no
      }
      OffPlayerRepo.setByeRound(player.id, newByeRound)
    }.sequenceFu.void
  }

  private def pairingFailed(contest: OffContest, round: OffRound, finish: OffContest => Funit): Funit = {
    val cr = Math.max(round.no - 1, 1)
    OffContestRepo.setCurrentRound(contest.id, cr) >>
      finish(contest.copy(currentRound = cr))
  }

  def publish(contest: OffContest, round: OffRound): Funit = {
    lg(contest, round.some, "比赛编排发布", none)
    for {
      players <- OffPlayerRepo.getByContest(contest.id)
      _ <- OffRoundRepo.setStatus(round.id, OffRound.Status.Published)
      _ <- setPlayerByeByByeRound(contest, round.no, players)
      _ <- OffPlayerRepo.unAbsentByContest(contest.id)
    } yield ()
  }

  // 根据 ByeRound 属性更新Bey Outcome（循环赛）
  def setPlayerByeByByeRound(contest: OffContest, roundNo: OffRound.No, players: List[OffPlayer]): Funit =
    contest.isRoundRobin.?? {
      val newByePlayers = players.filter(_.byeRound.contains(roundNo))
      val newByeNos = newByePlayers.map(_.no)
      val oldByePlayers = players.filter(_.roundOutcome(roundNo).contains(OffBoard.Outcome.Bye))
      val oldByeNos = oldByePlayers.map(_.no)

      oldByePlayers.filter(p => !newByeNos.contains(p.no)).map { player =>
        OffPlayerRepo.update(
          player.copy(
            outcomes = player.removeOutcomeByRound(roundNo)
          ) |> { p =>
              p.copy(
                score = p.allScore(contest.isRoundRobin),
                points = p.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void >> {
        newByePlayers.filter(p => !oldByeNos.contains(p.no)).map { player =>
          OffPlayerRepo.update(
            player.copy(
              outcomes = player.setOutcomeByRound(roundNo, OffBoard.Outcome.Bye)
            ) |> { p =>
                p.copy(
                  score = p.allScore(contest.isRoundRobin),
                  points = p.allScore(contest.isRoundRobin)
                )
              }
          )
        }.sequenceFu.void
      }
    }

  import lila.hub.actorApi.offContest._
  def publishResult(contest: OffContest, id: OffRound.ID, no: OffRound.No, me: User, finish: OffContest => Funit, playersWithUsers: (User, OffContest) => Fu[List[OffPlayer.PlayerWithUser]]): Funit = {
    lg(contest, OffRound.make(no, contest.id).some, "比赛成绩发布", none)
    val nextRound = no + 1
    for {
      _ <- computeScore(contest, no)
      _ <- OffRoundRepo.setStatus(id, OffRound.Status.PublishResult)
      players <- playersWithUsers(me, contest)
      boards <- OffBoardRepo.getByRound(id)
      team <- belongTeam(contest)
      res <- {
        if (nextRound > contest.rounds) {
          finish(contest.copy(currentRound = nextRound))
        } else {
          OffContestRepo.setCurrentRound(contest.id, nextRound)
        }
      }
    } yield {

      val result = OffContestRoundResult(
        contest.id,
        contest.fullName,
        team,
        contest.teamRated,
        no,
        boards.map { board =>
          val white = findPlayer(board.whitePlayer.no, players)
          val black = findPlayer(board.blackPlayer.no, players)
          OffContestBoard(
            board.id,
            OffContestUser(white.userId, white.realName, white.player.external, white.player.teamRating, board.whitePlayer.isWinner),
            OffContestUser(black.userId, black.realName, black.player.external, black.player.teamRating, board.blackPlayer.isWinner),
            board.isAbsent
          )
        }
      )
      bus.publish(result, 'offContestRoundResult)
    }
  }

  private def findPlayer(no: OffPlayer.No, players: List[OffPlayer.PlayerWithUser]): OffPlayer.PlayerWithUser =
    players.find(_.player.no == no) err s"can not find player：$no"

  def belongTeam(c: OffContest): Fu[Option[String]] = {
    c.typ match {
      case OffContest.Type.Public | OffContest.Type.TeamInner => fuccess(c.organizer.some)
      case OffContest.Type.ClazzInner => {
        clazzApi.byId(c.organizer).map {
          _.??(c => c.team.map(Campus.toTeamId))
        }
      }
    }
  }

  private def computeScore(contest: OffContest, no: OffRound.No): Funit = {
    lg(contest, none, "计算成绩册", s"第 $no 轮".some)
    for {
      players <- OffPlayerRepo.getByContest(contest.id)
      boards <- OffBoardRepo.getByContestLteRound(contest.id, no)
    } yield {
      val playerDeadlineRound = players.map { player =>
        player.copy(
          score = player.roundScoreWithCurr(no, contest.isRoundRobin),
          points = player.roundScoreWithCurr(no, contest.isRoundRobin),
          outcomes = player.outcomes.take(no)
        )
      }

      val playerBtssScores = OffBtss.PlayerBtssScores(playerDeadlineRound.map(OffBtss.PlayerBtssScore(_)))
      val newPlayerBtssScores = contest.btsss.foldLeft(playerBtssScores) {
        case (old, btss) => btss.score(boards, old)
      }

      val scoreSheets = newPlayerBtssScores.sort.zipWithIndex.map {
        case (playerBtssScore, i) => OffScoreSheet(
          id = OffScoreSheet.makeId(playerBtssScore.player.id, no),
          contestId = playerBtssScore.player.contestId,
          roundNo = no,
          playerUid = playerBtssScore.player.userId,
          playerNo = playerBtssScore.player.no,
          score = playerBtssScore.player.score,
          rank = i + 1,
          btssScores = playerBtssScore.btsss
        )
      }
      OffScoreSheetRepo.bulkInsert(contest.id, scoreSheets)
    }
  }

  def manualAbsent(contest: OffContest, round: OffRound, joins: List[OffPlayer.ID], absents: List[OffPlayer.ID]): Funit = {
    lg(contest, round.some, "手动弃权", s"Joins: $joins, Absents: $absents".some)
    OffPlayerRepo.byIds(joins).flatMap { players =>
      players.filter(p => p.absent && p.manualAbsent && !p.absentOr && p.roundOutcome(round.no).??(_ == OffBoard.Outcome.ManualAbsent)).map { player =>
        OffPlayerRepo.update(
          player.copy(
            absent = false,
            manualAbsent = false,
            outcomes = player.removeOutcomeByRound(round.no)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    } >> OffPlayerRepo.byIds(absents).flatMap { players =>
      players.filter(p => !p.manualAbsent && p.roundOutcome(round.no).isEmpty).map { player =>
        OffPlayerRepo.update(
          player.copy(
            absent = true,
            manualAbsent = true,
            outcomes = player.setOutcomeByRound(round.no, OffBoard.Outcome.ManualAbsent)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        )
      }.sequenceFu.void
    }
  }

  def manualResult(contest: OffContest, board: OffBoard, r: String): Funit = {
    val result = OffBoard.Result(r)
    for {
      whiteOption <- OffPlayerRepo.byId(board.whitePlayer.id)
      blackOption <- OffPlayerRepo.byId(board.blackPlayer.id)
    } yield (whiteOption |@| blackOption).tupled ?? {
      case (white, black) => {

        val whiteOutcome = result match {
          case OffBoard.Result.WhiteWin => OffBoard.Outcome.Win
          case OffBoard.Result.BlackWin => OffBoard.Outcome.Loss
          case OffBoard.Result.Draw => OffBoard.Outcome.Draw
          case OffBoard.Result.BlackAbsent => OffBoard.Outcome.Win
          case OffBoard.Result.WhiteAbsent => OffBoard.Outcome.NoStart
          case OffBoard.Result.AllAbsent => OffBoard.Outcome.NoStart
        }

        val blackOutcome = result match {
          case OffBoard.Result.WhiteWin => OffBoard.Outcome.Loss
          case OffBoard.Result.BlackWin => OffBoard.Outcome.Win
          case OffBoard.Result.Draw => OffBoard.Outcome.Draw
          case OffBoard.Result.BlackAbsent => OffBoard.Outcome.NoStart
          case OffBoard.Result.WhiteAbsent => OffBoard.Outcome.Win
          case OffBoard.Result.AllAbsent => OffBoard.Outcome.NoStart
        }

        val winner = result match {
          case OffBoard.Result.WhiteWin | OffBoard.Result.BlackAbsent => chess.Color.White.some
          case OffBoard.Result.BlackWin | OffBoard.Result.WhiteAbsent => chess.Color.Black.some
          case OffBoard.Result.Draw | OffBoard.Result.AllAbsent => None
        }

        val newWhite = white.manualResult(board.roundNo, whiteOutcome, contest.isRoundRobin)
        val newBlack = black.manualResult(board.roundNo, blackOutcome, contest.isRoundRobin)
        OffPlayerRepo.update(newWhite) >>
          OffPlayerRepo.update(newBlack) >>
          OffBoardRepo.setResult(board.id, winner, result)
      }
    }
  }

  def manualPairing(contest: OffContest, data: ManualPairing, round: OffRound): Funit = {
    lg(contest, round.some, "手动匹配", s"source: ${data.source}, target: ${data.target}".some)
    if (!data.source.isBye_ && !data.target.isBye_) {
      for {
        sourceBoardOption <- data.source.board.??(OffBoardRepo.byId)
        targetBoardOption <- data.target.board.??(OffBoardRepo.byId)
      } yield {
        val sourceBoard = sourceBoardOption.err(s"can find board ${data.source.board_}")
        val targetBoard = targetBoardOption.err(s"can find board ${data.target.board_}")
        val sourceWhite = data.source.color_ == 1
        val targetWhite = data.target.color_ == 1
        val sourcePlayer = sourceBoard.player(chess.Color(sourceWhite))
        val targetPlayer = targetBoard.player(chess.Color(targetWhite))
        if (sourceBoard.is(targetBoard)) {
          OffBoardRepo.update(
            sourceBoard.copy(
              whitePlayer = sourceBoard.blackPlayer,
              blackPlayer = sourceBoard.whitePlayer
            )
          )
        } else {
          val b1 = if (sourceWhite) sourceBoard.copy(whitePlayer = targetPlayer) else sourceBoard.copy(blackPlayer = targetPlayer)
          val b2 = if (targetWhite) targetBoard.copy(whitePlayer = sourcePlayer) else targetBoard.copy(blackPlayer = sourcePlayer)
          OffBoardRepo.update(b1) >> OffBoardRepo.update(b2)
        }
      }
    } else if (data.source.isBye_ && !data.target.isBye_) {
      for {
        sourcePlayerOption <- data.source.player.??(OffPlayerRepo.byId)
        targetBoardOption <- data.target.board.??(OffBoardRepo.byId)
        targetPlayerOption <- targetBoardOption.??(b => OffPlayerRepo.byId(b.player(chess.Color(data.target.color_ == 1)).id))
      } yield {
        val sourcePlayer = sourcePlayerOption.err(s"can find board ${data.source.board_}")
        val targetPlayer = targetPlayerOption.err(s"can find board ${data.source.board_}")
        val targetBoard = targetBoardOption.err(s"can find board ${data.target.board_}")
        val targetWhite = data.target.color_ == 1
        val newTargetBoard =
          if (targetWhite) {
            targetBoard.copy(whitePlayer = OffBoard.MiniPlayer(sourcePlayer.id, sourcePlayer.userId, sourcePlayer.no, None))
          } else targetBoard.copy(blackPlayer = OffBoard.MiniPlayer(sourcePlayer.id, sourcePlayer.userId, sourcePlayer.no, None))

        OffPlayerRepo.update(
          sourcePlayer.copy(
            outcomes = sourcePlayer.removeOutcomeByRound(round.no)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        ) >> OffPlayerRepo.update(
            targetPlayer.copy(
              outcomes = targetPlayer.setOutcomeByRound(round.no, OffBoard.Outcome.Bye)
            ) |> { player =>
                player.copy(
                  score = player.allScore(contest.isRoundRobin),
                  points = player.allScore(contest.isRoundRobin)
                )
              }
          ) >> OffBoardRepo.update(newTargetBoard)
      }
    } else if (!data.source.isBye_ && data.target.isBye_) {
      for {
        sourceBoardOption <- data.source.board.??(OffBoardRepo.byId)
        sourcePlayerOption <- sourceBoardOption.??(b => OffPlayerRepo.byId(b.player(chess.Color(data.source.color_ == 1)).id))
        targetPlayerOption <- data.target.player.??(OffPlayerRepo.byId)
      } yield {
        val sourcePlayer = sourcePlayerOption.err(s"can find board ${data.source.board_}")
        val targetPlayer = targetPlayerOption.err(s"can find board ${data.source.board_}")
        val sourceBoard = sourceBoardOption.err(s"can find board ${data.target.board_}")
        val sourceWhite = data.source.color_ == 1
        val newSourceBoard =
          if (sourceWhite) {
            sourceBoard.copy(whitePlayer = OffBoard.MiniPlayer(targetPlayer.id, targetPlayer.userId, targetPlayer.no, None))
          } else sourceBoard.copy(blackPlayer = OffBoard.MiniPlayer(targetPlayer.id, targetPlayer.userId, targetPlayer.no, None))

        OffPlayerRepo.update(
          sourcePlayer.copy(
            outcomes = sourcePlayer.setOutcomeByRound(round.no, OffBoard.Outcome.Bye)
          ) |> { player =>
              player.copy(
                score = player.allScore(contest.isRoundRobin),
                points = player.allScore(contest.isRoundRobin)
              )
            }
        ) >> OffPlayerRepo.update(
            targetPlayer.copy(
              outcomes = targetPlayer.removeOutcomeByRound(round.no)
            ) |> { player =>
                player.copy(
                  score = player.allScore(contest.isRoundRobin),
                  points = player.allScore(contest.isRoundRobin)
                )
              }
          ) >> OffBoardRepo.update(newSourceBoard)
      }
    } else funit
  }

  private def lg(contest: OffContest, round: Option[OffRound], title: String, additional: Option[String], warn: Boolean = false) = {
    val message = s"[$contest]${round.?? { r => s" - [第 ${r.no} 轮]" }} - [$title] ${additional | ""}"
    if (warn) logger.warn(message)
    else logger.info(message)
  }

}
