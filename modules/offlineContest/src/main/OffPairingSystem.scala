package lila.offlineContest

final class OffPairingSystem(
    swissPairing: OffSwissPairing,
    roundRobinPairing: OffRoundRobinPairing
) {

  import OffPairingSystem._

  def apply(contest: OffContest, random: Boolean = false): Fu[List[ByeOrPending]] =
    contest.rule match {
      case OffContest.Rule.Swiss => swissPairing.pairing(contest)
      case OffContest.Rule.RoundRobin => roundRobinPairing.pairing(contest)
      case _ => fuccess(Nil)
    }

  def pairingAll(contest: OffContest, players: List[OffPlayer]): Fu[Map[Int, List[ByeOrPending]]] =
    contest.rule match {
      case OffContest.Rule.Swiss => swissPairing.pairingAll(contest, players)
      case OffContest.Rule.RoundRobin => roundRobinPairing.pairingAll(contest, players)
      case OffContest.Rule.DBRoundRobin => roundRobinPairing.pairingAll(contest, players)
      case _ => fuccess(Map.empty[Int, List[ByeOrPending]])
    }

}

object OffPairingSystem {

  type ByeOrPending = Either[Bye, Pending]

  case class Pending(
      white: OffPlayer.No,
      black: OffPlayer.No
  )
  case class Bye(player: OffPlayer.No)

}
