package lila.offlineContest

import org.joda.time.DateTime
import ornicar.scalalib.Random
import lila.user.User

case class OffContest(
    id: OffContest.ID,
    name: String,
    groupName: Option[String],
    logo: Option[String],
    typ: OffContest.Type,
    teamRated: Boolean,
    organizer: String, // teamid / classid
    rule: OffContest.Rule,
    rounds: Int,
    swissBtss: OffBtsss,
    roundRobinBtss: OffBtsss,
    nbPlayers: Int = 0,
    currentRound: Int = 1, // Round PublishResult 之后更新此字段
    roundRobinPairing: Option[Boolean] = None,
    status: OffContest.Status = OffContest.Status.Created,
    createdBy: User.ID,
    createdAt: DateTime
) {

  def fullName = s"$name${groupName.fold("") { " " + _ }}"

  def isCreated = status == OffContest.Status.Created
  def isStarted = status == OffContest.Status.Started
  def isFinished = status == OffContest.Status.Finished
  def isCanceled = status == OffContest.Status.Canceled
  def isOverStarted = status >= OffContest.Status.Started
  def isFinishedOrCanceled = isFinished || isCanceled

  def isCreator(user: User) = user.id == createdBy

  def isPlayerFull = nbPlayers >= rule.setup.maxPlayer

  def isRoundRobin = rule == OffContest.Rule.RoundRobin || rule == OffContest.Rule.DBRoundRobin
  def isDbRoundRobin = rule == OffContest.Rule.DBRoundRobin
  def roundRobinPairingOr = roundRobinPairing | false

  def chooseable = isCreated || (isStarted && (!isRoundRobin || (isRoundRobin && !roundRobinPairingOr)))

  def playerRemoveable = isCreated || (isStarted && (isRoundRobin && !roundRobinPairingOr))

  def playerKickable = isCreated || isStarted

  def historyRounds = (1 to currentRound - 1).toList

  def robinRoundsByPlayer =
    if (nbPlayers == 0) rounds
    else if (nbPlayers % 2 == 0) nbPlayers - 1 else nbPlayers

  def robinRoundsByPlayerOfRule =
    rule match {
      case OffContest.Rule.RoundRobin => robinRoundsByPlayer
      case OffContest.Rule.DBRoundRobin => robinRoundsByPlayer * 2
      case _ => -1
    }

  def actualRound =
    rule match {
      //case Contest.Rule.Auto => Math.min(rounds, autoRound)
      case OffContest.Rule.Swiss => rounds
      case OffContest.Rule.RoundRobin => if (roundRobinPairingOr) Math.min(robinRoundsByPlayer, rounds) else rounds
      case OffContest.Rule.DBRoundRobin => if (roundRobinPairingOr) Math.min(robinRoundsByPlayer * 2, rounds) else rounds
    }

  def btsss: List[OffBtss] = {
    rule match {
      //      case OffContest.Rule.Auto => {
      //        if (nbPlayers <= 3) roundRobinBtss.list
      //        else if (nbPlayers <= 6) roundRobinBtss.list
      //        else swissBtss.list
      //      }
      case OffContest.Rule.Swiss => swissBtss.list
      case OffContest.Rule.RoundRobin => roundRobinBtss.list
      case OffContest.Rule.DBRoundRobin => roundRobinBtss.list
    }
  } :+ OffBtss.No

  override def toString = s"$fullName $id（${typ.name}）"
}

object OffContest {

  type ID = String

  def make(
    by: User.ID,
    name: String,
    groupName: Option[String],
    logo: Option[String],
    typ: OffContest.Type,
    teamRated: Boolean,
    organizer: String, // teamid / classid
    rule: OffContest.Rule,
    rounds: Int,
    swissBtss: OffBtsss,
    roundRobinBtss: OffBtsss
  ) = OffContest(
    id = makeId,
    name = name,
    groupName = groupName,
    logo = logo,
    typ = typ,
    teamRated = teamRated,
    organizer = organizer,
    rule = rule,
    rounds = rounds,
    swissBtss = swissBtss,
    roundRobinBtss = roundRobinBtss,
    createdBy = by,
    createdAt = DateTime.now
  )

  def makeId = Random nextString 8

  sealed abstract class Status(val id: Int, val name: String) extends Ordered[Status] {
    def compare(other: Status) = Integer.compare(id, other.id)
    def is(s: Status): Boolean = this == s
    def is(f: Status.type => Status): Boolean = is(f(Status))
  }
  object Status {
    case object Created extends Status(10, "筹备中")
    case object Started extends Status(20, "比赛中")
    case object Finished extends Status(50, "比赛结束")
    case object Canceled extends Status(60, "比赛取消")

    val all = List(Created, Started, Finished, Canceled)
    val current = List(Created, Started)
    val history = List(Finished, Canceled)

    def byId = all map { v => (v.id, v) } toMap
    def apply(id: Int): Status = byId get id err s"Bad Status $id"

    val empty = none -> "所有"
    def allChoice = empty :: all.map(s => s.id.some -> s.name)
    def currentChoice = empty :: current.map(s => s.id.some -> s.name)
    def historyChoice = empty :: history.map(s => s.id.some -> s.name)
  }

  sealed abstract class Type(val id: String, val name: String)
  object Type {
    case object Public extends Type("public", "公开赛")
    case object TeamInner extends Type("team-inner", "俱乐部内部赛")
    case object ClazzInner extends Type("clazz-inner", "班级内部赛")

    val all = List(Public, TeamInner, ClazzInner)

    def apply(id: String) = all.find(_.id == id) err s"Bad Type $id"

    def keySet = all.map(_.id).toSet

    def list = all.map { r => r.id -> r.name }

    def byId = all.map { x => x.id -> x }.toMap
  }

  sealed abstract class Rule(val id: String, val name: String, val setup: RuleSetup, val flow: RuleFlow)
  object Rule {
    //case object Auto extends Rule("auto", "人数自适应", 200)
    case object Swiss extends Rule("swiss", "瑞士制",
      RuleSetup("swissBtss", 16, 5, 2, 200, 50, OffBtsss.swiss),
      RuleFlow(forbidden = true, roundEdit = false, manualAbsent = true, roundPairing = true, roundSwap = false))
    case object RoundRobin extends Rule("round-robin", "单循环",
      RuleSetup("roundRobinBtss", 21, 5, 2, 22, 22, OffBtsss.roundRobin),
      RuleFlow(forbidden = false, roundEdit = true, manualAbsent = false, roundPairing = false, roundSwap = true))
    case object DBRoundRobin extends Rule("db-round-robin", "双循环",
      RuleSetup("roundRobinBtss", 22, 6, 2, 12, 12, OffBtsss.roundRobin),
      RuleFlow(forbidden = false, roundEdit = true, manualAbsent = false, roundPairing = false, roundSwap = true))

    val all = List(Swiss, RoundRobin, DBRoundRobin)

    val byId = all map { v => (v.id, v) } toMap

    def apply(id: String): Rule = byId get id err s"Bad Rule $id"

    def list = all.map { r => r.id -> r.name }
  }

  case class RuleSetup(formField: String, maxRound: Int, defaultRound: Int, minPlayer: Int, maxPlayer: Int, defaultPlayer: Int, btsss: OffBtsss)
  case class RuleFlow(forbidden: Boolean, roundEdit: Boolean, manualAbsent: Boolean, roundPairing: Boolean, roundSwap: Boolean)
}
