package lila.offlineContest

import OffPairingSystem.ByeOrPending

trait OffPairing {

  def pairing(contest: OffContest): Fu[List[ByeOrPending]]

  def pairingAll(contest: OffContest, players: List[OffPlayer]): Fu[Map[Int, List[ByeOrPending]]]

}
