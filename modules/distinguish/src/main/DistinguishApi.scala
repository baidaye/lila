package lila.distinguish

import chess.format.FEN
import lila.db.dsl._
import lila.user.User
import lila.game.{ Game, GameRepo, PgnDump }
import lila.importer.ImportData
import lila.common.paginator.Paginator
import lila.db.paginator.Adapter
import lila.common.MaxPerPage
import chess.format.Forsyth
import scalaz.{ Failure, Success }

case class GamePgnResult(name: String, fen: String, pgn: String)
final class DistinguishApi(
    coll: Coll,
    bus: lila.common.Bus,
    studyApi: lila.study.StudyApi,
    importer: lila.importer.Importer,
    pgnDump: lila.game.PgnDump
) {

  import BSONHandlers._

  def byId(id: Distinguish.ID): Fu[Option[Distinguish]] = coll.byId[Distinguish](id)

  def page(userId: User.ID, page: Int): Fu[Paginator[Distinguish]] = {
    val adapter = new Adapter[Distinguish](
      collection = coll,
      selector = $doc(
        "deleted" -> false,
        "createBy" -> userId
      ),
      projection = $empty,
      sort = $sort desc "createAt"
    )
    Paginator(
      adapter = adapter,
      currentPage = page,
      maxPerPage = MaxPerPage(15)
    )
  }

  def history(userId: User.ID): Fu[List[Distinguish]] =
    coll.find(
      $doc(
        "deleted" -> false,
        "createBy" -> userId
      )
    ).sort($sort desc "createAt").list(20)

  def createBy(distinguish: Distinguish): Fu[Distinguish] =
    coll.insert(distinguish) inject distinguish

  def create(data: DistinguishData, userId: User.ID): Fu[Distinguish] = {
    data.tab match {
      case "classic" => data.classic match {
        case Some(gameId) => fuccess(makeDistinguish(data, userId, gameId))
        case None => fufail("no available classic")
      }
      case "gamedb" => data.gamedb match {
        case Some(gameId) => fuccess(makeDistinguish(data, userId, gameId))
        case None => fufail("no available gamedb")
      }
      case "pgn" => data.pgn match {
        case Some(pgn) => addGame(pgn, userId).map { game =>
          makeDistinguish(data, userId, game.id)
        }
        case None => fufail("no available pgn")
      }
      case "chapter" => data.chapter match {
        case Some(chapter) => fetchChapterPgn(data.studyId(chapter), data.chapterId(chapter)) flatMap {
          case None => fufail("no available chapter")
          case Some(p) => addGame(p, userId).map { game =>
            makeDistinguish(data, userId, game.id)
          }
        }
        case None => fufail("no available chapter")
      }
      case "game" => data.game match {
        case Some(g) => fuccess(makeDistinguish(data, userId, data.gameId(g)))
        case None => fufail("no available game")
      }
      case _ => fufail("can not apply tab")
    }
  } flatMap { distinguish =>
    coll.insert(distinguish) inject distinguish
  }

  private def fetchChapterPgn(studyId: String, chapterId: String): Fu[Option[String]] =
    studyApi.chapterPgn(studyId, chapterId)

  private def addGame(pgn: String, userId: User.ID): Fu[Game] =
    importer(
      ImportData(pgn, none),
      user = userId.some
    )

  def gamePgn(data: DistinguishData, fetchCourseWarePgn: (String, String) => Fu[Option[(String, String)]]): Fu[GamePgnResult] = {
    data.tab match {
      case "classic" => data.classic match {
        case Some(gameId) => GameRepo.gameWithInitialFen(gameId) flatMap {
          case None => fufail("no available game")
          case Some((g, initialFen)) => toPgn(g, initialFen)
        }
        case None => fufail("no available classic")
      }
      case "gamedb" => data.gamedb match {
        case Some(gameId) => GameRepo.gameWithInitialFen(gameId) flatMap {
          case None => fufail("no available game")
          case Some((g, initialFen)) => toPgn(g, initialFen)
        }
        case None => fufail("no available gamedb")
      }
      case "pgn" => data.pgn match {
        case Some(pgn) => validToPgn(pgn)
        case None => fufail("no available pgn")
      }
      case "chapter" => data.chapter match {
        case Some(chapter) => fetchChapterPgn(data.studyId(chapter), data.chapterId(chapter)) flatMap {
          case None => fufail("no available chapter")
          case Some(p) => validToPgn(p, true)
        }
        case None => fufail("no available chapter")
      }
      case "game" => data.game match {
        case Some(url) => GameRepo.gameWithInitialFen(data.gameId(url)) flatMap {
          case None => fufail("no available game")
          case Some((g, initialFen)) => toPgn(g, initialFen)
        }
        case None => fufail("no available game")
      }
      case "courseWare" => data.courseWare match {
        case Some(courseWareData) => {
          val arr = courseWareData.split(":")
          fetchCourseWarePgn(arr(0), arr(1)) flatMap {
            case None => fufail("no available courseWare")
            case Some(p) => validToPgn(p._2, true).map { r => r.copy(name = p._1) }
          }
        }
        case None => fufail("no available courseWare")
      }
      case _ => fufail("can not apply tab")
    }
  }

  private def validToPgn(pgn: String, study: Boolean = false): Fu[GamePgnResult] =
    ImportData(pgn, none).preprocess(user = none) match {
      case Success(p) => toPgn(p.game.withId("-"), p.initialFen, study)
      case Failure(e) => fufail(e.toString())
    }

  private def toPgn(game: Game, initialFen: Option[FEN], study: Boolean = false): Fu[GamePgnResult] =
    pgnDump(
      game,
      initialFen,
      PgnDump.WithFlags(clocks = false, evals = false, opening = false)
    ).map { dump =>
        GamePgnResult(
          if (study) dump.tags(_.Event) | "" else s"${dump.tags(_.White) | ""} vs ${dump.tags(_.Black) | ""}",
          initialFen.map(_.value) | Forsyth.initial,
          dump.toString
        )
      }

  def update(distinguish: Distinguish, data: DistinguishEdit): Funit =
    coll.update(
      $id(distinguish.id),
      distinguish.copy(
        name = data.name,
        turns = data.turns,
        orientation = chess.Color(data.orientation)
      )
    ).void

  def delete(distinguish: Distinguish): Funit =
    coll.update(
      $id(distinguish.id),
      $set("deleted" -> true)
    ).void

  def finish(id: Option[Distinguish.ID], hashId: Option[String], win: Boolean, turns: Int, userId: User.ID): Funit = {
    bus.publish(lila.hub.actorApi.Distinguish(id, hashId, win, turns, userId), 'distinguishFinished)
    funit
  }

  private def makeDistinguish(data: DistinguishData, userId: User.ID, gameId: String): Distinguish =
    Distinguish.make(
      name = data.name,
      gameId = gameId,
      turns = data.turns,
      orientation = chess.Color(data.orientation),
      userId = userId
    )

}
