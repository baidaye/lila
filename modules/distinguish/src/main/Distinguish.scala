package lila.distinguish

import lila.game.Game
import lila.user.User
import org.joda.time.DateTime
import ornicar.scalalib.Random

case class Distinguish(
    _id: Distinguish.ID,
    name: String,
    gameId: Game.ID,
    turns: Option[Int],
    orientation: Option[chess.Color],
    readonly: Boolean,
    deleted: Boolean,
    createBy: User.ID,
    createAt: DateTime
) {

  def id = _id

  def isCreator(user: String) = user == createBy

}

object Distinguish {

  type ID = String

  def make(
    name: String,
    gameId: Game.ID,
    turns: Option[Int],
    orientation: Option[chess.Color],
    userId: User.ID,
    readonly: Boolean = false
  ) = Distinguish(
    _id = Random nextString 8,
    name = name,
    gameId = gameId,
    turns = turns,
    orientation = orientation,
    readonly = readonly,
    deleted = false,
    createBy = userId,
    createAt = DateTime.now
  )

  def makeSyntheticDistinguish =
    Distinguish(
      _id = "synthetic",
      name = "棋谱记录",
      gameId = "synthetic",
      turns = None,
      orientation = None,
      readonly = false,
      deleted = false,
      createBy = User.lichessId,
      createAt = DateTime.now
    )

  def makeTemporaryDistinguish(turns: Option[Int], orientation: Option[String], title: Option[String]) =
    Distinguish(
      _id = "temporary",
      name = title | "棋谱记录",
      gameId = "temporary",
      turns = turns,
      orientation = orientation.??(chess.Color(_)),
      readonly = true,
      deleted = true,
      createBy = User.lichessId,
      createAt = DateTime.now
    )

  case class Classic(gameId: String, title: String, full: String)
  object Classic {
    import play.api.libs.json._
    def toJson = JsArray(classics.map(c => Json.obj("id" -> c.gameId, "name" -> c.title)))

    val classics = List(
      Classic("lHn2BBSD", "马格努斯（卡尔森）效应\nCarlsen vs S Ernst, 2004 \n(B18) Caro-Kann, Classical, 29 moves, 1-0", "Magnus Effect\nCarlsen vs S Ernst, 2004 \n(B18) Caro-Kann, Classical, 29 moves, 1-0"),
      Classic("DGXQ0nGx", "23世纪对局\nViswanathan Anand vs Veselin Topalov,2005\n(B80) Sicilian Defense, Scheveningen Variation, English Attack , 60 moves,  1/2-1/2", "23rd Century Chess.\nViswanathan Anand vs Veselin Topalov,2005\n(B80) Sicilian Defense, Scheveningen Variation, English Attack , 60 moves,  1/2-1/2"),
      Classic("RsqjBgHr", "卡斯帕罗夫不朽对局\nKasparov vs Topalov, 1999 \n(B07) Pirc, 44 moves, 1-0", "Kasparov's Immortal\nKasparov vs Topalov, 1999 \n(B07) Pirc, 44 moves, 1-0"),
      Classic("sbFy5Nl9", "菲舍尔不朽对局-世纪对局\nD Byrne vs Fischer, 1956  \n(D92) Grunfeld, 5.Bf4, 41 moves, 0-1", "Fischer's Immortal-The Game Of The Century.\nD Byrne vs Fischer, 1956  \n(D92) Grunfeld, 5.Bf4, 41 moves, 0-1"),
      Classic("aR4gQATY", "纳恩不朽对局\nBeliavsky vs Nunn, 1985 \n(E81) King's Indian, Samisch, 27 moves, 0-1", "Nunn's Immortal\nBeliavsky vs Nunn, 1985 \n(E81) King's Indian, Samisch, 27 moves, 0-1"),
      Classic("0xqyDkGz", "尤素波夫不朽对局\nIvanchuk vs Yusupov, 1991 \n(E67) King's Indian, Fianchetto, 39 moves, 0-1", "Yusupov's Immortal\nIvanchuk vs Yusupov, 1991 \n(E67) King's Indian, Fianchetto, 39 moves, 0-1"),
      Classic("RHGHbhmJ", "古菲尔德不朽对局\nBagirov vs Gufeld, 1973 \n(E84) King's Indian, Samisch, Panno Main line, 32 moves, 0-1", "Gufeld's Immortal\nBagirov vs Gufeld, 1973 \n(E84) King's Indian, Samisch, Panno Main line, 32 moves, 0-1"),
      Classic("h3swTO9j", "塔尔不朽对局\nTal vs Larsen, 1965  \n(B82) Sicilian, Scheveningen, 37 moves, 1-0", "Tal's Immortal\nTal vs Larsen, 1965  \n(B82) Sicilian, Scheveningen, 37 moves, 1-0"),
      Classic("JarJYTQ5", "中村光不朽对局\nKrasenkow vs Nakamura, 2007 \n(A14) English, 28 moves, 0-1", "The Immortal Nakamura!\nKrasenkow vs Nakamura, 2007 \n(A14) English, 28 moves, 0-1"),
      Classic("9iTEonhR", "万古长青对局\nAnderssen vs Dufresne, 1852 \n(C52) Evans Gambit, 24 moves, 1-0", "The Evergreen Game.\nAnderssen vs Dufresne, 1852 \n(C52) Evans Gambit, 24 moves, 1-0")
    )
  }

}
