package lila.distinguish

import play.api.libs.json._

final class JsonView(
    gameJson: GameJson,
    animationDuration: scala.concurrent.duration.Duration
) {

  def apply(distinguish: Distinguish, history: List[Distinguish], pgn: Option[String] = None): Fu[JsObject] =
    gameJson(distinguish.gameId, pgn).map { gameJs =>
      Json.obj(
        "game" -> gameJs,
        "distinguish" -> Json.obj(
          "id" -> distinguish.id,
          "name" -> distinguish.name,
          "readonly" -> distinguish.readonly
        ).add("turns" -> distinguish.turns)
          .add("orientation" -> distinguish.orientation.map(_.name)),
        "history" -> historyJson(history)
      )
    }

  def pref(p: lila.pref.Pref) = Json.obj(
    "blindfold" -> p.blindfold,
    "coords" -> p.coords,
    "rookCastle" -> p.rookCastle,
    "animation" -> Json.obj(
      "duration" -> p.animationFactor * animationDuration.toMillis
    ),
    "destination" -> p.destination,
    "resizeHandle" -> p.resizeHandle,
    "moveEvent" -> p.moveEvent,
    "highlight" -> p.highlight,
    "is3d" -> p.is3d
  )

  def historyJson(replays: List[Distinguish]) =
    JsArray(
      replays.map { replay =>
        Json.obj(
          "id" -> replay.id,
          "name" -> replay.name
        )
      }
    )

}

